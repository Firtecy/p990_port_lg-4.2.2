.class public Lcom/android/internal/util/FileRotator;
.super Ljava/lang/Object;
.source "FileRotator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/util/FileRotator$FileInfo;,
        Lcom/android/internal/util/FileRotator$Rewriter;,
        Lcom/android/internal/util/FileRotator$Writer;,
        Lcom/android/internal/util/FileRotator$Reader;
    }
.end annotation


# static fields
.field private static final LOGD:Z = false

.field private static final SUFFIX_BACKUP:Ljava/lang/String; = ".backup"

.field private static final SUFFIX_NO_BACKUP:Ljava/lang/String; = ".no_backup"

.field private static final TAG:Ljava/lang/String; = "FileRotator"


# instance fields
.field private final mBasePath:Ljava/io/File;

.field private final mDeleteAgeMillis:J

.field private final mPrefix:Ljava/lang/String;

.field private final mRotateAgeMillis:J


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;JJ)V
    .registers 20
    .parameter "basePath"
    .parameter "prefix"
    .parameter "rotateAgeMillis"
    .parameter "deleteAgeMillis"

    #@0
    .prologue
    .line 98
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 99
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v9

    #@7
    check-cast v9, Ljava/io/File;

    #@9
    iput-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@b
    .line 100
    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v9

    #@f
    check-cast v9, Ljava/lang/String;

    #@11
    iput-object v9, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@13
    .line 101
    move-wide/from16 v0, p3

    #@15
    iput-wide v0, p0, Lcom/android/internal/util/FileRotator;->mRotateAgeMillis:J

    #@17
    .line 102
    move-wide/from16 v0, p5

    #@19
    iput-wide v0, p0, Lcom/android/internal/util/FileRotator;->mDeleteAgeMillis:J

    #@1b
    .line 105
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@1d
    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    #@20
    .line 108
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@22
    invoke-virtual {v9}, Ljava/io/File;->list()[Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .local v2, arr$:[Ljava/lang/String;
    array-length v6, v2

    #@27
    .local v6, len$:I
    const/4 v5, 0x0

    #@28
    .local v5, i$:I
    :goto_28
    if-ge v5, v6, :cond_8e

    #@2a
    aget-object v7, v2, v5

    #@2c
    .line 109
    .local v7, name:Ljava/lang/String;
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@2e
    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@31
    move-result v9

    #@32
    if-nez v9, :cond_37

    #@34
    .line 108
    :cond_34
    :goto_34
    add-int/lit8 v5, v5, 0x1

    #@36
    goto :goto_28

    #@37
    .line 111
    :cond_37
    const-string v9, ".backup"

    #@39
    invoke-virtual {v7, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@3c
    move-result v9

    #@3d
    if-eqz v9, :cond_61

    #@3f
    .line 114
    new-instance v3, Ljava/io/File;

    #@41
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@43
    invoke-direct {v3, v9, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@46
    .line 115
    .local v3, backupFile:Ljava/io/File;
    new-instance v4, Ljava/io/File;

    #@48
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@4a
    const/4 v10, 0x0

    #@4b
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@4e
    move-result v11

    #@4f
    const-string v12, ".backup"

    #@51
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@54
    move-result v12

    #@55
    sub-int/2addr v11, v12

    #@56
    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@59
    move-result-object v10

    #@5a
    invoke-direct {v4, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@5d
    .line 119
    .local v4, file:Ljava/io/File;
    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@60
    goto :goto_34

    #@61
    .line 121
    .end local v3           #backupFile:Ljava/io/File;
    .end local v4           #file:Ljava/io/File;
    :cond_61
    const-string v9, ".no_backup"

    #@63
    invoke-virtual {v7, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@66
    move-result v9

    #@67
    if-eqz v9, :cond_34

    #@69
    .line 124
    new-instance v8, Ljava/io/File;

    #@6b
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@6d
    invoke-direct {v8, v9, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@70
    .line 125
    .local v8, noBackupFile:Ljava/io/File;
    new-instance v4, Ljava/io/File;

    #@72
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@74
    const/4 v10, 0x0

    #@75
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@78
    move-result v11

    #@79
    const-string v12, ".no_backup"

    #@7b
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@7e
    move-result v12

    #@7f
    sub-int/2addr v11, v12

    #@80
    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@83
    move-result-object v10

    #@84
    invoke-direct {v4, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@87
    .line 129
    .restart local v4       #file:Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    #@8a
    .line 130
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@8d
    goto :goto_34

    #@8e
    .line 133
    .end local v4           #file:Ljava/io/File;
    .end local v7           #name:Ljava/lang/String;
    .end local v8           #noBackupFile:Ljava/io/File;
    :cond_8e
    return-void
.end method

.method private getActiveName(J)Ljava/lang/String;
    .registers 13
    .parameter "currentTimeMillis"

    #@0
    .prologue
    .line 305
    const/4 v5, 0x0

    #@1
    .line 306
    .local v5, oldestActiveName:Ljava/lang/String;
    const-wide v6, 0x7fffffffffffffffL

    #@6
    .line 308
    .local v6, oldestActiveStart:J
    new-instance v2, Lcom/android/internal/util/FileRotator$FileInfo;

    #@8
    iget-object v8, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@a
    invoke-direct {v2, v8}, Lcom/android/internal/util/FileRotator$FileInfo;-><init>(Ljava/lang/String;)V

    #@d
    .line 309
    .local v2, info:Lcom/android/internal/util/FileRotator$FileInfo;
    iget-object v8, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@f
    invoke-virtual {v8}, Ljava/io/File;->list()[Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@14
    .local v3, len$:I
    const/4 v1, 0x0

    #@15
    .local v1, i$:I
    :goto_15
    if-ge v1, v3, :cond_38

    #@17
    aget-object v4, v0, v1

    #@19
    .line 310
    .local v4, name:Ljava/lang/String;
    invoke-virtual {v2, v4}, Lcom/android/internal/util/FileRotator$FileInfo;->parse(Ljava/lang/String;)Z

    #@1c
    move-result v8

    #@1d
    if-nez v8, :cond_22

    #@1f
    .line 309
    :cond_1f
    :goto_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_15

    #@22
    .line 313
    :cond_22
    invoke-virtual {v2}, Lcom/android/internal/util/FileRotator$FileInfo;->isActive()Z

    #@25
    move-result v8

    #@26
    if-eqz v8, :cond_1f

    #@28
    iget-wide v8, v2, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@2a
    cmp-long v8, v8, p1

    #@2c
    if-gez v8, :cond_1f

    #@2e
    iget-wide v8, v2, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@30
    cmp-long v8, v8, v6

    #@32
    if-gez v8, :cond_1f

    #@34
    .line 315
    move-object v5, v4

    #@35
    .line 316
    iget-wide v6, v2, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@37
    goto :goto_1f

    #@38
    .line 320
    .end local v4           #name:Ljava/lang/String;
    :cond_38
    if-eqz v5, :cond_3b

    #@3a
    .line 326
    .end local v5           #oldestActiveName:Ljava/lang/String;
    :goto_3a
    return-object v5

    #@3b
    .line 324
    .restart local v5       #oldestActiveName:Ljava/lang/String;
    :cond_3b
    iput-wide p1, v2, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@3d
    .line 325
    const-wide v8, 0x7fffffffffffffffL

    #@42
    iput-wide v8, v2, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@44
    .line 326
    invoke-virtual {v2}, Lcom/android/internal/util/FileRotator$FileInfo;->build()Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    goto :goto_3a
.end method

.method private static readFile(Ljava/io/File;Lcom/android/internal/util/FileRotator$Reader;)V
    .registers 5
    .parameter "file"
    .parameter "reader"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 364
    new-instance v1, Ljava/io/FileInputStream;

    #@2
    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@5
    .line 365
    .local v1, fis:Ljava/io/FileInputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    #@7
    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@a
    .line 367
    .local v0, bis:Ljava/io/BufferedInputStream;
    :try_start_a
    invoke-interface {p1, v0}, Lcom/android/internal/util/FileRotator$Reader;->read(Ljava/io/InputStream;)V
    :try_end_d
    .catchall {:try_start_a .. :try_end_d} :catchall_11

    #@d
    .line 369
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@10
    .line 371
    return-void

    #@11
    .line 369
    :catchall_11
    move-exception v2

    #@12
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@15
    throw v2
.end method

.method private static rethrowAsIoException(Ljava/lang/Throwable;)Ljava/io/IOException;
    .registers 3
    .parameter "t"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 386
    instance-of v0, p0, Ljava/io/IOException;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 387
    check-cast p0, Ljava/io/IOException;

    #@6
    .end local p0
    throw p0

    #@7
    .line 389
    .restart local p0
    :cond_7
    new-instance v0, Ljava/io/IOException;

    #@9
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@10
    throw v0
.end method

.method private rewriteSingle(Lcom/android/internal/util/FileRotator$Rewriter;Ljava/lang/String;)V
    .registers 9
    .parameter "rewriter"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 235
    new-instance v1, Ljava/io/File;

    #@2
    iget-object v3, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@4
    invoke-direct {v1, v3, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7
    .line 238
    .local v1, file:Ljava/io/File;
    invoke-interface {p1}, Lcom/android/internal/util/FileRotator$Rewriter;->reset()V

    #@a
    .line 240
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_4a

    #@10
    .line 242
    invoke-static {v1, p1}, Lcom/android/internal/util/FileRotator;->readFile(Ljava/io/File;Lcom/android/internal/util/FileRotator$Reader;)V

    #@13
    .line 245
    invoke-interface {p1}, Lcom/android/internal/util/FileRotator$Rewriter;->shouldWrite()Z

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_1a

    #@19
    .line 280
    :goto_19
    return-void

    #@1a
    .line 248
    :cond_1a
    new-instance v0, Ljava/io/File;

    #@1c
    iget-object v3, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, ".backup"

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@34
    .line 249
    .local v0, backupFile:Ljava/io/File;
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@37
    .line 252
    :try_start_37
    invoke-static {v1, p1}, Lcom/android/internal/util/FileRotator;->writeFile(Ljava/io/File;Lcom/android/internal/util/FileRotator$Writer;)V

    #@3a
    .line 255
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_3d
    .catch Ljava/lang/Throwable; {:try_start_37 .. :try_end_3d} :catch_3e

    #@3d
    goto :goto_19

    #@3e
    .line 256
    :catch_3e
    move-exception v2

    #@3f
    .line 258
    .local v2, t:Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@42
    .line 259
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@45
    .line 260
    invoke-static {v2}, Lcom/android/internal/util/FileRotator;->rethrowAsIoException(Ljava/lang/Throwable;)Ljava/io/IOException;

    #@48
    move-result-object v3

    #@49
    throw v3

    #@4a
    .line 265
    .end local v0           #backupFile:Ljava/io/File;
    .end local v2           #t:Ljava/lang/Throwable;
    :cond_4a
    new-instance v0, Ljava/io/File;

    #@4c
    iget-object v3, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    const-string v5, ".no_backup"

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v4

    #@61
    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@64
    .line 266
    .restart local v0       #backupFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    #@67
    .line 269
    :try_start_67
    invoke-static {v1, p1}, Lcom/android/internal/util/FileRotator;->writeFile(Ljava/io/File;Lcom/android/internal/util/FileRotator$Writer;)V

    #@6a
    .line 272
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_6d
    .catch Ljava/lang/Throwable; {:try_start_67 .. :try_end_6d} :catch_6e

    #@6d
    goto :goto_19

    #@6e
    .line 273
    :catch_6e
    move-exception v2

    #@6f
    .line 275
    .restart local v2       #t:Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@72
    .line 276
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@75
    .line 277
    invoke-static {v2}, Lcom/android/internal/util/FileRotator;->rethrowAsIoException(Ljava/lang/Throwable;)Ljava/io/IOException;

    #@78
    move-result-object v3

    #@79
    throw v3
.end method

.method private static writeFile(Ljava/io/File;Lcom/android/internal/util/FileRotator$Writer;)V
    .registers 5
    .parameter "file"
    .parameter "writer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 374
    new-instance v1, Ljava/io/FileOutputStream;

    #@2
    invoke-direct {v1, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@5
    .line 375
    .local v1, fos:Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/BufferedOutputStream;

    #@7
    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@a
    .line 377
    .local v0, bos:Ljava/io/BufferedOutputStream;
    :try_start_a
    invoke-interface {p1, v0}, Lcom/android/internal/util/FileRotator$Writer;->write(Ljava/io/OutputStream;)V

    #@d
    .line 378
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_10
    .catchall {:try_start_a .. :try_end_10} :catchall_17

    #@10
    .line 380
    invoke-static {v1}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@13
    .line 381
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@16
    .line 383
    return-void

    #@17
    .line 380
    :catchall_17
    move-exception v2

    #@18
    invoke-static {v1}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@1b
    .line 381
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@1e
    throw v2
.end method


# virtual methods
.method public combineActive(Lcom/android/internal/util/FileRotator$Reader;Lcom/android/internal/util/FileRotator$Writer;J)V
    .registers 6
    .parameter "reader"
    .parameter "writer"
    .parameter "currentTimeMillis"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 190
    new-instance v0, Lcom/android/internal/util/FileRotator$1;

    #@2
    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/util/FileRotator$1;-><init>(Lcom/android/internal/util/FileRotator;Lcom/android/internal/util/FileRotator$Reader;Lcom/android/internal/util/FileRotator$Writer;)V

    #@5
    invoke-virtual {p0, v0, p3, p4}, Lcom/android/internal/util/FileRotator;->rewriteActive(Lcom/android/internal/util/FileRotator$Rewriter;J)V

    #@8
    .line 211
    return-void
.end method

.method public deleteAll()V
    .registers 8

    #@0
    .prologue
    .line 139
    new-instance v2, Lcom/android/internal/util/FileRotator$FileInfo;

    #@2
    iget-object v5, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@4
    invoke-direct {v2, v5}, Lcom/android/internal/util/FileRotator$FileInfo;-><init>(Ljava/lang/String;)V

    #@7
    .line 140
    .local v2, info:Lcom/android/internal/util/FileRotator$FileInfo;
    iget-object v5, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@9
    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@e
    .local v3, len$:I
    const/4 v1, 0x0

    #@f
    .local v1, i$:I
    :goto_f
    if-ge v1, v3, :cond_26

    #@11
    aget-object v4, v0, v1

    #@13
    .line 141
    .local v4, name:Ljava/lang/String;
    invoke-virtual {v2, v4}, Lcom/android/internal/util/FileRotator$FileInfo;->parse(Ljava/lang/String;)Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_23

    #@19
    .line 143
    new-instance v5, Ljava/io/File;

    #@1b
    iget-object v6, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@1d
    invoke-direct {v5, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@20
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@23
    .line 140
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_f

    #@26
    .line 146
    .end local v4           #name:Ljava/lang/String;
    :cond_26
    return-void
.end method

.method public dumpAll(Ljava/io/OutputStream;)V
    .registers 12
    .parameter "os"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 152
    new-instance v8, Ljava/util/zip/ZipOutputStream;

    #@2
    invoke-direct {v8, p1}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@5
    .line 154
    .local v8, zos:Ljava/util/zip/ZipOutputStream;
    :try_start_5
    new-instance v4, Lcom/android/internal/util/FileRotator$FileInfo;

    #@7
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@9
    invoke-direct {v4, v9}, Lcom/android/internal/util/FileRotator$FileInfo;-><init>(Ljava/lang/String;)V

    #@c
    .line 155
    .local v4, info:Lcom/android/internal/util/FileRotator$FileInfo;
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@e
    invoke-virtual {v9}, Ljava/io/File;->list()[Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@13
    .local v6, len$:I
    const/4 v3, 0x0

    #@14
    .local v3, i$:I
    :goto_14
    if-ge v3, v6, :cond_48

    #@16
    aget-object v7, v0, v3

    #@18
    .line 156
    .local v7, name:Ljava/lang/String;
    invoke-virtual {v4, v7}, Lcom/android/internal/util/FileRotator$FileInfo;->parse(Ljava/lang/String;)Z

    #@1b
    move-result v9

    #@1c
    if-eqz v9, :cond_3b

    #@1e
    .line 157
    new-instance v1, Ljava/util/zip/ZipEntry;

    #@20
    invoke-direct {v1, v7}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    #@23
    .line 158
    .local v1, entry:Ljava/util/zip/ZipEntry;
    invoke-virtual {v8, v1}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    #@26
    .line 160
    new-instance v2, Ljava/io/File;

    #@28
    iget-object v9, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@2a
    invoke-direct {v2, v9, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2d
    .line 161
    .local v2, file:Ljava/io/File;
    new-instance v5, Ljava/io/FileInputStream;

    #@2f
    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_32
    .catchall {:try_start_5 .. :try_end_32} :catchall_43

    #@32
    .line 163
    .local v5, is:Ljava/io/FileInputStream;
    :try_start_32
    invoke-static {v5, v8}, Llibcore/io/Streams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_3e

    #@35
    .line 165
    :try_start_35
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@38
    .line 168
    invoke-virtual {v8}, Ljava/util/zip/ZipOutputStream;->closeEntry()V

    #@3b
    .line 155
    .end local v1           #entry:Ljava/util/zip/ZipEntry;
    .end local v2           #file:Ljava/io/File;
    .end local v5           #is:Ljava/io/FileInputStream;
    :cond_3b
    add-int/lit8 v3, v3, 0x1

    #@3d
    goto :goto_14

    #@3e
    .line 165
    .restart local v1       #entry:Ljava/util/zip/ZipEntry;
    .restart local v2       #file:Ljava/io/File;
    .restart local v5       #is:Ljava/io/FileInputStream;
    :catchall_3e
    move-exception v9

    #@3f
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@42
    throw v9
    :try_end_43
    .catchall {:try_start_35 .. :try_end_43} :catchall_43

    #@43
    .line 172
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #entry:Ljava/util/zip/ZipEntry;
    .end local v2           #file:Ljava/io/File;
    .end local v3           #i$:I
    .end local v4           #info:Lcom/android/internal/util/FileRotator$FileInfo;
    .end local v5           #is:Ljava/io/FileInputStream;
    .end local v6           #len$:I
    .end local v7           #name:Ljava/lang/String;
    :catchall_43
    move-exception v9

    #@44
    invoke-static {v8}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@47
    throw v9

    #@48
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #info:Lcom/android/internal/util/FileRotator$FileInfo;
    .restart local v6       #len$:I
    :cond_48
    invoke-static {v8}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@4b
    .line 174
    return-void
.end method

.method public maybeRotate(J)V
    .registers 16
    .parameter "currentTimeMillis"

    #@0
    .prologue
    .line 335
    iget-wide v11, p0, Lcom/android/internal/util/FileRotator;->mRotateAgeMillis:J

    #@2
    sub-long v9, p1, v11

    #@4
    .line 336
    .local v9, rotateBefore:J
    iget-wide v11, p0, Lcom/android/internal/util/FileRotator;->mDeleteAgeMillis:J

    #@6
    sub-long v1, p1, v11

    #@8
    .line 338
    .local v1, deleteBefore:J
    new-instance v6, Lcom/android/internal/util/FileRotator$FileInfo;

    #@a
    iget-object v11, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@c
    invoke-direct {v6, v11}, Lcom/android/internal/util/FileRotator$FileInfo;-><init>(Ljava/lang/String;)V

    #@f
    .line 339
    .local v6, info:Lcom/android/internal/util/FileRotator$FileInfo;
    iget-object v11, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@11
    invoke-virtual {v11}, Ljava/io/File;->list()[Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .local v0, arr$:[Ljava/lang/String;
    array-length v7, v0

    #@16
    .local v7, len$:I
    const/4 v5, 0x0

    #@17
    .local v5, i$:I
    :goto_17
    if-ge v5, v7, :cond_59

    #@19
    aget-object v8, v0, v5

    #@1b
    .line 340
    .local v8, name:Ljava/lang/String;
    invoke-virtual {v6, v8}, Lcom/android/internal/util/FileRotator$FileInfo;->parse(Ljava/lang/String;)Z

    #@1e
    move-result v11

    #@1f
    if-nez v11, :cond_24

    #@21
    .line 339
    :cond_21
    :goto_21
    add-int/lit8 v5, v5, 0x1

    #@23
    goto :goto_17

    #@24
    .line 342
    :cond_24
    invoke-virtual {v6}, Lcom/android/internal/util/FileRotator$FileInfo;->isActive()Z

    #@27
    move-result v11

    #@28
    if-eqz v11, :cond_48

    #@2a
    .line 343
    iget-wide v11, v6, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@2c
    cmp-long v11, v11, v9

    #@2e
    if-gtz v11, :cond_21

    #@30
    .line 347
    iput-wide p1, v6, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@32
    .line 349
    new-instance v4, Ljava/io/File;

    #@34
    iget-object v11, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@36
    invoke-direct {v4, v11, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@39
    .line 350
    .local v4, file:Ljava/io/File;
    new-instance v3, Ljava/io/File;

    #@3b
    iget-object v11, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@3d
    invoke-virtual {v6}, Lcom/android/internal/util/FileRotator$FileInfo;->build()Ljava/lang/String;

    #@40
    move-result-object v12

    #@41
    invoke-direct {v3, v11, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@44
    .line 351
    .local v3, destFile:Ljava/io/File;
    invoke-virtual {v4, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@47
    goto :goto_21

    #@48
    .line 353
    .end local v3           #destFile:Ljava/io/File;
    .end local v4           #file:Ljava/io/File;
    :cond_48
    iget-wide v11, v6, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@4a
    cmp-long v11, v11, v1

    #@4c
    if-gtz v11, :cond_21

    #@4e
    .line 357
    new-instance v4, Ljava/io/File;

    #@50
    iget-object v11, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@52
    invoke-direct {v4, v11, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@55
    .line 358
    .restart local v4       #file:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@58
    goto :goto_21

    #@59
    .line 361
    .end local v4           #file:Ljava/io/File;
    .end local v8           #name:Ljava/lang/String;
    :cond_59
    return-void
.end method

.method public readMatching(Lcom/android/internal/util/FileRotator$Reader;JJ)V
    .registers 14
    .parameter "reader"
    .parameter "matchStartMillis"
    .parameter "matchEndMillis"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 287
    new-instance v3, Lcom/android/internal/util/FileRotator$FileInfo;

    #@2
    iget-object v6, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@4
    invoke-direct {v3, v6}, Lcom/android/internal/util/FileRotator$FileInfo;-><init>(Ljava/lang/String;)V

    #@7
    .line 288
    .local v3, info:Lcom/android/internal/util/FileRotator$FileInfo;
    iget-object v6, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@9
    invoke-virtual {v6}, Ljava/io/File;->list()[Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@e
    .local v4, len$:I
    const/4 v2, 0x0

    #@f
    .local v2, i$:I
    :goto_f
    if-ge v2, v4, :cond_33

    #@11
    aget-object v5, v0, v2

    #@13
    .line 289
    .local v5, name:Ljava/lang/String;
    invoke-virtual {v3, v5}, Lcom/android/internal/util/FileRotator$FileInfo;->parse(Ljava/lang/String;)Z

    #@16
    move-result v6

    #@17
    if-nez v6, :cond_1c

    #@19
    .line 288
    :cond_19
    :goto_19
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_f

    #@1c
    .line 292
    :cond_1c
    iget-wide v6, v3, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@1e
    cmp-long v6, v6, p4

    #@20
    if-gtz v6, :cond_19

    #@22
    iget-wide v6, v3, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@24
    cmp-long v6, p2, v6

    #@26
    if-gtz v6, :cond_19

    #@28
    .line 295
    new-instance v1, Ljava/io/File;

    #@2a
    iget-object v6, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@2c
    invoke-direct {v1, v6, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2f
    .line 296
    .local v1, file:Ljava/io/File;
    invoke-static {v1, p1}, Lcom/android/internal/util/FileRotator;->readFile(Ljava/io/File;Lcom/android/internal/util/FileRotator$Reader;)V

    #@32
    goto :goto_19

    #@33
    .line 299
    .end local v1           #file:Ljava/io/File;
    .end local v5           #name:Ljava/lang/String;
    :cond_33
    return-void
.end method

.method public rewriteActive(Lcom/android/internal/util/FileRotator$Rewriter;J)V
    .registers 5
    .parameter "rewriter"
    .parameter "currentTimeMillis"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    invoke-direct {p0, p2, p3}, Lcom/android/internal/util/FileRotator;->getActiveName(J)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 184
    .local v0, activeName:Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/android/internal/util/FileRotator;->rewriteSingle(Lcom/android/internal/util/FileRotator$Rewriter;Ljava/lang/String;)V

    #@7
    .line 185
    return-void
.end method

.method public rewriteAll(Lcom/android/internal/util/FileRotator$Rewriter;)V
    .registers 8
    .parameter "rewriter"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 218
    new-instance v2, Lcom/android/internal/util/FileRotator$FileInfo;

    #@2
    iget-object v5, p0, Lcom/android/internal/util/FileRotator;->mPrefix:Ljava/lang/String;

    #@4
    invoke-direct {v2, v5}, Lcom/android/internal/util/FileRotator$FileInfo;-><init>(Ljava/lang/String;)V

    #@7
    .line 219
    .local v2, info:Lcom/android/internal/util/FileRotator$FileInfo;
    iget-object v5, p0, Lcom/android/internal/util/FileRotator;->mBasePath:Ljava/io/File;

    #@9
    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@e
    .local v3, len$:I
    const/4 v1, 0x0

    #@f
    .local v1, i$:I
    :goto_f
    if-ge v1, v3, :cond_20

    #@11
    aget-object v4, v0, v1

    #@13
    .line 220
    .local v4, name:Ljava/lang/String;
    invoke-virtual {v2, v4}, Lcom/android/internal/util/FileRotator$FileInfo;->parse(Ljava/lang/String;)Z

    #@16
    move-result v5

    #@17
    if-nez v5, :cond_1c

    #@19
    .line 219
    :goto_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_f

    #@1c
    .line 223
    :cond_1c
    invoke-direct {p0, p1, v4}, Lcom/android/internal/util/FileRotator;->rewriteSingle(Lcom/android/internal/util/FileRotator$Rewriter;Ljava/lang/String;)V

    #@1f
    goto :goto_19

    #@20
    .line 225
    .end local v4           #name:Ljava/lang/String;
    :cond_20
    return-void
.end method
