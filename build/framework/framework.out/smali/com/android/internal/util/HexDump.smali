.class public Lcom/android/internal/util/HexDump;
.super Ljava/lang/Object;
.source "HexDump.java"


# static fields
.field private static final HEX_DIGITS:[C


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 21
    const/16 v0, 0x10

    #@2
    new-array v0, v0, [C

    #@4
    fill-array-data v0, :array_a

    #@7
    sput-object v0, Lcom/android/internal/util/HexDump;->HEX_DIGITS:[C

    #@9
    return-void

    #@a
    :array_a
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x41t 0x0t
        0x42t 0x0t
        0x43t 0x0t
        0x44t 0x0t
        0x45t 0x0t
        0x46t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 19
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static dumpHexString([B)Ljava/lang/String;
    .registers 3
    .parameter "array"

    #@0
    .prologue
    .line 25
    const/4 v0, 0x0

    #@1
    array-length v1, p0

    #@2
    invoke-static {p0, v0, v1}, Lcom/android/internal/util/HexDump;->dumpHexString([BII)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static dumpHexString([BII)Ljava/lang/String;
    .registers 13
    .parameter "array"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 30
    new-instance v7, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 32
    .local v7, result:Ljava/lang/StringBuilder;
    const/16 v8, 0x10

    #@7
    new-array v4, v8, [B

    #@9
    .line 33
    .local v4, line:[B
    const/4 v5, 0x0

    #@a
    .line 35
    .local v5, lineIndex:I
    const-string v8, "\n0x"

    #@c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 36
    invoke-static {p1}, Lcom/android/internal/util/HexDump;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 38
    move v2, p1

    #@17
    .local v2, i:I
    :goto_17
    add-int v8, p1, p2

    #@19
    if-ge v2, v8, :cond_77

    #@1b
    .line 40
    const/16 v8, 0x10

    #@1d
    if-ne v5, v8, :cond_54

    #@1f
    .line 42
    const-string v8, " "

    #@21
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 44
    const/4 v3, 0x0

    #@25
    .local v3, j:I
    :goto_25
    const/16 v8, 0x10

    #@27
    if-ge v3, v8, :cond_47

    #@29
    .line 46
    aget-byte v8, v4, v3

    #@2b
    const/16 v9, 0x20

    #@2d
    if-le v8, v9, :cond_41

    #@2f
    aget-byte v8, v4, v3

    #@31
    const/16 v9, 0x7e

    #@33
    if-ge v8, v9, :cond_41

    #@35
    .line 48
    new-instance v8, Ljava/lang/String;

    #@37
    const/4 v9, 0x1

    #@38
    invoke-direct {v8, v4, v3, v9}, Ljava/lang/String;-><init>([BII)V

    #@3b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 44
    :goto_3e
    add-int/lit8 v3, v3, 0x1

    #@40
    goto :goto_25

    #@41
    .line 52
    :cond_41
    const-string v8, "."

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    goto :goto_3e

    #@47
    .line 56
    :cond_47
    const-string v8, "\n0x"

    #@49
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    .line 57
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->toHexString(I)Ljava/lang/String;

    #@4f
    move-result-object v8

    #@50
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    .line 58
    const/4 v5, 0x0

    #@54
    .line 61
    .end local v3           #j:I
    :cond_54
    aget-byte v0, p0, v2

    #@56
    .line 62
    .local v0, b:B
    const-string v8, " "

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    .line 63
    sget-object v8, Lcom/android/internal/util/HexDump;->HEX_DIGITS:[C

    #@5d
    ushr-int/lit8 v9, v0, 0x4

    #@5f
    and-int/lit8 v9, v9, 0xf

    #@61
    aget-char v8, v8, v9

    #@63
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@66
    .line 64
    sget-object v8, Lcom/android/internal/util/HexDump;->HEX_DIGITS:[C

    #@68
    and-int/lit8 v9, v0, 0xf

    #@6a
    aget-char v8, v8, v9

    #@6c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6f
    .line 66
    add-int/lit8 v6, v5, 0x1

    #@71
    .end local v5           #lineIndex:I
    .local v6, lineIndex:I
    aput-byte v0, v4, v5

    #@73
    .line 38
    add-int/lit8 v2, v2, 0x1

    #@75
    move v5, v6

    #@76
    .end local v6           #lineIndex:I
    .restart local v5       #lineIndex:I
    goto :goto_17

    #@77
    .line 69
    .end local v0           #b:B
    :cond_77
    const/16 v8, 0x10

    #@79
    if-eq v5, v8, :cond_ad

    #@7b
    .line 71
    rsub-int/lit8 v8, v5, 0x10

    #@7d
    mul-int/lit8 v1, v8, 0x3

    #@7f
    .line 72
    .local v1, count:I
    add-int/lit8 v1, v1, 0x1

    #@81
    .line 73
    const/4 v2, 0x0

    #@82
    :goto_82
    if-ge v2, v1, :cond_8c

    #@84
    .line 75
    const-string v8, " "

    #@86
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    .line 73
    add-int/lit8 v2, v2, 0x1

    #@8b
    goto :goto_82

    #@8c
    .line 78
    :cond_8c
    const/4 v2, 0x0

    #@8d
    :goto_8d
    if-ge v2, v5, :cond_ad

    #@8f
    .line 80
    aget-byte v8, v4, v2

    #@91
    const/16 v9, 0x20

    #@93
    if-le v8, v9, :cond_a7

    #@95
    aget-byte v8, v4, v2

    #@97
    const/16 v9, 0x7e

    #@99
    if-ge v8, v9, :cond_a7

    #@9b
    .line 82
    new-instance v8, Ljava/lang/String;

    #@9d
    const/4 v9, 0x1

    #@9e
    invoke-direct {v8, v4, v2, v9}, Ljava/lang/String;-><init>([BII)V

    #@a1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    .line 78
    :goto_a4
    add-int/lit8 v2, v2, 0x1

    #@a6
    goto :goto_8d

    #@a7
    .line 86
    :cond_a7
    const-string v8, "."

    #@a9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    goto :goto_a4

    #@ad
    .line 91
    .end local v1           #count:I
    :cond_ad
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v8

    #@b1
    return-object v8
.end method

.method public static hexStringToByteArray(Ljava/lang/String;)[B
    .registers 7
    .parameter "hexString"

    #@0
    .prologue
    .line 154
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v2

    #@4
    .line 155
    .local v2, length:I
    div-int/lit8 v3, v2, 0x2

    #@6
    new-array v0, v3, [B

    #@8
    .line 157
    .local v0, buffer:[B
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v2, :cond_28

    #@b
    .line 159
    div-int/lit8 v3, v1, 0x2

    #@d
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@10
    move-result v4

    #@11
    invoke-static {v4}, Lcom/android/internal/util/HexDump;->toByte(C)I

    #@14
    move-result v4

    #@15
    shl-int/lit8 v4, v4, 0x4

    #@17
    add-int/lit8 v5, v1, 0x1

    #@19
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@1c
    move-result v5

    #@1d
    invoke-static {v5}, Lcom/android/internal/util/HexDump;->toByte(C)I

    #@20
    move-result v5

    #@21
    or-int/2addr v4, v5

    #@22
    int-to-byte v4, v4

    #@23
    aput-byte v4, v0, v3

    #@25
    .line 157
    add-int/lit8 v1, v1, 0x2

    #@27
    goto :goto_9

    #@28
    .line 162
    :cond_28
    return-object v0
.end method

.method private static toByte(C)I
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 145
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_b

    #@8
    add-int/lit8 v0, p0, -0x30

    #@a
    .line 147
    :goto_a
    return v0

    #@b
    .line 146
    :cond_b
    const/16 v0, 0x41

    #@d
    if-lt p0, v0, :cond_18

    #@f
    const/16 v0, 0x46

    #@11
    if-gt p0, v0, :cond_18

    #@13
    add-int/lit8 v0, p0, -0x41

    #@15
    add-int/lit8 v0, v0, 0xa

    #@17
    goto :goto_a

    #@18
    .line 147
    :cond_18
    const/16 v0, 0x61

    #@1a
    if-lt p0, v0, :cond_25

    #@1c
    const/16 v0, 0x66

    #@1e
    if-gt p0, v0, :cond_25

    #@20
    add-int/lit8 v0, p0, -0x61

    #@22
    add-int/lit8 v0, v0, 0xa

    #@24
    goto :goto_a

    #@25
    .line 149
    :cond_25
    new-instance v0, Ljava/lang/RuntimeException;

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "Invalid hex char \'"

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, "\'"

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@43
    throw v0
.end method

.method public static toByteArray(B)[B
    .registers 3
    .parameter "b"

    #@0
    .prologue
    .line 126
    const/4 v1, 0x1

    #@1
    new-array v0, v1, [B

    #@3
    .line 127
    .local v0, array:[B
    const/4 v1, 0x0

    #@4
    aput-byte p0, v0, v1

    #@6
    .line 128
    return-object v0
.end method

.method public static toByteArray(I)[B
    .registers 4
    .parameter "i"

    #@0
    .prologue
    .line 133
    const/4 v1, 0x4

    #@1
    new-array v0, v1, [B

    #@3
    .line 135
    .local v0, array:[B
    const/4 v1, 0x3

    #@4
    and-int/lit16 v2, p0, 0xff

    #@6
    int-to-byte v2, v2

    #@7
    aput-byte v2, v0, v1

    #@9
    .line 136
    const/4 v1, 0x2

    #@a
    shr-int/lit8 v2, p0, 0x8

    #@c
    and-int/lit16 v2, v2, 0xff

    #@e
    int-to-byte v2, v2

    #@f
    aput-byte v2, v0, v1

    #@11
    .line 137
    const/4 v1, 0x1

    #@12
    shr-int/lit8 v2, p0, 0x10

    #@14
    and-int/lit16 v2, v2, 0xff

    #@16
    int-to-byte v2, v2

    #@17
    aput-byte v2, v0, v1

    #@19
    .line 138
    const/4 v1, 0x0

    #@1a
    shr-int/lit8 v2, p0, 0x18

    #@1c
    and-int/lit16 v2, v2, 0xff

    #@1e
    int-to-byte v2, v2

    #@1f
    aput-byte v2, v0, v1

    #@21
    .line 140
    return-object v0
.end method

.method public static toHexString(B)Ljava/lang/String;
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 96
    invoke-static {p0}, Lcom/android/internal/util/HexDump;->toByteArray(B)[B

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static toHexString(I)Ljava/lang/String;
    .registers 2
    .parameter "i"

    #@0
    .prologue
    .line 121
    invoke-static {p0}, Lcom/android/internal/util/HexDump;->toByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static toHexString([B)Ljava/lang/String;
    .registers 3
    .parameter "array"

    #@0
    .prologue
    .line 101
    const/4 v0, 0x0

    #@1
    array-length v1, p0

    #@2
    invoke-static {p0, v0, v1}, Lcom/android/internal/util/HexDump;->toHexString([BII)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static toHexString([BII)Ljava/lang/String;
    .registers 10
    .parameter "array"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 106
    mul-int/lit8 v5, p2, 0x2

    #@2
    new-array v1, v5, [C

    #@4
    .line 108
    .local v1, buf:[C
    const/4 v2, 0x0

    #@5
    .line 109
    .local v2, bufIndex:I
    move v4, p1

    #@6
    .local v4, i:I
    move v3, v2

    #@7
    .end local v2           #bufIndex:I
    .local v3, bufIndex:I
    :goto_7
    add-int v5, p1, p2

    #@9
    if-ge v4, v5, :cond_26

    #@b
    .line 111
    aget-byte v0, p0, v4

    #@d
    .line 112
    .local v0, b:B
    add-int/lit8 v2, v3, 0x1

    #@f
    .end local v3           #bufIndex:I
    .restart local v2       #bufIndex:I
    sget-object v5, Lcom/android/internal/util/HexDump;->HEX_DIGITS:[C

    #@11
    ushr-int/lit8 v6, v0, 0x4

    #@13
    and-int/lit8 v6, v6, 0xf

    #@15
    aget-char v5, v5, v6

    #@17
    aput-char v5, v1, v3

    #@19
    .line 113
    add-int/lit8 v3, v2, 0x1

    #@1b
    .end local v2           #bufIndex:I
    .restart local v3       #bufIndex:I
    sget-object v5, Lcom/android/internal/util/HexDump;->HEX_DIGITS:[C

    #@1d
    and-int/lit8 v6, v0, 0xf

    #@1f
    aget-char v5, v5, v6

    #@21
    aput-char v5, v1, v2

    #@23
    .line 109
    add-int/lit8 v4, v4, 0x1

    #@25
    goto :goto_7

    #@26
    .line 116
    .end local v0           #b:B
    :cond_26
    new-instance v5, Ljava/lang/String;

    #@28
    invoke-direct {v5, v1}, Ljava/lang/String;-><init>([C)V

    #@2b
    return-object v5
.end method
