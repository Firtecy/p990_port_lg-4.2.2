.class public abstract Lcom/android/internal/app/IBatteryStats$Stub;
.super Landroid/os/Binder;
.source "IBatteryStats.java"

# interfaces
.implements Lcom/android/internal/app/IBatteryStats;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/IBatteryStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/IBatteryStats$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.app.IBatteryStats"

.field static final TRANSACTION_getAwakeTimeBattery:I = 0x29

.field static final TRANSACTION_getAwakeTimePlugged:I = 0x2a

.field static final TRANSACTION_getStatistics:I = 0x1

.field static final TRANSACTION_noteBluetoothOff:I = 0x1a

.field static final TRANSACTION_noteBluetoothOn:I = 0x19

.field static final TRANSACTION_noteFullWifiLockAcquired:I = 0x1b

.field static final TRANSACTION_noteFullWifiLockAcquiredFromSource:I = 0x21

.field static final TRANSACTION_noteFullWifiLockReleased:I = 0x1c

.field static final TRANSACTION_noteFullWifiLockReleasedFromSource:I = 0x22

.field static final TRANSACTION_noteInputEvent:I = 0xd

.field static final TRANSACTION_noteNetworkInterfaceType:I = 0x27

.field static final TRANSACTION_notePhoneDataConnectionState:I = 0x12

.field static final TRANSACTION_notePhoneOff:I = 0x10

.field static final TRANSACTION_notePhoneOn:I = 0xf

.field static final TRANSACTION_notePhoneSignalStrength:I = 0x11

.field static final TRANSACTION_notePhoneState:I = 0x13

.field static final TRANSACTION_noteScreenBrightness:I = 0xb

.field static final TRANSACTION_noteScreenOff:I = 0xc

.field static final TRANSACTION_noteScreenOn:I = 0xa

.field static final TRANSACTION_noteStartGps:I = 0x8

.field static final TRANSACTION_noteStartSensor:I = 0x4

.field static final TRANSACTION_noteStartWakelock:I = 0x2

.field static final TRANSACTION_noteStartWakelockFromSource:I = 0x6

.field static final TRANSACTION_noteStopGps:I = 0x9

.field static final TRANSACTION_noteStopSensor:I = 0x5

.field static final TRANSACTION_noteStopWakelock:I = 0x3

.field static final TRANSACTION_noteStopWakelockFromSource:I = 0x7

.field static final TRANSACTION_noteUserActivity:I = 0xe

.field static final TRANSACTION_noteWifiMulticastDisabled:I = 0x20

.field static final TRANSACTION_noteWifiMulticastDisabledFromSource:I = 0x26

.field static final TRANSACTION_noteWifiMulticastEnabled:I = 0x1f

.field static final TRANSACTION_noteWifiMulticastEnabledFromSource:I = 0x25

.field static final TRANSACTION_noteWifiOff:I = 0x15

.field static final TRANSACTION_noteWifiOn:I = 0x14

.field static final TRANSACTION_noteWifiRunning:I = 0x16

.field static final TRANSACTION_noteWifiRunningChanged:I = 0x17

.field static final TRANSACTION_noteWifiScanStarted:I = 0x1d

.field static final TRANSACTION_noteWifiScanStartedFromSource:I = 0x23

.field static final TRANSACTION_noteWifiScanStopped:I = 0x1e

.field static final TRANSACTION_noteWifiScanStoppedFromSource:I = 0x24

.field static final TRANSACTION_noteWifiStopped:I = 0x18

.field static final TRANSACTION_setBatteryState:I = 0x28


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/app/IBatteryStats$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IBatteryStats;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.app.IBatteryStats"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/app/IBatteryStats;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/app/IBatteryStats;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/app/IBatteryStats$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/app/IBatteryStats$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_3ae

    #@4
    .line 514
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v9

    #@8
    :goto_8
    return v9

    #@9
    .line 42
    :sswitch_9
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 48
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->getStatistics()[B

    #@17
    move-result-object v7

    #@18
    .line 49
    .local v7, _result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    .line 50
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeByteArray([B)V

    #@1e
    goto :goto_8

    #@1f
    .line 55
    .end local v7           #_result:[B
    :sswitch_1f
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@21
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v1

    #@28
    .line 59
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v2

    #@2c
    .line 61
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    .line 63
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v4

    #@34
    .line 64
    .local v4, _arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStartWakelock(IILjava/lang/String;I)V

    #@37
    .line 65
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a
    goto :goto_8

    #@3b
    .line 70
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:I
    :sswitch_3b
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@3d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v1

    #@44
    .line 74
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v2

    #@48
    .line 76
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    .line 78
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v4

    #@50
    .line 79
    .restart local v4       #_arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStopWakelock(IILjava/lang/String;I)V

    #@53
    .line 80
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@56
    goto :goto_8

    #@57
    .line 85
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:I
    :sswitch_57
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@59
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v1

    #@60
    .line 89
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v2

    #@64
    .line 90
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStartSensor(II)V

    #@67
    .line 91
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6a
    goto :goto_8

    #@6b
    .line 96
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_6b
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@6d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70
    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v1

    #@74
    .line 100
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v2

    #@78
    .line 101
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStopSensor(II)V

    #@7b
    .line 102
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e
    goto :goto_8

    #@7f
    .line 107
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_7f
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@81
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v0

    #@88
    if-eqz v0, :cond_a6

    #@8a
    .line 110
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@8c
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8f
    move-result-object v1

    #@90
    check-cast v1, Landroid/os/WorkSource;

    #@92
    .line 116
    .local v1, _arg0:Landroid/os/WorkSource;
    :goto_92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@95
    move-result v2

    #@96
    .line 118
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@99
    move-result-object v3

    #@9a
    .line 120
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9d
    move-result v4

    #@9e
    .line 121
    .restart local v4       #_arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStartWakelockFromSource(Landroid/os/WorkSource;ILjava/lang/String;I)V

    #@a1
    .line 122
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a4
    goto/16 :goto_8

    #@a6
    .line 113
    .end local v1           #_arg0:Landroid/os/WorkSource;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:I
    :cond_a6
    const/4 v1, 0x0

    #@a7
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_92

    #@a8
    .line 127
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_a8
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@aa
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ad
    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b0
    move-result v0

    #@b1
    if-eqz v0, :cond_cf

    #@b3
    .line 130
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b5
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b8
    move-result-object v1

    #@b9
    check-cast v1, Landroid/os/WorkSource;

    #@bb
    .line 136
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_bb
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@be
    move-result v2

    #@bf
    .line 138
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c2
    move-result-object v3

    #@c3
    .line 140
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c6
    move-result v4

    #@c7
    .line 141
    .restart local v4       #_arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStopWakelockFromSource(Landroid/os/WorkSource;ILjava/lang/String;I)V

    #@ca
    .line 142
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cd
    goto/16 :goto_8

    #@cf
    .line 133
    .end local v1           #_arg0:Landroid/os/WorkSource;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:I
    :cond_cf
    const/4 v1, 0x0

    #@d0
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_bb

    #@d1
    .line 147
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_d1
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@d3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d6
    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d9
    move-result v1

    #@da
    .line 150
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStartGps(I)V

    #@dd
    .line 151
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e0
    goto/16 :goto_8

    #@e2
    .line 156
    .end local v1           #_arg0:I
    :sswitch_e2
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@e4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e7
    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ea
    move-result v1

    #@eb
    .line 159
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteStopGps(I)V

    #@ee
    .line 160
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f1
    goto/16 :goto_8

    #@f3
    .line 165
    .end local v1           #_arg0:I
    :sswitch_f3
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@f5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f8
    .line 166
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->noteScreenOn()V

    #@fb
    .line 167
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fe
    goto/16 :goto_8

    #@100
    .line 172
    :sswitch_100
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@102
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@105
    .line 174
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@108
    move-result v1

    #@109
    .line 175
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteScreenBrightness(I)V

    #@10c
    .line 176
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10f
    goto/16 :goto_8

    #@111
    .line 181
    .end local v1           #_arg0:I
    :sswitch_111
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@113
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@116
    .line 182
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->noteScreenOff()V

    #@119
    .line 183
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11c
    goto/16 :goto_8

    #@11e
    .line 188
    :sswitch_11e
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@120
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@123
    .line 189
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->noteInputEvent()V

    #@126
    .line 190
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@129
    goto/16 :goto_8

    #@12b
    .line 195
    :sswitch_12b
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@12d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@130
    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@133
    move-result v1

    #@134
    .line 199
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@137
    move-result v2

    #@138
    .line 200
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IBatteryStats$Stub;->noteUserActivity(II)V

    #@13b
    .line 201
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13e
    goto/16 :goto_8

    #@140
    .line 206
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_140
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@142
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@145
    .line 207
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->notePhoneOn()V

    #@148
    .line 208
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14b
    goto/16 :goto_8

    #@14d
    .line 213
    :sswitch_14d
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@14f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@152
    .line 214
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->notePhoneOff()V

    #@155
    .line 215
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@158
    goto/16 :goto_8

    #@15a
    .line 220
    :sswitch_15a
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@15c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15f
    .line 222
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@162
    move-result v0

    #@163
    if-eqz v0, :cond_175

    #@165
    .line 223
    sget-object v0, Landroid/telephony/SignalStrength;->CREATOR:Landroid/os/Parcelable$Creator;

    #@167
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@16a
    move-result-object v1

    #@16b
    check-cast v1, Landroid/telephony/SignalStrength;

    #@16d
    .line 228
    .local v1, _arg0:Landroid/telephony/SignalStrength;
    :goto_16d
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->notePhoneSignalStrength(Landroid/telephony/SignalStrength;)V

    #@170
    .line 229
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@173
    goto/16 :goto_8

    #@175
    .line 226
    .end local v1           #_arg0:Landroid/telephony/SignalStrength;
    :cond_175
    const/4 v1, 0x0

    #@176
    .restart local v1       #_arg0:Landroid/telephony/SignalStrength;
    goto :goto_16d

    #@177
    .line 234
    .end local v1           #_arg0:Landroid/telephony/SignalStrength;
    :sswitch_177
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@179
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17c
    .line 236
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17f
    move-result v1

    #@180
    .line 238
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@183
    move-result v0

    #@184
    if-eqz v0, :cond_18f

    #@186
    move v2, v9

    #@187
    .line 239
    .local v2, _arg1:Z
    :goto_187
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IBatteryStats$Stub;->notePhoneDataConnectionState(IZ)V

    #@18a
    .line 240
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@18d
    goto/16 :goto_8

    #@18f
    .line 238
    .end local v2           #_arg1:Z
    :cond_18f
    const/4 v2, 0x0

    #@190
    goto :goto_187

    #@191
    .line 245
    .end local v1           #_arg0:I
    :sswitch_191
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@193
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@196
    .line 247
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@199
    move-result v1

    #@19a
    .line 248
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->notePhoneState(I)V

    #@19d
    .line 249
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a0
    goto/16 :goto_8

    #@1a2
    .line 254
    .end local v1           #_arg0:I
    :sswitch_1a2
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@1a4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a7
    .line 255
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiOn()V

    #@1aa
    .line 256
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ad
    goto/16 :goto_8

    #@1af
    .line 261
    :sswitch_1af
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@1b1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b4
    .line 262
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiOff()V

    #@1b7
    .line 263
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ba
    goto/16 :goto_8

    #@1bc
    .line 268
    :sswitch_1bc
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@1be
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c1
    .line 270
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c4
    move-result v0

    #@1c5
    if-eqz v0, :cond_1d7

    #@1c7
    .line 271
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c9
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1cc
    move-result-object v1

    #@1cd
    check-cast v1, Landroid/os/WorkSource;

    #@1cf
    .line 276
    .local v1, _arg0:Landroid/os/WorkSource;
    :goto_1cf
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiRunning(Landroid/os/WorkSource;)V

    #@1d2
    .line 277
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d5
    goto/16 :goto_8

    #@1d7
    .line 274
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_1d7
    const/4 v1, 0x0

    #@1d8
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_1cf

    #@1d9
    .line 282
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_1d9
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@1db
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1de
    .line 284
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1e1
    move-result v0

    #@1e2
    if-eqz v0, :cond_202

    #@1e4
    .line 285
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e6
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1e9
    move-result-object v1

    #@1ea
    check-cast v1, Landroid/os/WorkSource;

    #@1ec
    .line 291
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_1ec
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1ef
    move-result v0

    #@1f0
    if-eqz v0, :cond_204

    #@1f2
    .line 292
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f4
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f7
    move-result-object v2

    #@1f8
    check-cast v2, Landroid/os/WorkSource;

    #@1fa
    .line 297
    .local v2, _arg1:Landroid/os/WorkSource;
    :goto_1fa
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiRunningChanged(Landroid/os/WorkSource;Landroid/os/WorkSource;)V

    #@1fd
    .line 298
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@200
    goto/16 :goto_8

    #@202
    .line 288
    .end local v1           #_arg0:Landroid/os/WorkSource;
    .end local v2           #_arg1:Landroid/os/WorkSource;
    :cond_202
    const/4 v1, 0x0

    #@203
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_1ec

    #@204
    .line 295
    :cond_204
    const/4 v2, 0x0

    #@205
    .restart local v2       #_arg1:Landroid/os/WorkSource;
    goto :goto_1fa

    #@206
    .line 303
    .end local v1           #_arg0:Landroid/os/WorkSource;
    .end local v2           #_arg1:Landroid/os/WorkSource;
    :sswitch_206
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@208
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20b
    .line 305
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20e
    move-result v0

    #@20f
    if-eqz v0, :cond_221

    #@211
    .line 306
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@213
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@216
    move-result-object v1

    #@217
    check-cast v1, Landroid/os/WorkSource;

    #@219
    .line 311
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_219
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiStopped(Landroid/os/WorkSource;)V

    #@21c
    .line 312
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21f
    goto/16 :goto_8

    #@221
    .line 309
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_221
    const/4 v1, 0x0

    #@222
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_219

    #@223
    .line 317
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_223
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@225
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@228
    .line 318
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->noteBluetoothOn()V

    #@22b
    .line 319
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22e
    goto/16 :goto_8

    #@230
    .line 324
    :sswitch_230
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@232
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@235
    .line 325
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->noteBluetoothOff()V

    #@238
    .line 326
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23b
    goto/16 :goto_8

    #@23d
    .line 331
    :sswitch_23d
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@23f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@242
    .line 333
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@245
    move-result v1

    #@246
    .line 334
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteFullWifiLockAcquired(I)V

    #@249
    .line 335
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24c
    goto/16 :goto_8

    #@24e
    .line 340
    .end local v1           #_arg0:I
    :sswitch_24e
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@250
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@253
    .line 342
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@256
    move-result v1

    #@257
    .line 343
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteFullWifiLockReleased(I)V

    #@25a
    .line 344
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@25d
    goto/16 :goto_8

    #@25f
    .line 349
    .end local v1           #_arg0:I
    :sswitch_25f
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@261
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@264
    .line 351
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@267
    move-result v1

    #@268
    .line 352
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiScanStarted(I)V

    #@26b
    .line 353
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26e
    goto/16 :goto_8

    #@270
    .line 358
    .end local v1           #_arg0:I
    :sswitch_270
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@272
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@275
    .line 360
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@278
    move-result v1

    #@279
    .line 361
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiScanStopped(I)V

    #@27c
    .line 362
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@27f
    goto/16 :goto_8

    #@281
    .line 367
    .end local v1           #_arg0:I
    :sswitch_281
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@283
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@286
    .line 369
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@289
    move-result v1

    #@28a
    .line 370
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiMulticastEnabled(I)V

    #@28d
    .line 371
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@290
    goto/16 :goto_8

    #@292
    .line 376
    .end local v1           #_arg0:I
    :sswitch_292
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@294
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@297
    .line 378
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@29a
    move-result v1

    #@29b
    .line 379
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiMulticastDisabled(I)V

    #@29e
    .line 380
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a1
    goto/16 :goto_8

    #@2a3
    .line 385
    .end local v1           #_arg0:I
    :sswitch_2a3
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@2a5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a8
    .line 387
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2ab
    move-result v0

    #@2ac
    if-eqz v0, :cond_2be

    #@2ae
    .line 388
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2b0
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2b3
    move-result-object v1

    #@2b4
    check-cast v1, Landroid/os/WorkSource;

    #@2b6
    .line 393
    .local v1, _arg0:Landroid/os/WorkSource;
    :goto_2b6
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteFullWifiLockAcquiredFromSource(Landroid/os/WorkSource;)V

    #@2b9
    .line 394
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2bc
    goto/16 :goto_8

    #@2be
    .line 391
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_2be
    const/4 v1, 0x0

    #@2bf
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_2b6

    #@2c0
    .line 399
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_2c0
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@2c2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c5
    .line 401
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c8
    move-result v0

    #@2c9
    if-eqz v0, :cond_2db

    #@2cb
    .line 402
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2cd
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d0
    move-result-object v1

    #@2d1
    check-cast v1, Landroid/os/WorkSource;

    #@2d3
    .line 407
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_2d3
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteFullWifiLockReleasedFromSource(Landroid/os/WorkSource;)V

    #@2d6
    .line 408
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d9
    goto/16 :goto_8

    #@2db
    .line 405
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_2db
    const/4 v1, 0x0

    #@2dc
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_2d3

    #@2dd
    .line 413
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_2dd
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@2df
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e2
    .line 415
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2e5
    move-result v0

    #@2e6
    if-eqz v0, :cond_2f8

    #@2e8
    .line 416
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2ea
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2ed
    move-result-object v1

    #@2ee
    check-cast v1, Landroid/os/WorkSource;

    #@2f0
    .line 421
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_2f0
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiScanStartedFromSource(Landroid/os/WorkSource;)V

    #@2f3
    .line 422
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f6
    goto/16 :goto_8

    #@2f8
    .line 419
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_2f8
    const/4 v1, 0x0

    #@2f9
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_2f0

    #@2fa
    .line 427
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_2fa
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@2fc
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ff
    .line 429
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@302
    move-result v0

    #@303
    if-eqz v0, :cond_315

    #@305
    .line 430
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@307
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@30a
    move-result-object v1

    #@30b
    check-cast v1, Landroid/os/WorkSource;

    #@30d
    .line 435
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_30d
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiScanStoppedFromSource(Landroid/os/WorkSource;)V

    #@310
    .line 436
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@313
    goto/16 :goto_8

    #@315
    .line 433
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_315
    const/4 v1, 0x0

    #@316
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_30d

    #@317
    .line 441
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_317
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@319
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31c
    .line 443
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@31f
    move-result v0

    #@320
    if-eqz v0, :cond_332

    #@322
    .line 444
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@324
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@327
    move-result-object v1

    #@328
    check-cast v1, Landroid/os/WorkSource;

    #@32a
    .line 449
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_32a
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiMulticastEnabledFromSource(Landroid/os/WorkSource;)V

    #@32d
    .line 450
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@330
    goto/16 :goto_8

    #@332
    .line 447
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_332
    const/4 v1, 0x0

    #@333
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_32a

    #@334
    .line 455
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_334
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@336
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@339
    .line 457
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33c
    move-result v0

    #@33d
    if-eqz v0, :cond_34f

    #@33f
    .line 458
    sget-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@341
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@344
    move-result-object v1

    #@345
    check-cast v1, Landroid/os/WorkSource;

    #@347
    .line 463
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    :goto_347
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IBatteryStats$Stub;->noteWifiMulticastDisabledFromSource(Landroid/os/WorkSource;)V

    #@34a
    .line 464
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34d
    goto/16 :goto_8

    #@34f
    .line 461
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :cond_34f
    const/4 v1, 0x0

    #@350
    .restart local v1       #_arg0:Landroid/os/WorkSource;
    goto :goto_347

    #@351
    .line 469
    .end local v1           #_arg0:Landroid/os/WorkSource;
    :sswitch_351
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@353
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@356
    .line 471
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@359
    move-result-object v1

    #@35a
    .line 473
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35d
    move-result v2

    #@35e
    .line 474
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IBatteryStats$Stub;->noteNetworkInterfaceType(Ljava/lang/String;I)V

    #@361
    .line 475
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@364
    goto/16 :goto_8

    #@366
    .line 480
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    :sswitch_366
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@368
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36b
    .line 482
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@36e
    move-result v1

    #@36f
    .line 484
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@372
    move-result v2

    #@373
    .line 486
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@376
    move-result v3

    #@377
    .line 488
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@37a
    move-result v4

    #@37b
    .line 490
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@37e
    move-result v5

    #@37f
    .line 492
    .local v5, _arg4:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@382
    move-result v6

    #@383
    .local v6, _arg5:I
    move-object v0, p0

    #@384
    .line 493
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/app/IBatteryStats$Stub;->setBatteryState(IIIIII)V

    #@387
    .line 494
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@38a
    goto/16 :goto_8

    #@38c
    .line 499
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_arg5:I
    :sswitch_38c
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@38e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@391
    .line 500
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->getAwakeTimeBattery()J

    #@394
    move-result-wide v7

    #@395
    .line 501
    .local v7, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@398
    .line 502
    invoke-virtual {p3, v7, v8}, Landroid/os/Parcel;->writeLong(J)V

    #@39b
    goto/16 :goto_8

    #@39d
    .line 507
    .end local v7           #_result:J
    :sswitch_39d
    const-string v0, "com.android.internal.app.IBatteryStats"

    #@39f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a2
    .line 508
    invoke-virtual {p0}, Lcom/android/internal/app/IBatteryStats$Stub;->getAwakeTimePlugged()J

    #@3a5
    move-result-wide v7

    #@3a6
    .line 509
    .restart local v7       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a9
    .line 510
    invoke-virtual {p3, v7, v8}, Landroid/os/Parcel;->writeLong(J)V

    #@3ac
    goto/16 :goto_8

    #@3ae
    .line 38
    :sswitch_data_3ae
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_3b
        0x4 -> :sswitch_57
        0x5 -> :sswitch_6b
        0x6 -> :sswitch_7f
        0x7 -> :sswitch_a8
        0x8 -> :sswitch_d1
        0x9 -> :sswitch_e2
        0xa -> :sswitch_f3
        0xb -> :sswitch_100
        0xc -> :sswitch_111
        0xd -> :sswitch_11e
        0xe -> :sswitch_12b
        0xf -> :sswitch_140
        0x10 -> :sswitch_14d
        0x11 -> :sswitch_15a
        0x12 -> :sswitch_177
        0x13 -> :sswitch_191
        0x14 -> :sswitch_1a2
        0x15 -> :sswitch_1af
        0x16 -> :sswitch_1bc
        0x17 -> :sswitch_1d9
        0x18 -> :sswitch_206
        0x19 -> :sswitch_223
        0x1a -> :sswitch_230
        0x1b -> :sswitch_23d
        0x1c -> :sswitch_24e
        0x1d -> :sswitch_25f
        0x1e -> :sswitch_270
        0x1f -> :sswitch_281
        0x20 -> :sswitch_292
        0x21 -> :sswitch_2a3
        0x22 -> :sswitch_2c0
        0x23 -> :sswitch_2dd
        0x24 -> :sswitch_2fa
        0x25 -> :sswitch_317
        0x26 -> :sswitch_334
        0x27 -> :sswitch_351
        0x28 -> :sswitch_366
        0x29 -> :sswitch_38c
        0x2a -> :sswitch_39d
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
