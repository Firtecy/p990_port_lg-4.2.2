.class Lcom/android/internal/app/IUsageStats$Stub$Proxy;
.super Ljava/lang/Object;
.source "IUsageStats.java"

# interfaces
.implements Lcom/android/internal/app/IUsageStats;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/IUsageStats$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 126
    iput-object p1, p0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 127
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getAllPkgUsageStats()[Lcom/android/internal/os/PkgUsageStats;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 231
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 232
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 235
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.app.IUsageStats"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 236
    iget-object v3, p0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x5

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 237
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 238
    sget-object v3, Lcom/android/internal/os/PkgUsageStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, [Lcom/android/internal/os/PkgUsageStats;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_26

    #@1f
    .line 241
    .local v2, _result:[Lcom/android/internal/os/PkgUsageStats;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 242
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 244
    return-object v2

    #@26
    .line 241
    .end local v2           #_result:[Lcom/android/internal/os/PkgUsageStats;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 242
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 134
    const-string v0, "com.android.internal.app.IUsageStats"

    #@2
    return-object v0
.end method

.method public getPkgUsageStats(Landroid/content/ComponentName;)Lcom/android/internal/os/PkgUsageStats;
    .registers 8
    .parameter "componentName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 202
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 203
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 206
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.app.IUsageStats"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 207
    if-eqz p1, :cond_36

    #@f
    .line 208
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 209
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 214
    :goto_17
    iget-object v3, p0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x4

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 215
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 216
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_43

    #@27
    .line 217
    sget-object v3, Lcom/android/internal/os/PkgUsageStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c
    move-result-object v2

    #@2d
    check-cast v2, Lcom/android/internal/os/PkgUsageStats;
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_3b

    #@2f
    .line 224
    .local v2, _result:Lcom/android/internal/os/PkgUsageStats;
    :goto_2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 227
    return-object v2

    #@36
    .line 212
    .end local v2           #_result:Lcom/android/internal/os/PkgUsageStats;
    :cond_36
    const/4 v3, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_17

    #@3b
    .line 224
    :catchall_3b
    move-exception v3

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v3

    #@43
    .line 220
    :cond_43
    const/4 v2, 0x0

    #@44
    .restart local v2       #_result:Lcom/android/internal/os/PkgUsageStats;
    goto :goto_2f
.end method

.method public noteLaunchTime(Landroid/content/ComponentName;I)V
    .registers 8
    .parameter "componentName"
    .parameter "millis"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 180
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 181
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 183
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.app.IUsageStats"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 184
    if-eqz p1, :cond_2b

    #@f
    .line 185
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 186
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 191
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 192
    iget-object v2, p0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v3, 0x3

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 193
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_30

    #@24
    .line 196
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 199
    return-void

    #@2b
    .line 189
    :cond_2b
    const/4 v2, 0x0

    #@2c
    :try_start_2c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2f
    .catchall {:try_start_2c .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_17

    #@30
    .line 196
    :catchall_30
    move-exception v2

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    throw v2
.end method

.method public notePauseComponent(Landroid/content/ComponentName;)V
    .registers 7
    .parameter "componentName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 159
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 160
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 162
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.app.IUsageStats"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 163
    if-eqz p1, :cond_28

    #@f
    .line 164
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 165
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 170
    :goto_17
    iget-object v2, p0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x2

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 171
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 174
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 175
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 177
    return-void

    #@28
    .line 168
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 174
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 175
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method

.method public noteResumeComponent(Landroid/content/ComponentName;)V
    .registers 7
    .parameter "componentName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 138
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 139
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 141
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.app.IUsageStats"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 142
    if-eqz p1, :cond_28

    #@f
    .line 143
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 144
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 149
    :goto_17
    iget-object v2, p0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x1

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 150
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 153
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 154
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 156
    return-void

    #@28
    .line 147
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 153
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 154
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method
