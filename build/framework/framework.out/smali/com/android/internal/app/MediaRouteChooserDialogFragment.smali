.class public Lcom/android/internal/app/MediaRouteChooserDialogFragment;
.super Landroid/app/DialogFragment;
.source "MediaRouteChooserDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$1;,
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;,
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;,
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;,
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;,
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$MediaRouterCallback;,
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;,
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;
    }
.end annotation


# static fields
.field public static final FRAGMENT_TAG:Ljava/lang/String; = "android:MediaRouteChooserDialogFragment"

.field private static final ITEM_LAYOUTS:[I = null

.field private static final TAG:Ljava/lang/String; = "MediaRouteChooserDialogFragment"


# instance fields
.field private mAdapter:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

.field final mCallback:Lcom/android/internal/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

.field final mComparator:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;

.field mDisplayService:Landroid/hardware/display/DisplayManager;

.field private mExtendedSettingsListener:Landroid/view/View$OnClickListener;

.field private mIgnoreCallbackVolumeChanges:Z

.field private mIgnoreSliderVolumeChanges:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLauncherListener:Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;

.field private mListView:Landroid/widget/ListView;

.field private mRouteTypes:I

.field mRouter:Landroid/media/MediaRouter;

.field private mVolumeIcon:Landroid/widget/ImageView;

.field private mVolumeSlider:Landroid/widget/SeekBar;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 64
    const/4 v0, 0x5

    #@1
    new-array v0, v0, [I

    #@3
    fill-array-data v0, :array_a

    #@6
    sput-object v0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->ITEM_LAYOUTS:[I

    #@8
    return-void

    #@9
    nop

    #@a
    :array_a
    .array-data 0x4
        0x89t 0x0t 0x9t 0x1t
        0x88t 0x0t 0x9t 0x1t
        0x85t 0x0t 0x9t 0x1t
        0x86t 0x0t 0x9t 0x1t
        0x87t 0x0t 0x9t 0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 89
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    #@3
    .line 84
    new-instance v0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mComparator:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;

    #@a
    .line 85
    new-instance v0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    #@c
    invoke-direct {v0, p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$MediaRouterCallback;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)V

    #@f
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mCallback:Lcom/android/internal/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    #@11
    .line 90
    const/4 v0, 0x1

    #@12
    const v1, 0x103012e

    #@15
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->setStyle(II)V

    #@18
    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;

    #@2
    return-object v0
.end method

.method static synthetic access$200()[I
    .registers 1

    #@0
    .prologue
    .line 60
    sget-object v0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->ITEM_LAYOUTS:[I

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mIgnoreCallbackVolumeChanges:Z

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/android/internal/app/MediaRouteChooserDialogFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mIgnoreCallbackVolumeChanges:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/SeekBar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@2
    return-object v0
.end method


# virtual methods
.method changeVolume(I)V
    .registers 6
    .parameter "newValue"

    #@0
    .prologue
    .line 161
    iget-boolean v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChanges:Z

    #@2
    if-eqz v2, :cond_5

    #@4
    .line 170
    :cond_4
    :goto_4
    return-void

    #@5
    .line 163
    :cond_5
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@7
    iget v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    #@9
    invoke-virtual {v2, v3}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@c
    move-result-object v1

    #@d
    .line 164
    .local v1, selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    if-eqz v1, :cond_4

    #@f
    invoke-virtual {v1}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    #@12
    move-result v2

    #@13
    const/4 v3, 0x1

    #@14
    if-ne v2, v3, :cond_4

    #@16
    .line 166
    invoke-virtual {v1}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    #@19
    move-result v0

    #@1a
    .line 167
    .local v0, maxVolume:I
    const/4 v2, 0x0

    #@1b
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    #@1e
    move-result v3

    #@1f
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@22
    move-result p1

    #@23
    .line 168
    invoke-virtual {v1, p1}, Landroid/media/MediaRouter$RouteInfo;->requestSetVolume(I)V

    #@26
    goto :goto_4
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 3
    .parameter "activity"

    #@0
    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    #@3
    .line 100
    const-string/jumbo v0, "media_router"

    #@6
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/media/MediaRouter;

    #@c
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@e
    .line 101
    const-string v0, "display"

    #@10
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/hardware/display/DisplayManager;

    #@16
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@18
    .line 102
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 204
    new-instance v0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getActivity()Landroid/app/Activity;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getTheme()I

    #@9
    move-result v2

    #@a
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;Landroid/content/Context;I)V

    #@d
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 175
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;

    #@3
    .line 176
    const v3, 0x1090084

    #@6
    invoke-virtual {p1, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    .line 178
    .local v1, layout:Landroid/view/View;
    const v3, 0x102032a

    #@d
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/widget/ImageView;

    #@13
    iput-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeIcon:Landroid/widget/ImageView;

    #@15
    .line 179
    const v3, 0x102032b

    #@18
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1b
    move-result-object v3

    #@1c
    check-cast v3, Landroid/widget/SeekBar;

    #@1e
    iput-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@20
    .line 180
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->updateVolume()V

    #@23
    .line 181
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@25
    new-instance v4, Lcom/android/internal/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;

    #@27
    invoke-direct {v4, p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)V

    #@2a
    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    #@2d
    .line 183
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    #@2f
    if-eqz v3, :cond_40

    #@31
    .line 184
    const v3, 0x102032c

    #@34
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@37
    move-result-object v0

    #@38
    .line 185
    .local v0, extendedSettingsButton:Landroid/view/View;
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    #@3b
    .line 186
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    #@3d
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@40
    .line 189
    .end local v0           #extendedSettingsButton:Landroid/view/View;
    :cond_40
    const v3, 0x102000a

    #@43
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@46
    move-result-object v2

    #@47
    check-cast v2, Landroid/widget/ListView;

    #@49
    .line 190
    .local v2, list:Landroid/widget/ListView;
    const/4 v3, 0x1

    #@4a
    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    #@4d
    .line 191
    new-instance v3, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@4f
    invoke-direct {v3, p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)V

    #@52
    iput-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@54
    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@57
    .line 192
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@59
    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@5c
    .line 194
    iput-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;

    #@5e
    .line 195
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@60
    iget v4, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    #@62
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mCallback:Lcom/android/internal/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    #@64
    invoke-virtual {v3, v4, v5}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    #@67
    .line 197
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@69
    invoke-virtual {v3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->scrollToSelectedItem()V

    #@6c
    .line 199
    return-object v1
.end method

.method public onDetach()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 106
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    #@4
    .line 107
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mLauncherListener:Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 108
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mLauncherListener:Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;

    #@a
    invoke-interface {v0, p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;->onDetached(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)V

    #@d
    .line 110
    :cond_d
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@f
    if-eqz v0, :cond_13

    #@11
    .line 111
    iput-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@13
    .line 113
    :cond_13
    iput-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;

    #@15
    .line 114
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@17
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mCallback:Lcom/android/internal/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    #@19
    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    #@1c
    .line 115
    iput-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@1e
    .line 116
    return-void
.end method

.method public onResume()V
    .registers 2

    #@0
    .prologue
    .line 209
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    #@3
    .line 210
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 211
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@9
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    #@c
    .line 213
    :cond_c
    return-void
.end method

.method public setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 119
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    #@2
    .line 120
    return-void
.end method

.method public setLauncherListener(Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 94
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mLauncherListener:Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;

    #@2
    .line 95
    return-void
.end method

.method public setRouteTypes(I)V
    .registers 4
    .parameter "types"

    #@0
    .prologue
    .line 123
    iput p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    #@2
    .line 124
    iget v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    #@4
    and-int/lit8 v1, v1, 0x2

    #@6
    if-eqz v1, :cond_1d

    #@8
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@a
    if-nez v1, :cond_1d

    #@c
    .line 125
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getActivity()Landroid/app/Activity;

    #@f
    move-result-object v0

    #@10
    .line 126
    .local v0, activity:Landroid/content/Context;
    if-eqz v0, :cond_1c

    #@12
    .line 127
    const-string v1, "display"

    #@14
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/hardware/display/DisplayManager;

    #@1a
    iput-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@1c
    .line 133
    .end local v0           #activity:Landroid/content/Context;
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 131
    :cond_1d
    const/4 v1, 0x0

    #@1e
    iput-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@20
    goto :goto_1c
.end method

.method updateVolume()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 136
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@4
    if-nez v1, :cond_7

    #@6
    .line 158
    :goto_6
    return-void

    #@7
    .line 138
    :cond_7
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@9
    iget v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    #@b
    invoke-virtual {v1, v2}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@e
    move-result-object v0

    #@f
    .line 139
    .local v0, selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeIcon:Landroid/widget/ImageView;

    #@11
    if-eqz v0, :cond_19

    #@13
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_3b

    #@19
    :cond_19
    const v1, 0x10802a8

    #@1c
    :goto_1c
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@1f
    .line 143
    iput-boolean v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChanges:Z

    #@21
    .line 145
    if-eqz v0, :cond_29

    #@23
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    #@26
    move-result v1

    #@27
    if-nez v1, :cond_3f

    #@29
    .line 148
    :cond_29
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@2b
    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setMax(I)V

    #@2e
    .line 149
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@30
    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    #@33
    .line 150
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@35
    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@38
    .line 157
    :goto_38
    iput-boolean v4, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChanges:Z

    #@3a
    goto :goto_6

    #@3b
    .line 139
    :cond_3b
    const v1, 0x108031f

    #@3e
    goto :goto_1c

    #@3f
    .line 152
    :cond_3f
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@41
    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@44
    .line 153
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@46
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    #@49
    move-result v2

    #@4a
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    #@4d
    .line 154
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    #@4f
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    #@52
    move-result v2

    #@53
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    #@56
    goto :goto_38
.end method
