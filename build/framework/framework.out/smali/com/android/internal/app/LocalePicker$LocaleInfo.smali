.class public Lcom/android/internal/app/LocalePicker$LocaleInfo;
.super Ljava/lang/Object;
.source "LocalePicker.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/LocalePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocaleInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/internal/app/LocalePicker$LocaleInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final sCollator:Ljava/text/Collator;


# instance fields
.field label:Ljava/lang/String;

.field locale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 66
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    #@3
    move-result-object v0

    #@4
    sput-object v0, Lcom/android/internal/app/LocalePicker$LocaleInfo;->sCollator:Ljava/text/Collator;

    #@6
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .registers 3
    .parameter "label"
    .parameter "locale"

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    iput-object p1, p0, Lcom/android/internal/app/LocalePicker$LocaleInfo;->label:Ljava/lang/String;

    #@5
    .line 73
    iput-object p2, p0, Lcom/android/internal/app/LocalePicker$LocaleInfo;->locale:Ljava/util/Locale;

    #@7
    .line 74
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/internal/app/LocalePicker$LocaleInfo;)I
    .registers 14
    .parameter "another"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v9, 0x1

    #@2
    .line 92
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@5
    move-result-object v10

    #@6
    invoke-virtual {v10}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 93
    .local v3, curLocaleStr:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    #@d
    move-result-object v10

    #@e
    invoke-virtual {v10}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@11
    move-result-object v7

    #@12
    .line 94
    .local v7, thisLocaleStr:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    #@15
    move-result-object v10

    #@16
    invoke-virtual {v10}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 96
    .local v1, anotherLocaleStr:Ljava/lang/String;
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v10

    #@1e
    if-eqz v10, :cond_21

    #@20
    .line 120
    :cond_20
    :goto_20
    return v8

    #@21
    .line 98
    :cond_21
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v10

    #@25
    if-eqz v10, :cond_29

    #@27
    move v8, v9

    #@28
    .line 99
    goto :goto_20

    #@29
    .line 102
    :cond_29
    invoke-static {}, Lcom/android/internal/app/LocalePicker;->access$000()Ljava/lang/String;

    #@2c
    move-result-object v10

    #@2d
    const-string v11, "KR"

    #@2f
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v10

    #@33
    if-eqz v10, :cond_60

    #@35
    .line 103
    const/4 v10, 0x3

    #@36
    new-array v0, v10, [Ljava/lang/String;

    #@38
    const/4 v10, 0x0

    #@39
    const-string/jumbo v11, "ko"

    #@3c
    aput-object v11, v0, v10

    #@3e
    const-string v10, "en"

    #@40
    aput-object v10, v0, v9

    #@42
    const/4 v10, 0x2

    #@43
    const-string/jumbo v11, "zh"

    #@46
    aput-object v11, v0, v10

    #@48
    .line 104
    .local v0, LANG_PREFIXS:[Ljava/lang/String;
    move-object v2, v0

    #@49
    .local v2, arr$:[Ljava/lang/String;
    array-length v6, v2

    #@4a
    .local v6, len$:I
    const/4 v4, 0x0

    #@4b
    .local v4, i$:I
    :goto_4b
    if-ge v4, v6, :cond_72

    #@4d
    aget-object v5, v2, v4

    #@4f
    .line 105
    .local v5, language:Ljava/lang/String;
    invoke-virtual {v7, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@52
    move-result v10

    #@53
    if-nez v10, :cond_20

    #@55
    .line 107
    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@58
    move-result v10

    #@59
    if-eqz v10, :cond_5d

    #@5b
    move v8, v9

    #@5c
    .line 108
    goto :goto_20

    #@5d
    .line 104
    :cond_5d
    add-int/lit8 v4, v4, 0x1

    #@5f
    goto :goto_4b

    #@60
    .line 112
    .end local v0           #LANG_PREFIXS:[Ljava/lang/String;
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #language:Ljava/lang/String;
    .end local v6           #len$:I
    :cond_60
    const-string v10, "en"

    #@62
    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@65
    move-result v10

    #@66
    if-nez v10, :cond_20

    #@68
    .line 114
    const-string v8, "en"

    #@6a
    invoke-virtual {v1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@6d
    move-result v8

    #@6e
    if-eqz v8, :cond_72

    #@70
    move v8, v9

    #@71
    .line 115
    goto :goto_20

    #@72
    .line 120
    :cond_72
    sget-object v8, Lcom/android/internal/app/LocalePicker$LocaleInfo;->sCollator:Ljava/text/Collator;

    #@74
    iget-object v9, p0, Lcom/android/internal/app/LocalePicker$LocaleInfo;->label:Ljava/lang/String;

    #@76
    iget-object v10, p1, Lcom/android/internal/app/LocalePicker$LocaleInfo;->label:Ljava/lang/String;

    #@78
    invoke-virtual {v8, v9, v10}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    move-result v8

    #@7c
    goto :goto_20
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 65
    check-cast p1, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->compareTo(Lcom/android/internal/app/LocalePicker$LocaleInfo;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/internal/app/LocalePicker$LocaleInfo;->label:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/internal/app/LocalePicker$LocaleInfo;->locale:Ljava/util/Locale;

    #@2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/internal/app/LocalePicker$LocaleInfo;->label:Ljava/lang/String;

    #@2
    return-object v0
.end method
