.class Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;
.super Ljava/lang/Object;
.source "MediaRouteChooserDialogFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RouteComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/media/MediaRouter$RouteInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 633
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public compare(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteInfo;)I
    .registers 5
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 636
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getActivity()Landroid/app/Activity;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p1, v0}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@10
    invoke-virtual {v1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getActivity()Landroid/app/Activity;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {p2, v1}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1f
    move-result v0

    #@20
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 633
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    #@2
    .end local p1
    check-cast p2, Landroid/media/MediaRouter$RouteInfo;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;->compare(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteInfo;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
