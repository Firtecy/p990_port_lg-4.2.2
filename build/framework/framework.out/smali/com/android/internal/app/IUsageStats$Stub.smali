.class public abstract Lcom/android/internal/app/IUsageStats$Stub;
.super Landroid/os/Binder;
.source "IUsageStats.java"

# interfaces
.implements Lcom/android/internal/app/IUsageStats;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/IUsageStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/IUsageStats$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.app.IUsageStats"

.field static final TRANSACTION_getAllPkgUsageStats:I = 0x5

.field static final TRANSACTION_getPkgUsageStats:I = 0x4

.field static final TRANSACTION_noteLaunchTime:I = 0x3

.field static final TRANSACTION_notePauseComponent:I = 0x2

.field static final TRANSACTION_noteResumeComponent:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.app.IUsageStats"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/app/IUsageStats$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IUsageStats;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.app.IUsageStats"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/app/IUsageStats;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/app/IUsageStats;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/app/IUsageStats$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/app/IUsageStats$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_a4

    #@4
    .line 119
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 42
    :sswitch_9
    const-string v4, "com.android.internal.app.IUsageStats"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v4, "com.android.internal.app.IUsageStats"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_29

    #@1a
    .line 50
    sget-object v4, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/content/ComponentName;

    #@22
    .line 55
    .local v0, _arg0:Landroid/content/ComponentName;
    :goto_22
    invoke-virtual {p0, v0}, Lcom/android/internal/app/IUsageStats$Stub;->noteResumeComponent(Landroid/content/ComponentName;)V

    #@25
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@28
    goto :goto_8

    #@29
    .line 53
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :cond_29
    const/4 v0, 0x0

    #@2a
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    goto :goto_22

    #@2b
    .line 61
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :sswitch_2b
    const-string v4, "com.android.internal.app.IUsageStats"

    #@2d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_45

    #@36
    .line 64
    sget-object v4, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@38
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    check-cast v0, Landroid/content/ComponentName;

    #@3e
    .line 69
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    :goto_3e
    invoke-virtual {p0, v0}, Lcom/android/internal/app/IUsageStats$Stub;->notePauseComponent(Landroid/content/ComponentName;)V

    #@41
    .line 70
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44
    goto :goto_8

    #@45
    .line 67
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :cond_45
    const/4 v0, 0x0

    #@46
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    goto :goto_3e

    #@47
    .line 75
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :sswitch_47
    const-string v4, "com.android.internal.app.IUsageStats"

    #@49
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c
    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v4

    #@50
    if-eqz v4, :cond_65

    #@52
    .line 78
    sget-object v4, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@54
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@57
    move-result-object v0

    #@58
    check-cast v0, Landroid/content/ComponentName;

    #@5a
    .line 84
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    :goto_5a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v1

    #@5e
    .line 85
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/IUsageStats$Stub;->noteLaunchTime(Landroid/content/ComponentName;I)V

    #@61
    .line 86
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@64
    goto :goto_8

    #@65
    .line 81
    .end local v0           #_arg0:Landroid/content/ComponentName;
    .end local v1           #_arg1:I
    :cond_65
    const/4 v0, 0x0

    #@66
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    goto :goto_5a

    #@67
    .line 91
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :sswitch_67
    const-string v4, "com.android.internal.app.IUsageStats"

    #@69
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v4

    #@70
    if-eqz v4, :cond_8b

    #@72
    .line 94
    sget-object v4, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@74
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@77
    move-result-object v0

    #@78
    check-cast v0, Landroid/content/ComponentName;

    #@7a
    .line 99
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    :goto_7a
    invoke-virtual {p0, v0}, Lcom/android/internal/app/IUsageStats$Stub;->getPkgUsageStats(Landroid/content/ComponentName;)Lcom/android/internal/os/PkgUsageStats;

    #@7d
    move-result-object v2

    #@7e
    .line 100
    .local v2, _result:Lcom/android/internal/os/PkgUsageStats;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@81
    .line 101
    if-eqz v2, :cond_8d

    #@83
    .line 102
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    .line 103
    invoke-virtual {v2, p3, v3}, Lcom/android/internal/os/PkgUsageStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@89
    goto/16 :goto_8

    #@8b
    .line 97
    .end local v0           #_arg0:Landroid/content/ComponentName;
    .end local v2           #_result:Lcom/android/internal/os/PkgUsageStats;
    :cond_8b
    const/4 v0, 0x0

    #@8c
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    goto :goto_7a

    #@8d
    .line 106
    .restart local v2       #_result:Lcom/android/internal/os/PkgUsageStats;
    :cond_8d
    const/4 v4, 0x0

    #@8e
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@91
    goto/16 :goto_8

    #@93
    .line 112
    .end local v0           #_arg0:Landroid/content/ComponentName;
    .end local v2           #_result:Lcom/android/internal/os/PkgUsageStats;
    :sswitch_93
    const-string v4, "com.android.internal.app.IUsageStats"

    #@95
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@98
    .line 113
    invoke-virtual {p0}, Lcom/android/internal/app/IUsageStats$Stub;->getAllPkgUsageStats()[Lcom/android/internal/os/PkgUsageStats;

    #@9b
    move-result-object v2

    #@9c
    .line 114
    .local v2, _result:[Lcom/android/internal/os/PkgUsageStats;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f
    .line 115
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@a2
    goto/16 :goto_8

    #@a4
    .line 38
    :sswitch_data_a4
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_2b
        0x3 -> :sswitch_47
        0x4 -> :sswitch_67
        0x5 -> :sswitch_93
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
