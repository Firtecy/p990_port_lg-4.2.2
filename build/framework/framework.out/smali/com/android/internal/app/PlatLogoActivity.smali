.class public Lcom/android/internal/app/PlatLogoActivity;
.super Landroid/app/Activity;
.source "PlatLogoActivity.java"


# instance fields
.field mContent:Landroid/widget/ImageView;

.field mCount:I

.field final mHandler:Landroid/os/Handler;

.field mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    .line 39
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/app/PlatLogoActivity;->mHandler:Landroid/os/Handler;

    #@a
    return-void
.end method

.method private makeView()Landroid/view/View;
    .registers 16

    #@0
    .prologue
    const/high16 v14, 0x4080

    #@2
    const/high16 v13, 0x4000

    #@4
    const/4 v12, 0x0

    #@5
    const/4 v11, 0x1

    #@6
    const/4 v10, -0x2

    #@7
    .line 42
    new-instance v2, Landroid/util/DisplayMetrics;

    #@9
    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    #@c
    .line 43
    .local v2, metrics:Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/android/internal/app/PlatLogoActivity;->getWindowManager()Landroid/view/WindowManager;

    #@f
    move-result-object v8

    #@10
    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@13
    move-result-object v8

    #@14
    invoke-virtual {v8, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@17
    .line 45
    new-instance v7, Landroid/widget/LinearLayout;

    #@19
    invoke-direct {v7, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@1c
    .line 46
    .local v7, view:Landroid/widget/LinearLayout;
    invoke-virtual {v7, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    #@1f
    .line 47
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    #@21
    invoke-direct {v8, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@24
    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@27
    .line 52
    const/high16 v8, 0x4100

    #@29
    iget v9, v2, Landroid/util/DisplayMetrics;->density:F

    #@2b
    mul-float/2addr v8, v9

    #@2c
    float-to-int v4, v8

    #@2d
    .line 53
    .local v4, p:I
    invoke-virtual {v7, v4, v4, v4, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    #@30
    .line 55
    const-string/jumbo v8, "sans-serif-light"

    #@33
    const/4 v9, 0x0

    #@34
    invoke-static {v8, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@37
    move-result-object v0

    #@38
    .line 56
    .local v0, light:Landroid/graphics/Typeface;
    const-string/jumbo v8, "sans-serif"

    #@3b
    invoke-static {v8, v11}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@3e
    move-result-object v3

    #@3f
    .line 58
    .local v3, normal:Landroid/graphics/Typeface;
    const/high16 v8, 0x4160

    #@41
    iget v9, v2, Landroid/util/DisplayMetrics;->density:F

    #@43
    mul-float v5, v8, v9

    #@45
    .line 59
    .local v5, size:F
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    #@47
    invoke-direct {v1, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@4a
    .line 62
    .local v1, lp:Landroid/widget/LinearLayout$LayoutParams;
    iput v11, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@4c
    .line 63
    const/high16 v8, -0x3f80

    #@4e
    iget v9, v2, Landroid/util/DisplayMetrics;->density:F

    #@50
    mul-float/2addr v8, v9

    #@51
    float-to-int v8, v8

    #@52
    iput v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@54
    .line 65
    new-instance v6, Landroid/widget/TextView;

    #@56
    invoke-direct {v6, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@59
    .line 66
    .local v6, tv:Landroid/widget/TextView;
    if-eqz v0, :cond_5e

    #@5b
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@5e
    .line 67
    :cond_5e
    const/high16 v8, 0x3fa0

    #@60
    mul-float/2addr v8, v5

    #@61
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextSize(F)V

    #@64
    .line 68
    const/4 v8, -0x1

    #@65
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    #@68
    .line 69
    iget v8, v2, Landroid/util/DisplayMetrics;->density:F

    #@6a
    mul-float/2addr v8, v14

    #@6b
    iget v9, v2, Landroid/util/DisplayMetrics;->density:F

    #@6d
    mul-float/2addr v9, v13

    #@6e
    const/high16 v10, 0x6600

    #@70
    invoke-virtual {v6, v8, v12, v9, v10}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    #@73
    .line 70
    new-instance v8, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v9, "Android "

    #@7a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v8

    #@7e
    sget-object v9, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    #@80
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v8

    #@84
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@8b
    .line 71
    invoke-virtual {v7, v6, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@8e
    .line 73
    new-instance v6, Landroid/widget/TextView;

    #@90
    .end local v6           #tv:Landroid/widget/TextView;
    invoke-direct {v6, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@93
    .line 74
    .restart local v6       #tv:Landroid/widget/TextView;
    if-eqz v3, :cond_98

    #@95
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@98
    .line 75
    :cond_98
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setTextSize(F)V

    #@9b
    .line 76
    const/4 v8, -0x1

    #@9c
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    #@9f
    .line 77
    iget v8, v2, Landroid/util/DisplayMetrics;->density:F

    #@a1
    mul-float/2addr v8, v14

    #@a2
    iget v9, v2, Landroid/util/DisplayMetrics;->density:F

    #@a4
    mul-float/2addr v9, v13

    #@a5
    const/high16 v10, 0x6600

    #@a7
    invoke-virtual {v6, v8, v12, v9, v10}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    #@aa
    .line 78
    const-string v8, "JELLY BEAN"

    #@ac
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@af
    .line 79
    invoke-virtual {v7, v6, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@b2
    .line 81
    return-object v7
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 88
    const-string v2, ""

    #@5
    const/4 v3, 0x1

    #@6
    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@9
    move-result-object v2

    #@a
    iput-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mToast:Landroid/widget/Toast;

    #@c
    .line 89
    iget-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mToast:Landroid/widget/Toast;

    #@e
    invoke-direct {p0}, Lcom/android/internal/app/PlatLogoActivity;->makeView()Landroid/view/View;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    #@15
    .line 91
    new-instance v0, Landroid/util/DisplayMetrics;

    #@17
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@1a
    .line 92
    .local v0, metrics:Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/android/internal/app/PlatLogoActivity;->getWindowManager()Landroid/view/WindowManager;

    #@1d
    move-result-object v2

    #@1e
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@25
    .line 94
    new-instance v2, Landroid/widget/ImageView;

    #@27
    invoke-direct {v2, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@2a
    iput-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mContent:Landroid/widget/ImageView;

    #@2c
    .line 95
    iget-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mContent:Landroid/widget/ImageView;

    #@2e
    const v3, 0x1080468

    #@31
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    #@34
    .line 96
    iget-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mContent:Landroid/widget/ImageView;

    #@36
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    #@38
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    #@3b
    .line 98
    const/high16 v2, 0x4200

    #@3d
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    #@3f
    mul-float/2addr v2, v3

    #@40
    float-to-int v1, v2

    #@41
    .line 99
    .local v1, p:I
    iget-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mContent:Landroid/widget/ImageView;

    #@43
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    #@46
    .line 101
    iget-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mContent:Landroid/widget/ImageView;

    #@48
    new-instance v3, Lcom/android/internal/app/PlatLogoActivity$1;

    #@4a
    invoke-direct {v3, p0}, Lcom/android/internal/app/PlatLogoActivity$1;-><init>(Lcom/android/internal/app/PlatLogoActivity;)V

    #@4d
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@50
    .line 109
    iget-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mContent:Landroid/widget/ImageView;

    #@52
    new-instance v3, Lcom/android/internal/app/PlatLogoActivity$2;

    #@54
    invoke-direct {v3, p0}, Lcom/android/internal/app/PlatLogoActivity$2;-><init>(Lcom/android/internal/app/PlatLogoActivity;)V

    #@57
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@5a
    .line 127
    iget-object v2, p0, Lcom/android/internal/app/PlatLogoActivity;->mContent:Landroid/widget/ImageView;

    #@5c
    invoke-virtual {p0, v2}, Lcom/android/internal/app/PlatLogoActivity;->setContentView(Landroid/view/View;)V

    #@5f
    .line 128
    return-void
.end method
