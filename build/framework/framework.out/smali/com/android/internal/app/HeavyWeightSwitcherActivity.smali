.class public Lcom/android/internal/app/HeavyWeightSwitcherActivity;
.super Landroid/app/Activity;
.source "HeavyWeightSwitcherActivity.java"


# static fields
.field public static final KEY_CUR_APP:Ljava/lang/String; = "cur_app"

.field public static final KEY_CUR_TASK:Ljava/lang/String; = "cur_task"

.field public static final KEY_HAS_RESULT:Ljava/lang/String; = "has_result"

.field public static final KEY_INTENT:Ljava/lang/String; = "intent"

.field public static final KEY_NEW_APP:Ljava/lang/String; = "new_app"


# instance fields
.field private mCancelListener:Landroid/view/View$OnClickListener;

.field mCurApp:Ljava/lang/String;

.field mCurTask:I

.field mHasResult:Z

.field mNewApp:Ljava/lang/String;

.field mStartIntent:Landroid/content/IntentSender;

.field private mSwitchNewListener:Landroid/view/View$OnClickListener;

.field private mSwitchOldListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    .line 126
    new-instance v0, Lcom/android/internal/app/HeavyWeightSwitcherActivity$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity$1;-><init>(Lcom/android/internal/app/HeavyWeightSwitcherActivity;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mSwitchOldListener:Landroid/view/View$OnClickListener;

    #@a
    .line 136
    new-instance v0, Lcom/android/internal/app/HeavyWeightSwitcherActivity$2;

    #@c
    invoke-direct {v0, p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity$2;-><init>(Lcom/android/internal/app/HeavyWeightSwitcherActivity;)V

    #@f
    iput-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mSwitchNewListener:Landroid/view/View$OnClickListener;

    #@11
    .line 157
    new-instance v0, Lcom/android/internal/app/HeavyWeightSwitcherActivity$3;

    #@13
    invoke-direct {v0, p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity$3;-><init>(Lcom/android/internal/app/HeavyWeightSwitcherActivity;)V

    #@16
    iput-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mCancelListener:Landroid/view/View$OnClickListener;

    #@18
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 12
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v9, 0x3

    #@1
    const/4 v2, 0x0

    #@2
    .line 64
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@5
    .line 65
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 66
    const v0, 0x20a01c5

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->setTheme(I)V

    #@f
    .line 69
    :cond_f
    invoke-virtual {p0, v9}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->requestWindowFeature(I)Z

    #@12
    .line 71
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getIntent()Landroid/content/Intent;

    #@15
    move-result-object v0

    #@16
    const-string v1, "intent"

    #@18
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/content/IntentSender;

    #@1e
    iput-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mStartIntent:Landroid/content/IntentSender;

    #@20
    .line 72
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getIntent()Landroid/content/Intent;

    #@23
    move-result-object v0

    #@24
    const-string v1, "has_result"

    #@26
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@29
    move-result v0

    #@2a
    iput-boolean v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mHasResult:Z

    #@2c
    .line 73
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getIntent()Landroid/content/Intent;

    #@2f
    move-result-object v0

    #@30
    const-string v1, "cur_app"

    #@32
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    iput-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mCurApp:Ljava/lang/String;

    #@38
    .line 74
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getIntent()Landroid/content/Intent;

    #@3b
    move-result-object v0

    #@3c
    const-string v1, "cur_task"

    #@3e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@41
    move-result v0

    #@42
    iput v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mCurTask:I

    #@44
    .line 75
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getIntent()Landroid/content/Intent;

    #@47
    move-result-object v0

    #@48
    const-string/jumbo v1, "new_app"

    #@4b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v0

    #@4f
    iput-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mNewApp:Ljava/lang/String;

    #@51
    .line 77
    const v0, 0x1090047

    #@54
    invoke-virtual {p0, v0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->setContentView(I)V

    #@57
    .line 79
    const v1, 0x10202a0

    #@5a
    const v2, 0x10202a1

    #@5d
    const v3, 0x10202a2

    #@60
    iget-object v4, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mCurApp:Ljava/lang/String;

    #@62
    const v5, 0x1040417

    #@65
    const v6, 0x1040418

    #@68
    move-object v0, p0

    #@69
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->setIconAndText(IIILjava/lang/String;II)V

    #@6c
    .line 81
    const v1, 0x10202a4

    #@6f
    const v2, 0x10202a5

    #@72
    const v3, 0x10202a6

    #@75
    iget-object v4, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mNewApp:Ljava/lang/String;

    #@77
    const v5, 0x1040419

    #@7a
    const v6, 0x104041a

    #@7d
    move-object v0, p0

    #@7e
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->setIconAndText(IIILjava/lang/String;II)V

    #@81
    .line 84
    const v0, 0x102029f

    #@84
    invoke-virtual {p0, v0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->findViewById(I)Landroid/view/View;

    #@87
    move-result-object v7

    #@88
    .line 85
    .local v7, button:Landroid/view/View;
    iget-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mSwitchOldListener:Landroid/view/View$OnClickListener;

    #@8a
    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@8d
    .line 86
    const v0, 0x10202a3

    #@90
    invoke-virtual {p0, v0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->findViewById(I)Landroid/view/View;

    #@93
    move-result-object v7

    #@94
    .line 87
    iget-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mSwitchNewListener:Landroid/view/View$OnClickListener;

    #@96
    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@99
    .line 88
    const v0, 0x1020283

    #@9c
    invoke-virtual {p0, v0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->findViewById(I)Landroid/view/View;

    #@9f
    move-result-object v7

    #@a0
    .line 89
    iget-object v0, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mCancelListener:Landroid/view/View$OnClickListener;

    #@a2
    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@a5
    .line 91
    new-instance v8, Landroid/util/TypedValue;

    #@a7
    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    #@aa
    .line 92
    .local v8, out:Landroid/util/TypedValue;
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getTheme()Landroid/content/res/Resources$Theme;

    #@ad
    move-result-object v0

    #@ae
    const v1, 0x1010355

    #@b1
    const/4 v2, 0x1

    #@b2
    invoke-virtual {v0, v1, v8, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@b5
    .line 93
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getWindow()Landroid/view/Window;

    #@b8
    move-result-object v0

    #@b9
    iget v1, v8, Landroid/util/TypedValue;->resourceId:I

    #@bb
    invoke-virtual {v0, v9, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    #@be
    .line 95
    return-void
.end method

.method setDrawable(ILandroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "id"
    .parameter "dr"

    #@0
    .prologue
    .line 102
    if-eqz p2, :cond_b

    #@2
    .line 103
    invoke-virtual {p0, p1}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->findViewById(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/widget/ImageView;

    #@8
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 105
    :cond_b
    return-void
.end method

.method setIconAndText(IIILjava/lang/String;II)V
    .registers 13
    .parameter "iconId"
    .parameter "actionId"
    .parameter "descriptionId"
    .parameter "packageName"
    .parameter "actionStr"
    .parameter "descriptionStr"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 109
    const-string v1, ""

    #@3
    .line 110
    .local v1, appName:Ljava/lang/CharSequence;
    const/4 v0, 0x0

    #@4
    .line 111
    .local v0, appIcon:Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->mCurApp:Ljava/lang/String;

    #@6
    if-eqz v3, :cond_21

    #@8
    .line 113
    :try_start_8
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b
    move-result-object v3

    #@c
    const/4 v4, 0x0

    #@d
    invoke-virtual {v3, p4, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@10
    move-result-object v2

    #@11
    .line 115
    .local v2, info:Landroid/content/pm/ApplicationInfo;
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v2, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@18
    move-result-object v1

    #@19
    .line 116
    invoke-virtual {p0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v2, v3}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    :try_end_20
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_20} :catch_38

    #@20
    move-result-object v0

    #@21
    .line 121
    .end local v2           #info:Landroid/content/pm/ApplicationInfo;
    :cond_21
    :goto_21
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->setDrawable(ILandroid/graphics/drawable/Drawable;)V

    #@24
    .line 122
    const/4 v3, 0x1

    #@25
    new-array v3, v3, [Ljava/lang/Object;

    #@27
    aput-object v1, v3, v5

    #@29
    invoke-virtual {p0, p5, v3}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {p0, p2, v3}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->setText(ILjava/lang/CharSequence;)V

    #@30
    .line 123
    invoke-virtual {p0, p6}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->getText(I)Ljava/lang/CharSequence;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {p0, p3, v3}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->setText(ILjava/lang/CharSequence;)V

    #@37
    .line 124
    return-void

    #@38
    .line 117
    :catch_38
    move-exception v3

    #@39
    goto :goto_21
.end method

.method setText(ILjava/lang/CharSequence;)V
    .registers 4
    .parameter "id"
    .parameter "text"

    #@0
    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lcom/android/internal/app/HeavyWeightSwitcherActivity;->findViewById(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/widget/TextView;

    #@6
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@9
    .line 99
    return-void
.end method
