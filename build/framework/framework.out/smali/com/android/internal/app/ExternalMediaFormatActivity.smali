.class public Lcom/android/internal/app/ExternalMediaFormatActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "ExternalMediaFormatActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final POSITIVE_BUTTON:I = -0x1


# instance fields
.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mStorageReceiver:Landroid/content/BroadcastReceiver;

.field private mStorageVolume:Landroid/os/storage/StorageVolume;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 38
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@4
    .line 43
    iput-object v0, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@6
    .line 44
    iput-object v0, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageManager:Landroid/os/storage/StorageManager;

    #@8
    .line 47
    new-instance v0, Lcom/android/internal/app/ExternalMediaFormatActivity$1;

    #@a
    invoke-direct {v0, p0}, Lcom/android/internal/app/ExternalMediaFormatActivity$1;-><init>(Lcom/android/internal/app/ExternalMediaFormatActivity;)V

    #@d
    iput-object v0, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageReceiver:Landroid/content/BroadcastReceiver;

    #@f
    return-void
.end method

.method private getStorageVolume(Ljava/lang/String;)Landroid/os/storage/StorageVolume;
    .registers 8
    .parameter "path"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 125
    iget-object v4, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageManager:Landroid/os/storage/StorageManager;

    #@3
    if-nez v4, :cond_10

    #@5
    .line 126
    const-string/jumbo v4, "storage"

    #@8
    invoke-virtual {p0, v4}, Lcom/android/internal/app/ExternalMediaFormatActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v4

    #@c
    check-cast v4, Landroid/os/storage/StorageManager;

    #@e
    iput-object v4, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageManager:Landroid/os/storage/StorageManager;

    #@10
    .line 128
    :cond_10
    iget-object v4, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageManager:Landroid/os/storage/StorageManager;

    #@12
    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@15
    move-result-object v3

    #@16
    .line 129
    .local v3, storageVolumes:[Landroid/os/storage/StorageVolume;
    if-nez v3, :cond_1a

    #@18
    move-object v2, v5

    #@19
    .line 137
    :cond_19
    :goto_19
    return-object v2

    #@1a
    .line 130
    :cond_1a
    array-length v1, v3

    #@1b
    .line 131
    .local v1, length:I
    const/4 v0, 0x0

    #@1c
    .local v0, i:I
    :goto_1c
    if-ge v0, v1, :cond_2d

    #@1e
    .line 132
    aget-object v2, v3, v0

    #@20
    .line 133
    .local v2, storageVolume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v4

    #@28
    if-nez v4, :cond_19

    #@2a
    .line 131
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_1c

    #@2d
    .end local v2           #storageVolume:Landroid/os/storage/StorageVolume;
    :cond_2d
    move-object v2, v5

    #@2e
    .line 137
    goto :goto_19
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 108
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_1e

    #@3
    .line 109
    new-instance v0, Landroid/content/Intent;

    #@5
    const-string v1, "com.android.internal.os.storage.FORMAT_ONLY"

    #@7
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a
    .line 110
    .local v0, intent:Landroid/content/Intent;
    sget-object v1, Lcom/android/internal/os/storage/ExternalStorageFormatter;->COMPONENT_NAME:Landroid/content/ComponentName;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@f
    .line 112
    iget-object v1, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@11
    if-eqz v1, :cond_1b

    #@13
    .line 113
    const-string/jumbo v1, "storage_volume"

    #@16
    iget-object v2, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1b
    .line 116
    :cond_1b
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ExternalMediaFormatActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@1e
    .line 120
    .end local v0           #intent:Landroid/content/Intent;
    :cond_1e
    invoke-virtual {p0}, Lcom/android/internal/app/ExternalMediaFormatActivity;->finish()V

    #@21
    .line 121
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 66
    const-string v2, "ExternalMediaFormatActivity"

    #@5
    const-string/jumbo v3, "onCreate!"

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 68
    iget-object v0, p0, Lcom/android/internal/app/AlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@d
    .line 69
    .local v0, p:Lcom/android/internal/app/AlertController$AlertParams;
    const v2, 0x108008a

    #@10
    iput v2, v0, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    #@12
    .line 70
    const v2, 0x1040475

    #@15
    invoke-virtual {p0, v2}, Lcom/android/internal/app/ExternalMediaFormatActivity;->getString(I)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    iput-object v2, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@1b
    .line 71
    const v2, 0x1040476

    #@1e
    invoke-virtual {p0, v2}, Lcom/android/internal/app/ExternalMediaFormatActivity;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    iput-object v2, v0, Lcom/android/internal/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    #@24
    .line 72
    const v2, 0x1040477

    #@27
    invoke-virtual {p0, v2}, Lcom/android/internal/app/ExternalMediaFormatActivity;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    iput-object v2, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@2d
    .line 73
    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@2f
    .line 74
    const/high16 v2, 0x104

    #@31
    invoke-virtual {p0, v2}, Lcom/android/internal/app/ExternalMediaFormatActivity;->getString(I)Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    iput-object v2, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@37
    .line 75
    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@39
    .line 77
    invoke-virtual {p0}, Lcom/android/internal/app/ExternalMediaFormatActivity;->getIntent()Landroid/content/Intent;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, "STORAGE_PATH"

    #@3f
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    .line 78
    .local v1, path:Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/internal/app/ExternalMediaFormatActivity;->getStorageVolume(Ljava/lang/String;)Landroid/os/storage/StorageVolume;

    #@46
    move-result-object v2

    #@47
    iput-object v2, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@49
    .line 79
    const-string v2, "ExternalMediaFormatActivity"

    #@4b
    new-instance v3, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string/jumbo v4, "storage path "

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 81
    invoke-virtual {p0}, Lcom/android/internal/app/ExternalMediaFormatActivity;->setupAlert()V

    #@65
    .line 82
    return-void
.end method

.method protected onPause()V
    .registers 2

    #@0
    .prologue
    .line 98
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onPause()V

    #@3
    .line 100
    iget-object v0, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageReceiver:Landroid/content/BroadcastReceiver;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ExternalMediaFormatActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@8
    .line 101
    return-void
.end method

.method protected onResume()V
    .registers 3

    #@0
    .prologue
    .line 86
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onResume()V

    #@3
    .line 88
    new-instance v0, Landroid/content/IntentFilter;

    #@5
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@8
    .line 89
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    #@a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d
    .line 90
    const-string v1, "android.intent.action.MEDIA_CHECKING"

    #@f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@12
    .line 91
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 92
    const-string v1, "android.intent.action.MEDIA_SHARED"

    #@19
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 93
    iget-object v1, p0, Lcom/android/internal/app/ExternalMediaFormatActivity;->mStorageReceiver:Landroid/content/BroadcastReceiver;

    #@1e
    invoke-virtual {p0, v1, v0}, Lcom/android/internal/app/ExternalMediaFormatActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@21
    .line 94
    return-void
.end method
