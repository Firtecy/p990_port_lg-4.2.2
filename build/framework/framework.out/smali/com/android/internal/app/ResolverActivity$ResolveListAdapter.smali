.class final Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ResolverActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/ResolverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ResolveListAdapter"
.end annotation


# instance fields
.field private final mBaseResolveList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentResolveList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mInitialIntents:[Landroid/content/Intent;

.field private final mIntent:Landroid/content/Intent;

.field private final mLaunchedFromUid:I

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/internal/app/ResolverActivity;


# direct methods
.method public constructor <init>(Lcom/android/internal/app/ResolverActivity;Landroid/content/Context;Landroid/content/Intent;[Landroid/content/Intent;Ljava/util/List;I)V
    .registers 9
    .parameter
    .parameter "context"
    .parameter "intent"
    .parameter "initialIntents"
    .parameter
    .parameter "launchedFromUid"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "[",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 445
    .local p5, rList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@2
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@5
    .line 446
    new-instance v0, Landroid/content/Intent;

    #@7
    invoke-direct {v0, p3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@a
    iput-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    #@c
    .line 447
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@12
    .line 448
    iput-object p4, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    #@14
    .line 449
    iput-object p5, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mBaseResolveList:Ljava/util/List;

    #@16
    .line 450
    iput p6, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mLaunchedFromUid:I

    #@18
    .line 451
    const-string/jumbo v0, "layout_inflater"

    #@1b
    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/view/LayoutInflater;

    #@21
    iput-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@23
    .line 452
    invoke-direct {p0}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->rebuildList()V

    #@26
    .line 453
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 434
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@2
    return-object v0
.end method

.method private final bindView(Landroid/view/View;Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;)V
    .registers 8
    .parameter "view"
    .parameter "info"

    #@0
    .prologue
    .line 711
    const v3, 0x1020014

    #@3
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Landroid/widget/TextView;

    #@9
    .line 712
    .local v1, text:Landroid/widget/TextView;
    const v3, 0x1020015

    #@c
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/widget/TextView;

    #@12
    .line 713
    .local v2, text2:Landroid/widget/TextView;
    const v3, 0x1020006

    #@15
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/widget/ImageView;

    #@1b
    .line 714
    .local v0, icon:Landroid/widget/ImageView;
    iget-object v3, p2, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->displayLabel:Ljava/lang/CharSequence;

    #@1d
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@20
    .line 715
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@22
    invoke-static {v3}, Lcom/android/internal/app/ResolverActivity;->access$400(Lcom/android/internal/app/ResolverActivity;)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_45

    #@28
    .line 716
    const/4 v3, 0x0

    #@29
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    #@2c
    .line 717
    iget-object v3, p2, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->extendedInfo:Ljava/lang/CharSequence;

    #@2e
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@31
    .line 721
    :goto_31
    iget-object v3, p2, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->displayIcon:Landroid/graphics/drawable/Drawable;

    #@33
    if-nez v3, :cond_3f

    #@35
    .line 722
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@37
    iget-object v4, p2, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    #@39
    invoke-virtual {v3, v4}, Lcom/android/internal/app/ResolverActivity;->loadIconForResolveInfo(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    #@3c
    move-result-object v3

    #@3d
    iput-object v3, p2, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->displayIcon:Landroid/graphics/drawable/Drawable;

    #@3f
    .line 724
    :cond_3f
    iget-object v3, p2, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->displayIcon:Landroid/graphics/drawable/Drawable;

    #@41
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@44
    .line 725
    return-void

    #@45
    .line 719
    :cond_45
    const/16 v3, 0x8

    #@47
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    #@4a
    goto :goto_31
.end method

.method private processGroup(Ljava/util/List;IILandroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;)V
    .registers 22
    .parameter
    .parameter "start"
    .parameter "end"
    .parameter "ro"
    .parameter "roLabel"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;II",
            "Landroid/content/pm/ResolveInfo;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 611
    .local p1, rList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sub-int v1, p3, p2

    #@2
    add-int/lit8 v12, v1, 0x1

    #@4
    .line 612
    .local v12, num:I
    const/4 v1, 0x1

    #@5
    if-ne v12, v1, :cond_1e

    #@7
    .line 614
    move-object/from16 v0, p0

    #@9
    iget-object v15, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@b
    new-instance v1, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@d
    move-object/from16 v0, p0

    #@f
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@11
    const/4 v5, 0x0

    #@12
    const/4 v6, 0x0

    #@13
    move-object/from16 v3, p4

    #@15
    move-object/from16 v4, p5

    #@17
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/ResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    #@1a
    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1d
    .line 653
    :cond_1d
    return-void

    #@1e
    .line 616
    :cond_1e
    move-object/from16 v0, p0

    #@20
    iget-object v1, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@22
    const/4 v2, 0x1

    #@23
    invoke-static {v1, v2}, Lcom/android/internal/app/ResolverActivity;->access$402(Lcom/android/internal/app/ResolverActivity;Z)Z

    #@26
    .line 617
    const/4 v14, 0x0

    #@27
    .line 618
    .local v14, usePkg:Z
    move-object/from16 v0, p4

    #@29
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2b
    iget-object v1, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@31
    invoke-static {v2}, Lcom/android/internal/app/ResolverActivity;->access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@38
    move-result-object v13

    #@39
    .line 619
    .local v13, startApp:Ljava/lang/CharSequence;
    if-nez v13, :cond_3c

    #@3b
    .line 620
    const/4 v14, 0x1

    #@3c
    .line 622
    :cond_3c
    if-nez v14, :cond_70

    #@3e
    .line 624
    new-instance v7, Ljava/util/HashSet;

    #@40
    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    #@43
    .line 626
    .local v7, duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    invoke-virtual {v7, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@46
    .line 627
    add-int/lit8 v8, p2, 0x1

    #@48
    .local v8, j:I
    :goto_48
    move/from16 v0, p3

    #@4a
    if-gt v8, v0, :cond_6d

    #@4c
    .line 628
    move-object/from16 v0, p1

    #@4e
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v10

    #@52
    check-cast v10, Landroid/content/pm/ResolveInfo;

    #@54
    .line 629
    .local v10, jRi:Landroid/content/pm/ResolveInfo;
    iget-object v1, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@56
    iget-object v1, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@58
    move-object/from16 v0, p0

    #@5a
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@5c
    invoke-static {v2}, Lcom/android/internal/app/ResolverActivity;->access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@63
    move-result-object v9

    #@64
    .line 630
    .local v9, jApp:Ljava/lang/CharSequence;
    if-eqz v9, :cond_6c

    #@66
    invoke-virtual {v7, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@69
    move-result v1

    #@6a
    if-eqz v1, :cond_9a

    #@6c
    .line 631
    :cond_6c
    const/4 v14, 0x1

    #@6d
    .line 638
    .end local v9           #jApp:Ljava/lang/CharSequence;
    .end local v10           #jRi:Landroid/content/pm/ResolveInfo;
    :cond_6d
    invoke-virtual {v7}, Ljava/util/HashSet;->clear()V

    #@70
    .line 640
    .end local v7           #duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    .end local v8           #j:I
    :cond_70
    move/from16 v11, p2

    #@72
    .local v11, k:I
    :goto_72
    move/from16 v0, p3

    #@74
    if-gt v11, v0, :cond_1d

    #@76
    .line 641
    move-object/from16 v0, p1

    #@78
    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@7b
    move-result-object v3

    #@7c
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@7e
    .line 642
    .local v3, add:Landroid/content/pm/ResolveInfo;
    if-eqz v14, :cond_a0

    #@80
    .line 644
    move-object/from16 v0, p0

    #@82
    iget-object v15, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@84
    new-instance v1, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@86
    move-object/from16 v0, p0

    #@88
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@8a
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@8c
    iget-object v5, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8e
    const/4 v6, 0x0

    #@8f
    move-object/from16 v4, p5

    #@91
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/ResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    #@94
    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@97
    .line 640
    :goto_97
    add-int/lit8 v11, v11, 0x1

    #@99
    goto :goto_72

    #@9a
    .line 634
    .end local v3           #add:Landroid/content/pm/ResolveInfo;
    .end local v11           #k:I
    .restart local v7       #duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    .restart local v8       #j:I
    .restart local v9       #jApp:Ljava/lang/CharSequence;
    .restart local v10       #jRi:Landroid/content/pm/ResolveInfo;
    :cond_9a
    invoke-virtual {v7, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@9d
    .line 627
    add-int/lit8 v8, v8, 0x1

    #@9f
    goto :goto_48

    #@a0
    .line 648
    .end local v7           #duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    .end local v8           #j:I
    .end local v9           #jApp:Ljava/lang/CharSequence;
    .end local v10           #jRi:Landroid/content/pm/ResolveInfo;
    .restart local v3       #add:Landroid/content/pm/ResolveInfo;
    .restart local v11       #k:I
    :cond_a0
    move-object/from16 v0, p0

    #@a2
    iget-object v15, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@a4
    new-instance v1, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@a6
    move-object/from16 v0, p0

    #@a8
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@aa
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@ac
    iget-object v4, v4, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@ae
    move-object/from16 v0, p0

    #@b0
    iget-object v5, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@b2
    invoke-static {v5}, Lcom/android/internal/app/ResolverActivity;->access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v4, v5}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@b9
    move-result-object v5

    #@ba
    const/4 v6, 0x0

    #@bb
    move-object/from16 v4, p5

    #@bd
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/ResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    #@c0
    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@c3
    goto :goto_97
.end method

.method private rebuildList()V
    .registers 28

    #@0
    .prologue
    .line 475
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mBaseResolveList:Ljava/util/List;

    #@4
    if-eqz v2, :cond_7f

    #@6
    .line 476
    move-object/from16 v0, p0

    #@8
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mBaseResolveList:Ljava/util/List;

    #@a
    move-object/from16 v0, p0

    #@c
    iput-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@e
    .line 500
    :cond_e
    move-object/from16 v0, p0

    #@10
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@12
    if-eqz v2, :cond_254

    #@14
    move-object/from16 v0, p0

    #@16
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@18
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@1b
    move-result v17

    #@1c
    .local v17, N:I
    if-lez v17, :cond_254

    #@1e
    .line 503
    move-object/from16 v0, p0

    #@20
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@22
    const/4 v3, 0x0

    #@23
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v15

    #@27
    check-cast v15, Landroid/content/pm/ResolveInfo;

    #@29
    .line 506
    .local v15, r0:Landroid/content/pm/ResolveInfo;
    const/16 v22, 0x0

    #@2b
    .line 507
    .local v22, isSmartTextAvail:Z
    new-instance v26, Landroid/content/Intent;

    #@2d
    const-string v2, "com.lge.smarttext.action.SEND"

    #@2f
    move-object/from16 v0, v26

    #@31
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@34
    .line 508
    .local v26, smartTextIntent:Landroid/content/Intent;
    const-string/jumbo v2, "text/plain"

    #@37
    move-object/from16 v0, v26

    #@39
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@3c
    .line 509
    move-object/from16 v0, p0

    #@3e
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@40
    invoke-virtual {v2}, Lcom/android/internal/app/ResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@43
    move-result-object v2

    #@44
    const/high16 v3, 0x1

    #@46
    move-object/from16 v0, v26

    #@48
    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@4b
    move-result-object v18

    #@4c
    .line 510
    .local v18, activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v10, 0x0

    #@4d
    .line 513
    .local v10, TextLinkRi:Landroid/content/pm/ResolveInfo;
    const/16 v21, 0x1

    #@4f
    .local v21, i:I
    :goto_4f
    move/from16 v0, v21

    #@51
    move/from16 v1, v17

    #@53
    if-ge v0, v1, :cond_108

    #@55
    .line 514
    move-object/from16 v0, p0

    #@57
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@59
    move/from16 v0, v21

    #@5b
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v4

    #@5f
    check-cast v4, Landroid/content/pm/ResolveInfo;

    #@61
    .line 521
    .local v4, ri:Landroid/content/pm/ResolveInfo;
    iget v2, v15, Landroid/content/pm/ResolveInfo;->priority:I

    #@63
    iget v3, v4, Landroid/content/pm/ResolveInfo;->priority:I

    #@65
    if-ne v2, v3, :cond_6d

    #@67
    iget-boolean v2, v15, Landroid/content/pm/ResolveInfo;->isDefault:Z

    #@69
    iget-boolean v3, v4, Landroid/content/pm/ResolveInfo;->isDefault:Z

    #@6b
    if-eq v2, v3, :cond_ea

    #@6d
    .line 523
    :cond_6d
    :goto_6d
    move/from16 v0, v21

    #@6f
    move/from16 v1, v17

    #@71
    if-ge v0, v1, :cond_ea

    #@73
    .line 524
    move-object/from16 v0, p0

    #@75
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@77
    move/from16 v0, v21

    #@79
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@7c
    .line 525
    add-int/lit8 v17, v17, -0x1

    #@7e
    goto :goto_6d

    #@7f
    .line 478
    .end local v4           #ri:Landroid/content/pm/ResolveInfo;
    .end local v10           #TextLinkRi:Landroid/content/pm/ResolveInfo;
    .end local v15           #r0:Landroid/content/pm/ResolveInfo;
    .end local v17           #N:I
    .end local v18           #activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v21           #i:I
    .end local v22           #isSmartTextAvail:Z
    .end local v26           #smartTextIntent:Landroid/content/Intent;
    :cond_7f
    move-object/from16 v0, p0

    #@81
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@83
    invoke-static {v2}, Lcom/android/internal/app/ResolverActivity;->access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;

    #@86
    move-result-object v3

    #@87
    move-object/from16 v0, p0

    #@89
    iget-object v5, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    #@8b
    const/high16 v6, 0x1

    #@8d
    move-object/from16 v0, p0

    #@8f
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@91
    invoke-static {v2}, Lcom/android/internal/app/ResolverActivity;->access$200(Lcom/android/internal/app/ResolverActivity;)Z

    #@94
    move-result v2

    #@95
    if-eqz v2, :cond_e8

    #@97
    const/16 v2, 0x40

    #@99
    :goto_99
    or-int/2addr v2, v6

    #@9a
    invoke-virtual {v3, v5, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@9d
    move-result-object v2

    #@9e
    move-object/from16 v0, p0

    #@a0
    iput-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@a2
    .line 486
    move-object/from16 v0, p0

    #@a4
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@a6
    if-eqz v2, :cond_e

    #@a8
    .line 487
    move-object/from16 v0, p0

    #@aa
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@ac
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@af
    move-result v2

    #@b0
    add-int/lit8 v21, v2, -0x1

    #@b2
    .restart local v21       #i:I
    :goto_b2
    if-ltz v21, :cond_e

    #@b4
    .line 488
    move-object/from16 v0, p0

    #@b6
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@b8
    move/from16 v0, v21

    #@ba
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@bd
    move-result-object v2

    #@be
    check-cast v2, Landroid/content/pm/ResolveInfo;

    #@c0
    iget-object v0, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@c2
    move-object/from16 v19, v0

    #@c4
    .line 489
    .local v19, ai:Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, v19

    #@c6
    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@c8
    move-object/from16 v0, p0

    #@ca
    iget v3, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mLaunchedFromUid:I

    #@cc
    move-object/from16 v0, v19

    #@ce
    iget-object v5, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@d0
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@d2
    move-object/from16 v0, v19

    #@d4
    iget-boolean v6, v0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@d6
    invoke-static {v2, v3, v5, v6}, Landroid/app/ActivityManager;->checkComponentPermission(Ljava/lang/String;IIZ)I

    #@d9
    move-result v20

    #@da
    .line 492
    .local v20, granted:I
    if-eqz v20, :cond_e5

    #@dc
    .line 494
    move-object/from16 v0, p0

    #@de
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@e0
    move/from16 v0, v21

    #@e2
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@e5
    .line 487
    :cond_e5
    add-int/lit8 v21, v21, -0x1

    #@e7
    goto :goto_b2

    #@e8
    .line 478
    .end local v19           #ai:Landroid/content/pm/ActivityInfo;
    .end local v20           #granted:I
    .end local v21           #i:I
    :cond_e8
    const/4 v2, 0x0

    #@e9
    goto :goto_99

    #@ea
    .line 529
    .restart local v4       #ri:Landroid/content/pm/ResolveInfo;
    .restart local v10       #TextLinkRi:Landroid/content/pm/ResolveInfo;
    .restart local v15       #r0:Landroid/content/pm/ResolveInfo;
    .restart local v17       #N:I
    .restart local v18       #activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v21       #i:I
    .restart local v22       #isSmartTextAvail:Z
    .restart local v26       #smartTextIntent:Landroid/content/Intent;
    :cond_ea
    iget-object v2, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@ec
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@ee
    const-string v3, "com.lge.smarttext"

    #@f0
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f3
    move-result v2

    #@f4
    if-eqz v2, :cond_104

    #@f6
    .line 530
    const/16 v22, 0x1

    #@f8
    .line 531
    move-object v10, v4

    #@f9
    .line 533
    move-object/from16 v0, p0

    #@fb
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@fd
    move/from16 v0, v21

    #@ff
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@102
    .line 534
    add-int/lit8 v17, v17, -0x1

    #@104
    .line 513
    :cond_104
    add-int/lit8 v21, v21, 0x1

    #@106
    goto/16 :goto_4f

    #@108
    .line 538
    .end local v4           #ri:Landroid/content/pm/ResolveInfo;
    :cond_108
    const/4 v2, 0x1

    #@109
    move/from16 v0, v17

    #@10b
    if-le v0, v2, :cond_125

    #@10d
    .line 539
    new-instance v24, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    #@10f
    move-object/from16 v0, p0

    #@111
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@113
    invoke-static {v2}, Lcom/android/internal/app/ResolverActivity;->access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;

    #@116
    move-result-object v2

    #@117
    move-object/from16 v0, v24

    #@119
    invoke-direct {v0, v2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    #@11c
    .line 541
    .local v24, rComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;
    move-object/from16 v0, p0

    #@11e
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@120
    move-object/from16 v0, v24

    #@122
    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@125
    .line 544
    .end local v24           #rComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;
    :cond_125
    new-instance v2, Ljava/util/ArrayList;

    #@127
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@12a
    move-object/from16 v0, p0

    #@12c
    iput-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@12e
    .line 547
    move-object/from16 v0, p0

    #@130
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    #@132
    if-eqz v2, :cond_1b9

    #@134
    .line 548
    const/16 v21, 0x0

    #@136
    :goto_136
    move-object/from16 v0, p0

    #@138
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    #@13a
    array-length v2, v2

    #@13b
    move/from16 v0, v21

    #@13d
    if-ge v0, v2, :cond_1b9

    #@13f
    .line 549
    move-object/from16 v0, p0

    #@141
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    #@143
    aget-object v7, v2, v21

    #@145
    .line 550
    .local v7, ii:Landroid/content/Intent;
    if-nez v7, :cond_14a

    #@147
    .line 548
    :goto_147
    add-int/lit8 v21, v21, 0x1

    #@149
    goto :goto_136

    #@14a
    .line 553
    :cond_14a
    move-object/from16 v0, p0

    #@14c
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@14e
    invoke-virtual {v2}, Lcom/android/internal/app/ResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@151
    move-result-object v2

    #@152
    const/4 v3, 0x0

    #@153
    invoke-virtual {v7, v2, v3}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    #@156
    move-result-object v19

    #@157
    .line 555
    .restart local v19       #ai:Landroid/content/pm/ActivityInfo;
    if-nez v19, :cond_172

    #@159
    .line 556
    const-string v2, "ResolverActivity"

    #@15b
    new-instance v3, Ljava/lang/StringBuilder;

    #@15d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@160
    const-string v5, "No activity found for "

    #@162
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v3

    #@166
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v3

    #@16a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16d
    move-result-object v3

    #@16e
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@171
    goto :goto_147

    #@172
    .line 560
    :cond_172
    new-instance v4, Landroid/content/pm/ResolveInfo;

    #@174
    invoke-direct {v4}, Landroid/content/pm/ResolveInfo;-><init>()V

    #@177
    .line 561
    .restart local v4       #ri:Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v19

    #@179
    iput-object v0, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@17b
    .line 562
    instance-of v2, v7, Landroid/content/pm/LabeledIntent;

    #@17d
    if-eqz v2, :cond_19b

    #@17f
    move-object/from16 v23, v7

    #@181
    .line 563
    check-cast v23, Landroid/content/pm/LabeledIntent;

    #@183
    .line 564
    .local v23, li:Landroid/content/pm/LabeledIntent;
    invoke-virtual/range {v23 .. v23}, Landroid/content/pm/LabeledIntent;->getSourcePackage()Ljava/lang/String;

    #@186
    move-result-object v2

    #@187
    iput-object v2, v4, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@189
    .line 565
    invoke-virtual/range {v23 .. v23}, Landroid/content/pm/LabeledIntent;->getLabelResource()I

    #@18c
    move-result v2

    #@18d
    iput v2, v4, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@18f
    .line 566
    invoke-virtual/range {v23 .. v23}, Landroid/content/pm/LabeledIntent;->getNonLocalizedLabel()Ljava/lang/CharSequence;

    #@192
    move-result-object v2

    #@193
    iput-object v2, v4, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@195
    .line 567
    invoke-virtual/range {v23 .. v23}, Landroid/content/pm/LabeledIntent;->getIconResource()I

    #@198
    move-result v2

    #@199
    iput v2, v4, Landroid/content/pm/ResolveInfo;->icon:I

    #@19b
    .line 569
    .end local v23           #li:Landroid/content/pm/LabeledIntent;
    :cond_19b
    move-object/from16 v0, p0

    #@19d
    iget-object v8, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@19f
    new-instance v2, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    iget-object v3, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iget-object v5, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@1a9
    invoke-virtual {v5}, Lcom/android/internal/app/ResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1ac
    move-result-object v5

    #@1ad
    invoke-virtual {v4, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@1b0
    move-result-object v5

    #@1b1
    const/4 v6, 0x0

    #@1b2
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/ResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    #@1b5
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1b8
    goto :goto_147

    #@1b9
    .line 575
    .end local v4           #ri:Landroid/content/pm/ResolveInfo;
    .end local v7           #ii:Landroid/content/Intent;
    .end local v19           #ai:Landroid/content/pm/ActivityInfo;
    :cond_1b9
    if-eqz v22, :cond_1db

    #@1bb
    if-eqz v10, :cond_1db

    #@1bd
    .line 576
    move-object/from16 v0, p0

    #@1bf
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@1c1
    new-instance v8, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@1c3
    move-object/from16 v0, p0

    #@1c5
    iget-object v9, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@1c7
    move-object/from16 v0, p0

    #@1c9
    iget-object v3, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@1cb
    invoke-virtual {v3}, Lcom/android/internal/app/ResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1ce
    move-result-object v3

    #@1cf
    invoke-virtual {v10, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@1d2
    move-result-object v11

    #@1d3
    const/4 v12, 0x0

    #@1d4
    const/4 v13, 0x0

    #@1d5
    invoke-direct/range {v8 .. v13}, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/ResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    #@1d8
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1db
    .line 582
    :cond_1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@1df
    const/4 v3, 0x0

    #@1e0
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1e3
    move-result-object v15

    #@1e4
    .end local v15           #r0:Landroid/content/pm/ResolveInfo;
    check-cast v15, Landroid/content/pm/ResolveInfo;

    #@1e6
    .line 583
    .restart local v15       #r0:Landroid/content/pm/ResolveInfo;
    const/4 v13, 0x0

    #@1e7
    .line 584
    .local v13, start:I
    move-object/from16 v0, p0

    #@1e9
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@1eb
    invoke-static {v2}, Lcom/android/internal/app/ResolverActivity;->access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;

    #@1ee
    move-result-object v2

    #@1ef
    invoke-virtual {v15, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@1f2
    move-result-object v16

    #@1f3
    .line 585
    .local v16, r0Label:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@1f5
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@1f7
    const/4 v3, 0x0

    #@1f8
    invoke-static {v2, v3}, Lcom/android/internal/app/ResolverActivity;->access$402(Lcom/android/internal/app/ResolverActivity;Z)Z

    #@1fb
    .line 586
    const/16 v21, 0x1

    #@1fd
    :goto_1fd
    move/from16 v0, v21

    #@1ff
    move/from16 v1, v17

    #@201
    if-ge v0, v1, :cond_249

    #@203
    .line 587
    if-nez v16, :cond_20b

    #@205
    .line 588
    iget-object v2, v15, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@207
    iget-object v0, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@209
    move-object/from16 v16, v0

    #@20b
    .line 590
    :cond_20b
    move-object/from16 v0, p0

    #@20d
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@20f
    move/from16 v0, v21

    #@211
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@214
    move-result-object v4

    #@215
    check-cast v4, Landroid/content/pm/ResolveInfo;

    #@217
    .line 591
    .restart local v4       #ri:Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    #@219
    iget-object v2, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@21b
    invoke-static {v2}, Lcom/android/internal/app/ResolverActivity;->access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;

    #@21e
    move-result-object v2

    #@21f
    invoke-virtual {v4, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@222
    move-result-object v25

    #@223
    .line 592
    .local v25, riLabel:Ljava/lang/CharSequence;
    if-nez v25, :cond_22b

    #@225
    .line 593
    iget-object v2, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@227
    iget-object v0, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@229
    move-object/from16 v25, v0

    #@22b
    .line 595
    :cond_22b
    move-object/from16 v0, v25

    #@22d
    move-object/from16 v1, v16

    #@22f
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@232
    move-result v2

    #@233
    if-eqz v2, :cond_238

    #@235
    .line 586
    :goto_235
    add-int/lit8 v21, v21, 0x1

    #@237
    goto :goto_1fd

    #@238
    .line 598
    :cond_238
    move-object/from16 v0, p0

    #@23a
    iget-object v12, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@23c
    add-int/lit8 v14, v21, -0x1

    #@23e
    move-object/from16 v11, p0

    #@240
    invoke-direct/range {v11 .. v16}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->processGroup(Ljava/util/List;IILandroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;)V

    #@243
    .line 599
    move-object v15, v4

    #@244
    .line 600
    move-object/from16 v16, v25

    #@246
    .line 601
    move/from16 v13, v21

    #@248
    goto :goto_235

    #@249
    .line 604
    .end local v4           #ri:Landroid/content/pm/ResolveInfo;
    .end local v25           #riLabel:Ljava/lang/CharSequence;
    :cond_249
    move-object/from16 v0, p0

    #@24b
    iget-object v12, v0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@24d
    add-int/lit8 v14, v17, -0x1

    #@24f
    move-object/from16 v11, p0

    #@251
    invoke-direct/range {v11 .. v16}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->processGroup(Ljava/util/List;IILandroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;)V

    #@254
    .line 606
    .end local v10           #TextLinkRi:Landroid/content/pm/ResolveInfo;
    .end local v13           #start:I
    .end local v15           #r0:Landroid/content/pm/ResolveInfo;
    .end local v16           #r0Label:Ljava/lang/CharSequence;
    .end local v17           #N:I
    .end local v18           #activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v21           #i:I
    .end local v22           #isSmartTextAvail:Z
    .end local v26           #smartTextIntent:Landroid/content/Intent;
    :cond_254
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 681
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@6
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 685
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 689
    int-to-long v0, p1

    #@1
    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 694
    if-nez p2, :cond_35

    #@2
    .line 695
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@4
    const v4, 0x10900b5

    #@7
    const/4 v5, 0x0

    #@8
    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@b
    move-result-object v2

    #@c
    .line 698
    .local v2, view:Landroid/view/View;
    const v3, 0x202001e

    #@f
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    #@12
    .line 700
    const v3, 0x1020006

    #@15
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/widget/ImageView;

    #@1b
    .line 701
    .local v0, icon:Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1e
    move-result-object v1

    #@1f
    .line 702
    .local v1, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@21
    invoke-static {v3}, Lcom/android/internal/app/ResolverActivity;->access$500(Lcom/android/internal/app/ResolverActivity;)I

    #@24
    move-result v3

    #@25
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@27
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@29
    .line 706
    .end local v0           #icon:Landroid/widget/ImageView;
    .end local v1           #lp:Landroid/view/ViewGroup$LayoutParams;
    :goto_29
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@2b
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@31
    invoke-direct {p0, v2, v3}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->bindView(Landroid/view/View;Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;)V

    #@34
    .line 707
    return-object v2

    #@35
    .line 704
    .end local v2           #view:Landroid/view/View;
    :cond_35
    move-object v2, p2

    #@36
    .restart local v2       #view:Landroid/view/View;
    goto :goto_29
.end method

.method public handlePackagesChanged()V
    .registers 3

    #@0
    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->getCount()I

    #@3
    move-result v0

    #@4
    .line 457
    .local v0, oldItemCount:I
    invoke-direct {p0}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->rebuildList()V

    #@7
    .line 458
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->notifyDataSetChanged()V

    #@a
    .line 460
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@c
    if-eqz v1, :cond_1b

    #@e
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    #@10
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@13
    move-result v1

    #@14
    if-gtz v1, :cond_1b

    #@16
    .line 462
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/ResolverActivity;

    #@18
    invoke-virtual {v1}, Lcom/android/internal/app/ResolverActivity;->finish()V

    #@1b
    .line 472
    :cond_1b
    return-void
.end method

.method public intentForPosition(I)Landroid/content/Intent;
    .registers 8
    .parameter "position"

    #@0
    .prologue
    .line 664
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@2
    if-nez v3, :cond_6

    #@4
    .line 665
    const/4 v2, 0x0

    #@5
    .line 677
    :goto_5
    return-object v2

    #@6
    .line 668
    :cond_6
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@8
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@e
    .line 670
    .local v1, dri:Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;
    new-instance v2, Landroid/content/Intent;

    #@10
    iget-object v3, v1, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->origIntent:Landroid/content/Intent;

    #@12
    if-eqz v3, :cond_31

    #@14
    iget-object v3, v1, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->origIntent:Landroid/content/Intent;

    #@16
    :goto_16
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@19
    .line 672
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x300

    #@1b
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1e
    .line 674
    iget-object v3, v1, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    #@20
    iget-object v0, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@22
    .line 675
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    new-instance v3, Landroid/content/ComponentName;

    #@24
    iget-object v4, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@26
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@28
    iget-object v5, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@2a
    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@30
    goto :goto_5

    #@31
    .line 670
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    .end local v2           #intent:Landroid/content/Intent;
    :cond_31
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    #@33
    goto :goto_16
.end method

.method public resolveInfoForPosition(I)Landroid/content/pm/ResolveInfo;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 656
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 657
    const/4 v0, 0x0

    #@5
    .line 660
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    #@8
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@e
    iget-object v0, v0, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    #@10
    goto :goto_5
.end method
