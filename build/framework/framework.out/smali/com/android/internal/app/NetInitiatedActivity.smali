.class public Lcom/android/internal/app/NetInitiatedActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "NetInitiatedActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final DEBUG:Z = true

.field private static final GPS_NO_RESPONSE_TIME_OUT:I = 0x1

.field private static final NEGATIVE_BUTTON:I = -0x2

.field private static final POSITIVE_BUTTON:I = -0x1

.field private static final TAG:Ljava/lang/String; = "NetInitiatedActivity"

.field private static final VERBOSE:Z


# instance fields
.field private default_response:I

.field private default_response_timeout:I

.field private final mHandler:Landroid/os/Handler;

.field private mNetInitiatedReceiver:Landroid/content/BroadcastReceiver;

.field private notificationId:I

.field private timeout:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 39
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@4
    .line 51
    iput v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->notificationId:I

    #@6
    .line 52
    iput v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->timeout:I

    #@8
    .line 53
    iput v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->default_response:I

    #@a
    .line 54
    const/4 v0, 0x6

    #@b
    iput v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->default_response_timeout:I

    #@d
    .line 57
    new-instance v0, Lcom/android/internal/app/NetInitiatedActivity$1;

    #@f
    invoke-direct {v0, p0}, Lcom/android/internal/app/NetInitiatedActivity$1;-><init>(Lcom/android/internal/app/NetInitiatedActivity;)V

    #@12
    iput-object v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->mNetInitiatedReceiver:Landroid/content/BroadcastReceiver;

    #@14
    .line 67
    new-instance v0, Lcom/android/internal/app/NetInitiatedActivity$2;

    #@16
    invoke-direct {v0, p0}, Lcom/android/internal/app/NetInitiatedActivity$2;-><init>(Lcom/android/internal/app/NetInitiatedActivity;)V

    #@19
    iput-object v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->mHandler:Landroid/os/Handler;

    #@1b
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/app/NetInitiatedActivity;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/android/internal/app/NetInitiatedActivity;->handleNIVerify(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/app/NetInitiatedActivity;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 39
    iget v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->notificationId:I

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/app/NetInitiatedActivity;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 39
    iget v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->default_response:I

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/app/NetInitiatedActivity;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/android/internal/app/NetInitiatedActivity;->sendUserResponse(I)V

    #@3
    return-void
.end method

.method private handleNIVerify(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 149
    const-string/jumbo v1, "notif_id"

    #@3
    const/4 v2, -0x1

    #@4
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    .line 150
    .local v0, notifId:I
    iput v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->notificationId:I

    #@a
    .line 152
    const-string v1, "NetInitiatedActivity"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "handleNIVerify action: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 153
    return-void
.end method

.method private sendUserResponse(I)V
    .registers 6
    .parameter "response"

    #@0
    .prologue
    .line 142
    const-string v1, "NetInitiatedActivity"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "sendUserResponse, response: "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 143
    const-string/jumbo v1, "location"

    #@1c
    invoke-virtual {p0, v1}, Lcom/android/internal/app/NetInitiatedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/location/LocationManager;

    #@22
    .line 145
    .local v0, locationManager:Landroid/location/LocationManager;
    iget v1, p0, Lcom/android/internal/app/NetInitiatedActivity;->notificationId:I

    #@24
    invoke-virtual {v0, v1, p1}, Landroid/location/LocationManager;->sendNiResponse(II)Z

    #@27
    .line 146
    return-void
.end method

.method private showNIError()V
    .registers 3

    #@0
    .prologue
    .line 156
    const-string v0, "NI error"

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@a
    .line 158
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 5
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 128
    if-ne p2, v1, :cond_7

    #@3
    .line 129
    const/4 v0, 0x1

    #@4
    invoke-direct {p0, v0}, Lcom/android/internal/app/NetInitiatedActivity;->sendUserResponse(I)V

    #@7
    .line 131
    :cond_7
    const/4 v0, -0x2

    #@8
    if-ne p2, v0, :cond_e

    #@a
    .line 132
    const/4 v0, 0x2

    #@b
    invoke-direct {p0, v0}, Lcom/android/internal/app/NetInitiatedActivity;->sendUserResponse(I)V

    #@e
    .line 136
    :cond_e
    invoke-virtual {p0}, Lcom/android/internal/app/NetInitiatedActivity;->finish()V

    #@11
    .line 137
    iput v1, p0, Lcom/android/internal/app/NetInitiatedActivity;->notificationId:I

    #@13
    .line 138
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 84
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@5
    .line 85
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@7
    if-eqz v3, :cond_f

    #@9
    .line 86
    const v3, 0x20a01cb

    #@c
    invoke-virtual {p0, v3}, Lcom/android/internal/app/NetInitiatedActivity;->setTheme(I)V

    #@f
    .line 90
    :cond_f
    invoke-virtual {p0}, Lcom/android/internal/app/NetInitiatedActivity;->getIntent()Landroid/content/Intent;

    #@12
    move-result-object v1

    #@13
    .line 91
    .local v1, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/app/AlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@15
    .line 92
    .local v2, p:Lcom/android/internal/app/AlertController$AlertParams;
    invoke-virtual {p0}, Lcom/android/internal/app/NetInitiatedActivity;->getApplicationContext()Landroid/content/Context;

    #@18
    move-result-object v0

    #@19
    .line 93
    .local v0, context:Landroid/content/Context;
    const v3, 0x10802ce

    #@1c
    iput v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    #@1e
    .line 94
    const-string/jumbo v3, "title"

    #@21
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@27
    .line 95
    const-string/jumbo v3, "message"

    #@2a
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    #@30
    .line 96
    const v3, 0x10404d9

    #@33
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    new-array v4, v5, [Ljava/lang/Object;

    #@39
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@3f
    .line 97
    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@41
    .line 98
    const v3, 0x10404da

    #@44
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    new-array v4, v5, [Ljava/lang/Object;

    #@4a
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@50
    .line 99
    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@52
    .line 101
    const-string/jumbo v3, "notif_id"

    #@55
    const/4 v4, -0x1

    #@56
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@59
    move-result v3

    #@5a
    iput v3, p0, Lcom/android/internal/app/NetInitiatedActivity;->notificationId:I

    #@5c
    .line 102
    const-string/jumbo v3, "timeout"

    #@5f
    iget v4, p0, Lcom/android/internal/app/NetInitiatedActivity;->default_response_timeout:I

    #@61
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@64
    move-result v3

    #@65
    iput v3, p0, Lcom/android/internal/app/NetInitiatedActivity;->timeout:I

    #@67
    .line 103
    const-string v3, "default_resp"

    #@69
    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6c
    move-result v3

    #@6d
    iput v3, p0, Lcom/android/internal/app/NetInitiatedActivity;->default_response:I

    #@6f
    .line 104
    const-string v3, "NetInitiatedActivity"

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string/jumbo v5, "onCreate() : notificationId: "

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    iget v5, p0, Lcom/android/internal/app/NetInitiatedActivity;->notificationId:I

    #@7f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    const-string v5, " timeout: "

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    iget v5, p0, Lcom/android/internal/app/NetInitiatedActivity;->timeout:I

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    const-string v5, " default_response:"

    #@91
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    iget v5, p0, Lcom/android/internal/app/NetInitiatedActivity;->default_response:I

    #@97
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v4

    #@9f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 106
    iget-object v3, p0, Lcom/android/internal/app/NetInitiatedActivity;->mHandler:Landroid/os/Handler;

    #@a4
    iget-object v4, p0, Lcom/android/internal/app/NetInitiatedActivity;->mHandler:Landroid/os/Handler;

    #@a6
    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@a9
    move-result-object v4

    #@aa
    iget v5, p0, Lcom/android/internal/app/NetInitiatedActivity;->timeout:I

    #@ac
    mul-int/lit16 v5, v5, 0x3e8

    #@ae
    int-to-long v5, v5

    #@af
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@b2
    .line 107
    invoke-virtual {p0}, Lcom/android/internal/app/NetInitiatedActivity;->setupAlert()V

    #@b5
    .line 108
    return-void
.end method

.method protected onPause()V
    .registers 3

    #@0
    .prologue
    .line 119
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onPause()V

    #@3
    .line 120
    const-string v0, "NetInitiatedActivity"

    #@5
    const-string/jumbo v1, "onPause"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 121
    iget-object v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->mNetInitiatedReceiver:Landroid/content/BroadcastReceiver;

    #@d
    invoke-virtual {p0, v0}, Lcom/android/internal/app/NetInitiatedActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@10
    .line 122
    return-void
.end method

.method protected onResume()V
    .registers 4

    #@0
    .prologue
    .line 112
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onResume()V

    #@3
    .line 113
    const-string v0, "NetInitiatedActivity"

    #@5
    const-string/jumbo v1, "onResume"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 114
    iget-object v0, p0, Lcom/android/internal/app/NetInitiatedActivity;->mNetInitiatedReceiver:Landroid/content/BroadcastReceiver;

    #@d
    new-instance v1, Landroid/content/IntentFilter;

    #@f
    const-string v2, "android.intent.action.NETWORK_INITIATED_VERIFY"

    #@11
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@14
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/NetInitiatedActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@17
    .line 115
    return-void
.end method
