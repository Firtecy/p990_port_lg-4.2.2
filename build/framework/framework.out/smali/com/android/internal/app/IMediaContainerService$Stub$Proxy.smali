.class Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IMediaContainerService.java"

# interfaces
.implements Lcom/android/internal/app/IMediaContainerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/IMediaContainerService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 221
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 222
    iput-object p1, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 223
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public calculateDirectorySize(Ljava/lang/String;)J
    .registers 9
    .parameter "directory"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 401
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 402
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 405
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "com.android.internal.app.IMediaContainerService"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 406
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 407
    iget-object v4, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v5, 0x7

    #@13
    const/4 v6, 0x0

    #@14
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 408
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 409
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-wide v2

    #@1e
    .line 412
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 413
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 415
    return-wide v2

    #@25
    .line 412
    .end local v2           #_result:J
    :catchall_25
    move-exception v4

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 413
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v4
.end method

.method public calculateInstalledSize(Ljava/lang/String;Z)J
    .registers 10
    .parameter "packagePath"
    .parameter "isForwardLocked"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 453
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 454
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 457
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v5, "com.android.internal.app.IMediaContainerService"

    #@b
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 458
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 459
    if-eqz p2, :cond_14

    #@13
    const/4 v4, 0x1

    #@14
    :cond_14
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 460
    iget-object v4, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v5, 0xa

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 461
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 462
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_25
    .catchall {:try_start_9 .. :try_end_25} :catchall_2d

    #@25
    move-result-wide v2

    #@26
    .line 465
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 466
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 468
    return-wide v2

    #@2d
    .line 465
    .end local v2           #_result:J
    :catchall_2d
    move-exception v4

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 466
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v4
.end method

.method public checkExternalFreeStorage(Landroid/net/Uri;Z)Z
    .registers 10
    .parameter "fileUri"
    .parameter "isForwardLocked"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 353
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 354
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 357
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.app.IMediaContainerService"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 358
    if-eqz p1, :cond_36

    #@11
    .line 359
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 360
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 365
    :goto_19
    if-eqz p2, :cond_43

    #@1b
    move v4, v2

    #@1c
    :goto_1c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 366
    iget-object v4, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/4 v5, 0x5

    #@22
    const/4 v6, 0x0

    #@23
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@26
    .line 367
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@29
    .line 368
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2c
    .catchall {:try_start_a .. :try_end_2c} :catchall_3b

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_45

    #@2f
    .line 371
    .local v2, _result:Z
    :goto_2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 372
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 374
    return v2

    #@36
    .line 363
    .end local v2           #_result:Z
    :cond_36
    const/4 v4, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_19

    #@3b
    .line 371
    :catchall_3b
    move-exception v3

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 372
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v3

    #@43
    :cond_43
    move v4, v3

    #@44
    .line 365
    goto :goto_1c

    #@45
    :cond_45
    move v2, v3

    #@46
    .line 368
    goto :goto_2f
.end method

.method public checkInternalFreeStorage(Landroid/net/Uri;ZJ)Z
    .registers 12
    .parameter "fileUri"
    .parameter "isForwardLocked"
    .parameter "threshold"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 327
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 328
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 331
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.app.IMediaContainerService"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 332
    if-eqz p1, :cond_39

    #@11
    .line 333
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 334
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 339
    :goto_19
    if-eqz p2, :cond_46

    #@1b
    move v4, v2

    #@1c
    :goto_1c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 340
    invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V

    #@22
    .line 341
    iget-object v4, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/4 v5, 0x4

    #@25
    const/4 v6, 0x0

    #@26
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 342
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2c
    .line 343
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2f
    .catchall {:try_start_a .. :try_end_2f} :catchall_3e

    #@2f
    move-result v4

    #@30
    if-eqz v4, :cond_48

    #@32
    .line 346
    .local v2, _result:Z
    :goto_32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 347
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 349
    return v2

    #@39
    .line 337
    .end local v2           #_result:Z
    :cond_39
    const/4 v4, 0x0

    #@3a
    :try_start_3a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3d
    .catchall {:try_start_3a .. :try_end_3d} :catchall_3e

    #@3d
    goto :goto_19

    #@3e
    .line 346
    :catchall_3e
    move-exception v3

    #@3f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 347
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@45
    throw v3

    #@46
    :cond_46
    move v4, v3

    #@47
    .line 339
    goto :goto_1c

    #@48
    :cond_48
    move v2, v3

    #@49
    .line 343
    goto :goto_32
.end method

.method public clearDirectory(Ljava/lang/String;)V
    .registers 7
    .parameter "directory"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 438
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 439
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 441
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.app.IMediaContainerService"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 442
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 443
    iget-object v2, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x9

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 444
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 447
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 448
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 450
    return-void

    #@22
    .line 447
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 448
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public copyResource(Landroid/net/Uri;Landroid/content/pm/ContainerEncryptionParams;Landroid/os/ParcelFileDescriptor;)I
    .registers 10
    .parameter "packageURI"
    .parameter "encryptionParams"
    .parameter "outStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 264
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 265
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 268
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.app.IMediaContainerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 269
    if-eqz p1, :cond_40

    #@f
    .line 270
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 271
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 276
    :goto_17
    if-eqz p2, :cond_4d

    #@19
    .line 277
    const/4 v3, 0x1

    #@1a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 278
    const/4 v3, 0x0

    #@1e
    invoke-virtual {p2, v0, v3}, Landroid/content/pm/ContainerEncryptionParams;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 283
    :goto_21
    if-eqz p3, :cond_52

    #@23
    .line 284
    const/4 v3, 0x1

    #@24
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 285
    const/4 v3, 0x0

    #@28
    invoke-virtual {p3, v0, v3}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@2b
    .line 290
    :goto_2b
    iget-object v3, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2d
    const/4 v4, 0x2

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@32
    .line 291
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@35
    .line 292
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_38
    .catchall {:try_start_8 .. :try_end_38} :catchall_45

    #@38
    move-result v2

    #@39
    .line 295
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 296
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 298
    return v2

    #@40
    .line 274
    .end local v2           #_result:I
    :cond_40
    const/4 v3, 0x0

    #@41
    :try_start_41
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_45

    #@44
    goto :goto_17

    #@45
    .line 295
    :catchall_45
    move-exception v3

    #@46
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@49
    .line 296
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4c
    throw v3

    #@4d
    .line 281
    :cond_4d
    const/4 v3, 0x0

    #@4e
    :try_start_4e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@51
    goto :goto_21

    #@52
    .line 288
    :cond_52
    const/4 v3, 0x0

    #@53
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_56
    .catchall {:try_start_4e .. :try_end_56} :catchall_45

    #@56
    goto :goto_2b
.end method

.method public copyResourceToContainer(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .registers 14
    .parameter "packageURI"
    .parameter "containerId"
    .parameter "key"
    .parameter "resFileName"
    .parameter "publicResFileName"
    .parameter "isExternal"
    .parameter "isForwardLocked"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 234
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 235
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 238
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v5, "com.android.internal.app.IMediaContainerService"

    #@c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 239
    if-eqz p1, :cond_45

    #@11
    .line 240
    const/4 v5, 0x1

    #@12
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 241
    const/4 v5, 0x0

    #@16
    invoke-virtual {p1, v0, v5}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 246
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 247
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    .line 248
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@22
    .line 249
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    .line 250
    if-eqz p6, :cond_52

    #@27
    move v5, v3

    #@28
    :goto_28
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 251
    if-eqz p7, :cond_54

    #@2d
    :goto_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 252
    iget-object v3, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@32
    const/4 v4, 0x1

    #@33
    const/4 v5, 0x0

    #@34
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@37
    .line 253
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@3a
    .line 254
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_3d
    .catchall {:try_start_a .. :try_end_3d} :catchall_4a

    #@3d
    move-result-object v2

    #@3e
    .line 257
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 258
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    .line 260
    return-object v2

    #@45
    .line 244
    .end local v2           #_result:Ljava/lang/String;
    :cond_45
    const/4 v5, 0x0

    #@46
    :try_start_46
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_4a

    #@49
    goto :goto_19

    #@4a
    .line 257
    :catchall_4a
    move-exception v3

    #@4b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4e
    .line 258
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@51
    throw v3

    #@52
    :cond_52
    move v5, v4

    #@53
    .line 250
    goto :goto_28

    #@54
    :cond_54
    move v3, v4

    #@55
    .line 251
    goto :goto_2d
.end method

.method public getFileSystemStats(Ljava/lang/String;)[J
    .registers 8
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 420
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 421
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 424
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.app.IMediaContainerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 425
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 426
    iget-object v3, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x8

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 427
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 428
    invoke-virtual {v1}, Landroid/os/Parcel;->createLongArray()[J
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-object v2

    #@1f
    .line 431
    .local v2, _result:[J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 432
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 434
    return-object v2

    #@26
    .line 431
    .end local v2           #_result:[J
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 432
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 230
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@2
    return-object v0
.end method

.method public getMinimalPackageInfo(Ljava/lang/String;IJ)Landroid/content/pm/PackageInfoLite;
    .registers 11
    .parameter "packagePath"
    .parameter "flags"
    .parameter "threshold"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 302
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 303
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 306
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.app.IMediaContainerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 307
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 308
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 309
    invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V

    #@16
    .line 310
    iget-object v3, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v4, 0x3

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 311
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@20
    .line 312
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_35

    #@26
    .line 313
    sget-object v3, Landroid/content/pm/PackageInfoLite;->CREATOR:Landroid/os/Parcelable$Creator;

    #@28
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2b
    move-result-object v2

    #@2c
    check-cast v2, Landroid/content/pm/PackageInfoLite;
    :try_end_2e
    .catchall {:try_start_8 .. :try_end_2e} :catchall_37

    #@2e
    .line 320
    .local v2, _result:Landroid/content/pm/PackageInfoLite;
    :goto_2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 321
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 323
    return-object v2

    #@35
    .line 316
    .end local v2           #_result:Landroid/content/pm/PackageInfoLite;
    :cond_35
    const/4 v2, 0x0

    #@36
    .restart local v2       #_result:Landroid/content/pm/PackageInfoLite;
    goto :goto_2e

    #@37
    .line 320
    .end local v2           #_result:Landroid/content/pm/PackageInfoLite;
    :catchall_37
    move-exception v3

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 321
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v3
.end method

.method public getObbInfo(Ljava/lang/String;)Landroid/content/res/ObbInfo;
    .registers 8
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 378
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 379
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 382
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.app.IMediaContainerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 383
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 384
    iget-object v3, p0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x6

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 385
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 386
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_2f

    #@20
    .line 387
    sget-object v3, Landroid/content/res/ObbInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/content/res/ObbInfo;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_31

    #@28
    .line 394
    .local v2, _result:Landroid/content/res/ObbInfo;
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 395
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 397
    return-object v2

    #@2f
    .line 390
    .end local v2           #_result:Landroid/content/res/ObbInfo;
    :cond_2f
    const/4 v2, 0x0

    #@30
    .restart local v2       #_result:Landroid/content/res/ObbInfo;
    goto :goto_28

    #@31
    .line 394
    .end local v2           #_result:Landroid/content/res/ObbInfo;
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 395
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method
