.class Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;
.super Landroid/widget/BaseAdapter;
.source "MediaRouteChooserDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RouteAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;
    }
.end annotation


# static fields
.field private static final VIEW_GROUPING_DONE:I = 0x4

.field private static final VIEW_GROUPING_ROUTE:I = 0x3

.field private static final VIEW_ROUTE:I = 0x2

.field private static final VIEW_SECTION_HEADER:I = 0x1

.field private static final VIEW_TOP_HEADER:I


# instance fields
.field private final mCatRouteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

.field private mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

.field private mIgnoreUpdates:Z

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemPosition:I

.field private final mSortRouteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 244
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@5
    .line 232
    const/4 v0, -0x1

    #@6
    iput v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@8
    .line 233
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@f
    .line 239
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCatRouteList:Ljava/util/ArrayList;

    #@16
    .line 240
    new-instance v0, Ljava/util/ArrayList;

    #@18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    #@1d
    .line 245
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    #@20
    .line 246
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;)Landroid/media/MediaRouter$RouteGroup;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 225
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;Landroid/media/MediaRouter$RouteGroup;)Landroid/media/MediaRouter$RouteGroup;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 225
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@2
    return-object p1
.end method

.method static synthetic access$602(Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$RouteCategory;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 225
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@2
    return-object p1
.end method


# virtual methods
.method addGroupEditingCategoryRoutes(Ljava/util/List;)V
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 331
    .local p1, from:Ljava/util/List;,"Ljava/util/List<Landroid/media/MediaRouter$RouteInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@3
    move-result v6

    #@4
    .line 332
    .local v6, topCount:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v6, :cond_2e

    #@7
    .line 333
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v5

    #@b
    check-cast v5, Landroid/media/MediaRouter$RouteInfo;

    #@d
    .line 334
    .local v5, route:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v5}, Landroid/media/MediaRouter$RouteInfo;->getGroup()Landroid/media/MediaRouter$RouteGroup;

    #@10
    move-result-object v0

    #@11
    .line 335
    .local v0, group:Landroid/media/MediaRouter$RouteGroup;
    if-ne v0, v5, :cond_26

    #@13
    .line 337
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@16
    move-result v1

    #@17
    .line 338
    .local v1, groupCount:I
    const/4 v4, 0x0

    #@18
    .local v4, j:I
    :goto_18
    if-ge v4, v1, :cond_2b

    #@1a
    .line 339
    invoke-virtual {v0, v4}, Landroid/media/MediaRouter$RouteGroup;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    #@1d
    move-result-object v3

    #@1e
    .line 340
    .local v3, innerRoute:Landroid/media/MediaRouter$RouteInfo;
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 338
    add-int/lit8 v4, v4, 0x1

    #@25
    goto :goto_18

    #@26
    .line 343
    .end local v1           #groupCount:I
    .end local v3           #innerRoute:Landroid/media/MediaRouter$RouteInfo;
    .end local v4           #j:I
    :cond_26
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2b
    .line 332
    :cond_2b
    add-int/lit8 v2, v2, 0x1

    #@2d
    goto :goto_5

    #@2e
    .line 348
    .end local v0           #group:Landroid/media/MediaRouter$RouteGroup;
    .end local v5           #route:Landroid/media/MediaRouter$RouteInfo;
    :cond_2e
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    #@30
    iget-object v8, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@32
    iget-object v8, v8, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mComparator:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteComparator;

    #@34
    invoke-static {v7, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@37
    .line 350
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@39
    iget-object v8, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    #@3b
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@3e
    .line 351
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    #@43
    .line 353
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@45
    const/4 v8, 0x0

    #@46
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    .line 354
    return-void
.end method

.method addSelectableRoutes(Landroid/media/MediaRouter$RouteInfo;Ljava/util/List;)V
    .registers 7
    .parameter "selectedRoute"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/MediaRouter$RouteInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 318
    .local p2, from:Ljava/util/List;,"Ljava/util/List<Landroid/media/MediaRouter$RouteInfo;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@3
    move-result v2

    #@4
    .line 319
    .local v2, routeCount:I
    const/4 v1, 0x0

    #@5
    .local v1, j:I
    :goto_5
    if-ge v1, v2, :cond_1f

    #@7
    .line 320
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    #@d
    .line 321
    .local v0, info:Landroid/media/MediaRouter$RouteInfo;
    if-ne v0, p1, :cond_17

    #@f
    .line 322
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v3

    #@15
    iput v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@17
    .line 324
    :cond_17
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 319
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_5

    #@1f
    .line 326
    .end local v0           #info:Landroid/media/MediaRouter$RouteInfo;
    :cond_1f
    return-void
.end method

.method public areAllItemsEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 384
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method bindHeaderView(ILcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;)V
    .registers 6
    .parameter "position"
    .parameter "holder"

    #@0
    .prologue
    .line 502
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/media/MediaRouter$RouteCategory;

    #@8
    .line 503
    .local v0, cat:Landroid/media/MediaRouter$RouteCategory;
    iget-object v1, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    #@a
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@c
    invoke-virtual {v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getActivity()Landroid/app/Activity;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v0, v2}, Landroid/media/MediaRouter$RouteCategory;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@17
    .line 504
    return-void
.end method

.method bindItemView(ILcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;)V
    .registers 15
    .parameter "position"
    .parameter "holder"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/16 v8, 0x8

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v7, 0x0

    #@5
    .line 462
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v4

    #@b
    check-cast v4, Landroid/media/MediaRouter$RouteInfo;

    #@d
    .line 463
    .local v4, info:Landroid/media/MediaRouter$RouteInfo;
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    #@f
    iget-object v10, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@11
    invoke-virtual {v10}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getActivity()Landroid/app/Activity;

    #@14
    move-result-object v10

    #@15
    invoke-virtual {v4, v10}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@18
    move-result-object v10

    #@19
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1c
    .line 464
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getStatus()Ljava/lang/CharSequence;

    #@1f
    move-result-object v5

    #@20
    .line 465
    .local v5, status:Ljava/lang/CharSequence;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@23
    move-result v6

    #@24
    if-eqz v6, :cond_7e

    #@26
    .line 466
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    #@28
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    #@2b
    .line 471
    :goto_2b
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    #@2e
    move-result-object v3

    #@2f
    .line 472
    .local v3, icon:Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_3f

    #@31
    .line 474
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@34
    move-result-object v6

    #@35
    iget-object v10, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@37
    invoke-virtual {v10}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getResources()Landroid/content/res/Resources;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v6, v10}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v3

    #@3f
    .line 476
    :cond_3f
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    #@41
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@44
    .line 477
    iget-object v10, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    #@46
    if-eqz v3, :cond_89

    #@48
    move v6, v7

    #@49
    :goto_49
    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    #@4c
    .line 479
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@4f
    move-result-object v1

    #@50
    .line 480
    .local v1, cat:Landroid/media/MediaRouter$RouteCategory;
    const/4 v0, 0x0

    #@51
    .line 481
    .local v0, canGroup:Z
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@53
    if-ne v1, v6, :cond_8f

    #@55
    .line 482
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getGroup()Landroid/media/MediaRouter$RouteGroup;

    #@58
    move-result-object v2

    #@59
    .line 483
    .local v2, group:Landroid/media/MediaRouter$RouteGroup;
    iget-object v10, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->check:Landroid/widget/CheckBox;

    #@5b
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@5e
    move-result v6

    #@5f
    if-le v6, v9, :cond_8b

    #@61
    move v6, v9

    #@62
    :goto_62
    invoke-virtual {v10, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    #@65
    .line 484
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->check:Landroid/widget/CheckBox;

    #@67
    iget-object v10, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@69
    if-ne v2, v10, :cond_8d

    #@6b
    :goto_6b
    invoke-virtual {v6, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    #@6e
    .line 495
    .end local v2           #group:Landroid/media/MediaRouter$RouteGroup;
    :cond_6e
    :goto_6e
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    #@70
    if-eqz v6, :cond_7d

    #@72
    .line 496
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    #@74
    if-eqz v0, :cond_ba

    #@76
    :goto_76
    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    #@79
    .line 497
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupListener:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    #@7b
    iput p1, v6, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;->position:I

    #@7d
    .line 499
    :cond_7d
    return-void

    #@7e
    .line 468
    .end local v0           #canGroup:Z
    .end local v1           #cat:Landroid/media/MediaRouter$RouteCategory;
    .end local v3           #icon:Landroid/graphics/drawable/Drawable;
    :cond_7e
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    #@80
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    #@83
    .line 469
    iget-object v6, p2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    #@85
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@88
    goto :goto_2b

    #@89
    .restart local v3       #icon:Landroid/graphics/drawable/Drawable;
    :cond_89
    move v6, v8

    #@8a
    .line 477
    goto :goto_49

    #@8b
    .restart local v0       #canGroup:Z
    .restart local v1       #cat:Landroid/media/MediaRouter$RouteCategory;
    .restart local v2       #group:Landroid/media/MediaRouter$RouteGroup;
    :cond_8b
    move v6, v7

    #@8c
    .line 483
    goto :goto_62

    #@8d
    :cond_8d
    move v9, v7

    #@8e
    .line 484
    goto :goto_6b

    #@8f
    .line 486
    .end local v2           #group:Landroid/media/MediaRouter$RouteGroup;
    :cond_8f
    invoke-virtual {v1}, Landroid/media/MediaRouter$RouteCategory;->isGroupable()Z

    #@92
    move-result v6

    #@93
    if-eqz v6, :cond_6e

    #@95
    move-object v2, v4

    #@96
    .line 487
    check-cast v2, Landroid/media/MediaRouter$RouteGroup;

    #@98
    .line 488
    .restart local v2       #group:Landroid/media/MediaRouter$RouteGroup;
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@9b
    move-result v6

    #@9c
    if-gt v6, v9, :cond_b6

    #@9e
    add-int/lit8 v6, p1, -0x1

    #@a0
    invoke-virtual {p0, v6}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    #@a3
    move-result v6

    #@a4
    if-eq v6, v11, :cond_b6

    #@a6
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getCount()I

    #@a9
    move-result v6

    #@aa
    add-int/lit8 v6, v6, -0x1

    #@ac
    if-ge p1, v6, :cond_b8

    #@ae
    add-int/lit8 v6, p1, 0x1

    #@b0
    invoke-virtual {p0, v6}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    #@b3
    move-result v6

    #@b4
    if-ne v6, v11, :cond_b8

    #@b6
    :cond_b6
    move v0, v9

    #@b7
    :goto_b7
    goto :goto_6e

    #@b8
    :cond_b8
    move v0, v7

    #@b9
    goto :goto_b7

    #@ba
    .end local v2           #group:Landroid/media/MediaRouter$RouteGroup;
    :cond_ba
    move v7, v8

    #@bb
    .line 496
    goto :goto_76
.end method

.method finishGrouping()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 559
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@3
    .line 560
    iput-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@5
    .line 561
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->getDialog()Landroid/app/Dialog;

    #@a
    move-result-object v0

    #@b
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    #@f
    .line 562
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    #@12
    .line 563
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->scrollToSelectedItem()V

    #@15
    .line 564
    return-void
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 402
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 407
    int-to-long v0, p1

    #@1
    return-wide v0
.end method

.method public getItemViewType(I)I
    .registers 6
    .parameter "position"

    #@0
    .prologue
    .line 368
    invoke-virtual {p0, p1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItem(I)Ljava/lang/Object;

    #@3
    move-result-object v1

    #@4
    .line 369
    .local v1, item:Ljava/lang/Object;
    instance-of v2, v1, Landroid/media/MediaRouter$RouteCategory;

    #@6
    if-eqz v2, :cond_e

    #@8
    .line 370
    if-nez p1, :cond_c

    #@a
    const/4 v2, 0x0

    #@b
    .line 378
    :goto_b
    return v2

    #@c
    .line 370
    :cond_c
    const/4 v2, 0x1

    #@d
    goto :goto_b

    #@e
    .line 371
    :cond_e
    if-nez v1, :cond_12

    #@10
    .line 372
    const/4 v2, 0x4

    #@11
    goto :goto_b

    #@12
    :cond_12
    move-object v0, v1

    #@13
    .line 374
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    #@15
    .line 375
    .local v0, info:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@18
    move-result-object v2

    #@19
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@1b
    if-ne v2, v3, :cond_1f

    #@1d
    .line 376
    const/4 v2, 0x3

    #@1e
    goto :goto_b

    #@1f
    .line 378
    :cond_1f
    const/4 v2, 0x2

    #@20
    goto :goto_b
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 412
    invoke-virtual {p0, p1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    #@4
    move-result v4

    #@5
    .line 415
    .local v4, viewType:I
    if-nez p2, :cond_8b

    #@7
    .line 416
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@9
    invoke-static {v5}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$300(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/view/LayoutInflater;

    #@c
    move-result-object v5

    #@d
    invoke-static {}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$200()[I

    #@10
    move-result-object v7

    #@11
    aget v7, v7, v4

    #@13
    invoke-virtual {v5, v7, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@16
    move-result-object p2

    #@17
    .line 417
    new-instance v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-direct {v2, v5}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment$1;)V

    #@1d
    .line 418
    .local v2, holder:Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;
    iput p1, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->position:I

    #@1f
    .line 419
    const v5, 0x1020014

    #@22
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@25
    move-result-object v5

    #@26
    check-cast v5, Landroid/widget/TextView;

    #@28
    iput-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    #@2a
    .line 420
    const v5, 0x1020015

    #@2d
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Landroid/widget/TextView;

    #@33
    iput-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    #@35
    .line 421
    const v5, 0x1020006

    #@38
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3b
    move-result-object v5

    #@3c
    check-cast v5, Landroid/widget/ImageView;

    #@3e
    iput-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    #@40
    .line 422
    const v5, 0x102032e

    #@43
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@46
    move-result-object v5

    #@47
    check-cast v5, Landroid/widget/CheckBox;

    #@49
    iput-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->check:Landroid/widget/CheckBox;

    #@4b
    .line 423
    const v5, 0x102032d

    #@4e
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@51
    move-result-object v5

    #@52
    check-cast v5, Landroid/widget/ImageButton;

    #@54
    iput-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    #@56
    .line 425
    iget-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    #@58
    if-eqz v5, :cond_68

    #@5a
    .line 426
    new-instance v5, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    #@5c
    invoke-direct {v5, p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;)V

    #@5f
    iput-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupListener:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    #@61
    .line 427
    iget-object v5, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    #@63
    iget-object v7, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupListener:Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    #@65
    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@68
    .line 430
    :cond_68
    move-object v1, p2

    #@69
    .local v1, fview:Landroid/view/View;
    move-object v3, p3

    #@6a
    .line 431
    check-cast v3, Landroid/widget/ListView;

    #@6c
    .line 432
    .local v3, list:Landroid/widget/ListView;
    move-object v0, v2

    #@6d
    .line 433
    .local v0, fholder:Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;
    new-instance v5, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$1;

    #@6f
    invoke-direct {v5, p0, v3, v1, v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter$1;-><init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;Landroid/widget/ListView;Landroid/view/View;Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;)V

    #@72
    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@75
    .line 438
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    #@78
    .line 444
    .end local v0           #fholder:Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;
    .end local v1           #fview:Landroid/view/View;
    .end local v3           #list:Landroid/widget/ListView;
    :goto_78
    packed-switch v4, :pswitch_data_9e

    #@7b
    .line 455
    :goto_7b
    iget v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@7d
    if-ne p1, v5, :cond_9c

    #@7f
    const/4 v5, 0x1

    #@80
    :goto_80
    invoke-virtual {p2, v5}, Landroid/view/View;->setActivated(Z)V

    #@83
    .line 456
    invoke-virtual {p0, p1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->isEnabled(I)Z

    #@86
    move-result v5

    #@87
    invoke-virtual {p2, v5}, Landroid/view/View;->setEnabled(Z)V

    #@8a
    .line 458
    return-object p2

    #@8b
    .line 440
    .end local v2           #holder:Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;
    :cond_8b
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@8e
    move-result-object v2

    #@8f
    check-cast v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;

    #@91
    .line 441
    .restart local v2       #holder:Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;
    iput p1, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;->position:I

    #@93
    goto :goto_78

    #@94
    .line 447
    :pswitch_94
    invoke-virtual {p0, p1, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->bindItemView(ILcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;)V

    #@97
    goto :goto_7b

    #@98
    .line 451
    :pswitch_98
    invoke-virtual {p0, p1, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->bindHeaderView(ILcom/android/internal/app/MediaRouteChooserDialogFragment$ViewHolder;)V

    #@9b
    goto :goto_7b

    #@9c
    :cond_9c
    move v5, v6

    #@9d
    .line 455
    goto :goto_80

    #@9e
    .line 444
    :pswitch_data_9e
    .packed-switch 0x0
        :pswitch_98
        :pswitch_98
        :pswitch_94
        :pswitch_94
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .registers 2

    #@0
    .prologue
    .line 363
    const/4 v0, 0x5

    #@1
    return v0
.end method

.method public isEnabled(I)Z
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 389
    invoke-virtual {p0, p1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_18

    #@7
    .line 396
    const/4 v0, 0x0

    #@8
    :goto_8
    return v0

    #@9
    .line 391
    :pswitch_9
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    #@11
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    #@14
    move-result v0

    #@15
    goto :goto_8

    #@16
    .line 394
    :pswitch_16
    const/4 v0, 0x1

    #@17
    goto :goto_8

    #@18
    .line 389
    :pswitch_data_18
    .packed-switch 0x2
        :pswitch_9
        :pswitch_16
        :pswitch_16
    .end packed-switch
.end method

.method isGrouping()Z
    .registers 2

    #@0
    .prologue
    .line 555
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 16
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v9, 0x1

    #@1
    .line 508
    invoke-virtual {p0, p3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    #@4
    move-result v4

    #@5
    .line 509
    .local v4, type:I
    if-eq v4, v9, :cond_9

    #@7
    if-nez v4, :cond_a

    #@9
    .line 552
    :cond_9
    :goto_9
    return-void

    #@a
    .line 511
    :cond_a
    const/4 v6, 0x4

    #@b
    if-ne v4, v6, :cond_11

    #@d
    .line 512
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->finishGrouping()V

    #@10
    goto :goto_9

    #@11
    .line 515
    :cond_11
    invoke-virtual {p0, p3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItem(I)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    .line 516
    .local v1, item:Ljava/lang/Object;
    instance-of v6, v1, Landroid/media/MediaRouter$RouteInfo;

    #@17
    if-eqz v6, :cond_9

    #@19
    move-object v3, v1

    #@1a
    .line 521
    check-cast v3, Landroid/media/MediaRouter$RouteInfo;

    #@1c
    .line 522
    .local v3, route:Landroid/media/MediaRouter$RouteInfo;
    const/4 v6, 0x2

    #@1d
    if-ne v4, v6, :cond_32

    #@1f
    .line 523
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@21
    iget-object v6, v6, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@23
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@25
    invoke-static {v7}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I

    #@28
    move-result v7

    #@29
    invoke-virtual {v6, v7, v3}, Landroid/media/MediaRouter;->selectRouteInt(ILandroid/media/MediaRouter$RouteInfo;)V

    #@2c
    .line 524
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2e
    invoke-virtual {v6}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->dismiss()V

    #@31
    goto :goto_9

    #@32
    .line 525
    :cond_32
    const/4 v6, 0x3

    #@33
    if-ne v4, v6, :cond_9

    #@35
    move-object v0, p2

    #@36
    .line 526
    check-cast v0, Landroid/widget/Checkable;

    #@38
    .line 527
    .local v0, c:Landroid/widget/Checkable;
    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    #@3b
    move-result v5

    #@3c
    .line 529
    .local v5, wasChecked:Z
    iput-boolean v9, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mIgnoreUpdates:Z

    #@3e
    .line 530
    invoke-virtual {v3}, Landroid/media/MediaRouter$RouteInfo;->getGroup()Landroid/media/MediaRouter$RouteGroup;

    #@41
    move-result-object v2

    #@42
    .line 531
    .local v2, oldGroup:Landroid/media/MediaRouter$RouteGroup;
    if-nez v5, :cond_79

    #@44
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@46
    if-eq v2, v6, :cond_79

    #@48
    .line 533
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@4a
    iget-object v6, v6, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@4c
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@4e
    invoke-static {v7}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I

    #@51
    move-result v7

    #@52
    invoke-virtual {v6, v7}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@55
    move-result-object v6

    #@56
    if-ne v6, v2, :cond_67

    #@58
    .line 536
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@5a
    iget-object v6, v6, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@5c
    iget-object v7, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@5e
    invoke-static {v7}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I

    #@61
    move-result v7

    #@62
    iget-object v8, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@64
    invoke-virtual {v6, v7, v8}, Landroid/media/MediaRouter;->selectRouteInt(ILandroid/media/MediaRouter$RouteInfo;)V

    #@67
    .line 538
    :cond_67
    invoke-virtual {v2, v3}, Landroid/media/MediaRouter$RouteGroup;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@6a
    .line 539
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@6c
    invoke-virtual {v6, v3}, Landroid/media/MediaRouter$RouteGroup;->addRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@6f
    .line 540
    invoke-interface {v0, v9}, Landroid/widget/Checkable;->setChecked(Z)V

    #@72
    .line 548
    :cond_72
    :goto_72
    const/4 v6, 0x0

    #@73
    iput-boolean v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mIgnoreUpdates:Z

    #@75
    .line 549
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    #@78
    goto :goto_9

    #@79
    .line 541
    :cond_79
    if-eqz v5, :cond_72

    #@7b
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@7d
    invoke-virtual {v6}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@80
    move-result v6

    #@81
    if-le v6, v9, :cond_72

    #@83
    .line 542
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Landroid/media/MediaRouter$RouteGroup;

    #@85
    invoke-virtual {v6, v3}, Landroid/media/MediaRouter$RouteGroup;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@88
    .line 546
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@8a
    iget-object v6, v6, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@8c
    invoke-virtual {v6, v3}, Landroid/media/MediaRouter;->addRouteInt(Landroid/media/MediaRouter$RouteInfo;)V

    #@8f
    goto :goto_72
.end method

.method scrollToEditingGroup()V
    .registers 7

    #@0
    .prologue
    .line 292
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@2
    if-eqz v5, :cond_c

    #@4
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@6
    invoke-static {v5}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    #@9
    move-result-object v5

    #@a
    if-nez v5, :cond_d

    #@c
    .line 309
    :cond_c
    :goto_c
    return-void

    #@d
    .line 294
    :cond_d
    const/4 v4, 0x0

    #@e
    .line 295
    .local v4, pos:I
    const/4 v0, 0x0

    #@f
    .line 296
    .local v0, bound:I
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v3

    #@15
    .line 297
    .local v3, itemCount:I
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    :goto_16
    if-ge v1, v3, :cond_28

    #@18
    .line 298
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    .line 299
    .local v2, item:Ljava/lang/Object;
    if-eqz v2, :cond_25

    #@20
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@22
    if-ne v2, v5, :cond_25

    #@24
    .line 300
    move v0, v1

    #@25
    .line 302
    :cond_25
    if-nez v2, :cond_32

    #@27
    .line 303
    move v4, v1

    #@28
    .line 308
    .end local v2           #item:Ljava/lang/Object;
    :cond_28
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2a
    invoke-static {v5}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5, v4, v0}, Landroid/widget/ListView;->smoothScrollToPosition(II)V

    #@31
    goto :goto_c

    #@32
    .line 297
    .restart local v2       #item:Ljava/lang/Object;
    :cond_32
    add-int/lit8 v1, v1, 0x1

    #@34
    goto :goto_16
.end method

.method scrollToSelectedItem()V
    .registers 3

    #@0
    .prologue
    .line 312
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2
    invoke-static {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_c

    #@8
    iget v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@a
    if-gez v0, :cond_d

    #@c
    .line 315
    :cond_c
    :goto_c
    return-void

    #@d
    .line 314
    :cond_d
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@f
    invoke-static {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    #@12
    move-result-object v0

    #@13
    iget v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@15
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    #@18
    goto :goto_c
.end method

.method update()V
    .registers 9

    #@0
    .prologue
    .line 259
    iget-boolean v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mIgnoreUpdates:Z

    #@2
    if-eqz v5, :cond_5

    #@4
    .line 289
    :cond_4
    :goto_4
    return-void

    #@5
    .line 261
    :cond_5
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 263
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@c
    iget-object v5, v5, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@e
    iget-object v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@10
    invoke-static {v6}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I

    #@13
    move-result v6

    #@14
    invoke-virtual {v5, v6}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@17
    move-result-object v4

    #@18
    .line 264
    .local v4, selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    const/4 v5, -0x1

    #@19
    iput v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@1b
    .line 267
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@1d
    iget-object v5, v5, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v5}, Landroid/media/MediaRouter;->getCategoryCount()I

    #@22
    move-result v1

    #@23
    .line 268
    .local v1, catCount:I
    const/4 v2, 0x0

    #@24
    .local v2, i:I
    :goto_24
    if-ge v2, v1, :cond_50

    #@26
    .line 269
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@28
    iget-object v5, v5, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@2a
    invoke-virtual {v5, v2}, Landroid/media/MediaRouter;->getCategoryAt(I)Landroid/media/MediaRouter$RouteCategory;

    #@2d
    move-result-object v0

    #@2e
    .line 270
    .local v0, cat:Landroid/media/MediaRouter$RouteCategory;
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCatRouteList:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v0, v5}, Landroid/media/MediaRouter$RouteCategory;->getRoutes(Ljava/util/List;)Ljava/util/List;

    #@33
    move-result-object v3

    #@34
    .line 272
    .local v3, routes:Ljava/util/List;,"Ljava/util/List<Landroid/media/MediaRouter$RouteInfo;>;"
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteCategory;->isSystem()Z

    #@37
    move-result v5

    #@38
    if-nez v5, :cond_3f

    #@3a
    .line 273
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3f
    .line 276
    :cond_3f
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Landroid/media/MediaRouter$RouteCategory;

    #@41
    if-ne v0, v5, :cond_4c

    #@43
    .line 277
    invoke-virtual {p0, v3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->addGroupEditingCategoryRoutes(Ljava/util/List;)V

    #@46
    .line 282
    :goto_46
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@49
    .line 268
    add-int/lit8 v2, v2, 0x1

    #@4b
    goto :goto_24

    #@4c
    .line 279
    :cond_4c
    invoke-virtual {p0, v4, v3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->addSelectableRoutes(Landroid/media/MediaRouter$RouteInfo;Ljava/util/List;)V

    #@4f
    goto :goto_46

    #@50
    .line 285
    .end local v0           #cat:Landroid/media/MediaRouter$RouteCategory;
    .end local v3           #routes:Ljava/util/List;,"Ljava/util/List<Landroid/media/MediaRouter$RouteInfo;>;"
    :cond_50
    invoke-virtual {p0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->notifyDataSetChanged()V

    #@53
    .line 286
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@55
    invoke-static {v5}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    #@58
    move-result-object v5

    #@59
    if-eqz v5, :cond_4

    #@5b
    iget v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@5d
    if-ltz v5, :cond_4

    #@5f
    .line 287
    iget-object v5, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@61
    invoke-static {v5}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    #@64
    move-result-object v5

    #@65
    iget v6, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    #@67
    const/4 v7, 0x1

    #@68
    invoke-virtual {v5, v6, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    #@6b
    goto :goto_4
.end method
