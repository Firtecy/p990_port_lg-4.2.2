.class public Lcom/android/internal/app/ActionBarImpl;
.super Landroid/app/ActionBar;
.source "ActionBarImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/ActionBarImpl$TabImpl;,
        Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;
    }
.end annotation


# static fields
.field private static final CONTEXT_DISPLAY_NORMAL:I = 0x0

.field private static final CONTEXT_DISPLAY_SPLIT:I = 0x1

.field private static final INVALID_POSITION:I = -0x1

.field private static final TAG:Ljava/lang/String; = "ActionBarImpl"


# instance fields
.field mActionMode:Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;

.field private mActionView:Lcom/android/internal/widget/ActionBarView;

.field private mActivity:Landroid/app/Activity;

.field private mAnimationMode:Z

.field private mContainerView:Lcom/android/internal/widget/ActionBarContainer;

.field private mContentView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mContextDisplayMode:I

.field private mContextView:Lcom/android/internal/widget/ActionBarContextView;

.field private mCurWindowVisibility:I

.field private mCurrentShowAnim:Landroid/animation/Animator;

.field mDeferredDestroyActionMode:Landroid/view/ActionMode;

.field mDeferredModeDestroyCallback:Landroid/view/ActionMode$Callback;

.field private mDialog:Landroid/app/Dialog;

.field private mDisplayHomeAsUpSet:Z

.field final mHandler:Landroid/os/Handler;

.field private mHasEmbeddedTabs:Z

.field private mHiddenByApp:Z

.field private mHiddenBySystem:Z

.field final mHideListener:Landroid/animation/Animator$AnimatorListener;

.field private mLastMenuVisibility:Z

.field private mMenuVisibilityListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/ActionBar$OnMenuVisibilityListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNowShowing:Z

.field private mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

.field private mSavedTabPosition:I

.field private mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

.field private mShowHideAnimationEnabled:Z

.field final mShowListener:Landroid/animation/Animator$AnimatorListener;

.field private mShowingForMode:Z

.field private mSplitView:Lcom/android/internal/widget/ActionBarContainer;

.field private mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

.field mTabSelector:Ljava/lang/Runnable;

.field private mTabs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/app/ActionBarImpl$TabImpl;",
            ">;"
        }
    .end annotation
.end field

.field private mThemedContext:Landroid/content/Context;

.field private mTopVisibilityView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 6
    .parameter "activity"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 157
    invoke-direct {p0}, Landroid/app/ActionBar;-><init>()V

    #@4
    .line 88
    new-instance v2, Ljava/util/ArrayList;

    #@6
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@b
    .line 91
    const/4 v2, -0x1

    #@c
    iput v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@e
    .line 100
    new-instance v2, Ljava/util/ArrayList;

    #@10
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@13
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mMenuVisibilityListeners:Ljava/util/ArrayList;

    #@15
    .line 111
    new-instance v2, Landroid/os/Handler;

    #@17
    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    #@1a
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mHandler:Landroid/os/Handler;

    #@1c
    .line 114
    iput v3, p0, Lcom/android/internal/app/ActionBarImpl;->mCurWindowVisibility:I

    #@1e
    .line 120
    const/4 v2, 0x1

    #@1f
    iput-boolean v2, p0, Lcom/android/internal/app/ActionBarImpl;->mNowShowing:Z

    #@21
    .line 126
    iput-boolean v3, p0, Lcom/android/internal/app/ActionBarImpl;->mAnimationMode:Z

    #@23
    .line 129
    new-instance v2, Lcom/android/internal/app/ActionBarImpl$1;

    #@25
    invoke-direct {v2, p0}, Lcom/android/internal/app/ActionBarImpl$1;-><init>(Lcom/android/internal/app/ActionBarImpl;)V

    #@28
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mHideListener:Landroid/animation/Animator$AnimatorListener;

    #@2a
    .line 149
    new-instance v2, Lcom/android/internal/app/ActionBarImpl$2;

    #@2c
    invoke-direct {v2, p0}, Lcom/android/internal/app/ActionBarImpl$2;-><init>(Lcom/android/internal/app/ActionBarImpl;)V

    #@2f
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mShowListener:Landroid/animation/Animator$AnimatorListener;

    #@31
    .line 158
    iput-object p1, p0, Lcom/android/internal/app/ActionBarImpl;->mActivity:Landroid/app/Activity;

    #@33
    .line 159
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@36
    move-result-object v1

    #@37
    .line 160
    .local v1, window:Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@3a
    move-result-object v0

    #@3b
    .line 161
    .local v0, decor:Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->init(Landroid/view/View;)V

    #@3e
    .line 162
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActivity:Landroid/app/Activity;

    #@40
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@43
    move-result-object v2

    #@44
    const/16 v3, 0x9

    #@46
    invoke-virtual {v2, v3}, Landroid/view/Window;->hasFeature(I)Z

    #@49
    move-result v2

    #@4a
    if-nez v2, :cond_55

    #@4c
    .line 163
    const v2, 0x1020002

    #@4f
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@52
    move-result-object v2

    #@53
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@55
    .line 165
    :cond_55
    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;)V
    .registers 4
    .parameter "dialog"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 167
    invoke-direct {p0}, Landroid/app/ActionBar;-><init>()V

    #@4
    .line 88
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@b
    .line 91
    const/4 v0, -0x1

    #@c
    iput v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@e
    .line 100
    new-instance v0, Ljava/util/ArrayList;

    #@10
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@13
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mMenuVisibilityListeners:Ljava/util/ArrayList;

    #@15
    .line 111
    new-instance v0, Landroid/os/Handler;

    #@17
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHandler:Landroid/os/Handler;

    #@1c
    .line 114
    iput v1, p0, Lcom/android/internal/app/ActionBarImpl;->mCurWindowVisibility:I

    #@1e
    .line 120
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mNowShowing:Z

    #@21
    .line 126
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mAnimationMode:Z

    #@23
    .line 129
    new-instance v0, Lcom/android/internal/app/ActionBarImpl$1;

    #@25
    invoke-direct {v0, p0}, Lcom/android/internal/app/ActionBarImpl$1;-><init>(Lcom/android/internal/app/ActionBarImpl;)V

    #@28
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHideListener:Landroid/animation/Animator$AnimatorListener;

    #@2a
    .line 149
    new-instance v0, Lcom/android/internal/app/ActionBarImpl$2;

    #@2c
    invoke-direct {v0, p0}, Lcom/android/internal/app/ActionBarImpl$2;-><init>(Lcom/android/internal/app/ActionBarImpl;)V

    #@2f
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mShowListener:Landroid/animation/Animator$AnimatorListener;

    #@31
    .line 168
    iput-object p1, p0, Lcom/android/internal/app/ActionBarImpl;->mDialog:Landroid/app/Dialog;

    #@33
    .line 169
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@3a
    move-result-object v0

    #@3b
    invoke-direct {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->init(Landroid/view/View;)V

    #@3e
    .line 170
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/app/ActionBarImpl;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/app/ActionBarImpl;)Landroid/view/ViewGroup;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/app/ActionBarImpl;)Lcom/android/internal/widget/ActionBarContextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/app/ActionBarImpl;)Lcom/android/internal/widget/ActionBarView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/app/ActionBarImpl;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/app/ActionBarImpl;)Lcom/android/internal/widget/ScrollingTabContainerView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/app/ActionBarImpl;)Lcom/android/internal/widget/ActionBarContainer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/app/ActionBarImpl;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContextDisplayMode:I

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/internal/app/ActionBarImpl;)Lcom/android/internal/widget/ActionBarContainer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/app/ActionBarImpl;Landroid/animation/Animator;)Landroid/animation/Animator;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-object p1, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Lcom/android/internal/app/ActionBarImpl;)Lcom/android/internal/widget/ActionBarOverlayLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/app/ActionBarImpl;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenByApp:Z

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/android/internal/app/ActionBarImpl;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenBySystem:Z

    #@2
    return v0
.end method

.method static synthetic access$900(ZZZ)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 71
    invoke-static {p0, p1, p2}, Lcom/android/internal/app/ActionBarImpl;->checkShowingFlags(ZZZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static checkShowingFlags(ZZZ)Z
    .registers 4
    .parameter "hiddenByApp"
    .parameter "hiddenBySystem"
    .parameter "showingForMode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 710
    if-eqz p2, :cond_4

    #@3
    .line 715
    :cond_3
    :goto_3
    return v0

    #@4
    .line 712
    :cond_4
    if-nez p0, :cond_8

    #@6
    if-eqz p1, :cond_3

    #@8
    .line 713
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_3
.end method

.method private cleanupTabs()V
    .registers 2

    #@0
    .prologue
    .line 441
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 442
    const/4 v0, 0x0

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->selectTab(Landroid/app/ActionBar$Tab;)V

    #@8
    .line 444
    :cond_8
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@d
    .line 445
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 446
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeAllTabs()V

    #@16
    .line 448
    :cond_16
    const/4 v0, -0x1

    #@17
    iput v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@19
    .line 449
    return-void
.end method

.method private configureTab(Landroid/app/ActionBar$Tab;I)V
    .registers 9
    .parameter "tab"
    .parameter "position"

    #@0
    .prologue
    .line 536
    move-object v3, p1

    #@1
    check-cast v3, Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@3
    .line 537
    .local v3, tabi:Lcom/android/internal/app/ActionBarImpl$TabImpl;
    invoke-virtual {v3}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    #@6
    move-result-object v0

    #@7
    .line 539
    .local v0, callback:Landroid/app/ActionBar$TabListener;
    if-nez v0, :cond_11

    #@9
    .line 540
    new-instance v4, Ljava/lang/IllegalStateException;

    #@b
    const-string v5, "Action Bar Tab must have a Callback"

    #@d
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v4

    #@11
    .line 543
    :cond_11
    invoke-virtual {v3, p2}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->setPosition(I)V

    #@14
    .line 544
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v4, p2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@19
    .line 546
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v1

    #@1f
    .line 547
    .local v1, count:I
    add-int/lit8 v2, p2, 0x1

    #@21
    .local v2, i:I
    :goto_21
    if-ge v2, v1, :cond_31

    #@23
    .line 548
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    check-cast v4, Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@2b
    invoke-virtual {v4, v2}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->setPosition(I)V

    #@2e
    .line 547
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_21

    #@31
    .line 550
    :cond_31
    return-void
.end method

.method private ensureTabsExist()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 299
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 329
    :goto_5
    return-void

    #@6
    .line 307
    :cond_6
    iget-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mAnimationMode:Z

    #@8
    if-eqz v1, :cond_20

    #@a
    .line 308
    new-instance v0, Lcom/android/internal/widget/LGScrollingTabContainerView;

    #@c
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LGScrollingTabContainerView;-><init>(Landroid/content/Context;)V

    #@11
    .line 314
    .local v0, tabScroller:Lcom/android/internal/widget/ScrollingTabContainerView;
    :goto_11
    iget-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mHasEmbeddedTabs:Z

    #@13
    if-eqz v1, :cond_28

    #@15
    .line 315
    invoke-virtual {v0, v3}, Lcom/android/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    #@18
    .line 316
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@1a
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ActionBarView;->setEmbeddedTabView(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@1d
    .line 328
    :goto_1d
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@1f
    goto :goto_5

    #@20
    .line 310
    .end local v0           #tabScroller:Lcom/android/internal/widget/ScrollingTabContainerView;
    :cond_20
    new-instance v0, Lcom/android/internal/widget/ScrollingTabContainerView;

    #@22
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@24
    invoke-direct {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;-><init>(Landroid/content/Context;)V

    #@27
    .restart local v0       #tabScroller:Lcom/android/internal/widget/ScrollingTabContainerView;
    goto :goto_11

    #@28
    .line 318
    :cond_28
    invoke-virtual {p0}, Lcom/android/internal/app/ActionBarImpl;->getNavigationMode()I

    #@2b
    move-result v1

    #@2c
    const/4 v2, 0x2

    #@2d
    if-ne v1, v2, :cond_41

    #@2f
    .line 319
    invoke-virtual {v0, v3}, Lcom/android/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    #@32
    .line 320
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@34
    if-eqz v1, :cond_3b

    #@36
    .line 321
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@38
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    #@3b
    .line 326
    :cond_3b
    :goto_3b
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@3d
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ActionBarContainer;->setTabContainer(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@40
    goto :goto_1d

    #@41
    .line 324
    :cond_41
    const/16 v1, 0x8

    #@43
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    #@46
    goto :goto_3b
.end method

.method private hideForActionMode()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 692
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mShowingForMode:Z

    #@3
    if-eqz v0, :cond_13

    #@5
    .line 693
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mShowingForMode:Z

    #@7
    .line 694
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 695
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    #@10
    .line 697
    :cond_10
    invoke-direct {p0, v1}, Lcom/android/internal/app/ActionBarImpl;->updateVisibility(Z)V

    #@13
    .line 699
    :cond_13
    return-void
.end method

.method private init(Landroid/view/View;)V
    .registers 11
    .parameter "decor"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@5
    move-result-object v5

    #@6
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@8
    .line 174
    const v5, 0x1020370

    #@b
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@e
    move-result-object v5

    #@f
    check-cast v5, Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@11
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@13
    .line 176
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@15
    if-eqz v5, :cond_1c

    #@17
    .line 177
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@19
    invoke-virtual {v5, p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->setActionBar(Lcom/android/internal/app/ActionBarImpl;)V

    #@1c
    .line 179
    :cond_1c
    const v5, 0x102036d

    #@1f
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@22
    move-result-object v5

    #@23
    check-cast v5, Lcom/android/internal/widget/ActionBarView;

    #@25
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@27
    .line 180
    const v5, 0x102036e

    #@2a
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2d
    move-result-object v5

    #@2e
    check-cast v5, Lcom/android/internal/widget/ActionBarContextView;

    #@30
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@32
    .line 182
    const v5, 0x102036c

    #@35
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@38
    move-result-object v5

    #@39
    check-cast v5, Lcom/android/internal/widget/ActionBarContainer;

    #@3b
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@3d
    .line 184
    const v5, 0x1020371

    #@40
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@43
    move-result-object v5

    #@44
    check-cast v5, Landroid/view/ViewGroup;

    #@46
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@48
    .line 186
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@4a
    if-nez v5, :cond_50

    #@4c
    .line 187
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@4e
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@50
    .line 189
    :cond_50
    const v5, 0x102036f

    #@53
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@56
    move-result-object v5

    #@57
    check-cast v5, Lcom/android/internal/widget/ActionBarContainer;

    #@59
    iput-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@5b
    .line 192
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@5d
    if-eqz v5, :cond_67

    #@5f
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@61
    if-eqz v5, :cond_67

    #@63
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@65
    if-nez v5, :cond_8f

    #@67
    .line 193
    :cond_67
    new-instance v5, Ljava/lang/IllegalStateException;

    #@69
    new-instance v6, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@71
    move-result-object v7

    #@72
    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    const-string v7, " can only be used "

    #@7c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    const-string/jumbo v7, "with a compatible window decor layout"

    #@83
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v6

    #@87
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v6

    #@8b
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@8e
    throw v5

    #@8f
    .line 197
    :cond_8f
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@91
    iget-object v8, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@93
    invoke-virtual {v5, v8}, Lcom/android/internal/widget/ActionBarView;->setContextView(Lcom/android/internal/widget/ActionBarContextView;)V

    #@96
    .line 198
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@98
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->isSplitActionBar()Z

    #@9b
    move-result v5

    #@9c
    if-eqz v5, :cond_105

    #@9e
    move v5, v6

    #@9f
    :goto_9f
    iput v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContextDisplayMode:I

    #@a1
    .line 202
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@a3
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->getDisplayOptions()I

    #@a6
    move-result v1

    #@a7
    .line 203
    .local v1, current:I
    and-int/lit8 v5, v1, 0x4

    #@a9
    if-eqz v5, :cond_107

    #@ab
    move v2, v6

    #@ac
    .line 204
    .local v2, homeAsUp:Z
    :goto_ac
    if-eqz v2, :cond_b0

    #@ae
    .line 205
    iput-boolean v6, p0, Lcom/android/internal/app/ActionBarImpl;->mDisplayHomeAsUpSet:Z

    #@b0
    .line 208
    :cond_b0
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@b2
    invoke-static {v5}, Lcom/android/internal/view/ActionBarPolicy;->get(Landroid/content/Context;)Lcom/android/internal/view/ActionBarPolicy;

    #@b5
    move-result-object v0

    #@b6
    .line 209
    .local v0, abp:Lcom/android/internal/view/ActionBarPolicy;
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->enableHomeButtonByDefault()Z

    #@b9
    move-result v5

    #@ba
    if-nez v5, :cond_be

    #@bc
    if-eqz v2, :cond_109

    #@be
    :cond_be
    :goto_be
    invoke-virtual {p0, v6}, Lcom/android/internal/app/ActionBarImpl;->setHomeButtonEnabled(Z)V

    #@c1
    .line 210
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->setHasEmbeddedTabs()V

    #@c4
    .line 214
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@c6
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c9
    move-result-object v4

    #@ca
    .line 215
    .local v4, rcs:Landroid/content/res/Resources;
    if-eqz v4, :cond_104

    #@cc
    .line 216
    const/4 v3, 0x0

    #@cd
    .line 217
    .local v3, id:I
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mActivity:Landroid/app/Activity;

    #@cf
    if-eqz v5, :cond_104

    #@d1
    .line 218
    new-instance v5, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    iget-object v6, p0, Lcom/android/internal/app/ActionBarImpl;->mActivity:Landroid/app/Activity;

    #@d8
    invoke-virtual {v6}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    #@db
    move-result-object v6

    #@dc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v5

    #@e0
    const-string v6, "_action_bar_animation_tabs"

    #@e2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v5

    #@e6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v5

    #@ea
    const-string v6, "bool"

    #@ec
    iget-object v8, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@ee
    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@f1
    move-result-object v8

    #@f2
    invoke-virtual {v4, v5, v6, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    move-result v3

    #@f6
    .line 219
    if-eqz v3, :cond_10b

    #@f8
    .line 220
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@fa
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@fd
    move-result-object v5

    #@fe
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@101
    move-result v5

    #@102
    iput-boolean v5, p0, Lcom/android/internal/app/ActionBarImpl;->mAnimationMode:Z

    #@104
    .line 232
    .end local v3           #id:I
    :cond_104
    :goto_104
    return-void

    #@105
    .end local v0           #abp:Lcom/android/internal/view/ActionBarPolicy;
    .end local v1           #current:I
    .end local v2           #homeAsUp:Z
    .end local v4           #rcs:Landroid/content/res/Resources;
    :cond_105
    move v5, v7

    #@106
    .line 198
    goto :goto_9f

    #@107
    .restart local v1       #current:I
    :cond_107
    move v2, v7

    #@108
    .line 203
    goto :goto_ac

    #@109
    .restart local v0       #abp:Lcom/android/internal/view/ActionBarPolicy;
    .restart local v2       #homeAsUp:Z
    :cond_109
    move v6, v7

    #@10a
    .line 209
    goto :goto_be

    #@10b
    .line 222
    .restart local v3       #id:I
    .restart local v4       #rcs:Landroid/content/res/Resources;
    :cond_10b
    const-string v5, "action_bar_animation_tabs"

    #@10d
    const-string v6, "bool"

    #@10f
    iget-object v8, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@111
    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@114
    move-result-object v8

    #@115
    invoke-virtual {v4, v5, v6, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@118
    move-result v3

    #@119
    .line 223
    if-eqz v3, :cond_128

    #@11b
    .line 224
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@11d
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@120
    move-result-object v5

    #@121
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@124
    move-result v5

    #@125
    iput-boolean v5, p0, Lcom/android/internal/app/ActionBarImpl;->mAnimationMode:Z

    #@127
    goto :goto_104

    #@128
    .line 226
    :cond_128
    iput-boolean v7, p0, Lcom/android/internal/app/ActionBarImpl;->mAnimationMode:Z

    #@12a
    goto :goto_104
.end method

.method private setHasEmbeddedTabs()V
    .registers 6

    #@0
    .prologue
    .line 244
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v1

    #@6
    .line 246
    .local v1, rcs:Landroid/content/res/Resources;
    if-eqz v1, :cond_51

    #@8
    .line 247
    const/4 v0, 0x0

    #@9
    .line 248
    .local v0, id:I
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActivity:Landroid/app/Activity;

    #@b
    if-eqz v2, :cond_32

    #@d
    .line 249
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mActivity:Landroid/app/Activity;

    #@14
    invoke-virtual {v3}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, "_action_bar_embed_tabs"

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    const-string v3, "bool"

    #@28
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@31
    move-result v0

    #@32
    .line 254
    :cond_32
    if-nez v0, :cond_42

    #@34
    .line 255
    const-string v2, "action_bar_embed_tabs"

    #@36
    const-string v3, "bool"

    #@38
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@3a
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@41
    move-result v0

    #@42
    .line 260
    :cond_42
    if-nez v0, :cond_52

    #@44
    .line 261
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@46
    invoke-static {v2}, Lcom/android/internal/view/ActionBarPolicy;->get(Landroid/content/Context;)Lcom/android/internal/view/ActionBarPolicy;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Lcom/android/internal/view/ActionBarPolicy;->hasEmbeddedTabs()Z

    #@4d
    move-result v2

    #@4e
    invoke-direct {p0, v2}, Lcom/android/internal/app/ActionBarImpl;->setHasEmbeddedTabs(Z)V

    #@51
    .line 267
    .end local v0           #id:I
    :cond_51
    :goto_51
    return-void

    #@52
    .line 264
    .restart local v0       #id:I
    :cond_52
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@54
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@5b
    move-result v2

    #@5c
    invoke-direct {p0, v2}, Lcom/android/internal/app/ActionBarImpl;->setHasEmbeddedTabs(Z)V

    #@5f
    goto :goto_51
.end method

.method private setHasEmbeddedTabs(Z)V
    .registers 7
    .parameter "hasEmbeddedTabs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 271
    iput-boolean p1, p0, Lcom/android/internal/app/ActionBarImpl;->mHasEmbeddedTabs:Z

    #@5
    .line 273
    iget-boolean v3, p0, Lcom/android/internal/app/ActionBarImpl;->mHasEmbeddedTabs:Z

    #@7
    if-nez v3, :cond_3d

    #@9
    .line 274
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@b
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/ActionBarView;->setEmbeddedTabView(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@e
    .line 275
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@10
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@12
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/ActionBarContainer;->setTabContainer(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@15
    .line 280
    :goto_15
    invoke-virtual {p0}, Lcom/android/internal/app/ActionBarImpl;->getNavigationMode()I

    #@18
    move-result v3

    #@19
    const/4 v4, 0x2

    #@1a
    if-ne v3, v4, :cond_4a

    #@1c
    move v0, v1

    #@1d
    .line 281
    .local v0, isInTabMode:Z
    :goto_1d
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@1f
    if-eqz v3, :cond_31

    #@21
    .line 282
    if-eqz v0, :cond_4c

    #@23
    .line 283
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@25
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    #@28
    .line 284
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@2a
    if-eqz v3, :cond_31

    #@2c
    .line 285
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@2e
    invoke-virtual {v3}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    #@31
    .line 291
    :cond_31
    :goto_31
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@33
    iget-boolean v4, p0, Lcom/android/internal/app/ActionBarImpl;->mHasEmbeddedTabs:Z

    #@35
    if-nez v4, :cond_54

    #@37
    if-eqz v0, :cond_54

    #@39
    :goto_39
    invoke-virtual {v3, v1}, Lcom/android/internal/widget/ActionBarView;->setCollapsable(Z)V

    #@3c
    .line 292
    return-void

    #@3d
    .line 277
    .end local v0           #isInTabMode:Z
    :cond_3d
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@3f
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/ActionBarContainer;->setTabContainer(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@42
    .line 278
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@44
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@46
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/ActionBarView;->setEmbeddedTabView(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@49
    goto :goto_15

    #@4a
    :cond_4a
    move v0, v2

    #@4b
    .line 280
    goto :goto_1d

    #@4c
    .line 288
    .restart local v0       #isInTabMode:Z
    :cond_4c
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@4e
    const/16 v4, 0x8

    #@50
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    #@53
    goto :goto_31

    #@54
    :cond_54
    move v1, v2

    #@55
    .line 291
    goto :goto_39
.end method

.method private showForActionMode()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 667
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mShowingForMode:Z

    #@3
    if-nez v0, :cond_14

    #@5
    .line 668
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mShowingForMode:Z

    #@7
    .line 669
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 670
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    #@10
    .line 672
    :cond_10
    const/4 v0, 0x0

    #@11
    invoke-direct {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->updateVisibility(Z)V

    #@14
    .line 674
    :cond_14
    return-void
.end method

.method private updateVisibility(Z)V
    .registers 6
    .parameter "fromSystem"

    #@0
    .prologue
    .line 721
    iget-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenByApp:Z

    #@2
    iget-boolean v2, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenBySystem:Z

    #@4
    iget-boolean v3, p0, Lcom/android/internal/app/ActionBarImpl;->mShowingForMode:Z

    #@6
    invoke-static {v1, v2, v3}, Lcom/android/internal/app/ActionBarImpl;->checkShowingFlags(ZZZ)Z

    #@9
    move-result v0

    #@a
    .line 724
    .local v0, shown:Z
    if-eqz v0, :cond_17

    #@c
    .line 725
    iget-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mNowShowing:Z

    #@e
    if-nez v1, :cond_16

    #@10
    .line 726
    const/4 v1, 0x1

    #@11
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mNowShowing:Z

    #@13
    .line 727
    invoke-virtual {p0, p1}, Lcom/android/internal/app/ActionBarImpl;->doShow(Z)V

    #@16
    .line 735
    :cond_16
    :goto_16
    return-void

    #@17
    .line 730
    :cond_17
    iget-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mNowShowing:Z

    #@19
    if-eqz v1, :cond_16

    #@1b
    .line 731
    const/4 v1, 0x0

    #@1c
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mNowShowing:Z

    #@1e
    .line 732
    invoke-virtual {p0, p1}, Lcom/android/internal/app/ActionBarImpl;->doHide(Z)V

    #@21
    goto :goto_16
.end method


# virtual methods
.method public addOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mMenuVisibilityListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 359
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;)V
    .registers 3
    .parameter "tab"

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/app/ActionBarImpl;->addTab(Landroid/app/ActionBar$Tab;Z)V

    #@9
    .line 555
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;I)V
    .registers 4
    .parameter "tab"
    .parameter "position"

    #@0
    .prologue
    .line 559
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/internal/app/ActionBarImpl;->addTab(Landroid/app/ActionBar$Tab;IZ)V

    #@9
    .line 560
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;IZ)V
    .registers 5
    .parameter "tab"
    .parameter "position"
    .parameter "setSelected"

    #@0
    .prologue
    .line 574
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->ensureTabsExist()V

    #@3
    .line 575
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/widget/ScrollingTabContainerView;->addTab(Landroid/app/ActionBar$Tab;IZ)V

    #@8
    .line 576
    invoke-direct {p0, p1, p2}, Lcom/android/internal/app/ActionBarImpl;->configureTab(Landroid/app/ActionBar$Tab;I)V

    #@b
    .line 577
    if-eqz p3, :cond_10

    #@d
    .line 578
    invoke-virtual {p0, p1}, Lcom/android/internal/app/ActionBarImpl;->selectTab(Landroid/app/ActionBar$Tab;)V

    #@10
    .line 580
    :cond_10
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;Z)V
    .registers 4
    .parameter "tab"
    .parameter "setSelected"

    #@0
    .prologue
    .line 564
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->ensureTabsExist()V

    #@3
    .line 565
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@5
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/widget/ScrollingTabContainerView;->addTab(Landroid/app/ActionBar$Tab;Z)V

    #@8
    .line 566
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    invoke-direct {p0, p1, v0}, Lcom/android/internal/app/ActionBarImpl;->configureTab(Landroid/app/ActionBar$Tab;I)V

    #@11
    .line 567
    if-eqz p2, :cond_16

    #@13
    .line 568
    invoke-virtual {p0, p1}, Lcom/android/internal/app/ActionBarImpl;->selectTab(Landroid/app/ActionBar$Tab;)V

    #@16
    .line 570
    :cond_16
    return-void
.end method

.method animateToMode(Z)V
    .registers 6
    .parameter "toActionMode"

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    const/4 v2, 0x0

    #@3
    .line 843
    if-eqz p1, :cond_34

    #@5
    .line 844
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->showForActionMode()V

    #@8
    .line 849
    :goto_8
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@a
    if-eqz p1, :cond_38

    #@c
    move v0, v1

    #@d
    :goto_d
    invoke-virtual {v3, v0}, Lcom/android/internal/widget/ActionBarView;->animateToVisibility(I)V

    #@10
    .line 850
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@12
    if-eqz p1, :cond_3a

    #@14
    move v0, v2

    #@15
    :goto_15
    invoke-virtual {v3, v0}, Lcom/android/internal/widget/ActionBarContextView;->animateToVisibility(I)V

    #@18
    .line 851
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@1a
    if-eqz v0, :cond_33

    #@1c
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->hasEmbeddedTabs()Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_33

    #@24
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@26
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->isCollapsed()Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_33

    #@2c
    .line 852
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@2e
    if-eqz p1, :cond_3c

    #@30
    :goto_30
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->animateToVisibility(I)V

    #@33
    .line 854
    :cond_33
    return-void

    #@34
    .line 846
    :cond_34
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->hideForActionMode()V

    #@37
    goto :goto_8

    #@38
    :cond_38
    move v0, v2

    #@39
    .line 849
    goto :goto_d

    #@3a
    :cond_3a
    move v0, v1

    #@3b
    .line 850
    goto :goto_15

    #@3c
    :cond_3c
    move v1, v2

    #@3d
    .line 852
    goto :goto_30
.end method

.method completeDeferredDestroyActionMode()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 332
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mDeferredModeDestroyCallback:Landroid/view/ActionMode$Callback;

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 333
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mDeferredModeDestroyCallback:Landroid/view/ActionMode$Callback;

    #@7
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mDeferredDestroyActionMode:Landroid/view/ActionMode;

    #@9
    invoke-interface {v0, v1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    #@c
    .line 334
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mDeferredDestroyActionMode:Landroid/view/ActionMode;

    #@e
    .line 335
    iput-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mDeferredModeDestroyCallback:Landroid/view/ActionMode$Callback;

    #@10
    .line 337
    :cond_10
    return-void
.end method

.method public dispatchMenuVisibilityChanged(Z)V
    .registers 5
    .parameter "isVisible"

    #@0
    .prologue
    .line 366
    iget-boolean v2, p0, Lcom/android/internal/app/ActionBarImpl;->mLastMenuVisibility:Z

    #@2
    if-ne p1, v2, :cond_5

    #@4
    .line 375
    :cond_4
    return-void

    #@5
    .line 369
    :cond_5
    iput-boolean p1, p0, Lcom/android/internal/app/ActionBarImpl;->mLastMenuVisibility:Z

    #@7
    .line 371
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mMenuVisibilityListeners:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v0

    #@d
    .line 372
    .local v0, count:I
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v0, :cond_4

    #@10
    .line 373
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mMenuVisibilityListeners:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/app/ActionBar$OnMenuVisibilityListener;

    #@18
    invoke-interface {v2, p1}, Landroid/app/ActionBar$OnMenuVisibilityListener;->onMenuVisibilityChanged(Z)V

    #@1b
    .line 372
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_e
.end method

.method public doHide(Z)V
    .registers 13
    .parameter "fromSystem"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/high16 v10, 0x3f80

    #@3
    const/4 v9, 0x0

    #@4
    const/4 v8, 0x1

    #@5
    .line 797
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@7
    if-eqz v4, :cond_e

    #@9
    .line 798
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@b
    invoke-virtual {v4}, Landroid/animation/Animator;->end()V

    #@e
    .line 801
    :cond_e
    iget v4, p0, Lcom/android/internal/app/ActionBarImpl;->mCurWindowVisibility:I

    #@10
    if-nez v4, :cond_ab

    #@12
    iget-boolean v4, p0, Lcom/android/internal/app/ActionBarImpl;->mShowHideAnimationEnabled:Z

    #@14
    if-nez v4, :cond_18

    #@16
    if-eqz p1, :cond_ab

    #@18
    .line 803
    :cond_18
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@1a
    invoke-virtual {v4, v10}, Landroid/view/ViewGroup;->setAlpha(F)V

    #@1d
    .line 804
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@1f
    invoke-virtual {v4, v8}, Lcom/android/internal/widget/ActionBarContainer;->setTransitioning(Z)V

    #@22
    .line 805
    new-instance v0, Landroid/animation/AnimatorSet;

    #@24
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    #@27
    .line 806
    .local v0, anim:Landroid/animation/AnimatorSet;
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@29
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    #@2c
    move-result v4

    #@2d
    neg-int v4, v4

    #@2e
    int-to-float v2, v4

    #@2f
    .line 807
    .local v2, endingY:F
    if-eqz p1, :cond_3f

    #@31
    .line 808
    new-array v3, v7, [I

    #@33
    fill-array-data v3, :array_b2

    #@36
    .line 809
    .local v3, topLeft:[I
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@38
    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    #@3b
    .line 810
    aget v4, v3, v8

    #@3d
    int-to-float v4, v4

    #@3e
    sub-float/2addr v2, v4

    #@3f
    .line 812
    .end local v3           #topLeft:[I
    :cond_3f
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@41
    const-string/jumbo v5, "translationY"

    #@44
    new-array v6, v8, [F

    #@46
    aput v2, v6, v9

    #@48
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@4f
    move-result-object v1

    #@50
    .line 814
    .local v1, b:Landroid/animation/AnimatorSet$Builder;
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@52
    if-eqz v4, :cond_67

    #@54
    .line 815
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@56
    const-string/jumbo v5, "translationY"

    #@59
    new-array v6, v7, [F

    #@5b
    const/4 v7, 0x0

    #@5c
    aput v7, v6, v9

    #@5e
    aput v2, v6, v8

    #@60
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@67
    .line 818
    :cond_67
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@69
    if-eqz v4, :cond_8f

    #@6b
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@6d
    invoke-virtual {v4}, Lcom/android/internal/widget/ActionBarContainer;->getVisibility()I

    #@70
    move-result v4

    #@71
    if-nez v4, :cond_8f

    #@73
    .line 819
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@75
    invoke-virtual {v4, v10}, Lcom/android/internal/widget/ActionBarContainer;->setAlpha(F)V

    #@78
    .line 820
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@7a
    const-string/jumbo v5, "translationY"

    #@7d
    new-array v6, v8, [F

    #@7f
    iget-object v7, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@81
    invoke-virtual {v7}, Lcom/android/internal/widget/ActionBarContainer;->getHeight()I

    #@84
    move-result v7

    #@85
    int-to-float v7, v7

    #@86
    aput v7, v6, v9

    #@88
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@8b
    move-result-object v4

    #@8c
    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@8f
    .line 823
    :cond_8f
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@91
    const v5, 0x10c0002

    #@94
    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    #@97
    move-result-object v4

    #@98
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@9b
    .line 825
    const-wide/16 v4, 0xfa

    #@9d
    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@a0
    .line 826
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mHideListener:Landroid/animation/Animator$AnimatorListener;

    #@a2
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@a5
    .line 827
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@a7
    .line 828
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    #@aa
    .line 832
    .end local v0           #anim:Landroid/animation/AnimatorSet;
    .end local v1           #b:Landroid/animation/AnimatorSet$Builder;
    .end local v2           #endingY:F
    :goto_aa
    return-void

    #@ab
    .line 830
    :cond_ab
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mHideListener:Landroid/animation/Animator$AnimatorListener;

    #@ad
    const/4 v5, 0x0

    #@ae
    invoke-interface {v4, v5}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@b1
    goto :goto_aa

    #@b2
    .line 808
    :array_b2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public doShow(Z)V
    .registers 13
    .parameter "fromSystem"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/high16 v5, 0x3f80

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v7, 0x0

    #@6
    .line 738
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@8
    if-eqz v4, :cond_f

    #@a
    .line 739
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@c
    invoke-virtual {v4}, Landroid/animation/Animator;->end()V

    #@f
    .line 741
    :cond_f
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@11
    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@14
    .line 743
    iget v4, p0, Lcom/android/internal/app/ActionBarImpl;->mCurWindowVisibility:I

    #@16
    if-nez v4, :cond_ba

    #@18
    iget-boolean v4, p0, Lcom/android/internal/app/ActionBarImpl;->mShowHideAnimationEnabled:Z

    #@1a
    if-nez v4, :cond_1e

    #@1c
    if-eqz p1, :cond_ba

    #@1e
    .line 745
    :cond_1e
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@20
    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setTranslationY(F)V

    #@23
    .line 746
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@25
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    #@28
    move-result v4

    #@29
    neg-int v4, v4

    #@2a
    int-to-float v2, v4

    #@2b
    .line 747
    .local v2, startingY:F
    if-eqz p1, :cond_3b

    #@2d
    .line 748
    new-array v3, v10, [I

    #@2f
    fill-array-data v3, :array_ec

    #@32
    .line 749
    .local v3, topLeft:[I
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@34
    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    #@37
    .line 750
    aget v4, v3, v9

    #@39
    int-to-float v4, v4

    #@3a
    sub-float/2addr v2, v4

    #@3b
    .line 752
    .end local v3           #topLeft:[I
    :cond_3b
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@3d
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setTranslationY(F)V

    #@40
    .line 753
    new-instance v0, Landroid/animation/AnimatorSet;

    #@42
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    #@45
    .line 754
    .local v0, anim:Landroid/animation/AnimatorSet;
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@47
    const-string/jumbo v5, "translationY"

    #@4a
    new-array v6, v9, [F

    #@4c
    aput v7, v6, v8

    #@4e
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@55
    move-result-object v1

    #@56
    .line 756
    .local v1, b:Landroid/animation/AnimatorSet$Builder;
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@58
    if-eqz v4, :cond_6c

    #@5a
    .line 757
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@5c
    const-string/jumbo v5, "translationY"

    #@5f
    new-array v6, v10, [F

    #@61
    aput v2, v6, v8

    #@63
    aput v7, v6, v9

    #@65
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@6c
    .line 760
    :cond_6c
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@6e
    if-eqz v4, :cond_95

    #@70
    iget v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContextDisplayMode:I

    #@72
    if-ne v4, v9, :cond_95

    #@74
    .line 761
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@76
    iget-object v5, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@78
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarContainer;->getHeight()I

    #@7b
    move-result v5

    #@7c
    int-to-float v5, v5

    #@7d
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/ActionBarContainer;->setTranslationY(F)V

    #@80
    .line 762
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@82
    invoke-virtual {v4, v8}, Lcom/android/internal/widget/ActionBarContainer;->setVisibility(I)V

    #@85
    .line 763
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@87
    const-string/jumbo v5, "translationY"

    #@8a
    new-array v6, v9, [F

    #@8c
    aput v7, v6, v8

    #@8e
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@91
    move-result-object v4

    #@92
    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@95
    .line 765
    :cond_95
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@97
    const v5, 0x10c0003

    #@9a
    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@a1
    .line 767
    const-wide/16 v4, 0xfa

    #@a3
    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@a6
    .line 775
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mShowListener:Landroid/animation/Animator$AnimatorListener;

    #@a8
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@ab
    .line 776
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@ad
    .line 777
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    #@b0
    .line 791
    .end local v0           #anim:Landroid/animation/AnimatorSet;
    .end local v1           #b:Landroid/animation/AnimatorSet$Builder;
    .end local v2           #startingY:F
    :goto_b0
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@b2
    if-eqz v4, :cond_b9

    #@b4
    .line 792
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@b6
    invoke-virtual {v4}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    #@b9
    .line 794
    :cond_b9
    return-void

    #@ba
    .line 779
    :cond_ba
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@bc
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setAlpha(F)V

    #@bf
    .line 780
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTopVisibilityView:Landroid/view/ViewGroup;

    #@c1
    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setTranslationY(F)V

    #@c4
    .line 781
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@c6
    if-eqz v4, :cond_cd

    #@c8
    .line 782
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContentView:Landroid/view/View;

    #@ca
    invoke-virtual {v4, v7}, Landroid/view/View;->setTranslationY(F)V

    #@cd
    .line 784
    :cond_cd
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@cf
    if-eqz v4, :cond_e4

    #@d1
    iget v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContextDisplayMode:I

    #@d3
    if-ne v4, v9, :cond_e4

    #@d5
    .line 785
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@d7
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/ActionBarContainer;->setAlpha(F)V

    #@da
    .line 786
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@dc
    invoke-virtual {v4, v7}, Lcom/android/internal/widget/ActionBarContainer;->setTranslationY(F)V

    #@df
    .line 787
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@e1
    invoke-virtual {v4, v8}, Lcom/android/internal/widget/ActionBarContainer;->setVisibility(I)V

    #@e4
    .line 789
    :cond_e4
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mShowListener:Landroid/animation/Animator$AnimatorListener;

    #@e6
    const/4 v5, 0x0

    #@e7
    invoke-interface {v4, v5}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@ea
    goto :goto_b0

    #@eb
    .line 748
    nop

    #@ec
    :array_ec
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public getCustomView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 489
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getCustomNavigationView()Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDisplayOptions()I
    .registers 2

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getDisplayOptions()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 655
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarContainer;->getHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getNavigationItemCount()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1195
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/widget/ActionBarView;->getNavigationMode()I

    #@6
    move-result v2

    #@7
    packed-switch v2, :pswitch_data_20

    #@a
    .line 1202
    :cond_a
    :goto_a
    return v1

    #@b
    .line 1197
    :pswitch_b
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v1

    #@11
    goto :goto_a

    #@12
    .line 1199
    :pswitch_12
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@14
    invoke-virtual {v2}, Lcom/android/internal/widget/ActionBarView;->getDropdownAdapter()Landroid/widget/SpinnerAdapter;

    #@17
    move-result-object v0

    #@18
    .line 1200
    .local v0, adapter:Landroid/widget/SpinnerAdapter;
    if-eqz v0, :cond_a

    #@1a
    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    #@1d
    move-result v1

    #@1e
    goto :goto_a

    #@1f
    .line 1195
    nop

    #@20
    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_12
        :pswitch_b
    .end packed-switch
.end method

.method public getNavigationMode()I
    .registers 2

    #@0
    .prologue
    .line 501
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getNavigationMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getSelectedNavigationIndex()I
    .registers 3

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 1183
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarView;->getNavigationMode()I

    #@6
    move-result v1

    #@7
    packed-switch v1, :pswitch_data_1e

    #@a
    .line 1189
    :cond_a
    :goto_a
    return v0

    #@b
    .line 1185
    :pswitch_b
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@d
    if-eqz v1, :cond_a

    #@f
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->getPosition()I

    #@14
    move-result v0

    #@15
    goto :goto_a

    #@16
    .line 1187
    :pswitch_16
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@18
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getDropdownSelectedPosition()I

    #@1b
    move-result v0

    #@1c
    goto :goto_a

    #@1d
    .line 1183
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_16
        :pswitch_b
    .end packed-switch
.end method

.method public getSelectedTab()Landroid/app/ActionBar$Tab;
    .registers 2

    #@0
    .prologue
    .line 650
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@2
    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 497
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getSubtitle()Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTabAt(I)Landroid/app/ActionBar$Tab;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 1242
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActionBar$Tab;

    #@8
    return-object v0
.end method

.method public getTabCount()I
    .registers 2

    #@0
    .prologue
    .line 1208
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getThemedContext()Landroid/content/Context;
    .registers 6

    #@0
    .prologue
    .line 857
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mThemedContext:Landroid/content/Context;

    #@2
    if-nez v3, :cond_2b

    #@4
    .line 858
    new-instance v1, Landroid/util/TypedValue;

    #@6
    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    #@9
    .line 859
    .local v1, outValue:Landroid/util/TypedValue;
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@e
    move-result-object v0

    #@f
    .line 860
    .local v0, currentTheme:Landroid/content/res/Resources$Theme;
    const v3, 0x1010397

    #@12
    const/4 v4, 0x1

    #@13
    invoke-virtual {v0, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@16
    .line 862
    iget v2, v1, Landroid/util/TypedValue;->resourceId:I

    #@18
    .line 864
    .local v2, targetThemeRes:I
    if-eqz v2, :cond_2e

    #@1a
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v3}, Landroid/content/Context;->getThemeResId()I

    #@1f
    move-result v3

    #@20
    if-eq v3, v2, :cond_2e

    #@22
    .line 865
    new-instance v3, Landroid/view/ContextThemeWrapper;

    #@24
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@26
    invoke-direct {v3, v4, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    #@29
    iput-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mThemedContext:Landroid/content/Context;

    #@2b
    .line 870
    .end local v0           #currentTheme:Landroid/content/res/Resources$Theme;
    .end local v1           #outValue:Landroid/util/TypedValue;
    .end local v2           #targetThemeRes:I
    :cond_2b
    :goto_2b
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mThemedContext:Landroid/content/Context;

    #@2d
    return-object v3

    #@2e
    .line 867
    .restart local v0       #currentTheme:Landroid/content/res/Resources$Theme;
    .restart local v1       #outValue:Landroid/util/TypedValue;
    .restart local v2       #targetThemeRes:I
    :cond_2e
    iget-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@30
    iput-object v3, p0, Lcom/android/internal/app/ActionBarImpl;->mThemedContext:Landroid/content/Context;

    #@32
    goto :goto_2b
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 493
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getTitle()Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public hasNonEmbeddedTabs()Z
    .registers 3

    #@0
    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHasEmbeddedTabs:Z

    #@2
    if-nez v0, :cond_d

    #@4
    invoke-virtual {p0}, Lcom/android/internal/app/ActionBarImpl;->getNavigationMode()I

    #@7
    move-result v0

    #@8
    const/4 v1, 0x2

    #@9
    if-ne v0, v1, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public hide()V
    .registers 2

    #@0
    .prologue
    .line 685
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenByApp:Z

    #@2
    if-nez v0, :cond_b

    #@4
    .line 686
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenByApp:Z

    #@7
    .line 687
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->updateVisibility(Z)V

    #@b
    .line 689
    :cond_b
    return-void
.end method

.method public hideForSystem()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 702
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenBySystem:Z

    #@3
    if-nez v0, :cond_a

    #@5
    .line 703
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenBySystem:Z

    #@7
    .line 704
    invoke-direct {p0, v1}, Lcom/android/internal/app/ActionBarImpl;->updateVisibility(Z)V

    #@a
    .line 706
    :cond_a
    return-void
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 835
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mNowShowing:Z

    #@2
    return v0
.end method

.method public isSystemShowing()Z
    .registers 2

    #@0
    .prologue
    .line 839
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenBySystem:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isTitleTruncated()Z
    .registers 2

    #@0
    .prologue
    .line 875
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->isTitleTruncated()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public newTab()Landroid/app/ActionBar$Tab;
    .registers 2

    #@0
    .prologue
    .line 584
    new-instance v0, Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/app/ActionBarImpl$TabImpl;-><init>(Lcom/android/internal/app/ActionBarImpl;)V

    #@5
    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    #@0
    .prologue
    .line 236
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->setHasEmbeddedTabs()V

    #@3
    .line 237
    return-void
.end method

.method public removeAllTabs()V
    .registers 1

    #@0
    .prologue
    .line 437
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->cleanupTabs()V

    #@3
    .line 438
    return-void
.end method

.method public removeOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 362
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mMenuVisibilityListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    .line 363
    return-void
.end method

.method public removeTab(Landroid/app/ActionBar$Tab;)V
    .registers 3
    .parameter "tab"

    #@0
    .prologue
    .line 589
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->removeTabAt(I)V

    #@7
    .line 590
    return-void
.end method

.method public removeTabAt(I)V
    .registers 9
    .parameter "position"

    #@0
    .prologue
    .line 594
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@2
    if-nez v4, :cond_5

    #@4
    .line 615
    :cond_4
    :goto_4
    return-void

    #@5
    .line 599
    :cond_5
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@7
    if-eqz v4, :cond_39

    #@9
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@b
    invoke-virtual {v4}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->getPosition()I

    #@e
    move-result v3

    #@f
    .line 601
    .local v3, selectedTabPosition:I
    :goto_f
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@11
    invoke-virtual {v4, p1}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeTabAt(I)V

    #@14
    .line 602
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@1c
    .line 603
    .local v2, removedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;
    if-eqz v2, :cond_22

    #@1e
    .line 604
    const/4 v4, -0x1

    #@1f
    invoke-virtual {v2, v4}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->setPosition(I)V

    #@22
    .line 607
    :cond_22
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@27
    move-result v1

    #@28
    .line 608
    .local v1, newTabCount:I
    move v0, p1

    #@29
    .local v0, i:I
    :goto_29
    if-ge v0, v1, :cond_3c

    #@2b
    .line 609
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v4

    #@31
    check-cast v4, Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@33
    invoke-virtual {v4, v0}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->setPosition(I)V

    #@36
    .line 608
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_29

    #@39
    .line 599
    .end local v0           #i:I
    .end local v1           #newTabCount:I
    .end local v2           #removedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;
    .end local v3           #selectedTabPosition:I
    :cond_39
    iget v3, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@3b
    goto :goto_f

    #@3c
    .line 612
    .restart local v0       #i:I
    .restart local v1       #newTabCount:I
    .restart local v2       #removedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;
    .restart local v3       #selectedTabPosition:I
    :cond_3c
    if-ne v3, p1, :cond_4

    #@3e
    .line 613
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    #@43
    move-result v4

    #@44
    if-eqz v4, :cond_4b

    #@46
    const/4 v4, 0x0

    #@47
    :goto_47
    invoke-virtual {p0, v4}, Lcom/android/internal/app/ActionBarImpl;->selectTab(Landroid/app/ActionBar$Tab;)V

    #@4a
    goto :goto_4

    #@4b
    :cond_4b
    iget-object v4, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@4d
    const/4 v5, 0x0

    #@4e
    add-int/lit8 v6, p1, -0x1

    #@50
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    #@53
    move-result v5

    #@54
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@57
    move-result-object v4

    #@58
    check-cast v4, Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@5a
    goto :goto_47
.end method

.method public selectTab(Landroid/app/ActionBar$Tab;)V
    .registers 6
    .parameter "tab"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 619
    invoke-virtual {p0}, Lcom/android/internal/app/ActionBarImpl;->getNavigationMode()I

    #@4
    move-result v2

    #@5
    const/4 v3, 0x2

    #@6
    if-eq v2, v3, :cond_11

    #@8
    .line 620
    if-eqz p1, :cond_e

    #@a
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    #@d
    move-result v1

    #@e
    :cond_e
    iput v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@10
    .line 646
    .end local p1
    :cond_10
    :goto_10
    return-void

    #@11
    .line 624
    .restart local p1
    :cond_11
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActivity:Landroid/app/Activity;

    #@13
    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    #@1e
    move-result-object v0

    #@1f
    .line 627
    .local v0, trans:Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@21
    if-ne v2, p1, :cond_45

    #@23
    .line 628
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@25
    if-eqz v1, :cond_3b

    #@27
    .line 629
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@29
    invoke-virtual {v1}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    #@2c
    move-result-object v1

    #@2d
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@2f
    invoke-interface {v1, v2, v0}, Landroid/app/ActionBar$TabListener;->onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    #@32
    .line 630
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@34
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    #@37
    move-result v2

    #@38
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/ScrollingTabContainerView;->animateToTab(I)V

    #@3b
    .line 643
    .end local p1
    :cond_3b
    :goto_3b
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->isEmpty()Z

    #@3e
    move-result v1

    #@3f
    if-nez v1, :cond_10

    #@41
    .line 644
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    #@44
    goto :goto_10

    #@45
    .line 633
    .restart local p1
    :cond_45
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@47
    if-eqz p1, :cond_4d

    #@49
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    #@4c
    move-result v1

    #@4d
    :cond_4d
    invoke-virtual {v2, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setTabSelected(I)V

    #@50
    .line 634
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@52
    if-eqz v1, :cond_5f

    #@54
    .line 635
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@56
    invoke-virtual {v1}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    #@59
    move-result-object v1

    #@5a
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@5c
    invoke-interface {v1, v2, v0}, Landroid/app/ActionBar$TabListener;->onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    #@5f
    .line 637
    :cond_5f
    check-cast p1, Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@61
    .end local p1
    iput-object p1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@63
    .line 638
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@65
    if-eqz v1, :cond_3b

    #@67
    .line 639
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@69
    invoke-virtual {v1}, Lcom/android/internal/app/ActionBarImpl$TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    #@6c
    move-result-object v1

    #@6d
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSelectedTab:Lcom/android/internal/app/ActionBarImpl$TabImpl;

    #@6f
    invoke-interface {v1, v2, v0}, Landroid/app/ActionBar$TabListener;->onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    #@72
    goto :goto_3b
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 475
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarContainer;->setPrimaryBackground(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 476
    return-void
.end method

.method public setCustomView(I)V
    .registers 5
    .parameter "resId"

    #@0
    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/android/internal/app/ActionBarImpl;->getThemedContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@7
    move-result-object v0

    #@8
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@a
    const/4 v2, 0x0

    #@b
    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->setCustomView(Landroid/view/View;)V

    #@12
    .line 380
    return-void
.end method

.method public setCustomView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 1166
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setCustomNavigationView(Landroid/view/View;)V

    #@5
    .line 1167
    return-void
.end method

.method public setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V
    .registers 4
    .parameter "view"
    .parameter "layoutParams"

    #@0
    .prologue
    .line 1171
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 1172
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@5
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setCustomNavigationView(Landroid/view/View;)V

    #@8
    .line 1173
    return-void
.end method

.method public setDefaultDisplayHomeAsUpEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 1267
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mDisplayHomeAsUpSet:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 1268
    invoke-virtual {p0, p1}, Lcom/android/internal/app/ActionBarImpl;->setDisplayHomeAsUpEnabled(Z)V

    #@7
    .line 1270
    :cond_7
    return-void
.end method

.method public setDisplayHomeAsUpEnabled(Z)V
    .registers 4
    .parameter "showHomeAsUp"

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    .line 394
    if-eqz p1, :cond_8

    #@3
    move v0, v1

    #@4
    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/ActionBarImpl;->setDisplayOptions(II)V

    #@7
    .line 395
    return-void

    #@8
    .line 394
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_4
.end method

.method public setDisplayOptions(I)V
    .registers 3
    .parameter "options"

    #@0
    .prologue
    .line 460
    and-int/lit8 v0, p1, 0x4

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 461
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mDisplayHomeAsUpSet:Z

    #@7
    .line 463
    :cond_7
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setDisplayOptions(I)V

    #@c
    .line 464
    return-void
.end method

.method public setDisplayOptions(II)V
    .registers 7
    .parameter "options"
    .parameter "mask"

    #@0
    .prologue
    .line 467
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarView;->getDisplayOptions()I

    #@5
    move-result v0

    #@6
    .line 468
    .local v0, current:I
    and-int/lit8 v1, p2, 0x4

    #@8
    if-eqz v1, :cond_d

    #@a
    .line 469
    const/4 v1, 0x1

    #@b
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mDisplayHomeAsUpSet:Z

    #@d
    .line 471
    :cond_d
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@f
    and-int v2, p1, p2

    #@11
    xor-int/lit8 v3, p2, -0x1

    #@13
    and-int/2addr v3, v0

    #@14
    or-int/2addr v2, v3

    #@15
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/ActionBarView;->setDisplayOptions(I)V

    #@18
    .line 472
    return-void
.end method

.method public setDisplayShowCustomEnabled(Z)V
    .registers 4
    .parameter "showCustom"

    #@0
    .prologue
    const/16 v1, 0x10

    #@2
    .line 404
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/ActionBarImpl;->setDisplayOptions(II)V

    #@8
    .line 405
    return-void

    #@9
    .line 404
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setDisplayShowHomeEnabled(Z)V
    .registers 4
    .parameter "showHome"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 389
    if-eqz p1, :cond_8

    #@3
    move v0, v1

    #@4
    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/ActionBarImpl;->setDisplayOptions(II)V

    #@7
    .line 390
    return-void

    #@8
    .line 389
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_4
.end method

.method public setDisplayShowTitleEnabled(Z)V
    .registers 4
    .parameter "showTitle"

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 399
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/ActionBarImpl;->setDisplayOptions(II)V

    #@8
    .line 400
    return-void

    #@9
    .line 399
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setDisplayUseLogoEnabled(Z)V
    .registers 4
    .parameter "useLogo"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 384
    if-eqz p1, :cond_8

    #@3
    move v0, v1

    #@4
    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/ActionBarImpl;->setDisplayOptions(II)V

    #@7
    .line 385
    return-void

    #@8
    .line 384
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_4
.end method

.method public setHomeButtonEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 409
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setHomeButtonEnabled(Z)V

    #@5
    .line 410
    return-void
.end method

.method public setIcon(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 1248
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setIcon(I)V

    #@5
    .line 1249
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "icon"

    #@0
    .prologue
    .line 1253
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 1254
    return-void
.end method

.method public setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V
    .registers 4
    .parameter "adapter"
    .parameter "callback"

    #@0
    .prologue
    .line 1177
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setDropdownAdapter(Landroid/widget/SpinnerAdapter;)V

    #@5
    .line 1178
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@7
    invoke-virtual {v0, p2}, Lcom/android/internal/widget/ActionBarView;->setCallback(Landroid/app/ActionBar$OnNavigationListener;)V

    #@a
    .line 1179
    return-void
.end method

.method public setLogo(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 1258
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setLogo(I)V

    #@5
    .line 1259
    return-void
.end method

.method public setLogo(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "logo"

    #@0
    .prologue
    .line 1263
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setLogo(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 1264
    return-void
.end method

.method public setNavigationMode(I)V
    .registers 7
    .parameter "mode"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    .line 1213
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@4
    invoke-virtual {v2}, Lcom/android/internal/widget/ActionBarView;->getNavigationMode()I

    #@7
    move-result v0

    #@8
    .line 1214
    .local v0, oldMode:I
    packed-switch v0, :pswitch_data_56

    #@b
    .line 1221
    :goto_b
    if-eq v0, p1, :cond_1a

    #@d
    iget-boolean v2, p0, Lcom/android/internal/app/ActionBarImpl;->mHasEmbeddedTabs:Z

    #@f
    if-nez v2, :cond_1a

    #@11
    .line 1222
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@13
    if-eqz v2, :cond_1a

    #@15
    .line 1223
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@17
    invoke-virtual {v2}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    #@1a
    .line 1226
    :cond_1a
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@1c
    invoke-virtual {v2, p1}, Lcom/android/internal/widget/ActionBarView;->setNavigationMode(I)V

    #@1f
    .line 1227
    packed-switch p1, :pswitch_data_5c

    #@22
    .line 1237
    :cond_22
    :goto_22
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@24
    const/4 v3, 0x2

    #@25
    if-ne p1, v3, :cond_2c

    #@27
    iget-boolean v3, p0, Lcom/android/internal/app/ActionBarImpl;->mHasEmbeddedTabs:Z

    #@29
    if-nez v3, :cond_2c

    #@2b
    const/4 v1, 0x1

    #@2c
    :cond_2c
    invoke-virtual {v2, v1}, Lcom/android/internal/widget/ActionBarView;->setCollapsable(Z)V

    #@2f
    .line 1238
    return-void

    #@30
    .line 1216
    :pswitch_30
    invoke-virtual {p0}, Lcom/android/internal/app/ActionBarImpl;->getSelectedNavigationIndex()I

    #@33
    move-result v2

    #@34
    iput v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@36
    .line 1217
    const/4 v2, 0x0

    #@37
    invoke-virtual {p0, v2}, Lcom/android/internal/app/ActionBarImpl;->selectTab(Landroid/app/ActionBar$Tab;)V

    #@3a
    .line 1218
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@3c
    const/16 v3, 0x8

    #@3e
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    #@41
    goto :goto_b

    #@42
    .line 1229
    :pswitch_42
    invoke-direct {p0}, Lcom/android/internal/app/ActionBarImpl;->ensureTabsExist()V

    #@45
    .line 1230
    iget-object v2, p0, Lcom/android/internal/app/ActionBarImpl;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@47
    invoke-virtual {v2, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    #@4a
    .line 1231
    iget v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@4c
    if-eq v2, v4, :cond_22

    #@4e
    .line 1232
    iget v2, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@50
    invoke-virtual {p0, v2}, Lcom/android/internal/app/ActionBarImpl;->setSelectedNavigationItem(I)V

    #@53
    .line 1233
    iput v4, p0, Lcom/android/internal/app/ActionBarImpl;->mSavedTabPosition:I

    #@55
    goto :goto_22

    #@56
    .line 1214
    :pswitch_data_56
    .packed-switch 0x2
        :pswitch_30
    .end packed-switch

    #@5c
    .line 1227
    :pswitch_data_5c
    .packed-switch 0x2
        :pswitch_42
    .end packed-switch
.end method

.method public setSelectedNavigationItem(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getNavigationMode()I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_24

    #@9
    .line 431
    new-instance v0, Ljava/lang/IllegalStateException;

    #@b
    const-string/jumbo v1, "setSelectedNavigationIndex not valid for current navigation mode"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 425
    :pswitch_12
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mTabs:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/app/ActionBar$Tab;

    #@1a
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->selectTab(Landroid/app/ActionBar$Tab;)V

    #@1d
    .line 434
    :goto_1d
    return-void

    #@1e
    .line 428
    :pswitch_1e
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@20
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setDropdownSelectedPosition(I)V

    #@23
    goto :goto_1d

    #@24
    .line 423
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_12
    .end packed-switch
.end method

.method public setShowHideAnimationEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 351
    iput-boolean p1, p0, Lcom/android/internal/app/ActionBarImpl;->mShowHideAnimationEnabled:Z

    #@2
    .line 352
    if-nez p1, :cond_d

    #@4
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 353
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mCurrentShowAnim:Landroid/animation/Animator;

    #@a
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@d
    .line 355
    :cond_d
    return-void
.end method

.method public setSplitBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 483
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 484
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarContainer;->setSplitBackground(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 486
    :cond_9
    return-void
.end method

.method public setStackedBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 479
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarContainer;->setStackedBackground(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 480
    return-void
.end method

.method public setSubtitle(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 419
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->setSubtitle(Ljava/lang/CharSequence;)V

    #@9
    .line 420
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "subtitle"

    #@0
    .prologue
    .line 456
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setSubtitle(Ljava/lang/CharSequence;)V

    #@5
    .line 457
    return-void
.end method

.method public setTitle(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->setTitle(Ljava/lang/CharSequence;)V

    #@9
    .line 415
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 452
    iget-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setTitle(Ljava/lang/CharSequence;)V

    #@5
    .line 453
    return-void
.end method

.method public setWindowVisibility(I)V
    .registers 2
    .parameter "visibility"

    #@0
    .prologue
    .line 340
    iput p1, p0, Lcom/android/internal/app/ActionBarImpl;->mCurWindowVisibility:I

    #@2
    .line 341
    return-void
.end method

.method public show()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 660
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenByApp:Z

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 661
    iput-boolean v1, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenByApp:Z

    #@7
    .line 662
    invoke-direct {p0, v1}, Lcom/android/internal/app/ActionBarImpl;->updateVisibility(Z)V

    #@a
    .line 664
    :cond_a
    return-void
.end method

.method public showForSystem()V
    .registers 2

    #@0
    .prologue
    .line 677
    iget-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenBySystem:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 678
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/app/ActionBarImpl;->mHiddenBySystem:Z

    #@7
    .line 679
    const/4 v0, 0x1

    #@8
    invoke-direct {p0, v0}, Lcom/android/internal/app/ActionBarImpl;->updateVisibility(Z)V

    #@b
    .line 681
    :cond_b
    return-void
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 5
    .parameter "callback"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 509
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mActionMode:Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;

    #@3
    if-eqz v1, :cond_a

    #@5
    .line 510
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mActionMode:Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;

    #@7
    invoke-virtual {v1}, Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;->finish()V

    #@a
    .line 513
    :cond_a
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@c
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarContextView;->killMode()V

    #@f
    .line 514
    new-instance v0, Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;

    #@11
    invoke-direct {v0, p0, p1}, Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;-><init>(Lcom/android/internal/app/ActionBarImpl;Landroid/view/ActionMode$Callback;)V

    #@14
    .line 515
    .local v0, mode:Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;
    invoke-virtual {v0}, Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;->dispatchOnCreate()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_4e

    #@1a
    .line 516
    invoke-virtual {v0}, Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;->invalidate()V

    #@1d
    .line 517
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@1f
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ActionBarContextView;->initForMode(Landroid/view/ActionMode;)V

    #@22
    .line 518
    invoke-virtual {p0, v2}, Lcom/android/internal/app/ActionBarImpl;->animateToMode(Z)V

    #@25
    .line 519
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@27
    if-eqz v1, :cond_44

    #@29
    iget v1, p0, Lcom/android/internal/app/ActionBarImpl;->mContextDisplayMode:I

    #@2b
    if-ne v1, v2, :cond_44

    #@2d
    .line 521
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@2f
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarContainer;->getVisibility()I

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_44

    #@35
    .line 522
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@37
    const/4 v2, 0x0

    #@38
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/ActionBarContainer;->setVisibility(I)V

    #@3b
    .line 523
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@3d
    if-eqz v1, :cond_44

    #@3f
    .line 524
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mOverlayLayout:Lcom/android/internal/widget/ActionBarOverlayLayout;

    #@41
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    #@44
    .line 528
    :cond_44
    iget-object v1, p0, Lcom/android/internal/app/ActionBarImpl;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@46
    const/16 v2, 0x20

    #@48
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    #@4b
    .line 529
    iput-object v0, p0, Lcom/android/internal/app/ActionBarImpl;->mActionMode:Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;

    #@4d
    .line 532
    .end local v0           #mode:Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;
    :goto_4d
    return-object v0

    #@4e
    .restart local v0       #mode:Lcom/android/internal/app/ActionBarImpl$ActionModeImpl;
    :cond_4e
    const/4 v0, 0x0

    #@4f
    goto :goto_4d
.end method
