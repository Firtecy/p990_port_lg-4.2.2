.class public Lcom/android/internal/app/ResolverActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "ResolverActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/ResolverActivity$ItemLongClickListener;,
        Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;,
        Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ResolverActivity"


# instance fields
.field private mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

.field private mAlwaysButton:Landroid/widget/Button;

.field private mAlwaysUseOption:Z

.field private mGrid:Landroid/widget/ListView;

.field private mIconDpi:I

.field private mIconSize:I

.field private mLastSelected:I

.field private mLaunchedFromUid:I

.field private mMaxColumns:I

.field private mOnceButton:Landroid/widget/Button;

.field private final mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

.field private mPm:Landroid/content/pm/PackageManager;

.field private mRegistered:Z

.field private mShowExtended:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    .line 86
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/app/ResolverActivity;->mLastSelected:I

    #@6
    .line 89
    new-instance v0, Lcom/android/internal/app/ResolverActivity$1;

    #@8
    invoke-direct {v0, p0}, Lcom/android/internal/app/ResolverActivity$1;-><init>(Lcom/android/internal/app/ResolverActivity;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/app/ResolverActivity;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@d
    .line 728
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/app/ResolverActivity;)Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/app/ResolverActivity;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysUseOption:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/app/ResolverActivity;)Landroid/content/pm/PackageManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity;->mPm:Landroid/content/pm/PackageManager;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/app/ResolverActivity;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/android/internal/app/ResolverActivity;->mShowExtended:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/android/internal/app/ResolverActivity;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/android/internal/app/ResolverActivity;->mShowExtended:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/internal/app/ResolverActivity;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/android/internal/app/ResolverActivity;->mIconSize:I

    #@2
    return v0
.end method

.method private makeMyIntent()Landroid/content/Intent;
    .registers 4

    #@0
    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getIntent()Landroid/content/Intent;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@9
    .line 102
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    #@c
    move-result v1

    #@d
    const v2, -0x800001

    #@10
    and-int/2addr v1, v2

    #@11
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@14
    .line 103
    return-object v0
.end method


# virtual methods
.method getIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .registers 6
    .parameter "res"
    .parameter "resId"

    #@0
    .prologue
    .line 207
    :try_start_0
    iget v2, p0, Lcom/android/internal/app/ResolverActivity;->mIconDpi:I

    #@2
    invoke-virtual {p1, p2, v2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 212
    .local v1, result:Landroid/graphics/drawable/Drawable;
    :goto_6
    return-object v1

    #@7
    .line 208
    .end local v1           #result:Landroid/graphics/drawable/Drawable;
    :catch_7
    move-exception v0

    #@8
    .line 209
    .local v0, e:Landroid/content/res/Resources$NotFoundException;
    const/4 v1, 0x0

    #@9
    .restart local v1       #result:Landroid/graphics/drawable/Drawable;
    goto :goto_6
.end method

.method loadIconForResolveInfo(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter "ri"

    #@0
    .prologue
    .line 218
    :try_start_0
    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@2
    if-eqz v3, :cond_19

    #@4
    iget v3, p1, Landroid/content/pm/ResolveInfo;->icon:I

    #@6
    if-eqz v3, :cond_19

    #@8
    .line 219
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity;->mPm:Landroid/content/pm/PackageManager;

    #@a
    iget-object v4, p1, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@c
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    #@f
    move-result-object v3

    #@10
    iget v4, p1, Landroid/content/pm/ResolveInfo;->icon:I

    #@12
    invoke-virtual {p0, v3, v4}, Lcom/android/internal/app/ResolverActivity;->getIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    #@15
    move-result-object v0

    #@16
    .line 220
    .local v0, dr:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_19

    #@18
    .line 234
    .end local v0           #dr:Landroid/graphics/drawable/Drawable;
    :cond_18
    :goto_18
    return-object v0

    #@19
    .line 224
    :cond_19
    invoke-virtual {p1}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    #@1c
    move-result v2

    #@1d
    .line 225
    .local v2, iconRes:I
    if-eqz v2, :cond_2f

    #@1f
    .line 226
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity;->mPm:Landroid/content/pm/PackageManager;

    #@21
    iget-object v4, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@23
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@25
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/app/ResolverActivity;->getIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    :try_end_2c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_2c} :catch_36

    #@2c
    move-result-object v0

    #@2d
    .line 227
    .restart local v0       #dr:Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_18

    #@2f
    .line 234
    .end local v0           #dr:Landroid/graphics/drawable/Drawable;
    .end local v2           #iconRes:I
    :cond_2f
    :goto_2f
    iget-object v3, p0, Lcom/android/internal/app/ResolverActivity;->mPm:Landroid/content/pm/PackageManager;

    #@31
    invoke-virtual {p1, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@34
    move-result-object v0

    #@35
    goto :goto_18

    #@36
    .line 231
    :catch_36
    move-exception v1

    #@37
    .line 232
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "ResolverActivity"

    #@39
    const-string v4, "Couldn\'t find resources for package"

    #@3b
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3e
    goto :goto_2f
.end method

.method public onButtonClick(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 305
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@3
    move-result v0

    #@4
    .line 306
    .local v0, id:I
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@6
    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    #@9
    move-result v2

    #@a
    const v1, 0x1020365

    #@d
    if-ne v0, v1, :cond_17

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    invoke-virtual {p0, v2, v1}, Lcom/android/internal/app/ResolverActivity;->startSelected(IZ)V

    #@13
    .line 307
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->dismiss()V

    #@16
    .line 308
    return-void

    #@17
    .line 306
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_10
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 108
    invoke-direct {p0}, Lcom/android/internal/app/ResolverActivity;->makeMyIntent()Landroid/content/Intent;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v0

    #@9
    const v1, 0x10403f5

    #@c
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v3

    #@10
    const/4 v6, 0x1

    #@11
    move-object v0, p0

    #@12
    move-object v1, p1

    #@13
    move-object v5, v4

    #@14
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/app/ResolverActivity;->onCreate(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/CharSequence;[Landroid/content/Intent;Ljava/util/List;Z)V

    #@17
    .line 111
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/CharSequence;[Landroid/content/Intent;Ljava/util/List;Z)V
    .registers 20
    .parameter "savedInstanceState"
    .parameter "intent"
    .parameter "title"
    .parameter "initialIntents"
    .parameter
    .parameter "alwaysUseOption"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Landroid/content/Intent;",
            "Ljava/lang/CharSequence;",
            "[",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 116
    .local p5, rList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const v1, 0x103030f

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/app/ResolverActivity;->setTheme(I)V

    #@6
    .line 117
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@8
    if-eqz v1, :cond_10

    #@a
    .line 118
    const v1, 0x20a01cb

    #@d
    invoke-virtual {p0, v1}, Lcom/android/internal/app/ResolverActivity;->setTheme(I)V

    #@10
    .line 120
    :cond_10
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@13
    .line 122
    :try_start_13
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getActivityToken()Landroid/os/IBinder;

    #@1a
    move-result-object v2

    #@1b
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getLaunchedFromUid(Landroid/os/IBinder;)I

    #@1e
    move-result v1

    #@1f
    iput v1, p0, Lcom/android/internal/app/ResolverActivity;->mLaunchedFromUid:I
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_21} :catch_8e

    #@21
    .line 127
    :goto_21
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@24
    move-result-object v1

    #@25
    iput-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mPm:Landroid/content/pm/PackageManager;

    #@27
    .line 128
    move/from16 v0, p6

    #@29
    iput-boolean v0, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysUseOption:Z

    #@2b
    .line 129
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getResources()Landroid/content/res/Resources;

    #@2e
    move-result-object v1

    #@2f
    const v2, 0x10e0038

    #@32
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    #@35
    move-result v1

    #@36
    iput v1, p0, Lcom/android/internal/app/ResolverActivity;->mMaxColumns:I

    #@38
    .line 130
    const/4 v1, 0x0

    #@39
    invoke-virtual {p2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@3c
    .line 132
    iget-object v9, p0, Lcom/android/internal/app/AlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@3e
    .line 135
    .local v9, ap:Lcom/android/internal/app/AlertController$AlertParams;
    move-object/from16 v0, p3

    #@40
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ResolverActivity;->setTitle(Ljava/lang/CharSequence;)V

    #@43
    .line 138
    move-object/from16 v0, p3

    #@45
    iput-object v0, v9, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@47
    .line 140
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@49
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getMainLooper()Landroid/os/Looper;

    #@4c
    move-result-object v2

    #@4d
    const/4 v3, 0x0

    #@4e
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    #@51
    .line 141
    const/4 v1, 0x1

    #@52
    iput-boolean v1, p0, Lcom/android/internal/app/ResolverActivity;->mRegistered:Z

    #@54
    .line 143
    const-string v1, "activity"

    #@56
    invoke-virtual {p0, v1}, Lcom/android/internal/app/ResolverActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@59
    move-result-object v8

    #@5a
    check-cast v8, Landroid/app/ActivityManager;

    #@5c
    .line 144
    .local v8, am:Landroid/app/ActivityManager;
    invoke-virtual {v8}, Landroid/app/ActivityManager;->getLauncherLargeIconDensity()I

    #@5f
    move-result v1

    #@60
    iput v1, p0, Lcom/android/internal/app/ResolverActivity;->mIconDpi:I

    #@62
    .line 145
    invoke-virtual {v8}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    #@65
    move-result v1

    #@66
    iput v1, p0, Lcom/android/internal/app/ResolverActivity;->mIconSize:I

    #@68
    .line 147
    new-instance v1, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@6a
    iget v7, p0, Lcom/android/internal/app/ResolverActivity;->mLaunchedFromUid:I

    #@6c
    move-object v2, p0

    #@6d
    move-object v3, p0

    #@6e
    move-object v4, p2

    #@6f
    move-object/from16 v5, p4

    #@71
    move-object/from16 v6, p5

    #@73
    invoke-direct/range {v1 .. v7}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;-><init>(Lcom/android/internal/app/ResolverActivity;Landroid/content/Context;Landroid/content/Intent;[Landroid/content/Intent;Ljava/util/List;I)V

    #@76
    iput-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@78
    .line 149
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@7a
    invoke-virtual {v1}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->getCount()I

    #@7d
    move-result v11

    #@7e
    .line 150
    .local v11, count:I
    iget v1, p0, Lcom/android/internal/app/ResolverActivity;->mLaunchedFromUid:I

    #@80
    if-ltz v1, :cond_8a

    #@82
    iget v1, p0, Lcom/android/internal/app/ResolverActivity;->mLaunchedFromUid:I

    #@84
    invoke-static {v1}, Landroid/os/UserHandle;->isIsolated(I)Z

    #@87
    move-result v1

    #@88
    if-eqz v1, :cond_93

    #@8a
    .line 152
    :cond_8a
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->finish()V

    #@8d
    .line 194
    :cond_8d
    :goto_8d
    return-void

    #@8e
    .line 124
    .end local v8           #am:Landroid/app/ActivityManager;
    .end local v9           #ap:Lcom/android/internal/app/AlertController$AlertParams;
    .end local v11           #count:I
    :catch_8e
    move-exception v12

    #@8f
    .line 125
    .local v12, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@90
    iput v1, p0, Lcom/android/internal/app/ResolverActivity;->mLaunchedFromUid:I

    #@92
    goto :goto_21

    #@93
    .line 154
    .end local v12           #e:Landroid/os/RemoteException;
    .restart local v8       #am:Landroid/app/ActivityManager;
    .restart local v9       #ap:Lcom/android/internal/app/AlertController$AlertParams;
    .restart local v11       #count:I
    :cond_93
    const/4 v1, 0x1

    #@94
    if-le v11, v1, :cond_f4

    #@96
    .line 155
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@99
    move-result-object v1

    #@9a
    const v2, 0x10900b6

    #@9d
    const/4 v3, 0x0

    #@9e
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@a1
    move-result-object v1

    #@a2
    iput-object v1, v9, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@a4
    .line 158
    iget-object v1, v9, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@a6
    const v2, 0x1020364

    #@a9
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@ac
    move-result-object v1

    #@ad
    check-cast v1, Landroid/widget/ListView;

    #@af
    iput-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@b1
    .line 159
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@b3
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@b5
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@b8
    .line 160
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@ba
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@bd
    .line 164
    if-eqz p6, :cond_c5

    #@bf
    .line 165
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@c1
    const/4 v2, 0x1

    #@c2
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@c5
    .line 181
    :cond_c5
    :goto_c5
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->setupAlert()V

    #@c8
    .line 183
    if-eqz p6, :cond_8d

    #@ca
    .line 184
    const v1, 0x1020287

    #@cd
    invoke-virtual {p0, v1}, Lcom/android/internal/app/ResolverActivity;->findViewById(I)Landroid/view/View;

    #@d0
    move-result-object v10

    #@d1
    check-cast v10, Landroid/view/ViewGroup;

    #@d3
    .line 185
    .local v10, buttonLayout:Landroid/view/ViewGroup;
    if-eqz v10, :cond_11b

    #@d5
    .line 186
    const/4 v1, 0x0

    #@d6
    invoke-virtual {v10, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@d9
    .line 187
    const/4 v1, 0x0

    #@da
    invoke-virtual {v10, v1}, Landroid/view/ViewGroup;->setLayoutDirection(I)V

    #@dd
    .line 188
    const v1, 0x1020365

    #@e0
    invoke-virtual {v10, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@e3
    move-result-object v1

    #@e4
    check-cast v1, Landroid/widget/Button;

    #@e6
    iput-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysButton:Landroid/widget/Button;

    #@e8
    .line 189
    const v1, 0x1020366

    #@eb
    invoke-virtual {v10, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@ee
    move-result-object v1

    #@ef
    check-cast v1, Landroid/widget/Button;

    #@f1
    iput-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mOnceButton:Landroid/widget/Button;

    #@f3
    goto :goto_8d

    #@f4
    .line 171
    .end local v10           #buttonLayout:Landroid/view/ViewGroup;
    :cond_f4
    const/4 v1, 0x1

    #@f5
    if-ne v11, v1, :cond_10d

    #@f7
    .line 172
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@f9
    const/4 v2, 0x0

    #@fa
    invoke-virtual {v1, v2}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->intentForPosition(I)Landroid/content/Intent;

    #@fd
    move-result-object v1

    #@fe
    invoke-virtual {p0, v1}, Lcom/android/internal/app/ResolverActivity;->startActivity(Landroid/content/Intent;)V

    #@101
    .line 173
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@103
    invoke-virtual {v1}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    #@106
    .line 174
    const/4 v1, 0x0

    #@107
    iput-boolean v1, p0, Lcom/android/internal/app/ResolverActivity;->mRegistered:Z

    #@109
    .line 175
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->finish()V

    #@10c
    goto :goto_8d

    #@10d
    .line 178
    :cond_10d
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getResources()Landroid/content/res/Resources;

    #@110
    move-result-object v1

    #@111
    const v2, 0x10403fa

    #@114
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@117
    move-result-object v1

    #@118
    iput-object v1, v9, Lcom/android/internal/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    #@11a
    goto :goto_c5

    #@11b
    .line 191
    .restart local v10       #buttonLayout:Landroid/view/ViewGroup;
    :cond_11b
    const/4 v1, 0x0

    #@11c
    iput-boolean v1, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysUseOption:Z

    #@11e
    goto/16 :goto_8d
.end method

.method protected onIntentSelected(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;Z)V
    .registers 28
    .parameter "ri"
    .parameter "intent"
    .parameter "alwaysCheck"

    #@0
    .prologue
    .line 318
    if-eqz p3, :cond_16f

    #@2
    .line 320
    new-instance v11, Landroid/content/IntentFilter;

    #@4
    invoke-direct {v11}, Landroid/content/IntentFilter;-><init>()V

    #@7
    .line 322
    .local v11, filter:Landroid/content/IntentFilter;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v21

    #@b
    if-eqz v21, :cond_16

    #@d
    .line 323
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@10
    move-result-object v21

    #@11
    move-object/from16 v0, v21

    #@13
    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@16
    .line 325
    :cond_16
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@19
    move-result-object v8

    #@1a
    .line 326
    .local v8, categories:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v8, :cond_30

    #@1c
    .line 327
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v13

    #@20
    .local v13, i$:Ljava/util/Iterator;
    :goto_20
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@23
    move-result v21

    #@24
    if-eqz v21, :cond_30

    #@26
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@29
    move-result-object v7

    #@2a
    check-cast v7, Ljava/lang/String;

    #@2c
    .line 328
    .local v7, cat:Ljava/lang/String;
    invoke-virtual {v11, v7}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    #@2f
    goto :goto_20

    #@30
    .line 331
    .end local v7           #cat:Ljava/lang/String;
    .end local v13           #i$:Ljava/util/Iterator;
    :cond_30
    const-string v21, "android.intent.category.DEFAULT"

    #@32
    move-object/from16 v0, v21

    #@34
    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    #@37
    .line 333
    move-object/from16 v0, p1

    #@39
    iget v0, v0, Landroid/content/pm/ResolveInfo;->match:I

    #@3b
    move/from16 v21, v0

    #@3d
    const/high16 v22, 0xfff

    #@3f
    and-int v7, v21, v22

    #@41
    .line 334
    .local v7, cat:I
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@44
    move-result-object v9

    #@45
    .line 335
    .local v9, data:Landroid/net/Uri;
    const/high16 v21, 0x60

    #@47
    move/from16 v0, v21

    #@49
    if-ne v7, v0, :cond_58

    #@4b
    .line 336
    move-object/from16 v0, p2

    #@4d
    move-object/from16 v1, p0

    #@4f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    #@52
    move-result-object v14

    #@53
    .line 337
    .local v14, mimeType:Ljava/lang/String;
    if-eqz v14, :cond_58

    #@55
    .line 339
    :try_start_55
    invoke-virtual {v11, v14}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_58
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_55 .. :try_end_58} :catch_14f

    #@58
    .line 346
    .end local v14           #mimeType:Ljava/lang/String;
    :cond_58
    :goto_58
    if-eqz v9, :cond_ef

    #@5a
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@5d
    move-result-object v21

    #@5e
    if-eqz v21, :cond_ef

    #@60
    .line 350
    const/high16 v21, 0x60

    #@62
    move/from16 v0, v21

    #@64
    if-ne v7, v0, :cond_7e

    #@66
    const-string v21, "file"

    #@68
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@6b
    move-result-object v22

    #@6c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v21

    #@70
    if-nez v21, :cond_ef

    #@72
    const-string v21, "content"

    #@74
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@77
    move-result-object v22

    #@78
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7b
    move-result v21

    #@7c
    if-nez v21, :cond_ef

    #@7e
    .line 353
    :cond_7e
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@81
    move-result-object v21

    #@82
    move-object/from16 v0, v21

    #@84
    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@87
    .line 357
    move-object/from16 v0, p1

    #@89
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@8b
    move-object/from16 v21, v0

    #@8d
    invoke-virtual/range {v21 .. v21}, Landroid/content/IntentFilter;->authoritiesIterator()Ljava/util/Iterator;

    #@90
    move-result-object v5

    #@91
    .line 358
    .local v5, aIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/IntentFilter$AuthorityEntry;>;"
    if-eqz v5, :cond_ba

    #@93
    .line 359
    :cond_93
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@96
    move-result v21

    #@97
    if-eqz v21, :cond_ba

    #@99
    .line 360
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9c
    move-result-object v4

    #@9d
    check-cast v4, Landroid/content/IntentFilter$AuthorityEntry;

    #@9f
    .line 361
    .local v4, a:Landroid/content/IntentFilter$AuthorityEntry;
    invoke-virtual {v4, v9}, Landroid/content/IntentFilter$AuthorityEntry;->match(Landroid/net/Uri;)I

    #@a2
    move-result v21

    #@a3
    if-ltz v21, :cond_93

    #@a5
    .line 362
    invoke-virtual {v4}, Landroid/content/IntentFilter$AuthorityEntry;->getPort()I

    #@a8
    move-result v18

    #@a9
    .line 363
    .local v18, port:I
    invoke-virtual {v4}, Landroid/content/IntentFilter$AuthorityEntry;->getHost()Ljava/lang/String;

    #@ac
    move-result-object v22

    #@ad
    if-ltz v18, :cond_15a

    #@af
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b2
    move-result-object v21

    #@b3
    :goto_b3
    move-object/from16 v0, v22

    #@b5
    move-object/from16 v1, v21

    #@b7
    invoke-virtual {v11, v0, v1}, Landroid/content/IntentFilter;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    .line 369
    .end local v4           #a:Landroid/content/IntentFilter$AuthorityEntry;
    .end local v18           #port:I
    :cond_ba
    move-object/from16 v0, p1

    #@bc
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@be
    move-object/from16 v21, v0

    #@c0
    invoke-virtual/range {v21 .. v21}, Landroid/content/IntentFilter;->pathsIterator()Ljava/util/Iterator;

    #@c3
    move-result-object v16

    #@c4
    .line 370
    .local v16, pIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/os/PatternMatcher;>;"
    if-eqz v16, :cond_ef

    #@c6
    .line 371
    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@c9
    move-result-object v17

    #@ca
    .line 372
    .local v17, path:Ljava/lang/String;
    :cond_ca
    if-eqz v17, :cond_ef

    #@cc
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    #@cf
    move-result v21

    #@d0
    if-eqz v21, :cond_ef

    #@d2
    .line 373
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d5
    move-result-object v15

    #@d6
    check-cast v15, Landroid/os/PatternMatcher;

    #@d8
    .line 374
    .local v15, p:Landroid/os/PatternMatcher;
    move-object/from16 v0, v17

    #@da
    invoke-virtual {v15, v0}, Landroid/os/PatternMatcher;->match(Ljava/lang/String;)Z

    #@dd
    move-result v21

    #@de
    if-eqz v21, :cond_ca

    #@e0
    .line 375
    invoke-virtual {v15}, Landroid/os/PatternMatcher;->getPath()Ljava/lang/String;

    #@e3
    move-result-object v21

    #@e4
    invoke-virtual {v15}, Landroid/os/PatternMatcher;->getType()I

    #@e7
    move-result v22

    #@e8
    move-object/from16 v0, v21

    #@ea
    move/from16 v1, v22

    #@ec
    invoke-virtual {v11, v0, v1}, Landroid/content/IntentFilter;->addDataPath(Ljava/lang/String;I)V

    #@ef
    .line 383
    .end local v5           #aIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/IntentFilter$AuthorityEntry;>;"
    .end local v15           #p:Landroid/os/PatternMatcher;
    .end local v16           #pIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/os/PatternMatcher;>;"
    .end local v17           #path:Ljava/lang/String;
    :cond_ef
    if-eqz v11, :cond_16f

    #@f1
    .line 384
    move-object/from16 v0, p0

    #@f3
    iget-object v0, v0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@f5
    move-object/from16 v21, v0

    #@f7
    invoke-static/range {v21 .. v21}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->access$100(Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;)Ljava/util/List;

    #@fa
    move-result-object v21

    #@fb
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    #@fe
    move-result v3

    #@ff
    .line 385
    .local v3, N:I
    new-array v0, v3, [Landroid/content/ComponentName;

    #@101
    move-object/from16 v20, v0

    #@103
    .line 386
    .local v20, set:[Landroid/content/ComponentName;
    const/4 v6, 0x0

    #@104
    .line 387
    .local v6, bestMatch:I
    const/4 v12, 0x0

    #@105
    .local v12, i:I
    :goto_105
    if-ge v12, v3, :cond_15e

    #@107
    .line 388
    move-object/from16 v0, p0

    #@109
    iget-object v0, v0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@10b
    move-object/from16 v21, v0

    #@10d
    invoke-static/range {v21 .. v21}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->access$100(Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;)Ljava/util/List;

    #@110
    move-result-object v21

    #@111
    move-object/from16 v0, v21

    #@113
    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@116
    move-result-object v21

    #@117
    check-cast v21, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;

    #@119
    move-object/from16 v0, v21

    #@11b
    iget-object v0, v0, Lcom/android/internal/app/ResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    #@11d
    move-object/from16 v19, v0

    #@11f
    .line 389
    .local v19, r:Landroid/content/pm/ResolveInfo;
    new-instance v21, Landroid/content/ComponentName;

    #@121
    move-object/from16 v0, v19

    #@123
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@125
    move-object/from16 v22, v0

    #@127
    move-object/from16 v0, v22

    #@129
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@12b
    move-object/from16 v22, v0

    #@12d
    move-object/from16 v0, v19

    #@12f
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@131
    move-object/from16 v23, v0

    #@133
    move-object/from16 v0, v23

    #@135
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@137
    move-object/from16 v23, v0

    #@139
    invoke-direct/range {v21 .. v23}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@13c
    aput-object v21, v20, v12

    #@13e
    .line 391
    move-object/from16 v0, v19

    #@140
    iget v0, v0, Landroid/content/pm/ResolveInfo;->match:I

    #@142
    move/from16 v21, v0

    #@144
    move/from16 v0, v21

    #@146
    if-le v0, v6, :cond_14c

    #@148
    move-object/from16 v0, v19

    #@14a
    iget v6, v0, Landroid/content/pm/ResolveInfo;->match:I

    #@14c
    .line 387
    :cond_14c
    add-int/lit8 v12, v12, 0x1

    #@14e
    goto :goto_105

    #@14f
    .line 340
    .end local v3           #N:I
    .end local v6           #bestMatch:I
    .end local v12           #i:I
    .end local v19           #r:Landroid/content/pm/ResolveInfo;
    .end local v20           #set:[Landroid/content/ComponentName;
    .restart local v14       #mimeType:Ljava/lang/String;
    :catch_14f
    move-exception v10

    #@150
    .line 341
    .local v10, e:Landroid/content/IntentFilter$MalformedMimeTypeException;
    const-string v21, "ResolverActivity"

    #@152
    move-object/from16 v0, v21

    #@154
    invoke-static {v0, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@157
    .line 342
    const/4 v11, 0x0

    #@158
    goto/16 :goto_58

    #@15a
    .line 363
    .end local v10           #e:Landroid/content/IntentFilter$MalformedMimeTypeException;
    .end local v14           #mimeType:Ljava/lang/String;
    .restart local v4       #a:Landroid/content/IntentFilter$AuthorityEntry;
    .restart local v5       #aIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/IntentFilter$AuthorityEntry;>;"
    .restart local v18       #port:I
    :cond_15a
    const/16 v21, 0x0

    #@15c
    goto/16 :goto_b3

    #@15e
    .line 393
    .end local v4           #a:Landroid/content/IntentFilter$AuthorityEntry;
    .end local v5           #aIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/IntentFilter$AuthorityEntry;>;"
    .end local v18           #port:I
    .restart local v3       #N:I
    .restart local v6       #bestMatch:I
    .restart local v12       #i:I
    .restart local v20       #set:[Landroid/content/ComponentName;
    :cond_15e
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/app/ResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@161
    move-result-object v21

    #@162
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@165
    move-result-object v22

    #@166
    move-object/from16 v0, v21

    #@168
    move-object/from16 v1, v20

    #@16a
    move-object/from16 v2, v22

    #@16c
    invoke-virtual {v0, v11, v6, v1, v2}, Landroid/content/pm/PackageManager;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    #@16f
    .line 398
    .end local v3           #N:I
    .end local v6           #bestMatch:I
    .end local v7           #cat:I
    .end local v8           #categories:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .end local v9           #data:Landroid/net/Uri;
    .end local v11           #filter:Landroid/content/IntentFilter;
    .end local v12           #i:I
    .end local v20           #set:[Landroid/content/ComponentName;
    :cond_16f
    if-eqz p2, :cond_178

    #@171
    .line 401
    :try_start_171
    move-object/from16 v0, p0

    #@173
    move-object/from16 v1, p2

    #@175
    invoke-virtual {v0, v1}, Lcom/android/internal/app/ResolverActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_178
    .catch Landroid/content/ActivityNotFoundException; {:try_start_171 .. :try_end_178} :catch_179

    #@178
    .line 409
    :cond_178
    :goto_178
    return-void

    #@179
    .line 403
    :catch_179
    move-exception v10

    #@17a
    .line 404
    .local v10, e:Landroid/content/ActivityNotFoundException;
    const-string v21, "ResolverActivity"

    #@17c
    const-string v22, "Activity Not Found"

    #@17e
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@181
    .line 405
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/app/ResolverActivity;->getResources()Landroid/content/res/Resources;

    #@184
    move-result-object v21

    #@185
    const v22, 0x10403fa

    #@188
    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@18b
    move-result-object v21

    #@18c
    const/16 v22, 0x0

    #@18e
    move-object/from16 v0, p0

    #@190
    move-object/from16 v1, v21

    #@192
    move/from16 v2, v22

    #@194
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@197
    move-result-object v21

    #@198
    invoke-virtual/range {v21 .. v21}, Landroid/widget/Toast;->show()V

    #@19b
    goto :goto_178
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x0

    #@1
    .line 289
    iget-boolean v2, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysUseOption:Z

    #@3
    if-eqz v2, :cond_21

    #@5
    .line 290
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@7
    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemPosition()I

    #@a
    move-result v0

    #@b
    .line 293
    .local v0, checkedPos:I
    const/4 v2, -0x1

    #@c
    if-eq v0, v2, :cond_f

    #@e
    const/4 v1, 0x1

    #@f
    .line 294
    .local v1, enabled:Z
    :cond_f
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysButton:Landroid/widget/Button;

    #@11
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    #@14
    .line 295
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mOnceButton:Landroid/widget/Button;

    #@16
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    #@19
    .line 296
    if-eqz v1, :cond_20

    #@1b
    .line 297
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@1d
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    #@20
    .line 302
    .end local v0           #checkedPos:I
    .end local v1           #enabled:Z
    :cond_20
    :goto_20
    return-void

    #@21
    .line 300
    :cond_21
    invoke-virtual {p0, p3, v1}, Lcom/android/internal/app/ResolverActivity;->startSelected(IZ)V

    #@24
    goto :goto_20
.end method

.method protected onRestart()V
    .registers 4

    #@0
    .prologue
    .line 239
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onRestart()V

    #@3
    .line 240
    iget-boolean v0, p0, Lcom/android/internal/app/ResolverActivity;->mRegistered:Z

    #@5
    if-nez v0, :cond_14

    #@7
    .line 241
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@9
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getMainLooper()Landroid/os/Looper;

    #@c
    move-result-object v1

    #@d
    const/4 v2, 0x0

    #@e
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    #@11
    .line 242
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Lcom/android/internal/app/ResolverActivity;->mRegistered:Z

    #@14
    .line 244
    :cond_14
    iget-object v0, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->handlePackagesChanged()V

    #@19
    .line 245
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 273
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 274
    iget-boolean v2, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysUseOption:Z

    #@5
    if-eqz v2, :cond_22

    #@7
    .line 275
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@9
    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemPosition()I

    #@c
    move-result v0

    #@d
    .line 278
    .local v0, checkedPos:I
    const/4 v2, -0x1

    #@e
    if-eq v0, v2, :cond_23

    #@10
    const/4 v1, 0x1

    #@11
    .line 279
    .local v1, enabled:Z
    :goto_11
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mAlwaysButton:Landroid/widget/Button;

    #@13
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    #@16
    .line 280
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mOnceButton:Landroid/widget/Button;

    #@18
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    #@1b
    .line 281
    if-eqz v1, :cond_22

    #@1d
    .line 282
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mGrid:Landroid/widget/ListView;

    #@1f
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setSelection(I)V

    #@22
    .line 285
    .end local v0           #checkedPos:I
    .end local v1           #enabled:Z
    :cond_22
    return-void

    #@23
    .line 278
    .restart local v0       #checkedPos:I
    :cond_23
    const/4 v1, 0x0

    #@24
    goto :goto_11
.end method

.method protected onStop()V
    .registers 4

    #@0
    .prologue
    .line 249
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onStop()V

    #@3
    .line 250
    iget-boolean v1, p0, Lcom/android/internal/app/ResolverActivity;->mRegistered:Z

    #@5
    if-eqz v1, :cond_f

    #@7
    .line 251
    iget-object v1, p0, Lcom/android/internal/app/ResolverActivity;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    #@c
    .line 252
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Lcom/android/internal/app/ResolverActivity;->mRegistered:Z

    #@f
    .line 254
    :cond_f
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->getIntent()Landroid/content/Intent;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    #@16
    move-result v1

    #@17
    const/high16 v2, 0x1000

    #@19
    and-int/2addr v1, v2

    #@1a
    if-eqz v1, :cond_34

    #@1c
    .line 263
    const-string/jumbo v1, "keyguard"

    #@1f
    invoke-virtual {p0, v1}, Lcom/android/internal/app/ResolverActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Landroid/app/KeyguardManager;

    #@25
    .line 265
    .local v0, km:Landroid/app/KeyguardManager;
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->isChangingConfigurations()Z

    #@28
    move-result v1

    #@29
    if-nez v1, :cond_34

    #@2b
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_34

    #@31
    .line 266
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->finish()V

    #@34
    .line 269
    .end local v0           #km:Landroid/app/KeyguardManager;
    :cond_34
    return-void
.end method

.method showAppDetails(Landroid/content/pm/ResolveInfo;)V
    .registers 7
    .parameter "ri"

    #@0
    .prologue
    .line 412
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@5
    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    #@7
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@a
    move-result-object v1

    #@b
    const-string/jumbo v2, "package"

    #@e
    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@10
    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@12
    const/4 v4, 0x0

    #@13
    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@1a
    move-result-object v1

    #@1b
    const/high16 v2, 0x8

    #@1d
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@20
    move-result-object v0

    #@21
    .line 415
    .local v0, in:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/android/internal/app/ResolverActivity;->startActivity(Landroid/content/Intent;)V

    #@24
    .line 416
    return-void
.end method

.method startSelected(IZ)V
    .registers 6
    .parameter "which"
    .parameter "always"

    #@0
    .prologue
    .line 311
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@2
    invoke-virtual {v2, p1}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->resolveInfoForPosition(I)Landroid/content/pm/ResolveInfo;

    #@5
    move-result-object v1

    #@6
    .line 312
    .local v1, ri:Landroid/content/pm/ResolveInfo;
    iget-object v2, p0, Lcom/android/internal/app/ResolverActivity;->mAdapter:Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;

    #@8
    invoke-virtual {v2, p1}, Lcom/android/internal/app/ResolverActivity$ResolveListAdapter;->intentForPosition(I)Landroid/content/Intent;

    #@b
    move-result-object v0

    #@c
    .line 313
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v1, v0, p2}, Lcom/android/internal/app/ResolverActivity;->onIntentSelected(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;Z)V

    #@f
    .line 314
    invoke-virtual {p0}, Lcom/android/internal/app/ResolverActivity;->finish()V

    #@12
    .line 315
    return-void
.end method
