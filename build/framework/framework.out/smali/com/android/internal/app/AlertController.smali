.class public Lcom/android/internal/app/AlertController;
.super Ljava/lang/Object;
.source "AlertController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/AlertController$AlertParams;,
        Lcom/android/internal/app/AlertController$RecycleListView;,
        Lcom/android/internal/app/AlertController$ButtonHandler;
    }
.end annotation


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mAlertDialogLayout:I

.field mButtonHandler:Landroid/view/View$OnClickListener;

.field private mButtonNegative:Landroid/widget/Button;

.field private mButtonNegativeMessage:Landroid/os/Message;

.field private mButtonNegativeText:Ljava/lang/CharSequence;

.field private mButtonNeutral:Landroid/widget/Button;

.field private mButtonNeutralMessage:Landroid/os/Message;

.field private mButtonNeutralText:Ljava/lang/CharSequence;

.field private mButtonPositive:Landroid/widget/Button;

.field private mButtonPositiveMessage:Landroid/os/Message;

.field private mButtonPositiveText:Ljava/lang/CharSequence;

.field private mCheckedItem:I

.field private final mContext:Landroid/content/Context;

.field private mCustomTitleView:Landroid/view/View;

.field private final mDialogInterface:Landroid/content/DialogInterface;

.field private mForceInverseBackground:Z

.field private mHandler:Landroid/os/Handler;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIconId:I

.field private mIconView:Landroid/widget/ImageView;

.field private mListItemLayout:I

.field private mListLayout:I

.field private mListView:Landroid/widget/ListView;

.field private mMessage:Ljava/lang/CharSequence;

.field private mMessageView:Landroid/widget/TextView;

.field private mMultiChoiceItemLayout:I

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSingleChoiceItemLayout:I

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleView:Landroid/widget/TextView;

.field private mView:Landroid/view/View;

.field private mViewSpacingBottom:I

.field private mViewSpacingLeft:I

.field private mViewSpacingRight:I

.field private mViewSpacingSpecified:Z

.field private mViewSpacingTop:I

.field private final mWindow:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V
    .registers 9
    .parameter "context"
    .parameter "di"
    .parameter "window"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    .line 182
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 81
    iput-boolean v4, p0, Lcom/android/internal/app/AlertController;->mViewSpacingSpecified:Z

    #@7
    .line 103
    iput v1, p0, Lcom/android/internal/app/AlertController;->mIconId:I

    #@9
    .line 119
    iput v1, p0, Lcom/android/internal/app/AlertController;->mCheckedItem:I

    #@b
    .line 129
    new-instance v1, Lcom/android/internal/app/AlertController$1;

    #@d
    invoke-direct {v1, p0}, Lcom/android/internal/app/AlertController$1;-><init>(Lcom/android/internal/app/AlertController;)V

    #@10
    iput-object v1, p0, Lcom/android/internal/app/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    #@12
    .line 183
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mContext:Landroid/content/Context;

    #@14
    .line 184
    iput-object p2, p0, Lcom/android/internal/app/AlertController;->mDialogInterface:Landroid/content/DialogInterface;

    #@16
    .line 185
    iput-object p3, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@18
    .line 186
    new-instance v1, Lcom/android/internal/app/AlertController$ButtonHandler;

    #@1a
    invoke-direct {v1, p2}, Lcom/android/internal/app/AlertController$ButtonHandler;-><init>(Landroid/content/DialogInterface;)V

    #@1d
    iput-object v1, p0, Lcom/android/internal/app/AlertController;->mHandler:Landroid/os/Handler;

    #@1f
    .line 188
    const/4 v1, 0x0

    #@20
    sget-object v2, Lcom/android/internal/R$styleable;->AlertDialog:[I

    #@22
    const v3, 0x101005d

    #@25
    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@28
    move-result-object v0

    #@29
    .line 192
    .local v0, a:Landroid/content/res/TypedArray;
    const/16 v1, 0xa

    #@2b
    const v2, 0x1090024

    #@2e
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@31
    move-result v1

    #@32
    iput v1, p0, Lcom/android/internal/app/AlertController;->mAlertDialogLayout:I

    #@34
    .line 194
    const/16 v1, 0xb

    #@36
    const v2, 0x10900c6

    #@39
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@3c
    move-result v1

    #@3d
    iput v1, p0, Lcom/android/internal/app/AlertController;->mListLayout:I

    #@3f
    .line 197
    const/16 v1, 0xc

    #@41
    const v2, 0x1090013

    #@44
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@47
    move-result v1

    #@48
    iput v1, p0, Lcom/android/internal/app/AlertController;->mMultiChoiceItemLayout:I

    #@4a
    .line 200
    const/16 v1, 0xd

    #@4c
    const v2, 0x1090012

    #@4f
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@52
    move-result v1

    #@53
    iput v1, p0, Lcom/android/internal/app/AlertController;->mSingleChoiceItemLayout:I

    #@55
    .line 203
    const/16 v1, 0xe

    #@57
    const v2, 0x1090011

    #@5a
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5d
    move-result v1

    #@5e
    iput v1, p0, Lcom/android/internal/app/AlertController;->mListItemLayout:I

    #@60
    .line 207
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@63
    .line 208
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/app/AlertController;)Landroid/widget/Button;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/app/AlertController;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonPositiveMessage:Landroid/os/Message;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/app/AlertController;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/android/internal/app/AlertController;->mSingleChoiceItemLayout:I

    #@2
    return v0
.end method

.method static synthetic access$1100(Lcom/android/internal/app/AlertController;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/android/internal/app/AlertController;->mListItemLayout:I

    #@2
    return v0
.end method

.method static synthetic access$1202(Lcom/android/internal/app/AlertController;Landroid/widget/ListAdapter;)Landroid/widget/ListAdapter;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object p1
.end method

.method static synthetic access$1302(Lcom/android/internal/app/AlertController;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput p1, p0, Lcom/android/internal/app/AlertController;->mCheckedItem:I

    #@2
    return p1
.end method

.method static synthetic access$1402(Lcom/android/internal/app/AlertController;Landroid/widget/ListView;)Landroid/widget/ListView;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/internal/app/AlertController;)Landroid/widget/Button;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/app/AlertController;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonNegativeMessage:Landroid/os/Message;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/app/AlertController;)Landroid/widget/Button;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/app/AlertController;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonNeutralMessage:Landroid/os/Message;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/app/AlertController;)Landroid/content/DialogInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mDialogInterface:Landroid/content/DialogInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/app/AlertController;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/app/AlertController;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/android/internal/app/AlertController;->mListLayout:I

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/internal/app/AlertController;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/android/internal/app/AlertController;->mMultiChoiceItemLayout:I

    #@2
    return v0
.end method

.method static canTextInput(Landroid/view/View;)Z
    .registers 6
    .parameter "v"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 211
    invoke-virtual {p0}, Landroid/view/View;->onCheckIsTextEditor()Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_9

    #@8
    .line 229
    :goto_8
    return v2

    #@9
    .line 215
    :cond_9
    instance-of v4, p0, Landroid/view/ViewGroup;

    #@b
    if-nez v4, :cond_f

    #@d
    move v2, v3

    #@e
    .line 216
    goto :goto_8

    #@f
    :cond_f
    move-object v1, p0

    #@10
    .line 219
    check-cast v1, Landroid/view/ViewGroup;

    #@12
    .line 220
    .local v1, vg:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    #@15
    move-result v0

    #@16
    .line 221
    .local v0, i:I
    :cond_16
    if-lez v0, :cond_25

    #@18
    .line 222
    add-int/lit8 v0, v0, -0x1

    #@1a
    .line 223
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@1d
    move-result-object p0

    #@1e
    .line 224
    invoke-static {p0}, Lcom/android/internal/app/AlertController;->canTextInput(Landroid/view/View;)Z

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_16

    #@24
    goto :goto_8

    #@25
    :cond_25
    move v2, v3

    #@26
    .line 229
    goto :goto_8
.end method

.method private centerButton(Landroid/widget/Button;)V
    .registers 8
    .parameter "button"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 608
    invoke-virtual {p1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v1

    #@5
    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    #@7
    .line 609
    .local v1, params:Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, 0x1

    #@8
    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@a
    .line 610
    const/high16 v3, 0x3f00

    #@c
    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@e
    .line 611
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 612
    iget-object v3, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@13
    const v4, 0x102026a

    #@16
    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@19
    move-result-object v0

    #@1a
    .line 613
    .local v0, leftSpacer:Landroid/view/View;
    if-eqz v0, :cond_1f

    #@1c
    .line 614
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    #@1f
    .line 616
    :cond_1f
    iget-object v3, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@21
    const v4, 0x102026b

    #@24
    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@27
    move-result-object v2

    #@28
    .line 617
    .local v2, rightSpacer:Landroid/view/View;
    if-eqz v2, :cond_2d

    #@2a
    .line 618
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    #@2d
    .line 620
    :cond_2d
    return-void
.end method

.method private setBackground(Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/view/View;ZLandroid/content/res/TypedArray;ZLandroid/view/View;)V
    .registers 30
    .parameter "topPanel"
    .parameter "contentPanel"
    .parameter "customPanel"
    .parameter "hasButtons"
    .parameter "a"
    .parameter "hasTitle"
    .parameter "buttonPanel"

    #@0
    .prologue
    .line 627
    const/16 v19, 0x0

    #@2
    const v20, 0x1080478

    #@5
    move-object/from16 v0, p5

    #@7
    move/from16 v1, v19

    #@9
    move/from16 v2, v20

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@e
    move-result v9

    #@f
    .line 629
    .local v9, fullDark:I
    const/16 v19, 0x1

    #@11
    const v20, 0x1080480

    #@14
    move-object/from16 v0, p5

    #@16
    move/from16 v1, v19

    #@18
    move/from16 v2, v20

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1d
    move-result v16

    #@1e
    .line 631
    .local v16, topDark:I
    const/16 v19, 0x2

    #@20
    const v20, 0x1080475

    #@23
    move-object/from16 v0, p5

    #@25
    move/from16 v1, v19

    #@27
    move/from16 v2, v20

    #@29
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@2c
    move-result v7

    #@2d
    .line 633
    .local v7, centerDark:I
    const/16 v19, 0x3

    #@2f
    const v20, 0x1080472

    #@32
    move-object/from16 v0, p5

    #@34
    move/from16 v1, v19

    #@36
    move/from16 v2, v20

    #@38
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@3b
    move-result v4

    #@3c
    .line 635
    .local v4, bottomDark:I
    const/16 v19, 0x4

    #@3e
    const v20, 0x1080477

    #@41
    move-object/from16 v0, p5

    #@43
    move/from16 v1, v19

    #@45
    move/from16 v2, v20

    #@47
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@4a
    move-result v8

    #@4b
    .line 637
    .local v8, fullBright:I
    const/16 v19, 0x5

    #@4d
    const v20, 0x108047f

    #@50
    move-object/from16 v0, p5

    #@52
    move/from16 v1, v19

    #@54
    move/from16 v2, v20

    #@56
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@59
    move-result v15

    #@5a
    .line 639
    .local v15, topBright:I
    const/16 v19, 0x6

    #@5c
    const v20, 0x1080474

    #@5f
    move-object/from16 v0, p5

    #@61
    move/from16 v1, v19

    #@63
    move/from16 v2, v20

    #@65
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@68
    move-result v6

    #@69
    .line 641
    .local v6, centerBright:I
    const/16 v19, 0x7

    #@6b
    const v20, 0x1080471

    #@6e
    move-object/from16 v0, p5

    #@70
    move/from16 v1, v19

    #@72
    move/from16 v2, v20

    #@74
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@77
    move-result v3

    #@78
    .line 643
    .local v3, bottomBright:I
    const/16 v19, 0x8

    #@7a
    const v20, 0x1080473

    #@7d
    move-object/from16 v0, p5

    #@7f
    move/from16 v1, v19

    #@81
    move/from16 v2, v20

    #@83
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@86
    move-result v5

    #@87
    .line 655
    .local v5, bottomMedium:I
    const/16 v19, 0x4

    #@89
    move/from16 v0, v19

    #@8b
    new-array v0, v0, [Landroid/view/View;

    #@8d
    move-object/from16 v18, v0

    #@8f
    .line 656
    .local v18, views:[Landroid/view/View;
    const/16 v19, 0x4

    #@91
    move/from16 v0, v19

    #@93
    new-array v12, v0, [Z

    #@95
    .line 657
    .local v12, light:[Z
    const/4 v11, 0x0

    #@96
    .line 658
    .local v11, lastView:Landroid/view/View;
    const/4 v10, 0x0

    #@97
    .line 660
    .local v10, lastLight:Z
    const/4 v13, 0x0

    #@98
    .line 661
    .local v13, pos:I
    if-eqz p6, :cond_a2

    #@9a
    .line 662
    aput-object p1, v18, v13

    #@9c
    .line 663
    const/16 v19, 0x0

    #@9e
    aput-boolean v19, v12, v13

    #@a0
    .line 664
    add-int/lit8 v13, v13, 0x1

    #@a2
    .line 672
    :cond_a2
    invoke-virtual/range {p2 .. p2}, Landroid/widget/LinearLayout;->getVisibility()I

    #@a5
    move-result v19

    #@a6
    const/16 v20, 0x8

    #@a8
    move/from16 v0, v19

    #@aa
    move/from16 v1, v20

    #@ac
    if-ne v0, v1, :cond_b0

    #@ae
    const/16 p2, 0x0

    #@b0
    .end local p2
    :cond_b0
    aput-object p2, v18, v13

    #@b2
    .line 674
    move-object/from16 v0, p0

    #@b4
    iget-object v0, v0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@b6
    move-object/from16 v19, v0

    #@b8
    if-eqz v19, :cond_e8

    #@ba
    const/16 v19, 0x1

    #@bc
    :goto_bc
    aput-boolean v19, v12, v13

    #@be
    .line 675
    add-int/lit8 v13, v13, 0x1

    #@c0
    .line 676
    if-eqz p3, :cond_ce

    #@c2
    .line 677
    aput-object p3, v18, v13

    #@c4
    .line 678
    move-object/from16 v0, p0

    #@c6
    iget-boolean v0, v0, Lcom/android/internal/app/AlertController;->mForceInverseBackground:Z

    #@c8
    move/from16 v19, v0

    #@ca
    aput-boolean v19, v12, v13

    #@cc
    .line 679
    add-int/lit8 v13, v13, 0x1

    #@ce
    .line 681
    :cond_ce
    if-eqz p4, :cond_d6

    #@d0
    .line 682
    aput-object p7, v18, v13

    #@d2
    .line 683
    const/16 v19, 0x1

    #@d4
    aput-boolean v19, v12, v13

    #@d6
    .line 686
    :cond_d6
    const/4 v14, 0x0

    #@d7
    .line 687
    .local v14, setView:Z
    const/4 v13, 0x0

    #@d8
    :goto_d8
    move-object/from16 v0, v18

    #@da
    array-length v0, v0

    #@db
    move/from16 v19, v0

    #@dd
    move/from16 v0, v19

    #@df
    if-ge v13, v0, :cond_10e

    #@e1
    .line 688
    aget-object v17, v18, v13

    #@e3
    .line 689
    .local v17, v:Landroid/view/View;
    if-nez v17, :cond_eb

    #@e5
    .line 687
    :goto_e5
    add-int/lit8 v13, v13, 0x1

    #@e7
    goto :goto_d8

    #@e8
    .line 674
    .end local v14           #setView:Z
    .end local v17           #v:Landroid/view/View;
    :cond_e8
    const/16 v19, 0x0

    #@ea
    goto :goto_bc

    #@eb
    .line 692
    .restart local v14       #setView:Z
    .restart local v17       #v:Landroid/view/View;
    :cond_eb
    if-eqz v11, :cond_f9

    #@ed
    .line 693
    if-nez v14, :cond_101

    #@ef
    .line 694
    if-eqz v10, :cond_fe

    #@f1
    move/from16 v19, v15

    #@f3
    :goto_f3
    move/from16 v0, v19

    #@f5
    invoke-virtual {v11, v0}, Landroid/view/View;->setBackgroundResource(I)V

    #@f8
    .line 698
    :goto_f8
    const/4 v14, 0x1

    #@f9
    .line 700
    :cond_f9
    move-object/from16 v11, v17

    #@fb
    .line 701
    aget-boolean v10, v12, v13

    #@fd
    goto :goto_e5

    #@fe
    :cond_fe
    move/from16 v19, v16

    #@100
    .line 694
    goto :goto_f3

    #@101
    .line 696
    :cond_101
    if-eqz v10, :cond_10b

    #@103
    move/from16 v19, v6

    #@105
    :goto_105
    move/from16 v0, v19

    #@107
    invoke-virtual {v11, v0}, Landroid/view/View;->setBackgroundResource(I)V

    #@10a
    goto :goto_f8

    #@10b
    :cond_10b
    move/from16 v19, v7

    #@10d
    goto :goto_105

    #@10e
    .line 704
    .end local v17           #v:Landroid/view/View;
    :cond_10e
    if-eqz v11, :cond_119

    #@110
    .line 705
    if-eqz v14, :cond_16b

    #@112
    .line 710
    if-eqz v10, :cond_169

    #@114
    if-eqz p4, :cond_167

    #@116
    .end local v5           #bottomMedium:I
    :goto_116
    invoke-virtual {v11, v5}, Landroid/view/View;->setBackgroundResource(I)V

    #@119
    .line 742
    .end local v8           #fullBright:I
    :cond_119
    :goto_119
    move-object/from16 v0, p0

    #@11b
    iget-object v0, v0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@11d
    move-object/from16 v19, v0

    #@11f
    if-eqz v19, :cond_166

    #@121
    move-object/from16 v0, p0

    #@123
    iget-object v0, v0, Lcom/android/internal/app/AlertController;->mAdapter:Landroid/widget/ListAdapter;

    #@125
    move-object/from16 v19, v0

    #@127
    if-eqz v19, :cond_166

    #@129
    .line 743
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@12d
    move-object/from16 v19, v0

    #@12f
    move-object/from16 v0, p0

    #@131
    iget-object v0, v0, Lcom/android/internal/app/AlertController;->mAdapter:Landroid/widget/ListAdapter;

    #@133
    move-object/from16 v20, v0

    #@135
    invoke-virtual/range {v19 .. v20}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@138
    .line 744
    move-object/from16 v0, p0

    #@13a
    iget v0, v0, Lcom/android/internal/app/AlertController;->mCheckedItem:I

    #@13c
    move/from16 v19, v0

    #@13e
    const/16 v20, -0x1

    #@140
    move/from16 v0, v19

    #@142
    move/from16 v1, v20

    #@144
    if-le v0, v1, :cond_166

    #@146
    .line 745
    move-object/from16 v0, p0

    #@148
    iget-object v0, v0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@14a
    move-object/from16 v19, v0

    #@14c
    move-object/from16 v0, p0

    #@14e
    iget v0, v0, Lcom/android/internal/app/AlertController;->mCheckedItem:I

    #@150
    move/from16 v20, v0

    #@152
    const/16 v21, 0x1

    #@154
    invoke-virtual/range {v19 .. v21}, Landroid/widget/ListView;->setItemChecked(IZ)V

    #@157
    .line 746
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@15b
    move-object/from16 v19, v0

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget v0, v0, Lcom/android/internal/app/AlertController;->mCheckedItem:I

    #@161
    move/from16 v20, v0

    #@163
    invoke-virtual/range {v19 .. v20}, Landroid/widget/ListView;->setSelection(I)V

    #@166
    .line 749
    :cond_166
    return-void

    #@167
    .restart local v5       #bottomMedium:I
    .restart local v8       #fullBright:I
    :cond_167
    move v5, v3

    #@168
    .line 710
    goto :goto_116

    #@169
    :cond_169
    move v5, v4

    #@16a
    goto :goto_116

    #@16b
    .line 713
    :cond_16b
    if-eqz v10, :cond_171

    #@16d
    .end local v8           #fullBright:I
    :goto_16d
    invoke-virtual {v11, v8}, Landroid/view/View;->setBackgroundResource(I)V

    #@170
    goto :goto_119

    #@171
    .restart local v8       #fullBright:I
    :cond_171
    move v8, v9

    #@172
    goto :goto_16d
.end method

.method private setupButtons()Z
    .registers 13

    #@0
    .prologue
    const/16 v11, 0x8

    #@2
    const/4 v10, -0x1

    #@3
    const/4 v8, 0x0

    #@4
    .line 537
    const/4 v2, 0x1

    #@5
    .line 538
    .local v2, BIT_BUTTON_POSITIVE:I
    const/4 v0, 0x2

    #@6
    .line 539
    .local v0, BIT_BUTTON_NEGATIVE:I
    const/4 v1, 0x4

    #@7
    .line 540
    .local v1, BIT_BUTTON_NEUTRAL:I
    const/4 v6, 0x0

    #@8
    .line 541
    .local v6, whichButtons:I
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@a
    const v9, 0x1020019

    #@d
    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@10
    move-result-object v7

    #@11
    check-cast v7, Landroid/widget/Button;

    #@13
    iput-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@15
    .line 542
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@17
    iget-object v9, p0, Lcom/android/internal/app/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    #@19
    invoke-virtual {v7, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@1c
    .line 544
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositiveText:Ljava/lang/CharSequence;

    #@1e
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@21
    move-result v7

    #@22
    if-eqz v7, :cond_b0

    #@24
    .line 545
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@26
    invoke-virtual {v7, v11}, Landroid/widget/Button;->setVisibility(I)V

    #@29
    .line 552
    :goto_29
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@2b
    const v9, 0x102001a

    #@2e
    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@31
    move-result-object v7

    #@32
    check-cast v7, Landroid/widget/Button;

    #@34
    iput-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@36
    .line 553
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@38
    iget-object v9, p0, Lcom/android/internal/app/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    #@3a
    invoke-virtual {v7, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@3d
    .line 555
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegativeText:Ljava/lang/CharSequence;

    #@3f
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@42
    move-result v7

    #@43
    if-eqz v7, :cond_bf

    #@45
    .line 556
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@47
    invoke-virtual {v7, v11}, Landroid/widget/Button;->setVisibility(I)V

    #@4a
    .line 563
    :goto_4a
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@4c
    const v9, 0x102001b

    #@4f
    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@52
    move-result-object v7

    #@53
    check-cast v7, Landroid/widget/Button;

    #@55
    iput-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@57
    .line 564
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@59
    iget-object v9, p0, Lcom/android/internal/app/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    #@5b
    invoke-virtual {v7, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@5e
    .line 566
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutralText:Ljava/lang/CharSequence;

    #@60
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@63
    move-result v7

    #@64
    if-eqz v7, :cond_ce

    #@66
    .line 567
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@68
    invoke-virtual {v7, v11}, Landroid/widget/Button;->setVisibility(I)V

    #@6b
    .line 574
    :goto_6b
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mContext:Landroid/content/Context;

    #@6d
    invoke-static {v7}, Lcom/android/internal/app/AlertController;->shouldCenterSingleButton(Landroid/content/Context;)Z

    #@70
    move-result v7

    #@71
    if-eqz v7, :cond_7a

    #@73
    .line 579
    if-ne v6, v2, :cond_dc

    #@75
    .line 580
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@77
    invoke-direct {p0, v7}, Lcom/android/internal/app/AlertController;->centerButton(Landroid/widget/Button;)V

    #@7a
    .line 589
    :cond_7a
    :goto_7a
    const/4 v7, 0x2

    #@7b
    if-le v6, v7, :cond_ac

    #@7d
    if-eq v6, v1, :cond_ac

    #@7f
    .line 590
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@81
    invoke-virtual {v7}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@84
    move-result-object v5

    #@85
    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    #@87
    .line 591
    .local v5, mButtonPositiveParams:Landroid/widget/LinearLayout$LayoutParams;
    iput v10, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@89
    .line 592
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@8b
    invoke-virtual {v7, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@8e
    .line 594
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@90
    invoke-virtual {v7}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@93
    move-result-object v3

    #@94
    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    #@96
    .line 595
    .local v3, mButtonNegativeParams:Landroid/widget/LinearLayout$LayoutParams;
    iput v10, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@98
    .line 596
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@9a
    invoke-virtual {v7, v3}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@9d
    .line 598
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@9f
    invoke-virtual {v7}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@a2
    move-result-object v4

    #@a3
    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    #@a5
    .line 599
    .local v4, mButtonNeutralParams:Landroid/widget/LinearLayout$LayoutParams;
    iput v10, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@a7
    .line 600
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@a9
    invoke-virtual {v7, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@ac
    .line 604
    .end local v3           #mButtonNegativeParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v4           #mButtonNeutralParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v5           #mButtonPositiveParams:Landroid/widget/LinearLayout$LayoutParams;
    :cond_ac
    if-eqz v6, :cond_ec

    #@ae
    const/4 v7, 0x1

    #@af
    :goto_af
    return v7

    #@b0
    .line 547
    :cond_b0
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@b2
    iget-object v9, p0, Lcom/android/internal/app/AlertController;->mButtonPositiveText:Ljava/lang/CharSequence;

    #@b4
    invoke-virtual {v7, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@b7
    .line 548
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@b9
    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    #@bc
    .line 549
    or-int/2addr v6, v2

    #@bd
    goto/16 :goto_29

    #@bf
    .line 558
    :cond_bf
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@c1
    iget-object v9, p0, Lcom/android/internal/app/AlertController;->mButtonNegativeText:Ljava/lang/CharSequence;

    #@c3
    invoke-virtual {v7, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@c6
    .line 559
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@c8
    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    #@cb
    .line 560
    or-int/2addr v6, v0

    #@cc
    goto/16 :goto_4a

    #@ce
    .line 569
    :cond_ce
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@d0
    iget-object v9, p0, Lcom/android/internal/app/AlertController;->mButtonNeutralText:Ljava/lang/CharSequence;

    #@d2
    invoke-virtual {v7, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@d5
    .line 570
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@d7
    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    #@da
    .line 571
    or-int/2addr v6, v1

    #@db
    goto :goto_6b

    #@dc
    .line 581
    :cond_dc
    if-ne v6, v0, :cond_e4

    #@de
    .line 582
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@e0
    invoke-direct {p0, v7}, Lcom/android/internal/app/AlertController;->centerButton(Landroid/widget/Button;)V

    #@e3
    goto :goto_7a

    #@e4
    .line 583
    :cond_e4
    if-ne v6, v1, :cond_7a

    #@e6
    .line 584
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@e8
    invoke-direct {p0, v7}, Lcom/android/internal/app/AlertController;->centerButton(Landroid/widget/Button;)V

    #@eb
    goto :goto_7a

    #@ec
    :cond_ec
    move v7, v8

    #@ed
    .line 604
    goto :goto_af
.end method

.method private setupContent(Landroid/widget/LinearLayout;)V
    .registers 8
    .parameter "contentPanel"

    #@0
    .prologue
    const v5, 0x102026d

    #@3
    const/16 v4, 0x8

    #@5
    const/4 v3, 0x0

    #@6
    const/4 v2, -0x1

    #@7
    .line 510
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@9
    invoke-virtual {v0, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/widget/ScrollView;

    #@f
    iput-object v0, p0, Lcom/android/internal/app/AlertController;->mScrollView:Landroid/widget/ScrollView;

    #@11
    .line 511
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mScrollView:Landroid/widget/ScrollView;

    #@13
    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setFocusable(Z)V

    #@16
    .line 514
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@18
    const v1, 0x102000b

    #@1b
    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/widget/TextView;

    #@21
    iput-object v0, p0, Lcom/android/internal/app/AlertController;->mMessageView:Landroid/widget/TextView;

    #@23
    .line 515
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mMessageView:Landroid/widget/TextView;

    #@25
    if-nez v0, :cond_28

    #@27
    .line 534
    :goto_27
    return-void

    #@28
    .line 519
    :cond_28
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mMessage:Ljava/lang/CharSequence;

    #@2a
    if-eqz v0, :cond_34

    #@2c
    .line 520
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mMessageView:Landroid/widget/TextView;

    #@2e
    iget-object v1, p0, Lcom/android/internal/app/AlertController;->mMessage:Ljava/lang/CharSequence;

    #@30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@33
    goto :goto_27

    #@34
    .line 522
    :cond_34
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mMessageView:Landroid/widget/TextView;

    #@36
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    #@39
    .line 523
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mScrollView:Landroid/widget/ScrollView;

    #@3b
    iget-object v1, p0, Lcom/android/internal/app/AlertController;->mMessageView:Landroid/widget/TextView;

    #@3d
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    #@40
    .line 525
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@42
    if-eqz v0, :cond_62

    #@44
    .line 526
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@46
    invoke-virtual {v0, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@49
    move-result-object v0

    #@4a
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    #@4d
    .line 527
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@4f
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    #@51
    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@54
    invoke-virtual {p1, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@57
    .line 529
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@59
    const/high16 v1, 0x3f80

    #@5b
    invoke-direct {v0, v2, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@5e
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@61
    goto :goto_27

    #@62
    .line 531
    :cond_62
    invoke-virtual {p1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@65
    goto :goto_27
.end method

.method private setupTitle(Landroid/widget/LinearLayout;)Z
    .registers 12
    .parameter "topPanel"

    #@0
    .prologue
    const v6, 0x1020266

    #@3
    const/4 v0, 0x0

    #@4
    const/16 v9, 0x8

    #@6
    .line 455
    const/4 v1, 0x1

    #@7
    .line 457
    .local v1, hasTitle:Z
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mCustomTitleView:Landroid/view/View;

    #@9
    if-eqz v4, :cond_21

    #@b
    .line 459
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    #@d
    const/4 v4, -0x1

    #@e
    const/4 v5, -0x2

    #@f
    invoke-direct {v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@12
    .line 462
    .local v2, lp:Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mCustomTitleView:Landroid/view/View;

    #@14
    invoke-virtual {p1, v4, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@17
    .line 465
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@19
    invoke-virtual {v4, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@1c
    move-result-object v3

    #@1d
    .line 466
    .local v3, titleTemplate:Landroid/view/View;
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    #@20
    .line 506
    .end local v2           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v3           #titleTemplate:Landroid/view/View;
    :cond_20
    :goto_20
    return v1

    #@21
    .line 468
    :cond_21
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mTitle:Ljava/lang/CharSequence;

    #@23
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@26
    move-result v4

    #@27
    if-nez v4, :cond_2a

    #@29
    const/4 v0, 0x1

    #@2a
    .line 470
    .local v0, hasTextTitle:Z
    :cond_2a
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@2c
    const v5, 0x1020006

    #@2f
    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@32
    move-result-object v4

    #@33
    check-cast v4, Landroid/widget/ImageView;

    #@35
    iput-object v4, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@37
    .line 471
    if-eqz v0, :cond_8c

    #@39
    .line 473
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@3b
    const v5, 0x1020267

    #@3e
    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@41
    move-result-object v4

    #@42
    check-cast v4, Landroid/widget/TextView;

    #@44
    iput-object v4, p0, Lcom/android/internal/app/AlertController;->mTitleView:Landroid/widget/TextView;

    #@46
    .line 475
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mTitleView:Landroid/widget/TextView;

    #@48
    iget-object v5, p0, Lcom/android/internal/app/AlertController;->mTitle:Ljava/lang/CharSequence;

    #@4a
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@4d
    .line 481
    iget v4, p0, Lcom/android/internal/app/AlertController;->mIconId:I

    #@4f
    if-lez v4, :cond_59

    #@51
    .line 482
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@53
    iget v5, p0, Lcom/android/internal/app/AlertController;->mIconId:I

    #@55
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    #@58
    goto :goto_20

    #@59
    .line 483
    :cond_59
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mIcon:Landroid/graphics/drawable/Drawable;

    #@5b
    if-eqz v4, :cond_65

    #@5d
    .line 484
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@5f
    iget-object v5, p0, Lcom/android/internal/app/AlertController;->mIcon:Landroid/graphics/drawable/Drawable;

    #@61
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@64
    goto :goto_20

    #@65
    .line 485
    :cond_65
    iget v4, p0, Lcom/android/internal/app/AlertController;->mIconId:I

    #@67
    if-nez v4, :cond_20

    #@69
    .line 490
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mTitleView:Landroid/widget/TextView;

    #@6b
    iget-object v5, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@6d
    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingLeft()I

    #@70
    move-result v5

    #@71
    iget-object v6, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@73
    invoke-virtual {v6}, Landroid/widget/ImageView;->getPaddingTop()I

    #@76
    move-result v6

    #@77
    iget-object v7, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@79
    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingRight()I

    #@7c
    move-result v7

    #@7d
    iget-object v8, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@7f
    invoke-virtual {v8}, Landroid/widget/ImageView;->getPaddingBottom()I

    #@82
    move-result v8

    #@83
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    #@86
    .line 494
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@88
    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    #@8b
    goto :goto_20

    #@8c
    .line 499
    :cond_8c
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@8e
    invoke-virtual {v4, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@91
    move-result-object v3

    #@92
    .line 500
    .restart local v3       #titleTemplate:Landroid/view/View;
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    #@95
    .line 501
    iget-object v4, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@97
    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    #@9a
    .line 502
    invoke-virtual {p1, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@9d
    .line 503
    const/4 v1, 0x0

    #@9e
    goto :goto_20
.end method

.method private setupView()V
    .registers 15

    #@0
    .prologue
    .line 403
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@2
    const v10, 0x102026c

    #@5
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Landroid/widget/LinearLayout;

    #@b
    .line 404
    .local v2, contentPanel:Landroid/widget/LinearLayout;
    invoke-direct {p0, v2}, Lcom/android/internal/app/AlertController;->setupContent(Landroid/widget/LinearLayout;)V

    #@e
    .line 405
    invoke-direct {p0}, Lcom/android/internal/app/AlertController;->setupButtons()Z

    #@11
    move-result v4

    #@12
    .line 407
    .local v4, hasButtons:Z
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@14
    const v10, 0x1020265

    #@17
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Landroid/widget/LinearLayout;

    #@1d
    .line 408
    .local v1, topPanel:Landroid/widget/LinearLayout;
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mContext:Landroid/content/Context;

    #@1f
    const/4 v10, 0x0

    #@20
    sget-object v11, Lcom/android/internal/R$styleable;->AlertDialog:[I

    #@22
    const v12, 0x101005d

    #@25
    const/4 v13, 0x0

    #@26
    invoke-virtual {v0, v10, v11, v12, v13}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@29
    move-result-object v5

    #@2a
    .line 410
    .local v5, a:Landroid/content/res/TypedArray;
    invoke-direct {p0, v1}, Lcom/android/internal/app/AlertController;->setupTitle(Landroid/widget/LinearLayout;)Z

    #@2d
    move-result v6

    #@2e
    .line 412
    .local v6, hasTitle:Z
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@30
    const v10, 0x1020269

    #@33
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@36
    move-result-object v7

    #@37
    .line 413
    .local v7, buttonPanel:Landroid/view/View;
    if-nez v4, :cond_44

    #@39
    .line 414
    const/16 v0, 0x8

    #@3b
    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    #@3e
    .line 415
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@40
    const/4 v10, 0x1

    #@41
    invoke-virtual {v0, v10}, Landroid/view/Window;->setCloseOnTouchOutsideIfNotSet(Z)V

    #@44
    .line 418
    :cond_44
    const/4 v3, 0x0

    #@45
    .line 419
    .local v3, customPanel:Landroid/widget/FrameLayout;
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mView:Landroid/view/View;

    #@47
    if-eqz v0, :cond_ad

    #@49
    .line 420
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@4b
    const v10, 0x102026e

    #@4e
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@51
    move-result-object v3

    #@52
    .end local v3           #customPanel:Landroid/widget/FrameLayout;
    check-cast v3, Landroid/widget/FrameLayout;

    #@54
    .line 421
    .restart local v3       #customPanel:Landroid/widget/FrameLayout;
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@56
    const v10, 0x102002b

    #@59
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@5c
    move-result-object v8

    #@5d
    check-cast v8, Landroid/widget/FrameLayout;

    #@5f
    .line 422
    .local v8, custom:Landroid/widget/FrameLayout;
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mView:Landroid/view/View;

    #@61
    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    #@63
    const/4 v11, -0x1

    #@64
    const/4 v12, -0x1

    #@65
    invoke-direct {v10, v11, v12}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@68
    invoke-virtual {v8, v0, v10}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@6b
    .line 423
    iget-boolean v0, p0, Lcom/android/internal/app/AlertController;->mViewSpacingSpecified:Z

    #@6d
    if-eqz v0, :cond_7a

    #@6f
    .line 424
    iget v0, p0, Lcom/android/internal/app/AlertController;->mViewSpacingLeft:I

    #@71
    iget v10, p0, Lcom/android/internal/app/AlertController;->mViewSpacingTop:I

    #@73
    iget v11, p0, Lcom/android/internal/app/AlertController;->mViewSpacingRight:I

    #@75
    iget v12, p0, Lcom/android/internal/app/AlertController;->mViewSpacingBottom:I

    #@77
    invoke-virtual {v8, v0, v10, v11, v12}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    #@7a
    .line 427
    :cond_7a
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@7c
    if-eqz v0, :cond_87

    #@7e
    .line 428
    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@81
    move-result-object v0

    #@82
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    #@84
    const/4 v10, 0x0

    #@85
    iput v10, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@87
    .line 437
    .end local v8           #custom:Landroid/widget/FrameLayout;
    :cond_87
    :goto_87
    if-eqz v6, :cond_a5

    #@89
    .line 438
    const/4 v9, 0x0

    #@8a
    .line 439
    .local v9, divider:Landroid/view/View;
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mMessage:Ljava/lang/CharSequence;

    #@8c
    if-nez v0, :cond_96

    #@8e
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mView:Landroid/view/View;

    #@90
    if-nez v0, :cond_96

    #@92
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@94
    if-eqz v0, :cond_bc

    #@96
    .line 440
    :cond_96
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@98
    const v10, 0x1020268

    #@9b
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@9e
    move-result-object v9

    #@9f
    .line 445
    :goto_9f
    if-eqz v9, :cond_a5

    #@a1
    .line 446
    const/4 v0, 0x0

    #@a2
    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    #@a5
    .end local v9           #divider:Landroid/view/View;
    :cond_a5
    move-object v0, p0

    #@a6
    .line 450
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/app/AlertController;->setBackground(Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/view/View;ZLandroid/content/res/TypedArray;ZLandroid/view/View;)V

    #@a9
    .line 451
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    #@ac
    .line 452
    return-void

    #@ad
    .line 431
    :cond_ad
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@af
    const v10, 0x102026e

    #@b2
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@b5
    move-result-object v0

    #@b6
    const/16 v10, 0x8

    #@b8
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    #@bb
    goto :goto_87

    #@bc
    .line 442
    .restart local v9       #divider:Landroid/view/View;
    :cond_bc
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@be
    const v10, 0x102026f

    #@c1
    invoke-virtual {v0, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@c4
    move-result-object v9

    #@c5
    goto :goto_9f
.end method

.method private static shouldCenterSingleButton(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 176
    new-instance v0, Landroid/util/TypedValue;

    #@3
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@6
    .line 177
    .local v0, outValue:Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@9
    move-result-object v2

    #@a
    const v3, 0x10103d7

    #@d
    invoke-virtual {v2, v3, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@10
    .line 179
    iget v2, v0, Landroid/util/TypedValue;->data:I

    #@12
    if-eqz v2, :cond_15

    #@14
    :goto_14
    return v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method


# virtual methods
.method public getButton(I)Landroid/widget/Button;
    .registers 3
    .parameter "whichButton"

    #@0
    .prologue
    .line 380
    packed-switch p1, :pswitch_data_e

    #@3
    .line 388
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 382
    :pswitch_5
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonPositive:Landroid/widget/Button;

    #@7
    goto :goto_4

    #@8
    .line 384
    :pswitch_8
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonNegative:Landroid/widget/Button;

    #@a
    goto :goto_4

    #@b
    .line 386
    :pswitch_b
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mButtonNeutral:Landroid/widget/Button;

    #@d
    goto :goto_4

    #@e
    .line 380
    :pswitch_data_e
    .packed-switch -0x3
        :pswitch_b
        :pswitch_8
        :pswitch_5
    .end packed-switch
.end method

.method public getIconAttributeResId(I)I
    .registers 5
    .parameter "attrId"

    #@0
    .prologue
    .line 366
    new-instance v0, Landroid/util/TypedValue;

    #@2
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@5
    .line 367
    .local v0, out:Landroid/util/TypedValue;
    iget-object v1, p0, Lcom/android/internal/app/AlertController;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@a
    move-result-object v1

    #@b
    const/4 v2, 0x1

    #@c
    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@f
    .line 368
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    #@11
    return v1
.end method

.method public getListView()Landroid/widget/ListView;
    .registers 2

    #@0
    .prologue
    .line 376
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mListView:Landroid/widget/ListView;

    #@2
    return-object v0
.end method

.method public installContent()V
    .registers 2

    #@0
    .prologue
    .line 234
    iget v0, p0, Lcom/android/internal/app/AlertController;->mAlertDialogLayout:I

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/app/AlertController;->installContent(I)V

    #@5
    .line 235
    return-void
.end method

.method public installContent(I)V
    .registers 5
    .parameter "idRes"

    #@0
    .prologue
    const/high16 v2, 0x2

    #@2
    .line 242
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    #@8
    .line 244
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mView:Landroid/view/View;

    #@a
    if-eqz v0, :cond_14

    #@c
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mView:Landroid/view/View;

    #@e
    invoke-static {v0}, Lcom/android/internal/app/AlertController;->canTextInput(Landroid/view/View;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_19

    #@14
    .line 245
    :cond_14
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@16
    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    #@19
    .line 248
    :cond_19
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mWindow:Landroid/view/Window;

    #@1b
    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(I)V

    #@1e
    .line 249
    invoke-direct {p0}, Lcom/android/internal/app/AlertController;->setupView()V

    #@21
    .line 250
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 394
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mScrollView:Landroid/widget/ScrollView;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mScrollView:Landroid/widget/ScrollView;

    #@6
    invoke-virtual {v0, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mScrollView:Landroid/widget/ScrollView;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mScrollView:Landroid/widget/ScrollView;

    #@6
    invoke-virtual {v0, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    .registers 7
    .parameter "whichButton"
    .parameter "text"
    .parameter "listener"
    .parameter "msg"

    #@0
    .prologue
    .line 310
    if-nez p4, :cond_a

    #@2
    if-eqz p3, :cond_a

    #@4
    .line 311
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mHandler:Landroid/os/Handler;

    #@6
    invoke-virtual {v0, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object p4

    #@a
    .line 314
    :cond_a
    packed-switch p1, :pswitch_data_24

    #@d
    .line 332
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v1, "Button does not exist"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 317
    :pswitch_15
    iput-object p2, p0, Lcom/android/internal/app/AlertController;->mButtonPositiveText:Ljava/lang/CharSequence;

    #@17
    .line 318
    iput-object p4, p0, Lcom/android/internal/app/AlertController;->mButtonPositiveMessage:Landroid/os/Message;

    #@19
    .line 334
    :goto_19
    return-void

    #@1a
    .line 322
    :pswitch_1a
    iput-object p2, p0, Lcom/android/internal/app/AlertController;->mButtonNegativeText:Ljava/lang/CharSequence;

    #@1c
    .line 323
    iput-object p4, p0, Lcom/android/internal/app/AlertController;->mButtonNegativeMessage:Landroid/os/Message;

    #@1e
    goto :goto_19

    #@1f
    .line 327
    :pswitch_1f
    iput-object p2, p0, Lcom/android/internal/app/AlertController;->mButtonNeutralText:Ljava/lang/CharSequence;

    #@21
    .line 328
    iput-object p4, p0, Lcom/android/internal/app/AlertController;->mButtonNeutralMessage:Landroid/os/Message;

    #@23
    goto :goto_19

    #@24
    .line 314
    :pswitch_data_24
    .packed-switch -0x3
        :pswitch_1f
        :pswitch_1a
        :pswitch_15
    .end packed-switch
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .registers 2
    .parameter "customTitleView"

    #@0
    .prologue
    .line 264
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mCustomTitleView:Landroid/view/View;

    #@2
    .line 265
    return-void
.end method

.method public setIcon(I)V
    .registers 4
    .parameter "resId"

    #@0
    .prologue
    .line 342
    iput p1, p0, Lcom/android/internal/app/AlertController;->mIconId:I

    #@2
    .line 343
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 344
    if-lez p1, :cond_10

    #@8
    .line 345
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@a
    iget v1, p0, Lcom/android/internal/app/AlertController;->mIconId:I

    #@c
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@f
    .line 350
    :cond_f
    :goto_f
    return-void

    #@10
    .line 346
    :cond_10
    if-nez p1, :cond_f

    #@12
    .line 347
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@14
    const/16 v1, 0x8

    #@16
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@19
    goto :goto_f
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "icon"

    #@0
    .prologue
    .line 353
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mIcon:Landroid/graphics/drawable/Drawable;

    #@2
    .line 354
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@4
    if-eqz v0, :cond_f

    #@6
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mIcon:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 355
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mIconView:Landroid/widget/ImageView;

    #@c
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 357
    :cond_f
    return-void
.end method

.method public setInverseBackgroundForced(Z)V
    .registers 2
    .parameter "forceInverseBackground"

    #@0
    .prologue
    .line 372
    iput-boolean p1, p0, Lcom/android/internal/app/AlertController;->mForceInverseBackground:Z

    #@2
    .line 373
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "message"

    #@0
    .prologue
    .line 268
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mMessage:Ljava/lang/CharSequence;

    #@2
    .line 269
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mMessageView:Landroid/widget/TextView;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 270
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mMessageView:Landroid/widget/TextView;

    #@8
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@b
    .line 272
    :cond_b
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 254
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mTitle:Ljava/lang/CharSequence;

    #@2
    .line 255
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mTitleView:Landroid/widget/TextView;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 256
    iget-object v0, p0, Lcom/android/internal/app/AlertController;->mTitleView:Landroid/widget/TextView;

    #@8
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@b
    .line 258
    :cond_b
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 278
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mView:Landroid/view/View;

    #@2
    .line 279
    const/4 v0, 0x0

    #@3
    iput-boolean v0, p0, Lcom/android/internal/app/AlertController;->mViewSpacingSpecified:Z

    #@5
    .line 280
    return-void
.end method

.method public setView(Landroid/view/View;IIII)V
    .registers 7
    .parameter "view"
    .parameter "viewSpacingLeft"
    .parameter "viewSpacingTop"
    .parameter "viewSpacingRight"
    .parameter "viewSpacingBottom"

    #@0
    .prologue
    .line 287
    iput-object p1, p0, Lcom/android/internal/app/AlertController;->mView:Landroid/view/View;

    #@2
    .line 288
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Lcom/android/internal/app/AlertController;->mViewSpacingSpecified:Z

    #@5
    .line 289
    iput p2, p0, Lcom/android/internal/app/AlertController;->mViewSpacingLeft:I

    #@7
    .line 290
    iput p3, p0, Lcom/android/internal/app/AlertController;->mViewSpacingTop:I

    #@9
    .line 291
    iput p4, p0, Lcom/android/internal/app/AlertController;->mViewSpacingRight:I

    #@b
    .line 292
    iput p5, p0, Lcom/android/internal/app/AlertController;->mViewSpacingBottom:I

    #@d
    .line 293
    return-void
.end method
