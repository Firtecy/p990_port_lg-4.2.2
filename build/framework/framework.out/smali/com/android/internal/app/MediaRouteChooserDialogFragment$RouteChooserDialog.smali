.class Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;
.super Landroid/app/Dialog;
.source "MediaRouteChooserDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RouteChooserDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;


# direct methods
.method public constructor <init>(Lcom/android/internal/app/MediaRouteChooserDialogFragment;Landroid/content/Context;I)V
    .registers 4
    .parameter
    .parameter "context"
    .parameter "theme"

    #@0
    .prologue
    .line 642
    iput-object p1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2
    .line 643
    invoke-direct {p0, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@5
    .line 644
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .registers 2

    #@0
    .prologue
    .line 648
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2
    invoke-static {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$700(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_1e

    #@8
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@a
    invoke-static {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$700(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->isGrouping()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1e

    #@14
    .line 649
    iget-object v0, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@16
    invoke-static {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$700(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteAdapter;->finishGrouping()V

    #@1d
    .line 653
    :goto_1d
    return-void

    #@1e
    .line 651
    :cond_1e
    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    #@21
    goto :goto_1d
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 656
    const/16 v2, 0x19

    #@3
    if-ne p1, v2, :cond_26

    #@5
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@7
    invoke-static {v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$900(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/SeekBar;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Landroid/widget/SeekBar;->isEnabled()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_26

    #@11
    .line 657
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@13
    iget-object v2, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@15
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@17
    invoke-static {v3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I

    #@1a
    move-result v3

    #@1b
    invoke-virtual {v2, v3}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@1e
    move-result-object v0

    #@1f
    .line 658
    .local v0, selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    if-eqz v0, :cond_58

    #@21
    .line 659
    const/4 v2, -0x1

    #@22
    invoke-virtual {v0, v2}, Landroid/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    #@25
    .line 669
    .end local v0           #selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    :goto_25
    return v1

    #@26
    .line 662
    :cond_26
    const/16 v2, 0x18

    #@28
    if-ne p1, v2, :cond_58

    #@2a
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2c
    invoke-static {v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$900(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/SeekBar;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Landroid/widget/SeekBar;->isEnabled()Z

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_58

    #@36
    .line 663
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@38
    iget-object v2, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@3a
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@3c
    invoke-static {v3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I

    #@3f
    move-result v3

    #@40
    invoke-virtual {v2, v3}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@43
    move-result-object v0

    #@44
    .line 664
    .restart local v0       #selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    if-eqz v0, :cond_58

    #@46
    .line 665
    iget-object v2, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@48
    iget-object v2, v2, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->mRouter:Landroid/media/MediaRouter;

    #@4a
    iget-object v3, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@4c
    invoke-static {v3}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)I

    #@4f
    move-result v3

    #@50
    invoke-virtual {v2, v3}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2, v1}, Landroid/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    #@57
    goto :goto_25

    #@58
    .line 669
    .end local v0           #selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    :cond_58
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@5b
    move-result v1

    #@5c
    goto :goto_25
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 673
    const/16 v1, 0x19

    #@3
    if-ne p1, v1, :cond_12

    #@5
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@7
    invoke-static {v1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$900(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/SeekBar;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Landroid/widget/SeekBar;->isEnabled()Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 678
    :cond_11
    :goto_11
    return v0

    #@12
    .line 675
    :cond_12
    const/16 v1, 0x18

    #@14
    if-ne p1, v1, :cond_22

    #@16
    iget-object v1, p0, Lcom/android/internal/app/MediaRouteChooserDialogFragment$RouteChooserDialog;->this$0:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@18
    invoke-static {v1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->access$900(Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Landroid/widget/SeekBar;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Landroid/widget/SeekBar;->isEnabled()Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_11

    #@22
    .line 678
    :cond_22
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@25
    move-result v0

    #@26
    goto :goto_11
.end method
