.class public abstract Lcom/android/internal/app/IMediaContainerService$Stub;
.super Landroid/os/Binder;
.source "IMediaContainerService.java"

# interfaces
.implements Lcom/android/internal/app/IMediaContainerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/IMediaContainerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.app.IMediaContainerService"

.field static final TRANSACTION_calculateDirectorySize:I = 0x7

.field static final TRANSACTION_calculateInstalledSize:I = 0xa

.field static final TRANSACTION_checkExternalFreeStorage:I = 0x5

.field static final TRANSACTION_checkInternalFreeStorage:I = 0x4

.field static final TRANSACTION_clearDirectory:I = 0x9

.field static final TRANSACTION_copyResource:I = 0x2

.field static final TRANSACTION_copyResourceToContainer:I = 0x1

.field static final TRANSACTION_getFileSystemStats:I = 0x8

.field static final TRANSACTION_getMinimalPackageInfo:I = 0x3

.field static final TRANSACTION_getObbInfo:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/app/IMediaContainerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IMediaContainerService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.app.IMediaContainerService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/app/IMediaContainerService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/app/IMediaContainerService;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/app/IMediaContainerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 17
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_1a2

    #@3
    .line 215
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 42
    :sswitch_8
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 43
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 47
    :sswitch_f
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_4d

    #@1a
    .line 50
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/net/Uri;

    #@22
    .line 56
    .local v1, _arg0:Landroid/net/Uri;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .line 58
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    .line 60
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    .line 62
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    .line 64
    .local v5, _arg4:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_4f

    #@38
    const/4 v6, 0x1

    #@39
    .line 66
    .local v6, _arg5:Z
    :goto_39
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v0

    #@3d
    if-eqz v0, :cond_51

    #@3f
    const/4 v7, 0x1

    #@40
    .local v7, _arg6:Z
    :goto_40
    move-object v0, p0

    #@41
    .line 67
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/app/IMediaContainerService$Stub;->copyResourceToContainer(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    #@44
    move-result-object v10

    #@45
    .line 68
    .local v10, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    .line 69
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4b
    .line 70
    const/4 v0, 0x1

    #@4c
    goto :goto_7

    #@4d
    .line 53
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Z
    .end local v7           #_arg6:Z
    .end local v10           #_result:Ljava/lang/String;
    :cond_4d
    const/4 v1, 0x0

    #@4e
    .restart local v1       #_arg0:Landroid/net/Uri;
    goto :goto_22

    #@4f
    .line 64
    .restart local v2       #_arg1:Ljava/lang/String;
    .restart local v3       #_arg2:Ljava/lang/String;
    .restart local v4       #_arg3:Ljava/lang/String;
    .restart local v5       #_arg4:Ljava/lang/String;
    :cond_4f
    const/4 v6, 0x0

    #@50
    goto :goto_39

    #@51
    .line 66
    .restart local v6       #_arg5:Z
    :cond_51
    const/4 v7, 0x0

    #@52
    goto :goto_40

    #@53
    .line 74
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Z
    :sswitch_53
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@55
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58
    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_8f

    #@5e
    .line 77
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@60
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@63
    move-result-object v1

    #@64
    check-cast v1, Landroid/net/Uri;

    #@66
    .line 83
    .restart local v1       #_arg0:Landroid/net/Uri;
    :goto_66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v0

    #@6a
    if-eqz v0, :cond_91

    #@6c
    .line 84
    sget-object v0, Landroid/content/pm/ContainerEncryptionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6e
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@71
    move-result-object v2

    #@72
    check-cast v2, Landroid/content/pm/ContainerEncryptionParams;

    #@74
    .line 90
    .local v2, _arg1:Landroid/content/pm/ContainerEncryptionParams;
    :goto_74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v0

    #@78
    if-eqz v0, :cond_93

    #@7a
    .line 91
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7c
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7f
    move-result-object v3

    #@80
    check-cast v3, Landroid/os/ParcelFileDescriptor;

    #@82
    .line 96
    .local v3, _arg2:Landroid/os/ParcelFileDescriptor;
    :goto_82
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/app/IMediaContainerService$Stub;->copyResource(Landroid/net/Uri;Landroid/content/pm/ContainerEncryptionParams;Landroid/os/ParcelFileDescriptor;)I

    #@85
    move-result v10

    #@86
    .line 97
    .local v10, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@89
    .line 98
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@8c
    .line 99
    const/4 v0, 0x1

    #@8d
    goto/16 :goto_7

    #@8f
    .line 80
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Landroid/content/pm/ContainerEncryptionParams;
    .end local v3           #_arg2:Landroid/os/ParcelFileDescriptor;
    .end local v10           #_result:I
    :cond_8f
    const/4 v1, 0x0

    #@90
    .restart local v1       #_arg0:Landroid/net/Uri;
    goto :goto_66

    #@91
    .line 87
    :cond_91
    const/4 v2, 0x0

    #@92
    .restart local v2       #_arg1:Landroid/content/pm/ContainerEncryptionParams;
    goto :goto_74

    #@93
    .line 94
    :cond_93
    const/4 v3, 0x0

    #@94
    .restart local v3       #_arg2:Landroid/os/ParcelFileDescriptor;
    goto :goto_82

    #@95
    .line 103
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Landroid/content/pm/ContainerEncryptionParams;
    .end local v3           #_arg2:Landroid/os/ParcelFileDescriptor;
    :sswitch_95
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@97
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9d
    move-result-object v1

    #@9e
    .line 107
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a1
    move-result v2

    #@a2
    .line 109
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@a5
    move-result-wide v8

    #@a6
    .line 110
    .local v8, _arg2:J
    invoke-virtual {p0, v1, v2, v8, v9}, Lcom/android/internal/app/IMediaContainerService$Stub;->getMinimalPackageInfo(Ljava/lang/String;IJ)Landroid/content/pm/PackageInfoLite;

    #@a9
    move-result-object v10

    #@aa
    .line 111
    .local v10, _result:Landroid/content/pm/PackageInfoLite;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad
    .line 112
    if-eqz v10, :cond_ba

    #@af
    .line 113
    const/4 v0, 0x1

    #@b0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@b3
    .line 114
    const/4 v0, 0x1

    #@b4
    invoke-virtual {v10, p3, v0}, Landroid/content/pm/PackageInfoLite;->writeToParcel(Landroid/os/Parcel;I)V

    #@b7
    .line 119
    :goto_b7
    const/4 v0, 0x1

    #@b8
    goto/16 :goto_7

    #@ba
    .line 117
    :cond_ba
    const/4 v0, 0x0

    #@bb
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@be
    goto :goto_b7

    #@bf
    .line 123
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v8           #_arg2:J
    .end local v10           #_result:Landroid/content/pm/PackageInfoLite;
    :sswitch_bf
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@c1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c4
    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v0

    #@c8
    if-eqz v0, :cond_ed

    #@ca
    .line 126
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@cc
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@cf
    move-result-object v1

    #@d0
    check-cast v1, Landroid/net/Uri;

    #@d2
    .line 132
    .local v1, _arg0:Landroid/net/Uri;
    :goto_d2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d5
    move-result v0

    #@d6
    if-eqz v0, :cond_ef

    #@d8
    const/4 v2, 0x1

    #@d9
    .line 134
    .local v2, _arg1:Z
    :goto_d9
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@dc
    move-result-wide v8

    #@dd
    .line 135
    .restart local v8       #_arg2:J
    invoke-virtual {p0, v1, v2, v8, v9}, Lcom/android/internal/app/IMediaContainerService$Stub;->checkInternalFreeStorage(Landroid/net/Uri;ZJ)Z

    #@e0
    move-result v10

    #@e1
    .line 136
    .local v10, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e4
    .line 137
    if-eqz v10, :cond_f1

    #@e6
    const/4 v0, 0x1

    #@e7
    :goto_e7
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@ea
    .line 138
    const/4 v0, 0x1

    #@eb
    goto/16 :goto_7

    #@ed
    .line 129
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Z
    .end local v8           #_arg2:J
    .end local v10           #_result:Z
    :cond_ed
    const/4 v1, 0x0

    #@ee
    .restart local v1       #_arg0:Landroid/net/Uri;
    goto :goto_d2

    #@ef
    .line 132
    :cond_ef
    const/4 v2, 0x0

    #@f0
    goto :goto_d9

    #@f1
    .line 137
    .restart local v2       #_arg1:Z
    .restart local v8       #_arg2:J
    .restart local v10       #_result:Z
    :cond_f1
    const/4 v0, 0x0

    #@f2
    goto :goto_e7

    #@f3
    .line 142
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Z
    .end local v8           #_arg2:J
    .end local v10           #_result:Z
    :sswitch_f3
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@f5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f8
    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@fb
    move-result v0

    #@fc
    if-eqz v0, :cond_11d

    #@fe
    .line 145
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@100
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@103
    move-result-object v1

    #@104
    check-cast v1, Landroid/net/Uri;

    #@106
    .line 151
    .restart local v1       #_arg0:Landroid/net/Uri;
    :goto_106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@109
    move-result v0

    #@10a
    if-eqz v0, :cond_11f

    #@10c
    const/4 v2, 0x1

    #@10d
    .line 152
    .restart local v2       #_arg1:Z
    :goto_10d
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IMediaContainerService$Stub;->checkExternalFreeStorage(Landroid/net/Uri;Z)Z

    #@110
    move-result v10

    #@111
    .line 153
    .restart local v10       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@114
    .line 154
    if-eqz v10, :cond_121

    #@116
    const/4 v0, 0x1

    #@117
    :goto_117
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11a
    .line 155
    const/4 v0, 0x1

    #@11b
    goto/16 :goto_7

    #@11d
    .line 148
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Z
    .end local v10           #_result:Z
    :cond_11d
    const/4 v1, 0x0

    #@11e
    .restart local v1       #_arg0:Landroid/net/Uri;
    goto :goto_106

    #@11f
    .line 151
    :cond_11f
    const/4 v2, 0x0

    #@120
    goto :goto_10d

    #@121
    .line 154
    .restart local v2       #_arg1:Z
    .restart local v10       #_result:Z
    :cond_121
    const/4 v0, 0x0

    #@122
    goto :goto_117

    #@123
    .line 159
    .end local v1           #_arg0:Landroid/net/Uri;
    .end local v2           #_arg1:Z
    .end local v10           #_result:Z
    :sswitch_123
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@125
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@128
    .line 161
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12b
    move-result-object v1

    #@12c
    .line 162
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IMediaContainerService$Stub;->getObbInfo(Ljava/lang/String;)Landroid/content/res/ObbInfo;

    #@12f
    move-result-object v10

    #@130
    .line 163
    .local v10, _result:Landroid/content/res/ObbInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@133
    .line 164
    if-eqz v10, :cond_140

    #@135
    .line 165
    const/4 v0, 0x1

    #@136
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@139
    .line 166
    const/4 v0, 0x1

    #@13a
    invoke-virtual {v10, p3, v0}, Landroid/content/res/ObbInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@13d
    .line 171
    :goto_13d
    const/4 v0, 0x1

    #@13e
    goto/16 :goto_7

    #@140
    .line 169
    :cond_140
    const/4 v0, 0x0

    #@141
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@144
    goto :goto_13d

    #@145
    .line 175
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v10           #_result:Landroid/content/res/ObbInfo;
    :sswitch_145
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@147
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14a
    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14d
    move-result-object v1

    #@14e
    .line 178
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IMediaContainerService$Stub;->calculateDirectorySize(Ljava/lang/String;)J

    #@151
    move-result-wide v10

    #@152
    .line 179
    .local v10, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@155
    .line 180
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@158
    .line 181
    const/4 v0, 0x1

    #@159
    goto/16 :goto_7

    #@15b
    .line 185
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v10           #_result:J
    :sswitch_15b
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@15d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@160
    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@163
    move-result-object v1

    #@164
    .line 188
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IMediaContainerService$Stub;->getFileSystemStats(Ljava/lang/String;)[J

    #@167
    move-result-object v10

    #@168
    .line 189
    .local v10, _result:[J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@16b
    .line 190
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeLongArray([J)V

    #@16e
    .line 191
    const/4 v0, 0x1

    #@16f
    goto/16 :goto_7

    #@171
    .line 195
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v10           #_result:[J
    :sswitch_171
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@173
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@176
    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@179
    move-result-object v1

    #@17a
    .line 198
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/app/IMediaContainerService$Stub;->clearDirectory(Ljava/lang/String;)V

    #@17d
    .line 199
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@180
    .line 200
    const/4 v0, 0x1

    #@181
    goto/16 :goto_7

    #@183
    .line 204
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_183
    const-string v0, "com.android.internal.app.IMediaContainerService"

    #@185
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@188
    .line 206
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18b
    move-result-object v1

    #@18c
    .line 208
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18f
    move-result v0

    #@190
    if-eqz v0, :cond_1a0

    #@192
    const/4 v2, 0x1

    #@193
    .line 209
    .restart local v2       #_arg1:Z
    :goto_193
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/app/IMediaContainerService$Stub;->calculateInstalledSize(Ljava/lang/String;Z)J

    #@196
    move-result-wide v10

    #@197
    .line 210
    .local v10, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@19a
    .line 211
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@19d
    .line 212
    const/4 v0, 0x1

    #@19e
    goto/16 :goto_7

    #@1a0
    .line 208
    .end local v2           #_arg1:Z
    .end local v10           #_result:J
    :cond_1a0
    const/4 v2, 0x0

    #@1a1
    goto :goto_193

    #@1a2
    .line 38
    :sswitch_data_1a2
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_53
        0x3 -> :sswitch_95
        0x4 -> :sswitch_bf
        0x5 -> :sswitch_f3
        0x6 -> :sswitch_123
        0x7 -> :sswitch_145
        0x8 -> :sswitch_15b
        0x9 -> :sswitch_171
        0xa -> :sswitch_183
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
