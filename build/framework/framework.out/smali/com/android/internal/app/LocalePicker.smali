.class public Lcom/android/internal/app/LocalePicker;
.super Landroid/app/ListFragment;
.source "LocalePicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/LocalePicker$LocaleInfo;,
        Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "LocalePicker"

.field private static mCountryCode:Ljava/lang/String;

.field private static final sOperator:Ljava/lang/String;


# instance fields
.field private mCurrentCountry:Ljava/lang/String;

.field mListener:Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 58
    const-string/jumbo v0, "ro.build.target_operator"

    #@3
    const-string v1, ""

    #@5
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Lcom/android/internal/app/LocalePicker;->sOperator:Ljava/lang/String;

    #@b
    .line 60
    const-string/jumbo v0, "ro.build.target_country"

    #@e
    const-string v1, ""

    #@10
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    sput-object v0, Lcom/android/internal/app/LocalePicker;->mCountryCode:Ljava/lang/String;

    #@16
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    #@3
    .line 63
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/internal/app/LocalePicker;->mCurrentCountry:Ljava/lang/String;

    #@6
    .line 65
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 47
    sget-object v0, Lcom/android/internal/app/LocalePicker;->mCountryCode:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static constructAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .registers 3
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/internal/app/LocalePicker$LocaleInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 131
    const v0, 0x203000e

    #@3
    const v1, 0x20d0056

    #@6
    invoke-static {p0, v0, v1}, Lcom/android/internal/app/LocalePicker;->constructAdapter(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static constructAdapter(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;
    .registers 33
    .parameter "context"
    .parameter "layoutId"
    .parameter "fieldId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/internal/app/LocalePicker$LocaleInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 136
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v25

    #@4
    .line 137
    .local v25, resources:Landroid/content/res/Resources;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    #@f
    move-result-object v22

    #@10
    .line 138
    .local v22, locales:[Ljava/lang/String;
    const v3, 0x107000a

    #@13
    move-object/from16 v0, v25

    #@15
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@18
    move-result-object v28

    #@19
    .line 139
    .local v28, specialLocaleCodes:[Ljava/lang/String;
    const v3, 0x107000b

    #@1c
    move-object/from16 v0, v25

    #@1e
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@21
    move-result-object v29

    #@22
    .line 140
    .local v29, specialLocaleNames:[Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    #@25
    .line 142
    move-object/from16 v0, v22

    #@27
    array-length v0, v0

    #@28
    move/from16 v23, v0

    #@2a
    .line 143
    .local v23, origSize:I
    move/from16 v0, v23

    #@2c
    new-array v0, v0, [Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@2e
    move-object/from16 v24, v0

    #@30
    .line 144
    .local v24, preprocess:[Lcom/android/internal/app/LocalePicker$LocaleInfo;
    const/16 v16, 0x0

    #@32
    .line 145
    .local v16, finalSize:I
    const/16 v18, 0x0

    #@34
    .local v18, i:I
    move/from16 v17, v16

    #@36
    .end local v16           #finalSize:I
    .local v17, finalSize:I
    :goto_36
    move/from16 v0, v18

    #@38
    move/from16 v1, v23

    #@3a
    if-ge v0, v1, :cond_103

    #@3c
    .line 146
    aget-object v26, v22, v18

    #@3e
    .line 147
    .local v26, s:Ljava/lang/String;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    #@41
    move-result v21

    #@42
    .line 148
    .local v21, len:I
    const-string v3, "LocalePicker"

    #@44
    new-instance v4, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string/jumbo v5, "org locale s = "

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    move-object/from16 v0, v26

    #@52
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 149
    const/4 v3, 0x5

    #@5e
    move/from16 v0, v21

    #@60
    if-ne v0, v3, :cond_2d8

    #@62
    .line 150
    const/4 v3, 0x0

    #@63
    const/4 v4, 0x2

    #@64
    move-object/from16 v0, v26

    #@66
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@69
    move-result-object v20

    #@6a
    .line 151
    .local v20, language:Ljava/lang/String;
    const/4 v3, 0x3

    #@6b
    const/4 v4, 0x5

    #@6c
    move-object/from16 v0, v26

    #@6e
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@71
    move-result-object v13

    #@72
    .line 152
    .local v13, country:Ljava/lang/String;
    new-instance v19, Ljava/util/Locale;

    #@74
    move-object/from16 v0, v19

    #@76
    move-object/from16 v1, v20

    #@78
    invoke-direct {v0, v1, v13}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    .line 154
    .local v19, l:Ljava/util/Locale;
    if-nez v17, :cond_99

    #@7d
    .line 158
    add-int/lit8 v16, v17, 0x1

    #@7f
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@81
    move-object/from16 v0, v19

    #@83
    move-object/from16 v1, v19

    #@85
    invoke-virtual {v0, v1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    #@88
    move-result-object v4

    #@89
    invoke-static {v4}, Lcom/android/internal/app/LocalePicker;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@8c
    move-result-object v4

    #@8d
    move-object/from16 v0, v19

    #@8f
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@92
    aput-object v3, v24, v17

    #@94
    .line 145
    .end local v13           #country:Ljava/lang/String;
    .end local v19           #l:Ljava/util/Locale;
    .end local v20           #language:Ljava/lang/String;
    :goto_94
    add-int/lit8 v18, v18, 0x1

    #@96
    move/from16 v17, v16

    #@98
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    goto :goto_36

    #@99
    .line 165
    .restart local v13       #country:Ljava/lang/String;
    .restart local v19       #l:Ljava/util/Locale;
    .restart local v20       #language:Ljava/lang/String;
    :cond_99
    add-int/lit8 v3, v17, -0x1

    #@9b
    aget-object v3, v24, v3

    #@9d
    iget-object v3, v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;->locale:Ljava/util/Locale;

    #@9f
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@a2
    move-result-object v3

    #@a3
    move-object/from16 v0, v20

    #@a5
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v3

    #@a9
    if-eqz v3, :cond_dd

    #@ab
    .line 173
    add-int/lit8 v3, v17, -0x1

    #@ad
    aget-object v3, v24, v3

    #@af
    add-int/lit8 v4, v17, -0x1

    #@b1
    aget-object v4, v24, v4

    #@b3
    iget-object v4, v4, Lcom/android/internal/app/LocalePicker$LocaleInfo;->locale:Ljava/util/Locale;

    #@b5
    move-object/from16 v0, v28

    #@b7
    move-object/from16 v1, v29

    #@b9
    invoke-static {v4, v0, v1}, Lcom/android/internal/app/LocalePicker;->getDisplayName(Ljava/util/Locale;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@bc
    move-result-object v4

    #@bd
    invoke-static {v4}, Lcom/android/internal/app/LocalePicker;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@c0
    move-result-object v4

    #@c1
    iput-object v4, v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;->label:Ljava/lang/String;

    #@c3
    .line 180
    add-int/lit8 v16, v17, 0x1

    #@c5
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@c7
    move-object/from16 v0, v19

    #@c9
    move-object/from16 v1, v28

    #@cb
    move-object/from16 v2, v29

    #@cd
    invoke-static {v0, v1, v2}, Lcom/android/internal/app/LocalePicker;->getDisplayName(Ljava/util/Locale;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@d0
    move-result-object v4

    #@d1
    invoke-static {v4}, Lcom/android/internal/app/LocalePicker;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@d4
    move-result-object v4

    #@d5
    move-object/from16 v0, v19

    #@d7
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@da
    aput-object v3, v24, v17

    #@dc
    goto :goto_94

    #@dd
    .line 186
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    :cond_dd
    const-string/jumbo v3, "zz_ZZ"

    #@e0
    move-object/from16 v0, v26

    #@e2
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v3

    #@e6
    if-eqz v3, :cond_f6

    #@e8
    .line 187
    const-string v15, "Pseudo..."

    #@ea
    .line 194
    .local v15, displayName:Ljava/lang/String;
    :goto_ea
    add-int/lit8 v16, v17, 0x1

    #@ec
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@ee
    move-object/from16 v0, v19

    #@f0
    invoke-direct {v3, v15, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@f3
    aput-object v3, v24, v17

    #@f5
    goto :goto_94

    #@f6
    .line 189
    .end local v15           #displayName:Ljava/lang/String;
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    :cond_f6
    move-object/from16 v0, v19

    #@f8
    move-object/from16 v1, v19

    #@fa
    invoke-virtual {v0, v1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    #@fd
    move-result-object v3

    #@fe
    invoke-static {v3}, Lcom/android/internal/app/LocalePicker;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@101
    move-result-object v15

    #@102
    .restart local v15       #displayName:Ljava/lang/String;
    goto :goto_ea

    #@103
    .line 201
    .end local v13           #country:Ljava/lang/String;
    .end local v15           #displayName:Ljava/lang/String;
    .end local v19           #l:Ljava/util/Locale;
    .end local v20           #language:Ljava/lang/String;
    .end local v21           #len:I
    .end local v26           #s:Ljava/lang/String;
    :cond_103
    const/16 v3, 0xb

    #@105
    new-array v12, v3, [Ljava/lang/String;

    #@107
    const/4 v3, 0x0

    #@108
    const-string v4, "bs_BA"

    #@10a
    aput-object v4, v12, v3

    #@10c
    const/4 v3, 0x1

    #@10d
    const-string v4, "gl_ES"

    #@10f
    aput-object v4, v12, v3

    #@111
    const/4 v3, 0x2

    #@112
    const-string/jumbo v4, "is_IS"

    #@115
    aput-object v4, v12, v3

    #@117
    const/4 v3, 0x3

    #@118
    const-string/jumbo v4, "kk_KZ"

    #@11b
    aput-object v4, v12, v3

    #@11d
    const/4 v3, 0x4

    #@11e
    const-string/jumbo v4, "km_KH"

    #@121
    aput-object v4, v12, v3

    #@123
    const/4 v3, 0x5

    #@124
    const-string/jumbo v4, "ku_IQ"

    #@127
    aput-object v4, v12, v3

    #@129
    const/4 v3, 0x6

    #@12a
    const-string/jumbo v4, "mk_MK"

    #@12d
    aput-object v4, v12, v3

    #@12f
    const/4 v3, 0x7

    #@130
    const-string/jumbo v4, "my_MM"

    #@133
    aput-object v4, v12, v3

    #@135
    const/16 v3, 0x8

    #@137
    const-string/jumbo v4, "uz_UZ"

    #@13a
    aput-object v4, v12, v3

    #@13c
    const/16 v3, 0x9

    #@13e
    const-string v4, "et_EE"

    #@140
    aput-object v4, v12, v3

    #@142
    const/16 v3, 0xa

    #@144
    const-string v4, "eu_ES"

    #@146
    aput-object v4, v12, v3

    #@148
    .line 202
    .local v12, addtional:[Ljava/lang/String;
    array-length v3, v12

    #@149
    add-int v3, v3, v17

    #@14b
    add-int/lit8 v3, v3, 0x1

    #@14d
    new-array v11, v3, [Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@14f
    .line 203
    .local v11, additionalProcess:[Lcom/android/internal/app/LocalePicker$LocaleInfo;
    const/4 v3, 0x0

    #@150
    const/4 v4, 0x0

    #@151
    move-object/from16 v0, v24

    #@153
    move/from16 v1, v17

    #@155
    invoke-static {v0, v3, v11, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@158
    .line 204
    move-object/from16 v24, v11

    #@15a
    .line 206
    const/16 v18, 0x0

    #@15c
    move/from16 v16, v17

    #@15e
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    :goto_15e
    array-length v3, v12

    #@15f
    move/from16 v0, v18

    #@161
    if-ge v0, v3, :cond_287

    #@163
    .line 207
    aget-object v26, v12, v18

    #@165
    .line 208
    .restart local v26       #s:Ljava/lang/String;
    const/4 v3, 0x0

    #@166
    const/4 v4, 0x2

    #@167
    move-object/from16 v0, v26

    #@169
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@16c
    move-result-object v20

    #@16d
    .line 209
    .restart local v20       #language:Ljava/lang/String;
    const/4 v3, 0x3

    #@16e
    const/4 v4, 0x5

    #@16f
    move-object/from16 v0, v26

    #@171
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@174
    move-result-object v13

    #@175
    .line 210
    .restart local v13       #country:Ljava/lang/String;
    new-instance v19, Ljava/util/Locale;

    #@177
    move-object/from16 v0, v19

    #@179
    move-object/from16 v1, v20

    #@17b
    invoke-direct {v0, v1, v13}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@17e
    .line 212
    .restart local v19       #l:Ljava/util/Locale;
    sget-object v3, Lcom/android/internal/app/LocalePicker;->sOperator:Ljava/lang/String;

    #@180
    const-string v4, "OPEN"

    #@182
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@185
    move-result v3

    #@186
    if-eqz v3, :cond_192

    #@188
    sget-object v3, Lcom/android/internal/app/LocalePicker;->mCountryCode:Ljava/lang/String;

    #@18a
    const-string v4, "EU"

    #@18c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@18f
    move-result v3

    #@190
    if-nez v3, :cond_1e2

    #@192
    :cond_192
    sget-object v3, Lcom/android/internal/app/LocalePicker;->sOperator:Ljava/lang/String;

    #@194
    const-string v4, "VDF"

    #@196
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@199
    move-result v3

    #@19a
    if-eqz v3, :cond_1a6

    #@19c
    sget-object v3, Lcom/android/internal/app/LocalePicker;->mCountryCode:Ljava/lang/String;

    #@19e
    const-string v4, "COM"

    #@1a0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1a3
    move-result v3

    #@1a4
    if-nez v3, :cond_1e2

    #@1a6
    :cond_1a6
    sget-object v3, Lcom/android/internal/app/LocalePicker;->sOperator:Ljava/lang/String;

    #@1a8
    const-string v4, "TMO"

    #@1aa
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1ad
    move-result v3

    #@1ae
    if-eqz v3, :cond_1ba

    #@1b0
    sget-object v3, Lcom/android/internal/app/LocalePicker;->mCountryCode:Ljava/lang/String;

    #@1b2
    const-string v4, "COM"

    #@1b4
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b7
    move-result v3

    #@1b8
    if-nez v3, :cond_1e2

    #@1ba
    :cond_1ba
    sget-object v3, Lcom/android/internal/app/LocalePicker;->sOperator:Ljava/lang/String;

    #@1bc
    const-string v4, "ORG"

    #@1be
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1c1
    move-result v3

    #@1c2
    if-eqz v3, :cond_1ce

    #@1c4
    sget-object v3, Lcom/android/internal/app/LocalePicker;->mCountryCode:Ljava/lang/String;

    #@1c6
    const-string v4, "COM"

    #@1c8
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1cb
    move-result v3

    #@1cc
    if-nez v3, :cond_1e2

    #@1ce
    :cond_1ce
    sget-object v3, Lcom/android/internal/app/LocalePicker;->sOperator:Ljava/lang/String;

    #@1d0
    const-string v4, "H3G"

    #@1d2
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1d5
    move-result v3

    #@1d6
    if-eqz v3, :cond_26d

    #@1d8
    sget-object v3, Lcom/android/internal/app/LocalePicker;->mCountryCode:Ljava/lang/String;

    #@1da
    const-string v4, "COM"

    #@1dc
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1df
    move-result v3

    #@1e0
    if-eqz v3, :cond_26d

    #@1e2
    .line 217
    :cond_1e2
    const-string/jumbo v3, "mk_MK"

    #@1e5
    aget-object v4, v12, v18

    #@1e7
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ea
    move-result v3

    #@1eb
    if-eqz v3, :cond_239

    #@1ed
    .line 218
    const-string/jumbo v3, "phone"

    #@1f0
    move-object/from16 v0, p0

    #@1f2
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f5
    move-result-object v3

    #@1f6
    check-cast v3, Landroid/telephony/TelephonyManager;

    #@1f8
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@1fb
    move-result-object v27

    #@1fc
    .line 219
    .local v27, simOperator:Ljava/lang/String;
    const/4 v14, 0x0

    #@1fd
    .line 221
    .local v14, currentMCC:Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    #@200
    move-result v3

    #@201
    const/4 v4, 0x2

    #@202
    if-le v3, v4, :cond_20c

    #@204
    .line 222
    const/4 v3, 0x0

    #@205
    const/4 v4, 0x3

    #@206
    move-object/from16 v0, v27

    #@208
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@20b
    move-result-object v14

    #@20c
    .line 225
    :cond_20c
    if-eqz v14, :cond_225

    #@20e
    .line 226
    const-string v3, "294"

    #@210
    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@213
    move-result v3

    #@214
    if-eqz v3, :cond_229

    #@216
    .line 227
    add-int/lit8 v17, v16, 0x1

    #@218
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@21a
    const-string v4, "Macedonian"

    #@21c
    move-object/from16 v0, v19

    #@21e
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@221
    aput-object v3, v24, v16

    #@223
    move/from16 v16, v17

    #@225
    .line 206
    .end local v14           #currentMCC:Ljava/lang/String;
    .end local v17           #finalSize:I
    .end local v27           #simOperator:Ljava/lang/String;
    .restart local v16       #finalSize:I
    :cond_225
    :goto_225
    add-int/lit8 v18, v18, 0x1

    #@227
    goto/16 :goto_15e

    #@229
    .line 229
    .restart local v14       #currentMCC:Ljava/lang/String;
    .restart local v27       #simOperator:Ljava/lang/String;
    :cond_229
    add-int/lit8 v17, v16, 0x1

    #@22b
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@22d
    const-string v4, "FYROM"

    #@22f
    move-object/from16 v0, v19

    #@231
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@234
    aput-object v3, v24, v16

    #@236
    move/from16 v16, v17

    #@238
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    goto :goto_225

    #@239
    .line 234
    .end local v14           #currentMCC:Ljava/lang/String;
    .end local v27           #simOperator:Ljava/lang/String;
    :cond_239
    const-string v3, "eu_ES"

    #@23b
    aget-object v4, v12, v18

    #@23d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@240
    move-result v3

    #@241
    if-eqz v3, :cond_253

    #@243
    .line 235
    add-int/lit8 v17, v16, 0x1

    #@245
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@247
    const-string v4, "Euskera"

    #@249
    move-object/from16 v0, v19

    #@24b
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@24e
    aput-object v3, v24, v16

    #@250
    move/from16 v16, v17

    #@252
    .line 236
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    goto :goto_225

    #@253
    .line 238
    :cond_253
    const-string v3, "gl_ES"

    #@255
    aget-object v4, v12, v18

    #@257
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25a
    move-result v3

    #@25b
    if-eqz v3, :cond_26d

    #@25d
    .line 239
    add-int/lit8 v17, v16, 0x1

    #@25f
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@261
    const-string v4, "Gallego"

    #@263
    move-object/from16 v0, v19

    #@265
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@268
    aput-object v3, v24, v16

    #@26a
    move/from16 v16, v17

    #@26c
    .line 240
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    goto :goto_225

    #@26d
    .line 245
    :cond_26d
    add-int/lit8 v17, v16, 0x1

    #@26f
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@271
    move-object/from16 v0, v19

    #@273
    move-object/from16 v1, v19

    #@275
    invoke-virtual {v0, v1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    #@278
    move-result-object v4

    #@279
    invoke-static {v4}, Lcom/android/internal/app/LocalePicker;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@27c
    move-result-object v4

    #@27d
    move-object/from16 v0, v19

    #@27f
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@282
    aput-object v3, v24, v16

    #@284
    move/from16 v16, v17

    #@286
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    goto :goto_225

    #@287
    .line 247
    .end local v13           #country:Ljava/lang/String;
    .end local v19           #l:Ljava/util/Locale;
    .end local v20           #language:Ljava/lang/String;
    .end local v26           #s:Ljava/lang/String;
    :cond_287
    new-instance v19, Ljava/util/Locale;

    #@289
    const-string v3, "es"

    #@28b
    move-object/from16 v0, v19

    #@28d
    invoke-direct {v0, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    #@290
    .line 248
    .restart local v19       #l:Ljava/util/Locale;
    add-int/lit8 v17, v16, 0x1

    #@292
    .end local v16           #finalSize:I
    .restart local v17       #finalSize:I
    new-instance v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@294
    move-object/from16 v0, v19

    #@296
    move-object/from16 v1, v19

    #@298
    invoke-virtual {v0, v1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    #@29b
    move-result-object v4

    #@29c
    invoke-static {v4}, Lcom/android/internal/app/LocalePicker;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@29f
    move-result-object v4

    #@2a0
    move-object/from16 v0, v19

    #@2a2
    invoke-direct {v3, v4, v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    #@2a5
    aput-object v3, v24, v16

    #@2a7
    .line 252
    move/from16 v0, v17

    #@2a9
    new-array v7, v0, [Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@2ab
    .line 253
    .local v7, localeInfos:[Lcom/android/internal/app/LocalePicker$LocaleInfo;
    const/16 v18, 0x0

    #@2ad
    :goto_2ad
    move/from16 v0, v18

    #@2af
    move/from16 v1, v17

    #@2b1
    if-ge v0, v1, :cond_2ba

    #@2b3
    .line 254
    aget-object v3, v24, v18

    #@2b5
    aput-object v3, v7, v18

    #@2b7
    .line 253
    add-int/lit8 v18, v18, 0x1

    #@2b9
    goto :goto_2ad

    #@2ba
    .line 257
    :cond_2ba
    invoke-static {v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    #@2bd
    .line 259
    const-string/jumbo v3, "layout_inflater"

    #@2c0
    move-object/from16 v0, p0

    #@2c2
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2c5
    move-result-object v8

    #@2c6
    check-cast v8, Landroid/view/LayoutInflater;

    #@2c8
    .line 261
    .local v8, inflater:Landroid/view/LayoutInflater;
    new-instance v3, Lcom/android/internal/app/LocalePicker$1;

    #@2ca
    move-object/from16 v4, p0

    #@2cc
    move/from16 v5, p1

    #@2ce
    move/from16 v6, p2

    #@2d0
    move/from16 v9, p1

    #@2d2
    move/from16 v10, p2

    #@2d4
    invoke-direct/range {v3 .. v10}, Lcom/android/internal/app/LocalePicker$1;-><init>(Landroid/content/Context;II[Lcom/android/internal/app/LocalePicker$LocaleInfo;Landroid/view/LayoutInflater;II)V

    #@2d7
    return-object v3

    #@2d8
    .end local v7           #localeInfos:[Lcom/android/internal/app/LocalePicker$LocaleInfo;
    .end local v8           #inflater:Landroid/view/LayoutInflater;
    .end local v11           #additionalProcess:[Lcom/android/internal/app/LocalePicker$LocaleInfo;
    .end local v12           #addtional:[Ljava/lang/String;
    .end local v19           #l:Ljava/util/Locale;
    .restart local v21       #len:I
    .restart local v26       #s:Ljava/lang/String;
    :cond_2d8
    move/from16 v16, v17

    #@2da
    .end local v17           #finalSize:I
    .restart local v16       #finalSize:I
    goto/16 :goto_94
.end method

.method private currentCountry()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 401
    const/4 v2, 0x0

    #@1
    .line 403
    .local v2, mCountry:Ljava/lang/String;
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@4
    move-result-object v0

    #@5
    .line 404
    .local v0, am:Landroid/app/IActivityManager;
    invoke-interface {v0}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@8
    move-result-object v1

    #@9
    .line 405
    .local v1, config:Landroid/content/res/Configuration;
    iget-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@b
    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_e} :catch_10

    #@e
    move-result-object v2

    #@f
    .line 408
    .end local v0           #am:Landroid/app/IActivityManager;
    .end local v1           #config:Landroid/content/res/Configuration;
    :goto_f
    return-object v2

    #@10
    .line 406
    :catch_10
    move-exception v3

    #@11
    goto :goto_f
.end method

.method private static getDisplayName(Ljava/util/Locale;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "l"
    .parameter "specialLocaleCodes"
    .parameter "specialLocaleNames"

    #@0
    .prologue
    .line 293
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 295
    .local v0, code:Ljava/lang/String;
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    array-length v2, p1

    #@6
    if-ge v1, v2, :cond_16

    #@8
    .line 296
    aget-object v2, p1, v1

    #@a
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_13

    #@10
    .line 297
    aget-object v2, p2, v1

    #@12
    .line 301
    :goto_12
    return-object v2

    #@13
    .line 295
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_5

    #@16
    .line 301
    :cond_16
    invoke-virtual {p0, p0}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    goto :goto_12
.end method

.method private static toTitleCase(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 284
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 288
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const/4 v1, 0x0

    #@d
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@10
    move-result v1

    #@11
    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    #@14
    move-result v1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const/4 v1, 0x1

    #@1a
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object p0

    #@26
    goto :goto_6
.end method

.method public static updateLocale(Ljava/util/Locale;)V
    .registers 4
    .parameter "locale"

    #@0
    .prologue
    .line 384
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    .line 385
    .local v0, am:Landroid/app/IActivityManager;
    invoke-interface {v0}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v1

    #@8
    .line 389
    .local v1, config:Landroid/content/res/Configuration;
    invoke-virtual {v1, p0}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    #@b
    .line 391
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    #@e
    .line 393
    const-string v2, "com.android.providers.settings"

    #@10
    invoke-static {v2}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_13} :catch_14

    #@13
    .line 397
    .end local v0           #am:Landroid/app/IActivityManager;
    .end local v1           #config:Landroid/content/res/Configuration;
    :goto_13
    return-void

    #@14
    .line 394
    :catch_14
    move-exception v2

    #@15
    goto :goto_13
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 306
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    #@3
    .line 307
    invoke-virtual {p0}, Lcom/android/internal/app/LocalePicker;->getActivity()Landroid/app/Activity;

    #@6
    move-result-object v1

    #@7
    invoke-static {v1}, Lcom/android/internal/app/LocalePicker;->constructAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    #@a
    move-result-object v0

    #@b
    .line 308
    .local v0, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Lcom/android/internal/app/LocalePicker$LocaleInfo;>;"
    invoke-virtual {p0, v0}, Lcom/android/internal/app/LocalePicker;->setListAdapter(Landroid/widget/ListAdapter;)V

    #@e
    .line 309
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 11
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 351
    iget-object v3, p0, Lcom/android/internal/app/LocalePicker;->mListener:Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;

    #@2
    if-eqz v3, :cond_5f

    #@4
    .line 353
    invoke-virtual {p0}, Lcom/android/internal/app/LocalePicker;->getListAdapter()Landroid/widget/ListAdapter;

    #@7
    move-result-object v3

    #@8
    invoke-interface {v3, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@b
    move-result-object v3

    #@c
    check-cast v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    #@e
    iget-object v1, v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;->locale:Ljava/util/Locale;

    #@10
    .line 354
    .local v1, locale:Ljava/util/Locale;
    const-string v3, "SCA"

    #@12
    const-string/jumbo v4, "ro.build.target_region"

    #@15
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_5a

    #@1f
    .line 355
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    const-string v4, "es"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_5a

    #@2b
    .line 356
    const/4 v0, 0x0

    #@2c
    .line 357
    .local v0, countryCodeISO:Ljava/lang/String;
    const-string v3, "gsm.sim.operator.numeric"

    #@2e
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    .line 358
    .local v2, numeric:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@35
    move-result v3

    #@36
    if-nez v3, :cond_60

    #@38
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@3b
    move-result v3

    #@3c
    const/4 v4, 0x3

    #@3d
    if-le v3, v4, :cond_60

    #@3f
    .line 361
    const-string v3, "gsm.sim.operator.iso-country"

    #@41
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    .line 362
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@47
    invoke-virtual {v0, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    .line 367
    :goto_4b
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4e
    move-result v3

    #@4f
    if-eqz v3, :cond_53

    #@51
    .line 368
    const-string v0, "ES"

    #@53
    .line 370
    :cond_53
    new-instance v1, Ljava/util/Locale;

    #@55
    .end local v1           #locale:Ljava/util/Locale;
    const-string v3, "es"

    #@57
    invoke-direct {v1, v3, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 374
    .end local v0           #countryCodeISO:Ljava/lang/String;
    .end local v2           #numeric:Ljava/lang/String;
    .restart local v1       #locale:Ljava/util/Locale;
    :cond_5a
    iget-object v3, p0, Lcom/android/internal/app/LocalePicker;->mListener:Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;

    #@5c
    invoke-interface {v3, v1}, Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;->onLocaleSelected(Ljava/util/Locale;)V

    #@5f
    .line 376
    .end local v1           #locale:Ljava/util/Locale;
    :cond_5f
    return-void

    #@60
    .line 365
    .restart local v0       #countryCodeISO:Ljava/lang/String;
    .restart local v1       #locale:Ljava/util/Locale;
    .restart local v2       #numeric:Ljava/lang/String;
    :cond_60
    const-string/jumbo v3, "ro.build.default_country"

    #@63
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@66
    move-result-object v0

    #@67
    goto :goto_4b
.end method

.method public onPause()V
    .registers 3

    #@0
    .prologue
    .line 333
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    #@3
    .line 334
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS_JP:Z

    #@5
    if-eqz v0, :cond_1a

    #@7
    .line 335
    iget-object v0, p0, Lcom/android/internal/app/LocalePicker;->mCurrentCountry:Ljava/lang/String;

    #@9
    if-eqz v0, :cond_1a

    #@b
    iget-object v0, p0, Lcom/android/internal/app/LocalePicker;->mCurrentCountry:Ljava/lang/String;

    #@d
    invoke-direct {p0}, Lcom/android/internal/app/LocalePicker;->currentCountry()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_1a

    #@17
    .line 336
    invoke-static {}, Landroid/graphics/Canvas;->freeTextLayoutCaches()V

    #@1a
    .line 340
    :cond_1a
    return-void
.end method

.method public onResume()V
    .registers 2

    #@0
    .prologue
    .line 317
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    #@3
    .line 320
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS_JP:Z

    #@5
    if-eqz v0, :cond_10

    #@7
    .line 321
    invoke-static {}, Landroid/graphics/Canvas;->freeTextLayoutCaches()V

    #@a
    .line 323
    invoke-direct {p0}, Lcom/android/internal/app/LocalePicker;->currentCountry()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/internal/app/LocalePicker;->mCurrentCountry:Ljava/lang/String;

    #@10
    .line 327
    :cond_10
    invoke-virtual {p0}, Lcom/android/internal/app/LocalePicker;->getListView()Landroid/widget/ListView;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    #@17
    .line 328
    return-void
.end method

.method public setLocaleSelectionListener(Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 312
    iput-object p1, p0, Lcom/android/internal/app/LocalePicker;->mListener:Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;

    #@2
    .line 313
    return-void
.end method
