.class public Lcom/android/internal/app/ChooserActivity;
.super Lcom/android/internal/app/ResolverActivity;
.source "ChooserActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Lcom/android/internal/app/ResolverActivity;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/android/internal/app/ChooserActivity;->getIntent()Landroid/content/Intent;

    #@3
    move-result-object v8

    #@4
    .line 28
    .local v8, intent:Landroid/content/Intent;
    const-string v0, "android.intent.extra.INTENT"

    #@6
    invoke-virtual {v8, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@9
    move-result-object v10

    #@a
    .line 29
    .local v10, targetParcelable:Landroid/os/Parcelable;
    instance-of v0, v10, Landroid/content/Intent;

    #@c
    if-nez v0, :cond_2a

    #@e
    .line 30
    const-string v0, "ChooseActivity"

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "Target is not an intent: "

    #@17
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 31
    invoke-virtual {p0}, Lcom/android/internal/app/ChooserActivity;->finish()V

    #@29
    .line 54
    :goto_29
    return-void

    #@2a
    :cond_2a
    move-object v2, v10

    #@2b
    .line 34
    check-cast v2, Landroid/content/Intent;

    #@2d
    .line 35
    .local v2, target:Landroid/content/Intent;
    const-string v0, "android.intent.extra.TITLE"

    #@2f
    invoke-virtual {v8, v0}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@32
    move-result-object v3

    #@33
    .line 36
    .local v3, title:Ljava/lang/CharSequence;
    if-nez v3, :cond_40

    #@35
    .line 37
    invoke-virtual {p0}, Lcom/android/internal/app/ChooserActivity;->getResources()Landroid/content/res/Resources;

    #@38
    move-result-object v0

    #@39
    const v1, 0x10403f8

    #@3c
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3f
    move-result-object v3

    #@40
    .line 39
    :cond_40
    const-string v0, "android.intent.extra.INITIAL_INTENTS"

    #@42
    invoke-virtual {v8, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    #@45
    move-result-object v9

    #@46
    .line 40
    .local v9, pa:[Landroid/os/Parcelable;
    const/4 v4, 0x0

    #@47
    .line 41
    .local v4, initialIntents:[Landroid/content/Intent;
    if-eqz v9, :cond_87

    #@49
    .line 42
    array-length v0, v9

    #@4a
    new-array v4, v0, [Landroid/content/Intent;

    #@4c
    .line 43
    const/4 v7, 0x0

    #@4d
    .local v7, i:I
    :goto_4d
    array-length v0, v9

    #@4e
    if-ge v7, v0, :cond_87

    #@50
    .line 44
    aget-object v0, v9, v7

    #@52
    instance-of v0, v0, Landroid/content/Intent;

    #@54
    if-nez v0, :cond_7e

    #@56
    .line 45
    const-string v0, "ChooseActivity"

    #@58
    new-instance v1, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v5, "Initial intent #"

    #@5f
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    const-string v5, " not an Intent: "

    #@69
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    aget-object v5, v9, v7

    #@6f
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v1

    #@77
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 47
    invoke-virtual {p0}, Lcom/android/internal/app/ChooserActivity;->finish()V

    #@7d
    goto :goto_29

    #@7e
    .line 50
    :cond_7e
    aget-object v0, v9, v7

    #@80
    check-cast v0, Landroid/content/Intent;

    #@82
    aput-object v0, v4, v7

    #@84
    .line 43
    add-int/lit8 v7, v7, 0x1

    #@86
    goto :goto_4d

    #@87
    .line 53
    .end local v7           #i:I
    :cond_87
    const/4 v5, 0x0

    #@88
    const/4 v6, 0x0

    #@89
    move-object v0, p0

    #@8a
    move-object v1, p1

    #@8b
    invoke-super/range {v0 .. v6}, Lcom/android/internal/app/ResolverActivity;->onCreate(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/CharSequence;[Landroid/content/Intent;Ljava/util/List;Z)V

    #@8e
    goto :goto_29
.end method
