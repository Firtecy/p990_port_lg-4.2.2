.class public final Lcom/android/internal/policy/PolicyManager;
.super Ljava/lang/Object;
.source "PolicyManager.java"


# static fields
.field private static final POLICY_IMPL_CLASS_NAME:Ljava/lang/String; = "com.android.internal.policy.impl.Policy"

.field private static final sPolicy:Lcom/android/internal/policy/IPolicy;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 40
    :try_start_0
    const-string v2, "com.android.internal.policy.impl.Policy"

    #@2
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    .line 41
    .local v1, policyClass:Ljava/lang/Class;
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    check-cast v2, Lcom/android/internal/policy/IPolicy;

    #@c
    sput-object v2, Lcom/android/internal/policy/PolicyManager;->sPolicy:Lcom/android/internal/policy/IPolicy;
    :try_end_e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_e} :catch_f
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_e} :catch_18
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_e} :catch_21

    #@e
    .line 52
    return-void

    #@f
    .line 42
    :catch_f
    move-exception v0

    #@10
    .line 43
    .local v0, ex:Ljava/lang/ClassNotFoundException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@12
    const-string v3, "com.android.internal.policy.impl.Policy could not be loaded"

    #@14
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@17
    throw v2

    #@18
    .line 45
    .end local v0           #ex:Ljava/lang/ClassNotFoundException;
    :catch_18
    move-exception v0

    #@19
    .line 46
    .local v0, ex:Ljava/lang/InstantiationException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@1b
    const-string v3, "com.android.internal.policy.impl.Policy could not be instantiated"

    #@1d
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@20
    throw v2

    #@21
    .line 48
    .end local v0           #ex:Ljava/lang/InstantiationException;
    :catch_21
    move-exception v0

    #@22
    .line 49
    .local v0, ex:Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@24
    const-string v3, "com.android.internal.policy.impl.Policy could not be instantiated"

    #@26
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@29
    throw v2
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static makeNewFallbackEventHandler(Landroid/content/Context;)Landroid/view/FallbackEventHandler;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 71
    sget-object v0, Lcom/android/internal/policy/PolicyManager;->sPolicy:Lcom/android/internal/policy/IPolicy;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/policy/IPolicy;->makeNewFallbackEventHandler(Landroid/content/Context;)Landroid/view/FallbackEventHandler;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static makeNewLayoutInflater(Landroid/content/Context;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 63
    sget-object v0, Lcom/android/internal/policy/PolicyManager;->sPolicy:Lcom/android/internal/policy/IPolicy;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/policy/IPolicy;->makeNewLayoutInflater(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static makeNewWindow(Landroid/content/Context;)Landroid/view/Window;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 59
    sget-object v0, Lcom/android/internal/policy/PolicyManager;->sPolicy:Lcom/android/internal/policy/IPolicy;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/policy/IPolicy;->makeNewWindow(Landroid/content/Context;)Landroid/view/Window;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static makeNewWindowManager()Landroid/view/WindowManagerPolicy;
    .registers 1

    #@0
    .prologue
    .line 67
    sget-object v0, Lcom/android/internal/policy/PolicyManager;->sPolicy:Lcom/android/internal/policy/IPolicy;

    #@2
    invoke-interface {v0}, Lcom/android/internal/policy/IPolicy;->makeNewWindowManager()Landroid/view/WindowManagerPolicy;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
