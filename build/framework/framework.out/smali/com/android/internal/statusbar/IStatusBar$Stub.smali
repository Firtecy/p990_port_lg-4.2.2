.class public abstract Lcom/android/internal/statusbar/IStatusBar$Stub;
.super Landroid/os/Binder;
.source "IStatusBar.java"

# interfaces
.implements Lcom/android/internal/statusbar/IStatusBar;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/statusbar/IStatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/statusbar/IStatusBar$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.statusbar.IStatusBar"

.field static final TRANSACTION_addNotification:I = 0x3

.field static final TRANSACTION_animateCollapsePanels:I = 0x9

.field static final TRANSACTION_animateExpandNotificationsPanel:I = 0x7

.field static final TRANSACTION_animateExpandSettingsPanel:I = 0x8

.field static final TRANSACTION_cancelPreloadRecentApps:I = 0x10

.field static final TRANSACTION_disable:I = 0x6

.field static final TRANSACTION_disableTouch:I = 0x11

.field static final TRANSACTION_preloadRecentApps:I = 0xf

.field static final TRANSACTION_removeIcon:I = 0x2

.field static final TRANSACTION_removeNotification:I = 0x5

.field static final TRANSACTION_setHardKeyboardStatus:I = 0xd

.field static final TRANSACTION_setIcon:I = 0x1

.field static final TRANSACTION_setImeWindowStatus:I = 0xc

.field static final TRANSACTION_setNavigationBackground:I = 0x13

.field static final TRANSACTION_setNavigationRotation:I = 0x14

.field static final TRANSACTION_setSystemBarType:I = 0x12

.field static final TRANSACTION_setSystemUiVisibility:I = 0xa

.field static final TRANSACTION_toggleRecentApps:I = 0xe

.field static final TRANSACTION_topAppWindowChanged:I = 0xb

.field static final TRANSACTION_updateNotification:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/statusbar/IStatusBar$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBar;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.statusbar.IStatusBar"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/statusbar/IStatusBar;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/statusbar/IStatusBar;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/statusbar/IStatusBar$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/statusbar/IStatusBar$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_178

    #@5
    .line 236
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 43
    :sswitch_a
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v1

    #@19
    .line 52
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_2b

    #@1f
    .line 53
    sget-object v0, Lcom/android/internal/statusbar/StatusBarIcon;->CREATOR:Landroid/os/Parcelable$Creator;

    #@21
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Lcom/android/internal/statusbar/StatusBarIcon;

    #@27
    .line 58
    .local v2, _arg1:Lcom/android/internal/statusbar/StatusBarIcon;
    :goto_27
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/statusbar/IStatusBar$Stub;->setIcon(ILcom/android/internal/statusbar/StatusBarIcon;)V

    #@2a
    goto :goto_9

    #@2b
    .line 56
    .end local v2           #_arg1:Lcom/android/internal/statusbar/StatusBarIcon;
    :cond_2b
    const/4 v2, 0x0

    #@2c
    .restart local v2       #_arg1:Lcom/android/internal/statusbar/StatusBarIcon;
    goto :goto_27

    #@2d
    .line 63
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Lcom/android/internal/statusbar/StatusBarIcon;
    :sswitch_2d
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@2f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v1

    #@36
    .line 66
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/statusbar/IStatusBar$Stub;->removeIcon(I)V

    #@39
    goto :goto_9

    #@3a
    .line 71
    .end local v1           #_arg0:I
    :sswitch_3a
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@3c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3f
    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@42
    move-result-object v1

    #@43
    .line 75
    .local v1, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_55

    #@49
    .line 76
    sget-object v0, Lcom/android/internal/statusbar/StatusBarNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4b
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4e
    move-result-object v2

    #@4f
    check-cast v2, Lcom/android/internal/statusbar/StatusBarNotification;

    #@51
    .line 81
    .local v2, _arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    :goto_51
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/statusbar/IStatusBar$Stub;->addNotification(Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;)V

    #@54
    goto :goto_9

    #@55
    .line 79
    .end local v2           #_arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    :cond_55
    const/4 v2, 0x0

    #@56
    .restart local v2       #_arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    goto :goto_51

    #@57
    .line 86
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    :sswitch_57
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@59
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5f
    move-result-object v1

    #@60
    .line 90
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v0

    #@64
    if-eqz v0, :cond_72

    #@66
    .line 91
    sget-object v0, Lcom/android/internal/statusbar/StatusBarNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    #@68
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6b
    move-result-object v2

    #@6c
    check-cast v2, Lcom/android/internal/statusbar/StatusBarNotification;

    #@6e
    .line 96
    .restart local v2       #_arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    :goto_6e
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/statusbar/IStatusBar$Stub;->updateNotification(Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;)V

    #@71
    goto :goto_9

    #@72
    .line 94
    .end local v2           #_arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    :cond_72
    const/4 v2, 0x0

    #@73
    .restart local v2       #_arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    goto :goto_6e

    #@74
    .line 101
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Lcom/android/internal/statusbar/StatusBarNotification;
    :sswitch_74
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@76
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@79
    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7c
    move-result-object v1

    #@7d
    .line 104
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v1}, Lcom/android/internal/statusbar/IStatusBar$Stub;->removeNotification(Landroid/os/IBinder;)V

    #@80
    goto :goto_9

    #@81
    .line 109
    .end local v1           #_arg0:Landroid/os/IBinder;
    :sswitch_81
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@83
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@86
    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@89
    move-result v1

    #@8a
    .line 112
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/statusbar/IStatusBar$Stub;->disable(I)V

    #@8d
    goto/16 :goto_9

    #@8f
    .line 117
    .end local v1           #_arg0:I
    :sswitch_8f
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@91
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@94
    .line 118
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBar$Stub;->animateExpandNotificationsPanel()V

    #@97
    goto/16 :goto_9

    #@99
    .line 123
    :sswitch_99
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@9b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9e
    .line 124
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBar$Stub;->animateExpandSettingsPanel()V

    #@a1
    goto/16 :goto_9

    #@a3
    .line 129
    :sswitch_a3
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@a5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a8
    .line 130
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBar$Stub;->animateCollapsePanels()V

    #@ab
    goto/16 :goto_9

    #@ad
    .line 135
    :sswitch_ad
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@af
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b2
    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b5
    move-result v1

    #@b6
    .line 139
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b9
    move-result v2

    #@ba
    .line 140
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/statusbar/IStatusBar$Stub;->setSystemUiVisibility(II)V

    #@bd
    goto/16 :goto_9

    #@bf
    .line 145
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_bf
    const-string v7, "com.android.internal.statusbar.IStatusBar"

    #@c1
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c4
    .line 147
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v7

    #@c8
    if-eqz v7, :cond_d0

    #@ca
    move v1, v6

    #@cb
    .line 148
    .local v1, _arg0:Z
    :goto_cb
    invoke-virtual {p0, v1}, Lcom/android/internal/statusbar/IStatusBar$Stub;->topAppWindowChanged(Z)V

    #@ce
    goto/16 :goto_9

    #@d0
    .end local v1           #_arg0:Z
    :cond_d0
    move v1, v0

    #@d1
    .line 147
    goto :goto_cb

    #@d2
    .line 153
    :sswitch_d2
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@d4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d7
    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@da
    move-result-object v1

    #@db
    .line 157
    .local v1, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@de
    move-result v2

    #@df
    .line 159
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e2
    move-result v3

    #@e3
    .line 160
    .local v3, _arg2:I
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/statusbar/IStatusBar$Stub;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@e6
    goto/16 :goto_9

    #@e8
    .line 165
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_e8
    const-string v7, "com.android.internal.statusbar.IStatusBar"

    #@ea
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ed
    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f0
    move-result v7

    #@f1
    if-eqz v7, :cond_100

    #@f3
    move v1, v6

    #@f4
    .line 169
    .local v1, _arg0:Z
    :goto_f4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f7
    move-result v7

    #@f8
    if-eqz v7, :cond_102

    #@fa
    move v2, v6

    #@fb
    .line 170
    .local v2, _arg1:Z
    :goto_fb
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/statusbar/IStatusBar$Stub;->setHardKeyboardStatus(ZZ)V

    #@fe
    goto/16 :goto_9

    #@100
    .end local v1           #_arg0:Z
    .end local v2           #_arg1:Z
    :cond_100
    move v1, v0

    #@101
    .line 167
    goto :goto_f4

    #@102
    .restart local v1       #_arg0:Z
    :cond_102
    move v2, v0

    #@103
    .line 169
    goto :goto_fb

    #@104
    .line 175
    .end local v1           #_arg0:Z
    :sswitch_104
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@106
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@109
    .line 176
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBar$Stub;->toggleRecentApps()V

    #@10c
    goto/16 :goto_9

    #@10e
    .line 181
    :sswitch_10e
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@110
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@113
    .line 182
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBar$Stub;->preloadRecentApps()V

    #@116
    goto/16 :goto_9

    #@118
    .line 187
    :sswitch_118
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@11a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11d
    .line 188
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBar$Stub;->cancelPreloadRecentApps()V

    #@120
    goto/16 :goto_9

    #@122
    .line 193
    :sswitch_122
    const-string v7, "com.android.internal.statusbar.IStatusBar"

    #@124
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@127
    .line 195
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12a
    move-result v7

    #@12b
    if-eqz v7, :cond_133

    #@12d
    move v1, v6

    #@12e
    .line 196
    .restart local v1       #_arg0:Z
    :goto_12e
    invoke-virtual {p0, v1}, Lcom/android/internal/statusbar/IStatusBar$Stub;->disableTouch(Z)V

    #@131
    goto/16 :goto_9

    #@133
    .end local v1           #_arg0:Z
    :cond_133
    move v1, v0

    #@134
    .line 195
    goto :goto_12e

    #@135
    .line 201
    :sswitch_135
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@137
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13a
    .line 203
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@13d
    move-result v1

    #@13e
    .line 204
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/statusbar/IStatusBar$Stub;->setSystemBarType(I)V

    #@141
    goto/16 :goto_9

    #@143
    .line 209
    .end local v1           #_arg0:I
    :sswitch_143
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@145
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@148
    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14b
    move-result-object v1

    #@14c
    .line 213
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14f
    move-result v2

    #@150
    .line 215
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@153
    move-result v3

    #@154
    .line 217
    .restart local v3       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@157
    move-result v4

    #@158
    .line 219
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15b
    move-result v5

    #@15c
    .local v5, _arg4:I
    move-object v0, p0

    #@15d
    .line 220
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/statusbar/IStatusBar$Stub;->setNavigationBackground(Ljava/lang/String;IIII)V

    #@160
    goto/16 :goto_9

    #@162
    .line 225
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    :sswitch_162
    const-string v0, "com.android.internal.statusbar.IStatusBar"

    #@164
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@167
    .line 227
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16a
    move-result v1

    #@16b
    .line 229
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16e
    move-result v2

    #@16f
    .line 231
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@172
    move-result v3

    #@173
    .line 232
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/statusbar/IStatusBar$Stub;->setNavigationRotation(III)V

    #@176
    goto/16 :goto_9

    #@178
    .line 39
    :sswitch_data_178
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_3a
        0x4 -> :sswitch_57
        0x5 -> :sswitch_74
        0x6 -> :sswitch_81
        0x7 -> :sswitch_8f
        0x8 -> :sswitch_99
        0x9 -> :sswitch_a3
        0xa -> :sswitch_ad
        0xb -> :sswitch_bf
        0xc -> :sswitch_d2
        0xd -> :sswitch_e8
        0xe -> :sswitch_104
        0xf -> :sswitch_10e
        0x10 -> :sswitch_118
        0x11 -> :sswitch_122
        0x12 -> :sswitch_135
        0x13 -> :sswitch_143
        0x14 -> :sswitch_162
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
