.class public Lcom/android/internal/statusbar/StatusBarNotification;
.super Ljava/lang/Object;
.source "StatusBarNotification.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/statusbar/StatusBarNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final id:I

.field public final initialPid:I

.field public final notification:Landroid/app/Notification;

.field public final pkg:Ljava/lang/String;

.field public final score:I

.field public final tag:Ljava/lang/String;

.field public final uid:I

.field public final user:Landroid/os/UserHandle;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 110
    new-instance v0, Lcom/android/internal/statusbar/StatusBarNotification$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/statusbar/StatusBarNotification$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/statusbar/StatusBarNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->pkg:Ljava/lang/String;

    #@9
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->id:I

    #@f
    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_42

    #@15
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->tag:Ljava/lang/String;

    #@1b
    .line 82
    :goto_1b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->uid:I

    #@21
    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v0

    #@25
    iput v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->initialPid:I

    #@27
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->score:I

    #@2d
    .line 85
    new-instance v0, Landroid/app/Notification;

    #@2f
    invoke-direct {v0, p1}, Landroid/app/Notification;-><init>(Landroid/os/Parcel;)V

    #@32
    iput-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@34
    .line 86
    invoke-static {p1}, Landroid/os/UserHandle;->readFromParcel(Landroid/os/Parcel;)Landroid/os/UserHandle;

    #@37
    move-result-object v0

    #@38
    iput-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->user:Landroid/os/UserHandle;

    #@3a
    .line 87
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@3c
    iget-object v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->user:Landroid/os/UserHandle;

    #@3e
    invoke-virtual {v0, v1}, Landroid/app/Notification;->setUser(Landroid/os/UserHandle;)V

    #@41
    .line 88
    return-void

    #@42
    .line 80
    :cond_42
    const/4 v0, 0x0

    #@43
    iput-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->tag:Ljava/lang/String;

    #@45
    goto :goto_1b
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;IIILandroid/app/Notification;)V
    .registers 17
    .parameter "pkg"
    .parameter "id"
    .parameter "tag"
    .parameter "uid"
    .parameter "initialPid"
    .parameter "score"
    .parameter "notification"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 55
    sget-object v8, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move-object v3, p3

    #@6
    move v4, p4

    #@7
    move v5, p5

    #@8
    move v6, p6

    #@9
    move-object/from16 v7, p7

    #@b
    invoke-direct/range {v0 .. v8}, Lcom/android/internal/statusbar/StatusBarNotification;-><init>(Ljava/lang/String;ILjava/lang/String;IIILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@e
    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;IIILandroid/app/Notification;Landroid/os/UserHandle;)V
    .registers 10
    .parameter "pkg"
    .parameter "id"
    .parameter "tag"
    .parameter "uid"
    .parameter "initialPid"
    .parameter "score"
    .parameter "notification"
    .parameter "user"

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    if-nez p1, :cond_b

    #@5
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@a
    throw v0

    #@b
    .line 61
    :cond_b
    if-nez p7, :cond_13

    #@d
    new-instance v0, Ljava/lang/NullPointerException;

    #@f
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@12
    throw v0

    #@13
    .line 63
    :cond_13
    iput-object p1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->pkg:Ljava/lang/String;

    #@15
    .line 64
    iput p2, p0, Lcom/android/internal/statusbar/StatusBarNotification;->id:I

    #@17
    .line 65
    iput-object p3, p0, Lcom/android/internal/statusbar/StatusBarNotification;->tag:Ljava/lang/String;

    #@19
    .line 66
    iput p4, p0, Lcom/android/internal/statusbar/StatusBarNotification;->uid:I

    #@1b
    .line 67
    iput p5, p0, Lcom/android/internal/statusbar/StatusBarNotification;->initialPid:I

    #@1d
    .line 68
    iput p6, p0, Lcom/android/internal/statusbar/StatusBarNotification;->score:I

    #@1f
    .line 69
    iput-object p7, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@21
    .line 70
    iput-object p8, p0, Lcom/android/internal/statusbar/StatusBarNotification;->user:Landroid/os/UserHandle;

    #@23
    .line 71
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@25
    invoke-virtual {v0, p8}, Landroid/app/Notification;->setUser(Landroid/os/UserHandle;)V

    #@28
    .line 72
    return-void
.end method


# virtual methods
.method public clone()Lcom/android/internal/statusbar/StatusBarNotification;
    .registers 10

    #@0
    .prologue
    .line 126
    new-instance v0, Lcom/android/internal/statusbar/StatusBarNotification;

    #@2
    iget-object v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->pkg:Ljava/lang/String;

    #@4
    iget v2, p0, Lcom/android/internal/statusbar/StatusBarNotification;->id:I

    #@6
    iget-object v3, p0, Lcom/android/internal/statusbar/StatusBarNotification;->tag:Ljava/lang/String;

    #@8
    iget v4, p0, Lcom/android/internal/statusbar/StatusBarNotification;->uid:I

    #@a
    iget v5, p0, Lcom/android/internal/statusbar/StatusBarNotification;->initialPid:I

    #@c
    iget v6, p0, Lcom/android/internal/statusbar/StatusBarNotification;->score:I

    #@e
    iget-object v7, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@10
    invoke-virtual {v7}, Landroid/app/Notification;->clone()Landroid/app/Notification;

    #@13
    move-result-object v7

    #@14
    iget-object v8, p0, Lcom/android/internal/statusbar/StatusBarNotification;->user:Landroid/os/UserHandle;

    #@16
    invoke-direct/range {v0 .. v8}, Lcom/android/internal/statusbar/StatusBarNotification;-><init>(Ljava/lang/String;ILjava/lang/String;IIILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@19
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/android/internal/statusbar/StatusBarNotification;->clone()Lcom/android/internal/statusbar/StatusBarNotification;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 107
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getUserId()I
    .registers 2

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->user:Landroid/os/UserHandle;

    #@2
    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isClearable()Z
    .registers 2

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@2
    iget v0, v0, Landroid/app/Notification;->flags:I

    #@4
    and-int/lit8 v0, v0, 0x2

    #@6
    if-nez v0, :cond_12

    #@8
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@a
    iget v0, v0, Landroid/app/Notification;->flags:I

    #@c
    and-int/lit8 v0, v0, 0x20

    #@e
    if-nez v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public isOngoing()Z
    .registers 2

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@2
    iget v0, v0, Landroid/app/Notification;->flags:I

    #@4
    and-int/lit8 v0, v0, 0x2

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "StatusBarNotification(pkg="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->pkg:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " id="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->id:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " tag="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->tag:Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " score="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->score:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " notn="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, " user="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget-object v1, p0, Lcom/android/internal/statusbar/StatusBarNotification;->user:Landroid/os/UserHandle;

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, ")"

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v0

    #@57
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->pkg:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 92
    iget v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->id:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 93
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->tag:Ljava/lang/String;

    #@c
    if-eqz v0, :cond_31

    #@e
    .line 94
    const/4 v0, 0x1

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 95
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->tag:Ljava/lang/String;

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 99
    :goto_17
    iget v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->uid:I

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 100
    iget v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->initialPid:I

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 101
    iget v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->score:I

    #@23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 102
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@28
    invoke-virtual {v0, p1, p2}, Landroid/app/Notification;->writeToParcel(Landroid/os/Parcel;I)V

    #@2b
    .line 103
    iget-object v0, p0, Lcom/android/internal/statusbar/StatusBarNotification;->user:Landroid/os/UserHandle;

    #@2d
    invoke-virtual {v0, p1, p2}, Landroid/os/UserHandle;->writeToParcel(Landroid/os/Parcel;I)V

    #@30
    .line 104
    return-void

    #@31
    .line 97
    :cond_31
    const/4 v0, 0x0

    #@32
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@35
    goto :goto_17
.end method
