.class public abstract Lcom/android/internal/statusbar/IStatusBarService$Stub;
.super Landroid/os/Binder;
.source "IStatusBarService.java"

# interfaces
.implements Lcom/android/internal/statusbar/IStatusBarService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/statusbar/IStatusBarService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/statusbar/IStatusBarService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.statusbar.IStatusBarService"

.field static final TRANSACTION_cancelPreloadRecentApps:I = 0x15

.field static final TRANSACTION_collapsePanels:I = 0x2

.field static final TRANSACTION_disable:I = 0x3

.field static final TRANSACTION_disableTouch:I = 0x16

.field static final TRANSACTION_expandNotificationsPanel:I = 0x1

.field static final TRANSACTION_expandSettingsPanel:I = 0x9

.field static final TRANSACTION_onClearAllNotifications:I = 0xf

.field static final TRANSACTION_onNotificationClear:I = 0x10

.field static final TRANSACTION_onNotificationClick:I = 0xd

.field static final TRANSACTION_onNotificationError:I = 0xe

.field static final TRANSACTION_onPanelRevealed:I = 0xc

.field static final TRANSACTION_preloadRecentApps:I = 0x14

.field static final TRANSACTION_registerStatusBar:I = 0xb

.field static final TRANSACTION_removeIcon:I = 0x6

.field static final TRANSACTION_setCurrentUser:I = 0xa

.field static final TRANSACTION_setHardKeyboardEnabled:I = 0x12

.field static final TRANSACTION_setIcon:I = 0x4

.field static final TRANSACTION_setIconVisibility:I = 0x5

.field static final TRANSACTION_setImeWindowStatus:I = 0x8

.field static final TRANSACTION_setNavigationBackground:I = 0x18

.field static final TRANSACTION_setNavigationRotation:I = 0x19

.field static final TRANSACTION_setSystemBarType:I = 0x17

.field static final TRANSACTION_setSystemUiVisibility:I = 0x11

.field static final TRANSACTION_toggleRecentApps:I = 0x13

.field static final TRANSACTION_topAppWindowChanged:I = 0x7


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.statusbar.IStatusBarService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBarService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/statusbar/IStatusBarService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/statusbar/IStatusBarService;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/statusbar/IStatusBarService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 20
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 39
    sparse-switch p1, :sswitch_data_2aa

    #@3
    .line 333
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 43
    :sswitch_8
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 44
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 48
    :sswitch_11
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 49
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->expandNotificationsPanel()V

    #@1b
    .line 50
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e
    .line 51
    const/4 v1, 0x1

    #@1f
    goto :goto_7

    #@20
    .line 55
    :sswitch_20
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@22
    move-object/from16 v0, p2

    #@24
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27
    .line 56
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->collapsePanels()V

    #@2a
    .line 57
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d
    .line 58
    const/4 v1, 0x1

    #@2e
    goto :goto_7

    #@2f
    .line 62
    :sswitch_2f
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@31
    move-object/from16 v0, p2

    #@33
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 64
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v2

    #@3a
    .line 66
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3d
    move-result-object v3

    #@3e
    .line 68
    .local v3, _arg1:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    .line 69
    .local v4, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->disable(ILandroid/os/IBinder;Ljava/lang/String;)V

    #@45
    .line 70
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    .line 71
    const/4 v1, 0x1

    #@49
    goto :goto_7

    #@4a
    .line 75
    .end local v2           #_arg0:I
    .end local v3           #_arg1:Landroid/os/IBinder;
    .end local v4           #_arg2:Ljava/lang/String;
    :sswitch_4a
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@4c
    move-object/from16 v0, p2

    #@4e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@51
    .line 77
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    .line 79
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    .line 81
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5c
    move-result v4

    #@5d
    .line 83
    .local v4, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@60
    move-result v5

    #@61
    .line 85
    .local v5, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    .local v6, _arg4:Ljava/lang/String;
    move-object v1, p0

    #@66
    .line 86
    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setIcon(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    #@69
    .line 87
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6c
    .line 88
    const/4 v1, 0x1

    #@6d
    goto :goto_7

    #@6e
    .line 92
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Ljava/lang/String;
    :sswitch_6e
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@70
    move-object/from16 v0, p2

    #@72
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@75
    .line 94
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    .line 96
    .restart local v2       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7c
    move-result v1

    #@7d
    if-eqz v1, :cond_88

    #@7f
    const/4 v3, 0x1

    #@80
    .line 97
    .local v3, _arg1:Z
    :goto_80
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setIconVisibility(Ljava/lang/String;Z)V

    #@83
    .line 98
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@86
    .line 99
    const/4 v1, 0x1

    #@87
    goto :goto_7

    #@88
    .line 96
    .end local v3           #_arg1:Z
    :cond_88
    const/4 v3, 0x0

    #@89
    goto :goto_80

    #@8a
    .line 103
    .end local v2           #_arg0:Ljava/lang/String;
    :sswitch_8a
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@8c
    move-object/from16 v0, p2

    #@8e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@91
    .line 105
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@94
    move-result-object v2

    #@95
    .line 106
    .restart local v2       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->removeIcon(Ljava/lang/String;)V

    #@98
    .line 107
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9b
    .line 108
    const/4 v1, 0x1

    #@9c
    goto/16 :goto_7

    #@9e
    .line 112
    .end local v2           #_arg0:Ljava/lang/String;
    :sswitch_9e
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@a0
    move-object/from16 v0, p2

    #@a2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a5
    .line 114
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v1

    #@a9
    if-eqz v1, :cond_b5

    #@ab
    const/4 v2, 0x1

    #@ac
    .line 115
    .local v2, _arg0:Z
    :goto_ac
    invoke-virtual {p0, v2}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->topAppWindowChanged(Z)V

    #@af
    .line 116
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b2
    .line 117
    const/4 v1, 0x1

    #@b3
    goto/16 :goto_7

    #@b5
    .line 114
    .end local v2           #_arg0:Z
    :cond_b5
    const/4 v2, 0x0

    #@b6
    goto :goto_ac

    #@b7
    .line 121
    :sswitch_b7
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@b9
    move-object/from16 v0, p2

    #@bb
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@be
    .line 123
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c1
    move-result-object v2

    #@c2
    .line 125
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c5
    move-result v3

    #@c6
    .line 127
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c9
    move-result v4

    #@ca
    .line 128
    .restart local v4       #_arg2:I
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@cd
    .line 129
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d0
    .line 130
    const/4 v1, 0x1

    #@d1
    goto/16 :goto_7

    #@d3
    .line 134
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :sswitch_d3
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@d5
    move-object/from16 v0, p2

    #@d7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@da
    .line 135
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->expandSettingsPanel()V

    #@dd
    .line 136
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@e0
    .line 137
    const/4 v1, 0x1

    #@e1
    goto/16 :goto_7

    #@e3
    .line 141
    :sswitch_e3
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@e5
    move-object/from16 v0, p2

    #@e7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ea
    .line 143
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ed
    move-result v2

    #@ee
    .line 144
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setCurrentUser(I)V

    #@f1
    .line 145
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f4
    .line 146
    const/4 v1, 0x1

    #@f5
    goto/16 :goto_7

    #@f7
    .line 150
    .end local v2           #_arg0:I
    :sswitch_f7
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@f9
    move-object/from16 v0, p2

    #@fb
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fe
    .line 152
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@101
    move-result-object v1

    #@102
    invoke-static {v1}, Lcom/android/internal/statusbar/IStatusBar$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBar;

    #@105
    move-result-object v2

    #@106
    .line 154
    .local v2, _arg0:Lcom/android/internal/statusbar/IStatusBar;
    new-instance v3, Lcom/android/internal/statusbar/StatusBarIconList;

    #@108
    invoke-direct {v3}, Lcom/android/internal/statusbar/StatusBarIconList;-><init>()V

    #@10b
    .line 156
    .local v3, _arg1:Lcom/android/internal/statusbar/StatusBarIconList;
    new-instance v10, Ljava/util/ArrayList;

    #@10d
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@110
    .line 158
    .local v10, _arg2:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    new-instance v11, Ljava/util/ArrayList;

    #@112
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    #@115
    .line 160
    .local v11, _arg3:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/statusbar/StatusBarNotification;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@118
    move-result v14

    #@119
    .line 161
    .local v14, _arg4_length:I
    if-gez v14, :cond_150

    #@11b
    .line 162
    const/4 v6, 0x0

    #@11c
    .line 168
    .local v6, _arg4:[I
    :goto_11c
    new-instance v13, Ljava/util/ArrayList;

    #@11e
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    #@121
    .local v13, _arg5:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    move-object v7, p0

    #@122
    move-object v8, v2

    #@123
    move-object v9, v3

    #@124
    move-object v12, v6

    #@125
    .line 169
    invoke-virtual/range {v7 .. v13}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->registerStatusBar(Lcom/android/internal/statusbar/IStatusBar;Lcom/android/internal/statusbar/StatusBarIconList;Ljava/util/List;Ljava/util/List;[ILjava/util/List;)V

    #@128
    .line 170
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12b
    .line 171
    if-eqz v3, :cond_153

    #@12d
    .line 172
    const/4 v1, 0x1

    #@12e
    move-object/from16 v0, p3

    #@130
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@133
    .line 173
    const/4 v1, 0x1

    #@134
    move-object/from16 v0, p3

    #@136
    invoke-virtual {v3, v0, v1}, Lcom/android/internal/statusbar/StatusBarIconList;->writeToParcel(Landroid/os/Parcel;I)V

    #@139
    .line 178
    :goto_139
    move-object/from16 v0, p3

    #@13b
    invoke-virtual {v0, v10}, Landroid/os/Parcel;->writeBinderList(Ljava/util/List;)V

    #@13e
    .line 179
    move-object/from16 v0, p3

    #@140
    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@143
    .line 180
    move-object/from16 v0, p3

    #@145
    invoke-virtual {v0, v6}, Landroid/os/Parcel;->writeIntArray([I)V

    #@148
    .line 181
    move-object/from16 v0, p3

    #@14a
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeBinderList(Ljava/util/List;)V

    #@14d
    .line 182
    const/4 v1, 0x1

    #@14e
    goto/16 :goto_7

    #@150
    .line 165
    .end local v6           #_arg4:[I
    .end local v13           #_arg5:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    :cond_150
    new-array v6, v14, [I

    #@152
    .restart local v6       #_arg4:[I
    goto :goto_11c

    #@153
    .line 176
    .restart local v13       #_arg5:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    :cond_153
    const/4 v1, 0x0

    #@154
    move-object/from16 v0, p3

    #@156
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@159
    goto :goto_139

    #@15a
    .line 186
    .end local v2           #_arg0:Lcom/android/internal/statusbar/IStatusBar;
    .end local v3           #_arg1:Lcom/android/internal/statusbar/StatusBarIconList;
    .end local v6           #_arg4:[I
    .end local v10           #_arg2:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    .end local v11           #_arg3:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/statusbar/StatusBarNotification;>;"
    .end local v13           #_arg5:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    .end local v14           #_arg4_length:I
    :sswitch_15a
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@15c
    move-object/from16 v0, p2

    #@15e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@161
    .line 187
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->onPanelRevealed()V

    #@164
    .line 188
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@167
    .line 189
    const/4 v1, 0x1

    #@168
    goto/16 :goto_7

    #@16a
    .line 193
    :sswitch_16a
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@16c
    move-object/from16 v0, p2

    #@16e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@171
    .line 195
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@174
    move-result-object v2

    #@175
    .line 197
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@178
    move-result-object v3

    #@179
    .line 199
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@17c
    move-result v4

    #@17d
    .line 200
    .restart local v4       #_arg2:I
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->onNotificationClick(Ljava/lang/String;Ljava/lang/String;I)V

    #@180
    .line 201
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@183
    .line 202
    const/4 v1, 0x1

    #@184
    goto/16 :goto_7

    #@186
    .line 206
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    :sswitch_186
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@188
    move-object/from16 v0, p2

    #@18a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18d
    .line 208
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@190
    move-result-object v2

    #@191
    .line 210
    .restart local v2       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@194
    move-result-object v3

    #@195
    .line 212
    .restart local v3       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@198
    move-result v4

    #@199
    .line 214
    .restart local v4       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@19c
    move-result v5

    #@19d
    .line 216
    .restart local v5       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a0
    move-result v6

    #@1a1
    .line 218
    .local v6, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1a4
    move-result-object v7

    #@1a5
    .local v7, _arg5:Ljava/lang/String;
    move-object v1, p0

    #@1a6
    .line 219
    invoke-virtual/range {v1 .. v7}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->onNotificationError(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    #@1a9
    .line 220
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ac
    .line 221
    const/4 v1, 0x1

    #@1ad
    goto/16 :goto_7

    #@1af
    .line 225
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:Ljava/lang/String;
    :sswitch_1af
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@1b1
    move-object/from16 v0, p2

    #@1b3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b6
    .line 226
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->onClearAllNotifications()V

    #@1b9
    .line 227
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1bc
    .line 228
    const/4 v1, 0x1

    #@1bd
    goto/16 :goto_7

    #@1bf
    .line 232
    :sswitch_1bf
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@1c1
    move-object/from16 v0, p2

    #@1c3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c6
    .line 234
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c9
    move-result-object v2

    #@1ca
    .line 236
    .restart local v2       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1cd
    move-result-object v3

    #@1ce
    .line 238
    .restart local v3       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1d1
    move-result v4

    #@1d2
    .line 239
    .restart local v4       #_arg2:I
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->onNotificationClear(Ljava/lang/String;Ljava/lang/String;I)V

    #@1d5
    .line 240
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d8
    .line 241
    const/4 v1, 0x1

    #@1d9
    goto/16 :goto_7

    #@1db
    .line 245
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    :sswitch_1db
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@1dd
    move-object/from16 v0, p2

    #@1df
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e2
    .line 247
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e5
    move-result v2

    #@1e6
    .line 249
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e9
    move-result v3

    #@1ea
    .line 250
    .local v3, _arg1:I
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setSystemUiVisibility(II)V

    #@1ed
    .line 251
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f0
    .line 252
    const/4 v1, 0x1

    #@1f1
    goto/16 :goto_7

    #@1f3
    .line 256
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    :sswitch_1f3
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@1f5
    move-object/from16 v0, p2

    #@1f7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1fa
    .line 258
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1fd
    move-result v1

    #@1fe
    if-eqz v1, :cond_20a

    #@200
    const/4 v2, 0x1

    #@201
    .line 259
    .local v2, _arg0:Z
    :goto_201
    invoke-virtual {p0, v2}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setHardKeyboardEnabled(Z)V

    #@204
    .line 260
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@207
    .line 261
    const/4 v1, 0x1

    #@208
    goto/16 :goto_7

    #@20a
    .line 258
    .end local v2           #_arg0:Z
    :cond_20a
    const/4 v2, 0x0

    #@20b
    goto :goto_201

    #@20c
    .line 265
    :sswitch_20c
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@20e
    move-object/from16 v0, p2

    #@210
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@213
    .line 266
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->toggleRecentApps()V

    #@216
    .line 267
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@219
    .line 268
    const/4 v1, 0x1

    #@21a
    goto/16 :goto_7

    #@21c
    .line 272
    :sswitch_21c
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@21e
    move-object/from16 v0, p2

    #@220
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@223
    .line 273
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->preloadRecentApps()V

    #@226
    .line 274
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@229
    .line 275
    const/4 v1, 0x1

    #@22a
    goto/16 :goto_7

    #@22c
    .line 279
    :sswitch_22c
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@22e
    move-object/from16 v0, p2

    #@230
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@233
    .line 280
    invoke-virtual {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->cancelPreloadRecentApps()V

    #@236
    .line 281
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@239
    .line 282
    const/4 v1, 0x1

    #@23a
    goto/16 :goto_7

    #@23c
    .line 286
    :sswitch_23c
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@23e
    move-object/from16 v0, p2

    #@240
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@243
    .line 288
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@246
    move-result v1

    #@247
    if-eqz v1, :cond_253

    #@249
    const/4 v2, 0x1

    #@24a
    .line 289
    .restart local v2       #_arg0:Z
    :goto_24a
    invoke-virtual {p0, v2}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->disableTouch(Z)V

    #@24d
    .line 290
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@250
    .line 291
    const/4 v1, 0x1

    #@251
    goto/16 :goto_7

    #@253
    .line 288
    .end local v2           #_arg0:Z
    :cond_253
    const/4 v2, 0x0

    #@254
    goto :goto_24a

    #@255
    .line 295
    :sswitch_255
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@257
    move-object/from16 v0, p2

    #@259
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25c
    .line 297
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@25f
    move-result v2

    #@260
    .line 298
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setSystemBarType(I)V

    #@263
    .line 299
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@266
    .line 300
    const/4 v1, 0x1

    #@267
    goto/16 :goto_7

    #@269
    .line 304
    .end local v2           #_arg0:I
    :sswitch_269
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@26b
    move-object/from16 v0, p2

    #@26d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@270
    .line 306
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@273
    move-result-object v2

    #@274
    .line 308
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@277
    move-result v3

    #@278
    .line 310
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@27b
    move-result v4

    #@27c
    .line 312
    .restart local v4       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@27f
    move-result v5

    #@280
    .line 314
    .restart local v5       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@283
    move-result v6

    #@284
    .restart local v6       #_arg4:I
    move-object v1, p0

    #@285
    .line 315
    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setNavigationBackground(Ljava/lang/String;IIII)V

    #@288
    .line 316
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@28b
    .line 317
    const/4 v1, 0x1

    #@28c
    goto/16 :goto_7

    #@28e
    .line 321
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    :sswitch_28e
    const-string v1, "com.android.internal.statusbar.IStatusBarService"

    #@290
    move-object/from16 v0, p2

    #@292
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@295
    .line 323
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@298
    move-result v2

    #@299
    .line 325
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@29c
    move-result v3

    #@29d
    .line 327
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2a0
    move-result v4

    #@2a1
    .line 328
    .restart local v4       #_arg2:I
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->setNavigationRotation(III)V

    #@2a4
    .line 329
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a7
    .line 330
    const/4 v1, 0x1

    #@2a8
    goto/16 :goto_7

    #@2aa
    .line 39
    :sswitch_data_2aa
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_20
        0x3 -> :sswitch_2f
        0x4 -> :sswitch_4a
        0x5 -> :sswitch_6e
        0x6 -> :sswitch_8a
        0x7 -> :sswitch_9e
        0x8 -> :sswitch_b7
        0x9 -> :sswitch_d3
        0xa -> :sswitch_e3
        0xb -> :sswitch_f7
        0xc -> :sswitch_15a
        0xd -> :sswitch_16a
        0xe -> :sswitch_186
        0xf -> :sswitch_1af
        0x10 -> :sswitch_1bf
        0x11 -> :sswitch_1db
        0x12 -> :sswitch_1f3
        0x13 -> :sswitch_20c
        0x14 -> :sswitch_21c
        0x15 -> :sswitch_22c
        0x16 -> :sswitch_23c
        0x17 -> :sswitch_255
        0x18 -> :sswitch_269
        0x19 -> :sswitch_28e
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
