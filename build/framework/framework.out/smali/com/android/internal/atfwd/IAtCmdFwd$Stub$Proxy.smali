.class Lcom/android/internal/atfwd/IAtCmdFwd$Stub$Proxy;
.super Ljava/lang/Object;
.source "IAtCmdFwd.java"

# interfaces
.implements Lcom/android/internal/atfwd/IAtCmdFwd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/atfwd/IAtCmdFwd$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    iput-object p1, p0, Lcom/android/internal/atfwd/IAtCmdFwd$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 76
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/atfwd/IAtCmdFwd$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 83
    const-string v0, "com.android.internal.atfwd.IAtCmdFwd"

    #@2
    return-object v0
.end method

.method public processAtCmd(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 8
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 87
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 88
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 91
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.atfwd.IAtCmdFwd"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 92
    if-eqz p1, :cond_36

    #@f
    .line 93
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 94
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Lcom/android/internal/atfwd/AtCmd;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 99
    :goto_17
    iget-object v3, p0, Lcom/android/internal/atfwd/IAtCmdFwd$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x1

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 100
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 101
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_43

    #@27
    .line 102
    sget-object v3, Lcom/android/internal/atfwd/AtCmdResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c
    move-result-object v2

    #@2d
    check-cast v2, Lcom/android/internal/atfwd/AtCmdResponse;
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_3b

    #@2f
    .line 109
    .local v2, _result:Lcom/android/internal/atfwd/AtCmdResponse;
    :goto_2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 110
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 112
    return-object v2

    #@36
    .line 97
    .end local v2           #_result:Lcom/android/internal/atfwd/AtCmdResponse;
    :cond_36
    const/4 v3, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_17

    #@3b
    .line 109
    :catchall_3b
    move-exception v3

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 110
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v3

    #@43
    .line 105
    :cond_43
    const/4 v2, 0x0

    #@44
    .restart local v2       #_result:Lcom/android/internal/atfwd/AtCmdResponse;
    goto :goto_2f
.end method
