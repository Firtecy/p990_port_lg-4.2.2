.class public abstract Lcom/android/internal/atfwd/IAtCmdFwd$Stub;
.super Landroid/os/Binder;
.source "IAtCmdFwd.java"

# interfaces
.implements Lcom/android/internal/atfwd/IAtCmdFwd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/atfwd/IAtCmdFwd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/atfwd/IAtCmdFwd$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.atfwd.IAtCmdFwd"

.field static final TRANSACTION_processAtCmd:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.atfwd.IAtCmdFwd"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/atfwd/IAtCmdFwd$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/atfwd/IAtCmdFwd;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.atfwd.IAtCmdFwd"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/atfwd/IAtCmdFwd;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/atfwd/IAtCmdFwd;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/atfwd/IAtCmdFwd$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/atfwd/IAtCmdFwd$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 39
    sparse-switch p1, :sswitch_data_3a

    #@4
    .line 68
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 43
    :sswitch_9
    const-string v3, "com.android.internal.atfwd.IAtCmdFwd"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 48
    :sswitch_f
    const-string v3, "com.android.internal.atfwd.IAtCmdFwd"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_32

    #@1a
    .line 51
    sget-object v3, Lcom/android/internal/atfwd/AtCmd;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Lcom/android/internal/atfwd/AtCmd;

    #@22
    .line 56
    .local v0, _arg0:Lcom/android/internal/atfwd/AtCmd;
    :goto_22
    invoke-virtual {p0, v0}, Lcom/android/internal/atfwd/IAtCmdFwd$Stub;->processAtCmd(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;

    #@25
    move-result-object v1

    #@26
    .line 57
    .local v1, _result:Lcom/android/internal/atfwd/AtCmdResponse;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@29
    .line 58
    if-eqz v1, :cond_34

    #@2b
    .line 59
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 60
    invoke-virtual {v1, p3, v2}, Lcom/android/internal/atfwd/AtCmdResponse;->writeToParcel(Landroid/os/Parcel;I)V

    #@31
    goto :goto_8

    #@32
    .line 54
    .end local v0           #_arg0:Lcom/android/internal/atfwd/AtCmd;
    .end local v1           #_result:Lcom/android/internal/atfwd/AtCmdResponse;
    :cond_32
    const/4 v0, 0x0

    #@33
    .restart local v0       #_arg0:Lcom/android/internal/atfwd/AtCmd;
    goto :goto_22

    #@34
    .line 63
    .restart local v1       #_result:Lcom/android/internal/atfwd/AtCmdResponse;
    :cond_34
    const/4 v3, 0x0

    #@35
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    goto :goto_8

    #@39
    .line 39
    nop

    #@3a
    :sswitch_data_3a
    .sparse-switch
        0x1 -> :sswitch_f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
