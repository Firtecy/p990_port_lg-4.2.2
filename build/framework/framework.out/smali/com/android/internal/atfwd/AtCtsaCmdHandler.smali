.class public Lcom/android/internal/atfwd/AtCtsaCmdHandler;
.super Lcom/android/internal/atfwd/AtCmdBaseHandler;
.source "AtCtsaCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    }
.end annotation


# static fields
.field private static final DEFAULT_PAUSE_TIME:I = 0x190

.field private static final DEFAULT_PRESS_TIME:I = 0xc8

.field private static final EVT_CTSA_CMD:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "AtCtsaCmdHandler"


# instance fields
.field private mEventQ:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;",
            ">;"
        }
    .end annotation
.end field

.field private mInjectHandler:Landroid/os/Handler;

.field private mInjectThread:Landroid/os/HandlerThread;

.field private mWm:Landroid/view/IWindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdHandlerInstantiationException;
        }
    .end annotation

    #@0
    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmdBaseHandler;-><init>(Landroid/content/Context;)V

    #@3
    .line 172
    const-string/jumbo v1, "window"

    #@6
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v1

    #@a
    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@d
    move-result-object v0

    #@e
    .line 174
    .local v0, service:Landroid/view/IWindowManager;
    if-nez v0, :cond_18

    #@10
    .line 175
    new-instance v1, Ljava/lang/RuntimeException;

    #@12
    const-string v2, "Unable to connect to Window Service"

    #@14
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1

    #@18
    .line 176
    :cond_18
    iput-object v0, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mWm:Landroid/view/IWindowManager;

    #@1a
    .line 177
    new-instance v1, Ljava/util/LinkedList;

    #@1c
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    #@1f
    iput-object v1, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mEventQ:Ljava/util/LinkedList;

    #@21
    .line 179
    new-instance v1, Lcom/android/internal/atfwd/AtCtsaCmdHandler$1;

    #@23
    const-string v2, "CTSA Inject Thread"

    #@25
    const/4 v3, -0x8

    #@26
    invoke-direct {v1, p0, v2, v3}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$1;-><init>(Lcom/android/internal/atfwd/AtCtsaCmdHandler;Ljava/lang/String;I)V

    #@29
    iput-object v1, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mInjectThread:Landroid/os/HandlerThread;

    #@2b
    .line 182
    iget-object v1, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mInjectThread:Landroid/os/HandlerThread;

    #@2d
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@30
    .line 183
    new-instance v1, Lcom/android/internal/atfwd/AtCtsaCmdHandler$2;

    #@32
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mInjectThread:Landroid/os/HandlerThread;

    #@34
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@37
    move-result-object v2

    #@38
    invoke-direct {v1, p0, v2}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$2;-><init>(Lcom/android/internal/atfwd/AtCtsaCmdHandler;Landroid/os/Looper;)V

    #@3b
    iput-object v1, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mInjectHandler:Landroid/os/Handler;

    #@3d
    .line 223
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/atfwd/AtCtsaCmdHandler;Landroid/view/MotionEvent;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->injectPointerEvent(Landroid/view/MotionEvent;Z)V

    #@3
    return-void
.end method

.method private injectPointerEvent(Landroid/view/MotionEvent;Z)V
    .registers 6
    .parameter "event"
    .parameter "sync"

    #@0
    .prologue
    .line 286
    const/16 v0, 0x1002

    #@2
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setSource(I)V

    #@5
    .line 287
    const-string v0, "Input"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "InjectPointerEvent: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 288
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@20
    move-result-object v1

    #@21
    if-eqz p2, :cond_28

    #@23
    const/4 v0, 0x2

    #@24
    :goto_24
    invoke-virtual {v1, p1, v0}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@27
    .line 291
    return-void

    #@28
    .line 288
    :cond_28
    const/4 v0, 0x1

    #@29
    goto :goto_24
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 277
    const-string v0, "+CTSA"

    #@2
    return-object v0
.end method

.method public handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 13
    .parameter "cmd"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 226
    const/4 v4, 0x0

    #@3
    .line 227
    .local v4, valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    const/4 v0, 0x0

    #@4
    .line 228
    .local v0, dead:Z
    const/4 v3, 0x0

    #@5
    .line 229
    .local v3, result:Ljava/lang/String;
    const/4 v2, 0x0

    #@6
    .line 231
    .local v2, isAtCmdRespOK:Z
    const-string v6, "AtCtsaCmdHandler"

    #@8
    new-instance v9, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v10, "handleCommand: "

    #@f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v9

    #@1b
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 232
    const-string v6, "AtCtsaCmdHandler"

    #@20
    new-instance v9, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v10, "OpCode: "

    #@27
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v9

    #@2b
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getOpcode()I

    #@2e
    move-result v10

    #@2f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v9

    #@33
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v9

    #@37
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 234
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getOpcode()I

    #@3d
    move-result v6

    #@3e
    sparse-switch v6, :sswitch_data_ca

    #@41
    .line 267
    const-string v6, "AtCtsaCmdHandler"

    #@43
    const-string v9, "CTSA OpCode Error"

    #@45
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 268
    const/4 v6, 0x4

    #@49
    invoke-virtual {p1, v6}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    .line 272
    :goto_4d
    if-eqz v2, :cond_c0

    #@4f
    new-instance v6, Lcom/android/internal/atfwd/AtCmdResponse;

    #@51
    invoke-direct {v6, v7, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@54
    :goto_54
    return-object v6

    #@55
    .line 238
    :sswitch_55
    const/4 v2, 0x1

    #@56
    .line 239
    new-instance v6, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    invoke-virtual {p0}, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->getCommandName()Ljava/lang/String;

    #@5e
    move-result-object v9

    #@5f
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v6

    #@63
    const-string v9, ": (0-4)"

    #@65
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    .line 240
    goto :goto_4d

    #@6e
    .line 244
    :sswitch_6e
    iget-object v6, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mInjectThread:Landroid/os/HandlerThread;

    #@70
    invoke-virtual {v6}, Landroid/os/HandlerThread;->isAlive()Z

    #@73
    move-result v6

    #@74
    if-nez v6, :cond_99

    #@76
    move v0, v7

    #@77
    .line 245
    :goto_77
    if-nez v0, :cond_bb

    #@79
    .line 251
    :try_start_79
    new-instance v5, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;

    #@7b
    invoke-direct {v5, p0, p1}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;-><init>(Lcom/android/internal/atfwd/AtCtsaCmdHandler;Lcom/android/internal/atfwd/AtCmd;)V
    :try_end_7e
    .catch Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException; {:try_start_79 .. :try_end_7e} :catch_9b

    #@7e
    .line 252
    .end local v4           #valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    .local v5, valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    const/4 v2, 0x1

    #@7f
    .line 253
    :try_start_7f
    const-string v6, "AtCtsaCmdHandler"

    #@81
    const-string v9, "Queuing command"

    #@83
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 254
    iget-object v6, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->mInjectHandler:Landroid/os/Handler;

    #@88
    const/4 v9, 0x0

    #@89
    invoke-static {v6, v9, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@8c
    move-result-object v6

    #@8d
    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    #@90
    .line 255
    const-string v6, "AtCtsaCmdHandler"

    #@92
    const-string v9, "Command queued"

    #@94
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_97
    .catch Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException; {:try_start_7f .. :try_end_97} :catch_c6

    #@97
    move-object v4, v5

    #@98
    .line 259
    .end local v5           #valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    .restart local v4       #valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    goto :goto_4d

    #@99
    :cond_99
    move v0, v8

    #@9a
    .line 244
    goto :goto_77

    #@9b
    .line 256
    :catch_9b
    move-exception v1

    #@9c
    .line 257
    .local v1, e:Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
    :goto_9c
    const-string v6, "AtCtsaCmdHandler"

    #@9e
    new-instance v9, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v10, "Error parsing command "

    #@a5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v9

    #@a9
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v9

    #@b1
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 258
    const/16 v6, 0x19

    #@b6
    invoke-virtual {p1, v6}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@b9
    move-result-object v3

    #@ba
    .line 259
    goto :goto_4d

    #@bb
    .line 261
    .end local v1           #e:Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
    :cond_bb
    invoke-virtual {p1, v7}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@be
    move-result-object v3

    #@bf
    .line 263
    goto :goto_4d

    #@c0
    .line 272
    :cond_c0
    new-instance v6, Lcom/android/internal/atfwd/AtCmdResponse;

    #@c2
    invoke-direct {v6, v8, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@c5
    goto :goto_54

    #@c6
    .line 256
    .end local v4           #valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    .restart local v5       #valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    :catch_c6
    move-exception v1

    #@c7
    move-object v4, v5

    #@c8
    .end local v5           #valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    .restart local v4       #valid:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    goto :goto_9c

    #@c9
    .line 234
    nop

    #@ca
    :sswitch_data_ca
    .sparse-switch
        0x7 -> :sswitch_55
        0xb -> :sswitch_6e
    .end sparse-switch
.end method
