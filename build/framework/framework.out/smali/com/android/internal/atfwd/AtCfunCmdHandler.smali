.class public Lcom/android/internal/atfwd/AtCfunCmdHandler;
.super Lcom/android/internal/atfwd/AtCmdBaseHandler;
.source "AtCfunCmdHandler.java"

# interfaces
.implements Lcom/android/internal/atfwd/AtCmdHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "AtCfunCmdHandler"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdHandlerInstantiationException;
        }
    .end annotation

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmdBaseHandler;-><init>(Landroid/content/Context;)V

    #@3
    .line 50
    return-void
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 54
    const-string v0, "+CFUN"

    #@2
    return-object v0
.end method

.method public handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 9
    .parameter "cmd"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 59
    const/4 v1, 0x0

    #@3
    .line 61
    .local v1, ret:Lcom/android/internal/atfwd/AtCmdResponse;
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getTokens()[Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 63
    .local v2, tokens:[Ljava/lang/String;
    array-length v3, v2

    #@8
    const/4 v4, 0x2

    #@9
    if-ne v3, v4, :cond_1f

    #@b
    aget-object v3, v2, v5

    #@d
    const-string v4, "1"

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_1f

    #@15
    aget-object v3, v2, v6

    #@17
    const-string v4, "1"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_2e

    #@1f
    .line 67
    :cond_1f
    const-string v3, "AtCfunCmdHandler"

    #@21
    const-string v4, "+CFUN: Only +CFUN=1,1 supported"

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 68
    new-instance v1, Lcom/android/internal/atfwd/AtCmdResponse;

    #@28
    .end local v1           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    const-string v3, "+CME ERROR: 1"

    #@2a
    invoke-direct {v1, v5, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@2d
    .line 85
    .restart local v1       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    :goto_2d
    return-object v1

    #@2e
    .line 70
    :cond_2e
    new-instance v0, Lcom/android/internal/atfwd/AtCfunCmdHandler$1;

    #@30
    invoke-direct {v0, p0}, Lcom/android/internal/atfwd/AtCfunCmdHandler$1;-><init>(Lcom/android/internal/atfwd/AtCfunCmdHandler;)V

    #@33
    .line 81
    .local v0, rebootThread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@36
    .line 82
    new-instance v1, Lcom/android/internal/atfwd/AtCmdResponse;

    #@38
    .end local v1           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    const/4 v3, 0x0

    #@39
    invoke-direct {v1, v6, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@3c
    .restart local v1       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    goto :goto_2d
.end method
