.class Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
.super Ljava/lang/Object;
.source "AtCkpdCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/atfwd/AtCkpdCmdHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParsedCkpdCmd"
.end annotation


# instance fields
.field private mEvents:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mOriginalCommand:Lcom/android/internal/atfwd/AtCmd;

.field private mPauseTime:J

.field private mPressTime:J

.field final synthetic this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;


# direct methods
.method public constructor <init>(Lcom/android/internal/atfwd/AtCkpdCmdHandler;Lcom/android/internal/atfwd/AtCmd;)V
    .registers 5
    .parameter
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 82
    iput-object p2, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mOriginalCommand:Lcom/android/internal/atfwd/AtCmd;

    #@7
    .line 83
    const-wide/16 v0, 0xc8

    #@9
    iput-wide v0, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mPressTime:J

    #@b
    .line 84
    const-wide/16 v0, 0x190

    #@d
    iput-wide v0, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mPauseTime:J

    #@f
    .line 85
    new-instance v0, Ljava/util/Vector;

    #@11
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mEvents:Ljava/util/Vector;

    #@16
    .line 86
    invoke-direct {p0}, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->parse_cmd()V

    #@19
    .line 87
    return-void
.end method

.method private parse_cmd()V
    .registers 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 94
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mOriginalCommand:Lcom/android/internal/atfwd/AtCmd;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/atfwd/AtCmd;->getTokens()[Ljava/lang/String;

    #@7
    move-result-object v20

    #@8
    .line 98
    .local v20, tokens:[Ljava/lang/String;
    if-eqz v20, :cond_15

    #@a
    move-object/from16 v0, v20

    #@c
    array-length v1, v0

    #@d
    if-eqz v1, :cond_15

    #@f
    move-object/from16 v0, v20

    #@11
    array-length v1, v0

    #@12
    const/4 v2, 0x3

    #@13
    if-le v1, v2, :cond_1d

    #@15
    .line 99
    :cond_15
    new-instance v1, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;

    #@17
    const-string v2, "Must provide 1 to three tokens"

    #@19
    invoke-direct {v1, v2}, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 101
    :cond_1d
    const/4 v1, 0x0

    #@1e
    aget-object v1, v20, v1

    #@20
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    #@27
    move-result-object v16

    #@28
    .line 102
    .local v16, keys:[C
    const/4 v1, 0x0

    #@29
    aget-object v1, v20, v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    #@2e
    move-result-object v18

    #@2f
    .line 105
    .local v18, orig:[C
    move-object/from16 v0, v20

    #@31
    array-length v1, v0

    #@32
    const/4 v2, 0x2

    #@33
    if-lt v1, v2, :cond_43

    #@35
    .line 108
    const/4 v1, 0x1

    #@36
    :try_start_36
    aget-object v1, v20, v1

    #@38
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3b
    move-result v1

    #@3c
    mul-int/lit8 v1, v1, 0x64

    #@3e
    int-to-long v1, v1

    #@3f
    move-object/from16 v0, p0

    #@41
    iput-wide v1, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mPressTime:J
    :try_end_43
    .catch Ljava/lang/NumberFormatException; {:try_start_36 .. :try_end_43} :catch_9b

    #@43
    .line 114
    :cond_43
    move-object/from16 v0, v20

    #@45
    array-length v1, v0

    #@46
    const/4 v2, 0x3

    #@47
    if-ne v1, v2, :cond_57

    #@49
    .line 116
    const/4 v1, 0x2

    #@4a
    :try_start_4a
    aget-object v1, v20, v1

    #@4c
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4f
    move-result v1

    #@50
    mul-int/lit8 v1, v1, 0x64

    #@52
    int-to-long v1, v1

    #@53
    move-object/from16 v0, p0

    #@55
    iput-wide v1, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mPauseTime:J
    :try_end_57
    .catch Ljava/lang/NumberFormatException; {:try_start_4a .. :try_end_57} :catch_b8

    #@57
    .line 122
    :cond_57
    const/4 v14, 0x0

    #@58
    .line 123
    .local v14, instring:Z
    new-instance v19, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    .line 124
    .local v19, theString:Ljava/lang/StringBuilder;
    const/4 v12, 0x0

    #@5e
    .local v12, i:I
    :goto_5e
    move-object/from16 v0, v16

    #@60
    array-length v1, v0

    #@61
    if-ge v12, v1, :cond_1b6

    #@63
    .line 125
    if-eqz v14, :cond_fd

    #@65
    .line 126
    aget-char v1, v16, v12

    #@67
    const/16 v2, 0x3b

    #@69
    if-ne v1, v2, :cond_f6

    #@6b
    .line 127
    const/4 v14, 0x0

    #@6c
    .line 128
    move-object/from16 v0, p0

    #@6e
    iget-object v1, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@70
    iget-object v1, v1, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mKcm:Landroid/view/KeyCharacterMap;

    #@72
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v1, v2}, Landroid/view/KeyCharacterMap;->getEvents([C)[Landroid/view/KeyEvent;

    #@7d
    move-result-object v11

    #@7e
    .line 129
    .local v11, events:[Landroid/view/KeyEvent;
    const/4 v1, 0x0

    #@7f
    move-object/from16 v0, v19

    #@81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    #@84
    .line 130
    if-eqz v11, :cond_d5

    #@86
    .line 131
    move-object v9, v11

    #@87
    .local v9, arr$:[Landroid/view/KeyEvent;
    array-length v0, v9

    #@88
    move/from16 v17, v0

    #@8a
    .local v17, len$:I
    const/4 v13, 0x0

    #@8b
    .local v13, i$:I
    :goto_8b
    move/from16 v0, v17

    #@8d
    if-ge v13, v0, :cond_104

    #@8f
    aget-object v15, v9, v13

    #@91
    .line 132
    .local v15, keyEvent:Landroid/view/KeyEvent;
    move-object/from16 v0, p0

    #@93
    iget-object v1, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mEvents:Ljava/util/Vector;

    #@95
    invoke-virtual {v1, v15}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@98
    .line 131
    add-int/lit8 v13, v13, 0x1

    #@9a
    goto :goto_8b

    #@9b
    .line 109
    .end local v9           #arr$:[Landroid/view/KeyEvent;
    .end local v11           #events:[Landroid/view/KeyEvent;
    .end local v12           #i:I
    .end local v13           #i$:I
    .end local v14           #instring:Z
    .end local v15           #keyEvent:Landroid/view/KeyEvent;
    .end local v17           #len$:I
    .end local v19           #theString:Ljava/lang/StringBuilder;
    :catch_9b
    move-exception v10

    #@9c
    .line 110
    .local v10, e:Ljava/lang/NumberFormatException;
    new-instance v1, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;

    #@9e
    new-instance v2, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v3, "Wrong arg2: "

    #@a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v2

    #@a9
    const/4 v3, 0x1

    #@aa
    aget-object v3, v20, v3

    #@ac
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v2

    #@b0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v2

    #@b4
    invoke-direct {v1, v2}, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;-><init>(Ljava/lang/String;)V

    #@b7
    throw v1

    #@b8
    .line 117
    .end local v10           #e:Ljava/lang/NumberFormatException;
    :catch_b8
    move-exception v10

    #@b9
    .line 118
    .restart local v10       #e:Ljava/lang/NumberFormatException;
    new-instance v1, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;

    #@bb
    new-instance v2, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v3, "Wrong arg3: "

    #@c2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    const/4 v3, 0x2

    #@c7
    aget-object v3, v20, v3

    #@c9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v2

    #@cd
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v2

    #@d1
    invoke-direct {v1, v2}, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;-><init>(Ljava/lang/String;)V

    #@d4
    throw v1

    #@d5
    .line 135
    .end local v10           #e:Ljava/lang/NumberFormatException;
    .restart local v11       #events:[Landroid/view/KeyEvent;
    .restart local v12       #i:I
    .restart local v14       #instring:Z
    .restart local v19       #theString:Ljava/lang/StringBuilder;
    :cond_d5
    new-instance v1, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;

    #@d7
    new-instance v2, Ljava/lang/StringBuilder;

    #@d9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@dc
    const-string v3, "Unable to find all keycodes for string \'"

    #@de
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v2

    #@e2
    move-object/from16 v0, v19

    #@e4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v2

    #@e8
    const-string v3, "\'"

    #@ea
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v2

    #@ee
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v2

    #@f2
    invoke-direct {v1, v2}, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;-><init>(Ljava/lang/String;)V

    #@f5
    throw v1

    #@f6
    .line 139
    .end local v11           #events:[Landroid/view/KeyEvent;
    :cond_f6
    aget-char v1, v18, v12

    #@f8
    move-object/from16 v0, v19

    #@fa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@fd
    .line 141
    :cond_fd
    aget-char v1, v16, v12

    #@ff
    const/16 v2, 0x3b

    #@101
    if-ne v1, v2, :cond_108

    #@103
    .line 142
    const/4 v14, 0x1

    #@104
    .line 124
    :cond_104
    :goto_104
    add-int/lit8 v12, v12, 0x1

    #@106
    goto/16 :goto_5e

    #@108
    .line 145
    :cond_108
    aget-char v1, v16, v12

    #@10a
    const/16 v2, 0x22

    #@10c
    if-eq v1, v2, :cond_104

    #@10e
    .line 147
    aget-char v1, v16, v12

    #@110
    const/16 v2, 0x57

    #@112
    if-ne v1, v2, :cond_125

    #@114
    .line 148
    move-object/from16 v0, p0

    #@116
    iget-object v1, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mEvents:Ljava/util/Vector;

    #@118
    new-instance v2, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;

    #@11a
    move-object/from16 v0, p0

    #@11c
    iget-wide v3, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mPauseTime:J

    #@11e
    invoke-direct {v2, v3, v4}, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;-><init>(J)V

    #@121
    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@124
    goto :goto_104

    #@125
    .line 152
    :cond_125
    invoke-static {}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$000()Ljava/util/HashMap;

    #@128
    move-result-object v1

    #@129
    aget-char v2, v16, v12

    #@12b
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@12e
    move-result-object v2

    #@12f
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@132
    move-result v1

    #@133
    if-nez v1, :cond_150

    #@135
    .line 153
    new-instance v1, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;

    #@137
    new-instance v2, Ljava/lang/StringBuilder;

    #@139
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13c
    const-string v3, "Invalid Character "

    #@13e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v2

    #@142
    aget-char v3, v18, v12

    #@144
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@147
    move-result-object v2

    #@148
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v2

    #@14c
    invoke-direct {v1, v2}, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;-><init>(Ljava/lang/String;)V

    #@14f
    throw v1

    #@150
    .line 155
    :cond_150
    move-object/from16 v0, p0

    #@152
    iget-object v0, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mEvents:Ljava/util/Vector;

    #@154
    move-object/from16 v21, v0

    #@156
    new-instance v1, Landroid/view/KeyEvent;

    #@158
    const-wide/16 v2, 0x0

    #@15a
    const-wide/16 v4, 0x0

    #@15c
    const/4 v6, 0x0

    #@15d
    invoke-static {}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$000()Ljava/util/HashMap;

    #@160
    move-result-object v7

    #@161
    aget-char v8, v16, v12

    #@163
    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@166
    move-result-object v8

    #@167
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@16a
    move-result-object v7

    #@16b
    check-cast v7, Ljava/lang/Integer;

    #@16d
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@170
    move-result v7

    #@171
    const/4 v8, 0x0

    #@172
    invoke-direct/range {v1 .. v8}, Landroid/view/KeyEvent;-><init>(JJIII)V

    #@175
    move-object/from16 v0, v21

    #@177
    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@17a
    .line 156
    move-object/from16 v0, p0

    #@17c
    iget-object v0, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mEvents:Ljava/util/Vector;

    #@17e
    move-object/from16 v21, v0

    #@180
    new-instance v1, Landroid/view/KeyEvent;

    #@182
    const-wide/16 v2, 0x0

    #@184
    const-wide/16 v4, 0x0

    #@186
    const/4 v6, 0x1

    #@187
    invoke-static {}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$000()Ljava/util/HashMap;

    #@18a
    move-result-object v7

    #@18b
    aget-char v8, v16, v12

    #@18d
    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@190
    move-result-object v8

    #@191
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@194
    move-result-object v7

    #@195
    check-cast v7, Ljava/lang/Integer;

    #@197
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@19a
    move-result v7

    #@19b
    const/4 v8, 0x0

    #@19c
    invoke-direct/range {v1 .. v8}, Landroid/view/KeyEvent;-><init>(JJIII)V

    #@19f
    move-object/from16 v0, v21

    #@1a1
    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@1a4
    .line 157
    move-object/from16 v0, p0

    #@1a6
    iget-object v1, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mEvents:Ljava/util/Vector;

    #@1a8
    new-instance v2, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    iget-wide v3, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mPressTime:J

    #@1ae
    invoke-direct {v2, v3, v4}, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;-><init>(J)V

    #@1b1
    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@1b4
    goto/16 :goto_104

    #@1b6
    .line 159
    :cond_1b6
    return-void
.end method


# virtual methods
.method public getEvents()Ljava/util/Vector;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->mEvents:Ljava/util/Vector;

    #@2
    return-object v0
.end method
