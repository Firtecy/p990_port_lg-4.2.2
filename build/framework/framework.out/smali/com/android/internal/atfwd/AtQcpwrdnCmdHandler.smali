.class public Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler;
.super Lcom/android/internal/atfwd/AtCmdBaseHandler;
.source "AtQcpwrdnCmdHandler.java"

# interfaces
.implements Lcom/android/internal/atfwd/AtCmdHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "AtQcpwrdnCmdHandler"


# instance fields
.field private mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdHandlerInstantiationException;
        }
    .end annotation

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmdBaseHandler;-><init>(Landroid/content/Context;)V

    #@3
    .line 41
    new-instance v0, Landroid/os/Handler;

    #@5
    const/4 v1, 0x1

    #@6
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Z)V

    #@9
    iput-object v0, p0, Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler;->mHandler:Landroid/os/Handler;

    #@b
    .line 45
    iput-object p1, p0, Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler;->mContext:Landroid/content/Context;

    #@d
    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 50
    const-string v0, "$QCPWRDN"

    #@2
    return-object v0
.end method

.method public handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 6
    .parameter "cmd"

    #@0
    .prologue
    .line 55
    const/4 v0, 0x0

    #@1
    .line 56
    .local v0, ret:Lcom/android/internal/atfwd/AtCmdResponse;
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getTokens()[Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 58
    .local v1, tokens:[Ljava/lang/String;
    const-string v2, "AtQcpwrdnCmdHandler"

    #@7
    const-string v3, "$QCPWRDN command processing entry"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 60
    array-length v2, v1

    #@d
    if-lez v2, :cond_18

    #@f
    .line 62
    new-instance v0, Lcom/android/internal/atfwd/AtCmdResponse;

    #@11
    .end local v0           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    const/4 v2, 0x0

    #@12
    const-string v3, "+CME ERROR: 1"

    #@14
    invoke-direct {v0, v2, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@17
    .line 79
    .restart local v0       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    :goto_17
    return-object v0

    #@18
    .line 64
    :cond_18
    const-string v2, "AtQcpwrdnCmdHandler"

    #@1a
    const-string v3, "Initiating Shutdown process"

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 65
    iget-object v2, p0, Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler;->mHandler:Landroid/os/Handler;

    #@21
    new-instance v3, Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler$1;

    #@23
    invoke-direct {v3, p0}, Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler$1;-><init>(Lcom/android/internal/atfwd/AtQcpwrdnCmdHandler;)V

    #@26
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@29
    .line 75
    const-string v2, "AtQcpwrdnCmdHandler"

    #@2b
    const-string v3, "Initiated Shutdown process"

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 76
    new-instance v0, Lcom/android/internal/atfwd/AtCmdResponse;

    #@32
    .end local v0           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    const/4 v2, 0x1

    #@33
    const/4 v3, 0x0

    #@34
    invoke-direct {v0, v2, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@37
    .restart local v0       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    goto :goto_17
.end method
