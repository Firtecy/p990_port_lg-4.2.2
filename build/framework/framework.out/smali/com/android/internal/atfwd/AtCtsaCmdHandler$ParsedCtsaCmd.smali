.class Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
.super Ljava/lang/Object;
.source "AtCtsaCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/atfwd/AtCtsaCmdHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParsedCtsaCmd"
.end annotation


# static fields
.field private static final CTSA_ACTION_DBL_TAP:I = 0x3

.field private static final CTSA_ACTION_DEPRESS:I = 0x1

.field private static final CTSA_ACTION_LNG_TAP:I = 0x4

.field private static final CTSA_ACTION_RELEASE:I = 0x0

.field private static final CTSA_ACTION_TAP:I = 0x2

.field private static final LNG_PRESS_TIME:J = 0x5dcL

.field private static final PRESS_TIME:J = 0xc8L


# instance fields
.field private mEvents:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mOriginalCommand:Lcom/android/internal/atfwd/AtCmd;

.field final synthetic this$0:Lcom/android/internal/atfwd/AtCtsaCmdHandler;


# direct methods
.method public constructor <init>(Lcom/android/internal/atfwd/AtCtsaCmdHandler;Lcom/android/internal/atfwd/AtCmd;)V
    .registers 4
    .parameter
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->this$0:Lcom/android/internal/atfwd/AtCtsaCmdHandler;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 89
    iput-object p2, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mOriginalCommand:Lcom/android/internal/atfwd/AtCmd;

    #@7
    .line 90
    new-instance v0, Ljava/util/Vector;

    #@9
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@c
    iput-object v0, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mEvents:Ljava/util/Vector;

    #@e
    .line 91
    invoke-direct {p0}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->parse_cmd()V

    #@11
    .line 92
    return-void
.end method

.method private addClick(FFJ)V
    .registers 14
    .parameter "x"
    .parameter "y"
    .parameter "presstime"

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 155
    move-wide v2, v0

    #@4
    move v5, p1

    #@5
    move v6, p2

    #@6
    move v7, v4

    #@7
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@a
    move-result-object v8

    #@b
    .line 157
    .local v8, evt:Landroid/view/MotionEvent;
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mEvents:Ljava/util/Vector;

    #@d
    invoke-virtual {v0, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@10
    .line 159
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mEvents:Ljava/util/Vector;

    #@12
    new-instance v1, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;

    #@14
    invoke-direct {v1, p3, p4}, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;-><init>(J)V

    #@17
    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@1a
    .line 161
    invoke-static {v8}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@1d
    move-result-object v8

    #@1e
    .line 162
    const/4 v0, 0x1

    #@1f
    invoke-virtual {v8, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@22
    .line 163
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mEvents:Ljava/util/Vector;

    #@24
    invoke-virtual {v0, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@27
    .line 164
    return-void
.end method

.method private parse_cmd()V
    .registers 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 99
    const/4 v8, -0x1

    #@1
    .line 100
    .local v8, ctsaAction:I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4
    move-result-wide v0

    #@5
    .line 101
    .local v0, time:J
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mOriginalCommand:Lcom/android/internal/atfwd/AtCmd;

    #@7
    invoke-virtual {v2}, Lcom/android/internal/atfwd/AtCmd;->getTokens()[Ljava/lang/String;

    #@a
    move-result-object v13

    #@b
    .line 103
    .local v13, tokens:[Ljava/lang/String;
    const-wide/16 v11, 0xc8

    #@d
    .line 113
    .local v11, presstime:J
    if-eqz v13, :cond_13

    #@f
    array-length v2, v13

    #@10
    const/4 v3, 0x3

    #@11
    if-eq v2, v3, :cond_1b

    #@13
    .line 114
    :cond_13
    new-instance v2, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;

    #@15
    const-string v3, "Must provide three tokens"

    #@17
    invoke-direct {v2, v3}, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 117
    :cond_1b
    const/4 v2, 0x0

    #@1c
    :try_start_1c
    aget-object v2, v13, v2

    #@1e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@21
    move-result v8

    #@22
    .line 119
    const/4 v2, 0x1

    #@23
    aget-object v2, v13, v2

    #@25
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@28
    move-result v5

    #@29
    .line 120
    .local v5, x:F
    const/4 v2, 0x2

    #@2a
    aget-object v2, v13, v2

    #@2c
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@2f
    move-result v6

    #@30
    .line 122
    .local v6, y:F
    packed-switch v8, :pswitch_data_62

    #@33
    .line 150
    :goto_33
    return-void

    #@34
    .line 124
    :pswitch_34
    const/4 v4, 0x1

    #@35
    const/4 v7, 0x0

    #@36
    move-wide v2, v0

    #@37
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@3a
    move-result-object v10

    #@3b
    .line 126
    .local v10, evt:Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mEvents:Ljava/util/Vector;

    #@3d
    invoke-virtual {v2, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_40
    .catch Ljava/lang/NumberFormatException; {:try_start_1c .. :try_end_40} :catch_41

    #@40
    goto :goto_33

    #@41
    .line 147
    .end local v5           #x:F
    .end local v6           #y:F
    .end local v10           #evt:Landroid/view/MotionEvent;
    :catch_41
    move-exception v9

    #@42
    .line 148
    .local v9, e:Ljava/lang/NumberFormatException;
    new-instance v2, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;

    #@44
    invoke-direct {v2, v9}, Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;-><init>(Ljava/lang/Throwable;)V

    #@47
    throw v2

    #@48
    .line 129
    .end local v9           #e:Ljava/lang/NumberFormatException;
    .restart local v5       #x:F
    .restart local v6       #y:F
    :pswitch_48
    const/4 v4, 0x0

    #@49
    const/4 v7, 0x0

    #@4a
    move-wide v2, v0

    #@4b
    :try_start_4b
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@4e
    move-result-object v10

    #@4f
    .line 131
    .restart local v10       #evt:Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mEvents:Ljava/util/Vector;

    #@51
    invoke-virtual {v2, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@54
    goto :goto_33

    #@55
    .line 134
    .end local v10           #evt:Landroid/view/MotionEvent;
    :pswitch_55
    const-wide/16 v11, 0x5dc

    #@57
    .line 137
    :pswitch_57
    invoke-direct {p0, v5, v6, v11, v12}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->addClick(FFJ)V

    #@5a
    goto :goto_33

    #@5b
    .line 141
    :pswitch_5b
    invoke-direct {p0, v5, v6, v11, v12}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->addClick(FFJ)V

    #@5e
    .line 143
    invoke-direct {p0, v5, v6, v11, v12}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->addClick(FFJ)V
    :try_end_61
    .catch Ljava/lang/NumberFormatException; {:try_start_4b .. :try_end_61} :catch_41

    #@61
    goto :goto_33

    #@62
    .line 122
    :pswitch_data_62
    .packed-switch 0x0
        :pswitch_34
        :pswitch_48
        :pswitch_57
        :pswitch_5b
        :pswitch_55
    .end packed-switch
.end method


# virtual methods
.method public getEvents()Ljava/util/Vector;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mEvents:Ljava/util/Vector;

    #@2
    return-object v0
.end method

.method public getOriginalCommand()Lcom/android/internal/atfwd/AtCmd;
    .registers 2

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->mOriginalCommand:Lcom/android/internal/atfwd/AtCmd;

    #@2
    return-object v0
.end method
