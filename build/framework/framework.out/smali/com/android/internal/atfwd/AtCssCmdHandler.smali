.class public Lcom/android/internal/atfwd/AtCssCmdHandler;
.super Lcom/android/internal/atfwd/AtCmdBaseHandler;
.source "AtCssCmdHandler.java"

# interfaces
.implements Lcom/android/internal/atfwd/AtCmdHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "AtCssCmdHandler"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdHandlerInstantiationException;
        }
    .end annotation

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmdBaseHandler;-><init>(Landroid/content/Context;)V

    #@3
    .line 45
    return-void
.end method

.method private getWindowManager()Landroid/view/WindowManager;
    .registers 4

    #@0
    .prologue
    .line 48
    iget-object v1, p0, Lcom/android/internal/atfwd/AtCmdBaseHandler;->mContext:Landroid/content/Context;

    #@2
    const-string/jumbo v2, "window"

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/view/WindowManager;

    #@b
    .line 49
    .local v0, wm:Landroid/view/WindowManager;
    if-nez v0, :cond_14

    #@d
    .line 50
    const-string v1, "AtCssCmdHandler"

    #@f
    const-string v2, "Unable to find WindowManager interface."

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 52
    :cond_14
    return-object v0
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 57
    const-string v0, "+CSS"

    #@2
    return-object v0
.end method

.method public handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 13
    .parameter "cmd"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 62
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getTokens()[Ljava/lang/String;

    #@4
    move-result-object v5

    #@5
    .line 63
    .local v5, tokens:[Ljava/lang/String;
    const/4 v2, 0x0

    #@6
    .line 64
    .local v2, result:Ljava/lang/String;
    const/4 v1, 0x0

    #@7
    .line 66
    .local v1, isAtCmdRespOK:Z
    const-string v7, "AtCssCmdHandler"

    #@9
    new-instance v8, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v9, "OpCode  "

    #@10
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v8

    #@14
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getOpcode()I

    #@17
    move-result v9

    #@18
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v8

    #@1c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v8

    #@20
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 67
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getOpcode()I

    #@26
    move-result v7

    #@27
    packed-switch v7, :pswitch_data_bc

    #@2a
    .line 90
    :goto_2a
    if-eqz v1, :cond_b5

    #@2c
    new-instance v7, Lcom/android/internal/atfwd/AtCmdResponse;

    #@2e
    const/4 v8, 0x1

    #@2f
    invoke-direct {v7, v8, v2}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@32
    :goto_32
    return-object v7

    #@33
    .line 71
    :pswitch_33
    :try_start_33
    invoke-direct {p0}, Lcom/android/internal/atfwd/AtCssCmdHandler;->getWindowManager()Landroid/view/WindowManager;

    #@36
    move-result-object v6

    #@37
    .line 72
    .local v6, wm:Landroid/view/WindowManager;
    if-nez v6, :cond_46

    #@39
    .line 73
    const-string v7, "AtCssCmdHandler"

    #@3b
    const-string v8, "Unable to find WindowManager interface."

    #@3d
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 74
    const/4 v7, 0x0

    #@41
    invoke-virtual {p1, v7}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    goto :goto_2a

    #@46
    .line 77
    :cond_46
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@49
    move-result-object v7

    #@4a
    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    #@4d
    move-result v3

    #@4e
    .line 78
    .local v3, screenHeight:I
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@51
    move-result-object v7

    #@52
    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    #@55
    move-result v4

    #@56
    .line 79
    .local v4, screenWidth:I
    new-instance v7, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    invoke-virtual {p0}, Lcom/android/internal/atfwd/AtCssCmdHandler;->getCommandName()Ljava/lang/String;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, ": "

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v7

    #@6d
    const-string v8, ","

    #@6f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v7

    #@77
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    .line 80
    const-string v7, "AtCssCmdHandler"

    #@7d
    new-instance v8, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v9, " At Result :"

    #@84
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v8

    #@8c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v8

    #@90
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_93
    .catch Ljava/lang/SecurityException; {:try_start_33 .. :try_end_93} :catch_95

    #@93
    .line 81
    const/4 v1, 0x1

    #@94
    goto :goto_2a

    #@95
    .line 83
    .end local v3           #screenHeight:I
    .end local v4           #screenWidth:I
    .end local v6           #wm:Landroid/view/WindowManager;
    :catch_95
    move-exception v0

    #@96
    .line 84
    .local v0, e:Ljava/lang/SecurityException;
    const-string v7, "AtCssCmdHandler"

    #@98
    new-instance v8, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v9, "SecurityException: "

    #@9f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v8

    #@a3
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v8

    #@a7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v8

    #@ab
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 85
    const/4 v7, 0x3

    #@af
    invoke-virtual {p1, v7}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@b2
    move-result-object v2

    #@b3
    goto/16 :goto_2a

    #@b5
    .line 90
    .end local v0           #e:Ljava/lang/SecurityException;
    :cond_b5
    new-instance v7, Lcom/android/internal/atfwd/AtCmdResponse;

    #@b7
    invoke-direct {v7, v10, v2}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@ba
    goto/16 :goto_32

    #@bc
    .line 67
    :pswitch_data_bc
    .packed-switch 0x1
        :pswitch_33
    .end packed-switch
.end method
