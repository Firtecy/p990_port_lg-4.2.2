.class public Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;
.super Ljava/lang/Object;
.source "AtCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/atfwd/AtCmdHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PauseEvent"
.end annotation


# instance fields
.field private mTime:J


# direct methods
.method public constructor <init>(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 76
    iput-wide p1, p0, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;->mTime:J

    #@5
    .line 77
    return-void
.end method


# virtual methods
.method public getTime()J
    .registers 3

    #@0
    .prologue
    .line 79
    iget-wide v0, p0, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;->mTime:J

    #@2
    return-wide v0
.end method
