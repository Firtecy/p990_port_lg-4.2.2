.class public Lcom/android/internal/atfwd/AtCrslCmdHandler;
.super Lcom/android/internal/atfwd/AtCmdBaseHandler;
.source "AtCrslCmdHandler.java"

# interfaces
.implements Lcom/android/internal/atfwd/AtCmdHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "AtCrslCmdHandler"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdHandlerInstantiationException;
        }
    .end annotation

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmdBaseHandler;-><init>(Landroid/content/Context;)V

    #@3
    .line 46
    return-void
.end method

.method private static getAudioService()Landroid/media/IAudioService;
    .registers 3

    #@0
    .prologue
    .line 49
    const-string v1, "audio"

    #@2
    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@9
    move-result-object v0

    #@a
    .line 51
    .local v0, audioService:Landroid/media/IAudioService;
    if-nez v0, :cond_13

    #@c
    .line 52
    const-string v1, "AtCrslCmdHandler"

    #@e
    const-string v2, "Unable to find IAudioService interface."

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 54
    :cond_13
    return-object v0
.end method

.method private getFormattedRingerVolumeRange()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 87
    const-string v1, ""

    #@2
    .line 89
    .local v1, ret:Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/internal/atfwd/AtCrslCmdHandler;->getMaxVolume()I

    #@5
    move-result v2

    #@6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v0

    #@a
    .line 90
    .local v0, nMaxVol:Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result v2

    #@e
    if-ltz v2, :cond_31

    #@10
    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, "(0-"

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, ")"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 93
    :cond_31
    return-object v1
.end method

.method private getMaxVolume()I
    .registers 8

    #@0
    .prologue
    .line 73
    const/4 v2, -0x1

    #@1
    .line 74
    .local v2, nMaxVol:I
    invoke-static {}, Lcom/android/internal/atfwd/AtCrslCmdHandler;->getAudioService()Landroid/media/IAudioService;

    #@4
    move-result-object v0

    #@5
    .line 75
    .local v0, audioService:Landroid/media/IAudioService;
    if-nez v0, :cond_9

    #@7
    move v3, v2

    #@8
    .line 83
    .end local v2           #nMaxVol:I
    .local v3, nMaxVol:I
    :goto_8
    return v3

    #@9
    .line 79
    .end local v3           #nMaxVol:I
    .restart local v2       #nMaxVol:I
    :cond_9
    const/4 v4, 0x2

    #@a
    :try_start_a
    invoke-interface {v0, v4}, Landroid/media/IAudioService;->getStreamMaxVolume(I)I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_10

    #@d
    move-result v2

    #@e
    :goto_e
    move v3, v2

    #@f
    .line 83
    .end local v2           #nMaxVol:I
    .restart local v3       #nMaxVol:I
    goto :goto_8

    #@10
    .line 80
    .end local v3           #nMaxVol:I
    .restart local v2       #nMaxVol:I
    :catch_10
    move-exception v1

    #@11
    .line 81
    .local v1, ex:Landroid/os/RemoteException;
    const-string v4, "AtCrslCmdHandler"

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "Unable to obtain Ringer level : "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_e
.end method

.method static getRingerVolume(Ljava/lang/String;)I
    .registers 6
    .parameter "input"

    #@0
    .prologue
    .line 58
    const/4 v2, -0x1

    #@1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4
    move-result-object v1

    #@5
    .line 60
    .local v1, ringVolume:Ljava/lang/Integer;
    :try_start_5
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8
    move-result v2

    #@9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_c
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_c} :catch_12

    #@c
    move-result-object v1

    #@d
    .line 69
    :goto_d
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@10
    move-result v2

    #@11
    return v2

    #@12
    .line 66
    :catch_12
    move-exception v0

    #@13
    .line 67
    .local v0, ex:Ljava/lang/NumberFormatException;
    const-string v2, "AtCrslCmdHandler"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "Not an Integer: "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_d
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 98
    const-string v0, "+CRSL"

    #@2
    return-object v0
.end method

.method public handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 15
    .parameter "cmd"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v11, 0x3

    #@2
    .line 103
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getTokens()[Ljava/lang/String;

    #@5
    move-result-object v7

    #@6
    .line 104
    .local v7, tokens:[Ljava/lang/String;
    const/4 v5, 0x0

    #@7
    .line 105
    .local v5, result:Ljava/lang/String;
    const/4 v2, 0x0

    #@8
    .line 106
    .local v2, isAtCmdRespOK:Z
    const/4 v3, 0x0

    #@9
    .line 108
    .local v3, isSetCmd:Z
    invoke-static {}, Lcom/android/internal/atfwd/AtCrslCmdHandler;->getAudioService()Landroid/media/IAudioService;

    #@c
    move-result-object v0

    #@d
    .line 109
    .local v0, audioService:Landroid/media/IAudioService;
    if-nez v0, :cond_19

    #@f
    .line 110
    new-instance v8, Lcom/android/internal/atfwd/AtCmdResponse;

    #@11
    invoke-virtual {p1, v11}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@14
    move-result-object v9

    #@15
    invoke-direct {v8, v12, v9}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@18
    .line 171
    :goto_18
    return-object v8

    #@19
    .line 114
    :cond_19
    const-string v8, "AtCrslCmdHandler"

    #@1b
    new-instance v9, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v10, "OpCode"

    #@22
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v9

    #@26
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getOpcode()I

    #@29
    move-result v10

    #@2a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v9

    #@2e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v9

    #@32
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 116
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getOpcode()I

    #@38
    move-result v8

    #@39
    sparse-switch v8, :sswitch_data_120

    #@3c
    .line 167
    :goto_3c
    if-nez v3, :cond_5b

    #@3e
    if-eqz v2, :cond_5b

    #@40
    .line 168
    new-instance v8, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    invoke-virtual {p0}, Lcom/android/internal/atfwd/AtCrslCmdHandler;->getCommandName()Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    const-string v9, ": "

    #@4f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    .line 171
    :cond_5b
    if-eqz v2, :cond_118

    #@5d
    new-instance v8, Lcom/android/internal/atfwd/AtCmdResponse;

    #@5f
    const/4 v9, 0x1

    #@60
    invoke-direct {v8, v9, v5}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@63
    goto :goto_18

    #@64
    .line 121
    :sswitch_64
    if-eqz v7, :cond_69

    #@66
    :try_start_66
    array-length v8, v7

    #@67
    if-nez v8, :cond_70

    #@69
    .line 122
    :cond_69
    const/16 v8, 0x32

    #@6b
    invoke-virtual {p1, v8}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@6e
    move-result-object v5

    #@6f
    goto :goto_3c

    #@70
    .line 125
    :cond_70
    const/4 v8, 0x0

    #@71
    aget-object v8, v7, v8

    #@73
    invoke-static {v8}, Lcom/android/internal/atfwd/AtCrslCmdHandler;->getRingerVolume(Ljava/lang/String;)I

    #@76
    move-result v8

    #@77
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7a
    move-result-object v6

    #@7b
    .line 126
    .local v6, ringVolume:Ljava/lang/Integer;
    invoke-direct {p0}, Lcom/android/internal/atfwd/AtCrslCmdHandler;->getMaxVolume()I

    #@7e
    move-result v4

    #@7f
    .line 128
    .local v4, nMaxVol:I
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@82
    move-result v8

    #@83
    if-ltz v8, :cond_8d

    #@85
    if-ltz v4, :cond_8d

    #@87
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@8a
    move-result v8

    #@8b
    if-le v8, v4, :cond_93

    #@8d
    .line 129
    :cond_8d
    const/4 v8, 0x3

    #@8e
    invoke-virtual {p1, v8}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@91
    move-result-object v5

    #@92
    goto :goto_3c

    #@93
    .line 132
    :cond_93
    const/4 v8, 0x2

    #@94
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@97
    move-result v9

    #@98
    const/4 v10, 0x5

    #@99
    invoke-interface {v0, v8, v9, v10}, Landroid/media/IAudioService;->setStreamVolume(III)V
    :try_end_9c
    .catch Landroid/os/RemoteException; {:try_start_66 .. :try_end_9c} :catch_9f

    #@9c
    .line 134
    const/4 v3, 0x1

    #@9d
    .line 135
    const/4 v2, 0x1

    #@9e
    goto :goto_3c

    #@9f
    .line 137
    .end local v4           #nMaxVol:I
    .end local v6           #ringVolume:Ljava/lang/Integer;
    :catch_9f
    move-exception v1

    #@a0
    .line 138
    .local v1, ex:Landroid/os/RemoteException;
    const-string v8, "AtCrslCmdHandler"

    #@a2
    new-instance v9, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v10, "Unable to perfom AT+CRSL "

    #@a9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v9

    #@b1
    const-string v10, "Exception : "

    #@b3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v9

    #@b7
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v9

    #@bb
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v9

    #@bf
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .line 139
    invoke-virtual {p1, v11}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@c5
    move-result-object v5

    #@c6
    .line 141
    goto/16 :goto_3c

    #@c8
    .line 146
    .end local v1           #ex:Landroid/os/RemoteException;
    :sswitch_c8
    const/4 v8, 0x2

    #@c9
    :try_start_c9
    invoke-interface {v0, v8}, Landroid/media/IAudioService;->getStreamVolume(I)I

    #@cc
    move-result v8

    #@cd
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d0
    move-result-object v6

    #@d1
    .line 147
    .restart local v6       #ringVolume:Ljava/lang/Integer;
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@d4
    move-result v8

    #@d5
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_d8
    .catch Landroid/os/RemoteException; {:try_start_c9 .. :try_end_d8} :catch_dc

    #@d8
    move-result-object v5

    #@d9
    .line 148
    const/4 v2, 0x1

    #@da
    goto/16 :goto_3c

    #@dc
    .line 150
    .end local v6           #ringVolume:Ljava/lang/Integer;
    :catch_dc
    move-exception v1

    #@dd
    .line 151
    .restart local v1       #ex:Landroid/os/RemoteException;
    const-string v8, "AtCrslCmdHandler"

    #@df
    new-instance v9, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v10, "Unable to perfom AT+CRSL "

    #@e6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v9

    #@ea
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v9

    #@ee
    const-string v10, "Exception : "

    #@f0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v9

    #@f4
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v9

    #@f8
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v9

    #@fc
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 152
    invoke-virtual {p1, v11}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@102
    move-result-object v5

    #@103
    .line 154
    goto/16 :goto_3c

    #@105
    .line 158
    .end local v1           #ex:Landroid/os/RemoteException;
    :sswitch_105
    invoke-direct {p0}, Lcom/android/internal/atfwd/AtCrslCmdHandler;->getFormattedRingerVolumeRange()Ljava/lang/String;

    #@108
    move-result-object v5

    #@109
    .line 159
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@10c
    move-result v8

    #@10d
    if-lez v8, :cond_112

    #@10f
    .line 160
    const/4 v2, 0x1

    #@110
    goto/16 :goto_3c

    #@112
    .line 162
    :cond_112
    invoke-virtual {p1, v11}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@115
    move-result-object v5

    #@116
    goto/16 :goto_3c

    #@118
    .line 171
    :cond_118
    new-instance v8, Lcom/android/internal/atfwd/AtCmdResponse;

    #@11a
    invoke-direct {v8, v12, v5}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@11d
    goto/16 :goto_18

    #@11f
    .line 116
    nop

    #@120
    :sswitch_data_120
    .sparse-switch
        0x5 -> :sswitch_c8
        0x7 -> :sswitch_105
        0xb -> :sswitch_64
    .end sparse-switch
.end method
