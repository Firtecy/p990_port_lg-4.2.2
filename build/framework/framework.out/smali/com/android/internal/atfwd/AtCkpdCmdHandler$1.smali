.class Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;
.super Ljava/lang/Thread;
.source "AtCkpdCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/atfwd/AtCkpdCmdHandler;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;


# direct methods
.method constructor <init>(Lcom/android/internal/atfwd/AtCkpdCmdHandler;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 220
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 17

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 222
    const/4 v9, 0x0

    #@2
    .line 224
    .local v9, cmd:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    :cond_2
    const-string v4, "AtCkpdCmdHandler"

    #@4
    const-string v5, "De-queing command"

    #@6
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 225
    move-object/from16 v0, p0

    #@b
    iget-object v4, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@d
    invoke-static {v4}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$100(Lcom/android/internal/atfwd/AtCkpdCmdHandler;)Ljava/util/LinkedList;

    #@10
    move-result-object v5

    #@11
    monitor-enter v5

    #@12
    .line 226
    :goto_12
    :try_start_12
    move-object/from16 v0, p0

    #@14
    iget-object v4, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@16
    invoke-static {v4}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$100(Lcom/android/internal/atfwd/AtCkpdCmdHandler;)Ljava/util/LinkedList;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_35

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_38

    #@20
    .line 228
    :try_start_20
    move-object/from16 v0, p0

    #@22
    iget-object v4, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@24
    invoke-static {v4}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$100(Lcom/android/internal/atfwd/AtCkpdCmdHandler;)Ljava/util/LinkedList;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_2b
    .catchall {:try_start_20 .. :try_end_2b} :catchall_35
    .catch Ljava/lang/InterruptedException; {:try_start_20 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_12

    #@2c
    .line 229
    :catch_2c
    move-exception v10

    #@2d
    .line 230
    .local v10, e:Ljava/lang/InterruptedException;
    :try_start_2d
    const-string v4, "AtCkpdCmdHandler"

    #@2f
    const-string v6, "Inject thread interrupted"

    #@31
    invoke-static {v4, v6, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    goto :goto_12

    #@35
    .line 235
    .end local v10           #e:Ljava/lang/InterruptedException;
    :catchall_35
    move-exception v4

    #@36
    monitor-exit v5
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_35

    #@37
    throw v4

    #@38
    .line 234
    :cond_38
    :try_start_38
    move-object/from16 v0, p0

    #@3a
    iget-object v4, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@3c
    invoke-static {v4}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$100(Lcom/android/internal/atfwd/AtCkpdCmdHandler;)Ljava/util/LinkedList;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    #@43
    move-result-object v4

    #@44
    move-object v0, v4

    #@45
    check-cast v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;

    #@47
    move-object v9, v0

    #@48
    .line 235
    monitor-exit v5
    :try_end_49
    .catchall {:try_start_38 .. :try_end_49} :catchall_35

    #@49
    .line 236
    if-eqz v9, :cond_2

    #@4b
    .line 237
    const-string v4, "AtCkpdCmdHandler"

    #@4d
    new-instance v5, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v6, "Command de-queued: "

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 239
    move-object/from16 v0, p0

    #@65
    iget-object v4, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@67
    iget-object v4, v4, Lcom/android/internal/atfwd/AtCmdBaseHandler;->mContext:Landroid/content/Context;

    #@69
    const-string/jumbo v5, "power"

    #@6c
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6f
    move-result-object v14

    #@70
    check-cast v14, Landroid/os/PowerManager;

    #@72
    .line 241
    .local v14, pm:Landroid/os/PowerManager;
    const v4, 0x3000001a

    #@75
    const-string v5, "+CKPD Handler"

    #@77
    invoke-virtual {v14, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@7a
    move-result-object v15

    #@7b
    .line 244
    .local v15, wl:Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v9}, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;->getEvents()Ljava/util/Vector;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    #@82
    move-result-object v13

    #@83
    .local v13, i$:Ljava/util/Iterator;
    :goto_83
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@86
    move-result v4

    #@87
    if-eqz v4, :cond_2

    #@89
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8c
    move-result-object v12

    #@8d
    .line 245
    .local v12, evt:Ljava/lang/Object;
    instance-of v4, v12, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;

    #@8f
    if-eqz v4, :cond_a4

    #@91
    .line 247
    :try_start_91
    check-cast v12, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;

    #@93
    .end local v12           #evt:Ljava/lang/Object;
    invoke-virtual {v12}, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;->getTime()J

    #@96
    move-result-wide v4

    #@97
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9a
    .catch Ljava/lang/InterruptedException; {:try_start_91 .. :try_end_9a} :catch_9b

    #@9a
    goto :goto_83

    #@9b
    .line 248
    :catch_9b
    move-exception v10

    #@9c
    .line 249
    .restart local v10       #e:Ljava/lang/InterruptedException;
    const-string v4, "AtCkpdCmdHandler"

    #@9e
    const-string v5, "Interrupted pause"

    #@a0
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto :goto_83

    #@a4
    .line 253
    .end local v10           #e:Ljava/lang/InterruptedException;
    .restart local v12       #evt:Ljava/lang/Object;
    :cond_a4
    instance-of v4, v12, Landroid/view/KeyEvent;

    #@a6
    if-nez v4, :cond_c9

    #@a8
    .line 254
    const-string v4, "AtCkpdCmdHandler"

    #@aa
    new-instance v5, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v6, "Unknown type of event "

    #@b1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v5

    #@b5
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b8
    move-result-object v6

    #@b9
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@bc
    move-result-object v6

    #@bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v5

    #@c5
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    goto :goto_83

    #@c9
    .line 259
    :cond_c9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@cc
    move-result-wide v2

    #@cd
    .local v2, dntime:J
    move-object v11, v12

    #@ce
    .line 260
    check-cast v11, Landroid/view/KeyEvent;

    #@d0
    .line 261
    .local v11, ev:Landroid/view/KeyEvent;
    new-instance v1, Landroid/view/KeyEvent;

    #@d2
    invoke-virtual {v11}, Landroid/view/KeyEvent;->getAction()I

    #@d5
    move-result v6

    #@d6
    invoke-virtual {v11}, Landroid/view/KeyEvent;->getKeyCode()I

    #@d9
    move-result v7

    #@da
    move-wide v4, v2

    #@db
    invoke-direct/range {v1 .. v8}, Landroid/view/KeyEvent;-><init>(JJIII)V

    #@de
    .line 266
    .end local v11           #ev:Landroid/view/KeyEvent;
    .local v1, ev:Landroid/view/KeyEvent;
    invoke-virtual {v15}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@e1
    .line 267
    move-object/from16 v0, p0

    #@e3
    iget-object v4, v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCkpdCmdHandler;

    #@e5
    invoke-static {v4, v1, v8}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->access$200(Lcom/android/internal/atfwd/AtCkpdCmdHandler;Landroid/view/KeyEvent;Z)V

    #@e8
    .line 268
    invoke-virtual {v15}, Landroid/os/PowerManager$WakeLock;->release()V

    #@eb
    goto :goto_83
.end method
