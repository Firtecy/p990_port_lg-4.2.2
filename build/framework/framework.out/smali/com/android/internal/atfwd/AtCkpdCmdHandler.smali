.class public Lcom/android/internal/atfwd/AtCkpdCmdHandler;
.super Lcom/android/internal/atfwd/AtCmdBaseHandler;
.source "AtCkpdCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    }
.end annotation


# static fields
.field private static final DEFAULT_PAUSE_TIME:I = 0x190

.field private static final DEFAULT_PRESS_TIME:I = 0xc8

.field private static final LOG_TAG:Ljava/lang/String; = "AtCkpdCmdHandler"

.field private static alphacode:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static key2keycode:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEventQ:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;",
            ">;"
        }
    .end annotation
.end field

.field private mInjectThread:Ljava/lang/Thread;

.field mKcm:Landroid/view/KeyCharacterMap;

.field private mWm:Landroid/view/IWindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 13
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdHandlerInstantiationException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v10, 0x12

    #@2
    const/16 v9, 0x11

    #@4
    const/16 v8, 0xa

    #@6
    const/16 v7, 0x9

    #@8
    const/16 v6, 0x5b

    #@a
    .line 163
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmdBaseHandler;-><init>(Landroid/content/Context;)V

    #@d
    .line 164
    const-string/jumbo v3, "window"

    #@10
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    invoke-static {v3}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@17
    move-result-object v2

    #@18
    .line 167
    .local v2, service:Landroid/view/IWindowManager;
    if-nez v2, :cond_22

    #@1a
    .line 168
    new-instance v3, Ljava/lang/RuntimeException;

    #@1c
    const-string v4, "Unable to connect to Window Service"

    #@1e
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v3

    #@22
    .line 170
    :cond_22
    iput-object v2, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mWm:Landroid/view/IWindowManager;

    #@24
    .line 171
    new-instance v3, Ljava/util/LinkedList;

    #@26
    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    #@29
    iput-object v3, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mEventQ:Ljava/util/LinkedList;

    #@2b
    .line 172
    new-instance v3, Ljava/util/HashMap;

    #@2d
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@30
    sput-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@32
    .line 173
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@34
    const/16 v4, 0x23

    #@36
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    .line 174
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@43
    const/16 v4, 0x2a

    #@45
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@48
    move-result-object v4

    #@49
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@50
    .line 175
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@52
    const/16 v4, 0x30

    #@54
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@57
    move-result-object v4

    #@58
    const/4 v5, 0x7

    #@59
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@60
    .line 176
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@62
    const/16 v4, 0x31

    #@64
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@67
    move-result-object v4

    #@68
    const/16 v5, 0x8

    #@6a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@71
    .line 177
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@73
    const/16 v4, 0x32

    #@75
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@78
    move-result-object v4

    #@79
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@80
    .line 178
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@82
    const/16 v4, 0x33

    #@84
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@87
    move-result-object v4

    #@88
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8f
    .line 179
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@91
    const/16 v4, 0x34

    #@93
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@96
    move-result-object v4

    #@97
    const/16 v5, 0xb

    #@99
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9c
    move-result-object v5

    #@9d
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a0
    .line 180
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@a2
    const/16 v4, 0x35

    #@a4
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@a7
    move-result-object v4

    #@a8
    const/16 v5, 0xc

    #@aa
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ad
    move-result-object v5

    #@ae
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b1
    .line 181
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@b3
    const/16 v4, 0x36

    #@b5
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@b8
    move-result-object v4

    #@b9
    const/16 v5, 0xd

    #@bb
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@be
    move-result-object v5

    #@bf
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c2
    .line 182
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@c4
    const/16 v4, 0x37

    #@c6
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@c9
    move-result-object v4

    #@ca
    const/16 v5, 0xe

    #@cc
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cf
    move-result-object v5

    #@d0
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d3
    .line 183
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@d5
    const/16 v4, 0x38

    #@d7
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@da
    move-result-object v4

    #@db
    const/16 v5, 0xf

    #@dd
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e0
    move-result-object v5

    #@e1
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e4
    .line 184
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@e6
    const/16 v4, 0x39

    #@e8
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@eb
    move-result-object v4

    #@ec
    const/16 v5, 0x10

    #@ee
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f1
    move-result-object v5

    #@f2
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f5
    .line 185
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@f7
    const/16 v4, 0x3c

    #@f9
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@fc
    move-result-object v4

    #@fd
    const/16 v5, 0x15

    #@ff
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@102
    move-result-object v5

    #@103
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@106
    .line 186
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@108
    const/16 v4, 0x3e

    #@10a
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@10d
    move-result-object v4

    #@10e
    const/16 v5, 0x16

    #@110
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@113
    move-result-object v5

    #@114
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@117
    .line 187
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@119
    const/16 v4, 0x5e

    #@11b
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@11e
    move-result-object v4

    #@11f
    const/16 v5, 0x13

    #@121
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@124
    move-result-object v5

    #@125
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@128
    .line 188
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@12a
    const/16 v4, 0x56

    #@12c
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@12f
    move-result-object v4

    #@130
    const/16 v5, 0x14

    #@132
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@135
    move-result-object v5

    #@136
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@139
    .line 189
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@13b
    const/16 v4, 0x44

    #@13d
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@140
    move-result-object v4

    #@141
    const/16 v5, 0x19

    #@143
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@146
    move-result-object v5

    #@147
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14a
    .line 190
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@14c
    const/16 v4, 0x45

    #@14e
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@151
    move-result-object v4

    #@152
    const/4 v5, 0x6

    #@153
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@156
    move-result-object v5

    #@157
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15a
    .line 191
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@15c
    const/16 v4, 0x4d

    #@15e
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@161
    move-result-object v4

    #@162
    const/16 v5, 0x52

    #@164
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@167
    move-result-object v5

    #@168
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16b
    .line 192
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@16d
    const/16 v4, 0x50

    #@16f
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@172
    move-result-object v4

    #@173
    const/16 v5, 0x1a

    #@175
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@178
    move-result-object v5

    #@179
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17c
    .line 193
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@17e
    const/16 v4, 0x51

    #@180
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@183
    move-result-object v4

    #@184
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@187
    move-result-object v5

    #@188
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18b
    .line 194
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@18d
    const/16 v4, 0x53

    #@18f
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@192
    move-result-object v4

    #@193
    const/4 v5, 0x5

    #@194
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@197
    move-result-object v5

    #@198
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19b
    .line 195
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@19d
    const/16 v4, 0x55

    #@19f
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@1a2
    move-result-object v4

    #@1a3
    const/16 v5, 0x18

    #@1a5
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a8
    move-result-object v5

    #@1a9
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1ac
    .line 196
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@1ae
    const/16 v4, 0x56

    #@1b0
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@1b3
    move-result-object v4

    #@1b4
    const/16 v5, 0x14

    #@1b6
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b9
    move-result-object v5

    #@1ba
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1bd
    .line 197
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@1bf
    const/16 v4, 0x59

    #@1c1
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@1c4
    move-result-object v4

    #@1c5
    const/16 v5, 0x43

    #@1c7
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ca
    move-result-object v5

    #@1cb
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1ce
    .line 198
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@1d0
    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@1d3
    move-result-object v4

    #@1d4
    const/4 v5, 0x1

    #@1d5
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d8
    move-result-object v5

    #@1d9
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1dc
    .line 199
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@1de
    const/16 v4, 0x5d

    #@1e0
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@1e3
    move-result-object v4

    #@1e4
    const/4 v5, 0x2

    #@1e5
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e8
    move-result-object v5

    #@1e9
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1ec
    .line 201
    new-instance v3, Ljava/util/HashMap;

    #@1ee
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@1f1
    sput-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@1f3
    .line 202
    const/4 v0, 0x0

    #@1f4
    .local v0, i:I
    :goto_1f4
    const/16 v3, 0x1a

    #@1f6
    if-ge v0, v3, :cond_20d

    #@1f8
    .line 203
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@1fa
    add-int/lit8 v4, v0, 0x41

    #@1fc
    int-to-char v4, v4

    #@1fd
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@200
    move-result-object v4

    #@201
    add-int/lit8 v5, v0, 0x1d

    #@203
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@206
    move-result-object v5

    #@207
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20a
    .line 202
    add-int/lit8 v0, v0, 0x1

    #@20c
    goto :goto_1f4

    #@20d
    .line 204
    :cond_20d
    const/4 v0, 0x0

    #@20e
    :goto_20e
    if-ge v0, v8, :cond_225

    #@210
    .line 205
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@212
    add-int/lit8 v4, v0, 0x30

    #@214
    int-to-char v4, v4

    #@215
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@218
    move-result-object v4

    #@219
    add-int/lit8 v5, v0, 0x7

    #@21b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21e
    move-result-object v5

    #@21f
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@222
    .line 204
    add-int/lit8 v0, v0, 0x1

    #@224
    goto :goto_20e

    #@225
    .line 206
    :cond_225
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@227
    const/16 v4, 0x40

    #@229
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@22c
    move-result-object v4

    #@22d
    const/16 v5, 0x4d

    #@22f
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@232
    move-result-object v5

    #@233
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@236
    .line 207
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@238
    const/16 v4, 0x3d

    #@23a
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@23d
    move-result-object v4

    #@23e
    const/16 v5, 0x46

    #@240
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@243
    move-result-object v5

    #@244
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@247
    .line 208
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@249
    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@24c
    move-result-object v4

    #@24d
    const/16 v5, 0x47

    #@24f
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@252
    move-result-object v5

    #@253
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@256
    .line 209
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@258
    const/16 v4, 0x2e

    #@25a
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@25d
    move-result-object v4

    #@25e
    const/16 v5, 0x38

    #@260
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@263
    move-result-object v5

    #@264
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@267
    .line 210
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@269
    const/16 v4, 0x2b

    #@26b
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@26e
    move-result-object v4

    #@26f
    const/16 v5, 0x51

    #@271
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@274
    move-result-object v5

    #@275
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@278
    .line 211
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@27a
    const/16 v4, 0x23

    #@27c
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@27f
    move-result-object v4

    #@280
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@283
    move-result-object v5

    #@284
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@287
    .line 212
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@289
    const/16 v4, 0x5d

    #@28b
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@28e
    move-result-object v4

    #@28f
    const/16 v5, 0x48

    #@291
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@294
    move-result-object v5

    #@295
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@298
    .line 213
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@29a
    const/16 v4, 0x2f

    #@29c
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@29f
    move-result-object v4

    #@2a0
    const/16 v5, 0x4c

    #@2a2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a5
    move-result-object v5

    #@2a6
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a9
    .line 214
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@2ab
    const/16 v4, 0x20

    #@2ad
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@2b0
    move-result-object v4

    #@2b1
    const/16 v5, 0x3e

    #@2b3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b6
    move-result-object v5

    #@2b7
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2ba
    .line 215
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@2bc
    const/16 v4, 0x2a

    #@2be
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@2c1
    move-result-object v4

    #@2c2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c5
    move-result-object v5

    #@2c6
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c9
    .line 216
    sget-object v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->alphacode:Ljava/util/HashMap;

    #@2cb
    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@2ce
    move-result-object v4

    #@2cf
    const/16 v5, 0x3d

    #@2d1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d4
    move-result-object v5

    #@2d5
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d8
    .line 217
    const/4 v3, 0x0

    #@2d9
    invoke-static {v3}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    #@2dc
    move-result-object v1

    #@2dd
    .line 218
    .local v1, kcm:Landroid/view/KeyCharacterMap;
    iput-object v1, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mKcm:Landroid/view/KeyCharacterMap;

    #@2df
    .line 220
    new-instance v3, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;

    #@2e1
    invoke-direct {v3, p0}, Lcom/android/internal/atfwd/AtCkpdCmdHandler$1;-><init>(Lcom/android/internal/atfwd/AtCkpdCmdHandler;)V

    #@2e4
    iput-object v3, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mInjectThread:Ljava/lang/Thread;

    #@2e6
    .line 274
    iget-object v3, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mInjectThread:Ljava/lang/Thread;

    #@2e8
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    #@2eb
    .line 275
    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .registers 1

    #@0
    .prologue
    .line 58
    sget-object v0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->key2keycode:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/atfwd/AtCkpdCmdHandler;)Ljava/util/LinkedList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mEventQ:Ljava/util/LinkedList;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/atfwd/AtCkpdCmdHandler;Landroid/view/KeyEvent;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->injectKeyEvent(Landroid/view/KeyEvent;Z)V

    #@3
    return-void
.end method

.method private injectKeyEvent(Landroid/view/KeyEvent;Z)V
    .registers 6
    .parameter "event"
    .parameter "sync"

    #@0
    .prologue
    .line 320
    const-string v0, "AtCkpdCmdHandler"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "InjectKeyEvent: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 321
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@1b
    move-result-object v1

    #@1c
    if-eqz p2, :cond_23

    #@1e
    const/4 v0, 0x2

    #@1f
    :goto_1f
    invoke-virtual {v1, p1, v0}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@22
    .line 324
    return-void

    #@23
    .line 321
    :cond_23
    const/4 v0, 0x1

    #@24
    goto :goto_1f
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 309
    const-string v0, "+CKPD"

    #@2
    return-object v0
.end method

.method public handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 13
    .parameter "cmd"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 278
    const/4 v2, 0x0

    #@3
    .line 279
    .local v2, ret:Lcom/android/internal/atfwd/AtCmdResponse;
    const/4 v4, 0x0

    #@4
    .line 280
    .local v4, valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    const/4 v0, 0x0

    #@5
    .line 281
    .local v0, dead:Z
    const-string v8, "AtCkpdCmdHandler"

    #@7
    new-instance v9, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v10, "handleCommand: "

    #@e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v9

    #@12
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v9

    #@16
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v9

    #@1a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 283
    iget-object v8, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mInjectThread:Ljava/lang/Thread;

    #@1f
    invoke-virtual {v8}, Ljava/lang/Thread;->isAlive()Z

    #@22
    move-result v8

    #@23
    if-nez v8, :cond_53

    #@25
    move v0, v6

    #@26
    .line 284
    :goto_26
    if-nez v0, :cond_7b

    #@28
    .line 290
    :try_start_28
    new-instance v5, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;

    #@2a
    invoke-direct {v5, p0, p1}, Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;-><init>(Lcom/android/internal/atfwd/AtCkpdCmdHandler;Lcom/android/internal/atfwd/AtCmd;)V
    :try_end_2d
    .catch Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException; {:try_start_28 .. :try_end_2d} :catch_83

    #@2d
    .line 291
    .end local v4           #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    .local v5, valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    :try_start_2d
    new-instance v3, Lcom/android/internal/atfwd/AtCmdResponse;

    #@2f
    const/4 v6, 0x1

    #@30
    const/4 v8, 0x0

    #@31
    invoke-direct {v3, v6, v8}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V
    :try_end_34
    .catch Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException; {:try_start_2d .. :try_end_34} :catch_85

    #@34
    .line 292
    .end local v2           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    .local v3, ret:Lcom/android/internal/atfwd/AtCmdResponse;
    :try_start_34
    const-string v6, "AtCkpdCmdHandler"

    #@36
    const-string v8, "Queuing command"

    #@38
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 293
    iget-object v8, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mEventQ:Ljava/util/LinkedList;

    #@3d
    monitor-enter v8
    :try_end_3e
    .catch Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException; {:try_start_34 .. :try_end_3e} :catch_58

    #@3e
    .line 294
    :try_start_3e
    iget-object v6, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mEventQ:Ljava/util/LinkedList;

    #@40
    invoke-virtual {v6, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@43
    .line 295
    iget-object v6, p0, Lcom/android/internal/atfwd/AtCkpdCmdHandler;->mEventQ:Ljava/util/LinkedList;

    #@45
    invoke-virtual {v6}, Ljava/lang/Object;->notify()V

    #@48
    .line 296
    monitor-exit v8
    :try_end_49
    .catchall {:try_start_3e .. :try_end_49} :catchall_55

    #@49
    .line 297
    :try_start_49
    const-string v6, "AtCkpdCmdHandler"

    #@4b
    const-string v8, "Command queued"

    #@4d
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_50
    .catch Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException; {:try_start_49 .. :try_end_50} :catch_58

    #@50
    move-object v4, v5

    #@51
    .end local v5           #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    .restart local v4       #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    move-object v2, v3

    #@52
    .line 305
    .end local v3           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    .restart local v2       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    :goto_52
    return-object v2

    #@53
    :cond_53
    move v0, v7

    #@54
    .line 283
    goto :goto_26

    #@55
    .line 296
    .end local v2           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    .end local v4           #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    .restart local v3       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    .restart local v5       #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    :catchall_55
    move-exception v6

    #@56
    :try_start_56
    monitor-exit v8
    :try_end_57
    .catchall {:try_start_56 .. :try_end_57} :catchall_55

    #@57
    :try_start_57
    throw v6
    :try_end_58
    .catch Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException; {:try_start_57 .. :try_end_58} :catch_58

    #@58
    .line 298
    :catch_58
    move-exception v1

    #@59
    move-object v4, v5

    #@5a
    .end local v5           #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    .restart local v4       #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    move-object v2, v3

    #@5b
    .line 299
    .end local v3           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    .local v1, e:Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
    .restart local v2       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    :goto_5b
    const-string v6, "AtCkpdCmdHandler"

    #@5d
    new-instance v8, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v9, "Error parsing command "

    #@64
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v8

    #@68
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v8

    #@70
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 300
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@75
    .end local v2           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    const-string v6, "+CME ERROR: 25"

    #@77
    invoke-direct {v2, v7, v6}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@7a
    .line 301
    .restart local v2       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    goto :goto_52

    #@7b
    .line 303
    .end local v1           #e:Lcom/android/internal/atfwd/AtCmdHandler$AtCmdParseException;
    :cond_7b
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@7d
    .end local v2           #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    const-string v6, "+CME ERROR: 1"

    #@7f
    invoke-direct {v2, v7, v6}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@82
    .restart local v2       #ret:Lcom/android/internal/atfwd/AtCmdResponse;
    goto :goto_52

    #@83
    .line 298
    :catch_83
    move-exception v1

    #@84
    goto :goto_5b

    #@85
    .end local v4           #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    .restart local v5       #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    :catch_85
    move-exception v1

    #@86
    move-object v4, v5

    #@87
    .end local v5           #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    .restart local v4       #valid:Lcom/android/internal/atfwd/AtCkpdCmdHandler$ParsedCkpdCmd;
    goto :goto_5b
.end method
