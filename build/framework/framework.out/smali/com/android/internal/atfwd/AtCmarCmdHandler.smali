.class public Lcom/android/internal/atfwd/AtCmarCmdHandler;
.super Lcom/android/internal/atfwd/AtCmdBaseHandler;
.source "AtCmarCmdHandler.java"

# interfaces
.implements Lcom/android/internal/atfwd/AtCmdHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "AtCmarCmdHandler"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mSdReset:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/atfwd/AtCmdHandler$AtCmdHandlerInstantiationException;
        }
    .end annotation

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmdBaseHandler;-><init>(Landroid/content/Context;)V

    #@3
    .line 53
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mSdReset:Z

    #@6
    .line 57
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mContext:Landroid/content/Context;

    #@8
    .line 58
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    #@a
    iget-object v1, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mContext:Landroid/content/Context;

    #@c
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@f
    iput-object v0, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@11
    .line 59
    return-void
.end method

.method private processResetCommand()V
    .registers 6

    #@0
    .prologue
    .line 137
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 138
    .local v1, status:Ljava/lang/String;
    const-string v2, "bad_removal"

    #@6
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_19

    #@c
    const-string/jumbo v2, "removed"

    #@f
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_19

    #@15
    iget-boolean v2, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mSdReset:Z

    #@17
    if-nez v2, :cond_2d

    #@19
    .line 145
    :cond_19
    const-string v2, "AtCmarCmdHandler"

    #@1b
    const-string v3, " Phone Storage MASTER RESET triggered"

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 146
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mContext:Landroid/content/Context;

    #@22
    new-instance v3, Landroid/content/Intent;

    #@24
    const-string v4, "android.intent.action.MASTER_CLEAR"

    #@26
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@29
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2c
    .line 161
    :goto_2c
    return-void

    #@2d
    .line 155
    :cond_2d
    const-string v2, "AtCmarCmdHandler"

    #@2f
    const-string v3, " External Storage MASTER RESET triggered"

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 156
    new-instance v0, Landroid/content/Intent;

    #@36
    const-string v2, "com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET"

    #@38
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3b
    .line 157
    .local v0, intent:Landroid/content/Intent;
    sget-object v2, Lcom/android/internal/os/storage/ExternalStorageFormatter;->COMPONENT_NAME:Landroid/content/ComponentName;

    #@3d
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@40
    .line 158
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mContext:Landroid/content/Context;

    #@42
    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@45
    goto :goto_2c
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 63
    const-string v0, "+CMAR"

    #@2
    return-object v0
.end method

.method public handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
    .registers 9
    .parameter "cmd"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v3, 0x7

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 69
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getTokens()[Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 70
    .local v1, tokens:[Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/atfwd/AtCmd;->getOpcode()I

    #@b
    move-result v0

    #@c
    .line 72
    .local v0, opCode:I
    const/16 v2, 0xb

    #@e
    if-eq v0, v2, :cond_24

    #@10
    if-eq v0, v3, :cond_24

    #@12
    .line 74
    const-string v2, "AtCmarCmdHandler"

    #@14
    const-string v3, "CMAR opcode eror"

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 75
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@1b
    const/4 v3, 0x4

    #@1c
    invoke-virtual {p1, v3}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-direct {v2, v4, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@23
    .line 128
    :goto_23
    return-object v2

    #@24
    .line 79
    :cond_24
    if-ne v0, v3, :cond_33

    #@26
    .line 80
    const-string v2, "AtCmarCmdHandler"

    #@28
    const-string v3, "+CMAR=? test command, RESULT OK "

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 81
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@2f
    invoke-direct {v2, v5, v6}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@32
    goto :goto_23

    #@33
    .line 84
    :cond_33
    array-length v2, v1

    #@34
    if-nez v2, :cond_49

    #@36
    .line 85
    const-string v2, "AtCmarCmdHandler"

    #@38
    const-string v3, "CMAR mandatory parameter pin lock code missing "

    #@3a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 86
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@3f
    const/16 v3, 0x32

    #@41
    invoke-virtual {p1, v3}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-direct {v2, v4, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@48
    goto :goto_23

    #@49
    .line 90
    :cond_49
    array-length v2, v1

    #@4a
    if-le v2, v5, :cond_5f

    #@4c
    aget-object v2, v1, v5

    #@4e
    const-string v3, "1"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v2

    #@54
    if-eqz v2, :cond_5f

    #@56
    .line 91
    iput-boolean v5, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mSdReset:Z

    #@58
    .line 92
    const-string v2, "AtCmarCmdHandler"

    #@5a
    const-string v3, " Option enabled to erase SD card, if present "

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 111
    :cond_5f
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@61
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    #@64
    move-result v2

    #@65
    const/high16 v3, 0x1

    #@67
    if-ne v2, v3, :cond_7a

    #@69
    .line 113
    const-string v2, "AtCmarCmdHandler"

    #@6b
    const-string v3, "Pattern Lock enabled/No password set , CMAR is unsupported "

    #@6d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 114
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@72
    invoke-virtual {p1, v4}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    invoke-direct {v2, v4, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@79
    goto :goto_23

    #@7a
    .line 120
    :cond_7a
    iget-object v2, p0, Lcom/android/internal/atfwd/AtCmarCmdHandler;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@7c
    aget-object v3, v1, v4

    #@7e
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->checkPassword(Ljava/lang/String;)Z

    #@81
    move-result v2

    #@82
    if-eqz v2, :cond_8d

    #@84
    .line 122
    invoke-direct {p0}, Lcom/android/internal/atfwd/AtCmarCmdHandler;->processResetCommand()V

    #@87
    .line 123
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@89
    invoke-direct {v2, v5, v6}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@8c
    goto :goto_23

    #@8d
    .line 127
    :cond_8d
    const-string v2, "AtCmarCmdHandler"

    #@8f
    const-string v3, "+CMAR=<pin lock code > Verification failed"

    #@91
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 128
    new-instance v2, Lcom/android/internal/atfwd/AtCmdResponse;

    #@96
    const/16 v3, 0x10

    #@98
    invoke-virtual {p1, v3}, Lcom/android/internal/atfwd/AtCmd;->getAtCmdErrStr(I)Ljava/lang/String;

    #@9b
    move-result-object v3

    #@9c
    invoke-direct {v2, v4, v3}, Lcom/android/internal/atfwd/AtCmdResponse;-><init>(ILjava/lang/String;)V

    #@9f
    goto :goto_23
.end method
