.class Lcom/android/internal/atfwd/AtCfunCmdHandler$1;
.super Ljava/lang/Thread;
.source "AtCfunCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/atfwd/AtCfunCmdHandler;->handleCommand(Lcom/android/internal/atfwd/AtCmd;)Lcom/android/internal/atfwd/AtCmdResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/atfwd/AtCfunCmdHandler;


# direct methods
.method constructor <init>(Lcom/android/internal/atfwd/AtCfunCmdHandler;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 70
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCfunCmdHandler$1;->this$0:Lcom/android/internal/atfwd/AtCfunCmdHandler;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 73
    :try_start_0
    const-string/jumbo v2, "power"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    #@a
    move-result-object v1

    #@b
    .line 74
    .local v1, pm:Landroid/os/IPowerManager;
    const/4 v2, 0x0

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x0

    #@e
    invoke-interface {v1, v2, v3, v4}, Landroid/os/IPowerManager;->reboot(ZLjava/lang/String;Z)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 79
    .end local v1           #pm:Landroid/os/IPowerManager;
    :goto_11
    return-void

    #@12
    .line 75
    :catch_12
    move-exception v0

    #@13
    .line 76
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "AtCfunCmdHandler"

    #@15
    const-string v3, "PowerManager service died!"

    #@17
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    goto :goto_11
.end method
