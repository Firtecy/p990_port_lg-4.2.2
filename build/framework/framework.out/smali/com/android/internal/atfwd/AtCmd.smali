.class public Lcom/android/internal/atfwd/AtCmd;
.super Ljava/lang/Object;
.source "AtCmd.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ATCMD_OPCODE_NA_EQ_AR:I = 0xb

.field public static final ATCMD_OPCODE_NA_EQ_QU:I = 0x7

.field public static final ATCMD_OPCODE_NA_QU:I = 0x5

.field public static final AT_ERR_INCORRECT_PARAMS:I = 0x32

.field public static final AT_ERR_INCORRECT_PASSWORD:I = 0x10

.field public static final AT_ERR_INVALID_CHARS:I = 0x19

.field public static final AT_ERR_NOT_FOUND:I = 0x16

.field public static final AT_ERR_NO_CONN_TO_PHONE:I = 0x1

.field public static final AT_ERR_OP_NOT_ALLOW:I = 0x3

.field public static final AT_ERR_OP_NOT_SUPP:I = 0x4

.field public static final AT_ERR_PHONE_FAILURE:I = 0x0

.field public static final AT_ERR_UNKNOWN:I = 0x64

.field public static final AT_OPCODE_AR:I = 0x8

.field public static final AT_OPCODE_EQ:I = 0x2

.field public static final AT_OPCODE_NA:I = 0x1

.field public static final AT_OPCODE_QU:I = 0x4

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/atfwd/AtCmd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mName:Ljava/lang/String;

.field private mOpcode:I

.field private mTokens:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 170
    new-instance v0, Lcom/android/internal/atfwd/AtCmd$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/atfwd/AtCmd$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/atfwd/AtCmd;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Ljava/lang/String;)V
    .registers 4
    .parameter "opcode"
    .parameter "name"
    .parameter "tokens"

    #@0
    .prologue
    .line 89
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 90
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/atfwd/AtCmd;->init(ILjava/lang/String;[Ljava/lang/String;)V

    #@6
    .line 91
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    .line 93
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v1

    #@7
    .line 95
    .local v1, opcode:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 96
    .local v0, name:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    .line 97
    .local v2, tokens:[Ljava/lang/String;
    invoke-direct {p0, v1, v0, v2}, Lcom/android/internal/atfwd/AtCmd;->init(ILjava/lang/String;[Ljava/lang/String;)V

    #@12
    .line 98
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/internal/atfwd/AtCmd$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/android/internal/atfwd/AtCmd;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private init(ILjava/lang/String;[Ljava/lang/String;)V
    .registers 4
    .parameter "opcode"
    .parameter "name"
    .parameter "tokens"

    #@0
    .prologue
    .line 101
    iput p1, p0, Lcom/android/internal/atfwd/AtCmd;->mOpcode:I

    #@2
    .line 102
    iput-object p2, p0, Lcom/android/internal/atfwd/AtCmd;->mName:Ljava/lang/String;

    #@4
    .line 103
    iput-object p3, p0, Lcom/android/internal/atfwd/AtCmd;->mTokens:[Ljava/lang/String;

    #@6
    .line 104
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 108
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAtCmdErrStr(I)Ljava/lang/String;
    .registers 3
    .parameter "errCode"

    #@0
    .prologue
    .line 134
    const-string v0, "+CME ERROR: 100"

    #@2
    .line 136
    .local v0, errStrResult:Ljava/lang/String;
    sparse-switch p1, :sswitch_data_1e

    #@5
    .line 167
    :goto_5
    :sswitch_5
    return-object v0

    #@6
    .line 138
    :sswitch_6
    const-string v0, "+CME ERROR: 0"

    #@8
    .line 139
    goto :goto_5

    #@9
    .line 141
    :sswitch_9
    const-string v0, "+CME ERROR: 1"

    #@b
    .line 142
    goto :goto_5

    #@c
    .line 144
    :sswitch_c
    const-string v0, "+CME ERROR: 3"

    #@e
    .line 145
    goto :goto_5

    #@f
    .line 147
    :sswitch_f
    const-string v0, "+CME ERROR: 4"

    #@11
    .line 148
    goto :goto_5

    #@12
    .line 150
    :sswitch_12
    const-string v0, "+CME ERROR: 16"

    #@14
    .line 151
    goto :goto_5

    #@15
    .line 153
    :sswitch_15
    const-string v0, "+CME ERROR: 22"

    #@17
    .line 154
    goto :goto_5

    #@18
    .line 156
    :sswitch_18
    const-string v0, "+CME ERROR: 25"

    #@1a
    .line 157
    goto :goto_5

    #@1b
    .line 159
    :sswitch_1b
    const-string v0, "+CME ERROR: 50"

    #@1d
    .line 160
    goto :goto_5

    #@1e
    .line 136
    :sswitch_data_1e
    .sparse-switch
        0x0 -> :sswitch_6
        0x1 -> :sswitch_9
        0x3 -> :sswitch_c
        0x4 -> :sswitch_f
        0x10 -> :sswitch_12
        0x16 -> :sswitch_15
        0x19 -> :sswitch_18
        0x32 -> :sswitch_1b
        0x64 -> :sswitch_5
    .end sparse-switch
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCmd;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOpcode()I
    .registers 2

    #@0
    .prologue
    .line 65
    iget v0, p0, Lcom/android/internal/atfwd/AtCmd;->mOpcode:I

    #@2
    return v0
.end method

.method public getTokens()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCmd;->mTokens:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .parameter "mName"

    #@0
    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCmd;->mName:Ljava/lang/String;

    #@2
    .line 78
    return-void
.end method

.method public setOpcode(I)V
    .registers 2
    .parameter "mOpcode"

    #@0
    .prologue
    .line 69
    iput p1, p0, Lcom/android/internal/atfwd/AtCmd;->mOpcode:I

    #@2
    .line 70
    return-void
.end method

.method public setTokens([Ljava/lang/String;)V
    .registers 2
    .parameter "mTokens"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCmd;->mTokens:[Ljava/lang/String;

    #@2
    .line 86
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 118
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v6, "AtCmd { opcode = "

    #@7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    iget v6, p0, Lcom/android/internal/atfwd/AtCmd;->mOpcode:I

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    const-string v6, ", name = "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    iget-object v6, p0, Lcom/android/internal/atfwd/AtCmd;->mName:Ljava/lang/String;

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    const-string v6, " mTokens = {"

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    .line 119
    .local v3, ret:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCmd;->mTokens:[Ljava/lang/String;

    #@29
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@2a
    .local v2, len$:I
    const/4 v1, 0x0

    #@2b
    .local v1, i$:I
    :goto_2b
    if-ge v1, v2, :cond_4f

    #@2d
    aget-object v4, v0, v1

    #@2f
    .line 120
    .local v4, token:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    const-string v6, " "

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    const-string v6, ","

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    .line 119
    add-int/lit8 v1, v1, 0x1

    #@4e
    goto :goto_2b

    #@4f
    .line 122
    .end local v4           #token:Ljava/lang/String;
    :cond_4f
    new-instance v5, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    const-string/jumbo v6, "}"

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    .line 123
    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 112
    iget v0, p0, Lcom/android/internal/atfwd/AtCmd;->mOpcode:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 113
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCmd;->mName:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 114
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCmd;->mTokens:[Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@f
    .line 115
    return-void
.end method
