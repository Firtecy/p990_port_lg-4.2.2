.class public Lcom/android/internal/atfwd/AtCmdResponse;
.super Ljava/lang/Object;
.source "AtCmdResponse.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/atfwd/AtCmdResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final RESULT_ERROR:I = 0x0

.field public static final RESULT_OK:I = 0x1

.field public static final RESULT_OTHER:I = 0x2


# instance fields
.field private mResponse:Ljava/lang/String;

.field private mResult:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 83
    new-instance v0, Lcom/android/internal/atfwd/AtCmdResponse$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/atfwd/AtCmdResponse$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/atfwd/AtCmdResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method constructor <init>(ILjava/lang/String;)V
    .registers 3
    .parameter "result"
    .parameter "response"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/android/internal/atfwd/AtCmdResponse;->init(ILjava/lang/String;)V

    #@6
    .line 62
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "p"
    .parameter "flags"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v1

    #@7
    .line 66
    .local v1, result:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 67
    .local v0, response:Ljava/lang/String;
    invoke-direct {p0, v1, v0}, Lcom/android/internal/atfwd/AtCmdResponse;->init(ILjava/lang/String;)V

    #@e
    .line 68
    return-void
.end method

.method private init(ILjava/lang/String;)V
    .registers 3
    .parameter "result"
    .parameter "response"

    #@0
    .prologue
    .line 71
    iput p1, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResult:I

    #@2
    .line 72
    iput-object p2, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResponse:Ljava/lang/String;

    #@4
    .line 73
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getResponse()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResponse:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getResult()I
    .registers 2

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResult:I

    #@2
    return v0
.end method

.method public setResponse(Ljava/lang/String;)V
    .registers 2
    .parameter "mResponse"

    #@0
    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResponse:Ljava/lang/String;

    #@2
    .line 58
    return-void
.end method

.method public setResult(I)V
    .registers 2
    .parameter "mResult"

    #@0
    .prologue
    .line 49
    iput p1, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResult:I

    #@2
    .line 50
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 79
    iget v0, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResult:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 80
    iget-object v0, p0, Lcom/android/internal/atfwd/AtCmdResponse;->mResponse:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 81
    return-void
.end method
