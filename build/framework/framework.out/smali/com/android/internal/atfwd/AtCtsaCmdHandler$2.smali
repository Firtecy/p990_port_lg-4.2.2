.class Lcom/android/internal/atfwd/AtCtsaCmdHandler$2;
.super Landroid/os/Handler;
.source "AtCtsaCmdHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/atfwd/AtCtsaCmdHandler;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/atfwd/AtCtsaCmdHandler;


# direct methods
.method constructor <init>(Lcom/android/internal/atfwd/AtCtsaCmdHandler;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 183
    iput-object p1, p0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$2;->this$0:Lcom/android/internal/atfwd/AtCtsaCmdHandler;

    #@2
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 18
    .parameter "msg"

    #@0
    .prologue
    .line 185
    move-object/from16 v0, p1

    #@2
    iget v3, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v3, :pswitch_data_9e

    #@7
    .line 221
    :cond_7
    return-void

    #@8
    .line 187
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v9, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;

    #@e
    .line 188
    .local v9, cmd:Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;
    const-string v3, "AtCtsaCmdHandler"

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "Command de-queued: "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 189
    if-eqz v9, :cond_7

    #@28
    .line 191
    invoke-virtual {v9}, Lcom/android/internal/atfwd/AtCtsaCmdHandler$ParsedCtsaCmd;->getEvents()Ljava/util/Vector;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    #@2f
    move-result-object v13

    #@30
    .local v13, i$:Ljava/util/Iterator;
    :goto_30
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_7

    #@36
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@39
    move-result-object v14

    #@3a
    .line 192
    .local v14, obj:Ljava/lang/Object;
    instance-of v3, v14, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;

    #@3c
    if-eqz v3, :cond_51

    #@3e
    move-object v12, v14

    #@3f
    .line 193
    check-cast v12, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;

    #@41
    .line 195
    .local v12, evt:Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;
    :try_start_41
    invoke-virtual {v12}, Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;->getTime()J

    #@44
    move-result-wide v3

    #@45
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_48
    .catch Ljava/lang/InterruptedException; {:try_start_41 .. :try_end_48} :catch_49

    #@48
    goto :goto_30

    #@49
    .line 197
    :catch_49
    move-exception v10

    #@4a
    .line 198
    .local v10, e:Ljava/lang/InterruptedException;
    const-string v3, "AtCtsaCmdHandler"

    #@4c
    const-string v4, "PauseEvent interrupted"

    #@4e
    invoke-static {v3, v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51
    .line 202
    .end local v10           #e:Ljava/lang/InterruptedException;
    .end local v12           #evt:Lcom/android/internal/atfwd/AtCmdHandler$PauseEvent;
    :cond_51
    instance-of v3, v14, Landroid/view/MotionEvent;

    #@53
    if-nez v3, :cond_76

    #@55
    .line 203
    const-string v3, "AtCtsaCmdHandler"

    #@57
    new-instance v4, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v5, "Ignoring unkown event of type "

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    goto :goto_30

    #@76
    .line 208
    :cond_76
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@79
    move-result-wide v1

    #@7a
    .local v1, time:J
    move-object v15, v14

    #@7b
    .line 210
    check-cast v15, Landroid/view/MotionEvent;

    #@7d
    .line 211
    .local v15, oev:Landroid/view/MotionEvent;
    invoke-virtual {v15}, Landroid/view/MotionEvent;->getAction()I

    #@80
    move-result v5

    #@81
    invoke-virtual {v15}, Landroid/view/MotionEvent;->getX()F

    #@84
    move-result v6

    #@85
    invoke-virtual {v15}, Landroid/view/MotionEvent;->getY()F

    #@88
    move-result v7

    #@89
    invoke-virtual {v15}, Landroid/view/MotionEvent;->getMetaState()I

    #@8c
    move-result v8

    #@8d
    move-wide v3, v1

    #@8e
    invoke-static/range {v1 .. v8}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@91
    move-result-object v11

    #@92
    .line 216
    .local v11, ev:Landroid/view/MotionEvent;
    invoke-virtual {v15}, Landroid/view/MotionEvent;->recycle()V

    #@95
    .line 217
    move-object/from16 v0, p0

    #@97
    iget-object v3, v0, Lcom/android/internal/atfwd/AtCtsaCmdHandler$2;->this$0:Lcom/android/internal/atfwd/AtCtsaCmdHandler;

    #@99
    const/4 v4, 0x0

    #@9a
    invoke-static {v3, v11, v4}, Lcom/android/internal/atfwd/AtCtsaCmdHandler;->access$000(Lcom/android/internal/atfwd/AtCtsaCmdHandler;Landroid/view/MotionEvent;Z)V

    #@9d
    goto :goto_30

    #@9e
    .line 185
    :pswitch_data_9e
    .packed-switch 0x0
        :pswitch_8
    .end packed-switch
.end method
