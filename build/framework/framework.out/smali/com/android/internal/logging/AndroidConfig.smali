.class public Lcom/android/internal/logging/AndroidConfig;
.super Ljava/lang/Object;
.source "AndroidConfig.java"


# direct methods
.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    :try_start_3
    const-string v2, ""

    #@5
    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    #@8
    move-result-object v1

    #@9
    .line 38
    .local v1, rootLogger:Ljava/util/logging/Logger;
    new-instance v2, Lcom/android/internal/logging/AndroidHandler;

    #@b
    invoke-direct {v2}, Lcom/android/internal/logging/AndroidHandler;-><init>()V

    #@e
    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    #@11
    .line 39
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    #@13
    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    #@16
    .line 42
    const-string/jumbo v2, "org.apache"

    #@19
    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    #@1c
    move-result-object v2

    #@1d
    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    #@1f
    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_22} :catch_23

    #@22
    .line 46
    .end local v1           #rootLogger:Ljava/util/logging/Logger;
    :goto_22
    return-void

    #@23
    .line 43
    :catch_23
    move-exception v0

    #@24
    .line 44
    .local v0, ex:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@27
    goto :goto_22
.end method
