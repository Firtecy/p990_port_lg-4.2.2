.class final Lcom/android/internal/logging/AndroidHandler$1;
.super Ljava/util/logging/Formatter;
.source "AndroidHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/logging/AndroidHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Ljava/util/logging/Formatter;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public format(Ljava/util/logging/LogRecord;)Ljava/lang/String;
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 91
    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getThrown()Ljava/lang/Throwable;

    #@3
    move-result-object v2

    #@4
    .line 92
    .local v2, thrown:Ljava/lang/Throwable;
    if-eqz v2, :cond_27

    #@6
    .line 93
    new-instance v1, Ljava/io/StringWriter;

    #@8
    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    #@b
    .line 94
    .local v1, sw:Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    #@d
    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@10
    .line 95
    .local v0, pw:Ljava/io/PrintWriter;
    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getMessage()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v1, v3}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    #@17
    .line 96
    const-string v3, "\n"

    #@19
    invoke-virtual {v1, v3}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    #@1c
    .line 97
    invoke-virtual {v2, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    #@1f
    .line 98
    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V

    #@22
    .line 99
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    .line 101
    .end local v0           #pw:Ljava/io/PrintWriter;
    .end local v1           #sw:Ljava/io/StringWriter;
    :goto_26
    return-object v3

    #@27
    :cond_27
    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getMessage()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    goto :goto_26
.end method
