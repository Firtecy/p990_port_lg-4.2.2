.class public Lcom/android/internal/logging/AndroidHandler;
.super Ljava/util/logging/Handler;
.source "AndroidHandler.java"

# interfaces
.implements Ldalvik/system/DalvikLogHandler;


# static fields
.field private static final THE_FORMATTER:Ljava/util/logging/Formatter;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 88
    new-instance v0, Lcom/android/internal/logging/AndroidHandler$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/logging/AndroidHandler$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/logging/AndroidHandler;->THE_FORMATTER:Ljava/util/logging/Formatter;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 109
    invoke-direct {p0}, Ljava/util/logging/Handler;-><init>()V

    #@3
    .line 110
    sget-object v0, Lcom/android/internal/logging/AndroidHandler;->THE_FORMATTER:Ljava/util/logging/Formatter;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/logging/AndroidHandler;->setFormatter(Ljava/util/logging/Formatter;)V

    #@8
    .line 111
    return-void
.end method

.method static getAndroidLevel(Ljava/util/logging/Level;)I
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 161
    invoke-virtual {p0}, Ljava/util/logging/Level;->intValue()I

    #@3
    move-result v0

    #@4
    .line 162
    .local v0, value:I
    const/16 v1, 0x3e8

    #@6
    if-lt v0, v1, :cond_a

    #@8
    .line 163
    const/4 v1, 0x6

    #@9
    .line 169
    :goto_9
    return v1

    #@a
    .line 164
    :cond_a
    const/16 v1, 0x384

    #@c
    if-lt v0, v1, :cond_10

    #@e
    .line 165
    const/4 v1, 0x5

    #@f
    goto :goto_9

    #@10
    .line 166
    :cond_10
    const/16 v1, 0x320

    #@12
    if-lt v0, v1, :cond_16

    #@14
    .line 167
    const/4 v1, 0x4

    #@15
    goto :goto_9

    #@16
    .line 169
    :cond_16
    const/4 v1, 0x3

    #@17
    goto :goto_9
.end method


# virtual methods
.method public close()V
    .registers 1

    #@0
    .prologue
    .line 116
    return-void
.end method

.method public flush()V
    .registers 1

    #@0
    .prologue
    .line 121
    return-void
.end method

.method public publish(Ljava/util/logging/LogRecord;)V
    .registers 8
    .parameter "record"

    #@0
    .prologue
    .line 125
    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getLevel()Ljava/util/logging/Level;

    #@3
    move-result-object v4

    #@4
    invoke-static {v4}, Lcom/android/internal/logging/AndroidHandler;->getAndroidLevel(Ljava/util/logging/Level;)I

    #@7
    move-result v1

    #@8
    .line 126
    .local v1, level:I
    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getLoggerName()Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    invoke-static {v4}, Ldalvik/system/DalvikLogging;->loggerNameToTag(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    .line 127
    .local v3, tag:Ljava/lang/String;
    invoke-static {v3, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_17

    #@16
    .line 137
    :goto_16
    return-void

    #@17
    .line 132
    :cond_17
    :try_start_17
    invoke-virtual {p0}, Lcom/android/internal/logging/AndroidHandler;->getFormatter()Ljava/util/logging/Formatter;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p1}, Ljava/util/logging/Formatter;->format(Ljava/util/logging/LogRecord;)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    .line 133
    .local v2, message:Ljava/lang/String;
    invoke-static {v1, v3, v2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_22
    .catch Ljava/lang/RuntimeException; {:try_start_17 .. :try_end_22} :catch_23

    #@22
    goto :goto_16

    #@23
    .line 134
    .end local v2           #message:Ljava/lang/String;
    :catch_23
    move-exception v0

    #@24
    .line 135
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v4, "AndroidHandler"

    #@26
    const-string v5, "Error logging message."

    #@28
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2b
    goto :goto_16
.end method

.method public publish(Ljava/util/logging/Logger;Ljava/lang/String;Ljava/util/logging/Level;Ljava/lang/String;)V
    .registers 9
    .parameter "source"
    .parameter "tag"
    .parameter "level"
    .parameter "message"

    #@0
    .prologue
    .line 141
    invoke-static {p3}, Lcom/android/internal/logging/AndroidHandler;->getAndroidLevel(Ljava/util/logging/Level;)I

    #@3
    move-result v1

    #@4
    .line 142
    .local v1, priority:I
    invoke-static {p2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_b

    #@a
    .line 151
    :goto_a
    return-void

    #@b
    .line 147
    :cond_b
    :try_start_b
    invoke-static {v1, p2, p4}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_e} :catch_f

    #@e
    goto :goto_a

    #@f
    .line 148
    :catch_f
    move-exception v0

    #@10
    .line 149
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "AndroidHandler"

    #@12
    const-string v3, "Error logging message."

    #@14
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    goto :goto_a
.end method
