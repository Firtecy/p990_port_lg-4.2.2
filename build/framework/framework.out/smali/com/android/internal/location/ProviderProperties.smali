.class public final Lcom/android/internal/location/ProviderProperties;
.super Ljava/lang/Object;
.source "ProviderProperties.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/location/ProviderProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mAccuracy:I

.field public final mHasMonetaryCost:Z

.field public final mPowerRequirement:I

.field public final mRequiresCell:Z

.field public final mRequiresNetwork:Z

.field public final mRequiresSatellite:Z

.field public final mSupportsAltitude:Z

.field public final mSupportsBearing:Z

.field public final mSupportsSpeed:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 112
    new-instance v0, Lcom/android/internal/location/ProviderProperties$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/location/ProviderProperties$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/location/ProviderProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(ZZZZZZZII)V
    .registers 10
    .parameter "mRequiresNetwork"
    .parameter "mRequiresSatellite"
    .parameter "mRequiresCell"
    .parameter "mHasMonetaryCost"
    .parameter "mSupportsAltitude"
    .parameter "mSupportsSpeed"
    .parameter "mSupportsBearing"
    .parameter "mPowerRequirement"
    .parameter "mAccuracy"

    #@0
    .prologue
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 101
    iput-boolean p1, p0, Lcom/android/internal/location/ProviderProperties;->mRequiresNetwork:Z

    #@5
    .line 102
    iput-boolean p2, p0, Lcom/android/internal/location/ProviderProperties;->mRequiresSatellite:Z

    #@7
    .line 103
    iput-boolean p3, p0, Lcom/android/internal/location/ProviderProperties;->mRequiresCell:Z

    #@9
    .line 104
    iput-boolean p4, p0, Lcom/android/internal/location/ProviderProperties;->mHasMonetaryCost:Z

    #@b
    .line 105
    iput-boolean p5, p0, Lcom/android/internal/location/ProviderProperties;->mSupportsAltitude:Z

    #@d
    .line 106
    iput-boolean p6, p0, Lcom/android/internal/location/ProviderProperties;->mSupportsSpeed:Z

    #@f
    .line 107
    iput-boolean p7, p0, Lcom/android/internal/location/ProviderProperties;->mSupportsBearing:Z

    #@11
    .line 108
    iput p8, p0, Lcom/android/internal/location/ProviderProperties;->mPowerRequirement:I

    #@13
    .line 109
    iput p9, p0, Lcom/android/internal/location/ProviderProperties;->mAccuracy:I

    #@15
    .line 110
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 137
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 142
    iget-boolean v0, p0, Lcom/android/internal/location/ProviderProperties;->mRequiresNetwork:Z

    #@4
    if-eqz v0, :cond_44

    #@6
    move v0, v1

    #@7
    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 143
    iget-boolean v0, p0, Lcom/android/internal/location/ProviderProperties;->mRequiresSatellite:Z

    #@c
    if-eqz v0, :cond_46

    #@e
    move v0, v1

    #@f
    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 144
    iget-boolean v0, p0, Lcom/android/internal/location/ProviderProperties;->mRequiresCell:Z

    #@14
    if-eqz v0, :cond_48

    #@16
    move v0, v1

    #@17
    :goto_17
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 145
    iget-boolean v0, p0, Lcom/android/internal/location/ProviderProperties;->mHasMonetaryCost:Z

    #@1c
    if-eqz v0, :cond_4a

    #@1e
    move v0, v1

    #@1f
    :goto_1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 146
    iget-boolean v0, p0, Lcom/android/internal/location/ProviderProperties;->mSupportsAltitude:Z

    #@24
    if-eqz v0, :cond_4c

    #@26
    move v0, v1

    #@27
    :goto_27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 147
    iget-boolean v0, p0, Lcom/android/internal/location/ProviderProperties;->mSupportsSpeed:Z

    #@2c
    if-eqz v0, :cond_4e

    #@2e
    move v0, v1

    #@2f
    :goto_2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 148
    iget-boolean v0, p0, Lcom/android/internal/location/ProviderProperties;->mSupportsBearing:Z

    #@34
    if-eqz v0, :cond_50

    #@36
    :goto_36
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 149
    iget v0, p0, Lcom/android/internal/location/ProviderProperties;->mPowerRequirement:I

    #@3b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 150
    iget v0, p0, Lcom/android/internal/location/ProviderProperties;->mAccuracy:I

    #@40
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    .line 151
    return-void

    #@44
    :cond_44
    move v0, v2

    #@45
    .line 142
    goto :goto_7

    #@46
    :cond_46
    move v0, v2

    #@47
    .line 143
    goto :goto_f

    #@48
    :cond_48
    move v0, v2

    #@49
    .line 144
    goto :goto_17

    #@4a
    :cond_4a
    move v0, v2

    #@4b
    .line 145
    goto :goto_1f

    #@4c
    :cond_4c
    move v0, v2

    #@4d
    .line 146
    goto :goto_27

    #@4e
    :cond_4e
    move v0, v2

    #@4f
    .line 147
    goto :goto_2f

    #@50
    :cond_50
    move v1, v2

    #@51
    .line 148
    goto :goto_36
.end method
