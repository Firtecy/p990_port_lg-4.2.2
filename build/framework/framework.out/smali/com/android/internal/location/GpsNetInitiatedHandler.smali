.class public Lcom/android/internal/location/GpsNetInitiatedHandler;
.super Ljava/lang/Object;
.source "GpsNetInitiatedHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiResponse;,
        Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;
    }
.end annotation


# static fields
.field public static final ACTION_NI_VERIFY:Ljava/lang/String; = "android.intent.action.NETWORK_INITIATED_VERIFY"

.field private static final DEBUG:Z = true

.field public static final GPS_ENC_NONE:I = 0x0

.field public static final GPS_ENC_SUPL_GSM_DEFAULT:I = 0x1

.field public static final GPS_ENC_SUPL_UCS2:I = 0x3

.field public static final GPS_ENC_SUPL_UTF8:I = 0x2

.field public static final GPS_ENC_UNKNOWN:I = -0x1

.field public static final GPS_NI_NEED_NOTIFY:I = 0x1

.field public static final GPS_NI_NEED_VERIFY:I = 0x2

.field public static final GPS_NI_PRIVACY_OVERRIDE:I = 0x4

.field public static final GPS_NI_RESPONSE_ACCEPT:I = 0x1

.field public static final GPS_NI_RESPONSE_DENY:I = 0x2

.field public static final GPS_NI_RESPONSE_NORESP:I = 0x3

.field public static final GPS_NI_TYPE_UMTS_CTRL_PLANE:I = 0x3

.field public static final GPS_NI_TYPE_UMTS_SUPL:I = 0x2

.field public static final GPS_NI_TYPE_VOICE:I = 0x1

.field public static final NI_EXTRA_CMD_NOTIF_ID:Ljava/lang/String; = "notif_id"

.field public static final NI_EXTRA_CMD_RESPONSE:Ljava/lang/String; = "response"

.field public static final NI_INTENT_KEY_DEFAULT_RESPONSE:Ljava/lang/String; = "default_resp"

.field public static final NI_INTENT_KEY_MESSAGE:Ljava/lang/String; = "message"

.field public static final NI_INTENT_KEY_NOTIF_ID:Ljava/lang/String; = "notif_id"

.field public static final NI_INTENT_KEY_TIMEOUT:Ljava/lang/String; = "timeout"

.field public static final NI_INTENT_KEY_TITLE:Ljava/lang/String; = "title"

.field public static final NI_RESPONSE_EXTRA_CMD:Ljava/lang/String; = "send_ni_response"

.field private static final TAG:Ljava/lang/String; = "GpsNetInitiatedHandler"

.field private static final VERBOSE:Z

.field private static mIsHexInput:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLocationManager:Landroid/location/LocationManager;

.field private mNiNotification:Landroid/app/Notification;

.field private mPlaySounds:Z

.field private mPopupImmediately:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 96
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mIsHexInput:Z

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 92
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mPlaySounds:Z

    #@6
    .line 93
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mPopupImmediately:Z

    #@9
    .line 130
    iput-object p1, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@b
    .line 131
    const-string/jumbo v0, "location"

    #@e
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/location/LocationManager;

    #@14
    iput-object v0, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mLocationManager:Landroid/location/LocationManager;

    #@16
    .line 132
    return-void
.end method

.method static decodeGSMPackedString([B)Ljava/lang/String;
    .registers 7
    .parameter "input"

    #@0
    .prologue
    .line 289
    const/4 v0, 0x0

    #@1
    .line 290
    .local v0, PADDING_CHAR:C
    array-length v2, p0

    #@2
    .line 291
    .local v2, lengthBytes:I
    mul-int/lit8 v4, v2, 0x8

    #@4
    div-int/lit8 v3, v4, 0x7

    #@6
    .line 298
    .local v3, lengthSeptets:I
    rem-int/lit8 v4, v2, 0x7

    #@8
    if-nez v4, :cond_16

    #@a
    .line 299
    if-lez v2, :cond_16

    #@c
    .line 300
    add-int/lit8 v4, v2, -0x1

    #@e
    aget-byte v4, p0, v4

    #@10
    shr-int/lit8 v4, v4, 0x1

    #@12
    if-nez v4, :cond_16

    #@14
    .line 301
    add-int/lit8 v3, v3, -0x1

    #@16
    .line 306
    :cond_16
    const/4 v4, 0x0

    #@17
    invoke-static {p0, v4, v3}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 309
    .local v1, decoded:Ljava/lang/String;
    if-nez v1, :cond_26

    #@1d
    .line 310
    const-string v4, "GpsNetInitiatedHandler"

    #@1f
    const-string v5, "Decoding of GSM packed string failed"

    #@21
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 311
    const-string v1, ""

    #@26
    .line 314
    :cond_26
    return-object v1
.end method

.method private static decodeString(Ljava/lang/String;ZI)Ljava/lang/String;
    .registers 8
    .parameter "original"
    .parameter "isHex"
    .parameter "coding"

    #@0
    .prologue
    .line 357
    move-object v0, p0

    #@1
    .line 358
    .local v0, decoded:Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->stringToByteArray(Ljava/lang/String;Z)[B

    #@4
    move-result-object v1

    #@5
    .line 360
    .local v1, input:[B
    packed-switch p2, :pswitch_data_3e

    #@8
    .line 382
    const-string v2, "GpsNetInitiatedHandler"

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Unknown encoding "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, " for NI text "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 385
    :goto_2a
    return-object v0

    #@2b
    .line 362
    :pswitch_2b
    move-object v0, p0

    #@2c
    .line 363
    goto :goto_2a

    #@2d
    .line 366
    :pswitch_2d
    invoke-static {v1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->decodeGSMPackedString([B)Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    .line 367
    goto :goto_2a

    #@32
    .line 370
    :pswitch_32
    invoke-static {v1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->decodeUTF8String([B)Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    .line 371
    goto :goto_2a

    #@37
    .line 374
    :pswitch_37
    invoke-static {v1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->decodeUCS2String([B)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    .line 375
    goto :goto_2a

    #@3c
    .line 378
    :pswitch_3c
    move-object v0, p0

    #@3d
    .line 379
    goto :goto_2a

    #@3e
    .line 360
    :pswitch_data_3e
    .packed-switch -0x1
        :pswitch_3c
        :pswitch_2b
        :pswitch_2d
        :pswitch_32
        :pswitch_37
    .end packed-switch
.end method

.method static decodeUCS2String([B)Ljava/lang/String;
    .registers 4
    .parameter "input"

    #@0
    .prologue
    .line 332
    const-string v0, ""

    #@2
    .line 334
    .local v0, decoded:Ljava/lang/String;
    :try_start_2
    new-instance v0, Ljava/lang/String;

    #@4
    .end local v0           #decoded:Ljava/lang/String;
    const-string v2, "UTF-16"

    #@6
    invoke-direct {v0, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_9} :catch_a

    #@9
    .line 340
    .restart local v0       #decoded:Ljava/lang/String;
    return-object v0

    #@a
    .line 336
    .end local v0           #decoded:Ljava/lang/String;
    :catch_a
    move-exception v1

    #@b
    .line 338
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/AssertionError;

    #@d
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@10
    throw v2
.end method

.method static decodeUTF8String([B)Ljava/lang/String;
    .registers 4
    .parameter "input"

    #@0
    .prologue
    .line 319
    const-string v0, ""

    #@2
    .line 321
    .local v0, decoded:Ljava/lang/String;
    :try_start_2
    new-instance v0, Ljava/lang/String;

    #@4
    .end local v0           #decoded:Ljava/lang/String;
    const-string v2, "UTF-8"

    #@6
    invoke-direct {v0, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_9} :catch_a

    #@9
    .line 327
    .restart local v0       #decoded:Ljava/lang/String;
    return-object v0

    #@a
    .line 323
    .end local v0           #decoded:Ljava/lang/String;
    :catch_a
    move-exception v1

    #@b
    .line 325
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/AssertionError;

    #@d
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@10
    throw v2
.end method

.method private static getDialogMessage(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "notif"
    .parameter "context"

    #@0
    .prologue
    .line 422
    invoke-static {p0, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getNotifMessage(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getDialogTitle(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "notif"
    .parameter "context"

    #@0
    .prologue
    .line 416
    invoke-static {p0, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getNotifTitle(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private getDlgIntent(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)Landroid/content/Intent;
    .registers 8
    .parameter "notif"

    #@0
    .prologue
    .line 236
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 237
    .local v0, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@7
    invoke-static {p1, v3}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getDialogTitle(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    .line 238
    .local v2, title:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@d
    invoke-static {p1, v3}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getDialogMessage(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 241
    .local v1, message:Ljava/lang/String;
    const/high16 v3, 0x1000

    #@13
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@16
    .line 242
    iget-object v3, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@18
    const-class v4, Lcom/android/internal/app/NetInitiatedActivity;

    #@1a
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    #@1d
    .line 245
    const-string/jumbo v3, "notif_id"

    #@20
    iget v4, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->notificationId:I

    #@22
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@25
    .line 246
    const-string/jumbo v3, "title"

    #@28
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2b
    .line 247
    const-string/jumbo v3, "message"

    #@2e
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@31
    .line 248
    const-string/jumbo v3, "timeout"

    #@34
    iget v4, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->timeout:I

    #@36
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@39
    .line 249
    const-string v3, "default_resp"

    #@3b
    iget v4, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->defaultResponse:I

    #@3d
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@40
    .line 251
    const-string v3, "GpsNetInitiatedHandler"

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "generateIntent, title: "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    const-string v5, ", message: "

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    const-string v5, ", timeout: "

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    iget v5, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->timeout:I

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v4

    #@6b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 254
    return-object v0
.end method

.method private static getNotifMessage(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    .registers 9
    .parameter "notif"
    .parameter "context"

    #@0
    .prologue
    .line 407
    const v1, 0x10404d8

    #@3
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    const/4 v2, 0x2

    #@8
    new-array v2, v2, [Ljava/lang/Object;

    #@a
    const/4 v3, 0x0

    #@b
    iget-object v4, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorId:Ljava/lang/String;

    #@d
    sget-boolean v5, Lcom/android/internal/location/GpsNetInitiatedHandler;->mIsHexInput:Z

    #@f
    iget v6, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorIdEncoding:I

    #@11
    invoke-static {v4, v5, v6}, Lcom/android/internal/location/GpsNetInitiatedHandler;->decodeString(Ljava/lang/String;ZI)Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    aput-object v4, v2, v3

    #@17
    const/4 v3, 0x1

    #@18
    iget-object v4, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->text:Ljava/lang/String;

    #@1a
    sget-boolean v5, Lcom/android/internal/location/GpsNetInitiatedHandler;->mIsHexInput:Z

    #@1c
    iget v6, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->textEncoding:I

    #@1e
    invoke-static {v4, v5, v6}, Lcom/android/internal/location/GpsNetInitiatedHandler;->decodeString(Ljava/lang/String;ZI)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    aput-object v4, v2, v3

    #@24
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 410
    .local v0, message:Ljava/lang/String;
    return-object v0
.end method

.method private static getNotifTicker(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    .registers 9
    .parameter "notif"
    .parameter "context"

    #@0
    .prologue
    .line 391
    const v1, 0x10404d6

    #@3
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    const/4 v2, 0x2

    #@8
    new-array v2, v2, [Ljava/lang/Object;

    #@a
    const/4 v3, 0x0

    #@b
    iget-object v4, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorId:Ljava/lang/String;

    #@d
    sget-boolean v5, Lcom/android/internal/location/GpsNetInitiatedHandler;->mIsHexInput:Z

    #@f
    iget v6, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorIdEncoding:I

    #@11
    invoke-static {v4, v5, v6}, Lcom/android/internal/location/GpsNetInitiatedHandler;->decodeString(Ljava/lang/String;ZI)Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    aput-object v4, v2, v3

    #@17
    const/4 v3, 0x1

    #@18
    iget-object v4, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->text:Ljava/lang/String;

    #@1a
    sget-boolean v5, Lcom/android/internal/location/GpsNetInitiatedHandler;->mIsHexInput:Z

    #@1c
    iget v6, p0, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->textEncoding:I

    #@1e
    invoke-static {v4, v5, v6}, Lcom/android/internal/location/GpsNetInitiatedHandler;->decodeString(Ljava/lang/String;ZI)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    aput-object v4, v2, v3

    #@24
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 394
    .local v0, ticker:Ljava/lang/String;
    return-object v0
.end method

.method private static getNotifTitle(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "notif"
    .parameter "context"

    #@0
    .prologue
    .line 400
    const v1, 0x10404d7

    #@3
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    const/4 v2, 0x0

    #@8
    new-array v2, v2, [Ljava/lang/Object;

    #@a
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 401
    .local v0, title:Ljava/lang/String;
    return-object v0
.end method

.method private openNiDialog(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)V
    .registers 6
    .parameter "notif"

    #@0
    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getDlgIntent(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 225
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "GpsNetInitiatedHandler"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string/jumbo v3, "openNiDialog, notifyId: "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    iget v3, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->notificationId:I

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, ", requestorId: "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    iget-object v3, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorId:Ljava/lang/String;

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    const-string v3, ", text: "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    iget-object v3, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->text:Ljava/lang/String;

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 229
    iget-object v1, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@39
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@3c
    .line 230
    return-void
.end method

.method private declared-synchronized setNiNotification(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)V
    .registers 11
    .parameter "notif"

    #@0
    .prologue
    .line 182
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@3
    const-string/jumbo v6, "notification"

    #@6
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    check-cast v2, Landroid/app/NotificationManager;
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a2

    #@c
    .line 184
    .local v2, notificationManager:Landroid/app/NotificationManager;
    if-nez v2, :cond_10

    #@e
    .line 218
    :goto_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 188
    :cond_10
    :try_start_10
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@12
    invoke-static {p1, v5}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getNotifTitle(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    .line 189
    .local v4, title:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@18
    invoke-static {p1, v5}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getNotifMessage(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 191
    .local v1, message:Ljava/lang/String;
    const-string v5, "GpsNetInitiatedHandler"

    #@1e
    new-instance v6, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string/jumbo v7, "setNiNotification, notifyId: "

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    iget v7, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->notificationId:I

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    const-string v7, ", title: "

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    const-string v7, ", message: "

    #@3c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 196
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@4d
    if-nez v5, :cond_63

    #@4f
    .line 197
    new-instance v5, Landroid/app/Notification;

    #@51
    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    #@54
    iput-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@56
    .line 198
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@58
    const v6, 0x1080556

    #@5b
    iput v6, v5, Landroid/app/Notification;->icon:I

    #@5d
    .line 199
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@5f
    const-wide/16 v6, 0x0

    #@61
    iput-wide v6, v5, Landroid/app/Notification;->when:J

    #@63
    .line 202
    :cond_63
    iget-boolean v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mPlaySounds:Z

    #@65
    if-eqz v5, :cond_a5

    #@67
    .line 203
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@69
    iget v6, v5, Landroid/app/Notification;->defaults:I

    #@6b
    or-int/lit8 v6, v6, 0x1

    #@6d
    iput v6, v5, Landroid/app/Notification;->defaults:I

    #@6f
    .line 208
    :goto_6f
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@71
    const/16 v6, 0x12

    #@73
    iput v6, v5, Landroid/app/Notification;->flags:I

    #@75
    .line 209
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@77
    iget-object v6, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@79
    invoke-static {p1, v6}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getNotifTicker(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;

    #@7c
    move-result-object v6

    #@7d
    iput-object v6, v5, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@7f
    .line 212
    iget-boolean v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mPopupImmediately:Z

    #@81
    if-nez v5, :cond_ae

    #@83
    invoke-direct {p0, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->getDlgIntent(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)Landroid/content/Intent;

    #@86
    move-result-object v0

    #@87
    .line 213
    .local v0, intent:Landroid/content/Intent;
    :goto_87
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@89
    const/4 v6, 0x0

    #@8a
    const/4 v7, 0x0

    #@8b
    invoke-static {v5, v6, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@8e
    move-result-object v3

    #@8f
    .line 214
    .local v3, pi:Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@91
    iget-object v6, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mContext:Landroid/content/Context;

    #@93
    invoke-virtual {v5, v6, v4, v1, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@96
    .line 216
    const/4 v5, 0x0

    #@97
    iget v6, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->notificationId:I

    #@99
    iget-object v7, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@9b
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@9d
    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V
    :try_end_a0
    .catchall {:try_start_10 .. :try_end_a0} :catchall_a2

    #@a0
    goto/16 :goto_e

    #@a2
    .line 182
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #message:Ljava/lang/String;
    .end local v2           #notificationManager:Landroid/app/NotificationManager;
    .end local v3           #pi:Landroid/app/PendingIntent;
    .end local v4           #title:Ljava/lang/String;
    :catchall_a2
    move-exception v5

    #@a3
    monitor-exit p0

    #@a4
    throw v5

    #@a5
    .line 205
    .restart local v1       #message:Ljava/lang/String;
    .restart local v2       #notificationManager:Landroid/app/NotificationManager;
    .restart local v4       #title:Ljava/lang/String;
    :cond_a5
    :try_start_a5
    iget-object v5, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mNiNotification:Landroid/app/Notification;

    #@a7
    iget v6, v5, Landroid/app/Notification;->defaults:I

    #@a9
    and-int/lit8 v6, v6, -0x2

    #@ab
    iput v6, v5, Landroid/app/Notification;->defaults:I

    #@ad
    goto :goto_6f

    #@ae
    .line 212
    :cond_ae
    new-instance v0, Landroid/content/Intent;

    #@b0
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V
    :try_end_b3
    .catchall {:try_start_a5 .. :try_end_b3} :catchall_a2

    #@b3
    goto :goto_87
.end method

.method static stringToByteArray(Ljava/lang/String;Z)[B
    .registers 7
    .parameter "original"
    .parameter "isHex"

    #@0
    .prologue
    .line 260
    if-eqz p1, :cond_25

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v3

    #@6
    div-int/lit8 v1, v3, 0x2

    #@8
    .line 261
    .local v1, length:I
    :goto_8
    new-array v2, v1, [B

    #@a
    .line 264
    .local v2, output:[B
    if-eqz p1, :cond_2a

    #@c
    .line 266
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v1, :cond_37

    #@f
    .line 268
    mul-int/lit8 v3, v0, 0x2

    #@11
    mul-int/lit8 v4, v0, 0x2

    #@13
    add-int/lit8 v4, v4, 0x2

    #@15
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    const/16 v4, 0x10

    #@1b
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1e
    move-result v3

    #@1f
    int-to-byte v3, v3

    #@20
    aput-byte v3, v2, v0

    #@22
    .line 266
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_d

    #@25
    .line 260
    .end local v0           #i:I
    .end local v1           #length:I
    .end local v2           #output:[B
    :cond_25
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@28
    move-result v1

    #@29
    goto :goto_8

    #@2a
    .line 272
    .restart local v1       #length:I
    .restart local v2       #output:[B
    :cond_2a
    const/4 v0, 0x0

    #@2b
    .restart local v0       #i:I
    :goto_2b
    if-ge v0, v1, :cond_37

    #@2d
    .line 274
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@30
    move-result v3

    #@31
    int-to-byte v3, v3

    #@32
    aput-byte v3, v2, v0

    #@34
    .line 272
    add-int/lit8 v0, v0, 0x1

    #@36
    goto :goto_2b

    #@37
    .line 278
    :cond_37
    return-object v2
.end method


# virtual methods
.method public handleNiNotification(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)V
    .registers 5
    .parameter "notif"

    #@0
    .prologue
    .line 137
    const-string v0, "GpsNetInitiatedHandler"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "handleNiNotification notificationId: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->notificationId:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " requestorId: "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorId:Ljava/lang/String;

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " text: "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-object v2, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->text:Ljava/lang/String;

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 141
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needNotify:Z

    #@34
    if-eqz v0, :cond_41

    #@36
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needVerify:Z

    #@38
    if-eqz v0, :cond_41

    #@3a
    iget-boolean v0, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mPopupImmediately:Z

    #@3c
    if-eqz v0, :cond_41

    #@3e
    .line 144
    invoke-direct {p0, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->openNiDialog(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)V

    #@41
    .line 148
    :cond_41
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needNotify:Z

    #@43
    if-eqz v0, :cond_49

    #@45
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needVerify:Z

    #@47
    if-eqz v0, :cond_55

    #@49
    :cond_49
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needNotify:Z

    #@4b
    if-eqz v0, :cond_58

    #@4d
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needVerify:Z

    #@4f
    if-eqz v0, :cond_58

    #@51
    iget-boolean v0, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mPopupImmediately:Z

    #@53
    if-nez v0, :cond_58

    #@55
    .line 156
    :cond_55
    invoke-direct {p0, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;->setNiNotification(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)V

    #@58
    .line 160
    :cond_58
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needNotify:Z

    #@5a
    if-eqz v0, :cond_60

    #@5c
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needVerify:Z

    #@5e
    if-eqz v0, :cond_6c

    #@60
    :cond_60
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needNotify:Z

    #@62
    if-nez v0, :cond_68

    #@64
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needVerify:Z

    #@66
    if-eqz v0, :cond_6c

    #@68
    :cond_68
    iget-boolean v0, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->privacyOverride:Z

    #@6a
    if-eqz v0, :cond_74

    #@6c
    .line 164
    :cond_6c
    iget-object v0, p0, Lcom/android/internal/location/GpsNetInitiatedHandler;->mLocationManager:Landroid/location/LocationManager;

    #@6e
    iget v1, p1, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->notificationId:I

    #@70
    const/4 v2, 0x1

    #@71
    invoke-virtual {v0, v1, v2}, Landroid/location/LocationManager;->sendNiResponse(II)Z

    #@74
    .line 178
    :cond_74
    return-void
.end method
