.class Lcom/android/internal/location/ILocationProvider$Stub$Proxy;
.super Ljava/lang/Object;
.source "ILocationProvider.java"

# interfaces
.implements Lcom/android/internal/location/ILocationProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/location/ILocationProvider$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 156
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 157
    iput-object p1, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 158
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public disable()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 184
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 186
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.location.ILocationProvider"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 187
    iget-object v2, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x2

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 188
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 191
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 192
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 194
    return-void

    #@1e
    .line 191
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 192
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public enable()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 169
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 170
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 172
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.location.ILocationProvider"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 173
    iget-object v2, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x1

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 174
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 177
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 178
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 180
    return-void

    #@1e
    .line 177
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 178
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 165
    const-string v0, "com.android.internal.location.ILocationProvider"

    #@2
    return-object v0
.end method

.method public getProperties()Lcom/android/internal/location/ProviderProperties;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 227
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 228
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 231
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.location.ILocationProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 232
    iget-object v3, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x4

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 233
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 234
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 235
    sget-object v3, Lcom/android/internal/location/ProviderProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Lcom/android/internal/location/ProviderProperties;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 242
    .local v2, _result:Lcom/android/internal/location/ProviderProperties;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 243
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 245
    return-object v2

    #@2c
    .line 238
    .end local v2           #_result:Lcom/android/internal/location/ProviderProperties;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Lcom/android/internal/location/ProviderProperties;
    goto :goto_25

    #@2e
    .line 242
    .end local v2           #_result:Lcom/android/internal/location/ProviderProperties;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 243
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public getStatus(Landroid/os/Bundle;)I
    .registers 8
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 249
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 250
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 253
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.location.ILocationProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 254
    iget-object v3, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x5

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 255
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 256
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v2

    #@1b
    .line 257
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_24

    #@21
    .line 258
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2b

    #@24
    .line 262
    :cond_24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 263
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 265
    return v2

    #@2b
    .line 262
    .end local v2           #_result:I
    :catchall_2b
    move-exception v3

    #@2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 263
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v3
.end method

.method public getStatusUpdateTime()J
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 269
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 270
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 273
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "com.android.internal.location.ILocationProvider"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 274
    iget-object v4, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v5, 0x6

    #@10
    const/4 v6, 0x0

    #@11
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 275
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 276
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result-wide v2

    #@1b
    .line 279
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 280
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 282
    return-wide v2

    #@22
    .line 279
    .end local v2           #_result:J
    :catchall_22
    move-exception v4

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 280
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v4
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 10
    .parameter "command"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 286
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 287
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 290
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.location.ILocationProvider"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 291
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 292
    if-eqz p2, :cond_3c

    #@14
    .line 293
    const/4 v4, 0x1

    #@15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 294
    const/4 v4, 0x0

    #@19
    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c
    .line 299
    :goto_1c
    iget-object v4, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/4 v5, 0x7

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 300
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@26
    .line 301
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_49

    #@2c
    .line 302
    .local v2, _result:Z
    :goto_2c
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v3

    #@30
    if-eqz v3, :cond_35

    #@32
    .line 303
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_35
    .catchall {:try_start_a .. :try_end_35} :catchall_41

    #@35
    .line 307
    :cond_35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 308
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 310
    return v2

    #@3c
    .line 297
    .end local v2           #_result:Z
    :cond_3c
    const/4 v4, 0x0

    #@3d
    :try_start_3d
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_41

    #@40
    goto :goto_1c

    #@41
    .line 307
    :catchall_41
    move-exception v3

    #@42
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 308
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@48
    throw v3

    #@49
    :cond_49
    move v2, v3

    #@4a
    .line 301
    goto :goto_2c
.end method

.method public setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 8
    .parameter "request"
    .parameter "ws"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 197
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 198
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 200
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.location.ILocationProvider"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 201
    if-eqz p1, :cond_32

    #@f
    .line 202
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 203
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/android/internal/location/ProviderRequest;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 208
    :goto_17
    if-eqz p2, :cond_3f

    #@19
    .line 209
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 210
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Landroid/os/WorkSource;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 215
    :goto_21
    iget-object v2, p0, Lcom/android/internal/location/ILocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/4 v3, 0x3

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 216
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_37

    #@2b
    .line 219
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 220
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 222
    return-void

    #@32
    .line 206
    :cond_32
    const/4 v2, 0x0

    #@33
    :try_start_33
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_37

    #@36
    goto :goto_17

    #@37
    .line 219
    :catchall_37
    move-exception v2

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 220
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v2

    #@3f
    .line 213
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_37

    #@43
    goto :goto_21
.end method
