.class public abstract Lcom/android/internal/http/multipart/PartBase;
.super Lcom/android/internal/http/multipart/Part;
.source "PartBase.java"


# instance fields
.field private charSet:Ljava/lang/String;

.field private contentType:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private transferEncoding:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "name"
    .parameter "contentType"
    .parameter "charSet"
    .parameter "transferEncoding"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Lcom/android/internal/http/multipart/Part;-><init>()V

    #@3
    .line 63
    if-nez p1, :cond_d

    #@5
    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "Name must not be null"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 66
    :cond_d
    iput-object p1, p0, Lcom/android/internal/http/multipart/PartBase;->name:Ljava/lang/String;

    #@f
    .line 67
    iput-object p2, p0, Lcom/android/internal/http/multipart/PartBase;->contentType:Ljava/lang/String;

    #@11
    .line 68
    iput-object p3, p0, Lcom/android/internal/http/multipart/PartBase;->charSet:Ljava/lang/String;

    #@13
    .line 69
    iput-object p4, p0, Lcom/android/internal/http/multipart/PartBase;->transferEncoding:Ljava/lang/String;

    #@15
    .line 70
    return-void
.end method


# virtual methods
.method public getCharSet()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/internal/http/multipart/PartBase;->charSet:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/internal/http/multipart/PartBase;->contentType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/http/multipart/PartBase;->name:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTransferEncoding()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/internal/http/multipart/PartBase;->transferEncoding:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public setCharSet(Ljava/lang/String;)V
    .registers 2
    .parameter "charSet"

    #@0
    .prologue
    .line 116
    iput-object p1, p0, Lcom/android/internal/http/multipart/PartBase;->charSet:Ljava/lang/String;

    #@2
    .line 117
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .registers 2
    .parameter "contentType"

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Lcom/android/internal/http/multipart/PartBase;->contentType:Ljava/lang/String;

    #@2
    .line 126
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 134
    if-nez p1, :cond_a

    #@2
    .line 135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Name must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 137
    :cond_a
    iput-object p1, p0, Lcom/android/internal/http/multipart/PartBase;->name:Ljava/lang/String;

    #@c
    .line 138
    return-void
.end method

.method public setTransferEncoding(Ljava/lang/String;)V
    .registers 2
    .parameter "transferEncoding"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Lcom/android/internal/http/multipart/PartBase;->transferEncoding:Ljava/lang/String;

    #@2
    .line 148
    return-void
.end method
