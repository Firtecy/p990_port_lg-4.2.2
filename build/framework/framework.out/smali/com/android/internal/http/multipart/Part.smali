.class public abstract Lcom/android/internal/http/multipart/Part;
.super Ljava/lang/Object;
.source "Part.java"


# static fields
.field protected static final BOUNDARY:Ljava/lang/String; = "----------------314159265358979323846"

.field protected static final BOUNDARY_BYTES:[B = null

.field protected static final CHARSET:Ljava/lang/String; = "; charset="

.field protected static final CHARSET_BYTES:[B = null

.field protected static final CONTENT_DISPOSITION:Ljava/lang/String; = "Content-Disposition: form-data; name="

.field protected static final CONTENT_DISPOSITION_BYTES:[B = null

.field protected static final CONTENT_TRANSFER_ENCODING:Ljava/lang/String; = "Content-Transfer-Encoding: "

.field protected static final CONTENT_TRANSFER_ENCODING_BYTES:[B = null

.field protected static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type: "

.field protected static final CONTENT_TYPE_BYTES:[B = null

.field protected static final CRLF:Ljava/lang/String; = "\r\n"

.field protected static final CRLF_BYTES:[B = null

.field private static final DEFAULT_BOUNDARY_BYTES:[B = null

.field protected static final EXTRA:Ljava/lang/String; = "--"

.field protected static final EXTRA_BYTES:[B = null

.field private static final LOG:Lorg/apache/commons/logging/Log; = null

.field protected static final QUOTE:Ljava/lang/String; = "\""

.field protected static final QUOTE_BYTES:[B


# instance fields
.field private boundaryBytes:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 55
    const-class v0, Lcom/android/internal/http/multipart/Part;

    #@2
    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@8
    .line 67
    const-string v0, "----------------314159265358979323846"

    #@a
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/android/internal/http/multipart/Part;->BOUNDARY_BYTES:[B

    #@10
    .line 73
    sget-object v0, Lcom/android/internal/http/multipart/Part;->BOUNDARY_BYTES:[B

    #@12
    sput-object v0, Lcom/android/internal/http/multipart/Part;->DEFAULT_BOUNDARY_BYTES:[B

    #@14
    .line 79
    const-string v0, "\r\n"

    #@16
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@19
    move-result-object v0

    #@1a
    sput-object v0, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@1c
    .line 85
    const-string v0, "\""

    #@1e
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@21
    move-result-object v0

    #@22
    sput-object v0, Lcom/android/internal/http/multipart/Part;->QUOTE_BYTES:[B

    #@24
    .line 92
    const-string v0, "--"

    #@26
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Lcom/android/internal/http/multipart/Part;->EXTRA_BYTES:[B

    #@2c
    .line 99
    const-string v0, "Content-Disposition: form-data; name="

    #@2e
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@31
    move-result-object v0

    #@32
    sput-object v0, Lcom/android/internal/http/multipart/Part;->CONTENT_DISPOSITION_BYTES:[B

    #@34
    .line 106
    const-string v0, "Content-Type: "

    #@36
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@39
    move-result-object v0

    #@3a
    sput-object v0, Lcom/android/internal/http/multipart/Part;->CONTENT_TYPE_BYTES:[B

    #@3c
    .line 113
    const-string v0, "; charset="

    #@3e
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@41
    move-result-object v0

    #@42
    sput-object v0, Lcom/android/internal/http/multipart/Part;->CHARSET_BYTES:[B

    #@44
    .line 120
    const-string v0, "Content-Transfer-Encoding: "

    #@46
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@49
    move-result-object v0

    #@4a
    sput-object v0, Lcom/android/internal/http/multipart/Part;->CONTENT_TRANSFER_ENCODING_BYTES:[B

    #@4c
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getBoundary()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 129
    const-string v0, "----------------314159265358979323846"

    #@2
    return-object v0
.end method

.method public static getLengthOfParts([Lcom/android/internal/http/multipart/Part;)J
    .registers 3
    .parameter "parts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 404
    sget-object v0, Lcom/android/internal/http/multipart/Part;->DEFAULT_BOUNDARY_BYTES:[B

    #@2
    invoke-static {p0, v0}, Lcom/android/internal/http/multipart/Part;->getLengthOfParts([Lcom/android/internal/http/multipart/Part;[B)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public static getLengthOfParts([Lcom/android/internal/http/multipart/Part;[B)J
    .registers 9
    .parameter "parts"
    .parameter "partBoundary"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 419
    sget-object v5, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v6, "getLengthOfParts(Parts[])"

    #@4
    invoke-interface {v5, v6}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 420
    if-nez p0, :cond_11

    #@9
    .line 421
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v6, "Parts may not be null"

    #@d
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v5

    #@11
    .line 423
    :cond_11
    const-wide/16 v3, 0x0

    #@13
    .line 424
    .local v3, total:J
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    array-length v5, p0

    #@15
    if-ge v0, v5, :cond_2f

    #@17
    .line 426
    aget-object v5, p0, v0

    #@19
    invoke-virtual {v5, p1}, Lcom/android/internal/http/multipart/Part;->setPartBoundary([B)V

    #@1c
    .line 427
    aget-object v5, p0, v0

    #@1e
    invoke-virtual {v5}, Lcom/android/internal/http/multipart/Part;->length()J

    #@21
    move-result-wide v1

    #@22
    .line 428
    .local v1, l:J
    const-wide/16 v5, 0x0

    #@24
    cmp-long v5, v1, v5

    #@26
    if-gez v5, :cond_2b

    #@28
    .line 429
    const-wide/16 v5, -0x1

    #@2a
    .line 437
    .end local v1           #l:J
    :goto_2a
    return-wide v5

    #@2b
    .line 431
    .restart local v1       #l:J
    :cond_2b
    add-long/2addr v3, v1

    #@2c
    .line 424
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_14

    #@2f
    .line 433
    .end local v1           #l:J
    :cond_2f
    sget-object v5, Lcom/android/internal/http/multipart/Part;->EXTRA_BYTES:[B

    #@31
    array-length v5, v5

    #@32
    int-to-long v5, v5

    #@33
    add-long/2addr v3, v5

    #@34
    .line 434
    array-length v5, p1

    #@35
    int-to-long v5, v5

    #@36
    add-long/2addr v3, v5

    #@37
    .line 435
    sget-object v5, Lcom/android/internal/http/multipart/Part;->EXTRA_BYTES:[B

    #@39
    array-length v5, v5

    #@3a
    int-to-long v5, v5

    #@3b
    add-long/2addr v3, v5

    #@3c
    .line 436
    sget-object v5, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@3e
    array-length v5, v5

    #@3f
    int-to-long v5, v5

    #@40
    add-long/2addr v3, v5

    #@41
    move-wide v5, v3

    #@42
    .line 437
    goto :goto_2a
.end method

.method public static sendParts(Ljava/io/OutputStream;[Lcom/android/internal/http/multipart/Part;)V
    .registers 3
    .parameter "out"
    .parameter "parts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 360
    sget-object v0, Lcom/android/internal/http/multipart/Part;->DEFAULT_BOUNDARY_BYTES:[B

    #@2
    invoke-static {p0, p1, v0}, Lcom/android/internal/http/multipart/Part;->sendParts(Ljava/io/OutputStream;[Lcom/android/internal/http/multipart/Part;[B)V

    #@5
    .line 361
    return-void
.end method

.method public static sendParts(Ljava/io/OutputStream;[Lcom/android/internal/http/multipart/Part;[B)V
    .registers 6
    .parameter "out"
    .parameter "parts"
    .parameter "partBoundary"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 377
    if-nez p1, :cond_a

    #@2
    .line 378
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "Parts may not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 380
    :cond_a
    if-eqz p2, :cond_f

    #@c
    array-length v1, p2

    #@d
    if-nez v1, :cond_18

    #@f
    .line 381
    :cond_f
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v2, "partBoundary may not be empty"

    #@14
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1

    #@18
    .line 383
    :cond_18
    const/4 v0, 0x0

    #@19
    .local v0, i:I
    :goto_19
    array-length v1, p1

    #@1a
    if-ge v0, v1, :cond_29

    #@1c
    .line 385
    aget-object v1, p1, v0

    #@1e
    invoke-virtual {v1, p2}, Lcom/android/internal/http/multipart/Part;->setPartBoundary([B)V

    #@21
    .line 386
    aget-object v1, p1, v0

    #@23
    invoke-virtual {v1, p0}, Lcom/android/internal/http/multipart/Part;->send(Ljava/io/OutputStream;)V

    #@26
    .line 383
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_19

    #@29
    .line 388
    :cond_29
    sget-object v1, Lcom/android/internal/http/multipart/Part;->EXTRA_BYTES:[B

    #@2b
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    #@2e
    .line 389
    invoke-virtual {p0, p2}, Ljava/io/OutputStream;->write([B)V

    #@31
    .line 390
    sget-object v1, Lcom/android/internal/http/multipart/Part;->EXTRA_BYTES:[B

    #@33
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    #@36
    .line 391
    sget-object v1, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@38
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    #@3b
    .line 392
    return-void
.end method


# virtual methods
.method public abstract getCharSet()Ljava/lang/String;
.end method

.method public abstract getContentType()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method protected getPartBoundary()[B
    .registers 2

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/internal/http/multipart/Part;->boundaryBytes:[B

    #@2
    if-nez v0, :cond_7

    #@4
    .line 171
    sget-object v0, Lcom/android/internal/http/multipart/Part;->DEFAULT_BOUNDARY_BYTES:[B

    #@6
    .line 173
    :goto_6
    return-object v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/http/multipart/Part;->boundaryBytes:[B

    #@9
    goto :goto_6
.end method

.method public abstract getTransferEncoding()Ljava/lang/String;
.end method

.method public isRepeatable()Z
    .registers 2

    #@0
    .prologue
    .line 195
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public length()J
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 326
    sget-object v1, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v2, "enter length()"

    #@4
    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 327
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->lengthOfData()J

    #@a
    move-result-wide v1

    #@b
    const-wide/16 v3, 0x0

    #@d
    cmp-long v1, v1, v3

    #@f
    if-gez v1, :cond_14

    #@11
    .line 328
    const-wide/16 v1, -0x1

    #@13
    .line 337
    :goto_13
    return-wide v1

    #@14
    .line 330
    :cond_14
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@16
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@19
    .line 331
    .local v0, overhead:Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/internal/http/multipart/Part;->sendStart(Ljava/io/OutputStream;)V

    #@1c
    .line 332
    invoke-virtual {p0, v0}, Lcom/android/internal/http/multipart/Part;->sendDispositionHeader(Ljava/io/OutputStream;)V

    #@1f
    .line 333
    invoke-virtual {p0, v0}, Lcom/android/internal/http/multipart/Part;->sendContentTypeHeader(Ljava/io/OutputStream;)V

    #@22
    .line 334
    invoke-virtual {p0, v0}, Lcom/android/internal/http/multipart/Part;->sendTransferEncodingHeader(Ljava/io/OutputStream;)V

    #@25
    .line 335
    invoke-virtual {p0, v0}, Lcom/android/internal/http/multipart/Part;->sendEndOfHeader(Ljava/io/OutputStream;)V

    #@28
    .line 336
    invoke-virtual {p0, v0}, Lcom/android/internal/http/multipart/Part;->sendEnd(Ljava/io/OutputStream;)V

    #@2b
    .line 337
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    #@2e
    move-result v1

    #@2f
    int-to-long v1, v1

    #@30
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->lengthOfData()J

    #@33
    move-result-wide v3

    #@34
    add-long/2addr v1, v3

    #@35
    goto :goto_13
.end method

.method protected abstract lengthOfData()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public send(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 306
    sget-object v0, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter send(OutputStream out)"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 307
    invoke-virtual {p0, p1}, Lcom/android/internal/http/multipart/Part;->sendStart(Ljava/io/OutputStream;)V

    #@a
    .line 308
    invoke-virtual {p0, p1}, Lcom/android/internal/http/multipart/Part;->sendDispositionHeader(Ljava/io/OutputStream;)V

    #@d
    .line 309
    invoke-virtual {p0, p1}, Lcom/android/internal/http/multipart/Part;->sendContentTypeHeader(Ljava/io/OutputStream;)V

    #@10
    .line 310
    invoke-virtual {p0, p1}, Lcom/android/internal/http/multipart/Part;->sendTransferEncodingHeader(Ljava/io/OutputStream;)V

    #@13
    .line 311
    invoke-virtual {p0, p1}, Lcom/android/internal/http/multipart/Part;->sendEndOfHeader(Ljava/io/OutputStream;)V

    #@16
    .line 312
    invoke-virtual {p0, p1}, Lcom/android/internal/http/multipart/Part;->sendData(Ljava/io/OutputStream;)V

    #@19
    .line 313
    invoke-virtual {p0, p1}, Lcom/android/internal/http/multipart/Part;->sendEnd(Ljava/io/OutputStream;)V

    #@1c
    .line 314
    return-void
.end method

.method protected sendContentTypeHeader(Ljava/io/OutputStream;)V
    .registers 6
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 230
    sget-object v2, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v3, "enter sendContentTypeHeader(OutputStream out)"

    #@4
    invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 231
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->getContentType()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 232
    .local v1, contentType:Ljava/lang/String;
    if-eqz v1, :cond_30

    #@d
    .line 233
    sget-object v2, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@f
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    #@12
    .line 234
    sget-object v2, Lcom/android/internal/http/multipart/Part;->CONTENT_TYPE_BYTES:[B

    #@14
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    #@17
    .line 235
    invoke-static {v1}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    #@1e
    .line 236
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->getCharSet()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    .line 237
    .local v0, charSet:Ljava/lang/String;
    if-eqz v0, :cond_30

    #@24
    .line 238
    sget-object v2, Lcom/android/internal/http/multipart/Part;->CHARSET_BYTES:[B

    #@26
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    #@29
    .line 239
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    #@30
    .line 242
    .end local v0           #charSet:Ljava/lang/String;
    :cond_30
    return-void
.end method

.method protected abstract sendData(Ljava/io/OutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected sendDispositionHeader(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    sget-object v0, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter sendDispositionHeader(OutputStream out)"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 218
    sget-object v0, Lcom/android/internal/http/multipart/Part;->CONTENT_DISPOSITION_BYTES:[B

    #@9
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@c
    .line 219
    sget-object v0, Lcom/android/internal/http/multipart/Part;->QUOTE_BYTES:[B

    #@e
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@11
    .line 220
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->getName()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@1c
    .line 221
    sget-object v0, Lcom/android/internal/http/multipart/Part;->QUOTE_BYTES:[B

    #@1e
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@21
    .line 222
    return-void
.end method

.method protected sendEnd(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 293
    sget-object v0, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter sendEnd(OutputStream out)"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 294
    sget-object v0, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@9
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@c
    .line 295
    return-void
.end method

.method protected sendEndOfHeader(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 267
    sget-object v0, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter sendEndOfHeader(OutputStream out)"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 268
    sget-object v0, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@9
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@c
    .line 269
    sget-object v0, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@e
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@11
    .line 270
    return-void
.end method

.method protected sendStart(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 204
    sget-object v0, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter sendStart(OutputStream out)"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 205
    sget-object v0, Lcom/android/internal/http/multipart/Part;->EXTRA_BYTES:[B

    #@9
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@c
    .line 206
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->getPartBoundary()[B

    #@f
    move-result-object v0

    #@10
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@13
    .line 207
    sget-object v0, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@15
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@18
    .line 208
    return-void
.end method

.method protected sendTransferEncodingHeader(Ljava/io/OutputStream;)V
    .registers 5
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 252
    sget-object v1, Lcom/android/internal/http/multipart/Part;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v2, "enter sendTransferEncodingHeader(OutputStream out)"

    #@4
    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 253
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->getTransferEncoding()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 254
    .local v0, transferEncoding:Ljava/lang/String;
    if-eqz v0, :cond_1e

    #@d
    .line 255
    sget-object v1, Lcom/android/internal/http/multipart/Part;->CRLF_BYTES:[B

    #@f
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@12
    .line 256
    sget-object v1, Lcom/android/internal/http/multipart/Part;->CONTENT_TRANSFER_ENCODING_BYTES:[B

    #@14
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@17
    .line 257
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@1e
    .line 259
    :cond_1e
    return-void
.end method

.method setPartBoundary([B)V
    .registers 2
    .parameter "boundaryBytes"

    #@0
    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/internal/http/multipart/Part;->boundaryBytes:[B

    #@2
    .line 186
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/Part;->getName()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
