.class public Lcom/android/internal/http/multipart/StringPart;
.super Lcom/android/internal/http/multipart/PartBase;
.source "StringPart.java"


# static fields
.field public static final DEFAULT_CHARSET:Ljava/lang/String; = "US-ASCII"

.field public static final DEFAULT_CONTENT_TYPE:Ljava/lang/String; = "text/plain"

.field public static final DEFAULT_TRANSFER_ENCODING:Ljava/lang/String; = "8bit"

.field private static final LOG:Lorg/apache/commons/logging/Log;


# instance fields
.field private content:[B

.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    const-class v0, Lcom/android/internal/http/multipart/StringPart;

    #@2
    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/http/multipart/StringPart;->LOG:Lorg/apache/commons/logging/Log;

    #@8
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@4
    .line 104
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "name"
    .parameter "value"
    .parameter "charset"

    #@0
    .prologue
    .line 80
    const-string/jumbo v0, "text/plain"

    #@3
    if-nez p3, :cond_7

    #@5
    const-string p3, "US-ASCII"

    #@7
    .end local p3
    :cond_7
    const-string v1, "8bit"

    #@9
    invoke-direct {p0, p1, v0, p3, v1}, Lcom/android/internal/http/multipart/PartBase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 86
    if-nez p2, :cond_16

    #@e
    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v1, "Value may not be null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 89
    :cond_16
    const/4 v0, 0x0

    #@17
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    #@1a
    move-result v0

    #@1b
    const/4 v1, -0x1

    #@1c
    if-eq v0, v1, :cond_26

    #@1e
    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@20
    const-string v1, "NULs may not be present in string parts"

    #@22
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 93
    :cond_26
    iput-object p2, p0, Lcom/android/internal/http/multipart/StringPart;->value:Ljava/lang/String;

    #@28
    .line 94
    return-void
.end method

.method private getContent()[B
    .registers 3

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Lcom/android/internal/http/multipart/StringPart;->content:[B

    #@2
    if-nez v0, :cond_10

    #@4
    .line 114
    iget-object v0, p0, Lcom/android/internal/http/multipart/StringPart;->value:Ljava/lang/String;

    #@6
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/StringPart;->getCharSet()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-static {v0, v1}, Lorg/apache/http/util/EncodingUtils;->getBytes(Ljava/lang/String;Ljava/lang/String;)[B

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/internal/http/multipart/StringPart;->content:[B

    #@10
    .line 116
    :cond_10
    iget-object v0, p0, Lcom/android/internal/http/multipart/StringPart;->content:[B

    #@12
    return-object v0
.end method


# virtual methods
.method protected lengthOfData()J
    .registers 3

    #@0
    .prologue
    .line 137
    sget-object v0, Lcom/android/internal/http/multipart/StringPart;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter lengthOfData()"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 138
    invoke-direct {p0}, Lcom/android/internal/http/multipart/StringPart;->getContent()[B

    #@a
    move-result-object v0

    #@b
    array-length v0, v0

    #@c
    int-to-long v0, v0

    #@d
    return-wide v0
.end method

.method protected sendData(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 126
    sget-object v0, Lcom/android/internal/http/multipart/StringPart;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter sendData(OutputStream)"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 127
    invoke-direct {p0}, Lcom/android/internal/http/multipart/StringPart;->getContent()[B

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    #@e
    .line 128
    return-void
.end method

.method public setCharSet(Ljava/lang/String;)V
    .registers 3
    .parameter "charSet"

    #@0
    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/android/internal/http/multipart/PartBase;->setCharSet(Ljava/lang/String;)V

    #@3
    .line 147
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/internal/http/multipart/StringPart;->content:[B

    #@6
    .line 148
    return-void
.end method
