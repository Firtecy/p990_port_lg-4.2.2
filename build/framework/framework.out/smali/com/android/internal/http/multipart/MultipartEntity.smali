.class public Lcom/android/internal/http/multipart/MultipartEntity;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "MultipartEntity.java"


# static fields
.field public static final MULTIPART_BOUNDARY:Ljava/lang/String; = "http.method.multipart.boundary"

.field private static MULTIPART_CHARS:[B = null

.field private static final MULTIPART_FORM_CONTENT_TYPE:Ljava/lang/String; = "multipart/form-data"

.field private static final log:Lorg/apache/commons/logging/Log;


# instance fields
.field private contentConsumed:Z

.field private multipartBoundary:[B

.field private params:Lorg/apache/http/params/HttpParams;

.field protected parts:[Lcom/android/internal/http/multipart/Part;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 86
    const-class v0, Lcom/android/internal/http/multipart/MultipartEntity;

    #@2
    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/http/multipart/MultipartEntity;->log:Lorg/apache/commons/logging/Log;

    #@8
    .line 102
    const-string v0, "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    #@a
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/android/internal/http/multipart/MultipartEntity;->MULTIPART_CHARS:[B

    #@10
    return-void
.end method

.method public constructor <init>([Lcom/android/internal/http/multipart/Part;)V
    .registers 4
    .parameter "parts"

    #@0
    .prologue
    .line 142
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    #@3
    .line 124
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/http/multipart/MultipartEntity;->contentConsumed:Z

    #@6
    .line 143
    const-string/jumbo v0, "multipart/form-data"

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/http/multipart/MultipartEntity;->setContentType(Ljava/lang/String;)V

    #@c
    .line 144
    if-nez p1, :cond_17

    #@e
    .line 145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string/jumbo v1, "parts cannot be null"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 147
    :cond_17
    iput-object p1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->parts:[Lcom/android/internal/http/multipart/Part;

    #@19
    .line 148
    const/4 v0, 0x0

    #@1a
    iput-object v0, p0, Lcom/android/internal/http/multipart/MultipartEntity;->params:Lorg/apache/http/params/HttpParams;

    #@1c
    .line 149
    return-void
.end method

.method public constructor <init>([Lcom/android/internal/http/multipart/Part;Lorg/apache/http/params/HttpParams;)V
    .registers 5
    .parameter "parts"
    .parameter "params"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    #@3
    .line 124
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/http/multipart/MultipartEntity;->contentConsumed:Z

    #@6
    .line 132
    if-nez p1, :cond_11

    #@8
    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "parts cannot be null"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 135
    :cond_11
    if-nez p2, :cond_1c

    #@13
    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string/jumbo v1, "params cannot be null"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 138
    :cond_1c
    iput-object p1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->parts:[Lcom/android/internal/http/multipart/Part;

    #@1e
    .line 139
    iput-object p2, p0, Lcom/android/internal/http/multipart/MultipartEntity;->params:Lorg/apache/http/params/HttpParams;

    #@20
    .line 140
    return-void
.end method

.method private static generateMultipartBoundary()[B
    .registers 5

    #@0
    .prologue
    .line 109
    new-instance v2, Ljava/util/Random;

    #@2
    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    #@5
    .line 110
    .local v2, rand:Ljava/util/Random;
    const/16 v3, 0xb

    #@7
    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    #@a
    move-result v3

    #@b
    add-int/lit8 v3, v3, 0x1e

    #@d
    new-array v0, v3, [B

    #@f
    .line 111
    .local v0, bytes:[B
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    array-length v3, v0

    #@11
    if-ge v1, v3, :cond_23

    #@13
    .line 112
    sget-object v3, Lcom/android/internal/http/multipart/MultipartEntity;->MULTIPART_CHARS:[B

    #@15
    sget-object v4, Lcom/android/internal/http/multipart/MultipartEntity;->MULTIPART_CHARS:[B

    #@17
    array-length v4, v4

    #@18
    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    #@1b
    move-result v4

    #@1c
    aget-byte v3, v3, v4

    #@1e
    aput-byte v3, v0, v1

    #@20
    .line 111
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_10

    #@23
    .line 114
    :cond_23
    return-object v0
.end method


# virtual methods
.method public getContent()Ljava/io/InputStream;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/MultipartEntity;->isRepeatable()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_12

    #@6
    iget-boolean v2, p0, Lcom/android/internal/http/multipart/MultipartEntity;->contentConsumed:Z

    #@8
    if-eqz v2, :cond_12

    #@a
    .line 217
    new-instance v2, Ljava/lang/IllegalStateException;

    #@c
    const-string v3, "Content has been consumed"

    #@e
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v2

    #@12
    .line 219
    :cond_12
    const/4 v2, 0x1

    #@13
    iput-boolean v2, p0, Lcom/android/internal/http/multipart/MultipartEntity;->contentConsumed:Z

    #@15
    .line 221
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@17
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@1a
    .line 222
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    iget-object v2, p0, Lcom/android/internal/http/multipart/MultipartEntity;->parts:[Lcom/android/internal/http/multipart/Part;

    #@1c
    iget-object v3, p0, Lcom/android/internal/http/multipart/MultipartEntity;->multipartBoundary:[B

    #@1e
    invoke-static {v1, v2, v3}, Lcom/android/internal/http/multipart/Part;->sendParts(Ljava/io/OutputStream;[Lcom/android/internal/http/multipart/Part;[B)V

    #@21
    .line 223
    new-instance v0, Ljava/io/ByteArrayInputStream;

    #@23
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@26
    move-result-object v2

    #@27
    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@2a
    .line 224
    .local v0, bais:Ljava/io/ByteArrayInputStream;
    return-object v0
.end method

.method public getContentLength()J
    .registers 4

    #@0
    .prologue
    .line 208
    :try_start_0
    iget-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->parts:[Lcom/android/internal/http/multipart/Part;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/MultipartEntity;->getMultipartBoundary()[B

    #@5
    move-result-object v2

    #@6
    invoke-static {v1, v2}, Lcom/android/internal/http/multipart/Part;->getLengthOfParts([Lcom/android/internal/http/multipart/Part;[B)J
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-wide v1

    #@a
    .line 211
    :goto_a
    return-wide v1

    #@b
    .line 209
    :catch_b
    move-exception v0

    #@c
    .line 210
    .local v0, e:Ljava/lang/Exception;
    sget-object v1, Lcom/android/internal/http/multipart/MultipartEntity;->log:Lorg/apache/commons/logging/Log;

    #@e
    const-string v2, "An exception occurred while getting the length of the parts"

    #@10
    invoke-interface {v1, v2, v0}, Lorg/apache/commons/logging/Log;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@13
    .line 211
    const-wide/16 v1, 0x0

    #@15
    goto :goto_a
.end method

.method public getContentType()Lorg/apache/http/Header;
    .registers 5

    #@0
    .prologue
    .line 197
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    const-string/jumbo v1, "multipart/form-data"

    #@5
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@8
    .line 198
    .local v0, buffer:Ljava/lang/StringBuffer;
    const-string v1, "; boundary="

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d
    .line 199
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/MultipartEntity;->getMultipartBoundary()[B

    #@10
    move-result-object v1

    #@11
    invoke-static {v1}, Lorg/apache/http/util/EncodingUtils;->getAsciiString([B)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@18
    .line 200
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    #@1a
    const-string v2, "Content-Type"

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    return-object v1
.end method

.method protected getMultipartBoundary()[B
    .registers 4

    #@0
    .prologue
    .line 161
    iget-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->multipartBoundary:[B

    #@2
    if-nez v1, :cond_1b

    #@4
    .line 162
    const/4 v0, 0x0

    #@5
    .line 163
    .local v0, temp:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->params:Lorg/apache/http/params/HttpParams;

    #@7
    if-eqz v1, :cond_13

    #@9
    .line 164
    iget-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->params:Lorg/apache/http/params/HttpParams;

    #@b
    const-string v2, "http.method.multipart.boundary"

    #@d
    invoke-interface {v1, v2}, Lorg/apache/http/params/HttpParams;->getParameter(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    .end local v0           #temp:Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    #@13
    .line 166
    .restart local v0       #temp:Ljava/lang/String;
    :cond_13
    if-eqz v0, :cond_1e

    #@15
    .line 167
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@18
    move-result-object v1

    #@19
    iput-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->multipartBoundary:[B

    #@1b
    .line 172
    .end local v0           #temp:Ljava/lang/String;
    :cond_1b
    :goto_1b
    iget-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->multipartBoundary:[B

    #@1d
    return-object v1

    #@1e
    .line 169
    .restart local v0       #temp:Ljava/lang/String;
    :cond_1e
    invoke-static {}, Lcom/android/internal/http/multipart/MultipartEntity;->generateMultipartBoundary()[B

    #@21
    move-result-object v1

    #@22
    iput-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->multipartBoundary:[B

    #@24
    goto :goto_1b
.end method

.method public isRepeatable()Z
    .registers 3

    #@0
    .prologue
    .line 179
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->parts:[Lcom/android/internal/http/multipart/Part;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_15

    #@6
    .line 180
    iget-object v1, p0, Lcom/android/internal/http/multipart/MultipartEntity;->parts:[Lcom/android/internal/http/multipart/Part;

    #@8
    aget-object v1, v1, v0

    #@a
    invoke-virtual {v1}, Lcom/android/internal/http/multipart/Part;->isRepeatable()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_12

    #@10
    .line 181
    const/4 v1, 0x0

    #@11
    .line 184
    :goto_11
    return v1

    #@12
    .line 179
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_1

    #@15
    .line 184
    :cond_15
    const/4 v1, 0x1

    #@16
    goto :goto_11
.end method

.method public isStreaming()Z
    .registers 2

    #@0
    .prologue
    .line 228
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 190
    iget-object v0, p0, Lcom/android/internal/http/multipart/MultipartEntity;->parts:[Lcom/android/internal/http/multipart/Part;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/MultipartEntity;->getMultipartBoundary()[B

    #@5
    move-result-object v1

    #@6
    invoke-static {p1, v0, v1}, Lcom/android/internal/http/multipart/Part;->sendParts(Ljava/io/OutputStream;[Lcom/android/internal/http/multipart/Part;[B)V

    #@9
    .line 191
    return-void
.end method
