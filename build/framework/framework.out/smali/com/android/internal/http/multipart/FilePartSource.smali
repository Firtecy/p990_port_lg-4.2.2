.class public Lcom/android/internal/http/multipart/FilePartSource;
.super Ljava/lang/Object;
.source "FilePartSource.java"

# interfaces
.implements Lcom/android/internal/http/multipart/PartSource;


# instance fields
.field private file:Ljava/io/File;

.field private fileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .registers 4
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 52
    iput-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->file:Ljava/io/File;

    #@6
    .line 55
    iput-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->fileName:Ljava/lang/String;

    #@8
    .line 66
    iput-object p1, p0, Lcom/android/internal/http/multipart/FilePartSource;->file:Ljava/io/File;

    #@a
    .line 67
    if-eqz p1, :cond_2e

    #@c
    .line 68
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1a

    #@12
    .line 69
    new-instance v0, Ljava/io/FileNotFoundException;

    #@14
    const-string v1, "File is not a normal file."

    #@16
    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 71
    :cond_1a
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_28

    #@20
    .line 72
    new-instance v0, Ljava/io/FileNotFoundException;

    #@22
    const-string v1, "File is not readable."

    #@24
    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 74
    :cond_28
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->fileName:Ljava/lang/String;

    #@2e
    .line 76
    :cond_2e
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;)V
    .registers 3
    .parameter "fileName"
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p2}, Lcom/android/internal/http/multipart/FilePartSource;-><init>(Ljava/io/File;)V

    #@3
    .line 90
    if-eqz p1, :cond_7

    #@5
    .line 91
    iput-object p1, p0, Lcom/android/internal/http/multipart/FilePartSource;->fileName:Ljava/lang/String;

    #@7
    .line 93
    :cond_7
    return-void
.end method


# virtual methods
.method public createInputStream()Ljava/io/InputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->file:Ljava/io/File;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 125
    new-instance v0, Ljava/io/FileInputStream;

    #@6
    iget-object v1, p0, Lcom/android/internal/http/multipart/FilePartSource;->file:Ljava/io/File;

    #@8
    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@b
    .line 127
    :goto_b
    return-object v0

    #@c
    :cond_c
    new-instance v0, Ljava/io/ByteArrayInputStream;

    #@e
    const/4 v1, 0x0

    #@f
    new-array v1, v1, [B

    #@11
    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@14
    goto :goto_b
.end method

.method public getFileName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->fileName:Ljava/lang/String;

    #@2
    if-nez v0, :cond_8

    #@4
    const-string/jumbo v0, "noname"

    #@7
    :goto_7
    return-object v0

    #@8
    :cond_8
    iget-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->fileName:Ljava/lang/String;

    #@a
    goto :goto_7
.end method

.method public getLength()J
    .registers 3

    #@0
    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->file:Ljava/io/File;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 102
    iget-object v0, p0, Lcom/android/internal/http/multipart/FilePartSource;->file:Ljava/io/File;

    #@6
    invoke-virtual {v0}, Ljava/io/File;->length()J

    #@9
    move-result-wide v0

    #@a
    .line 104
    :goto_a
    return-wide v0

    #@b
    :cond_b
    const-wide/16 v0, 0x0

    #@d
    goto :goto_a
.end method
