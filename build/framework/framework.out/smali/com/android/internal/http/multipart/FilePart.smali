.class public Lcom/android/internal/http/multipart/FilePart;
.super Lcom/android/internal/http/multipart/PartBase;
.source "FilePart.java"


# static fields
.field public static final DEFAULT_CHARSET:Ljava/lang/String; = "ISO-8859-1"

.field public static final DEFAULT_CONTENT_TYPE:Ljava/lang/String; = "application/octet-stream"

.field public static final DEFAULT_TRANSFER_ENCODING:Ljava/lang/String; = "binary"

.field protected static final FILE_NAME:Ljava/lang/String; = "; filename="

.field private static final FILE_NAME_BYTES:[B

.field private static final LOG:Lorg/apache/commons/logging/Log;


# instance fields
.field private source:Lcom/android/internal/http/multipart/PartSource;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 69
    const-class v0, Lcom/android/internal/http/multipart/FilePart;

    #@2
    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/http/multipart/FilePart;->LOG:Lorg/apache/commons/logging/Log;

    #@8
    .line 75
    const-string v0, "; filename="

    #@a
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/android/internal/http/multipart/FilePart;->FILE_NAME_BYTES:[B

    #@10
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/internal/http/multipart/PartSource;)V
    .registers 4
    .parameter "name"
    .parameter "partSource"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 113
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/android/internal/http/multipart/FilePart;-><init>(Ljava/lang/String;Lcom/android/internal/http/multipart/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    #@4
    .line 114
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/internal/http/multipart/PartSource;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "name"
    .parameter "partSource"
    .parameter "contentType"
    .parameter "charset"

    #@0
    .prologue
    .line 93
    if-nez p3, :cond_4

    #@2
    const-string p3, "application/octet-stream"

    #@4
    .end local p3
    :cond_4
    if-nez p4, :cond_8

    #@6
    const-string p4, "ISO-8859-1"

    #@8
    .end local p4
    :cond_8
    const-string v0, "binary"

    #@a
    invoke-direct {p0, p1, p3, p4, v0}, Lcom/android/internal/http/multipart/PartBase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 100
    if-nez p2, :cond_17

    #@f
    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string v1, "Source may not be null"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 103
    :cond_17
    iput-object p2, p0, Lcom/android/internal/http/multipart/FilePart;->source:Lcom/android/internal/http/multipart/PartSource;

    #@19
    .line 104
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;)V
    .registers 5
    .parameter "name"
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 127
    new-instance v0, Lcom/android/internal/http/multipart/FilePartSource;

    #@3
    invoke-direct {v0, p2}, Lcom/android/internal/http/multipart/FilePartSource;-><init>(Ljava/io/File;)V

    #@6
    invoke-direct {p0, p1, v0, v1, v1}, Lcom/android/internal/http/multipart/FilePart;-><init>(Ljava/lang/String;Lcom/android/internal/http/multipart/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 128
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "name"
    .parameter "file"
    .parameter "contentType"
    .parameter "charset"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 145
    new-instance v0, Lcom/android/internal/http/multipart/FilePartSource;

    #@2
    invoke-direct {v0, p2}, Lcom/android/internal/http/multipart/FilePartSource;-><init>(Ljava/io/File;)V

    #@5
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/android/internal/http/multipart/FilePart;-><init>(Ljava/lang/String;Lcom/android/internal/http/multipart/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 146
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .registers 6
    .parameter "name"
    .parameter "fileName"
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 160
    new-instance v0, Lcom/android/internal/http/multipart/FilePartSource;

    #@3
    invoke-direct {v0, p2, p3}, Lcom/android/internal/http/multipart/FilePartSource;-><init>(Ljava/lang/String;Ljava/io/File;)V

    #@6
    invoke-direct {p0, p1, v0, v1, v1}, Lcom/android/internal/http/multipart/FilePart;-><init>(Ljava/lang/String;Lcom/android/internal/http/multipart/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 161
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "name"
    .parameter "fileName"
    .parameter "file"
    .parameter "contentType"
    .parameter "charset"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 179
    new-instance v0, Lcom/android/internal/http/multipart/FilePartSource;

    #@2
    invoke-direct {v0, p2, p3}, Lcom/android/internal/http/multipart/FilePartSource;-><init>(Ljava/lang/String;Ljava/io/File;)V

    #@5
    invoke-direct {p0, p1, v0, p4, p5}, Lcom/android/internal/http/multipart/FilePart;-><init>(Ljava/lang/String;Lcom/android/internal/http/multipart/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 180
    return-void
.end method


# virtual methods
.method protected getSource()Lcom/android/internal/http/multipart/PartSource;
    .registers 3

    #@0
    .prologue
    .line 239
    sget-object v0, Lcom/android/internal/http/multipart/FilePart;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter getSource()"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 240
    iget-object v0, p0, Lcom/android/internal/http/multipart/FilePart;->source:Lcom/android/internal/http/multipart/PartSource;

    #@9
    return-object v0
.end method

.method protected lengthOfData()J
    .registers 3

    #@0
    .prologue
    .line 250
    sget-object v0, Lcom/android/internal/http/multipart/FilePart;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v1, "enter lengthOfData()"

    #@4
    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 251
    iget-object v0, p0, Lcom/android/internal/http/multipart/FilePart;->source:Lcom/android/internal/http/multipart/PartSource;

    #@9
    invoke-interface {v0}, Lcom/android/internal/http/multipart/PartSource;->getLength()J

    #@c
    move-result-wide v0

    #@d
    return-wide v0
.end method

.method protected sendData(Ljava/io/OutputStream;)V
    .registers 9
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 210
    sget-object v3, Lcom/android/internal/http/multipart/FilePart;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v4, "enter sendData(OutputStream out)"

    #@4
    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 211
    invoke-virtual {p0}, Lcom/android/internal/http/multipart/FilePart;->lengthOfData()J

    #@a
    move-result-wide v3

    #@b
    const-wide/16 v5, 0x0

    #@d
    cmp-long v3, v3, v5

    #@f
    if-nez v3, :cond_19

    #@11
    .line 216
    sget-object v3, Lcom/android/internal/http/multipart/FilePart;->LOG:Lorg/apache/commons/logging/Log;

    #@13
    const-string v4, "No data to send."

    #@15
    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    #@18
    .line 231
    :goto_18
    return-void

    #@19
    .line 220
    :cond_19
    const/16 v3, 0x1000

    #@1b
    new-array v2, v3, [B

    #@1d
    .line 221
    .local v2, tmp:[B
    iget-object v3, p0, Lcom/android/internal/http/multipart/FilePart;->source:Lcom/android/internal/http/multipart/PartSource;

    #@1f
    invoke-interface {v3}, Lcom/android/internal/http/multipart/PartSource;->createInputStream()Ljava/io/InputStream;

    #@22
    move-result-object v0

    #@23
    .line 224
    .local v0, instream:Ljava/io/InputStream;
    :goto_23
    :try_start_23
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    #@26
    move-result v1

    #@27
    .local v1, len:I
    if-ltz v1, :cond_33

    #@29
    .line 225
    const/4 v3, 0x0

    #@2a
    invoke-virtual {p1, v2, v3, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2d
    .catchall {:try_start_23 .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_23

    #@2e
    .line 229
    .end local v1           #len:I
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    #@32
    throw v3

    #@33
    .restart local v1       #len:I
    :cond_33
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    #@36
    goto :goto_18
.end method

.method protected sendDispositionHeader(Ljava/io/OutputStream;)V
    .registers 5
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 191
    sget-object v1, Lcom/android/internal/http/multipart/FilePart;->LOG:Lorg/apache/commons/logging/Log;

    #@2
    const-string v2, "enter sendDispositionHeader(OutputStream out)"

    #@4
    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->trace(Ljava/lang/Object;)V

    #@7
    .line 192
    invoke-super {p0, p1}, Lcom/android/internal/http/multipart/PartBase;->sendDispositionHeader(Ljava/io/OutputStream;)V

    #@a
    .line 193
    iget-object v1, p0, Lcom/android/internal/http/multipart/FilePart;->source:Lcom/android/internal/http/multipart/PartSource;

    #@c
    invoke-interface {v1}, Lcom/android/internal/http/multipart/PartSource;->getFileName()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 194
    .local v0, filename:Ljava/lang/String;
    if-eqz v0, :cond_28

    #@12
    .line 195
    sget-object v1, Lcom/android/internal/http/multipart/FilePart;->FILE_NAME_BYTES:[B

    #@14
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@17
    .line 196
    sget-object v1, Lcom/android/internal/http/multipart/FilePart;->QUOTE_BYTES:[B

    #@19
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@1c
    .line 197
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@23
    .line 198
    sget-object v1, Lcom/android/internal/http/multipart/FilePart;->QUOTE_BYTES:[B

    #@25
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@28
    .line 200
    :cond_28
    return-void
.end method
