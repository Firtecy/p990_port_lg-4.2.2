.class public final Lcom/android/internal/http/HttpDateTime;
.super Ljava/lang/Object;
.source "HttpDateTime.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/http/HttpDateTime$TimeOfDay;
    }
.end annotation


# static fields
.field private static final HTTP_DATE_ANSIC_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final HTTP_DATE_ANSIC_REGEXP:Ljava/lang/String; = "[ ]([A-Za-z]{3,9})[ ]+([0-9]{1,2})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])[ ]([0-9]{2,4})"

.field private static final HTTP_DATE_RFC_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final HTTP_DATE_RFC_REGEXP:Ljava/lang/String; = "([0-9]{1,2})[- ]([A-Za-z]{3,9})[- ]([0-9]{2,4})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 68
    const-string v0, "([0-9]{1,2})[- ]([A-Za-z]{3,9})[- ]([0-9]{2,4})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])"

    #@2
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/http/HttpDateTime;->HTTP_DATE_RFC_PATTERN:Ljava/util/regex/Pattern;

    #@8
    .line 70
    const-string v0, "[ ]([A-Za-z]{3,9})[ ]+([0-9]{1,2})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])[ ]([0-9]{2,4})"

    #@a
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/android/internal/http/HttpDateTime;->HTTP_DATE_ANSIC_PATTERN:Ljava/util/regex/Pattern;

    #@10
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    return-void
.end method

.method private static getDate(Ljava/lang/String;)I
    .registers 4
    .parameter "dateString"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 125
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v0

    #@5
    const/4 v1, 0x2

    #@6
    if-ne v0, v1, :cond_19

    #@8
    .line 126
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@b
    move-result v0

    #@c
    add-int/lit8 v0, v0, -0x30

    #@e
    mul-int/lit8 v0, v0, 0xa

    #@10
    const/4 v1, 0x1

    #@11
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v1

    #@15
    add-int/lit8 v1, v1, -0x30

    #@17
    add-int/2addr v0, v1

    #@18
    .line 129
    :goto_18
    return v0

    #@19
    :cond_19
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@1c
    move-result v0

    #@1d
    add-int/lit8 v0, v0, -0x30

    #@1f
    goto :goto_18
.end method

.method private static getMonth(Ljava/lang/String;)I
    .registers 7
    .parameter "monthString"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 148
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@6
    move-result v4

    #@7
    invoke-static {v4}, Ljava/lang/Character;->toLowerCase(C)C

    #@a
    move-result v4

    #@b
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@e
    move-result v5

    #@f
    invoke-static {v5}, Ljava/lang/Character;->toLowerCase(C)C

    #@12
    move-result v5

    #@13
    add-int/2addr v4, v5

    #@14
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v5

    #@18
    invoke-static {v5}, Ljava/lang/Character;->toLowerCase(C)C

    #@1b
    move-result v5

    #@1c
    add-int/2addr v4, v5

    #@1d
    add-int/lit16 v0, v4, -0x123

    #@1f
    .line 151
    .local v0, hash:I
    sparse-switch v0, :sswitch_data_42

    #@22
    .line 177
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@24
    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@27
    throw v1

    #@28
    :sswitch_28
    move v1, v2

    #@29
    .line 175
    :goto_29
    :sswitch_29
    return v1

    #@2a
    :sswitch_2a
    move v1, v3

    #@2b
    .line 157
    goto :goto_29

    #@2c
    .line 159
    :sswitch_2c
    const/4 v1, 0x3

    #@2d
    goto :goto_29

    #@2e
    .line 161
    :sswitch_2e
    const/4 v1, 0x4

    #@2f
    goto :goto_29

    #@30
    .line 163
    :sswitch_30
    const/4 v1, 0x5

    #@31
    goto :goto_29

    #@32
    .line 165
    :sswitch_32
    const/4 v1, 0x6

    #@33
    goto :goto_29

    #@34
    .line 167
    :sswitch_34
    const/4 v1, 0x7

    #@35
    goto :goto_29

    #@36
    .line 169
    :sswitch_36
    const/16 v1, 0x8

    #@38
    goto :goto_29

    #@39
    .line 171
    :sswitch_39
    const/16 v1, 0x9

    #@3b
    goto :goto_29

    #@3c
    .line 173
    :sswitch_3c
    const/16 v1, 0xa

    #@3e
    goto :goto_29

    #@3f
    .line 175
    :sswitch_3f
    const/16 v1, 0xb

    #@41
    goto :goto_29

    #@42
    .line 151
    :sswitch_data_42
    .sparse-switch
        0x9 -> :sswitch_3f
        0xa -> :sswitch_28
        0x16 -> :sswitch_29
        0x1a -> :sswitch_34
        0x1d -> :sswitch_2a
        0x20 -> :sswitch_2c
        0x23 -> :sswitch_39
        0x24 -> :sswitch_2e
        0x25 -> :sswitch_36
        0x28 -> :sswitch_32
        0x2a -> :sswitch_30
        0x30 -> :sswitch_3c
    .end sparse-switch
.end method

.method private static getTime(Ljava/lang/String;)Lcom/android/internal/http/HttpDateTime$TimeOfDay;
    .registers 8
    .parameter "timeString"

    #@0
    .prologue
    .line 208
    const/4 v1, 0x0

    #@1
    .line 209
    .local v1, i:I
    add-int/lit8 v2, v1, 0x1

    #@3
    .end local v1           #i:I
    .local v2, i:I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@6
    move-result v5

    #@7
    add-int/lit8 v0, v5, -0x30

    #@9
    .line 210
    .local v0, hour:I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v5

    #@d
    const/16 v6, 0x3a

    #@f
    if-eq v5, v6, :cond_4f

    #@11
    .line 211
    mul-int/lit8 v5, v0, 0xa

    #@13
    add-int/lit8 v1, v2, 0x1

    #@15
    .end local v2           #i:I
    .restart local v1       #i:I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@18
    move-result v6

    #@19
    add-int/lit8 v6, v6, -0x30

    #@1b
    add-int v0, v5, v6

    #@1d
    .line 213
    :goto_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    .line 215
    add-int/lit8 v2, v1, 0x1

    #@21
    .end local v1           #i:I
    .restart local v2       #i:I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@24
    move-result v5

    #@25
    add-int/lit8 v5, v5, -0x30

    #@27
    mul-int/lit8 v5, v5, 0xa

    #@29
    add-int/lit8 v1, v2, 0x1

    #@2b
    .end local v2           #i:I
    .restart local v1       #i:I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@2e
    move-result v6

    #@2f
    add-int/lit8 v6, v6, -0x30

    #@31
    add-int v3, v5, v6

    #@33
    .line 218
    .local v3, minute:I
    add-int/lit8 v1, v1, 0x1

    #@35
    .line 220
    add-int/lit8 v2, v1, 0x1

    #@37
    .end local v1           #i:I
    .restart local v2       #i:I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@3a
    move-result v5

    #@3b
    add-int/lit8 v5, v5, -0x30

    #@3d
    mul-int/lit8 v5, v5, 0xa

    #@3f
    add-int/lit8 v1, v2, 0x1

    #@41
    .end local v2           #i:I
    .restart local v1       #i:I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@44
    move-result v6

    #@45
    add-int/lit8 v6, v6, -0x30

    #@47
    add-int v4, v5, v6

    #@49
    .line 223
    .local v4, second:I
    new-instance v5, Lcom/android/internal/http/HttpDateTime$TimeOfDay;

    #@4b
    invoke-direct {v5, v0, v3, v4}, Lcom/android/internal/http/HttpDateTime$TimeOfDay;-><init>(III)V

    #@4e
    return-object v5

    #@4f
    .end local v1           #i:I
    .end local v3           #minute:I
    .end local v4           #second:I
    .restart local v2       #i:I
    :cond_4f
    move v1, v2

    #@50
    .end local v2           #i:I
    .restart local v1       #i:I
    goto :goto_1d
.end method

.method private static getYear(Ljava/lang/String;)I
    .registers 8
    .parameter "yearString"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 182
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    if-ne v1, v5, :cond_24

    #@a
    .line 183
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v1

    #@e
    add-int/lit8 v1, v1, -0x30

    #@10
    mul-int/lit8 v1, v1, 0xa

    #@12
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v2

    #@16
    add-int/lit8 v2, v2, -0x30

    #@18
    add-int v0, v1, v2

    #@1a
    .line 185
    .local v0, year:I
    const/16 v1, 0x46

    #@1c
    if-lt v0, v1, :cond_21

    #@1e
    .line 186
    add-int/lit16 v1, v0, 0x76c

    #@20
    .line 202
    .end local v0           #year:I
    :goto_20
    return v1

    #@21
    .line 188
    .restart local v0       #year:I
    :cond_21
    add-int/lit16 v1, v0, 0x7d0

    #@23
    goto :goto_20

    #@24
    .line 190
    .end local v0           #year:I
    :cond_24
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@27
    move-result v1

    #@28
    if-ne v1, v6, :cond_46

    #@2a
    .line 192
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@2d
    move-result v1

    #@2e
    add-int/lit8 v1, v1, -0x30

    #@30
    mul-int/lit8 v1, v1, 0x64

    #@32
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@35
    move-result v2

    #@36
    add-int/lit8 v2, v2, -0x30

    #@38
    mul-int/lit8 v2, v2, 0xa

    #@3a
    add-int/2addr v1, v2

    #@3b
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@3e
    move-result v2

    #@3f
    add-int/lit8 v2, v2, -0x30

    #@41
    add-int v0, v1, v2

    #@43
    .line 195
    .restart local v0       #year:I
    add-int/lit16 v1, v0, 0x76c

    #@45
    goto :goto_20

    #@46
    .line 196
    .end local v0           #year:I
    :cond_46
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@49
    move-result v1

    #@4a
    const/4 v2, 0x4

    #@4b
    if-ne v1, v2, :cond_6f

    #@4d
    .line 197
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@50
    move-result v1

    #@51
    add-int/lit8 v1, v1, -0x30

    #@53
    mul-int/lit16 v1, v1, 0x3e8

    #@55
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@58
    move-result v2

    #@59
    add-int/lit8 v2, v2, -0x30

    #@5b
    mul-int/lit8 v2, v2, 0x64

    #@5d
    add-int/2addr v1, v2

    #@5e
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@61
    move-result v2

    #@62
    add-int/lit8 v2, v2, -0x30

    #@64
    mul-int/lit8 v2, v2, 0xa

    #@66
    add-int/2addr v1, v2

    #@67
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    #@6a
    move-result v2

    #@6b
    add-int/lit8 v2, v2, -0x30

    #@6d
    add-int/2addr v1, v2

    #@6e
    goto :goto_20

    #@6f
    .line 202
    :cond_6f
    const/16 v1, 0x7b2

    #@71
    goto :goto_20
.end method

.method public static parse(Ljava/lang/String;)J
    .registers 13
    .parameter "timeString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v11, 0x4

    #@1
    const/4 v10, 0x3

    #@2
    const/4 v3, 0x2

    #@3
    const/4 v2, 0x1

    #@4
    .line 88
    const/4 v4, 0x1

    #@5
    .line 89
    .local v4, date:I
    const/4 v5, 0x0

    #@6
    .line 90
    .local v5, month:I
    const/16 v6, 0x7b2

    #@8
    .line 93
    .local v6, year:I
    sget-object v1, Lcom/android/internal/http/HttpDateTime;->HTTP_DATE_RFC_PATTERN:Ljava/util/regex/Pattern;

    #@a
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@d
    move-result-object v8

    #@e
    .line 94
    .local v8, rfcMatcher:Ljava/util/regex/Matcher;
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->find()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_52

    #@14
    .line 95
    invoke-virtual {v8, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getDate(Ljava/lang/String;)I

    #@1b
    move-result v4

    #@1c
    .line 96
    invoke-virtual {v8, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getMonth(Ljava/lang/String;)I

    #@23
    move-result v5

    #@24
    .line 97
    invoke-virtual {v8, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getYear(Ljava/lang/String;)I

    #@2b
    move-result v6

    #@2c
    .line 98
    invoke-virtual {v8, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getTime(Ljava/lang/String;)Lcom/android/internal/http/HttpDateTime$TimeOfDay;

    #@33
    move-result-object v9

    #@34
    .line 112
    .local v9, timeOfDay:Lcom/android/internal/http/HttpDateTime$TimeOfDay;
    :goto_34
    const/16 v1, 0x7f6

    #@36
    if-lt v6, v1, :cond_3c

    #@38
    .line 113
    const/16 v6, 0x7f6

    #@3a
    .line 114
    const/4 v5, 0x0

    #@3b
    .line 115
    const/4 v4, 0x1

    #@3c
    .line 118
    :cond_3c
    new-instance v0, Landroid/text/format/Time;

    #@3e
    const-string v1, "UTC"

    #@40
    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@43
    .line 119
    .local v0, time:Landroid/text/format/Time;
    iget v1, v9, Lcom/android/internal/http/HttpDateTime$TimeOfDay;->second:I

    #@45
    iget v2, v9, Lcom/android/internal/http/HttpDateTime$TimeOfDay;->minute:I

    #@47
    iget v3, v9, Lcom/android/internal/http/HttpDateTime$TimeOfDay;->hour:I

    #@49
    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    #@4c
    .line 121
    const/4 v1, 0x0

    #@4d
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    #@50
    move-result-wide v1

    #@51
    return-wide v1

    #@52
    .line 100
    .end local v0           #time:Landroid/text/format/Time;
    .end local v9           #timeOfDay:Lcom/android/internal/http/HttpDateTime$TimeOfDay;
    :cond_52
    sget-object v1, Lcom/android/internal/http/HttpDateTime;->HTTP_DATE_ANSIC_PATTERN:Ljava/util/regex/Pattern;

    #@54
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@57
    move-result-object v7

    #@58
    .line 101
    .local v7, ansicMatcher:Ljava/util/regex/Matcher;
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    #@5b
    move-result v1

    #@5c
    if-eqz v1, :cond_7f

    #@5e
    .line 102
    invoke-virtual {v7, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getMonth(Ljava/lang/String;)I

    #@65
    move-result v5

    #@66
    .line 103
    invoke-virtual {v7, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getDate(Ljava/lang/String;)I

    #@6d
    move-result v4

    #@6e
    .line 104
    invoke-virtual {v7, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getTime(Ljava/lang/String;)Lcom/android/internal/http/HttpDateTime$TimeOfDay;

    #@75
    move-result-object v9

    #@76
    .line 105
    .restart local v9       #timeOfDay:Lcom/android/internal/http/HttpDateTime$TimeOfDay;
    invoke-virtual {v7, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    invoke-static {v1}, Lcom/android/internal/http/HttpDateTime;->getYear(Ljava/lang/String;)I

    #@7d
    move-result v6

    #@7e
    goto :goto_34

    #@7f
    .line 107
    .end local v9           #timeOfDay:Lcom/android/internal/http/HttpDateTime$TimeOfDay;
    :cond_7f
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@81
    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@84
    throw v1
.end method
