.class public final enum Lcom/android/internal/telephony/IccCardConstants$State;
.super Ljava/lang/Enum;
.source "IccCardConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccCardConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/IccCardConstants$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum CARD_IO_ERROR:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum HPLMN_SIMTYPE:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum OTP_FAILED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum READY:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum RUIM_CORPORATE_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum RUIM_HRPD_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum RUIM_NETWORK1_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum RUIM_NETWORK2_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum RUIM_RUIM_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum RUIM_SERVICE_PROVIDER_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum SIM_CORPORATE_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum SIM_NETWORK_SUBSET_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum SIM_REMOVED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum SIM_SERVICE_PROVIDER_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum SIM_SIM_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

.field public static final enum UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 84
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@7
    const-string v1, "UNKNOWN"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@e
    .line 85
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@10
    const-string v1, "ABSENT"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@17
    .line 86
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@19
    const-string v1, "PIN_REQUIRED"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@20
    .line 87
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@22
    const-string v1, "PUK_REQUIRED"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@29
    .line 88
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@2b
    const-string v1, "PERSO_LOCKED"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@32
    .line 89
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@34
    const-string v1, "READY"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3c
    .line 90
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@3e
    const-string v1, "NOT_READY"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@46
    .line 91
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@48
    const-string v1, "PERM_DISABLED"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@50
    .line 93
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@52
    const-string v1, "CARD_IO_ERROR"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->CARD_IO_ERROR:Lcom/android/internal/telephony/IccCardConstants$State;

    #@5b
    .line 94
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@5d
    const-string v1, "HPLMN_SIMTYPE"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->HPLMN_SIMTYPE:Lcom/android/internal/telephony/IccCardConstants$State;

    #@66
    .line 95
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@68
    const-string v1, "SIM_NETWORK_SUBSET_LOCKED"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_NETWORK_SUBSET_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@71
    .line 96
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@73
    const-string v1, "SIM_CORPORATE_LOCKED"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_CORPORATE_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7c
    .line 97
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@7e
    const-string v1, "SIM_SERVICE_PROVIDER_LOCKED"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_SERVICE_PROVIDER_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@87
    .line 98
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@89
    const-string v1, "SIM_SIM_LOCKED"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_SIM_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@92
    .line 99
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@94
    const-string v1, "OTP_FAILED"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->OTP_FAILED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@9d
    .line 100
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@9f
    const-string v1, "RUIM_NETWORK1_LOCKED"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_NETWORK1_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@a8
    .line 101
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@aa
    const-string v1, "RUIM_NETWORK2_LOCKED"

    #@ac
    const/16 v2, 0x10

    #@ae
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@b1
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_NETWORK2_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@b3
    .line 102
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@b5
    const-string v1, "RUIM_HRPD_LOCKED"

    #@b7
    const/16 v2, 0x11

    #@b9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@bc
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_HRPD_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@be
    .line 103
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@c0
    const-string v1, "RUIM_CORPORATE_LOCKED"

    #@c2
    const/16 v2, 0x12

    #@c4
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@c7
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_CORPORATE_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@c9
    .line 104
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@cb
    const-string v1, "RUIM_SERVICE_PROVIDER_LOCKED"

    #@cd
    const/16 v2, 0x13

    #@cf
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@d2
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_SERVICE_PROVIDER_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@d4
    .line 105
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@d6
    const-string v1, "RUIM_RUIM_LOCKED"

    #@d8
    const/16 v2, 0x14

    #@da
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@dd
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_RUIM_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@df
    .line 106
    new-instance v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@e1
    const-string v1, "SIM_REMOVED"

    #@e3
    const/16 v2, 0x15

    #@e5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/IccCardConstants$State;-><init>(Ljava/lang/String;I)V

    #@e8
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_REMOVED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@ea
    .line 83
    const/16 v0, 0x16

    #@ec
    new-array v0, v0, [Lcom/android/internal/telephony/IccCardConstants$State;

    #@ee
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@f0
    aput-object v1, v0, v3

    #@f2
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@f4
    aput-object v1, v0, v4

    #@f6
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@f8
    aput-object v1, v0, v5

    #@fa
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@fc
    aput-object v1, v0, v6

    #@fe
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@100
    aput-object v1, v0, v7

    #@102
    const/4 v1, 0x5

    #@103
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@105
    aput-object v2, v0, v1

    #@107
    const/4 v1, 0x6

    #@108
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@10a
    aput-object v2, v0, v1

    #@10c
    const/4 v1, 0x7

    #@10d
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@10f
    aput-object v2, v0, v1

    #@111
    const/16 v1, 0x8

    #@113
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->CARD_IO_ERROR:Lcom/android/internal/telephony/IccCardConstants$State;

    #@115
    aput-object v2, v0, v1

    #@117
    const/16 v1, 0x9

    #@119
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->HPLMN_SIMTYPE:Lcom/android/internal/telephony/IccCardConstants$State;

    #@11b
    aput-object v2, v0, v1

    #@11d
    const/16 v1, 0xa

    #@11f
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_NETWORK_SUBSET_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@121
    aput-object v2, v0, v1

    #@123
    const/16 v1, 0xb

    #@125
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_CORPORATE_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@127
    aput-object v2, v0, v1

    #@129
    const/16 v1, 0xc

    #@12b
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_SERVICE_PROVIDER_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@12d
    aput-object v2, v0, v1

    #@12f
    const/16 v1, 0xd

    #@131
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_SIM_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@133
    aput-object v2, v0, v1

    #@135
    const/16 v1, 0xe

    #@137
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->OTP_FAILED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@139
    aput-object v2, v0, v1

    #@13b
    const/16 v1, 0xf

    #@13d
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_NETWORK1_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@13f
    aput-object v2, v0, v1

    #@141
    const/16 v1, 0x10

    #@143
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_NETWORK2_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@145
    aput-object v2, v0, v1

    #@147
    const/16 v1, 0x11

    #@149
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_HRPD_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@14b
    aput-object v2, v0, v1

    #@14d
    const/16 v1, 0x12

    #@14f
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_CORPORATE_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@151
    aput-object v2, v0, v1

    #@153
    const/16 v1, 0x13

    #@155
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_SERVICE_PROVIDER_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@157
    aput-object v2, v0, v1

    #@159
    const/16 v1, 0x14

    #@15b
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->RUIM_RUIM_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@15d
    aput-object v2, v0, v1

    #@15f
    const/16 v1, 0x15

    #@161
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_REMOVED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@163
    aput-object v2, v0, v1

    #@165
    sput-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->$VALUES:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@167
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 83
    const-class v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/IccCardConstants$State;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 1

    #@0
    .prologue
    .line 83
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->$VALUES:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/IccCardConstants$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/IccCardConstants$State;

    #@8
    return-object v0
.end method


# virtual methods
.method public iccCardExist()Z
    .registers 2

    #@0
    .prologue
    .line 115
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    if-eq p0, v0, :cond_18

    #@4
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@6
    if-eq p0, v0, :cond_18

    #@8
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@a
    if-eq p0, v0, :cond_18

    #@c
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@e
    if-eq p0, v0, :cond_18

    #@10
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@12
    if-eq p0, v0, :cond_18

    #@14
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->CARD_IO_ERROR:Lcom/android/internal/telephony/IccCardConstants$State;

    #@16
    if-ne p0, v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public isPinLocked()Z
    .registers 2

    #@0
    .prologue
    .line 111
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    if-eq p0, v0, :cond_8

    #@4
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@6
    if-ne p0, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method
