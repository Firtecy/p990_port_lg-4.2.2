.class public Lcom/android/internal/telephony/CallerInfo;
.super Ljava/lang/Object;
.source "CallerInfo.java"


# static fields
.field static final CUSTOM_LED_TYPE:Ljava/lang/String; = "custom_led_type"

.field public static final EMPTY_NUMBER:Ljava/lang/String; = "-4"

.field public static final PAYPHONE_NUMBER:Ljava/lang/String; = "-3"

.field public static final PRIVATE_NUMBER:Ljava/lang/String; = "-2"

.field private static final TAG:Ljava/lang/String; = "CallerInfo"

.field public static final UNKNOWN_NUMBER:Ljava/lang/String; = "-1"

.field private static final VDBG:Z


# instance fields
.field public cachedPhoto:Landroid/graphics/drawable/Drawable;

.field public cachedPhotoIcon:Landroid/graphics/Bitmap;

.field public cdnipNumber:Ljava/lang/String;

.field public cnapName:Ljava/lang/String;

.field public contactExists:Z

.field private contactExistsCount:I

.field public contactNumber:Ljava/lang/String;

.field public contactRefUri:Landroid/net/Uri;

.field public contactRingtoneUri:Landroid/net/Uri;

.field public custom_led_type:I

.field public displayNumber:Ljava/lang/String;

.field public distinctiveVib:I

.field public eventDate:Ljava/lang/String;

.field public geoDescription:Ljava/lang/String;

.field public isCachedPhotoCurrent:Z

.field public lookupKey:Ljava/lang/String;

.field private mIsEmergency:Z

.field private mIsN11OrSpecial:Z

.field private mIsVoiceMail:Z

.field public mMapUserData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public namePresentation:I

.field public needUpdate:Z

.field public normalizedNumber:Ljava/lang/String;

.field public numberLabel:Ljava/lang/String;

.field public numberPresentation:I

.field public numberType:I

.field public originalNumber:Ljava/lang/String;

.field public person_id:J

.field public phoneLabel:Ljava/lang/String;

.field public phoneNumber:Ljava/lang/String;

.field public photoResource:I

.field public shouldSendToVoicemail:Z

.field public socialStatus:Ljava/lang/String;

.field public socialStatusIconRes:Landroid/graphics/drawable/Drawable;

.field public starred:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 66
    const-string v0, "CallerInfo"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 202
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 126
    iput-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->contactNumber:Ljava/lang/String;

    #@7
    .line 127
    iput-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->displayNumber:Ljava/lang/String;

    #@9
    .line 195
    iput-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@b
    .line 204
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsEmergency:Z

    #@d
    .line 205
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsVoiceMail:Z

    #@f
    .line 207
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsN11OrSpecial:Z

    #@11
    .line 209
    return-void
.end method

.method static doSecondaryLookupIfNecessary(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/CallerInfo;)Lcom/android/internal/telephony/CallerInfo;
    .registers 6
    .parameter "context"
    .parameter "number"
    .parameter "previousResult"

    #@0
    .prologue
    .line 757
    iget-boolean v1, p2, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@2
    if-nez v1, :cond_22

    #@4
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isUriNumber(Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_22

    #@a
    .line 759
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->getUsernameFromUriNumber(Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 760
    .local v0, username:Ljava/lang/String;
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_22

    #@14
    .line 761
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@16
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {p0, v1}, Lcom/android/internal/telephony/CallerInfo;->getCallerInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/internal/telephony/CallerInfo;

    #@21
    move-result-object p2

    #@22
    .line 766
    .end local v0           #username:Ljava/lang/String;
    :cond_22
    return-object p2
.end method

.method public static getCallerId(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    .line 786
    invoke-static {p0, p1}, Lcom/android/internal/telephony/CallerInfo;->getCallerInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/CallerInfo;

    #@3
    move-result-object v1

    #@4
    .line 787
    .local v1, info:Lcom/android/internal/telephony/CallerInfo;
    const/4 v0, 0x0

    #@5
    .line 789
    .local v0, callerID:Ljava/lang/String;
    if-eqz v1, :cond_10

    #@7
    .line 790
    iget-object v2, v1, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@9
    .line 792
    .local v2, name:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v3

    #@d
    if-nez v3, :cond_11

    #@f
    .line 793
    move-object v0, v2

    #@10
    .line 799
    .end local v2           #name:Ljava/lang/String;
    :cond_10
    :goto_10
    return-object v0

    #@11
    .line 795
    .restart local v2       #name:Ljava/lang/String;
    :cond_11
    move-object v0, p1

    #@12
    goto :goto_10
.end method

.method public static getCallerInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/internal/telephony/CallerInfo;
    .registers 8
    .parameter "context"
    .parameter "contactRef"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 675
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    move-object v1, p1

    #@6
    move-object v3, v2

    #@7
    move-object v4, v2

    #@8
    move-object v5, v2

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c
    move-result-object v0

    #@d
    invoke-static {p0, p1, v0}, Lcom/android/internal/telephony/CallerInfo;->getCallerInfo(Landroid/content/Context;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/android/internal/telephony/CallerInfo;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public static getCallerInfo(Landroid/content/Context;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/android/internal/telephony/CallerInfo;
    .registers 15
    .parameter "context"
    .parameter "contactRef"
    .parameter "cursor"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v10, -0x1

    #@4
    .line 220
    new-instance v1, Lcom/android/internal/telephony/CallerInfo;

    #@6
    invoke-direct {v1}, Lcom/android/internal/telephony/CallerInfo;-><init>()V

    #@9
    .line 221
    .local v1, info:Lcom/android/internal/telephony/CallerInfo;
    iput v6, v1, Lcom/android/internal/telephony/CallerInfo;->photoResource:I

    #@b
    .line 222
    iput-object v11, v1, Lcom/android/internal/telephony/CallerInfo;->phoneLabel:Ljava/lang/String;

    #@d
    .line 223
    iput v6, v1, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@f
    .line 224
    iput-object v11, v1, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@11
    .line 225
    iput-object v11, v1, Lcom/android/internal/telephony/CallerInfo;->cachedPhoto:Landroid/graphics/drawable/Drawable;

    #@13
    .line 226
    iput-boolean v6, v1, Lcom/android/internal/telephony/CallerInfo;->isCachedPhotoCurrent:Z

    #@15
    .line 227
    iput-boolean v6, v1, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@17
    .line 229
    const-string v4, ""

    #@19
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->socialStatus:Ljava/lang/String;

    #@1b
    .line 230
    const-string v4, ""

    #@1d
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->eventDate:Ljava/lang/String;

    #@1f
    .line 231
    iput-object v11, v1, Lcom/android/internal/telephony/CallerInfo;->socialStatusIconRes:Landroid/graphics/drawable/Drawable;

    #@21
    .line 234
    invoke-direct {v1, v6}, Lcom/android/internal/telephony/CallerInfo;->setContactExistsCount(I)V

    #@24
    .line 238
    iput v6, v1, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@26
    .line 241
    sget-boolean v4, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@28
    if-eqz v4, :cond_31

    #@2a
    const-string v4, "CallerInfo"

    #@2c
    const-string v7, "getCallerInfo() based on cursor..."

    #@2e
    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 243
    :cond_31
    if-eqz p2, :cond_216

    #@33
    .line 265
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_20c

    #@39
    .line 273
    const-string v4, "display_name"

    #@3b
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3e
    move-result v0

    #@3f
    .line 274
    .local v0, columnIndex:I
    if-eq v0, v10, :cond_47

    #@41
    .line 275
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@47
    .line 279
    :cond_47
    const-string/jumbo v4, "number"

    #@4a
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4d
    move-result v0

    #@4e
    .line 280
    if-eq v0, v10, :cond_56

    #@50
    .line 281
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@56
    .line 285
    :cond_56
    const-string/jumbo v4, "normalized_number"

    #@59
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@5c
    move-result v0

    #@5d
    .line 286
    if-eq v0, v10, :cond_65

    #@5f
    .line 287
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@65
    .line 291
    :cond_65
    const-string/jumbo v4, "label"

    #@68
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@6b
    move-result v0

    #@6c
    .line 292
    if-eq v0, v10, :cond_91

    #@6e
    .line 293
    const-string/jumbo v4, "type"

    #@71
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@74
    move-result v3

    #@75
    .line 294
    .local v3, typeColumnIndex:I
    if-eq v3, v10, :cond_91

    #@77
    .line 295
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    #@7a
    move-result v4

    #@7b
    iput v4, v1, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@7d
    .line 296
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@83
    .line 297
    iget v4, v1, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@85
    iget-object v7, v1, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@87
    invoke-static {p0, v4, v7}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getDisplayLabel(Landroid/content/Context;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->phoneLabel:Ljava/lang/String;

    #@91
    .line 304
    .end local v3           #typeColumnIndex:I
    :cond_91
    invoke-static {p1, p2}, Lcom/android/internal/telephony/CallerInfo;->getColumnIndexForPersonId(Landroid/net/Uri;Landroid/database/Cursor;)I

    #@94
    move-result v0

    #@95
    .line 305
    if-eq v0, v10, :cond_223

    #@97
    .line 306
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    #@9a
    move-result-wide v7

    #@9b
    iput-wide v7, v1, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@9d
    .line 307
    sget-boolean v4, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@9f
    if-eqz v4, :cond_bb

    #@a1
    const-string v4, "CallerInfo"

    #@a3
    new-instance v7, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v8, "==> got info.person_id: "

    #@aa
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v7

    #@ae
    iget-wide v8, v1, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@b0
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v7

    #@b4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v7

    #@b8
    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 318
    :cond_bb
    :goto_bb
    const-string v4, "custom_ringtone"

    #@bd
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@c0
    move-result v0

    #@c1
    .line 319
    if-eq v0, v10, :cond_d3

    #@c3
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c6
    move-result-object v4

    #@c7
    if-eqz v4, :cond_d3

    #@c9
    .line 320
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@cc
    move-result-object v4

    #@cd
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d0
    move-result-object v4

    #@d1
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@d3
    .line 325
    :cond_d3
    const/4 v2, 0x0

    #@d4
    .line 326
    .local v2, path:Ljava/lang/String;
    iget-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@d6
    if-eqz v4, :cond_de

    #@d8
    .line 327
    iget-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@da
    invoke-static {p0, v4}, Lcom/android/internal/telephony/CallerInfo;->getFilepathFromContentUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    #@dd
    move-result-object v2

    #@de
    .line 330
    :cond_de
    sget-boolean v4, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@e0
    if-eqz v4, :cond_106

    #@e2
    const-string v4, "CallerInfo"

    #@e4
    new-instance v7, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v8, "1st RingTone :: contactRingtoneUri="

    #@eb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v7

    #@ef
    iget-object v8, v1, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@f1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v7

    #@f5
    const-string v8, ", path="

    #@f7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v7

    #@fb
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v7

    #@ff
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v7

    #@103
    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@106
    .line 332
    :cond_106
    if-nez v2, :cond_13a

    #@108
    .line 340
    const-string v4, "group_custom_ringtone"

    #@10a
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@10d
    move-result v0

    #@10e
    .line 341
    if-eq v0, v10, :cond_23d

    #@110
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@113
    move-result-object v4

    #@114
    if-eqz v4, :cond_23d

    #@116
    .line 342
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@119
    move-result-object v4

    #@11a
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@11d
    move-result-object v4

    #@11e
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@120
    .line 343
    const-string v4, "CallerInfo"

    #@122
    new-instance v7, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v8, "group_custom_ringtone = "

    #@129
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v7

    #@12d
    iget-object v8, v1, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@12f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v7

    #@133
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v7

    #@137
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    .line 352
    :cond_13a
    :goto_13a
    const-string v4, "custom_vibration_type"

    #@13c
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@13f
    move-result v0

    #@140
    .line 353
    if-eq v0, v10, :cond_241

    #@142
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@145
    move-result v4

    #@146
    if-eqz v4, :cond_241

    #@148
    .line 354
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@14b
    move-result v4

    #@14c
    iput v4, v1, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@14e
    .line 355
    const-string v4, "CallerInfo"

    #@150
    new-instance v7, Ljava/lang/StringBuilder;

    #@152
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v8, "==> got info.distinctiveVib: "

    #@157
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v7

    #@15b
    iget v8, v1, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@15d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@160
    move-result-object v7

    #@161
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v7

    #@165
    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@168
    .line 370
    :goto_168
    const-string/jumbo v4, "starred"

    #@16b
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@16e
    move-result v0

    #@16f
    .line 372
    const-string v4, "CallerInfo"

    #@171
    new-instance v7, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v8, "==> Indicate favorite call columnIndex: "

    #@178
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v7

    #@17c
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v7

    #@180
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@183
    move-result-object v7

    #@184
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@187
    .line 374
    if-eq v0, v10, :cond_1a9

    #@189
    .line 375
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@18c
    move-result v4

    #@18d
    iput v4, v1, Lcom/android/internal/telephony/CallerInfo;->starred:I

    #@18f
    .line 376
    const-string v4, "CallerInfo"

    #@191
    new-instance v7, Ljava/lang/StringBuilder;

    #@193
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@196
    const-string v8, "==> Indicate favorite call: "

    #@198
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v7

    #@19c
    iget v8, v1, Lcom/android/internal/telephony/CallerInfo;->starred:I

    #@19e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v7

    #@1a2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a5
    move-result-object v7

    #@1a6
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a9
    .line 379
    :cond_1a9
    const-string v4, "custom_led_type"

    #@1ab
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1ae
    move-result v0

    #@1af
    .line 381
    const-string v4, "CallerInfo"

    #@1b1
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1b6
    const-string v8, "==> Indicate CUSTOM_LED_TYPE columnIndex: "

    #@1b8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v7

    #@1bc
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v7

    #@1c0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c3
    move-result-object v7

    #@1c4
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c7
    .line 383
    if-eq v0, v10, :cond_1e9

    #@1c9
    .line 384
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@1cc
    move-result v4

    #@1cd
    iput v4, v1, Lcom/android/internal/telephony/CallerInfo;->custom_led_type:I

    #@1cf
    .line 385
    const-string v4, "CallerInfo"

    #@1d1
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d6
    const-string v8, "==> Indicate CUSTOM_LED_TYPE : "

    #@1d8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v7

    #@1dc
    iget v8, v1, Lcom/android/internal/telephony/CallerInfo;->custom_led_type:I

    #@1de
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v7

    #@1e2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e5
    move-result-object v7

    #@1e6
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e9
    .line 390
    :cond_1e9
    const-string/jumbo v4, "send_to_voicemail"

    #@1ec
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1ef
    move-result v0

    #@1f0
    .line 391
    if-eq v0, v10, :cond_27c

    #@1f2
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@1f5
    move-result v4

    #@1f6
    if-ne v4, v5, :cond_27c

    #@1f8
    move v4, v5

    #@1f9
    :goto_1f9
    iput-boolean v4, v1, Lcom/android/internal/telephony/CallerInfo;->shouldSendToVoicemail:Z

    #@1fb
    .line 393
    iput-boolean v5, v1, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@1fd
    .line 395
    const-string/jumbo v4, "lookup"

    #@200
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@203
    move-result v0

    #@204
    .line 396
    if-eq v0, v10, :cond_20c

    #@206
    .line 397
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@209
    move-result-object v4

    #@20a
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@20c
    .line 403
    .end local v0           #columnIndex:I
    .end local v2           #path:Ljava/lang/String;
    :cond_20c
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    #@20f
    move-result v4

    #@210
    invoke-direct {v1, v4}, Lcom/android/internal/telephony/CallerInfo;->setContactExistsCount(I)V

    #@213
    .line 406
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    #@216
    .line 409
    :cond_216
    iput-boolean v6, v1, Lcom/android/internal/telephony/CallerInfo;->needUpdate:Z

    #@218
    .line 410
    iget-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@21a
    invoke-static {v4}, Lcom/android/internal/telephony/CallerInfo;->normalize(Ljava/lang/String;)Ljava/lang/String;

    #@21d
    move-result-object v4

    #@21e
    iput-object v4, v1, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@220
    .line 411
    iput-object p1, v1, Lcom/android/internal/telephony/CallerInfo;->contactRefUri:Landroid/net/Uri;

    #@222
    .line 413
    return-object v1

    #@223
    .line 310
    .restart local v0       #columnIndex:I
    :cond_223
    const-string v4, "CallerInfo"

    #@225
    new-instance v7, Ljava/lang/StringBuilder;

    #@227
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@22a
    const-string v8, "Couldn\'t find person_id column for "

    #@22c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v7

    #@230
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v7

    #@234
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v7

    #@238
    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23b
    goto/16 :goto_bb

    #@23d
    .line 345
    .restart local v2       #path:Ljava/lang/String;
    :cond_23d
    iput-object v11, v1, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@23f
    goto/16 :goto_13a

    #@241
    .line 357
    :cond_241
    const-string v4, "group_custom_vibrator"

    #@243
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@246
    move-result v0

    #@247
    .line 358
    if-eq v0, v10, :cond_271

    #@249
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@24c
    move-result v4

    #@24d
    if-eqz v4, :cond_271

    #@24f
    .line 359
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@252
    move-result v4

    #@253
    iput v4, v1, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@255
    .line 360
    const-string v4, "CallerInfo"

    #@257
    new-instance v7, Ljava/lang/StringBuilder;

    #@259
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@25c
    const-string v8, "==> (group : got info.distinctiveVib: "

    #@25e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@261
    move-result-object v7

    #@262
    iget v8, v1, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@264
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@267
    move-result-object v7

    #@268
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26b
    move-result-object v7

    #@26c
    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26f
    goto/16 :goto_168

    #@271
    .line 362
    :cond_271
    iput v6, v1, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@273
    .line 363
    const-string v4, "CallerInfo"

    #@275
    const-string v7, "==> (info.distinctiveVib : default value 0"

    #@277
    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27a
    goto/16 :goto_168

    #@27c
    :cond_27c
    move v4, v6

    #@27d
    .line 391
    goto/16 :goto_1f9
.end method

.method public static getCallerInfo(Landroid/content/Context;Landroid/net/Uri;Landroid/database/Cursor;Ljava/lang/String;)Lcom/android/internal/telephony/CallerInfo;
    .registers 16
    .parameter "context"
    .parameter "contactRef"
    .parameter "cursor"
    .parameter "inputNumber"

    #@0
    .prologue
    .line 426
    new-instance v3, Lcom/android/internal/telephony/CallerInfo;

    #@2
    invoke-direct {v3}, Lcom/android/internal/telephony/CallerInfo;-><init>()V

    #@5
    .line 427
    .local v3, info:Lcom/android/internal/telephony/CallerInfo;
    const/4 v8, 0x0

    #@6
    iput v8, v3, Lcom/android/internal/telephony/CallerInfo;->photoResource:I

    #@8
    .line 428
    const/4 v8, 0x0

    #@9
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->phoneLabel:Ljava/lang/String;

    #@b
    .line 429
    const/4 v8, 0x0

    #@c
    iput v8, v3, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@e
    .line 430
    const/4 v8, 0x0

    #@f
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@11
    .line 431
    const/4 v8, 0x0

    #@12
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->cachedPhoto:Landroid/graphics/drawable/Drawable;

    #@14
    .line 432
    const/4 v8, 0x0

    #@15
    iput-boolean v8, v3, Lcom/android/internal/telephony/CallerInfo;->isCachedPhotoCurrent:Z

    #@17
    .line 433
    const/4 v8, 0x0

    #@18
    iput-boolean v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@1a
    .line 435
    const/4 v8, 0x0

    #@1b
    invoke-direct {v3, v8}, Lcom/android/internal/telephony/CallerInfo;->setContactExistsCount(I)V

    #@1e
    .line 439
    const-string v8, ""

    #@20
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->socialStatus:Ljava/lang/String;

    #@22
    .line 440
    const-string v8, ""

    #@24
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->eventDate:Ljava/lang/String;

    #@26
    .line 441
    const/4 v8, 0x0

    #@27
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->socialStatusIconRes:Landroid/graphics/drawable/Drawable;

    #@29
    .line 444
    new-instance v2, Lcom/android/internal/telephony/CallerInfo;

    #@2b
    invoke-direct {v2}, Lcom/android/internal/telephony/CallerInfo;-><init>()V

    #@2e
    .line 445
    .local v2, firstCallerInfo:Lcom/android/internal/telephony/CallerInfo;
    const/4 v8, 0x0

    #@2f
    iput v8, v2, Lcom/android/internal/telephony/CallerInfo;->photoResource:I

    #@31
    .line 446
    const/4 v8, 0x0

    #@32
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->phoneLabel:Ljava/lang/String;

    #@34
    .line 447
    const/4 v8, 0x0

    #@35
    iput v8, v2, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@37
    .line 448
    const/4 v8, 0x0

    #@38
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@3a
    .line 449
    const/4 v8, 0x0

    #@3b
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->cachedPhoto:Landroid/graphics/drawable/Drawable;

    #@3d
    .line 450
    const/4 v8, 0x0

    #@3e
    iput-boolean v8, v2, Lcom/android/internal/telephony/CallerInfo;->isCachedPhotoCurrent:Z

    #@40
    .line 451
    const/4 v8, 0x0

    #@41
    iput-boolean v8, v2, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@43
    .line 453
    const/4 v8, 0x0

    #@44
    invoke-direct {v2, v8}, Lcom/android/internal/telephony/CallerInfo;->setContactExistsCount(I)V

    #@47
    .line 457
    const-string v8, ""

    #@49
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->socialStatus:Ljava/lang/String;

    #@4b
    .line 458
    const-string v8, ""

    #@4d
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->eventDate:Ljava/lang/String;

    #@4f
    .line 459
    const/4 v8, 0x0

    #@50
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->socialStatusIconRes:Landroid/graphics/drawable/Drawable;

    #@52
    .line 462
    const/4 v4, 0x0

    #@53
    .line 463
    .local v4, isExactlyMatch:Z
    const/4 v1, 0x0

    #@54
    .line 464
    .local v1, compareNumber:Ljava/lang/String;
    const-string v8, "CallerInfo"

    #@56
    new-instance v9, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v10, "getCallerInfo() LGE phone number query - inputNumber : "

    #@5d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v9

    #@61
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v9

    #@69
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 465
    sget-boolean v8, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@6e
    if-eqz v8, :cond_77

    #@70
    const-string v8, "CallerInfo"

    #@72
    const-string v9, "getCallerInfo() based on cursor..."

    #@74
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 467
    :cond_77
    if-eqz p2, :cond_28e

    #@79
    .line 469
    const/4 v5, 0x0

    #@7a
    .line 470
    .local v5, isFirst:Z
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    #@7d
    move-result v8

    #@7e
    if-eqz v8, :cond_27d

    #@80
    .line 472
    :cond_80
    invoke-interface {p2}, Landroid/database/Cursor;->isFirst()Z

    #@83
    move-result v8

    #@84
    if-eqz v8, :cond_2b2

    #@86
    .line 473
    const-string v8, "CallerInfo"

    #@88
    const-string v9, "getCallerInfo() LGE phone number query - cursor.isFirst()"

    #@8a
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 474
    const/4 v5, 0x1

    #@8e
    .line 484
    :goto_8e
    const-string v8, "display_name"

    #@90
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@93
    move-result v0

    #@94
    .line 485
    .local v0, columnIndex:I
    const/4 v8, -0x1

    #@95
    if-eq v0, v8, :cond_9d

    #@97
    .line 486
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9a
    move-result-object v8

    #@9b
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@9d
    .line 490
    :cond_9d
    const-string/jumbo v8, "number"

    #@a0
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a3
    move-result v0

    #@a4
    .line 491
    const/4 v8, -0x1

    #@a5
    if-eq v0, v8, :cond_ad

    #@a7
    .line 492
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v8

    #@ab
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@ad
    .line 496
    :cond_ad
    const-string/jumbo v8, "normalized_number"

    #@b0
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@b3
    move-result v0

    #@b4
    .line 497
    const/4 v8, -0x1

    #@b5
    if-eq v0, v8, :cond_bd

    #@b7
    .line 498
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v8

    #@bb
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@bd
    .line 502
    :cond_bd
    const-string/jumbo v8, "label"

    #@c0
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@c3
    move-result v0

    #@c4
    .line 503
    const/4 v8, -0x1

    #@c5
    if-eq v0, v8, :cond_eb

    #@c7
    .line 504
    const-string/jumbo v8, "type"

    #@ca
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@cd
    move-result v7

    #@ce
    .line 505
    .local v7, typeColumnIndex:I
    const/4 v8, -0x1

    #@cf
    if-eq v7, v8, :cond_eb

    #@d1
    .line 506
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    #@d4
    move-result v8

    #@d5
    iput v8, v3, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@d7
    .line 507
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@da
    move-result-object v8

    #@db
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@dd
    .line 508
    iget v8, v3, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@df
    iget-object v9, v3, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@e1
    invoke-static {p0, v8, v9}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getDisplayLabel(Landroid/content/Context;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@e4
    move-result-object v8

    #@e5
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@e8
    move-result-object v8

    #@e9
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->phoneLabel:Ljava/lang/String;

    #@eb
    .line 515
    .end local v7           #typeColumnIndex:I
    :cond_eb
    invoke-static {p1, p2}, Lcom/android/internal/telephony/CallerInfo;->getColumnIndexForPersonId(Landroid/net/Uri;Landroid/database/Cursor;)I

    #@ee
    move-result v0

    #@ef
    .line 516
    const/4 v8, -0x1

    #@f0
    if-eq v0, v8, :cond_2b5

    #@f2
    .line 517
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    #@f5
    move-result-wide v8

    #@f6
    iput-wide v8, v3, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@f8
    .line 518
    sget-boolean v8, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@fa
    if-eqz v8, :cond_116

    #@fc
    const-string v8, "CallerInfo"

    #@fe
    new-instance v9, Ljava/lang/StringBuilder;

    #@100
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@103
    const-string v10, "==> got info.person_id: "

    #@105
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v9

    #@109
    iget-wide v10, v3, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@10b
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v9

    #@10f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@112
    move-result-object v9

    #@113
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@116
    .line 529
    :cond_116
    :goto_116
    const-string v8, "custom_ringtone"

    #@118
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@11b
    move-result v0

    #@11c
    .line 530
    const/4 v8, -0x1

    #@11d
    if-eq v0, v8, :cond_12f

    #@11f
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@122
    move-result-object v8

    #@123
    if-eqz v8, :cond_12f

    #@125
    .line 531
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@128
    move-result-object v8

    #@129
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@12c
    move-result-object v8

    #@12d
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@12f
    .line 535
    :cond_12f
    const/4 v6, 0x0

    #@130
    .line 536
    .local v6, path:Ljava/lang/String;
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@132
    if-eqz v8, :cond_13a

    #@134
    .line 537
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@136
    invoke-static {p0, v8}, Lcom/android/internal/telephony/CallerInfo;->getFilepathFromContentUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    #@139
    move-result-object v6

    #@13a
    .line 540
    :cond_13a
    sget-boolean v8, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@13c
    if-eqz v8, :cond_162

    #@13e
    const-string v8, "CallerInfo"

    #@140
    new-instance v9, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v10, "1st RingTone :: contactRingtoneUri="

    #@147
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v9

    #@14b
    iget-object v10, v3, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@14d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v9

    #@151
    const-string v10, ", path="

    #@153
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v9

    #@157
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v9

    #@15b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15e
    move-result-object v9

    #@15f
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@162
    .line 542
    :cond_162
    if-nez v6, :cond_17d

    #@164
    .line 552
    const-string v8, "group_custom_ringtone"

    #@166
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@169
    move-result v0

    #@16a
    .line 553
    const/4 v8, -0x1

    #@16b
    if-eq v0, v8, :cond_2cf

    #@16d
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@170
    move-result-object v8

    #@171
    if-eqz v8, :cond_2cf

    #@173
    .line 554
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@176
    move-result-object v8

    #@177
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@17a
    move-result-object v8

    #@17b
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@17d
    .line 565
    :cond_17d
    :goto_17d
    const-string v8, "custom_vibration_type"

    #@17f
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@182
    move-result v0

    #@183
    .line 566
    const/4 v8, -0x1

    #@184
    if-eq v0, v8, :cond_2dc

    #@186
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@189
    move-result v8

    #@18a
    if-eqz v8, :cond_2dc

    #@18c
    .line 567
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@18f
    move-result v8

    #@190
    iput v8, v3, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@192
    .line 568
    const-string v8, "CallerInfo"

    #@194
    new-instance v9, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    const-string v10, "==> got info.distinctiveVib: "

    #@19b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v9

    #@19f
    iget v10, v3, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@1a1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v9

    #@1a5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v9

    #@1a9
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ac
    .line 577
    :goto_1ac
    const-string/jumbo v8, "starred"

    #@1af
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1b2
    move-result v0

    #@1b3
    .line 579
    const-string v8, "CallerInfo"

    #@1b5
    new-instance v9, Ljava/lang/StringBuilder;

    #@1b7
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1ba
    const-string v10, "==> Indicate favorite call columnIndex: "

    #@1bc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v9

    #@1c0
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v9

    #@1c4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c7
    move-result-object v9

    #@1c8
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cb
    .line 581
    const/4 v8, -0x1

    #@1cc
    if-eq v0, v8, :cond_1ee

    #@1ce
    .line 582
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@1d1
    move-result v8

    #@1d2
    iput v8, v3, Lcom/android/internal/telephony/CallerInfo;->starred:I

    #@1d4
    .line 583
    const-string v8, "CallerInfo"

    #@1d6
    new-instance v9, Ljava/lang/StringBuilder;

    #@1d8
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1db
    const-string v10, "==> Indicate favorite call : "

    #@1dd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e0
    move-result-object v9

    #@1e1
    iget v10, v3, Lcom/android/internal/telephony/CallerInfo;->starred:I

    #@1e3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v9

    #@1e7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ea
    move-result-object v9

    #@1eb
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ee
    .line 586
    :cond_1ee
    const-string v8, "custom_led_type"

    #@1f0
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1f3
    move-result v0

    #@1f4
    .line 588
    const-string v8, "CallerInfo"

    #@1f6
    new-instance v9, Ljava/lang/StringBuilder;

    #@1f8
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1fb
    const-string v10, "==> Indicate CUSTOM_LED_TYPE columnIndex: "

    #@1fd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v9

    #@201
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@204
    move-result-object v9

    #@205
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@208
    move-result-object v9

    #@209
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20c
    .line 590
    const/4 v8, -0x1

    #@20d
    if-eq v0, v8, :cond_22f

    #@20f
    .line 591
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@212
    move-result v8

    #@213
    iput v8, v3, Lcom/android/internal/telephony/CallerInfo;->custom_led_type:I

    #@215
    .line 592
    const-string v8, "CallerInfo"

    #@217
    new-instance v9, Ljava/lang/StringBuilder;

    #@219
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@21c
    const-string v10, "==> Indicate CUSTOM_LED_TYPE : "

    #@21e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@221
    move-result-object v9

    #@222
    iget v10, v3, Lcom/android/internal/telephony/CallerInfo;->custom_led_type:I

    #@224
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@227
    move-result-object v9

    #@228
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22b
    move-result-object v9

    #@22c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22f
    .line 598
    :cond_22f
    const-string/jumbo v8, "send_to_voicemail"

    #@232
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@235
    move-result v0

    #@236
    .line 599
    const/4 v8, -0x1

    #@237
    if-eq v0, v8, :cond_2e8

    #@239
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    #@23c
    move-result v8

    #@23d
    const/4 v9, 0x1

    #@23e
    if-ne v8, v9, :cond_2e8

    #@240
    const/4 v8, 0x1

    #@241
    :goto_241
    iput-boolean v8, v3, Lcom/android/internal/telephony/CallerInfo;->shouldSendToVoicemail:Z

    #@243
    .line 601
    const/4 v8, 0x1

    #@244
    iput-boolean v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@246
    .line 604
    const-string/jumbo v8, "lookup"

    #@249
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@24c
    move-result v0

    #@24d
    .line 605
    const/4 v8, -0x1

    #@24e
    if-eq v0, v8, :cond_256

    #@250
    .line 606
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@253
    move-result-object v8

    #@254
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@256
    .line 609
    :cond_256
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@258
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    #@25b
    move-result-object v1

    #@25c
    .line 610
    const-string v8, "CallerInfo"

    #@25e
    new-instance v9, Ljava/lang/StringBuilder;

    #@260
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@263
    const-string v10, "getCallerInfo() LGE phone number query - compareNumber (remove hyphen) : "

    #@265
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@268
    move-result-object v9

    #@269
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26c
    move-result-object v9

    #@26d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@270
    move-result-object v9

    #@271
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@274
    .line 612
    if-eqz p3, :cond_2eb

    #@276
    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@279
    move-result v8

    #@27a
    if-eqz v8, :cond_2eb

    #@27c
    .line 613
    const/4 v4, 0x1

    #@27d
    .line 642
    .end local v0           #columnIndex:I
    .end local v6           #path:Ljava/lang/String;
    :cond_27d
    :goto_27d
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    #@280
    move-result v8

    #@281
    invoke-direct {v3, v8}, Lcom/android/internal/telephony/CallerInfo;->setContactExistsCount(I)V

    #@284
    .line 645
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    #@287
    move-result v8

    #@288
    invoke-direct {v2, v8}, Lcom/android/internal/telephony/CallerInfo;->setContactExistsCount(I)V

    #@28b
    .line 646
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    #@28e
    .line 649
    .end local v5           #isFirst:Z
    :cond_28e
    const/4 v8, 0x0

    #@28f
    iput-boolean v8, v3, Lcom/android/internal/telephony/CallerInfo;->needUpdate:Z

    #@291
    .line 650
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@293
    invoke-static {v8}, Lcom/android/internal/telephony/CallerInfo;->normalize(Ljava/lang/String;)Ljava/lang/String;

    #@296
    move-result-object v8

    #@297
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@299
    .line 651
    iput-object p1, v3, Lcom/android/internal/telephony/CallerInfo;->contactRefUri:Landroid/net/Uri;

    #@29b
    .line 653
    const/4 v8, 0x0

    #@29c
    iput-boolean v8, v2, Lcom/android/internal/telephony/CallerInfo;->needUpdate:Z

    #@29e
    .line 654
    iget-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@2a0
    invoke-static {v8}, Lcom/android/internal/telephony/CallerInfo;->normalize(Ljava/lang/String;)Ljava/lang/String;

    #@2a3
    move-result-object v8

    #@2a4
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@2a6
    .line 655
    iput-object p1, v2, Lcom/android/internal/telephony/CallerInfo;->contactRefUri:Landroid/net/Uri;

    #@2a8
    .line 657
    if-nez v4, :cond_363

    #@2aa
    .line 658
    const-string v8, "CallerInfo"

    #@2ac
    const-string v9, "getCallerInfo() LGE phone number query - isn\'t exactly match, so return first CallerInfo"

    #@2ae
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b1
    .line 662
    .end local v2           #firstCallerInfo:Lcom/android/internal/telephony/CallerInfo;
    :goto_2b1
    return-object v2

    #@2b2
    .line 476
    .restart local v2       #firstCallerInfo:Lcom/android/internal/telephony/CallerInfo;
    .restart local v5       #isFirst:Z
    :cond_2b2
    const/4 v5, 0x0

    #@2b3
    goto/16 :goto_8e

    #@2b5
    .line 521
    .restart local v0       #columnIndex:I
    :cond_2b5
    const-string v8, "CallerInfo"

    #@2b7
    new-instance v9, Ljava/lang/StringBuilder;

    #@2b9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2bc
    const-string v10, "Couldn\'t find person_id column for "

    #@2be
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v9

    #@2c2
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v9

    #@2c6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c9
    move-result-object v9

    #@2ca
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2cd
    goto/16 :goto_116

    #@2cf
    .line 557
    .restart local v6       #path:Ljava/lang/String;
    :cond_2cf
    const-string v8, "CallerInfo"

    #@2d1
    const-string/jumbo v9, "ringtone is null"

    #@2d4
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d7
    .line 558
    const/4 v8, 0x0

    #@2d8
    iput-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@2da
    goto/16 :goto_17d

    #@2dc
    .line 570
    :cond_2dc
    const/4 v8, 0x0

    #@2dd
    iput v8, v3, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@2df
    .line 571
    const-string v8, "CallerInfo"

    #@2e1
    const-string v9, "==> (info.distinctiveVib : default value 0"

    #@2e3
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e6
    goto/16 :goto_1ac

    #@2e8
    .line 599
    :cond_2e8
    const/4 v8, 0x0

    #@2e9
    goto/16 :goto_241

    #@2eb
    .line 616
    :cond_2eb
    const/4 v4, 0x0

    #@2ec
    .line 617
    const-string v8, "CallerInfo"

    #@2ee
    new-instance v9, Ljava/lang/StringBuilder;

    #@2f0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2f3
    const-string v10, "getCallerInfo()LGE phone number query - don\'t exactly mateched, inputNumber : "

    #@2f5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f8
    move-result-object v9

    #@2f9
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fc
    move-result-object v9

    #@2fd
    const-string v10, ", info.phoneNumber : "

    #@2ff
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@302
    move-result-object v9

    #@303
    iget-object v10, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@305
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@308
    move-result-object v9

    #@309
    const-string v10, ", isExactlyMatch : "

    #@30b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30e
    move-result-object v9

    #@30f
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@312
    move-result-object v9

    #@313
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@316
    move-result-object v9

    #@317
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31a
    .line 619
    if-eqz v5, :cond_35b

    #@31c
    .line 620
    const-string v8, "CallerInfo"

    #@31e
    const-string v9, "getCallerInfo() LGE phone number query - save first CallserInfo"

    #@320
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@323
    .line 621
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@325
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@327
    .line 622
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@329
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@32b
    .line 623
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@32d
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@32f
    .line 624
    iget v8, v3, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@331
    iput v8, v2, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@333
    .line 625
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@335
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@337
    .line 626
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->phoneLabel:Ljava/lang/String;

    #@339
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->phoneLabel:Ljava/lang/String;

    #@33b
    .line 627
    iget-wide v8, v3, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@33d
    iput-wide v8, v2, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@33f
    .line 628
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@341
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    #@343
    .line 629
    iget v8, v3, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@345
    iput v8, v2, Lcom/android/internal/telephony/CallerInfo;->distinctiveVib:I

    #@347
    .line 630
    iget v8, v3, Lcom/android/internal/telephony/CallerInfo;->starred:I

    #@349
    iput v8, v2, Lcom/android/internal/telephony/CallerInfo;->starred:I

    #@34b
    .line 631
    iget v8, v3, Lcom/android/internal/telephony/CallerInfo;->custom_led_type:I

    #@34d
    iput v8, v2, Lcom/android/internal/telephony/CallerInfo;->custom_led_type:I

    #@34f
    .line 632
    iget-boolean v8, v3, Lcom/android/internal/telephony/CallerInfo;->shouldSendToVoicemail:Z

    #@351
    iput-boolean v8, v2, Lcom/android/internal/telephony/CallerInfo;->shouldSendToVoicemail:Z

    #@353
    .line 633
    iget-boolean v8, v3, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@355
    iput-boolean v8, v2, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@357
    .line 634
    iget-object v8, v3, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@359
    iput-object v8, v2, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@35b
    .line 638
    :cond_35b
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    #@35e
    move-result v8

    #@35f
    if-nez v8, :cond_80

    #@361
    goto/16 :goto_27d

    #@363
    .end local v0           #columnIndex:I
    .end local v5           #isFirst:Z
    .end local v6           #path:Ljava/lang/String;
    :cond_363
    move-object v2, v3

    #@364
    .line 662
    goto/16 :goto_2b1
.end method

.method public static getCallerInfo(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/android/internal/telephony/CallerInfo;
    .registers 9
    .parameter "context"
    .parameter "contactRef"
    .parameter "inputNumber"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 691
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    move-object v1, p1

    #@6
    move-object v3, v2

    #@7
    move-object v4, v2

    #@8
    move-object v5, v2

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c
    move-result-object v0

    #@d
    invoke-static {p0, p1, v0, p2}, Lcom/android/internal/telephony/CallerInfo;->getCallerInfo(Landroid/content/Context;Landroid/net/Uri;Landroid/database/Cursor;Ljava/lang/String;)Lcom/android/internal/telephony/CallerInfo;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public static getCallerInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/CallerInfo;
    .registers 6
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    .line 707
    sget-boolean v2, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@2
    if-eqz v2, :cond_b

    #@4
    const-string v2, "CallerInfo"

    #@6
    const-string v3, "getCallerInfo() based on number..."

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 709
    :cond_b
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_13

    #@11
    .line 710
    const/4 v1, 0x0

    #@12
    .line 742
    :cond_12
    :goto_12
    return-object v1

    #@13
    .line 716
    :cond_13
    invoke-static {p1, p0}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_23

    #@19
    .line 717
    new-instance v2, Lcom/android/internal/telephony/CallerInfo;

    #@1b
    invoke-direct {v2}, Lcom/android/internal/telephony/CallerInfo;-><init>()V

    #@1e
    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/CallerInfo;->markAsEmergency(Landroid/content/Context;)Lcom/android/internal/telephony/CallerInfo;

    #@21
    move-result-object v1

    #@22
    goto :goto_12

    #@23
    .line 718
    :cond_23
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isVoiceMailNumber(Ljava/lang/String;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_33

    #@29
    .line 719
    new-instance v2, Lcom/android/internal/telephony/CallerInfo;

    #@2b
    invoke-direct {v2}, Lcom/android/internal/telephony/CallerInfo;-><init>()V

    #@2e
    invoke-virtual {v2}, Lcom/android/internal/telephony/CallerInfo;->markAsVoiceMail()Lcom/android/internal/telephony/CallerInfo;

    #@31
    move-result-object v1

    #@32
    goto :goto_12

    #@33
    .line 723
    :cond_33
    const-string/jumbo v2, "support_sprint_n11"

    #@36
    invoke-static {p0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_53

    #@3c
    .line 724
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isN11orSpecialNumber(Ljava/lang/String;)Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_53

    #@42
    .line 725
    const-string v2, "CallerInfo"

    #@44
    const-string v3, "[SPRINT-Telephony] ADC - N11 !"

    #@46
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 726
    new-instance v2, Lcom/android/internal/telephony/CallerInfo;

    #@4b
    invoke-direct {v2}, Lcom/android/internal/telephony/CallerInfo;-><init>()V

    #@4e
    invoke-virtual {v2, p0, p1}, Lcom/android/internal/telephony/CallerInfo;->markAsN11OrSpecial(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/CallerInfo;

    #@51
    move-result-object v1

    #@52
    goto :goto_12

    #@53
    .line 731
    :cond_53
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@55
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@5c
    move-result-object v0

    #@5d
    .line 733
    .local v0, contactUri:Landroid/net/Uri;
    invoke-static {p0, v0}, Lcom/android/internal/telephony/CallerInfo;->getCallerInfo(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/internal/telephony/CallerInfo;

    #@60
    move-result-object v1

    #@61
    .line 734
    .local v1, info:Lcom/android/internal/telephony/CallerInfo;
    invoke-static {p0, p1, v1}, Lcom/android/internal/telephony/CallerInfo;->doSecondaryLookupIfNecessary(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/CallerInfo;)Lcom/android/internal/telephony/CallerInfo;

    #@64
    move-result-object v1

    #@65
    .line 738
    iget-object v2, v1, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@67
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6a
    move-result v2

    #@6b
    if-eqz v2, :cond_12

    #@6d
    .line 739
    iput-object p1, v1, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@6f
    goto :goto_12
.end method

.method private static getColumnIndexForPersonId(Landroid/net/Uri;Landroid/database/Cursor;)I
    .registers 8
    .parameter "contactRef"
    .parameter "cursor"

    #@0
    .prologue
    .line 929
    sget-boolean v3, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@2
    if-eqz v3, :cond_22

    #@4
    const-string v3, "CallerInfo"

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "- getColumnIndexForPersonId: contactRef URI = \'"

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    const-string v5, "\'..."

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 935
    :cond_22
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .line 936
    .local v2, url:Ljava/lang/String;
    const/4 v1, 0x0

    #@27
    .line 937
    .local v1, columnName:Ljava/lang/String;
    const-string v3, "content://com.android.contacts/data/phones"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_6f

    #@2f
    .line 940
    sget-boolean v3, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@31
    if-eqz v3, :cond_3a

    #@33
    const-string v3, "CallerInfo"

    #@35
    const-string v4, "\'data/phones\' URI; using RawContacts.CONTACT_ID"

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 941
    :cond_3a
    const-string v1, "contact_id"

    #@3c
    .line 957
    :goto_3c
    if-eqz v1, :cond_ba

    #@3e
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@41
    move-result v0

    #@42
    .line 958
    .local v0, columnIndex:I
    :goto_42
    sget-boolean v3, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@44
    if-eqz v3, :cond_6e

    #@46
    const-string v3, "CallerInfo"

    #@48
    new-instance v4, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v5, "==> Using column \'"

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    const-string v5, "\' (columnIndex = "

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    const-string v5, ") for person_id lookup..."

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v4

    #@6b
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 960
    :cond_6e
    return v0

    #@6f
    .line 942
    .end local v0           #columnIndex:I
    :cond_6f
    const-string v3, "content://com.android.contacts/data"

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@74
    move-result v3

    #@75
    if-eqz v3, :cond_85

    #@77
    .line 945
    sget-boolean v3, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@79
    if-eqz v3, :cond_82

    #@7b
    const-string v3, "CallerInfo"

    #@7d
    const-string v4, "\'data\' URI; using Data.CONTACT_ID"

    #@7f
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 947
    :cond_82
    const-string v1, "contact_id"

    #@84
    goto :goto_3c

    #@85
    .line 948
    :cond_85
    const-string v3, "content://com.android.contacts/phone_lookup"

    #@87
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8a
    move-result v3

    #@8b
    if-eqz v3, :cond_9b

    #@8d
    .line 952
    sget-boolean v3, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@8f
    if-eqz v3, :cond_98

    #@91
    const-string v3, "CallerInfo"

    #@93
    const-string v4, "\'phone_lookup\' URI; using PhoneLookup._ID"

    #@95
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 953
    :cond_98
    const-string v1, "_id"

    #@9a
    goto :goto_3c

    #@9b
    .line 955
    :cond_9b
    const-string v3, "CallerInfo"

    #@9d
    new-instance v4, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v5, "Unexpected prefix for contactRef \'"

    #@a4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v4

    #@a8
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    const-string v5, "\'"

    #@ae
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v4

    #@b2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v4

    #@b6
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    goto :goto_3c

    #@ba
    .line 957
    :cond_ba
    const/4 v0, -0x1

    #@bb
    goto :goto_42
.end method

.method private static getCurrentCountryIso(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;
    .registers 7
    .parameter "context"
    .parameter "locale"

    #@0
    .prologue
    .line 1023
    const-string v2, "country_detector"

    #@2
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/location/CountryDetector;

    #@8
    .line 1025
    .local v1, detector:Landroid/location/CountryDetector;
    if-eqz v1, :cond_13

    #@a
    .line 1026
    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 1032
    .local v0, countryIso:Ljava/lang/String;
    :goto_12
    return-object v0

    #@13
    .line 1028
    .end local v0           #countryIso:Ljava/lang/String;
    :cond_13
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 1029
    .restart local v0       #countryIso:Ljava/lang/String;
    const-string v2, "CallerInfo"

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "No CountryDetector; falling back to countryIso based on locale: "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_12
.end method

.method static getFilepathFromContentUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .registers 16
    .parameter "mContext"
    .parameter "uri"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v13, 0x0

    #@2
    .line 1089
    const/4 v11, 0x0

    #@3
    .line 1090
    .local v11, filepath:Ljava/lang/String;
    const/4 v7, 0x0

    #@4
    .line 1091
    .local v7, c:Landroid/database/Cursor;
    const/4 v0, 0x0

    #@5
    .line 1094
    .local v0, testProvider:Landroid/content/IContentProvider;
    :try_start_5
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@c
    move-result-object v0

    #@d
    .line 1095
    if-eqz v0, :cond_20

    #@f
    .line 1096
    const/4 v1, 0x1

    #@10
    new-array v2, v1, [Ljava/lang/String;

    #@12
    const/4 v1, 0x0

    #@13
    const-string v3, "_data"

    #@15
    aput-object v3, v2, v1

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x0

    #@1a
    const/4 v6, 0x0

    #@1b
    move-object v1, p1

    #@1c
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
    :try_end_1f
    .catchall {:try_start_5 .. :try_end_1f} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_1f} :catch_34
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_1f} :catch_46

    #@1f
    move-result-object v7

    #@20
    .line 1107
    :cond_20
    if-eqz v0, :cond_29

    #@22
    .line 1108
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@29
    .line 1112
    :cond_29
    :goto_29
    if-eqz v7, :cond_2f

    #@2b
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@2e
    move-result v8

    #@2f
    .line 1113
    .local v8, count:I
    :cond_2f
    if-lez v8, :cond_72

    #@31
    .line 1114
    if-nez v7, :cond_63

    #@33
    .line 1131
    :cond_33
    :goto_33
    return-object v13

    #@34
    .line 1099
    .end local v8           #count:I
    :catch_34
    move-exception v9

    #@35
    .line 1101
    .local v9, e:Landroid/os/RemoteException;
    :try_start_35
    const-string v1, "CallerInfo"

    #@37
    const-string v2, "getFilepathFromContentUri... error!! "

    #@39
    invoke-static {v1, v2, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3c
    .catchall {:try_start_35 .. :try_end_3c} :catchall_58

    #@3c
    .line 1107
    if-eqz v0, :cond_29

    #@3e
    .line 1108
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@45
    goto :goto_29

    #@46
    .line 1103
    .end local v9           #e:Landroid/os/RemoteException;
    :catch_46
    move-exception v10

    #@47
    .line 1104
    .local v10, ex:Ljava/lang/Exception;
    :try_start_47
    const-string v1, "CallerInfo"

    #@49
    const-string v2, "getFilepathFromContentUri error is occured"

    #@4b
    invoke-static {v1, v2, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4e
    .catchall {:try_start_47 .. :try_end_4e} :catchall_58

    #@4e
    .line 1107
    if-eqz v0, :cond_29

    #@50
    .line 1108
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@57
    goto :goto_29

    #@58
    .line 1107
    .end local v10           #ex:Ljava/lang/Exception;
    :catchall_58
    move-exception v1

    #@59
    if-eqz v0, :cond_62

    #@5b
    .line 1108
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@62
    :cond_62
    throw v1

    #@63
    .line 1118
    .restart local v8       #count:I
    :cond_63
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@66
    .line 1119
    const-string v1, "_data"

    #@68
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@6b
    move-result v12

    #@6c
    .line 1120
    .local v12, i:I
    if-ltz v12, :cond_7b

    #@6e
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v11

    #@72
    .line 1123
    .end local v12           #i:I
    :cond_72
    :goto_72
    if-eqz v7, :cond_77

    #@74
    .line 1124
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@77
    .line 1127
    :cond_77
    if-eqz v11, :cond_33

    #@79
    move-object v13, v11

    #@7a
    .line 1131
    goto :goto_33

    #@7b
    .restart local v12       #i:I
    :cond_7b
    move-object v11, v13

    #@7c
    .line 1120
    goto :goto_72
.end method

.method private static getGeoDescription(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 985
    sget-boolean v8, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@3
    if-eqz v8, :cond_23

    #@5
    const-string v8, "CallerInfo"

    #@7
    new-instance v9, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v10, "getGeoDescription(\'"

    #@e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v9

    #@12
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v9

    #@16
    const-string v10, "\')..."

    #@18
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 987
    :cond_23
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@26
    move-result v8

    #@27
    if-eqz v8, :cond_2a

    #@29
    .line 1013
    :cond_29
    :goto_29
    return-object v1

    #@2a
    .line 991
    :cond_2a
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    #@2d
    move-result-object v7

    #@2e
    .line 992
    .local v7, util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    invoke-static {}, Lcom/android/i18n/phonenumbers/geocoding/PhoneNumberOfflineGeocoder;->getInstance()Lcom/android/i18n/phonenumbers/geocoding/PhoneNumberOfflineGeocoder;

    #@31
    move-result-object v4

    #@32
    .line 994
    .local v4, geocoder:Lcom/android/i18n/phonenumbers/geocoding/PhoneNumberOfflineGeocoder;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@35
    move-result-object v8

    #@36
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@39
    move-result-object v8

    #@3a
    iget-object v5, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@3c
    .line 995
    .local v5, locale:Ljava/util/Locale;
    invoke-static {p0, v5}, Lcom/android/internal/telephony/CallerInfo;->getCurrentCountryIso(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 996
    .local v0, countryIso:Ljava/lang/String;
    const/4 v6, 0x0

    #@41
    .line 998
    .local v6, pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :try_start_41
    sget-boolean v8, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@43
    if-eqz v8, :cond_6e

    #@45
    const-string v8, "CallerInfo"

    #@47
    new-instance v9, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string/jumbo v10, "parsing \'"

    #@4f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v9

    #@53
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v9

    #@57
    const-string v10, "\' for countryIso \'"

    #@59
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v9

    #@5d
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v9

    #@61
    const-string v10, "\'..."

    #@63
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v9

    #@67
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v9

    #@6b
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 1000
    :cond_6e
    invoke-virtual {v7, p1, v0}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;

    #@71
    move-result-object v6

    #@72
    .line 1001
    sget-boolean v8, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@74
    if-eqz v8, :cond_8e

    #@76
    const-string v8, "CallerInfo"

    #@78
    new-instance v9, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v10, "- parsed number: "

    #@7f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v9

    #@83
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v9

    #@87
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v9

    #@8b
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8e
    .catch Lcom/android/i18n/phonenumbers/NumberParseException; {:try_start_41 .. :try_end_8e} :catch_b8
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_8e} :catch_d8

    #@8e
    .line 1008
    :cond_8e
    :goto_8e
    if-eqz v6, :cond_29

    #@90
    .line 1009
    invoke-virtual {v4, v6, v5}, Lcom/android/i18n/phonenumbers/geocoding/PhoneNumberOfflineGeocoder;->getDescriptionForNumber(Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;Ljava/util/Locale;)Ljava/lang/String;

    #@93
    move-result-object v1

    #@94
    .line 1010
    .local v1, description:Ljava/lang/String;
    sget-boolean v8, Lcom/android/internal/telephony/CallerInfo;->VDBG:Z

    #@96
    if-eqz v8, :cond_29

    #@98
    const-string v8, "CallerInfo"

    #@9a
    new-instance v9, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v10, "- got description: \'"

    #@a1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v9

    #@a5
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v9

    #@a9
    const-string v10, "\'"

    #@ab
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v9

    #@af
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v9

    #@b3
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto/16 :goto_29

    #@b8
    .line 1002
    .end local v1           #description:Ljava/lang/String;
    :catch_b8
    move-exception v2

    #@b9
    .line 1003
    .local v2, e:Lcom/android/i18n/phonenumbers/NumberParseException;
    const-string v8, "CallerInfo"

    #@bb
    new-instance v9, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v10, "getGeoDescription: NumberParseException for incoming number \'"

    #@c2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v9

    #@c6
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v9

    #@ca
    const-string v10, "\'"

    #@cc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v9

    #@d0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v9

    #@d4
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    goto :goto_8e

    #@d8
    .line 1004
    .end local v2           #e:Lcom/android/i18n/phonenumbers/NumberParseException;
    :catch_d8
    move-exception v3

    #@d9
    .line 1005
    .local v3, ex:Ljava/lang/Exception;
    const-string v8, "CallerInfo"

    #@db
    new-instance v9, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v10, "Exception Caught"

    #@e2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v9

    #@e6
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v9

    #@ea
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ed
    move-result-object v9

    #@ee
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f1
    goto :goto_8e
.end method

.method private static normalize(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 870
    if-eqz p0, :cond_8

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    if-lez v0, :cond_9

    #@8
    .line 873
    .end local p0
    :cond_8
    :goto_8
    return-object p0

    #@9
    .restart local p0
    :cond_9
    const/4 p0, 0x0

    #@a
    goto :goto_8
.end method

.method private setContactExistsCount(I)V
    .registers 2
    .parameter "count"

    #@0
    .prologue
    .line 879
    iput p1, p0, Lcom/android/internal/telephony/CallerInfo;->contactExistsCount:I

    #@2
    .line 880
    return-void
.end method


# virtual methods
.method public addUserData(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter "key"
    .parameter "data"

    #@0
    .prologue
    .line 1152
    iget-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->mMapUserData:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1153
    new-instance v0, Ljava/util/HashMap;

    #@6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->mMapUserData:Ljava/util/HashMap;

    #@b
    .line 1154
    :cond_b
    iget-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->mMapUserData:Ljava/util/HashMap;

    #@d
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public getContactExistsCount()I
    .registers 2

    #@0
    .prologue
    .line 882
    iget v0, p0, Lcom/android/internal/telephony/CallerInfo;->contactExistsCount:I

    #@2
    return v0
.end method

.method public getUserData(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 1164
    iget-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->mMapUserData:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 1165
    const/4 v0, 0x0

    #@5
    .line 1166
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->mMapUserData:Ljava/util/HashMap;

    #@8
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public isEmergencyNumber()Z
    .registers 2

    #@0
    .prologue
    .line 808
    iget-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsEmergency:Z

    #@2
    return v0
.end method

.method public isN11OrSpecialNumber()Z
    .registers 2

    #@0
    .prologue
    .line 1184
    iget-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsN11OrSpecial:Z

    #@2
    return v0
.end method

.method public isVoiceMailNumber()Z
    .registers 2

    #@0
    .prologue
    .line 815
    iget-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsVoiceMail:Z

    #@2
    return v0
.end method

.method markAsEmergency(Landroid/content/Context;)Lcom/android/internal/telephony/CallerInfo;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 831
    const v0, 0x104030a

    #@3
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@9
    .line 833
    const v0, 0x1080466

    #@c
    iput v0, p0, Lcom/android/internal/telephony/CallerInfo;->photoResource:I

    #@e
    .line 834
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsEmergency:Z

    #@11
    .line 835
    return-object p0
.end method

.method markAsN11OrSpecial(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/CallerInfo;
    .registers 4
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    .line 1188
    invoke-static {p1, p2}, Landroid/telephony/PhoneNumberUtils;->getN11OrSpecialNumberString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@6
    .line 1189
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallerInfo;->mIsN11OrSpecial:Z

    #@9
    .line 1190
    return-object p0
.end method

.method markAsVoiceMail()Lcom/android/internal/telephony/CallerInfo;
    .registers 5

    #@0
    .prologue
    .line 849
    const/4 v2, 0x1

    #@1
    iput-boolean v2, p0, Lcom/android/internal/telephony/CallerInfo;->mIsVoiceMail:Z

    #@3
    .line 852
    :try_start_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getVoiceMailAlphaTag()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 854
    .local v1, voiceMailLabel:Ljava/lang/String;
    iput-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_d} :catch_e

    #@d
    .line 866
    .end local v1           #voiceMailLabel:Ljava/lang/String;
    :goto_d
    return-object p0

    #@e
    .line 855
    :catch_e
    move-exception v0

    #@f
    .line 861
    .local v0, se:Ljava/lang/SecurityException;
    const-string v2, "CallerInfo"

    #@11
    const-string v3, "Cannot access VoiceMail."

    #@13
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public removeUserData(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 1176
    iget-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->mMapUserData:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 1177
    const/4 v0, 0x0

    #@5
    .line 1178
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->mMapUserData:Ljava/util/HashMap;

    #@8
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1041
    const/4 v0, 0x0

    #@1
    .line 1077
    .local v0, VERBOSE_DEBUG:Z
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    const/16 v2, 0x80

    #@5
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " { "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string/jumbo v3, "name "

    #@2b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    iget-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@31
    if-nez v1, :cond_6b

    #@33
    const-string/jumbo v1, "null"

    #@36
    :goto_36
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    new-instance v1, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v3, ", phoneNumber "

    #@49
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    iget-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@4f
    if-nez v1, :cond_6f

    #@51
    const-string/jumbo v1, "null"

    #@54
    :goto_54
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    const-string v2, " }"

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    return-object v1

    #@6b
    :cond_6b
    const-string/jumbo v1, "non-null"

    #@6e
    goto :goto_36

    #@6f
    :cond_6f
    const-string/jumbo v1, "non-null"

    #@72
    goto :goto_54
.end method

.method public updateGeoDescription(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "fallbackNumber"

    #@0
    .prologue
    .line 976
    iget-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_10

    #@8
    move-object v0, p2

    #@9
    .line 977
    .local v0, number:Ljava/lang/String;
    :goto_9
    invoke-static {p1, v0}, Lcom/android/internal/telephony/CallerInfo;->getGeoDescription(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    iput-object v1, p0, Lcom/android/internal/telephony/CallerInfo;->geoDescription:Ljava/lang/String;

    #@f
    .line 978
    return-void

    #@10
    .line 976
    .end local v0           #number:Ljava/lang/String;
    :cond_10
    iget-object v0, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@12
    goto :goto_9
.end method
