.class Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;
.super Ljava/lang/Object;
.source "ITelephonyRegistryMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/ITelephonyRegistryMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 229
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 230
    iput-object p1, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 231
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 238
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@2
    return-object v0
.end method

.method public listen(Ljava/lang/String;Lcom/android/internal/telephony/IPhoneStateListener;IZI)V
    .registers 11
    .parameter "pkg"
    .parameter "callback"
    .parameter "events"
    .parameter "notifyNow"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 242
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 243
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 245
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 246
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 247
    if-eqz p2, :cond_37

    #@14
    invoke-interface {p2}, Lcom/android/internal/telephony/IPhoneStateListener;->asBinder()Landroid/os/IBinder;

    #@17
    move-result-object v4

    #@18
    :goto_18
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1b
    .line 248
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 249
    if-eqz p4, :cond_39

    #@20
    :goto_20
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 250
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 251
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/4 v3, 0x1

    #@29
    const/4 v4, 0x0

    #@2a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 252
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_30
    .catchall {:try_start_a .. :try_end_30} :catchall_3b

    #@30
    .line 255
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 256
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 258
    return-void

    #@37
    .line 247
    :cond_37
    const/4 v4, 0x0

    #@38
    goto :goto_18

    #@39
    :cond_39
    move v2, v3

    #@3a
    .line 249
    goto :goto_20

    #@3b
    .line 255
    :catchall_3b
    move-exception v2

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 256
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v2
.end method

.method public notifyCallForwardingChanged(ZI)V
    .registers 8
    .parameter "cfi"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 338
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 339
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 341
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 342
    if-eqz p1, :cond_11

    #@10
    const/4 v2, 0x1

    #@11
    :cond_11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 343
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 344
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x6

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 345
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_9 .. :try_end_21} :catchall_28

    #@21
    .line 348
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 349
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 351
    return-void

    #@28
    .line 348
    :catchall_28
    move-exception v2

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 349
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v2
.end method

.method public notifyCallState(ILjava/lang/String;I)V
    .registers 9
    .parameter "state"
    .parameter "incomingNumber"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 261
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 262
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 264
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 265
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 266
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 267
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 268
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x2

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 269
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_27

    #@20
    .line 272
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 273
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 275
    return-void

    #@27
    .line 272
    :catchall_27
    move-exception v2

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 273
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v2
.end method

.method public notifyCellInfo(Ljava/util/List;I)V
    .registers 8
    .parameter
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 464
    .local p1, cellInfo:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 465
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 467
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 468
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@10
    .line 469
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 470
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0xc

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 471
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 474
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 475
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 477
    return-void

    #@25
    .line 474
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 475
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public notifyCellLocation(Landroid/os/Bundle;I)V
    .registers 8
    .parameter "cellLocation"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 427
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 428
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 430
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 431
    if-eqz p1, :cond_2c

    #@f
    .line 432
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 433
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 438
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 439
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v3, 0xa

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 440
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_31

    #@25
    .line 443
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 444
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 446
    return-void

    #@2c
    .line 436
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 443
    :catchall_31
    move-exception v2

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 444
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v2
.end method

.method public notifyDataActivity(I)V
    .registers 7
    .parameter "state"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 354
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 355
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 357
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 358
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 359
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x7

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 360
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 363
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 364
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 366
    return-void

    #@21
    .line 363
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 364
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public notifyDataConnection(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;IZI)V
    .registers 16
    .parameter "state"
    .parameter "isDataConnectivityPossible"
    .parameter "reason"
    .parameter "apn"
    .parameter "apnType"
    .parameter "linkProperties"
    .parameter "linkCapabilities"
    .parameter "networkType"
    .parameter "roaming"
    .parameter "smCause"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 370
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 371
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 373
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 374
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 375
    if-eqz p2, :cond_52

    #@14
    move v4, v2

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 376
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 377
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 378
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@21
    .line 379
    if-eqz p6, :cond_54

    #@23
    .line 380
    const/4 v4, 0x1

    #@24
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 381
    const/4 v4, 0x0

    #@28
    invoke-virtual {p6, v0, v4}, Landroid/net/LinkProperties;->writeToParcel(Landroid/os/Parcel;I)V

    #@2b
    .line 386
    :goto_2b
    if-eqz p7, :cond_61

    #@2d
    .line 387
    const/4 v4, 0x1

    #@2e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 388
    const/4 v4, 0x0

    #@32
    invoke-virtual {p7, v0, v4}, Landroid/net/LinkCapabilities;->writeToParcel(Landroid/os/Parcel;I)V

    #@35
    .line 393
    :goto_35
    invoke-virtual {v0, p8}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 394
    if-eqz p9, :cond_66

    #@3a
    :goto_3a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 395
    invoke-virtual {v0, p10}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 396
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@42
    const/16 v3, 0x8

    #@44
    const/4 v4, 0x0

    #@45
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@48
    .line 397
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_4b
    .catchall {:try_start_a .. :try_end_4b} :catchall_59

    #@4b
    .line 400
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4e
    .line 401
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@51
    .line 403
    return-void

    #@52
    :cond_52
    move v4, v3

    #@53
    .line 375
    goto :goto_15

    #@54
    .line 384
    :cond_54
    const/4 v4, 0x0

    #@55
    :try_start_55
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_58
    .catchall {:try_start_55 .. :try_end_58} :catchall_59

    #@58
    goto :goto_2b

    #@59
    .line 400
    :catchall_59
    move-exception v2

    #@5a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@5d
    .line 401
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@60
    throw v2

    #@61
    .line 391
    :cond_61
    const/4 v4, 0x0

    #@62
    :try_start_62
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_59

    #@65
    goto :goto_35

    #@66
    :cond_66
    move v2, v3

    #@67
    .line 394
    goto :goto_3a
.end method

.method public notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "reason"
    .parameter "apnType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 411
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 412
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 414
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 415
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 416
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 417
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x9

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 418
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 421
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 422
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 424
    return-void

    #@25
    .line 421
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 422
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public notifyMessageWaitingChanged(ZI)V
    .registers 8
    .parameter "mwi"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 322
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 323
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 325
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 326
    if-eqz p1, :cond_11

    #@10
    const/4 v2, 0x1

    #@11
    :cond_11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 327
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 328
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x5

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 329
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_9 .. :try_end_21} :catchall_28

    #@21
    .line 332
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 333
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 335
    return-void

    #@28
    .line 332
    :catchall_28
    move-exception v2

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 333
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v2
.end method

.method public notifyOtaspChanged(I)V
    .registers 7
    .parameter "otaspMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 449
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 450
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 452
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 453
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 454
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0xb

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 455
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 458
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 459
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 461
    return-void

    #@22
    .line 458
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 459
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public notifyServiceState(Landroid/telephony/ServiceState;I)V
    .registers 8
    .parameter "state"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 278
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 279
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 281
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 282
    if-eqz p1, :cond_2b

    #@f
    .line 283
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 284
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/telephony/ServiceState;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 289
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 290
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v3, 0x3

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 291
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_30

    #@24
    .line 294
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 297
    return-void

    #@2b
    .line 287
    :cond_2b
    const/4 v2, 0x0

    #@2c
    :try_start_2c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2f
    .catchall {:try_start_2c .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_17

    #@30
    .line 294
    :catchall_30
    move-exception v2

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    throw v2
.end method

.method public notifySignalStrength(Landroid/telephony/SignalStrength;I)V
    .registers 8
    .parameter "signalStrength"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 300
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 301
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 303
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 304
    if-eqz p1, :cond_2b

    #@f
    .line 305
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 306
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/telephony/SignalStrength;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 311
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 312
    iget-object v2, p0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v3, 0x4

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 313
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_30

    #@24
    .line 316
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 317
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 319
    return-void

    #@2b
    .line 309
    :cond_2b
    const/4 v2, 0x0

    #@2c
    :try_start_2c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2f
    .catchall {:try_start_2c .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_17

    #@30
    .line 316
    :catchall_30
    move-exception v2

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 317
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    throw v2
.end method
