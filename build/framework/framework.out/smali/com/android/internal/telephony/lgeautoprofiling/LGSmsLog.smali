.class public Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;
.super Ljava/lang/Object;
.source "LGSmsLog.java"


# static fields
.field public static final DEBUG:I = 0x2

#the value of this static final field might be set in the static constructor
.field private static final DISABLELOG:I = 0x0

.field public static final ERROR:I = 0x10

.field public static final FLOW:I = 0x20

.field public static final INFO:I = 0x4

.field public static final PRIVACY:I = 0x40

.field private static final PROPERTY_DISABLELOG:Ljava/lang/String; = "ro.gsm.sms.disablelog"

.field private static final TAG_DEBUG:Ljava/lang/String; = "[SMS_LD]"

.field private static final TAG_ERROR:Ljava/lang/String; = "[SMS_LE]"

.field private static final TAG_FLOW:Ljava/lang/String; = "[SMS_LF]"

.field private static final TAG_INFO:Ljava/lang/String; = "[SMS_LI]"

.field private static final TAG_PRIVACY:Ljava/lang/String; = "[SMS_LP]"

.field private static final TAG_VERBOSE:Ljava/lang/String; = "[SMS_LV]"

.field private static final TAG_WARN:Ljava/lang/String; = "[SMS_LW]"

.field public static final VERBOSE:I = 0x1

.field public static final WARN:I = 0x8


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 88
    const-string/jumbo v0, "ro.gsm.sms.disablelog"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    sput v0, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->DISABLELOG:I

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static d(Ljava/lang/String;)I
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 122
    if-nez p0, :cond_4

    #@3
    .line 129
    :cond_3
    :goto_3
    return v0

    #@4
    .line 126
    :cond_4
    const/4 v1, 0x2

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3

    #@b
    .line 127
    const-string v0, "[SMS_LD]"

    #@d
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    goto :goto_3
.end method

.method public static d(Ljava/lang/String;Ljava/lang/Throwable;)I
    .registers 4
    .parameter "msg"
    .parameter "tr"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 133
    if-nez p0, :cond_4

    #@3
    .line 140
    :cond_3
    :goto_3
    return v0

    #@4
    .line 137
    :cond_4
    const/4 v1, 0x2

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3

    #@b
    .line 138
    const-string v0, "[SMS_LD]"

    #@d
    invoke-static {v0, p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    move-result v0

    #@11
    goto :goto_3
.end method

.method public static e(Ljava/lang/String;)I
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 188
    if-nez p0, :cond_4

    #@3
    .line 195
    :cond_3
    :goto_3
    return v0

    #@4
    .line 192
    :cond_4
    const/16 v1, 0x10

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_3

    #@c
    .line 193
    const-string v0, "[SMS_LW]"

    #@e
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    goto :goto_3
.end method

.method public static e(Ljava/lang/String;Ljava/lang/Throwable;)I
    .registers 4
    .parameter "msg"
    .parameter "tr"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 199
    if-nez p0, :cond_4

    #@3
    .line 206
    :cond_3
    :goto_3
    return v0

    #@4
    .line 203
    :cond_4
    const/16 v1, 0x10

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_3

    #@c
    .line 204
    const-string v0, "[SMS_LE]"

    #@e
    invoke-static {v0, p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    move-result v0

    #@12
    goto :goto_3
.end method

.method public static f(Ljava/lang/String;)I
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 211
    if-nez p0, :cond_4

    #@3
    .line 218
    :cond_3
    :goto_3
    return v0

    #@4
    .line 215
    :cond_4
    const/16 v1, 0x20

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_3

    #@c
    .line 216
    const-string v0, "[SMS_LF]"

    #@e
    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    goto :goto_3
.end method

.method public static i(Ljava/lang/String;)I
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 144
    if-nez p0, :cond_4

    #@3
    .line 151
    :cond_3
    :goto_3
    return v0

    #@4
    .line 148
    :cond_4
    const/4 v1, 0x4

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3

    #@b
    .line 149
    const-string v0, "[SMS_LI]"

    #@d
    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    goto :goto_3
.end method

.method public static i(Ljava/lang/String;Ljava/lang/Throwable;)I
    .registers 4
    .parameter "msg"
    .parameter "tr"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 155
    if-nez p0, :cond_4

    #@3
    .line 162
    :cond_3
    :goto_3
    return v0

    #@4
    .line 159
    :cond_4
    const/4 v1, 0x4

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3

    #@b
    .line 160
    const-string v0, "[SMS_LI]"

    #@d
    invoke-static {v0, p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    move-result v0

    #@11
    goto :goto_3
.end method

.method public static isLoggable(I)Z
    .registers 2
    .parameter "priority"

    #@0
    .prologue
    .line 92
    sget v0, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->DISABLELOG:I

    #@2
    and-int/2addr v0, p0

    #@3
    if-nez v0, :cond_7

    #@5
    .line 93
    const/4 v0, 0x1

    #@6
    .line 96
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public static p(Ljava/lang/String;)I
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 222
    if-nez p0, :cond_4

    #@3
    .line 229
    :cond_3
    :goto_3
    return v0

    #@4
    .line 226
    :cond_4
    const/16 v1, 0x40

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_3

    #@c
    .line 227
    const-string v0, "[SMS_LP]"

    #@e
    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    goto :goto_3
.end method

.method public static v(Ljava/lang/String;)I
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 100
    if-nez p0, :cond_4

    #@3
    .line 107
    :cond_3
    :goto_3
    return v0

    #@4
    .line 104
    :cond_4
    const/4 v1, 0x1

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3

    #@b
    .line 105
    const-string v0, "[SMS_LV]"

    #@d
    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    goto :goto_3
.end method

.method public static v(Ljava/lang/String;Ljava/lang/Throwable;)I
    .registers 4
    .parameter "msg"
    .parameter "tr"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 111
    if-nez p0, :cond_4

    #@3
    .line 118
    :cond_3
    :goto_3
    return v0

    #@4
    .line 115
    :cond_4
    const/4 v1, 0x1

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3

    #@b
    .line 116
    const-string v0, "[SMS_LV]"

    #@d
    invoke-static {v0, p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    move-result v0

    #@11
    goto :goto_3
.end method

.method public static w(Ljava/lang/String;)I
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 166
    if-nez p0, :cond_4

    #@3
    .line 173
    :cond_3
    :goto_3
    return v0

    #@4
    .line 170
    :cond_4
    const/16 v1, 0x8

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_3

    #@c
    .line 171
    const-string v0, "[SMS_LW]"

    #@e
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    goto :goto_3
.end method

.method public static w(Ljava/lang/String;Ljava/lang/Throwable;)I
    .registers 4
    .parameter "msg"
    .parameter "tr"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 177
    if-nez p0, :cond_4

    #@3
    .line 184
    :cond_3
    :goto_3
    return v0

    #@4
    .line 181
    :cond_4
    const/16 v1, 0x8

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->isLoggable(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_3

    #@c
    .line 182
    const-string v0, "[SMS_LW]"

    #@e
    invoke-static {v0, p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    move-result v0

    #@12
    goto :goto_3
.end method
