.class public abstract Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;
.super Landroid/os/Binder;
.source "ITelephonyMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/msim/ITelephonyMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/msim/ITelephonyMSim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.msim.ITelephonyMSim"

.field static final TRANSACTION_answerRingingCall:I = 0x6

.field static final TRANSACTION_call:I = 0x2

.field static final TRANSACTION_cancelMissedCallsNotification:I = 0xd

.field static final TRANSACTION_dial:I = 0x1

.field static final TRANSACTION_disableApnType:I = 0x15

.field static final TRANSACTION_disableDataConnectivity:I = 0x17

.field static final TRANSACTION_enableApnType:I = 0x14

.field static final TRANSACTION_enableDataConnectivity:I = 0x16

.field static final TRANSACTION_endCall:I = 0x5

.field static final TRANSACTION_getActivePhoneType:I = 0x1c

.field static final TRANSACTION_getAllCellInfo:I = 0x25

.field static final TRANSACTION_getCallState:I = 0x19

.field static final TRANSACTION_getCdmaEriIconIndex:I = 0x1d

.field static final TRANSACTION_getCdmaEriIconMode:I = 0x1e

.field static final TRANSACTION_getCdmaEriText:I = 0x1f

.field static final TRANSACTION_getDataActivity:I = 0x1a

.field static final TRANSACTION_getDataState:I = 0x1b

.field static final TRANSACTION_getDefaultSubscription:I = 0x26

.field static final TRANSACTION_getIccPin1RetryCount:I = 0x2a

.field static final TRANSACTION_getIccPin2RetryCount:I = 0x2b

.field static final TRANSACTION_getIccPuk1RetryCount:I = 0x2c

.field static final TRANSACTION_getIccPuk2RetryCount:I = 0x2d

.field static final TRANSACTION_getLteOnCdmaMode:I = 0x24

.field static final TRANSACTION_getNetworkType:I = 0x22

.field static final TRANSACTION_getPreferredDataSubscription:I = 0x28

.field static final TRANSACTION_getPreferredVoiceSubscription:I = 0x27

.field static final TRANSACTION_getVoiceMessageCount:I = 0x21

.field static final TRANSACTION_handlePinMmi:I = 0x10

.field static final TRANSACTION_hasIccCard:I = 0x23

.field static final TRANSACTION_isDataConnectivityPossible:I = 0x18

.field static final TRANSACTION_isIdle:I = 0xa

.field static final TRANSACTION_isOffhook:I = 0x8

.field static final TRANSACTION_isRadioOn:I = 0xb

.field static final TRANSACTION_isRinging:I = 0x9

.field static final TRANSACTION_isSimPinEnabled:I = 0xc

.field static final TRANSACTION_needsOtaServiceProvisioning:I = 0x20

.field static final TRANSACTION_setPreferredDataSubscription:I = 0x29

.field static final TRANSACTION_setRadio:I = 0x12

.field static final TRANSACTION_showCallScreen:I = 0x3

.field static final TRANSACTION_showCallScreenWithDialpad:I = 0x4

.field static final TRANSACTION_silenceRinger:I = 0x7

.field static final TRANSACTION_supplyPin:I = 0xe

.field static final TRANSACTION_supplyPuk:I = 0xf

.field static final TRANSACTION_toggleRadioOnOff:I = 0x11

.field static final TRANSACTION_updateServiceLocation:I = 0x13


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 22
    const-string v0, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ITelephonyMSim;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 30
    if-nez p0, :cond_4

    #@2
    .line 31
    const/4 v0, 0x0

    #@3
    .line 37
    :goto_3
    return-object v0

    #@4
    .line 33
    :cond_4
    const-string v1, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 34
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 35
    check-cast v0, Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@12
    goto :goto_3

    #@13
    .line 37
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 45
    sparse-switch p1, :sswitch_data_3bc

    #@5
    .line 478
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 49
    :sswitch_a
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 54
    :sswitch_10
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 58
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v1

    #@1d
    .line 59
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->dial(Ljava/lang/String;I)V

    #@20
    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    goto :goto_9

    #@24
    .line 65
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_24
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@26
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 69
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v1

    #@31
    .line 70
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->call(Ljava/lang/String;I)V

    #@34
    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37
    goto :goto_9

    #@38
    .line 76
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_38
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@3a
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 77
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->showCallScreen()Z

    #@40
    move-result v3

    #@41
    .line 78
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44
    .line 79
    if-eqz v3, :cond_47

    #@46
    move v5, v6

    #@47
    :cond_47
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@4a
    goto :goto_9

    #@4b
    .line 84
    .end local v3           #_result:Z
    :sswitch_4b
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@4d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v7

    #@54
    if-eqz v7, :cond_65

    #@56
    move v0, v6

    #@57
    .line 87
    .local v0, _arg0:Z
    :goto_57
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->showCallScreenWithDialpad(Z)Z

    #@5a
    move-result v3

    #@5b
    .line 88
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    .line 89
    if-eqz v3, :cond_61

    #@60
    move v5, v6

    #@61
    :cond_61
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@64
    goto :goto_9

    #@65
    .end local v0           #_arg0:Z
    .end local v3           #_result:Z
    :cond_65
    move v0, v5

    #@66
    .line 86
    goto :goto_57

    #@67
    .line 94
    :sswitch_67
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@69
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v0

    #@70
    .line 97
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->endCall(I)Z

    #@73
    move-result v3

    #@74
    .line 98
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@77
    .line 99
    if-eqz v3, :cond_7a

    #@79
    move v5, v6

    #@7a
    :cond_7a
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@7d
    goto :goto_9

    #@7e
    .line 104
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_7e
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@80
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@83
    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@86
    move-result v0

    #@87
    .line 107
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->answerRingingCall(I)V

    #@8a
    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d
    goto/16 :goto_9

    #@8f
    .line 113
    .end local v0           #_arg0:I
    :sswitch_8f
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@91
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@94
    .line 114
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->silenceRinger()V

    #@97
    .line 115
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9a
    goto/16 :goto_9

    #@9c
    .line 120
    :sswitch_9c
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@9e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1
    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a4
    move-result v0

    #@a5
    .line 123
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->isOffhook(I)Z

    #@a8
    move-result v3

    #@a9
    .line 124
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ac
    .line 125
    if-eqz v3, :cond_af

    #@ae
    move v5, v6

    #@af
    :cond_af
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@b2
    goto/16 :goto_9

    #@b4
    .line 130
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_b4
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@b6
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b9
    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@bc
    move-result v0

    #@bd
    .line 133
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->isRinging(I)Z

    #@c0
    move-result v3

    #@c1
    .line 134
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c4
    .line 135
    if-eqz v3, :cond_c7

    #@c6
    move v5, v6

    #@c7
    :cond_c7
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@ca
    goto/16 :goto_9

    #@cc
    .line 140
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_cc
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@ce
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d1
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d4
    move-result v0

    #@d5
    .line 143
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->isIdle(I)Z

    #@d8
    move-result v3

    #@d9
    .line 144
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@dc
    .line 145
    if-eqz v3, :cond_df

    #@de
    move v5, v6

    #@df
    :cond_df
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@e2
    goto/16 :goto_9

    #@e4
    .line 150
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_e4
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@e6
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e9
    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ec
    move-result v0

    #@ed
    .line 153
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->isRadioOn(I)Z

    #@f0
    move-result v3

    #@f1
    .line 154
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f4
    .line 155
    if-eqz v3, :cond_f7

    #@f6
    move v5, v6

    #@f7
    :cond_f7
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@fa
    goto/16 :goto_9

    #@fc
    .line 160
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_fc
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@fe
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@101
    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@104
    move-result v0

    #@105
    .line 163
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->isSimPinEnabled(I)Z

    #@108
    move-result v3

    #@109
    .line 164
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10c
    .line 165
    if-eqz v3, :cond_10f

    #@10e
    move v5, v6

    #@10f
    :cond_10f
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@112
    goto/16 :goto_9

    #@114
    .line 170
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_114
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@116
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@119
    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11c
    move-result v0

    #@11d
    .line 173
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->cancelMissedCallsNotification(I)V

    #@120
    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@123
    goto/16 :goto_9

    #@125
    .line 179
    .end local v0           #_arg0:I
    :sswitch_125
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@127
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12a
    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12d
    move-result-object v0

    #@12e
    .line 183
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@131
    move-result v1

    #@132
    .line 184
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->supplyPin(Ljava/lang/String;I)Z

    #@135
    move-result v3

    #@136
    .line 185
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@139
    .line 186
    if-eqz v3, :cond_13c

    #@13b
    move v5, v6

    #@13c
    :cond_13c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@13f
    goto/16 :goto_9

    #@141
    .line 191
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v3           #_result:Z
    :sswitch_141
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@143
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@146
    .line 193
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@149
    move-result-object v0

    #@14a
    .line 195
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14d
    move-result-object v1

    #@14e
    .line 197
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@151
    move-result v2

    #@152
    .line 198
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->supplyPuk(Ljava/lang/String;Ljava/lang/String;I)Z

    #@155
    move-result v3

    #@156
    .line 199
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@159
    .line 200
    if-eqz v3, :cond_15c

    #@15b
    move v5, v6

    #@15c
    :cond_15c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@15f
    goto/16 :goto_9

    #@161
    .line 205
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:I
    .end local v3           #_result:Z
    :sswitch_161
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@163
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@166
    .line 207
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@169
    move-result-object v0

    #@16a
    .line 209
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16d
    move-result v1

    #@16e
    .line 210
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->handlePinMmi(Ljava/lang/String;I)Z

    #@171
    move-result v3

    #@172
    .line 211
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@175
    .line 212
    if-eqz v3, :cond_178

    #@177
    move v5, v6

    #@178
    :cond_178
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@17b
    goto/16 :goto_9

    #@17d
    .line 217
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v3           #_result:Z
    :sswitch_17d
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@17f
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@182
    .line 219
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@185
    move-result v0

    #@186
    .line 220
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->toggleRadioOnOff(I)V

    #@189
    .line 221
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@18c
    goto/16 :goto_9

    #@18e
    .line 226
    .end local v0           #_arg0:I
    :sswitch_18e
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@190
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@193
    .line 228
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@196
    move-result v7

    #@197
    if-eqz v7, :cond_1ad

    #@199
    move v0, v6

    #@19a
    .line 230
    .local v0, _arg0:Z
    :goto_19a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19d
    move-result v1

    #@19e
    .line 231
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->setRadio(ZI)Z

    #@1a1
    move-result v3

    #@1a2
    .line 232
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a5
    .line 233
    if-eqz v3, :cond_1a8

    #@1a7
    move v5, v6

    #@1a8
    :cond_1a8
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1ab
    goto/16 :goto_9

    #@1ad
    .end local v0           #_arg0:Z
    .end local v1           #_arg1:I
    .end local v3           #_result:Z
    :cond_1ad
    move v0, v5

    #@1ae
    .line 228
    goto :goto_19a

    #@1af
    .line 238
    :sswitch_1af
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@1b1
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b4
    .line 240
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b7
    move-result v0

    #@1b8
    .line 241
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->updateServiceLocation(I)V

    #@1bb
    .line 242
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1be
    goto/16 :goto_9

    #@1c0
    .line 247
    .end local v0           #_arg0:I
    :sswitch_1c0
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@1c2
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c5
    .line 249
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c8
    move-result-object v0

    #@1c9
    .line 250
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->enableApnType(Ljava/lang/String;)I

    #@1cc
    move-result v3

    #@1cd
    .line 251
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d0
    .line 252
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d3
    goto/16 :goto_9

    #@1d5
    .line 257
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_1d5
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@1d7
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1da
    .line 259
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1dd
    move-result-object v0

    #@1de
    .line 260
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->disableApnType(Ljava/lang/String;)I

    #@1e1
    move-result v3

    #@1e2
    .line 261
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e5
    .line 262
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1e8
    goto/16 :goto_9

    #@1ea
    .line 267
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_1ea
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@1ec
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ef
    .line 268
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->enableDataConnectivity()Z

    #@1f2
    move-result v3

    #@1f3
    .line 269
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f6
    .line 270
    if-eqz v3, :cond_1f9

    #@1f8
    move v5, v6

    #@1f9
    :cond_1f9
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1fc
    goto/16 :goto_9

    #@1fe
    .line 275
    .end local v3           #_result:Z
    :sswitch_1fe
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@200
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@203
    .line 276
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->disableDataConnectivity()Z

    #@206
    move-result v3

    #@207
    .line 277
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20a
    .line 278
    if-eqz v3, :cond_20d

    #@20c
    move v5, v6

    #@20d
    :cond_20d
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@210
    goto/16 :goto_9

    #@212
    .line 283
    .end local v3           #_result:Z
    :sswitch_212
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@214
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@217
    .line 284
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->isDataConnectivityPossible()Z

    #@21a
    move-result v3

    #@21b
    .line 285
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21e
    .line 286
    if-eqz v3, :cond_221

    #@220
    move v5, v6

    #@221
    :cond_221
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@224
    goto/16 :goto_9

    #@226
    .line 291
    .end local v3           #_result:Z
    :sswitch_226
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@228
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22b
    .line 293
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@22e
    move-result v0

    #@22f
    .line 294
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getCallState(I)I

    #@232
    move-result v3

    #@233
    .line 295
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@236
    .line 296
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@239
    goto/16 :goto_9

    #@23b
    .line 301
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_23b
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@23d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@240
    .line 302
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getDataActivity()I

    #@243
    move-result v3

    #@244
    .line 303
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@247
    .line 304
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@24a
    goto/16 :goto_9

    #@24c
    .line 309
    .end local v3           #_result:I
    :sswitch_24c
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@24e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@251
    .line 310
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getDataState()I

    #@254
    move-result v3

    #@255
    .line 311
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@258
    .line 312
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@25b
    goto/16 :goto_9

    #@25d
    .line 317
    .end local v3           #_result:I
    :sswitch_25d
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@25f
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@262
    .line 319
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@265
    move-result v0

    #@266
    .line 320
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getActivePhoneType(I)I

    #@269
    move-result v3

    #@26a
    .line 321
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26d
    .line 322
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@270
    goto/16 :goto_9

    #@272
    .line 327
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_272
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@274
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@277
    .line 329
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27a
    move-result v0

    #@27b
    .line 330
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getCdmaEriIconIndex(I)I

    #@27e
    move-result v3

    #@27f
    .line 331
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@282
    .line 332
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@285
    goto/16 :goto_9

    #@287
    .line 337
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_287
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@289
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28c
    .line 339
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28f
    move-result v0

    #@290
    .line 340
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getCdmaEriIconMode(I)I

    #@293
    move-result v3

    #@294
    .line 341
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@297
    .line 342
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@29a
    goto/16 :goto_9

    #@29c
    .line 347
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_29c
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@29e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a1
    .line 349
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2a4
    move-result v0

    #@2a5
    .line 350
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getCdmaEriText(I)Ljava/lang/String;

    #@2a8
    move-result-object v3

    #@2a9
    .line 351
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ac
    .line 352
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2af
    goto/16 :goto_9

    #@2b1
    .line 357
    .end local v0           #_arg0:I
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_2b1
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@2b3
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b6
    .line 358
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->needsOtaServiceProvisioning()Z

    #@2b9
    move-result v3

    #@2ba
    .line 359
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2bd
    .line 360
    if-eqz v3, :cond_2c0

    #@2bf
    move v5, v6

    #@2c0
    :cond_2c0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2c3
    goto/16 :goto_9

    #@2c5
    .line 365
    .end local v3           #_result:Z
    :sswitch_2c5
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@2c7
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ca
    .line 367
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2cd
    move-result v0

    #@2ce
    .line 368
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getVoiceMessageCount(I)I

    #@2d1
    move-result v3

    #@2d2
    .line 369
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d5
    .line 370
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2d8
    goto/16 :goto_9

    #@2da
    .line 375
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_2da
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@2dc
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2df
    .line 377
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2e2
    move-result v0

    #@2e3
    .line 378
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getNetworkType(I)I

    #@2e6
    move-result v3

    #@2e7
    .line 379
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ea
    .line 380
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2ed
    goto/16 :goto_9

    #@2ef
    .line 385
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_2ef
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@2f1
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f4
    .line 387
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2f7
    move-result v0

    #@2f8
    .line 388
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->hasIccCard(I)Z

    #@2fb
    move-result v3

    #@2fc
    .line 389
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ff
    .line 390
    if-eqz v3, :cond_302

    #@301
    move v5, v6

    #@302
    :cond_302
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@305
    goto/16 :goto_9

    #@307
    .line 395
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_307
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@309
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30c
    .line 397
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30f
    move-result v0

    #@310
    .line 398
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getLteOnCdmaMode(I)I

    #@313
    move-result v3

    #@314
    .line 399
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@317
    .line 400
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@31a
    goto/16 :goto_9

    #@31c
    .line 405
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_31c
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@31e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@321
    .line 406
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getAllCellInfo()Ljava/util/List;

    #@324
    move-result-object v4

    #@325
    .line 407
    .local v4, _result:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@328
    .line 408
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@32b
    goto/16 :goto_9

    #@32d
    .line 413
    .end local v4           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    :sswitch_32d
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@32f
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@332
    .line 414
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getDefaultSubscription()I

    #@335
    move-result v3

    #@336
    .line 415
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@339
    .line 416
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@33c
    goto/16 :goto_9

    #@33e
    .line 421
    .end local v3           #_result:I
    :sswitch_33e
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@340
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@343
    .line 422
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getPreferredVoiceSubscription()I

    #@346
    move-result v3

    #@347
    .line 423
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34a
    .line 424
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@34d
    goto/16 :goto_9

    #@34f
    .line 429
    .end local v3           #_result:I
    :sswitch_34f
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@351
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@354
    .line 430
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getPreferredDataSubscription()I

    #@357
    move-result v3

    #@358
    .line 431
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@35b
    .line 432
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@35e
    goto/16 :goto_9

    #@360
    .line 437
    .end local v3           #_result:I
    :sswitch_360
    const-string v7, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@362
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@365
    .line 439
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@368
    move-result v0

    #@369
    .line 440
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->setPreferredDataSubscription(I)Z

    #@36c
    move-result v3

    #@36d
    .line 441
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@370
    .line 442
    if-eqz v3, :cond_373

    #@372
    move v5, v6

    #@373
    :cond_373
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@376
    goto/16 :goto_9

    #@378
    .line 447
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_378
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@37a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@37d
    .line 448
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getIccPin1RetryCount()I

    #@380
    move-result v3

    #@381
    .line 449
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@384
    .line 450
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@387
    goto/16 :goto_9

    #@389
    .line 455
    .end local v3           #_result:I
    :sswitch_389
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@38b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38e
    .line 456
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getIccPin2RetryCount()I

    #@391
    move-result v3

    #@392
    .line 457
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@395
    .line 458
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@398
    goto/16 :goto_9

    #@39a
    .line 463
    .end local v3           #_result:I
    :sswitch_39a
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@39c
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@39f
    .line 464
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getIccPuk1RetryCount()I

    #@3a2
    move-result v3

    #@3a3
    .line 465
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a6
    .line 466
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3a9
    goto/16 :goto_9

    #@3ab
    .line 471
    .end local v3           #_result:I
    :sswitch_3ab
    const-string v5, "com.android.internal.telephony.msim.ITelephonyMSim"

    #@3ad
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b0
    .line 472
    invoke-virtual {p0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->getIccPuk2RetryCount()I

    #@3b3
    move-result v3

    #@3b4
    .line 473
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b7
    .line 474
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3ba
    goto/16 :goto_9

    #@3bc
    .line 45
    :sswitch_data_3bc
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_38
        0x4 -> :sswitch_4b
        0x5 -> :sswitch_67
        0x6 -> :sswitch_7e
        0x7 -> :sswitch_8f
        0x8 -> :sswitch_9c
        0x9 -> :sswitch_b4
        0xa -> :sswitch_cc
        0xb -> :sswitch_e4
        0xc -> :sswitch_fc
        0xd -> :sswitch_114
        0xe -> :sswitch_125
        0xf -> :sswitch_141
        0x10 -> :sswitch_161
        0x11 -> :sswitch_17d
        0x12 -> :sswitch_18e
        0x13 -> :sswitch_1af
        0x14 -> :sswitch_1c0
        0x15 -> :sswitch_1d5
        0x16 -> :sswitch_1ea
        0x17 -> :sswitch_1fe
        0x18 -> :sswitch_212
        0x19 -> :sswitch_226
        0x1a -> :sswitch_23b
        0x1b -> :sswitch_24c
        0x1c -> :sswitch_25d
        0x1d -> :sswitch_272
        0x1e -> :sswitch_287
        0x1f -> :sswitch_29c
        0x20 -> :sswitch_2b1
        0x21 -> :sswitch_2c5
        0x22 -> :sswitch_2da
        0x23 -> :sswitch_2ef
        0x24 -> :sswitch_307
        0x25 -> :sswitch_31c
        0x26 -> :sswitch_32d
        0x27 -> :sswitch_33e
        0x28 -> :sswitch_34f
        0x29 -> :sswitch_360
        0x2a -> :sswitch_378
        0x2b -> :sswitch_389
        0x2c -> :sswitch_39a
        0x2d -> :sswitch_3ab
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
