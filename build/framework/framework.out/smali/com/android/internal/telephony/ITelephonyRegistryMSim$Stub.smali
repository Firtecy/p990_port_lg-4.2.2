.class public abstract Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;
.super Landroid/os/Binder;
.source "ITelephonyRegistryMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/ITelephonyRegistryMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/ITelephonyRegistryMSim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.ITelephonyRegistryMSim"

.field static final TRANSACTION_listen:I = 0x1

.field static final TRANSACTION_notifyCallForwardingChanged:I = 0x6

.field static final TRANSACTION_notifyCallState:I = 0x2

.field static final TRANSACTION_notifyCellInfo:I = 0xc

.field static final TRANSACTION_notifyCellLocation:I = 0xa

.field static final TRANSACTION_notifyDataActivity:I = 0x7

.field static final TRANSACTION_notifyDataConnection:I = 0x8

.field static final TRANSACTION_notifyDataConnectionFailed:I = 0x9

.field static final TRANSACTION_notifyMessageWaitingChanged:I = 0x5

.field static final TRANSACTION_notifyOtaspChanged:I = 0xb

.field static final TRANSACTION_notifyServiceState:I = 0x3

.field static final TRANSACTION_notifySignalStrength:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephonyRegistryMSim;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/ITelephonyRegistryMSim;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/telephony/ITelephonyRegistryMSim;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 17
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_19a

    #@3
    .line 223
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 42
    :sswitch_8
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 43
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 47
    :sswitch_f
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    .line 51
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IPhoneStateListener;

    #@1f
    move-result-object v2

    #@20
    .line 53
    .local v2, _arg1:Lcom/android/internal/telephony/IPhoneStateListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v3

    #@24
    .line 55
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_38

    #@2a
    const/4 v4, 0x1

    #@2b
    .line 57
    .local v4, _arg3:Z
    :goto_2b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2e
    move-result v5

    #@2f
    .local v5, _arg4:I
    move-object v0, p0

    #@30
    .line 58
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->listen(Ljava/lang/String;Lcom/android/internal/telephony/IPhoneStateListener;IZI)V

    #@33
    .line 59
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@36
    .line 60
    const/4 v0, 0x1

    #@37
    goto :goto_7

    #@38
    .line 55
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:I
    :cond_38
    const/4 v4, 0x0

    #@39
    goto :goto_2b

    #@3a
    .line 64
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Lcom/android/internal/telephony/IPhoneStateListener;
    .end local v3           #_arg2:I
    :sswitch_3a
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@3c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3f
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@42
    move-result v1

    #@43
    .line 68
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    .line 70
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4a
    move-result v3

    #@4b
    .line 71
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyCallState(ILjava/lang/String;I)V

    #@4e
    .line 72
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@51
    .line 73
    const/4 v0, 0x1

    #@52
    goto :goto_7

    #@53
    .line 77
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    :sswitch_53
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@55
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58
    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_72

    #@5e
    .line 80
    sget-object v0, Landroid/telephony/ServiceState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@60
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@63
    move-result-object v1

    #@64
    check-cast v1, Landroid/telephony/ServiceState;

    #@66
    .line 86
    .local v1, _arg0:Landroid/telephony/ServiceState;
    :goto_66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v2

    #@6a
    .line 87
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyServiceState(Landroid/telephony/ServiceState;I)V

    #@6d
    .line 88
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@70
    .line 89
    const/4 v0, 0x1

    #@71
    goto :goto_7

    #@72
    .line 83
    .end local v1           #_arg0:Landroid/telephony/ServiceState;
    .end local v2           #_arg1:I
    :cond_72
    const/4 v1, 0x0

    #@73
    .restart local v1       #_arg0:Landroid/telephony/ServiceState;
    goto :goto_66

    #@74
    .line 93
    .end local v1           #_arg0:Landroid/telephony/ServiceState;
    :sswitch_74
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@76
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@79
    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7c
    move-result v0

    #@7d
    if-eqz v0, :cond_94

    #@7f
    .line 96
    sget-object v0, Landroid/telephony/SignalStrength;->CREATOR:Landroid/os/Parcelable$Creator;

    #@81
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@84
    move-result-object v1

    #@85
    check-cast v1, Landroid/telephony/SignalStrength;

    #@87
    .line 102
    .local v1, _arg0:Landroid/telephony/SignalStrength;
    :goto_87
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8a
    move-result v2

    #@8b
    .line 103
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifySignalStrength(Landroid/telephony/SignalStrength;I)V

    #@8e
    .line 104
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@91
    .line 105
    const/4 v0, 0x1

    #@92
    goto/16 :goto_7

    #@94
    .line 99
    .end local v1           #_arg0:Landroid/telephony/SignalStrength;
    .end local v2           #_arg1:I
    :cond_94
    const/4 v1, 0x0

    #@95
    .restart local v1       #_arg0:Landroid/telephony/SignalStrength;
    goto :goto_87

    #@96
    .line 109
    .end local v1           #_arg0:Landroid/telephony/SignalStrength;
    :sswitch_96
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@98
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9b
    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9e
    move-result v0

    #@9f
    if-eqz v0, :cond_af

    #@a1
    const/4 v1, 0x1

    #@a2
    .line 113
    .local v1, _arg0:Z
    :goto_a2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a5
    move-result v2

    #@a6
    .line 114
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyMessageWaitingChanged(ZI)V

    #@a9
    .line 115
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ac
    .line 116
    const/4 v0, 0x1

    #@ad
    goto/16 :goto_7

    #@af
    .line 111
    .end local v1           #_arg0:Z
    .end local v2           #_arg1:I
    :cond_af
    const/4 v1, 0x0

    #@b0
    goto :goto_a2

    #@b1
    .line 120
    :sswitch_b1
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@b3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b6
    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b9
    move-result v0

    #@ba
    if-eqz v0, :cond_ca

    #@bc
    const/4 v1, 0x1

    #@bd
    .line 124
    .restart local v1       #_arg0:Z
    :goto_bd
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c0
    move-result v2

    #@c1
    .line 125
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyCallForwardingChanged(ZI)V

    #@c4
    .line 126
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c7
    .line 127
    const/4 v0, 0x1

    #@c8
    goto/16 :goto_7

    #@ca
    .line 122
    .end local v1           #_arg0:Z
    .end local v2           #_arg1:I
    :cond_ca
    const/4 v1, 0x0

    #@cb
    goto :goto_bd

    #@cc
    .line 131
    :sswitch_cc
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@ce
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d1
    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d4
    move-result v1

    #@d5
    .line 134
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyDataActivity(I)V

    #@d8
    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@db
    .line 136
    const/4 v0, 0x1

    #@dc
    goto/16 :goto_7

    #@de
    .line 140
    .end local v1           #_arg0:I
    :sswitch_de
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@e0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e3
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e6
    move-result v1

    #@e7
    .line 144
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ea
    move-result v0

    #@eb
    if-eqz v0, :cond_12f

    #@ed
    const/4 v2, 0x1

    #@ee
    .line 146
    .local v2, _arg1:Z
    :goto_ee
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f1
    move-result-object v3

    #@f2
    .line 148
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f5
    move-result-object v4

    #@f6
    .line 150
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f9
    move-result-object v5

    #@fa
    .line 152
    .local v5, _arg4:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@fd
    move-result v0

    #@fe
    if-eqz v0, :cond_131

    #@100
    .line 153
    sget-object v0, Landroid/net/LinkProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@102
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@105
    move-result-object v6

    #@106
    check-cast v6, Landroid/net/LinkProperties;

    #@108
    .line 159
    .local v6, _arg5:Landroid/net/LinkProperties;
    :goto_108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10b
    move-result v0

    #@10c
    if-eqz v0, :cond_133

    #@10e
    .line 160
    sget-object v0, Landroid/net/LinkCapabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    #@110
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@113
    move-result-object v7

    #@114
    check-cast v7, Landroid/net/LinkCapabilities;

    #@116
    .line 166
    .local v7, _arg6:Landroid/net/LinkCapabilities;
    :goto_116
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@119
    move-result v8

    #@11a
    .line 168
    .local v8, _arg7:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11d
    move-result v0

    #@11e
    if-eqz v0, :cond_135

    #@120
    const/4 v9, 0x1

    #@121
    .line 170
    .local v9, _arg8:Z
    :goto_121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@124
    move-result v10

    #@125
    .local v10, _arg9:I
    move-object v0, p0

    #@126
    .line 171
    invoke-virtual/range {v0 .. v10}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyDataConnection(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;IZI)V

    #@129
    .line 172
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@12c
    .line 173
    const/4 v0, 0x1

    #@12d
    goto/16 :goto_7

    #@12f
    .line 144
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Landroid/net/LinkProperties;
    .end local v7           #_arg6:Landroid/net/LinkCapabilities;
    .end local v8           #_arg7:I
    .end local v9           #_arg8:Z
    .end local v10           #_arg9:I
    :cond_12f
    const/4 v2, 0x0

    #@130
    goto :goto_ee

    #@131
    .line 156
    .restart local v2       #_arg1:Z
    .restart local v3       #_arg2:Ljava/lang/String;
    .restart local v4       #_arg3:Ljava/lang/String;
    .restart local v5       #_arg4:Ljava/lang/String;
    :cond_131
    const/4 v6, 0x0

    #@132
    .restart local v6       #_arg5:Landroid/net/LinkProperties;
    goto :goto_108

    #@133
    .line 163
    :cond_133
    const/4 v7, 0x0

    #@134
    .restart local v7       #_arg6:Landroid/net/LinkCapabilities;
    goto :goto_116

    #@135
    .line 168
    .restart local v8       #_arg7:I
    :cond_135
    const/4 v9, 0x0

    #@136
    goto :goto_121

    #@137
    .line 177
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Landroid/net/LinkProperties;
    .end local v7           #_arg6:Landroid/net/LinkCapabilities;
    .end local v8           #_arg7:I
    :sswitch_137
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@139
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13c
    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13f
    move-result-object v1

    #@140
    .line 181
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@143
    move-result-object v2

    #@144
    .line 182
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V

    #@147
    .line 183
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14a
    .line 184
    const/4 v0, 0x1

    #@14b
    goto/16 :goto_7

    #@14d
    .line 188
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_14d
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@14f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@152
    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@155
    move-result v0

    #@156
    if-eqz v0, :cond_16d

    #@158
    .line 191
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@15a
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@15d
    move-result-object v1

    #@15e
    check-cast v1, Landroid/os/Bundle;

    #@160
    .line 197
    .local v1, _arg0:Landroid/os/Bundle;
    :goto_160
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@163
    move-result v2

    #@164
    .line 198
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyCellLocation(Landroid/os/Bundle;I)V

    #@167
    .line 199
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@16a
    .line 200
    const/4 v0, 0x1

    #@16b
    goto/16 :goto_7

    #@16d
    .line 194
    .end local v1           #_arg0:Landroid/os/Bundle;
    .end local v2           #_arg1:I
    :cond_16d
    const/4 v1, 0x0

    #@16e
    .restart local v1       #_arg0:Landroid/os/Bundle;
    goto :goto_160

    #@16f
    .line 204
    .end local v1           #_arg0:Landroid/os/Bundle;
    :sswitch_16f
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@171
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@174
    .line 206
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@177
    move-result v1

    #@178
    .line 207
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyOtaspChanged(I)V

    #@17b
    .line 208
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17e
    .line 209
    const/4 v0, 0x1

    #@17f
    goto/16 :goto_7

    #@181
    .line 213
    .end local v1           #_arg0:I
    :sswitch_181
    const-string v0, "com.android.internal.telephony.ITelephonyRegistryMSim"

    #@183
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@186
    .line 215
    sget-object v0, Landroid/telephony/CellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@188
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@18b
    move-result-object v11

    #@18c
    .line 217
    .local v11, _arg0:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18f
    move-result v2

    #@190
    .line 218
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v11, v2}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->notifyCellInfo(Ljava/util/List;I)V

    #@193
    .line 219
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@196
    .line 220
    const/4 v0, 0x1

    #@197
    goto/16 :goto_7

    #@199
    .line 38
    nop

    #@19a
    :sswitch_data_19a
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_3a
        0x3 -> :sswitch_53
        0x4 -> :sswitch_74
        0x5 -> :sswitch_96
        0x6 -> :sswitch_b1
        0x7 -> :sswitch_cc
        0x8 -> :sswitch_de
        0x9 -> :sswitch_137
        0xa -> :sswitch_14d
        0xb -> :sswitch_16f
        0xc -> :sswitch_181
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
