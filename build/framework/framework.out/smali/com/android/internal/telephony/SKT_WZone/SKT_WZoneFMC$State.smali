.class public final enum Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;
.super Ljava/lang/Enum;
.source "SKT_WZoneFMC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

.field public static final enum ACTIVE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

.field public static final enum DIALING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

.field public static final enum IDLE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

.field public static final enum RINGING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 10
    new-instance v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@6
    const-string v1, "IDLE"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->IDLE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@d
    new-instance v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@f
    const-string v1, "ACTIVE"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->ACTIVE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@16
    new-instance v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@18
    const-string v1, "DIALING"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->DIALING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@1f
    new-instance v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@21
    const-string v1, "RINGING"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->RINGING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@28
    .line 9
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2b
    sget-object v1, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->IDLE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->ACTIVE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->DIALING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->RINGING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->$VALUES:[Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 9
    const-class v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;
    .registers 1

    #@0
    .prologue
    .line 9
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->$VALUES:[Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@8
    return-object v0
.end method


# virtual methods
.method public isAlive()Z
    .registers 2

    #@0
    .prologue
    .line 17
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->IDLE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    if-eq p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isDialing()Z
    .registers 2

    #@0
    .prologue
    .line 25
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->DIALING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    if-ne p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isIdle()Z
    .registers 2

    #@0
    .prologue
    .line 13
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->IDLE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    if-ne p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isRinging()Z
    .registers 2

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->RINGING:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    if-ne p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method
