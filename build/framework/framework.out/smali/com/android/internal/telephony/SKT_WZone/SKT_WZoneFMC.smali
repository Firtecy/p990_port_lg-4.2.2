.class public Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;
.super Ljava/lang/Object;
.source "SKT_WZoneFMC.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$1;,
        Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$SKT_WZoneFMCHolder;,
        Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;
    }
.end annotation


# static fields
.field public static final ACTION_EXIT_NAME_ANSWER_VOIP:Ljava/lang/String; = "answer_voip"

.field public static final ACTION_EXIT_NAME_HANGUP_VOIP:Ljava/lang/String; = "hangup_voip"

.field public static final ACTION_EXIT_NAME_MOVETOTOP_VOIP:Ljava/lang/String; = "movetotop_voip"


# instance fields
.field private mActionExtName:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;

.field private mSActionintent:Ljava/lang/String;

.field private mState:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->IDLE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mState:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@7
    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3
    invoke-direct {p0}, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;
    .registers 1

    #@0
    .prologue
    .line 44
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$SKT_WZoneFMCHolder;->mSKT_WZoneFMC:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;

    #@2
    return-object v0
.end method


# virtual methods
.method public cleanCurrentVoIP()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 62
    iput-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mActionExtName:Ljava/lang/String;

    #@3
    .line 63
    iput-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mSActionintent:Ljava/lang/String;

    #@5
    .line 64
    iput-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mPhoneNumber:Ljava/lang/String;

    #@7
    .line 66
    sget-object v0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;->IDLE:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mState:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@b
    .line 67
    return-void
.end method

.method public getActionExtName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mActionExtName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mPhoneNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSActionintent()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mSActionintent:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getState()Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;
    .registers 2

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mState:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    return-object v0
.end method

.method public setCurrentVoIP(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "actionExtName"
    .parameter "sActionintent"
    .parameter "PhoneNumber"

    #@0
    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mActionExtName:Ljava/lang/String;

    #@2
    .line 57
    iput-object p2, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mSActionintent:Ljava/lang/String;

    #@4
    .line 58
    iput-object p3, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mPhoneNumber:Ljava/lang/String;

    #@6
    .line 59
    return-void
.end method

.method public setState(Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 51
    iput-object p1, p0, Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC;->mState:Lcom/android/internal/telephony/SKT_WZone/SKT_WZoneFMC$State;

    #@2
    .line 52
    return-void
.end method
