.class public Lcom/android/internal/telephony/GsmAlphabet;
.super Ljava/lang/Object;
.source "GsmAlphabet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;,
        Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final GSM_EXTENDED_ESCAPE:B = 0x1bt

.field private static final TAG:Ljava/lang/String; = "GSM"

.field public static final UDH_SEPTET_COST_CONCATENATED_MESSAGE:I = 0x6

.field public static final UDH_SEPTET_COST_LENGTH:I = 0x1

.field public static final UDH_SEPTET_COST_ONE_SHIFT_TABLE:I = 0x4

.field public static final UDH_SEPTET_COST_TWO_SHIFT_TABLES:I = 0x7

.field private static final charToGsmBasicLatin:Landroid/util/SparseIntArray;

.field private static final charToGsmCyrillic:Landroid/util/SparseIntArray;

.field private static final charToGsmGeneralPunctuation:Landroid/util/SparseIntArray;

.field private static final charToGsmGreekCoptic:Landroid/util/SparseIntArray;

.field private static final charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

.field private static final charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

.field private static final charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

.field private static final sCharsToGsmTables:[Landroid/util/SparseIntArray;

.field private static final sCharsToShiftTables:[Landroid/util/SparseIntArray;

.field private static sDisableCountryEncodingCheck:Z

.field private static sEnabledLockingShiftTables:[I

.field private static sEnabledSingleShiftTables:[I

.field private static sHighestEnabledSingleShiftCode:I

.field private static final sLanguageShiftTables:[Ljava/lang/String;

.field private static final sLanguageTables:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 14

    #@0
    .prologue
    .line 1520
    const/4 v11, 0x0

    #@1
    sput-boolean v11, Lcom/android/internal/telephony/GsmAlphabet;->sDisableCountryEncodingCheck:Z

    #@3
    .line 1562
    const/16 v11, 0xe

    #@5
    new-array v11, v11, [Ljava/lang/String;

    #@7
    const/4 v12, 0x0

    #@8
    const-string v13, "@\u00a3$\u00a5\u00e8\u00e9\u00f9\u00ec\u00f2\u00c7\n\u00d8\u00f8\r\u00c5\u00e5\u0394_\u03a6\u0393\u039b\u03a9\u03a0\u03a8\u03a3\u0398\u039e\uffff\u00c6\u00e6\u00df\u00c9 !\"#\u00a4%&\'()*+,-./0123456789:;<=>?\u00a1ABCDEFGHIJKLMNOPQRSTUVWXYZ\u00c4\u00d6\u00d1\u00dc\u00a7\u00bfabcdefghijklmnopqrstuvwxyz\u00e4\u00f6\u00f1\u00fc\u00e0"

    #@a
    aput-object v13, v11, v12

    #@c
    const/4 v12, 0x1

    #@d
    const-string v13, "@\u00a3$\u00a5\u20ac\u00e9\u00f9\u0131\u00f2\u00c7\n\u011e\u011f\r\u00c5\u00e5\u0394_\u03a6\u0393\u039b\u03a9\u03a0\u03a8\u03a3\u0398\u039e\uffff\u015e\u015f\u00df\u00c9 !\"#\u00a4%&\'()*+,-./0123456789:;<=>?\u0130ABCDEFGHIJKLMNOPQRSTUVWXYZ\u00c4\u00d6\u00d1\u00dc\u00a7\u00e7abcdefghijklmnopqrstuvwxyz\u00e4\u00f6\u00f1\u00fc\u00e0"

    #@f
    aput-object v13, v11, v12

    #@11
    const/4 v12, 0x2

    #@12
    const-string v13, ""

    #@14
    aput-object v13, v11, v12

    #@16
    const/4 v12, 0x3

    #@17
    const-string v13, "@\u00a3$\u00a5\u00ea\u00e9\u00fa\u00ed\u00f3\u00e7\n\u00d4\u00f4\r\u00c1\u00e1\u0394_\u00aa\u00c7\u00c0\u221e^\\\u20ac\u00d3|\uffff\u00c2\u00e2\u00ca\u00c9 !\"#\u00ba%&\'()*+,-./0123456789:;<=>?\u00cdABCDEFGHIJKLMNOPQRSTUVWXYZ\u00c3\u00d5\u00da\u00dc\u00a7~abcdefghijklmnopqrstuvwxyz\u00e3\u00f5`\u00fc\u00e0"

    #@19
    aput-object v13, v11, v12

    #@1b
    const/4 v12, 0x4

    #@1c
    const-string/jumbo v13, "\u0981\u0982\u0983\u0985\u0986\u0987\u0988\u0989\u098a\u098b\n\u098c \r \u098f\u0990  \u0993\u0994\u0995\u0996\u0997\u0998\u0999\u099a\uffff\u099b\u099c\u099d\u099e !\u099f\u09a0\u09a1\u09a2\u09a3\u09a4)(\u09a5\u09a6,\u09a7.\u09a80123456789:; \u09aa\u09ab?\u09ac\u09ad\u09ae\u09af\u09b0 \u09b2   \u09b6\u09b7\u09b8\u09b9\u09bc\u09bd\u09be\u09bf\u09c0\u09c1\u09c2\u09c3\u09c4  \u09c7\u09c8  \u09cb\u09cc\u09cd\u09ceabcdefghijklmnopqrstuvwxyz\u09d7\u09dc\u09dd\u09f0\u09f1"

    #@1f
    aput-object v13, v11, v12

    #@21
    const/4 v12, 0x5

    #@22
    const-string/jumbo v13, "\u0a81\u0a82\u0a83\u0a85\u0a86\u0a87\u0a88\u0a89\u0a8a\u0a8b\n\u0a8c\u0a8d\r \u0a8f\u0a90\u0a91 \u0a93\u0a94\u0a95\u0a96\u0a97\u0a98\u0a99\u0a9a\uffff\u0a9b\u0a9c\u0a9d\u0a9e !\u0a9f\u0aa0\u0aa1\u0aa2\u0aa3\u0aa4)(\u0aa5\u0aa6,\u0aa7.\u0aa80123456789:; \u0aaa\u0aab?\u0aac\u0aad\u0aae\u0aaf\u0ab0 \u0ab2\u0ab3 \u0ab5\u0ab6\u0ab7\u0ab8\u0ab9\u0abc\u0abd\u0abe\u0abf\u0ac0\u0ac1\u0ac2\u0ac3\u0ac4\u0ac5 \u0ac7\u0ac8\u0ac9 \u0acb\u0acc\u0acd\u0ad0abcdefghijklmnopqrstuvwxyz\u0ae0\u0ae1\u0ae2\u0ae3\u0af1"

    #@25
    aput-object v13, v11, v12

    #@27
    const/4 v12, 0x6

    #@28
    const-string/jumbo v13, "\u0901\u0902\u0903\u0905\u0906\u0907\u0908\u0909\u090a\u090b\n\u090c\u090d\r\u090e\u090f\u0910\u0911\u0912\u0913\u0914\u0915\u0916\u0917\u0918\u0919\u091a\uffff\u091b\u091c\u091d\u091e !\u091f\u0920\u0921\u0922\u0923\u0924)(\u0925\u0926,\u0927.\u09280123456789:;\u0929\u092a\u092b?\u092c\u092d\u092e\u092f\u0930\u0931\u0932\u0933\u0934\u0935\u0936\u0937\u0938\u0939\u093c\u093d\u093e\u093f\u0940\u0941\u0942\u0943\u0944\u0945\u0946\u0947\u0948\u0949\u094a\u094b\u094c\u094d\u0950abcdefghijklmnopqrstuvwxyz\u0972\u097b\u097c\u097e\u097f"

    #@2b
    aput-object v13, v11, v12

    #@2d
    const/4 v12, 0x7

    #@2e
    const-string v13, " \u0c82\u0c83\u0c85\u0c86\u0c87\u0c88\u0c89\u0c8a\u0c8b\n\u0c8c \r\u0c8e\u0c8f\u0c90 \u0c92\u0c93\u0c94\u0c95\u0c96\u0c97\u0c98\u0c99\u0c9a\uffff\u0c9b\u0c9c\u0c9d\u0c9e !\u0c9f\u0ca0\u0ca1\u0ca2\u0ca3\u0ca4)(\u0ca5\u0ca6,\u0ca7.\u0ca80123456789:; \u0caa\u0cab?\u0cac\u0cad\u0cae\u0caf\u0cb0\u0cb1\u0cb2\u0cb3 \u0cb5\u0cb6\u0cb7\u0cb8\u0cb9\u0cbc\u0cbd\u0cbe\u0cbf\u0cc0\u0cc1\u0cc2\u0cc3\u0cc4 \u0cc6\u0cc7\u0cc8 \u0cca\u0ccb\u0ccc\u0ccd\u0cd5abcdefghijklmnopqrstuvwxyz\u0cd6\u0ce0\u0ce1\u0ce2\u0ce3"

    #@30
    aput-object v13, v11, v12

    #@32
    const/16 v12, 0x8

    #@34
    const-string v13, " \u0d02\u0d03\u0d05\u0d06\u0d07\u0d08\u0d09\u0d0a\u0d0b\n\u0d0c \r\u0d0e\u0d0f\u0d10 \u0d12\u0d13\u0d14\u0d15\u0d16\u0d17\u0d18\u0d19\u0d1a\uffff\u0d1b\u0d1c\u0d1d\u0d1e !\u0d1f\u0d20\u0d21\u0d22\u0d23\u0d24)(\u0d25\u0d26,\u0d27.\u0d280123456789:; \u0d2a\u0d2b?\u0d2c\u0d2d\u0d2e\u0d2f\u0d30\u0d31\u0d32\u0d33\u0d34\u0d35\u0d36\u0d37\u0d38\u0d39 \u0d3d\u0d3e\u0d3f\u0d40\u0d41\u0d42\u0d43\u0d44 \u0d46\u0d47\u0d48 \u0d4a\u0d4b\u0d4c\u0d4d\u0d57abcdefghijklmnopqrstuvwxyz\u0d60\u0d61\u0d62\u0d63\u0d79"

    #@36
    aput-object v13, v11, v12

    #@38
    const/16 v12, 0x9

    #@3a
    const-string/jumbo v13, "\u0b01\u0b02\u0b03\u0b05\u0b06\u0b07\u0b08\u0b09\u0b0a\u0b0b\n\u0b0c \r \u0b0f\u0b10  \u0b13\u0b14\u0b15\u0b16\u0b17\u0b18\u0b19\u0b1a\uffff\u0b1b\u0b1c\u0b1d\u0b1e !\u0b1f\u0b20\u0b21\u0b22\u0b23\u0b24)(\u0b25\u0b26,\u0b27.\u0b280123456789:; \u0b2a\u0b2b?\u0b2c\u0b2d\u0b2e\u0b2f\u0b30 \u0b32\u0b33 \u0b35\u0b36\u0b37\u0b38\u0b39\u0b3c\u0b3d\u0b3e\u0b3f\u0b40\u0b41\u0b42\u0b43\u0b44  \u0b47\u0b48  \u0b4b\u0b4c\u0b4d\u0b56abcdefghijklmnopqrstuvwxyz\u0b57\u0b60\u0b61\u0b62\u0b63"

    #@3d
    aput-object v13, v11, v12

    #@3f
    const/16 v12, 0xa

    #@41
    const-string/jumbo v13, "\u0a01\u0a02\u0a03\u0a05\u0a06\u0a07\u0a08\u0a09\u0a0a \n  \r \u0a0f\u0a10  \u0a13\u0a14\u0a15\u0a16\u0a17\u0a18\u0a19\u0a1a\uffff\u0a1b\u0a1c\u0a1d\u0a1e !\u0a1f\u0a20\u0a21\u0a22\u0a23\u0a24)(\u0a25\u0a26,\u0a27.\u0a280123456789:; \u0a2a\u0a2b?\u0a2c\u0a2d\u0a2e\u0a2f\u0a30 \u0a32\u0a33 \u0a35\u0a36 \u0a38\u0a39\u0a3c \u0a3e\u0a3f\u0a40\u0a41\u0a42    \u0a47\u0a48  \u0a4b\u0a4c\u0a4d\u0a51abcdefghijklmnopqrstuvwxyz\u0a70\u0a71\u0a72\u0a73\u0a74"

    #@44
    aput-object v13, v11, v12

    #@46
    const/16 v12, 0xb

    #@48
    const-string v13, " \u0b82\u0b83\u0b85\u0b86\u0b87\u0b88\u0b89\u0b8a \n  \r\u0b8e\u0b8f\u0b90 \u0b92\u0b93\u0b94\u0b95   \u0b99\u0b9a\uffff \u0b9c \u0b9e !\u0b9f   \u0ba3\u0ba4)(  , .\u0ba80123456789:;\u0ba9\u0baa ?  \u0bae\u0baf\u0bb0\u0bb1\u0bb2\u0bb3\u0bb4\u0bb5\u0bb6\u0bb7\u0bb8\u0bb9  \u0bbe\u0bbf\u0bc0\u0bc1\u0bc2   \u0bc6\u0bc7\u0bc8 \u0bca\u0bcb\u0bcc\u0bcd\u0bd0abcdefghijklmnopqrstuvwxyz\u0bd7\u0bf0\u0bf1\u0bf2\u0bf9"

    #@4a
    aput-object v13, v11, v12

    #@4c
    const/16 v12, 0xc

    #@4e
    const-string/jumbo v13, "\u0c01\u0c02\u0c03\u0c05\u0c06\u0c07\u0c08\u0c09\u0c0a\u0c0b\n\u0c0c \r\u0c0e\u0c0f\u0c10 \u0c12\u0c13\u0c14\u0c15\u0c16\u0c17\u0c18\u0c19\u0c1a\uffff\u0c1b\u0c1c\u0c1d\u0c1e !\u0c1f\u0c20\u0c21\u0c22\u0c23\u0c24)(\u0c25\u0c26,\u0c27.\u0c280123456789:; \u0c2a\u0c2b?\u0c2c\u0c2d\u0c2e\u0c2f\u0c30\u0c31\u0c32\u0c33 \u0c35\u0c36\u0c37\u0c38\u0c39 \u0c3d\u0c3e\u0c3f\u0c40\u0c41\u0c42\u0c43\u0c44 \u0c46\u0c47\u0c48 \u0c4a\u0c4b\u0c4c\u0c4d\u0c55abcdefghijklmnopqrstuvwxyz\u0c56\u0c60\u0c61\u0c62\u0c63"

    #@51
    aput-object v13, v11, v12

    #@53
    const/16 v12, 0xd

    #@55
    const-string/jumbo v13, "\u0627\u0622\u0628\u067b\u0680\u067e\u06a6\u062a\u06c2\u067f\n\u0679\u067d\r\u067a\u067c\u062b\u062c\u0681\u0684\u0683\u0685\u0686\u0687\u062d\u062e\u062f\uffff\u068c\u0688\u0689\u068a !\u068f\u068d\u0630\u0631\u0691\u0693)(\u0699\u0632,\u0696.\u06980123456789:;\u069a\u0633\u0634?\u0635\u0636\u0637\u0638\u0639\u0641\u0642\u06a9\u06aa\u06ab\u06af\u06b3\u06b1\u0644\u0645\u0646\u06ba\u06bb\u06bc\u0648\u06c4\u06d5\u06c1\u06be\u0621\u06cc\u06d0\u06d2\u064d\u0650\u064f\u0657\u0654abcdefghijklmnopqrstuvwxyz\u0655\u0651\u0653\u0656\u0670"

    #@58
    aput-object v13, v11, v12

    #@5a
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@5c
    .line 1755
    const/16 v11, 0xe

    #@5e
    new-array v11, v11, [Ljava/lang/String;

    #@60
    const/4 v12, 0x0

    #@61
    const-string v13, "          \u000c         ^                   {}     \\            [~] |                                    \u20ac                          "

    #@63
    aput-object v13, v11, v12

    #@65
    const/4 v12, 0x1

    #@66
    const-string v13, "          \u000c         ^                   {}     \\            [~] |      \u011e \u0130         \u015e               \u00e7 \u20ac \u011f \u0131         \u015f            "

    #@68
    aput-object v13, v11, v12

    #@6a
    const/4 v12, 0x2

    #@6b
    const-string v13, "         \u00e7\u000c         ^                   {}     \\            [~] |\u00c1       \u00cd     \u00d3     \u00da           \u00e1   \u20ac   \u00ed     \u00f3     \u00fa          "

    #@6d
    aput-object v13, v11, v12

    #@6f
    const/4 v12, 0x3

    #@70
    const-string v13, "     \u00ea   \u00e7\u000c\u00d4\u00f4 \u00c1\u00e1  \u03a6\u0393^\u03a9\u03a0\u03a8\u03a3\u0398     \u00ca        {}     \\            [~] |\u00c0       \u00cd     \u00d3     \u00da     \u00c3\u00d5    \u00c2   \u20ac   \u00ed     \u00f3     \u00fa     \u00e3\u00f5  \u00e2"

    #@72
    aput-object v13, v11, v12

    #@74
    const/4 v12, 0x4

    #@75
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u09e6\u09e7 \u09e8\u09e9\u09ea\u09eb\u09ec\u09ed\u09ee\u09ef\u09df\u09e0\u09e1\u09e2{}\u09e3\u09f2\u09f3\u09f4\u09f5\\\u09f6\u09f7\u09f8\u09f9\u09fa       [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@77
    aput-object v13, v11, v12

    #@79
    const/4 v12, 0x5

    #@7a
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0964\u0965 \u0ae6\u0ae7\u0ae8\u0ae9\u0aea\u0aeb\u0aec\u0aed\u0aee\u0aef  {}     \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@7c
    aput-object v13, v11, v12

    #@7e
    const/4 v12, 0x6

    #@7f
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0964\u0965 \u0966\u0967\u0968\u0969\u096a\u096b\u096c\u096d\u096e\u096f\u0951\u0952{}\u0953\u0954\u0958\u0959\u095a\\\u095b\u095c\u095d\u095e\u095f\u0960\u0961\u0962\u0963\u0970\u0971 [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@81
    aput-object v13, v11, v12

    #@83
    const/4 v12, 0x7

    #@84
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0964\u0965 \u0ce6\u0ce7\u0ce8\u0ce9\u0cea\u0ceb\u0cec\u0ced\u0cee\u0cef\u0cde\u0cf1{}\u0cf2    \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@86
    aput-object v13, v11, v12

    #@88
    const/16 v12, 0x8

    #@8a
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0964\u0965 \u0d66\u0d67\u0d68\u0d69\u0d6a\u0d6b\u0d6c\u0d6d\u0d6e\u0d6f\u0d70\u0d71{}\u0d72\u0d73\u0d74\u0d75\u0d7a\\\u0d7b\u0d7c\u0d7d\u0d7e\u0d7f       [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@8c
    aput-object v13, v11, v12

    #@8e
    const/16 v12, 0x9

    #@90
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0964\u0965 \u0b66\u0b67\u0b68\u0b69\u0b6a\u0b6b\u0b6c\u0b6d\u0b6e\u0b6f\u0b5c\u0b5d{}\u0b5f\u0b70\u0b71  \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@92
    aput-object v13, v11, v12

    #@94
    const/16 v12, 0xa

    #@96
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0964\u0965 \u0a66\u0a67\u0a68\u0a69\u0a6a\u0a6b\u0a6c\u0a6d\u0a6e\u0a6f\u0a59\u0a5a{}\u0a5b\u0a5c\u0a5e\u0a75 \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@98
    aput-object v13, v11, v12

    #@9a
    const/16 v12, 0xb

    #@9c
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0964\u0965 \u0be6\u0be7\u0be8\u0be9\u0bea\u0beb\u0bec\u0bed\u0bee\u0bef\u0bf3\u0bf4{}\u0bf5\u0bf6\u0bf7\u0bf8\u0bfa\\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@9e
    aput-object v13, v11, v12

    #@a0
    const/16 v12, 0xc

    #@a2
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*   \u0c66\u0c67\u0c68\u0c69\u0c6a\u0c6b\u0c6c\u0c6d\u0c6e\u0c6f\u0c58\u0c59{}\u0c78\u0c79\u0c7a\u0c7b\u0c7c\\\u0c7d\u0c7e\u0c7f         [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@a4
    aput-object v13, v11, v12

    #@a6
    const/16 v12, 0xd

    #@a8
    const-string v13, "@\u00a3$\u00a5\u00bf\"\u00a4%&\'\u000c*+ -/<=>\u00a1^\u00a1_#*\u0600\u0601 \u06f0\u06f1\u06f2\u06f3\u06f4\u06f5\u06f6\u06f7\u06f8\u06f9\u060c\u060d{}\u060e\u060f\u0610\u0611\u0612\\\u0613\u0614\u061b\u061f\u0640\u0652\u0658\u066b\u066c\u0672\u0673\u06cd[~]\u06d4|ABCDEFGHIJKLMNOPQRSTUVWXYZ          \u20ac                          "

    #@aa
    aput-object v13, v11, v12

    #@ac
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@ae
    .line 1894
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@b0
    array-length v6, v11

    #@b1
    .line 1895
    .local v6, numTables:I
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@b3
    array-length v5, v11

    #@b4
    .line 1896
    .local v5, numShiftTables:I
    if-eq v6, v5, :cond_d8

    #@b6
    .line 1897
    const-string v11, "GSM"

    #@b8
    new-instance v12, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v13, "Error: language tables array length "

    #@bf
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v12

    #@c3
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v12

    #@c7
    const-string v13, " != shift tables array length "

    #@c9
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v12

    #@cd
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v12

    #@d1
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v12

    #@d5
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 1905
    :cond_d8
    invoke-static {}, Lcom/android/internal/telephony/GsmAlphabet;->setEnabledShiftTablesLG()V

    #@db
    .line 1907
    new-array v11, v6, [Landroid/util/SparseIntArray;

    #@dd
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@df
    .line 1908
    const/4 v3, 0x0

    #@e0
    .local v3, i:I
    :goto_e0
    if-ge v3, v6, :cond_131

    #@e2
    .line 1909
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@e4
    aget-object v9, v11, v3

    #@e6
    .line 1911
    .local v9, table:Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@e9
    move-result v10

    #@ea
    .line 1912
    .local v10, tableLen:I
    if-eqz v10, :cond_118

    #@ec
    const/16 v11, 0x80

    #@ee
    if-eq v10, v11, :cond_118

    #@f0
    .line 1913
    const-string v11, "GSM"

    #@f2
    new-instance v12, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v13, "Error: language tables index "

    #@f9
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v12

    #@fd
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v12

    #@101
    const-string v13, " length "

    #@103
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v12

    #@107
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v12

    #@10b
    const-string v13, " (expected 128 or 0)"

    #@10d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v12

    #@111
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@114
    move-result-object v12

    #@115
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@118
    .line 1917
    :cond_118
    new-instance v1, Landroid/util/SparseIntArray;

    #@11a
    invoke-direct {v1, v10}, Landroid/util/SparseIntArray;-><init>(I)V

    #@11d
    .line 1918
    .local v1, charToGsmTable:Landroid/util/SparseIntArray;
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@11f
    aput-object v1, v11, v3

    #@121
    .line 1919
    const/4 v4, 0x0

    #@122
    .local v4, j:I
    :goto_122
    if-ge v4, v10, :cond_12e

    #@124
    .line 1920
    invoke-virtual {v9, v4}, Ljava/lang/String;->charAt(I)C

    #@127
    move-result v0

    #@128
    .line 1921
    .local v0, c:C
    invoke-virtual {v1, v0, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@12b
    .line 1919
    add-int/lit8 v4, v4, 0x1

    #@12d
    goto :goto_122

    #@12e
    .line 1908
    .end local v0           #c:C
    :cond_12e
    add-int/lit8 v3, v3, 0x1

    #@130
    goto :goto_e0

    #@131
    .line 1925
    .end local v1           #charToGsmTable:Landroid/util/SparseIntArray;
    .end local v4           #j:I
    .end local v9           #table:Ljava/lang/String;
    .end local v10           #tableLen:I
    :cond_131
    new-array v11, v6, [Landroid/util/SparseIntArray;

    #@133
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@135
    .line 1926
    const/4 v3, 0x0

    #@136
    :goto_136
    if-ge v3, v5, :cond_18b

    #@138
    .line 1927
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@13a
    aget-object v7, v11, v3

    #@13c
    .line 1929
    .local v7, shiftTable:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@13f
    move-result v8

    #@140
    .line 1930
    .local v8, shiftTableLen:I
    if-eqz v8, :cond_16e

    #@142
    const/16 v11, 0x80

    #@144
    if-eq v8, v11, :cond_16e

    #@146
    .line 1931
    const-string v11, "GSM"

    #@148
    new-instance v12, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v13, "Error: language shift tables index "

    #@14f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v12

    #@153
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@156
    move-result-object v12

    #@157
    const-string v13, " length "

    #@159
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v12

    #@15d
    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@160
    move-result-object v12

    #@161
    const-string v13, " (expected 128 or 0)"

    #@163
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v12

    #@167
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16a
    move-result-object v12

    #@16b
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    .line 1935
    :cond_16e
    new-instance v2, Landroid/util/SparseIntArray;

    #@170
    invoke-direct {v2, v8}, Landroid/util/SparseIntArray;-><init>(I)V

    #@173
    .line 1936
    .local v2, charToShiftTable:Landroid/util/SparseIntArray;
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@175
    aput-object v2, v11, v3

    #@177
    .line 1937
    const/4 v4, 0x0

    #@178
    .restart local v4       #j:I
    :goto_178
    if-ge v4, v8, :cond_188

    #@17a
    .line 1938
    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    #@17d
    move-result v0

    #@17e
    .line 1939
    .restart local v0       #c:C
    const/16 v11, 0x20

    #@180
    if-eq v0, v11, :cond_185

    #@182
    .line 1940
    invoke-virtual {v2, v0, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@185
    .line 1937
    :cond_185
    add-int/lit8 v4, v4, 0x1

    #@187
    goto :goto_178

    #@188
    .line 1926
    .end local v0           #c:C
    :cond_188
    add-int/lit8 v3, v3, 0x1

    #@18a
    goto :goto_136

    #@18b
    .line 1946
    .end local v2           #charToShiftTable:Landroid/util/SparseIntArray;
    .end local v4           #j:I
    .end local v7           #shiftTable:Ljava/lang/String;
    .end local v8           #shiftTableLen:I
    :cond_18b
    new-instance v11, Landroid/util/SparseIntArray;

    #@18d
    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    #@190
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmBasicLatin:Landroid/util/SparseIntArray;

    #@192
    .line 1947
    new-instance v11, Landroid/util/SparseIntArray;

    #@194
    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    #@197
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@199
    .line 1948
    new-instance v11, Landroid/util/SparseIntArray;

    #@19b
    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    #@19e
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@1a0
    .line 1949
    new-instance v11, Landroid/util/SparseIntArray;

    #@1a2
    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    #@1a5
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@1a7
    .line 1950
    new-instance v11, Landroid/util/SparseIntArray;

    #@1a9
    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    #@1ac
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@1ae
    .line 1951
    new-instance v11, Landroid/util/SparseIntArray;

    #@1b0
    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    #@1b3
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmCyrillic:Landroid/util/SparseIntArray;

    #@1b5
    .line 1952
    new-instance v11, Landroid/util/SparseIntArray;

    #@1b7
    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    #@1ba
    sput-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGeneralPunctuation:Landroid/util/SparseIntArray;

    #@1bc
    .line 1955
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@1be
    const/16 v12, 0xe1

    #@1c0
    const/16 v13, 0x61

    #@1c2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@1c5
    .line 1956
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@1c7
    const/16 v12, 0xe2

    #@1c9
    const/16 v13, 0x61

    #@1cb
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@1ce
    .line 1957
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@1d0
    const/16 v12, 0xe3

    #@1d2
    const/16 v13, 0x61

    #@1d4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@1d7
    .line 1958
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@1d9
    const/16 v12, 0x101

    #@1db
    const/16 v13, 0x61

    #@1dd
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@1e0
    .line 1959
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@1e2
    const/16 v12, 0x103

    #@1e4
    const/16 v13, 0x61

    #@1e6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@1e9
    .line 1960
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@1eb
    const/16 v12, 0x105

    #@1ed
    const/16 v13, 0x61

    #@1ef
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@1f2
    .line 1961
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@1f4
    const/16 v12, 0x1ce

    #@1f6
    const/16 v13, 0x61

    #@1f8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@1fb
    .line 1962
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@1fd
    const/16 v12, 0x1df

    #@1ff
    const/16 v13, 0x61

    #@201
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@204
    .line 1963
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@206
    const/16 v12, 0x1e1

    #@208
    const/16 v13, 0x61

    #@20a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@20d
    .line 1964
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@20f
    const/16 v12, 0x1fb

    #@211
    const/16 v13, 0x61

    #@213
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@216
    .line 1965
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@218
    const/16 v12, 0x201

    #@21a
    const/16 v13, 0x61

    #@21c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@21f
    .line 1966
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@221
    const/16 v12, 0x203

    #@223
    const/16 v13, 0x61

    #@225
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@228
    .line 1967
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@22a
    const/16 v12, 0x227

    #@22c
    const/16 v13, 0x61

    #@22e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@231
    .line 1968
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@233
    const/16 v12, 0x180

    #@235
    const/16 v13, 0x62

    #@237
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@23a
    .line 1969
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@23c
    const/16 v12, 0x182

    #@23e
    const/16 v13, 0x62

    #@240
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@243
    .line 1970
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@245
    const/16 v12, 0x183

    #@247
    const/16 v13, 0x62

    #@249
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@24c
    .line 1971
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@24e
    const/16 v12, 0x184

    #@250
    const/16 v13, 0x62

    #@252
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@255
    .line 1972
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@257
    const/16 v12, 0x185

    #@259
    const/16 v13, 0x62

    #@25b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@25e
    .line 1974
    const/4 v11, 0x0

    #@25f
    const-string v12, "VIVO_UCS2GSM_Encoding"

    #@261
    invoke-static {v11, v12}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@264
    move-result v11

    #@265
    const/4 v12, 0x1

    #@266
    if-ne v11, v12, :cond_e66

    #@268
    .line 1975
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@26a
    const/16 v12, 0xe7

    #@26c
    const/16 v13, 0x9

    #@26e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@271
    .line 1981
    :goto_271
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@273
    const/16 v12, 0x107

    #@275
    const/16 v13, 0x63

    #@277
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@27a
    .line 1982
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@27c
    const/16 v12, 0x109

    #@27e
    const/16 v13, 0x63

    #@280
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@283
    .line 1983
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@285
    const/16 v12, 0x10b

    #@287
    const/16 v13, 0x63

    #@289
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@28c
    .line 1984
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@28e
    const/16 v12, 0x10d

    #@290
    const/16 v13, 0x63

    #@292
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@295
    .line 1985
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@297
    const/16 v12, 0x188

    #@299
    const/16 v13, 0x63

    #@29b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@29e
    .line 1986
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@2a0
    const/16 v12, 0x10f

    #@2a2
    const/16 v13, 0x64

    #@2a4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2a7
    .line 1987
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@2a9
    const/16 v12, 0x111

    #@2ab
    const/16 v13, 0x64

    #@2ad
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2b0
    .line 1988
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@2b2
    const/16 v12, 0x18b

    #@2b4
    const/16 v13, 0x64

    #@2b6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2b9
    .line 1989
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@2bb
    const/16 v12, 0x18c

    #@2bd
    const/16 v13, 0x64

    #@2bf
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2c2
    .line 1990
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@2c4
    const/16 v12, 0x221

    #@2c6
    const/16 v13, 0x64

    #@2c8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2cb
    .line 1991
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@2cd
    const/16 v12, 0xea

    #@2cf
    const/16 v13, 0x65

    #@2d1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2d4
    .line 1992
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@2d6
    const/16 v12, 0xeb

    #@2d8
    const/16 v13, 0x65

    #@2da
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2dd
    .line 1993
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@2df
    const/16 v12, 0x113

    #@2e1
    const/16 v13, 0x65

    #@2e3
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2e6
    .line 1994
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@2e8
    const/16 v12, 0x115

    #@2ea
    const/16 v13, 0x65

    #@2ec
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2ef
    .line 1995
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@2f1
    const/16 v12, 0x117

    #@2f3
    const/16 v13, 0x65

    #@2f5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@2f8
    .line 1996
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@2fa
    const/16 v12, 0x119

    #@2fc
    const/16 v13, 0x65

    #@2fe
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@301
    .line 1997
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@303
    const/16 v12, 0x11b

    #@305
    const/16 v13, 0x65

    #@307
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@30a
    .line 1998
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@30c
    const/16 v12, 0x18f

    #@30e
    const/16 v13, 0x65

    #@310
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@313
    .line 1999
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@315
    const/16 v12, 0x190

    #@317
    const/16 v13, 0x65

    #@319
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@31c
    .line 2000
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@31e
    const/16 v12, 0x205

    #@320
    const/16 v13, 0x65

    #@322
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@325
    .line 2001
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@327
    const/16 v12, 0x207

    #@329
    const/16 v13, 0x65

    #@32b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@32e
    .line 2002
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@330
    const/16 v12, 0x229

    #@332
    const/16 v13, 0x65

    #@334
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@337
    .line 2003
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@339
    const/16 v12, 0x192

    #@33b
    const/16 v13, 0x66

    #@33d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@340
    .line 2004
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@342
    const/16 v12, 0x11d

    #@344
    const/16 v13, 0x67

    #@346
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@349
    .line 2005
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@34b
    const/16 v12, 0x11f

    #@34d
    const/16 v13, 0x67

    #@34f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@352
    .line 2006
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@354
    const/16 v12, 0x121

    #@356
    const/16 v13, 0x67

    #@358
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@35b
    .line 2007
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@35d
    const/16 v12, 0x123

    #@35f
    const/16 v13, 0x67

    #@361
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@364
    .line 2008
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@366
    const/16 v12, 0x1e5

    #@368
    const/16 v13, 0x67

    #@36a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@36d
    .line 2009
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@36f
    const/16 v12, 0x1e7

    #@371
    const/16 v13, 0x67

    #@373
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@376
    .line 2010
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@378
    const/16 v12, 0x1f5

    #@37a
    const/16 v13, 0x67

    #@37c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@37f
    .line 2011
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@381
    const/16 v12, 0x125

    #@383
    const/16 v13, 0x68

    #@385
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@388
    .line 2012
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@38a
    const/16 v12, 0x127

    #@38c
    const/16 v13, 0x68

    #@38e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@391
    .line 2013
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@393
    const/16 v12, 0x195

    #@395
    const/16 v13, 0x68

    #@397
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@39a
    .line 2014
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@39c
    const/16 v12, 0x21f

    #@39e
    const/16 v13, 0x68

    #@3a0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3a3
    .line 2015
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@3a5
    const/16 v12, 0xed

    #@3a7
    const/16 v13, 0x69

    #@3a9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3ac
    .line 2016
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@3ae
    const/16 v12, 0xee

    #@3b0
    const/16 v13, 0x69

    #@3b2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3b5
    .line 2017
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@3b7
    const/16 v12, 0xef

    #@3b9
    const/16 v13, 0x69

    #@3bb
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3be
    .line 2018
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@3c0
    const/16 v12, 0x129

    #@3c2
    const/16 v13, 0x69

    #@3c4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3c7
    .line 2019
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@3c9
    const/16 v12, 0x12b

    #@3cb
    const/16 v13, 0x69

    #@3cd
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3d0
    .line 2020
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@3d2
    const/16 v12, 0x12f

    #@3d4
    const/16 v13, 0x69

    #@3d6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3d9
    .line 2021
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@3db
    const/16 v12, 0x131

    #@3dd
    const/16 v13, 0x69

    #@3df
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3e2
    .line 2022
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@3e4
    const/16 v12, 0x196

    #@3e6
    const/16 v13, 0x69

    #@3e8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3eb
    .line 2023
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@3ed
    const/16 v12, 0x1d0

    #@3ef
    const/16 v13, 0x69

    #@3f1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3f4
    .line 2024
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@3f6
    const/16 v12, 0x209

    #@3f8
    const/16 v13, 0x69

    #@3fa
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@3fd
    .line 2025
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@3ff
    const/16 v12, 0x20b

    #@401
    const/16 v13, 0x69

    #@403
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@406
    .line 2026
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@408
    const/16 v12, 0x135

    #@40a
    const/16 v13, 0x6a

    #@40c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@40f
    .line 2027
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@411
    const/16 v12, 0x1f0

    #@413
    const/16 v13, 0x6a

    #@415
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@418
    .line 2028
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@41a
    const/16 v12, 0x137

    #@41c
    const/16 v13, 0x6b

    #@41e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@421
    .line 2029
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@423
    const/16 v12, 0x199

    #@425
    const/16 v13, 0x6b

    #@427
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@42a
    .line 2030
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@42c
    const/16 v12, 0x1e9

    #@42e
    const/16 v13, 0x6b

    #@430
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@433
    .line 2031
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@435
    const/16 v12, 0x13a

    #@437
    const/16 v13, 0x6c

    #@439
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@43c
    .line 2032
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@43e
    const/16 v12, 0x13c

    #@440
    const/16 v13, 0x6c

    #@442
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@445
    .line 2033
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@447
    const/16 v12, 0x13e

    #@449
    const/16 v13, 0x6c

    #@44b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@44e
    .line 2034
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@450
    const/16 v12, 0x140

    #@452
    const/16 v13, 0x6c

    #@454
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@457
    .line 2035
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@459
    const/16 v12, 0x142

    #@45b
    const/16 v13, 0x6c

    #@45d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@460
    .line 2036
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@462
    const/16 v12, 0x19a

    #@464
    const/16 v13, 0x6c

    #@466
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@469
    .line 2037
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@46b
    const/16 v12, 0x19c

    #@46d
    const/16 v13, 0x6d

    #@46f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@472
    .line 2038
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@474
    const/16 v12, 0x144

    #@476
    const/16 v13, 0x6e

    #@478
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@47b
    .line 2039
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@47d
    const/16 v12, 0x146

    #@47f
    const/16 v13, 0x6e

    #@481
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@484
    .line 2040
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@486
    const/16 v12, 0x148

    #@488
    const/16 v13, 0x6e

    #@48a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@48d
    .line 2041
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@48f
    const/16 v12, 0x149

    #@491
    const/16 v13, 0x6e

    #@493
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@496
    .line 2042
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@498
    const/16 v12, 0x14b

    #@49a
    const/16 v13, 0x6e

    #@49c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@49f
    .line 2043
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@4a1
    const/16 v12, 0x19e

    #@4a3
    const/16 v13, 0x6e

    #@4a5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4a8
    .line 2044
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@4aa
    const/16 v12, 0x1f9

    #@4ac
    const/16 v13, 0x6e

    #@4ae
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4b1
    .line 2045
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@4b3
    const/16 v12, 0x235

    #@4b5
    const/16 v13, 0x6e

    #@4b7
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4ba
    .line 2046
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@4bc
    const/16 v12, 0xf3

    #@4be
    const/16 v13, 0x6f

    #@4c0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4c3
    .line 2047
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@4c5
    const/16 v12, 0xf4

    #@4c7
    const/16 v13, 0x6f

    #@4c9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4cc
    .line 2048
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@4ce
    const/16 v12, 0xf5

    #@4d0
    const/16 v13, 0x6f

    #@4d2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4d5
    .line 2049
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@4d7
    const/16 v12, 0x14d

    #@4d9
    const/16 v13, 0x6f

    #@4db
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4de
    .line 2050
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@4e0
    const/16 v12, 0x14f

    #@4e2
    const/16 v13, 0x6f

    #@4e4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4e7
    .line 2051
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@4e9
    const/16 v12, 0x151

    #@4eb
    const/16 v13, 0x6f

    #@4ed
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4f0
    .line 2052
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@4f2
    const/16 v12, 0x1a1

    #@4f4
    const/16 v13, 0x6f

    #@4f6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@4f9
    .line 2053
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@4fb
    const/16 v12, 0x1d2

    #@4fd
    const/16 v13, 0x6f

    #@4ff
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@502
    .line 2054
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@504
    const/16 v12, 0x1eb

    #@506
    const/16 v13, 0x6f

    #@508
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@50b
    .line 2055
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@50d
    const/16 v12, 0x1ed

    #@50f
    const/16 v13, 0x6f

    #@511
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@514
    .line 2056
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@516
    const/16 v12, 0x20d

    #@518
    const/16 v13, 0x6f

    #@51a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@51d
    .line 2057
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@51f
    const/16 v12, 0x20f

    #@521
    const/16 v13, 0x6f

    #@523
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@526
    .line 2058
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@528
    const/16 v12, 0x22b

    #@52a
    const/16 v13, 0x6f

    #@52c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@52f
    .line 2059
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@531
    const/16 v12, 0x22d

    #@533
    const/16 v13, 0x6f

    #@535
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@538
    .line 2060
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@53a
    const/16 v12, 0x22f

    #@53c
    const/16 v13, 0x6f

    #@53e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@541
    .line 2061
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@543
    const/16 v12, 0x231

    #@545
    const/16 v13, 0x6f

    #@547
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@54a
    .line 2062
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@54c
    const/16 v12, 0x153

    #@54e
    const/16 v13, 0x6f

    #@550
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@553
    .line 2063
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@555
    const/16 v12, 0x1a5

    #@557
    const/16 v13, 0x70

    #@559
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@55c
    .line 2064
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@55e
    const/16 v12, 0x155

    #@560
    const/16 v13, 0x72

    #@562
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@565
    .line 2065
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@567
    const/16 v12, 0x157

    #@569
    const/16 v13, 0x72

    #@56b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@56e
    .line 2066
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@570
    const/16 v12, 0x159

    #@572
    const/16 v13, 0x72

    #@574
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@577
    .line 2067
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@579
    const/16 v12, 0x211

    #@57b
    const/16 v13, 0x72

    #@57d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@580
    .line 2068
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@582
    const/16 v12, 0x213

    #@584
    const/16 v13, 0x72

    #@586
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@589
    .line 2069
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@58b
    const/16 v12, 0x15b

    #@58d
    const/16 v13, 0x73

    #@58f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@592
    .line 2070
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@594
    const/16 v12, 0x15d

    #@596
    const/16 v13, 0x73

    #@598
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@59b
    .line 2071
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@59d
    const/16 v12, 0x15f

    #@59f
    const/16 v13, 0x73

    #@5a1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5a4
    .line 2072
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@5a6
    const/16 v12, 0x161

    #@5a8
    const/16 v13, 0x73

    #@5aa
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5ad
    .line 2073
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@5af
    const/16 v12, 0x1a8

    #@5b1
    const/16 v13, 0x73

    #@5b3
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5b6
    .line 2074
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@5b8
    const/16 v12, 0x219

    #@5ba
    const/16 v13, 0x73

    #@5bc
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5bf
    .line 2075
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@5c1
    const/16 v12, 0x163

    #@5c3
    const/16 v13, 0x74

    #@5c5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5c8
    .line 2076
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@5ca
    const/16 v12, 0x165

    #@5cc
    const/16 v13, 0x74

    #@5ce
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5d1
    .line 2077
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@5d3
    const/16 v12, 0x167

    #@5d5
    const/16 v13, 0x74

    #@5d7
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5da
    .line 2078
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@5dc
    const/16 v12, 0x1ab

    #@5de
    const/16 v13, 0x74

    #@5e0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5e3
    .line 2079
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@5e5
    const/16 v12, 0x1ad

    #@5e7
    const/16 v13, 0x74

    #@5e9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5ec
    .line 2080
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@5ee
    const/16 v12, 0x21b

    #@5f0
    const/16 v13, 0x74

    #@5f2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5f5
    .line 2081
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@5f7
    const/16 v12, 0x236

    #@5f9
    const/16 v13, 0x74

    #@5fb
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@5fe
    .line 2082
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@600
    const/16 v12, 0xfa

    #@602
    const/16 v13, 0x75

    #@604
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@607
    .line 2083
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@609
    const/16 v12, 0xfb

    #@60b
    const/16 v13, 0x75

    #@60d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@610
    .line 2084
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@612
    const/16 v12, 0x169

    #@614
    const/16 v13, 0x75

    #@616
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@619
    .line 2085
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@61b
    const/16 v12, 0x16b

    #@61d
    const/16 v13, 0x75

    #@61f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@622
    .line 2086
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@624
    const/16 v12, 0x16d

    #@626
    const/16 v13, 0x75

    #@628
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@62b
    .line 2087
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@62d
    const/16 v12, 0x16f

    #@62f
    const/16 v13, 0x75

    #@631
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@634
    .line 2088
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@636
    const/16 v12, 0x171

    #@638
    const/16 v13, 0x75

    #@63a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@63d
    .line 2089
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@63f
    const/16 v12, 0x173

    #@641
    const/16 v13, 0x75

    #@643
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@646
    .line 2090
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@648
    const/16 v12, 0x1b0

    #@64a
    const/16 v13, 0x75

    #@64c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@64f
    .line 2091
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@651
    const/16 v12, 0x1d4

    #@653
    const/16 v13, 0x75

    #@655
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@658
    .line 2092
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@65a
    const/16 v12, 0x1d6

    #@65c
    const/16 v13, 0x75

    #@65e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@661
    .line 2093
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@663
    const/16 v12, 0x1d8

    #@665
    const/16 v13, 0x75

    #@667
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@66a
    .line 2094
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@66c
    const/16 v12, 0x1da

    #@66e
    const/16 v13, 0x75

    #@670
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@673
    .line 2095
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@675
    const/16 v12, 0x1dc

    #@677
    const/16 v13, 0x75

    #@679
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@67c
    .line 2096
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@67e
    const/16 v12, 0x215

    #@680
    const/16 v13, 0x75

    #@682
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@685
    .line 2097
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@687
    const/16 v12, 0x217

    #@689
    const/16 v13, 0x75

    #@68b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@68e
    .line 2098
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@690
    const/16 v12, 0x1b4

    #@692
    const/16 v13, 0x76

    #@694
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@697
    .line 2099
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@699
    const/16 v12, 0x175

    #@69b
    const/16 v13, 0x77

    #@69d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6a0
    .line 2100
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@6a2
    const/16 v12, 0xfd

    #@6a4
    const/16 v13, 0x79

    #@6a6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6a9
    .line 2101
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@6ab
    const/16 v12, 0xff

    #@6ad
    const/16 v13, 0x79

    #@6af
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6b2
    .line 2102
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@6b4
    const/16 v12, 0x177

    #@6b6
    const/16 v13, 0x79

    #@6b8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6bb
    .line 2103
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@6bd
    const/16 v12, 0x233

    #@6bf
    const/16 v13, 0x79

    #@6c1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6c4
    .line 2104
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@6c6
    const/16 v12, 0x17a

    #@6c8
    const/16 v13, 0x7a

    #@6ca
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6cd
    .line 2105
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@6cf
    const/16 v12, 0x17c

    #@6d1
    const/16 v13, 0x7a

    #@6d3
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6d6
    .line 2106
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@6d8
    const/16 v12, 0x17e

    #@6da
    const/16 v13, 0x7a

    #@6dc
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6df
    .line 2107
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@6e1
    const/16 v12, 0x1b6

    #@6e3
    const/16 v13, 0x7a

    #@6e5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6e8
    .line 2108
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@6ea
    const/16 v12, 0x225

    #@6ec
    const/16 v13, 0x7a

    #@6ee
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6f1
    .line 2109
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmBasicLatin:Landroid/util/SparseIntArray;

    #@6f3
    const/16 v12, 0x60

    #@6f5
    const/16 v13, 0x27

    #@6f7
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@6fa
    .line 2110
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@6fc
    const/16 v12, 0xc0

    #@6fe
    const/16 v13, 0x41

    #@700
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@703
    .line 2111
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@705
    const/16 v12, 0xc1

    #@707
    const/16 v13, 0x41

    #@709
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@70c
    .line 2112
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@70e
    const/16 v12, 0xc2

    #@710
    const/16 v13, 0x41

    #@712
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@715
    .line 2113
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@717
    const/16 v12, 0xc3

    #@719
    const/16 v13, 0x41

    #@71b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@71e
    .line 2114
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@720
    const/16 v12, 0x100

    #@722
    const/16 v13, 0x41

    #@724
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@727
    .line 2115
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@729
    const/16 v12, 0x102

    #@72b
    const/16 v13, 0x41

    #@72d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@730
    .line 2116
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@732
    const/16 v12, 0x104

    #@734
    const/16 v13, 0x41

    #@736
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@739
    .line 2117
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@73b
    const/16 v12, 0x1cd

    #@73d
    const/16 v13, 0x41

    #@73f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@742
    .line 2118
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@744
    const/16 v12, 0x1de

    #@746
    const/16 v13, 0x41

    #@748
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@74b
    .line 2119
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@74d
    const/16 v12, 0x1e0

    #@74f
    const/16 v13, 0x41

    #@751
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@754
    .line 2120
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@756
    const/16 v12, 0x1fa

    #@758
    const/16 v13, 0x41

    #@75a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@75d
    .line 2121
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@75f
    const/16 v12, 0x200

    #@761
    const/16 v13, 0x41

    #@763
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@766
    .line 2122
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@768
    const/16 v12, 0x202

    #@76a
    const/16 v13, 0x41

    #@76c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@76f
    .line 2123
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@771
    const/16 v12, 0x226

    #@773
    const/16 v13, 0x41

    #@775
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@778
    .line 2124
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@77a
    const/16 v12, 0x181

    #@77c
    const/16 v13, 0x42

    #@77e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@781
    .line 2125
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@783
    const/16 v12, 0x106

    #@785
    const/16 v13, 0x43

    #@787
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@78a
    .line 2126
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@78c
    const/16 v12, 0x108

    #@78e
    const/16 v13, 0x43

    #@790
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@793
    .line 2127
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@795
    const/16 v12, 0x10a

    #@797
    const/16 v13, 0x43

    #@799
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@79c
    .line 2128
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@79e
    const/16 v12, 0x10c

    #@7a0
    const/16 v13, 0x43

    #@7a2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7a5
    .line 2129
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@7a7
    const/16 v12, 0x186

    #@7a9
    const/16 v13, 0x43

    #@7ab
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7ae
    .line 2130
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@7b0
    const/16 v12, 0x187

    #@7b2
    const/16 v13, 0x43

    #@7b4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7b7
    .line 2131
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@7b9
    const/16 v12, 0xd0

    #@7bb
    const/16 v13, 0x44

    #@7bd
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7c0
    .line 2132
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@7c2
    const/16 v12, 0x10e

    #@7c4
    const/16 v13, 0x44

    #@7c6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7c9
    .line 2133
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@7cb
    const/16 v12, 0x110

    #@7cd
    const/16 v13, 0x44

    #@7cf
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7d2
    .line 2134
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@7d4
    const/16 v12, 0x189

    #@7d6
    const/16 v13, 0x44

    #@7d8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7db
    .line 2135
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@7dd
    const/16 v12, 0x18a

    #@7df
    const/16 v13, 0x44

    #@7e1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7e4
    .line 2136
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@7e6
    const/16 v12, 0xc8

    #@7e8
    const/16 v13, 0x45

    #@7ea
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7ed
    .line 2137
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@7ef
    const/16 v12, 0xca

    #@7f1
    const/16 v13, 0x45

    #@7f3
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7f6
    .line 2138
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@7f8
    const/16 v12, 0xcb

    #@7fa
    const/16 v13, 0x45

    #@7fc
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@7ff
    .line 2139
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@801
    const/16 v12, 0x112

    #@803
    const/16 v13, 0x45

    #@805
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@808
    .line 2140
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@80a
    const/16 v12, 0x114

    #@80c
    const/16 v13, 0x45

    #@80e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@811
    .line 2141
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@813
    const/16 v12, 0x116

    #@815
    const/16 v13, 0x45

    #@817
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@81a
    .line 2142
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@81c
    const/16 v12, 0x118

    #@81e
    const/16 v13, 0x45

    #@820
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@823
    .line 2143
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@825
    const/16 v12, 0x11a

    #@827
    const/16 v13, 0x45

    #@829
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@82c
    .line 2144
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@82e
    const/16 v12, 0x18e

    #@830
    const/16 v13, 0x45

    #@832
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@835
    .line 2145
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@837
    const/16 v12, 0x204

    #@839
    const/16 v13, 0x45

    #@83b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@83e
    .line 2146
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@840
    const/16 v12, 0x206

    #@842
    const/16 v13, 0x45

    #@844
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@847
    .line 2147
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@849
    const/16 v12, 0x228

    #@84b
    const/16 v13, 0x45

    #@84d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@850
    .line 2148
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@852
    const/16 v12, 0x191

    #@854
    const/16 v13, 0x46

    #@856
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@859
    .line 2149
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@85b
    const/16 v12, 0x11c

    #@85d
    const/16 v13, 0x47

    #@85f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@862
    .line 2150
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@864
    const/16 v12, 0x11e

    #@866
    const/16 v13, 0x47

    #@868
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@86b
    .line 2151
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@86d
    const/16 v12, 0x120

    #@86f
    const/16 v13, 0x47

    #@871
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@874
    .line 2152
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@876
    const/16 v12, 0x122

    #@878
    const/16 v13, 0x47

    #@87a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@87d
    .line 2153
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@87f
    const/16 v12, 0x193

    #@881
    const/16 v13, 0x47

    #@883
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@886
    .line 2154
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@888
    const/16 v12, 0x1e4

    #@88a
    const/16 v13, 0x47

    #@88c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@88f
    .line 2155
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@891
    const/16 v12, 0x1e6

    #@893
    const/16 v13, 0x47

    #@895
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@898
    .line 2156
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@89a
    const/16 v12, 0x1f4

    #@89c
    const/16 v13, 0x47

    #@89e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8a1
    .line 2157
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@8a3
    const/16 v12, 0x124

    #@8a5
    const/16 v13, 0x48

    #@8a7
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8aa
    .line 2158
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@8ac
    const/16 v12, 0x126

    #@8ae
    const/16 v13, 0x48

    #@8b0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8b3
    .line 2159
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@8b5
    const/16 v12, 0x1f6

    #@8b7
    const/16 v13, 0x48

    #@8b9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8bc
    .line 2160
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@8be
    const/16 v12, 0x21e

    #@8c0
    const/16 v13, 0x48

    #@8c2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8c5
    .line 2161
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@8c7
    const/16 v12, 0xcc

    #@8c9
    const/16 v13, 0x49

    #@8cb
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8ce
    .line 2162
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@8d0
    const/16 v12, 0xcd

    #@8d2
    const/16 v13, 0x49

    #@8d4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8d7
    .line 2163
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@8d9
    const/16 v12, 0xce

    #@8db
    const/16 v13, 0x49

    #@8dd
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8e0
    .line 2164
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@8e2
    const/16 v12, 0xcf

    #@8e4
    const/16 v13, 0x49

    #@8e6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8e9
    .line 2165
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@8eb
    const/16 v12, 0x128

    #@8ed
    const/16 v13, 0x49

    #@8ef
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8f2
    .line 2166
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@8f4
    const/16 v12, 0x12a

    #@8f6
    const/16 v13, 0x49

    #@8f8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@8fb
    .line 2167
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@8fd
    const/16 v12, 0x12c

    #@8ff
    const/16 v13, 0x49

    #@901
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@904
    .line 2168
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@906
    const/16 v12, 0x12e

    #@908
    const/16 v13, 0x49

    #@90a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@90d
    .line 2169
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@90f
    const/16 v12, 0x130

    #@911
    const/16 v13, 0x49

    #@913
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@916
    .line 2170
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@918
    const/16 v12, 0x197

    #@91a
    const/16 v13, 0x49

    #@91c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@91f
    .line 2171
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@921
    const/16 v12, 0x1cf

    #@923
    const/16 v13, 0x49

    #@925
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@928
    .line 2172
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@92a
    const/16 v12, 0x208

    #@92c
    const/16 v13, 0x49

    #@92e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@931
    .line 2173
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@933
    const/16 v12, 0x20a

    #@935
    const/16 v13, 0x49

    #@937
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@93a
    .line 2174
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@93c
    const/16 v12, 0x134

    #@93e
    const/16 v13, 0x4a

    #@940
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@943
    .line 2175
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@945
    const/16 v12, 0x136

    #@947
    const/16 v13, 0x4b

    #@949
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@94c
    .line 2176
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@94e
    const/16 v12, 0x138

    #@950
    const/16 v13, 0x4b

    #@952
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@955
    .line 2177
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@957
    const/16 v12, 0x198

    #@959
    const/16 v13, 0x4b

    #@95b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@95e
    .line 2178
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@960
    const/16 v12, 0x1e8

    #@962
    const/16 v13, 0x4b

    #@964
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@967
    .line 2179
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@969
    const/16 v12, 0x139

    #@96b
    const/16 v13, 0x4c

    #@96d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@970
    .line 2180
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@972
    const/16 v12, 0x13b

    #@974
    const/16 v13, 0x4c

    #@976
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@979
    .line 2181
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@97b
    const/16 v12, 0x13d

    #@97d
    const/16 v13, 0x4c

    #@97f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@982
    .line 2182
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@984
    const/16 v12, 0x13f

    #@986
    const/16 v13, 0x4c

    #@988
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@98b
    .line 2183
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@98d
    const/16 v12, 0x141

    #@98f
    const/16 v13, 0x4c

    #@991
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@994
    .line 2184
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@996
    const/16 v12, 0x143

    #@998
    const/16 v13, 0x4e

    #@99a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@99d
    .line 2185
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@99f
    const/16 v12, 0x145

    #@9a1
    const/16 v13, 0x4e

    #@9a3
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9a6
    .line 2186
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@9a8
    const/16 v12, 0x147

    #@9aa
    const/16 v13, 0x4e

    #@9ac
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9af
    .line 2187
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@9b1
    const/16 v12, 0x14a

    #@9b3
    const/16 v13, 0x4e

    #@9b5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9b8
    .line 2188
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@9ba
    const/16 v12, 0x19d

    #@9bc
    const/16 v13, 0x4e

    #@9be
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9c1
    .line 2189
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@9c3
    const/16 v12, 0x1f8

    #@9c5
    const/16 v13, 0x4e

    #@9c7
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9ca
    .line 2190
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@9cc
    const/16 v12, 0xd2

    #@9ce
    const/16 v13, 0x4f

    #@9d0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9d3
    .line 2191
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@9d5
    const/16 v12, 0xd3

    #@9d7
    const/16 v13, 0x4f

    #@9d9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9dc
    .line 2192
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@9de
    const/16 v12, 0xd4

    #@9e0
    const/16 v13, 0x4f

    #@9e2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9e5
    .line 2193
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@9e7
    const/16 v12, 0xd5

    #@9e9
    const/16 v13, 0x4f

    #@9eb
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9ee
    .line 2194
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@9f0
    const/16 v12, 0x14c

    #@9f2
    const/16 v13, 0x4f

    #@9f4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@9f7
    .line 2195
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@9f9
    const/16 v12, 0x14e

    #@9fb
    const/16 v13, 0x4f

    #@9fd
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a00
    .line 2196
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@a02
    const/16 v12, 0x150

    #@a04
    const/16 v13, 0x4f

    #@a06
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a09
    .line 2197
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a0b
    const/16 v12, 0x1a0

    #@a0d
    const/16 v13, 0x4f

    #@a0f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a12
    .line 2198
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a14
    const/16 v12, 0x1d1

    #@a16
    const/16 v13, 0x4f

    #@a18
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a1b
    .line 2199
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a1d
    const/16 v12, 0x1ea

    #@a1f
    const/16 v13, 0x4f

    #@a21
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a24
    .line 2200
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a26
    const/16 v12, 0x1ec

    #@a28
    const/16 v13, 0x4f

    #@a2a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a2d
    .line 2201
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a2f
    const/16 v12, 0x20c

    #@a31
    const/16 v13, 0x4f

    #@a33
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a36
    .line 2202
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a38
    const/16 v12, 0x20e

    #@a3a
    const/16 v13, 0x4f

    #@a3c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a3f
    .line 2203
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@a41
    const/16 v12, 0x152

    #@a43
    const/16 v13, 0x4f

    #@a45
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a48
    .line 2204
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a4a
    const/16 v12, 0x1a4

    #@a4c
    const/16 v13, 0x50

    #@a4e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a51
    .line 2205
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a53
    const/16 v12, 0x22a

    #@a55
    const/16 v13, 0x50

    #@a57
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a5a
    .line 2206
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a5c
    const/16 v12, 0x22c

    #@a5e
    const/16 v13, 0x50

    #@a60
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a63
    .line 2207
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a65
    const/16 v12, 0x22e

    #@a67
    const/16 v13, 0x50

    #@a69
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a6c
    .line 2208
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a6e
    const/16 v12, 0x230

    #@a70
    const/16 v13, 0x50

    #@a72
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a75
    .line 2209
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@a77
    const/16 v12, 0x154

    #@a79
    const/16 v13, 0x52

    #@a7b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a7e
    .line 2210
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@a80
    const/16 v12, 0x156

    #@a82
    const/16 v13, 0x52

    #@a84
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a87
    .line 2211
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@a89
    const/16 v12, 0x158

    #@a8b
    const/16 v13, 0x52

    #@a8d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a90
    .line 2212
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a92
    const/16 v12, 0x1a6

    #@a94
    const/16 v13, 0x52

    #@a96
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@a99
    .line 2213
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@a9b
    const/16 v12, 0x210

    #@a9d
    const/16 v13, 0x52

    #@a9f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@aa2
    .line 2214
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@aa4
    const/16 v12, 0x212

    #@aa6
    const/16 v13, 0x52

    #@aa8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@aab
    .line 2215
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@aad
    const/16 v12, 0x15a

    #@aaf
    const/16 v13, 0x53

    #@ab1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ab4
    .line 2216
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@ab6
    const/16 v12, 0x15c

    #@ab8
    const/16 v13, 0x53

    #@aba
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@abd
    .line 2217
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@abf
    const/16 v12, 0x15e

    #@ac1
    const/16 v13, 0x53

    #@ac3
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ac6
    .line 2218
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@ac8
    const/16 v12, 0x160

    #@aca
    const/16 v13, 0x53

    #@acc
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@acf
    .line 2219
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@ad1
    const/16 v12, 0x1a7

    #@ad3
    const/16 v13, 0x53

    #@ad5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ad8
    .line 2220
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@ada
    const/16 v12, 0x218

    #@adc
    const/16 v13, 0x53

    #@ade
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ae1
    .line 2221
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@ae3
    const/16 v12, 0x162

    #@ae5
    const/16 v13, 0x54

    #@ae7
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@aea
    .line 2222
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@aec
    const/16 v12, 0x164

    #@aee
    const/16 v13, 0x54

    #@af0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@af3
    .line 2223
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@af5
    const/16 v12, 0x166

    #@af7
    const/16 v13, 0x54

    #@af9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@afc
    .line 2224
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@afe
    const/16 v12, 0x1ac

    #@b00
    const/16 v13, 0x54

    #@b02
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b05
    .line 2225
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b07
    const/16 v12, 0x1ae

    #@b09
    const/16 v13, 0x54

    #@b0b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b0e
    .line 2226
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b10
    const/16 v12, 0x21a

    #@b12
    const/16 v13, 0x54

    #@b14
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b17
    .line 2227
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@b19
    const/16 v12, 0xd9

    #@b1b
    const/16 v13, 0x55

    #@b1d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b20
    .line 2228
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@b22
    const/16 v12, 0xda

    #@b24
    const/16 v13, 0x55

    #@b26
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b29
    .line 2229
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@b2b
    const/16 v12, 0xdb

    #@b2d
    const/16 v13, 0x55

    #@b2f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b32
    .line 2230
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@b34
    const/16 v12, 0x168

    #@b36
    const/16 v13, 0x55

    #@b38
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b3b
    .line 2231
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@b3d
    const/16 v12, 0x16a

    #@b3f
    const/16 v13, 0x55

    #@b41
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b44
    .line 2232
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@b46
    const/16 v12, 0x16c

    #@b48
    const/16 v13, 0x55

    #@b4a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b4d
    .line 2233
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@b4f
    const/16 v12, 0x16e

    #@b51
    const/16 v13, 0x55

    #@b53
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b56
    .line 2234
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@b58
    const/16 v12, 0x170

    #@b5a
    const/16 v13, 0x55

    #@b5c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b5f
    .line 2235
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@b61
    const/16 v12, 0x172

    #@b63
    const/16 v13, 0x55

    #@b65
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b68
    .line 2236
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b6a
    const/16 v12, 0x1b1

    #@b6c
    const/16 v13, 0x55

    #@b6e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b71
    .line 2237
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b73
    const/16 v12, 0x1b2

    #@b75
    const/16 v13, 0x55

    #@b77
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b7a
    .line 2238
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b7c
    const/16 v12, 0x1d3

    #@b7e
    const/16 v13, 0x55

    #@b80
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b83
    .line 2239
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b85
    const/16 v12, 0x1d5

    #@b87
    const/16 v13, 0x55

    #@b89
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b8c
    .line 2240
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b8e
    const/16 v12, 0x1d7

    #@b90
    const/16 v13, 0x55

    #@b92
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b95
    .line 2241
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@b97
    const/16 v12, 0x1d9

    #@b99
    const/16 v13, 0x55

    #@b9b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@b9e
    .line 2242
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@ba0
    const/16 v12, 0x1db

    #@ba2
    const/16 v13, 0x55

    #@ba4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ba7
    .line 2243
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@ba9
    const/16 v12, 0x214

    #@bab
    const/16 v13, 0x55

    #@bad
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bb0
    .line 2244
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@bb2
    const/16 v12, 0x216

    #@bb4
    const/16 v13, 0x55

    #@bb6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bb9
    .line 2245
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@bbb
    const/16 v12, 0x174

    #@bbd
    const/16 v13, 0x57

    #@bbf
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bc2
    .line 2246
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@bc4
    const/16 v12, 0xdd

    #@bc6
    const/16 v13, 0x59

    #@bc8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bcb
    .line 2247
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@bcd
    const/16 v12, 0x176

    #@bcf
    const/16 v13, 0x59

    #@bd1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bd4
    .line 2248
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@bd6
    const/16 v12, 0x178

    #@bd8
    const/16 v13, 0x59

    #@bda
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bdd
    .line 2249
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@bdf
    const/16 v12, 0x1b3

    #@be1
    const/16 v13, 0x59

    #@be3
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@be6
    .line 2250
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@be8
    const/16 v12, 0x232

    #@bea
    const/16 v13, 0x59

    #@bec
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bef
    .line 2251
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@bf1
    const/16 v12, 0x179

    #@bf3
    const/16 v13, 0x5a

    #@bf5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@bf8
    .line 2252
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@bfa
    const/16 v12, 0x17b

    #@bfc
    const/16 v13, 0x5a

    #@bfe
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c01
    .line 2253
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@c03
    const/16 v12, 0x17d

    #@c05
    const/16 v13, 0x5a

    #@c07
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c0a
    .line 2254
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@c0c
    const/16 v12, 0x1b5

    #@c0e
    const/16 v13, 0x5a

    #@c10
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c13
    .line 2255
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@c15
    const/16 v12, 0x224

    #@c17
    const/16 v13, 0x5a

    #@c19
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c1c
    .line 2256
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c1e
    const/16 v12, 0x391

    #@c20
    const/16 v13, 0x41

    #@c22
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c25
    .line 2257
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c27
    const/16 v12, 0x386

    #@c29
    const/16 v13, 0x41

    #@c2b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c2e
    .line 2258
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c30
    const/16 v12, 0x3b1

    #@c32
    const/16 v13, 0x41

    #@c34
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c37
    .line 2259
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c39
    const/16 v12, 0x3ac

    #@c3b
    const/16 v13, 0x41

    #@c3d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c40
    .line 2260
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c42
    const/16 v12, 0x392

    #@c44
    const/16 v13, 0x42

    #@c46
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c49
    .line 2261
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c4b
    const/16 v12, 0x3b2

    #@c4d
    const/16 v13, 0x42

    #@c4f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c52
    .line 2262
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c54
    const/16 v12, 0x388

    #@c56
    const/16 v13, 0x45

    #@c58
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c5b
    .line 2263
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c5d
    const/16 v12, 0x395

    #@c5f
    const/16 v13, 0x45

    #@c61
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c64
    .line 2264
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c66
    const/16 v12, 0x3b5

    #@c68
    const/16 v13, 0x45

    #@c6a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c6d
    .line 2265
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c6f
    const/16 v12, 0x3ad

    #@c71
    const/16 v13, 0x45

    #@c73
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c76
    .line 2266
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c78
    const/16 v12, 0x389

    #@c7a
    const/16 v13, 0x48

    #@c7c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c7f
    .line 2267
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c81
    const/16 v12, 0x397

    #@c83
    const/16 v13, 0x48

    #@c85
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c88
    .line 2268
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c8a
    const/16 v12, 0x3b7

    #@c8c
    const/16 v13, 0x48

    #@c8e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c91
    .line 2269
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c93
    const/16 v12, 0x3ae

    #@c95
    const/16 v13, 0x48

    #@c97
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@c9a
    .line 2270
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@c9c
    const/16 v12, 0x399

    #@c9e
    const/16 v13, 0x49

    #@ca0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ca3
    .line 2271
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@ca5
    const/16 v12, 0x38a

    #@ca7
    const/16 v13, 0x49

    #@ca9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cac
    .line 2272
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cae
    const/16 v12, 0x3aa

    #@cb0
    const/16 v13, 0x49

    #@cb2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cb5
    .line 2273
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cb7
    const/16 v12, 0x3b9

    #@cb9
    const/16 v13, 0x49

    #@cbb
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cbe
    .line 2274
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cc0
    const/16 v12, 0x3af

    #@cc2
    const/16 v13, 0x49

    #@cc4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cc7
    .line 2275
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cc9
    const/16 v12, 0x3ca

    #@ccb
    const/16 v13, 0x49

    #@ccd
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cd0
    .line 2276
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cd2
    const/16 v12, 0x39a

    #@cd4
    const/16 v13, 0x4b

    #@cd6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cd9
    .line 2277
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cdb
    const/16 v12, 0x3ba

    #@cdd
    const/16 v13, 0x4b

    #@cdf
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ce2
    .line 2278
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@ce4
    const/16 v12, 0x39c

    #@ce6
    const/16 v13, 0x4d

    #@ce8
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@ceb
    .line 2279
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@ced
    const/16 v12, 0x3bc

    #@cef
    const/16 v13, 0x4d

    #@cf1
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cf4
    .line 2280
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cf6
    const/16 v12, 0x39d

    #@cf8
    const/16 v13, 0x4e

    #@cfa
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@cfd
    .line 2281
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@cff
    const/16 v12, 0x3bd

    #@d01
    const/16 v13, 0x4e

    #@d03
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d06
    .line 2282
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d08
    const/16 v12, 0x39f

    #@d0a
    const/16 v13, 0x4f

    #@d0c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d0f
    .line 2283
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d11
    const/16 v12, 0x38c

    #@d13
    const/16 v13, 0x4f

    #@d15
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d18
    .line 2284
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d1a
    const/16 v12, 0x3bf

    #@d1c
    const/16 v13, 0x4f

    #@d1e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d21
    .line 2285
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d23
    const/16 v12, 0x3cc

    #@d25
    const/16 v13, 0x4f

    #@d27
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d2a
    .line 2286
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d2c
    const/16 v12, 0x3a1

    #@d2e
    const/16 v13, 0x50

    #@d30
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d33
    .line 2287
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d35
    const/16 v12, 0x3c1

    #@d37
    const/16 v13, 0x50

    #@d39
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d3c
    .line 2288
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d3e
    const/16 v12, 0x3a4

    #@d40
    const/16 v13, 0x54

    #@d42
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d45
    .line 2289
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d47
    const/16 v12, 0x3c4

    #@d49
    const/16 v13, 0x54

    #@d4b
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d4e
    .line 2290
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d50
    const/16 v12, 0x3a7

    #@d52
    const/16 v13, 0x58

    #@d54
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d57
    .line 2291
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d59
    const/16 v12, 0x3c7

    #@d5b
    const/16 v13, 0x58

    #@d5d
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d60
    .line 2292
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d62
    const/16 v12, 0x3a5

    #@d64
    const/16 v13, 0x59

    #@d66
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d69
    .line 2293
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d6b
    const/16 v12, 0x38e

    #@d6d
    const/16 v13, 0x59

    #@d6f
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d72
    .line 2294
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d74
    const/16 v12, 0x3ab

    #@d76
    const/16 v13, 0x59

    #@d78
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d7b
    .line 2295
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d7d
    const/16 v12, 0x3c5

    #@d7f
    const/16 v13, 0x59

    #@d81
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d84
    .line 2296
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d86
    const/16 v12, 0x3cd

    #@d88
    const/16 v13, 0x59

    #@d8a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d8d
    .line 2297
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d8f
    const/16 v12, 0x3cb

    #@d91
    const/16 v13, 0x59

    #@d93
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d96
    .line 2298
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@d98
    const/16 v12, 0x396

    #@d9a
    const/16 v13, 0x5a

    #@d9c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@d9f
    .line 2299
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@da1
    const/16 v12, 0x3b6

    #@da3
    const/16 v13, 0x5a

    #@da5
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@da8
    .line 2300
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@daa
    const/16 v12, 0x3b4

    #@dac
    const/16 v13, 0x10

    #@dae
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@db1
    .line 2301
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@db3
    const/16 v12, 0x3c6

    #@db5
    const/16 v13, 0x12

    #@db7
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@dba
    .line 2302
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@dbc
    const/16 v12, 0x3b3

    #@dbe
    const/16 v13, 0x13

    #@dc0
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@dc3
    .line 2303
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@dc5
    const/16 v12, 0x3bb

    #@dc7
    const/16 v13, 0x14

    #@dc9
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@dcc
    .line 2304
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@dce
    const/16 v12, 0x38f

    #@dd0
    const/16 v13, 0x15

    #@dd2
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@dd5
    .line 2305
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@dd7
    const/16 v12, 0x3c9

    #@dd9
    const/16 v13, 0x15

    #@ddb
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@dde
    .line 2306
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@de0
    const/16 v12, 0x3ce

    #@de2
    const/16 v13, 0x15

    #@de4
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@de7
    .line 2307
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@de9
    const/16 v12, 0x3c0

    #@deb
    const/16 v13, 0x16

    #@ded
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@df0
    .line 2308
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@df2
    const/16 v12, 0x3c8

    #@df4
    const/16 v13, 0x17

    #@df6
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@df9
    .line 2309
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@dfb
    const/16 v12, 0x3c3

    #@dfd
    const/16 v13, 0x18

    #@dff
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e02
    .line 2310
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@e04
    const/16 v12, 0x3c2

    #@e06
    const/16 v13, 0x18

    #@e08
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e0b
    .line 2311
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@e0d
    const/16 v12, 0x3b8

    #@e0f
    const/16 v13, 0x19

    #@e11
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e14
    .line 2312
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@e16
    const/16 v12, 0x3be

    #@e18
    const/16 v13, 0x1a

    #@e1a
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e1d
    .line 2314
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGeneralPunctuation:Landroid/util/SparseIntArray;

    #@e1f
    const/16 v12, 0x201c

    #@e21
    const/16 v13, 0x22

    #@e23
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e26
    .line 2315
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGeneralPunctuation:Landroid/util/SparseIntArray;

    #@e28
    const/16 v12, 0x2026

    #@e2a
    const/16 v13, 0x2e

    #@e2c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e2f
    .line 2316
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@e31
    const/16 v12, 0xb0

    #@e33
    const/16 v13, 0x6f

    #@e35
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e38
    .line 2317
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@e3a
    const/16 v12, 0xb6

    #@e3c
    const/16 v13, 0x20

    #@e3e
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e41
    .line 2318
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@e43
    const/16 v12, 0xb7

    #@e45
    const/16 v13, 0x2e

    #@e47
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e4a
    .line 2319
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmCyrillic:Landroid/util/SparseIntArray;

    #@e4c
    const/16 v12, 0x413

    #@e4e
    const/16 v13, 0x13

    #@e50
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e53
    .line 2320
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmCyrillic:Landroid/util/SparseIntArray;

    #@e55
    const/16 v12, 0x424

    #@e57
    const/16 v13, 0x12

    #@e59
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e5c
    .line 2321
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmCyrillic:Landroid/util/SparseIntArray;

    #@e5e
    const/16 v12, 0x401

    #@e60
    const/16 v13, 0x45

    #@e62
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e65
    .line 2322
    return-void

    #@e66
    .line 1978
    :cond_e66
    sget-object v11, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@e68
    const/16 v12, 0xe7

    #@e6a
    const/16 v13, 0x63

    #@e6c
    invoke-virtual {v11, v12, v13}, Landroid/util/SparseIntArray;->put(II)V

    #@e6f
    goto/16 :goto_271
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000()I
    .registers 1

    #@0
    .prologue
    .line 48
    sget v0, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I

    #@2
    return v0
.end method

.method static synthetic access$100()[I
    .registers 1

    #@0
    .prologue
    .line 48
    sget-object v0, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@2
    return-object v0
.end method

.method public static charToGsm(C)I
    .registers 5
    .parameter "c"

    #@0
    .prologue
    const/16 v3, 0x20

    #@2
    const/4 v2, 0x0

    #@3
    .line 155
    const/4 v1, 0x0

    #@4
    :try_start_4
    invoke-static {p0, v1}, Lcom/android/internal/telephony/GsmAlphabet;->charToGsm(CZ)I
    :try_end_7
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 158
    :goto_8
    return v1

    #@9
    .line 156
    :catch_9
    move-exception v0

    #@a
    .line 158
    .local v0, ex:Lcom/android/internal/telephony/EncodeException;
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@c
    aget-object v1, v1, v2

    #@e
    invoke-virtual {v1, v3, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@11
    move-result v1

    #@12
    goto :goto_8
.end method

.method public static charToGsm(CZ)I
    .registers 7
    .parameter "c"
    .parameter "throwException"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x20

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, -0x1

    #@4
    .line 178
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@6
    aget-object v1, v1, v3

    #@8
    invoke-virtual {v1, p0, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@b
    move-result v0

    #@c
    .line 180
    .local v0, ret:I
    if-ne v0, v2, :cond_2c

    #@e
    .line 181
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@10
    aget-object v1, v1, v3

    #@12
    invoke-virtual {v1, p0, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@15
    move-result v0

    #@16
    .line 183
    if-ne v0, v2, :cond_29

    #@18
    .line 184
    if-eqz p1, :cond_20

    #@1a
    .line 185
    new-instance v1, Lcom/android/internal/telephony/EncodeException;

    #@1c
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/EncodeException;-><init>(C)V

    #@1f
    throw v1

    #@20
    .line 187
    :cond_20
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@22
    aget-object v1, v1, v3

    #@24
    invoke-virtual {v1, v4, v4}, Landroid/util/SparseIntArray;->get(II)I

    #@27
    move-result v1

    #@28
    .line 194
    :goto_28
    return v1

    #@29
    .line 190
    :cond_29
    const/16 v1, 0x1b

    #@2b
    goto :goto_28

    #@2c
    :cond_2c
    move v1, v0

    #@2d
    .line 194
    goto :goto_28
.end method

.method public static charToGsmExtended(C)I
    .registers 6
    .parameter "c"

    #@0
    .prologue
    const/16 v4, 0x20

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, -0x1

    #@4
    .line 208
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@6
    aget-object v1, v1, v3

    #@8
    invoke-virtual {v1, p0, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@b
    move-result v0

    #@c
    .line 210
    .local v0, ret:I
    if-ne v0, v2, :cond_16

    #@e
    .line 211
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@10
    aget-object v1, v1, v3

    #@12
    invoke-virtual {v1, v4, v4}, Landroid/util/SparseIntArray;->get(II)I

    #@15
    move-result v0

    #@16
    .line 214
    .end local v0           #ret:I
    :cond_16
    return v0
.end method

.method public static countGsmSeptets(C)I
    .registers 4
    .parameter "c"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 808
    const/4 v2, 0x0

    #@2
    :try_start_2
    invoke-static {p0, v2}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptets(CZ)I
    :try_end_5
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_2 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 811
    :goto_6
    return v1

    #@7
    .line 809
    :catch_7
    move-exception v0

    #@8
    .line 811
    .local v0, ex:Lcom/android/internal/telephony/EncodeException;
    goto :goto_6
.end method

.method public static countGsmSeptets(CZ)I
    .registers 6
    .parameter "c"
    .parameter "throwsException"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, -0x1

    #@3
    .line 826
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@5
    aget-object v1, v1, v3

    #@7
    invoke-virtual {v1, p0, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@a
    move-result v1

    #@b
    if-eq v1, v2, :cond_e

    #@d
    .line 838
    :cond_d
    :goto_d
    return v0

    #@e
    .line 830
    :cond_e
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@10
    aget-object v1, v1, v3

    #@12
    invoke-virtual {v1, p0, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@15
    move-result v1

    #@16
    if-eq v1, v2, :cond_1a

    #@18
    .line 831
    const/4 v0, 0x2

    #@19
    goto :goto_d

    #@1a
    .line 834
    :cond_1a
    if-eqz p1, :cond_d

    #@1c
    .line 835
    new-instance v0, Lcom/android/internal/telephony/EncodeException;

    #@1e
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/EncodeException;-><init>(C)V

    #@21
    throw v0
.end method

.method public static countGsmSeptets(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 29
    .parameter "s"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 979
    sget-object v25, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@2
    move-object/from16 v0, v25

    #@4
    array-length v0, v0

    #@5
    move/from16 v25, v0

    #@7
    sget-object v26, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@9
    move-object/from16 v0, v26

    #@b
    array-length v0, v0

    #@c
    move/from16 v26, v0

    #@e
    add-int v25, v25, v26

    #@10
    if-nez v25, :cond_82

    #@12
    .line 980
    new-instance v22, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@14
    invoke-direct/range {v22 .. v22}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@17
    .line 981
    .local v22, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const/16 v25, 0x0

    #@19
    const/16 v26, 0x0

    #@1b
    move-object/from16 v0, p0

    #@1d
    move/from16 v1, p1

    #@1f
    move/from16 v2, v25

    #@21
    move/from16 v3, v26

    #@23
    invoke-static {v0, v1, v2, v3}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptetsUsingTables(Ljava/lang/CharSequence;ZII)I

    #@26
    move-result v14

    #@27
    .line 982
    .local v14, septets:I
    const/16 v25, -0x1

    #@29
    move/from16 v0, v25

    #@2b
    if-ne v14, v0, :cond_30

    #@2d
    .line 983
    const/16 v22, 0x0

    #@2f
    .line 1110
    .end local v14           #septets:I
    .end local v22           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_2f
    :goto_2f
    return-object v22

    #@30
    .line 985
    .restart local v14       #septets:I
    .restart local v22       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_30
    const/16 v25, 0x1

    #@32
    move/from16 v0, v25

    #@34
    move-object/from16 v1, v22

    #@36
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@38
    .line 986
    move-object/from16 v0, v22

    #@3a
    iput v14, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@3c
    .line 987
    const/16 v25, 0xa0

    #@3e
    move/from16 v0, v25

    #@40
    if-le v14, v0, :cond_6f

    #@42
    .line 988
    add-int/lit16 v0, v14, 0x98

    #@44
    move/from16 v25, v0

    #@46
    move/from16 v0, v25

    #@48
    div-int/lit16 v0, v0, 0x99

    #@4a
    move/from16 v25, v0

    #@4c
    move/from16 v0, v25

    #@4e
    move-object/from16 v1, v22

    #@50
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@52
    .line 990
    move-object/from16 v0, v22

    #@54
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@56
    move/from16 v25, v0

    #@58
    move/from16 v0, v25

    #@5a
    mul-int/lit16 v0, v0, 0x99

    #@5c
    move/from16 v25, v0

    #@5e
    sub-int v25, v25, v14

    #@60
    move/from16 v0, v25

    #@62
    move-object/from16 v1, v22

    #@64
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@66
    .line 996
    :goto_66
    const/16 v25, 0x1

    #@68
    move/from16 v0, v25

    #@6a
    move-object/from16 v1, v22

    #@6c
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@6e
    goto :goto_2f

    #@6f
    .line 993
    :cond_6f
    const/16 v25, 0x1

    #@71
    move/from16 v0, v25

    #@73
    move-object/from16 v1, v22

    #@75
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@77
    .line 994
    rsub-int v0, v14, 0xa0

    #@79
    move/from16 v25, v0

    #@7b
    move/from16 v0, v25

    #@7d
    move-object/from16 v1, v22

    #@7f
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@81
    goto :goto_66

    #@82
    .line 1000
    .end local v14           #septets:I
    .end local v22           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_82
    sget v11, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I

    #@84
    .line 1001
    .local v11, maxSingleShiftCode:I
    new-instance v10, Ljava/util/ArrayList;

    #@86
    sget-object v25, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@88
    move-object/from16 v0, v25

    #@8a
    array-length v0, v0

    #@8b
    move/from16 v25, v0

    #@8d
    add-int/lit8 v25, v25, 0x1

    #@8f
    move/from16 v0, v25

    #@91
    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@94
    .line 1005
    .local v10, lpcList:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;>;"
    new-instance v25, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@96
    const/16 v26, 0x0

    #@98
    invoke-direct/range {v25 .. v26}, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;-><init>(I)V

    #@9b
    move-object/from16 v0, v25

    #@9d
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a0
    .line 1006
    sget-object v4, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@a2
    .local v4, arr$:[I
    array-length v8, v4

    #@a3
    .local v8, len$:I
    const/4 v7, 0x0

    #@a4
    .local v7, i$:I
    :goto_a4
    if-ge v7, v8, :cond_c3

    #@a6
    aget v6, v4, v7

    #@a8
    .line 1008
    .local v6, i:I
    if-eqz v6, :cond_c0

    #@aa
    sget-object v25, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@ac
    aget-object v25, v25, v6

    #@ae
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    #@b1
    move-result v25

    #@b2
    if-nez v25, :cond_c0

    #@b4
    .line 1009
    new-instance v25, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@b6
    move-object/from16 v0, v25

    #@b8
    invoke-direct {v0, v6}, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;-><init>(I)V

    #@bb
    move-object/from16 v0, v25

    #@bd
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@c0
    .line 1006
    :cond_c0
    add-int/lit8 v7, v7, 0x1

    #@c2
    goto :goto_a4

    #@c3
    .line 1013
    .end local v6           #i:I
    :cond_c3
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    #@c6
    move-result v19

    #@c7
    .line 1015
    .local v19, sz:I
    const/4 v6, 0x0

    #@c8
    .end local v7           #i$:I
    .restart local v6       #i:I
    :goto_c8
    move/from16 v0, v19

    #@ca
    if-ge v6, v0, :cond_18a

    #@cc
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    #@cf
    move-result v25

    #@d0
    if-nez v25, :cond_18a

    #@d2
    .line 1016
    move-object/from16 v0, p0

    #@d4
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@d7
    move-result v5

    #@d8
    .line 1017
    .local v5, c:C
    const/16 v25, 0x1b

    #@da
    move/from16 v0, v25

    #@dc
    if-ne v5, v0, :cond_e8

    #@de
    .line 1018
    const-string v25, "GSM"

    #@e0
    const-string v26, "countGsmSeptets() string contains Escape character, ignoring!"

    #@e2
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 1015
    :cond_e5
    add-int/lit8 v6, v6, 0x1

    #@e7
    goto :goto_c8

    #@e8
    .line 1022
    :cond_e8
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@eb
    move-result-object v7

    #@ec
    .local v7, i$:Ljava/util/Iterator;
    :cond_ec
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@ef
    move-result v25

    #@f0
    if-eqz v25, :cond_e5

    #@f2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f5
    move-result-object v9

    #@f6
    check-cast v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@f8
    .line 1023
    .local v9, lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    sget-object v25, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@fa
    iget v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@fc
    move/from16 v26, v0

    #@fe
    aget-object v25, v25, v26

    #@100
    const/16 v26, -0x1

    #@102
    move-object/from16 v0, v25

    #@104
    move/from16 v1, v26

    #@106
    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->get(II)I

    #@109
    move-result v21

    #@10a
    .line 1024
    .local v21, tableIndex:I
    const/16 v25, -0x1

    #@10c
    move/from16 v0, v21

    #@10e
    move/from16 v1, v25

    #@110
    if-ne v0, v1, :cond_169

    #@112
    .line 1026
    const/16 v20, 0x0

    #@114
    .local v20, table:I
    :goto_114
    move/from16 v0, v20

    #@116
    if-gt v0, v11, :cond_ec

    #@118
    .line 1027
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@11a
    move-object/from16 v25, v0

    #@11c
    aget v25, v25, v20

    #@11e
    const/16 v26, -0x1

    #@120
    move/from16 v0, v25

    #@122
    move/from16 v1, v26

    #@124
    if-eq v0, v1, :cond_152

    #@126
    .line 1028
    sget-object v25, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@128
    aget-object v25, v25, v20

    #@12a
    const/16 v26, -0x1

    #@12c
    move-object/from16 v0, v25

    #@12e
    move/from16 v1, v26

    #@130
    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->get(II)I

    #@133
    move-result v18

    #@134
    .line 1029
    .local v18, shiftTableIndex:I
    const/16 v25, -0x1

    #@136
    move/from16 v0, v18

    #@138
    move/from16 v1, v25

    #@13a
    if-ne v0, v1, :cond_15e

    #@13c
    .line 1030
    if-eqz p1, :cond_155

    #@13e
    .line 1032
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@140
    move-object/from16 v25, v0

    #@142
    aget v26, v25, v20

    #@144
    add-int/lit8 v26, v26, 0x1

    #@146
    aput v26, v25, v20

    #@148
    .line 1033
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->unencodableCounts:[I

    #@14a
    move-object/from16 v25, v0

    #@14c
    aget v26, v25, v20

    #@14e
    add-int/lit8 v26, v26, 0x1

    #@150
    aput v26, v25, v20

    #@152
    .line 1026
    .end local v18           #shiftTableIndex:I
    :cond_152
    :goto_152
    add-int/lit8 v20, v20, 0x1

    #@154
    goto :goto_114

    #@155
    .line 1036
    .restart local v18       #shiftTableIndex:I
    :cond_155
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@157
    move-object/from16 v25, v0

    #@159
    const/16 v26, -0x1

    #@15b
    aput v26, v25, v20

    #@15d
    goto :goto_152

    #@15e
    .line 1040
    :cond_15e
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@160
    move-object/from16 v25, v0

    #@162
    aget v26, v25, v20

    #@164
    add-int/lit8 v26, v26, 0x2

    #@166
    aput v26, v25, v20

    #@168
    goto :goto_152

    #@169
    .line 1046
    .end local v18           #shiftTableIndex:I
    .end local v20           #table:I
    :cond_169
    const/16 v20, 0x0

    #@16b
    .restart local v20       #table:I
    :goto_16b
    move/from16 v0, v20

    #@16d
    if-gt v0, v11, :cond_ec

    #@16f
    .line 1047
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@171
    move-object/from16 v25, v0

    #@173
    aget v25, v25, v20

    #@175
    const/16 v26, -0x1

    #@177
    move/from16 v0, v25

    #@179
    move/from16 v1, v26

    #@17b
    if-eq v0, v1, :cond_187

    #@17d
    .line 1048
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@17f
    move-object/from16 v25, v0

    #@181
    aget v26, v25, v20

    #@183
    add-int/lit8 v26, v26, 0x1

    #@185
    aput v26, v25, v20

    #@187
    .line 1046
    :cond_187
    add-int/lit8 v20, v20, 0x1

    #@189
    goto :goto_16b

    #@18a
    .line 1056
    .end local v5           #c:C
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v9           #lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    .end local v20           #table:I
    .end local v21           #tableIndex:I
    :cond_18a
    new-instance v22, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@18c
    invoke-direct/range {v22 .. v22}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@18f
    .line 1057
    .restart local v22       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const v25, 0x7fffffff

    #@192
    move/from16 v0, v25

    #@194
    move-object/from16 v1, v22

    #@196
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@198
    .line 1058
    const/16 v25, 0x1

    #@19a
    move/from16 v0, v25

    #@19c
    move-object/from16 v1, v22

    #@19e
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@1a0
    .line 1059
    const v12, 0x7fffffff

    #@1a3
    .line 1060
    .local v12, minUnencodableCount:I
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1a6
    move-result-object v7

    #@1a7
    .restart local v7       #i$:Ljava/util/Iterator;
    :cond_1a7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@1aa
    move-result v25

    #@1ab
    if-eqz v25, :cond_25b

    #@1ad
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b0
    move-result-object v9

    #@1b1
    check-cast v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@1b3
    .line 1061
    .restart local v9       #lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    const/16 v17, 0x0

    #@1b5
    .local v17, shiftTable:I
    :goto_1b5
    move/from16 v0, v17

    #@1b7
    if-gt v0, v11, :cond_1a7

    #@1b9
    .line 1062
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@1bb
    move-object/from16 v25, v0

    #@1bd
    aget v14, v25, v17

    #@1bf
    .line 1063
    .restart local v14       #septets:I
    const/16 v25, -0x1

    #@1c1
    move/from16 v0, v25

    #@1c3
    if-ne v14, v0, :cond_1c8

    #@1c5
    .line 1061
    :cond_1c5
    :goto_1c5
    add-int/lit8 v17, v17, 0x1

    #@1c7
    goto :goto_1b5

    #@1c8
    .line 1067
    :cond_1c8
    iget v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@1ca
    move/from16 v25, v0

    #@1cc
    if-eqz v25, :cond_243

    #@1ce
    if-eqz v17, :cond_243

    #@1d0
    .line 1068
    const/16 v23, 0x8

    #@1d2
    .line 1076
    .local v23, udhLength:I
    :goto_1d2
    add-int v25, v14, v23

    #@1d4
    const/16 v26, 0xa0

    #@1d6
    move/from16 v0, v25

    #@1d8
    move/from16 v1, v26

    #@1da
    if-le v0, v1, :cond_251

    #@1dc
    .line 1077
    if-nez v23, :cond_1e0

    #@1de
    .line 1078
    const/16 v23, 0x1

    #@1e0
    .line 1080
    :cond_1e0
    add-int/lit8 v23, v23, 0x6

    #@1e2
    .line 1081
    move/from16 v0, v23

    #@1e4
    rsub-int v15, v0, 0xa0

    #@1e6
    .line 1082
    .local v15, septetsPerMessage:I
    add-int v25, v14, v15

    #@1e8
    add-int/lit8 v25, v25, -0x1

    #@1ea
    div-int v13, v25, v15

    #@1ec
    .line 1083
    .local v13, msgCount:I
    mul-int v25, v13, v15

    #@1ee
    sub-int v16, v25, v14

    #@1f0
    .line 1089
    .end local v15           #septetsPerMessage:I
    .local v16, septetsRemaining:I
    :goto_1f0
    iget-object v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->unencodableCounts:[I

    #@1f2
    move-object/from16 v25, v0

    #@1f4
    aget v24, v25, v17

    #@1f6
    .line 1090
    .local v24, unencodableCount:I
    if-eqz p1, :cond_1fc

    #@1f8
    move/from16 v0, v24

    #@1fa
    if-gt v0, v12, :cond_1c5

    #@1fc
    .line 1093
    :cond_1fc
    if-eqz p1, :cond_202

    #@1fe
    move/from16 v0, v24

    #@200
    if-lt v0, v12, :cond_222

    #@202
    :cond_202
    move-object/from16 v0, v22

    #@204
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@206
    move/from16 v25, v0

    #@208
    move/from16 v0, v25

    #@20a
    if-lt v13, v0, :cond_222

    #@20c
    move-object/from16 v0, v22

    #@20e
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@210
    move/from16 v25, v0

    #@212
    move/from16 v0, v25

    #@214
    if-ne v13, v0, :cond_1c5

    #@216
    move-object/from16 v0, v22

    #@218
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@21a
    move/from16 v25, v0

    #@21c
    move/from16 v0, v16

    #@21e
    move/from16 v1, v25

    #@220
    if-le v0, v1, :cond_1c5

    #@222
    .line 1096
    :cond_222
    move/from16 v12, v24

    #@224
    .line 1097
    move-object/from16 v0, v22

    #@226
    iput v13, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@228
    .line 1098
    move-object/from16 v0, v22

    #@22a
    iput v14, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@22c
    .line 1099
    move/from16 v0, v16

    #@22e
    move-object/from16 v1, v22

    #@230
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@232
    .line 1100
    iget v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@234
    move/from16 v25, v0

    #@236
    move/from16 v0, v25

    #@238
    move-object/from16 v1, v22

    #@23a
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@23c
    .line 1101
    move/from16 v0, v17

    #@23e
    move-object/from16 v1, v22

    #@240
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@242
    goto :goto_1c5

    #@243
    .line 1069
    .end local v13           #msgCount:I
    .end local v16           #septetsRemaining:I
    .end local v23           #udhLength:I
    .end local v24           #unencodableCount:I
    :cond_243
    iget v0, v9, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@245
    move/from16 v25, v0

    #@247
    if-nez v25, :cond_24b

    #@249
    if-eqz v17, :cond_24e

    #@24b
    .line 1070
    :cond_24b
    const/16 v23, 0x5

    #@24d
    .restart local v23       #udhLength:I
    goto :goto_1d2

    #@24e
    .line 1072
    .end local v23           #udhLength:I
    :cond_24e
    const/16 v23, 0x0

    #@250
    .restart local v23       #udhLength:I
    goto :goto_1d2

    #@251
    .line 1085
    :cond_251
    const/4 v13, 0x1

    #@252
    .line 1086
    .restart local v13       #msgCount:I
    move/from16 v0, v23

    #@254
    rsub-int v0, v0, 0xa0

    #@256
    move/from16 v25, v0

    #@258
    sub-int v16, v25, v14

    #@25a
    .restart local v16       #septetsRemaining:I
    goto :goto_1f0

    #@25b
    .line 1106
    .end local v9           #lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    .end local v13           #msgCount:I
    .end local v14           #septets:I
    .end local v16           #septetsRemaining:I
    .end local v17           #shiftTable:I
    .end local v23           #udhLength:I
    :cond_25b
    move-object/from16 v0, v22

    #@25d
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@25f
    move/from16 v25, v0

    #@261
    const v26, 0x7fffffff

    #@264
    move/from16 v0, v25

    #@266
    move/from16 v1, v26

    #@268
    if-ne v0, v1, :cond_2f

    #@26a
    .line 1107
    const/16 v22, 0x0

    #@26c
    goto/16 :goto_2f
.end method

.method public static countGsmSeptetsEx(Ljava/lang/CharSequence;Z)[I
    .registers 7
    .parameter "s"
    .parameter "throwsException"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    .line 1277
    const/4 v4, 0x2

    #@1
    new-array v2, v4, [I

    #@3
    .line 1279
    .local v2, ret:[I
    const/4 v0, 0x0

    #@4
    .line 1280
    .local v0, charIndex:I
    const/4 v3, 0x0

    #@5
    .line 1281
    .local v3, sz:I
    const/4 v1, 0x0

    #@6
    .line 1283
    .local v1, count:I
    if-eqz p0, :cond_c

    #@8
    .line 1284
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@b
    move-result v3

    #@c
    .line 1287
    :cond_c
    :goto_c
    if-ge v0, v3, :cond_1a

    #@e
    .line 1288
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@11
    move-result v4

    #@12
    invoke-static {v4, p1}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptets(CZ)I

    #@15
    move-result v4

    #@16
    add-int/2addr v1, v4

    #@17
    .line 1289
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_c

    #@1a
    .line 1291
    :cond_1a
    const/4 v4, 0x0

    #@1b
    aput v1, v2, v4

    #@1d
    .line 1292
    const/4 v4, 0x1

    #@1e
    aput v0, v2, v4

    #@20
    .line 1293
    return-object v2
.end method

.method public static countGsmSeptetsLossyAuto(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 30
    .parameter "s"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1129
    sget-object v26, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@2
    move-object/from16 v0, v26

    #@4
    array-length v0, v0

    #@5
    move/from16 v26, v0

    #@7
    sget-object v27, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@9
    move-object/from16 v0, v27

    #@b
    array-length v0, v0

    #@c
    move/from16 v27, v0

    #@e
    add-int v26, v26, v27

    #@10
    if-nez v26, :cond_82

    #@12
    .line 1130
    new-instance v23, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@14
    invoke-direct/range {v23 .. v23}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@17
    .line 1131
    .local v23, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const/16 v26, 0x0

    #@19
    const/16 v27, 0x0

    #@1b
    move-object/from16 v0, p0

    #@1d
    move/from16 v1, p1

    #@1f
    move/from16 v2, v26

    #@21
    move/from16 v3, v27

    #@23
    invoke-static {v0, v1, v2, v3}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptetsUsingTablesLossyAuto(Ljava/lang/CharSequence;ZII)I

    #@26
    move-result v15

    #@27
    .line 1132
    .local v15, septets:I
    const/16 v26, -0x1

    #@29
    move/from16 v0, v26

    #@2b
    if-ne v15, v0, :cond_30

    #@2d
    .line 1133
    const/16 v23, 0x0

    #@2f
    .line 1261
    .end local v15           #septets:I
    .end local v23           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_2f
    :goto_2f
    return-object v23

    #@30
    .line 1135
    .restart local v15       #septets:I
    .restart local v23       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_30
    const/16 v26, 0x1

    #@32
    move/from16 v0, v26

    #@34
    move-object/from16 v1, v23

    #@36
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@38
    .line 1136
    move-object/from16 v0, v23

    #@3a
    iput v15, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@3c
    .line 1137
    const/16 v26, 0xa0

    #@3e
    move/from16 v0, v26

    #@40
    if-le v15, v0, :cond_6f

    #@42
    .line 1138
    add-int/lit16 v0, v15, 0x98

    #@44
    move/from16 v26, v0

    #@46
    move/from16 v0, v26

    #@48
    div-int/lit16 v0, v0, 0x99

    #@4a
    move/from16 v26, v0

    #@4c
    move/from16 v0, v26

    #@4e
    move-object/from16 v1, v23

    #@50
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@52
    .line 1140
    move-object/from16 v0, v23

    #@54
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@56
    move/from16 v26, v0

    #@58
    move/from16 v0, v26

    #@5a
    mul-int/lit16 v0, v0, 0x99

    #@5c
    move/from16 v26, v0

    #@5e
    sub-int v26, v26, v15

    #@60
    move/from16 v0, v26

    #@62
    move-object/from16 v1, v23

    #@64
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@66
    .line 1146
    :goto_66
    const/16 v26, 0x1

    #@68
    move/from16 v0, v26

    #@6a
    move-object/from16 v1, v23

    #@6c
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@6e
    goto :goto_2f

    #@6f
    .line 1143
    :cond_6f
    const/16 v26, 0x1

    #@71
    move/from16 v0, v26

    #@73
    move-object/from16 v1, v23

    #@75
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@77
    .line 1144
    rsub-int v0, v15, 0xa0

    #@79
    move/from16 v26, v0

    #@7b
    move/from16 v0, v26

    #@7d
    move-object/from16 v1, v23

    #@7f
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@81
    goto :goto_66

    #@82
    .line 1150
    .end local v15           #septets:I
    .end local v23           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_82
    sget v12, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I

    #@84
    .line 1151
    .local v12, maxSingleShiftCode:I
    new-instance v11, Ljava/util/ArrayList;

    #@86
    sget-object v26, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@88
    move-object/from16 v0, v26

    #@8a
    array-length v0, v0

    #@8b
    move/from16 v26, v0

    #@8d
    add-int/lit8 v26, v26, 0x1

    #@8f
    move/from16 v0, v26

    #@91
    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@94
    .line 1155
    .local v11, lpcList:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;>;"
    new-instance v26, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@96
    const/16 v27, 0x0

    #@98
    invoke-direct/range {v26 .. v27}, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;-><init>(I)V

    #@9b
    move-object/from16 v0, v26

    #@9d
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a0
    .line 1156
    sget-object v4, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@a2
    .local v4, arr$:[I
    array-length v9, v4

    #@a3
    .local v9, len$:I
    const/4 v8, 0x0

    #@a4
    .local v8, i$:I
    :goto_a4
    if-ge v8, v9, :cond_c3

    #@a6
    aget v7, v4, v8

    #@a8
    .line 1158
    .local v7, i:I
    if-eqz v7, :cond_c0

    #@aa
    sget-object v26, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@ac
    aget-object v26, v26, v7

    #@ae
    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    #@b1
    move-result v26

    #@b2
    if-nez v26, :cond_c0

    #@b4
    .line 1159
    new-instance v26, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@b6
    move-object/from16 v0, v26

    #@b8
    invoke-direct {v0, v7}, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;-><init>(I)V

    #@bb
    move-object/from16 v0, v26

    #@bd
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@c0
    .line 1156
    :cond_c0
    add-int/lit8 v8, v8, 0x1

    #@c2
    goto :goto_a4

    #@c3
    .line 1163
    .end local v7           #i:I
    :cond_c3
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    #@c6
    move-result v20

    #@c7
    .line 1165
    .local v20, sz:I
    const/4 v7, 0x0

    #@c8
    .end local v8           #i$:I
    .restart local v7       #i:I
    :goto_c8
    move/from16 v0, v20

    #@ca
    if-ge v7, v0, :cond_196

    #@cc
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    #@cf
    move-result v26

    #@d0
    if-nez v26, :cond_196

    #@d2
    .line 1166
    move-object/from16 v0, p0

    #@d4
    invoke-interface {v0, v7}, Ljava/lang/CharSequence;->charAt(I)C

    #@d7
    move-result v5

    #@d8
    .line 1167
    .local v5, c:C
    const/16 v26, 0x1b

    #@da
    move/from16 v0, v26

    #@dc
    if-ne v5, v0, :cond_e8

    #@de
    .line 1168
    const-string v26, "GSM"

    #@e0
    const-string v27, "countGsmSeptets() string contains Escape character, ignoring!"

    #@e2
    invoke-static/range {v26 .. v27}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 1165
    :cond_e5
    add-int/lit8 v7, v7, 0x1

    #@e7
    goto :goto_c8

    #@e8
    .line 1172
    :cond_e8
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@eb
    move-result-object v8

    #@ec
    .local v8, i$:Ljava/util/Iterator;
    :cond_ec
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@ef
    move-result v26

    #@f0
    if-eqz v26, :cond_e5

    #@f2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f5
    move-result-object v10

    #@f6
    check-cast v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@f8
    .line 1173
    .local v10, lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    sget-object v26, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@fa
    iget v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@fc
    move/from16 v27, v0

    #@fe
    aget-object v26, v26, v27

    #@100
    const/16 v27, -0x1

    #@102
    move-object/from16 v0, v26

    #@104
    move/from16 v1, v27

    #@106
    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->get(II)I

    #@109
    move-result v22

    #@10a
    .line 1174
    .local v22, tableIndex:I
    const/16 v26, -0x1

    #@10c
    move/from16 v0, v22

    #@10e
    move/from16 v1, v26

    #@110
    if-ne v0, v1, :cond_175

    #@112
    .line 1176
    const/16 v21, 0x0

    #@114
    .local v21, table:I
    :goto_114
    move/from16 v0, v21

    #@116
    if-gt v0, v12, :cond_ec

    #@118
    .line 1177
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@11a
    move-object/from16 v26, v0

    #@11c
    aget v26, v26, v21

    #@11e
    const/16 v27, -0x1

    #@120
    move/from16 v0, v26

    #@122
    move/from16 v1, v27

    #@124
    if-eq v0, v1, :cond_15e

    #@126
    .line 1178
    sget-object v26, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@128
    aget-object v26, v26, v21

    #@12a
    const/16 v27, -0x1

    #@12c
    move-object/from16 v0, v26

    #@12e
    move/from16 v1, v27

    #@130
    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->get(II)I

    #@133
    move-result v19

    #@134
    .line 1179
    .local v19, shiftTableIndex:I
    const/16 v26, -0x1

    #@136
    move/from16 v0, v19

    #@138
    move/from16 v1, v26

    #@13a
    if-ne v0, v1, :cond_16a

    #@13c
    .line 1180
    if-eqz p1, :cond_161

    #@13e
    invoke-static {v5}, Lcom/android/internal/telephony/GsmAlphabet;->lookupLossy7bitTable(C)I

    #@141
    move-result v26

    #@142
    const/16 v27, -0x1

    #@144
    move/from16 v0, v26

    #@146
    move/from16 v1, v27

    #@148
    if-eq v0, v1, :cond_161

    #@14a
    .line 1181
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@14c
    move-object/from16 v26, v0

    #@14e
    aget v27, v26, v21

    #@150
    add-int/lit8 v27, v27, 0x1

    #@152
    aput v27, v26, v21

    #@154
    .line 1182
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->unencodableCounts:[I

    #@156
    move-object/from16 v26, v0

    #@158
    aget v27, v26, v21

    #@15a
    add-int/lit8 v27, v27, 0x1

    #@15c
    aput v27, v26, v21

    #@15e
    .line 1176
    .end local v19           #shiftTableIndex:I
    :cond_15e
    :goto_15e
    add-int/lit8 v21, v21, 0x1

    #@160
    goto :goto_114

    #@161
    .line 1185
    .restart local v19       #shiftTableIndex:I
    :cond_161
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@163
    move-object/from16 v26, v0

    #@165
    const/16 v27, -0x1

    #@167
    aput v27, v26, v21

    #@169
    goto :goto_15e

    #@16a
    .line 1189
    :cond_16a
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@16c
    move-object/from16 v26, v0

    #@16e
    aget v27, v26, v21

    #@170
    add-int/lit8 v27, v27, 0x2

    #@172
    aput v27, v26, v21

    #@174
    goto :goto_15e

    #@175
    .line 1195
    .end local v19           #shiftTableIndex:I
    .end local v21           #table:I
    :cond_175
    const/16 v21, 0x0

    #@177
    .restart local v21       #table:I
    :goto_177
    move/from16 v0, v21

    #@179
    if-gt v0, v12, :cond_ec

    #@17b
    .line 1196
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@17d
    move-object/from16 v26, v0

    #@17f
    aget v26, v26, v21

    #@181
    const/16 v27, -0x1

    #@183
    move/from16 v0, v26

    #@185
    move/from16 v1, v27

    #@187
    if-eq v0, v1, :cond_193

    #@189
    .line 1197
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@18b
    move-object/from16 v26, v0

    #@18d
    aget v27, v26, v21

    #@18f
    add-int/lit8 v27, v27, 0x1

    #@191
    aput v27, v26, v21

    #@193
    .line 1195
    :cond_193
    add-int/lit8 v21, v21, 0x1

    #@195
    goto :goto_177

    #@196
    .line 1205
    .end local v5           #c:C
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v10           #lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    .end local v21           #table:I
    .end local v22           #tableIndex:I
    :cond_196
    new-instance v23, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@198
    invoke-direct/range {v23 .. v23}, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;-><init>()V

    #@19b
    .line 1206
    .restart local v23       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    const v26, 0x7fffffff

    #@19e
    move/from16 v0, v26

    #@1a0
    move-object/from16 v1, v23

    #@1a2
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@1a4
    .line 1207
    const/16 v26, 0x1

    #@1a6
    move/from16 v0, v26

    #@1a8
    move-object/from16 v1, v23

    #@1aa
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitSize:I

    #@1ac
    .line 1208
    const v13, 0x7fffffff

    #@1af
    .line 1209
    .local v13, minUnencodableCount:I
    const/4 v6, 0x0

    #@1b0
    .line 1210
    .local v6, count2:I
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1b3
    move-result-object v8

    #@1b4
    .restart local v8       #i$:Ljava/util/Iterator;
    :cond_1b4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@1b7
    move-result v26

    #@1b8
    if-eqz v26, :cond_26a

    #@1ba
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1bd
    move-result-object v10

    #@1be
    check-cast v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;

    #@1c0
    .line 1211
    .restart local v10       #lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    const/16 v18, 0x0

    #@1c2
    .local v18, shiftTable:I
    :goto_1c2
    move/from16 v0, v18

    #@1c4
    if-gt v0, v12, :cond_1b4

    #@1c6
    .line 1212
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@1c8
    move-object/from16 v26, v0

    #@1ca
    aget v15, v26, v18

    #@1cc
    .line 1213
    .restart local v15       #septets:I
    const/16 v26, -0x1

    #@1ce
    move/from16 v0, v26

    #@1d0
    if-ne v15, v0, :cond_1d5

    #@1d2
    .line 1211
    :cond_1d2
    :goto_1d2
    add-int/lit8 v18, v18, 0x1

    #@1d4
    goto :goto_1c2

    #@1d5
    .line 1217
    :cond_1d5
    iget v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@1d7
    move/from16 v26, v0

    #@1d9
    if-eqz v26, :cond_252

    #@1db
    if-eqz v18, :cond_252

    #@1dd
    .line 1218
    const/16 v24, 0x8

    #@1df
    .line 1227
    .local v24, udhLength:I
    :goto_1df
    add-int v26, v15, v24

    #@1e1
    const/16 v27, 0xa0

    #@1e3
    move/from16 v0, v26

    #@1e5
    move/from16 v1, v27

    #@1e7
    if-le v0, v1, :cond_260

    #@1e9
    .line 1228
    if-nez v24, :cond_1ed

    #@1eb
    .line 1229
    const/16 v24, 0x1

    #@1ed
    .line 1231
    :cond_1ed
    add-int/lit8 v24, v24, 0x6

    #@1ef
    .line 1232
    move/from16 v0, v24

    #@1f1
    rsub-int v0, v0, 0xa0

    #@1f3
    move/from16 v16, v0

    #@1f5
    .line 1233
    .local v16, septetsPerMessage:I
    add-int v26, v15, v16

    #@1f7
    add-int/lit8 v26, v26, -0x1

    #@1f9
    div-int v14, v26, v16

    #@1fb
    .line 1234
    .local v14, msgCount:I
    mul-int v26, v14, v16

    #@1fd
    sub-int v17, v26, v15

    #@1ff
    .line 1240
    .end local v16           #septetsPerMessage:I
    .local v17, septetsRemaining:I
    :goto_1ff
    iget-object v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->unencodableCounts:[I

    #@201
    move-object/from16 v26, v0

    #@203
    aget v25, v26, v18

    #@205
    .line 1241
    .local v25, unencodableCount:I
    if-eqz p1, :cond_20b

    #@207
    move/from16 v0, v25

    #@209
    if-gt v0, v13, :cond_1d2

    #@20b
    .line 1244
    :cond_20b
    if-eqz p1, :cond_211

    #@20d
    move/from16 v0, v25

    #@20f
    if-lt v0, v13, :cond_231

    #@211
    :cond_211
    move-object/from16 v0, v23

    #@213
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@215
    move/from16 v26, v0

    #@217
    move/from16 v0, v26

    #@219
    if-lt v14, v0, :cond_231

    #@21b
    move-object/from16 v0, v23

    #@21d
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@21f
    move/from16 v26, v0

    #@221
    move/from16 v0, v26

    #@223
    if-ne v14, v0, :cond_1d2

    #@225
    move-object/from16 v0, v23

    #@227
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@229
    move/from16 v26, v0

    #@22b
    move/from16 v0, v17

    #@22d
    move/from16 v1, v26

    #@22f
    if-le v0, v1, :cond_1d2

    #@231
    .line 1247
    :cond_231
    move/from16 v13, v25

    #@233
    .line 1248
    move-object/from16 v0, v23

    #@235
    iput v14, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@237
    .line 1249
    move-object/from16 v0, v23

    #@239
    iput v15, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@23b
    .line 1250
    move/from16 v0, v17

    #@23d
    move-object/from16 v1, v23

    #@23f
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitsRemaining:I

    #@241
    .line 1251
    iget v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@243
    move/from16 v26, v0

    #@245
    move/from16 v0, v26

    #@247
    move-object/from16 v1, v23

    #@249
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageTable:I

    #@24b
    .line 1252
    move/from16 v0, v18

    #@24d
    move-object/from16 v1, v23

    #@24f
    iput v0, v1, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->languageShiftTable:I

    #@251
    goto :goto_1d2

    #@252
    .line 1219
    .end local v14           #msgCount:I
    .end local v17           #septetsRemaining:I
    .end local v24           #udhLength:I
    .end local v25           #unencodableCount:I
    :cond_252
    iget v0, v10, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@254
    move/from16 v26, v0

    #@256
    if-nez v26, :cond_25a

    #@258
    if-eqz v18, :cond_25d

    #@25a
    .line 1220
    :cond_25a
    const/16 v24, 0x5

    #@25c
    .restart local v24       #udhLength:I
    goto :goto_1df

    #@25d
    .line 1222
    .end local v24           #udhLength:I
    :cond_25d
    const/16 v24, 0x0

    #@25f
    .restart local v24       #udhLength:I
    goto :goto_1df

    #@260
    .line 1236
    :cond_260
    const/4 v14, 0x1

    #@261
    .line 1237
    .restart local v14       #msgCount:I
    move/from16 v0, v24

    #@263
    rsub-int v0, v0, 0xa0

    #@265
    move/from16 v26, v0

    #@267
    sub-int v17, v26, v15

    #@269
    .restart local v17       #septetsRemaining:I
    goto :goto_1ff

    #@26a
    .line 1257
    .end local v10           #lpc:Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
    .end local v14           #msgCount:I
    .end local v15           #septets:I
    .end local v17           #septetsRemaining:I
    .end local v18           #shiftTable:I
    .end local v24           #udhLength:I
    :cond_26a
    move-object/from16 v0, v23

    #@26c
    iget v0, v0, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->msgCount:I

    #@26e
    move/from16 v26, v0

    #@270
    const v27, 0x7fffffff

    #@273
    move/from16 v0, v26

    #@275
    move/from16 v1, v27

    #@277
    if-ne v0, v1, :cond_2f

    #@279
    .line 1258
    const/16 v23, 0x0

    #@27b
    goto/16 :goto_2f
.end method

.method public static countGsmSeptetsUsingTables(Ljava/lang/CharSequence;ZII)I
    .registers 13
    .parameter "s"
    .parameter "use7bitOnly"
    .parameter "languageTable"
    .parameter "languageShiftTable"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 857
    const/4 v3, 0x0

    #@2
    .line 858
    .local v3, count:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v5

    #@6
    .line 859
    .local v5, sz:I
    sget-object v7, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@8
    aget-object v1, v7, p2

    #@a
    .line 860
    .local v1, charToLanguageTable:Landroid/util/SparseIntArray;
    sget-object v7, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@c
    aget-object v2, v7, p3

    #@e
    .line 861
    .local v2, charToShiftTable:Landroid/util/SparseIntArray;
    const/4 v4, 0x0

    #@f
    .local v4, i:I
    :goto_f
    if-ge v4, v5, :cond_3b

    #@11
    .line 862
    invoke-interface {p0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@14
    move-result v0

    #@15
    .line 863
    .local v0, c:C
    const/16 v7, 0x1b

    #@17
    if-ne v0, v7, :cond_23

    #@19
    .line 864
    const-string v7, "GSM"

    #@1b
    const-string v8, "countGsmSeptets() string contains Escape character, skipping."

    #@1d
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 861
    :goto_20
    add-int/lit8 v4, v4, 0x1

    #@22
    goto :goto_f

    #@23
    .line 867
    :cond_23
    invoke-virtual {v1, v0, v6}, Landroid/util/SparseIntArray;->get(II)I

    #@26
    move-result v7

    #@27
    if-eq v7, v6, :cond_2c

    #@29
    .line 868
    add-int/lit8 v3, v3, 0x1

    #@2b
    goto :goto_20

    #@2c
    .line 869
    :cond_2c
    invoke-virtual {v2, v0, v6}, Landroid/util/SparseIntArray;->get(II)I

    #@2f
    move-result v7

    #@30
    if-eq v7, v6, :cond_35

    #@32
    .line 870
    add-int/lit8 v3, v3, 0x2

    #@34
    goto :goto_20

    #@35
    .line 871
    :cond_35
    if-eqz p1, :cond_3a

    #@37
    .line 872
    add-int/lit8 v3, v3, 0x1

    #@39
    goto :goto_20

    #@3a
    :cond_3a
    move v3, v6

    #@3b
    .line 877
    .end local v0           #c:C
    .end local v3           #count:I
    :cond_3b
    return v3
.end method

.method public static countGsmSeptetsUsingTablesLossyAuto(Ljava/lang/CharSequence;ZII)I
    .registers 13
    .parameter "s"
    .parameter "use7bitOnly"
    .parameter "languageTable"
    .parameter "languageShiftTable"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 886
    const/4 v3, 0x0

    #@2
    .line 887
    .local v3, count:I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v5

    #@6
    .line 888
    .local v5, sz:I
    sget-object v7, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@8
    aget-object v1, v7, p2

    #@a
    .line 889
    .local v1, charToLanguageTable:Landroid/util/SparseIntArray;
    sget-object v7, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@c
    aget-object v2, v7, p3

    #@e
    .line 890
    .local v2, charToShiftTable:Landroid/util/SparseIntArray;
    const/4 v4, 0x0

    #@f
    .local v4, i:I
    :goto_f
    if-ge v4, v5, :cond_41

    #@11
    .line 891
    invoke-interface {p0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@14
    move-result v0

    #@15
    .line 892
    .local v0, c:C
    const/16 v7, 0x1b

    #@17
    if-ne v0, v7, :cond_23

    #@19
    .line 893
    const-string v7, "GSM"

    #@1b
    const-string v8, "countGsmSeptets() string contains Escape character, skipping."

    #@1d
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 890
    :goto_20
    add-int/lit8 v4, v4, 0x1

    #@22
    goto :goto_f

    #@23
    .line 897
    :cond_23
    invoke-virtual {v1, v0, v6}, Landroid/util/SparseIntArray;->get(II)I

    #@26
    move-result v7

    #@27
    if-eq v7, v6, :cond_2c

    #@29
    .line 898
    add-int/lit8 v3, v3, 0x1

    #@2b
    goto :goto_20

    #@2c
    .line 899
    :cond_2c
    invoke-virtual {v2, v0, v6}, Landroid/util/SparseIntArray;->get(II)I

    #@2f
    move-result v7

    #@30
    if-eq v7, v6, :cond_35

    #@32
    .line 900
    add-int/lit8 v3, v3, 0x2

    #@34
    goto :goto_20

    #@35
    .line 901
    :cond_35
    if-eqz p1, :cond_42

    #@37
    .line 902
    invoke-static {v0}, Lcom/android/internal/telephony/GsmAlphabet;->lookupLossy7bitTable(C)I

    #@3a
    move-result v7

    #@3b
    if-eq v7, v6, :cond_40

    #@3d
    .line 903
    add-int/lit8 v3, v3, 0x1

    #@3f
    goto :goto_20

    #@40
    :cond_40
    move v3, v6

    #@41
    .line 911
    .end local v0           #c:C
    .end local v3           #count:I
    :cond_41
    :goto_41
    return v3

    #@42
    .restart local v0       #c:C
    .restart local v3       #count:I
    :cond_42
    move v3, v6

    #@43
    .line 908
    goto :goto_41
.end method

.method private static enableCountrySpecificEncodings()V
    .registers 3

    #@0
    .prologue
    .line 1491
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    .line 1493
    .local v0, r:Landroid/content/res/Resources;
    const v1, 0x1070037

    #@7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@a
    move-result-object v1

    #@b
    sput-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@d
    .line 1494
    const v1, 0x1070038

    #@10
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@13
    move-result-object v1

    #@14
    sput-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@16
    .line 1496
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@18
    array-length v1, v1

    #@19
    if-lez v1, :cond_27

    #@1b
    .line 1497
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@1d
    sget-object v2, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@1f
    array-length v2, v2

    #@20
    add-int/lit8 v2, v2, -0x1

    #@22
    aget v1, v1, v2

    #@24
    sput v1, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I

    #@26
    .line 1502
    :goto_26
    return-void

    #@27
    .line 1500
    :cond_27
    const/4 v1, 0x0

    #@28
    sput v1, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I

    #@2a
    goto :goto_26
.end method

.method public static findGsmSeptetLimitIndex(Ljava/lang/String;IIII)I
    .registers 13
    .parameter "s"
    .parameter "start"
    .parameter "limit"
    .parameter "langTable"
    .parameter "langShiftTable"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 1314
    const/4 v0, 0x0

    #@2
    .line 1315
    .local v0, accumulator:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v5

    #@6
    .line 1317
    .local v5, size:I
    sget-object v6, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@8
    aget-object v2, v6, p3

    #@a
    .line 1318
    .local v2, charToLangTable:Landroid/util/SparseIntArray;
    sget-object v6, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@c
    aget-object v1, v6, p4

    #@e
    .line 1319
    .local v1, charToLangShiftTable:Landroid/util/SparseIntArray;
    move v4, p1

    #@f
    .local v4, i:I
    :goto_f
    if-ge v4, v5, :cond_33

    #@11
    .line 1320
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v6

    #@15
    invoke-virtual {v2, v6, v7}, Landroid/util/SparseIntArray;->get(II)I

    #@18
    move-result v3

    #@19
    .line 1321
    .local v3, encodedSeptet:I
    if-ne v3, v7, :cond_2d

    #@1b
    .line 1322
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@1e
    move-result v6

    #@1f
    invoke-virtual {v1, v6, v7}, Landroid/util/SparseIntArray;->get(II)I

    #@22
    move-result v3

    #@23
    .line 1323
    if-ne v3, v7, :cond_2a

    #@25
    .line 1325
    add-int/lit8 v0, v0, 0x1

    #@27
    .line 1332
    :goto_27
    if-le v0, p2, :cond_30

    #@29
    .line 1336
    .end local v3           #encodedSeptet:I
    .end local v4           #i:I
    :goto_29
    return v4

    #@2a
    .line 1327
    .restart local v3       #encodedSeptet:I
    .restart local v4       #i:I
    :cond_2a
    add-int/lit8 v0, v0, 0x2

    #@2c
    goto :goto_27

    #@2d
    .line 1330
    :cond_2d
    add-int/lit8 v0, v0, 0x1

    #@2f
    goto :goto_27

    #@30
    .line 1319
    :cond_30
    add-int/lit8 v4, v4, 0x1

    #@32
    goto :goto_f

    #@33
    .end local v3           #encodedSeptet:I
    :cond_33
    move v4, v5

    #@34
    .line 1336
    goto :goto_29
.end method

.method static declared-synchronized getEnabledLockingShiftTables()[I
    .registers 2

    #@0
    .prologue
    .line 1388
    const-class v0, Lcom/android/internal/telephony/GsmAlphabet;

    #@2
    monitor-enter v0

    #@3
    :try_start_3
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    #@5
    monitor-exit v0

    #@6
    return-object v1

    #@7
    :catchall_7
    move-exception v1

    #@8
    monitor-exit v0

    #@9
    throw v1
.end method

.method public static getEnabledLockingShiftTablesLG()[I
    .registers 1

    #@0
    .prologue
    .line 1483
    sget-object v0, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@2
    return-object v0
.end method

.method static declared-synchronized getEnabledSingleShiftTables()[I
    .registers 2

    #@0
    .prologue
    .line 1377
    const-class v0, Lcom/android/internal/telephony/GsmAlphabet;

    #@2
    monitor-enter v0

    #@3
    :try_start_3
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    #@5
    monitor-exit v0

    #@6
    return-object v1

    #@7
    :catchall_7
    move-exception v1

    #@8
    monitor-exit v0

    #@9
    throw v1
.end method

.method public static getEnabledSingleShiftTablesLG()[I
    .registers 1

    #@0
    .prologue
    .line 1480
    sget-object v0, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@2
    return-object v0
.end method

.method public static getNationalLockingShiftEnableIndex(Ljava/lang/String;)[I
    .registers 13
    .parameter "mcc"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 1416
    new-array v5, v11, [I

    #@3
    .line 1420
    .local v5, nationalLSTable:[I
    const/4 v9, 0x3

    #@4
    new-array v7, v9, [Ljava/lang/String;

    #@6
    const-string v9, "286"

    #@8
    aput-object v9, v7, v11

    #@a
    const/4 v9, 0x1

    #@b
    const-string v10, "214"

    #@d
    aput-object v10, v7, v9

    #@f
    const/4 v9, 0x2

    #@10
    const-string v10, "268"

    #@12
    aput-object v10, v7, v9

    #@14
    .line 1421
    .local v7, nationalLanuageMccList:[Ljava/lang/String;
    const/4 v9, 0x0

    #@15
    const-string/jumbo v10, "support_nl_locking_shift"

    #@18
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b
    move-result v9

    #@1c
    if-nez v9, :cond_20

    #@1e
    move-object v6, v5

    #@1f
    .line 1435
    .end local v5           #nationalLSTable:[I
    .local v6, nationalLSTable:[I
    :goto_1f
    return-object v6

    #@20
    .line 1424
    .end local v6           #nationalLSTable:[I
    .restart local v5       #nationalLSTable:[I
    :cond_20
    new-array v1, v11, [[I

    #@22
    .line 1425
    .local v1, enableLockingSiftTable:[[I
    const/4 v3, 0x0

    #@23
    .line 1426
    .local v3, index:I
    move-object v0, v7

    #@24
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@25
    .local v4, len$:I
    const/4 v2, 0x0

    #@26
    .local v2, i$:I
    :goto_26
    if-ge v2, v4, :cond_35

    #@28
    aget-object v8, v0, v2

    #@2a
    .line 1427
    .local v8, strMcc:Ljava/lang/String;
    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v9

    #@2e
    if-eqz v9, :cond_37

    #@30
    .line 1428
    array-length v9, v1

    #@31
    if-ge v3, v9, :cond_37

    #@33
    .line 1429
    aget-object v5, v1, v3

    #@35
    .end local v8           #strMcc:Ljava/lang/String;
    :cond_35
    move-object v6, v5

    #@36
    .line 1435
    .end local v5           #nationalLSTable:[I
    .restart local v6       #nationalLSTable:[I
    goto :goto_1f

    #@37
    .line 1433
    .end local v6           #nationalLSTable:[I
    .restart local v5       #nationalLSTable:[I
    .restart local v8       #strMcc:Ljava/lang/String;
    :cond_37
    add-int/lit8 v3, v3, 0x1

    #@39
    .line 1426
    add-int/lit8 v2, v2, 0x1

    #@3b
    goto :goto_26
.end method

.method public static getNationalSingleShiftEnableIndex(Ljava/lang/String;)[I
    .registers 16
    .parameter "mcc"

    #@0
    .prologue
    const/4 v14, 0x3

    #@1
    const/4 v13, 0x2

    #@2
    const/4 v12, 0x1

    #@3
    const/4 v11, 0x0

    #@4
    .line 1393
    new-array v6, v11, [I

    #@6
    .line 1397
    .local v6, nationalSSTable:[I
    new-array v5, v14, [Ljava/lang/String;

    #@8
    const-string v9, "286"

    #@a
    aput-object v9, v5, v11

    #@c
    const-string v9, "214"

    #@e
    aput-object v9, v5, v12

    #@10
    const-string v9, "268"

    #@12
    aput-object v9, v5, v13

    #@14
    .line 1398
    .local v5, nationalLanuageMccList:[Ljava/lang/String;
    const/4 v9, 0x0

    #@15
    const-string/jumbo v10, "support_nl_single_shift"

    #@18
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1b
    move-result v9

    #@1c
    if-nez v9, :cond_20

    #@1e
    move-object v7, v6

    #@1f
    .line 1412
    .end local v6           #nationalSSTable:[I
    .local v7, nationalSSTable:[I
    :goto_1f
    return-object v7

    #@20
    .line 1401
    .end local v7           #nationalSSTable:[I
    .restart local v6       #nationalSSTable:[I
    :cond_20
    new-array v1, v14, [[I

    #@22
    new-array v9, v12, [I

    #@24
    aput v12, v9, v11

    #@26
    aput-object v9, v1, v11

    #@28
    new-array v9, v13, [I

    #@2a
    fill-array-data v9, :array_50

    #@2d
    aput-object v9, v1, v12

    #@2f
    new-array v9, v14, [I

    #@31
    fill-array-data v9, :array_58

    #@34
    aput-object v9, v1, v13

    #@36
    .line 1402
    .local v1, enableSingleSiftTable:[[I
    const/4 v3, 0x0

    #@37
    .line 1403
    .local v3, index:I
    move-object v0, v5

    #@38
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@39
    .local v4, len$:I
    const/4 v2, 0x0

    #@3a
    .local v2, i$:I
    :goto_3a
    if-ge v2, v4, :cond_49

    #@3c
    aget-object v8, v0, v2

    #@3e
    .line 1404
    .local v8, strMcc:Ljava/lang/String;
    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v9

    #@42
    if-eqz v9, :cond_4b

    #@44
    .line 1405
    array-length v9, v1

    #@45
    if-ge v3, v9, :cond_4b

    #@47
    .line 1406
    aget-object v6, v1, v3

    #@49
    .end local v8           #strMcc:Ljava/lang/String;
    :cond_49
    move-object v7, v6

    #@4a
    .line 1412
    .end local v6           #nationalSSTable:[I
    .restart local v7       #nationalSSTable:[I
    goto :goto_1f

    #@4b
    .line 1410
    .end local v7           #nationalSSTable:[I
    .restart local v6       #nationalSSTable:[I
    .restart local v8       #strMcc:Ljava/lang/String;
    :cond_4b
    add-int/lit8 v3, v3, 0x1

    #@4d
    .line 1403
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_3a

    #@50
    .line 1401
    :array_50
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@58
    :array_58
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public static gsm7BitPackedToString([BII)Ljava/lang/String;
    .registers 9
    .parameter "pdu"
    .parameter "offset"
    .parameter "lengthSeptets"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 529
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move v4, v3

    #@5
    move v5, v3

    #@6
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BIIIII)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static gsm7BitPackedToString([BIIIII)Ljava/lang/String;
    .registers 21
    .parameter "pdu"
    .parameter "offset"
    .parameter "lengthSeptets"
    .parameter "numPaddingBits"
    .parameter "languageTable"
    .parameter "shiftTable"

    #@0
    .prologue
    .line 550
    new-instance v9, Ljava/lang/StringBuilder;

    #@2
    move/from16 v0, p2

    #@4
    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 552
    .local v9, ret:Ljava/lang/StringBuilder;
    if-ltz p4, :cond_10

    #@9
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@b
    array-length v12, v12

    #@c
    move/from16 v0, p4

    #@e
    if-le v0, v12, :cond_33

    #@10
    .line 553
    :cond_10
    const-string v12, "GSM"

    #@12
    new-instance v13, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v14, "unknown language table "

    #@1a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v13

    #@1e
    move/from16 v0, p4

    #@20
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v13

    #@24
    const-string v14, ", using default"

    #@26
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v13

    #@2a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v13

    #@2e
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 554
    const/16 p4, 0x0

    #@33
    .line 556
    :cond_33
    if-ltz p5, :cond_3c

    #@35
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@37
    array-length v12, v12

    #@38
    move/from16 v0, p5

    #@3a
    if-le v0, v12, :cond_5f

    #@3c
    .line 557
    :cond_3c
    const-string v12, "GSM"

    #@3e
    new-instance v13, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string/jumbo v14, "unknown single shift table "

    #@46
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v13

    #@4a
    move/from16 v0, p5

    #@4c
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v13

    #@50
    const-string v14, ", using default"

    #@52
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v13

    #@56
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v13

    #@5a
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 558
    const/16 p5, 0x0

    #@5f
    .line 562
    :cond_5f
    const/4 v8, 0x0

    #@60
    .line 563
    .local v8, prevCharWasEscape:Z
    :try_start_60
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@62
    aget-object v7, v12, p4

    #@64
    .line 564
    .local v7, languageTableToChar:Ljava/lang/String;
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@66
    aget-object v11, v12, p5

    #@68
    .line 566
    .local v11, shiftTableToChar:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    #@6b
    move-result v12

    #@6c
    if-eqz v12, :cond_94

    #@6e
    .line 567
    const-string v12, "GSM"

    #@70
    new-instance v13, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string/jumbo v14, "no language table for code "

    #@78
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v13

    #@7c
    move/from16 v0, p4

    #@7e
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v13

    #@82
    const-string v14, ", using default"

    #@84
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v13

    #@88
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v13

    #@8c
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 568
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@91
    const/4 v13, 0x0

    #@92
    aget-object v7, v12, v13

    #@94
    .line 570
    :cond_94
    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    #@97
    move-result v12

    #@98
    if-eqz v12, :cond_c0

    #@9a
    .line 571
    const-string v12, "GSM"

    #@9c
    new-instance v13, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string/jumbo v14, "no single shift table for code "

    #@a4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v13

    #@a8
    move/from16 v0, p5

    #@aa
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v13

    #@ae
    const-string v14, ", using default"

    #@b0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v13

    #@b4
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v13

    #@b8
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 572
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@bd
    const/4 v13, 0x0

    #@be
    aget-object v11, v12, v13

    #@c0
    .line 575
    :cond_c0
    const/4 v6, 0x0

    #@c1
    .local v6, i:I
    :goto_c1
    move/from16 v0, p2

    #@c3
    if-ge v6, v0, :cond_132

    #@c5
    .line 576
    mul-int/lit8 v12, v6, 0x7

    #@c7
    add-int v1, v12, p3

    #@c9
    .line 578
    .local v1, bitOffset:I
    div-int/lit8 v2, v1, 0x8

    #@cb
    .line 579
    .local v2, byteOffset:I
    rem-int/lit8 v10, v1, 0x8

    #@cd
    .line 580
    .local v10, shift:I
    const/4 v5, 0x1

    #@ce
    .line 582
    .local v5, gsmVal:I
    add-int v12, p1, v2

    #@d0
    array-length v13, p0

    #@d1
    if-ge v12, v13, :cond_da

    #@d3
    .line 583
    add-int v12, p1, v2

    #@d5
    aget-byte v12, p0, v12

    #@d7
    shr-int/2addr v12, v10

    #@d8
    and-int/lit8 v5, v12, 0x7f

    #@da
    .line 588
    :cond_da
    const/4 v12, 0x1

    #@db
    if-le v10, v12, :cond_f6

    #@dd
    .line 590
    const/16 v12, 0x7f

    #@df
    add-int/lit8 v13, v10, -0x1

    #@e1
    shr-int/2addr v12, v13

    #@e2
    and-int/2addr v5, v12

    #@e3
    .line 592
    add-int v12, p1, v2

    #@e5
    add-int/lit8 v12, v12, 0x1

    #@e7
    array-length v13, p0

    #@e8
    if-ge v12, v13, :cond_f6

    #@ea
    .line 593
    add-int v12, p1, v2

    #@ec
    add-int/lit8 v12, v12, 0x1

    #@ee
    aget-byte v12, p0, v12

    #@f0
    rsub-int/lit8 v13, v10, 0x8

    #@f2
    shl-int/2addr v12, v13

    #@f3
    and-int/lit8 v12, v12, 0x7f

    #@f5
    or-int/2addr v5, v12

    #@f6
    .line 598
    :cond_f6
    if-eqz v8, :cond_124

    #@f8
    .line 599
    const/16 v12, 0x1b

    #@fa
    if-ne v5, v12, :cond_105

    #@fc
    .line 600
    const/16 v12, 0x20

    #@fe
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@101
    .line 609
    :goto_101
    const/4 v8, 0x0

    #@102
    .line 575
    :goto_102
    add-int/lit8 v6, v6, 0x1

    #@104
    goto :goto_c1

    #@105
    .line 602
    :cond_105
    invoke-virtual {v11, v5}, Ljava/lang/String;->charAt(I)C

    #@108
    move-result v3

    #@109
    .line 603
    .local v3, c:C
    const/16 v12, 0x20

    #@10b
    if-ne v3, v12, :cond_120

    #@10d
    .line 604
    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    #@110
    move-result v12

    #@111
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_114
    .catch Ljava/lang/RuntimeException; {:try_start_60 .. :try_end_114} :catch_115

    #@114
    goto :goto_101

    #@115
    .line 616
    .end local v1           #bitOffset:I
    .end local v2           #byteOffset:I
    .end local v3           #c:C
    .end local v5           #gsmVal:I
    .end local v6           #i:I
    .end local v7           #languageTableToChar:Ljava/lang/String;
    .end local v10           #shift:I
    .end local v11           #shiftTableToChar:Ljava/lang/String;
    :catch_115
    move-exception v4

    #@116
    .line 617
    .local v4, ex:Ljava/lang/RuntimeException;
    const-string v12, "GSM"

    #@118
    const-string v13, "Error GSM 7 bit packed: "

    #@11a
    invoke-static {v12, v13, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11d
    .line 620
    const-string v12, "It is a wrong formatted message"

    #@11f
    .line 624
    .end local v4           #ex:Ljava/lang/RuntimeException;
    :goto_11f
    return-object v12

    #@120
    .line 606
    .restart local v1       #bitOffset:I
    .restart local v2       #byteOffset:I
    .restart local v3       #c:C
    .restart local v5       #gsmVal:I
    .restart local v6       #i:I
    .restart local v7       #languageTableToChar:Ljava/lang/String;
    .restart local v10       #shift:I
    .restart local v11       #shiftTableToChar:Ljava/lang/String;
    :cond_120
    :try_start_120
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@123
    goto :goto_101

    #@124
    .line 610
    .end local v3           #c:C
    :cond_124
    const/16 v12, 0x1b

    #@126
    if-ne v5, v12, :cond_12a

    #@128
    .line 611
    const/4 v8, 0x1

    #@129
    goto :goto_102

    #@12a
    .line 613
    :cond_12a
    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    #@12d
    move-result v12

    #@12e
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_131
    .catch Ljava/lang/RuntimeException; {:try_start_120 .. :try_end_131} :catch_115

    #@131
    goto :goto_102

    #@132
    .line 624
    .end local v1           #bitOffset:I
    .end local v2           #byteOffset:I
    .end local v5           #gsmVal:I
    .end local v10           #shift:I
    :cond_132
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@135
    move-result-object v12

    #@136
    goto :goto_11f
.end method

.method public static gsm8BitUnpackedToString([BII)Ljava/lang/String;
    .registers 4
    .parameter "data"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 642
    const-string v0, ""

    #@2
    invoke-static {p0, p1, p2, v0}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BIILjava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static gsm8BitUnpackedToString([BIILjava/lang/String;)Ljava/lang/String;
    .registers 18
    .parameter "data"
    .parameter "offset"
    .parameter "length"
    .parameter "characterset"

    #@0
    .prologue
    .line 657
    const/4 v5, 0x0

    #@1
    .line 658
    .local v5, isMbcs:Z
    const/4 v2, 0x0

    #@2
    .line 659
    .local v2, charset:Ljava/nio/charset/Charset;
    const/4 v7, 0x0

    #@3
    .line 661
    .local v7, mbcsBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v12

    #@7
    if-nez v12, :cond_24

    #@9
    const-string/jumbo v12, "us-ascii"

    #@c
    move-object/from16 v0, p3

    #@e
    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@11
    move-result v12

    #@12
    if-nez v12, :cond_24

    #@14
    invoke-static/range {p3 .. p3}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    #@17
    move-result v12

    #@18
    if-eqz v12, :cond_24

    #@1a
    .line 664
    const/4 v5, 0x1

    #@1b
    .line 665
    invoke-static/range {p3 .. p3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@1e
    move-result-object v2

    #@1f
    .line 666
    const/4 v12, 0x2

    #@20
    invoke-static {v12}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@23
    move-result-object v7

    #@24
    .line 670
    :cond_24
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@26
    const/4 v13, 0x0

    #@27
    aget-object v6, v12, v13

    #@29
    .line 671
    .local v6, languageTableToChar:Ljava/lang/String;
    sget-object v12, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@2b
    const/4 v13, 0x0

    #@2c
    aget-object v11, v12, v13

    #@2e
    .line 673
    .local v11, shiftTableToChar:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@30
    move/from16 v0, p2

    #@32
    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@35
    .line 674
    .local v9, ret:Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    #@36
    .line 675
    .local v8, prevWasEscape:Z
    move v3, p1

    #@37
    .local v3, i:I
    move v4, v3

    #@38
    .end local v3           #i:I
    .local v4, i:I
    :goto_38
    add-int v12, p1, p2

    #@3a
    if-ge v4, v12, :cond_44

    #@3c
    .line 678
    aget-byte v12, p0, v4

    #@3e
    and-int/lit16 v1, v12, 0xff

    #@40
    .line 680
    .local v1, c:I
    const/16 v12, 0xff

    #@42
    if-ne v1, v12, :cond_49

    #@44
    .line 728
    .end local v1           #c:I
    :cond_44
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v12

    #@48
    return-object v12

    #@49
    .line 682
    .restart local v1       #c:I
    :cond_49
    const/16 v12, 0x1b

    #@4b
    if-ne v1, v12, :cond_5d

    #@4d
    .line 683
    if-eqz v8, :cond_5a

    #@4f
    .line 687
    const/16 v12, 0x20

    #@51
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@54
    .line 688
    const/4 v8, 0x0

    #@55
    move v3, v4

    #@56
    .line 675
    .end local v4           #i:I
    .restart local v3       #i:I
    :goto_56
    add-int/lit8 v3, v3, 0x1

    #@58
    move v4, v3

    #@59
    .end local v3           #i:I
    .restart local v4       #i:I
    goto :goto_38

    #@5a
    .line 690
    :cond_5a
    const/4 v8, 0x1

    #@5b
    move v3, v4

    #@5c
    .end local v4           #i:I
    .restart local v3       #i:I
    goto :goto_56

    #@5d
    .line 694
    .end local v3           #i:I
    .restart local v4       #i:I
    :cond_5d
    if-eqz v8, :cond_87

    #@5f
    .line 695
    const/16 v12, 0x80

    #@61
    if-ge v1, v12, :cond_79

    #@63
    .line 696
    invoke-virtual {v11, v1}, Ljava/lang/String;->charAt(I)C

    #@66
    move-result v10

    #@67
    .line 697
    .local v10, shiftChar:C
    const/16 v12, 0x20

    #@69
    if-ne v10, v12, :cond_75

    #@6b
    .line 699
    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    #@6e
    move-result v12

    #@6f
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@72
    :goto_72
    move v3, v4

    #@73
    .line 724
    .end local v4           #i:I
    .end local v10           #shiftChar:C
    .restart local v3       #i:I
    :goto_73
    const/4 v8, 0x0

    #@74
    goto :goto_56

    #@75
    .line 701
    .end local v3           #i:I
    .restart local v4       #i:I
    .restart local v10       #shiftChar:C
    :cond_75
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@78
    goto :goto_72

    #@79
    .line 704
    .end local v10           #shiftChar:C
    :cond_79
    const-string v12, "GsmAlphabet"

    #@7b
    const-string v13, "[TEL-SMS] gsm8BitUnpackedToString extend GSM 7 bit default alphabet"

    #@7d
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 705
    const/16 v12, 0x20

    #@82
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@85
    move v3, v4

    #@86
    .end local v4           #i:I
    .restart local v3       #i:I
    goto :goto_73

    #@87
    .line 708
    .end local v3           #i:I
    .restart local v4       #i:I
    :cond_87
    if-eqz v5, :cond_8f

    #@89
    add-int/lit8 v12, v4, 0x1

    #@8b
    add-int v13, p1, p2

    #@8d
    if-lt v12, v13, :cond_a3

    #@8f
    .line 709
    :cond_8f
    const/16 v12, 0x80

    #@91
    if-ge v1, v12, :cond_9c

    #@93
    .line 710
    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    #@96
    move-result v12

    #@97
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@9a
    move v3, v4

    #@9b
    .end local v4           #i:I
    .restart local v3       #i:I
    goto :goto_73

    #@9c
    .line 713
    .end local v3           #i:I
    .restart local v4       #i:I
    :cond_9c
    const/16 v12, 0x20

    #@9e
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@a1
    move v3, v4

    #@a2
    .end local v4           #i:I
    .restart local v3       #i:I
    goto :goto_73

    #@a3
    .line 718
    .end local v3           #i:I
    .restart local v4       #i:I
    :cond_a3
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@a6
    .line 719
    add-int/lit8 v3, v4, 0x1

    #@a8
    .end local v4           #i:I
    .restart local v3       #i:I
    const/4 v12, 0x2

    #@a9
    invoke-virtual {v7, p0, v4, v12}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    #@ac
    .line 720
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@af
    .line 721
    invoke-virtual {v2, v7}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    #@b2
    move-result-object v12

    #@b3
    invoke-virtual {v12}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    #@b6
    move-result-object v12

    #@b7
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    goto :goto_73
.end method

.method public static gsmExtendedToChar(I)C
    .registers 5
    .parameter "gsmChar"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/16 v1, 0x20

    #@3
    .line 253
    const/16 v2, 0x1b

    #@5
    if-ne p0, v2, :cond_9

    #@7
    move v0, v1

    #@8
    .line 263
    :cond_8
    :goto_8
    return v0

    #@9
    .line 255
    :cond_9
    if-ltz p0, :cond_22

    #@b
    const/16 v2, 0x80

    #@d
    if-ge p0, v2, :cond_22

    #@f
    .line 256
    sget-object v2, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageShiftTables:[Ljava/lang/String;

    #@11
    aget-object v2, v2, v3

    #@13
    invoke-virtual {v2, p0}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v0

    #@17
    .line 257
    .local v0, c:C
    if-ne v0, v1, :cond_8

    #@19
    .line 258
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@1b
    aget-object v1, v1, v3

    #@1d
    invoke-virtual {v1, p0}, Ljava/lang/String;->charAt(I)C

    #@20
    move-result v0

    #@21
    goto :goto_8

    #@22
    .end local v0           #c:C
    :cond_22
    move v0, v1

    #@23
    .line 263
    goto :goto_8
.end method

.method public static gsmToChar(I)C
    .registers 3
    .parameter "gsmChar"

    #@0
    .prologue
    .line 231
    if-ltz p0, :cond_10

    #@2
    const/16 v0, 0x80

    #@4
    if-ge p0, v0, :cond_10

    #@6
    .line 232
    sget-object v0, Lcom/android/internal/telephony/GsmAlphabet;->sLanguageTables:[Ljava/lang/String;

    #@8
    const/4 v1, 0x0

    #@9
    aget-object v0, v0, v1

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/String;->charAt(I)C

    #@e
    move-result v0

    #@f
    .line 234
    :goto_f
    return v0

    #@10
    :cond_10
    const/16 v0, 0x20

    #@12
    goto :goto_f
.end method

.method private static lookupLossy7bitTable(C)I
    .registers 5
    .parameter "c"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 918
    const/4 v0, -0x1

    #@2
    .line 920
    .local v0, v:I
    const/16 v1, 0x80

    #@4
    if-ge p0, v1, :cond_1a

    #@6
    .line 921
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmBasicLatin:Landroid/util/SparseIntArray;

    #@8
    invoke-virtual {v1, p0, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@b
    move-result v0

    #@c
    .line 938
    :cond_c
    :goto_c
    const/4 v1, 0x0

    #@d
    const-string v2, "gsm_strict_encoding"

    #@f
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_19

    #@15
    .line 939
    if-ne v0, v3, :cond_19

    #@17
    .line 941
    const/16 v0, 0x20

    #@19
    .line 946
    :cond_19
    return v0

    #@1a
    .line 922
    :cond_1a
    const/16 v1, 0x7f

    #@1c
    if-le p0, v1, :cond_29

    #@1e
    const/16 v1, 0x100

    #@20
    if-ge p0, v1, :cond_29

    #@22
    .line 923
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatin1Supplement:Landroid/util/SparseIntArray;

    #@24
    invoke-virtual {v1, p0, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@27
    move-result v0

    #@28
    goto :goto_c

    #@29
    .line 924
    :cond_29
    const/16 v1, 0xff

    #@2b
    if-le p0, v1, :cond_38

    #@2d
    const/16 v1, 0x180

    #@2f
    if-ge p0, v1, :cond_38

    #@31
    .line 925
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedA:Landroid/util/SparseIntArray;

    #@33
    invoke-virtual {v1, p0, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@36
    move-result v0

    #@37
    goto :goto_c

    #@38
    .line 926
    :cond_38
    const/16 v1, 0x17f

    #@3a
    if-le p0, v1, :cond_47

    #@3c
    const/16 v1, 0x250

    #@3e
    if-ge p0, v1, :cond_47

    #@40
    .line 927
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmLatinExtendedB:Landroid/util/SparseIntArray;

    #@42
    invoke-virtual {v1, p0, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@45
    move-result v0

    #@46
    goto :goto_c

    #@47
    .line 928
    :cond_47
    const/16 v1, 0x36f

    #@49
    if-le p0, v1, :cond_56

    #@4b
    const/16 v1, 0x400

    #@4d
    if-ge p0, v1, :cond_56

    #@4f
    .line 929
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGreekCoptic:Landroid/util/SparseIntArray;

    #@51
    invoke-virtual {v1, p0, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@54
    move-result v0

    #@55
    goto :goto_c

    #@56
    .line 930
    :cond_56
    const/16 v1, 0x3ff

    #@58
    if-le p0, v1, :cond_65

    #@5a
    const/16 v1, 0x500

    #@5c
    if-ge p0, v1, :cond_65

    #@5e
    .line 931
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmCyrillic:Landroid/util/SparseIntArray;

    #@60
    invoke-virtual {v1, p0, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@63
    move-result v0

    #@64
    goto :goto_c

    #@65
    .line 932
    :cond_65
    const/16 v1, 0x1fff

    #@67
    if-le p0, v1, :cond_c

    #@69
    const/16 v1, 0x2070

    #@6b
    if-ge p0, v1, :cond_c

    #@6d
    .line 933
    sget-object v1, Lcom/android/internal/telephony/GsmAlphabet;->charToGsmGeneralPunctuation:Landroid/util/SparseIntArray;

    #@6f
    invoke-virtual {v1, p0, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@72
    move-result v0

    #@73
    goto :goto_c
.end method

.method private static packSmsChar([BII)V
    .registers 7
    .parameter "packedChars"
    .parameter "bitOffset"
    .parameter "value"

    #@0
    .prologue
    .line 506
    div-int/lit8 v0, p1, 0x8

    #@2
    .line 507
    .local v0, byteOffset:I
    rem-int/lit8 v1, p1, 0x8

    #@4
    .line 509
    .local v1, shift:I
    add-int/lit8 v0, v0, 0x1

    #@6
    aget-byte v2, p0, v0

    #@8
    shl-int v3, p2, v1

    #@a
    or-int/2addr v2, v3

    #@b
    int-to-byte v2, v2

    #@c
    aput-byte v2, p0, v0

    #@e
    .line 511
    const/4 v2, 0x1

    #@f
    if-le v1, v2, :cond_1a

    #@11
    .line 512
    add-int/lit8 v0, v0, 0x1

    #@13
    rsub-int/lit8 v2, v1, 0x8

    #@15
    shr-int v2, p2, v2

    #@17
    int-to-byte v2, v2

    #@18
    aput-byte v2, p0, v0

    #@1a
    .line 514
    :cond_1a
    return-void
.end method

.method static declared-synchronized setEnabledLockingShiftTables([I)V
    .registers 3
    .parameter "tables"

    #@0
    .prologue
    .line 1365
    const-class v1, Lcom/android/internal/telephony/GsmAlphabet;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sput-object p0, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@5
    .line 1366
    const/4 v0, 0x1

    #@6
    sput-boolean v0, Lcom/android/internal/telephony/GsmAlphabet;->sDisableCountryEncodingCheck:Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    #@8
    .line 1367
    monitor-exit v1

    #@9
    return-void

    #@a
    .line 1365
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1

    #@c
    throw v0
.end method

.method public static setEnabledLockingShiftTablesLG(Ljava/lang/String;)V
    .registers 2
    .parameter "mcc"

    #@0
    .prologue
    .line 1456
    invoke-static {p0}, Lcom/android/internal/telephony/GsmAlphabet;->getNationalLockingShiftEnableIndex(Ljava/lang/String;)[I

    #@3
    move-result-object v0

    #@4
    .line 1460
    .local v0, tables:[I
    sput-object v0, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledLockingShiftTables:[I

    #@6
    .line 1464
    return-void
.end method

.method public static setEnabledShiftTablesLG()V
    .registers 4

    #@0
    .prologue
    .line 1467
    const-string v2, "gsm.sim.operator.numeric"

    #@2
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 1471
    .local v1, networkOperator:Ljava/lang/String;
    const-string v0, "000"

    #@8
    .line 1472
    .local v0, mcc:Ljava/lang/String;
    if-eqz v1, :cond_17

    #@a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@d
    move-result v2

    #@e
    const/4 v3, 0x5

    #@f
    if-lt v2, v3, :cond_17

    #@11
    .line 1473
    const/4 v2, 0x0

    #@12
    const/4 v3, 0x3

    #@13
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 1475
    :cond_17
    invoke-static {v0}, Lcom/android/internal/telephony/GsmAlphabet;->setEnabledSingleShiftTablesLG(Ljava/lang/String;)V

    #@1a
    .line 1476
    invoke-static {v0}, Lcom/android/internal/telephony/GsmAlphabet;->setEnabledLockingShiftTablesLG(Ljava/lang/String;)V

    #@1d
    .line 1477
    return-void
.end method

.method static declared-synchronized setEnabledSingleShiftTables([I)V
    .registers 3
    .parameter "tables"

    #@0
    .prologue
    .line 1347
    const-class v1, Lcom/android/internal/telephony/GsmAlphabet;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sput-object p0, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@5
    .line 1348
    const/4 v0, 0x1

    #@6
    sput-boolean v0, Lcom/android/internal/telephony/GsmAlphabet;->sDisableCountryEncodingCheck:Z

    #@8
    .line 1350
    array-length v0, p0

    #@9
    if-lez v0, :cond_14

    #@b
    .line 1351
    array-length v0, p0

    #@c
    add-int/lit8 v0, v0, -0x1

    #@e
    aget v0, p0, v0

    #@10
    sput v0, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_18

    #@12
    .line 1355
    :goto_12
    monitor-exit v1

    #@13
    return-void

    #@14
    .line 1353
    :cond_14
    const/4 v0, 0x0

    #@15
    :try_start_15
    sput v0, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I
    :try_end_17
    .catchall {:try_start_15 .. :try_end_17} :catchall_18

    #@17
    goto :goto_12

    #@18
    .line 1347
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1

    #@1a
    throw v0
.end method

.method public static setEnabledSingleShiftTablesLG(Ljava/lang/String;)V
    .registers 3
    .parameter "mcc"

    #@0
    .prologue
    .line 1439
    invoke-static {p0}, Lcom/android/internal/telephony/GsmAlphabet;->getNationalSingleShiftEnableIndex(Ljava/lang/String;)[I

    #@3
    move-result-object v0

    #@4
    .line 1443
    .local v0, tables:[I
    sput-object v0, Lcom/android/internal/telephony/GsmAlphabet;->sEnabledSingleShiftTables:[I

    #@6
    .line 1445
    array-length v1, v0

    #@7
    if-lez v1, :cond_11

    #@9
    .line 1446
    array-length v1, v0

    #@a
    add-int/lit8 v1, v1, -0x1

    #@c
    aget v1, v0, v1

    #@e
    sput v1, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I

    #@10
    .line 1453
    :goto_10
    return-void

    #@11
    .line 1448
    :cond_11
    const/4 v1, 0x0

    #@12
    sput v1, Lcom/android/internal/telephony/GsmAlphabet;->sHighestEnabledSingleShiftCode:I

    #@14
    goto :goto_10
.end method

.method public static stringToGsm7BitPacked(Ljava/lang/String;)[B
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 383
    const/4 v0, 0x1

    #@2
    invoke-static {p0, v1, v0, v1, v1}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;IZII)[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static stringToGsm7BitPacked(Ljava/lang/String;II)[B
    .registers 5
    .parameter "data"
    .parameter "languageTable"
    .parameter "languageShiftTable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    .line 407
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-static {p0, v0, v1, p1, p2}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;IZII)[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static stringToGsm7BitPacked(Ljava/lang/String;IZII)[B
    .registers 20
    .parameter "data"
    .parameter "startingSeptetOffset"
    .parameter "throwException"
    .parameter "languageTable"
    .parameter "languageShiftTable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    .line 434
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v7

    #@4
    .line 440
    .local v7, dataLen:I
    if-nez p2, :cond_1e

    #@6
    const/4 v13, 0x1

    #@7
    :goto_7
    move/from16 v0, p3

    #@9
    move/from16 v1, p4

    #@b
    invoke-static {p0, v13, v0, v1}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptetsUsingTablesLossyAuto(Ljava/lang/CharSequence;ZII)I

    #@e
    move-result v10

    #@f
    .line 442
    .local v10, septetCount:I
    if-lez p4, :cond_13

    #@11
    .line 443
    const/16 p2, 0x1

    #@13
    .line 446
    :cond_13
    const/4 v13, -0x1

    #@14
    if-ne v10, v13, :cond_20

    #@16
    .line 447
    new-instance v13, Lcom/android/internal/telephony/EncodeException;

    #@18
    const-string v14, "countGsmSeptetsUsingTables(): unencodable char"

    #@1a
    invoke-direct {v13, v14}, Lcom/android/internal/telephony/EncodeException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v13

    #@1e
    .line 440
    .end local v10           #septetCount:I
    :cond_1e
    const/4 v13, 0x0

    #@1f
    goto :goto_7

    #@20
    .line 449
    .restart local v10       #septetCount:I
    :cond_20
    add-int v10, v10, p1

    #@22
    .line 450
    const/16 v13, 0xff

    #@24
    if-le v10, v13, :cond_2e

    #@26
    .line 451
    new-instance v13, Lcom/android/internal/telephony/EncodeException;

    #@28
    const-string v14, "Payload cannot exceed 255 septets"

    #@2a
    invoke-direct {v13, v14}, Lcom/android/internal/telephony/EncodeException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v13

    #@2e
    .line 453
    :cond_2e
    mul-int/lit8 v13, v10, 0x7

    #@30
    add-int/lit8 v13, v13, 0x7

    #@32
    div-int/lit8 v3, v13, 0x8

    #@34
    .line 454
    .local v3, byteCount:I
    add-int/lit8 v13, v3, 0x1

    #@36
    new-array v9, v13, [B

    #@38
    .line 455
    .local v9, ret:[B
    sget-object v13, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@3a
    aget-object v5, v13, p3

    #@3c
    .line 456
    .local v5, charToLanguageTable:Landroid/util/SparseIntArray;
    sget-object v13, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@3e
    aget-object v6, v13, p4

    #@40
    .line 457
    .local v6, charToShiftTable:Landroid/util/SparseIntArray;
    const/4 v8, 0x0

    #@41
    .local v8, i:I
    move/from16 v11, p1

    #@43
    .local v11, septets:I
    mul-int/lit8 v2, p1, 0x7

    #@45
    .line 458
    .local v2, bitOffset:I
    :goto_45
    if-ge v8, v7, :cond_82

    #@47
    if-ge v11, v10, :cond_82

    #@49
    .line 460
    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    #@4c
    move-result v4

    #@4d
    .line 461
    .local v4, c:C
    const/4 v13, -0x1

    #@4e
    invoke-virtual {v5, v4, v13}, Landroid/util/SparseIntArray;->get(II)I

    #@51
    move-result v12

    #@52
    .line 462
    .local v12, v:I
    const/4 v13, -0x1

    #@53
    if-ne v12, v13, :cond_78

    #@55
    .line 464
    if-nez p2, :cond_5b

    #@57
    .line 465
    invoke-static {v4}, Lcom/android/internal/telephony/GsmAlphabet;->lookupLossy7bitTable(C)I

    #@5a
    move-result v12

    #@5b
    .line 467
    :cond_5b
    const/4 v13, -0x1

    #@5c
    if-ne v12, v13, :cond_78

    #@5e
    .line 470
    const/4 v13, -0x1

    #@5f
    invoke-virtual {v6, v4, v13}, Landroid/util/SparseIntArray;->get(II)I

    #@62
    move-result v12

    #@63
    .line 471
    const/4 v13, -0x1

    #@64
    if-ne v12, v13, :cond_6f

    #@66
    .line 480
    new-instance v13, Lcom/android/internal/telephony/EncodeException;

    #@68
    const-string/jumbo v14, "stringToGsm7BitPacked(): unencodable char"

    #@6b
    invoke-direct {v13, v14}, Lcom/android/internal/telephony/EncodeException;-><init>(Ljava/lang/String;)V

    #@6e
    throw v13

    #@6f
    .line 483
    :cond_6f
    const/16 v13, 0x1b

    #@71
    invoke-static {v9, v2, v13}, Lcom/android/internal/telephony/GsmAlphabet;->packSmsChar([BII)V

    #@74
    .line 484
    add-int/lit8 v2, v2, 0x7

    #@76
    .line 485
    add-int/lit8 v11, v11, 0x1

    #@78
    .line 489
    :cond_78
    invoke-static {v9, v2, v12}, Lcom/android/internal/telephony/GsmAlphabet;->packSmsChar([BII)V

    #@7b
    .line 490
    add-int/lit8 v11, v11, 0x1

    #@7d
    .line 459
    add-int/lit8 v8, v8, 0x1

    #@7f
    add-int/lit8 v2, v2, 0x7

    #@81
    goto :goto_45

    #@82
    .line 492
    .end local v4           #c:C
    .end local v12           #v:I
    :cond_82
    const/4 v13, 0x0

    #@83
    int-to-byte v14, v10

    #@84
    aput-byte v14, v9, v13

    #@86
    .line 493
    return-object v9
.end method

.method public static stringToGsm7BitPackedWithHeader(Ljava/lang/String;[B)[B
    .registers 3
    .parameter "data"
    .parameter "header"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 282
    invoke-static {p0, p1, v0, v0}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BII)[B

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BII)[B
    .registers 13
    .parameter "data"
    .parameter "header"
    .parameter "languageTable"
    .parameter "languageShiftTable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 321
    const-string/jumbo v7, "persist.gsm.sms.forcegsm7"

    #@5
    const-string v8, "1"

    #@7
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 322
    .local v0, encodingType:Ljava/lang/String;
    const-string v7, "0"

    #@d
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v7

    #@11
    if-nez v7, :cond_1e

    #@13
    move v3, v5

    #@14
    .line 323
    .local v3, isThrowException:Z
    :goto_14
    if-eqz p1, :cond_19

    #@16
    array-length v7, p1

    #@17
    if-nez v7, :cond_20

    #@19
    .line 324
    :cond_19
    invoke-static {p0, v6, v3, p2, p3}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;IZII)[B

    #@1c
    move-result-object v4

    #@1d
    .line 342
    :goto_1d
    return-object v4

    #@1e
    .end local v3           #isThrowException:Z
    :cond_1e
    move v3, v6

    #@1f
    .line 322
    goto :goto_14

    #@20
    .line 328
    .restart local v3       #isThrowException:Z
    :cond_20
    array-length v7, p1

    #@21
    add-int/lit8 v7, v7, 0x1

    #@23
    mul-int/lit8 v1, v7, 0x8

    #@25
    .line 329
    .local v1, headerBits:I
    add-int/lit8 v7, v1, 0x6

    #@27
    div-int/lit8 v2, v7, 0x7

    #@29
    .line 336
    .local v2, headerSeptets:I
    invoke-static {p0, v2, v3, p2, p3}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;IZII)[B

    #@2c
    move-result-object v4

    #@2d
    .line 340
    .local v4, ret:[B
    array-length v7, p1

    #@2e
    int-to-byte v7, v7

    #@2f
    aput-byte v7, v4, v5

    #@31
    .line 341
    const/4 v5, 0x2

    #@32
    array-length v7, p1

    #@33
    invoke-static {p1, v6, v4, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@36
    goto :goto_1d
.end method

.method public static stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BIIZ)[B
    .registers 11
    .parameter "data"
    .parameter "header"
    .parameter "languageTable"
    .parameter "languageShiftTable"
    .parameter "throwException"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/EncodeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 350
    if-eqz p1, :cond_6

    #@3
    array-length v3, p1

    #@4
    if-nez v3, :cond_b

    #@6
    .line 351
    :cond_6
    invoke-static {p0, v5, p4, v5, v5}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;IZII)[B

    #@9
    move-result-object v2

    #@a
    .line 362
    :goto_a
    return-object v2

    #@b
    .line 354
    :cond_b
    array-length v3, p1

    #@c
    add-int/lit8 v3, v3, 0x1

    #@e
    mul-int/lit8 v0, v3, 0x8

    #@10
    .line 355
    .local v0, headerBits:I
    add-int/lit8 v3, v0, 0x6

    #@12
    div-int/lit8 v1, v3, 0x7

    #@14
    .line 357
    .local v1, headerSeptets:I
    invoke-static {p0, v1, p4, p2, p3}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;IZII)[B

    #@17
    move-result-object v2

    #@18
    .line 360
    .local v2, ret:[B
    const/4 v3, 0x1

    #@19
    array-length v4, p1

    #@1a
    int-to-byte v4, v4

    #@1b
    aput-byte v4, v2, v3

    #@1d
    .line 361
    const/4 v3, 0x2

    #@1e
    array-length v4, p1

    #@1f
    invoke-static {p1, v5, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@22
    goto :goto_a
.end method

.method public static stringToGsm8BitPacked(Ljava/lang/String;)[B
    .registers 5
    .parameter "s"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 741
    const/4 v2, 0x1

    #@2
    invoke-static {p0, v2, v3, v3}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptetsUsingTables(Ljava/lang/CharSequence;ZII)I

    #@5
    move-result v1

    #@6
    .line 744
    .local v1, septets:I
    new-array v0, v1, [B

    #@8
    .line 746
    .local v0, ret:[B
    array-length v2, v0

    #@9
    invoke-static {p0, v0, v3, v2}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm8BitUnpackedField(Ljava/lang/String;[BII)V

    #@c
    .line 748
    return-object v0
.end method

.method public static stringToGsm8BitUnpackedField(Ljava/lang/String;[BII)V
    .registers 16
    .parameter "s"
    .parameter "dest"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/16 v11, 0x20

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v9, -0x1

    #@4
    .line 763
    move v4, p2

    #@5
    .line 764
    .local v4, outByteIndex:I
    sget-object v8, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToGsmTables:[Landroid/util/SparseIntArray;

    #@7
    aget-object v1, v8, v10

    #@9
    .line 765
    .local v1, charToLanguageTable:Landroid/util/SparseIntArray;
    sget-object v8, Lcom/android/internal/telephony/GsmAlphabet;->sCharsToShiftTables:[Landroid/util/SparseIntArray;

    #@b
    aget-object v2, v8, v10

    #@d
    .line 768
    .local v2, charToShiftTable:Landroid/util/SparseIntArray;
    const/4 v3, 0x0

    #@e
    .local v3, i:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@11
    move-result v6

    #@12
    .local v6, sz:I
    move v5, v4

    #@13
    .line 769
    .end local v4           #outByteIndex:I
    .local v5, outByteIndex:I
    :goto_13
    if-ge v3, v6, :cond_3b

    #@15
    sub-int v8, v5, p2

    #@17
    if-ge v8, p3, :cond_3b

    #@19
    .line 772
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@1c
    move-result v0

    #@1d
    .line 774
    .local v0, c:C
    invoke-virtual {v1, v0, v9}, Landroid/util/SparseIntArray;->get(II)I

    #@20
    move-result v7

    #@21
    .line 776
    .local v7, v:I
    if-ne v7, v9, :cond_4d

    #@23
    .line 777
    invoke-virtual {v2, v0, v9}, Landroid/util/SparseIntArray;->get(II)I

    #@26
    move-result v7

    #@27
    .line 778
    if-ne v7, v9, :cond_36

    #@29
    .line 779
    invoke-virtual {v1, v11, v11}, Landroid/util/SparseIntArray;->get(II)I

    #@2c
    move-result v7

    #@2d
    move v4, v5

    #@2e
    .line 790
    .end local v5           #outByteIndex:I
    .restart local v4       #outByteIndex:I
    :goto_2e
    add-int/lit8 v5, v4, 0x1

    #@30
    .end local v4           #outByteIndex:I
    .restart local v5       #outByteIndex:I
    int-to-byte v8, v7

    #@31
    aput-byte v8, p1, v4

    #@33
    .line 770
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_13

    #@36
    .line 782
    :cond_36
    add-int/lit8 v8, v5, 0x1

    #@38
    sub-int/2addr v8, p2

    #@39
    if-lt v8, p3, :cond_45

    #@3b
    .line 794
    .end local v0           #c:C
    .end local v7           #v:I
    :cond_3b
    :goto_3b
    sub-int v8, v5, p2

    #@3d
    if-ge v8, p3, :cond_4c

    #@3f
    .line 795
    add-int/lit8 v4, v5, 0x1

    #@41
    .end local v5           #outByteIndex:I
    .restart local v4       #outByteIndex:I
    aput-byte v9, p1, v5

    #@43
    move v5, v4

    #@44
    .end local v4           #outByteIndex:I
    .restart local v5       #outByteIndex:I
    goto :goto_3b

    #@45
    .line 786
    .restart local v0       #c:C
    .restart local v7       #v:I
    :cond_45
    add-int/lit8 v4, v5, 0x1

    #@47
    .end local v5           #outByteIndex:I
    .restart local v4       #outByteIndex:I
    const/16 v8, 0x1b

    #@49
    aput-byte v8, p1, v5

    #@4b
    goto :goto_2e

    #@4c
    .line 797
    .end local v0           #c:C
    .end local v4           #outByteIndex:I
    .end local v7           #v:I
    .restart local v5       #outByteIndex:I
    :cond_4c
    return-void

    #@4d
    .restart local v0       #c:C
    .restart local v7       #v:I
    :cond_4d
    move v4, v5

    #@4e
    .end local v5           #outByteIndex:I
    .restart local v4       #outByteIndex:I
    goto :goto_2e
.end method
