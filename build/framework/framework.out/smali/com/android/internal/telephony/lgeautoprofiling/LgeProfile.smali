.class public Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;
.super Ljava/lang/Object;
.source "LgeProfile.java"

# interfaces
.implements Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfilingConstants;


# static fields
.field private static final DBG:Z = true

.field private static final EDBG:Z = true

.field private static final VDBG:Z = true

.field private static instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLastSimState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mProfiles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@5
    .line 53
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mProfiles:Ljava/util/HashMap;

    #@c
    .line 54
    new-instance v0, Ljava/util/HashMap;

    #@e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mLastSimState:Ljava/util/HashMap;

    #@13
    .line 55
    return-void
.end method

.method private getEccListSimLock(I)Ljava/lang/String;
    .registers 6
    .parameter "subId"

    #@0
    .prologue
    .line 275
    const-string/jumbo v2, "sim_lock_ecclist"

    #@3
    invoke-virtual {p0, v2, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getValue(Ljava/lang/String;I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 278
    .local v0, eccListSimLock:Ljava/lang/String;
    const-string/jumbo v2, "ril.temp.countrycodeforoneimage"

    #@a
    const/4 v3, 0x0

    #@b
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@e
    move-result v1

    #@f
    .line 280
    .local v1, mcc:I
    const/16 v2, 0xea

    #@11
    if-eq v1, v2, :cond_1f

    #@13
    const/16 v2, 0x110

    #@15
    if-eq v1, v2, :cond_1f

    #@17
    const/16 v2, 0x1c6

    #@19
    if-eq v1, v2, :cond_1f

    #@1b
    const/16 v2, 0x1c7

    #@1d
    if-ne v1, v2, :cond_21

    #@1f
    .line 283
    :cond_1f
    const-string v0, "999"

    #@21
    .line 287
    :cond_21
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 58
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 59
    new-instance v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@6
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@b
    .line 64
    :goto_b
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@d
    return-object v0

    #@e
    .line 61
    :cond_e
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@10
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->setContext(Landroid/content/Context;)V

    #@13
    goto :goto_b
.end method

.method private loadProfileFromPreferences(IZ)Z
    .registers 10
    .parameter "subId"
    .parameter "defaultProfile"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 91
    const-string v4, "TelephonyAutoProfiling"

    #@3
    new-instance v5, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v6, "[loadProfileFromPreferences] *** start profile loading from preferences - defaultProfile : "

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v5

    #@16
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 93
    const/4 v0, 0x0

    #@1a
    .line 95
    .local v0, prefName:Ljava/lang/String;
    if-eqz p2, :cond_46

    #@1c
    .line 96
    const-string v0, "defaultProfile"

    #@1e
    .line 101
    :goto_1e
    iget-object v4, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@20
    invoke-virtual {v4, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@23
    move-result-object v1

    #@24
    .line 103
    .local v1, preferences:Landroid/content/SharedPreferences;
    const-string v4, "init"

    #@26
    const/4 v5, 0x0

    #@27
    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    if-nez v4, :cond_5b

    #@2d
    .line 104
    const-string v4, "TelephonyAutoProfiling"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "[loadProfileFromPreferences] preferences do not exist - subId : "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 118
    :goto_45
    return v3

    #@46
    .line 98
    .end local v1           #preferences:Landroid/content/SharedPreferences;
    :cond_46
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string/jumbo v5, "profile_"

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    goto :goto_1e

    #@5b
    .line 110
    .restart local v1       #preferences:Landroid/content/SharedPreferences;
    :cond_5b
    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    #@5e
    move-result-object v2

    #@5f
    check-cast v2, Ljava/util/HashMap;

    #@61
    .line 112
    .local v2, profile:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "init"

    #@63
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@66
    .line 114
    iget-object v3, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mProfiles:Ljava/util/HashMap;

    #@68
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6f
    .line 116
    const-string v3, "TelephonyAutoProfiling"

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v5, "[loadProfileFromPreferences] *** profile loading from preferences complete - defaultProfile : "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    const-string v5, ", subId : "

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@89
    move-result-object v4

    #@8a
    const-string v5, " - "

    #@8c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v4

    #@90
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v4

    #@98
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    .line 118
    const/4 v3, 0x1

    #@9c
    goto :goto_45
.end method

.method private loadProfileFromXml(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;I)V
    .registers 9
    .parameter "simInfo"
    .parameter "subId"

    #@0
    .prologue
    .line 122
    const-string v3, "TelephonyAutoProfiling"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "[loadProfileFromXml] *** start profile loading from xml - subId : "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 124
    new-instance v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;

    #@1a
    invoke-direct {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;-><init>()V

    #@1d
    .line 126
    .local v0, parser:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;
    const/4 v3, 0x1

    #@1e
    invoke-virtual {v0, v3, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->getMatchedProfile(ILcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;

    #@24
    .line 128
    .local v1, profileFromXml:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;
    if-nez v1, :cond_2e

    #@26
    .line 129
    const-string v3, "TelephonyAutoProfiling"

    #@28
    const-string v4, "[profileFromXml] load profile from xml failed"

    #@2a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 153
    :goto_2d
    return-void

    #@2e
    .line 134
    :cond_2e
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->profileToMap(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@31
    move-result-object v2

    #@32
    .line 136
    .local v2, profileMapFromXml:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v2, :cond_3c

    #@34
    .line 137
    const-string v3, "TelephonyAutoProfiling"

    #@36
    const-string v4, "[loadDataFromXml] load profile Map from xml failed"

    #@38
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    goto :goto_2d

    #@3c
    .line 142
    :cond_3c
    iget-object v3, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mProfiles:Ljava/util/HashMap;

    #@3e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    .line 144
    iget-object v3, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@47
    if-eqz v3, :cond_62

    #@49
    .line 145
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->isNull()Z

    #@4c
    move-result v3

    #@4d
    if-nez v3, :cond_5b

    #@4f
    .line 146
    const-string v3, "TelephonyAutoProfiling"

    #@51
    const-string v4, "[loadProfileFromXml] save SimInfo to preferences"

    #@53
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 147
    iget-object v3, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@58
    invoke-static {v3, p1, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->writeToPreference(Landroid/content/Context;Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;I)V

    #@5b
    .line 149
    :cond_5b
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->isNull()Z

    #@5e
    move-result v3

    #@5f
    invoke-direct {p0, v2, p2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->saveProfileToPreferences(Ljava/util/HashMap;IZ)V

    #@62
    .line 152
    :cond_62
    const-string v3, "TelephonyAutoProfiling"

    #@64
    new-instance v4, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v5, "[loadProfileFromXml] *** end profile loading from xml - "

    #@6b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v4

    #@77
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    goto :goto_2d
.end method

.method private saveProfileToPreferences(Ljava/util/HashMap;IZ)V
    .registers 11
    .parameter
    .parameter "subId"
    .parameter "simInfoIsNull"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;IZ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 157
    .local p1, profileMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@1
    .line 159
    .local v3, prefName:Ljava/lang/String;
    if-eqz p3, :cond_56

    #@3
    .line 160
    const-string v3, "defaultProfile"

    #@5
    .line 169
    :goto_5
    iget-object v4, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@7
    const/4 v5, 0x0

    #@8
    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@b
    move-result-object v4

    #@c
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@f
    move-result-object v0

    #@10
    .line 171
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "TelephonyAutoProfiling"

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "[saveProfileToPreferences] save to "

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 173
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    #@2b
    .line 175
    const-string v4, "init"

    #@2d
    const-string v5, "done"

    #@2f
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@32
    .line 177
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@35
    move-result-object v4

    #@36
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@39
    move-result-object v2

    #@3a
    .local v2, i$:Ljava/util/Iterator;
    :goto_3a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_86

    #@40
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@43
    move-result-object v1

    #@44
    check-cast v1, Ljava/util/Map$Entry;

    #@46
    .line 178
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@49
    move-result-object v4

    #@4a
    check-cast v4, Ljava/lang/String;

    #@4c
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@4f
    move-result-object v5

    #@50
    check-cast v5, Ljava/lang/String;

    #@52
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@55
    goto :goto_3a

    #@56
    .line 161
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_56
    if-nez p3, :cond_6d

    #@58
    .line 162
    new-instance v4, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string/jumbo v5, "profile_"

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    goto :goto_5

    #@6d
    .line 164
    :cond_6d
    const-string v4, "TelephonyAutoProfiling"

    #@6f
    new-instance v5, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v6, "[saveProfileToPreferences] simInfo is null, do not save to preferences, subId : "

    #@76
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 182
    :goto_85
    return-void

    #@86
    .line 181
    .restart local v0       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_86
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@89
    goto :goto_85
.end method

.method private setContext(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 69
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@6
    .line 71
    :cond_6
    return-void
.end method


# virtual methods
.method public getLastSimState(I)Ljava/lang/String;
    .registers 5
    .parameter "subId"

    #@0
    .prologue
    .line 237
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mLastSimState:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    .line 239
    .local v0, lastSimState:Ljava/lang/String;
    if-nez v0, :cond_10

    #@e
    const-string v0, ""

    #@10
    .end local v0           #lastSimState:Ljava/lang/String;
    :cond_10
    return-object v0
.end method

.method public getValue(Ljava/lang/String;I)Ljava/lang/String;
    .registers 7
    .parameter "key"
    .parameter "subId"

    #@0
    .prologue
    .line 291
    const/4 v0, 0x0

    #@1
    .line 293
    .local v0, value:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mProfiles:Ljava/util/HashMap;

    #@3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    if-nez v1, :cond_10

    #@d
    .line 294
    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->loadProfile(I)V

    #@10
    .line 297
    :cond_10
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mProfiles:Ljava/util/HashMap;

    #@12
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    if-eqz v1, :cond_2e

    #@1c
    .line 298
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mProfiles:Ljava/util/HashMap;

    #@1e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Ljava/util/HashMap;

    #@28
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    .end local v0           #value:Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    #@2e
    .line 301
    .restart local v0       #value:Ljava/lang/String;
    :cond_2e
    const-string v1, "TelephonyAutoProfiling"

    #@30
    new-instance v2, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v3, "[getValue] PROFILE key : "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    const-string v3, ", value : "

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    const-string v3, ", subId : "

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 303
    if-nez v0, :cond_5e

    #@5c
    const-string v0, ""

    #@5e
    .end local v0           #value:Ljava/lang/String;
    :cond_5e
    return-object v0
.end method

.method public loadProfile(I)V
    .registers 5
    .parameter "subId"

    #@0
    .prologue
    .line 74
    invoke-static {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo(I)Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@3
    move-result-object v1

    #@4
    .line 75
    .local v1, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    const/4 v0, 0x0

    #@5
    .line 77
    .local v0, loadSuccessFromPreferences:Z
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@7
    if-eqz v2, :cond_14

    #@9
    .line 78
    invoke-virtual {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->isNull()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1a

    #@f
    .line 79
    const/4 v2, 0x1

    #@10
    invoke-direct {p0, p1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->loadProfileFromPreferences(IZ)Z

    #@13
    move-result v0

    #@14
    .line 85
    :cond_14
    :goto_14
    if-nez v0, :cond_19

    #@16
    .line 86
    invoke-direct {p0, v1, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->loadProfileFromXml(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;I)V

    #@19
    .line 88
    :cond_19
    return-void

    #@1a
    .line 80
    :cond_1a
    invoke-virtual {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->isNull()Z

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_14

    #@20
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@22
    invoke-static {v2, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->createFromPreference(Landroid/content/Context;I)Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->equals(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_14

    #@2c
    .line 81
    const/4 v2, 0x0

    #@2d
    invoke-direct {p0, p1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->loadProfileFromPreferences(IZ)Z

    #@30
    move-result v0

    #@31
    goto :goto_14
.end method

.method public setEccList(Ljava/lang/String;I)V
    .registers 9
    .parameter "simState"
    .parameter "subId"

    #@0
    .prologue
    .line 243
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@3
    move-result v1

    #@4
    .line 244
    .local v1, defaultSubId:I
    const/4 v2, 0x0

    #@5
    .line 245
    .local v2, eccList:Ljava/lang/String;
    const/4 v0, 0x0

    #@6
    .line 247
    .local v0, changed:Z
    if-ne p2, v1, :cond_46

    #@8
    .line 248
    const-string v3, "ABSENT"

    #@a
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_47

    #@10
    .line 249
    const-string/jumbo v3, "no_sim_ecclist"

    #@13
    invoke-virtual {p0, v3, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getValue(Ljava/lang/String;I)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    .line 250
    const/4 v0, 0x1

    #@18
    .line 252
    sget-boolean v3, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->ENABLE_PRIVACY_LOG:Z

    #@1a
    if-eqz v3, :cond_3e

    #@1c
    .line 253
    const-string v3, "TelephonyAutoProfiling"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, "[setEccList] subId : "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, ", KEY_NO_SIM_ECCLIST : "

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 268
    :cond_3e
    :goto_3e
    if-eqz v0, :cond_46

    #@40
    .line 269
    const-string/jumbo v3, "ril.ecclist.autoprofile"

    #@43
    invoke-static {v3, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 272
    :cond_46
    return-void

    #@47
    .line 254
    :cond_47
    const-string v3, "LOCKED"

    #@49
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v3

    #@4d
    if-eqz v3, :cond_7b

    #@4f
    .line 255
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getEccListSimLock(I)Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    .line 256
    const/4 v0, 0x1

    #@54
    .line 258
    sget-boolean v3, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->ENABLE_PRIVACY_LOG:Z

    #@56
    if-eqz v3, :cond_3e

    #@58
    .line 259
    const-string v3, "TelephonyAutoProfiling"

    #@5a
    new-instance v4, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v5, "[setEccList] subId : "

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    const-string v5, ", KEY_SIM_LOCK_ECCLIST : "

    #@6b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v4

    #@77
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    goto :goto_3e

    #@7b
    .line 260
    :cond_7b
    const-string v3, "LOADED"

    #@7d
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@80
    move-result v3

    #@81
    if-eqz v3, :cond_3e

    #@83
    .line 261
    const-string v3, "ECC_list"

    #@85
    invoke-virtual {p0, v3, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getValue(Ljava/lang/String;I)Ljava/lang/String;

    #@88
    move-result-object v2

    #@89
    .line 262
    const/4 v0, 0x1

    #@8a
    .line 264
    sget-boolean v3, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->ENABLE_PRIVACY_LOG:Z

    #@8c
    if-eqz v3, :cond_3e

    #@8e
    .line 265
    const-string v3, "TelephonyAutoProfiling"

    #@90
    new-instance v4, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v5, "[setEccList] subId : "

    #@97
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    const-string v5, ", KEY_ECC_LIST : "

    #@a1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v4

    #@a5
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v4

    #@a9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v4

    #@ad
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    goto :goto_3e
.end method

.method public setLastSimState(ILjava/lang/String;)V
    .registers 5
    .parameter "subId"
    .parameter "simState"

    #@0
    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mLastSimState:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 234
    return-void
.end method

.method public updateProfile(Landroid/content/Intent;)V
    .registers 9
    .parameter "intent"

    #@0
    .prologue
    .line 185
    iget-object v4, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->mContext:Landroid/content/Context;

    #@2
    if-nez v4, :cond_c

    #@4
    .line 186
    const-string v4, "TelephonyAutoProfiling"

    #@6
    const-string v5, "[updateProfile] context is null, return"

    #@8
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 230
    :cond_b
    :goto_b
    return-void

    #@c
    .line 191
    :cond_c
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 193
    .local v0, action:Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@13
    move-result v3

    #@14
    .line 196
    .local v3, subId:I
    sget-boolean v4, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->MULTI_SIM_ENABLED:Z

    #@16
    if-eqz v4, :cond_1f

    #@18
    .line 197
    const-string/jumbo v4, "subscription"

    #@1b
    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1e
    move-result v3

    #@1f
    .line 200
    :cond_1f
    const-string v4, "android.intent.action.SIM_STATE_CHANGED"

    #@21
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_93

    #@27
    .line 201
    const-string/jumbo v4, "ss"

    #@2a
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    .line 203
    .local v2, simState:Ljava/lang/String;
    const-string v4, "ABSENT"

    #@30
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v4

    #@34
    if-nez v4, :cond_46

    #@36
    const-string v4, "LOCKED"

    #@38
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v4

    #@3c
    if-nez v4, :cond_46

    #@3e
    const-string v4, "LOADED"

    #@40
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v4

    #@44
    if-eqz v4, :cond_b

    #@46
    .line 206
    :cond_46
    const-string v4, "TelephonyAutoProfiling"

    #@48
    new-instance v5, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v6, "[updateProfile] ACTION_SIM_STATE_CHANGED - simState : "

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    const-string v6, ", subId : "

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 208
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getLastSimState(I)Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v4

    #@70
    if-eqz v4, :cond_7a

    #@72
    .line 209
    const-string v4, "TelephonyAutoProfiling"

    #@74
    const-string v5, "[updateProfile] repeated SIM_STATE, ignore"

    #@76
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    goto :goto_b

    #@7a
    .line 212
    :cond_7a
    const-string v4, "TelephonyAutoProfiling"

    #@7c
    const-string v5, "[updateProfile] new SIM_STATE, continue process"

    #@7e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 213
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->setLastSimState(ILjava/lang/String;)V

    #@84
    .line 216
    const-string v4, "TelephonyAutoProfiling"

    #@86
    const-string v5, "[updateProfile] loadProfile"

    #@88
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 218
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->loadProfile(I)V

    #@8e
    .line 220
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->setEccList(Ljava/lang/String;I)V

    #@91
    goto/16 :goto_b

    #@93
    .line 222
    .end local v2           #simState:Ljava/lang/String;
    :cond_93
    const-string/jumbo v4, "qualcomm.intent.action.ACTION_DEFAULT_SUBSCRIPTION_CHANGED"

    #@96
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v4

    #@9a
    if-eqz v4, :cond_b

    #@9c
    .line 223
    const-string v4, "ECC_list"

    #@9e
    invoke-virtual {p0, v4, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getValue(Ljava/lang/String;I)Ljava/lang/String;

    #@a1
    move-result-object v1

    #@a2
    .line 225
    .local v1, eccList:Ljava/lang/String;
    const-string/jumbo v4, "ril.ecclist.autoprofile"

    #@a5
    invoke-static {v4, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a8
    .line 227
    sget-boolean v4, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->ENABLE_PRIVACY_LOG:Z

    #@aa
    if-eqz v4, :cond_b

    #@ac
    .line 228
    const-string v4, "TelephonyAutoProfiling"

    #@ae
    new-instance v5, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v6, "[updateProfile] ACTION_DEFAULT_SUBSCRIPTION_CHANGED - subId : "

    #@b5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v5

    #@b9
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    const-string v6, ", setEccList : "

    #@bf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v5

    #@c3
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v5

    #@c7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v5

    #@cb
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    goto/16 :goto_b
.end method
