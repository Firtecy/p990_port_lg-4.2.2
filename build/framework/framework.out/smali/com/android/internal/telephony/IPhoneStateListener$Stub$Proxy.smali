.class Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IPhoneStateListener.java"

# interfaces
.implements Lcom/android/internal/telephony/IPhoneStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IPhoneStateListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 159
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 160
    iput-object p1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 161
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 168
    const-string v0, "com.android.internal.telephony.IPhoneStateListener"

    #@2
    return-object v0
.end method

.method public onCallForwardingIndicatorChanged(Z)V
    .registers 7
    .parameter "cfi"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 214
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 216
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.telephony.IPhoneStateListener"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 217
    if-eqz p1, :cond_1b

    #@c
    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 218
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x4

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1d

    #@17
    .line 221
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 223
    return-void

    #@1b
    .line 217
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_c

    #@1d
    .line 221
    :catchall_1d
    move-exception v1

    #@1e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    throw v1
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .registers 8
    .parameter "state"
    .parameter "incomingNumber"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 246
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 248
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 249
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 250
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 251
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x6

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 254
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 256
    return-void

    #@1b
    .line 254
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method

.method public onCellInfoChanged(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 314
    .local p1, cellInfo:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 316
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 317
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@c
    .line 318
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/16 v2, 0xb

    #@10
    const/4 v3, 0x0

    #@11
    const/4 v4, 0x1

    #@12
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_19

    #@15
    .line 321
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@18
    .line 323
    return-void

    #@19
    .line 321
    :catchall_19
    move-exception v1

    #@1a
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    throw v1
.end method

.method public onCellLocationChanged(Landroid/os/Bundle;)V
    .registers 7
    .parameter "location"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 228
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 230
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 231
    if-eqz p1, :cond_1f

    #@b
    .line 232
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 233
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 238
    :goto_13
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x5

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_24

    #@1b
    .line 241
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 243
    return-void

    #@1f
    .line 236
    :cond_1f
    const/4 v1, 0x0

    #@20
    :try_start_20
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 241
    :catchall_24
    move-exception v1

    #@25
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v1
.end method

.method public onDataActivity(I)V
    .registers 7
    .parameter "direction"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 272
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 274
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 275
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 276
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/16 v2, 0x8

    #@10
    const/4 v3, 0x0

    #@11
    const/4 v4, 0x1

    #@12
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_19

    #@15
    .line 279
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@18
    .line 281
    return-void

    #@19
    .line 279
    :catchall_19
    move-exception v1

    #@1a
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    throw v1
.end method

.method public onDataConnectionStateChanged(II)V
    .registers 8
    .parameter "state"
    .parameter "networkType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 261
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 262
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 263
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 264
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x7

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 267
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 269
    return-void

    #@1b
    .line 267
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method

.method public onMessageWaitingIndicatorChanged(Z)V
    .registers 7
    .parameter "mwi"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 202
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 204
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.telephony.IPhoneStateListener"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 205
    if-eqz p1, :cond_1b

    #@c
    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 206
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x3

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1d

    #@17
    .line 209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 211
    return-void

    #@1b
    .line 205
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_c

    #@1d
    .line 209
    :catchall_1d
    move-exception v1

    #@1e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    throw v1
.end method

.method public onOtaspChanged(I)V
    .registers 7
    .parameter "otaspMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 302
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 304
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 305
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 306
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/16 v2, 0xa

    #@10
    const/4 v3, 0x0

    #@11
    const/4 v4, 0x1

    #@12
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_19

    #@15
    .line 309
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@18
    .line 311
    return-void

    #@19
    .line 309
    :catchall_19
    move-exception v1

    #@1a
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    throw v1
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 7
    .parameter "serviceState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 172
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 174
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 175
    if-eqz p1, :cond_1f

    #@b
    .line 176
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 177
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/telephony/ServiceState;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 182
    :goto_13
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x1

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_24

    #@1b
    .line 185
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 187
    return-void

    #@1f
    .line 180
    :cond_1f
    const/4 v1, 0x0

    #@20
    :try_start_20
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 185
    :catchall_24
    move-exception v1

    #@25
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v1
.end method

.method public onSignalStrengthChanged(I)V
    .registers 7
    .parameter "asu"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 190
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 192
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 193
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 194
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x2

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 199
    return-void

    #@18
    .line 197
    :catchall_18
    move-exception v1

    #@19
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    throw v1
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .registers 7
    .parameter "signalStrength"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 284
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 286
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 287
    if-eqz p1, :cond_20

    #@b
    .line 288
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 289
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/telephony/SignalStrength;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 294
    :goto_13
    iget-object v1, p0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v2, 0x9

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x1

    #@19
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_25

    #@1c
    .line 297
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 299
    return-void

    #@20
    .line 292
    :cond_20
    const/4 v1, 0x0

    #@21
    :try_start_21
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_24
    .catchall {:try_start_21 .. :try_end_24} :catchall_25

    #@24
    goto :goto_13

    #@25
    .line 297
    :catchall_25
    move-exception v1

    #@26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v1
.end method
