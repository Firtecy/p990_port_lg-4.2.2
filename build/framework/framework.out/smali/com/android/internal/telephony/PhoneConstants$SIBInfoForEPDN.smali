.class public final enum Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
.super Ljava/lang/Enum;
.source "PhoneConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PhoneConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SIBInfoForEPDN"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public static final enum EMER_ATTACH_NOT_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public static final enum EMER_ATTACH_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public static final enum EPDN_BARRED:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public static final enum EPDN_NOT_BARRED:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field public static final enum NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

.field private static final sSIBInfoForEPDNMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCode:I


# direct methods
.method static constructor <clinit>()V
    .registers 11

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    const/4 v9, 0x3

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 240
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@7
    const-string v5, "NONE"

    #@9
    invoke-direct {v4, v5, v6, v6}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@e
    .line 241
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@10
    const-string v5, "EMER_ATTACH_NOT_SUPPORT"

    #@12
    invoke-direct {v4, v5, v7, v7}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EMER_ATTACH_NOT_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@17
    .line 242
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@19
    const-string v5, "EMER_ATTACH_SUPPORT"

    #@1b
    invoke-direct {v4, v5, v8, v8}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EMER_ATTACH_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@20
    .line 243
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@22
    const-string v5, "EPDN_NOT_BARRED"

    #@24
    invoke-direct {v4, v5, v9, v9}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EPDN_NOT_BARRED:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@29
    .line 244
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@2b
    const-string v5, "EPDN_BARRED"

    #@2d
    invoke-direct {v4, v5, v10, v10}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EPDN_BARRED:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@32
    .line 239
    const/4 v4, 0x5

    #@33
    new-array v4, v4, [Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@35
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@37
    aput-object v5, v4, v6

    #@39
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EMER_ATTACH_NOT_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@3b
    aput-object v5, v4, v7

    #@3d
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EMER_ATTACH_SUPPORT:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@3f
    aput-object v5, v4, v8

    #@41
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EPDN_NOT_BARRED:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@43
    aput-object v5, v4, v9

    #@45
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->EPDN_BARRED:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@47
    aput-object v5, v4, v10

    #@49
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@4b
    .line 249
    new-instance v4, Ljava/util/HashMap;

    #@4d
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@50
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->sSIBInfoForEPDNMap:Ljava/util/HashMap;

    #@52
    .line 250
    invoke-static {}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->values()[Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@55
    move-result-object v0

    #@56
    .local v0, arr$:[Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
    array-length v2, v0

    #@57
    .local v2, len$:I
    const/4 v1, 0x0

    #@58
    .local v1, i$:I
    :goto_58
    if-ge v1, v2, :cond_6c

    #@5a
    aget-object v3, v0, v1

    #@5c
    .line 251
    .local v3, sie:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
    sget-object v4, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->sSIBInfoForEPDNMap:Ljava/util/HashMap;

    #@5e
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->getCode()I

    #@61
    move-result v5

    #@62
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    .line 250
    add-int/lit8 v1, v1, 0x1

    #@6b
    goto :goto_58

    #@6c
    .line 253
    .end local v3           #sie:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
    :cond_6c
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "code"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 256
    iput p3, p0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->mCode:I

    #@5
    .line 257
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
    .registers 4
    .parameter "code"

    #@0
    .prologue
    .line 264
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->sSIBInfoForEPDNMap:Ljava/util/HashMap;

    #@2
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@c
    .line 265
    .local v0, sie:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
    if-nez v0, :cond_10

    #@e
    .line 266
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->NONE:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@10
    .line 268
    :cond_10
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 239
    const-class v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;
    .registers 1

    #@0
    .prologue
    .line 239
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@8
    return-object v0
.end method


# virtual methods
.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 260
    iget v0, p0, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->mCode:I

    #@2
    return v0
.end method
