.class public abstract Lcom/android/internal/telephony/IWapPushManager$Stub;
.super Landroid/os/Binder;
.source "IWapPushManager.java"

# interfaces
.implements Lcom/android/internal/telephony/IWapPushManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IWapPushManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.IWapPushManager"

.field static final TRANSACTION_addPackage:I = 0x2

.field static final TRANSACTION_deletePackage:I = 0x4

.field static final TRANSACTION_processMessage:I = 0x1

.field static final TRANSACTION_updatePackage:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.telephony.IWapPushManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/IWapPushManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IWapPushManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.telephony.IWapPushManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/IWapPushManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/telephony/IWapPushManager;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 16
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_d2

    #@5
    .line 125
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v9

    #@9
    :goto_9
    return v9

    #@a
    .line 42
    :sswitch_a
    const-string v0, "com.android.internal.telephony.IWapPushManager"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v0, "com.android.internal.telephony.IWapPushManager"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 51
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    .line 53
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_36

    #@23
    .line 54
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@25
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Landroid/content/Intent;

    #@2b
    .line 59
    .local v3, _arg2:Landroid/content/Intent;
    :goto_2b
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/telephony/IWapPushManager$Stub;->processMessage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)I

    #@2e
    move-result v8

    #@2f
    .line 60
    .local v8, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@32
    .line 61
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@35
    goto :goto_9

    #@36
    .line 57
    .end local v3           #_arg2:Landroid/content/Intent;
    .end local v8           #_result:I
    :cond_36
    const/4 v3, 0x0

    #@37
    .restart local v3       #_arg2:Landroid/content/Intent;
    goto :goto_2b

    #@38
    .line 66
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Landroid/content/Intent;
    :sswitch_38
    const-string v0, "com.android.internal.telephony.IWapPushManager"

    #@3a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    .line 70
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    .line 72
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    .line 74
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    .line 76
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v5

    #@51
    .line 78
    .local v5, _arg4:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v0

    #@55
    if-eqz v0, :cond_6e

    #@57
    move v6, v9

    #@58
    .line 80
    .local v6, _arg5:Z
    :goto_58
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_70

    #@5e
    move v7, v9

    #@5f
    .local v7, _arg6:Z
    :goto_5f
    move-object v0, p0

    #@60
    .line 81
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IWapPushManager$Stub;->addPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Z

    #@63
    move-result v8

    #@64
    .line 82
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    .line 83
    if-eqz v8, :cond_6a

    #@69
    move v10, v9

    #@6a
    :cond_6a
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    goto :goto_9

    #@6e
    .end local v6           #_arg5:Z
    .end local v7           #_arg6:Z
    .end local v8           #_result:Z
    :cond_6e
    move v6, v10

    #@6f
    .line 78
    goto :goto_58

    #@70
    .restart local v6       #_arg5:Z
    :cond_70
    move v7, v10

    #@71
    .line 80
    goto :goto_5f

    #@72
    .line 88
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:I
    .end local v6           #_arg5:Z
    :sswitch_72
    const-string v0, "com.android.internal.telephony.IWapPushManager"

    #@74
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@77
    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    .line 92
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7e
    move-result-object v2

    #@7f
    .line 94
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@82
    move-result-object v3

    #@83
    .line 96
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@86
    move-result-object v4

    #@87
    .line 98
    .restart local v4       #_arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8a
    move-result v5

    #@8b
    .line 100
    .restart local v5       #_arg4:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8e
    move-result v0

    #@8f
    if-eqz v0, :cond_a9

    #@91
    move v6, v9

    #@92
    .line 102
    .restart local v6       #_arg5:Z
    :goto_92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@95
    move-result v0

    #@96
    if-eqz v0, :cond_ab

    #@98
    move v7, v9

    #@99
    .restart local v7       #_arg6:Z
    :goto_99
    move-object v0, p0

    #@9a
    .line 103
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IWapPushManager$Stub;->updatePackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Z

    #@9d
    move-result v8

    #@9e
    .line 104
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a1
    .line 105
    if-eqz v8, :cond_a4

    #@a3
    move v10, v9

    #@a4
    :cond_a4
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@a7
    goto/16 :goto_9

    #@a9
    .end local v6           #_arg5:Z
    .end local v7           #_arg6:Z
    .end local v8           #_result:Z
    :cond_a9
    move v6, v10

    #@aa
    .line 100
    goto :goto_92

    #@ab
    .restart local v6       #_arg5:Z
    :cond_ab
    move v7, v10

    #@ac
    .line 102
    goto :goto_99

    #@ad
    .line 110
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:I
    .end local v6           #_arg5:Z
    :sswitch_ad
    const-string v0, "com.android.internal.telephony.IWapPushManager"

    #@af
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b2
    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b5
    move-result-object v1

    #@b6
    .line 114
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b9
    move-result-object v2

    #@ba
    .line 116
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@bd
    move-result-object v3

    #@be
    .line 118
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c1
    move-result-object v4

    #@c2
    .line 119
    .restart local v4       #_arg3:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/telephony/IWapPushManager$Stub;->deletePackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@c5
    move-result v8

    #@c6
    .line 120
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c9
    .line 121
    if-eqz v8, :cond_cc

    #@cb
    move v10, v9

    #@cc
    :cond_cc
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@cf
    goto/16 :goto_9

    #@d1
    .line 38
    nop

    #@d2
    :sswitch_data_d2
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_38
        0x3 -> :sswitch_72
        0x4 -> :sswitch_ad
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
