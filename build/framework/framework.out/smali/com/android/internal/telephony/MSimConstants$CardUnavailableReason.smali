.class public final enum Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;
.super Ljava/lang/Enum;
.source "MSimConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/MSimConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CardUnavailableReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

.field public static final enum REASON_CARD_REMOVED:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

.field public static final enum REASON_RADIO_UNAVAILABLE:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

.field public static final enum REASON_SIM_REFRESH_RESET:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 54
    new-instance v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@5
    const-string v1, "REASON_CARD_REMOVED"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->REASON_CARD_REMOVED:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@c
    .line 55
    new-instance v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@e
    const-string v1, "REASON_RADIO_UNAVAILABLE"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->REASON_RADIO_UNAVAILABLE:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@15
    .line 56
    new-instance v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@17
    const-string v1, "REASON_SIM_REFRESH_RESET"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->REASON_SIM_REFRESH_RESET:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@1e
    .line 53
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@21
    sget-object v1, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->REASON_CARD_REMOVED:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->REASON_RADIO_UNAVAILABLE:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->REASON_SIM_REFRESH_RESET:Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->$VALUES:[Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 53
    const-class v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;
    .registers 1

    #@0
    .prologue
    .line 53
    sget-object v0, Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->$VALUES:[Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/MSimConstants$CardUnavailableReason;

    #@8
    return-object v0
.end method
