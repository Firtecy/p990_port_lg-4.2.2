.class public Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
.super Ljava/lang/Object;
.source "LgeSimInfo.java"

# interfaces
.implements Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfilingConstants;


# static fields
.field private static final DBG:Z = true

.field private static final EDBG:Z = true

.field private static final TAG:Ljava/lang/String; = "LgeSimInfo"

.field private static final VDBG:Z = true

.field private static final VDF_MCC_MNC_TABLE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mGid:Ljava/lang/String;

.field private mImsi:Ljava/lang/String;

.field private mMcc:Ljava/lang/String;

.field private mMnc:Ljava/lang/String;

.field private mSpn:Ljava/lang/String;

.field private mSubId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 61
    new-instance v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->VDF_MCC_MNC_TABLE:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .parameter "mcc"
    .parameter "mnc"
    .parameter "gid"
    .parameter "spn"
    .parameter "imsi"
    .parameter "subId"

    #@0
    .prologue
    .line 86
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 87
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMcc:Ljava/lang/String;

    #@5
    .line 88
    iput-object p2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMnc:Ljava/lang/String;

    #@7
    .line 89
    iput-object p3, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mGid:Ljava/lang/String;

    #@9
    .line 90
    iput-object p4, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mSpn:Ljava/lang/String;

    #@b
    .line 91
    iput-object p5, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mImsi:Ljava/lang/String;

    #@d
    .line 92
    iput p6, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mSubId:I

    #@f
    .line 93
    return-void
.end method

.method public static createFromPreference(Landroid/content/Context;I)Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .registers 10
    .parameter "context"
    .parameter "subId"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string/jumbo v1, "simInfo_"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    const/4 v1, 0x0

    #@16
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@19
    move-result-object v7

    #@1a
    .line 231
    .local v7, preference:Landroid/content/SharedPreferences;
    new-instance v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@1c
    const-string/jumbo v1, "mcc"

    #@1f
    invoke-interface {v7, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    const-string/jumbo v2, "mnc"

    #@26
    invoke-interface {v7, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    const-string v3, "gid"

    #@2c
    invoke-interface {v7, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    const-string/jumbo v4, "spn"

    #@33
    invoke-interface {v7, v4, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    const-string v5, "imsi"

    #@39
    invoke-interface {v7, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    move v6, p1

    #@3e
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@41
    return-object v0
.end method

.method public static getDefaultSubScription()I
    .registers 1

    #@0
    .prologue
    .line 298
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .registers 1

    #@0
    .prologue
    .line 96
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo(I)Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getSimInfo(I)Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .registers 13
    .parameter "subId"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v11, 0x0

    #@2
    .line 101
    const/4 v1, 0x0

    #@3
    .line 102
    .local v1, mcc:Ljava/lang/String;
    const/4 v2, 0x0

    #@4
    .line 103
    .local v2, mnc:Ljava/lang/String;
    const/4 v4, 0x0

    #@5
    .line 104
    .local v4, spn:Ljava/lang/String;
    const/4 v3, 0x0

    #@6
    .line 105
    .local v3, gid:Ljava/lang/String;
    const-string v5, ""

    #@8
    .line 106
    .local v5, imsi:Ljava/lang/String;
    const/4 v8, 0x0

    #@9
    .line 110
    .local v8, numeric:Ljava/lang/String;
    sget-boolean v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->MULTI_SIM_ENABLED:Z

    #@b
    if-eqz v0, :cond_126

    #@d
    .line 111
    const-string v0, "gsm.sim.operator.numeric"

    #@f
    const-string v6, ""

    #@11
    invoke-static {v0, p0, v6}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v8

    #@15
    .line 112
    const-string v0, "gsm.sim.operator.gid"

    #@17
    const-string v6, ""

    #@19
    invoke-static {v0, p0, v6}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    .line 113
    const-string v0, "gsm.sim.operator.alpha"

    #@1f
    const-string v6, ""

    #@21
    invoke-static {v0, p0, v6}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    .line 122
    :goto_25
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_13a

    #@2b
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@2e
    move-result v0

    #@2f
    const/4 v6, 0x5

    #@30
    if-lt v0, v6, :cond_13a

    #@32
    .line 123
    invoke-virtual {v8, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    .line 124
    invoke-virtual {v8, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    .line 137
    :goto_3a
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3d
    move-result-object v0

    #@3e
    if-eqz v0, :cond_83

    #@40
    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getMSIN()Ljava/lang/String;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    .line 141
    sget-boolean v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->ENABLE_PRIVACY_LOG:Z

    #@5b
    if-eqz v0, :cond_16b

    #@5d
    .line 142
    const-string v0, "LgeSimInfo"

    #@5f
    new-instance v6, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v10, "TelephonyManager : "

    #@66
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v6

    #@6a
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6d
    move-result-object v10

    #@6e
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v6

    #@72
    const-string v10, "getIMSI : "

    #@74
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v6

    #@80
    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 151
    :cond_83
    :goto_83
    const-string v0, "EU"

    #@85
    const-string v6, "VDF"

    #@87
    invoke-static {v0, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@8a
    move-result v0

    #@8b
    if-eqz v0, :cond_f3

    #@8d
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@90
    move-result v0

    #@91
    if-nez v0, :cond_f3

    #@93
    const-string/jumbo v0, "ro.build.regional"

    #@96
    invoke-static {v0, v11}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@99
    move-result v0

    #@9a
    if-eqz v0, :cond_f3

    #@9c
    .line 156
    const-string v0, "LgeSimInfo"

    #@9e
    const-string v6, "[getSimInfo] VDF Regional Feature"

    #@a0
    invoke-static {v0, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 158
    const-string/jumbo v0, "persist.radio.mcc-list"

    #@a6
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a9
    move-result-object v7

    #@aa
    .line 160
    .local v7, mccList:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    #@ad
    move-result v0

    #@ae
    if-nez v0, :cond_f3

    #@b0
    .line 161
    const-string v0, ","

    #@b2
    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@b5
    move-result v0

    #@b6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@b9
    move-result-object v9

    #@ba
    .line 163
    .local v9, regionalVersion:Ljava/lang/Boolean;
    const-string v0, "LgeSimInfo"

    #@bc
    new-instance v6, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v10, "[getSimInfo] mccList exists, mccList : "

    #@c3
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v6

    #@c7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v6

    #@cb
    const-string v10, ", regionalVersion : "

    #@cd
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v6

    #@d1
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v6

    #@d5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v6

    #@d9
    invoke-static {v0, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 165
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    #@df
    move-result v0

    #@e0
    if-eqz v0, :cond_189

    #@e2
    .line 166
    invoke-virtual {v7, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@e5
    move-result v0

    #@e6
    if-nez v0, :cond_f3

    #@e8
    .line 167
    const-string v0, "LgeSimInfo"

    #@ea
    const-string v6, "[getSimInfo] MCC from SIM doesn\'t exist in the list"

    #@ec
    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 169
    const-string v1, ""

    #@f1
    .line 170
    const-string v2, ""

    #@f3
    .line 185
    .end local v7           #mccList:Ljava/lang/String;
    .end local v9           #regionalVersion:Ljava/lang/Boolean;
    :cond_f3
    :goto_f3
    const-string v0, "LgeSimInfo"

    #@f5
    new-instance v6, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v10, "[getSimInfo] *** SIM Info, MCC : "

    #@fc
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v6

    #@100
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v6

    #@104
    const-string v10, ", MNC : "

    #@106
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v6

    #@10a
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v6

    #@10e
    const-string v10, ", subId : "

    #@110
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v6

    #@114
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@117
    move-result-object v6

    #@118
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v6

    #@11c
    invoke-static {v0, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11f
    .line 188
    new-instance v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@121
    move v6, p0

    #@122
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@125
    return-object v0

    #@126
    .line 116
    :cond_126
    const-string v0, "gsm.sim.operator.numeric"

    #@128
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@12b
    move-result-object v8

    #@12c
    .line 117
    const-string v0, "gsm.sim.operator.gid"

    #@12e
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@131
    move-result-object v3

    #@132
    .line 118
    const-string v0, "gsm.sim.operator.alpha"

    #@134
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@137
    move-result-object v4

    #@138
    goto/16 :goto_25

    #@13a
    .line 127
    :cond_13a
    sget-boolean v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->ENABLE_PRIVACY_LOG:Z

    #@13c
    if-eqz v0, :cond_162

    #@13e
    .line 128
    const-string v0, "LgeSimInfo"

    #@140
    new-instance v6, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v10, "[getSimInfo] numeric is invalid, numeric : "

    #@147
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v6

    #@14b
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v6

    #@14f
    const-string v10, ", subId : "

    #@151
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v6

    #@155
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@158
    move-result-object v6

    #@159
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v6

    #@15d
    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    goto/16 :goto_3a

    #@162
    .line 131
    :cond_162
    const-string v0, "LgeSimInfo"

    #@164
    const-string v6, "[getSimInfo] numeric is invalid"

    #@166
    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@169
    goto/16 :goto_3a

    #@16b
    .line 144
    :cond_16b
    const-string v0, "LgeSimInfo"

    #@16d
    new-instance v6, Ljava/lang/StringBuilder;

    #@16f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@172
    const-string v10, "TelephonyManager : "

    #@174
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v6

    #@178
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@17b
    move-result-object v10

    #@17c
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v6

    #@180
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@183
    move-result-object v6

    #@184
    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@187
    goto/16 :goto_83

    #@189
    .line 173
    .restart local v7       #mccList:Ljava/lang/String;
    .restart local v9       #regionalVersion:Ljava/lang/Boolean;
    :cond_189
    move-object v1, v7

    #@18a
    .line 174
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->VDF_MCC_MNC_TABLE:Ljava/util/HashMap;

    #@18c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18f
    move-result-object v2

    #@190
    .end local v2           #mnc:Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    #@192
    .line 176
    .restart local v2       #mnc:Ljava/lang/String;
    if-nez v2, :cond_f3

    #@194
    .line 177
    const-string v0, "LgeSimInfo"

    #@196
    const-string v6, "[getSimInfo] error.. cannot find matched mnc"

    #@198
    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 179
    const-string v1, ""

    #@19d
    .line 180
    const-string v2, ""

    #@19f
    goto/16 :goto_f3
.end method

.method public static writeToPreference(Landroid/content/Context;Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;I)V
    .registers 6
    .parameter "context"
    .parameter "simInfo"
    .parameter "subId"

    #@0
    .prologue
    .line 239
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v2, "simInfo_"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    const/4 v2, 0x1

    #@15
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@18
    move-result-object v1

    #@19
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@1c
    move-result-object v0

    #@1d
    .line 241
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    #@20
    .line 243
    const-string/jumbo v1, "mcc"

    #@23
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@2a
    .line 244
    const-string/jumbo v1, "mnc"

    #@2d
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@34
    .line 245
    const-string v1, "gid"

    #@36
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getGid()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@3d
    .line 246
    const-string/jumbo v1, "spn"

    #@40
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSpn()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@47
    .line 247
    const-string v1, "imsi"

    #@49
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getImsi()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@50
    .line 249
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@53
    .line 250
    return-void
.end method


# virtual methods
.method public equals(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;)Z
    .registers 6
    .parameter "simInfo"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 199
    if-ne p0, p1, :cond_c

    #@4
    .line 200
    const-string v1, "LgeSimInfo"

    #@6
    const-string v2, "[equals] return true"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 221
    :goto_b
    return v0

    #@c
    .line 204
    :cond_c
    if-nez p1, :cond_17

    #@e
    .line 205
    const-string v0, "LgeSimInfo"

    #@10
    const-string v2, "[equals] return false"

    #@12
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    move v0, v1

    #@16
    .line 206
    goto :goto_b

    #@17
    .line 209
    :cond_17
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMcc:Ljava/lang/String;

    #@19
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c
    move-result v2

    #@1d
    if-nez v2, :cond_73

    #@1f
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMnc:Ljava/lang/String;

    #@21
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@24
    move-result v2

    #@25
    if-nez v2, :cond_73

    #@27
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mImsi:Ljava/lang/String;

    #@29
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_73

    #@2f
    .line 210
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMcc:Ljava/lang/String;

    #@31
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_73

    #@3b
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMnc:Ljava/lang/String;

    #@3d
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@44
    move-result v2

    #@45
    if-eqz v2, :cond_73

    #@47
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mGid:Ljava/lang/String;

    #@49
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getGid()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@50
    move-result v2

    #@51
    if-eqz v2, :cond_73

    #@53
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mSpn:Ljava/lang/String;

    #@55
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSpn()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5c
    move-result v2

    #@5d
    if-eqz v2, :cond_73

    #@5f
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mImsi:Ljava/lang/String;

    #@61
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getImsi()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@68
    move-result v2

    #@69
    if-eqz v2, :cond_73

    #@6b
    .line 215
    const-string v1, "LgeSimInfo"

    #@6d
    const-string v2, "[equals] return true"

    #@6f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_b

    #@73
    .line 220
    :cond_73
    const-string v0, "LgeSimInfo"

    #@75
    const-string v2, "[equals] return false"

    #@77
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    move v0, v1

    #@7b
    .line 221
    goto :goto_b
.end method

.method public getGid()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 273
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mGid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getImsi()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mImsi:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMcc()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMcc:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMnc()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 265
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMnc:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSpn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 281
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mSpn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubId()I
    .registers 2

    #@0
    .prologue
    .line 293
    iget v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mSubId:I

    #@2
    return v0
.end method

.method public isNull()Z
    .registers 2

    #@0
    .prologue
    .line 225
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMcc:Ljava/lang/String;

    #@2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_12

    #@8
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMnc:Ljava/lang/String;

    #@a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public setGid(Ljava/lang/String;)V
    .registers 2
    .parameter "gid"

    #@0
    .prologue
    .line 269
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mGid:Ljava/lang/String;

    #@2
    .line 270
    return-void
.end method

.method public setImsi(Ljava/lang/String;)V
    .registers 2
    .parameter "imsi"

    #@0
    .prologue
    .line 285
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mImsi:Ljava/lang/String;

    #@2
    .line 286
    return-void
.end method

.method public setMcc(Ljava/lang/String;)V
    .registers 2
    .parameter "mcc"

    #@0
    .prologue
    .line 253
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMcc:Ljava/lang/String;

    #@2
    .line 254
    return-void
.end method

.method public setMnc(Ljava/lang/String;)V
    .registers 2
    .parameter "mnc"

    #@0
    .prologue
    .line 261
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMnc:Ljava/lang/String;

    #@2
    .line 262
    return-void
.end method

.method public setSpn(Ljava/lang/String;)V
    .registers 2
    .parameter "spn"

    #@0
    .prologue
    .line 277
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mSpn:Ljava/lang/String;

    #@2
    .line 278
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SimInfo - MCC : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMcc:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", MNC : "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mMnc:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", GID : "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mGid:Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", SPN : "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mSpn:Ljava/lang/String;

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ", IMSI : "

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->mImsi:Ljava/lang/String;

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    return-object v0
.end method
