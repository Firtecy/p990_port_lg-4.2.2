.class Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;
.super Ljava/lang/Object;
.source "GsmAlphabet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/GsmAlphabet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LanguagePairCount"
.end annotation


# instance fields
.field final languageCode:I

.field final septetCounts:[I

.field final unencodableCounts:[I


# direct methods
.method constructor <init>(I)V
    .registers 9
    .parameter "code"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, -0x1

    #@3
    .line 1530
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 1531
    iput p1, p0, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->languageCode:I

    #@8
    .line 1532
    invoke-static {}, Lcom/android/internal/telephony/GsmAlphabet;->access$000()I

    #@b
    move-result v1

    #@c
    .line 1533
    .local v1, maxSingleShiftCode:I
    add-int/lit8 v3, v1, 0x1

    #@e
    new-array v3, v3, [I

    #@10
    iput-object v3, p0, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@12
    .line 1534
    add-int/lit8 v3, v1, 0x1

    #@14
    new-array v3, v3, [I

    #@16
    iput-object v3, p0, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->unencodableCounts:[I

    #@18
    .line 1537
    const/4 v0, 0x1

    #@19
    .local v0, i:I
    const/4 v2, 0x0

    #@1a
    .local v2, tableOffset:I
    :goto_1a
    if-gt v0, v1, :cond_30

    #@1c
    .line 1538
    invoke-static {}, Lcom/android/internal/telephony/GsmAlphabet;->access$100()[I

    #@1f
    move-result-object v3

    #@20
    aget v3, v3, v2

    #@22
    if-ne v3, v0, :cond_29

    #@24
    .line 1539
    add-int/lit8 v2, v2, 0x1

    #@26
    .line 1537
    :goto_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_1a

    #@29
    .line 1541
    :cond_29
    iget-object v3, p0, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@2b
    aput v4, v3, v0

    #@2d
    .line 1543
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_26

    #@30
    .line 1550
    :cond_30
    if-ne p1, v5, :cond_39

    #@32
    if-lt v1, v5, :cond_39

    #@34
    .line 1551
    iget-object v3, p0, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@36
    aput v4, v3, v5

    #@38
    .line 1555
    :cond_38
    :goto_38
    return-void

    #@39
    .line 1552
    :cond_39
    const/4 v3, 0x3

    #@3a
    if-ne p1, v3, :cond_38

    #@3c
    if-lt v1, v6, :cond_38

    #@3e
    .line 1553
    iget-object v3, p0, Lcom/android/internal/telephony/GsmAlphabet$LanguagePairCount;->septetCounts:[I

    #@40
    aput v4, v3, v6

    #@42
    goto :goto_38
.end method
