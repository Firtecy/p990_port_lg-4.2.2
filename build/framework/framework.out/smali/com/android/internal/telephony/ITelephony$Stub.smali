.class public abstract Lcom/android/internal/telephony/ITelephony$Stub;
.super Landroid/os/Binder;
.source "ITelephony.java"

# interfaces
.implements Lcom/android/internal/telephony/ITelephony;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/ITelephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/ITelephony$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.ITelephony"

.field static final TRANSACTION_answerRingingCall:I = 0x9

.field static final TRANSACTION_busyCall:I = 0x44

.field static final TRANSACTION_calculateAkaResponse:I = 0x5b

.field static final TRANSACTION_calculateGbaBootstrappingResponse:I = 0x5c

.field static final TRANSACTION_calculateNafExternalKey:I = 0x5d

.field static final TRANSACTION_call:I = 0x2

.field static final TRANSACTION_cancelMissedCallsNotification:I = 0x10

.field static final TRANSACTION_checkDataProfileEx:I = 0x57

.field static final TRANSACTION_dial:I = 0x1

.field static final TRANSACTION_disableApnType:I = 0x1d

.field static final TRANSACTION_disableDataConnectivity:I = 0x1f

.field static final TRANSACTION_disableLocationUpdates:I = 0x1b

.field static final TRANSACTION_disableStatusBarforVoIP:I = 0x43

.field static final TRANSACTION_enableApnType:I = 0x1c

.field static final TRANSACTION_enableDataConnectivity:I = 0x1e

.field static final TRANSACTION_enableLocationUpdates:I = 0x1a

.field static final TRANSACTION_endAllCall:I = 0x8

.field static final TRANSACTION_endCall:I = 0x5

.field static final TRANSACTION_getAPNList:I = 0x58

.field static final TRANSACTION_getActivePhoneType:I = 0x26

.field static final TRANSACTION_getAllCellInfo:I = 0x33

.field static final TRANSACTION_getCallState:I = 0x23

.field static final TRANSACTION_getCdmaEriHomeSystems:I = 0x39

.field static final TRANSACTION_getCdmaEriIconIndex:I = 0x27

.field static final TRANSACTION_getCdmaEriIconMode:I = 0x28

.field static final TRANSACTION_getCdmaEriText:I = 0x29

.field static final TRANSACTION_getCellLocation:I = 0x21

.field static final TRANSACTION_getCurrentLine:I = 0x36

.field static final TRANSACTION_getDataActivity:I = 0x24

.field static final TRANSACTION_getDataState:I = 0x25

.field static final TRANSACTION_getDebugInfo:I = 0x56

.field static final TRANSACTION_getIccFdnEnabled:I = 0x5f

.field static final TRANSACTION_getIccPin1RetryCount:I = 0x14

.field static final TRANSACTION_getIccPin2RetryCount:I = 0x52

.field static final TRANSACTION_getIccPuk1RetryCount:I = 0x53

.field static final TRANSACTION_getIccPuk2RetryCount:I = 0x54

.field static final TRANSACTION_getLteOnCdmaMode:I = 0x2e

.field static final TRANSACTION_getMipErrorCode:I = 0x55

.field static final TRANSACTION_getMobileQualityInformation:I = 0x51

.field static final TRANSACTION_getNeighboringCellInfo:I = 0x22

.field static final TRANSACTION_getNetworkType:I = 0x2c

.field static final TRANSACTION_getRoamingCountryUpdate:I = 0x30

.field static final TRANSACTION_getVoiceMessageCount:I = 0x2b

.field static final TRANSACTION_getVzwGpsFieldTestScreenState:I = 0x61

.field static final TRANSACTION_handlePinMmi:I = 0x16

.field static final TRANSACTION_hasIccCard:I = 0x2d

.field static final TRANSACTION_isBluetoothAudioOn:I = 0x7

.field static final TRANSACTION_isCSCallIdle:I = 0x47

.field static final TRANSACTION_isDataConnectivityPossible:I = 0x20

.field static final TRANSACTION_isDialingOrRinging:I = 0x6

.field static final TRANSACTION_isHeadsetPlugged:I = 0x5a

.field static final TRANSACTION_isIdle:I = 0xd

.field static final TRANSACTION_isOemDialing:I = 0x46

.field static final TRANSACTION_isOemRinging:I = 0x45

.field static final TRANSACTION_isOffhook:I = 0xb

.field static final TRANSACTION_isRadioOn:I = 0xe

.field static final TRANSACTION_isReservedCall:I = 0x34

.field static final TRANSACTION_isRinging:I = 0xc

.field static final TRANSACTION_isSimPinEnabled:I = 0xf

.field static final TRANSACTION_isTwoLineSupported:I = 0x37

.field static final TRANSACTION_isVideoCall:I = 0x3a

.field static final TRANSACTION_isVoiceInService:I = 0x38

.field static final TRANSACTION_needsOtaServiceProvisioning:I = 0x2a

.field static final TRANSACTION_reenableStatusBarforVoIP:I = 0x42

.field static final TRANSACTION_registerForCurrentVoIP:I = 0x3c

.field static final TRANSACTION_releaseRoamingCountryUpdate:I = 0x31

.field static final TRANSACTION_resetVoiceMessageCount:I = 0x48

.field static final TRANSACTION_setGbaBootstrappingParams:I = 0x5e

.field static final TRANSACTION_setNetworkModePreference:I = 0x59

.field static final TRANSACTION_setRadio:I = 0x18

.field static final TRANSACTION_setRoamingCountryUpdate:I = 0x2f

.field static final TRANSACTION_setVoIPDialing:I = 0x3f

.field static final TRANSACTION_setVoIPIdle:I = 0x41

.field static final TRANSACTION_setVoIPInCall:I = 0x40

.field static final TRANSACTION_setVoIPRining:I = 0x3e

.field static final TRANSACTION_setVzwGpsFieldTestScreenState:I = 0x60

.field static final TRANSACTION_showCallScreen:I = 0x3

.field static final TRANSACTION_showCallScreenWithDialpad:I = 0x4

.field static final TRANSACTION_silenceRinger:I = 0xa

.field static final TRANSACTION_startMobileQualityInformation:I = 0x4f

.field static final TRANSACTION_startRoamingCountryUpdate:I = 0x32

.field static final TRANSACTION_stopMobileQualityInformation:I = 0x50

.field static final TRANSACTION_supplyPin:I = 0x11

.field static final TRANSACTION_supplyPinReportResult:I = 0x12

.field static final TRANSACTION_supplyPuk:I = 0x15

.field static final TRANSACTION_supplyPukReportResult:I = 0x13

.field static final TRANSACTION_toggleCurrentLine:I = 0x35

.field static final TRANSACTION_toggleRadioOnOff:I = 0x17

.field static final TRANSACTION_uknightEventSet:I = 0x4a

.field static final TRANSACTION_uknightGetData:I = 0x4d

.field static final TRANSACTION_uknightLogSet:I = 0x49

.field static final TRANSACTION_uknightMemCheck:I = 0x4e

.field static final TRANSACTION_uknightMemSet:I = 0x4c

.field static final TRANSACTION_uknightStateChangeSet:I = 0x4b

.field static final TRANSACTION_unregisterForCurrentVoIP:I = 0x3d

.field static final TRANSACTION_updateServiceLocation:I = 0x19

.field static final TRANSACTION_videoDial:I = 0x3b


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 22
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 23
    const-string v0, "com.android.internal.telephony.ITelephony"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 24
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 31
    if-nez p0, :cond_4

    #@2
    .line 32
    const/4 v0, 0x0

    #@3
    .line 38
    :goto_3
    return-object v0

    #@4
    .line 34
    :cond_4
    const-string v1, "com.android.internal.telephony.ITelephony"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 35
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/ITelephony;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 36
    check-cast v0, Lcom/android/internal/telephony/ITelephony;

    #@12
    goto :goto_3

    #@13
    .line 38
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/ITelephony$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/ITelephony$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 42
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 46
    sparse-switch p1, :sswitch_data_73e

    #@5
    .line 902
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v7

    #@9
    :goto_9
    return v7

    #@a
    .line 50
    :sswitch_a
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 55
    :sswitch_10
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@12
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 58
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->dial(Ljava/lang/String;)V

    #@1c
    .line 59
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f
    goto :goto_9

    #@20
    .line 64
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_20
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@22
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 67
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->call(Ljava/lang/String;)V

    #@2c
    .line 68
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f
    goto :goto_9

    #@30
    .line 73
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_30
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@32
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 74
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->showCallScreen()Z

    #@38
    move-result v3

    #@39
    .line 75
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 76
    if-eqz v3, :cond_3f

    #@3e
    move v6, v7

    #@3f
    :cond_3f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    goto :goto_9

    #@43
    .line 81
    .end local v3           #_result:Z
    :sswitch_43
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@45
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48
    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4b
    move-result v8

    #@4c
    if-eqz v8, :cond_5d

    #@4e
    move v0, v7

    #@4f
    .line 84
    .local v0, _arg0:Z
    :goto_4f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->showCallScreenWithDialpad(Z)Z

    #@52
    move-result v3

    #@53
    .line 85
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@56
    .line 86
    if-eqz v3, :cond_59

    #@58
    move v6, v7

    #@59
    :cond_59
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    goto :goto_9

    #@5d
    .end local v0           #_arg0:Z
    .end local v3           #_result:Z
    :cond_5d
    move v0, v6

    #@5e
    .line 83
    goto :goto_4f

    #@5f
    .line 91
    :sswitch_5f
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@61
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 92
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->endCall()Z

    #@67
    move-result v3

    #@68
    .line 93
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6b
    .line 94
    if-eqz v3, :cond_6e

    #@6d
    move v6, v7

    #@6e
    :cond_6e
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@71
    goto :goto_9

    #@72
    .line 99
    .end local v3           #_result:Z
    :sswitch_72
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@74
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@77
    .line 100
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isDialingOrRinging()Z

    #@7a
    move-result v3

    #@7b
    .line 101
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e
    .line 102
    if-eqz v3, :cond_81

    #@80
    move v6, v7

    #@81
    :cond_81
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@84
    goto :goto_9

    #@85
    .line 107
    .end local v3           #_result:Z
    :sswitch_85
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@87
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8a
    .line 108
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isBluetoothAudioOn()Z

    #@8d
    move-result v3

    #@8e
    .line 109
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@91
    .line 110
    if-eqz v3, :cond_94

    #@93
    move v6, v7

    #@94
    :cond_94
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@97
    goto/16 :goto_9

    #@99
    .line 115
    .end local v3           #_result:Z
    :sswitch_99
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@9b
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9e
    .line 116
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->endAllCall()Z

    #@a1
    move-result v3

    #@a2
    .line 117
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a5
    .line 118
    if-eqz v3, :cond_a8

    #@a7
    move v6, v7

    #@a8
    :cond_a8
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@ab
    goto/16 :goto_9

    #@ad
    .line 123
    .end local v3           #_result:Z
    :sswitch_ad
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@af
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b2
    .line 124
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->answerRingingCall()V

    #@b5
    .line 125
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b8
    goto/16 :goto_9

    #@ba
    .line 130
    :sswitch_ba
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@bc
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 131
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->silenceRinger()V

    #@c2
    .line 132
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c5
    goto/16 :goto_9

    #@c7
    .line 137
    :sswitch_c7
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@c9
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cc
    .line 138
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isOffhook()Z

    #@cf
    move-result v3

    #@d0
    .line 139
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d3
    .line 140
    if-eqz v3, :cond_d6

    #@d5
    move v6, v7

    #@d6
    :cond_d6
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@d9
    goto/16 :goto_9

    #@db
    .line 145
    .end local v3           #_result:Z
    :sswitch_db
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@dd
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e0
    .line 146
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isRinging()Z

    #@e3
    move-result v3

    #@e4
    .line 147
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e7
    .line 148
    if-eqz v3, :cond_ea

    #@e9
    move v6, v7

    #@ea
    :cond_ea
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@ed
    goto/16 :goto_9

    #@ef
    .line 153
    .end local v3           #_result:Z
    :sswitch_ef
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@f1
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f4
    .line 154
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isIdle()Z

    #@f7
    move-result v3

    #@f8
    .line 155
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fb
    .line 156
    if-eqz v3, :cond_fe

    #@fd
    move v6, v7

    #@fe
    :cond_fe
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@101
    goto/16 :goto_9

    #@103
    .line 161
    .end local v3           #_result:Z
    :sswitch_103
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@105
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@108
    .line 162
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isRadioOn()Z

    #@10b
    move-result v3

    #@10c
    .line 163
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10f
    .line 164
    if-eqz v3, :cond_112

    #@111
    move v6, v7

    #@112
    :cond_112
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@115
    goto/16 :goto_9

    #@117
    .line 169
    .end local v3           #_result:Z
    :sswitch_117
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@119
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11c
    .line 170
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isSimPinEnabled()Z

    #@11f
    move-result v3

    #@120
    .line 171
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@123
    .line 172
    if-eqz v3, :cond_126

    #@125
    move v6, v7

    #@126
    :cond_126
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@129
    goto/16 :goto_9

    #@12b
    .line 177
    .end local v3           #_result:Z
    :sswitch_12b
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@12d
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@130
    .line 178
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->cancelMissedCallsNotification()V

    #@133
    .line 179
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@136
    goto/16 :goto_9

    #@138
    .line 184
    :sswitch_138
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@13a
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13d
    .line 186
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@140
    move-result-object v0

    #@141
    .line 187
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->supplyPin(Ljava/lang/String;)Z

    #@144
    move-result v3

    #@145
    .line 188
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@148
    .line 189
    if-eqz v3, :cond_14b

    #@14a
    move v6, v7

    #@14b
    :cond_14b
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@14e
    goto/16 :goto_9

    #@150
    .line 194
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_150
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@152
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@155
    .line 196
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@158
    move-result-object v0

    #@159
    .line 197
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->supplyPinReportResult(Ljava/lang/String;)I

    #@15c
    move-result v3

    #@15d
    .line 198
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@160
    .line 199
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@163
    goto/16 :goto_9

    #@165
    .line 204
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_165
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@167
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16a
    .line 206
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@16d
    move-result-object v0

    #@16e
    .line 208
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@171
    move-result-object v1

    #@172
    .line 209
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/ITelephony$Stub;->supplyPukReportResult(Ljava/lang/String;Ljava/lang/String;)I

    #@175
    move-result v3

    #@176
    .line 210
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@179
    .line 211
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@17c
    goto/16 :goto_9

    #@17e
    .line 216
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_17e
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@180
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@183
    .line 217
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getIccPin1RetryCount()I

    #@186
    move-result v3

    #@187
    .line 218
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@18a
    .line 219
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@18d
    goto/16 :goto_9

    #@18f
    .line 224
    .end local v3           #_result:I
    :sswitch_18f
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@191
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@194
    .line 226
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@197
    move-result-object v0

    #@198
    .line 228
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19b
    move-result-object v1

    #@19c
    .line 229
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/ITelephony$Stub;->supplyPuk(Ljava/lang/String;Ljava/lang/String;)Z

    #@19f
    move-result v3

    #@1a0
    .line 230
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a3
    .line 231
    if-eqz v3, :cond_1a6

    #@1a5
    move v6, v7

    #@1a6
    :cond_1a6
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1a9
    goto/16 :goto_9

    #@1ab
    .line 236
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_1ab
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@1ad
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b0
    .line 238
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b3
    move-result-object v0

    #@1b4
    .line 239
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->handlePinMmi(Ljava/lang/String;)Z

    #@1b7
    move-result v3

    #@1b8
    .line 240
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1bb
    .line 241
    if-eqz v3, :cond_1be

    #@1bd
    move v6, v7

    #@1be
    :cond_1be
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1c1
    goto/16 :goto_9

    #@1c3
    .line 246
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_1c3
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@1c5
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c8
    .line 247
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->toggleRadioOnOff()V

    #@1cb
    .line 248
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ce
    goto/16 :goto_9

    #@1d0
    .line 253
    :sswitch_1d0
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@1d2
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d5
    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d8
    move-result v8

    #@1d9
    if-eqz v8, :cond_1eb

    #@1db
    move v0, v7

    #@1dc
    .line 256
    .local v0, _arg0:Z
    :goto_1dc
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->setRadio(Z)Z

    #@1df
    move-result v3

    #@1e0
    .line 257
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e3
    .line 258
    if-eqz v3, :cond_1e6

    #@1e5
    move v6, v7

    #@1e6
    :cond_1e6
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1e9
    goto/16 :goto_9

    #@1eb
    .end local v0           #_arg0:Z
    .end local v3           #_result:Z
    :cond_1eb
    move v0, v6

    #@1ec
    .line 255
    goto :goto_1dc

    #@1ed
    .line 263
    :sswitch_1ed
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@1ef
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f2
    .line 264
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->updateServiceLocation()V

    #@1f5
    .line 265
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f8
    goto/16 :goto_9

    #@1fa
    .line 270
    :sswitch_1fa
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@1fc
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ff
    .line 271
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->enableLocationUpdates()V

    #@202
    .line 272
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@205
    goto/16 :goto_9

    #@207
    .line 277
    :sswitch_207
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@209
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20c
    .line 278
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->disableLocationUpdates()V

    #@20f
    .line 279
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@212
    goto/16 :goto_9

    #@214
    .line 284
    :sswitch_214
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@216
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@219
    .line 286
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@21c
    move-result-object v0

    #@21d
    .line 287
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->enableApnType(Ljava/lang/String;)I

    #@220
    move-result v3

    #@221
    .line 288
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@224
    .line 289
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@227
    goto/16 :goto_9

    #@229
    .line 294
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_229
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@22b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22e
    .line 296
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@231
    move-result-object v0

    #@232
    .line 297
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->disableApnType(Ljava/lang/String;)I

    #@235
    move-result v3

    #@236
    .line 298
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@239
    .line 299
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@23c
    goto/16 :goto_9

    #@23e
    .line 304
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_23e
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@240
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@243
    .line 305
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->enableDataConnectivity()Z

    #@246
    move-result v3

    #@247
    .line 306
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24a
    .line 307
    if-eqz v3, :cond_24d

    #@24c
    move v6, v7

    #@24d
    :cond_24d
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@250
    goto/16 :goto_9

    #@252
    .line 312
    .end local v3           #_result:Z
    :sswitch_252
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@254
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@257
    .line 313
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->disableDataConnectivity()Z

    #@25a
    move-result v3

    #@25b
    .line 314
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@25e
    .line 315
    if-eqz v3, :cond_261

    #@260
    move v6, v7

    #@261
    :cond_261
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@264
    goto/16 :goto_9

    #@266
    .line 320
    .end local v3           #_result:Z
    :sswitch_266
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@268
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@26b
    .line 321
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isDataConnectivityPossible()Z

    #@26e
    move-result v3

    #@26f
    .line 322
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@272
    .line 323
    if-eqz v3, :cond_275

    #@274
    move v6, v7

    #@275
    :cond_275
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@278
    goto/16 :goto_9

    #@27a
    .line 328
    .end local v3           #_result:Z
    :sswitch_27a
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@27c
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27f
    .line 329
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getCellLocation()Landroid/os/Bundle;

    #@282
    move-result-object v3

    #@283
    .line 330
    .local v3, _result:Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@286
    .line 331
    if-eqz v3, :cond_290

    #@288
    .line 332
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@28b
    .line 333
    invoke-virtual {v3, p3, v7}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@28e
    goto/16 :goto_9

    #@290
    .line 336
    :cond_290
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@293
    goto/16 :goto_9

    #@295
    .line 342
    .end local v3           #_result:Landroid/os/Bundle;
    :sswitch_295
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@297
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29a
    .line 343
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getNeighboringCellInfo()Ljava/util/List;

    #@29d
    move-result-object v5

    #@29e
    .line 344
    .local v5, _result:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/NeighboringCellInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a1
    .line 345
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@2a4
    goto/16 :goto_9

    #@2a6
    .line 350
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/NeighboringCellInfo;>;"
    :sswitch_2a6
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@2a8
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ab
    .line 351
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getCallState()I

    #@2ae
    move-result v3

    #@2af
    .line 352
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b2
    .line 353
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2b5
    goto/16 :goto_9

    #@2b7
    .line 358
    .end local v3           #_result:I
    :sswitch_2b7
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@2b9
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2bc
    .line 359
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getDataActivity()I

    #@2bf
    move-result v3

    #@2c0
    .line 360
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c3
    .line 361
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2c6
    goto/16 :goto_9

    #@2c8
    .line 366
    .end local v3           #_result:I
    :sswitch_2c8
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@2ca
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2cd
    .line 367
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getDataState()I

    #@2d0
    move-result v3

    #@2d1
    .line 368
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d4
    .line 369
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2d7
    goto/16 :goto_9

    #@2d9
    .line 374
    .end local v3           #_result:I
    :sswitch_2d9
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@2db
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2de
    .line 375
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getActivePhoneType()I

    #@2e1
    move-result v3

    #@2e2
    .line 376
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e5
    .line 377
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2e8
    goto/16 :goto_9

    #@2ea
    .line 382
    .end local v3           #_result:I
    :sswitch_2ea
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@2ec
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ef
    .line 383
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getCdmaEriIconIndex()I

    #@2f2
    move-result v3

    #@2f3
    .line 384
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f6
    .line 385
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2f9
    goto/16 :goto_9

    #@2fb
    .line 390
    .end local v3           #_result:I
    :sswitch_2fb
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@2fd
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@300
    .line 391
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getCdmaEriIconMode()I

    #@303
    move-result v3

    #@304
    .line 392
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@307
    .line 393
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@30a
    goto/16 :goto_9

    #@30c
    .line 398
    .end local v3           #_result:I
    :sswitch_30c
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@30e
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@311
    .line 399
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getCdmaEriText()Ljava/lang/String;

    #@314
    move-result-object v3

    #@315
    .line 400
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@318
    .line 401
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@31b
    goto/16 :goto_9

    #@31d
    .line 406
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_31d
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@31f
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@322
    .line 407
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->needsOtaServiceProvisioning()Z

    #@325
    move-result v3

    #@326
    .line 408
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@329
    .line 409
    if-eqz v3, :cond_32c

    #@32b
    move v6, v7

    #@32c
    :cond_32c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@32f
    goto/16 :goto_9

    #@331
    .line 414
    .end local v3           #_result:Z
    :sswitch_331
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@333
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@336
    .line 415
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getVoiceMessageCount()I

    #@339
    move-result v3

    #@33a
    .line 416
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@33d
    .line 417
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@340
    goto/16 :goto_9

    #@342
    .line 422
    .end local v3           #_result:I
    :sswitch_342
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@344
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@347
    .line 423
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getNetworkType()I

    #@34a
    move-result v3

    #@34b
    .line 424
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34e
    .line 425
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@351
    goto/16 :goto_9

    #@353
    .line 430
    .end local v3           #_result:I
    :sswitch_353
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@355
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@358
    .line 431
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->hasIccCard()Z

    #@35b
    move-result v3

    #@35c
    .line 432
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@35f
    .line 433
    if-eqz v3, :cond_362

    #@361
    move v6, v7

    #@362
    :cond_362
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@365
    goto/16 :goto_9

    #@367
    .line 438
    .end local v3           #_result:Z
    :sswitch_367
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@369
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36c
    .line 439
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getLteOnCdmaMode()I

    #@36f
    move-result v3

    #@370
    .line 440
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@373
    .line 441
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@376
    goto/16 :goto_9

    #@378
    .line 446
    .end local v3           #_result:I
    :sswitch_378
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@37a
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@37d
    .line 448
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@380
    move-result v8

    #@381
    if-eqz v8, :cond_38c

    #@383
    move v0, v7

    #@384
    .line 449
    .local v0, _arg0:Z
    :goto_384
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->setRoamingCountryUpdate(Z)V

    #@387
    .line 450
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@38a
    goto/16 :goto_9

    #@38c
    .end local v0           #_arg0:Z
    :cond_38c
    move v0, v6

    #@38d
    .line 448
    goto :goto_384

    #@38e
    .line 455
    :sswitch_38e
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@390
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@393
    .line 456
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getRoamingCountryUpdate()Z

    #@396
    move-result v3

    #@397
    .line 457
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@39a
    .line 458
    if-eqz v3, :cond_39d

    #@39c
    move v6, v7

    #@39d
    :cond_39d
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@3a0
    goto/16 :goto_9

    #@3a2
    .line 463
    .end local v3           #_result:Z
    :sswitch_3a2
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@3a4
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a7
    .line 464
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->releaseRoamingCountryUpdate()V

    #@3aa
    .line 465
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3ad
    goto/16 :goto_9

    #@3af
    .line 470
    :sswitch_3af
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@3b1
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b4
    .line 472
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3b7
    move-result-object v0

    #@3b8
    .line 473
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->startRoamingCountryUpdate(Ljava/lang/String;)V

    #@3bb
    .line 474
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3be
    goto/16 :goto_9

    #@3c0
    .line 479
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_3c0
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@3c2
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c5
    .line 480
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getAllCellInfo()Ljava/util/List;

    #@3c8
    move-result-object v4

    #@3c9
    .line 481
    .local v4, _result:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3cc
    .line 482
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@3cf
    goto/16 :goto_9

    #@3d1
    .line 487
    .end local v4           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    :sswitch_3d1
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@3d3
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d6
    .line 488
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isReservedCall()Z

    #@3d9
    move-result v3

    #@3da
    .line 489
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3dd
    .line 490
    if-eqz v3, :cond_3e0

    #@3df
    move v6, v7

    #@3e0
    :cond_3e0
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@3e3
    goto/16 :goto_9

    #@3e5
    .line 495
    .end local v3           #_result:Z
    :sswitch_3e5
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@3e7
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ea
    .line 496
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->toggleCurrentLine()I

    #@3ed
    move-result v3

    #@3ee
    .line 497
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f1
    .line 498
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3f4
    goto/16 :goto_9

    #@3f6
    .line 503
    .end local v3           #_result:I
    :sswitch_3f6
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@3f8
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3fb
    .line 504
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getCurrentLine()I

    #@3fe
    move-result v3

    #@3ff
    .line 505
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@402
    .line 506
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@405
    goto/16 :goto_9

    #@407
    .line 511
    .end local v3           #_result:I
    :sswitch_407
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@409
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40c
    .line 512
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isTwoLineSupported()Z

    #@40f
    move-result v3

    #@410
    .line 513
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@413
    .line 514
    if-eqz v3, :cond_416

    #@415
    move v6, v7

    #@416
    :cond_416
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@419
    goto/16 :goto_9

    #@41b
    .line 519
    .end local v3           #_result:Z
    :sswitch_41b
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@41d
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@420
    .line 520
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isVoiceInService()Z

    #@423
    move-result v3

    #@424
    .line 521
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@427
    .line 522
    if-eqz v3, :cond_42a

    #@429
    move v6, v7

    #@42a
    :cond_42a
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@42d
    goto/16 :goto_9

    #@42f
    .line 527
    .end local v3           #_result:Z
    :sswitch_42f
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@431
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@434
    .line 528
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getCdmaEriHomeSystems()Ljava/lang/String;

    #@437
    move-result-object v3

    #@438
    .line 529
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@43b
    .line 530
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@43e
    goto/16 :goto_9

    #@440
    .line 535
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_440
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@442
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@445
    .line 536
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isVideoCall()Z

    #@448
    move-result v3

    #@449
    .line 537
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44c
    .line 538
    if-eqz v3, :cond_44f

    #@44e
    move v6, v7

    #@44f
    :cond_44f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@452
    goto/16 :goto_9

    #@454
    .line 543
    .end local v3           #_result:Z
    :sswitch_454
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@456
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@459
    .line 545
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@45c
    move-result-object v0

    #@45d
    .line 546
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->videoDial(Ljava/lang/String;)V

    #@460
    .line 547
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@463
    goto/16 :goto_9

    #@465
    .line 552
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_465
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@467
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46a
    .line 554
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@46d
    move-result-object v0

    #@46e
    .line 556
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@471
    move-result-object v1

    #@472
    .line 558
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@475
    move-result-object v2

    #@476
    .line 559
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/ITelephony$Stub;->registerForCurrentVoIP(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@479
    .line 560
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@47c
    goto/16 :goto_9

    #@47e
    .line 565
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Ljava/lang/String;
    :sswitch_47e
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@480
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@483
    .line 566
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->unregisterForCurrentVoIP()V

    #@486
    .line 567
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@489
    goto/16 :goto_9

    #@48b
    .line 572
    :sswitch_48b
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@48d
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@490
    .line 573
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->setVoIPRining()V

    #@493
    .line 574
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@496
    goto/16 :goto_9

    #@498
    .line 579
    :sswitch_498
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@49a
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49d
    .line 580
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->setVoIPDialing()V

    #@4a0
    .line 581
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a3
    goto/16 :goto_9

    #@4a5
    .line 586
    :sswitch_4a5
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@4a7
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4aa
    .line 587
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->setVoIPInCall()V

    #@4ad
    .line 588
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b0
    goto/16 :goto_9

    #@4b2
    .line 593
    :sswitch_4b2
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@4b4
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b7
    .line 594
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->setVoIPIdle()V

    #@4ba
    .line 595
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4bd
    goto/16 :goto_9

    #@4bf
    .line 600
    :sswitch_4bf
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@4c1
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c4
    .line 601
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->reenableStatusBarforVoIP()V

    #@4c7
    .line 602
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4ca
    goto/16 :goto_9

    #@4cc
    .line 607
    :sswitch_4cc
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@4ce
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d1
    .line 608
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->disableStatusBarforVoIP()V

    #@4d4
    .line 609
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d7
    goto/16 :goto_9

    #@4d9
    .line 614
    :sswitch_4d9
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@4db
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4de
    .line 615
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->busyCall()V

    #@4e1
    .line 616
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e4
    goto/16 :goto_9

    #@4e6
    .line 621
    :sswitch_4e6
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@4e8
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4eb
    .line 622
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isOemRinging()Z

    #@4ee
    move-result v3

    #@4ef
    .line 623
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f2
    .line 624
    if-eqz v3, :cond_4f5

    #@4f4
    move v6, v7

    #@4f5
    :cond_4f5
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@4f8
    goto/16 :goto_9

    #@4fa
    .line 629
    .end local v3           #_result:Z
    :sswitch_4fa
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@4fc
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ff
    .line 630
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isOemDialing()Z

    #@502
    move-result v3

    #@503
    .line 631
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@506
    .line 632
    if-eqz v3, :cond_509

    #@508
    move v6, v7

    #@509
    :cond_509
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@50c
    goto/16 :goto_9

    #@50e
    .line 637
    .end local v3           #_result:Z
    :sswitch_50e
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@510
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@513
    .line 638
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isCSCallIdle()Z

    #@516
    move-result v3

    #@517
    .line 639
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@51a
    .line 640
    if-eqz v3, :cond_51d

    #@51c
    move v6, v7

    #@51d
    :cond_51d
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@520
    goto/16 :goto_9

    #@522
    .line 645
    .end local v3           #_result:Z
    :sswitch_522
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@524
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@527
    .line 646
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->resetVoiceMessageCount()V

    #@52a
    .line 647
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@52d
    goto/16 :goto_9

    #@52f
    .line 652
    :sswitch_52f
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@531
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@534
    .line 654
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@537
    move-result-object v0

    #@538
    .line 655
    .local v0, _arg0:[B
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->uknightLogSet([B)[B

    #@53b
    move-result-object v3

    #@53c
    .line 656
    .local v3, _result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@53f
    .line 657
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@542
    goto/16 :goto_9

    #@544
    .line 662
    .end local v0           #_arg0:[B
    .end local v3           #_result:[B
    :sswitch_544
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@546
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@549
    .line 664
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@54c
    move-result-object v0

    #@54d
    .line 665
    .restart local v0       #_arg0:[B
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->uknightEventSet([B)[B

    #@550
    move-result-object v3

    #@551
    .line 666
    .restart local v3       #_result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@554
    .line 667
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@557
    goto/16 :goto_9

    #@559
    .line 672
    .end local v0           #_arg0:[B
    .end local v3           #_result:[B
    :sswitch_559
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@55b
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@55e
    .line 674
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@561
    move-result v0

    #@562
    .line 675
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->uknightStateChangeSet(I)Z

    #@565
    move-result v3

    #@566
    .line 676
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@569
    .line 677
    if-eqz v3, :cond_56c

    #@56b
    move v6, v7

    #@56c
    :cond_56c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@56f
    goto/16 :goto_9

    #@571
    .line 682
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_571
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@573
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@576
    .line 684
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@579
    move-result v0

    #@57a
    .line 685
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->uknightMemSet(I)Z

    #@57d
    move-result v3

    #@57e
    .line 686
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@581
    .line 687
    if-eqz v3, :cond_584

    #@583
    move v6, v7

    #@584
    :cond_584
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@587
    goto/16 :goto_9

    #@589
    .line 692
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_589
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@58b
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58e
    .line 694
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@591
    move-result v0

    #@592
    .line 695
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->uknightGetData(I)Lcom/android/internal/telephony/KNDataResponse;

    #@595
    move-result-object v3

    #@596
    .line 696
    .local v3, _result:Lcom/android/internal/telephony/KNDataResponse;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@599
    .line 697
    if-eqz v3, :cond_5a3

    #@59b
    .line 698
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@59e
    .line 699
    invoke-virtual {v3, p3, v7}, Lcom/android/internal/telephony/KNDataResponse;->writeToParcel(Landroid/os/Parcel;I)V

    #@5a1
    goto/16 :goto_9

    #@5a3
    .line 702
    :cond_5a3
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@5a6
    goto/16 :goto_9

    #@5a8
    .line 708
    .end local v0           #_arg0:I
    .end local v3           #_result:Lcom/android/internal/telephony/KNDataResponse;
    :sswitch_5a8
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@5aa
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5ad
    .line 709
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->uknightMemCheck()[I

    #@5b0
    move-result-object v3

    #@5b1
    .line 710
    .local v3, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5b4
    .line 711
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@5b7
    goto/16 :goto_9

    #@5b9
    .line 716
    .end local v3           #_result:[I
    :sswitch_5b9
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@5bb
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5be
    .line 717
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->startMobileQualityInformation()V

    #@5c1
    .line 718
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c4
    goto/16 :goto_9

    #@5c6
    .line 723
    :sswitch_5c6
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@5c8
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5cb
    .line 724
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->stopMobileQualityInformation()V

    #@5ce
    .line 725
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5d1
    goto/16 :goto_9

    #@5d3
    .line 730
    :sswitch_5d3
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@5d5
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d8
    .line 731
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getMobileQualityInformation()Ljava/util/Map;

    #@5db
    move-result-object v3

    #@5dc
    .line 732
    .local v3, _result:Ljava/util/Map;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5df
    .line 733
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    #@5e2
    goto/16 :goto_9

    #@5e4
    .line 738
    .end local v3           #_result:Ljava/util/Map;
    :sswitch_5e4
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@5e6
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5e9
    .line 739
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getIccPin2RetryCount()I

    #@5ec
    move-result v3

    #@5ed
    .line 740
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5f0
    .line 741
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5f3
    goto/16 :goto_9

    #@5f5
    .line 746
    .end local v3           #_result:I
    :sswitch_5f5
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@5f7
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5fa
    .line 747
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getIccPuk1RetryCount()I

    #@5fd
    move-result v3

    #@5fe
    .line 748
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@601
    .line 749
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@604
    goto/16 :goto_9

    #@606
    .line 754
    .end local v3           #_result:I
    :sswitch_606
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@608
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60b
    .line 755
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getIccPuk2RetryCount()I

    #@60e
    move-result v3

    #@60f
    .line 756
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@612
    .line 757
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@615
    goto/16 :goto_9

    #@617
    .line 762
    .end local v3           #_result:I
    :sswitch_617
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@619
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61c
    .line 763
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getMipErrorCode()I

    #@61f
    move-result v3

    #@620
    .line 764
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@623
    .line 765
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@626
    goto/16 :goto_9

    #@628
    .line 770
    .end local v3           #_result:I
    :sswitch_628
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@62a
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@62d
    .line 772
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@630
    move-result v0

    #@631
    .line 774
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@634
    move-result v1

    #@635
    .line 775
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/ITelephony$Stub;->getDebugInfo(II)[I

    #@638
    move-result-object v3

    #@639
    .line 776
    .local v3, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@63c
    .line 777
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@63f
    goto/16 :goto_9

    #@641
    .line 782
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v3           #_result:[I
    :sswitch_641
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@643
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@646
    .line 784
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@649
    move-result v0

    #@64a
    .line 786
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@64d
    move-result v1

    #@64e
    .line 787
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/ITelephony$Stub;->checkDataProfileEx(II)Z

    #@651
    move-result v3

    #@652
    .line 788
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@655
    .line 789
    if-eqz v3, :cond_658

    #@657
    move v6, v7

    #@658
    :cond_658
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@65b
    goto/16 :goto_9

    #@65d
    .line 794
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v3           #_result:Z
    :sswitch_65d
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@65f
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@662
    .line 795
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getAPNList()Ljava/lang/String;

    #@665
    move-result-object v3

    #@666
    .line 796
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@669
    .line 797
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@66c
    goto/16 :goto_9

    #@66e
    .line 802
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_66e
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@670
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@673
    .line 804
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@676
    move-result v0

    #@677
    .line 805
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->setNetworkModePreference(I)V

    #@67a
    .line 806
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67d
    goto/16 :goto_9

    #@67f
    .line 811
    .end local v0           #_arg0:I
    :sswitch_67f
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@681
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@684
    .line 812
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->isHeadsetPlugged()Z

    #@687
    move-result v3

    #@688
    .line 813
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@68b
    .line 814
    if-eqz v3, :cond_68e

    #@68d
    move v6, v7

    #@68e
    :cond_68e
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@691
    goto/16 :goto_9

    #@693
    .line 819
    .end local v3           #_result:Z
    :sswitch_693
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@695
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@698
    .line 821
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@69b
    move-result-object v0

    #@69c
    .line 823
    .local v0, _arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@69f
    move-result-object v1

    #@6a0
    .line 824
    .local v1, _arg1:[B
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/ITelephony$Stub;->calculateAkaResponse([B[B)Landroid/os/Bundle;

    #@6a3
    move-result-object v3

    #@6a4
    .line 825
    .local v3, _result:Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6a7
    .line 826
    if-eqz v3, :cond_6b1

    #@6a9
    .line 827
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@6ac
    .line 828
    invoke-virtual {v3, p3, v7}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@6af
    goto/16 :goto_9

    #@6b1
    .line 831
    :cond_6b1
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@6b4
    goto/16 :goto_9

    #@6b6
    .line 837
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:[B
    .end local v3           #_result:Landroid/os/Bundle;
    :sswitch_6b6
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@6b8
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6bb
    .line 839
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@6be
    move-result-object v0

    #@6bf
    .line 841
    .restart local v0       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@6c2
    move-result-object v1

    #@6c3
    .line 842
    .restart local v1       #_arg1:[B
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/ITelephony$Stub;->calculateGbaBootstrappingResponse([B[B)Landroid/os/Bundle;

    #@6c6
    move-result-object v3

    #@6c7
    .line 843
    .restart local v3       #_result:Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6ca
    .line 844
    if-eqz v3, :cond_6d4

    #@6cc
    .line 845
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@6cf
    .line 846
    invoke-virtual {v3, p3, v7}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@6d2
    goto/16 :goto_9

    #@6d4
    .line 849
    :cond_6d4
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@6d7
    goto/16 :goto_9

    #@6d9
    .line 855
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:[B
    .end local v3           #_result:Landroid/os/Bundle;
    :sswitch_6d9
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@6db
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6de
    .line 857
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@6e1
    move-result-object v0

    #@6e2
    .line 858
    .restart local v0       #_arg0:[B
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->calculateNafExternalKey([B)[B

    #@6e5
    move-result-object v3

    #@6e6
    .line 859
    .local v3, _result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e9
    .line 860
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@6ec
    goto/16 :goto_9

    #@6ee
    .line 865
    .end local v0           #_arg0:[B
    .end local v3           #_result:[B
    :sswitch_6ee
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@6f0
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6f3
    .line 867
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@6f6
    move-result-object v0

    #@6f7
    .line 869
    .restart local v0       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6fa
    move-result-object v1

    #@6fb
    .line 871
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6fe
    move-result-object v2

    #@6ff
    .line 872
    .restart local v2       #_arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/ITelephony$Stub;->setGbaBootstrappingParams([BLjava/lang/String;Ljava/lang/String;)V

    #@702
    .line 873
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@705
    goto/16 :goto_9

    #@707
    .line 878
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Ljava/lang/String;
    :sswitch_707
    const-string v8, "com.android.internal.telephony.ITelephony"

    #@709
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70c
    .line 879
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getIccFdnEnabled()Z

    #@70f
    move-result v3

    #@710
    .line 880
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@713
    .line 881
    if-eqz v3, :cond_716

    #@715
    move v6, v7

    #@716
    :cond_716
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@719
    goto/16 :goto_9

    #@71b
    .line 886
    .end local v3           #_result:Z
    :sswitch_71b
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@71d
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@720
    .line 888
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@723
    move-result v0

    #@724
    .line 889
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ITelephony$Stub;->setVzwGpsFieldTestScreenState(I)V

    #@727
    .line 890
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@72a
    goto/16 :goto_9

    #@72c
    .line 895
    .end local v0           #_arg0:I
    :sswitch_72c
    const-string v6, "com.android.internal.telephony.ITelephony"

    #@72e
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@731
    .line 896
    invoke-virtual {p0}, Lcom/android/internal/telephony/ITelephony$Stub;->getVzwGpsFieldTestScreenState()I

    #@734
    move-result v3

    #@735
    .line 897
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@738
    .line 898
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@73b
    goto/16 :goto_9

    #@73d
    .line 46
    nop

    #@73e
    :sswitch_data_73e
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_30
        0x4 -> :sswitch_43
        0x5 -> :sswitch_5f
        0x6 -> :sswitch_72
        0x7 -> :sswitch_85
        0x8 -> :sswitch_99
        0x9 -> :sswitch_ad
        0xa -> :sswitch_ba
        0xb -> :sswitch_c7
        0xc -> :sswitch_db
        0xd -> :sswitch_ef
        0xe -> :sswitch_103
        0xf -> :sswitch_117
        0x10 -> :sswitch_12b
        0x11 -> :sswitch_138
        0x12 -> :sswitch_150
        0x13 -> :sswitch_165
        0x14 -> :sswitch_17e
        0x15 -> :sswitch_18f
        0x16 -> :sswitch_1ab
        0x17 -> :sswitch_1c3
        0x18 -> :sswitch_1d0
        0x19 -> :sswitch_1ed
        0x1a -> :sswitch_1fa
        0x1b -> :sswitch_207
        0x1c -> :sswitch_214
        0x1d -> :sswitch_229
        0x1e -> :sswitch_23e
        0x1f -> :sswitch_252
        0x20 -> :sswitch_266
        0x21 -> :sswitch_27a
        0x22 -> :sswitch_295
        0x23 -> :sswitch_2a6
        0x24 -> :sswitch_2b7
        0x25 -> :sswitch_2c8
        0x26 -> :sswitch_2d9
        0x27 -> :sswitch_2ea
        0x28 -> :sswitch_2fb
        0x29 -> :sswitch_30c
        0x2a -> :sswitch_31d
        0x2b -> :sswitch_331
        0x2c -> :sswitch_342
        0x2d -> :sswitch_353
        0x2e -> :sswitch_367
        0x2f -> :sswitch_378
        0x30 -> :sswitch_38e
        0x31 -> :sswitch_3a2
        0x32 -> :sswitch_3af
        0x33 -> :sswitch_3c0
        0x34 -> :sswitch_3d1
        0x35 -> :sswitch_3e5
        0x36 -> :sswitch_3f6
        0x37 -> :sswitch_407
        0x38 -> :sswitch_41b
        0x39 -> :sswitch_42f
        0x3a -> :sswitch_440
        0x3b -> :sswitch_454
        0x3c -> :sswitch_465
        0x3d -> :sswitch_47e
        0x3e -> :sswitch_48b
        0x3f -> :sswitch_498
        0x40 -> :sswitch_4a5
        0x41 -> :sswitch_4b2
        0x42 -> :sswitch_4bf
        0x43 -> :sswitch_4cc
        0x44 -> :sswitch_4d9
        0x45 -> :sswitch_4e6
        0x46 -> :sswitch_4fa
        0x47 -> :sswitch_50e
        0x48 -> :sswitch_522
        0x49 -> :sswitch_52f
        0x4a -> :sswitch_544
        0x4b -> :sswitch_559
        0x4c -> :sswitch_571
        0x4d -> :sswitch_589
        0x4e -> :sswitch_5a8
        0x4f -> :sswitch_5b9
        0x50 -> :sswitch_5c6
        0x51 -> :sswitch_5d3
        0x52 -> :sswitch_5e4
        0x53 -> :sswitch_5f5
        0x54 -> :sswitch_606
        0x55 -> :sswitch_617
        0x56 -> :sswitch_628
        0x57 -> :sswitch_641
        0x58 -> :sswitch_65d
        0x59 -> :sswitch_66e
        0x5a -> :sswitch_67f
        0x5b -> :sswitch_693
        0x5c -> :sswitch_6b6
        0x5d -> :sswitch_6d9
        0x5e -> :sswitch_6ee
        0x5f -> :sswitch_707
        0x60 -> :sswitch_71b
        0x61 -> :sswitch_72c
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
