.class public Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;
.super Ljava/lang/Object;
.source "LgeAutoProfiling.java"

# interfaces
.implements Lcom/android/internal/telephony/lgeautoprofiling/LgeKeyProfiling;
.implements Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfilingConstants;


# static fields
.field private static final DBG:Z = false

.field private static final EDBG:Z = true

.field private static final VDBG:Z = true


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    return-void
.end method

.method public static checkEccIdleMode(Ljava/lang/String;)Z
    .registers 10
    .parameter "dialNumber"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 136
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v7

    #@5
    if-eqz v7, :cond_8

    #@7
    .line 152
    :cond_7
    :goto_7
    return v6

    #@8
    .line 138
    :cond_8
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@b
    move-result v1

    #@c
    .line 140
    .local v1, defaultSubId:I
    const/4 v7, 0x0

    #@d
    const-string v8, "ECC_IdleMode"

    #@f
    invoke-static {v7, v8, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 142
    .local v2, eccIdleModeList:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16
    move-result v7

    #@17
    if-nez v7, :cond_7

    #@19
    .line 144
    const-string v7, ","

    #@1b
    invoke-virtual {v2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@20
    .local v4, len$:I
    const/4 v3, 0x0

    #@21
    .local v3, i$:I
    :goto_21
    if-ge v3, v4, :cond_7

    #@23
    aget-object v5, v0, v3

    #@25
    .line 145
    .local v5, token:Ljava/lang/String;
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v7

    #@29
    if-eqz v7, :cond_49

    #@2b
    .line 146
    sget-boolean v6, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->ENABLE_PRIVACY_LOG:Z

    #@2d
    if-eqz v6, :cond_47

    #@2f
    .line 147
    const-string v6, "TelephonyAutoProfiling"

    #@31
    new-instance v7, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v8, "[checkEccIdleMode] Ecc_IdleMode : true - "

    #@38
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v7

    #@3c
    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v7

    #@44
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 148
    :cond_47
    const/4 v6, 0x1

    #@48
    goto :goto_7

    #@49
    .line 144
    :cond_49
    add-int/lit8 v3, v3, 0x1

    #@4b
    goto :goto_21
.end method

.method public static checkShortCodeCall(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 10
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 180
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v7

    #@5
    if-eqz v7, :cond_8

    #@7
    .line 198
    :cond_7
    :goto_7
    return v6

    #@8
    .line 184
    :cond_8
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@b
    move-result v1

    #@c
    .line 186
    .local v1, defaultSubId:I
    const-string v7, "ShortCodeCall"

    #@e
    invoke-static {p0, v7, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    .line 188
    .local v4, shortCode:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@15
    move-result v7

    #@16
    if-nez v7, :cond_7

    #@18
    .line 189
    const-string v7, ","

    #@1a
    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@1f
    .local v3, len$:I
    const/4 v2, 0x0

    #@20
    .local v2, i$:I
    :goto_20
    if-ge v2, v3, :cond_7

    #@22
    aget-object v5, v0, v2

    #@24
    .line 190
    .local v5, token:Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_2c

    #@2a
    .line 193
    const/4 v6, 0x1

    #@2b
    goto :goto_7

    #@2c
    .line 189
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_20
.end method

.method public static getClirSettingValue(Landroid/content/Context;)Ljava/lang/String;
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 109
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@3
    move-result v2

    #@4
    .line 110
    .local v2, defaultSubId:I
    const-string v0, "-1"

    #@6
    .line 111
    .local v0, clirSetting:Ljava/lang/String;
    const-string/jumbo v4, "persist.radio.iccid-changed"

    #@9
    const-string v5, "F"

    #@b
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    .line 113
    .local v3, sim_changed:Ljava/lang/String;
    const-string v4, "0"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_24

    #@17
    .line 114
    const-string v4, "SendMyNumberInformation"

    #@19
    invoke-static {p0, v4, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 115
    .local v1, clirSettingTemp:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@20
    move-result v4

    #@21
    if-nez v4, :cond_24

    #@23
    .line 116
    move-object v0, v1

    #@24
    .line 121
    .end local v1           #clirSettingTemp:Ljava/lang/String;
    :cond_24
    return-object v0
.end method

.method public static getECCList(Landroid/content/Context;)[Ljava/lang/String;
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 243
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@3
    move-result v2

    #@4
    .line 244
    .local v2, defaultSubId:I
    const-string v6, "ECC_list"

    #@6
    invoke-static {p0, v6, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 246
    .local v0, NumberString:Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    #@c
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 248
    .local v4, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/StringTokenizer;

    #@11
    const-string v6, ","

    #@13
    invoke-direct {v5, v0, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 250
    .local v5, st:Ljava/util/StringTokenizer;
    :goto_16
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@19
    move-result v6

    #@1a
    if-eqz v6, :cond_24

    #@1c
    .line 251
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@1f
    move-result-object v6

    #@20
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@23
    goto :goto_16

    #@24
    .line 253
    :cond_24
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@27
    move-result v6

    #@28
    new-array v1, v6, [Ljava/lang/String;

    #@2a
    .line 255
    .local v1, StrArray:[Ljava/lang/String;
    const/4 v3, 0x0

    #@2b
    .local v3, i:I
    :goto_2b
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@2e
    move-result v6

    #@2f
    if-ge v3, v6, :cond_3c

    #@31
    .line 256
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v6

    #@35
    check-cast v6, Ljava/lang/String;

    #@37
    aput-object v6, v1, v3

    #@39
    .line 255
    add-int/lit8 v3, v3, 0x1

    #@3b
    goto :goto_2b

    #@3c
    .line 259
    :cond_3c
    return-object v1
.end method

.method public static getEccListMerged(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "eccList"

    #@0
    .prologue
    .line 156
    const-string/jumbo v10, "ril.ecclist.autoprofile"

    #@3
    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v3

    #@7
    .line 159
    .local v3, eccListAutoProfile:Ljava/lang/String;
    const-string v8, ""

    #@9
    .line 161
    .local v8, tempEcclist:Ljava/lang/String;
    const-string v10, ","

    #@b
    invoke-virtual {v3, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@10
    .local v6, len$:I
    const/4 v4, 0x0

    #@11
    .local v4, i$:I
    move v5, v4

    #@12
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v6           #len$:I
    .local v5, i$:I
    :goto_12
    if-ge v5, v6, :cond_41

    #@14
    aget-object v9, v0, v5

    #@16
    .line 162
    .local v9, token:Ljava/lang/String;
    const-string v10, ","

    #@18
    invoke-virtual {p0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .local v1, arr$:[Ljava/lang/String;
    array-length v7, v1

    #@1d
    .local v7, len$:I
    const/4 v4, 0x0

    #@1e
    .end local v5           #i$:I
    .restart local v4       #i$:I
    :goto_1e
    if-ge v4, v7, :cond_3d

    #@20
    aget-object v2, v1, v4

    #@22
    .line 163
    .local v2, ecc:Ljava/lang/String;
    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v10

    #@26
    if-nez v10, :cond_3a

    #@28
    .line 164
    const-string v10, ","

    #@2a
    invoke-virtual {p0, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@2d
    move-result v10

    #@2e
    if-nez v10, :cond_36

    #@30
    .line 165
    const-string v10, ","

    #@32
    invoke-virtual {v8, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v8

    #@36
    .line 167
    :cond_36
    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@39
    move-result-object v8

    #@3a
    .line 162
    :cond_3a
    add-int/lit8 v4, v4, 0x1

    #@3c
    goto :goto_1e

    #@3d
    .line 161
    .end local v2           #ecc:Ljava/lang/String;
    :cond_3d
    add-int/lit8 v4, v5, 0x1

    #@3f
    move v5, v4

    #@40
    .end local v4           #i$:I
    .restart local v5       #i$:I
    goto :goto_12

    #@41
    .line 171
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v7           #len$:I
    .end local v9           #token:Ljava/lang/String;
    :cond_41
    invoke-virtual {p0, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object p0

    #@45
    .line 176
    return-object p0
.end method

.method public static getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "context"
    .parameter "key"

    #@0
    .prologue
    .line 84
    invoke-static {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "key"
    .parameter "subId"

    #@0
    .prologue
    .line 79
    invoke-static {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getValue(Ljava/lang/String;I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getRVMS(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 212
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@3
    move-result v0

    #@4
    .line 214
    .local v0, defaultSubId:I
    invoke-static {p0, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getRVMS(Landroid/content/Context;I)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public static getRVMS(Landroid/content/Context;I)Ljava/lang/String;
    .registers 3
    .parameter "context"
    .parameter "subId"

    #@0
    .prologue
    .line 218
    const-string v0, "RVMS"

    #@2
    invoke-static {p0, v0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static getVMS(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 202
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@3
    move-result v0

    #@4
    .line 204
    .local v0, defaultSubId:I
    invoke-static {p0, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getVMS(Landroid/content/Context;I)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public static getVMS(Landroid/content/Context;I)Ljava/lang/String;
    .registers 3
    .parameter "context"
    .parameter "subId"

    #@0
    .prologue
    .line 208
    const-string v0, "VMS"

    #@2
    invoke-static {p0, v0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static init(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "phoneType"

    #@0
    .prologue
    .line 56
    const-string v1, "TelephonyAutoProfiling"

    #@2
    const-string v2, "[init] ******** Telephony Auto Profiling *******"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 58
    if-nez p0, :cond_11

    #@9
    .line 59
    const-string v1, "TelephonyAutoProfiling"

    #@b
    const-string v2, "[init] context is null, return"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 72
    :cond_10
    :goto_10
    return-void

    #@11
    .line 64
    :cond_11
    invoke-static {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->loadFeature()V

    #@18
    .line 66
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getDefaultSubScription()I

    #@1b
    move-result v0

    #@1c
    .line 67
    .local v0, defaultSubId:I
    invoke-static {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->loadProfile(I)V

    #@23
    .line 69
    const/4 v1, 0x2

    #@24
    if-ne p1, v1, :cond_10

    #@26
    .line 70
    const-string/jumbo v1, "ril.ecclist.autoprofile"

    #@29
    const-string v2, "ECC_list"

    #@2b
    invoke-static {p0, v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getProfileInfo(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    goto :goto_10
.end method

.method public static isCountry(Ljava/lang/String;)Z
    .registers 2
    .parameter "country"

    #@0
    .prologue
    .line 226
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->COUNTRY:Ljava/lang/String;

    #@2
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isOperator(Ljava/lang/String;)Z
    .registers 2
    .parameter "operator"

    #@0
    .prologue
    .line 230
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->OPERATOR:Ljava/lang/String;

    #@2
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "Country"
    .parameter "Operator"

    #@0
    .prologue
    .line 222
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->COUNTRY:Ljava/lang/String;

    #@2
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_12

    #@8
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->OPERATOR:Ljava/lang/String;

    #@a
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public static isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 5
    .parameter "context"
    .parameter "key"

    #@0
    .prologue
    .line 126
    const/4 v0, 0x0

    #@1
    .line 129
    .local v0, result:Z
    invoke-static {p0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    const-string/jumbo v2, "true"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    .line 132
    return v0
.end method

.method public static isUserMode()Z
    .registers 3

    #@0
    .prologue
    .line 265
    const-string/jumbo v1, "user"

    #@3
    .line 266
    .local v1, strUserMode:Ljava/lang/String;
    const-string/jumbo v2, "ro.build.type"

    #@6
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 276
    .local v0, buildType:Ljava/lang/String;
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@d
    move-result v2

    #@e
    return v2
.end method

.method protected static profileToMap(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;
    .registers 7
    .parameter "profile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 88
    new-instance v2, Ljava/util/HashMap;

    #@2
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 90
    .local v2, profileMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@6
    .line 91
    .local v0, key:Ljava/lang/String;
    const/4 v4, 0x0

    #@7
    .line 92
    .local v4, value:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->getValueMap()Ljava/util/HashMap;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@e
    move-result-object v3

    #@f
    .line 93
    .local v3, set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v1

    #@13
    .line 95
    .local v1, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_27

    #@19
    .line 96
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    .end local v0           #key:Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    #@1f
    .line 97
    .restart local v0       #key:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    .line 101
    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    goto :goto_13

    #@27
    .line 104
    :cond_27
    return-object v2
.end method

.method public static updateProfile(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 3
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 75
    invoke-static {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfile;->updateProfile(Landroid/content/Intent;)V

    #@7
    .line 76
    return-void
.end method


# virtual methods
.method public getTargetCountry()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 238
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->COUNTRY:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTargetOperator()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 234
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->OPERATOR:Ljava/lang/String;

    #@2
    return-object v0
.end method
