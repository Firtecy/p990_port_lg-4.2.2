.class public final enum Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
.super Ljava/lang/Enum;
.source "PhoneConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PhoneConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LteStateInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field public static final enum EMERGENCY_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field public static final enum EMERGENCY_DETACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field public static final enum NONE:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field public static final enum NORMAL_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field public static final enum NORMAL_DETACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field private static final sLteStateInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCode:I


# direct methods
.method static constructor <clinit>()V
    .registers 11

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    const/4 v9, 0x3

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 305
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@7
    const-string v5, "NONE"

    #@9
    invoke-direct {v4, v5, v6, v6}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NONE:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@e
    .line 306
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@10
    const-string v5, "NORMAL_DETACHED"

    #@12
    invoke-direct {v4, v5, v7, v7}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NORMAL_DETACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@17
    .line 307
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@19
    const-string v5, "EMERGENCY_ATTACHED"

    #@1b
    invoke-direct {v4, v5, v8, v8}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->EMERGENCY_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@20
    .line 308
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@22
    const-string v5, "NORMAL_ATTACHED"

    #@24
    invoke-direct {v4, v5, v9, v9}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NORMAL_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@29
    .line 309
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@2b
    const-string v5, "EMERGENCY_DETACHED"

    #@2d
    invoke-direct {v4, v5, v10, v10}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->EMERGENCY_DETACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@32
    .line 304
    const/4 v4, 0x5

    #@33
    new-array v4, v4, [Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@35
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NONE:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@37
    aput-object v5, v4, v6

    #@39
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NORMAL_DETACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@3b
    aput-object v5, v4, v7

    #@3d
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->EMERGENCY_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@3f
    aput-object v5, v4, v8

    #@41
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NORMAL_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@43
    aput-object v5, v4, v9

    #@45
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->EMERGENCY_DETACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@47
    aput-object v5, v4, v10

    #@49
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@4b
    .line 314
    new-instance v4, Ljava/util/HashMap;

    #@4d
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@50
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->sLteStateInfoMap:Ljava/util/HashMap;

    #@52
    .line 315
    invoke-static {}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->values()[Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@55
    move-result-object v0

    #@56
    .local v0, arr$:[Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    array-length v2, v0

    #@57
    .local v2, len$:I
    const/4 v1, 0x0

    #@58
    .local v1, i$:I
    :goto_58
    if-ge v1, v2, :cond_6c

    #@5a
    aget-object v3, v0, v1

    #@5c
    .line 316
    .local v3, lsi:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    sget-object v4, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->sLteStateInfoMap:Ljava/util/HashMap;

    #@5e
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->getCode()I

    #@61
    move-result v5

    #@62
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    .line 315
    add-int/lit8 v1, v1, 0x1

    #@6b
    goto :goto_58

    #@6c
    .line 318
    .end local v3           #lsi:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    :cond_6c
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "code"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 320
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 321
    iput p3, p0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->mCode:I

    #@5
    .line 322
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    .registers 4
    .parameter "code"

    #@0
    .prologue
    .line 329
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->sLteStateInfoMap:Ljava/util/HashMap;

    #@2
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@c
    .line 330
    .local v0, lsi:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    if-nez v0, :cond_10

    #@e
    .line 331
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NONE:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@10
    .line 333
    :cond_10
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 304
    const-class v0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    .registers 1

    #@0
    .prologue
    .line 304
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@8
    return-object v0
.end method


# virtual methods
.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 325
    iget v0, p0, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->mCode:I

    #@2
    return v0
.end method
