.class public Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;
.super Ljava/lang/Object;
.source "LgeFeature.java"

# interfaces
.implements Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfilingConstants;


# static fields
.field private static final DBG:Z = true

.field private static final EDBG:Z = true

.field private static final VDBG:Z = true

.field private static instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFeature:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mContext:Landroid/content/Context;

    #@5
    .line 46
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 49
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 50
    new-instance v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;

    #@6
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;

    #@b
    .line 55
    :goto_b
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;

    #@d
    return-object v0

    #@e
    .line 52
    :cond_e
    sget-object v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->instance:Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;

    #@10
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->setContext(Landroid/content/Context;)V

    #@13
    goto :goto_b
.end method

.method private loadFeatureFromPreferences()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 78
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "feature"

    #@5
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@8
    move-result-object v0

    #@9
    .line 80
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v2, "init"

    #@b
    const/4 v3, 0x0

    #@c
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    if-nez v2, :cond_1a

    #@12
    .line 81
    const-string v2, "TelephonyAutoProfiling"

    #@14
    const-string v3, "[loadFeatureFromPreferences] preferences do not exist"

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 91
    :goto_19
    return v1

    #@1a
    .line 86
    :cond_1a
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Ljava/util/HashMap;

    #@20
    iput-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@22
    .line 88
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@24
    const-string v2, "init"

    #@26
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    .line 90
    const-string v1, "TelephonyAutoProfiling"

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "[loadFeatureFromPreferences] load from preferences complete - "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    iget-object v3, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 91
    const/4 v1, 0x1

    #@44
    goto :goto_19
.end method

.method private loadFeatureFromXml()V
    .registers 6

    #@0
    .prologue
    .line 95
    const-string v2, "TelephonyAutoProfiling"

    #@2
    const-string v3, "[loadFeatureFromXml] *** start feature loading from xml"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 97
    new-instance v1, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;

    #@9
    invoke-direct {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;-><init>()V

    #@c
    .line 99
    .local v1, parser:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;
    const/4 v2, 0x2

    #@d
    const/4 v3, 0x0

    #@e
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->getMatchedProfile(ILcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;

    #@14
    .line 101
    .local v0, featureFromXml:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;
    if-nez v0, :cond_1e

    #@16
    .line 102
    const-string v2, "TelephonyAutoProfiling"

    #@18
    const-string v3, "[loadDataFromXml] load feature from xml failed"

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 116
    :goto_1d
    return-void

    #@1e
    .line 107
    :cond_1e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->profileToMap(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@21
    move-result-object v2

    #@22
    iput-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@24
    .line 109
    iget-object v2, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mContext:Landroid/content/Context;

    #@26
    if-eqz v2, :cond_28

    #@28
    .line 115
    :cond_28
    const-string v2, "TelephonyAutoProfiling"

    #@2a
    new-instance v3, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v4, "[loadDataFromXml] load feature from xml complete : "

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    iget-object v4, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    goto :goto_1d
.end method

.method private saveFeatureToPreferences(Ljava/util/HashMap;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 119
    .local p1, featureMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "feature"

    #@4
    const/4 v5, 0x0

    #@5
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@8
    move-result-object v3

    #@9
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@c
    move-result-object v0

    #@d
    .line 121
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "TelephonyAutoProfiling"

    #@f
    const-string v4, "[saveFeatureToPreferences] save to file : feature"

    #@11
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 123
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    #@17
    .line 125
    const-string v3, "init"

    #@19
    const-string v4, "done"

    #@1b
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@1e
    .line 127
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@21
    move-result-object v3

    #@22
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v2

    #@26
    .local v2, i$:Ljava/util/Iterator;
    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_42

    #@2c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v1

    #@30
    check-cast v1, Ljava/util/Map$Entry;

    #@32
    .line 128
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@35
    move-result-object v3

    #@36
    check-cast v3, Ljava/lang/String;

    #@38
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@3b
    move-result-object v4

    #@3c
    check-cast v4, Ljava/lang/String;

    #@3e
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@41
    goto :goto_26

    #@42
    .line 131
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_42
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@45
    .line 132
    return-void
.end method

.method private setContext(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 60
    iput-object p1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mContext:Landroid/content/Context;

    #@6
    .line 62
    :cond_6
    return-void
.end method


# virtual methods
.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    .line 135
    const/4 v0, 0x0

    #@1
    .line 137
    .local v0, value:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@3
    if-nez v1, :cond_8

    #@5
    .line 138
    invoke-virtual {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->loadFeature()V

    #@8
    .line 141
    :cond_8
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@a
    if-eqz v1, :cond_14

    #@c
    .line 142
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mFeature:Ljava/util/HashMap;

    #@e
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    .end local v0           #value:Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    #@14
    .line 145
    .restart local v0       #value:Ljava/lang/String;
    :cond_14
    const-string v1, "TelephonyAutoProfiling"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "[getValue] FEATURE key : "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, ", value : "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 147
    if-nez v0, :cond_3a

    #@38
    const-string v0, ""

    #@3a
    .end local v0           #value:Ljava/lang/String;
    :cond_3a
    return-object v0
.end method

.method public loadFeature()V
    .registers 3

    #@0
    .prologue
    .line 65
    const/4 v0, 0x0

    #@1
    .line 67
    .local v0, loadSuccessFromPreferences:Z
    iget-object v1, p0, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->mContext:Landroid/content/Context;

    #@3
    if-eqz v1, :cond_5

    #@5
    .line 72
    :cond_5
    if-nez v0, :cond_a

    #@7
    .line 73
    invoke-direct {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeFeature;->loadFeatureFromXml()V

    #@a
    .line 75
    :cond_a
    return-void
.end method
