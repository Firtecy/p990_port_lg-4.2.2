.class public Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;
.super Ljava/lang/Object;
.source "LgeProfileParser.java"

# interfaces
.implements Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfilingConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;,
        Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final EDBG:Z = true

.field private static final VDBG:Z = true


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 475
    return-void
.end method

.method private isFileExist(Ljava/io/File;)Z
    .registers 3
    .parameter "file"

    #@0
    .prologue
    .line 472
    if-eqz p1, :cond_a

    #@2
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method


# virtual methods
.method protected final beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .registers 8
    .parameter "parser"
    .parameter "firstElementName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 55
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@4
    move-result v1

    #@5
    .local v1, type:I
    if-eq v1, v3, :cond_a

    #@7
    const/4 v2, 0x1

    #@8
    if-ne v1, v2, :cond_1

    #@a
    .line 59
    :cond_a
    if-eq v1, v3, :cond_14

    #@c
    .line 60
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@e
    const-string v3, "No start tag found"

    #@10
    invoke-direct {v2, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@13
    throw v2

    #@14
    .line 63
    :cond_14
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 64
    .local v0, first:Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_41

    #@1e
    .line 65
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "Unexpected start tag: found "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, ", expected "

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-direct {v2, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@40
    throw v2

    #@41
    .line 68
    :cond_41
    return-void
.end method

.method protected existInTokens(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "string"
    .parameter "v"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 126
    if-nez p1, :cond_4

    #@3
    .line 136
    :cond_3
    :goto_3
    return v1

    #@4
    .line 130
    :cond_4
    new-instance v0, Ljava/util/StringTokenizer;

    #@6
    const-string v2, ","

    #@8
    invoke-direct {v0, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 131
    .local v0, st:Ljava/util/StringTokenizer;
    :cond_b
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_3

    #@11
    .line 132
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_b

    #@1b
    .line 133
    const/4 v1, 0x1

    #@1c
    goto :goto_3
.end method

.method protected existInTokens(Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 10
    .parameter "string"
    .parameter "v"
    .parameter "len"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 140
    if-eqz p1, :cond_5

    #@3
    if-nez p2, :cond_6

    #@5
    .line 163
    :cond_5
    :goto_5
    return v4

    #@6
    .line 144
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@9
    move-result v3

    #@a
    .line 147
    .local v3, xml_length:I
    if-le v3, p3, :cond_1d

    #@c
    .line 148
    move v0, p3

    #@d
    .line 156
    .local v0, final_length:I
    :goto_d
    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    .line 157
    .local v2, fixed_xml_gid:Ljava/lang/String;
    invoke-virtual {p2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 159
    .local v1, fixed_sim_gid:Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_5

    #@1b
    .line 160
    const/4 v4, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 151
    .end local v0           #final_length:I
    .end local v1           #fixed_sim_gid:Ljava/lang/String;
    .end local v2           #fixed_xml_gid:Ljava/lang/String;
    :cond_1d
    move v0, v3

    #@1e
    .restart local v0       #final_length:I
    goto :goto_d
.end method

.method public getMatchedProfile(ILcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    .registers 13
    .parameter "type"
    .parameter "simInfo"

    #@0
    .prologue
    .line 404
    const/4 v5, 0x0

    #@1
    .line 407
    .local v5, matchedProfile:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    const/4 v3, 0x0

    #@2
    .line 408
    .local v3, in:Ljava/io/FileReader;
    const/4 v0, 0x0

    #@3
    .line 410
    .local v0, confFile:Ljava/io/File;
    packed-switch p1, :pswitch_data_ce

    #@6
    .line 440
    const-string v7, "TelephonyAutoProfiling"

    #@8
    const-string v8, "[getMatchedProfile] unsupported type"

    #@a
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 441
    const/4 v7, 0x0

    #@e
    .line 468
    :goto_e
    return-object v7

    #@f
    .line 413
    :pswitch_f
    new-instance v0, Ljava/io/File;

    #@11
    .end local v0           #confFile:Ljava/io/File;
    sget-object v7, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->FILE_PATH_CUPSS_FEATURE:Ljava/lang/String;

    #@13
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    .line 415
    .restart local v0       #confFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@19
    move-result v7

    #@1a
    if-nez v7, :cond_23

    #@1c
    .line 416
    new-instance v0, Ljava/io/File;

    #@1e
    .end local v0           #confFile:Ljava/io/File;
    const-string v7, "/etc/featureset.xml"

    #@20
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@23
    .line 444
    .restart local v0       #confFile:Ljava/io/File;
    :cond_23
    :goto_23
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->isFileExist(Ljava/io/File;)Z

    #@26
    move-result v7

    #@27
    if-eqz v7, :cond_45

    #@29
    .line 445
    const-string v7, "TelephonyAutoProfiling"

    #@2b
    new-instance v8, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v9, "[getMatchedProfile] selected file : "

    #@32
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v8

    #@36
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v8

    #@42
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 449
    :cond_45
    :try_start_45
    new-instance v4, Ljava/io/FileReader;

    #@47
    invoke-direct {v4, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_4a
    .catchall {:try_start_45 .. :try_end_4a} :catchall_b8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_45 .. :try_end_4a} :catch_9a
    .catch Ljava/io/FileNotFoundException; {:try_start_45 .. :try_end_4a} :catch_a9

    #@4a
    .line 451
    .end local v3           #in:Ljava/io/FileReader;
    .local v4, in:Ljava/io/FileReader;
    :try_start_4a
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    #@4d
    move-result-object v2

    #@4e
    .line 452
    .local v2, factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@51
    move-result-object v6

    #@52
    .line 453
    .local v6, parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v6, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@55
    .line 455
    invoke-virtual {p0, p1, v6, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->getMatchedProfile(ILorg/xmlpull/v1/XmlPullParser;Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    :try_end_58
    .catchall {:try_start_4a .. :try_end_58} :catchall_c4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4a .. :try_end_58} :catch_ca
    .catch Ljava/io/FileNotFoundException; {:try_start_4a .. :try_end_58} :catch_c7

    #@58
    move-result-object v5

    #@59
    .line 462
    if-eqz v4, :cond_5e

    #@5b
    :try_start_5b
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_5e
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_5e} :catch_94

    #@5e
    :cond_5e
    move-object v3, v4

    #@5f
    .end local v2           #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v4           #in:Ljava/io/FileReader;
    .end local v6           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v3       #in:Ljava/io/FileReader;
    :cond_5f
    :goto_5f
    move-object v7, v5

    #@60
    .line 468
    goto :goto_e

    #@61
    .line 423
    :pswitch_61
    new-instance v0, Ljava/io/File;

    #@63
    .end local v0           #confFile:Ljava/io/File;
    sget-object v7, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->FILE_PATH_CUPSS_PROFILE:Ljava/lang/String;

    #@65
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@68
    .line 425
    .restart local v0       #confFile:Ljava/io/File;
    const-string/jumbo v7, "ro.lge.autoprofile"

    #@6b
    const/4 v8, 0x0

    #@6c
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6f
    move-result v7

    #@70
    if-eqz v7, :cond_86

    #@72
    .line 426
    new-instance v0, Ljava/io/File;

    #@74
    .end local v0           #confFile:Ljava/io/File;
    sget-object v7, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->FILE_PATH_CUPSS_PROFILE_OPEN:Ljava/lang/String;

    #@76
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@79
    .line 428
    .restart local v0       #confFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@7c
    move-result v7

    #@7d
    if-nez v7, :cond_86

    #@7f
    .line 429
    const-string v7, "TelephonyAutoProfiling"

    #@81
    const-string v8, "can not found telephonyOpen.xml"

    #@83
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 433
    :cond_86
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@89
    move-result v7

    #@8a
    if-nez v7, :cond_23

    #@8c
    .line 434
    new-instance v0, Ljava/io/File;

    #@8e
    .end local v0           #confFile:Ljava/io/File;
    const-string v7, "/etc/telephony.xml"

    #@90
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@93
    .restart local v0       #confFile:Ljava/io/File;
    goto :goto_23

    #@94
    .line 463
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v2       #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4       #in:Ljava/io/FileReader;
    .restart local v6       #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_94
    move-exception v1

    #@95
    .line 464
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@98
    move-object v3, v4

    #@99
    .line 466
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_5f

    #@9a
    .line 456
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v6           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_9a
    move-exception v1

    #@9b
    .line 457
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_9b
    :try_start_9b
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_9e
    .catchall {:try_start_9b .. :try_end_9e} :catchall_b8

    #@9e
    .line 462
    if-eqz v3, :cond_5f

    #@a0
    :try_start_a0
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_a3
    .catch Ljava/io/IOException; {:try_start_a0 .. :try_end_a3} :catch_a4

    #@a3
    goto :goto_5f

    #@a4
    .line 463
    :catch_a4
    move-exception v1

    #@a5
    .line 464
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@a8
    goto :goto_5f

    #@a9
    .line 458
    .end local v1           #e:Ljava/io/IOException;
    :catch_a9
    move-exception v1

    #@aa
    .line 459
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_aa
    :try_start_aa
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_ad
    .catchall {:try_start_aa .. :try_end_ad} :catchall_b8

    #@ad
    .line 462
    if-eqz v3, :cond_5f

    #@af
    :try_start_af
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_b2
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_b2} :catch_b3

    #@b2
    goto :goto_5f

    #@b3
    .line 463
    :catch_b3
    move-exception v1

    #@b4
    .line 464
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@b7
    goto :goto_5f

    #@b8
    .line 461
    .end local v1           #e:Ljava/io/IOException;
    :catchall_b8
    move-exception v7

    #@b9
    .line 462
    :goto_b9
    if-eqz v3, :cond_be

    #@bb
    :try_start_bb
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_bf

    #@be
    .line 465
    :cond_be
    :goto_be
    throw v7

    #@bf
    .line 463
    :catch_bf
    move-exception v1

    #@c0
    .line 464
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@c3
    goto :goto_be

    #@c4
    .line 461
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catchall_c4
    move-exception v7

    #@c5
    move-object v3, v4

    #@c6
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_b9

    #@c7
    .line 458
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catch_c7
    move-exception v1

    #@c8
    move-object v3, v4

    #@c9
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_aa

    #@ca
    .line 456
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catch_ca
    move-exception v1

    #@cb
    move-object v3, v4

    #@cc
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_9b

    #@cd
    .line 410
    nop

    #@ce
    :pswitch_data_ce
    .packed-switch 0x1
        :pswitch_61
        :pswitch_f
    .end packed-switch
.end method

.method public getMatchedProfile(ILorg/xmlpull/v1/XmlPullParser;Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    .registers 25
    .parameter "type"
    .parameter "parser"
    .parameter "simInfo"

    #@0
    .prologue
    .line 271
    const/4 v5, 0x0

    #@1
    .line 272
    .local v5, commonProfile:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    const/4 v3, 0x0

    #@2
    .line 273
    .local v3, bestMatchedProfile:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    const/4 v4, 0x0

    #@3
    .line 274
    .local v4, candidateProfile:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    const/4 v7, 0x0

    #@4
    .line 275
    .local v7, defaultProfile:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    const/4 v9, 0x0

    #@5
    .line 277
    .local v9, featureProfile:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    if-nez p2, :cond_a

    #@7
    .line 278
    const/16 v18, 0x0

    #@9
    .line 390
    :goto_9
    return-object v18

    #@a
    .line 283
    :cond_a
    :try_start_a
    const-string/jumbo v18, "profiles"

    #@d
    move-object/from16 v0, p0

    #@f
    move-object/from16 v1, p2

    #@11
    move-object/from16 v2, v18

    #@13
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@16
    .line 287
    :cond_16
    :goto_16
    const-string/jumbo v18, "profiles"

    #@19
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1c
    move-result-object v19

    #@1d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v18

    #@21
    if-eqz v18, :cond_2a

    #@23
    .line 288
    move-object/from16 v0, p0

    #@25
    move-object/from16 v1, p2

    #@27
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2a
    .line 291
    :cond_2a
    const-string/jumbo v18, "profile"

    #@2d
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@30
    move-result-object v19

    #@31
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v18

    #@35
    if-eqz v18, :cond_3e

    #@37
    .line 292
    move-object/from16 v0, p0

    #@39
    move-object/from16 v1, p2

    #@3b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@3e
    .line 294
    :cond_3e
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_41
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a .. :try_end_41} :catch_a0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_41} :catch_1e0

    #@41
    move-result v18

    #@42
    const/16 v19, 0x1

    #@44
    move/from16 v0, v18

    #@46
    move/from16 v1, v19

    #@48
    if-ne v0, v1, :cond_57

    #@4a
    .line 390
    :goto_4a
    if-eqz v3, :cond_22f

    #@4c
    move-object/from16 v18, v3

    #@4e
    :goto_4e
    move-object/from16 v0, p0

    #@50
    move-object/from16 v1, v18

    #@52
    invoke-virtual {v0, v5, v1, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->mergeProfileIfNeeded(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@55
    move-result-object v18

    #@56
    goto :goto_9

    #@57
    .line 298
    :cond_57
    :try_start_57
    const-string/jumbo v18, "siminfo"

    #@5a
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@5d
    move-result-object v19

    #@5e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v18

    #@62
    if-eqz v18, :cond_1e6

    #@64
    .line 299
    const/16 v16, 0x0

    #@66
    .line 300
    .local v16, p:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    const-string/jumbo v18, "true"

    #@69
    const/16 v19, 0x0

    #@6b
    const-string v20, "default"

    #@6d
    move-object/from16 v0, p2

    #@6f
    move-object/from16 v1, v19

    #@71
    move-object/from16 v2, v20

    #@73
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@76
    move-result-object v19

    #@77
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7a
    move-result v12

    #@7b
    .line 302
    .local v12, isDefault:Z
    if-eqz v12, :cond_a5

    #@7d
    .line 304
    if-nez v7, :cond_90

    #@7f
    .line 306
    const-string v18, "TelephonyAutoProfiling"

    #@81
    const-string v19, "[getMatchedProfile] Set defaultProfile"

    #@83
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 308
    move-object/from16 v0, p0

    #@88
    move-object/from16 v1, p2

    #@8a
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->readProfile(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@8d
    move-result-object v16

    #@8e
    .line 309
    move-object/from16 v7, v16

    #@90
    .line 312
    :cond_90
    if-eqz p3, :cond_98

    #@92
    invoke-virtual/range {p3 .. p3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->isNull()Z

    #@95
    move-result v18

    #@96
    if-eqz v18, :cond_a5

    #@98
    .line 313
    :cond_98
    const-string v18, "TelephonyAutoProfiling"

    #@9a
    const-string v19, "[getMatchedProfile] sim info is null, use default profile and escape loop"

    #@9c
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_57 .. :try_end_9f} :catch_a0
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_9f} :catch_1e0

    #@9f
    goto :goto_4a

    #@a0
    .line 384
    .end local v12           #isDefault:Z
    .end local v16           #p:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    :catch_a0
    move-exception v8

    #@a1
    .line 385
    .local v8, e:Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v8}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    #@a4
    goto :goto_4a

    #@a5
    .line 318
    .end local v8           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v12       #isDefault:Z
    .restart local v16       #p:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    :cond_a5
    const/16 v18, 0x0

    #@a7
    :try_start_a7
    const-string/jumbo v19, "mcc"

    #@aa
    move-object/from16 v0, p2

    #@ac
    move-object/from16 v1, v18

    #@ae
    move-object/from16 v2, v19

    #@b0
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b3
    move-result-object v13

    #@b4
    .line 319
    .local v13, mccValue:Ljava/lang/String;
    const/16 v18, 0x0

    #@b6
    const-string/jumbo v19, "mnc"

    #@b9
    move-object/from16 v0, p2

    #@bb
    move-object/from16 v1, v18

    #@bd
    move-object/from16 v2, v19

    #@bf
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c2
    move-result-object v14

    #@c3
    .line 320
    .local v14, mncValue:Ljava/lang/String;
    const/16 v18, 0x0

    #@c5
    const-string/jumbo v19, "operator"

    #@c8
    move-object/from16 v0, p2

    #@ca
    move-object/from16 v1, v18

    #@cc
    move-object/from16 v2, v19

    #@ce
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d1
    move-result-object v15

    #@d2
    .line 321
    .local v15, operatorValue:Ljava/lang/String;
    const/16 v18, 0x0

    #@d4
    const-string v19, "country"

    #@d6
    move-object/from16 v0, p2

    #@d8
    move-object/from16 v1, v18

    #@da
    move-object/from16 v2, v19

    #@dc
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@df
    move-result-object v6

    #@e0
    .line 322
    .local v6, countryValue:Ljava/lang/String;
    const/16 v18, 0x0

    #@e2
    const-string v19, "gid"

    #@e4
    move-object/from16 v0, p2

    #@e6
    move-object/from16 v1, v18

    #@e8
    move-object/from16 v2, v19

    #@ea
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ed
    move-result-object v10

    #@ee
    .line 323
    .local v10, gidValue:Ljava/lang/String;
    const/16 v18, 0x0

    #@f0
    const-string/jumbo v19, "spn"

    #@f3
    move-object/from16 v0, p2

    #@f5
    move-object/from16 v1, v18

    #@f7
    move-object/from16 v2, v19

    #@f9
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@fc
    move-result-object v17

    #@fd
    .line 324
    .local v17, spnValue:Ljava/lang/String;
    const/16 v18, 0x0

    #@ff
    const-string v19, "imsi"

    #@101
    move-object/from16 v0, p2

    #@103
    move-object/from16 v1, v18

    #@105
    move-object/from16 v2, v19

    #@107
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10a
    move-result-object v11

    #@10b
    .line 334
    .local v11, imsiValue:Ljava/lang/String;
    move-object/from16 v0, p0

    #@10d
    move-object/from16 v1, p3

    #@10f
    invoke-virtual {v0, v1, v13, v14}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->matchMccMnc(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;Ljava/lang/String;Ljava/lang/String;)Z

    #@112
    move-result v18

    #@113
    if-eqz v18, :cond_1d5

    #@115
    .line 337
    if-nez v4, :cond_15b

    #@117
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11a
    move-result v18

    #@11b
    if-eqz v18, :cond_15b

    #@11d
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@120
    move-result v18

    #@121
    if-eqz v18, :cond_15b

    #@123
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@126
    move-result v18

    #@127
    if-eqz v18, :cond_15b

    #@129
    .line 338
    if-nez v16, :cond_159

    #@12b
    .line 340
    const-string v18, "TelephonyAutoProfiling"

    #@12d
    new-instance v19, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v20, "[getMatchedProfile] Set candidateProfile - MCC : "

    #@134
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v19

    #@138
    move-object/from16 v0, v19

    #@13a
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v19

    #@13e
    const-string v20, ", MNC : "

    #@140
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v19

    #@144
    move-object/from16 v0, v19

    #@146
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v19

    #@14a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14d
    move-result-object v19

    #@14e
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@151
    .line 342
    move-object/from16 v0, p0

    #@153
    move-object/from16 v1, p2

    #@155
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->readProfile(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@158
    move-result-object v16

    #@159
    .line 344
    :cond_159
    move-object/from16 v4, v16

    #@15b
    .line 353
    :cond_15b
    if-nez v3, :cond_1d5

    #@15d
    .line 355
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@160
    move-result v18

    #@161
    if-eqz v18, :cond_16f

    #@163
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@166
    move-result v18

    #@167
    if-eqz v18, :cond_16f

    #@169
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16c
    move-result v18

    #@16d
    if-nez v18, :cond_1d5

    #@16f
    .line 356
    :cond_16f
    move-object/from16 v0, p0

    #@171
    move-object/from16 v1, p3

    #@173
    move-object/from16 v2, v17

    #@175
    invoke-virtual {v0, v1, v10, v2, v11}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->matchExtension(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@178
    move-result v18

    #@179
    if-eqz v18, :cond_1d5

    #@17b
    .line 357
    sget-boolean v18, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->ENABLE_PRIVACY_LOG:Z

    #@17d
    if-eqz v18, :cond_1cb

    #@17f
    .line 358
    const-string v18, "TelephonyAutoProfiling"

    #@181
    new-instance v19, Ljava/lang/StringBuilder;

    #@183
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@186
    const-string v20, "[getMatchedProfile] Set bestMatchedProfile -MCC : "

    #@188
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v19

    #@18c
    move-object/from16 v0, v19

    #@18e
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    move-result-object v19

    #@192
    const-string v20, ", MNC : "

    #@194
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v19

    #@198
    move-object/from16 v0, v19

    #@19a
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v19

    #@19e
    const-string v20, ", GID : "

    #@1a0
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v19

    #@1a4
    move-object/from16 v0, v19

    #@1a6
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v19

    #@1aa
    const-string v20, ", SPN : "

    #@1ac
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v19

    #@1b0
    move-object/from16 v0, v19

    #@1b2
    move-object/from16 v1, v17

    #@1b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v19

    #@1b8
    const-string v20, ", IMSI : "

    #@1ba
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v19

    #@1be
    move-object/from16 v0, v19

    #@1c0
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v19

    #@1c4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c7
    move-result-object v19

    #@1c8
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1cb
    .line 361
    :cond_1cb
    move-object/from16 v0, p0

    #@1cd
    move-object/from16 v1, p2

    #@1cf
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->readProfile(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@1d2
    move-result-object v3

    #@1d3
    .line 362
    goto/16 :goto_4a

    #@1d5
    .line 369
    :cond_1d5
    if-nez v16, :cond_16

    #@1d7
    .line 370
    move-object/from16 v0, p0

    #@1d9
    move-object/from16 v1, p2

    #@1db
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->skipCurrentElement(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1de
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a7 .. :try_end_1de} :catch_a0
    .catch Ljava/io/IOException; {:try_start_a7 .. :try_end_1de} :catch_1e0

    #@1de
    goto/16 :goto_16

    #@1e0
    .line 386
    .end local v6           #countryValue:Ljava/lang/String;
    .end local v10           #gidValue:Ljava/lang/String;
    .end local v11           #imsiValue:Ljava/lang/String;
    .end local v12           #isDefault:Z
    .end local v13           #mccValue:Ljava/lang/String;
    .end local v14           #mncValue:Ljava/lang/String;
    .end local v15           #operatorValue:Ljava/lang/String;
    .end local v16           #p:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    .end local v17           #spnValue:Ljava/lang/String;
    :catch_1e0
    move-exception v8

    #@1e1
    .line 387
    .local v8, e:Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    #@1e4
    goto/16 :goto_4a

    #@1e6
    .line 374
    .end local v8           #e:Ljava/io/IOException;
    :cond_1e6
    :try_start_1e6
    const-string v18, "CommonProfile"

    #@1e8
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1eb
    move-result-object v19

    #@1ec
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ef
    move-result v18

    #@1f0
    if-eqz v18, :cond_1fc

    #@1f2
    .line 375
    move-object/from16 v0, p0

    #@1f4
    move-object/from16 v1, p2

    #@1f6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->readProfile(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@1f9
    move-result-object v5

    #@1fa
    goto/16 :goto_16

    #@1fc
    .line 376
    :cond_1fc
    const-string v18, "FeatureSet"

    #@1fe
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@201
    move-result-object v19

    #@202
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@205
    move-result v18

    #@206
    if-eqz v18, :cond_212

    #@208
    .line 377
    move-object/from16 v0, p0

    #@20a
    move-object/from16 v1, p2

    #@20c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->readProfile(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@20f
    move-result-object v9

    #@210
    goto/16 :goto_16

    #@212
    .line 380
    :cond_212
    new-instance v18, Lorg/xmlpull/v1/XmlPullParserException;

    #@214
    new-instance v19, Ljava/lang/StringBuilder;

    #@216
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@219
    const-string v20, "Unexpected tag: found "

    #@21b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v19

    #@21f
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@222
    move-result-object v20

    #@223
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v19

    #@227
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22a
    move-result-object v19

    #@22b
    invoke-direct/range {v18 .. v19}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@22e
    throw v18
    :try_end_22f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1e6 .. :try_end_22f} :catch_a0
    .catch Ljava/io/IOException; {:try_start_1e6 .. :try_end_22f} :catch_1e0

    #@22f
    .line 390
    :cond_22f
    if-eqz v4, :cond_235

    #@231
    move-object/from16 v18, v4

    #@233
    goto/16 :goto_4e

    #@235
    :cond_235
    move-object/from16 v18, v7

    #@237
    goto/16 :goto_4e
.end method

.method protected matchExtension(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 18
    .parameter "simInfo"
    .parameter "gidParsed"
    .parameter "spnParsed"
    .parameter "imsiParsed"

    #@0
    .prologue
    .line 185
    if-nez p1, :cond_4

    #@2
    .line 186
    const/4 v12, 0x0

    #@3
    .line 259
    :goto_3
    return v12

    #@4
    .line 189
    :cond_4
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getGid()Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 190
    .local v3, gid:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSpn()Ljava/lang/String;

    #@b
    move-result-object v9

    #@c
    .line 191
    .local v9, spn:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getImsi()Ljava/lang/String;

    #@f
    move-result-object v6

    #@10
    .line 193
    .local v6, imsi:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@13
    move-result v12

    #@14
    if-eqz v12, :cond_24

    #@16
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v12

    #@1a
    if-eqz v12, :cond_24

    #@1c
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v12

    #@20
    if-eqz v12, :cond_24

    #@22
    .line 194
    const/4 v12, 0x0

    #@23
    goto :goto_3

    #@24
    .line 201
    :cond_24
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@27
    move-result v12

    #@28
    if-nez v12, :cond_38

    #@2a
    .line 203
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@2d
    move-result v4

    #@2e
    .line 204
    .local v4, gidLength:I
    if-eqz p2, :cond_3c

    #@30
    invoke-virtual {p0, p2, v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->existInTokens(Ljava/lang/String;Ljava/lang/String;I)Z

    #@33
    move-result v12

    #@34
    if-nez v12, :cond_3c

    #@36
    .line 207
    const/4 v12, 0x0

    #@37
    goto :goto_3

    #@38
    .line 211
    .end local v4           #gidLength:I
    :cond_38
    if-eqz p2, :cond_3c

    #@3a
    .line 212
    const/4 v12, 0x0

    #@3b
    goto :goto_3

    #@3c
    .line 216
    :cond_3c
    if-eqz v9, :cond_4a

    #@3e
    .line 217
    if-eqz p3, :cond_4e

    #@40
    move-object/from16 v0, p3

    #@42
    invoke-virtual {p0, v0, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->existInTokens(Ljava/lang/String;Ljava/lang/String;)Z

    #@45
    move-result v12

    #@46
    if-nez v12, :cond_4e

    #@48
    .line 220
    const/4 v12, 0x0

    #@49
    goto :goto_3

    #@4a
    .line 224
    :cond_4a
    if-eqz p3, :cond_4e

    #@4c
    const/4 v12, 0x0

    #@4d
    goto :goto_3

    #@4e
    .line 227
    :cond_4e
    if-eqz p4, :cond_93

    #@50
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    #@53
    move-result v12

    #@54
    if-eqz v12, :cond_93

    #@56
    .line 228
    const/4 v2, 0x0

    #@57
    .line 229
    .local v2, found:Z
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@5a
    move-result v7

    #@5b
    .line 231
    .local v7, imsiLength:I
    new-instance v10, Ljava/util/StringTokenizer;

    #@5d
    const-string v12, ","

    #@5f
    move-object/from16 v0, p4

    #@61
    invoke-direct {v10, v0, v12}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 232
    .local v10, st:Ljava/util/StringTokenizer;
    :cond_64
    :goto_64
    if-nez v2, :cond_8e

    #@66
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@69
    move-result v12

    #@6a
    if-eqz v12, :cond_8e

    #@6c
    .line 233
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@6f
    move-result-object v11

    #@70
    .line 234
    .local v11, t:Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@73
    move-result v8

    #@74
    .line 236
    .local v8, len:I
    if-gt v8, v7, :cond_64

    #@76
    .line 242
    const/4 v5, 0x0

    #@77
    .local v5, i:I
    :goto_77
    if-ge v5, v8, :cond_87

    #@79
    .line 243
    invoke-virtual {v11, v5}, Ljava/lang/String;->charAt(I)C

    #@7c
    move-result v1

    #@7d
    .line 244
    .local v1, c:C
    const/16 v12, 0x78

    #@7f
    if-eq v1, v12, :cond_8b

    #@81
    invoke-virtual {v6, v5}, Ljava/lang/String;->charAt(I)C

    #@84
    move-result v12

    #@85
    if-eq v1, v12, :cond_8b

    #@87
    .line 248
    .end local v1           #c:C
    :cond_87
    if-ne v5, v8, :cond_64

    #@89
    .line 249
    const/4 v2, 0x1

    #@8a
    goto :goto_64

    #@8b
    .line 242
    .restart local v1       #c:C
    :cond_8b
    add-int/lit8 v5, v5, 0x1

    #@8d
    goto :goto_77

    #@8e
    .line 253
    .end local v1           #c:C
    .end local v5           #i:I
    .end local v8           #len:I
    .end local v11           #t:Ljava/lang/String;
    :cond_8e
    if-nez v2, :cond_93

    #@90
    const/4 v12, 0x0

    #@91
    goto/16 :goto_3

    #@93
    .line 259
    .end local v2           #found:Z
    .end local v7           #imsiLength:I
    .end local v10           #st:Ljava/util/StringTokenizer;
    :cond_93
    const/4 v12, 0x1

    #@94
    goto/16 :goto_3
.end method

.method protected matchMccMnc(Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "simInfo"
    .parameter "mccParsed"
    .parameter "mncParsed"

    #@0
    .prologue
    .line 167
    if-eqz p1, :cond_24

    #@2
    .line 168
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 169
    .local v0, mcc:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMnc()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 171
    .local v1, mnc:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_24

    #@10
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_24

    #@16
    .line 172
    invoke-virtual {p0, p2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->existInTokens(Ljava/lang/String;Ljava/lang/String;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_24

    #@1c
    invoke-virtual {p0, p3, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->existInTokens(Ljava/lang/String;Ljava/lang/String;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_24

    #@22
    .line 175
    const/4 v2, 0x1

    #@23
    .line 181
    .end local v0           #mcc:Ljava/lang/String;
    .end local v1           #mnc:Ljava/lang/String;
    :goto_23
    return v2

    #@24
    :cond_24
    const/4 v2, 0x0

    #@25
    goto :goto_23
.end method

.method protected mergeProfile(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    .registers 12
    .parameter "commonProfile"
    .parameter "matchedProfile"
    .parameter "featureProfile"

    #@0
    .prologue
    .line 563
    move-object v0, p1

    #@1
    check-cast v0, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;

    #@3
    .local v0, cp:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;
    move-object v4, p2

    #@4
    .line 564
    check-cast v4, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;

    #@6
    .local v4, mp:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;
    move-object v1, p3

    #@7
    .line 565
    check-cast v1, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;

    #@9
    .line 570
    .local v1, fp:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;
    if-eqz v0, :cond_39

    #@b
    .line 572
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->access$000(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@12
    move-result-object v5

    #@13
    .line 573
    .local v5, set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v3

    #@17
    .line 575
    .local v3, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_17
    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v6

    #@1b
    if-eqz v6, :cond_39

    #@1d
    .line 576
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v2

    #@21
    check-cast v2, Ljava/lang/String;

    #@23
    .line 577
    .local v2, key:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->access$000(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@2a
    move-result v6

    #@2b
    if-nez v6, :cond_17

    #@2d
    .line 578
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->access$000(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v6, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    goto :goto_17

    #@39
    .line 583
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v5           #set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_39
    if-eqz v1, :cond_69

    #@3b
    .line 585
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->access$000(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@42
    move-result-object v5

    #@43
    .line 586
    .restart local v5       #set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@46
    move-result-object v3

    #@47
    .line 588
    .restart local v3       #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_47
    :goto_47
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4a
    move-result v6

    #@4b
    if-eqz v6, :cond_69

    #@4d
    .line 589
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@50
    move-result-object v2

    #@51
    check-cast v2, Ljava/lang/String;

    #@53
    .line 590
    .restart local v2       #key:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->access$000(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5a
    move-result v6

    #@5b
    if-nez v6, :cond_47

    #@5d
    .line 591
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->access$000(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;)Ljava/util/HashMap;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v6, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@68
    goto :goto_47

    #@69
    .line 596
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v5           #set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_69
    return-object v4
.end method

.method protected mergeProfileIfNeeded(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    .registers 5
    .parameter "globalProfile"
    .parameter "matchedProfile"
    .parameter "featureProfile"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 104
    if-nez p1, :cond_9

    #@3
    if-nez p3, :cond_9

    #@5
    if-nez p2, :cond_9

    #@7
    move-object p1, v0

    #@8
    .line 121
    .end local p1
    :cond_8
    :goto_8
    return-object p1

    #@9
    .line 109
    .restart local p1
    :cond_9
    if-nez p2, :cond_12

    #@b
    .line 112
    if-eqz p3, :cond_8

    #@d
    .line 113
    invoke-virtual {p0, p1, p3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->mergeProfile(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@10
    move-result-object p1

    #@11
    goto :goto_8

    #@12
    .line 121
    :cond_12
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->mergeProfile(Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;

    #@15
    move-result-object p1

    #@16
    goto :goto_8
.end method

.method protected final nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 4
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@3
    move-result v0

    #@4
    .local v0, type:I
    const/4 v1, 0x2

    #@5
    if-eq v0, v1, :cond_a

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_0

    #@a
    .line 76
    :cond_a
    return-void
.end method

.method protected readProfile(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$ProfileData;
    .registers 9
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 522
    new-instance v1, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;

    #@2
    invoke-direct {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;-><init>()V

    #@5
    .line 526
    .local v1, p:Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;
    :goto_5
    const-string/jumbo v5, "siminfo"

    #@8
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@b
    move-result-object v6

    #@c
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v5

    #@10
    if-nez v5, :cond_1e

    #@12
    const-string v5, "FeatureSet"

    #@14
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_22

    #@1e
    .line 527
    :cond_1e
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@21
    goto :goto_5

    #@22
    .line 530
    :cond_22
    :goto_22
    const-string/jumbo v5, "item"

    #@25
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v5

    #@2d
    if-eqz v5, :cond_4f

    #@2f
    .line 532
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    .line 537
    .local v2, tag:Ljava/lang/String;
    const/4 v5, 0x0

    #@34
    const-string/jumbo v6, "name"

    #@37
    invoke-interface {p1, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    .line 538
    .local v0, key:Ljava/lang/String;
    if-eqz v0, :cond_4b

    #@3d
    .line 539
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@40
    move-result v3

    #@41
    .line 540
    .local v3, type:I
    const/4 v5, 0x4

    #@42
    if-ne v3, v5, :cond_4b

    #@44
    .line 541
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    .line 542
    .local v4, value:Ljava/lang/String;
    invoke-virtual {v1, v0, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser$NameValueProfile;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 548
    .end local v3           #type:I
    .end local v4           #value:Ljava/lang/String;
    :cond_4b
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@4e
    goto :goto_22

    #@4f
    .line 551
    .end local v0           #key:Ljava/lang/String;
    .end local v2           #tag:Ljava/lang/String;
    :cond_4f
    return-object v1
.end method

.method protected final skipCurrentElement(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 6
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@3
    move-result v0

    #@4
    .line 83
    .local v0, depth:I
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@7
    .line 86
    const-string/jumbo v2, "siminfo"

    #@a
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_21

    #@14
    const-string/jumbo v2, "profile"

    #@17
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_22

    #@21
    .line 100
    :cond_21
    :goto_21
    return-void

    #@22
    .line 89
    :cond_22
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@25
    move-result v1

    #@26
    .local v1, type:I
    const/4 v2, 0x1

    #@27
    if-eq v1, v2, :cond_21

    #@29
    .line 90
    const-string/jumbo v2, "item"

    #@2c
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_39

    #@36
    .line 91
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeProfileParser;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@39
    .line 96
    :cond_39
    const-string/jumbo v2, "profile"

    #@3c
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v2

    #@44
    if-eqz v2, :cond_22

    #@46
    goto :goto_21
.end method
