.class public Lcom/android/internal/telephony/LGfeature;
.super Ljava/lang/Object;
.source "LGfeature.java"


# static fields
.field public static final ATANDT:I = 0x4

.field public static CDMA_CPA:Z = false

.field public static final DCMSET:I = 0x3

.field public static final KDDI:I = 0x8

.field public static KDDI_1X_DUN:Z = false

.field public static KDDI_dipose_GsmDCT:Z = false

.field public static final KDDIbaseSet:I = 0x8

.field public static final KT:I = 0x5

.field public static LGP_DATA_APN_PREFFERED_APN_SUPPORT_NON_DEFAULT_KDDI:Z = false

.field public static LGP_DATA_DATACONNECTION_DELAY_GSMDCT_DISPOSE_WHEN_CDMADCT_RATTYPE_IN_26SEC_KDDI:Z = false

.field public static LGP_DATA_DATACONNECTION_DONOT_DEACTIVATE_DUN_TYPE_KDDI:Z = false

.field public static LGP_DATA_DATACONNECTION_KDDI_CPA_SUPPORT_ON_LEGACY_CDMA_KDDI:Z = false

.field public static LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_KDDI:Z = false

.field public static LGP_DATA_IMS_SEND_INTENT_ON_CONNECTED_KDDI:Z = false

.field public static LGP_DATA_KDDI_SUPPORT_DYNAMIC_APN_NAI_SETTING_FOR_CPA:Z = false

.field public static LGP_DATA_ROAMING_SET_ROAMING_STATUS:Z = false

.field public static LGP_DATA_SET_APN_IA_TIMER_KDDI:Z = false

.field public static LGP_DATA_TCPIP_DNS_KDDI_CPA_CONFIG_KDDI:Z = false

.field public static LGP_DATA_TCPIP_DNS_KDDI_SET_TETHERING_DNS_KDDI:Z = false

.field public static final LGUPLUS:I = 0x2

.field static final LOG_TAG:Ljava/lang/String; = "LGfeature"

.field public static final MPCS:I = 0x7

.field public static final MPDN_NOTSUPPORT:I = 0x0

.field public static final NAI_ALLSUPPORT:I = 0x0

.field public static final NAI_IntAndAppV4:I = 0x1

.field public static Prefered_APN_DUN:Z = false

.field public static final SKT:I = 0x6

.field public static final SPCS:I = 0x9

#the value of this static final field might be set in the static constructor
.field public static final SUPPORT_DEBUG:Z = false

.field public static final TMUS:I = 0xb

.field public static final VDF:I = 0xa

.field public static final VZWbaseSet:I = 0x1

.field public static defaultMinAllow:Z

.field public static mipErrorPopup:Z

.field public static omadmDataBlock:Z

.field private static sLGfeature:Lcom/android/internal/telephony/LGfeature;


# instance fields
.field public CALL_REATTACHED:Z

.field public CHECK_LW_AND_WONLY_MODE:Z

.field public DISALLOW_CHNAGING_PHONETYPE:Z

.field public DefaultPDNdependancy:Z

.field public DisableFD_OnTethering:Z

.field public DoNotReadDummyAPN:Z

.field public EMERGENCY_SUPPORT:Z

.field public FEATURE_DATA_NO_MPDP_CHECK:Z

.field public FIXED_JB_RETRYCOUNT_INIT_ERR:Z

.field public IMSPowerOffdelaytime:I

.field public INFINITE_DATA_CALL_RETRY_COUNT:Z

.field public IsPossibleSetMobileNOSVC:Z

.field public KDDI_APN_FOR_ROAMING:Z

.field public KDDI_CPA_DNS_SETTING:Z

.field public KDDI_SUSPENDED:Z

.field public KDDI_TETHERING_DNS:Z

.field public LGE_DATA_LGU_FIX_DNS_PARSING:Z

.field public LGP_DATA_APN_ADD_APN_SCENARIO_TLS:Z

.field public LGP_DATA_APN_ADD_BIP_TYPE:Z

.field public LGP_DATA_APN_ADD_DUN_TYPE:Z

.field public LGP_DATA_APN_ADD_RCS_TYPE:Z

.field public LGP_DATA_APN_APN2_ENABLE_BACKUP_RESTORE_VZW:Z

.field public LGP_DATA_APN_APNPROVISION_ATT:Z

.field public LGP_DATA_APN_APNSYNC:Z

.field public LGP_DATA_APN_APNSYNC_KR:Z

.field public LGP_DATA_APN_APNSYNC_ON_CDMA:Z

.field public LGP_DATA_APN_AUTOPROFILE_CA:Z

.field public LGP_DATA_APN_AUTOPROFILE_CA_KT:Z

.field public LGP_DATA_APN_AUTOPROFILE_KR:Z

.field public LGP_DATA_APN_AUTOPROFILE_MPDN_SKT:Z

.field public LGP_DATA_APN_AVOID_APN_DB_ERASING_ON_FACTORY_DATA_RESET_AND_HFA_SPRINT:Z

.field public LGP_DATA_APN_BACKUP:Z

.field public LGP_DATA_APN_BIP_PROFILE_UPLUS:Z

.field public LGP_DATA_APN_BLOCK_APP_REQUEST_WHEN_USER_DATA_DISABLED:Z

.field public LGP_DATA_APN_CHECK_PROFILE_DB_EXTENSION_SPRINT:Z

.field public LGP_DATA_APN_CONFIG:Z

.field public LGP_DATA_APN_CONTROL_PDN_ON_POA:Z

.field public LGP_DATA_APN_DISABLE_ESM_INFO:Z

.field public LGP_DATA_APN_DISABLE_ROAM_PROTOCOL:Z

.field public LGP_DATA_APN_EFS_PDN_LIST_ERASE:Z

.field public LGP_DATA_APN_FOTA_UPGRADE_ATT:Z

.field public LGP_DATA_APN_HANDLE_ALL_TYPE_KR:Z

.field public LGP_DATA_APN_HANDLE_SUPL_WITH_DEFAULT:Z

.field public LGP_DATA_APN_INIT_RETURN_KOR_MPDN_SKT:Z

.field public LGP_DATA_APN_INIT_RETURN_KOR__MPDN_KT:Z

.field public LGP_DATA_APN_KDDI_USE_PREFERREDDUN_APN_KDDI:Z

.field public LGP_DATA_APN_PDN_LIST_ERASE:Z

.field public LGP_DATA_APN_PREFERAPN_SETTING_DCM:Z

.field public LGP_DATA_APN_PROFILE_SETTING_KT:Z

.field public LGP_DATA_APN_PROFILE_SETTING_MPDN_UPLUS:Z

.field public LGP_DATA_APN_PROFILE_SETTING_SINGLE_PDN:Z

.field public LGP_DATA_APN_PROFILE_SETTING_SKT:Z

.field public LGP_DATA_APN_PROFILE_SETTING__MPDN_KT:Z

.field public LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

.field public LGP_DATA_APN_ROAMING_AUTOPROFILE_KT:Z

.field public LGP_DATA_APN_ROAMING_AUTOPROFILE_SKT:Z

.field public LGP_DATA_APN_SELECT_KR:Z

.field public LGP_DATA_APN_SETTING_PLMN_CHG_MODEM:Z

.field public LGP_DATA_APN_SET_AUTH_ON_USER_NULL:Z

.field public LGP_DATA_APN_SUPPORT_MPDN_SPRINT:Z

.field public LGP_DATA_APN_SUPPORT_SUPL_ON_DEFAULT_TYPE_VZW:Z

.field public LGP_DATA_APN_SYNC_PARAMETER:Z

.field public LGP_DATA_APN_SYNC_REATTACH_DEFAULT_PDN:Z

.field public LGP_DATA_APN_USER_SELECTION_SCEANARIO_EU:Z

.field public LGP_DATA_APN_USE_TELEPHONY_DB_TRANSACTION:Z

.field public LGP_DATA_APN_VZWAPNE_AT_COMMAND_VZW:Z

.field public LGP_DATA_APN_VZW_APN_RESTORE_TIME_SET_VZW:Z

.field public LGP_DATA_APN_VZW_DATA_USAGE_DEFAULT_CONFIG_VZW:Z

.field public LGP_DATA_ATCMD_BUGFIX_GCF_QOS:Z

.field public LGP_DATA_ATCMD_NO_READ_ESN:Z

.field public LGP_DATA_CHAMELEON_USE_DEFAULT_OPERATOR_SPRINT:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_ADD_RT_API_KR:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_BUGFIX_CONNSRV_EXCEPTION:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_DUN_TYPE_TIMER_SKT:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_FEATURE_USER_MEMORY_OVERFLOW:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_HIPRI_TYPE_TIMER_UPLUS:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_KAF_KT:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

.field public LGP_DATA_CONNECTIVITYSERVICE_VPN_DEF_PROXY_DCM:Z

.field public LGP_DATA_DAEMON_NETD_BANDWIDTH:Z

.field public LGP_DATA_DAEMON_NETD_BUGFIX_ILLEGALSTATE_EXCEPTION:Z

.field public LGP_DATA_DAEMON_NETMGRD_PORT_INIT_RETRY:Z

.field public LGP_DATA_DAEMON_NETMGRD_RECOVER_ON_KILLED:Z

.field public LGP_DATA_DATACONNECTION_ADD_PDN_RESET_API_SKT:Z

.field public LGP_DATA_DATACONNECTION_AIRPLANEMODE:Z

.field public LGP_DATA_DATACONNECTION_AIRPLANEMODE_DETACH:Z

.field public LGP_DATA_DATACONNECTION_ARBITRATION:Z

.field public LGP_DATA_DATACONNECTION_ATTCH_AFTER_10SEC_KR:Z

.field public LGP_DATA_DATACONNECTION_BLOCK_1XEVDO_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_BLOCK_CREATE_CDMADATACONNECTIONTRACKER:Z

.field public LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

.field public LGP_DATA_DATACONNECTION_BLOCK_SSDP_PKT_TO_MOBILE:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_DATACONNFAIL_NO_APN:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_DATACONNFAIL_NO_DC_LINK:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_DATACONNFAIL_WITH_2GCALL:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_DCT_TYPE_CHECK:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_DISCONNECTING_STATE_MISMATCH:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_NOTIFY_DATACONN_ON_STATE_FAILED:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_PHONETYPE_CHANGE:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

.field public LGP_DATA_DATACONNECTION_BUGFIX_UNSOL_PROCESS:Z

.field public LGP_DATA_DATACONNECTION_CAN_GO_DORMANT_TRUE_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_CAUSE_FIX_GET_REJCAUSE:Z

.field public LGP_DATA_DATACONNECTION_CAUSE_HANDLE_TYPE2_DETACH:Z

.field public LGP_DATA_DATACONNECTION_CIQ_TMUS:Z

.field public LGP_DATA_DATACONNECTION_CONDITION_FOR_AUTO_ATTACH:Z

.field public LGP_DATA_DATACONNECTION_DATACALL:Z

.field public LGP_DATA_DATACONNECTION_DATAENABLED_CONFIG_TLF_ES:Z

.field public LGP_DATA_DATACONNECTION_DATAROAMING_CONFIG:Z

.field public LGP_DATA_DATACONNECTION_DATASTALL_ALARM_CONNECTED_OLNY:Z

.field public LGP_DATA_DATACONNECTION_DATASTALL_ALARM_NO_WAKEUP:Z

.field public LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

.field public LGP_DATA_DATACONNECTION_EXCEPTION_HANDLING_FOR_GET_SERVICESTATE:Z

.field public LGP_DATA_DATACONNECTION_FAKE_ROAMING_APN_SETTING_KT:Z

.field public LGP_DATA_DATACONNECTION_FAKE_ROAMING_APN_SETTING_SKT:Z

.field public LGP_DATA_DATACONNECTION_FAST_CONNECT_DEFAULT_PDN_KR:Z

.field public LGP_DATA_DATACONNECTION_HANDLE_CONNECTING_DATACALL_ON_DCLISTCHANGED:Z

.field public LGP_DATA_DATACONNECTION_HANDLE_PERMANENT_CAUSE_DCM:Z

.field public LGP_DATA_DATACONNECTION_INIT_NWMODE_ON_FACTORY_RESET:Z

.field public LGP_DATA_DATACONNECTION_KEEP_ROUTE_INFO_ON_SUSPEND_VZW:Z

.field public LGP_DATA_DATACONNECTION_LCP_RETRY_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

.field public LGP_DATA_DATACONNECTION_LOCK_ORDER_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_LTE_ATTACH_ON_INSRV_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_LTE_ROAMING_KT:Z

.field public LGP_DATA_DATACONNECTION_MAINTAIN_USER_DATA_SETTING:Z

.field public LGP_DATA_DATACONNECTION_MANUALSEARCH:Z

.field public LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

.field public LGP_DATA_DATACONNECTION_MODECHANGE:Z

.field public LGP_DATA_DATACONNECTION_MODECHANGE_KT:Z

.field public LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

.field public LGP_DATA_DATACONNECTION_MODIFY_SPDN_PROCESS:Z

.field public LGP_DATA_DATACONNECTION_MULTIRAB_KT:Z

.field public LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

.field public LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

.field public LGP_DATA_DATACONNECTION_PCSCF_ON_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_PREVENT_RECONNECT:Z

.field public LGP_DATA_DATACONNECTION_PSRETRY:Z

.field public LGP_DATA_DATACONNECTION_PSRETRY_BLOCK_ANOTHER_TYPE:Z

.field public LGP_DATA_DATACONNECTION_PSRETRY_NOTAPPLIED_ON_DEFAULT_USERDATADISABLE:Z

.field public LGP_DATA_DATACONNECTION_PSRETRY_ON_SCREENON:Z

.field public LGP_DATA_DATACONNECTION_PSRETRY_SKT:Z

.field public LGP_DATA_DATACONNECTION_QOS_NOTIFY:Z

.field public LGP_DATA_DATACONNECTION_RADIOOFF_WAITINGTIME:Z

.field public LGP_DATA_DATACONNECTION_RECONNECT_AFTER_USER_PASSWORD_CHANGED:Z

.field public LGP_DATA_DATACONNECTION_RECONNECT_ON_APN_CHANGED_IN_MPDN:Z

.field public LGP_DATA_DATACONNECTION_REDEFINE_PERMANENT_CAUSE_EU:Z

.field public LGP_DATA_DATACONNECTION_REDUCE_HO_DELAY:Z

.field public LGP_DATA_DATACONNECTION_RETRYCOUNT_INIT_ERR:Z

.field public LGP_DATA_DATACONNECTION_RETRY_CONFIG_VZW:Z

.field public LGP_DATA_DATACONNECTION_RETRY_READY_APN:Z

.field public LGP_DATA_DATACONNECTION_SENDMMS_ON_DATADISABLED:Z

.field public LGP_DATA_DATACONNECTION_SENDMMS_ON_DATAROAMINGDISABLED:Z

.field public LGP_DATA_DATACONNECTION_SMCAUSE_NOTIFY:Z

.field public LGP_DATA_DATACONNECTION_SUPPORT_NSWO_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

.field public LGP_DATA_DATACONNECTION_VSNCP_RETRY_NUM_UPLUS:Z

.field public LGP_DATA_DATACONNECTION_VZWAPP_CHECK_PERMISSION_VZW:Z

.field public LGP_DATA_DATAUSAGE_CONFIG:Z

.field public LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

.field public LGP_DATA_DATAUSAGE_CONFIG_WARNING_VALUE_KT:Z

.field public LGP_DATA_DATAUSAGE_DISABLE_BACKGROUND_SKT:Z

.field public LGP_DATA_DATAUSAGE_EXCEPT_HOTSPOT_UPLUS:Z

.field public LGP_DATA_DATAUSAGE_FACTORY_RESET_KR:Z

.field public LGP_DATA_DATAUSAGE_LIMIT_EXCEED:Z

.field public LGP_DATA_DATAUSAGE_TRAFFICSTATS_EXTENSIONS_VZW:Z

.field public LGP_DATA_DATAUSAGE_WARNINGBYTE_TMUS:Z

.field public LGP_DATA_DEBUG_1xEVDO_DEBUG:Z

.field public LGP_DATA_DEBUG_DATABLOCK:Z

.field public LGP_DATA_DEBUG_DATACALL_INFO:Z

.field public LGP_DATA_DEBUG_DISPLAY_INFO:Z

.field public LGP_DATA_DEBUG_MPDN_INFO_UPLUS:Z

.field public LGP_DATA_DEBUG_RIL_CONN_HISTORY:Z

.field public LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

.field public LGP_DATA_DORMANT_FD:Z

.field public LGP_DATA_DORMANT_FD_ENHANCE_DCM:Z

.field public LGP_DATA_DORMANT_FD_LGU:Z

.field public LGP_DATA_DORMANT_FD_REL8_PCH_NETWORK:Z

.field public LGP_DATA_DORMANT_FD_VOICE_5SEC_DELAY_SKT:Z

.field public LGP_DATA_EHRPD_FIX_PDN_TYPE_MATCH:Z

.field public LGP_DATA_EHRPD_INIT_EFS_CONFIG_FILE_UPLUS:Z

.field public LGP_DATA_EHRPD_UPDATE_PROFILE_DB:Z

.field public LGP_DATA_IMS_BARRING_UPLUS:Z

.field public LGP_DATA_IMS_BLOCK_IMS_CONNECTION_TRY_FOR_15MIN_WHEN_CONNECT_FAIL:Z

.field public LGP_DATA_IMS_BUGFIX_SIO_PORT_RELEASE_KT:Z

.field public LGP_DATA_IMS_CELL_INFO:Z

.field public LGP_DATA_IMS_DATA_MENU_NOT_CONRTOL:Z

.field public LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

.field public LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

.field public LGP_DATA_IMS_DEREGISTRATION:Z

.field public LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_VZW:Z

.field public LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

.field public LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

.field public LGP_DATA_IMS_KEEP_INFO_ON_RAT_CHANGE:Z

.field public LGP_DATA_IMS_KR:Z

.field public LGP_DATA_IMS_PEND_STARTING_TIME:Z

.field public LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

.field public LGP_DATA_KERNEL_BUGFIX_ROUTE:Z

.field public LGP_DATA_KERNEL_CONFIG_CTS_FIX:Z

.field public LGP_DATA_LTE_ROAMING_LGU:Z

.field public LGP_DATA_NV_POWER_UP_INIT:Z

.field public LGP_DATA_PATCH_DORMANT_FEATURE_DATA_NO_MPDP_CHECK:Z

.field public LGP_DATA_PATCH_GOOGLE_ENCRYPTION_FIX:Z

.field public LGP_DATA_PATCH_SUSPEND_BUG_FIX_SIM_LOCK_WRC:Z

.field public LGP_DATA_PDN_ARBITRATION_CONFIG:Z

.field public LGP_DATA_PDN_CHECK_DATACALL_VALID:Z

.field public LGP_DATA_PDN_DISABLE_MULTI_PDN_DCM:Z

.field public LGP_DATA_PDN_EMERGENCY_CALL:Z

.field public LGP_DATA_PDN_HO_TAU_REJECT_UPLUS:Z

.field public LGP_DATA_PDN_LTE_ATTACH_RETRY_DCM:Z

.field public LGP_DATA_PDN_LTE_FIRST_IS_IMS:Z

.field public LGP_DATA_PDN_MEAN_TPUT_TLS:Z

.field public LGP_DATA_PDN_MODIFY_EPS_BEARER_REJECT:Z

.field public LGP_DATA_PDN_MPDN_KDDI:Z

.field public LGP_DATA_PDN_MPDN_KR:Z

.field public LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

.field public LGP_DATA_PDN_MPDN_VZW:Z

.field public LGP_DATA_PDN_PCO_CONFIG_DCM:Z

.field public LGP_DATA_PDN_QMI_WDS_CONNECTED_STATE_MISMATCH_FIX:Z

.field public LGP_DATA_PDN_QOS_CONFIG_KT:Z

.field public LGP_DATA_PDN_RECONN_NOT_ALLOWED_VZW:Z

.field public LGP_DATA_PDN_REJECT_INTENT_UPLUS:Z

.field public LGP_DATA_PDN_TRYSETUP_AFTER_SIM_LOADED_ON_CDMA:Z

.field public LGP_DATA_PPP_LCP_VENDOR_SPECIFIC_PROTOCOL:Z

.field public LGP_DATA_PPP_LINK_CLOSE:Z

.field public LGP_DATA_RIL_ACCESSCTRL_FIX:Z

.field public LGP_DATA_RIL_BUGFIX_FD_KT:Z

.field public LGP_DATA_RIL_BUGFIX_NETWORKINFO_NULL:Z

.field public LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

.field public LGP_DATA_RIL_DISABLE_PATIALRETRY:Z

.field public LGP_DATA_RIL_RESTART_ON_DATASTALL:Z

.field public LGP_DATA_RIL_RESTART_ON_RILERROR:Z

.field public LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

.field public LGP_DATA_TCPIP_DHCP_OPTION_CONFIG_KT:Z

.field public LGP_DATA_TCPIP_DNS_AVOID_UNEXPECTED_QUERY:Z

.field public LGP_DATA_TCPIP_DNS_BUGFIX_MDNSD_MEMORY_ERROR_KR:Z

.field public LGP_DATA_TCPIP_DNS_CACHE_FIX_ABOUT_IPV6_VZW:Z

.field public LGP_DATA_TCPIP_DNS_FIX_NO_DNSSERVER:Z

.field public LGP_DATA_TCPIP_DNS_MPDN:Z

.field public LGP_DATA_TCPIP_DNS_QUERY_SELECTION_UPLUS:Z

.field public LGP_DATA_TCPIP_DNS_RECORD_TYPE:Z

.field public LGP_DATA_TCPIP_DNS_RETRANSMISSION_ATT:Z

.field public LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

.field public LGP_DATA_TCPIP_IMS_PDN_IPV6:Z

.field public LGP_DATA_TCPIP_IP_MULTICAST:Z

.field public LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

.field public LGP_DATA_TCPIP_IP_V6_SOCK_CLOSE:Z

.field public LGP_DATA_TCPIP_MTU_CONFIG:Z

.field public LGP_DATA_TCPIP_MTU_HOOKING_ON_TETHER:Z

.field public LGP_DATA_TCPIP_MTU_SET_ROAMING_NETWORK:Z

.field public LGP_DATA_TCPIP_SET_DEFAULT_MTU_VZW:Z

.field public LGP_DATA_TCPIP_SET_MTU_SIZE_VZW:Z

.field public LGP_DATA_TCPIP_TCP_BUFSIZE_DCM:Z

.field public LGP_DATA_TCPIP_TCP_BUFSIZE_KT:Z

.field public LGP_DATA_TCPIP_TCP_BUFSIZE_ON_RAT_CHANGE_UPLUS:Z

.field public LGP_DATA_TCPIP_TCP_BUFSIZE_SKT:Z

.field public LGP_DATA_TCPIP_TCP_BUFSIZE_TMUS:Z

.field public LGP_DATA_TCPIP_TCP_MODEM_DEF_RCVBUF_DCM:Z

.field public LGP_DATA_TCPIP_TCP_SOCKET_CONN_IN_OOS_DCM:Z

.field public LGP_DATA_TCPIP_TCP_SYN_RETRY_CONFIG_UPLUS:Z

.field public LGP_DATA_TCPIP_TCP_TYPE_BIG_KT:Z

.field public LGP_DATA_TCPIP_TCP_TYPE_BIG_SKT:Z

.field public LGP_DATA_TD_DEAD_OBJECT_EXCEPTION_IN_TRAFFIC_STATS:Z

.field public LGP_DATA_TETHERING_APN_LIST:Z

.field public LGP_DATA_TETHER_APN_CHANGE_DCM:Z

.field public LGP_DATA_TETHER_APN_CHANGE_FOR_MDM_DCM:Z

.field public LGP_DATA_TETHER_BLOCK_TETHERINGPDN_ON_GSMROAMING_KDDI:Z

.field public LGP_DATA_TETHER_BUGFIX_CHECK_BT_STATUS:Z

.field public LGP_DATA_TETHER_BUGFIX_CHECK_WIFI_STATUS:Z

.field public LGP_DATA_TETHER_BUGFIX_INFINTE_RETRY_ON_DISABLE_DATA:Z

.field public LGP_DATA_TETHER_BUGFIX_UPSTREM_TYPE:Z

.field public LGP_DATA_TETHER_CHANGE_UPSTREM_TYPE:Z

.field public LGP_DATA_TETHER_DISABLE_FETCHDUN:Z

.field public LGP_DATA_TETHER_DUN_NV_ONOFF_UPLUS:Z

.field public LGP_DATA_TETHER_FIX_ROUTE_TABLE_EXCEPTION:Z

.field public LGP_DATA_TETHER_IPV6_SUPPORT:Z

.field public LGP_DATA_TETHER_RETRY_UPLUS:Z

.field public LGP_DATA_TOOL_DATA_BLOCK_HIDDEN_MENU:Z

.field public LGP_DATA_TOOL_EMUL_RIL:Z

.field public LGP_DATA_TOOL_PING6:Z

.field public LGP_DATA_TOOL_TCPDUMP:Z

.field public LGP_DATA_TOOL_TRACEROUTE:Z

.field public LGP_DATA_UIAPP_BACKGROUND_DATA_NOTI_IN_AIRPLANE_UPLUS:Z

.field public LGP_DATA_UIAPP_BLOCK_PAYPOPUP_AND_TRYSETUP:Z

.field public LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

.field public LGP_DATA_UIAPP_DSAC_NOTIFICATION_DCM:Z

.field public LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

.field public LGP_DATA_UIAPP_GPRS_REJECTED_SKT:Z

.field public LGP_DATA_UIAPP_PAYPOPUP_KR:Z

.field public LGP_DATA_UIAPP_PAYPOPUP_KT:Z

.field public LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KR:Z

.field public LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KT:Z

.field public LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

.field public LGP_DATA_UIAPP_PAYPOPUP_SKT:Z

.field public LGP_DATA_UIAPP_ROAMING_NOTI_DCM:Z

.field public LGP_DATA_UIAPP_ROAMING_POPUP_TMUS:Z

.field public LGP_DATA_UIAPP_SEND_DATA_ROAM_POPUP_INTENT_VZW:Z

.field public LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

.field public LGP_DATA_UIAPP_TOAST_ON_WIFI_OFF_UPLUS:Z

.field public LGP_DATA_VOICEPROTECTION_ATT:Z

.field public LGP_GSM_GLOBAL_PREFERED_APN_SPR:Z

.field public LTE_ROAMING_FOR_KT:Z

.field public MPDNset:I

.field public NAI_support:I

.field public PS_ATTACH_FOR_2_DETACH:Z

.field public RESETTING_FD_FOR_HO:Z

.field public SUPPORT_BLOCK_UNNECESSARY_DISCONNECT:Z

.field public SUPPORT_DEFAULT_PREFER_APN:Z

.field public SUPPORT_DOCOMO_DSAC:Z

.field public SUPPORT_DOCOMO_DUAL_CONNECTIVITY:Z

.field public SUPPORT_DOCOMO_TETHER:Z

.field public SUPPORT_IPV6:Z

.field public SUPPORT_REATTACH_DEFAULT_PDN:Z

.field public SUPPORT_RETAIN_SOCKET_CONN_IN_OOS:Z

.field public SUPPORT_RETRY_READY_APN:Z

.field public SUPPORT_ROAMING_LOCK:Z

.field public SUPPORT_ROAMING_NOTIFICATION:Z

.field public SUPPORT_SINGLE_PDN:Z

.field public SUPPORT_SPR_MTU_CHANGE:Z

.field public SUPPORT_TETHERING_NON_APN:Z

.field public SUPPORT_UPDATE_TCP_BUF:Z

.field public SUPPORT_VPN_DEFAULTPROXY:Z

.field public SupportRCSe:Z

.field public USE_ORIGIN_STALLALARM:Z

.field public VZW_GFIT_IP_address_utilization:Z

.field public apn_sync_using_sequence_pdn_list:Z

.field public apnbackup:Z

.field public bypassforuplus:Z

.field public cdmaTimer:I

.field public closeDelayForIms:Z

.field public default_ESN:Z

.field public fixthephonetypetoCDMA:Z

.field public imsstarttiming:Z

.field public lg_data_sprint_oamdm_provisioning:Z

.field public myfeatureset:Ljava/lang/String;

.field public pam_Connection_retry_error_pop_up:Z

.field public poweroffdelayneed:Z

.field public regenFdInHandover:Z

.field public retry_interval:I

.field public rxtx_debug:Z

.field public usage_data_warning:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2262
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_KDDI_SUPPORT_DYNAMIC_APN_NAI_SETTING_FOR_CPA:Z

    #@3
    .line 2273
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DELAY_GSMDCT_DISPOSE_WHEN_CDMADCT_RATTYPE_IN_26SEC_KDDI:Z

    #@5
    .line 2282
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_INTENT_ON_CONNECTED_KDDI:Z

    #@7
    .line 2291
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DONOT_DEACTIVATE_DUN_TYPE_KDDI:Z

    #@9
    .line 2300
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_SET_APN_IA_TIMER_KDDI:Z

    #@b
    .line 2310
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_KDDI:Z

    #@d
    .line 2318
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_KDDI_CPA_CONFIG_KDDI:Z

    #@f
    .line 2327
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_KDDI_SET_TETHERING_DNS_KDDI:Z

    #@11
    .line 2336
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PREFFERED_APN_SUPPORT_NON_DEFAULT_KDDI:Z

    #@13
    .line 2345
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_KDDI_CPA_SUPPORT_ON_LEGACY_CDMA_KDDI:Z

    #@15
    .line 2353
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_ROAMING_SET_ROAMING_STATUS:Z

    #@17
    .line 2591
    const-string/jumbo v0, "persist.lge.data.debug"

    #@1a
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@1d
    move-result v0

    #@1e
    sput-boolean v0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_DEBUG:Z

    #@20
    .line 2649
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->defaultMinAllow:Z

    #@22
    .line 2650
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->omadmDataBlock:Z

    #@24
    .line 2651
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->mipErrorPopup:Z

    #@26
    .line 2745
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->CDMA_CPA:Z

    #@28
    .line 2746
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->Prefered_APN_DUN:Z

    #@2a
    .line 2747
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->KDDI_1X_DUN:Z

    #@2c
    .line 2748
    sput-boolean v1, Lcom/android/internal/telephony/LGfeature;->KDDI_dipose_GsmDCT:Z

    #@2e
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "featureset"

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    const/4 v6, 0x2

    #@2
    const/16 v5, 0xbb8

    #@4
    const/4 v4, 0x0

    #@5
    const/4 v3, 0x1

    #@6
    .line 2804
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 40
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC:Z

    #@b
    .line 49
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_CONFIG:Z

    #@d
    .line 53
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_DISABLE_ROAM_PROTOCOL:Z

    #@f
    .line 62
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_HANDLE_SUPL_WITH_DEFAULT:Z

    #@11
    .line 75
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SET_AUTH_ON_USER_NULL:Z

    #@13
    .line 86
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_USE_TELEPHONY_DB_TRANSACTION:Z

    #@15
    .line 95
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_ATCMD_BUGFIX_GCF_QOS:Z

    #@17
    .line 104
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_BUGFIX_CONNSRV_EXCEPTION:Z

    #@19
    .line 115
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_FEATURE_USER_MEMORY_OVERFLOW:Z

    #@1b
    .line 124
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG:Z

    #@1d
    .line 134
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_DATACONNFAIL_NO_APN:Z

    #@1f
    .line 144
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_DATACONNFAIL_NO_DC_LINK:Z

    #@21
    .line 155
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_DATACONNFAIL_WITH_2GCALL:Z

    #@23
    .line 166
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_DISCONNECTING_STATE_MISMATCH:Z

    #@25
    .line 176
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_NOTIFY_DATACONN_ON_STATE_FAILED:Z

    #@27
    .line 188
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_PHONETYPE_CHANGE:Z

    #@29
    .line 199
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@2b
    .line 208
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_UNSOL_PROCESS:Z

    #@2d
    .line 220
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_CAUSE_FIX_GET_REJCAUSE:Z

    #@2f
    .line 229
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_CAUSE_HANDLE_TYPE2_DETACH:Z

    #@31
    .line 239
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DATAROAMING_CONFIG:Z

    #@33
    .line 249
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DATASTALL_ALARM_CONNECTED_OLNY:Z

    #@35
    .line 257
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DATASTALL_ALARM_NO_WAKEUP:Z

    #@37
    .line 266
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_INIT_NWMODE_ON_FACTORY_RESET:Z

    #@39
    .line 275
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODIFY_SPDN_PROCESS:Z

    #@3b
    .line 285
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_ON_SCREENON:Z

    #@3d
    .line 296
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RADIOOFF_WAITINGTIME:Z

    #@3f
    .line 304
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RECONNECT_ON_APN_CHANGED_IN_MPDN:Z

    #@41
    .line 313
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_DATABLOCK:Z

    #@43
    .line 322
    const-string/jumbo v0, "persist.lg.data.debug"

    #@46
    invoke-static {v0, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@49
    move-result v0

    #@4a
    iput-boolean v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_DATACALL_INFO:Z

    #@4c
    .line 330
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_DISPLAY_INFO:Z

    #@4e
    .line 338
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KEEP_INFO_ON_RAT_CHANGE:Z

    #@50
    .line 349
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_FIX_NO_DNSSERVER:Z

    #@52
    .line 362
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_RECORD_TYPE:Z

    #@54
    .line 372
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD:Z

    #@56
    .line 381
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_KERNEL_BUGFIX_ROUTE:Z

    #@58
    .line 390
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PATCH_GOOGLE_ENCRYPTION_FIX:Z

    #@5a
    .line 392
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_MTU_HOOKING_ON_TETHER:Z

    #@5c
    .line 400
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_MTU_SET_ROAMING_NETWORK:Z

    #@5e
    .line 408
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DAEMON_NETD_BANDWIDTH:Z

    #@60
    .line 416
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DAEMON_NETMGRD_PORT_INIT_RETRY:Z

    #@62
    .line 425
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DAEMON_NETMGRD_RECOVER_ON_KILLED:Z

    #@64
    .line 433
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_ACCESSCTRL_FIX:Z

    #@66
    .line 443
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_BUGFIX_NETWORKINFO_NULL:Z

    #@68
    .line 454
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DISABLE_PATIALRETRY:Z

    #@6a
    .line 465
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_DATASTALL:Z

    #@6c
    .line 473
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BUGFIX_CHECK_BT_STATUS:Z

    #@6e
    .line 481
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BUGFIX_CHECK_WIFI_STATUS:Z

    #@70
    .line 489
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BUGFIX_INFINTE_RETRY_ON_DISABLE_DATA:Z

    #@72
    .line 497
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BUGFIX_UPSTREM_TYPE:Z

    #@74
    .line 505
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_CHANGE_UPSTREM_TYPE:Z

    #@76
    .line 513
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_FIX_ROUTE_TABLE_EXCEPTION:Z

    #@78
    .line 521
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_ADD_DUN_TYPE:Z

    #@7a
    .line 530
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_ADD_BIP_TYPE:Z

    #@7c
    .line 539
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TOOL_PING6:Z

    #@7e
    .line 547
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TOOL_TCPDUMP:Z

    #@80
    .line 555
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TOOL_TRACEROUTE:Z

    #@82
    .line 564
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DATA_MENU_NOT_CONRTOL:Z

    #@84
    .line 572
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RETRYCOUNT_INIT_ERR:Z

    #@86
    .line 580
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_AIRPLANEMODE:Z

    #@88
    .line 588
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_AIRPLANEMODE_DETACH:Z

    #@8a
    .line 596
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY:Z

    #@8c
    .line 604
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_SOCK_CLOSE:Z

    #@8e
    .line 612
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DAEMON_NETD_BUGFIX_ILLEGALSTATE_EXCEPTION:Z

    #@90
    .line 621
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PDN_LIST_ERASE:Z

    #@92
    .line 624
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TOOL_EMUL_RIL:Z

    #@94
    .line 632
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TD_DEAD_OBJECT_EXCEPTION_IN_TRAFFIC_STATS:Z

    #@96
    .line 640
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_MULTICAST:Z

    #@98
    .line 654
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_KERNEL_CONFIG_CTS_FIX:Z

    #@9a
    .line 664
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_MTU_CONFIG:Z

    #@9c
    .line 673
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_CONDITION_FOR_AUTO_ATTACH:Z

    #@9e
    .line 682
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_LIMIT_EXCEED:Z

    #@a0
    .line 690
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_EXCEPTION_HANDLING_FOR_GET_SERVICESTATE:Z

    #@a2
    .line 698
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RECONNECT_AFTER_USER_PASSWORD_CHANGED:Z

    #@a4
    .line 707
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_SSDP_PKT_TO_MOBILE:Z

    #@a6
    .line 722
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_DISABLE_FETCHDUN:Z

    #@a8
    .line 735
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@aa
    .line 736
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@ac
    .line 746
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@ae
    .line 756
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@b0
    .line 764
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_CREATE_CDMADATACONNECTIONTRACKER:Z

    #@b2
    .line 771
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_RIL_CONN_HISTORY:Z

    #@b4
    .line 772
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@b6
    .line 773
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@b8
    .line 775
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_PEND_STARTING_TIME:Z

    #@ba
    .line 777
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MAINTAIN_USER_DATA_SETTING:Z

    #@bc
    .line 785
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@be
    .line 797
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_QOS_NOTIFY:Z

    #@c0
    .line 805
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SMCAUSE_NOTIFY:Z

    #@c2
    .line 813
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_GSM_GLOBAL_PREFERED_APN_SPR:Z

    #@c4
    .line 822
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_DISABLE_ESM_INFO:Z

    #@c6
    .line 830
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@c8
    .line 839
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DEREGISTRATION:Z

    #@ca
    .line 841
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MANUALSEARCH:Z

    #@cc
    .line 843
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@ce
    .line 846
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@d0
    .line 854
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_ON_CDMA:Z

    #@d2
    .line 855
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@d4
    .line 863
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@d6
    .line 871
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_BLOCK_IMS_CONNECTION_TRY_FOR_15MIN_WHEN_CONNECT_FAIL:Z

    #@d8
    .line 879
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@da
    .line 887
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@dc
    .line 895
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHERING_APN_LIST:Z

    #@de
    .line 904
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_BLOCK_APP_REQUEST_WHEN_USER_DATA_DISABLED:Z

    #@e0
    .line 913
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_HANDLE_CONNECTING_DATACALL_ON_DCLISTCHANGED:Z

    #@e2
    .line 923
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_TRYSETUP_AFTER_SIM_LOADED_ON_CDMA:Z

    #@e4
    .line 932
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@e6
    .line 941
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TOOL_DATA_BLOCK_HIDDEN_MENU:Z

    #@e8
    .line 949
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_DCT_TYPE_CHECK:Z

    #@ea
    .line 958
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_AVOID_UNEXPECTED_QUERY:Z

    #@ec
    .line 974
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_RETRANSMISSION_ATT:Z

    #@ee
    .line 988
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_BACKUP:Z

    #@f0
    .line 998
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_VOICEPROTECTION_ATT:Z

    #@f2
    .line 1006
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_FOTA_UPGRADE_ATT:Z

    #@f4
    .line 1014
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNPROVISION_ATT:Z

    #@f6
    .line 1026
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_CIQ_TMUS:Z

    #@f8
    .line 1037
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_BUFSIZE_TMUS:Z

    #@fa
    .line 1045
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_WARNINGBYTE_TMUS:Z

    #@fc
    .line 1053
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_ROAMING_POPUP_TMUS:Z

    #@fe
    .line 1062
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_CA:Z

    #@100
    .line 1074
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MEAN_TPUT_TLS:Z

    #@102
    .line 1082
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_ADD_APN_SCENARIO_TLS:Z

    #@104
    .line 1094
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_VZW:Z

    #@106
    .line 1102
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

    #@108
    .line 1109
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_KEEP_ROUTE_INFO_ON_SUSPEND_VZW:Z

    #@10a
    .line 1117
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_VZW:Z

    #@10c
    .line 1124
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RETRY_CONFIG_VZW:Z

    #@10e
    .line 1127
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

    #@110
    .line 1135
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SEND_DATA_ROAM_POPUP_INTENT_VZW:Z

    #@112
    .line 1142
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_TRAFFICSTATS_EXTENSIONS_VZW:Z

    #@114
    .line 1149
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_CACHE_FIX_ABOUT_IPV6_VZW:Z

    #@116
    .line 1158
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_SET_MTU_SIZE_VZW:Z

    #@118
    .line 1166
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_VZW_DATA_USAGE_DEFAULT_CONFIG_VZW:Z

    #@11a
    .line 1174
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APN2_ENABLE_BACKUP_RESTORE_VZW:Z

    #@11c
    .line 1182
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VZWAPP_CHECK_PERMISSION_VZW:Z

    #@11e
    .line 1190
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_VZW_APN_RESTORE_TIME_SET_VZW:Z

    #@120
    .line 1198
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_VZWAPNE_AT_COMMAND_VZW:Z

    #@122
    .line 1206
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_SET_DEFAULT_MTU_VZW:Z

    #@124
    .line 1214
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SUPPORT_SUPL_ON_DEFAULT_TYPE_VZW:Z

    #@126
    .line 1223
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_RECONN_NOT_ALLOWED_VZW:Z

    #@128
    .line 1235
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CHAMELEON_USE_DEFAULT_OPERATOR_SPRINT:Z

    #@12a
    .line 1245
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AVOID_APN_DB_ERASING_ON_FACTORY_DATA_RESET_AND_HFA_SPRINT:Z

    #@12c
    .line 1256
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_CHECK_PROFILE_DB_EXTENSION_SPRINT:Z

    #@12e
    .line 1273
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_CONTROL_PDN_ON_POA:Z

    #@130
    .line 1286
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SUPPORT_MPDN_SPRINT:Z

    #@132
    .line 1307
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@134
    .line 1331
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_NOTAPPLIED_ON_DEFAULT_USERDATADISABLE:Z

    #@136
    .line 1332
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_ATTCH_AFTER_10SEC_KR:Z

    #@138
    .line 1344
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@13a
    .line 1354
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@13c
    .line 1364
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_FACTORY_RESET_KR:Z

    #@13e
    .line 1376
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_FAST_CONNECT_DEFAULT_PDN_KR:Z

    #@140
    .line 1384
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

    #@142
    .line 1392
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@144
    .line 1405
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@146
    .line 1414
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_BUGFIX_MDNSD_MEMORY_ERROR_KR:Z

    #@148
    .line 1422
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KR:Z

    #@14a
    .line 1432
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@14c
    .line 1450
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_MPDN:Z

    #@14e
    .line 1466
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KR:Z

    #@150
    .line 1474
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KR:Z

    #@152
    .line 1482
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_KR:Z

    #@154
    .line 1490
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_HANDLE_ALL_TYPE_KR:Z

    #@156
    .line 1500
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_ADD_RT_API_KR:Z

    #@158
    .line 1508
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_AND_TRYSETUP:Z

    #@15a
    .line 1516
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

    #@15c
    .line 1524
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

    #@15e
    .line 1535
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@160
    .line 1548
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_REL8_PCH_NETWORK:Z

    #@162
    .line 1565
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MODIFY_EPS_BEARER_REJECT:Z

    #@164
    .line 1566
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SYNC_PARAMETER:Z

    #@166
    .line 1579
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_DUN_TYPE_TIMER_SKT:Z

    #@168
    .line 1587
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_DISABLE_BACKGROUND_SKT:Z

    #@16a
    .line 1595
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_ADD_PDN_RESET_API_SKT:Z

    #@16c
    .line 1603
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@16e
    .line 1611
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_SKT:Z

    #@170
    .line 1627
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_VOICE_5SEC_DELAY_SKT:Z

    #@172
    .line 1635
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_BUFSIZE_SKT:Z

    #@174
    .line 1643
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_TYPE_BIG_SKT:Z

    #@176
    .line 1651
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_SKT:Z

    #@178
    .line 1659
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@17a
    .line 1667
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_SKT:Z

    #@17c
    .line 1680
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_INIT_RETURN_KOR_MPDN_SKT:Z

    #@17e
    .line 1681
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING_SKT:Z

    #@180
    .line 1682
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_FAKE_ROAMING_APN_SETTING_SKT:Z

    #@182
    .line 1683
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_ATCMD_NO_READ_ESN:Z

    #@184
    .line 1684
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_CHECK_DATACALL_VALID:Z

    #@186
    .line 1685
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING_SINGLE_PDN:Z

    #@188
    .line 1686
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SYNC_REATTACH_DEFAULT_PDN:Z

    #@18a
    .line 1687
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SETTING_PLMN_CHG_MODEM:Z

    #@18c
    .line 1688
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_EFS_PDN_LIST_ERASE:Z

    #@18e
    .line 1689
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PATCH_DORMANT_FEATURE_DATA_NO_MPDP_CHECK:Z

    #@190
    .line 1690
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PATCH_SUSPEND_BUG_FIX_SIM_LOCK_WRC:Z

    #@192
    .line 1691
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_QMI_WDS_CONNECTED_STATE_MISMATCH_FIX:Z

    #@194
    .line 1693
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->FEATURE_DATA_NO_MPDP_CHECK:Z

    #@196
    .line 1695
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_MPDN_SKT:Z

    #@198
    .line 1696
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_ROAMING_AUTOPROFILE_SKT:Z

    #@19a
    .line 1708
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_WARNING_VALUE_KT:Z

    #@19c
    .line 1719
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_KAF_KT:Z

    #@19e
    .line 1726
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LTE_ROAMING_KT:Z

    #@1a0
    .line 1733
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE_KT:Z

    #@1a2
    .line 1740
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MULTIRAB_KT:Z

    #@1a4
    .line 1747
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DHCP_OPTION_CONFIG_KT:Z

    #@1a6
    .line 1754
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_BUGFIX_SIO_PORT_RELEASE_KT:Z

    #@1a8
    .line 1761
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_QOS_CONFIG_KT:Z

    #@1aa
    .line 1768
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_BUGFIX_FD_KT:Z

    #@1ac
    .line 1775
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_BUFSIZE_KT:Z

    #@1ae
    .line 1782
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_TYPE_BIG_KT:Z

    #@1b0
    .line 1789
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@1b2
    .line 1797
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KT:Z

    #@1b4
    .line 1805
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KT:Z

    #@1b6
    .line 1815
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING__MPDN_KT:Z

    #@1b8
    .line 1822
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_INIT_RETURN_KOR__MPDN_KT:Z

    #@1ba
    .line 1829
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING_KT:Z

    #@1bc
    .line 1836
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_ROAMING_AUTOPROFILE_KT:Z

    #@1be
    .line 1843
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_FAKE_ROAMING_APN_SETTING_KT:Z

    #@1c0
    .line 1852
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_CA_KT:Z

    #@1c2
    .line 1871
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_ARBITRATION_CONFIG:Z

    #@1c4
    .line 1879
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_HIPRI_TYPE_TIMER_UPLUS:Z

    #@1c6
    .line 1887
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_EXCEPT_HOTSPOT_UPLUS:Z

    #@1c8
    .line 1895
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_1XEVDO_UPLUS:Z

    #@1ca
    .line 1903
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LOCK_ORDER_UPLUS:Z

    #@1cc
    .line 1911
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_ON_UPLUS:Z

    #@1ce
    .line 1919
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_MPDN_INFO_UPLUS:Z

    #@1d0
    .line 1927
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_QUERY_SELECTION_UPLUS:Z

    #@1d2
    .line 1935
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_DUN_NV_ONOFF_UPLUS:Z

    #@1d4
    .line 1943
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@1d6
    .line 1951
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_REJECT_INTENT_UPLUS:Z

    #@1d8
    .line 1959
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_BUFSIZE_ON_RAT_CHANGE_UPLUS:Z

    #@1da
    .line 1967
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_SYN_RETRY_CONFIG_UPLUS:Z

    #@1dc
    .line 1975
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_RETRY_UPLUS:Z

    #@1de
    .line 1983
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_TOAST_ON_WIFI_OFF_UPLUS:Z

    #@1e0
    .line 1991
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SUPPORT_NSWO_UPLUS:Z

    #@1e2
    .line 2001
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BACKGROUND_DATA_NOTI_IN_AIRPLANE_UPLUS:Z

    #@1e4
    .line 2011
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_LTE_ROAMING_LGU:Z

    #@1e6
    .line 2014
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_LGU:Z

    #@1e8
    .line 2017
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_EHRPD_INIT_EFS_CONFIG_FILE_UPLUS:Z

    #@1ea
    .line 2018
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_BARRING_UPLUS:Z

    #@1ec
    .line 2019
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_BIP_PROFILE_UPLUS:Z

    #@1ee
    .line 2020
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_CAN_GO_DORMANT_TRUE_UPLUS:Z

    #@1f0
    .line 2021
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PPP_LCP_VENDOR_SPECIFIC_PROTOCOL:Z

    #@1f2
    .line 2022
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LTE_ATTACH_ON_INSRV_UPLUS:Z

    #@1f4
    .line 2023
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VSNCP_RETRY_NUM_UPLUS:Z

    #@1f6
    .line 2026
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_REDUCE_HO_DELAY:Z

    #@1f8
    .line 2027
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LCP_RETRY_UPLUS:Z

    #@1fa
    .line 2028
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_HO_TAU_REJECT_UPLUS:Z

    #@1fc
    .line 2029
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IMS_PDN_IPV6:Z

    #@1fe
    .line 2030
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_EMERGENCY_CALL:Z

    #@200
    .line 2031
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING_MPDN_UPLUS:Z

    #@202
    .line 2034
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_NV_POWER_UP_INIT:Z

    #@204
    .line 2035
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DATACALL:Z

    #@206
    .line 2036
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PPP_LINK_CLOSE:Z

    #@208
    .line 2037
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_1xEVDO_DEBUG:Z

    #@20a
    .line 2038
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_EHRPD_UPDATE_PROFILE_DB:Z

    #@20c
    .line 2039
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_EHRPD_FIX_PDN_TYPE_MATCH:Z

    #@20e
    .line 2040
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGE_DATA_LGU_FIX_DNS_PARSING:Z

    #@210
    .line 2045
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_LTE_FIRST_IS_IMS:Z

    #@212
    .line 2058
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PREFERAPN_SETTING_DCM:Z

    #@214
    .line 2071
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_VPN_DEF_PROXY_DCM:Z

    #@216
    .line 2080
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@218
    .line 2088
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_HANDLE_PERMANENT_CAUSE_DCM:Z

    #@21a
    .line 2097
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_ENHANCE_DCM:Z

    #@21c
    .line 2106
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_SOCKET_CONN_IN_OOS_DCM:Z

    #@21e
    .line 2117
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_APN_CHANGE_DCM:Z

    #@220
    .line 2126
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_APN_CHANGE_FOR_MDM_DCM:Z

    #@222
    .line 2134
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_DSAC_NOTIFICATION_DCM:Z

    #@224
    .line 2142
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_ROAMING_NOTI_DCM:Z

    #@226
    .line 2151
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PREVENT_RECONNECT:Z

    #@228
    .line 2161
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RETRY_READY_APN:Z

    #@22a
    .line 2171
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_BUFSIZE_DCM:Z

    #@22c
    .line 2190
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_PCO_CONFIG_DCM:Z

    #@22e
    .line 2202
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_MODEM_DEF_RCVBUF_DCM:Z

    #@230
    .line 2211
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_DISABLE_MULTI_PDN_DCM:Z

    #@232
    .line 2220
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_LTE_ATTACH_RETRY_DCM:Z

    #@234
    .line 2232
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KDDI:Z

    #@236
    .line 2240
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_KDDI_USE_PREFERREDDUN_APN_KDDI:Z

    #@238
    .line 2251
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BLOCK_TETHERINGPDN_ON_GSMROAMING_KDDI:Z

    #@23a
    .line 2364
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_ADD_RCS_TYPE:Z

    #@23c
    .line 2373
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_ARBITRATION:Z

    #@23e
    .line 2385
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@240
    .line 2397
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_REDEFINE_PERMANENT_CAUSE_EU:Z

    #@242
    .line 2408
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SENDMMS_ON_DATADISABLED:Z

    #@244
    .line 2420
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_USER_SELECTION_SCEANARIO_EU:Z

    #@246
    .line 2434
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SENDMMS_ON_DATAROAMINGDISABLED:Z

    #@248
    .line 2445
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DATAENABLED_CONFIG_TLF_ES:Z

    #@24a
    .line 2568
    const/4 v0, 0x0

    #@24b
    iput-object v0, p0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@24d
    .line 2570
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@24f
    .line 2572
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->NAI_support:I

    #@251
    .line 2574
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->retry_interval:I

    #@253
    .line 2576
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->IMSPowerOffdelaytime:I

    #@255
    .line 2579
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->DefaultPDNdependancy:Z

    #@257
    .line 2582
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->poweroffdelayneed:Z

    #@259
    .line 2588
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_REATTACH_DEFAULT_PDN:Z

    #@25b
    .line 2589
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_SINGLE_PDN:Z

    #@25d
    .line 2590
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_IPV6:Z

    #@25f
    .line 2592
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_DEFAULT_PREFER_APN:Z

    #@261
    .line 2593
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_DOCOMO_DSAC:Z

    #@263
    .line 2594
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_ROAMING_NOTIFICATION:Z

    #@265
    .line 2595
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_DOCOMO_TETHER:Z

    #@267
    .line 2599
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_DOCOMO_DUAL_CONNECTIVITY:Z

    #@269
    .line 2601
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_UPDATE_TCP_BUF:Z

    #@26b
    .line 2602
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_ROAMING_LOCK:Z

    #@26d
    .line 2603
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_BLOCK_UNNECESSARY_DISCONNECT:Z

    #@26f
    .line 2604
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_RETAIN_SOCKET_CONN_IN_OOS:Z

    #@271
    .line 2605
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_RETRY_READY_APN:Z

    #@273
    .line 2607
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_TETHERING_NON_APN:Z

    #@275
    .line 2608
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_VPN_DEFAULTPROXY:Z

    #@277
    .line 2620
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->regenFdInHandover:Z

    #@279
    .line 2623
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->RESETTING_FD_FOR_HO:Z

    #@27b
    .line 2626
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->INFINITE_DATA_CALL_RETRY_COUNT:Z

    #@27d
    .line 2630
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->bypassforuplus:Z

    #@27f
    .line 2633
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->DISALLOW_CHNAGING_PHONETYPE:Z

    #@281
    .line 2640
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->imsstarttiming:Z

    #@283
    .line 2646
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->fixthephonetypetoCDMA:Z

    #@285
    .line 2659
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->rxtx_debug:Z

    #@287
    .line 2663
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LTE_ROAMING_FOR_KT:Z

    #@289
    .line 2668
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->CALL_REATTACHED:Z

    #@28b
    .line 2674
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->cdmaTimer:I

    #@28d
    .line 2680
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->VZW_GFIT_IP_address_utilization:Z

    #@28f
    .line 2699
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->closeDelayForIms:Z

    #@291
    .line 2702
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->IsPossibleSetMobileNOSVC:Z

    #@293
    .line 2705
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->CHECK_LW_AND_WONLY_MODE:Z

    #@295
    .line 2710
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->lg_data_sprint_oamdm_provisioning:Z

    #@297
    .line 2714
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@299
    .line 2718
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SupportRCSe:Z

    #@29b
    .line 2721
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->DisableFD_OnTethering:Z

    #@29d
    .line 2724
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->USE_ORIGIN_STALLALARM:Z

    #@29f
    .line 2727
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_SPR_MTU_CHANGE:Z

    #@2a1
    .line 2731
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_CPA_DNS_SETTING:Z

    #@2a3
    .line 2735
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_SUSPENDED:Z

    #@2a5
    .line 2739
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_APN_FOR_ROAMING:Z

    #@2a7
    .line 2743
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_TETHERING_DNS:Z

    #@2a9
    .line 2755
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_BLOCK_ANOTHER_TYPE:Z

    #@2ab
    .line 2759
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->pam_Connection_retry_error_pop_up:Z

    #@2ad
    .line 2763
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->apn_sync_using_sequence_pdn_list:Z

    #@2af
    .line 2765
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->FIXED_JB_RETRYCOUNT_INIT_ERR:Z

    #@2b1
    .line 2772
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->default_ESN:Z

    #@2b3
    .line 2777
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@2b5
    .line 2780
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->usage_data_warning:Z

    #@2b7
    .line 2781
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->apnbackup:Z

    #@2b9
    .line 2782
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->DoNotReadDummyAPN:Z

    #@2bb
    .line 2805
    iput-object p2, p0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@2bd
    .line 2807
    const-string v0, "VZWBASE"

    #@2bf
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2c2
    move-result v0

    #@2c3
    if-eqz v0, :cond_338

    #@2c5
    .line 2808
    const-string v0, "GSM"

    #@2c7
    const-string v1, "LG feature - VZWBASE"

    #@2c9
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2cc
    .line 2811
    iput v3, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@2ce
    .line 2818
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@2d0
    .line 2819
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@2d2
    .line 2820
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_ON_CDMA:Z

    #@2d4
    .line 2821
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@2d6
    .line 2822
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_BLOCK_IMS_CONNECTION_TRY_FOR_15MIN_WHEN_CONNECT_FAIL:Z

    #@2d8
    .line 2823
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@2da
    .line 2824
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@2dc
    .line 2825
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@2de
    .line 2826
    const/16 v0, 0x7d0

    #@2e0
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@2e2
    .line 2827
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_MTU_SET_ROAMING_NETWORK:Z

    #@2e4
    .line 2828
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@2e6
    .line 2829
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_BLOCK_APP_REQUEST_WHEN_USER_DATA_DISABLED:Z

    #@2e8
    .line 2830
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_TRYSETUP_AFTER_SIM_LOADED_ON_CDMA:Z

    #@2ea
    .line 2831
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@2ec
    .line 2832
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TOOL_DATA_BLOCK_HIDDEN_MENU:Z

    #@2ee
    .line 2834
    iput v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@2f0
    .line 2835
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_VZW:Z

    #@2f2
    .line 2836
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

    #@2f4
    .line 2837
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_KEEP_ROUTE_INFO_ON_SUSPEND_VZW:Z

    #@2f6
    .line 2838
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_VZW:Z

    #@2f8
    .line 2839
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RETRY_CONFIG_VZW:Z

    #@2fa
    .line 2840
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

    #@2fc
    .line 2841
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SEND_DATA_ROAM_POPUP_INTENT_VZW:Z

    #@2fe
    .line 2842
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_SET_MTU_SIZE_VZW:Z

    #@300
    .line 2843
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APN2_ENABLE_BACKUP_RESTORE_VZW:Z

    #@302
    .line 2844
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_VZW_APN_RESTORE_TIME_SET_VZW:Z

    #@304
    .line 2845
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VZWAPP_CHECK_PERMISSION_VZW:Z

    #@306
    .line 2846
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_SET_DEFAULT_MTU_VZW:Z

    #@308
    .line 2847
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@30a
    .line 2848
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_RECONN_NOT_ALLOWED_VZW:Z

    #@30c
    .line 2851
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_VZW:Z

    #@30e
    .line 2852
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_TRAFFICSTATS_EXTENSIONS_VZW:Z

    #@310
    .line 2853
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_VZW_DATA_USAGE_DEFAULT_CONFIG_VZW:Z

    #@312
    .line 2854
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_VZWAPNE_AT_COMMAND_VZW:Z

    #@314
    .line 2855
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SUPPORT_SUPL_ON_DEFAULT_TYPE_VZW:Z

    #@316
    .line 2857
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@318
    .line 3403
    :cond_318
    :goto_318
    iget-boolean v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@31a
    if-eqz v0, :cond_337

    #@31c
    .line 3407
    const-string/jumbo v0, "none"

    #@31f
    const-string/jumbo v1, "net.pdnlist"

    #@322
    const-string/jumbo v2, "none"

    #@325
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@328
    move-result-object v1

    #@329
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32c
    move-result v0

    #@32d
    if-eqz v0, :cond_337

    #@32f
    .line 3409
    const-string/jumbo v0, "net.pdnlist"

    #@332
    const-string v1, ""

    #@334
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@337
    .line 3415
    :cond_337
    return-void

    #@338
    .line 2863
    :cond_338
    const-string v0, "LGTBASE"

    #@33a
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@33d
    move-result v0

    #@33e
    if-eqz v0, :cond_3be

    #@340
    .line 2866
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->RESETTING_FD_FOR_HO:Z

    #@342
    .line 2867
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@344
    .line 2871
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_CREATE_CDMADATACONNECTIONTRACKER:Z

    #@346
    .line 2873
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@348
    .line 2874
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@34a
    .line 2875
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@34c
    .line 2876
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MANUALSEARCH:Z

    #@34e
    .line 2877
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@350
    .line 2878
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@352
    .line 2879
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@354
    .line 2880
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@356
    .line 2881
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_BLOCK_ANOTHER_TYPE:Z

    #@358
    .line 2882
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_NOTAPPLIED_ON_DEFAULT_USERDATADISABLE:Z

    #@35a
    .line 2883
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_MPDN:Z

    #@35c
    .line 2884
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@35e
    .line 2888
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KR:Z

    #@360
    .line 2889
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@362
    .line 2891
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MAINTAIN_USER_DATA_SETTING:Z

    #@364
    .line 2892
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

    #@366
    .line 2893
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@368
    .line 2895
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KR:Z

    #@36a
    .line 2896
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KR:Z

    #@36c
    .line 2897
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@36e
    .line 2898
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@370
    .line 2899
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

    #@372
    .line 2900
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_FAST_CONNECT_DEFAULT_PDN_KR:Z

    #@374
    .line 2902
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@376
    .line 2903
    const-string/jumbo v0, "true"

    #@379
    const-string/jumbo v1, "ro.support_mpdn"

    #@37c
    const-string/jumbo v2, "true"

    #@37f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@382
    move-result-object v1

    #@383
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@386
    move-result v0

    #@387
    if-eqz v0, :cond_3b7

    #@389
    .line 2905
    iput v6, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@38b
    .line 2906
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@38d
    .line 2907
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_PEND_STARTING_TIME:Z

    #@38f
    .line 2908
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@391
    .line 2909
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@393
    .line 2910
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@395
    .line 2921
    :goto_395
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_LTE_ROAMING_LGU:Z

    #@397
    .line 2923
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_RETRY_UPLUS:Z

    #@399
    .line 2924
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_QUERY_SELECTION_UPLUS:Z

    #@39b
    .line 2925
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_ON_UPLUS:Z

    #@39d
    .line 2926
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LOCK_ORDER_UPLUS:Z

    #@39f
    .line 2927
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@3a1
    .line 2928
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_TOAST_ON_WIFI_OFF_UPLUS:Z

    #@3a3
    .line 2929
    iput v6, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@3a5
    .line 2930
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_REJECT_INTENT_UPLUS:Z

    #@3a7
    .line 2931
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_DUN_NV_ONOFF_UPLUS:Z

    #@3a9
    .line 2932
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_LGU:Z

    #@3ab
    .line 2933
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_1XEVDO_UPLUS:Z

    #@3ad
    .line 2934
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LOCK_ORDER_UPLUS:Z

    #@3af
    .line 2935
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_HIPRI_TYPE_TIMER_UPLUS:Z

    #@3b1
    .line 2936
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SUPPORT_NSWO_UPLUS:Z

    #@3b3
    .line 2937
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BACKGROUND_DATA_NOTI_IN_AIRPLANE_UPLUS:Z

    #@3b5
    goto/16 :goto_318

    #@3b7
    .line 2913
    :cond_3b7
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_PEND_STARTING_TIME:Z

    #@3b9
    .line 2914
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DEREGISTRATION:Z

    #@3bb
    .line 2915
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@3bd
    goto :goto_395

    #@3be
    .line 2938
    :cond_3be
    const-string v0, "ATTBASE"

    #@3c0
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3c3
    move-result v0

    #@3c4
    if-eqz v0, :cond_3ee

    #@3c6
    .line 2939
    iput v1, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@3c8
    .line 2940
    iput v1, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@3ca
    .line 2941
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@3cc
    .line 2942
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@3ce
    .line 2943
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@3d0
    .line 2944
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@3d2
    .line 2945
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@3d4
    .line 2946
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@3d6
    .line 2947
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@3d8
    .line 2948
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@3da
    .line 2949
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@3dc
    .line 2950
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_QOS_NOTIFY:Z

    #@3de
    .line 2951
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SMCAUSE_NOTIFY:Z

    #@3e0
    .line 2952
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_RETRANSMISSION_ATT:Z

    #@3e2
    .line 2953
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_BACKUP:Z

    #@3e4
    .line 2954
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_VOICEPROTECTION_ATT:Z

    #@3e6
    .line 2955
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_AVOID_UNEXPECTED_QUERY:Z

    #@3e8
    .line 2956
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_FOTA_UPGRADE_ATT:Z

    #@3ea
    .line 2957
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNPROVISION_ATT:Z

    #@3ec
    goto/16 :goto_318

    #@3ee
    .line 2958
    :cond_3ee
    const-string v0, "TMUSBASE"

    #@3f0
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3f3
    move-result v0

    #@3f4
    if-eqz v0, :cond_42d

    #@3f6
    .line 2959
    const/16 v0, 0xb

    #@3f8
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@3fa
    .line 2960
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@3fc
    .line 2961
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@3fe
    .line 2962
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@400
    .line 2963
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@402
    .line 2964
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@404
    .line 2965
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@406
    .line 2966
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_DISABLE_ESM_INFO:Z

    #@408
    .line 2967
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@40a
    .line 2968
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@40c
    .line 2969
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@40e
    .line 2970
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_QOS_NOTIFY:Z

    #@410
    .line 2971
    const-string/jumbo v0, "persist.lgiqc.ext"

    #@413
    invoke-static {v0, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@416
    move-result v0

    #@417
    iput-boolean v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_CIQ_TMUS:Z

    #@419
    .line 2972
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@41b
    .line 2973
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SMCAUSE_NOTIFY:Z

    #@41d
    .line 2974
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_BUFSIZE_TMUS:Z

    #@41f
    .line 2975
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_WARNINGBYTE_TMUS:Z

    #@421
    .line 2976
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_ROAMING_POPUP_TMUS:Z

    #@423
    .line 2977
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHERING_APN_LIST:Z

    #@425
    .line 2978
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_HANDLE_CONNECTING_DATACALL_ON_DCLISTCHANGED:Z

    #@427
    .line 2979
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_AIRPLANEMODE_DETACH:Z

    #@429
    .line 2980
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_CHANGE_UPSTREM_TYPE:Z

    #@42b
    goto/16 :goto_318

    #@42d
    .line 2981
    :cond_42d
    const-string v0, "BELLBASE"

    #@42f
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@432
    move-result v0

    #@433
    if-eqz v0, :cond_44f

    #@435
    .line 2982
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@437
    .line 2983
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@439
    .line 2984
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@43b
    .line 2985
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@43d
    .line 2986
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@43f
    .line 2987
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@441
    .line 2988
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@443
    .line 2989
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@445
    .line 2990
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@447
    .line 2991
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@449
    .line 2992
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@44b
    .line 2993
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_CA:Z

    #@44d
    goto/16 :goto_318

    #@44f
    .line 2994
    :cond_44f
    const-string v0, "RGSBASE"

    #@451
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@454
    move-result v0

    #@455
    if-eqz v0, :cond_471

    #@457
    .line 2995
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@459
    .line 2996
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@45b
    .line 2997
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@45d
    .line 2998
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@45f
    .line 2999
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@461
    .line 3000
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@463
    .line 3001
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@465
    .line 3002
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@467
    .line 3003
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@469
    .line 3004
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@46b
    .line 3005
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@46d
    .line 3006
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_CA:Z

    #@46f
    goto/16 :goto_318

    #@471
    .line 3007
    :cond_471
    const-string v0, "TLSBASE"

    #@473
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@476
    move-result v0

    #@477
    if-eqz v0, :cond_495

    #@479
    .line 3008
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@47b
    .line 3009
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@47d
    .line 3010
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@47f
    .line 3011
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@481
    .line 3012
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@483
    .line 3013
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@485
    .line 3014
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@487
    .line 3015
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@489
    .line 3016
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@48b
    .line 3017
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@48d
    .line 3018
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@48f
    .line 3019
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_CA:Z

    #@491
    .line 3021
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_ADD_APN_SCENARIO_TLS:Z

    #@493
    goto/16 :goto_318

    #@495
    .line 3022
    :cond_495
    const-string v0, "VTRBASE"

    #@497
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@49a
    move-result v0

    #@49b
    if-eqz v0, :cond_4b7

    #@49d
    .line 3023
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@49f
    .line 3024
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@4a1
    .line 3025
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@4a3
    .line 3026
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@4a5
    .line 3027
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@4a7
    .line 3028
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@4a9
    .line 3029
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@4ab
    .line 3030
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@4ad
    .line 3031
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@4af
    .line 3032
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@4b1
    .line 3033
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@4b3
    .line 3034
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AUTOPROFILE_CA:Z

    #@4b5
    goto/16 :goto_318

    #@4b7
    .line 3035
    :cond_4b7
    const-string v0, "KTBASE"

    #@4b9
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@4bc
    move-result v0

    #@4bd
    if-eqz v0, :cond_52d

    #@4bf
    .line 3038
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_IPV6:Z

    #@4c1
    .line 3042
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@4c3
    .line 3043
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@4c5
    .line 3044
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@4c7
    .line 3045
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MANUALSEARCH:Z

    #@4c9
    .line 3046
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@4cb
    .line 3047
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_BLOCK_ANOTHER_TYPE:Z

    #@4cd
    .line 3048
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_NOTAPPLIED_ON_DEFAULT_USERDATADISABLE:Z

    #@4cf
    .line 3049
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_MPDN:Z

    #@4d1
    .line 3050
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@4d3
    .line 3054
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KR:Z

    #@4d5
    .line 3056
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MAINTAIN_USER_DATA_SETTING:Z

    #@4d7
    .line 3057
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

    #@4d9
    .line 3058
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@4db
    .line 3060
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KR:Z

    #@4dd
    .line 3061
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KR:Z

    #@4df
    .line 3062
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@4e1
    .line 3063
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@4e3
    .line 3064
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

    #@4e5
    .line 3065
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@4e7
    .line 3066
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

    #@4e9
    .line 3067
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_FAST_CONNECT_DEFAULT_PDN_KR:Z

    #@4eb
    .line 3069
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@4ed
    .line 3070
    const-string/jumbo v0, "true"

    #@4f0
    const-string/jumbo v1, "ro.support_mpdn"

    #@4f3
    const-string/jumbo v2, "true"

    #@4f6
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4f9
    move-result-object v1

    #@4fa
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4fd
    move-result v0

    #@4fe
    if-eqz v0, :cond_526

    #@500
    .line 3071
    const/4 v0, 0x5

    #@501
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@503
    .line 3072
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@505
    .line 3073
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_PEND_STARTING_TIME:Z

    #@507
    .line 3074
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@509
    .line 3075
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@50b
    .line 3076
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@50d
    .line 3077
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@50f
    .line 3078
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@511
    .line 3088
    :goto_511
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE_KT:Z

    #@513
    .line 3089
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LTE_ROAMING_KT:Z

    #@515
    .line 3090
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_WARNING_VALUE_KT:Z

    #@517
    .line 3091
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KT:Z

    #@519
    .line 3092
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KT:Z

    #@51b
    .line 3093
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_KT:Z

    #@51d
    .line 3094
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MULTIRAB_KT:Z

    #@51f
    .line 3095
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_KAF_KT:Z

    #@521
    .line 3096
    const/4 v0, 0x5

    #@522
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@524
    goto/16 :goto_318

    #@526
    .line 3081
    :cond_526
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_PEND_STARTING_TIME:Z

    #@528
    .line 3082
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DEREGISTRATION:Z

    #@52a
    .line 3083
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@52c
    goto :goto_511

    #@52d
    .line 3098
    :cond_52d
    const-string v0, "SKTBASE"

    #@52f
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@532
    move-result v0

    #@533
    if-eqz v0, :cond_5a9

    #@535
    .line 3101
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_IPV6:Z

    #@537
    .line 3105
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@539
    .line 3106
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@53b
    .line 3107
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@53d
    .line 3108
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MANUALSEARCH:Z

    #@53f
    .line 3109
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@541
    .line 3110
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@543
    .line 3111
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@545
    .line 3112
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_BLOCK_ANOTHER_TYPE:Z

    #@547
    .line 3113
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_NOTAPPLIED_ON_DEFAULT_USERDATADISABLE:Z

    #@549
    .line 3114
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PROFILE_SETTING_SINGLE_PDN:Z

    #@54b
    .line 3115
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_MPDN:Z

    #@54d
    .line 3116
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@54f
    .line 3120
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KR:Z

    #@551
    .line 3121
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SELECT_KR:Z

    #@553
    .line 3123
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MAINTAIN_USER_DATA_SETTING:Z

    #@555
    .line 3124
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BLOCK_PAYPOPUP_BUT_TRYSETUP:Z

    #@557
    .line 3125
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@559
    .line 3127
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KR:Z

    #@55b
    .line 3128
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_KR:Z

    #@55d
    .line 3129
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VOICE_PROTECTION_KR:Z

    #@55f
    .line 3130
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_KR:Z

    #@561
    .line 3131
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODE_CHANGE_USER3G_KR:Z

    #@563
    .line 3132
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_NOTI_USERDATADISABLE_KR:Z

    #@565
    .line 3133
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

    #@567
    .line 3134
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_ADD_RT_API_KR:Z

    #@569
    .line 3135
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_FAST_CONNECT_DEFAULT_PDN_KR:Z

    #@56b
    .line 3137
    iput v4, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@56d
    .line 3138
    const-string/jumbo v0, "true"

    #@570
    const-string/jumbo v1, "ro.support_mpdn"

    #@573
    const-string/jumbo v2, "true"

    #@576
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@579
    move-result-object v1

    #@57a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57d
    move-result v0

    #@57e
    if-eqz v0, :cond_5a0

    #@580
    .line 3139
    const/4 v0, 0x6

    #@581
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@583
    .line 3140
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@585
    .line 3141
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_PEND_STARTING_TIME:Z

    #@587
    .line 3142
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@589
    .line 3153
    :goto_589
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_GPRS_REJECTED_SKT:Z

    #@58b
    .line 3154
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_SKT:Z

    #@58d
    .line 3155
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@58f
    .line 3156
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_VOICE_5SEC_DELAY_SKT:Z

    #@591
    .line 3157
    iput v5, p0, Lcom/android/internal/telephony/LGfeature;->IMSPowerOffdelaytime:I

    #@593
    .line 3158
    const/4 v0, 0x6

    #@594
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@596
    .line 3159
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_ROAMING_SKT:Z

    #@598
    .line 3160
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_ADD_PDN_RESET_API_SKT:Z

    #@59a
    .line 3161
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_DUN_TYPE_TIMER_SKT:Z

    #@59c
    .line 3162
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_DISABLE_BACKGROUND_SKT:Z

    #@59e
    goto/16 :goto_318

    #@5a0
    .line 3145
    :cond_5a0
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_PEND_STARTING_TIME:Z

    #@5a2
    .line 3146
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DEREGISTRATION:Z

    #@5a4
    .line 3147
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@5a6
    .line 3148
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@5a8
    goto :goto_589

    #@5a9
    .line 3166
    :cond_5a9
    const-string v0, "DCMBASE"

    #@5ab
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5ae
    move-result v0

    #@5af
    if-eqz v0, :cond_5dd

    #@5b1
    .line 3169
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MODECHANGE:Z

    #@5b3
    .line 3170
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_IPV6:Z

    #@5b5
    .line 3171
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_ROAMING_LOCK:Z

    #@5b7
    .line 3172
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@5b9
    .line 3173
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_LG_DATA_RECOVERY:Z

    #@5bb
    .line 3174
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_REL8_PCH_NETWORK:Z

    #@5bd
    .line 3175
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@5bf
    .line 3179
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_APN_CHANGE_DCM:Z

    #@5c1
    .line 3180
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PREFERAPN_SETTING_DCM:Z

    #@5c3
    .line 3181
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_DSAC_NOTIFICATION_DCM:Z

    #@5c5
    .line 3182
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@5c7
    .line 3183
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_SOCKET_CONN_IN_OOS_DCM:Z

    #@5c9
    .line 3184
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_APN_CHANGE_FOR_MDM_DCM:Z

    #@5cb
    .line 3185
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_RETRY_READY_APN:Z

    #@5cd
    .line 3186
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_VPN_DEF_PROXY_DCM:Z

    #@5cf
    .line 3187
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_HANDLE_PERMANENT_CAUSE_DCM:Z

    #@5d1
    .line 3188
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_ENHANCE_DCM:Z

    #@5d3
    .line 3189
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_ROAMING_NOTI_DCM:Z

    #@5d5
    .line 3190
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PREVENT_RECONNECT:Z

    #@5d7
    .line 3191
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_BUFSIZE_DCM:Z

    #@5d9
    .line 3192
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_ON_SCREENON:Z

    #@5db
    goto/16 :goto_318

    #@5dd
    .line 3195
    :cond_5dd
    const-string v0, "MPCSBASE"

    #@5df
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5e2
    move-result v0

    #@5e3
    if-nez v0, :cond_318

    #@5e5
    .line 3200
    const-string v0, "KDDIBASE"

    #@5e7
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5ea
    move-result v0

    #@5eb
    if-eqz v0, :cond_654

    #@5ed
    .line 3202
    const-string v0, "GSM"

    #@5ef
    const-string v1, "LG feature - KDDIBASE"

    #@5f1
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5f4
    .line 3204
    const/16 v0, 0x8

    #@5f6
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@5f8
    .line 3211
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@5fa
    .line 3212
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION:Z

    #@5fc
    .line 3213
    const/16 v0, 0x7d0

    #@5fe
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DELAY_CLEANUP_FOR_DEREGISTRATION_VALUE:I

    #@600
    .line 3214
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_BLOCK_APP_REQUEST_WHEN_USER_DATA_DISABLED:Z

    #@602
    .line 3215
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_ON_CDMA:Z

    #@604
    .line 3216
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@606
    .line 3217
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@608
    .line 3218
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_IPV46_PCSCF_INTERACE_ON_RIL_AFW:Z

    #@60a
    .line 3219
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_GET_PCSCF_ON_IMS_TYPE:Z

    #@60c
    .line 3220
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_CELL_INFO:Z

    #@60e
    .line 3221
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_KEEP_INFO_ON_RAT_CHANGE:Z

    #@610
    .line 3224
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KDDI:Z

    #@612
    .line 3225
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_KDDI_USE_PREFERREDDUN_APN_KDDI:Z

    #@614
    .line 3226
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_BLOCK_TETHERINGPDN_ON_GSMROAMING_KDDI:Z

    #@616
    .line 3227
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_KDDI_SUPPORT_DYNAMIC_APN_NAI_SETTING_FOR_CPA:Z

    #@618
    .line 3228
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DELAY_GSMDCT_DISPOSE_WHEN_CDMADCT_RATTYPE_IN_26SEC_KDDI:Z

    #@61a
    .line 3229
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DONOT_DEACTIVATE_DUN_TYPE_KDDI:Z

    #@61c
    .line 3230
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_INTENT_ON_CONNECTED_KDDI:Z

    #@61e
    .line 3231
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_KDDI:Z

    #@620
    .line 3232
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_SET_APN_IA_TIMER_KDDI:Z

    #@622
    .line 3233
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_KDDI_CPA_CONFIG_KDDI:Z

    #@624
    .line 3234
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_KDDI_SET_TETHERING_DNS_KDDI:Z

    #@626
    .line 3235
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_PREFFERED_APN_SUPPORT_NON_DEFAULT_KDDI:Z

    #@628
    .line 3236
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_KDDI_CPA_SUPPORT_ON_LEGACY_CDMA_KDDI:Z

    #@62a
    .line 3241
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@62c
    .line 3248
    iput v3, p0, Lcom/android/internal/telephony/LGfeature;->NAI_support:I

    #@62e
    .line 3249
    iput v3, p0, Lcom/android/internal/telephony/LGfeature;->retry_interval:I

    #@630
    .line 3250
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->DefaultPDNdependancy:Z

    #@632
    .line 3251
    iput v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@634
    .line 3254
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->VZW_GFIT_IP_address_utilization:Z

    #@636
    .line 3258
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_CPA_DNS_SETTING:Z

    #@638
    .line 3262
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_SUSPENDED:Z

    #@63a
    .line 3266
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_APN_FOR_ROAMING:Z

    #@63c
    .line 3270
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->KDDI_TETHERING_DNS:Z

    #@63e
    .line 3273
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

    #@640
    .line 3274
    const-string/jumbo v0, "ril.current.vzwfeature"

    #@643
    const-string v1, "1"

    #@645
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@648
    .line 3276
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SEND_DATA_ROAM_POPUP_INTENT_VZW:Z

    #@64a
    .line 3279
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_RIL_CONN_HISTORY:Z

    #@64c
    .line 3281
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->CDMA_CPA:Z

    #@64e
    .line 3282
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->KDDI_1X_DUN:Z

    #@650
    .line 3283
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->KDDI_dipose_GsmDCT:Z

    #@652
    goto/16 :goto_318

    #@654
    .line 3287
    :cond_654
    const-string v0, "SPCSBASE"

    #@656
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@659
    move-result v0

    #@65a
    if-eqz v0, :cond_6b9

    #@65c
    .line 3289
    const-string v0, "LGfeature"

    #@65e
    const-string v1, "base SPCSBASE!!!"

    #@660
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@663
    .line 3290
    const/16 v0, 0x9

    #@665
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@667
    .line 3292
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_AVOID_APN_DB_ERASING_ON_FACTORY_DATA_RESET_AND_HFA_SPRINT:Z

    #@669
    .line 3293
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_CHECK_PROFILE_DB_EXTENSION_SPRINT:Z

    #@66b
    .line 3294
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_CONTROL_PDN_ON_POA:Z

    #@66d
    .line 3295
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_SUPPORT_MPDN_SPRINT:Z

    #@66f
    .line 3297
    const-string v0, "LGfeature"

    #@671
    new-instance v1, Ljava/lang/StringBuilder;

    #@673
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@676
    const-string v2, "fixthephonetypetoCDMA"

    #@678
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67b
    move-result-object v1

    #@67c
    iget-boolean v2, p0, Lcom/android/internal/telephony/LGfeature;->fixthephonetypetoCDMA:Z

    #@67e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@681
    move-result-object v1

    #@682
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@685
    move-result-object v1

    #@686
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@689
    .line 3298
    const/16 v0, 0x9

    #@68b
    iput v0, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@68d
    .line 3299
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->lg_data_sprint_oamdm_provisioning:Z

    #@68f
    .line 3300
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_ON_CDMA:Z

    #@691
    .line 3301
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->SUPPORT_SPR_MTU_CHANGE:Z

    #@693
    .line 3303
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->defaultMinAllow:Z

    #@695
    .line 3304
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->omadmDataBlock:Z

    #@697
    .line 3305
    sput-boolean v3, Lcom/android/internal/telephony/LGfeature;->mipErrorPopup:Z

    #@699
    .line 3307
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DEBUG_RIL_CONN_HISTORY:Z

    #@69b
    .line 3308
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@69d
    .line 3309
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@69f
    .line 3310
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->pam_Connection_retry_error_pop_up:Z

    #@6a1
    .line 3311
    iput-boolean v4, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_MTU_SET_ROAMING_NETWORK:Z

    #@6a3
    .line 3312
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->default_ESN:Z

    #@6a5
    .line 3313
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->usage_data_warning:Z

    #@6a7
    .line 3314
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->apnbackup:Z

    #@6a9
    .line 3315
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->DoNotReadDummyAPN:Z

    #@6ab
    .line 3316
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@6ad
    .line 3317
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_GSM_GLOBAL_PREFERED_APN_SPR:Z

    #@6af
    .line 3318
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@6b1
    .line 3319
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@6b3
    .line 3320
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_TRYSETUP_AFTER_SIM_LOADED_ON_CDMA:Z

    #@6b5
    .line 3321
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_DEACTIVATE_DATA_CALL_PENDING_RECOVERY:Z

    #@6b7
    goto/16 :goto_318

    #@6b9
    .line 3323
    :cond_6b9
    const-string v0, "VDFBASE"

    #@6bb
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@6be
    move-result v0

    #@6bf
    if-eqz v0, :cond_6d7

    #@6c1
    .line 3324
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->USE_ORIGIN_STALLALARM:Z

    #@6c3
    .line 3329
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@6c5
    .line 3330
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@6c7
    .line 3331
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_USER_SELECTION_SCEANARIO_EU:Z

    #@6c9
    .line 3332
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->DisableFD_OnTethering:Z

    #@6cb
    .line 3333
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@6cd
    .line 3334
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@6cf
    .line 3335
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@6d1
    .line 3336
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_REDEFINE_PERMANENT_CAUSE_EU:Z

    #@6d3
    .line 3337
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@6d5
    goto/16 :goto_318

    #@6d7
    .line 3338
    :cond_6d7
    const-string v0, "OPENBASE"

    #@6d9
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@6dc
    move-result v0

    #@6dd
    if-eqz v0, :cond_6f7

    #@6df
    .line 3339
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->USE_ORIGIN_STALLALARM:Z

    #@6e1
    .line 3343
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@6e3
    .line 3344
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@6e5
    .line 3345
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_ARBITRATION:Z

    #@6e7
    .line 3346
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_USER_SELECTION_SCEANARIO_EU:Z

    #@6e9
    .line 3347
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->DisableFD_OnTethering:Z

    #@6eb
    .line 3348
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@6ed
    .line 3349
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@6ef
    .line 3350
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@6f1
    .line 3351
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_REDEFINE_PERMANENT_CAUSE_EU:Z

    #@6f3
    .line 3352
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@6f5
    goto/16 :goto_318

    #@6f7
    .line 3353
    :cond_6f7
    const-string v0, "SHBBASE"

    #@6f9
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@6fc
    move-result v0

    #@6fd
    if-eqz v0, :cond_70d

    #@6ff
    .line 3357
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@701
    .line 3358
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@703
    .line 3359
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->DisableFD_OnTethering:Z

    #@705
    .line 3360
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@707
    .line 3361
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@709
    .line 3362
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@70b
    goto/16 :goto_318

    #@70d
    .line 3363
    :cond_70d
    const-string v0, "MONBASE"

    #@70f
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@712
    move-result v0

    #@713
    if-eqz v0, :cond_71d

    #@715
    .line 3366
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@717
    .line 3367
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->DisableFD_OnTethering:Z

    #@719
    .line 3368
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@71b
    goto/16 :goto_318

    #@71d
    .line 3369
    :cond_71d
    const-string v0, "VIVBASE"

    #@71f
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@722
    move-result v0

    #@723
    if-eqz v0, :cond_731

    #@725
    .line 3373
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@727
    .line 3374
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@729
    .line 3375
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@72b
    .line 3376
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@72d
    .line 3377
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@72f
    goto/16 :goto_318

    #@731
    .line 3378
    :cond_731
    const-string v0, "CLRBASE"

    #@733
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@736
    move-result v0

    #@737
    if-nez v0, :cond_741

    #@739
    const-string v0, "SCABASE"

    #@73b
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@73e
    move-result v0

    #@73f
    if-eqz v0, :cond_74d

    #@741
    .line 3382
    :cond_741
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@743
    .line 3383
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@745
    .line 3384
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@747
    .line 3385
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@749
    .line 3386
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@74b
    goto/16 :goto_318

    #@74d
    .line 3387
    :cond_74d
    const-string v0, "TCLBASE"

    #@74f
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@752
    move-result v0

    #@753
    if-eqz v0, :cond_761

    #@755
    .line 3391
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_NETSEARCH_KR:Z

    #@757
    .line 3392
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@759
    .line 3393
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_RESET_PREFAPN_SIM_CHANGED:Z

    #@75b
    .line 3394
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->PS_ATTACH_FOR_2_DETACH:Z

    #@75d
    .line 3395
    iput-boolean v3, p0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_SETUP_PENDING_WHEN_DCT_CHANGE:Z

    #@75f
    goto/16 :goto_318

    #@761
    .line 3399
    :cond_761
    const-string v0, "LGfeature"

    #@763
    const-string v1, "Wrong feature"

    #@765
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@768
    goto/16 :goto_318
.end method

.method public static getInstance()Lcom/android/internal/telephony/LGfeature;
    .registers 4

    #@0
    .prologue
    .line 2788
    sget-object v0, Lcom/android/internal/telephony/LGfeature;->sLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@2
    if-nez v0, :cond_16

    #@4
    .line 2790
    new-instance v0, Lcom/android/internal/telephony/LGfeature;

    #@6
    const/4 v1, 0x0

    #@7
    const-string/jumbo v2, "ro.afwdata.LGfeatureset"

    #@a
    const-string/jumbo v3, "none"

    #@d
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/LGfeature;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@14
    sput-object v0, Lcom/android/internal/telephony/LGfeature;->sLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@16
    .line 2792
    :cond_16
    sget-object v0, Lcom/android/internal/telephony/LGfeature;->sLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@18
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/LGfeature;
    .registers 3
    .parameter "c"
    .parameter "featureset"

    #@0
    .prologue
    .line 2797
    sget-object v0, Lcom/android/internal/telephony/LGfeature;->sLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 2799
    new-instance v0, Lcom/android/internal/telephony/LGfeature;

    #@6
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/LGfeature;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/LGfeature;->sLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@b
    .line 2801
    :cond_b
    sget-object v0, Lcom/android/internal/telephony/LGfeature;->sLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@d
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 3418
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 3419
    .local v0, sb:Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v2, "my featureset: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget-object v2, p0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " MPDN: "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget v2, p0, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, " NAI_SUPPORT: "

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    iget v2, p0, Lcom/android/internal/telephony/LGfeature;->NAI_support:I

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    .line 3421
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    return-object v1
.end method
