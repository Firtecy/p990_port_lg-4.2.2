.class public Lcom/android/internal/telephony/LgeRssiData;
.super Ljava/lang/Object;
.source "LgeRssiData.java"


# static fields
.field private static final ATTR_NAME_KEY:Ljava/lang/String; = "name"

.field private static final DBG:Z = true

.field private static final DEFAULT_RSSI_LEVEL:I = 0x5

.field private static final ELEMENT_NAME_ITEM:Ljava/lang/String; = "item"

.field private static final ELEMENT_NAME_RSSI:Ljava/lang/String; = "RSSI"

.field private static final FILE_PATH_RSSI:Ljava/lang/String; = "/etc/rssi.xml"

.field private static final TAG:Ljava/lang/String; = "LgeRssiData"

.field private static instance:Lcom/android/internal/telephony/LgeRssiData;

.field private static mRssiMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAsuEtcValue:[I

.field private mAsuGsmValue:[I

.field private mAsuUmtsValue:[I

.field private mCdmaDbmValue:[I

.field private mCdmaEcioValue:[I

.field private mEvdoDbmValue:[I

.field private mEvdoSnrValue:[I

.field private mLteRsrpValue:[I

.field private mLteRsrqValue:[I

.field private mLteRssnrValue:[I

.field private mLteSignalValue:[I

.field private mRssiLevel:I


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mRssiLevel:I

    #@6
    .line 64
    return-void
.end method

.method private convertToInt(Ljava/lang/String;)[I
    .registers 9
    .parameter "value"

    #@0
    .prologue
    .line 224
    if-nez p1, :cond_4

    #@2
    .line 225
    const/4 v0, 0x0

    #@3
    .line 234
    :goto_3
    return-object v0

    #@4
    .line 227
    :cond_4
    const-string v4, ","

    #@6
    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 228
    .local v3, tempValue:[Ljava/lang/String;
    const/4 v4, 0x0

    #@b
    aget-object v4, v3, v4

    #@d
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@10
    move-result v2

    #@11
    .line 229
    .local v2, size:I
    new-array v0, v2, [I

    #@13
    .line 230
    .local v0, array:[I
    const/4 v1, 0x1

    #@14
    .local v1, i:I
    :goto_14
    array-length v4, v3

    #@15
    if-ge v1, v4, :cond_24

    #@17
    .line 231
    add-int/lit8 v4, v1, -0x1

    #@19
    aget-object v5, v3, v1

    #@1b
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1e
    move-result v5

    #@1f
    aput v5, v0, v4

    #@21
    .line 230
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_14

    #@24
    .line 233
    :cond_24
    const-string v4, "LgeRssiData"

    #@26
    new-instance v5, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v6, "[convertToInt] value : "

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_3
.end method

.method public static getInstance()Lcom/android/internal/telephony/LgeRssiData;
    .registers 1

    #@0
    .prologue
    .line 66
    sget-object v0, Lcom/android/internal/telephony/LgeRssiData;->instance:Lcom/android/internal/telephony/LgeRssiData;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 67
    new-instance v0, Lcom/android/internal/telephony/LgeRssiData;

    #@6
    invoke-direct {v0}, Lcom/android/internal/telephony/LgeRssiData;-><init>()V

    #@9
    sput-object v0, Lcom/android/internal/telephony/LgeRssiData;->instance:Lcom/android/internal/telephony/LgeRssiData;

    #@b
    .line 69
    :cond_b
    sget-object v0, Lcom/android/internal/telephony/LgeRssiData;->instance:Lcom/android/internal/telephony/LgeRssiData;

    #@d
    return-object v0
.end method

.method private getItemValue(Ljava/lang/String;)[I
    .registers 13
    .parameter "key"

    #@0
    .prologue
    .line 173
    const-string v8, "LgeRssiData"

    #@2
    new-instance v9, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v10, "[getItemValue]mRssiMap is "

    #@9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v9

    #@d
    sget-object v10, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 174
    const/4 v1, 0x0

    #@1b
    .line 175
    .local v1, count:I
    const/4 v2, 0x0

    #@1c
    .line 177
    .local v2, dataNum:I
    sget-object v8, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@1e
    if-nez v8, :cond_23

    #@20
    .line 178
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeRssiData;->init()V

    #@23
    .line 181
    :cond_23
    sget-object v8, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@25
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v7

    #@29
    check-cast v7, Ljava/lang/String;

    #@2b
    .line 183
    .local v7, value:Ljava/lang/String;
    const-string v8, "LgeRssiData"

    #@2d
    new-instance v9, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v10, "[getItemValue] KEY : "

    #@34
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v9

    #@38
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v9

    #@3c
    const-string v10, ", VALUE : "

    #@3e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v9

    #@42
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v9

    #@46
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v9

    #@4a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 187
    if-eqz v7, :cond_57

    #@4f
    sget-object v8, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@51
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@54
    move-result v8

    #@55
    if-nez v8, :cond_59

    #@57
    .line 188
    :cond_57
    const/4 v8, 0x0

    #@58
    .line 212
    :goto_58
    return-object v8

    #@59
    .line 191
    :cond_59
    if-eqz v7, :cond_72

    #@5b
    .line 192
    const-string v8, ","

    #@5d
    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@62
    .local v5, len$:I
    const/4 v4, 0x0

    #@63
    .local v4, i$:I
    :goto_63
    if-ge v4, v5, :cond_72

    #@65
    aget-object v6, v0, v4

    #@67
    .line 194
    .local v6, tempValue:Ljava/lang/String;
    if-nez v1, :cond_6d

    #@69
    .line 195
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6c
    move-result v2

    #@6d
    .line 197
    :cond_6d
    add-int/lit8 v1, v1, 0x1

    #@6f
    .line 192
    add-int/lit8 v4, v4, 0x1

    #@71
    goto :goto_63

    #@72
    .line 201
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v6           #tempValue:Ljava/lang/String;
    :cond_72
    const-string v8, "LgeRssiData"

    #@74
    new-instance v9, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v10, "[getItemValue] dataNum : "

    #@7b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v9

    #@7f
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v9

    #@83
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v9

    #@87
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 205
    add-int/lit8 v8, v1, -0x1

    #@8c
    if-eq v2, v8, :cond_bc

    #@8e
    .line 206
    :try_start_8e
    new-instance v8, Ljava/lang/Exception;

    #@90
    const-string v9, "Data Size MisMatch"

    #@92
    invoke-direct {v8, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@95
    throw v8
    :try_end_96
    .catch Ljava/lang/Exception; {:try_start_8e .. :try_end_96} :catch_96

    #@96
    .line 207
    :catch_96
    move-exception v3

    #@97
    .line 208
    .local v3, e:Ljava/lang/Exception;
    const-string v8, "LgeRssiData"

    #@99
    new-instance v9, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v10, "[getItemValue] Data Size MisMatch ->count : "

    #@a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    const-string v10, ", dataNum : "

    #@aa
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v9

    #@b2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v9

    #@b6
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 209
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    #@bc
    .line 212
    .end local v3           #e:Ljava/lang/Exception;
    :cond_bc
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/LgeRssiData;->convertToInt(Ljava/lang/String;)[I

    #@bf
    move-result-object v8

    #@c0
    goto :goto_58
.end method

.method private loadRssi()V
    .registers 10

    #@0
    .prologue
    .line 80
    const/4 v3, 0x0

    #@1
    .line 81
    .local v3, in:Ljava/io/FileReader;
    const/4 v0, 0x0

    #@2
    .line 84
    .local v0, confFile:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    #@4
    .end local v0           #confFile:Ljava/io/File;
    const-string v6, "/etc/rssi.xml"

    #@6
    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    .line 85
    .restart local v0       #confFile:Ljava/io/File;
    if-eqz v0, :cond_32

    #@b
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@e
    move-result v6

    #@f
    if-eqz v6, :cond_32

    #@11
    .line 86
    const-string v6, "LgeRssiData"

    #@13
    new-instance v7, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v8, "[loadRssi] selected file : "

    #@1a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@21
    move-result-object v8

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 92
    sget-object v6, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@2f
    if-eqz v6, :cond_3a

    #@31
    .line 118
    :cond_31
    :goto_31
    return-void

    #@32
    .line 88
    :cond_32
    const-string v6, "LgeRssiData"

    #@34
    const-string v7, "[loadRssi] File not exist "

    #@36
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_31

    #@3a
    .line 96
    :cond_3a
    :try_start_3a
    new-instance v4, Ljava/io/FileReader;

    #@3c
    invoke-direct {v4, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_3f
    .catchall {:try_start_3a .. :try_end_3f} :catchall_8a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3a .. :try_end_3f} :catch_5d
    .catch Ljava/io/FileNotFoundException; {:try_start_3a .. :try_end_3f} :catch_6c
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3f} :catch_7b

    #@3f
    .line 98
    .end local v3           #in:Ljava/io/FileReader;
    .local v4, in:Ljava/io/FileReader;
    :try_start_3f
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    #@42
    move-result-object v2

    #@43
    .line 99
    .local v2, factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@46
    move-result-object v5

    #@47
    .line 100
    .local v5, parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@4a
    .line 102
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/LgeRssiData;->readRssiData(Lorg/xmlpull/v1/XmlPullParser;)V

    #@4d
    .line 103
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeRssiData;->parseRssiData()V
    :try_end_50
    .catchall {:try_start_3f .. :try_end_50} :catchall_96
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3f .. :try_end_50} :catch_9f
    .catch Ljava/io/FileNotFoundException; {:try_start_3f .. :try_end_50} :catch_9c
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_50} :catch_99

    #@50
    .line 112
    if-eqz v4, :cond_55

    #@52
    :try_start_52
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_55} :catch_57

    #@55
    :cond_55
    move-object v3, v4

    #@56
    .line 115
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_31

    #@57
    .line 113
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catch_57
    move-exception v1

    #@58
    .line 114
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@5b
    move-object v3, v4

    #@5c
    .line 116
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_31

    #@5d
    .line 104
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_5d
    move-exception v1

    #@5e
    .line 105
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_5e
    :try_start_5e
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_61
    .catchall {:try_start_5e .. :try_end_61} :catchall_8a

    #@61
    .line 112
    if-eqz v3, :cond_31

    #@63
    :try_start_63
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_66} :catch_67

    #@66
    goto :goto_31

    #@67
    .line 113
    :catch_67
    move-exception v1

    #@68
    .line 114
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@6b
    goto :goto_31

    #@6c
    .line 106
    .end local v1           #e:Ljava/io/IOException;
    :catch_6c
    move-exception v1

    #@6d
    .line 107
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_6d
    :try_start_6d
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_8a

    #@70
    .line 112
    if-eqz v3, :cond_31

    #@72
    :try_start_72
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_75} :catch_76

    #@75
    goto :goto_31

    #@76
    .line 113
    :catch_76
    move-exception v1

    #@77
    .line 114
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@7a
    goto :goto_31

    #@7b
    .line 108
    .end local v1           #e:Ljava/io/IOException;
    :catch_7b
    move-exception v1

    #@7c
    .line 109
    .restart local v1       #e:Ljava/io/IOException;
    :goto_7c
    :try_start_7c
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7f
    .catchall {:try_start_7c .. :try_end_7f} :catchall_8a

    #@7f
    .line 112
    if-eqz v3, :cond_31

    #@81
    :try_start_81
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_84
    .catch Ljava/io/IOException; {:try_start_81 .. :try_end_84} :catch_85

    #@84
    goto :goto_31

    #@85
    .line 113
    :catch_85
    move-exception v1

    #@86
    .line 114
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@89
    goto :goto_31

    #@8a
    .line 111
    .end local v1           #e:Ljava/io/IOException;
    :catchall_8a
    move-exception v6

    #@8b
    .line 112
    :goto_8b
    if-eqz v3, :cond_90

    #@8d
    :try_start_8d
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_90
    .catch Ljava/io/IOException; {:try_start_8d .. :try_end_90} :catch_91

    #@90
    .line 115
    :cond_90
    :goto_90
    throw v6

    #@91
    .line 113
    :catch_91
    move-exception v1

    #@92
    .line 114
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@95
    goto :goto_90

    #@96
    .line 111
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catchall_96
    move-exception v6

    #@97
    move-object v3, v4

    #@98
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_8b

    #@99
    .line 108
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catch_99
    move-exception v1

    #@9a
    move-object v3, v4

    #@9b
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_7c

    #@9c
    .line 106
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catch_9c
    move-exception v1

    #@9d
    move-object v3, v4

    #@9e
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_6d

    #@9f
    .line 104
    .end local v3           #in:Ljava/io/FileReader;
    .restart local v4       #in:Ljava/io/FileReader;
    :catch_9f
    move-exception v1

    #@a0
    move-object v3, v4

    #@a1
    .end local v4           #in:Ljava/io/FileReader;
    .restart local v3       #in:Ljava/io/FileReader;
    goto :goto_5e
.end method

.method private parseRssiData()V
    .registers 5

    #@0
    .prologue
    .line 155
    sget-object v1, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@2
    const-string/jumbo v2, "levelNum"

    #@5
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/String;

    #@b
    .line 156
    .local v0, value:Ljava/lang/String;
    if-nez v0, :cond_88

    #@d
    const/4 v1, 0x5

    #@e
    :goto_e
    iput v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mRssiLevel:I

    #@10
    .line 157
    const-string v1, "LgeRssiData"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v3, "mRssiLevel : "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    iget v3, p0, Lcom/android/internal/telephony/LgeRssiData;->mRssiLevel:I

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 159
    const-string/jumbo v1, "mLteRsrp"

    #@2e
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@31
    move-result-object v1

    #@32
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteRsrpValue:[I

    #@34
    .line 160
    const-string/jumbo v1, "mLteRssnr"

    #@37
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@3a
    move-result-object v1

    #@3b
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteRssnrValue:[I

    #@3d
    .line 161
    const-string/jumbo v1, "mLteSignalStrength"

    #@40
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@43
    move-result-object v1

    #@44
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteSignalValue:[I

    #@46
    .line 162
    const-string/jumbo v1, "mLteRsrq"

    #@49
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@4c
    move-result-object v1

    #@4d
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteRsrqValue:[I

    #@4f
    .line 163
    const-string v1, "asu_gsm"

    #@51
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@54
    move-result-object v1

    #@55
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mAsuGsmValue:[I

    #@57
    .line 164
    const-string v1, "asu_umts"

    #@59
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@5c
    move-result-object v1

    #@5d
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mAsuUmtsValue:[I

    #@5f
    .line 165
    const-string v1, "asu_etc"

    #@61
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@64
    move-result-object v1

    #@65
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mAsuEtcValue:[I

    #@67
    .line 166
    const-string v1, "cdmaDbm"

    #@69
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@6c
    move-result-object v1

    #@6d
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mCdmaDbmValue:[I

    #@6f
    .line 167
    const-string v1, "cdmaEcio"

    #@71
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@74
    move-result-object v1

    #@75
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mCdmaEcioValue:[I

    #@77
    .line 168
    const-string v1, "evdoDbm"

    #@79
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@7c
    move-result-object v1

    #@7d
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mEvdoDbmValue:[I

    #@7f
    .line 169
    const-string v1, "evdoSnr"

    #@81
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeRssiData;->getItemValue(Ljava/lang/String;)[I

    #@84
    move-result-object v1

    #@85
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRssiData;->mEvdoSnrValue:[I

    #@87
    .line 171
    return-void

    #@88
    .line 156
    :cond_88
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8b
    move-result v1

    #@8c
    goto :goto_e
.end method

.method private readRssiData(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 9
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 122
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@3
    move-result v0

    #@4
    .line 124
    .local v0, eventType:I
    new-instance v4, Ljava/util/HashMap;

    #@6
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@9
    sput-object v4, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@b
    .line 126
    :goto_b
    const/4 v4, 0x1

    #@c
    if-eq v0, v4, :cond_60

    #@e
    .line 127
    packed-switch v0, :pswitch_data_7c

    #@11
    .line 147
    :cond_11
    :goto_11
    :pswitch_11
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@14
    move-result v0

    #@15
    goto :goto_b

    #@16
    .line 131
    :pswitch_16
    const-string/jumbo v4, "item"

    #@19
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_11

    #@23
    .line 132
    const/4 v4, 0x0

    #@24
    const-string/jumbo v5, "name"

    #@27
    invoke-interface {p1, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    .line 133
    .local v1, key:Ljava/lang/String;
    if-eqz v1, :cond_11

    #@2d
    .line 134
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@30
    move-result v2

    #@31
    .line 135
    .local v2, type:I
    const/4 v4, 0x4

    #@32
    if-ne v2, v4, :cond_11

    #@34
    .line 136
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    .line 137
    .local v3, value:Ljava/lang/String;
    sget-object v4, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@3a
    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 139
    const-string v4, "LgeRssiData"

    #@3f
    new-instance v5, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v6, "[readRssiData] KEY : "

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    const-string v6, ", VALUE : "

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v5

    #@5c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_11

    #@60
    .line 149
    .end local v1           #key:Ljava/lang/String;
    .end local v2           #type:I
    .end local v3           #value:Ljava/lang/String;
    :cond_60
    const-string v4, "LgeRssiData"

    #@62
    new-instance v5, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v6, "[readRssiData]mRssiMap is "

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    sget-object v6, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@6f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v5

    #@77
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 150
    return-void

    #@7b
    .line 127
    nop

    #@7c
    :pswitch_data_7c
    .packed-switch 0x0
        :pswitch_11
        :pswitch_11
        :pswitch_16
    .end packed-switch
.end method


# virtual methods
.method public getAsuEtcValue()[I
    .registers 2

    #@0
    .prologue
    .line 278
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mAsuUmtsValue:[I

    #@2
    return-object v0
.end method

.method public getAsuGsmValue()[I
    .registers 2

    #@0
    .prologue
    .line 270
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mAsuGsmValue:[I

    #@2
    return-object v0
.end method

.method public getAsuUmtsValue()[I
    .registers 2

    #@0
    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mAsuUmtsValue:[I

    #@2
    return-object v0
.end method

.method public getCdmaDbmValue()[I
    .registers 2

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mCdmaDbmValue:[I

    #@2
    return-object v0
.end method

.method public getEcioValue()[I
    .registers 2

    #@0
    .prologue
    .line 258
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mCdmaEcioValue:[I

    #@2
    return-object v0
.end method

.method public getEvdoDbmValue()[I
    .registers 2

    #@0
    .prologue
    .line 262
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mEvdoDbmValue:[I

    #@2
    return-object v0
.end method

.method public getEvdoSnrValue()[I
    .registers 2

    #@0
    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mEvdoSnrValue:[I

    #@2
    return-object v0
.end method

.method public getLteSignalValue()[I
    .registers 2

    #@0
    .prologue
    .line 250
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteSignalValue:[I

    #@2
    return-object v0
.end method

.method public getRsrpValue()[I
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteRsrpValue:[I

    #@2
    return-object v0
.end method

.method public getRsrqValue()[I
    .registers 2

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteRsrqValue:[I

    #@2
    return-object v0
.end method

.method public getRssiLevel()I
    .registers 2

    #@0
    .prologue
    .line 216
    sget-object v0, Lcom/android/internal/telephony/LgeRssiData;->mRssiMap:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 217
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeRssiData;->init()V

    #@7
    .line 219
    :cond_7
    iget v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mRssiLevel:I

    #@9
    return v0
.end method

.method public getRssnrValue()[I
    .registers 2

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRssiData;->mLteRssnrValue:[I

    #@2
    return-object v0
.end method

.method public init()V
    .registers 1

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeRssiData;->loadRssi()V

    #@3
    .line 74
    return-void
.end method
