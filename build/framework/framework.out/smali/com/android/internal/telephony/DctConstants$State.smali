.class public final enum Lcom/android/internal/telephony/DctConstants$State;
.super Ljava/lang/Enum;
.source "DctConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DctConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/DctConstants$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/DctConstants$State;

.field public static final enum CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

.field public static final enum CONNECTING:Lcom/android/internal/telephony/DctConstants$State;

.field public static final enum DISCONNECTING:Lcom/android/internal/telephony/DctConstants$State;

.field public static final enum FAILED:Lcom/android/internal/telephony/DctConstants$State;

.field public static final enum IDLE:Lcom/android/internal/telephony/DctConstants$State;

.field public static final enum INITING:Lcom/android/internal/telephony/DctConstants$State;

.field public static final enum SCANNING:Lcom/android/internal/telephony/DctConstants$State;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 42
    new-instance v0, Lcom/android/internal/telephony/DctConstants$State;

    #@7
    const-string v1, "IDLE"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/DctConstants$State;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@e
    .line 43
    new-instance v0, Lcom/android/internal/telephony/DctConstants$State;

    #@10
    const-string v1, "INITING"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/DctConstants$State;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->INITING:Lcom/android/internal/telephony/DctConstants$State;

    #@17
    .line 44
    new-instance v0, Lcom/android/internal/telephony/DctConstants$State;

    #@19
    const-string v1, "CONNECTING"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/DctConstants$State;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->CONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@20
    .line 45
    new-instance v0, Lcom/android/internal/telephony/DctConstants$State;

    #@22
    const-string v1, "SCANNING"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/DctConstants$State;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->SCANNING:Lcom/android/internal/telephony/DctConstants$State;

    #@29
    .line 46
    new-instance v0, Lcom/android/internal/telephony/DctConstants$State;

    #@2b
    const-string v1, "CONNECTED"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/DctConstants$State;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@32
    .line 47
    new-instance v0, Lcom/android/internal/telephony/DctConstants$State;

    #@34
    const-string v1, "DISCONNECTING"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DctConstants$State;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->DISCONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@3c
    .line 48
    new-instance v0, Lcom/android/internal/telephony/DctConstants$State;

    #@3e
    const-string v1, "FAILED"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DctConstants$State;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@46
    .line 41
    const/4 v0, 0x7

    #@47
    new-array v0, v0, [Lcom/android/internal/telephony/DctConstants$State;

    #@49
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@4b
    aput-object v1, v0, v3

    #@4d
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->INITING:Lcom/android/internal/telephony/DctConstants$State;

    #@4f
    aput-object v1, v0, v4

    #@51
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@53
    aput-object v1, v0, v5

    #@55
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->SCANNING:Lcom/android/internal/telephony/DctConstants$State;

    #@57
    aput-object v1, v0, v6

    #@59
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->CONNECTED:Lcom/android/internal/telephony/DctConstants$State;

    #@5b
    aput-object v1, v0, v7

    #@5d
    const/4 v1, 0x5

    #@5e
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->DISCONNECTING:Lcom/android/internal/telephony/DctConstants$State;

    #@60
    aput-object v2, v0, v1

    #@62
    const/4 v1, 0x6

    #@63
    sget-object v2, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@65
    aput-object v2, v0, v1

    #@67
    sput-object v0, Lcom/android/internal/telephony/DctConstants$State;->$VALUES:[Lcom/android/internal/telephony/DctConstants$State;

    #@69
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/DctConstants$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 41
    const-class v0, Lcom/android/internal/telephony/DctConstants$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/DctConstants$State;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/DctConstants$State;
    .registers 1

    #@0
    .prologue
    .line 41
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->$VALUES:[Lcom/android/internal/telephony/DctConstants$State;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/DctConstants$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/DctConstants$State;

    #@8
    return-object v0
.end method
