.class public abstract Lcom/android/internal/telephony/IPhoneSubInfo$Stub;
.super Landroid/os/Binder;
.source "IPhoneSubInfo.java"

# interfaces
.implements Lcom/android/internal/telephony/IPhoneSubInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IPhoneSubInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IPhoneSubInfo$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.IPhoneSubInfo"

.field static final TRANSACTION_getBtid:I = 0x14

.field static final TRANSACTION_getCompleteVoiceMailNumber:I = 0xa

.field static final TRANSACTION_getDeviceId:I = 0x1

.field static final TRANSACTION_getDeviceId_type:I = 0xf

.field static final TRANSACTION_getDeviceSvn:I = 0x2

.field static final TRANSACTION_getIPPhoneState:I = 0x16

.field static final TRANSACTION_getIccSerialNumber:I = 0x4

.field static final TRANSACTION_getInternationalMdnVoiceMailNumberForVZW:I = 0x9

.field static final TRANSACTION_getIsimDomain:I = 0xd

.field static final TRANSACTION_getIsimImpi:I = 0xc

.field static final TRANSACTION_getIsimImpu:I = 0xe

.field static final TRANSACTION_getKeyLifetime:I = 0x15

.field static final TRANSACTION_getLine1AlphaTag:I = 0x6

.field static final TRANSACTION_getLine1Number:I = 0x5

.field static final TRANSACTION_getMSIN:I = 0x10

.field static final TRANSACTION_getMsisdn:I = 0x7

.field static final TRANSACTION_getRand:I = 0x13

.field static final TRANSACTION_getSubscriberId:I = 0x3

.field static final TRANSACTION_getVoiceMailAlphaTag:I = 0xb

.field static final TRANSACTION_getVoiceMailNumber:I = 0x8

.field static final TRANSACTION_hasIsim:I = 0x11

.field static final TRANSACTION_isGbaSupported:I = 0x12


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.android.internal.telephony.IPhoneSubInfo"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IPhoneSubInfo;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.android.internal.telephony.IPhoneSubInfo"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/IPhoneSubInfo;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/android/internal/telephony/IPhoneSubInfo;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/IPhoneSubInfo$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 42
    sparse-switch p1, :sswitch_data_18c

    #@5
    .line 228
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v3

    #@9
    :goto_9
    return v3

    #@a
    .line 46
    :sswitch_a
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 51
    :sswitch_10
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@12
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getDeviceId()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 53
    .local v1, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 54
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    goto :goto_9

    #@20
    .line 59
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_20
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@22
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 60
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getDeviceSvn()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    .line 61
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c
    .line 62
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2f
    goto :goto_9

    #@30
    .line 67
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_30
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@32
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 68
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getSubscriberId()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    .line 69
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 70
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3f
    goto :goto_9

    #@40
    .line 75
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_40
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@42
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45
    .line 76
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getIccSerialNumber()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    .line 77
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c
    .line 78
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4f
    goto :goto_9

    #@50
    .line 83
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_50
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@52
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@55
    .line 84
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getLine1Number()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    .line 85
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    .line 86
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5f
    goto :goto_9

    #@60
    .line 91
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_60
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@62
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@65
    .line 92
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getLine1AlphaTag()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    .line 93
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6c
    .line 94
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6f
    goto :goto_9

    #@70
    .line 99
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_70
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@72
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@75
    .line 100
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getMsisdn()Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    .line 101
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c
    .line 102
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7f
    goto :goto_9

    #@80
    .line 107
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_80
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@82
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 108
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getVoiceMailNumber()Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    .line 109
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    .line 110
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@8f
    goto/16 :goto_9

    #@91
    .line 115
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_91
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@93
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@96
    .line 116
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;

    #@99
    move-result-object v1

    #@9a
    .line 117
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9d
    .line 118
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a0
    goto/16 :goto_9

    #@a2
    .line 123
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_a2
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@a4
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a7
    .line 124
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getCompleteVoiceMailNumber()Ljava/lang/String;

    #@aa
    move-result-object v1

    #@ab
    .line 125
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ae
    .line 126
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b1
    goto/16 :goto_9

    #@b3
    .line 131
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_b3
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@b5
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b8
    .line 132
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getVoiceMailAlphaTag()Ljava/lang/String;

    #@bb
    move-result-object v1

    #@bc
    .line 133
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@bf
    .line 134
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c2
    goto/16 :goto_9

    #@c4
    .line 139
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_c4
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@c6
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c9
    .line 140
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getIsimImpi()Ljava/lang/String;

    #@cc
    move-result-object v1

    #@cd
    .line 141
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d0
    .line 142
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d3
    goto/16 :goto_9

    #@d5
    .line 147
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_d5
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@d7
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@da
    .line 148
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getIsimDomain()Ljava/lang/String;

    #@dd
    move-result-object v1

    #@de
    .line 149
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e1
    .line 150
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e4
    goto/16 :goto_9

    #@e6
    .line 155
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_e6
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@e8
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@eb
    .line 156
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getIsimImpu()[Ljava/lang/String;

    #@ee
    move-result-object v1

    #@ef
    .line 157
    .local v1, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f2
    .line 158
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@f5
    goto/16 :goto_9

    #@f7
    .line 163
    .end local v1           #_result:[Ljava/lang/String;
    :sswitch_f7
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@f9
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fc
    .line 165
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ff
    move-result v0

    #@100
    .line 166
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getDeviceId_type(I)Ljava/lang/String;

    #@103
    move-result-object v1

    #@104
    .line 167
    .local v1, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@107
    .line 168
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10a
    goto/16 :goto_9

    #@10c
    .line 173
    .end local v0           #_arg0:I
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_10c
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@10e
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@111
    .line 174
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getMSIN()Ljava/lang/String;

    #@114
    move-result-object v1

    #@115
    .line 175
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@118
    .line 176
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11b
    goto/16 :goto_9

    #@11d
    .line 181
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_11d
    const-string v4, "com.android.internal.telephony.IPhoneSubInfo"

    #@11f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@122
    .line 182
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->hasIsim()Z

    #@125
    move-result v1

    #@126
    .line 183
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@129
    .line 184
    if-eqz v1, :cond_12c

    #@12b
    move v2, v3

    #@12c
    :cond_12c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@12f
    goto/16 :goto_9

    #@131
    .line 189
    .end local v1           #_result:Z
    :sswitch_131
    const-string v4, "com.android.internal.telephony.IPhoneSubInfo"

    #@133
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@136
    .line 190
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->isGbaSupported()Z

    #@139
    move-result v1

    #@13a
    .line 191
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13d
    .line 192
    if-eqz v1, :cond_140

    #@13f
    move v2, v3

    #@140
    :cond_140
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@143
    goto/16 :goto_9

    #@145
    .line 197
    .end local v1           #_result:Z
    :sswitch_145
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@147
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14a
    .line 198
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getRand()[B

    #@14d
    move-result-object v1

    #@14e
    .line 199
    .local v1, _result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@151
    .line 200
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@154
    goto/16 :goto_9

    #@156
    .line 205
    .end local v1           #_result:[B
    :sswitch_156
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@158
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15b
    .line 206
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getBtid()Ljava/lang/String;

    #@15e
    move-result-object v1

    #@15f
    .line 207
    .local v1, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@162
    .line 208
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@165
    goto/16 :goto_9

    #@167
    .line 213
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_167
    const-string v2, "com.android.internal.telephony.IPhoneSubInfo"

    #@169
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16c
    .line 214
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getKeyLifetime()Ljava/lang/String;

    #@16f
    move-result-object v1

    #@170
    .line 215
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@173
    .line 216
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@176
    goto/16 :goto_9

    #@178
    .line 221
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_178
    const-string v4, "com.android.internal.telephony.IPhoneSubInfo"

    #@17a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17d
    .line 222
    invoke-virtual {p0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->getIPPhoneState()Z

    #@180
    move-result v1

    #@181
    .line 223
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@184
    .line 224
    if-eqz v1, :cond_187

    #@186
    move v2, v3

    #@187
    :cond_187
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18a
    goto/16 :goto_9

    #@18c
    .line 42
    :sswitch_data_18c
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_30
        0x4 -> :sswitch_40
        0x5 -> :sswitch_50
        0x6 -> :sswitch_60
        0x7 -> :sswitch_70
        0x8 -> :sswitch_80
        0x9 -> :sswitch_91
        0xa -> :sswitch_a2
        0xb -> :sswitch_b3
        0xc -> :sswitch_c4
        0xd -> :sswitch_d5
        0xe -> :sswitch_e6
        0xf -> :sswitch_f7
        0x10 -> :sswitch_10c
        0x11 -> :sswitch_11d
        0x12 -> :sswitch_131
        0x13 -> :sswitch_145
        0x14 -> :sswitch_156
        0x15 -> :sswitch_167
        0x16 -> :sswitch_178
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
