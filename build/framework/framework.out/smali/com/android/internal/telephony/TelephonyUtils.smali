.class public final Lcom/android/internal/telephony/TelephonyUtils;
.super Ljava/lang/Object;
.source "TelephonyUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/TelephonyUtils$Roaming;
    }
.end annotation


# static fields
.field public static final GMM_ATTACH_MODE:Ljava/lang/String; = "gmm_attach_mode"

.field public static final KT_SHOW_ROAMING_PREFIX:Ljava/lang/String; = "show_roaming_prefix"

.field private static final LOG_TAG:Ljava/lang/String; = "TelephonyUtils"

.field public static final OEM_RAD_DIALER_POPUP:Ljava/lang/String; = "oem_rad_dialer_popup"

.field public static final OEM_RAD_SETTING:Ljava/lang/String; = "oem_rad_setting"

.field public static final OEM_RAD_TEST:Ljava/lang/String; = "oem_rad_test"

.field public static final OEM_RAD_TEST_RCV_PRFIX:Ljava/lang/String; = "oem_rad_test_rcv_prfix"

.field private static final WFC_FEATURE:Z

.field private static mIPPhoneRegistered:Z

.field public static persoLockChecked:Z

.field private static sContext:Landroid/content/Context;

.field private static sEnglishStringMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sKoreanStringMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 53
    new-instance v0, Ljava/util/HashMap;

    #@3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@6
    sput-object v0, Lcom/android/internal/telephony/TelephonyUtils;->sKoreanStringMap:Ljava/util/HashMap;

    #@8
    .line 54
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    sput-object v0, Lcom/android/internal/telephony/TelephonyUtils;->sEnglishStringMap:Ljava/util/HashMap;

    #@f
    .line 77
    sput-boolean v2, Lcom/android/internal/telephony/TelephonyUtils;->persoLockChecked:Z

    #@11
    .line 258
    sput-boolean v2, Lcom/android/internal/telephony/TelephonyUtils;->mIPPhoneRegistered:Z

    #@13
    .line 272
    const-string v0, "ims"

    #@15
    const-string/jumbo v1, "ro.product.ims"

    #@18
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_26

    #@22
    .line 273
    const/4 v0, 0x1

    #@23
    sput-boolean v0, Lcom/android/internal/telephony/TelephonyUtils;->WFC_FEATURE:Z

    #@25
    .line 276
    :goto_25
    return-void

    #@26
    .line 275
    :cond_26
    sput-boolean v2, Lcom/android/internal/telephony/TelephonyUtils;->WFC_FEATURE:Z

    #@28
    goto :goto_25
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 194
    return-void
.end method

.method private static ChildNode(Lorg/w3c/dom/Node;Ljava/util/HashMap;)V
    .registers 9
    .parameter "node"
    .parameter "map"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 134
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 135
    .local v1, Node_Name:Ljava/lang/String;
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    .line 136
    .local v2, Text_Content:Ljava/lang/String;
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    #@c
    move-result-object v5

    #@d
    invoke-interface {v5}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    #@10
    move-result v0

    #@11
    .line 143
    .local v0, Attr_len:I
    const/4 v5, 0x1

    #@12
    if-eq v0, v5, :cond_31

    #@14
    new-instance v5, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v6, "ERROR!!! Attr_len musb be 1. ("

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    const-string v6, ")"

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-static {v5}, Lcom/android/internal/telephony/TelephonyUtils;->log(Ljava/lang/String;)V

    #@30
    .line 153
    :cond_30
    :goto_30
    return-void

    #@31
    .line 145
    :cond_31
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    #@34
    move-result-object v5

    #@35
    invoke-interface {v5, v6}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    #@38
    move-result-object v5

    #@39
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    .line 146
    .local v3, attrName:Ljava/lang/String;
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    #@40
    move-result-object v5

    #@41
    invoke-interface {v5, v6}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    #@44
    move-result-object v5

    #@45
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    .line 148
    .local v4, attrValue:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4c
    move-result v5

    #@4d
    if-nez v5, :cond_30

    #@4f
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@52
    move-result v5

    #@53
    if-nez v5, :cond_30

    #@55
    .line 150
    invoke-virtual {p1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    goto :goto_30
.end method

.method public static getIPPhoneState()Z
    .registers 1

    #@0
    .prologue
    .line 261
    sget-boolean v0, Lcom/android/internal/telephony/TelephonyUtils;->mIPPhoneRegistered:Z

    #@2
    return v0
.end method

.method public static getTelephonyString(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "key"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 157
    const-string v6, ""

    #@3
    .line 159
    .local v6, retValue:Ljava/lang/String;
    sget-object v9, Lcom/android/internal/telephony/TelephonyUtils;->sContext:Landroid/content/Context;

    #@5
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v9

    #@9
    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@c
    move-result-object v9

    #@d
    iget-object v4, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@f
    .line 160
    .local v4, locale:Ljava/util/Locale;
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 161
    .local v2, lancode:Ljava/lang/String;
    const-string/jumbo v9, "ko"

    #@16
    invoke-virtual {v2, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@19
    move-result v9

    #@1a
    if-eqz v9, :cond_96

    #@1c
    .line 162
    sget-object v9, Lcom/android/internal/telephony/TelephonyUtils;->sKoreanStringMap:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v9, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v8

    #@22
    check-cast v8, Ljava/lang/String;

    #@24
    .line 163
    .local v8, value:Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@27
    move-result v9

    #@28
    if-eqz v9, :cond_32

    #@2a
    .line 164
    sget-object v9, Lcom/android/internal/telephony/TelephonyUtils;->sEnglishStringMap:Ljava/util/HashMap;

    #@2c
    invoke-virtual {v9, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v8

    #@30
    .end local v8           #value:Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    #@32
    .line 171
    .restart local v8       #value:Ljava/lang/String;
    :cond_32
    :goto_32
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@35
    move-result v9

    #@36
    if-eqz v9, :cond_3a

    #@38
    .line 172
    const-string v8, ""

    #@3a
    .line 174
    :cond_3a
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@3d
    move-result-object v8

    #@3e
    .line 175
    const-string v9, "\""

    #@40
    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@43
    move-result v9

    #@44
    if-eqz v9, :cond_4b

    #@46
    .line 176
    const/4 v9, 0x1

    #@47
    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@4a
    move-result-object v8

    #@4b
    .line 177
    :cond_4b
    const-string v9, "\""

    #@4d
    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@50
    move-result v9

    #@51
    if-eqz v9, :cond_5d

    #@53
    .line 178
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@56
    move-result v9

    #@57
    add-int/lit8 v9, v9, -0x1

    #@59
    invoke-virtual {v8, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5c
    move-result-object v8

    #@5d
    .line 180
    :cond_5d
    const-string v9, "\\\\[n]"

    #@5f
    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@62
    move-result-object v7

    #@63
    .line 181
    .local v7, splitStr:[Ljava/lang/String;
    move-object v0, v7

    #@64
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@65
    .local v3, len$:I
    const/4 v1, 0x0

    #@66
    .local v1, i$:I
    :goto_66
    if-ge v1, v3, :cond_ad

    #@68
    aget-object v5, v0, v1

    #@6a
    .line 182
    .local v5, paramPair:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v9

    #@73
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v9

    #@77
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v6

    #@7b
    .line 183
    new-instance v9, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v9

    #@84
    const-string/jumbo v10, "line.separator"

    #@87
    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@8a
    move-result-object v10

    #@8b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v9

    #@8f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v6

    #@93
    .line 181
    add-int/lit8 v1, v1, 0x1

    #@95
    goto :goto_66

    #@96
    .line 166
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v3           #len$:I
    .end local v5           #paramPair:Ljava/lang/String;
    .end local v7           #splitStr:[Ljava/lang/String;
    .end local v8           #value:Ljava/lang/String;
    :cond_96
    sget-object v9, Lcom/android/internal/telephony/TelephonyUtils;->sEnglishStringMap:Ljava/util/HashMap;

    #@98
    invoke-virtual {v9, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9b
    move-result-object v8

    #@9c
    check-cast v8, Ljava/lang/String;

    #@9e
    .line 167
    .restart local v8       #value:Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a1
    move-result v9

    #@a2
    if-eqz v9, :cond_32

    #@a4
    .line 168
    sget-object v9, Lcom/android/internal/telephony/TelephonyUtils;->sKoreanStringMap:Ljava/util/HashMap;

    #@a6
    invoke-virtual {v9, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a9
    move-result-object v8

    #@aa
    .end local v8           #value:Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    #@ac
    .restart local v8       #value:Ljava/lang/String;
    goto :goto_32

    #@ad
    .line 186
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #i$:I
    .restart local v3       #len$:I
    .restart local v7       #splitStr:[Ljava/lang/String;
    :cond_ad
    const-string/jumbo v9, "line.separator"

    #@b0
    invoke-static {v9}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@b3
    move-result-object v9

    #@b4
    invoke-virtual {v6, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@b7
    move-result v9

    #@b8
    if-eqz v9, :cond_c4

    #@ba
    .line 187
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@bd
    move-result v9

    #@be
    add-int/lit8 v9, v9, -0x1

    #@c0
    invoke-virtual {v6, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c3
    move-result-object v6

    #@c4
    .line 190
    :cond_c4
    return-object v6
.end method

.method public static getUsimPersoImsi(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 253
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    const-string/jumbo v1, "usim_perso_imsi"

    #@7
    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static final isFactoryMode()Z
    .registers 2

    #@0
    .prologue
    .line 221
    const-string/jumbo v1, "ro.factorytest"

    #@3
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 222
    .local v0, factoryTestStr:Ljava/lang/String;
    if-eqz v0, :cond_13

    #@9
    const-string v1, "2"

    #@b
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_13

    #@11
    .line 223
    const/4 v1, 0x1

    #@12
    .line 225
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method public static isIPPhoneSupported()Z
    .registers 1

    #@0
    .prologue
    .line 279
    sget-boolean v0, Lcom/android/internal/telephony/TelephonyUtils;->WFC_FEATURE:Z

    #@2
    return v0
.end method

.method public static isKTOTAMode(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 242
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v2

    #@6
    const-string/jumbo v3, "ota_usim_running"

    #@9
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v2

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method public static final isQCRIL()Z
    .registers 2

    #@0
    .prologue
    .line 214
    const/4 v0, 0x0

    #@1
    const-string v1, "Is_QCRIL"

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_b

    #@9
    .line 215
    const/4 v0, 0x1

    #@a
    .line 217
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public static isUsimPersoLocked(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 235
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v2

    #@6
    const-string/jumbo v3, "usim_perso_locked"

    #@9
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v2

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 284
    const-string v0, "TelephonyUtils"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 285
    return-void
.end method

.method public static final parseKrTelephonyStringXML(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 89
    sput-object p0, Lcom/android/internal/telephony/TelephonyUtils;->sContext:Landroid/content/Context;

    #@2
    .line 92
    :try_start_2
    const-string v4, "/system/etc/kr_telephony_string.xml"

    #@4
    .line 93
    .local v4, filepath:Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    #@7
    move-result-object v2

    #@8
    .line 94
    .local v2, docFactory:Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    #@b
    move-result-object v1

    #@c
    .line 95
    .local v1, docBuilder:Ljavax/xml/parsers/DocumentBuilder;
    new-instance v5, Ljava/io/File;

    #@e
    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    invoke-virtual {v1, v5}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    #@14
    move-result-object v0

    #@15
    .line 97
    .local v0, doc:Lorg/w3c/dom/Document;
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    #@18
    move-result-object v5

    #@19
    invoke-interface {v5}, Lorg/w3c/dom/Element;->normalize()V

    #@1c
    .line 102
    const-string v5, "Korean"

    #@1e
    sget-object v6, Lcom/android/internal/telephony/TelephonyUtils;->sKoreanStringMap:Ljava/util/HashMap;

    #@20
    invoke-static {v5, v0, v6}, Lcom/android/internal/telephony/TelephonyUtils;->viewElement(Ljava/lang/String;Lorg/w3c/dom/Document;Ljava/util/HashMap;)V

    #@23
    .line 103
    const-string v5, "English"

    #@25
    sget-object v6, Lcom/android/internal/telephony/TelephonyUtils;->sEnglishStringMap:Ljava/util/HashMap;

    #@27
    invoke-static {v5, v0, v6}, Lcom/android/internal/telephony/TelephonyUtils;->viewElement(Ljava/lang/String;Lorg/w3c/dom/Document;Ljava/util/HashMap;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2a} :catch_2b

    #@2a
    .line 108
    .end local v0           #doc:Lorg/w3c/dom/Document;
    .end local v1           #docBuilder:Ljavax/xml/parsers/DocumentBuilder;
    .end local v2           #docFactory:Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v4           #filepath:Ljava/lang/String;
    :goto_2a
    return-void

    #@2b
    .line 105
    :catch_2b
    move-exception v3

    #@2c
    .line 106
    .local v3, e:Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v6, "Exception create KR Telephony String XML : "

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-static {v5}, Lcom/android/internal/telephony/TelephonyUtils;->log(Ljava/lang/String;)V

    #@42
    goto :goto_2a
.end method

.method public static setIPPhoneState(Z)V
    .registers 1
    .parameter "state"

    #@0
    .prologue
    .line 265
    sput-boolean p0, Lcom/android/internal/telephony/TelephonyUtils;->mIPPhoneRegistered:Z

    #@2
    .line 266
    return-void
.end method

.method private static viewElement(Ljava/lang/String;Lorg/w3c/dom/Document;Ljava/util/HashMap;)V
    .registers 10
    .parameter "NodeName"
    .parameter "doc"
    .parameter "map"

    #@0
    .prologue
    .line 111
    invoke-interface {p1, p0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    #@3
    move-result-object v4

    #@4
    .line 112
    .local v4, nodeLst:Lorg/w3c/dom/NodeList;
    const/4 v1, 0x0

    #@5
    .line 114
    .local v1, cnt:I
    const-string v5, "================================"

    #@7
    invoke-static {v5}, Lcom/android/internal/telephony/TelephonyUtils;->log(Ljava/lang/String;)V

    #@a
    .line 117
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    #@e
    move-result v5

    #@f
    if-ge v2, v5, :cond_4b

    #@11
    .line 119
    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    #@14
    move-result-object v5

    #@15
    check-cast v5, Lorg/w3c/dom/Element;

    #@17
    invoke-interface {v5}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    #@1a
    move-result-object v0

    #@1b
    .line 121
    .local v0, clist:Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    #@1c
    .local v3, j:I
    :goto_1c
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    #@1f
    move-result v5

    #@20
    if-ge v3, v5, :cond_48

    #@22
    .line 123
    invoke-interface {v0, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    #@25
    move-result-object v5

    #@26
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeType()S

    #@29
    move-result v5

    #@2a
    const/4 v6, 0x1

    #@2b
    if-ne v5, v6, :cond_45

    #@2d
    .line 124
    invoke-interface {v0, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    #@30
    move-result-object v5

    #@31
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    const-string/jumbo v6, "string"

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_45

    #@3e
    .line 126
    invoke-interface {v0, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    #@41
    move-result-object v5

    #@42
    invoke-static {v5, p2}, Lcom/android/internal/telephony/TelephonyUtils;->ChildNode(Lorg/w3c/dom/Node;Ljava/util/HashMap;)V

    #@45
    .line 121
    :cond_45
    add-int/lit8 v3, v3, 0x1

    #@47
    goto :goto_1c

    #@48
    .line 117
    :cond_48
    add-int/lit8 v2, v2, 0x1

    #@4a
    goto :goto_b

    #@4b
    .line 130
    .end local v0           #clist:Lorg/w3c/dom/NodeList;
    .end local v3           #j:I
    :cond_4b
    const-string v5, "================================"

    #@4d
    invoke-static {v5}, Lcom/android/internal/telephony/TelephonyUtils;->log(Ljava/lang/String;)V

    #@50
    .line 131
    return-void
.end method
