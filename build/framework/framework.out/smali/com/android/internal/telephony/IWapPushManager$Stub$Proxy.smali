.class Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IWapPushManager.java"

# interfaces
.implements Lcom/android/internal/telephony/IWapPushManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IWapPushManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 131
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 132
    iput-object p1, p0, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 133
    return-void
.end method


# virtual methods
.method public addPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Z
    .registers 15
    .parameter "x_app_id"
    .parameter "content_type"
    .parameter "package_name"
    .parameter "class_name"
    .parameter "app_type"
    .parameter "need_signature"
    .parameter "further_processing"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 179
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 180
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 183
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.telephony.IWapPushManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 184
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 185
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    .line 186
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@18
    .line 187
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 188
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 189
    if-eqz p6, :cond_41

    #@20
    move v4, v2

    #@21
    :goto_21
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 190
    if-eqz p7, :cond_43

    #@26
    move v4, v2

    #@27
    :goto_27
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 191
    iget-object v4, p0, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2c
    const/4 v5, 0x2

    #@2d
    const/4 v6, 0x0

    #@2e
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@31
    .line 192
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@34
    .line 193
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_37
    .catchall {:try_start_a .. :try_end_37} :catchall_47

    #@37
    move-result v4

    #@38
    if-eqz v4, :cond_45

    #@3a
    .line 196
    .local v2, _result:Z
    :goto_3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 199
    return v2

    #@41
    .end local v2           #_result:Z
    :cond_41
    move v4, v3

    #@42
    .line 189
    goto :goto_21

    #@43
    :cond_43
    move v4, v3

    #@44
    .line 190
    goto :goto_27

    #@45
    :cond_45
    move v2, v3

    #@46
    .line 193
    goto :goto_3a

    #@47
    .line 196
    :catchall_47
    move-exception v3

    #@48
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4b
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4e
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public deletePackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "x_app_id"
    .parameter "content_type"
    .parameter "package_name"
    .parameter "class_name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 235
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 236
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 239
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.IWapPushManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 240
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 241
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 242
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 243
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 244
    iget-object v3, p0, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v4, 0x4

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 245
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 246
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_9 .. :try_end_27} :catchall_32

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2b

    #@2a
    const/4 v2, 0x1

    #@2b
    .line 249
    .local v2, _result:Z
    :cond_2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 250
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 252
    return v2

    #@32
    .line 249
    .end local v2           #_result:Z
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 250
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 140
    const-string v0, "com.android.internal.telephony.IWapPushManager"

    #@2
    return-object v0
.end method

.method public processMessage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)I
    .registers 10
    .parameter "app_id"
    .parameter "content_type"
    .parameter "intent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 148
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 149
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 152
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.IWapPushManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 153
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 154
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 155
    if-eqz p3, :cond_32

    #@15
    .line 156
    const/4 v3, 0x1

    #@16
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 157
    const/4 v3, 0x0

    #@1a
    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 162
    :goto_1d
    iget-object v3, p0, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/4 v4, 0x1

    #@20
    const/4 v5, 0x0

    #@21
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 163
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 164
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2a
    .catchall {:try_start_8 .. :try_end_2a} :catchall_37

    #@2a
    move-result v2

    #@2b
    .line 167
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 168
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 170
    return v2

    #@32
    .line 160
    .end local v2           #_result:I
    :cond_32
    const/4 v3, 0x0

    #@33
    :try_start_33
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_37

    #@36
    goto :goto_1d

    #@37
    .line 167
    :catchall_37
    move-exception v3

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 168
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v3
.end method

.method public updatePackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Z
    .registers 15
    .parameter "x_app_id"
    .parameter "content_type"
    .parameter "package_name"
    .parameter "class_name"
    .parameter "app_type"
    .parameter "need_signature"
    .parameter "further_processing"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 207
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 208
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 211
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.telephony.IWapPushManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 212
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 213
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    .line 214
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@18
    .line 215
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 216
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 217
    if-eqz p6, :cond_41

    #@20
    move v4, v2

    #@21
    :goto_21
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 218
    if-eqz p7, :cond_43

    #@26
    move v4, v2

    #@27
    :goto_27
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 219
    iget-object v4, p0, Lcom/android/internal/telephony/IWapPushManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2c
    const/4 v5, 0x3

    #@2d
    const/4 v6, 0x0

    #@2e
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@31
    .line 220
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@34
    .line 221
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_37
    .catchall {:try_start_a .. :try_end_37} :catchall_47

    #@37
    move-result v4

    #@38
    if-eqz v4, :cond_45

    #@3a
    .line 224
    .local v2, _result:Z
    :goto_3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 227
    return v2

    #@41
    .end local v2           #_result:Z
    :cond_41
    move v4, v3

    #@42
    .line 217
    goto :goto_21

    #@43
    :cond_43
    move v4, v3

    #@44
    .line 218
    goto :goto_27

    #@45
    :cond_45
    move v2, v3

    #@46
    .line 221
    goto :goto_3a

    #@47
    .line 224
    :catchall_47
    move-exception v3

    #@48
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4b
    .line 225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4e
    throw v3
.end method
