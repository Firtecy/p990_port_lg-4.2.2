.class public final enum Lcom/android/internal/telephony/PhoneConstants$DataState;
.super Ljava/lang/Enum;
.source "PhoneConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PhoneConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/PhoneConstants$DataState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/PhoneConstants$DataState;

.field public static final enum CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

.field public static final enum CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

.field public static final enum DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

.field public static final enum SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 54
    new-instance v0, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@6
    const-string v1, "CONNECTED"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/PhoneConstants$DataState;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@d
    new-instance v0, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@f
    const-string v1, "CONNECTING"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/PhoneConstants$DataState;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@16
    new-instance v0, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@18
    const-string v1, "DISCONNECTED"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/PhoneConstants$DataState;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@1f
    new-instance v0, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@21
    const-string v1, "SUSPENDED"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/PhoneConstants$DataState;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@28
    .line 53
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2b
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 53
    const-class v0, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 1

    #@0
    .prologue
    .line 53
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/PhoneConstants$DataState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@8
    return-object v0
.end method
