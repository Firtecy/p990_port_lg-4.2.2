.class public abstract Lcom/android/internal/telephony/IPhoneStateListener$Stub;
.super Landroid/os/Binder;
.source "IPhoneStateListener.java"

# interfaces
.implements Lcom/android/internal/telephony/IPhoneStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IPhoneStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.IPhoneStateListener"

.field static final TRANSACTION_onCallForwardingIndicatorChanged:I = 0x4

.field static final TRANSACTION_onCallStateChanged:I = 0x6

.field static final TRANSACTION_onCellInfoChanged:I = 0xb

.field static final TRANSACTION_onCellLocationChanged:I = 0x5

.field static final TRANSACTION_onDataActivity:I = 0x8

.field static final TRANSACTION_onDataConnectionStateChanged:I = 0x7

.field static final TRANSACTION_onMessageWaitingIndicatorChanged:I = 0x3

.field static final TRANSACTION_onOtaspChanged:I = 0xa

.field static final TRANSACTION_onServiceStateChanged:I = 0x1

.field static final TRANSACTION_onSignalStrengthChanged:I = 0x2

.field static final TRANSACTION_onSignalStrengthsChanged:I = 0x9


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.telephony.IPhoneStateListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IPhoneStateListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.telephony.IPhoneStateListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/IPhoneStateListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/telephony/IPhoneStateListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_d8

    #@5
    .line 153
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v3

    #@9
    :goto_9
    return v3

    #@a
    .line 42
    :sswitch_a
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_27

    #@1b
    .line 50
    sget-object v4, Landroid/telephony/ServiceState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/telephony/ServiceState;

    #@23
    .line 55
    .local v0, _arg0:Landroid/telephony/ServiceState;
    :goto_23
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onServiceStateChanged(Landroid/telephony/ServiceState;)V

    #@26
    goto :goto_9

    #@27
    .line 53
    .end local v0           #_arg0:Landroid/telephony/ServiceState;
    :cond_27
    const/4 v0, 0x0

    #@28
    .restart local v0       #_arg0:Landroid/telephony/ServiceState;
    goto :goto_23

    #@29
    .line 60
    .end local v0           #_arg0:Landroid/telephony/ServiceState;
    :sswitch_29
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@2b
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v0

    #@32
    .line 63
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onSignalStrengthChanged(I)V

    #@35
    goto :goto_9

    #@36
    .line 68
    .end local v0           #_arg0:I
    :sswitch_36
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@38
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3e
    move-result v4

    #@3f
    if-eqz v4, :cond_42

    #@41
    move v0, v3

    #@42
    .line 71
    .local v0, _arg0:Z
    :cond_42
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onMessageWaitingIndicatorChanged(Z)V

    #@45
    goto :goto_9

    #@46
    .line 76
    .end local v0           #_arg0:Z
    :sswitch_46
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@48
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b
    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v4

    #@4f
    if-eqz v4, :cond_52

    #@51
    move v0, v3

    #@52
    .line 79
    .restart local v0       #_arg0:Z
    :cond_52
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onCallForwardingIndicatorChanged(Z)V

    #@55
    goto :goto_9

    #@56
    .line 84
    .end local v0           #_arg0:Z
    :sswitch_56
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@58
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5e
    move-result v4

    #@5f
    if-eqz v4, :cond_6d

    #@61
    .line 87
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@63
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@66
    move-result-object v0

    #@67
    check-cast v0, Landroid/os/Bundle;

    #@69
    .line 92
    .local v0, _arg0:Landroid/os/Bundle;
    :goto_69
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onCellLocationChanged(Landroid/os/Bundle;)V

    #@6c
    goto :goto_9

    #@6d
    .line 90
    .end local v0           #_arg0:Landroid/os/Bundle;
    :cond_6d
    const/4 v0, 0x0

    #@6e
    .restart local v0       #_arg0:Landroid/os/Bundle;
    goto :goto_69

    #@6f
    .line 97
    .end local v0           #_arg0:Landroid/os/Bundle;
    :sswitch_6f
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@71
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@74
    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v0

    #@78
    .line 101
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7b
    move-result-object v2

    #@7c
    .line 102
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onCallStateChanged(ILjava/lang/String;)V

    #@7f
    goto :goto_9

    #@80
    .line 107
    .end local v0           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_80
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@82
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@88
    move-result v0

    #@89
    .line 111
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8c
    move-result v2

    #@8d
    .line 112
    .local v2, _arg1:I
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onDataConnectionStateChanged(II)V

    #@90
    goto/16 :goto_9

    #@92
    .line 117
    .end local v0           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_92
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@94
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@97
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9a
    move-result v0

    #@9b
    .line 120
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onDataActivity(I)V

    #@9e
    goto/16 :goto_9

    #@a0
    .line 125
    .end local v0           #_arg0:I
    :sswitch_a0
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@a2
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a5
    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v4

    #@a9
    if-eqz v4, :cond_b8

    #@ab
    .line 128
    sget-object v4, Landroid/telephony/SignalStrength;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ad
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b0
    move-result-object v0

    #@b1
    check-cast v0, Landroid/telephony/SignalStrength;

    #@b3
    .line 133
    .local v0, _arg0:Landroid/telephony/SignalStrength;
    :goto_b3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V

    #@b6
    goto/16 :goto_9

    #@b8
    .line 131
    .end local v0           #_arg0:Landroid/telephony/SignalStrength;
    :cond_b8
    const/4 v0, 0x0

    #@b9
    .restart local v0       #_arg0:Landroid/telephony/SignalStrength;
    goto :goto_b3

    #@ba
    .line 138
    .end local v0           #_arg0:Landroid/telephony/SignalStrength;
    :sswitch_ba
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@bc
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 140
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c2
    move-result v0

    #@c3
    .line 141
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onOtaspChanged(I)V

    #@c6
    goto/16 :goto_9

    #@c8
    .line 146
    .end local v0           #_arg0:I
    :sswitch_c8
    const-string v4, "com.android.internal.telephony.IPhoneStateListener"

    #@ca
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cd
    .line 148
    sget-object v4, Landroid/telephony/CellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@cf
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@d2
    move-result-object v1

    #@d3
    .line 149
    .local v1, _arg0:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->onCellInfoChanged(Ljava/util/List;)V

    #@d6
    goto/16 :goto_9

    #@d8
    .line 38
    :sswitch_data_d8
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_29
        0x3 -> :sswitch_36
        0x4 -> :sswitch_46
        0x5 -> :sswitch_56
        0x6 -> :sswitch_6f
        0x7 -> :sswitch_80
        0x8 -> :sswitch_92
        0x9 -> :sswitch_a0
        0xa -> :sswitch_ba
        0xb -> :sswitch_c8
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
