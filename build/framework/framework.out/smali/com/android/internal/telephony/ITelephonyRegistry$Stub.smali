.class public abstract Lcom/android/internal/telephony/ITelephonyRegistry$Stub;
.super Landroid/os/Binder;
.source "ITelephonyRegistry.java"

# interfaces
.implements Lcom/android/internal/telephony/ITelephonyRegistry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/ITelephonyRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/ITelephonyRegistry$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.ITelephonyRegistry"

.field static final TRANSACTION_listen:I = 0x1

.field static final TRANSACTION_notifyCallForwardingChanged:I = 0x6

.field static final TRANSACTION_notifyCallState:I = 0x2

.field static final TRANSACTION_notifyCellInfo:I = 0xc

.field static final TRANSACTION_notifyCellLocation:I = 0xa

.field static final TRANSACTION_notifyDataActivity:I = 0x7

.field static final TRANSACTION_notifyDataConnection:I = 0x8

.field static final TRANSACTION_notifyDataConnectionFailed:I = 0x9

.field static final TRANSACTION_notifyMessageWaitingChanged:I = 0x5

.field static final TRANSACTION_notifyOtaspChanged:I = 0xb

.field static final TRANSACTION_notifyServiceState:I = 0x3

.field static final TRANSACTION_notifySignalStrength:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephonyRegistry;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.android.internal.telephony.ITelephonyRegistry"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/ITelephonyRegistry;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/android/internal/telephony/ITelephonyRegistry;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/ITelephonyRegistry$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 17
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_178

    #@3
    .line 207
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 42
    :sswitch_8
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 43
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 47
    :sswitch_f
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    .line 51
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Lcom/android/internal/telephony/IPhoneStateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IPhoneStateListener;

    #@1f
    move-result-object v2

    #@20
    .line 53
    .local v2, _arg1:Lcom/android/internal/telephony/IPhoneStateListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v3

    #@24
    .line 55
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_33

    #@2a
    const/4 v4, 0x1

    #@2b
    .line 56
    .local v4, _arg3:Z
    :goto_2b
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->listen(Ljava/lang/String;Lcom/android/internal/telephony/IPhoneStateListener;IZ)V

    #@2e
    .line 57
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31
    .line 58
    const/4 v0, 0x1

    #@32
    goto :goto_7

    #@33
    .line 55
    .end local v4           #_arg3:Z
    :cond_33
    const/4 v4, 0x0

    #@34
    goto :goto_2b

    #@35
    .line 62
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Lcom/android/internal/telephony/IPhoneStateListener;
    .end local v3           #_arg2:I
    :sswitch_35
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@37
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v1

    #@3e
    .line 66
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    .line 67
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyCallState(ILjava/lang/String;)V

    #@45
    .line 68
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    .line 69
    const/4 v0, 0x1

    #@49
    goto :goto_7

    #@4a
    .line 73
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_4a
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@4c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_65

    #@55
    .line 76
    sget-object v0, Landroid/telephony/ServiceState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@57
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5a
    move-result-object v1

    #@5b
    check-cast v1, Landroid/telephony/ServiceState;

    #@5d
    .line 81
    .local v1, _arg0:Landroid/telephony/ServiceState;
    :goto_5d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyServiceState(Landroid/telephony/ServiceState;)V

    #@60
    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@63
    .line 83
    const/4 v0, 0x1

    #@64
    goto :goto_7

    #@65
    .line 79
    .end local v1           #_arg0:Landroid/telephony/ServiceState;
    :cond_65
    const/4 v1, 0x0

    #@66
    .restart local v1       #_arg0:Landroid/telephony/ServiceState;
    goto :goto_5d

    #@67
    .line 87
    .end local v1           #_arg0:Landroid/telephony/ServiceState;
    :sswitch_67
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@69
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v0

    #@70
    if-eqz v0, :cond_82

    #@72
    .line 90
    sget-object v0, Landroid/telephony/SignalStrength;->CREATOR:Landroid/os/Parcelable$Creator;

    #@74
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@77
    move-result-object v1

    #@78
    check-cast v1, Landroid/telephony/SignalStrength;

    #@7a
    .line 95
    .local v1, _arg0:Landroid/telephony/SignalStrength;
    :goto_7a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifySignalStrength(Landroid/telephony/SignalStrength;)V

    #@7d
    .line 96
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@80
    .line 97
    const/4 v0, 0x1

    #@81
    goto :goto_7

    #@82
    .line 93
    .end local v1           #_arg0:Landroid/telephony/SignalStrength;
    :cond_82
    const/4 v1, 0x0

    #@83
    .restart local v1       #_arg0:Landroid/telephony/SignalStrength;
    goto :goto_7a

    #@84
    .line 101
    .end local v1           #_arg0:Landroid/telephony/SignalStrength;
    :sswitch_84
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@86
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@89
    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8c
    move-result v0

    #@8d
    if-eqz v0, :cond_99

    #@8f
    const/4 v1, 0x1

    #@90
    .line 104
    .local v1, _arg0:Z
    :goto_90
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyMessageWaitingChanged(Z)V

    #@93
    .line 105
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@96
    .line 106
    const/4 v0, 0x1

    #@97
    goto/16 :goto_7

    #@99
    .line 103
    .end local v1           #_arg0:Z
    :cond_99
    const/4 v1, 0x0

    #@9a
    goto :goto_90

    #@9b
    .line 110
    :sswitch_9b
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@9d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a0
    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a3
    move-result v0

    #@a4
    if-eqz v0, :cond_b0

    #@a6
    const/4 v1, 0x1

    #@a7
    .line 113
    .restart local v1       #_arg0:Z
    :goto_a7
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyCallForwardingChanged(Z)V

    #@aa
    .line 114
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad
    .line 115
    const/4 v0, 0x1

    #@ae
    goto/16 :goto_7

    #@b0
    .line 112
    .end local v1           #_arg0:Z
    :cond_b0
    const/4 v1, 0x0

    #@b1
    goto :goto_a7

    #@b2
    .line 119
    :sswitch_b2
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@b4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b7
    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ba
    move-result v1

    #@bb
    .line 122
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyDataActivity(I)V

    #@be
    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c1
    .line 124
    const/4 v0, 0x1

    #@c2
    goto/16 :goto_7

    #@c4
    .line 128
    .end local v1           #_arg0:I
    :sswitch_c4
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@c6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c9
    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cc
    move-result v1

    #@cd
    .line 132
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d0
    move-result v0

    #@d1
    if-eqz v0, :cond_115

    #@d3
    const/4 v2, 0x1

    #@d4
    .line 134
    .local v2, _arg1:Z
    :goto_d4
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d7
    move-result-object v3

    #@d8
    .line 136
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@db
    move-result-object v4

    #@dc
    .line 138
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@df
    move-result-object v5

    #@e0
    .line 140
    .local v5, _arg4:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e3
    move-result v0

    #@e4
    if-eqz v0, :cond_117

    #@e6
    .line 141
    sget-object v0, Landroid/net/LinkProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e8
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@eb
    move-result-object v6

    #@ec
    check-cast v6, Landroid/net/LinkProperties;

    #@ee
    .line 147
    .local v6, _arg5:Landroid/net/LinkProperties;
    :goto_ee
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f1
    move-result v0

    #@f2
    if-eqz v0, :cond_119

    #@f4
    .line 148
    sget-object v0, Landroid/net/LinkCapabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f6
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f9
    move-result-object v7

    #@fa
    check-cast v7, Landroid/net/LinkCapabilities;

    #@fc
    .line 154
    .local v7, _arg6:Landroid/net/LinkCapabilities;
    :goto_fc
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ff
    move-result v8

    #@100
    .line 156
    .local v8, _arg7:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@103
    move-result v0

    #@104
    if-eqz v0, :cond_11b

    #@106
    const/4 v9, 0x1

    #@107
    .line 158
    .local v9, _arg8:Z
    :goto_107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10a
    move-result v10

    #@10b
    .local v10, _arg9:I
    move-object v0, p0

    #@10c
    .line 159
    invoke-virtual/range {v0 .. v10}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyDataConnection(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;IZI)V

    #@10f
    .line 160
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@112
    .line 161
    const/4 v0, 0x1

    #@113
    goto/16 :goto_7

    #@115
    .line 132
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Landroid/net/LinkProperties;
    .end local v7           #_arg6:Landroid/net/LinkCapabilities;
    .end local v8           #_arg7:I
    .end local v9           #_arg8:Z
    .end local v10           #_arg9:I
    :cond_115
    const/4 v2, 0x0

    #@116
    goto :goto_d4

    #@117
    .line 144
    .restart local v2       #_arg1:Z
    .restart local v3       #_arg2:Ljava/lang/String;
    .restart local v4       #_arg3:Ljava/lang/String;
    .restart local v5       #_arg4:Ljava/lang/String;
    :cond_117
    const/4 v6, 0x0

    #@118
    .restart local v6       #_arg5:Landroid/net/LinkProperties;
    goto :goto_ee

    #@119
    .line 151
    :cond_119
    const/4 v7, 0x0

    #@11a
    .restart local v7       #_arg6:Landroid/net/LinkCapabilities;
    goto :goto_fc

    #@11b
    .line 156
    .restart local v8       #_arg7:I
    :cond_11b
    const/4 v9, 0x0

    #@11c
    goto :goto_107

    #@11d
    .line 165
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Landroid/net/LinkProperties;
    .end local v7           #_arg6:Landroid/net/LinkCapabilities;
    .end local v8           #_arg7:I
    :sswitch_11d
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@11f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@122
    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@125
    move-result-object v1

    #@126
    .line 169
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@129
    move-result-object v2

    #@12a
    .line 170
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V

    #@12d
    .line 171
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@130
    .line 172
    const/4 v0, 0x1

    #@131
    goto/16 :goto_7

    #@133
    .line 176
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_133
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@135
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@138
    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@13b
    move-result v0

    #@13c
    if-eqz v0, :cond_14f

    #@13e
    .line 179
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@140
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@143
    move-result-object v1

    #@144
    check-cast v1, Landroid/os/Bundle;

    #@146
    .line 184
    .local v1, _arg0:Landroid/os/Bundle;
    :goto_146
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyCellLocation(Landroid/os/Bundle;)V

    #@149
    .line 185
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14c
    .line 186
    const/4 v0, 0x1

    #@14d
    goto/16 :goto_7

    #@14f
    .line 182
    .end local v1           #_arg0:Landroid/os/Bundle;
    :cond_14f
    const/4 v1, 0x0

    #@150
    .restart local v1       #_arg0:Landroid/os/Bundle;
    goto :goto_146

    #@151
    .line 190
    .end local v1           #_arg0:Landroid/os/Bundle;
    :sswitch_151
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@153
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@156
    .line 192
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@159
    move-result v1

    #@15a
    .line 193
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyOtaspChanged(I)V

    #@15d
    .line 194
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@160
    .line 195
    const/4 v0, 0x1

    #@161
    goto/16 :goto_7

    #@163
    .line 199
    .end local v1           #_arg0:I
    :sswitch_163
    const-string v0, "com.android.internal.telephony.ITelephonyRegistry"

    #@165
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@168
    .line 201
    sget-object v0, Landroid/telephony/CellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@16a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@16d
    move-result-object v11

    #@16e
    .line 202
    .local v11, _arg0:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->notifyCellInfo(Ljava/util/List;)V

    #@171
    .line 203
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@174
    .line 204
    const/4 v0, 0x1

    #@175
    goto/16 :goto_7

    #@177
    .line 38
    nop

    #@178
    :sswitch_data_178
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_35
        0x3 -> :sswitch_4a
        0x4 -> :sswitch_67
        0x5 -> :sswitch_84
        0x6 -> :sswitch_9b
        0x7 -> :sswitch_b2
        0x8 -> :sswitch_c4
        0x9 -> :sswitch_11d
        0xa -> :sswitch_133
        0xb -> :sswitch_151
        0xc -> :sswitch_163
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
