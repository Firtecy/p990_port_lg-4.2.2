.class Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;
.super Ljava/lang/Object;
.source "IPhoneSubInfoMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 146
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 147
    iput-object p1, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 148
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getCompleteVoiceMailNumber(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 311
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 312
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 315
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 316
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 317
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x8

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 318
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 319
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-object v2

    #@1f
    .line 322
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 323
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 325
    return-object v2

    #@26
    .line 322
    .end local v2           #_result:Ljava/lang/String;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 323
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getDeviceId(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 163
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 164
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 167
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 168
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 169
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 170
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 171
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-object v2

    #@1e
    .line 174
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 175
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 177
    return-object v2

    #@25
    .line 174
    .end local v2           #_result:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 175
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public getDeviceSvn(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 186
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 189
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 190
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 191
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x2

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 192
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 193
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-object v2

    #@1e
    .line 196
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 199
    return-object v2

    #@25
    .line 196
    .end local v2           #_result:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public getIccSerialNumber(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 227
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 228
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 231
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 232
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 233
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x4

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 234
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 235
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-object v2

    #@1e
    .line 238
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 239
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 241
    return-object v2

    #@25
    .line 238
    .end local v2           #_result:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 239
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 155
    const-string v0, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@2
    return-object v0
.end method

.method public getLine1AlphaTag(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 269
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 270
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 273
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 274
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 275
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x6

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 276
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 277
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-object v2

    #@1e
    .line 280
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 281
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 283
    return-object v2

    #@25
    .line 280
    .end local v2           #_result:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 281
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public getLine1Number(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 248
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 249
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 252
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 253
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 254
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x5

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 255
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 256
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-object v2

    #@1e
    .line 259
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 260
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 262
    return-object v2

    #@25
    .line 259
    .end local v2           #_result:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 260
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public getSubscriberId(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 206
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 207
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 210
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 211
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 212
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x3

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 213
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 214
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-object v2

    #@1e
    .line 217
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 218
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 220
    return-object v2

    #@25
    .line 217
    .end local v2           #_result:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 218
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public getVoiceMailAlphaTag(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 333
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 334
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 337
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 338
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 339
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x9

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 340
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 341
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-object v2

    #@1f
    .line 344
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 345
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 347
    return-object v2

    #@26
    .line 344
    .end local v2           #_result:Ljava/lang/String;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 345
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getVoiceMailNumber(I)Ljava/lang/String;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 290
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 291
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 294
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IPhoneSubInfoMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 295
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 296
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x7

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 297
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 298
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-object v2

    #@1e
    .line 301
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 302
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 304
    return-object v2

    #@25
    .line 301
    .end local v2           #_result:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 302
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method
