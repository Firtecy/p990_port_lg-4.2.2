.class final Lcom/android/internal/telephony/KNDataResponse$1;
.super Ljava/lang/Object;
.source "KNDataResponse.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/KNDataResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/android/internal/telephony/KNDataResponse;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/KNDataResponse;
    .registers 6
    .parameter "in"

    #@0
    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v2

    #@4
    .line 60
    .local v2, send_buf_num:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v1

    #@8
    .line 61
    .local v1, data_len:I
    new-array v0, v1, [B

    #@a
    .line 62
    .local v0, data:[B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    #@d
    .line 63
    new-instance v3, Lcom/android/internal/telephony/KNDataResponse;

    #@f
    invoke-direct {v3, v2, v1, v0}, Lcom/android/internal/telephony/KNDataResponse;-><init>(II[B)V

    #@12
    return-object v3
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/KNDataResponse$1;->createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/KNDataResponse;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/android/internal/telephony/KNDataResponse;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 67
    new-array v0, p1, [Lcom/android/internal/telephony/KNDataResponse;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/KNDataResponse$1;->newArray(I)[Lcom/android/internal/telephony/KNDataResponse;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
