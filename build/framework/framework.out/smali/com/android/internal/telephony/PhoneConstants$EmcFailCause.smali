.class public final enum Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
.super Ljava/lang/Enum;
.source "PhoneConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PhoneConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EmcFailCause"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

.field public static final enum ATTACH_FAILED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

.field public static final enum BARRED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

.field public static final enum NONE:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

.field public static final enum PDN_FAILED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

.field private static final sEmcFailCauseMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCode:I


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v9, 0x3

    #@1
    const/4 v8, 0x2

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 273
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@6
    const-string v5, "NONE"

    #@8
    invoke-direct {v4, v5, v6, v6}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;-><init>(Ljava/lang/String;II)V

    #@b
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->NONE:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@d
    .line 274
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@f
    const-string v5, "PDN_FAILED"

    #@11
    invoke-direct {v4, v5, v7, v7}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;-><init>(Ljava/lang/String;II)V

    #@14
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->PDN_FAILED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@16
    .line 275
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@18
    const-string v5, "ATTACH_FAILED"

    #@1a
    invoke-direct {v4, v5, v8, v8}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;-><init>(Ljava/lang/String;II)V

    #@1d
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->ATTACH_FAILED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@1f
    .line 276
    new-instance v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@21
    const-string v5, "BARRED"

    #@23
    invoke-direct {v4, v5, v9, v9}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;-><init>(Ljava/lang/String;II)V

    #@26
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->BARRED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@28
    .line 272
    const/4 v4, 0x4

    #@29
    new-array v4, v4, [Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@2b
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->NONE:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@2d
    aput-object v5, v4, v6

    #@2f
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->PDN_FAILED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@31
    aput-object v5, v4, v7

    #@33
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->ATTACH_FAILED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@35
    aput-object v5, v4, v8

    #@37
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->BARRED:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@39
    aput-object v5, v4, v9

    #@3b
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@3d
    .line 281
    new-instance v4, Ljava/util/HashMap;

    #@3f
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@42
    sput-object v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->sEmcFailCauseMap:Ljava/util/HashMap;

    #@44
    .line 282
    invoke-static {}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->values()[Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@47
    move-result-object v0

    #@48
    .local v0, arr$:[Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    array-length v3, v0

    #@49
    .local v3, len$:I
    const/4 v2, 0x0

    #@4a
    .local v2, i$:I
    :goto_4a
    if-ge v2, v3, :cond_5e

    #@4c
    aget-object v1, v0, v2

    #@4e
    .line 283
    .local v1, efc:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    sget-object v4, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->sEmcFailCauseMap:Ljava/util/HashMap;

    #@50
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->getCode()I

    #@53
    move-result v5

    #@54
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    .line 282
    add-int/lit8 v2, v2, 0x1

    #@5d
    goto :goto_4a

    #@5e
    .line 285
    .end local v1           #efc:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    :cond_5e
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "code"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 287
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 288
    iput p3, p0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->mCode:I

    #@5
    .line 289
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    .registers 4
    .parameter "code"

    #@0
    .prologue
    .line 296
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->sEmcFailCauseMap:Ljava/util/HashMap;

    #@2
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@c
    .line 297
    .local v0, efc:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    if-nez v0, :cond_10

    #@e
    .line 298
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->NONE:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@10
    .line 300
    :cond_10
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 272
    const-class v0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    .registers 1

    #@0
    .prologue
    .line 272
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->$VALUES:[Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@8
    return-object v0
.end method


# virtual methods
.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 292
    iget v0, p0, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->mCode:I

    #@2
    return v0
.end method
