.class public Lcom/android/internal/content/NativeLibraryHelper;
.super Ljava/lang/Object;
.source "NativeLibraryHelper.java"


# static fields
.field private static final DEBUG_NATIVE:Z = false

.field private static final TAG:Ljava/lang/String; = "NativeHelper"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static copyNativeBinariesIfNeededLI(Ljava/io/File;Ljava/io/File;)I
    .registers 6
    .parameter "apkFile"
    .parameter "sharedLibraryDir"

    #@0
    .prologue
    .line 60
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    #@2
    .line 61
    .local v0, cpuAbi:Ljava/lang/String;
    sget-object v1, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    #@4
    .line 62
    .local v1, cpuAbi2:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    invoke-static {v2, v3, v0, v1}, Lcom/android/internal/content/NativeLibraryHelper;->nativeCopyNativeBinaries(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@f
    move-result v2

    #@10
    return v2
.end method

.method private static native nativeCopyNativeBinaries(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private static native nativeSumNativeBinaries(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
.end method

.method public static removeNativeBinariesFromDirLI(Ljava/io/File;)Z
    .registers 7
    .parameter "nativeLibraryDir"

    #@0
    .prologue
    .line 78
    const/4 v1, 0x0

    #@1
    .line 85
    .local v1, deletedFiles:Z
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_3c

    #@7
    .line 86
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@a
    move-result-object v0

    #@b
    .line 87
    .local v0, binaries:[Ljava/io/File;
    if-eqz v0, :cond_3c

    #@d
    .line 88
    const/4 v2, 0x0

    #@e
    .local v2, nn:I
    :goto_e
    array-length v3, v0

    #@f
    if-ge v2, v3, :cond_3c

    #@11
    .line 93
    aget-object v3, v0, v2

    #@13
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_3a

    #@19
    .line 94
    const-string v3, "NativeHelper"

    #@1b
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v5, "Could not delete native binary: "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    aget-object v5, v0, v2

    #@28
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 88
    :goto_37
    add-int/lit8 v2, v2, 0x1

    #@39
    goto :goto_e

    #@3a
    .line 96
    :cond_3a
    const/4 v1, 0x1

    #@3b
    goto :goto_37

    #@3c
    .line 104
    .end local v0           #binaries:[Ljava/io/File;
    .end local v2           #nn:I
    :cond_3c
    return v1
.end method

.method public static removeNativeBinariesLI(Ljava/lang/String;)Z
    .registers 2
    .parameter "nativeLibraryPath"

    #@0
    .prologue
    .line 68
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5
    invoke-static {v0}, Lcom/android/internal/content/NativeLibraryHelper;->removeNativeBinariesFromDirLI(Ljava/io/File;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public static sumNativeBinariesLI(Ljava/io/File;)J
    .registers 5
    .parameter "apkFile"

    #@0
    .prologue
    .line 43
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    #@2
    .line 44
    .local v0, cpuAbi:Ljava/lang/String;
    sget-object v1, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    #@4
    .line 45
    .local v1, cpuAbi2:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-static {v2, v0, v1}, Lcom/android/internal/content/NativeLibraryHelper;->nativeSumNativeBinaries(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    #@b
    move-result-wide v2

    #@c
    return-wide v2
.end method
