.class public abstract Lcom/android/internal/content/PackageMonitor;
.super Landroid/content/BroadcastReceiver;
.source "PackageMonitor.java"


# static fields
.field public static final PACKAGE_PERMANENT_CHANGE:I = 0x3

.field public static final PACKAGE_TEMPORARY_CHANGE:I = 0x2

.field public static final PACKAGE_UNCHANGED:I = 0x0

.field public static final PACKAGE_UPDATING:I = 0x1

.field static sBackgroundHandler:Landroid/os/Handler;

.field static sBackgroundThread:Landroid/os/HandlerThread;

.field static final sExternalFilt:Landroid/content/IntentFilter;

.field static final sLock:Ljava/lang/Object;

.field static final sNonDataFilt:Landroid/content/IntentFilter;

.field static final sPackageFilt:Landroid/content/IntentFilter;


# instance fields
.field mAppearingPackages:[Ljava/lang/String;

.field mChangeType:I

.field mChangeUserId:I

.field mDisappearingPackages:[Ljava/lang/String;

.field mModifiedPackages:[Ljava/lang/String;

.field mRegisteredContext:Landroid/content/Context;

.field mRegisteredHandler:Landroid/os/Handler;

.field mSomePackagesChanged:Z

.field mTempArray:[Ljava/lang/String;

.field final mUpdatingPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 36
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@7
    .line 37
    new-instance v0, Landroid/content/IntentFilter;

    #@9
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@c
    sput-object v0, Lcom/android/internal/content/PackageMonitor;->sNonDataFilt:Landroid/content/IntentFilter;

    #@e
    .line 38
    new-instance v0, Landroid/content/IntentFilter;

    #@10
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@13
    sput-object v0, Lcom/android/internal/content/PackageMonitor;->sExternalFilt:Landroid/content/IntentFilter;

    #@15
    .line 40
    new-instance v0, Ljava/lang/Object;

    #@17
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@1a
    sput-object v0, Lcom/android/internal/content/PackageMonitor;->sLock:Ljava/lang/Object;

    #@1c
    .line 45
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@1e
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    #@20
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@23
    .line 46
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@25
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    #@27
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2a
    .line 47
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@2c
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    #@2e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@31
    .line 48
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@33
    const-string v1, "android.intent.action.QUERY_PACKAGE_RESTART"

    #@35
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@38
    .line 49
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@3a
    const-string v1, "android.intent.action.PACKAGE_RESTARTED"

    #@3c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3f
    .line 50
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@41
    const-string v1, "android.intent.action.UID_REMOVED"

    #@43
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@46
    .line 51
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@48
    const-string/jumbo v1, "package"

    #@4b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@4e
    .line 52
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sNonDataFilt:Landroid/content/IntentFilter;

    #@50
    const-string v1, "android.intent.action.UID_REMOVED"

    #@52
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@55
    .line 53
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sNonDataFilt:Landroid/content/IntentFilter;

    #@57
    const-string v1, "android.intent.action.USER_STOPPED"

    #@59
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@5c
    .line 54
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sExternalFilt:Landroid/content/IntentFilter;

    #@5e
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    #@60
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@63
    .line 55
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sExternalFilt:Landroid/content/IntentFilter;

    #@65
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@67
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6a
    .line 56
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 58
    new-instance v0, Ljava/util/HashSet;

    #@5
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mUpdatingPackages:Ljava/util/HashSet;

    #@a
    .line 66
    const/16 v0, -0x2710

    #@c
    iput v0, p0, Lcom/android/internal/content/PackageMonitor;->mChangeUserId:I

    #@e
    .line 69
    const/4 v0, 0x1

    #@f
    new-array v0, v0, [Ljava/lang/String;

    #@11
    iput-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@13
    return-void
.end method


# virtual methods
.method public anyPackagesAppearing()Z
    .registers 2

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mAppearingPackages:[Ljava/lang/String;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public anyPackagesDisappearing()Z
    .registers 2

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public didSomePackagesChange()Z
    .registers 2

    #@0
    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/android/internal/content/PackageMonitor;->mSomePackagesChanged:Z

    #@2
    return v0
.end method

.method public getChangingUserId()I
    .registers 2

    #@0
    .prologue
    .line 248
    iget v0, p0, Lcom/android/internal/content/PackageMonitor;->mChangeUserId:I

    #@2
    return v0
.end method

.method getPackageName(Landroid/content/Intent;)Ljava/lang/String;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 252
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@3
    move-result-object v1

    #@4
    .line 253
    .local v1, uri:Landroid/net/Uri;
    if-eqz v1, :cond_b

    #@6
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 254
    .local v0, pkg:Ljava/lang/String;
    :goto_a
    return-object v0

    #@b
    .line 253
    .end local v0           #pkg:Ljava/lang/String;
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getRegisteredHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public isPackageAppearing(Ljava/lang/String;)I
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 201
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mAppearingPackages:[Ljava/lang/String;

    #@2
    if-eqz v1, :cond_1b

    #@4
    .line 202
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mAppearingPackages:[Ljava/lang/String;

    #@6
    array-length v1, v1

    #@7
    add-int/lit8 v0, v1, -0x1

    #@9
    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_1b

    #@b
    .line 203
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mAppearingPackages:[Ljava/lang/String;

    #@d
    aget-object v1, v1, v0

    #@f
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_18

    #@15
    .line 204
    iget v1, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@17
    .line 208
    .end local v0           #i:I
    :goto_17
    return v1

    #@18
    .line 202
    .restart local v0       #i:I
    :cond_18
    add-int/lit8 v0, v0, -0x1

    #@1a
    goto :goto_9

    #@1b
    .line 208
    .end local v0           #i:I
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_17
.end method

.method public isPackageDisappearing(Ljava/lang/String;)I
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 216
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@2
    if-eqz v1, :cond_1b

    #@4
    .line 217
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@6
    array-length v1, v1

    #@7
    add-int/lit8 v0, v1, -0x1

    #@9
    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_1b

    #@b
    .line 218
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@d
    aget-object v1, v1, v0

    #@f
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_18

    #@15
    .line 219
    iget v1, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@17
    .line 223
    .end local v0           #i:I
    :goto_17
    return v1

    #@18
    .line 217
    .restart local v0       #i:I
    :cond_18
    add-int/lit8 v0, v0, -0x1

    #@1a
    goto :goto_9

    #@1b
    .line 223
    .end local v0           #i:I
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_17
.end method

.method public isPackageModified(Ljava/lang/String;)Z
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 231
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mModifiedPackages:[Ljava/lang/String;

    #@2
    if-eqz v1, :cond_1a

    #@4
    .line 232
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mModifiedPackages:[Ljava/lang/String;

    #@6
    array-length v1, v1

    #@7
    add-int/lit8 v0, v1, -0x1

    #@9
    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_1a

    #@b
    .line 233
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mModifiedPackages:[Ljava/lang/String;

    #@d
    aget-object v1, v1, v0

    #@f
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_17

    #@15
    .line 234
    const/4 v1, 0x1

    #@16
    .line 238
    .end local v0           #i:I
    :goto_16
    return v1

    #@17
    .line 232
    .restart local v0       #i:I
    :cond_17
    add-int/lit8 v0, v0, -0x1

    #@19
    goto :goto_9

    #@1a
    .line 238
    .end local v0           #i:I
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_16
.end method

.method isPackageUpdating(Ljava/lang/String;)Z
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 124
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mUpdatingPackages:Ljava/util/HashSet;

    #@2
    monitor-enter v1

    #@3
    .line 125
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mUpdatingPackages:Ljava/util/HashSet;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 126
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public onBeginPackageChanges()V
    .registers 1

    #@0
    .prologue
    .line 130
    return-void
.end method

.method public onFinishPackageChanges()V
    .registers 1

    #@0
    .prologue
    .line 245
    return-void
.end method

.method public onHandleForceStop(Landroid/content/Intent;[Ljava/lang/String;IZ)Z
    .registers 6
    .parameter "intent"
    .parameter "packages"
    .parameter "uid"
    .parameter "doit"

    #@0
    .prologue
    .line 161
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onHandleUserStop(Landroid/content/Intent;I)V
    .registers 3
    .parameter "intent"
    .parameter "userHandle"

    #@0
    .prologue
    .line 165
    return-void
.end method

.method public onPackageAdded(Ljava/lang/String;I)V
    .registers 3
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 136
    return-void
.end method

.method public onPackageAppeared(Ljava/lang/String;I)V
    .registers 3
    .parameter "packageName"
    .parameter "reason"

    #@0
    .prologue
    .line 191
    return-void
.end method

.method public onPackageChanged(Ljava/lang/String;I[Ljava/lang/String;)V
    .registers 4
    .parameter "packageName"
    .parameter "uid"
    .parameter "components"

    #@0
    .prologue
    .line 158
    return-void
.end method

.method public onPackageDisappeared(Ljava/lang/String;I)V
    .registers 3
    .parameter "packageName"
    .parameter "reason"

    #@0
    .prologue
    .line 185
    return-void
.end method

.method public onPackageModified(Ljava/lang/String;)V
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 194
    return-void
.end method

.method public onPackageRemoved(Ljava/lang/String;I)V
    .registers 3
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 142
    return-void
.end method

.method public onPackageRemovedAllUsers(Ljava/lang/String;I)V
    .registers 3
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 149
    return-void
.end method

.method public onPackageUpdateFinished(Ljava/lang/String;I)V
    .registers 3
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 155
    return-void
.end method

.method public onPackageUpdateStarted(Ljava/lang/String;I)V
    .registers 3
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 152
    return-void
.end method

.method public onPackagesAvailable([Ljava/lang/String;)V
    .registers 2
    .parameter "packages"

    #@0
    .prologue
    .line 171
    return-void
.end method

.method public onPackagesUnavailable([Ljava/lang/String;)V
    .registers 2
    .parameter "packages"

    #@0
    .prologue
    .line 174
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/16 v12, -0x2710

    #@3
    const/4 v11, 0x2

    #@4
    const/4 v10, 0x1

    #@5
    const/4 v9, 0x0

    #@6
    .line 259
    const-string v7, "android.intent.extra.user_handle"

    #@8
    invoke-virtual {p2, v7, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@b
    move-result v7

    #@c
    iput v7, p0, Lcom/android/internal/content/PackageMonitor;->mChangeUserId:I

    #@e
    .line 261
    iget v7, p0, Lcom/android/internal/content/PackageMonitor;->mChangeUserId:I

    #@10
    if-ne v7, v12, :cond_2b

    #@12
    .line 262
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@14
    new-instance v8, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v9, "Intent broadcast does not contain user handle: "

    #@1b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v8

    #@27
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v7

    #@2b
    .line 265
    :cond_2b
    invoke-virtual {p0}, Lcom/android/internal/content/PackageMonitor;->onBeginPackageChanges()V

    #@2e
    .line 267
    const/4 v7, 0x0

    #@2f
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mAppearingPackages:[Ljava/lang/String;

    #@31
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@33
    .line 268
    iput-boolean v9, p0, Lcom/android/internal/content/PackageMonitor;->mSomePackagesChanged:Z

    #@35
    .line 270
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    .line 271
    .local v0, action:Ljava/lang/String;
    const-string v7, "android.intent.action.PACKAGE_ADDED"

    #@3b
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v7

    #@3f
    if-eqz v7, :cond_93

    #@41
    .line 272
    invoke-virtual {p0, p2}, Lcom/android/internal/content/PackageMonitor;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    .line 273
    .local v4, pkg:Ljava/lang/String;
    const-string v7, "android.intent.extra.UID"

    #@47
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@4a
    move-result v6

    #@4b
    .line 277
    .local v6, uid:I
    iput-boolean v10, p0, Lcom/android/internal/content/PackageMonitor;->mSomePackagesChanged:Z

    #@4d
    .line 278
    if-eqz v4, :cond_7d

    #@4f
    .line 279
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@51
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mAppearingPackages:[Ljava/lang/String;

    #@53
    .line 280
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@55
    aput-object v4, v7, v9

    #@57
    .line 281
    const-string v7, "android.intent.extra.REPLACING"

    #@59
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@5c
    move-result v7

    #@5d
    if-eqz v7, :cond_8a

    #@5f
    .line 282
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@61
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mModifiedPackages:[Ljava/lang/String;

    #@63
    .line 283
    iput v10, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@65
    .line 284
    invoke-virtual {p0, v4, v6}, Lcom/android/internal/content/PackageMonitor;->onPackageUpdateFinished(Ljava/lang/String;I)V

    #@68
    .line 285
    invoke-virtual {p0, v4}, Lcom/android/internal/content/PackageMonitor;->onPackageModified(Ljava/lang/String;)V

    #@6b
    .line 290
    :goto_6b
    iget v7, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@6d
    invoke-virtual {p0, v4, v7}, Lcom/android/internal/content/PackageMonitor;->onPackageAppeared(Ljava/lang/String;I)V

    #@70
    .line 291
    iget v7, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@72
    if-ne v7, v10, :cond_7d

    #@74
    .line 292
    iget-object v8, p0, Lcom/android/internal/content/PackageMonitor;->mUpdatingPackages:Ljava/util/HashSet;

    #@76
    monitor-enter v8

    #@77
    .line 293
    :try_start_77
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mUpdatingPackages:Ljava/util/HashSet;

    #@79
    invoke-virtual {v7, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@7c
    .line 294
    monitor-exit v8
    :try_end_7d
    .catchall {:try_start_77 .. :try_end_7d} :catchall_90

    #@7d
    .line 378
    .end local v4           #pkg:Ljava/lang/String;
    .end local v6           #uid:I
    :cond_7d
    :goto_7d
    iget-boolean v7, p0, Lcom/android/internal/content/PackageMonitor;->mSomePackagesChanged:Z

    #@7f
    if-eqz v7, :cond_84

    #@81
    .line 379
    invoke-virtual {p0}, Lcom/android/internal/content/PackageMonitor;->onSomePackagesChanged()V

    #@84
    .line 382
    :cond_84
    invoke-virtual {p0}, Lcom/android/internal/content/PackageMonitor;->onFinishPackageChanges()V

    #@87
    .line 383
    iput v12, p0, Lcom/android/internal/content/PackageMonitor;->mChangeUserId:I

    #@89
    .line 384
    return-void

    #@8a
    .line 287
    .restart local v4       #pkg:Ljava/lang/String;
    .restart local v6       #uid:I
    :cond_8a
    iput v8, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@8c
    .line 288
    invoke-virtual {p0, v4, v6}, Lcom/android/internal/content/PackageMonitor;->onPackageAdded(Ljava/lang/String;I)V

    #@8f
    goto :goto_6b

    #@90
    .line 294
    :catchall_90
    move-exception v7

    #@91
    :try_start_91
    monitor-exit v8
    :try_end_92
    .catchall {:try_start_91 .. :try_end_92} :catchall_90

    #@92
    throw v7

    #@93
    .line 297
    .end local v4           #pkg:Ljava/lang/String;
    .end local v6           #uid:I
    :cond_93
    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    #@95
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@98
    move-result v7

    #@99
    if-eqz v7, :cond_dc

    #@9b
    .line 298
    invoke-virtual {p0, p2}, Lcom/android/internal/content/PackageMonitor;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    #@9e
    move-result-object v4

    #@9f
    .line 299
    .restart local v4       #pkg:Ljava/lang/String;
    const-string v7, "android.intent.extra.UID"

    #@a1
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@a4
    move-result v6

    #@a5
    .line 300
    .restart local v6       #uid:I
    if-eqz v4, :cond_7d

    #@a7
    .line 301
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@a9
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@ab
    .line 302
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@ad
    aput-object v4, v7, v9

    #@af
    .line 303
    const-string v7, "android.intent.extra.REPLACING"

    #@b1
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@b4
    move-result v7

    #@b5
    if-eqz v7, :cond_c9

    #@b7
    .line 304
    iput v10, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@b9
    .line 305
    iget-object v8, p0, Lcom/android/internal/content/PackageMonitor;->mUpdatingPackages:Ljava/util/HashSet;

    #@bb
    monitor-enter v8

    #@bc
    .line 308
    :try_start_bc
    monitor-exit v8
    :try_end_bd
    .catchall {:try_start_bc .. :try_end_bd} :catchall_c6

    #@bd
    .line 309
    invoke-virtual {p0, v4, v6}, Lcom/android/internal/content/PackageMonitor;->onPackageUpdateStarted(Ljava/lang/String;I)V

    #@c0
    .line 321
    :cond_c0
    :goto_c0
    iget v7, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@c2
    invoke-virtual {p0, v4, v7}, Lcom/android/internal/content/PackageMonitor;->onPackageDisappeared(Ljava/lang/String;I)V

    #@c5
    goto :goto_7d

    #@c6
    .line 308
    :catchall_c6
    move-exception v7

    #@c7
    :try_start_c7
    monitor-exit v8
    :try_end_c8
    .catchall {:try_start_c7 .. :try_end_c8} :catchall_c6

    #@c8
    throw v7

    #@c9
    .line 311
    :cond_c9
    iput v8, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@cb
    .line 315
    iput-boolean v10, p0, Lcom/android/internal/content/PackageMonitor;->mSomePackagesChanged:Z

    #@cd
    .line 316
    invoke-virtual {p0, v4, v6}, Lcom/android/internal/content/PackageMonitor;->onPackageRemoved(Ljava/lang/String;I)V

    #@d0
    .line 317
    const-string v7, "android.intent.extra.REMOVED_FOR_ALL_USERS"

    #@d2
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@d5
    move-result v7

    #@d6
    if-eqz v7, :cond_c0

    #@d8
    .line 318
    invoke-virtual {p0, v4, v6}, Lcom/android/internal/content/PackageMonitor;->onPackageRemovedAllUsers(Ljava/lang/String;I)V

    #@db
    goto :goto_c0

    #@dc
    .line 323
    .end local v4           #pkg:Ljava/lang/String;
    .end local v6           #uid:I
    :cond_dc
    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    #@de
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e1
    move-result v7

    #@e2
    if-eqz v7, :cond_106

    #@e4
    .line 324
    invoke-virtual {p0, p2}, Lcom/android/internal/content/PackageMonitor;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    #@e7
    move-result-object v4

    #@e8
    .line 325
    .restart local v4       #pkg:Ljava/lang/String;
    const-string v7, "android.intent.extra.UID"

    #@ea
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@ed
    move-result v6

    #@ee
    .line 326
    .restart local v6       #uid:I
    const-string v7, "android.intent.extra.changed_component_name_list"

    #@f0
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@f3
    move-result-object v2

    #@f4
    .line 328
    .local v2, components:[Ljava/lang/String;
    if-eqz v4, :cond_7d

    #@f6
    .line 329
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@f8
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mModifiedPackages:[Ljava/lang/String;

    #@fa
    .line 330
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mTempArray:[Ljava/lang/String;

    #@fc
    aput-object v4, v7, v9

    #@fe
    .line 331
    invoke-virtual {p0, v4, v6, v2}, Lcom/android/internal/content/PackageMonitor;->onPackageChanged(Ljava/lang/String;I[Ljava/lang/String;)V

    #@101
    .line 334
    invoke-virtual {p0, v4}, Lcom/android/internal/content/PackageMonitor;->onPackageModified(Ljava/lang/String;)V

    #@104
    goto/16 :goto_7d

    #@106
    .line 336
    .end local v2           #components:[Ljava/lang/String;
    .end local v4           #pkg:Ljava/lang/String;
    .end local v6           #uid:I
    :cond_106
    const-string v7, "android.intent.action.QUERY_PACKAGE_RESTART"

    #@108
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10b
    move-result v7

    #@10c
    if-eqz v7, :cond_12c

    #@10e
    .line 337
    const-string v7, "android.intent.extra.PACKAGES"

    #@110
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@113
    move-result-object v7

    #@114
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@116
    .line 338
    iput v11, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@118
    .line 339
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@11a
    const-string v8, "android.intent.extra.UID"

    #@11c
    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@11f
    move-result v8

    #@120
    invoke-virtual {p0, p2, v7, v8, v9}, Lcom/android/internal/content/PackageMonitor;->onHandleForceStop(Landroid/content/Intent;[Ljava/lang/String;IZ)Z

    #@123
    move-result v1

    #@124
    .line 342
    .local v1, canRestart:Z
    if-eqz v1, :cond_7d

    #@126
    const/4 v7, -0x1

    #@127
    invoke-virtual {p0, v7}, Lcom/android/internal/content/PackageMonitor;->setResultCode(I)V

    #@12a
    goto/16 :goto_7d

    #@12c
    .line 343
    .end local v1           #canRestart:Z
    :cond_12c
    const-string v7, "android.intent.action.PACKAGE_RESTARTED"

    #@12e
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@131
    move-result v7

    #@132
    if-eqz v7, :cond_14d

    #@134
    .line 344
    new-array v7, v10, [Ljava/lang/String;

    #@136
    invoke-virtual {p0, p2}, Lcom/android/internal/content/PackageMonitor;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    #@139
    move-result-object v8

    #@13a
    aput-object v8, v7, v9

    #@13c
    iput-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@13e
    .line 345
    iput v11, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@140
    .line 346
    iget-object v7, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@142
    const-string v8, "android.intent.extra.UID"

    #@144
    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@147
    move-result v8

    #@148
    invoke-virtual {p0, p2, v7, v8, v10}, Lcom/android/internal/content/PackageMonitor;->onHandleForceStop(Landroid/content/Intent;[Ljava/lang/String;IZ)Z

    #@14b
    goto/16 :goto_7d

    #@14d
    .line 348
    :cond_14d
    const-string v7, "android.intent.action.UID_REMOVED"

    #@14f
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@152
    move-result v7

    #@153
    if-eqz v7, :cond_160

    #@155
    .line 349
    const-string v7, "android.intent.extra.UID"

    #@157
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@15a
    move-result v7

    #@15b
    invoke-virtual {p0, v7}, Lcom/android/internal/content/PackageMonitor;->onUidRemoved(I)V

    #@15e
    goto/16 :goto_7d

    #@160
    .line 350
    :cond_160
    const-string v7, "android.intent.action.USER_STOPPED"

    #@162
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@165
    move-result v7

    #@166
    if-eqz v7, :cond_17b

    #@168
    .line 351
    const-string v7, "android.intent.extra.user_handle"

    #@16a
    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@16d
    move-result v7

    #@16e
    if-eqz v7, :cond_7d

    #@170
    .line 352
    const-string v7, "android.intent.extra.user_handle"

    #@172
    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@175
    move-result v7

    #@176
    invoke-virtual {p0, p2, v7}, Lcom/android/internal/content/PackageMonitor;->onHandleUserStop(Landroid/content/Intent;I)V

    #@179
    goto/16 :goto_7d

    #@17b
    .line 354
    :cond_17b
    const-string v7, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    #@17d
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@180
    move-result v7

    #@181
    if-eqz v7, :cond_1a0

    #@183
    .line 355
    const-string v7, "android.intent.extra.changed_package_list"

    #@185
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@188
    move-result-object v5

    #@189
    .line 356
    .local v5, pkgList:[Ljava/lang/String;
    iput-object v5, p0, Lcom/android/internal/content/PackageMonitor;->mAppearingPackages:[Ljava/lang/String;

    #@18b
    .line 357
    iput v11, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@18d
    .line 358
    iput-boolean v10, p0, Lcom/android/internal/content/PackageMonitor;->mSomePackagesChanged:Z

    #@18f
    .line 359
    if-eqz v5, :cond_7d

    #@191
    .line 360
    invoke-virtual {p0, v5}, Lcom/android/internal/content/PackageMonitor;->onPackagesAvailable([Ljava/lang/String;)V

    #@194
    .line 361
    const/4 v3, 0x0

    #@195
    .local v3, i:I
    :goto_195
    array-length v7, v5

    #@196
    if-ge v3, v7, :cond_7d

    #@198
    .line 362
    aget-object v7, v5, v3

    #@19a
    invoke-virtual {p0, v7, v11}, Lcom/android/internal/content/PackageMonitor;->onPackageAppeared(Ljava/lang/String;I)V

    #@19d
    .line 361
    add-int/lit8 v3, v3, 0x1

    #@19f
    goto :goto_195

    #@1a0
    .line 365
    .end local v3           #i:I
    .end local v5           #pkgList:[Ljava/lang/String;
    :cond_1a0
    const-string v7, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@1a2
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a5
    move-result v7

    #@1a6
    if-eqz v7, :cond_7d

    #@1a8
    .line 366
    const-string v7, "android.intent.extra.changed_package_list"

    #@1aa
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@1ad
    move-result-object v5

    #@1ae
    .line 367
    .restart local v5       #pkgList:[Ljava/lang/String;
    iput-object v5, p0, Lcom/android/internal/content/PackageMonitor;->mDisappearingPackages:[Ljava/lang/String;

    #@1b0
    .line 368
    iput v11, p0, Lcom/android/internal/content/PackageMonitor;->mChangeType:I

    #@1b2
    .line 369
    iput-boolean v10, p0, Lcom/android/internal/content/PackageMonitor;->mSomePackagesChanged:Z

    #@1b4
    .line 370
    if-eqz v5, :cond_7d

    #@1b6
    .line 371
    invoke-virtual {p0, v5}, Lcom/android/internal/content/PackageMonitor;->onPackagesUnavailable([Ljava/lang/String;)V

    #@1b9
    .line 372
    const/4 v3, 0x0

    #@1ba
    .restart local v3       #i:I
    :goto_1ba
    array-length v7, v5

    #@1bb
    if-ge v3, v7, :cond_7d

    #@1bd
    .line 373
    aget-object v7, v5, v3

    #@1bf
    invoke-virtual {p0, v7, v11}, Lcom/android/internal/content/PackageMonitor;->onPackageDisappeared(Ljava/lang/String;I)V

    #@1c2
    .line 372
    add-int/lit8 v3, v3, 0x1

    #@1c4
    goto :goto_1ba
.end method

.method public onSomePackagesChanged()V
    .registers 1

    #@0
    .prologue
    .line 242
    return-void
.end method

.method public onUidRemoved(I)V
    .registers 2
    .parameter "uid"

    #@0
    .prologue
    .line 168
    return-void
.end method

.method public register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V
    .registers 11
    .parameter "context"
    .parameter "thread"
    .parameter "user"
    .parameter "externalStorage"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 77
    iget-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredContext:Landroid/content/Context;

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 78
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "Already registered"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 80
    :cond_d
    iput-object p1, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredContext:Landroid/content/Context;

    #@f
    .line 81
    if-nez p2, :cond_60

    #@11
    .line 82
    sget-object v1, Lcom/android/internal/content/PackageMonitor;->sLock:Ljava/lang/Object;

    #@13
    monitor-enter v1

    #@14
    .line 83
    :try_start_14
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sBackgroundThread:Landroid/os/HandlerThread;

    #@16
    if-nez v0, :cond_35

    #@18
    .line 84
    new-instance v0, Landroid/os/HandlerThread;

    #@1a
    const-string v2, "PackageMonitor"

    #@1c
    const/16 v3, 0xa

    #@1e
    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    #@21
    sput-object v0, Lcom/android/internal/content/PackageMonitor;->sBackgroundThread:Landroid/os/HandlerThread;

    #@23
    .line 86
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sBackgroundThread:Landroid/os/HandlerThread;

    #@25
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@28
    .line 87
    new-instance v0, Landroid/os/Handler;

    #@2a
    sget-object v2, Lcom/android/internal/content/PackageMonitor;->sBackgroundThread:Landroid/os/HandlerThread;

    #@2c
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@2f
    move-result-object v2

    #@30
    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@33
    sput-object v0, Lcom/android/internal/content/PackageMonitor;->sBackgroundHandler:Landroid/os/Handler;

    #@35
    .line 89
    :cond_35
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sBackgroundHandler:Landroid/os/Handler;

    #@37
    iput-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@39
    .line 90
    monitor-exit v1
    :try_end_3a
    .catchall {:try_start_14 .. :try_end_3a} :catchall_5d

    #@3a
    .line 94
    :goto_3a
    if-eqz p3, :cond_68

    #@3c
    .line 95
    sget-object v3, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@3e
    iget-object v5, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@40
    move-object v0, p1

    #@41
    move-object v1, p0

    #@42
    move-object v2, p3

    #@43
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@46
    .line 96
    sget-object v3, Lcom/android/internal/content/PackageMonitor;->sNonDataFilt:Landroid/content/IntentFilter;

    #@48
    iget-object v5, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@4a
    move-object v0, p1

    #@4b
    move-object v1, p0

    #@4c
    move-object v2, p3

    #@4d
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@50
    .line 97
    if-eqz p4, :cond_5c

    #@52
    .line 98
    sget-object v3, Lcom/android/internal/content/PackageMonitor;->sExternalFilt:Landroid/content/IntentFilter;

    #@54
    iget-object v5, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@56
    move-object v0, p1

    #@57
    move-object v1, p0

    #@58
    move-object v2, p3

    #@59
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@5c
    .line 108
    :cond_5c
    :goto_5c
    return-void

    #@5d
    .line 90
    :catchall_5d
    move-exception v0

    #@5e
    :try_start_5e
    monitor-exit v1
    :try_end_5f
    .catchall {:try_start_5e .. :try_end_5f} :catchall_5d

    #@5f
    throw v0

    #@60
    .line 92
    :cond_60
    new-instance v0, Landroid/os/Handler;

    #@62
    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@65
    iput-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@67
    goto :goto_3a

    #@68
    .line 102
    :cond_68
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sPackageFilt:Landroid/content/IntentFilter;

    #@6a
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@6c
    invoke-virtual {p1, p0, v0, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@6f
    .line 103
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sNonDataFilt:Landroid/content/IntentFilter;

    #@71
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@73
    invoke-virtual {p1, p0, v0, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@76
    .line 104
    if-eqz p4, :cond_5c

    #@78
    .line 105
    sget-object v0, Lcom/android/internal/content/PackageMonitor;->sExternalFilt:Landroid/content/IntentFilter;

    #@7a
    iget-object v1, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredHandler:Landroid/os/Handler;

    #@7c
    invoke-virtual {p1, p0, v0, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@7f
    goto :goto_5c
.end method

.method public register(Landroid/content/Context;Landroid/os/Looper;Z)V
    .registers 5
    .parameter "context"
    .parameter "thread"
    .parameter "externalStorage"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    #@4
    .line 73
    return-void
.end method

.method public unregister()V
    .registers 3

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Not registered"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 118
    :cond_c
    iget-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredContext:Landroid/content/Context;

    #@e
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@11
    .line 119
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Lcom/android/internal/content/PackageMonitor;->mRegisteredContext:Landroid/content/Context;

    #@14
    .line 120
    return-void
.end method
