.class public Lcom/android/internal/content/PackageHelper;
.super Ljava/lang/Object;
.source "PackageHelper.java"


# static fields
.field public static final APP_INSTALL_AUTO:I = 0x0

.field public static final APP_INSTALL_EXTERNAL:I = 0x2

.field public static final APP_INSTALL_INTERNAL:I = 0x1

.field public static final RECOMMEND_FAILED_ALREADY_EXISTS:I = -0x4

.field public static final RECOMMEND_FAILED_INSUFFICIENT_STORAGE:I = -0x1

.field public static final RECOMMEND_FAILED_INVALID_APK:I = -0x2

.field public static final RECOMMEND_FAILED_INVALID_LOCATION:I = -0x3

.field public static final RECOMMEND_FAILED_INVALID_URI:I = -0x6

.field public static final RECOMMEND_FAILED_VERSION_DOWNGRADE:I = -0x7

.field public static final RECOMMEND_INSTALL_EXTERNAL:I = 0x2

.field public static final RECOMMEND_INSTALL_INTERNAL:I = 0x1

.field public static final RECOMMEND_MEDIA_UNAVAILABLE:I = -0x5

.field private static final TAG:Ljava/lang/String; = "PackageHelper"

.field private static final localLOGV:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static copyZipEntry(Ljava/util/zip/ZipEntry;Ljava/util/zip/ZipFile;Ljava/util/zip/ZipOutputStream;)V
    .registers 8
    .parameter "zipEntry"
    .parameter "inZipFile"
    .parameter "outZipStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 270
    const/16 v4, 0x1000

    #@2
    new-array v0, v4, [B

    #@4
    .line 274
    .local v0, buffer:[B
    invoke-virtual {p0}, Ljava/util/zip/ZipEntry;->getMethod()I

    #@7
    move-result v4

    #@8
    if-nez v4, :cond_26

    #@a
    .line 276
    new-instance v2, Ljava/util/zip/ZipEntry;

    #@c
    invoke-direct {v2, p0}, Ljava/util/zip/ZipEntry;-><init>(Ljava/util/zip/ZipEntry;)V

    #@f
    .line 281
    .local v2, newEntry:Ljava/util/zip/ZipEntry;
    :goto_f
    invoke-virtual {p2, v2}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    #@12
    .line 283
    invoke-virtual {p1, p0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    #@15
    move-result-object v1

    #@16
    .line 285
    .local v1, data:Ljava/io/InputStream;
    :goto_16
    :try_start_16
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    #@19
    move-result v3

    #@1a
    .local v3, num:I
    if-lez v3, :cond_30

    #@1c
    .line 286
    const/4 v4, 0x0

    #@1d
    invoke-virtual {p2, v0, v4, v3}, Ljava/util/zip/ZipOutputStream;->write([BII)V
    :try_end_20
    .catchall {:try_start_16 .. :try_end_20} :catchall_21

    #@20
    goto :goto_16

    #@21
    .line 290
    .end local v3           #num:I
    :catchall_21
    move-exception v4

    #@22
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@25
    throw v4

    #@26
    .line 279
    .end local v1           #data:Ljava/io/InputStream;
    .end local v2           #newEntry:Ljava/util/zip/ZipEntry;
    :cond_26
    new-instance v2, Ljava/util/zip/ZipEntry;

    #@28
    invoke-virtual {p0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-direct {v2, v4}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    #@2f
    .restart local v2       #newEntry:Ljava/util/zip/ZipEntry;
    goto :goto_f

    #@30
    .line 288
    .restart local v1       #data:Ljava/io/InputStream;
    .restart local v3       #num:I
    :cond_30
    :try_start_30
    invoke-virtual {p2}, Ljava/util/zip/ZipOutputStream;->flush()V
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_21

    #@33
    .line 290
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@36
    .line 292
    return-void
.end method

.method public static createSdDir(ILjava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;
    .registers 15
    .parameter "sizeMb"
    .parameter "cid"
    .parameter "sdEncKey"
    .parameter "uid"
    .parameter "isExternal"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 75
    :try_start_1
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@4
    move-result-object v0

    #@5
    .line 80
    .local v0, mountService:Landroid/os/storage/IMountService;
    const-string v3, "ext4"

    #@7
    move-object v1, p1

    #@8
    move v2, p0

    #@9
    move-object v4, p2

    #@a
    move v5, p3

    #@b
    move v6, p4

    #@c
    invoke-interface/range {v0 .. v6}, Landroid/os/storage/IMountService;->createSecureContainer(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)I

    #@f
    move-result v9

    #@10
    .line 82
    .local v9, rc:I
    if-eqz v9, :cond_2b

    #@12
    .line 83
    const-string v1, "PackageHelper"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Failed to create secure container "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 93
    .end local v0           #mountService:Landroid/os/storage/IMountService;
    .end local v9           #rc:I
    :goto_2a
    return-object v7

    #@2b
    .line 86
    .restart local v0       #mountService:Landroid/os/storage/IMountService;
    .restart local v9       #rc:I
    :cond_2b
    invoke-interface {v0, p1}, Landroid/os/storage/IMountService;->getSecureContainerPath(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_2e} :catch_30

    #@2e
    move-result-object v7

    #@2f
    .line 89
    .local v7, cachePath:Ljava/lang/String;
    goto :goto_2a

    #@30
    .line 90
    .end local v0           #mountService:Landroid/os/storage/IMountService;
    .end local v7           #cachePath:Ljava/lang/String;
    .end local v9           #rc:I
    :catch_30
    move-exception v8

    #@31
    .line 91
    .local v8, e:Landroid/os/RemoteException;
    const-string v1, "PackageHelper"

    #@33
    const-string v2, "MountService running?"

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_2a
.end method

.method public static destroySdDir(Ljava/lang/String;)Z
    .registers 7
    .parameter "cid"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 187
    :try_start_2
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@5
    move-result-object v4

    #@6
    const/4 v5, 0x1

    #@7
    invoke-interface {v4, p0, v5}, Landroid/os/storage/IMountService;->destroySecureContainer(Ljava/lang/String;Z)I

    #@a
    move-result v1

    #@b
    .line 188
    .local v1, rc:I
    if-eqz v1, :cond_26

    #@d
    .line 189
    const-string v3, "PackageHelper"

    #@f
    new-instance v4, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v5, "Failed to destroy container "

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_25} :catch_28

    #@25
    .line 197
    .end local v1           #rc:I
    :goto_25
    return v2

    #@26
    .restart local v1       #rc:I
    :cond_26
    move v2, v3

    #@27
    .line 192
    goto :goto_25

    #@28
    .line 193
    .end local v1           #rc:I
    :catch_28
    move-exception v0

    #@29
    .line 194
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "PackageHelper"

    #@2b
    new-instance v4, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v5, "Failed to destroy container "

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    const-string v5, " with exception "

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    goto :goto_25
.end method

.method public static extractPublicFiles(Ljava/lang/String;Ljava/io/File;)I
    .registers 13
    .parameter "packagePath"
    .parameter "publicZipFile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 224
    if-nez p1, :cond_57

    #@2
    .line 225
    const/4 v0, 0x0

    #@3
    .line 226
    .local v0, fstr:Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    #@4
    .line 232
    .local v3, publicZipOutStream:Ljava/util/zip/ZipOutputStream;
    :goto_4
    const/4 v4, 0x0

    #@5
    .line 235
    .local v4, size:I
    :try_start_5
    new-instance v2, Ljava/util/zip/ZipFile;

    #@7
    invoke-direct {v2, p0}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_52

    #@a
    .line 238
    .local v2, privateZip:Ljava/util/zip/ZipFile;
    :try_start_a
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    #@d
    move-result-object v7

    #@e
    invoke-static {v7}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    #@11
    move-result-object v7

    #@12
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v7

    #@1a
    if-eqz v7, :cond_62

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v5

    #@20
    check-cast v5, Ljava/util/zip/ZipEntry;

    #@22
    .line 239
    .local v5, zipEntry:Ljava/util/zip/ZipEntry;
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    #@25
    move-result-object v6

    #@26
    .line 240
    .local v6, zipEntryName:Ljava/lang/String;
    const-string v7, "AndroidManifest.xml"

    #@28
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v7

    #@2c
    if-nez v7, :cond_40

    #@2e
    const-string/jumbo v7, "resources.arsc"

    #@31
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v7

    #@35
    if-nez v7, :cond_40

    #@37
    const-string/jumbo v7, "res/"

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_16

    #@40
    .line 243
    :cond_40
    int-to-long v7, v4

    #@41
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getSize()J

    #@44
    move-result-wide v9

    #@45
    add-long/2addr v7, v9

    #@46
    long-to-int v4, v7

    #@47
    .line 244
    if-eqz p1, :cond_16

    #@49
    .line 245
    invoke-static {v5, v2, v3}, Lcom/android/internal/content/PackageHelper;->copyZipEntry(Ljava/util/zip/ZipEntry;Ljava/util/zip/ZipFile;Ljava/util/zip/ZipOutputStream;)V
    :try_end_4c
    .catchall {:try_start_a .. :try_end_4c} :catchall_4d

    #@4c
    goto :goto_16

    #@4d
    .line 250
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v5           #zipEntry:Ljava/util/zip/ZipEntry;
    .end local v6           #zipEntryName:Ljava/lang/String;
    :catchall_4d
    move-exception v7

    #@4e
    :try_start_4e
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_51
    .catchall {:try_start_4e .. :try_end_51} :catchall_52
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_84

    #@51
    :goto_51
    :try_start_51
    throw v7
    :try_end_52
    .catchall {:try_start_51 .. :try_end_52} :catchall_52

    #@52
    .line 262
    .end local v2           #privateZip:Ljava/util/zip/ZipFile;
    :catchall_52
    move-exception v7

    #@53
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@56
    throw v7

    #@57
    .line 228
    .end local v0           #fstr:Ljava/io/FileOutputStream;
    .end local v3           #publicZipOutStream:Ljava/util/zip/ZipOutputStream;
    .end local v4           #size:I
    :cond_57
    new-instance v0, Ljava/io/FileOutputStream;

    #@59
    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@5c
    .line 229
    .restart local v0       #fstr:Ljava/io/FileOutputStream;
    new-instance v3, Ljava/util/zip/ZipOutputStream;

    #@5e
    invoke-direct {v3, v0}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@61
    .restart local v3       #publicZipOutStream:Ljava/util/zip/ZipOutputStream;
    goto :goto_4

    #@62
    .line 250
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #privateZip:Ljava/util/zip/ZipFile;
    .restart local v4       #size:I
    :cond_62
    :try_start_62
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_52
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_65} :catch_82

    #@65
    .line 253
    :goto_65
    if-eqz p1, :cond_7e

    #@67
    .line 254
    :try_start_67
    invoke-virtual {v3}, Ljava/util/zip/ZipOutputStream;->finish()V

    #@6a
    .line 255
    invoke-virtual {v3}, Ljava/util/zip/ZipOutputStream;->flush()V

    #@6d
    .line 256
    invoke-static {v0}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@70
    .line 257
    invoke-virtual {v3}, Ljava/util/zip/ZipOutputStream;->close()V

    #@73
    .line 258
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    const/16 v8, 0x1a4

    #@79
    const/4 v9, -0x1

    #@7a
    const/4 v10, -0x1

    #@7b
    invoke-static {v7, v8, v9, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_7e
    .catchall {:try_start_67 .. :try_end_7e} :catchall_52

    #@7e
    .line 262
    :cond_7e
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@81
    .line 265
    return v4

    #@82
    .line 250
    :catch_82
    move-exception v7

    #@83
    goto :goto_65

    #@84
    .end local v1           #i$:Ljava/util/Iterator;
    :catch_84
    move-exception v8

    #@85
    goto :goto_51
.end method

.method public static finalizeSdDir(Ljava/lang/String;)Z
    .registers 7
    .parameter "cid"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 171
    :try_start_1
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3, p0}, Landroid/os/storage/IMountService;->finalizeSecureContainer(Ljava/lang/String;)I

    #@8
    move-result v1

    #@9
    .line 172
    .local v1, rc:I
    if-eqz v1, :cond_24

    #@b
    .line 173
    const-string v3, "PackageHelper"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "Failed to finalize container "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_23} :catch_26

    #@23
    .line 181
    .end local v1           #rc:I
    :goto_23
    return v2

    #@24
    .line 176
    .restart local v1       #rc:I
    :cond_24
    const/4 v2, 0x1

    #@25
    goto :goto_23

    #@26
    .line 177
    .end local v1           #rc:I
    :catch_26
    move-exception v0

    #@27
    .line 178
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "PackageHelper"

    #@29
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v5, "Failed to finalize container "

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    const-string v5, " with exception "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_23
.end method

.method public static fixSdPermissions(Ljava/lang/String;ILjava/lang/String;)Z
    .registers 9
    .parameter "cid"
    .parameter "gid"
    .parameter "filename"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 296
    :try_start_1
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3, p0, p1, p2}, Landroid/os/storage/IMountService;->fixPermissionsSecureContainer(Ljava/lang/String;ILjava/lang/String;)I

    #@8
    move-result v1

    #@9
    .line 297
    .local v1, rc:I
    if-eqz v1, :cond_24

    #@b
    .line 298
    const-string v3, "PackageHelper"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "Failed to fixperms container "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_23} :catch_26

    #@23
    .line 305
    .end local v1           #rc:I
    :goto_23
    return v2

    #@24
    .line 301
    .restart local v1       #rc:I
    :cond_24
    const/4 v2, 0x1

    #@25
    goto :goto_23

    #@26
    .line 302
    .end local v1           #rc:I
    :catch_26
    move-exception v0

    #@27
    .line 303
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "PackageHelper"

    #@29
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v5, "Failed to fixperms container "

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    const-string v5, " with exception "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_23
.end method

.method public static getMountService()Landroid/os/storage/IMountService;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 62
    const-string/jumbo v1, "mount"

    #@3
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    .line 63
    .local v0, service:Landroid/os/IBinder;
    if-eqz v0, :cond_e

    #@9
    .line 64
    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@c
    move-result-object v1

    #@d
    return-object v1

    #@e
    .line 66
    :cond_e
    const-string v1, "PackageHelper"

    #@10
    const-string v2, "Can\'t get mount service"

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 67
    new-instance v1, Landroid/os/RemoteException;

    #@17
    const-string v2, "Could not contact mount service"

    #@19
    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1
.end method

.method public static getSdDir(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "cid"

    #@0
    .prologue
    .line 142
    :try_start_0
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0}, Landroid/os/storage/IMountService;->getSecureContainerPath(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_2e

    #@7
    move-result-object v1

    #@8
    .line 156
    :goto_8
    return-object v1

    #@9
    .line 143
    :catch_9
    move-exception v0

    #@a
    .line 144
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "PackageHelper"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Failed to get container path for "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, " with exception "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 156
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_2c
    const/4 v1, 0x0

    #@2d
    goto :goto_8

    #@2e
    .line 151
    :catch_2e
    move-exception v0

    #@2f
    .line 152
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "PackageHelper"

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "Failed to get container path for "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    const-string v3, " with Exception "

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    goto :goto_2c
.end method

.method public static getSdFilesystem(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "cid"

    #@0
    .prologue
    .line 161
    :try_start_0
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0}, Landroid/os/storage/IMountService;->getSecureContainerFilesystemPath(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 166
    :goto_8
    return-object v1

    #@9
    .line 162
    :catch_9
    move-exception v0

    #@a
    .line 163
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "PackageHelper"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Failed to get container path for "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, " with exception "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 166
    const/4 v1, 0x0

    #@2d
    goto :goto_8
.end method

.method public static getSecureContainerList()[Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 202
    :try_start_0
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/os/storage/IMountService;->getSecureContainerList()[Ljava/lang/String;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 207
    .local v0, e:Landroid/os/RemoteException;
    :goto_8
    return-object v1

    #@9
    .line 203
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_9
    move-exception v0

    #@a
    .line 204
    .restart local v0       #e:Landroid/os/RemoteException;
    const-string v1, "PackageHelper"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Failed to get secure container list with exception"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 207
    const/4 v1, 0x0

    #@23
    goto :goto_8
.end method

.method public static isContainerMounted(Ljava/lang/String;)Z
    .registers 5
    .parameter "cid"

    #@0
    .prologue
    .line 212
    :try_start_0
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0}, Landroid/os/storage/IMountService;->isSecureContainerMounted(Ljava/lang/String;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 216
    :goto_8
    return v1

    #@9
    .line 213
    :catch_9
    move-exception v0

    #@a
    .line 214
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "PackageHelper"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Failed to find out if container "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, " mounted"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 216
    const/4 v1, 0x0

    #@29
    goto :goto_8
.end method

.method public static mountSdDir(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .registers 9
    .parameter "cid"
    .parameter "key"
    .parameter "ownerUid"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 98
    :try_start_1
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3, p0, p1, p2}, Landroid/os/storage/IMountService;->mountSecureContainer(Ljava/lang/String;Ljava/lang/String;I)I

    #@8
    move-result v1

    #@9
    .line 99
    .local v1, rc:I
    if-eqz v1, :cond_2e

    #@b
    .line 100
    const-string v3, "PackageHelper"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "Failed to mount container "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, " rc : "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 107
    .end local v1           #rc:I
    :goto_2d
    return-object v2

    #@2e
    .line 103
    .restart local v1       #rc:I
    :cond_2e
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@31
    move-result-object v3

    #@32
    invoke-interface {v3, p0}, Landroid/os/storage/IMountService;->getSecureContainerPath(Ljava/lang/String;)Ljava/lang/String;
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_35} :catch_37

    #@35
    move-result-object v2

    #@36
    goto :goto_2d

    #@37
    .line 104
    .end local v1           #rc:I
    :catch_37
    move-exception v0

    #@38
    .line 105
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "PackageHelper"

    #@3a
    const-string v4, "MountService running?"

    #@3c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_2d
.end method

.method public static renameSdDir(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8
    .parameter "oldId"
    .parameter "newId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 126
    :try_start_1
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3, p0, p1}, Landroid/os/storage/IMountService;->renameSecureContainer(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    move-result v1

    #@9
    .line 127
    .local v1, rc:I
    if-eqz v1, :cond_39

    #@b
    .line 128
    const-string v3, "PackageHelper"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "Failed to rename "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, " to "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    const-string/jumbo v5, "with rc "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_38
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_38} :catch_3b

    #@38
    .line 137
    .end local v1           #rc:I
    :goto_38
    return v2

    #@39
    .line 132
    .restart local v1       #rc:I
    :cond_39
    const/4 v2, 0x1

    #@3a
    goto :goto_38

    #@3b
    .line 133
    .end local v1           #rc:I
    :catch_3b
    move-exception v0

    #@3c
    .line 134
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "PackageHelper"

    #@3e
    new-instance v4, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v5, "Failed ot rename  "

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v5, " to "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    const-string v5, " with exception : "

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_38
.end method

.method public static unMountSdDir(Ljava/lang/String;)Z
    .registers 7
    .parameter "cid"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 112
    :try_start_2
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@5
    move-result-object v4

    #@6
    const/4 v5, 0x1

    #@7
    invoke-interface {v4, p0, v5}, Landroid/os/storage/IMountService;->unmountSecureContainer(Ljava/lang/String;Z)I

    #@a
    move-result v1

    #@b
    .line 113
    .local v1, rc:I
    if-eqz v1, :cond_30

    #@d
    .line 114
    const-string v3, "PackageHelper"

    #@f
    new-instance v4, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v5, "Failed to unmount "

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    const-string v5, " with rc "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2f} :catch_32

    #@2f
    .line 121
    .end local v1           #rc:I
    :goto_2f
    return v2

    #@30
    .restart local v1       #rc:I
    :cond_30
    move v2, v3

    #@31
    .line 117
    goto :goto_2f

    #@32
    .line 118
    .end local v1           #rc:I
    :catch_32
    move-exception v0

    #@33
    .line 119
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "PackageHelper"

    #@35
    const-string v4, "MountService running?"

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_2f
.end method
