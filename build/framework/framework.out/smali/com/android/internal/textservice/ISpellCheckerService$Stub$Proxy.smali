.class Lcom/android/internal/textservice/ISpellCheckerService$Stub$Proxy;
.super Ljava/lang/Object;
.source "ISpellCheckerService.java"

# interfaces
.implements Lcom/android/internal/textservice/ISpellCheckerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/textservice/ISpellCheckerService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 76
    iput-object p1, p0, Lcom/android/internal/textservice/ISpellCheckerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 77
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/internal/textservice/ISpellCheckerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getISpellCheckerSession(Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)Lcom/android/internal/textservice/ISpellCheckerSession;
    .registers 10
    .parameter "locale"
    .parameter "listener"
    .parameter "bundle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 89
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 92
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.textservice.ISpellCheckerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 93
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 94
    if-eqz p2, :cond_3c

    #@12
    invoke-interface {p2}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->asBinder()Landroid/os/IBinder;

    #@15
    move-result-object v3

    #@16
    :goto_16
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@19
    .line 95
    if-eqz p3, :cond_3e

    #@1b
    .line 96
    const/4 v3, 0x1

    #@1c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 97
    const/4 v3, 0x0

    #@20
    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 102
    :goto_23
    iget-object v3, p0, Lcom/android/internal/textservice/ISpellCheckerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@25
    const/4 v4, 0x1

    #@26
    const/4 v5, 0x0

    #@27
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 103
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 104
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@30
    move-result-object v3

    #@31
    invoke-static {v3}, Lcom/android/internal/textservice/ISpellCheckerSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/textservice/ISpellCheckerSession;
    :try_end_34
    .catchall {:try_start_8 .. :try_end_34} :catchall_43

    #@34
    move-result-object v2

    #@35
    .line 107
    .local v2, _result:Lcom/android/internal/textservice/ISpellCheckerSession;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 108
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 110
    return-object v2

    #@3c
    .line 94
    .end local v2           #_result:Lcom/android/internal/textservice/ISpellCheckerSession;
    :cond_3c
    const/4 v3, 0x0

    #@3d
    goto :goto_16

    #@3e
    .line 100
    :cond_3e
    const/4 v3, 0x0

    #@3f
    :try_start_3f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_43

    #@42
    goto :goto_23

    #@43
    .line 107
    :catchall_43
    move-exception v3

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 108
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 84
    const-string v0, "com.android.internal.textservice.ISpellCheckerService"

    #@2
    return-object v0
.end method
