.class Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "ITextServicesManager.java"

# interfaces
.implements Lcom/android/internal/textservice/ITextServicesManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/textservice/ITextServicesManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 163
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 164
    iput-object p1, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 165
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public finishSpellCheckerService(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 245
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 247
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.textservice.ITextServicesManager"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 248
    if-eqz p1, :cond_10

    #@c
    invoke-interface {p1}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->asBinder()Landroid/os/IBinder;

    #@f
    move-result-object v1

    #@10
    :cond_10
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@13
    .line 249
    iget-object v1, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x4

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_5 .. :try_end_1b} :catchall_1f

    #@1b
    .line 252
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 254
    return-void

    #@1f
    .line 252
    :catchall_1f
    move-exception v1

    #@20
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    throw v1
.end method

.method public getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;
    .registers 8
    .parameter "locale"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 176
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 177
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 180
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.textservice.ITextServicesManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 181
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 182
    iget-object v3, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 183
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 184
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_2f

    #@20
    .line 185
    sget-object v3, Landroid/view/textservice/SpellCheckerInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/view/textservice/SpellCheckerInfo;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_31

    #@28
    .line 192
    .local v2, _result:Landroid/view/textservice/SpellCheckerInfo;
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 193
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 195
    return-object v2

    #@2f
    .line 188
    .end local v2           #_result:Landroid/view/textservice/SpellCheckerInfo;
    :cond_2f
    const/4 v2, 0x0

    #@30
    .restart local v2       #_result:Landroid/view/textservice/SpellCheckerInfo;
    goto :goto_28

    #@31
    .line 192
    .end local v2           #_result:Landroid/view/textservice/SpellCheckerInfo;
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 193
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public getCurrentSpellCheckerSubtype(Ljava/lang/String;Z)Landroid/view/textservice/SpellCheckerSubtype;
    .registers 9
    .parameter "locale"
    .parameter "allowImplicitlySelectedSubtype"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 199
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 200
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 203
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v4, "com.android.internal.textservice.ITextServicesManager"

    #@b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 204
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 205
    if-eqz p2, :cond_14

    #@13
    const/4 v3, 0x1

    #@14
    :cond_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 206
    iget-object v3, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x2

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 207
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 208
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_36

    #@27
    .line 209
    sget-object v3, Landroid/view/textservice/SpellCheckerSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c
    move-result-object v2

    #@2d
    check-cast v2, Landroid/view/textservice/SpellCheckerSubtype;
    :try_end_2f
    .catchall {:try_start_9 .. :try_end_2f} :catchall_38

    #@2f
    .line 216
    .local v2, _result:Landroid/view/textservice/SpellCheckerSubtype;
    :goto_2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 217
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 219
    return-object v2

    #@36
    .line 212
    .end local v2           #_result:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_36
    const/4 v2, 0x0

    #@37
    .restart local v2       #_result:Landroid/view/textservice/SpellCheckerSubtype;
    goto :goto_2f

    #@38
    .line 216
    .end local v2           #_result:Landroid/view/textservice/SpellCheckerSubtype;
    :catchall_38
    move-exception v3

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 217
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v3
.end method

.method public getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 312
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 313
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 316
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.textservice.ITextServicesManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 317
    iget-object v3, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x9

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 318
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 319
    sget-object v3, Landroid/view/textservice/SpellCheckerInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, [Landroid/view/textservice/SpellCheckerInfo;
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_27

    #@20
    .line 322
    .local v2, _result:[Landroid/view/textservice/SpellCheckerInfo;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 323
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 325
    return-object v2

    #@27
    .line 322
    .end local v2           #_result:[Landroid/view/textservice/SpellCheckerInfo;
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 323
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 172
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@2
    return-object v0
.end method

.method public getSpellCheckerService(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)V
    .registers 11
    .parameter "sciId"
    .parameter "locale"
    .parameter "tsListener"
    .parameter "scListener"
    .parameter "bundle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 223
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 225
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.textservice.ITextServicesManager"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 226
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 227
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 228
    if-eqz p3, :cond_38

    #@12
    invoke-interface {p3}, Lcom/android/internal/textservice/ITextServicesSessionListener;->asBinder()Landroid/os/IBinder;

    #@15
    move-result-object v2

    #@16
    :goto_16
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@19
    .line 229
    if-eqz p4, :cond_1f

    #@1b
    invoke-interface {p4}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v1

    #@1f
    :cond_1f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@22
    .line 230
    if-eqz p5, :cond_3a

    #@24
    .line 231
    const/4 v1, 0x1

    #@25
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 232
    const/4 v1, 0x0

    #@29
    invoke-virtual {p5, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@2c
    .line 237
    :goto_2c
    iget-object v1, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2e
    const/4 v2, 0x3

    #@2f
    const/4 v3, 0x0

    #@30
    const/4 v4, 0x1

    #@31
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_34
    .catchall {:try_start_5 .. :try_end_34} :catchall_3f

    #@34
    .line 240
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 242
    return-void

    #@38
    :cond_38
    move-object v2, v1

    #@39
    .line 228
    goto :goto_16

    #@3a
    .line 235
    :cond_3a
    const/4 v1, 0x0

    #@3b
    :try_start_3b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_3f

    #@3e
    goto :goto_2c

    #@3f
    .line 240
    :catchall_3f
    move-exception v1

    #@40
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v1
.end method

.method public isSpellCheckerEnabled()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 295
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 296
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 299
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.textservice.ITextServicesManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 300
    iget-object v3, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x8

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 301
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 302
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 305
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 306
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 308
    return v2

    #@27
    .line 305
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 306
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public setCurrentSpellChecker(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "locale"
    .parameter "sciId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 257
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 259
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.textservice.ITextServicesManager"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 260
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 261
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 262
    iget-object v1, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x5

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 265
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 267
    return-void

    #@1b
    .line 265
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method

.method public setCurrentSpellCheckerSubtype(Ljava/lang/String;I)V
    .registers 8
    .parameter "locale"
    .parameter "hashCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 270
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 272
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.textservice.ITextServicesManager"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 273
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 274
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 275
    iget-object v1, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x6

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 278
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 280
    return-void

    #@1b
    .line 278
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method

.method public setSpellCheckerEnabled(Z)V
    .registers 7
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 283
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 285
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.textservice.ITextServicesManager"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 286
    if-eqz p1, :cond_1b

    #@c
    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 287
    iget-object v1, p0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x7

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1d

    #@17
    .line 290
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 292
    return-void

    #@1b
    .line 286
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_c

    #@1d
    .line 290
    :catchall_1d
    move-exception v1

    #@1e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    throw v1
.end method
