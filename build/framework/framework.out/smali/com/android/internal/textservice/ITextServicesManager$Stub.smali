.class public abstract Lcom/android/internal/textservice/ITextServicesManager$Stub;
.super Landroid/os/Binder;
.source "ITextServicesManager.java"

# interfaces
.implements Lcom/android/internal/textservice/ITextServicesManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/textservice/ITextServicesManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.textservice.ITextServicesManager"

.field static final TRANSACTION_finishSpellCheckerService:I = 0x4

.field static final TRANSACTION_getCurrentSpellChecker:I = 0x1

.field static final TRANSACTION_getCurrentSpellCheckerSubtype:I = 0x2

.field static final TRANSACTION_getEnabledSpellCheckers:I = 0x9

.field static final TRANSACTION_getSpellCheckerService:I = 0x3

.field static final TRANSACTION_isSpellCheckerEnabled:I = 0x8

.field static final TRANSACTION_setCurrentSpellChecker:I = 0x5

.field static final TRANSACTION_setCurrentSpellCheckerSubtype:I = 0x6

.field static final TRANSACTION_setSpellCheckerEnabled:I = 0x7


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/textservice/ITextServicesManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.android.internal.textservice.ITextServicesManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/textservice/ITextServicesManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/android/internal/textservice/ITextServicesManager;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/textservice/ITextServicesManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 42
    sparse-switch p1, :sswitch_data_f4

    #@5
    .line 157
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v7

    #@9
    :goto_9
    return v7

    #@a
    .line 46
    :sswitch_a
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 51
    :sswitch_10
    const-string v8, "com.android.internal.textservice.ITextServicesManager"

    #@12
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 54
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@1c
    move-result-object v6

    #@1d
    .line 55
    .local v6, _result:Landroid/view/textservice/SpellCheckerInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 56
    if-eqz v6, :cond_29

    #@22
    .line 57
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 58
    invoke-virtual {v6, p3, v7}, Landroid/view/textservice/SpellCheckerInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@28
    goto :goto_9

    #@29
    .line 61
    :cond_29
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    goto :goto_9

    #@2d
    .line 67
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v6           #_result:Landroid/view/textservice/SpellCheckerInfo;
    :sswitch_2d
    const-string v8, "com.android.internal.textservice.ITextServicesManager"

    #@2f
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    .line 71
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v8

    #@3a
    if-eqz v8, :cond_4d

    #@3c
    move v2, v7

    #@3d
    .line 72
    .local v2, _arg1:Z
    :goto_3d
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->getCurrentSpellCheckerSubtype(Ljava/lang/String;Z)Landroid/view/textservice/SpellCheckerSubtype;

    #@40
    move-result-object v6

    #@41
    .line 73
    .local v6, _result:Landroid/view/textservice/SpellCheckerSubtype;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44
    .line 74
    if-eqz v6, :cond_4f

    #@46
    .line 75
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    .line 76
    invoke-virtual {v6, p3, v7}, Landroid/view/textservice/SpellCheckerSubtype;->writeToParcel(Landroid/os/Parcel;I)V

    #@4c
    goto :goto_9

    #@4d
    .end local v2           #_arg1:Z
    .end local v6           #_result:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_4d
    move v2, v0

    #@4e
    .line 71
    goto :goto_3d

    #@4f
    .line 79
    .restart local v2       #_arg1:Z
    .restart local v6       #_result:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_4f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    goto :goto_9

    #@53
    .line 85
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Z
    .end local v6           #_result:Landroid/view/textservice/SpellCheckerSubtype;
    :sswitch_53
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@55
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58
    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    .line 89
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    .line 91
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@63
    move-result-object v0

    #@64
    invoke-static {v0}, Lcom/android/internal/textservice/ITextServicesSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/textservice/ITextServicesSessionListener;

    #@67
    move-result-object v3

    #@68
    .line 93
    .local v3, _arg2:Lcom/android/internal/textservice/ITextServicesSessionListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@6b
    move-result-object v0

    #@6c
    invoke-static {v0}, Lcom/android/internal/textservice/ISpellCheckerSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@6f
    move-result-object v4

    #@70
    .line 95
    .local v4, _arg3:Lcom/android/internal/textservice/ISpellCheckerSessionListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v0

    #@74
    if-eqz v0, :cond_83

    #@76
    .line 96
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@78
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7b
    move-result-object v5

    #@7c
    check-cast v5, Landroid/os/Bundle;

    #@7e
    .local v5, _arg4:Landroid/os/Bundle;
    :goto_7e
    move-object v0, p0

    #@7f
    .line 101
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->getSpellCheckerService(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)V

    #@82
    goto :goto_9

    #@83
    .line 99
    .end local v5           #_arg4:Landroid/os/Bundle;
    :cond_83
    const/4 v5, 0x0

    #@84
    .restart local v5       #_arg4:Landroid/os/Bundle;
    goto :goto_7e

    #@85
    .line 106
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Lcom/android/internal/textservice/ITextServicesSessionListener;
    .end local v4           #_arg3:Lcom/android/internal/textservice/ISpellCheckerSessionListener;
    .end local v5           #_arg4:Landroid/os/Bundle;
    :sswitch_85
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@87
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8a
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8d
    move-result-object v0

    #@8e
    invoke-static {v0}, Lcom/android/internal/textservice/ISpellCheckerSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@91
    move-result-object v1

    #@92
    .line 109
    .local v1, _arg0:Lcom/android/internal/textservice/ISpellCheckerSessionListener;
    invoke-virtual {p0, v1}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->finishSpellCheckerService(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)V

    #@95
    goto/16 :goto_9

    #@97
    .line 114
    .end local v1           #_arg0:Lcom/android/internal/textservice/ISpellCheckerSessionListener;
    :sswitch_97
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@99
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9c
    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9f
    move-result-object v1

    #@a0
    .line 118
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a3
    move-result-object v2

    #@a4
    .line 119
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->setCurrentSpellChecker(Ljava/lang/String;Ljava/lang/String;)V

    #@a7
    goto/16 :goto_9

    #@a9
    .line 124
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_a9
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@ab
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b1
    move-result-object v1

    #@b2
    .line 128
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b5
    move-result v2

    #@b6
    .line 129
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->setCurrentSpellCheckerSubtype(Ljava/lang/String;I)V

    #@b9
    goto/16 :goto_9

    #@bb
    .line 134
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    :sswitch_bb
    const-string v8, "com.android.internal.textservice.ITextServicesManager"

    #@bd
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0
    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c3
    move-result v8

    #@c4
    if-eqz v8, :cond_cc

    #@c6
    move v1, v7

    #@c7
    .line 137
    .local v1, _arg0:Z
    :goto_c7
    invoke-virtual {p0, v1}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->setSpellCheckerEnabled(Z)V

    #@ca
    goto/16 :goto_9

    #@cc
    .end local v1           #_arg0:Z
    :cond_cc
    move v1, v0

    #@cd
    .line 136
    goto :goto_c7

    #@ce
    .line 142
    :sswitch_ce
    const-string v8, "com.android.internal.textservice.ITextServicesManager"

    #@d0
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d3
    .line 143
    invoke-virtual {p0}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->isSpellCheckerEnabled()Z

    #@d6
    move-result v6

    #@d7
    .line 144
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@da
    .line 145
    if-eqz v6, :cond_dd

    #@dc
    move v0, v7

    #@dd
    :cond_dd
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e0
    goto/16 :goto_9

    #@e2
    .line 150
    .end local v6           #_result:Z
    :sswitch_e2
    const-string v0, "com.android.internal.textservice.ITextServicesManager"

    #@e4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e7
    .line 151
    invoke-virtual {p0}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;

    #@ea
    move-result-object v6

    #@eb
    .line 152
    .local v6, _result:[Landroid/view/textservice/SpellCheckerInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ee
    .line 153
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@f1
    goto/16 :goto_9

    #@f3
    .line 42
    nop

    #@f4
    :sswitch_data_f4
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_53
        0x4 -> :sswitch_85
        0x5 -> :sswitch_97
        0x6 -> :sswitch_a9
        0x7 -> :sswitch_bb
        0x8 -> :sswitch_ce
        0x9 -> :sswitch_e2
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
