.class Lcom/android/internal/textservice/ISpellCheckerSession$Stub$Proxy;
.super Ljava/lang/Object;
.source "ISpellCheckerSession.java"

# interfaces
.implements Lcom/android/internal/textservice/ISpellCheckerSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/textservice/ISpellCheckerSession$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 89
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 90
    iput-object p1, p0, Lcom/android/internal/textservice/ISpellCheckerSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 91
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/textservice/ISpellCheckerSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 98
    const-string v0, "com.android.internal.textservice.ISpellCheckerSession"

    #@2
    return-object v0
.end method

.method public onCancel()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 131
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.textservice.ISpellCheckerSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 132
    iget-object v1, p0, Lcom/android/internal/textservice/ISpellCheckerSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x3

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 135
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 137
    return-void

    #@15
    .line 135
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public onClose()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 140
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 142
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.textservice.ISpellCheckerSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 143
    iget-object v1, p0, Lcom/android/internal/textservice/ISpellCheckerSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x4

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 146
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 148
    return-void

    #@15
    .line 146
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public onGetSentenceSuggestionsMultiple([Landroid/view/textservice/TextInfo;I)V
    .registers 8
    .parameter "textInfos"
    .parameter "suggestionsLimit"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 116
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 118
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.textservice.ISpellCheckerSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 119
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, p1, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@d
    .line 120
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 121
    iget-object v1, p0, Lcom/android/internal/textservice/ISpellCheckerSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v2, 0x2

    #@13
    const/4 v3, 0x0

    #@14
    const/4 v4, 0x1

    #@15
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_1c

    #@18
    .line 124
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 126
    return-void

    #@1c
    .line 124
    :catchall_1c
    move-exception v1

    #@1d
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    throw v1
.end method

.method public onGetSuggestionsMultiple([Landroid/view/textservice/TextInfo;IZ)V
    .registers 9
    .parameter "textInfos"
    .parameter "suggestionsLimit"
    .parameter "multipleWords"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 102
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 104
    .local v0, _data:Landroid/os/Parcel;
    :try_start_6
    const-string v3, "com.android.internal.textservice.ISpellCheckerSession"

    #@8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@b
    .line 105
    const/4 v3, 0x0

    #@c
    invoke-virtual {v0, p1, v3}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@f
    .line 106
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 107
    if-eqz p3, :cond_23

    #@14
    :goto_14
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 108
    iget-object v1, p0, Lcom/android/internal/textservice/ISpellCheckerSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v2, 0x1

    #@1a
    const/4 v3, 0x0

    #@1b
    const/4 v4, 0x1

    #@1c
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1f
    .catchall {:try_start_6 .. :try_end_1f} :catchall_25

    #@1f
    .line 111
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 113
    return-void

    #@23
    :cond_23
    move v1, v2

    #@24
    .line 107
    goto :goto_14

    #@25
    .line 111
    :catchall_25
    move-exception v1

    #@26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v1
.end method
