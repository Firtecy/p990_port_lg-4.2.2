.class public Lcom/vzw/location/VzwGpsPerformance;
.super Ljava/lang/Object;
.source "VzwGpsPerformance.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vzw/location/VzwGpsPerformance;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mHorizontalAccuracy:J

.field private mPreferredResponseTime:J

.field private mVerticalAccuracy:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 83
    new-instance v0, Lcom/vzw/location/VzwGpsPerformance$1;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsPerformance$1;-><init>()V

    #@5
    sput-object v0, Lcom/vzw/location/VzwGpsPerformance;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 11
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const-wide/16 v0, 0x5

    #@2
    .line 16
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 12
    iput-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mVerticalAccuracy:J

    #@7
    .line 13
    iput-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mHorizontalAccuracy:J

    #@9
    .line 14
    const-wide/16 v0, 0x3c

    #@b
    iput-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mPreferredResponseTime:J

    #@d
    .line 17
    return-void
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwGpsPerformance;J)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 13
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsPerformance;->mHorizontalAccuracy:J

    #@2
    return-void
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwGpsPerformance;J)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 12
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsPerformance;->mVerticalAccuracy:J

    #@2
    return-void
.end method

.method static synthetic access$2(Lcom/vzw/location/VzwGpsPerformance;J)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 14
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsPerformance;->mPreferredResponseTime:J

    #@2
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 74
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    const-string/jumbo v1, "mHorizontalAccuracy ="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsPerformance;->mHorizontalAccuracy:J

    #@12
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1d
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    #@1f
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@26
    const-string/jumbo v1, "mVerticalAccuracy ="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsPerformance;->mVerticalAccuracy:J

    #@2f
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@3a
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    #@3c
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@43
    const-string/jumbo v1, "mPreferredResponseTime ="

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsPerformance;->mPreferredResponseTime:J

    #@4c
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@57
    .line 66
    return-void
.end method

.method public getHorizontalAccuracy()J
    .registers 3

    #@0
    .prologue
    .line 43
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mHorizontalAccuracy:J

    #@2
    return-wide v0
.end method

.method public getPreferredResponseTime()J
    .registers 3

    #@0
    .prologue
    .line 59
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mPreferredResponseTime:J

    #@2
    return-wide v0
.end method

.method public getVerticalAccuracy()J
    .registers 3

    #@0
    .prologue
    .line 51
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mVerticalAccuracy:J

    #@2
    return-wide v0
.end method

.method public setPerformance(JJJ)V
    .registers 10
    .parameter "verticalAccuracy"
    .parameter "horizontalAccuracy"
    .parameter "preferredResponseTime"

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 24
    cmp-long v0, p3, v1

    #@4
    if-gez v0, :cond_1b

    #@6
    .line 25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    const-string v2, "horizontalAccuracy="

    #@c
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@f
    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 27
    :cond_1b
    cmp-long v0, p1, v1

    #@1d
    if-gez v0, :cond_35

    #@1f
    .line 28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@21
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    const-string/jumbo v2, "verticalAccuracy="

    #@26
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@29
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@34
    throw v0

    #@35
    .line 30
    :cond_35
    cmp-long v0, p5, v1

    #@37
    if-gez v0, :cond_4f

    #@39
    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@3b
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d
    const-string/jumbo v2, "preferredResponseTime="

    #@40
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@43
    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4e
    throw v0

    #@4f
    .line 33
    :cond_4f
    iput-wide p3, p0, Lcom/vzw/location/VzwGpsPerformance;->mHorizontalAccuracy:J

    #@51
    .line 34
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsPerformance;->mVerticalAccuracy:J

    #@53
    .line 35
    iput-wide p5, p0, Lcom/vzw/location/VzwGpsPerformance;->mPreferredResponseTime:J

    #@55
    .line 36
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string/jumbo v1, "mHorizontalAccuracy="

    #@5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsPerformance;->mHorizontalAccuracy:J

    #@a
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v1, ", mVerticalAccuracy="

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsPerformance;->mVerticalAccuracy:J

    #@16
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v1, ", mPreferredResponseTime ="

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsPerformance;->mPreferredResponseTime:J

    #@22
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 78
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mHorizontalAccuracy:J

    #@2
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 79
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mVerticalAccuracy:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 80
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsPerformance;->mPreferredResponseTime:J

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 81
    return-void
.end method
