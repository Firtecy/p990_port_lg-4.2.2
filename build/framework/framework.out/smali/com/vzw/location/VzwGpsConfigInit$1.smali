.class Lcom/vzw/location/VzwGpsConfigInit$1;
.super Ljava/lang/Object;
.source "VzwGpsConfigInit.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwGpsConfigInit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/vzw/location/VzwGpsConfigInit;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 266
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsConfigInit;
    .registers 7
    .parameter "parcel"

    #@0
    .prologue
    .line 268
    new-instance v2, Lcom/vzw/location/VzwGpsConfigInit;

    #@2
    invoke-direct {v2}, Lcom/vzw/location/VzwGpsConfigInit;-><init>()V

    #@5
    .line 269
    .local v2, vz:Lcom/vzw/location/VzwGpsConfigInit;
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@8
    move-result-wide v3

    #@9
    invoke-static {v2, v3, v4}, Lcom/vzw/location/VzwGpsConfigInit;->access$0(Lcom/vzw/location/VzwGpsConfigInit;J)V

    #@c
    .line 270
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    invoke-static {v2, v3}, Lcom/vzw/location/VzwGpsConfigInit;->access$1(Lcom/vzw/location/VzwGpsConfigInit;Ljava/lang/String;)V

    #@13
    .line 271
    const/4 v1, 0x0

    #@14
    .line 272
    .local v1, len:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v1

    #@18
    if-ltz v1, :cond_26

    #@1a
    .line 273
    new-array v0, v1, [B

    #@1c
    .line 274
    .local v0, bytes:[B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    #@1f
    .line 276
    :try_start_1f
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@22
    move-result-object v3

    #@23
    invoke-static {v2, v3}, Lcom/vzw/location/VzwGpsConfigInit;->access$2(Lcom/vzw/location/VzwGpsConfigInit;Ljava/net/InetAddress;)V
    :try_end_26
    .catch Ljava/net/UnknownHostException; {:try_start_1f .. :try_end_26} :catch_42

    #@26
    .line 280
    .end local v0           #bytes:[B
    :cond_26
    :goto_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v3

    #@2a
    invoke-static {v2, v3}, Lcom/vzw/location/VzwGpsConfigInit;->access$3(Lcom/vzw/location/VzwGpsConfigInit;I)V

    #@2d
    .line 281
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Lcom/vzw/location/VzwGpsConfigInit;->access$4(Lcom/vzw/location/VzwGpsConfigInit;Ljava/lang/String;)V

    #@34
    .line 282
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3}, Lcom/vzw/location/VzwGpsConfigInit;->access$5(Lcom/vzw/location/VzwGpsConfigInit;Ljava/lang/String;)V

    #@3b
    .line 283
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    iput-object v3, v2, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@41
    .line 284
    return-object v2

    #@42
    .line 277
    .restart local v0       #bytes:[B
    :catch_42
    move-exception v3

    #@43
    goto :goto_26
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsConfigInit$1;->createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsConfigInit;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/vzw/location/VzwGpsConfigInit;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 288
    new-array v0, p1, [Lcom/vzw/location/VzwGpsConfigInit;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsConfigInit$1;->newArray(I)[Lcom/vzw/location/VzwGpsConfigInit;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
