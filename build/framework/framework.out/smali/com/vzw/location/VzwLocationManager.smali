.class public final Lcom/vzw/location/VzwLocationManager;
.super Ljava/lang/Object;
.source "VzwLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vzw/location/VzwLocationManager$ListenerTransport;,
        Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;
    }
.end annotation


# static fields
.field public static final LOCAL_LOGD:Z = false

.field public static final LOCAL_LOGV:Z = false

.field private static final TAG:Ljava/lang/String; = "VzwLocationManager"

.field public static final VZW_LBS_PROVIDER:Ljava/lang/String; = "vzw_lbs"


# instance fields
.field private final mGpsDeviceStatus:Lcom/vzw/location/VzwGpsDeviceStatus;

.field private final mGpsStatus:Lcom/vzw/location/VzwGpsStatus;

.field private final mGpsStatusListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vzw/location/VzwGpsStatus$Listener;",
            "Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/location/LocationListener;",
            "Lcom/vzw/location/VzwLocationManager$ListenerTransport;",
            ">;"
        }
    .end annotation
.end field

.field private mPrevCriteria:Lcom/vzw/location/VzwCriteria;

.field private mService:Lcom/vzw/location/IVzwLocationManager;


# direct methods
.method public constructor <init>(Lcom/vzw/location/IVzwLocationManager;)V
    .registers 4
    .parameter "service"

    #@0
    .prologue
    .line 209
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mGpsStatusListeners:Ljava/util/HashMap;

    #@a
    .line 35
    new-instance v0, Lcom/vzw/location/VzwGpsStatus;

    #@c
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsStatus;-><init>()V

    #@f
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mGpsStatus:Lcom/vzw/location/VzwGpsStatus;

    #@11
    .line 36
    new-instance v0, Lcom/vzw/location/VzwGpsDeviceStatus;

    #@13
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsDeviceStatus;-><init>()V

    #@16
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mGpsDeviceStatus:Lcom/vzw/location/VzwGpsDeviceStatus;

    #@18
    .line 37
    const/4 v0, 0x0

    #@19
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mPrevCriteria:Lcom/vzw/location/VzwCriteria;

    #@1b
    .line 40
    new-instance v0, Ljava/util/HashMap;

    #@1d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@20
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mLocationListeners:Ljava/util/HashMap;

    #@22
    .line 211
    if-nez p1, :cond_2d

    #@24
    .line 212
    new-instance v0, Ljava/security/InvalidParameterException;

    #@26
    const-string/jumbo v1, "service = null"

    #@29
    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 214
    :cond_2d
    iput-object p1, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@2f
    .line 215
    return-void
.end method

.method private _requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .registers 11
    .parameter "provider"
    .parameter "isSingleFix"
    .parameter "criteria"
    .parameter "listener"
    .parameter "looper"

    #@0
    .prologue
    .line 605
    :try_start_0
    iget-object v3, p0, Lcom/vzw/location/VzwLocationManager;->mLocationListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v3
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_3} :catch_2e

    #@3
    .line 606
    :try_start_3
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mLocationListeners:Ljava/util/HashMap;

    #@5
    invoke-virtual {v2, p4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Lcom/vzw/location/VzwLocationManager$ListenerTransport;

    #@b
    .line 607
    .local v1, transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    if-nez v1, :cond_18

    #@d
    .line 609
    if-nez p5, :cond_38

    #@f
    .line 610
    new-instance v1, Lcom/vzw/location/VzwLocationManager$ListenerTransport;

    #@11
    .end local v1           #transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@14
    move-result-object v2

    #@15
    invoke-direct {v1, p0, p4, v2}, Lcom/vzw/location/VzwLocationManager$ListenerTransport;-><init>(Lcom/vzw/location/VzwLocationManager;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@18
    .line 614
    .restart local v1       #transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    :cond_18
    :goto_18
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mLocationListeners:Ljava/util/HashMap;

    #@1a
    invoke-virtual {v2, p4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    .line 616
    if-nez p3, :cond_4e

    #@1f
    .line 617
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mPrevCriteria:Lcom/vzw/location/VzwCriteria;

    #@21
    if-nez v2, :cond_3e

    #@23
    .line 618
    new-instance v2, Ljava/lang/IllegalStateException;

    #@25
    const-string v4, "No previous criteria found"

    #@27
    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v2

    #@2b
    .line 605
    .end local v1           #transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    :catchall_2b
    move-exception v2

    #@2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_2b

    #@2d
    :try_start_2d
    throw v2
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_2e} :catch_2e

    #@2e
    .line 631
    :catch_2e
    move-exception v0

    #@2f
    .line 632
    .local v0, ex:Landroid/os/RemoteException;
    const-string v2, "VzwLocationManager"

    #@31
    const-string/jumbo v3, "requestLocationUpdates: DeadObjectException"

    #@34
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@37
    .line 634
    .end local v0           #ex:Landroid/os/RemoteException;
    :goto_37
    return-void

    #@38
    .line 612
    .restart local v1       #transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    :cond_38
    :try_start_38
    new-instance v1, Lcom/vzw/location/VzwLocationManager$ListenerTransport;

    #@3a
    .end local v1           #transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    invoke-direct {v1, p0, p4, p5}, Lcom/vzw/location/VzwLocationManager$ListenerTransport;-><init>(Lcom/vzw/location/VzwLocationManager;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@3d
    .restart local v1       #transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    goto :goto_18

    #@3e
    .line 620
    :cond_3e
    new-instance v2, Lcom/vzw/location/VzwCriteria;

    #@40
    invoke-direct {v2}, Lcom/vzw/location/VzwCriteria;-><init>()V

    #@43
    iput-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mPrevCriteria:Lcom/vzw/location/VzwCriteria;

    #@45
    .line 629
    :goto_45
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@47
    iget-object v4, p0, Lcom/vzw/location/VzwLocationManager;->mPrevCriteria:Lcom/vzw/location/VzwCriteria;

    #@49
    invoke-interface {v2, p1, p2, v4, v1}, Lcom/vzw/location/IVzwLocationManager;->requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Lcom/vzw/location/ILocationListener;)V

    #@4c
    .line 605
    monitor-exit v3

    #@4d
    goto :goto_37

    #@4e
    .line 621
    :cond_4e
    instance-of v2, p3, Lcom/vzw/location/VzwCriteria;

    #@50
    if-eqz v2, :cond_55

    #@52
    .line 622
    iput-object p3, p0, Lcom/vzw/location/VzwLocationManager;->mPrevCriteria:Lcom/vzw/location/VzwCriteria;

    #@54
    goto :goto_45

    #@55
    .line 624
    :cond_55
    new-instance v2, Lcom/vzw/location/VzwCriteria;

    #@57
    invoke-direct {v2, p3}, Lcom/vzw/location/VzwCriteria;-><init>(Landroid/location/Criteria;)V

    #@5a
    iput-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mPrevCriteria:Lcom/vzw/location/VzwCriteria;
    :try_end_5c
    .catchall {:try_start_38 .. :try_end_5c} :catchall_2b

    #@5c
    goto :goto_45
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwLocationManager;)Lcom/vzw/location/VzwGpsStatus;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 35
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mGpsStatus:Lcom/vzw/location/VzwGpsStatus;

    #@2
    return-object v0
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwLocationManager;)Lcom/vzw/location/VzwGpsDeviceStatus;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mGpsDeviceStatus:Lcom/vzw/location/VzwGpsDeviceStatus;

    #@2
    return-object v0
.end method

.method private createProvider(Ljava/lang/String;Landroid/os/Bundle;)Landroid/location/LocationProvider;
    .registers 7
    .parameter "name"
    .parameter "info"

    #@0
    .prologue
    .line 254
    const-string v1, "VzwLocationManager"

    #@2
    const-string v2, "+ createProvider starting"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 255
    new-instance v0, Lcom/vzw/location/VzwGPSLocationProvider;

    #@9
    invoke-direct {v0, p1}, Lcom/vzw/location/VzwGPSLocationProvider;-><init>(Ljava/lang/String;)V

    #@c
    .line 256
    .local v0, provider:Lcom/vzw/location/VzwGPSLocationProvider;
    const-string/jumbo v1, "network"

    #@f
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setRequiresNetwork(Z)V

    #@16
    .line 257
    const-string/jumbo v1, "satellite"

    #@19
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@1c
    move-result v1

    #@1d
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setRequiresSatellite(Z)V

    #@20
    .line 258
    const-string v1, "cell"

    #@22
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@25
    move-result v1

    #@26
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setRequiresCell(Z)V

    #@29
    .line 259
    const-string v1, "cost"

    #@2b
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@2e
    move-result v1

    #@2f
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setHasMonetaryCost(Z)V

    #@32
    .line 260
    const-string v1, "altitude"

    #@34
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@37
    move-result v1

    #@38
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setSupportsAltitude(Z)V

    #@3b
    .line 261
    const-string/jumbo v1, "speed"

    #@3e
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@41
    move-result v1

    #@42
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setSupportsSpeed(Z)V

    #@45
    .line 262
    const-string v1, "bearing"

    #@47
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@4a
    move-result v1

    #@4b
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setSupportsBearing(Z)V

    #@4e
    .line 263
    const-string/jumbo v1, "power"

    #@51
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@54
    move-result v1

    #@55
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setPowerRequirement(I)V

    #@58
    .line 264
    const-string v1, "accuracy"

    #@5a
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@5d
    move-result v1

    #@5e
    invoke-virtual {v0, v1}, Lcom/vzw/location/VzwGPSLocationProvider;->setAccuracy(I)V

    #@61
    .line 265
    const-string v1, "VzwLocationManager"

    #@63
    new-instance v2, Ljava/lang/StringBuilder;

    #@65
    const-string v3, "-VzwLocationProvider complete: provider = "

    #@67
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@6a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v2

    #@72
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 266
    return-object v0
.end method


# virtual methods
.method public addVzwGpsStatusListener(Lcom/vzw/location/VzwGpsStatus$Listener;)Z
    .registers 7
    .parameter "listener"

    #@0
    .prologue
    .line 702
    iget-object v3, p0, Lcom/vzw/location/VzwLocationManager;->mGpsStatusListeners:Ljava/util/HashMap;

    #@2
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    if-eqz v3, :cond_a

    #@8
    .line 706
    const/4 v1, 0x1

    #@9
    .line 722
    :cond_9
    :goto_9
    return v1

    #@a
    .line 709
    :cond_a
    :try_start_a
    new-instance v2, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;

    #@c
    invoke-direct {v2, p0, p1}, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;-><init>(Lcom/vzw/location/VzwLocationManager;Lcom/vzw/location/VzwGpsStatus$Listener;)V

    #@f
    .line 710
    .local v2, transport:Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;
    iget-object v3, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@11
    invoke-interface {v3, v2}, Lcom/vzw/location/IVzwLocationManager;->addGpsStatusListener(Lcom/vzw/location/IVzwGpsStatusListener;)Z

    #@14
    move-result v1

    #@15
    .line 711
    .local v1, result:Z
    if-eqz v1, :cond_9

    #@17
    .line 712
    iget-object v3, p0, Lcom/vzw/location/VzwLocationManager;->mGpsStatusListeners:Ljava/util/HashMap;

    #@19
    invoke-virtual {v3, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_1c} :catch_1d

    #@1c
    goto :goto_9

    #@1d
    .line 714
    .end local v1           #result:Z
    .end local v2           #transport:Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;
    :catch_1d
    move-exception v0

    #@1e
    .line 715
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "VzwLocationManager"

    #@20
    const-string v4, "RemoteException in registerGpsStatusListener: "

    #@22
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    .line 718
    const/4 v1, 0x0

    #@26
    .restart local v1       #result:Z
    goto :goto_9
.end method

.method public getAllProviders()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 278
    :try_start_0
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@2
    invoke-interface {v1}, Lcom/vzw/location/IVzwLocationManager;->getAllProviders()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 282
    :goto_6
    return-object v1

    #@7
    .line 279
    :catch_7
    move-exception v0

    #@8
    .line 280
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "VzwLocationManager"

    #@a
    const-string v2, "getAllProviders: RemoteException"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 282
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getBestProvider(Z)Ljava/lang/String;
    .registers 4
    .parameter "enabledOnly"

    #@0
    .prologue
    .line 336
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwLocationManager;->getProviders(Z)Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    .line 337
    .local v0, providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v1

    #@8
    if-lez v1, :cond_12

    #@a
    .line 338
    const/4 v1, 0x0

    #@b
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/lang/String;

    #@11
    .line 340
    :goto_11
    return-object v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method

.method public getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;
    .registers 7
    .parameter "name"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 314
    if-nez p1, :cond_c

    #@3
    .line 315
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v3, "name==null"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 318
    :cond_c
    :try_start_c
    iget-object v3, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@e
    invoke-interface {v3, p1}, Lcom/vzw/location/IVzwLocationManager;->getProviderInfo(Ljava/lang/String;)Landroid/os/Bundle;

    #@11
    move-result-object v1

    #@12
    .line 319
    .local v1, info:Landroid/os/Bundle;
    if-nez v1, :cond_15

    #@14
    .line 326
    .end local v1           #info:Landroid/os/Bundle;
    :goto_14
    return-object v2

    #@15
    .line 322
    .restart local v1       #info:Landroid/os/Bundle;
    :cond_15
    invoke-direct {p0, p1, v1}, Lcom/vzw/location/VzwLocationManager;->createProvider(Ljava/lang/String;Landroid/os/Bundle;)Landroid/location/LocationProvider;
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_18} :catch_1a

    #@18
    move-result-object v2

    #@19
    goto :goto_14

    #@1a
    .line 323
    .end local v1           #info:Landroid/os/Bundle;
    :catch_1a
    move-exception v0

    #@1b
    .line 324
    .local v0, ex:Landroid/os/RemoteException;
    const-string v3, "VzwLocationManager"

    #@1d
    const-string v4, "getProvider: RemoteException"

    #@1f
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    goto :goto_14
.end method

.method public getProviders(Z)Ljava/util/List;
    .registers 5
    .parameter "enabledOnly"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 295
    :try_start_0
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@2
    invoke-interface {v1, p1}, Lcom/vzw/location/IVzwLocationManager;->getProviders(Z)Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 299
    :goto_6
    return-object v1

    #@7
    .line 296
    :catch_7
    move-exception v0

    #@8
    .line 297
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "VzwLocationManager"

    #@a
    const-string v2, "getProviders: RemoteException"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 299
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getVzwGpsDeviceStatus(Lcom/vzw/location/VzwGpsDeviceStatus;)Lcom/vzw/location/VzwGpsDeviceStatus;
    .registers 3
    .parameter "status"

    #@0
    .prologue
    .line 778
    if-nez p1, :cond_7

    #@2
    .line 779
    new-instance p1, Lcom/vzw/location/VzwGpsDeviceStatus;

    #@4
    .end local p1
    invoke-direct {p1}, Lcom/vzw/location/VzwGpsDeviceStatus;-><init>()V

    #@7
    .line 781
    .restart local p1
    :cond_7
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mGpsDeviceStatus:Lcom/vzw/location/VzwGpsDeviceStatus;

    #@9
    invoke-virtual {p1, v0}, Lcom/vzw/location/VzwGpsDeviceStatus;->set(Lcom/vzw/location/VzwGpsDeviceStatus;)V

    #@c
    .line 782
    return-object p1
.end method

.method public getVzwGpsStatus(Lcom/vzw/location/VzwGpsStatus;)Lcom/vzw/location/VzwGpsStatus;
    .registers 3
    .parameter "status"

    #@0
    .prologue
    .line 759
    if-nez p1, :cond_7

    #@2
    .line 760
    new-instance p1, Lcom/vzw/location/VzwGpsStatus;

    #@4
    .end local p1
    invoke-direct {p1}, Lcom/vzw/location/VzwGpsStatus;-><init>()V

    #@7
    .line 762
    .restart local p1
    :cond_7
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager;->mGpsStatus:Lcom/vzw/location/VzwGpsStatus;

    #@9
    invoke-virtual {p1, v0}, Lcom/vzw/location/VzwGpsStatus;->setStatus(Lcom/vzw/location/VzwGpsStatus;)V

    #@c
    .line 763
    return-object p1
.end method

.method public isProviderEnabled(Ljava/lang/String;)Z
    .registers 5
    .parameter "provider"

    #@0
    .prologue
    .line 676
    if-nez p1, :cond_b

    #@2
    .line 677
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "provider==null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 681
    :cond_b
    :try_start_b
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@d
    invoke-interface {v1, p1}, Lcom/vzw/location/IVzwLocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 684
    :goto_11
    return v1

    #@12
    .line 682
    :catch_12
    move-exception v0

    #@13
    .line 683
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "VzwLocationManager"

    #@15
    const-string v2, "isProviderEnabled: RemoteException"

    #@17
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 684
    const/4 v1, 0x0

    #@1b
    goto :goto_11
.end method

.method public removeUpdates(Landroid/location/LocationListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 648
    if-nez p1, :cond_b

    #@2
    .line 649
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "listener==null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 653
    :cond_b
    :try_start_b
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mLocationListeners:Ljava/util/HashMap;

    #@d
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/vzw/location/VzwLocationManager$ListenerTransport;

    #@13
    .line 654
    .local v1, transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    if-eqz v1, :cond_1a

    #@15
    .line 655
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@17
    invoke-interface {v2, v1}, Lcom/vzw/location/IVzwLocationManager;->removeUpdates(Lcom/vzw/location/ILocationListener;)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1a} :catch_1b

    #@1a
    .line 659
    .end local v1           #transport:Lcom/vzw/location/VzwLocationManager$ListenerTransport;
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 656
    :catch_1b
    move-exception v0

    #@1c
    .line 657
    .local v0, ex:Landroid/os/RemoteException;
    const-string v2, "VzwLocationManager"

    #@1e
    const-string/jumbo v3, "removeUpdates: DeadObjectException"

    #@21
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@24
    goto :goto_1a
.end method

.method public removeVzwGpsStatusListener(Lcom/vzw/location/VzwGpsStatus$Listener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 736
    :try_start_0
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mGpsStatusListeners:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;

    #@8
    .line 737
    .local v1, transport:Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;
    if-eqz v1, :cond_f

    #@a
    .line 738
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@c
    invoke-interface {v2, v1}, Lcom/vzw/location/IVzwLocationManager;->removeGpsStatusListener(Lcom/vzw/location/IVzwGpsStatusListener;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f} :catch_10

    #@f
    .line 745
    .end local v1           #transport:Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;
    :cond_f
    :goto_f
    return-void

    #@10
    .line 740
    :catch_10
    move-exception v0

    #@11
    .line 741
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "VzwLocationManager"

    #@13
    const-string v3, "RemoteException in unregisterGpsStatusListener: "

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_f
.end method

.method public requestLocationUpdates(Ljava/lang/String;Landroid/location/LocationListener;)V
    .registers 9
    .parameter "provider"
    .parameter "listener"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 391
    if-nez p1, :cond_c

    #@3
    .line 392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v1, "provider==null"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 394
    :cond_c
    if-nez p2, :cond_17

    #@e
    .line 395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string/jumbo v1, "listener==null"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 397
    :cond_17
    const/4 v2, 0x0

    #@18
    move-object v0, p0

    #@19
    move-object v1, p1

    #@1a
    move-object v4, p2

    #@1b
    move-object v5, v3

    #@1c
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@1f
    .line 398
    return-void
.end method

.method public requestLocationUpdates(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .registers 10
    .parameter "provider"
    .parameter "listener"
    .parameter "looper"

    #@0
    .prologue
    .line 479
    if-nez p1, :cond_b

    #@2
    .line 480
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "provider==null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 482
    :cond_b
    if-nez p2, :cond_16

    #@d
    .line 483
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string/jumbo v1, "listener==null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 485
    :cond_16
    const/4 v2, 0x0

    #@17
    const/4 v3, 0x0

    #@18
    move-object v0, p0

    #@19
    move-object v1, p1

    #@1a
    move-object v4, p2

    #@1b
    move-object v5, p3

    #@1c
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@1f
    .line 486
    return-void
.end method

.method public requestLocationUpdates(Ljava/lang/String;Lcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;)V
    .registers 10
    .parameter "provider"
    .parameter "criteria"
    .parameter "listener"

    #@0
    .prologue
    .line 419
    if-nez p1, :cond_b

    #@2
    .line 420
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "provider==null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 422
    :cond_b
    if-nez p2, :cond_16

    #@d
    .line 423
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string/jumbo v1, "listcriteriaener==null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 425
    :cond_16
    if-nez p3, :cond_21

    #@18
    .line 426
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string/jumbo v1, "listener==null"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 428
    :cond_21
    const/4 v2, 0x0

    #@22
    const/4 v5, 0x0

    #@23
    move-object v0, p0

    #@24
    move-object v1, p1

    #@25
    move-object v3, p2

    #@26
    move-object v4, p3

    #@27
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@2a
    .line 429
    return-void
.end method

.method public requestLocationUpdates(Ljava/lang/String;Lcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .registers 11
    .parameter "provider"
    .parameter "criteria"
    .parameter "listener"
    .parameter "looper"

    #@0
    .prologue
    .line 450
    if-nez p1, :cond_b

    #@2
    .line 451
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "provider==null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 453
    :cond_b
    if-nez p2, :cond_16

    #@d
    .line 454
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string/jumbo v1, "listcriteriaener==null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 456
    :cond_16
    if-nez p3, :cond_21

    #@18
    .line 457
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string/jumbo v1, "listener==null"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 459
    :cond_21
    const/4 v2, 0x0

    #@22
    move-object v0, p0

    #@23
    move-object v1, p1

    #@24
    move-object v3, p2

    #@25
    move-object v4, p3

    #@26
    move-object v5, p4

    #@27
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@2a
    .line 460
    return-void
.end method

.method public requestSingleLocationUpdate(Ljava/lang/String;Landroid/location/LocationListener;)V
    .registers 9
    .parameter "provider"
    .parameter "listener"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 505
    if-nez p1, :cond_c

    #@3
    .line 506
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v1, "provider==null"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 508
    :cond_c
    if-nez p2, :cond_17

    #@e
    .line 509
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string/jumbo v1, "listener==null"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 511
    :cond_17
    const/4 v2, 0x1

    #@18
    move-object v0, p0

    #@19
    move-object v1, p1

    #@1a
    move-object v4, p2

    #@1b
    move-object v5, v3

    #@1c
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@1f
    .line 512
    return-void
.end method

.method public requestSingleLocationUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .registers 10
    .parameter "provider"
    .parameter "listener"
    .parameter "looper"

    #@0
    .prologue
    .line 563
    if-nez p1, :cond_b

    #@2
    .line 564
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "provider==null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 566
    :cond_b
    if-nez p2, :cond_16

    #@d
    .line 567
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string/jumbo v1, "listener==null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 569
    :cond_16
    const/4 v2, 0x1

    #@17
    const/4 v3, 0x0

    #@18
    move-object v0, p0

    #@19
    move-object v1, p1

    #@1a
    move-object v4, p2

    #@1b
    move-object v5, p3

    #@1c
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@1f
    .line 570
    return-void
.end method

.method public requestSingleLocationUpdate(Ljava/lang/String;Lcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;)V
    .registers 10
    .parameter "provider"
    .parameter "criteria"
    .parameter "listener"

    #@0
    .prologue
    .line 533
    if-nez p1, :cond_b

    #@2
    .line 534
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "provider==null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 536
    :cond_b
    if-nez p2, :cond_15

    #@d
    .line 537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v1, "criteria==null"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 539
    :cond_15
    if-nez p3, :cond_20

    #@17
    .line 540
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@19
    const-string/jumbo v1, "listener==null"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 542
    :cond_20
    const/4 v2, 0x1

    #@21
    const/4 v5, 0x0

    #@22
    move-object v0, p0

    #@23
    move-object v1, p1

    #@24
    move-object v3, p2

    #@25
    move-object v4, p3

    #@26
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@29
    .line 543
    return-void
.end method

.method public requestSingleLocationUpdate(Ljava/lang/String;Lcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .registers 11
    .parameter "provider"
    .parameter "criteria"
    .parameter "listener"
    .parameter "looper"

    #@0
    .prologue
    .line 591
    if-nez p1, :cond_b

    #@2
    .line 592
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "provider==null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 594
    :cond_b
    if-nez p2, :cond_15

    #@d
    .line 595
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v1, "criteria==null"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 597
    :cond_15
    if-nez p3, :cond_20

    #@17
    .line 598
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@19
    const-string/jumbo v1, "listener==null"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 600
    :cond_20
    const/4 v2, 0x1

    #@21
    move-object v0, p0

    #@22
    move-object v1, p1

    #@23
    move-object v3, p2

    #@24
    move-object v4, p3

    #@25
    move-object v5, p4

    #@26
    invoke-direct/range {v0 .. v5}, Lcom/vzw/location/VzwLocationManager;->_requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@29
    .line 601
    return-void
.end method

.method public setVzwGpsConfigInit(Ljava/lang/String;Lcom/vzw/location/VzwGpsConfigInit;)Z
    .registers 6
    .parameter "provider"
    .parameter "params"

    #@0
    .prologue
    .line 352
    if-nez p1, :cond_b

    #@2
    .line 353
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "provider==null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 355
    :cond_b
    if-nez p2, :cond_16

    #@d
    .line 356
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@f
    const-string/jumbo v2, "params==null"

    #@12
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v1

    #@16
    .line 362
    :cond_16
    :try_start_16
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager;->mService:Lcom/vzw/location/IVzwLocationManager;

    #@18
    invoke-interface {v1, p1, p2}, Lcom/vzw/location/IVzwLocationManager;->setConfigInit(Ljava/lang/String;Lcom/vzw/location/VzwGpsConfigInit;)Z
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1b} :catch_1d

    #@1b
    .line 365
    const/4 v1, 0x1

    #@1c
    .line 371
    :goto_1c
    return v1

    #@1d
    .line 366
    :catch_1d
    move-exception v0

    #@1e
    .line 367
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "VzwLocationManager"

    #@20
    const-string/jumbo v2, "removeUpdates: RemoteException"

    #@23
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 371
    const/4 v1, 0x0

    #@27
    goto :goto_1c
.end method
