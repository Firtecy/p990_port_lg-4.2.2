.class public Lcom/vzw/location/VzwGpsConfigInit;
.super Ljava/lang/Object;
.source "VzwGpsConfigInit.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vzw/location/VzwGpsConfigInit;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "VzwGpsConfigInit"


# instance fields
.field private isWifi:Z

.field private mApplicationId:J

.field private mApplicationPassword:Ljava/lang/String;

.field private mFullyQualifiedAppName:Ljava/lang/String;

.field private mMpcHost:Ljava/net/InetAddress;

.field public mMpcHostName:Ljava/lang/String;

.field private mMpcPort:I

.field private mSmsPrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 266
    new-instance v0, Lcom/vzw/location/VzwGpsConfigInit$1;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsConfigInit$1;-><init>()V

    #@5
    sput-object v0, Lcom/vzw/location/VzwGpsConfigInit;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 16
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 20
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationId:J

    #@9
    .line 21
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationPassword:Ljava/lang/String;

    #@b
    .line 22
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@d
    .line 23
    iput v3, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@f
    .line 24
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mSmsPrefix:Ljava/lang/String;

    #@11
    .line 25
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mFullyQualifiedAppName:Ljava/lang/String;

    #@13
    .line 27
    iput-boolean v3, p0, Lcom/vzw/location/VzwGpsConfigInit;->isWifi:Z

    #@15
    .line 31
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->isWifi:Z

    #@18
    .line 32
    return-void
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwGpsConfigInit;J)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 20
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationId:J

    #@2
    return-void
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwGpsConfigInit;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 21
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationPassword:Ljava/lang/String;

    #@2
    return-void
.end method

.method static synthetic access$2(Lcom/vzw/location/VzwGpsConfigInit;Ljava/net/InetAddress;)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 22
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@2
    return-void
.end method

.method static synthetic access$3(Lcom/vzw/location/VzwGpsConfigInit;I)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 23
    iput p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@2
    return-void
.end method

.method static synthetic access$4(Lcom/vzw/location/VzwGpsConfigInit;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 24
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mSmsPrefix:Ljava/lang/String;

    #@2
    return-void
.end method

.method static synthetic access$5(Lcom/vzw/location/VzwGpsConfigInit;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 25
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mFullyQualifiedAppName:Ljava/lang/String;

    #@2
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 247
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 5
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    const-string/jumbo v1, "mApplicationId=***"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@17
    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    #@19
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@20
    const-string/jumbo v1, "mApplicationPassword=***"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2e
    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    #@30
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@37
    const-string/jumbo v1, "mMpcHost="

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@4b
    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    #@4d
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@54
    const-string/jumbo v1, "mMpcPort="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@68
    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@6d
    move-result-object v1

    #@6e
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@71
    const-string/jumbo v1, "mSmsPrefix="

    #@74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v0

    #@78
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mSmsPrefix:Ljava/lang/String;

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v0

    #@7e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v0

    #@82
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@85
    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    #@87
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8e
    const-string/jumbo v1, "mFullyQualifiedAppName="

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v0

    #@95
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mFullyQualifiedAppName:Ljava/lang/String;

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v0

    #@9b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v0

    #@9f
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@a2
    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    #@a4
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@a7
    move-result-object v1

    #@a8
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@ab
    const-string/jumbo v1, "mMpcHostName="

    #@ae
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v0

    #@b2
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v0

    #@bc
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@bf
    .line 239
    return-void
.end method

.method public getApplicationId()J
    .registers 3

    #@0
    .prologue
    .line 50
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationId:J

    #@2
    return-wide v0
.end method

.method public getApplicationPassword()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationPassword:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getFullyQualifiedAppName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 228
    iget-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mFullyQualifiedAppName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMpcHost()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 183
    iget-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@2
    return-object v0
.end method

.method public getMpcPort()I
    .registers 2

    #@0
    .prologue
    .line 191
    iget v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@2
    return v0
.end method

.method public getSmsPrefixInfo()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mSmsPrefix:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public resetMpc()V
    .registers 2

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@3
    .line 81
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@6
    .line 82
    return-void
.end method

.method public setCredentials(JLjava/lang/String;)V
    .registers 5
    .parameter "applicationId"
    .parameter "applicationPassword"

    #@0
    .prologue
    .line 69
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationId:J

    #@2
    .line 70
    if-nez p3, :cond_c

    #@4
    .line 71
    new-instance v0, Ljava/lang/String;

    #@6
    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    #@9
    iput-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationPassword:Ljava/lang/String;

    #@b
    .line 74
    :goto_b
    return-void

    #@c
    .line 73
    :cond_c
    iput-object p3, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationPassword:Ljava/lang/String;

    #@e
    goto :goto_b
.end method

.method public setMpc(Ljava/lang/String;I)V
    .registers 6
    .parameter "mpcHost"
    .parameter "mpcPort"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 110
    if-nez p1, :cond_8

    #@3
    .line 111
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@5
    .line 127
    :goto_5
    iput p2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@7
    .line 128
    return-void

    #@8
    .line 114
    :cond_8
    :try_start_8
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@a
    .line 115
    iget-boolean v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->isWifi:Z

    #@c
    if-nez v1, :cond_19

    #@e
    .line 116
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@11
    move-result-object v1

    #@12
    iput-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;
    :try_end_14
    .catch Ljava/net/UnknownHostException; {:try_start_8 .. :try_end_14} :catch_15

    #@14
    goto :goto_5

    #@15
    .line 122
    :catch_15
    move-exception v0

    #@16
    .line 124
    .local v0, uhe:Ljava/net/UnknownHostException;
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@18
    goto :goto_5

    #@19
    .line 119
    .end local v0           #uhe:Ljava/net/UnknownHostException;
    :cond_19
    const/4 v1, 0x0

    #@1a
    :try_start_1a
    iput-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;
    :try_end_1c
    .catch Ljava/net/UnknownHostException; {:try_start_1a .. :try_end_1c} :catch_15

    #@1c
    goto :goto_5
.end method

.method public setMpc(Ljava/net/InetAddress;I)V
    .registers 4
    .parameter "mpcHost"
    .parameter "mpcPort"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@2
    .line 93
    iput p2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@4
    .line 94
    if-nez p1, :cond_a

    #@6
    .line 95
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@9
    .line 100
    :goto_9
    return-void

    #@a
    .line 98
    :cond_a
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@10
    goto :goto_9
.end method

.method public setMpcHost(Ljava/lang/String;)V
    .registers 5
    .parameter "mpcHost"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 151
    if-nez p1, :cond_6

    #@3
    .line 152
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@5
    .line 168
    :goto_5
    return-void

    #@6
    .line 155
    :cond_6
    :try_start_6
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@8
    .line 156
    iget-boolean v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->isWifi:Z

    #@a
    if-nez v1, :cond_17

    #@c
    .line 157
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@f
    move-result-object v1

    #@10
    iput-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;
    :try_end_12
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_12} :catch_13

    #@12
    goto :goto_5

    #@13
    .line 163
    :catch_13
    move-exception v0

    #@14
    .line 165
    .local v0, uhe:Ljava/net/UnknownHostException;
    iput-object v2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@16
    goto :goto_5

    #@17
    .line 160
    .end local v0           #uhe:Ljava/net/UnknownHostException;
    :cond_17
    const/4 v1, 0x0

    #@18
    :try_start_18
    iput-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;
    :try_end_1a
    .catch Ljava/net/UnknownHostException; {:try_start_18 .. :try_end_1a} :catch_13

    #@1a
    goto :goto_5
.end method

.method public setMpcHost(Ljava/net/InetAddress;)V
    .registers 3
    .parameter "mpcHost"

    #@0
    .prologue
    .line 136
    if-nez p1, :cond_6

    #@2
    .line 137
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@5
    .line 142
    :goto_5
    return-void

    #@6
    .line 139
    :cond_6
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@c
    .line 140
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@e
    goto :goto_5
.end method

.method public setMpcPort(I)V
    .registers 2
    .parameter "mpcPort"

    #@0
    .prologue
    .line 175
    iput p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@2
    .line 176
    return-void
.end method

.method public setSmsPrefixInfo(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "smsPrefix"
    .parameter "fullyQualifiedAppName"

    #@0
    .prologue
    .line 207
    iput-object p1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mSmsPrefix:Ljava/lang/String;

    #@2
    .line 208
    iput-object p2, p0, Lcom/vzw/location/VzwGpsConfigInit;->mFullyQualifiedAppName:Ljava/lang/String;

    #@4
    .line 209
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string/jumbo v1, "mApplicationId=***, mApplicationPassword=***, mMpcHostName="

    #@5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v1, ", mMpcPort="

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    iget v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v1, ", mSmsPrefix="

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mSmsPrefix:Ljava/lang/String;

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    const-string v1, ", mFullyQualifiedAppName="

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v0

    #@2c
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mFullyQualifiedAppName:Ljava/lang/String;

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 251
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationId:J

    #@2
    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 252
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mApplicationPassword:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 253
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@c
    if-nez v1, :cond_27

    #@e
    .line 254
    const/4 v1, -0x1

    #@f
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 260
    :goto_12
    iget v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcPort:I

    #@14
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 261
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mSmsPrefix:Ljava/lang/String;

    #@19
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 262
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mFullyQualifiedAppName:Ljava/lang/String;

    #@1e
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@21
    .line 263
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHostName:Ljava/lang/String;

    #@23
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@26
    .line 264
    return-void

    #@27
    .line 256
    :cond_27
    iget-object v1, p0, Lcom/vzw/location/VzwGpsConfigInit;->mMpcHost:Ljava/net/InetAddress;

    #@29
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    #@2c
    move-result-object v0

    #@2d
    .line 257
    .local v0, arrray:[B
    array-length v1, v0

    #@2e
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 258
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@34
    goto :goto_12
.end method
