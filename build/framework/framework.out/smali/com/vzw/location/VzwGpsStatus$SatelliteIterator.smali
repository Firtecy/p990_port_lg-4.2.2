.class final Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;
.super Ljava/lang/Object;
.source "VzwGpsStatus.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwGpsStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SatelliteIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/vzw/location/VzwGpsSatellite;",
        ">;"
    }
.end annotation


# instance fields
.field mIndex:I

.field private mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

.field final synthetic this$0:Lcom/vzw/location/VzwGpsStatus;


# direct methods
.method constructor <init>(Lcom/vzw/location/VzwGpsStatus;[Lcom/vzw/location/VzwGpsSatellite;)V
    .registers 4
    .parameter
    .parameter "satellites"

    #@0
    .prologue
    .line 25
    iput-object p1, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->this$0:Lcom/vzw/location/VzwGpsStatus;

    #@2
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 21
    const/4 v0, 0x0

    #@6
    iput v0, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mIndex:I

    #@8
    .line 24
    iput-object p2, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@a
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .registers 3

    #@0
    .prologue
    .line 28
    iget v0, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mIndex:I

    #@2
    .local v0, i:I
    :goto_2
    iget-object v1, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@4
    array-length v1, v1

    #@5
    if-lt v0, v1, :cond_9

    #@7
    .line 33
    const/4 v1, 0x0

    #@8
    :goto_8
    return v1

    #@9
    .line 29
    :cond_9
    iget-object v1, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@b
    aget-object v1, v1, v0

    #@d
    iget-boolean v1, v1, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@f
    if-eqz v1, :cond_13

    #@11
    .line 30
    const/4 v1, 0x1

    #@12
    goto :goto_8

    #@13
    .line 28
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_2
.end method

.method public next()Lcom/vzw/location/VzwGpsSatellite;
    .registers 5

    #@0
    .prologue
    .line 37
    :cond_0
    iget v1, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mIndex:I

    #@2
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@4
    array-length v2, v2

    #@5
    if-lt v1, v2, :cond_d

    #@7
    .line 43
    new-instance v1, Ljava/util/NoSuchElementException;

    #@9
    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    #@c
    throw v1

    #@d
    .line 38
    :cond_d
    iget-object v1, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@f
    iget v2, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mIndex:I

    #@11
    add-int/lit8 v3, v2, 0x1

    #@13
    iput v3, p0, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->mIndex:I

    #@15
    aget-object v0, v1, v2

    #@17
    .line 39
    .local v0, satellite:Lcom/vzw/location/VzwGpsSatellite;
    iget-boolean v1, v0, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@19
    if-eqz v1, :cond_0

    #@1b
    .line 40
    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;->next()Lcom/vzw/location/VzwGpsSatellite;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public remove()V
    .registers 2

    #@0
    .prologue
    .line 47
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method
