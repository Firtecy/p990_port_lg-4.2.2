.class Lcom/vzw/location/VzwCriteria$1;
.super Ljava/lang/Object;
.source "VzwCriteria.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwCriteria;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/vzw/location/VzwCriteria;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwCriteria;
    .registers 4
    .parameter "parcel"

    #@0
    .prologue
    .line 117
    new-instance v0, Lcom/vzw/location/VzwCriteria;

    #@2
    sget-object v1, Landroid/location/Criteria;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/location/Criteria;

    #@a
    invoke-direct {v0, v1}, Lcom/vzw/location/VzwCriteria;-><init>(Landroid/location/Criteria;)V

    #@d
    .line 118
    .local v0, vz:Lcom/vzw/location/VzwCriteria;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v1

    #@11
    invoke-static {v0, v1}, Lcom/vzw/location/VzwCriteria;->access$0(Lcom/vzw/location/VzwCriteria;I)V

    #@14
    .line 119
    sget-object v1, Lcom/vzw/location/VzwGpsPerformance;->CREATOR:Landroid/os/Parcelable$Creator;

    #@16
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Lcom/vzw/location/VzwGpsPerformance;

    #@1c
    invoke-static {v0, v1}, Lcom/vzw/location/VzwCriteria;->access$1(Lcom/vzw/location/VzwCriteria;Lcom/vzw/location/VzwGpsPerformance;)V

    #@1f
    .line 120
    sget-object v1, Lcom/vzw/location/VzwGpsFixRate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@21
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Lcom/vzw/location/VzwGpsFixRate;

    #@27
    invoke-static {v0, v1}, Lcom/vzw/location/VzwCriteria;->access$2(Lcom/vzw/location/VzwCriteria;Lcom/vzw/location/VzwGpsFixRate;)V

    #@2a
    .line 121
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwCriteria$1;->createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwCriteria;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/vzw/location/VzwCriteria;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 125
    new-array v0, p1, [Lcom/vzw/location/VzwCriteria;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwCriteria$1;->newArray(I)[Lcom/vzw/location/VzwCriteria;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
