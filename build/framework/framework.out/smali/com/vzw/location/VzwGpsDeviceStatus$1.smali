.class Lcom/vzw/location/VzwGpsDeviceStatus$1;
.super Ljava/lang/Object;
.source "VzwGpsDeviceStatus.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwGpsDeviceStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/vzw/location/VzwGpsDeviceStatus;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 224
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsDeviceStatus;
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    .line 226
    new-instance v0, Lcom/vzw/location/VzwGpsDeviceStatus;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsDeviceStatus;-><init>()V

    #@5
    .line 227
    .local v0, vz:Lcom/vzw/location/VzwGpsDeviceStatus;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v1

    #@9
    invoke-static {v0, v1}, Lcom/vzw/location/VzwGpsDeviceStatus;->access$0(Lcom/vzw/location/VzwGpsDeviceStatus;I)V

    #@c
    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v1

    #@10
    invoke-static {v0, v1}, Lcom/vzw/location/VzwGpsDeviceStatus;->access$1(Lcom/vzw/location/VzwGpsDeviceStatus;I)V

    #@13
    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v1

    #@17
    invoke-static {v0, v1}, Lcom/vzw/location/VzwGpsDeviceStatus;->access$2(Lcom/vzw/location/VzwGpsDeviceStatus;I)V

    #@1a
    .line 230
    invoke-static {v0}, Lcom/vzw/location/VzwGpsDeviceStatus;->access$3(Lcom/vzw/location/VzwGpsDeviceStatus;)[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@1d
    move-result-object v1

    #@1e
    sget-object v2, Lcom/vzw/location/VzwGpsSatelliteStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    #@23
    .line 231
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsDeviceStatus$1;->createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsDeviceStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/vzw/location/VzwGpsDeviceStatus;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 235
    new-array v0, p1, [Lcom/vzw/location/VzwGpsDeviceStatus;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsDeviceStatus$1;->newArray(I)[Lcom/vzw/location/VzwGpsDeviceStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
