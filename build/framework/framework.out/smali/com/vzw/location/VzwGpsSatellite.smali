.class public Lcom/vzw/location/VzwGpsSatellite;
.super Ljava/lang/Object;
.source "VzwGpsSatellite.java"


# instance fields
.field mAzimuth:F

.field mElevation:F

.field mPrn:I

.field mSnr:F

.field mUsedInFix:Z

.field mValid:Z


# direct methods
.method constructor <init>(I)V
    .registers 2
    .parameter "prn"

    #@0
    .prologue
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 21
    iput p1, p0, Lcom/vzw/location/VzwGpsSatellite;->mPrn:I

    #@5
    .line 22
    return-void
.end method


# virtual methods
.method public getAzimuth()F
    .registers 2

    #@0
    .prologue
    .line 52
    iget v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mAzimuth:F

    #@2
    return v0
.end method

.method public getElevation()F
    .registers 2

    #@0
    .prologue
    .line 61
    iget v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mElevation:F

    #@2
    return v0
.end method

.method public getPrn()I
    .registers 2

    #@0
    .prologue
    .line 43
    iget v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mPrn:I

    #@2
    return v0
.end method

.method public getSnr()F
    .registers 2

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mSnr:F

    #@2
    return v0
.end method

.method public isUsedInFix()Z
    .registers 2

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z

    #@2
    return v0
.end method

.method setStatus(Lcom/vzw/location/VzwGpsSatellite;)V
    .registers 3
    .parameter "satellite"

    #@0
    .prologue
    .line 29
    iget-boolean v0, p1, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@2
    iput-boolean v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@4
    .line 32
    iget-boolean v0, p1, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z

    #@6
    iput-boolean v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z

    #@8
    .line 33
    iget v0, p1, Lcom/vzw/location/VzwGpsSatellite;->mSnr:F

    #@a
    iput v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mSnr:F

    #@c
    .line 34
    iget v0, p1, Lcom/vzw/location/VzwGpsSatellite;->mElevation:F

    #@e
    iput v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mElevation:F

    #@10
    .line 35
    iget v0, p1, Lcom/vzw/location/VzwGpsSatellite;->mAzimuth:F

    #@12
    iput v0, p0, Lcom/vzw/location/VzwGpsSatellite;->mAzimuth:F

    #@14
    .line 36
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "PRN: "

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    iget v1, p0, Lcom/vzw/location/VzwGpsSatellite;->mPrn:I

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, ", AZ: "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget v1, p0, Lcom/vzw/location/VzwGpsSatellite;->mAzimuth:F

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ", ELV: "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget v1, p0, Lcom/vzw/location/VzwGpsSatellite;->mElevation:F

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    const-string v1, ", S/N: "

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget v1, p0, Lcom/vzw/location/VzwGpsSatellite;->mSnr:F

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    .line 103
    const-string v1, ", Used In Fix: "

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    iget-boolean v1, p0, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    .line 102
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    return-object v0
.end method
