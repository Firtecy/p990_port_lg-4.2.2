.class public Lcom/vzw/location/VzwGpsStatus;
.super Ljava/lang/Object;
.source "VzwGpsStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vzw/location/VzwGpsStatus$Listener;,
        Lcom/vzw/location/VzwGpsStatus$SatelliteIterator;
    }
.end annotation


# static fields
.field public static final GPS_EVENT_AGPS_AUTH_DNS_FAIL:I = 0x6

.field public static final GPS_EVENT_AGPS_AUTH_EXPIRED:I = 0x5

.field public static final GPS_EVENT_AGPS_AUTH_FAIL:I = 0x3

.field public static final GPS_EVENT_AGPS_AUTH_PASS:I = 0x2

.field public static final GPS_EVENT_AGPS_AUTH_PDE_NOT_REACHABLE:I = 0x4

.field public static final GPS_EVENT_DEVICE_STATUS:I = 0x19

.field public static final GPS_EVENT_ESTABLISH_PDE_CONNECTION_FAILED:I = 0x1

.field public static final GPS_EVENT_FIRST_FIX:I = 0xa

.field public static final GPS_EVENT_FIX_REQUESTED:I = 0xc

.field public static final GPS_EVENT_FIX_REQ_FAIL:I = 0xb

.field public static final GPS_EVENT_GENERAL_AGPS_FAILURE:I = 0xd

.field public static final GPS_EVENT_GENERAL_FAILURE:I = 0xe

.field public static final GPS_EVENT_INIT_CONFIG_NOT_PROVIDED:I = 0xf

.field public static final GPS_EVENT_INIT_FAIL:I = 0x7

.field public static final GPS_EVENT_INIT_IN_PROGRESS:I = 0x65

.field public static final GPS_EVENT_INIT_PASS:I = 0x8

.field public static final GPS_EVENT_LOCATION_AVAILABLE:I = 0x9

.field public static final GPS_EVENT_LOCATION_REQUEST_TIMEDOUT:I = 0x10

.field public static final GPS_EVENT_SATELLITE_STATUS:I = 0x11

.field public static final GPS_EVENT_SECURITY_FAILED:I = 0x12

.field public static final GPS_EVENT_SET_FIX_MODE_FAIL:I = 0x13

.field public static final GPS_EVENT_SET_FIX_RATE_FAIL:I = 0x14

.field public static final GPS_EVENT_SET_GPS_PERFORMANCE_FAIL:I = 0x15

.field public static final GPS_EVENT_SET_PDE_FAIL:I = 0x16

.field public static final GPS_EVENT_SMS_REGISTER_FAILED:I = 0x1a

.field public static final GPS_EVENT_STARTED:I = 0x17

.field public static final GPS_EVENT_STOPPED:I = 0x18

.field public static final GPS_EVENT_TRACKING_SESSION_TIMEDOUT:I = 0x131

.field static final NUM_SATELLITES:I = 0xff


# instance fields
.field private mSatelliteList:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/vzw/location/VzwGpsSatellite;",
            ">;"
        }
    .end annotation
.end field

.field mSatellites:[Lcom/vzw/location/VzwGpsSatellite;


# direct methods
.method constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 17
    const/16 v1, 0xff

    #@5
    new-array v1, v1, [Lcom/vzw/location/VzwGpsSatellite;

    #@7
    iput-object v1, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@9
    .line 51
    new-instance v1, Lcom/vzw/location/VzwGpsStatus$1;

    #@b
    invoke-direct {v1, p0}, Lcom/vzw/location/VzwGpsStatus$1;-><init>(Lcom/vzw/location/VzwGpsStatus;)V

    #@e
    iput-object v1, p0, Lcom/vzw/location/VzwGpsStatus;->mSatelliteList:Ljava/lang/Iterable;

    #@10
    .line 112
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    iget-object v1, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@13
    array-length v1, v1

    #@14
    if-lt v0, v1, :cond_17

    #@16
    .line 115
    return-void

    #@17
    .line 113
    :cond_17
    iget-object v1, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@19
    new-instance v2, Lcom/vzw/location/VzwGpsSatellite;

    #@1b
    add-int/lit8 v3, v0, 0x1

    #@1d
    invoke-direct {v2, v3}, Lcom/vzw/location/VzwGpsSatellite;-><init>(I)V

    #@20
    aput-object v2, v1, v0

    #@22
    .line 112
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_11
.end method


# virtual methods
.method public getMaximumPossibleSatelliteCount()I
    .registers 2

    #@0
    .prologue
    .line 193
    const/16 v0, 0xff

    #@2
    return v0
.end method

.method public getSatellitesInView()Ljava/lang/Iterable;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/vzw/location/VzwGpsSatellite;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Lcom/vzw/location/VzwGpsStatus;->mSatelliteList:Ljava/lang/Iterable;

    #@2
    return-object v0
.end method

.method declared-synchronized setStatus(II[I[I[F[F[F)V
    .registers 12
    .parameter "svCount"
    .parameter "svInViewCount"
    .parameter "prn"
    .parameter "prnInView"
    .parameter "elevInView"
    .parameter "azimInView"
    .parameter "snrInView"

    #@0
    .prologue
    .line 125
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@4
    array-length v2, v2
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_40

    #@5
    if-lt v0, v2, :cond_f

    #@7
    .line 129
    const/4 v0, 0x0

    #@8
    :goto_8
    if-lt v0, p2, :cond_19

    #@a
    .line 138
    const/4 v0, 0x0

    #@b
    :goto_b
    if-lt v0, p1, :cond_34

    #@d
    .line 142
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 126
    :cond_f
    :try_start_f
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@11
    aget-object v2, v2, v0

    #@13
    const/4 v3, 0x0

    #@14
    iput-boolean v3, v2, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@16
    .line 125
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_2

    #@19
    .line 130
    :cond_19
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@1b
    aget v3, p4, v0

    #@1d
    aget-object v1, v2, v3

    #@1f
    .line 132
    .local v1, satellite:Lcom/vzw/location/VzwGpsSatellite;
    const/4 v2, 0x1

    #@20
    iput-boolean v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@22
    .line 133
    aget v2, p7, v0

    #@24
    iput v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mSnr:F

    #@26
    .line 134
    aget v2, p5, v0

    #@28
    iput v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mElevation:F

    #@2a
    .line 135
    aget v2, p6, v0

    #@2c
    iput v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mAzimuth:F

    #@2e
    .line 136
    const/4 v2, 0x0

    #@2f
    iput-boolean v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z

    #@31
    .line 129
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_8

    #@34
    .line 139
    .end local v1           #satellite:Lcom/vzw/location/VzwGpsSatellite;
    :cond_34
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@36
    aget v3, p3, v0

    #@38
    aget-object v1, v2, v3

    #@3a
    .line 140
    .restart local v1       #satellite:Lcom/vzw/location/VzwGpsSatellite;
    const/4 v2, 0x1

    #@3b
    iput-boolean v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z
    :try_end_3d
    .catchall {:try_start_f .. :try_end_3d} :catchall_40

    #@3d
    .line 138
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_b

    #@40
    .line 125
    .end local v1           #satellite:Lcom/vzw/location/VzwGpsSatellite;
    :catchall_40
    move-exception v2

    #@41
    monitor-exit p0

    #@42
    throw v2
.end method

.method declared-synchronized setStatus(II[I[I[I[I[I)V
    .registers 12
    .parameter "svCount"
    .parameter "svInViewCount"
    .parameter "prn"
    .parameter "prnInView"
    .parameter "elevInView"
    .parameter "azimInView"
    .parameter "snrInView"

    #@0
    .prologue
    .line 155
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@4
    array-length v2, v2
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_43

    #@5
    if-lt v0, v2, :cond_f

    #@7
    .line 159
    const/4 v0, 0x0

    #@8
    :goto_8
    if-lt v0, p2, :cond_19

    #@a
    .line 168
    const/4 v0, 0x0

    #@b
    :goto_b
    if-lt v0, p1, :cond_37

    #@d
    .line 172
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 156
    :cond_f
    :try_start_f
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@11
    aget-object v2, v2, v0

    #@13
    const/4 v3, 0x0

    #@14
    iput-boolean v3, v2, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@16
    .line 155
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_2

    #@19
    .line 160
    :cond_19
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@1b
    aget v3, p4, v0

    #@1d
    aget-object v1, v2, v3

    #@1f
    .line 162
    .local v1, satellite:Lcom/vzw/location/VzwGpsSatellite;
    const/4 v2, 0x1

    #@20
    iput-boolean v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mValid:Z

    #@22
    .line 163
    aget v2, p7, v0

    #@24
    int-to-float v2, v2

    #@25
    iput v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mSnr:F

    #@27
    .line 164
    aget v2, p5, v0

    #@29
    int-to-float v2, v2

    #@2a
    iput v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mElevation:F

    #@2c
    .line 165
    aget v2, p6, v0

    #@2e
    int-to-float v2, v2

    #@2f
    iput v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mAzimuth:F

    #@31
    .line 166
    const/4 v2, 0x0

    #@32
    iput-boolean v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z

    #@34
    .line 159
    add-int/lit8 v0, v0, 0x1

    #@36
    goto :goto_8

    #@37
    .line 169
    .end local v1           #satellite:Lcom/vzw/location/VzwGpsSatellite;
    :cond_37
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@39
    aget v3, p3, v0

    #@3b
    aget-object v1, v2, v3

    #@3d
    .line 170
    .restart local v1       #satellite:Lcom/vzw/location/VzwGpsSatellite;
    const/4 v2, 0x1

    #@3e
    iput-boolean v2, v1, Lcom/vzw/location/VzwGpsSatellite;->mUsedInFix:Z
    :try_end_40
    .catchall {:try_start_f .. :try_end_40} :catchall_43

    #@40
    .line 168
    add-int/lit8 v0, v0, 0x1

    #@42
    goto :goto_b

    #@43
    .line 155
    .end local v1           #satellite:Lcom/vzw/location/VzwGpsSatellite;
    :catchall_43
    move-exception v2

    #@44
    monitor-exit p0

    #@45
    throw v2
.end method

.method setStatus(Lcom/vzw/location/VzwGpsStatus;)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    .line 181
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@3
    array-length v1, v1

    #@4
    if-lt v0, v1, :cond_7

    #@6
    .line 184
    return-void

    #@7
    .line 182
    :cond_7
    iget-object v1, p0, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@9
    aget-object v1, v1, v0

    #@b
    iget-object v2, p1, Lcom/vzw/location/VzwGpsStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatellite;

    #@d
    aget-object v2, v2, v0

    #@f
    invoke-virtual {v1, v2}, Lcom/vzw/location/VzwGpsSatellite;->setStatus(Lcom/vzw/location/VzwGpsSatellite;)V

    #@12
    .line 181
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 209
    .local v0, builder:Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/vzw/location/VzwGpsStatus;->mSatelliteList:Ljava/lang/Iterable;

    #@7
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_16

    #@11
    .line 215
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    return-object v2

    #@16
    .line 209
    :cond_16
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Lcom/vzw/location/VzwGpsSatellite;

    #@1c
    .line 210
    .local v1, satellite:Lcom/vzw/location/VzwGpsSatellite;
    const-string v3, "Satellite: "

    #@1e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 211
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    .line 212
    const-string v3, " "

    #@26
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    goto :goto_b
.end method
