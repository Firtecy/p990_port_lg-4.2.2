.class public Lcom/vzw/location/VzwCriteria;
.super Landroid/location/Criteria;
.source "VzwCriteria.java"


# static fields
.field public static final ACCURACY_OPTIMAL:I = 0x6

.field public static final AFLT:I = 0x4

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vzw/location/VzwCriteria;",
            ">;"
        }
    .end annotation
.end field

.field public static final DATA_OPTIMAL:I = 0x7

.field public static final MS_ASSISTED:I = 0x1

.field public static final MS_BASED:I = 0x2

.field public static final SPEED_OPTIMAL:I = 0x5

.field public static final STANDALONE:I = 0x3

.field public static final UNKNOWN:I


# instance fields
.field private mFixMode:I

.field private mFixRate:Lcom/vzw/location/VzwGpsFixRate;

.field private mPerformance:Lcom/vzw/location/VzwGpsPerformance;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 115
    new-instance v0, Lcom/vzw/location/VzwCriteria$1;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwCriteria$1;-><init>()V

    #@5
    sput-object v0, Lcom/vzw/location/VzwCriteria;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 11
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 25
    invoke-direct {p0}, Landroid/location/Criteria;-><init>()V

    #@3
    .line 21
    const/4 v0, 0x3

    #@4
    iput v0, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@6
    .line 26
    new-instance v0, Lcom/vzw/location/VzwGpsPerformance;

    #@8
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsPerformance;-><init>()V

    #@b
    iput-object v0, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@d
    .line 27
    new-instance v0, Lcom/vzw/location/VzwGpsFixRate;

    #@f
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsFixRate;-><init>()V

    #@12
    iput-object v0, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@14
    .line 28
    return-void
.end method

.method constructor <init>(Landroid/location/Criteria;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/location/Criteria;-><init>(Landroid/location/Criteria;)V

    #@3
    .line 21
    const/4 v0, 0x3

    #@4
    iput v0, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@6
    .line 32
    new-instance v0, Lcom/vzw/location/VzwGpsPerformance;

    #@8
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsPerformance;-><init>()V

    #@b
    iput-object v0, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@d
    .line 33
    new-instance v0, Lcom/vzw/location/VzwGpsFixRate;

    #@f
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsFixRate;-><init>()V

    #@12
    iput-object v0, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@14
    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwCriteria;I)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 21
    iput p1, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@2
    return-void
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwCriteria;Lcom/vzw/location/VzwGpsPerformance;)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 22
    iput-object p1, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@2
    return-void
.end method

.method static synthetic access$2(Lcom/vzw/location/VzwCriteria;Lcom/vzw/location/VzwGpsFixRate;)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 23
    iput-object p1, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@2
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 104
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 5
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    const-string/jumbo v1, "mFixMode="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget v1, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1d
    .line 93
    iget-object v0, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@1f
    invoke-virtual {v0, p1, p2}, Lcom/vzw/location/VzwGpsPerformance;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@22
    .line 94
    iget-object v0, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@24
    invoke-virtual {v0, p1, p2}, Lcom/vzw/location/VzwGpsFixRate;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@27
    .line 95
    return-void
.end method

.method public getFixMode()I
    .registers 2

    #@0
    .prologue
    .line 50
    iget v0, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@2
    return v0
.end method

.method public getFixRate()Lcom/vzw/location/VzwGpsFixRate;
    .registers 2

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@2
    return-object v0
.end method

.method public getPerformance()Lcom/vzw/location/VzwGpsPerformance;
    .registers 2

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@2
    return-object v0
.end method

.method public setFixMode(I)V
    .registers 5
    .parameter "fixMode"

    #@0
    .prologue
    .line 69
    const/4 v0, 0x1

    #@1
    if-lt p1, v0, :cond_6

    #@3
    const/4 v0, 0x7

    #@4
    if-le p1, v0, :cond_1b

    #@6
    .line 70
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    const-string v2, "fixMode="

    #@c
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 72
    :cond_1b
    iput p1, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@1d
    .line 73
    return-void
.end method

.method public setFixRate(Lcom/vzw/location/VzwGpsFixRate;)V
    .registers 2
    .parameter "fixRate"

    #@0
    .prologue
    .line 88
    iput-object p1, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@2
    .line 89
    return-void
.end method

.method public setPerformance(Lcom/vzw/location/VzwGpsPerformance;)V
    .registers 2
    .parameter "performance"

    #@0
    .prologue
    .line 80
    iput-object p1, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@2
    .line 81
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string/jumbo v1, "mFixMode="

    #@5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8
    iget v1, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v1, ", mPerformance={"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    iget-object v1, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@16
    invoke-virtual {v1}, Lcom/vzw/location/VzwGpsPerformance;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string/jumbo v1, "}, mFixRate={"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    iget-object v1, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    const-string/jumbo v1, "}"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 109
    invoke-super {p0, p1, p2}, Landroid/location/Criteria;->writeToParcel(Landroid/os/Parcel;I)V

    #@3
    .line 110
    iget v0, p0, Lcom/vzw/location/VzwCriteria;->mFixMode:I

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 111
    iget-object v0, p0, Lcom/vzw/location/VzwCriteria;->mPerformance:Lcom/vzw/location/VzwGpsPerformance;

    #@a
    invoke-virtual {v0, p1, p2}, Lcom/vzw/location/VzwGpsPerformance;->writeToParcel(Landroid/os/Parcel;I)V

    #@d
    .line 112
    iget-object v0, p0, Lcom/vzw/location/VzwCriteria;->mFixRate:Lcom/vzw/location/VzwGpsFixRate;

    #@f
    invoke-virtual {v0, p1, p2}, Lcom/vzw/location/VzwGpsFixRate;->writeToParcel(Landroid/os/Parcel;I)V

    #@12
    .line 113
    return-void
.end method
