.class Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;
.super Lcom/vzw/location/IVzwGpsStatusListener$Stub;
.source "VzwLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VzwGpsStatusListenerTransport"
.end annotation


# instance fields
.field private final mGpsHandler:Landroid/os/Handler;

.field private final mListener:Lcom/vzw/location/VzwGpsStatus$Listener;

.field final synthetic this$0:Lcom/vzw/location/VzwLocationManager;


# direct methods
.method constructor <init>(Lcom/vzw/location/VzwLocationManager;Lcom/vzw/location/VzwGpsStatus$Listener;)V
    .registers 5
    .parameter
    .parameter "listener"

    #@0
    .prologue
    .line 144
    iput-object p1, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->this$0:Lcom/vzw/location/VzwLocationManager;

    #@2
    .line 142
    invoke-direct {p0}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;-><init>()V

    #@5
    .line 198
    new-instance v0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport$1;

    #@7
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@a
    move-result-object v1

    #@b
    invoke-direct {v0, p0, v1}, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport$1;-><init>(Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;Landroid/os/Looper;)V

    #@e
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@10
    .line 143
    iput-object p2, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mListener:Lcom/vzw/location/VzwGpsStatus$Listener;

    #@12
    return-void
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;)Lcom/vzw/location/VzwGpsStatus$Listener;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mListener:Lcom/vzw/location/VzwGpsStatus$Listener;

    #@2
    return-object v0
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;)Lcom/vzw/location/VzwLocationManager;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->this$0:Lcom/vzw/location/VzwLocationManager;

    #@2
    return-object v0
.end method


# virtual methods
.method public onGpsDeviceStatusChanged(Lcom/vzw/location/VzwGpsDeviceStatus;)V
    .registers 5
    .parameter "deviceStatus"

    #@0
    .prologue
    const/16 v2, 0x19

    #@2
    .line 184
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mListener:Lcom/vzw/location/VzwGpsStatus$Listener;

    #@4
    if-eqz v1, :cond_1f

    #@6
    .line 185
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->this$0:Lcom/vzw/location/VzwLocationManager;

    #@8
    invoke-static {v1}, Lcom/vzw/location/VzwLocationManager;->access$1(Lcom/vzw/location/VzwLocationManager;)Lcom/vzw/location/VzwGpsDeviceStatus;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Lcom/vzw/location/VzwGpsDeviceStatus;->set(Lcom/vzw/location/VzwGpsDeviceStatus;)V

    #@f
    .line 187
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    .line 188
    .local v0, msg:Landroid/os/Message;
    iput v2, v0, Landroid/os/Message;->what:I

    #@15
    .line 190
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@17
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@1a
    .line 191
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@1c
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1f
    .line 196
    .end local v0           #msg:Landroid/os/Message;
    :cond_1f
    return-void
.end method

.method public onGpsStatusChanged(I)V
    .registers 4
    .parameter "status"

    #@0
    .prologue
    .line 147
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mListener:Lcom/vzw/location/VzwGpsStatus$Listener;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 148
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 149
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@a
    .line 150
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@c
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@f
    .line 152
    .end local v0           #msg:Landroid/os/Message;
    :cond_f
    return-void
.end method

.method public onSatStatusChanged(II[I[I[I[I[I)V
    .registers 17
    .parameter "svCount"
    .parameter "svInViewCount"
    .parameter "prn"
    .parameter "prnInView"
    .parameter "elevInView"
    .parameter "aziminView"
    .parameter "snrInView"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mListener:Lcom/vzw/location/VzwGpsStatus$Listener;

    #@2
    if-eqz v0, :cond_29

    #@4
    .line 172
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->this$0:Lcom/vzw/location/VzwLocationManager;

    #@6
    invoke-static {v0}, Lcom/vzw/location/VzwLocationManager;->access$0(Lcom/vzw/location/VzwLocationManager;)Lcom/vzw/location/VzwGpsStatus;

    #@9
    move-result-object v0

    #@a
    move v1, p1

    #@b
    move v2, p2

    #@c
    move-object v3, p3

    #@d
    move-object v4, p4

    #@e
    move-object v5, p5

    #@f
    move-object v6, p6

    #@10
    move-object/from16 v7, p7

    #@12
    invoke-virtual/range {v0 .. v7}, Lcom/vzw/location/VzwGpsStatus;->setStatus(II[I[I[I[I[I)V

    #@15
    .line 174
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@18
    move-result-object v8

    #@19
    .line 175
    .local v8, msg:Landroid/os/Message;
    const/16 v0, 0x9

    #@1b
    iput v0, v8, Landroid/os/Message;->what:I

    #@1d
    .line 177
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@1f
    const/16 v1, 0x9

    #@21
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@24
    .line 178
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@26
    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@29
    .line 180
    .end local v8           #msg:Landroid/os/Message;
    :cond_29
    return-void
.end method

.method public onSvStatusChanged(II[I[I[F[F[F)V
    .registers 17
    .parameter "svCount"
    .parameter "svInViewCount"
    .parameter "prn"
    .parameter "prnInView"
    .parameter "elevInView"
    .parameter "aziminView"
    .parameter "snrInView"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mListener:Lcom/vzw/location/VzwGpsStatus$Listener;

    #@2
    if-eqz v0, :cond_29

    #@4
    .line 157
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->this$0:Lcom/vzw/location/VzwLocationManager;

    #@6
    invoke-static {v0}, Lcom/vzw/location/VzwLocationManager;->access$0(Lcom/vzw/location/VzwLocationManager;)Lcom/vzw/location/VzwGpsStatus;

    #@9
    move-result-object v0

    #@a
    move v1, p1

    #@b
    move v2, p2

    #@c
    move-object v3, p3

    #@d
    move-object v4, p4

    #@e
    move-object v5, p5

    #@f
    move-object v6, p6

    #@10
    move-object/from16 v7, p7

    #@12
    invoke-virtual/range {v0 .. v7}, Lcom/vzw/location/VzwGpsStatus;->setStatus(II[I[I[F[F[F)V

    #@15
    .line 159
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@18
    move-result-object v8

    #@19
    .line 160
    .local v8, msg:Landroid/os/Message;
    const/16 v0, 0x9

    #@1b
    iput v0, v8, Landroid/os/Message;->what:I

    #@1d
    .line 162
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@1f
    const/16 v1, 0x9

    #@21
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@24
    .line 163
    iget-object v0, p0, Lcom/vzw/location/VzwLocationManager$VzwGpsStatusListenerTransport;->mGpsHandler:Landroid/os/Handler;

    #@26
    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@29
    .line 165
    .end local v8           #msg:Landroid/os/Message;
    :cond_29
    return-void
.end method
