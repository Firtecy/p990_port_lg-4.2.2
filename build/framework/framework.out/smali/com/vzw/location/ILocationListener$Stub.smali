.class public abstract Lcom/vzw/location/ILocationListener$Stub;
.super Landroid/os/Binder;
.source "ILocationListener.java"

# interfaces
.implements Lcom/vzw/location/ILocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/ILocationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vzw/location/ILocationListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.vzw.location.ILocationListener"

.field static final TRANSACTION_onLocationChanged:I = 0x1

.field static final TRANSACTION_onProviderDisabled:I = 0x4

.field static final TRANSACTION_onProviderEnabled:I = 0x3

.field static final TRANSACTION_onStatusChanged:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.vzw.location.ILocationListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/vzw/location/ILocationListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/vzw/location/ILocationListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.vzw.location.ILocationListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/vzw/location/ILocationListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/vzw/location/ILocationListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/vzw/location/ILocationListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/vzw/location/ILocationListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_64

    #@4
    .line 92
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 42
    :sswitch_9
    const-string v4, "com.vzw.location.ILocationListener"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v4, "com.vzw.location.ILocationListener"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_26

    #@1a
    .line 50
    sget-object v4, Lcom/vzw/location/VzwLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Lcom/vzw/location/VzwLocation;

    #@22
    .line 55
    .local v0, _arg0:Lcom/vzw/location/VzwLocation;
    :goto_22
    invoke-virtual {p0, v0}, Lcom/vzw/location/ILocationListener$Stub;->onLocationChanged(Lcom/vzw/location/VzwLocation;)V

    #@25
    goto :goto_8

    #@26
    .line 53
    .end local v0           #_arg0:Lcom/vzw/location/VzwLocation;
    :cond_26
    const/4 v0, 0x0

    #@27
    .restart local v0       #_arg0:Lcom/vzw/location/VzwLocation;
    goto :goto_22

    #@28
    .line 60
    .end local v0           #_arg0:Lcom/vzw/location/VzwLocation;
    :sswitch_28
    const-string v4, "com.vzw.location.ILocationListener"

    #@2a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    .line 64
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v1

    #@35
    .line 66
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v4

    #@39
    if-eqz v4, :cond_47

    #@3b
    .line 67
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3d
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@40
    move-result-object v2

    #@41
    check-cast v2, Landroid/os/Bundle;

    #@43
    .line 72
    .local v2, _arg2:Landroid/os/Bundle;
    :goto_43
    invoke-virtual {p0, v0, v1, v2}, Lcom/vzw/location/ILocationListener$Stub;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    #@46
    goto :goto_8

    #@47
    .line 70
    .end local v2           #_arg2:Landroid/os/Bundle;
    :cond_47
    const/4 v2, 0x0

    #@48
    .restart local v2       #_arg2:Landroid/os/Bundle;
    goto :goto_43

    #@49
    .line 77
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_arg2:Landroid/os/Bundle;
    :sswitch_49
    const-string v4, "com.vzw.location.ILocationListener"

    #@4b
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@51
    move-result-object v0

    #@52
    .line 80
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vzw/location/ILocationListener$Stub;->onProviderEnabled(Ljava/lang/String;)V

    #@55
    goto :goto_8

    #@56
    .line 85
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_56
    const-string v4, "com.vzw.location.ILocationListener"

    #@58
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b
    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    .line 88
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vzw/location/ILocationListener$Stub;->onProviderDisabled(Ljava/lang/String;)V

    #@62
    goto :goto_8

    #@63
    .line 38
    nop

    #@64
    :sswitch_data_64
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_28
        0x3 -> :sswitch_49
        0x4 -> :sswitch_56
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
