.class public Lcom/vzw/location/VzwGPSLocationProvider;
.super Landroid/location/LocationProvider;
.source "VzwGPSLocationProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VzwGPSLocationProvider"


# instance fields
.field mAccuracy:I

.field mHasMonetaryCost:Z

.field mName:Ljava/lang/String;

.field mPowerRequirement:I

.field mRequiresCell:Z

.field mRequiresNetwork:Z

.field mRequiresSatellite:Z

.field mSupportsAltitude:Z

.field mSupportsBearing:Z

.field mSupportsSpeed:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 22
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/location/LocationProvider;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    #@4
    .line 23
    return-void
.end method


# virtual methods
.method public getAccuracy()I
    .registers 2

    #@0
    .prologue
    .line 144
    iget v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mAccuracy:I

    #@2
    return v0
.end method

.method public getPowerRequirement()I
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mPowerRequirement:I

    #@2
    return v0
.end method

.method public hasMonetaryCost()Z
    .registers 2

    #@0
    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mHasMonetaryCost:Z

    #@2
    return v0
.end method

.method public meetsCriteria(Landroid/location/Criteria;)Z
    .registers 3
    .parameter "criteria"

    #@0
    .prologue
    .line 149
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public requiresCell()Z
    .registers 2

    #@0
    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mRequiresCell:Z

    #@2
    return v0
.end method

.method public requiresNetwork()Z
    .registers 2

    #@0
    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mRequiresNetwork:Z

    #@2
    return v0
.end method

.method public requiresSatellite()Z
    .registers 2

    #@0
    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mRequiresSatellite:Z

    #@2
    return v0
.end method

.method public setAccuracy(I)V
    .registers 2
    .parameter "accuracy"

    #@0
    .prologue
    .line 58
    iput p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mAccuracy:I

    #@2
    .line 59
    return-void
.end method

.method public setHasMonetaryCost(Z)V
    .registers 2
    .parameter "hasMonetaryCost"

    #@0
    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mHasMonetaryCost:Z

    #@2
    .line 39
    return-void
.end method

.method public setPowerRequirement(I)V
    .registers 2
    .parameter "powerRequirement"

    #@0
    .prologue
    .line 54
    iput p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mPowerRequirement:I

    #@2
    .line 55
    return-void
.end method

.method public setRequiresCell(Z)V
    .registers 2
    .parameter "requiresCell"

    #@0
    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mRequiresCell:Z

    #@2
    .line 35
    return-void
.end method

.method public setRequiresNetwork(Z)V
    .registers 2
    .parameter "requiresNetwork"

    #@0
    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mRequiresNetwork:Z

    #@2
    .line 27
    return-void
.end method

.method public setRequiresSatellite(Z)V
    .registers 2
    .parameter "requiresSatellite"

    #@0
    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mRequiresSatellite:Z

    #@2
    .line 31
    return-void
.end method

.method public setSupportsAltitude(Z)V
    .registers 2
    .parameter "supportsAltitude"

    #@0
    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mSupportsAltitude:Z

    #@2
    .line 43
    return-void
.end method

.method public setSupportsBearing(Z)V
    .registers 2
    .parameter "supportsBearing"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mSupportsBearing:Z

    #@2
    .line 51
    return-void
.end method

.method public setSupportsSpeed(Z)V
    .registers 2
    .parameter "supportsSpeed"

    #@0
    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mSupportsSpeed:Z

    #@2
    .line 47
    return-void
.end method

.method public supportsAltitude()Z
    .registers 2

    #@0
    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mSupportsAltitude:Z

    #@2
    return v0
.end method

.method public supportsBearing()Z
    .registers 2

    #@0
    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mSupportsBearing:Z

    #@2
    return v0
.end method

.method public supportsSpeed()Z
    .registers 2

    #@0
    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/vzw/location/VzwGPSLocationProvider;->mSupportsSpeed:Z

    #@2
    return v0
.end method
