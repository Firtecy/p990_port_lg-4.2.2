.class public Lcom/vzw/location/VzwLocation;
.super Landroid/location/Location;
.source "VzwLocation.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vzw/location/VzwLocation;",
            ">;"
        }
    .end annotation
.end field

.field public static final GPS_VALID_ALTITUDE_WRT_ELLIPSOID:I = 0x80

.field public static final GPS_VALID_ALTITUDE_WRT_SEA_LEVEL:I = 0x40

.field public static final GPS_VALID_FIX_ERROR:I = 0x100000

.field public static final GPS_VALID_FIX_MODE:I = 0x80000

.field public static final GPS_VALID_HEADING:I = 0x10

.field public static final GPS_VALID_HORIZONTAL_DILUTION_OF_PRECISION:I = 0x200

.field public static final GPS_VALID_LATITUDE:I = 0x2

.field public static final GPS_VALID_LONGITUDE:I = 0x4

.field public static final GPS_VALID_MAGNETIC_VARIATION:I = 0x20

.field public static final GPS_VALID_POSITION_DILUTION_OF_PRECISION:I = 0x100

.field public static final GPS_VALID_POSITION_UNCERTAINTY_ERROR:I = 0x40000

.field public static final GPS_VALID_SATELLITES_IN_VIEW:I = 0x2000

.field public static final GPS_VALID_SATELLITES_IN_VIEW_AZIMUTH:I = 0x10000

.field public static final GPS_VALID_SATELLITES_IN_VIEW_ELEVATION:I = 0x8000

.field public static final GPS_VALID_SATELLITES_IN_VIEW_PRNS:I = 0x4000

.field public static final GPS_VALID_SATELLITES_IN_VIEW_SIGNAL_TO_NOISE_RATIO:I = 0x20000

.field public static final GPS_VALID_SATELLITES_USED_PRNS:I = 0x1000

.field public static final GPS_VALID_SATELLITE_COUNT:I = 0x800

.field public static final GPS_VALID_SPEED:I = 0x8

.field public static final GPS_VALID_UTC_TIME:I = 0x1

.field public static final GPS_VALID_VERTICAL_DILUTION_OF_PRECISION:I = 0x400


# instance fields
.field private mAltWrtEllipsoid:F

.field private mAltWrtSeaLevel:F

.field private mFixMode:I

.field private mHDOP:F

.field private mHorConf:F

.field private mMagVar:F

.field private mMajorAxis:F

.field private mMajorAxisAngle:F

.field private mMinorAxis:F

.field private mPDOP:F

.field private mVDOP:F

.field private mValidFields:I

.field private mVerticalError:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 465
    new-instance v0, Lcom/vzw/location/VzwLocation$1;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwLocation$1;-><init>()V

    #@5
    sput-object v0, Lcom/vzw/location/VzwLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 12
    return-void
.end method

.method private constructor <init>(Landroid/location/Location;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 69
    invoke-direct {p0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@4
    .line 38
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@6
    .line 39
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@8
    .line 40
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@a
    .line 41
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@c
    .line 42
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@e
    .line 43
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@10
    .line 44
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@12
    .line 45
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@14
    .line 46
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@16
    .line 47
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@18
    .line 48
    const/4 v0, 0x0

    #@19
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@1b
    .line 49
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@1d
    .line 70
    return-void
.end method

.method synthetic constructor <init>(Landroid/location/Location;Lcom/vzw/location/VzwLocation;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/vzw/location/VzwLocation;-><init>(Landroid/location/Location;)V

    #@3
    return-void
.end method

.method public constructor <init>(Lcom/vzw/location/VzwLocation;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 64
    invoke-direct {p0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@4
    .line 38
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@6
    .line 39
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@8
    .line 40
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@a
    .line 41
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@c
    .line 42
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@e
    .line 43
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@10
    .line 44
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@12
    .line 45
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@14
    .line 46
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@16
    .line 47
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@18
    .line 48
    const/4 v0, 0x0

    #@19
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@1b
    .line 49
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@1d
    .line 65
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwLocation;->set(Lcom/vzw/location/VzwLocation;)V

    #@20
    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 56
    invoke-direct {p0, p1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@4
    .line 38
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@6
    .line 39
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@8
    .line 40
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@a
    .line 41
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@c
    .line 42
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@e
    .line 43
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@10
    .line 44
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@12
    .line 45
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@14
    .line 46
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@16
    .line 47
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@18
    .line 48
    const/4 v0, 0x0

    #@19
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@1b
    .line 49
    iput v1, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@1d
    .line 57
    return-void
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwLocation;I)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 37
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    return-void
.end method

.method static synthetic access$10(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 46
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@2
    return-void
.end method

.method static synthetic access$11(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 47
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@2
    return-void
.end method

.method static synthetic access$12(Lcom/vzw/location/VzwLocation;I)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 48
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@2
    return-void
.end method

.method static synthetic access$13(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 49
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@2
    return-void
.end method

.method static synthetic access$2(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 38
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@2
    return-void
.end method

.method static synthetic access$3(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 39
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@2
    return-void
.end method

.method static synthetic access$4(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 40
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@2
    return-void
.end method

.method static synthetic access$5(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 41
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@2
    return-void
.end method

.method static synthetic access$6(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 42
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@2
    return-void
.end method

.method static synthetic access$7(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 43
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@2
    return-void
.end method

.method static synthetic access$8(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 44
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@2
    return-void
.end method

.method static synthetic access$9(Lcom/vzw/location/VzwLocation;F)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 45
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@2
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 444
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 354
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_1f7

    #@6
    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@f
    const-string v1, "Time: "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0}, Lcom/vzw/location/VzwLocation;->getTime()J

    #@18
    move-result-wide v1

    #@19
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@24
    .line 360
    :goto_24
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@26
    and-int/lit8 v0, v0, 0x2

    #@28
    if-eqz v0, :cond_20f

    #@2a
    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    #@2c
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@33
    const-string v1, "Latitude: "

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {p0}, Lcom/vzw/location/VzwLocation;->getLatitude()D

    #@3c
    move-result-wide v1

    #@3d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@48
    .line 366
    :goto_48
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4a
    and-int/lit8 v0, v0, 0x4

    #@4c
    if-eqz v0, :cond_227

    #@4e
    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    #@50
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@57
    const-string v1, "Longitude: "

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {p0}, Lcom/vzw/location/VzwLocation;->getLongitude()D

    #@60
    move-result-wide v1

    #@61
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@6c
    .line 372
    :goto_6c
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@6e
    and-int/lit8 v0, v0, 0x8

    #@70
    if-eqz v0, :cond_23f

    #@72
    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    #@74
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@77
    move-result-object v1

    #@78
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7b
    const-string v1, "Speed: "

    #@7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v0

    #@81
    invoke-virtual {p0}, Lcom/vzw/location/VzwLocation;->getSpeed()F

    #@84
    move-result v1

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v0

    #@8d
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@90
    .line 378
    :goto_90
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@92
    and-int/lit8 v0, v0, 0x10

    #@94
    if-eqz v0, :cond_257

    #@96
    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    #@98
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@9b
    move-result-object v1

    #@9c
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9f
    const-string v1, "Bearing: "

    #@a1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v0

    #@a5
    invoke-virtual {p0}, Lcom/vzw/location/VzwLocation;->getBearing()F

    #@a8
    move-result v1

    #@a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v0

    #@ad
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v0

    #@b1
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@b4
    .line 384
    :goto_b4
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@b6
    and-int/lit8 v0, v0, 0x20

    #@b8
    if-eqz v0, :cond_26f

    #@ba
    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    #@bc
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@bf
    move-result-object v1

    #@c0
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@c3
    const-string v1, "Magnetic Variation: "

    #@c5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v0

    #@c9
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@cb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v0

    #@cf
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v0

    #@d3
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@d6
    .line 390
    :goto_d6
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@d8
    and-int/lit8 v0, v0, 0x40

    #@da
    if-eqz v0, :cond_287

    #@dc
    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    #@de
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@e1
    move-result-object v1

    #@e2
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@e5
    const-string v1, "Altitude (Sea Level): "

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v0

    #@f1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v0

    #@f5
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@f8
    .line 396
    :goto_f8
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@fa
    and-int/lit16 v0, v0, 0x80

    #@fc
    if-eqz v0, :cond_29f

    #@fe
    .line 397
    new-instance v0, Ljava/lang/StringBuilder;

    #@100
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@103
    move-result-object v1

    #@104
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@107
    const-string v1, "Altitude (Ellipsoid): "

    #@109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v0

    #@10d
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@10f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@112
    move-result-object v0

    #@113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v0

    #@117
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@11a
    .line 402
    :goto_11a
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@11c
    and-int/lit16 v0, v0, 0x100

    #@11e
    if-eqz v0, :cond_2b7

    #@120
    .line 403
    new-instance v0, Ljava/lang/StringBuilder;

    #@122
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@125
    move-result-object v1

    #@126
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@129
    const-string v1, "PDOP: "

    #@12b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v0

    #@12f
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@131
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@134
    move-result-object v0

    #@135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@138
    move-result-object v0

    #@139
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@13c
    .line 408
    :goto_13c
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@13e
    and-int/lit16 v0, v0, 0x200

    #@140
    if-eqz v0, :cond_2cf

    #@142
    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    #@144
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@147
    move-result-object v1

    #@148
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@14b
    const-string v1, "HDOP: "

    #@14d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v0

    #@151
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@153
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@156
    move-result-object v0

    #@157
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v0

    #@15b
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@15e
    .line 414
    :goto_15e
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@160
    and-int/lit16 v0, v0, 0x400

    #@162
    if-eqz v0, :cond_2e7

    #@164
    .line 415
    new-instance v0, Ljava/lang/StringBuilder;

    #@166
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@169
    move-result-object v1

    #@16a
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@16d
    const-string v1, "VDOP: "

    #@16f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v0

    #@173
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@175
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@178
    move-result-object v0

    #@179
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17c
    move-result-object v0

    #@17d
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@180
    .line 420
    :goto_180
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@182
    const/high16 v1, 0x4

    #@184
    and-int/2addr v0, v1

    #@185
    if-eqz v0, :cond_2ff

    #@187
    .line 421
    new-instance v0, Ljava/lang/StringBuilder;

    #@189
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@18c
    move-result-object v1

    #@18d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@190
    const-string v1, "Uncertainty: HC: "

    #@192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v0

    #@196
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@198
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v0

    #@19c
    const-string v1, ", Maj: "

    #@19e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v0

    #@1a2
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@1a4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v0

    #@1a8
    .line 422
    const-string v1, ", MajAng: "

    #@1aa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v0

    #@1ae
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@1b0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v0

    #@1b4
    const-string v1, ", Min: "

    #@1b6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v0

    #@1ba
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@1bc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v0

    #@1c0
    const-string v1, " VE: "

    #@1c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v0

    #@1c6
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@1c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v0

    #@1cc
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cf
    move-result-object v0

    #@1d0
    .line 421
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1d3
    .line 427
    :goto_1d3
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@1d5
    const/high16 v1, 0x8

    #@1d7
    and-int/2addr v0, v1

    #@1d8
    if-eqz v0, :cond_317

    #@1da
    .line 428
    new-instance v0, Ljava/lang/StringBuilder;

    #@1dc
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@1df
    move-result-object v1

    #@1e0
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@1e3
    const-string v1, "Fix Mode: "

    #@1e5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v0

    #@1e9
    iget v1, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@1eb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v0

    #@1ef
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f2
    move-result-object v0

    #@1f3
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1f6
    .line 432
    :goto_1f6
    return-void

    #@1f7
    .line 357
    :cond_1f7
    new-instance v0, Ljava/lang/StringBuilder;

    #@1f9
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@1fc
    move-result-object v1

    #@1fd
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@200
    const-string v1, "Time: ? "

    #@202
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v0

    #@206
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@209
    move-result-object v0

    #@20a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@20d
    goto/16 :goto_24

    #@20f
    .line 363
    :cond_20f
    new-instance v0, Ljava/lang/StringBuilder;

    #@211
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@214
    move-result-object v1

    #@215
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@218
    const-string v1, "Latitude: ? "

    #@21a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v0

    #@21e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@221
    move-result-object v0

    #@222
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@225
    goto/16 :goto_48

    #@227
    .line 369
    :cond_227
    new-instance v0, Ljava/lang/StringBuilder;

    #@229
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@22c
    move-result-object v1

    #@22d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@230
    const-string v1, "Longitude: ? "

    #@232
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@235
    move-result-object v0

    #@236
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@239
    move-result-object v0

    #@23a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@23d
    goto/16 :goto_6c

    #@23f
    .line 375
    :cond_23f
    new-instance v0, Ljava/lang/StringBuilder;

    #@241
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@244
    move-result-object v1

    #@245
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@248
    const-string v1, "Speed: ? "

    #@24a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v0

    #@24e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@251
    move-result-object v0

    #@252
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@255
    goto/16 :goto_90

    #@257
    .line 381
    :cond_257
    new-instance v0, Ljava/lang/StringBuilder;

    #@259
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@25c
    move-result-object v1

    #@25d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@260
    const-string v1, "Bearing: ? "

    #@262
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@265
    move-result-object v0

    #@266
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@269
    move-result-object v0

    #@26a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@26d
    goto/16 :goto_b4

    #@26f
    .line 387
    :cond_26f
    new-instance v0, Ljava/lang/StringBuilder;

    #@271
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@274
    move-result-object v1

    #@275
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@278
    const-string v1, "Magnetic Variation: ? "

    #@27a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v0

    #@27e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@281
    move-result-object v0

    #@282
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@285
    goto/16 :goto_d6

    #@287
    .line 393
    :cond_287
    new-instance v0, Ljava/lang/StringBuilder;

    #@289
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@28c
    move-result-object v1

    #@28d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@290
    const-string v1, "Altitude (Sea Level): ?"

    #@292
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@295
    move-result-object v0

    #@296
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@299
    move-result-object v0

    #@29a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@29d
    goto/16 :goto_f8

    #@29f
    .line 399
    :cond_29f
    new-instance v0, Ljava/lang/StringBuilder;

    #@2a1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@2a4
    move-result-object v1

    #@2a5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@2a8
    const-string v1, "Altitude (Ellipsoid): ?"

    #@2aa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v0

    #@2ae
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b1
    move-result-object v0

    #@2b2
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2b5
    goto/16 :goto_11a

    #@2b7
    .line 405
    :cond_2b7
    new-instance v0, Ljava/lang/StringBuilder;

    #@2b9
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@2bc
    move-result-object v1

    #@2bd
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@2c0
    const-string v1, "PDOP: ? "

    #@2c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v0

    #@2c6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c9
    move-result-object v0

    #@2ca
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2cd
    goto/16 :goto_13c

    #@2cf
    .line 411
    :cond_2cf
    new-instance v0, Ljava/lang/StringBuilder;

    #@2d1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@2d4
    move-result-object v1

    #@2d5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@2d8
    const-string v1, "HDOP: ? "

    #@2da
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2dd
    move-result-object v0

    #@2de
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e1
    move-result-object v0

    #@2e2
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2e5
    goto/16 :goto_15e

    #@2e7
    .line 417
    :cond_2e7
    new-instance v0, Ljava/lang/StringBuilder;

    #@2e9
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@2ec
    move-result-object v1

    #@2ed
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@2f0
    const-string v1, "VDOP: ? "

    #@2f2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f5
    move-result-object v0

    #@2f6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f9
    move-result-object v0

    #@2fa
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2fd
    goto/16 :goto_180

    #@2ff
    .line 424
    :cond_2ff
    new-instance v0, Ljava/lang/StringBuilder;

    #@301
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@304
    move-result-object v1

    #@305
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@308
    const-string v1, "Uncertainty: ? "

    #@30a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30d
    move-result-object v0

    #@30e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@311
    move-result-object v0

    #@312
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@315
    goto/16 :goto_1d3

    #@317
    .line 430
    :cond_317
    new-instance v0, Ljava/lang/StringBuilder;

    #@319
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@31c
    move-result-object v1

    #@31d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@320
    const-string v1, "Fix Mode: ? "

    #@322
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@325
    move-result-object v0

    #@326
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@329
    move-result-object v0

    #@32a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@32d
    goto/16 :goto_1f6
.end method

.method public getAltitudeWrtEllipsoid()F
    .registers 3

    #@0
    .prologue
    .line 187
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit16 v0, v0, 0x80

    #@4
    const/16 v1, 0x80

    #@6
    if-ne v0, v1, :cond_b

    #@8
    .line 188
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@a
    .line 190
    :goto_a
    return v0

    #@b
    :cond_b
    const/high16 v0, 0x7fc0

    #@d
    goto :goto_a
.end method

.method public getAltitudeWrtSeaLevel()F
    .registers 3

    #@0
    .prologue
    .line 169
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit8 v0, v0, 0x40

    #@4
    const/16 v1, 0x40

    #@6
    if-ne v0, v1, :cond_b

    #@8
    .line 170
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@a
    .line 172
    :goto_a
    return v0

    #@b
    :cond_b
    const/high16 v0, 0x7fc0

    #@d
    goto :goto_a
.end method

.method public getFixMode()I
    .registers 2

    #@0
    .prologue
    .line 344
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@2
    return v0
.end method

.method public getHorizontalConfidence()F
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x4

    #@2
    .line 274
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_a

    #@7
    .line 275
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@9
    .line 277
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public getHorizontalDilutionOfPrecision()F
    .registers 3

    #@0
    .prologue
    .line 229
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit16 v0, v0, 0x200

    #@4
    const/16 v1, 0x200

    #@6
    if-ne v0, v1, :cond_b

    #@8
    .line 230
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@a
    .line 232
    :goto_a
    return v0

    #@b
    :cond_b
    const/high16 v0, 0x7fc0

    #@d
    goto :goto_a
.end method

.method public getMagneticVariation()F
    .registers 2

    #@0
    .prologue
    .line 156
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@2
    return v0
.end method

.method public getMajorAxis()F
    .registers 2

    #@0
    .prologue
    .line 295
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@2
    return v0
.end method

.method public getMajorAxisAngle()F
    .registers 2

    #@0
    .prologue
    .line 309
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@2
    return v0
.end method

.method public getMinorAxis()F
    .registers 2

    #@0
    .prologue
    .line 325
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@2
    return v0
.end method

.method public getPositionDilutionOfPrecision()F
    .registers 3

    #@0
    .prologue
    .line 208
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit16 v0, v0, 0x100

    #@4
    const/16 v1, 0x100

    #@6
    if-ne v0, v1, :cond_b

    #@8
    .line 209
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@a
    .line 211
    :goto_a
    return v0

    #@b
    :cond_b
    const/high16 v0, 0x7fc0

    #@d
    goto :goto_a
.end method

.method public getValidFields()I
    .registers 2

    #@0
    .prologue
    .line 104
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    return v0
.end method

.method public getVerticalDilutionOfPrecision()F
    .registers 3

    #@0
    .prologue
    .line 250
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit16 v0, v0, 0x400

    #@4
    const/16 v1, 0x400

    #@6
    if-ne v0, v1, :cond_b

    #@8
    .line 251
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@a
    .line 253
    :goto_a
    return v0

    #@b
    :cond_b
    const/high16 v0, 0x7fc0

    #@d
    goto :goto_a
.end method

.method public getVerticalError()F
    .registers 2

    #@0
    .prologue
    .line 329
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@2
    return v0
.end method

.method public hasBearing()Z
    .registers 2

    #@0
    .prologue
    .line 152
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit8 v0, v0, 0x10

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public hasSpeed()Z
    .registers 2

    #@0
    .prologue
    .line 141
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public removeAccuracy()V
    .registers 2

    #@0
    .prologue
    .line 112
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 95
    invoke-super {p0}, Landroid/location/Location;->reset()V

    #@3
    .line 96
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@6
    .line 97
    return-void
.end method

.method public set(Lcom/vzw/location/VzwLocation;)V
    .registers 3
    .parameter "location"

    #@0
    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/location/Location;->set(Landroid/location/Location;)V

    #@3
    .line 75
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@5
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@7
    .line 76
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@9
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@b
    .line 77
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@d
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@f
    .line 78
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@11
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@13
    .line 79
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@15
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@17
    .line 80
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@19
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@1b
    .line 81
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@1d
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@1f
    .line 82
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@21
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@23
    .line 83
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@25
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@27
    .line 84
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@29
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@2b
    .line 85
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@2d
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@2f
    .line 86
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@31
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@33
    .line 87
    iget v0, p1, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@35
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@37
    .line 88
    return-void
.end method

.method public setAltitudeWrtEllipsoid(F)V
    .registers 3
    .parameter "altitudeWrtEllipsoid"

    #@0
    .prologue
    .line 194
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@2
    .line 195
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    or-int/lit16 v0, v0, 0x80

    #@6
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@8
    .line 196
    return-void
.end method

.method public setAltitudeWrtSeaLevel(F)V
    .registers 3
    .parameter "altitudeWrtSeaLevel"

    #@0
    .prologue
    .line 176
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@2
    .line 177
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    or-int/lit8 v0, v0, 0x40

    #@6
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@8
    .line 178
    return-void
.end method

.method public setBearing(F)V
    .registers 3
    .parameter "bearing"

    #@0
    .prologue
    .line 146
    invoke-super {p0, p1}, Landroid/location/Location;->setBearing(F)V

    #@3
    .line 147
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@5
    or-int/lit8 v0, v0, 0x10

    #@7
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@9
    .line 148
    return-void
.end method

.method public setFixMode(I)V
    .registers 4
    .parameter "fixMode"

    #@0
    .prologue
    .line 348
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@2
    .line 349
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    const/high16 v1, 0x8

    #@6
    or-int/2addr v0, v1

    #@7
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@9
    .line 350
    return-void
.end method

.method public setHorizontalDilutionOfPrecision(F)V
    .registers 3
    .parameter "horizontalDilutionOfPrecision"

    #@0
    .prologue
    .line 236
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@2
    .line 237
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    or-int/lit16 v0, v0, 0x200

    #@6
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@8
    .line 238
    return-void
.end method

.method public setLatitude(D)V
    .registers 4
    .parameter "latitude"

    #@0
    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Landroid/location/Location;->setLatitude(D)V

    #@3
    .line 124
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@5
    or-int/lit8 v0, v0, 0x2

    #@7
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@9
    .line 125
    return-void
.end method

.method public setLongitude(D)V
    .registers 4
    .parameter "longitude"

    #@0
    .prologue
    .line 129
    invoke-super {p0, p1, p2}, Landroid/location/Location;->setLongitude(D)V

    #@3
    .line 130
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@5
    or-int/lit8 v0, v0, 0x4

    #@7
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@9
    .line 131
    return-void
.end method

.method public setMagneticVariation(F)V
    .registers 3
    .parameter "magneticVariation"

    #@0
    .prologue
    .line 160
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@2
    .line 161
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    or-int/lit8 v0, v0, 0x20

    #@6
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@8
    .line 162
    return-void
.end method

.method public setPositionDilutionOfPrecision(F)V
    .registers 3
    .parameter "positionDilutionOfPrecision"

    #@0
    .prologue
    .line 215
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@2
    .line 216
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    or-int/lit16 v0, v0, 0x100

    #@6
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@8
    .line 217
    return-void
.end method

.method public setPositionError(FFFFF)V
    .registers 8
    .parameter "horConf"
    .parameter "majorAxis"
    .parameter "majorAxisAngle"
    .parameter "minorAxis"
    .parameter "verticalError"

    #@0
    .prologue
    .line 334
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@2
    .line 335
    iput p2, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@4
    .line 336
    iput p3, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@6
    .line 337
    iput p4, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@8
    .line 338
    iput p5, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@a
    .line 340
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@c
    const/high16 v1, 0x4

    #@e
    or-int/2addr v0, v1

    #@f
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@11
    .line 341
    return-void
.end method

.method public setSpeed(F)V
    .registers 3
    .parameter "speed"

    #@0
    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/location/Location;->setSpeed(F)V

    #@3
    .line 136
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@5
    or-int/lit8 v0, v0, 0x8

    #@7
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@9
    .line 137
    return-void
.end method

.method public setTime(J)V
    .registers 4
    .parameter "time"

    #@0
    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Landroid/location/Location;->setTime(J)V

    #@3
    .line 118
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@5
    or-int/lit8 v0, v0, 0x1

    #@7
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@9
    .line 119
    return-void
.end method

.method public setVerticalDilutionOfPrecision(F)V
    .registers 3
    .parameter "verticalDilutionOfPrecision"

    #@0
    .prologue
    .line 257
    iput p1, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@2
    .line 258
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@4
    or-int/lit16 v0, v0, 0x400

    #@6
    iput v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@8
    .line 259
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 436
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 437
    .local v1, stringBuffer:Ljava/lang/StringBuilder;
    new-instance v0, Landroid/util/StringBuilderPrinter;

    #@7
    invoke-direct {v0, v1}, Landroid/util/StringBuilderPrinter;-><init>(Ljava/lang/StringBuilder;)V

    #@a
    .line 438
    .local v0, printer:Landroid/util/StringBuilderPrinter;
    const-string v2, ""

    #@c
    invoke-virtual {p0, v0, v2}, Lcom/vzw/location/VzwLocation;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@f
    .line 439
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    return-object v2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 449
    invoke-super {p0, p1, p2}, Landroid/location/Location;->writeToParcel(Landroid/os/Parcel;I)V

    #@3
    .line 450
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mValidFields:I

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 451
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mHDOP:F

    #@a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@d
    .line 452
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mPDOP:F

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@12
    .line 453
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mVDOP:F

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@17
    .line 454
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mAltWrtEllipsoid:F

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@1c
    .line 455
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mAltWrtSeaLevel:F

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@21
    .line 456
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mHorConf:F

    #@23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@26
    .line 457
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMajorAxis:F

    #@28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@2b
    .line 458
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMajorAxisAngle:F

    #@2d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@30
    .line 459
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMinorAxis:F

    #@32
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@35
    .line 460
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mVerticalError:F

    #@37
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@3a
    .line 461
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mFixMode:I

    #@3c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3f
    .line 462
    iget v0, p0, Lcom/vzw/location/VzwLocation;->mMagVar:F

    #@41
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@44
    .line 463
    return-void
.end method
