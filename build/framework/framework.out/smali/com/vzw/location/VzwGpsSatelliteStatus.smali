.class public Lcom/vzw/location/VzwGpsSatelliteStatus;
.super Ljava/lang/Object;
.source "VzwGpsSatelliteStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vzw/location/VzwGpsSatelliteStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCnr:F

.field mHasAlmanac:Z

.field mHasEphemeris:Z

.field mPrn:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 78
    new-instance v0, Lcom/vzw/location/VzwGpsSatelliteStatus$1;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsSatelliteStatus$1;-><init>()V

    #@5
    sput-object v0, Lcom/vzw/location/VzwGpsSatelliteStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 11
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 18
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 68
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCnr()F
    .registers 2

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@2
    return v0
.end method

.method public getPrn()I
    .registers 2

    #@0
    .prologue
    .line 32
    iget v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@2
    return v0
.end method

.method public hasAlmanac()Z
    .registers 2

    #@0
    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@2
    return v0
.end method

.method public hasEphemeris()Z
    .registers 2

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@2
    return v0
.end method

.method setStatus(Lcom/vzw/location/VzwGpsSatelliteStatus;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 21
    iget v0, p1, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@2
    iput v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@4
    .line 22
    iget-boolean v0, p1, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@6
    iput-boolean v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@8
    .line 23
    iget-boolean v0, p1, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@a
    iput-boolean v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@c
    .line 24
    iget v0, p1, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@e
    iput v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@10
    .line 25
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "PRN: "

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    iget v1, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, ", C/N: "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget v1, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ", In Alamanac: "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget-boolean v1, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    const-string v1, ", In Ephm: "

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-boolean v1, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 72
    iget v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 73
    iget v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@c
    .line 74
    iget-boolean v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@e
    if-eqz v0, :cond_1c

    #@10
    move v0, v1

    #@11
    :goto_11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@14
    .line 75
    iget-boolean v0, p0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@16
    if-eqz v0, :cond_1e

    #@18
    :goto_18
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    #@1b
    .line 76
    return-void

    #@1c
    :cond_1c
    move v0, v2

    #@1d
    .line 74
    goto :goto_11

    #@1e
    :cond_1e
    move v1, v2

    #@1f
    .line 75
    goto :goto_18
.end method
