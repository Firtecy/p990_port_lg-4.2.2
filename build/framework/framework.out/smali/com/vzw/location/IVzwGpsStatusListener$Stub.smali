.class public abstract Lcom/vzw/location/IVzwGpsStatusListener$Stub;
.super Landroid/os/Binder;
.source "IVzwGpsStatusListener.java"

# interfaces
.implements Lcom/vzw/location/IVzwGpsStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/IVzwGpsStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.vzw.location.IVzwGpsStatusListener"

.field static final TRANSACTION_onGpsDeviceStatusChanged:I = 0x4

.field static final TRANSACTION_onGpsStatusChanged:I = 0x1

.field static final TRANSACTION_onSatStatusChanged:I = 0x3

.field static final TRANSACTION_onSvStatusChanged:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.vzw.location.IVzwGpsStatusListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/vzw/location/IVzwGpsStatusListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.vzw.location.IVzwGpsStatusListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/vzw/location/IVzwGpsStatusListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/vzw/location/IVzwGpsStatusListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_86

    #@4
    .line 107
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 42
    :sswitch_9
    const-string v0, "com.vzw.location.IVzwGpsStatusListener"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    move v0, v8

    #@f
    .line 43
    goto :goto_8

    #@10
    .line 47
    :sswitch_10
    const-string v0, "com.vzw.location.IVzwGpsStatusListener"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v1

    #@19
    .line 50
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;->onGpsStatusChanged(I)V

    #@1c
    move v0, v8

    #@1d
    .line 51
    goto :goto_8

    #@1e
    .line 55
    .end local v1           #_arg0:I
    :sswitch_1e
    const-string v0, "com.vzw.location.IVzwGpsStatusListener"

    #@20
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@23
    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26
    move-result v1

    #@27
    .line 59
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v2

    #@2b
    .line 61
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@2e
    move-result-object v3

    #@2f
    .line 63
    .local v3, _arg2:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@32
    move-result-object v4

    #@33
    .line 65
    .local v4, _arg3:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createFloatArray()[F

    #@36
    move-result-object v5

    #@37
    .line 67
    .local v5, _arg4:[F
    invoke-virtual {p2}, Landroid/os/Parcel;->createFloatArray()[F

    #@3a
    move-result-object v6

    #@3b
    .line 69
    .local v6, _arg5:[F
    invoke-virtual {p2}, Landroid/os/Parcel;->createFloatArray()[F

    #@3e
    move-result-object v7

    #@3f
    .local v7, _arg6:[F
    move-object v0, p0

    #@40
    .line 70
    invoke-virtual/range {v0 .. v7}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;->onSvStatusChanged(II[I[I[F[F[F)V

    #@43
    move v0, v8

    #@44
    .line 71
    goto :goto_8

    #@45
    .line 75
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:[I
    .end local v5           #_arg4:[F
    .end local v6           #_arg5:[F
    .end local v7           #_arg6:[F
    :sswitch_45
    const-string v0, "com.vzw.location.IVzwGpsStatusListener"

    #@47
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4a
    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v1

    #@4e
    .line 79
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v2

    #@52
    .line 81
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@55
    move-result-object v3

    #@56
    .line 83
    .restart local v3       #_arg2:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@59
    move-result-object v4

    #@5a
    .line 85
    .restart local v4       #_arg3:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@5d
    move-result-object v5

    #@5e
    .line 87
    .local v5, _arg4:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@61
    move-result-object v6

    #@62
    .line 89
    .local v6, _arg5:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@65
    move-result-object v7

    #@66
    .local v7, _arg6:[I
    move-object v0, p0

    #@67
    .line 90
    invoke-virtual/range {v0 .. v7}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;->onSatStatusChanged(II[I[I[I[I[I)V

    #@6a
    move v0, v8

    #@6b
    .line 91
    goto :goto_8

    #@6c
    .line 95
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:[I
    .end local v5           #_arg4:[I
    .end local v6           #_arg5:[I
    .end local v7           #_arg6:[I
    :sswitch_6c
    const-string v0, "com.vzw.location.IVzwGpsStatusListener"

    #@6e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@71
    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@74
    move-result v0

    #@75
    if-eqz v0, :cond_84

    #@77
    .line 98
    sget-object v0, Lcom/vzw/location/VzwGpsDeviceStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    #@79
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7c
    move-result-object v1

    #@7d
    check-cast v1, Lcom/vzw/location/VzwGpsDeviceStatus;

    #@7f
    .line 103
    .local v1, _arg0:Lcom/vzw/location/VzwGpsDeviceStatus;
    :goto_7f
    invoke-virtual {p0, v1}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;->onGpsDeviceStatusChanged(Lcom/vzw/location/VzwGpsDeviceStatus;)V

    #@82
    move v0, v8

    #@83
    .line 104
    goto :goto_8

    #@84
    .line 101
    .end local v1           #_arg0:Lcom/vzw/location/VzwGpsDeviceStatus;
    :cond_84
    const/4 v1, 0x0

    #@85
    .restart local v1       #_arg0:Lcom/vzw/location/VzwGpsDeviceStatus;
    goto :goto_7f

    #@86
    .line 38
    :sswitch_data_86
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_1e
        0x3 -> :sswitch_45
        0x4 -> :sswitch_6c
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
