.class public Lcom/vzw/location/VzwGpsFixRate;
.super Ljava/lang/Object;
.source "VzwGpsFixRate.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vzw/location/VzwGpsFixRate;",
            ">;"
        }
    .end annotation
.end field

.field public static final INFINITE_NUM_FIXES:I = -0x1


# instance fields
.field private mNumFixes:J

.field private mTimeBetweenFixes:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 65
    new-instance v0, Lcom/vzw/location/VzwGpsFixRate$1;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsFixRate$1;-><init>()V

    #@5
    sput-object v0, Lcom/vzw/location/VzwGpsFixRate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 11
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 14
    const-wide/16 v0, 0xa

    #@5
    iput-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@7
    .line 15
    const-wide/16 v0, 0x5

    #@9
    iput-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mTimeBetweenFixes:J

    #@b
    .line 18
    return-void
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwGpsFixRate;J)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 14
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@2
    return-void
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwGpsFixRate;J)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 15
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsFixRate;->mTimeBetweenFixes:J

    #@2
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 57
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    const-string/jumbo v1, "mNumFixes ="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@12
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1d
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    #@1f
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@26
    const-string/jumbo v1, "mTimeBetweenFixes ="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsFixRate;->mTimeBetweenFixes:J

    #@2f
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@3a
    .line 49
    return-void
.end method

.method public getNumFixes()J
    .registers 5

    #@0
    .prologue
    .line 36
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@2
    const-wide/32 v2, 0x7fffffff

    #@5
    cmp-long v0, v0, v2

    #@7
    if-nez v0, :cond_c

    #@9
    .line 37
    const-wide/16 v0, -0x1

    #@b
    .line 39
    :goto_b
    return-wide v0

    #@c
    :cond_c
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@e
    goto :goto_b
.end method

.method public getTimeBetweenFixes()J
    .registers 3

    #@0
    .prologue
    .line 43
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mTimeBetweenFixes:J

    #@2
    return-wide v0
.end method

.method public setGpsFixRate(JJ)V
    .registers 9
    .parameter "numFixes"
    .parameter "timeBetweenFixes"

    #@0
    .prologue
    const-wide/16 v2, -0x1

    #@2
    .line 21
    cmp-long v0, p1, v2

    #@4
    if-eqz v0, :cond_22

    #@6
    const-wide/16 v0, 0x1

    #@8
    cmp-long v0, p1, v0

    #@a
    if-gez v0, :cond_22

    #@c
    .line 22
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    const-string/jumbo v2, "numFixes="

    #@13
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@16
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 24
    :cond_22
    const-wide/16 v0, 0x0

    #@24
    cmp-long v0, p3, v0

    #@26
    if-gez v0, :cond_3e

    #@28
    .line 25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2a
    new-instance v1, Ljava/lang/StringBuilder;

    #@2c
    const-string/jumbo v2, "timeBetweenFixes="

    #@2f
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@32
    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v0

    #@3e
    .line 27
    :cond_3e
    cmp-long v0, p1, v2

    #@40
    if-nez v0, :cond_4a

    #@42
    .line 28
    const-wide/32 v0, 0x7fffffff

    #@45
    iput-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@47
    .line 32
    :goto_47
    iput-wide p3, p0, Lcom/vzw/location/VzwGpsFixRate;->mTimeBetweenFixes:J

    #@49
    .line 33
    return-void

    #@4a
    .line 30
    :cond_4a
    iput-wide p1, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@4c
    goto :goto_47
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string/jumbo v1, "mNumFixes="

    #@5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@a
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v1, ", mTimeBetweenFixes="

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    iget-wide v1, p0, Lcom/vzw/location/VzwGpsFixRate;->mTimeBetweenFixes:J

    #@16
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 61
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mNumFixes:J

    #@2
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 62
    iget-wide v0, p0, Lcom/vzw/location/VzwGpsFixRate;->mTimeBetweenFixes:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 63
    return-void
.end method
