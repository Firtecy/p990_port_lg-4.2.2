.class Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;
.super Ljava/lang/Object;
.source "IVzwLocationProvider.java"

# interfaces
.implements Lcom/vzw/location/IVzwLocationProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/IVzwLocationProvider$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 239
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 241
    iput-object p1, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 242
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public cancelFix()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 580
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 581
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 584
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 585
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x13

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 586
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 587
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 590
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 591
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 593
    return v2

    #@27
    .line 589
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    .line 590
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 591
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 592
    throw v3
.end method

.method public disable()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 420
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 421
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 423
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.vzw.location.IVzwLocationProvider"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 424
    iget-object v2, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0xb

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 425
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 428
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 429
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 431
    return-void

    #@1f
    .line 427
    :catchall_1f
    move-exception v2

    #@20
    .line 428
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 429
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 430
    throw v2
.end method

.method public enable()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 406
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 407
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 409
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.vzw.location.IVzwLocationProvider"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 410
    iget-object v2, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0xa

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 411
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 414
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 415
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 417
    return-void

    #@1f
    .line 413
    :catchall_1f
    move-exception v2

    #@20
    .line 414
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 415
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 416
    throw v2
.end method

.method public getAccuracy()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 389
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 390
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 393
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 394
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x9

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 395
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 396
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 399
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 400
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 402
    return v2

    #@23
    .line 398
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    .line 399
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 400
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 401
    throw v3
.end method

.method public getGpsPrivacySetting()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 481
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 482
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 485
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 486
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xe

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 487
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 488
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 491
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 492
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 494
    return v2

    #@23
    .line 490
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    .line 491
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 492
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 493
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 249
    const-string v0, "com.vzw.location.IVzwLocationProvider"

    #@2
    return-object v0
.end method

.method public getPowerRequirement()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 372
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 373
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 376
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 377
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x8

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 378
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 379
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 382
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 383
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 385
    return v2

    #@23
    .line 381
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    .line 382
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 383
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 384
    throw v3
.end method

.method public hasMonetaryCost()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 304
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 305
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 308
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 309
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x4

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 310
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 311
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 314
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 315
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 317
    return v2

    #@26
    .line 313
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    .line 314
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 315
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 316
    throw v3
.end method

.method public isEnabled()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 434
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 435
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 438
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 439
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0xc

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 440
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 441
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 444
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 445
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 447
    return v2

    #@27
    .line 443
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    .line 444
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 445
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 446
    throw v3
.end method

.method public requiresCell()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 287
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 288
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 291
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 292
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x3

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 293
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 294
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 297
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 298
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 300
    return v2

    #@26
    .line 296
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    .line 297
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 298
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 299
    throw v3
.end method

.method public requiresNetwork()Z
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 253
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 254
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 257
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.vzw.location.IVzwLocationProvider"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 258
    iget-object v4, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v5, 0x1

    #@12
    const/4 v6, 0x0

    #@13
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 259
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 260
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_a .. :try_end_1c} :catchall_28

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_26

    #@1f
    .line 263
    .local v2, _result:Z
    :goto_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 264
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 266
    return v2

    #@26
    .end local v2           #_result:Z
    :cond_26
    move v2, v3

    #@27
    .line 260
    goto :goto_1f

    #@28
    .line 262
    :catchall_28
    move-exception v3

    #@29
    .line 263
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 264
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 265
    throw v3
.end method

.method public requiresSatellite()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 270
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 271
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 274
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 275
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x2

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 276
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 277
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 280
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 281
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 283
    return v2

    #@26
    .line 279
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    .line 280
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 281
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 282
    throw v3
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 10
    .parameter "command"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 451
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 452
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 455
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.vzw.location.IVzwLocationProvider"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 456
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 457
    if-eqz p2, :cond_3d

    #@14
    .line 458
    const/4 v4, 0x1

    #@15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 459
    const/4 v4, 0x0

    #@19
    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c
    .line 464
    :goto_1c
    iget-object v4, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0xd

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 465
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 466
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_4a

    #@2d
    .line 467
    .local v2, _result:Z
    :goto_2d
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_36

    #@33
    .line 468
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_36
    .catchall {:try_start_a .. :try_end_36} :catchall_42

    #@36
    .line 472
    :cond_36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 473
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 475
    return v2

    #@3d
    .line 462
    .end local v2           #_result:Z
    :cond_3d
    const/4 v4, 0x0

    #@3e
    :try_start_3e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_41
    .catchall {:try_start_3e .. :try_end_41} :catchall_42

    #@41
    goto :goto_1c

    #@42
    .line 471
    :catchall_42
    move-exception v3

    #@43
    .line 472
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 473
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    .line 474
    throw v3

    #@4a
    :cond_4a
    move v2, v3

    #@4b
    .line 466
    goto :goto_2d
.end method

.method public setGpsConfig(ILcom/vzw/location/VzwGpsPerformance;)Z
    .registers 10
    .parameter "fixMode"
    .parameter "performance"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 516
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 517
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 520
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.vzw.location.IVzwLocationProvider"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 521
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 522
    if-eqz p2, :cond_34

    #@14
    .line 523
    const/4 v4, 0x1

    #@15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 524
    const/4 v4, 0x0

    #@19
    invoke-virtual {p2, v0, v4}, Lcom/vzw/location/VzwGpsPerformance;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c
    .line 529
    :goto_1c
    iget-object v4, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0x10

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 530
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 531
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2a
    .catchall {:try_start_a .. :try_end_2a} :catchall_39

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_41

    #@2d
    .line 534
    .local v2, _result:Z
    :goto_2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 535
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 537
    return v2

    #@34
    .line 527
    .end local v2           #_result:Z
    :cond_34
    const/4 v4, 0x0

    #@35
    :try_start_35
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_39

    #@38
    goto :goto_1c

    #@39
    .line 533
    :catchall_39
    move-exception v3

    #@3a
    .line 534
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 535
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 536
    throw v3

    #@41
    :cond_41
    move v2, v3

    #@42
    .line 531
    goto :goto_2d
.end method

.method public setGpsPrivacySetting(I)Z
    .registers 8
    .parameter "setting"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 498
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 499
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 502
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 503
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 504
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0xf

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 505
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 506
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 509
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 510
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 512
    return v2

    #@2a
    .line 508
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    .line 509
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 510
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 511
    throw v3
.end method

.method public setPde([BI)Z
    .registers 9
    .parameter "pdeAddress"
    .parameter "pdePort"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 541
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 542
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 545
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 546
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@11
    .line 547
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 548
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x11

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 549
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 550
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 553
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 554
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 556
    return v2

    #@2d
    .line 552
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    .line 553
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 554
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 555
    throw v3
.end method

.method public startFix(I)Z
    .registers 8
    .parameter "fixUId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 562
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 563
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 566
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 567
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 568
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x12

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 569
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 570
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 573
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 574
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 576
    return v2

    #@2a
    .line 572
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    .line 573
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 574
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 575
    throw v3
.end method

.method public supportsAltitude()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 321
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 322
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 325
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 326
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x5

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 327
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 328
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 331
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 332
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 334
    return v2

    #@26
    .line 330
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    .line 331
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 332
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 333
    throw v3
.end method

.method public supportsBearing()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 355
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 356
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 359
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 360
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x7

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 361
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 362
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 365
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 366
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 368
    return v2

    #@26
    .line 364
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    .line 365
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 366
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 367
    throw v3
.end method

.method public supportsSpeed()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 338
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 339
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 342
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 343
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x6

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 344
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 345
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 348
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 349
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 351
    return v2

    #@26
    .line 347
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    .line 348
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 349
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 350
    throw v3
.end method
