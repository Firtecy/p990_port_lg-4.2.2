.class public abstract Lcom/vzw/location/IVzwLocationProvider$Stub;
.super Landroid/os/Binder;
.source "IVzwLocationProvider.java"

# interfaces
.implements Lcom/vzw/location/IVzwLocationProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/IVzwLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.vzw.location.IVzwLocationProvider"

.field static final TRANSACTION_cancelFix:I = 0x13

.field static final TRANSACTION_disable:I = 0xb

.field static final TRANSACTION_enable:I = 0xa

.field static final TRANSACTION_getAccuracy:I = 0x9

.field static final TRANSACTION_getGpsPrivacySetting:I = 0xe

.field static final TRANSACTION_getPowerRequirement:I = 0x8

.field static final TRANSACTION_hasMonetaryCost:I = 0x4

.field static final TRANSACTION_isEnabled:I = 0xc

.field static final TRANSACTION_requiresCell:I = 0x3

.field static final TRANSACTION_requiresNetwork:I = 0x1

.field static final TRANSACTION_requiresSatellite:I = 0x2

.field static final TRANSACTION_sendExtraCommand:I = 0xd

.field static final TRANSACTION_setGpsConfig:I = 0x10

.field static final TRANSACTION_setGpsPrivacySetting:I = 0xf

.field static final TRANSACTION_setPde:I = 0x11

.field static final TRANSACTION_startFix:I = 0x12

.field static final TRANSACTION_supportsAltitude:I = 0x5

.field static final TRANSACTION_supportsBearing:I = 0x7

.field static final TRANSACTION_supportsSpeed:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.vzw.location.IVzwLocationProvider"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/vzw/location/IVzwLocationProvider;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.vzw.location.IVzwLocationProvider"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/vzw/location/IVzwLocationProvider;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/vzw/location/IVzwLocationProvider;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/vzw/location/IVzwLocationProvider$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_1b6

    #@5
    .line 234
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 47
    :sswitch_a
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->requiresNetwork()Z

    #@18
    move-result v2

    #@19
    .line 54
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 55
    if-eqz v2, :cond_1f

    #@1e
    move v3, v4

    #@1f
    :cond_1f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    goto :goto_9

    #@23
    .line 60
    .end local v2           #_result:Z
    :sswitch_23
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@25
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 61
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->requiresSatellite()Z

    #@2b
    move-result v2

    #@2c
    .line 62
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f
    .line 63
    if-eqz v2, :cond_32

    #@31
    move v3, v4

    #@32
    :cond_32
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@35
    goto :goto_9

    #@36
    .line 68
    .end local v2           #_result:Z
    :sswitch_36
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@38
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b
    .line 69
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->requiresCell()Z

    #@3e
    move-result v2

    #@3f
    .line 70
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@42
    .line 71
    if-eqz v2, :cond_45

    #@44
    move v3, v4

    #@45
    :cond_45
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@48
    goto :goto_9

    #@49
    .line 76
    .end local v2           #_result:Z
    :sswitch_49
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@4b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 77
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->hasMonetaryCost()Z

    #@51
    move-result v2

    #@52
    .line 78
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@55
    .line 79
    if-eqz v2, :cond_58

    #@57
    move v3, v4

    #@58
    :cond_58
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    goto :goto_9

    #@5c
    .line 84
    .end local v2           #_result:Z
    :sswitch_5c
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@5e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61
    .line 85
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->supportsAltitude()Z

    #@64
    move-result v2

    #@65
    .line 86
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@68
    .line 87
    if-eqz v2, :cond_6b

    #@6a
    move v3, v4

    #@6b
    :cond_6b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6e
    goto :goto_9

    #@6f
    .line 92
    .end local v2           #_result:Z
    :sswitch_6f
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@71
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@74
    .line 93
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->supportsSpeed()Z

    #@77
    move-result v2

    #@78
    .line 94
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7b
    .line 95
    if-eqz v2, :cond_7e

    #@7d
    move v3, v4

    #@7e
    :cond_7e
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@81
    goto :goto_9

    #@82
    .line 100
    .end local v2           #_result:Z
    :sswitch_82
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@84
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@87
    .line 101
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->supportsBearing()Z

    #@8a
    move-result v2

    #@8b
    .line 102
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8e
    .line 103
    if-eqz v2, :cond_91

    #@90
    move v3, v4

    #@91
    :cond_91
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@94
    goto/16 :goto_9

    #@96
    .line 108
    .end local v2           #_result:Z
    :sswitch_96
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@98
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9b
    .line 109
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->getPowerRequirement()I

    #@9e
    move-result v2

    #@9f
    .line 110
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a2
    .line 111
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@a5
    goto/16 :goto_9

    #@a7
    .line 116
    .end local v2           #_result:I
    :sswitch_a7
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@a9
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ac
    .line 117
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->getAccuracy()I

    #@af
    move-result v2

    #@b0
    .line 118
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b3
    .line 119
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@b6
    goto/16 :goto_9

    #@b8
    .line 124
    .end local v2           #_result:I
    :sswitch_b8
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@ba
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bd
    .line 125
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->enable()V

    #@c0
    .line 126
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c3
    goto/16 :goto_9

    #@c5
    .line 131
    :sswitch_c5
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@c7
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ca
    .line 132
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->disable()V

    #@cd
    .line 133
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d0
    goto/16 :goto_9

    #@d2
    .line 138
    :sswitch_d2
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@d4
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d7
    .line 139
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->isEnabled()Z

    #@da
    move-result v2

    #@db
    .line 140
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@de
    .line 141
    if-eqz v2, :cond_e1

    #@e0
    move v3, v4

    #@e1
    :cond_e1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@e4
    goto/16 :goto_9

    #@e6
    .line 146
    .end local v2           #_result:Z
    :sswitch_e6
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@e8
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@eb
    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ee
    move-result-object v0

    #@ef
    .line 150
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f2
    move-result v5

    #@f3
    if-eqz v5, :cond_114

    #@f5
    .line 151
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f7
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@fa
    move-result-object v1

    #@fb
    check-cast v1, Landroid/os/Bundle;

    #@fd
    .line 156
    .local v1, _arg1:Landroid/os/Bundle;
    :goto_fd
    invoke-virtual {p0, v0, v1}, Lcom/vzw/location/IVzwLocationProvider$Stub;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@100
    move-result v2

    #@101
    .line 157
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@104
    .line 158
    if-eqz v2, :cond_116

    #@106
    move v5, v4

    #@107
    :goto_107
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@10a
    .line 159
    if-eqz v1, :cond_118

    #@10c
    .line 160
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@10f
    .line 161
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@112
    goto/16 :goto_9

    #@114
    .line 154
    .end local v1           #_arg1:Landroid/os/Bundle;
    .end local v2           #_result:Z
    :cond_114
    const/4 v1, 0x0

    #@115
    .restart local v1       #_arg1:Landroid/os/Bundle;
    goto :goto_fd

    #@116
    .restart local v2       #_result:Z
    :cond_116
    move v5, v3

    #@117
    .line 158
    goto :goto_107

    #@118
    .line 164
    :cond_118
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11b
    goto/16 :goto_9

    #@11d
    .line 170
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Landroid/os/Bundle;
    .end local v2           #_result:Z
    :sswitch_11d
    const-string v3, "com.vzw.location.IVzwLocationProvider"

    #@11f
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@122
    .line 171
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->getGpsPrivacySetting()I

    #@125
    move-result v2

    #@126
    .line 172
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@129
    .line 173
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@12c
    goto/16 :goto_9

    #@12e
    .line 178
    .end local v2           #_result:I
    :sswitch_12e
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@130
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@133
    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@136
    move-result v0

    #@137
    .line 181
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->setGpsPrivacySetting(I)Z

    #@13a
    move-result v2

    #@13b
    .line 182
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13e
    .line 183
    if-eqz v2, :cond_141

    #@140
    move v3, v4

    #@141
    :cond_141
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@144
    goto/16 :goto_9

    #@146
    .line 188
    .end local v0           #_arg0:I
    .end local v2           #_result:Z
    :sswitch_146
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@148
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14b
    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14e
    move-result v0

    #@14f
    .line 192
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@152
    move-result v5

    #@153
    if-eqz v5, :cond_16c

    #@155
    .line 193
    sget-object v5, Lcom/vzw/location/VzwGpsPerformance;->CREATOR:Landroid/os/Parcelable$Creator;

    #@157
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@15a
    move-result-object v1

    #@15b
    check-cast v1, Lcom/vzw/location/VzwGpsPerformance;

    #@15d
    .line 198
    .local v1, _arg1:Lcom/vzw/location/VzwGpsPerformance;
    :goto_15d
    invoke-virtual {p0, v0, v1}, Lcom/vzw/location/IVzwLocationProvider$Stub;->setGpsConfig(ILcom/vzw/location/VzwGpsPerformance;)Z

    #@160
    move-result v2

    #@161
    .line 199
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@164
    .line 200
    if-eqz v2, :cond_167

    #@166
    move v3, v4

    #@167
    :cond_167
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@16a
    goto/16 :goto_9

    #@16c
    .line 196
    .end local v1           #_arg1:Lcom/vzw/location/VzwGpsPerformance;
    .end local v2           #_result:Z
    :cond_16c
    const/4 v1, 0x0

    #@16d
    .restart local v1       #_arg1:Lcom/vzw/location/VzwGpsPerformance;
    goto :goto_15d

    #@16e
    .line 205
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Lcom/vzw/location/VzwGpsPerformance;
    :sswitch_16e
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@170
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@173
    .line 207
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@176
    move-result-object v0

    #@177
    .line 209
    .local v0, _arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17a
    move-result v1

    #@17b
    .line 210
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/vzw/location/IVzwLocationProvider$Stub;->setPde([BI)Z

    #@17e
    move-result v2

    #@17f
    .line 211
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@182
    .line 212
    if-eqz v2, :cond_185

    #@184
    move v3, v4

    #@185
    :cond_185
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@188
    goto/16 :goto_9

    #@18a
    .line 217
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:I
    .end local v2           #_result:Z
    :sswitch_18a
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@18c
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18f
    .line 219
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@192
    move-result v0

    #@193
    .line 220
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->startFix(I)Z

    #@196
    move-result v2

    #@197
    .line 221
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@19a
    .line 222
    if-eqz v2, :cond_19d

    #@19c
    move v3, v4

    #@19d
    :cond_19d
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1a0
    goto/16 :goto_9

    #@1a2
    .line 227
    .end local v0           #_arg0:I
    .end local v2           #_result:Z
    :sswitch_1a2
    const-string v5, "com.vzw.location.IVzwLocationProvider"

    #@1a4
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a7
    .line 228
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationProvider$Stub;->cancelFix()Z

    #@1aa
    move-result v2

    #@1ab
    .line 229
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ae
    .line 230
    if-eqz v2, :cond_1b1

    #@1b0
    move v3, v4

    #@1b1
    :cond_1b1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b4
    goto/16 :goto_9

    #@1b6
    .line 43
    :sswitch_data_1b6
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_23
        0x3 -> :sswitch_36
        0x4 -> :sswitch_49
        0x5 -> :sswitch_5c
        0x6 -> :sswitch_6f
        0x7 -> :sswitch_82
        0x8 -> :sswitch_96
        0x9 -> :sswitch_a7
        0xa -> :sswitch_b8
        0xb -> :sswitch_c5
        0xc -> :sswitch_d2
        0xd -> :sswitch_e6
        0xe -> :sswitch_11d
        0xf -> :sswitch_12e
        0x10 -> :sswitch_146
        0x11 -> :sswitch_16e
        0x12 -> :sswitch_18a
        0x13 -> :sswitch_1a2
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
