.class Lcom/vzw/location/ILocationListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "ILocationListener.java"

# interfaces
.implements Lcom/vzw/location/ILocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/ILocationListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 97
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 99
    iput-object p1, p0, Lcom/vzw/location/ILocationListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 100
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/vzw/location/ILocationListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 107
    const-string v0, "com.vzw.location.ILocationListener"

    #@2
    return-object v0
.end method

.method public onLocationChanged(Lcom/vzw/location/VzwLocation;)V
    .registers 7
    .parameter "location"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 111
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 113
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.ILocationListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 114
    if-eqz p1, :cond_1f

    #@b
    .line 115
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 116
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Lcom/vzw/location/VzwLocation;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 121
    :goto_13
    iget-object v1, p0, Lcom/vzw/location/ILocationListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x1

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_24

    #@1b
    .line 124
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 126
    return-void

    #@1f
    .line 119
    :cond_1f
    const/4 v1, 0x0

    #@20
    :try_start_20
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 123
    :catchall_24
    move-exception v1

    #@25
    .line 124
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 125
    throw v1
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 7
    .parameter "provider"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 163
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.ILocationListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 164
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 165
    iget-object v1, p0, Lcom/vzw/location/ILocationListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x4

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 168
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 170
    return-void

    #@18
    .line 167
    :catchall_18
    move-exception v1

    #@19
    .line 168
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    .line 169
    throw v1
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 7
    .parameter "provider"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 149
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 151
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.ILocationListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 152
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 153
    iget-object v1, p0, Lcom/vzw/location/ILocationListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x3

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 156
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 158
    return-void

    #@18
    .line 155
    :catchall_18
    move-exception v1

    #@19
    .line 156
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    .line 157
    throw v1
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 9
    .parameter "provider"
    .parameter "status"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 131
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.ILocationListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 132
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 133
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 134
    if-eqz p3, :cond_25

    #@11
    .line 135
    const/4 v1, 0x1

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 136
    const/4 v1, 0x0

    #@16
    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 141
    :goto_19
    iget-object v1, p0, Lcom/vzw/location/ILocationListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v2, 0x2

    #@1c
    const/4 v3, 0x0

    #@1d
    const/4 v4, 0x1

    #@1e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_21
    .catchall {:try_start_4 .. :try_end_21} :catchall_2a

    #@21
    .line 144
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 146
    return-void

    #@25
    .line 139
    :cond_25
    const/4 v1, 0x0

    #@26
    :try_start_26
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_2a

    #@29
    goto :goto_19

    #@2a
    .line 143
    :catchall_2a
    move-exception v1

    #@2b
    .line 144
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 145
    throw v1
.end method
