.class Lcom/vzw/location/VzwLocation$1;
.super Ljava/lang/Object;
.source "VzwLocation.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/vzw/location/VzwLocation;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 465
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwLocation;
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    .line 467
    new-instance v0, Lcom/vzw/location/VzwLocation;

    #@2
    sget-object v1, Landroid/location/Location;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/location/Location;

    #@a
    const/4 v2, 0x0

    #@b
    invoke-direct {v0, v1, v2}, Lcom/vzw/location/VzwLocation;-><init>(Landroid/location/Location;Lcom/vzw/location/VzwLocation;)V

    #@e
    .line 468
    .local v0, vz:Lcom/vzw/location/VzwLocation;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v1

    #@12
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$1(Lcom/vzw/location/VzwLocation;I)V

    #@15
    .line 469
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@18
    move-result v1

    #@19
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$2(Lcom/vzw/location/VzwLocation;F)V

    #@1c
    .line 470
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@1f
    move-result v1

    #@20
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$3(Lcom/vzw/location/VzwLocation;F)V

    #@23
    .line 471
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@26
    move-result v1

    #@27
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$4(Lcom/vzw/location/VzwLocation;F)V

    #@2a
    .line 472
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@2d
    move-result v1

    #@2e
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$5(Lcom/vzw/location/VzwLocation;F)V

    #@31
    .line 473
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@34
    move-result v1

    #@35
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$6(Lcom/vzw/location/VzwLocation;F)V

    #@38
    .line 474
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@3b
    move-result v1

    #@3c
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$7(Lcom/vzw/location/VzwLocation;F)V

    #@3f
    .line 475
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@42
    move-result v1

    #@43
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$8(Lcom/vzw/location/VzwLocation;F)V

    #@46
    .line 476
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@49
    move-result v1

    #@4a
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$9(Lcom/vzw/location/VzwLocation;F)V

    #@4d
    .line 477
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@50
    move-result v1

    #@51
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$10(Lcom/vzw/location/VzwLocation;F)V

    #@54
    .line 478
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@57
    move-result v1

    #@58
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$11(Lcom/vzw/location/VzwLocation;F)V

    #@5b
    .line 479
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5e
    move-result v1

    #@5f
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$12(Lcom/vzw/location/VzwLocation;I)V

    #@62
    .line 480
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@65
    move-result v1

    #@66
    invoke-static {v0, v1}, Lcom/vzw/location/VzwLocation;->access$13(Lcom/vzw/location/VzwLocation;F)V

    #@69
    .line 481
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwLocation$1;->createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwLocation;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/vzw/location/VzwLocation;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 485
    new-array v0, p1, [Lcom/vzw/location/VzwLocation;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwLocation$1;->newArray(I)[Lcom/vzw/location/VzwLocation;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
