.class public Lcom/vzw/location/VzwGpsDeviceStatus;
.super Ljava/lang/Object;
.source "VzwGpsDeviceStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vzw/location/VzwGpsDeviceStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final HW_STATE_IDLE:I = 0x2

.field public static final HW_STATE_ON:I = 0x1

.field public static final HW_STATE_UNKNOWN:I = 0x0

.field public static final VALID_ALM_SV_MASK:I = 0x4

.field public static final VALID_DEV_ERROR:I = 0x20

.field public static final VALID_EPH_SV_MASK:I = 0x2

.field public static final VALID_HW_STATE:I = 0x1

.field public static final VALID_SAT_IN_VIEW_CARRIER_TO_NOISE_RATIO:I = 0x10

.field public static final VALID_SAT_IN_VIEW_PRN:I = 0x8


# instance fields
.field private mHwState:I

.field private mSatelliteCount:I

.field private mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

.field private mValidFields:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 224
    new-instance v0, Lcom/vzw/location/VzwGpsDeviceStatus$1;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsDeviceStatus$1;-><init>()V

    #@5
    sput-object v0, Lcom/vzw/location/VzwGpsDeviceStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 15
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    const/16 v1, 0x40

    #@5
    new-array v1, v1, [Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@7
    iput-object v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@9
    .line 59
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    iget-object v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@c
    array-length v1, v1

    #@d
    if-lt v0, v1, :cond_10

    #@f
    .line 62
    return-void

    #@10
    .line 60
    :cond_10
    iget-object v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@12
    new-instance v2, Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@14
    invoke-direct {v2}, Lcom/vzw/location/VzwGpsSatelliteStatus;-><init>()V

    #@17
    aput-object v2, v1, v0

    #@19
    .line 59
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_a
.end method

.method constructor <init>(Lcom/vzw/location/VzwGpsDeviceStatus;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    const/16 v0, 0x40

    #@5
    new-array v0, v0, [Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@7
    iput-object v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@9
    .line 65
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsDeviceStatus;->set(Lcom/vzw/location/VzwGpsDeviceStatus;)V

    #@c
    .line 66
    return-void
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwGpsDeviceStatus;I)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 27
    iput p1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@2
    return-void
.end method

.method static synthetic access$1(Lcom/vzw/location/VzwGpsDeviceStatus;I)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 28
    iput p1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mHwState:I

    #@2
    return-void
.end method

.method static synthetic access$2(Lcom/vzw/location/VzwGpsDeviceStatus;I)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 30
    iput p1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@2
    return-void
.end method

.method static synthetic access$3(Lcom/vzw/location/VzwGpsDeviceStatus;)[Lcom/vzw/location/VzwGpsSatelliteStatus;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 29
    iget-object v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@2
    return-object v0
.end method

.method static synthetic access$4(Lcom/vzw/location/VzwGpsDeviceStatus;)I
    .registers 2
    .parameter

    #@0
    .prologue
    .line 30
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@2
    return v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 214
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getHwState()I
    .registers 2

    #@0
    .prologue
    .line 97
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mHwState:I

    #@2
    return v0
.end method

.method public getMaximumPossibleSatelliteCount()I
    .registers 2

    #@0
    .prologue
    .line 182
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@2
    return v0
.end method

.method public getValidFields()I
    .registers 2

    #@0
    .prologue
    .line 89
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@2
    return v0
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 81
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@3
    .line 82
    return-void
.end method

.method public satellites()Ljava/lang/Iterable;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/vzw/location/VzwGpsSatelliteStatus;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 168
    new-instance v0, Lcom/vzw/location/VzwGpsDeviceStatus$2;

    #@2
    invoke-direct {v0, p0}, Lcom/vzw/location/VzwGpsDeviceStatus$2;-><init>(Lcom/vzw/location/VzwGpsDeviceStatus;)V

    #@5
    return-object v0
.end method

.method declared-synchronized set(Lcom/vzw/location/VzwGpsDeviceStatus;)V
    .registers 5
    .parameter "other"

    #@0
    .prologue
    .line 69
    monitor-enter p0

    #@1
    :try_start_1
    iget v1, p1, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@3
    iput v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@5
    .line 70
    iget v1, p1, Lcom/vzw/location/VzwGpsDeviceStatus;->mHwState:I

    #@7
    iput v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mHwState:I

    #@9
    .line 71
    iget v1, p1, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@b
    iput v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@d
    .line 72
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    iget v1, p1, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_22

    #@10
    if-lt v0, v1, :cond_14

    #@12
    .line 75
    monitor-exit p0

    #@13
    return-void

    #@14
    .line 73
    :cond_14
    :try_start_14
    iget-object v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@16
    aget-object v1, v1, v0

    #@18
    iget-object v2, p1, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@1a
    aget-object v2, v2, v0

    #@1c
    invoke-virtual {v1, v2}, Lcom/vzw/location/VzwGpsSatelliteStatus;->setStatus(Lcom/vzw/location/VzwGpsSatelliteStatus;)V
    :try_end_1f
    .catchall {:try_start_14 .. :try_end_1f} :catchall_22

    #@1f
    .line 72
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_e

    #@22
    .line 69
    .end local v0           #i:I
    :catchall_22
    move-exception v1

    #@23
    monitor-exit p0

    #@24
    throw v1
.end method

.method public setHwState(I)V
    .registers 3
    .parameter "hwState"

    #@0
    .prologue
    .line 101
    iput p1, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mHwState:I

    #@2
    .line 102
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@4
    or-int/lit8 v0, v0, 0x1

    #@6
    iput v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@8
    .line 103
    return-void
.end method

.method public declared-synchronized setSatelliteStatus([I[IJJ)V
    .registers 17
    .parameter "svInViewPrns"
    .parameter "svInViewCnr"
    .parameter "ephSVMask"
    .parameter "almSVMask"

    #@0
    .prologue
    .line 111
    monitor-enter p0

    #@1
    :try_start_1
    array-length v6, p1

    #@2
    array-length v7, p2

    #@3
    if-ge v6, v7, :cond_14

    #@5
    array-length v5, p1

    #@6
    .line 115
    .local v5, toIterate:I
    :goto_6
    const/4 v6, 0x0

    #@7
    iput v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@9
    .line 117
    const/4 v4, 0x0

    #@a
    .local v4, i:I
    :goto_a
    if-lt v4, v5, :cond_16

    #@c
    .line 131
    iget v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@e
    or-int/lit8 v6, v6, 0x1e

    #@10
    iput v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_62

    #@12
    .line 132
    monitor-exit p0

    #@13
    return-void

    #@14
    .line 111
    .end local v4           #i:I
    .end local v5           #toIterate:I
    :cond_14
    :try_start_14
    array-length v5, p2

    #@15
    goto :goto_6

    #@16
    .line 118
    .restart local v4       #i:I
    .restart local v5       #toIterate:I
    :cond_16
    const-wide/16 v6, 0x1

    #@18
    shl-long v0, v6, v4

    #@1a
    .line 119
    .local v0, flag:J
    and-long v6, p3, v0

    #@1c
    const-wide/16 v8, 0x0

    #@1e
    cmp-long v6, v6, v8

    #@20
    if-lez v6, :cond_5e

    #@22
    const/4 v3, 0x1

    #@23
    .line 120
    .local v3, hasEphemeris:Z
    :goto_23
    and-long v6, p5, v0

    #@25
    const-wide/16 v8, 0x0

    #@27
    cmp-long v6, v6, v8

    #@29
    if-lez v6, :cond_60

    #@2b
    const/4 v2, 0x1

    #@2c
    .line 122
    .local v2, hasAlmanac:Z
    :goto_2c
    if-nez v3, :cond_30

    #@2e
    if-eqz v2, :cond_5b

    #@30
    .line 123
    :cond_30
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@32
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@34
    aget-object v6, v6, v7

    #@36
    aget v7, p1, v4

    #@38
    iput v7, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@3a
    .line 124
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@3c
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@3e
    aget-object v6, v6, v7

    #@40
    aget v7, p2, v4

    #@42
    int-to-float v7, v7

    #@43
    iput v7, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@45
    .line 125
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@47
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@49
    aget-object v6, v6, v7

    #@4b
    iput-boolean v3, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@4d
    .line 126
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@4f
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@51
    aget-object v6, v6, v7

    #@53
    iput-boolean v2, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@55
    .line 127
    iget v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@57
    add-int/lit8 v6, v6, 0x1

    #@59
    iput v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I
    :try_end_5b
    .catchall {:try_start_14 .. :try_end_5b} :catchall_62

    #@5b
    .line 117
    :cond_5b
    add-int/lit8 v4, v4, 0x1

    #@5d
    goto :goto_a

    #@5e
    .line 119
    .end local v2           #hasAlmanac:Z
    .end local v3           #hasEphemeris:Z
    :cond_5e
    const/4 v3, 0x0

    #@5f
    goto :goto_23

    #@60
    .line 120
    .restart local v3       #hasEphemeris:Z
    :cond_60
    const/4 v2, 0x0

    #@61
    goto :goto_2c

    #@62
    .line 111
    .end local v0           #flag:J
    .end local v3           #hasEphemeris:Z
    .end local v4           #i:I
    .end local v5           #toIterate:I
    :catchall_62
    move-exception v6

    #@63
    monitor-exit p0

    #@64
    throw v6
.end method

.method public declared-synchronized setStatus([I[FJJ)V
    .registers 17
    .parameter "svInViewPrns"
    .parameter "svInViewCnr"
    .parameter "ephSVMask"
    .parameter "almSVMask"

    #@0
    .prologue
    .line 140
    monitor-enter p0

    #@1
    :try_start_1
    array-length v6, p1

    #@2
    array-length v7, p2

    #@3
    if-ge v6, v7, :cond_14

    #@5
    array-length v5, p1

    #@6
    .line 144
    .local v5, toIterate:I
    :goto_6
    const/4 v6, 0x0

    #@7
    iput v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@9
    .line 145
    const/4 v4, 0x0

    #@a
    .local v4, i:I
    :goto_a
    if-lt v4, v5, :cond_16

    #@c
    .line 158
    iget v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@e
    or-int/lit8 v6, v6, 0x1e

    #@10
    iput v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_61

    #@12
    .line 159
    monitor-exit p0

    #@13
    return-void

    #@14
    .line 140
    .end local v4           #i:I
    .end local v5           #toIterate:I
    :cond_14
    :try_start_14
    array-length v5, p2

    #@15
    goto :goto_6

    #@16
    .line 146
    .restart local v4       #i:I
    .restart local v5       #toIterate:I
    :cond_16
    const-wide/16 v6, 0x1

    #@18
    shl-long v0, v6, v4

    #@1a
    .line 147
    .local v0, flag:J
    and-long v6, p3, v0

    #@1c
    const-wide/16 v8, 0x0

    #@1e
    cmp-long v6, v6, v8

    #@20
    if-lez v6, :cond_5d

    #@22
    const/4 v3, 0x1

    #@23
    .line 148
    .local v3, hasEphemeris:Z
    :goto_23
    and-long v6, p5, v0

    #@25
    const-wide/16 v8, 0x0

    #@27
    cmp-long v6, v6, v8

    #@29
    if-lez v6, :cond_5f

    #@2b
    const/4 v2, 0x1

    #@2c
    .line 149
    .local v2, hasAlmanac:Z
    :goto_2c
    if-nez v3, :cond_30

    #@2e
    if-eqz v2, :cond_5a

    #@30
    .line 150
    :cond_30
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@32
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@34
    aget-object v6, v6, v7

    #@36
    aget v7, p1, v4

    #@38
    iput v7, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@3a
    .line 151
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@3c
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@3e
    aget-object v6, v6, v7

    #@40
    aget v7, p2, v4

    #@42
    iput v7, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@44
    .line 152
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@46
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@48
    aget-object v6, v6, v7

    #@4a
    iput-boolean v3, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@4c
    .line 153
    iget-object v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@4e
    iget v7, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@50
    aget-object v6, v6, v7

    #@52
    iput-boolean v2, v6, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@54
    .line 154
    iget v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@56
    add-int/lit8 v6, v6, 0x1

    #@58
    iput v6, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I
    :try_end_5a
    .catchall {:try_start_14 .. :try_end_5a} :catchall_61

    #@5a
    .line 145
    :cond_5a
    add-int/lit8 v4, v4, 0x1

    #@5c
    goto :goto_a

    #@5d
    .line 147
    .end local v2           #hasAlmanac:Z
    .end local v3           #hasEphemeris:Z
    :cond_5d
    const/4 v3, 0x0

    #@5e
    goto :goto_23

    #@5f
    .line 148
    .restart local v3       #hasEphemeris:Z
    :cond_5f
    const/4 v2, 0x0

    #@60
    goto :goto_2c

    #@61
    .line 140
    .end local v0           #flag:J
    .end local v3           #hasEphemeris:Z
    .end local v4           #i:I
    .end local v5           #toIterate:I
    :catchall_61
    move-exception v6

    #@62
    monitor-exit p0

    #@63
    throw v6
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 189
    .local v0, builder:Ljava/lang/StringBuilder;
    iget v2, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@7
    and-int/lit8 v2, v2, 0x1

    #@9
    if-lez v2, :cond_1a

    #@b
    .line 190
    const-string v2, "Hw state: "

    #@d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 191
    iget v2, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mHwState:I

    #@12
    packed-switch v2, :pswitch_data_4e

    #@15
    .line 199
    const-string v2, "Unknown"

    #@17
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 203
    :cond_1a
    :goto_1a
    iget v2, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@1c
    and-int/lit8 v2, v2, 0x1e

    #@1e
    if-lez v2, :cond_2e

    #@20
    .line 204
    invoke-virtual {p0}, Lcom/vzw/location/VzwGpsDeviceStatus;->satellites()Ljava/lang/Iterable;

    #@23
    move-result-object v2

    #@24
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v2

    #@28
    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2b
    move-result v3

    #@2c
    if-nez v3, :cond_3f

    #@2e
    .line 210
    :cond_2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    return-object v2

    #@33
    .line 193
    :pswitch_33
    const-string v2, "On"

    #@35
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    goto :goto_1a

    #@39
    .line 196
    :pswitch_39
    const-string v2, "Idle"

    #@3b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    goto :goto_1a

    #@3f
    .line 204
    :cond_3f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@42
    move-result-object v1

    #@43
    check-cast v1, Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@45
    .line 205
    .local v1, satellite:Lcom/vzw/location/VzwGpsSatelliteStatus;
    const-string v3, " Satellite: "

    #@47
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    .line 206
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d
    goto :goto_28

    #@4e
    .line 191
    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_33
        :pswitch_39
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 218
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mValidFields:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 219
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mHwState:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 220
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatelliteCount:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 221
    iget-object v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@11
    const/4 v1, 0x0

    #@12
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@15
    .line 222
    return-void
.end method
