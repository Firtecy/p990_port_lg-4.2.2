.class final Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;
.super Ljava/lang/Object;
.source "VzwGpsDeviceStatus.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwGpsDeviceStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SatelliteIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/vzw/location/VzwGpsSatelliteStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCount:I

.field private mIndex:I

.field private final mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

.field final synthetic this$0:Lcom/vzw/location/VzwGpsDeviceStatus;


# direct methods
.method constructor <init>(Lcom/vzw/location/VzwGpsDeviceStatus;[Lcom/vzw/location/VzwGpsSatelliteStatus;I)V
    .registers 5
    .parameter
    .parameter "satellites"
    .parameter "count"

    #@0
    .prologue
    .line 40
    iput-object p1, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->this$0:Lcom/vzw/location/VzwGpsDeviceStatus;

    #@2
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 35
    const/4 v0, 0x0

    #@6
    iput v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mIndex:I

    #@8
    .line 38
    iput-object p2, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@a
    .line 39
    iput p3, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mCount:I

    #@c
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .registers 3

    #@0
    .prologue
    .line 43
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mIndex:I

    #@2
    iget v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mCount:I

    #@4
    if-ge v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public next()Lcom/vzw/location/VzwGpsSatelliteStatus;
    .registers 4

    #@0
    .prologue
    .line 47
    iget v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mIndex:I

    #@2
    iget v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mCount:I

    #@4
    if-ge v0, v1, :cond_11

    #@6
    .line 48
    iget-object v0, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mSatellites:[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@8
    iget v1, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mIndex:I

    #@a
    add-int/lit8 v2, v1, 0x1

    #@c
    iput v2, p0, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->mIndex:I

    #@e
    aget-object v0, v0, v1

    #@10
    return-object v0

    #@11
    .line 50
    :cond_11
    new-instance v0, Ljava/util/NoSuchElementException;

    #@13
    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    #@16
    throw v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/vzw/location/VzwGpsDeviceStatus$SatelliteIterator;->next()Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public remove()V
    .registers 2

    #@0
    .prologue
    .line 54
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method
