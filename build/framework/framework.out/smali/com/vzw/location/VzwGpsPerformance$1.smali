.class Lcom/vzw/location/VzwGpsPerformance$1;
.super Ljava/lang/Object;
.source "VzwGpsPerformance.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwGpsPerformance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/vzw/location/VzwGpsPerformance;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsPerformance;
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    .line 85
    new-instance v0, Lcom/vzw/location/VzwGpsPerformance;

    #@2
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsPerformance;-><init>()V

    #@5
    .line 86
    .local v0, vz:Lcom/vzw/location/VzwGpsPerformance;
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@8
    move-result-wide v1

    #@9
    invoke-static {v0, v1, v2}, Lcom/vzw/location/VzwGpsPerformance;->access$0(Lcom/vzw/location/VzwGpsPerformance;J)V

    #@c
    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@f
    move-result-wide v1

    #@10
    invoke-static {v0, v1, v2}, Lcom/vzw/location/VzwGpsPerformance;->access$1(Lcom/vzw/location/VzwGpsPerformance;J)V

    #@13
    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@16
    move-result-wide v1

    #@17
    invoke-static {v0, v1, v2}, Lcom/vzw/location/VzwGpsPerformance;->access$2(Lcom/vzw/location/VzwGpsPerformance;J)V

    #@1a
    .line 89
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsPerformance$1;->createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsPerformance;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/vzw/location/VzwGpsPerformance;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 93
    new-array v0, p1, [Lcom/vzw/location/VzwGpsPerformance;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsPerformance$1;->newArray(I)[Lcom/vzw/location/VzwGpsPerformance;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
