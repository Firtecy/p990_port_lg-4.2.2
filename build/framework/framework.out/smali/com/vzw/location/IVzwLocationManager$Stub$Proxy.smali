.class Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IVzwLocationManager.java"

# interfaces
.implements Lcom/vzw/location/IVzwLocationManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/IVzwLocationManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 190
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 192
    iput-object p1, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 193
    return-void
.end method


# virtual methods
.method public addGpsStatusListener(Lcom/vzw/location/IVzwGpsStatusListener;)Z
    .registers 8
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 278
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 279
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 282
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 283
    if-eqz p1, :cond_2f

    #@10
    invoke-interface {p1}, Lcom/vzw/location/IVzwGpsStatusListener;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 284
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x5

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 285
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 286
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_9 .. :try_end_24} :catchall_31

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_28

    #@27
    const/4 v2, 0x1

    #@28
    .line 289
    .local v2, _result:Z
    :cond_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 290
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 292
    return v2

    #@2f
    .line 283
    .end local v2           #_result:Z
    :cond_2f
    const/4 v3, 0x0

    #@30
    goto :goto_14

    #@31
    .line 288
    :catchall_31
    move-exception v3

    #@32
    .line 289
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 290
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 291
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 196
    iget-object v0, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getAllProviders()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 204
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 205
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 208
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.vzw.location.IVzwLocationManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 209
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x1

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 210
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 211
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result-object v2

    #@1b
    .line 214
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 217
    return-object v2

    #@22
    .line 213
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :catchall_22
    move-exception v3

    #@23
    .line 214
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 216
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 200
    const-string v0, "com.vzw.location.IVzwLocationManager"

    #@2
    return-object v0
.end method

.method public getProviderInfo(Ljava/lang/String;)Landroid/os/Bundle;
    .registers 8
    .parameter "provider"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 340
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 341
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 344
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.vzw.location.IVzwLocationManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 345
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 346
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x8

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 347
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 348
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_30

    #@21
    .line 349
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@23
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@26
    move-result-object v2

    #@27
    check-cast v2, Landroid/os/Bundle;
    :try_end_29
    .catchall {:try_start_8 .. :try_end_29} :catchall_32

    #@29
    .line 356
    .local v2, _result:Landroid/os/Bundle;
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 357
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 359
    return-object v2

    #@30
    .line 352
    .end local v2           #_result:Landroid/os/Bundle;
    :cond_30
    const/4 v2, 0x0

    #@31
    .restart local v2       #_result:Landroid/os/Bundle;
    goto :goto_29

    #@32
    .line 355
    .end local v2           #_result:Landroid/os/Bundle;
    :catchall_32
    move-exception v3

    #@33
    .line 356
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 357
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 358
    throw v3
.end method

.method public getProviders(Z)Ljava/util/List;
    .registers 8
    .parameter "enabledOnly"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 221
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 222
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 225
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v4, "com.vzw.location.IVzwLocationManager"

    #@b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 226
    if-eqz p1, :cond_11

    #@10
    const/4 v3, 0x1

    #@11
    :cond_11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 227
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v4, 0x2

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 228
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 229
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;
    :try_end_21
    .catchall {:try_start_9 .. :try_end_21} :catchall_29

    #@21
    move-result-object v2

    #@22
    .line 232
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 233
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 235
    return-object v2

    #@29
    .line 231
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :catchall_29
    move-exception v3

    #@2a
    .line 232
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 233
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 234
    throw v3
.end method

.method public isProviderEnabled(Ljava/lang/String;)Z
    .registers 8
    .parameter "provider"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 363
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 364
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 367
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.vzw.location.IVzwLocationManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 368
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 369
    iget-object v3, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x9

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 370
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 371
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 374
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 375
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 377
    return v2

    #@2a
    .line 373
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    .line 374
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 375
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 376
    throw v3
.end method

.method public removeGpsStatusListener(Lcom/vzw/location/IVzwGpsStatusListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 296
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 297
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 299
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.vzw.location.IVzwLocationManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 300
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/vzw/location/IVzwGpsStatusListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 301
    iget-object v2, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x6

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 302
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 305
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 306
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 308
    return-void

    #@27
    .line 300
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 304
    :catchall_29
    move-exception v2

    #@2a
    .line 305
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 306
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 307
    throw v2
.end method

.method public removeUpdates(Lcom/vzw/location/ILocationListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 263
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 264
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 266
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.vzw.location.IVzwLocationManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 267
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/vzw/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 268
    iget-object v2, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x4

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 269
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 272
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 273
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 275
    return-void

    #@27
    .line 267
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 271
    :catchall_29
    move-exception v2

    #@2a
    .line 272
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 273
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 274
    throw v2
.end method

.method public requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Lcom/vzw/location/ILocationListener;)V
    .registers 10
    .parameter "provider"
    .parameter "isSingleFix"
    .parameter "criteria"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 239
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 240
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 242
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.vzw.location.IVzwLocationManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 243
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 244
    if-eqz p2, :cond_3b

    #@14
    :goto_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 245
    if-eqz p3, :cond_3d

    #@19
    .line 246
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 247
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p3, v0, v2}, Lcom/vzw/location/VzwCriteria;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 252
    :goto_21
    if-eqz p4, :cond_4a

    #@23
    invoke-interface {p4}, Lcom/vzw/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    #@26
    move-result-object v2

    #@27
    :goto_27
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2a
    .line 253
    iget-object v2, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2c
    const/4 v3, 0x3

    #@2d
    const/4 v4, 0x0

    #@2e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@31
    .line 254
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_34
    .catchall {:try_start_a .. :try_end_34} :catchall_42

    #@34
    .line 257
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 258
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 260
    return-void

    #@3b
    :cond_3b
    move v2, v3

    #@3c
    .line 244
    goto :goto_14

    #@3d
    .line 250
    :cond_3d
    const/4 v2, 0x0

    #@3e
    :try_start_3e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_41
    .catchall {:try_start_3e .. :try_end_41} :catchall_42

    #@41
    goto :goto_21

    #@42
    .line 256
    :catchall_42
    move-exception v2

    #@43
    .line 257
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 258
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    .line 259
    throw v2

    #@4a
    .line 252
    :cond_4a
    const/4 v2, 0x0

    #@4b
    goto :goto_27
.end method

.method public sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 11
    .parameter "provider"
    .parameter "command"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 311
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 312
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 315
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.vzw.location.IVzwLocationManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 316
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 317
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    .line 318
    if-eqz p3, :cond_3f

    #@17
    .line 319
    const/4 v4, 0x1

    #@18
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 320
    const/4 v4, 0x0

    #@1c
    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1f
    .line 325
    :goto_1f
    iget-object v4, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/4 v5, 0x7

    #@22
    const/4 v6, 0x0

    #@23
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@26
    .line 326
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@29
    .line 327
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_4c

    #@2f
    .line 328
    .local v2, _result:Z
    :goto_2f
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_38

    #@35
    .line 329
    invoke-virtual {p3, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_38
    .catchall {:try_start_a .. :try_end_38} :catchall_44

    #@38
    .line 333
    :cond_38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 334
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 336
    return v2

    #@3f
    .line 323
    .end local v2           #_result:Z
    :cond_3f
    const/4 v4, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_44

    #@43
    goto :goto_1f

    #@44
    .line 332
    :catchall_44
    move-exception v3

    #@45
    .line 333
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 334
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4b
    .line 335
    throw v3

    #@4c
    :cond_4c
    move v2, v3

    #@4d
    .line 327
    goto :goto_2f
.end method

.method public setConfigInit(Ljava/lang/String;Lcom/vzw/location/VzwGpsConfigInit;)Z
    .registers 10
    .parameter "provider"
    .parameter "configInit"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 381
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 382
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 385
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.vzw.location.IVzwLocationManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 386
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 387
    if-eqz p2, :cond_34

    #@14
    .line 388
    const/4 v4, 0x1

    #@15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 389
    const/4 v4, 0x0

    #@19
    invoke-virtual {p2, v0, v4}, Lcom/vzw/location/VzwGpsConfigInit;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c
    .line 394
    :goto_1c
    iget-object v4, p0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0xa

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 395
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 396
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2a
    .catchall {:try_start_a .. :try_end_2a} :catchall_39

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_41

    #@2d
    .line 399
    .local v2, _result:Z
    :goto_2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 400
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 402
    return v2

    #@34
    .line 392
    .end local v2           #_result:Z
    :cond_34
    const/4 v4, 0x0

    #@35
    :try_start_35
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_39

    #@38
    goto :goto_1c

    #@39
    .line 398
    :catchall_39
    move-exception v3

    #@3a
    .line 399
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 400
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 401
    throw v3

    #@41
    :cond_41
    move v2, v3

    #@42
    .line 396
    goto :goto_2d
.end method
