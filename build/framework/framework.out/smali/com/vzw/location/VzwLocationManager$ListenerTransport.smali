.class Lcom/vzw/location/VzwLocationManager$ListenerTransport;
.super Lcom/vzw/location/ILocationListener$Stub;
.source "VzwLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenerTransport"
.end annotation


# static fields
.field private static final TYPE_LOCATION_CHANGED:I = 0x1

.field private static final TYPE_PROVIDER_DISABLED:I = 0x4

.field private static final TYPE_PROVIDER_ENABLED:I = 0x3

.field private static final TYPE_STATUS_CHANGED:I = 0x2


# instance fields
.field private mListener:Landroid/location/LocationListener;

.field private final mListenerHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/vzw/location/VzwLocationManager;


# direct methods
.method constructor <init>(Lcom/vzw/location/VzwLocationManager;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .registers 5
    .parameter
    .parameter "listener"
    .parameter "looper"

    #@0
    .prologue
    .line 69
    iput-object p1, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->this$0:Lcom/vzw/location/VzwLocationManager;

    #@2
    .line 51
    invoke-direct {p0}, Lcom/vzw/location/ILocationListener$Stub;-><init>()V

    #@5
    .line 52
    iput-object p2, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@7
    .line 54
    if-nez p3, :cond_11

    #@9
    .line 55
    new-instance v0, Lcom/vzw/location/VzwLocationManager$ListenerTransport$1;

    #@b
    invoke-direct {v0, p0}, Lcom/vzw/location/VzwLocationManager$ListenerTransport$1;-><init>(Lcom/vzw/location/VzwLocationManager$ListenerTransport;)V

    #@e
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@10
    .line 62
    :goto_10
    return-void

    #@11
    :cond_11
    new-instance v0, Lcom/vzw/location/VzwLocationManager$ListenerTransport$2;

    #@13
    invoke-direct {v0, p0, p3}, Lcom/vzw/location/VzwLocationManager$ListenerTransport$2;-><init>(Lcom/vzw/location/VzwLocationManager$ListenerTransport;Landroid/os/Looper;)V

    #@16
    iput-object v0, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@18
    goto :goto_10
.end method

.method private _handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 115
    iget v5, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v5, :pswitch_data_48

    #@5
    .line 134
    :goto_5
    return-void

    #@6
    .line 117
    :pswitch_6
    new-instance v2, Lcom/vzw/location/VzwLocation;

    #@8
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    check-cast v5, Lcom/vzw/location/VzwLocation;

    #@c
    invoke-direct {v2, v5}, Lcom/vzw/location/VzwLocation;-><init>(Lcom/vzw/location/VzwLocation;)V

    #@f
    .line 118
    .local v2, location:Lcom/vzw/location/VzwLocation;
    iget-object v5, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@11
    invoke-interface {v5, v2}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    #@14
    goto :goto_5

    #@15
    .line 121
    .end local v2           #location:Lcom/vzw/location/VzwLocation;
    :pswitch_15
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@17
    check-cast v0, Landroid/os/Bundle;

    #@19
    .line 122
    .local v0, b:Landroid/os/Bundle;
    const-string/jumbo v5, "provider"

    #@1c
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 123
    .local v3, provider:Ljava/lang/String;
    const-string/jumbo v5, "status"

    #@23
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@26
    move-result v4

    #@27
    .line 124
    .local v4, status:I
    const-string v5, "extras"

    #@29
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@2c
    move-result-object v1

    #@2d
    .line 125
    .local v1, extras:Landroid/os/Bundle;
    iget-object v5, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@2f
    invoke-interface {v5, v3, v4, v1}, Landroid/location/LocationListener;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    #@32
    goto :goto_5

    #@33
    .line 128
    .end local v0           #b:Landroid/os/Bundle;
    .end local v1           #extras:Landroid/os/Bundle;
    .end local v3           #provider:Ljava/lang/String;
    .end local v4           #status:I
    :pswitch_33
    iget-object v6, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@35
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@37
    check-cast v5, Ljava/lang/String;

    #@39
    invoke-interface {v6, v5}, Landroid/location/LocationListener;->onProviderEnabled(Ljava/lang/String;)V

    #@3c
    goto :goto_5

    #@3d
    .line 131
    :pswitch_3d
    iget-object v6, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@3f
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@41
    check-cast v5, Ljava/lang/String;

    #@43
    invoke-interface {v6, v5}, Landroid/location/LocationListener;->onProviderDisabled(Ljava/lang/String;)V

    #@46
    goto :goto_5

    #@47
    .line 115
    nop

    #@48
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_6
        :pswitch_15
        :pswitch_33
        :pswitch_3d
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/vzw/location/VzwLocationManager$ListenerTransport;Landroid/os/Message;)V
    .registers 2
    .parameter
    .parameter

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->_handleMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method


# virtual methods
.method public onLocationChanged(Lcom/vzw/location/VzwLocation;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    .line 75
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 76
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@5
    iput v1, v0, Landroid/os/Message;->what:I

    #@7
    .line 77
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 78
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@e
    .line 79
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    .line 108
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 109
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x4

    #@5
    iput v1, v0, Landroid/os/Message;->what:I

    #@7
    .line 110
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 111
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@e
    .line 112
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    .line 99
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 100
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x3

    #@5
    iput v1, v0, Landroid/os/Message;->what:I

    #@7
    .line 101
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 102
    iget-object v1, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@e
    .line 103
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 7
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    #@0
    .prologue
    .line 84
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v1

    #@4
    .line 85
    .local v1, msg:Landroid/os/Message;
    const/4 v2, 0x2

    #@5
    iput v2, v1, Landroid/os/Message;->what:I

    #@7
    .line 86
    new-instance v0, Landroid/os/Bundle;

    #@9
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@c
    .line 87
    .local v0, b:Landroid/os/Bundle;
    const-string/jumbo v2, "provider"

    #@f
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 88
    const-string/jumbo v2, "status"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 89
    if-eqz p3, :cond_1f

    #@1a
    .line 90
    const-string v2, "extras"

    #@1c
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@1f
    .line 92
    :cond_1f
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    .line 93
    iget-object v2, p0, Lcom/vzw/location/VzwLocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@23
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@26
    .line 94
    return-void
.end method
