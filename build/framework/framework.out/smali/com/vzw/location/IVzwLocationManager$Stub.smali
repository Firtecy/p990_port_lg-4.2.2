.class public abstract Lcom/vzw/location/IVzwLocationManager$Stub;
.super Landroid/os/Binder;
.source "IVzwLocationManager.java"

# interfaces
.implements Lcom/vzw/location/IVzwLocationManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/IVzwLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.vzw.location.IVzwLocationManager"

.field static final TRANSACTION_addGpsStatusListener:I = 0x5

.field static final TRANSACTION_getAllProviders:I = 0x1

.field static final TRANSACTION_getProviderInfo:I = 0x8

.field static final TRANSACTION_getProviders:I = 0x2

.field static final TRANSACTION_isProviderEnabled:I = 0x9

.field static final TRANSACTION_removeGpsStatusListener:I = 0x6

.field static final TRANSACTION_removeUpdates:I = 0x4

.field static final TRANSACTION_requestLocationUpdates:I = 0x3

.field static final TRANSACTION_sendExtraCommand:I = 0x7

.field static final TRANSACTION_setConfigInit:I = 0xa


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.vzw.location.IVzwLocationManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/vzw/location/IVzwLocationManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/vzw/location/IVzwLocationManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.vzw.location.IVzwLocationManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/vzw/location/IVzwLocationManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/vzw/location/IVzwLocationManager;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/vzw/location/IVzwLocationManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 42
    sparse-switch p1, :sswitch_data_14a

    #@5
    .line 185
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 46
    :sswitch_a
    const-string v7, "com.vzw.location.IVzwLocationManager"

    #@c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 51
    :sswitch_10
    const-string v7, "com.vzw.location.IVzwLocationManager"

    #@12
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p0}, Lcom/vzw/location/IVzwLocationManager$Stub;->getAllProviders()Ljava/util/List;

    #@18
    move-result-object v5

    #@19
    .line 53
    .local v5, _result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 54
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@1f
    goto :goto_9

    #@20
    .line 59
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_20
    const-string v8, "com.vzw.location.IVzwLocationManager"

    #@22
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 61
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v8

    #@29
    if-eqz v8, :cond_37

    #@2b
    move v0, v6

    #@2c
    .line 62
    .local v0, _arg0:Z
    :goto_2c
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationManager$Stub;->getProviders(Z)Ljava/util/List;

    #@2f
    move-result-object v5

    #@30
    .line 63
    .restart local v5       #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@33
    .line 64
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@36
    goto :goto_9

    #@37
    .end local v0           #_arg0:Z
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_37
    move v0, v7

    #@38
    .line 61
    goto :goto_2c

    #@39
    .line 69
    :sswitch_39
    const-string v8, "com.vzw.location.IVzwLocationManager"

    #@3b
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e
    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@41
    move-result-object v0

    #@42
    .line 73
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v8

    #@46
    if-eqz v8, :cond_66

    #@48
    move v1, v6

    #@49
    .line 75
    .local v1, _arg1:Z
    :goto_49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4c
    move-result v7

    #@4d
    if-eqz v7, :cond_68

    #@4f
    .line 76
    sget-object v7, Lcom/vzw/location/VzwCriteria;->CREATOR:Landroid/os/Parcelable$Creator;

    #@51
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@54
    move-result-object v2

    #@55
    check-cast v2, Lcom/vzw/location/VzwCriteria;

    #@57
    .line 82
    .local v2, _arg2:Lcom/vzw/location/VzwCriteria;
    :goto_57
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5a
    move-result-object v7

    #@5b
    invoke-static {v7}, Lcom/vzw/location/ILocationListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/vzw/location/ILocationListener;

    #@5e
    move-result-object v3

    #@5f
    .line 83
    .local v3, _arg3:Lcom/vzw/location/ILocationListener;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vzw/location/IVzwLocationManager$Stub;->requestLocationUpdates(Ljava/lang/String;ZLcom/vzw/location/VzwCriteria;Lcom/vzw/location/ILocationListener;)V

    #@62
    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@65
    goto :goto_9

    #@66
    .end local v1           #_arg1:Z
    .end local v2           #_arg2:Lcom/vzw/location/VzwCriteria;
    .end local v3           #_arg3:Lcom/vzw/location/ILocationListener;
    :cond_66
    move v1, v7

    #@67
    .line 73
    goto :goto_49

    #@68
    .line 79
    .restart local v1       #_arg1:Z
    :cond_68
    const/4 v2, 0x0

    #@69
    .restart local v2       #_arg2:Lcom/vzw/location/VzwCriteria;
    goto :goto_57

    #@6a
    .line 89
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Z
    .end local v2           #_arg2:Lcom/vzw/location/VzwCriteria;
    :sswitch_6a
    const-string v7, "com.vzw.location.IVzwLocationManager"

    #@6c
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6f
    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@72
    move-result-object v7

    #@73
    invoke-static {v7}, Lcom/vzw/location/ILocationListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/vzw/location/ILocationListener;

    #@76
    move-result-object v0

    #@77
    .line 92
    .local v0, _arg0:Lcom/vzw/location/ILocationListener;
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationManager$Stub;->removeUpdates(Lcom/vzw/location/ILocationListener;)V

    #@7a
    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7d
    goto :goto_9

    #@7e
    .line 98
    .end local v0           #_arg0:Lcom/vzw/location/ILocationListener;
    :sswitch_7e
    const-string v8, "com.vzw.location.IVzwLocationManager"

    #@80
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@83
    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@86
    move-result-object v8

    #@87
    invoke-static {v8}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/vzw/location/IVzwGpsStatusListener;

    #@8a
    move-result-object v0

    #@8b
    .line 101
    .local v0, _arg0:Lcom/vzw/location/IVzwGpsStatusListener;
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationManager$Stub;->addGpsStatusListener(Lcom/vzw/location/IVzwGpsStatusListener;)Z

    #@8e
    move-result v4

    #@8f
    .line 102
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@92
    .line 103
    if-eqz v4, :cond_95

    #@94
    move v7, v6

    #@95
    :cond_95
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@98
    goto/16 :goto_9

    #@9a
    .line 108
    .end local v0           #_arg0:Lcom/vzw/location/IVzwGpsStatusListener;
    .end local v4           #_result:Z
    :sswitch_9a
    const-string v7, "com.vzw.location.IVzwLocationManager"

    #@9c
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9f
    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a2
    move-result-object v7

    #@a3
    invoke-static {v7}, Lcom/vzw/location/IVzwGpsStatusListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/vzw/location/IVzwGpsStatusListener;

    #@a6
    move-result-object v0

    #@a7
    .line 111
    .restart local v0       #_arg0:Lcom/vzw/location/IVzwGpsStatusListener;
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationManager$Stub;->removeGpsStatusListener(Lcom/vzw/location/IVzwGpsStatusListener;)V

    #@aa
    .line 112
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad
    goto/16 :goto_9

    #@af
    .line 117
    .end local v0           #_arg0:Lcom/vzw/location/IVzwGpsStatusListener;
    :sswitch_af
    const-string v8, "com.vzw.location.IVzwLocationManager"

    #@b1
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b4
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b7
    move-result-object v0

    #@b8
    .line 121
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@bb
    move-result-object v1

    #@bc
    .line 123
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@bf
    move-result v8

    #@c0
    if-eqz v8, :cond_e1

    #@c2
    .line 124
    sget-object v8, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c4
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c7
    move-result-object v2

    #@c8
    check-cast v2, Landroid/os/Bundle;

    #@ca
    .line 129
    .local v2, _arg2:Landroid/os/Bundle;
    :goto_ca
    invoke-virtual {p0, v0, v1, v2}, Lcom/vzw/location/IVzwLocationManager$Stub;->sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    #@cd
    move-result v4

    #@ce
    .line 130
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d1
    .line 131
    if-eqz v4, :cond_e3

    #@d3
    move v8, v6

    #@d4
    :goto_d4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@d7
    .line 132
    if-eqz v2, :cond_e5

    #@d9
    .line 133
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@dc
    .line 134
    invoke-virtual {v2, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@df
    goto/16 :goto_9

    #@e1
    .line 127
    .end local v2           #_arg2:Landroid/os/Bundle;
    .end local v4           #_result:Z
    :cond_e1
    const/4 v2, 0x0

    #@e2
    .restart local v2       #_arg2:Landroid/os/Bundle;
    goto :goto_ca

    #@e3
    .restart local v4       #_result:Z
    :cond_e3
    move v8, v7

    #@e4
    .line 131
    goto :goto_d4

    #@e5
    .line 137
    :cond_e5
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@e8
    goto/16 :goto_9

    #@ea
    .line 143
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Landroid/os/Bundle;
    .end local v4           #_result:Z
    :sswitch_ea
    const-string v8, "com.vzw.location.IVzwLocationManager"

    #@ec
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ef
    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f2
    move-result-object v0

    #@f3
    .line 146
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationManager$Stub;->getProviderInfo(Ljava/lang/String;)Landroid/os/Bundle;

    #@f6
    move-result-object v4

    #@f7
    .line 147
    .local v4, _result:Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fa
    .line 148
    if-eqz v4, :cond_104

    #@fc
    .line 149
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@ff
    .line 150
    invoke-virtual {v4, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@102
    goto/16 :goto_9

    #@104
    .line 153
    :cond_104
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@107
    goto/16 :goto_9

    #@109
    .line 159
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Landroid/os/Bundle;
    :sswitch_109
    const-string v8, "com.vzw.location.IVzwLocationManager"

    #@10b
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10e
    .line 161
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@111
    move-result-object v0

    #@112
    .line 162
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vzw/location/IVzwLocationManager$Stub;->isProviderEnabled(Ljava/lang/String;)Z

    #@115
    move-result v4

    #@116
    .line 163
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@119
    .line 164
    if-eqz v4, :cond_11c

    #@11b
    move v7, v6

    #@11c
    :cond_11c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@11f
    goto/16 :goto_9

    #@121
    .line 169
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_121
    const-string v8, "com.vzw.location.IVzwLocationManager"

    #@123
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@126
    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@129
    move-result-object v0

    #@12a
    .line 173
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12d
    move-result v8

    #@12e
    if-eqz v8, :cond_147

    #@130
    .line 174
    sget-object v8, Lcom/vzw/location/VzwGpsConfigInit;->CREATOR:Landroid/os/Parcelable$Creator;

    #@132
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@135
    move-result-object v1

    #@136
    check-cast v1, Lcom/vzw/location/VzwGpsConfigInit;

    #@138
    .line 179
    .local v1, _arg1:Lcom/vzw/location/VzwGpsConfigInit;
    :goto_138
    invoke-virtual {p0, v0, v1}, Lcom/vzw/location/IVzwLocationManager$Stub;->setConfigInit(Ljava/lang/String;Lcom/vzw/location/VzwGpsConfigInit;)Z

    #@13b
    move-result v4

    #@13c
    .line 180
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13f
    .line 181
    if-eqz v4, :cond_142

    #@141
    move v7, v6

    #@142
    :cond_142
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@145
    goto/16 :goto_9

    #@147
    .line 177
    .end local v1           #_arg1:Lcom/vzw/location/VzwGpsConfigInit;
    .end local v4           #_result:Z
    :cond_147
    const/4 v1, 0x0

    #@148
    .restart local v1       #_arg1:Lcom/vzw/location/VzwGpsConfigInit;
    goto :goto_138

    #@149
    .line 42
    nop

    #@14a
    :sswitch_data_14a
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_39
        0x4 -> :sswitch_6a
        0x5 -> :sswitch_7e
        0x6 -> :sswitch_9a
        0x7 -> :sswitch_af
        0x8 -> :sswitch_ea
        0x9 -> :sswitch_109
        0xa -> :sswitch_121
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
