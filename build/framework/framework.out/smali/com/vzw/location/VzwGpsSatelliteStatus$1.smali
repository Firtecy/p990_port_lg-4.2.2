.class Lcom/vzw/location/VzwGpsSatelliteStatus$1;
.super Ljava/lang/Object;
.source "VzwGpsSatelliteStatus.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/VzwGpsSatelliteStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/vzw/location/VzwGpsSatelliteStatus;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsSatelliteStatus;
    .registers 6
    .parameter "parcel"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 80
    new-instance v0, Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@4
    invoke-direct {v0}, Lcom/vzw/location/VzwGpsSatelliteStatus;-><init>()V

    #@7
    .line 81
    .local v0, vz:Lcom/vzw/location/VzwGpsSatelliteStatus;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v1

    #@b
    iput v1, v0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mPrn:I

    #@d
    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@10
    move-result v1

    #@11
    iput v1, v0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mCnr:F

    #@13
    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@16
    move-result v1

    #@17
    if-ne v1, v2, :cond_25

    #@19
    move v1, v2

    #@1a
    :goto_1a
    iput-boolean v1, v0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasAlmanac:Z

    #@1c
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@1f
    move-result v1

    #@20
    if-ne v1, v2, :cond_27

    #@22
    :goto_22
    iput-boolean v2, v0, Lcom/vzw/location/VzwGpsSatelliteStatus;->mHasEphemeris:Z

    #@24
    .line 85
    return-object v0

    #@25
    :cond_25
    move v1, v3

    #@26
    .line 83
    goto :goto_1a

    #@27
    :cond_27
    move v2, v3

    #@28
    .line 84
    goto :goto_22
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsSatelliteStatus$1;->createFromParcel(Landroid/os/Parcel;)Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/vzw/location/VzwGpsSatelliteStatus;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 89
    new-array v0, p1, [Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/vzw/location/VzwGpsSatelliteStatus$1;->newArray(I)[Lcom/vzw/location/VzwGpsSatelliteStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
