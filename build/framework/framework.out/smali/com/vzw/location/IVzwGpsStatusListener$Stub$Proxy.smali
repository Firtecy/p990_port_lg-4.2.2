.class Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IVzwGpsStatusListener.java"

# interfaces
.implements Lcom/vzw/location/IVzwGpsStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vzw/location/IVzwGpsStatusListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 112
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 114
    iput-object p1, p0, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 115
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 118
    iget-object v0, p0, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 122
    const-string v0, "com.vzw.location.IVzwGpsStatusListener"

    #@2
    return-object v0
.end method

.method public onGpsDeviceStatusChanged(Lcom/vzw/location/VzwGpsDeviceStatus;)V
    .registers 7
    .parameter "deviceStatus"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 176
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 178
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.IVzwGpsStatusListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 179
    if-eqz p1, :cond_1f

    #@b
    .line 180
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 181
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Lcom/vzw/location/VzwGpsDeviceStatus;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 186
    :goto_13
    iget-object v1, p0, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x4

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_24

    #@1b
    .line 189
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 191
    return-void

    #@1f
    .line 184
    :cond_1f
    const/4 v1, 0x0

    #@20
    :try_start_20
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 188
    :catchall_24
    move-exception v1

    #@25
    .line 189
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 190
    throw v1
.end method

.method public onGpsStatusChanged(I)V
    .registers 7
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 126
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 128
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.IVzwGpsStatusListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 129
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 130
    iget-object v1, p0, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x1

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 133
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 135
    return-void

    #@18
    .line 132
    :catchall_18
    move-exception v1

    #@19
    .line 133
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    .line 134
    throw v1
.end method

.method public onSatStatusChanged(II[I[I[I[I[I)V
    .registers 13
    .parameter "svCount"
    .parameter "svInViewCount"
    .parameter "prn"
    .parameter "prnInView"
    .parameter "elevInView"
    .parameter "aziminView"
    .parameter "snrInView"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 158
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 160
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.IVzwGpsStatusListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 161
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 162
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 163
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@12
    .line 164
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeIntArray([I)V

    #@15
    .line 165
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeIntArray([I)V

    #@18
    .line 166
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeIntArray([I)V

    #@1b
    .line 167
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeIntArray([I)V

    #@1e
    .line 168
    iget-object v1, p0, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@20
    const/4 v2, 0x3

    #@21
    const/4 v3, 0x0

    #@22
    const/4 v4, 0x1

    #@23
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_26
    .catchall {:try_start_4 .. :try_end_26} :catchall_2a

    #@26
    .line 171
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 173
    return-void

    #@2a
    .line 170
    :catchall_2a
    move-exception v1

    #@2b
    .line 171
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 172
    throw v1
.end method

.method public onSvStatusChanged(II[I[I[F[F[F)V
    .registers 13
    .parameter "svCount"
    .parameter "svInViewCount"
    .parameter "prn"
    .parameter "prnInView"
    .parameter "elevInView"
    .parameter "aziminView"
    .parameter "snrInView"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 140
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 142
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.vzw.location.IVzwGpsStatusListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 143
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 144
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 145
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@12
    .line 146
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeIntArray([I)V

    #@15
    .line 147
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeFloatArray([F)V

    #@18
    .line 148
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeFloatArray([F)V

    #@1b
    .line 149
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeFloatArray([F)V

    #@1e
    .line 150
    iget-object v1, p0, Lcom/vzw/location/IVzwGpsStatusListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@20
    const/4 v2, 0x2

    #@21
    const/4 v3, 0x0

    #@22
    const/4 v4, 0x1

    #@23
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_26
    .catchall {:try_start_4 .. :try_end_26} :catchall_2a

    #@26
    .line 153
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 155
    return-void

    #@2a
    .line 152
    :catchall_2a
    move-exception v1

    #@2b
    .line 153
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 154
    throw v1
.end method
