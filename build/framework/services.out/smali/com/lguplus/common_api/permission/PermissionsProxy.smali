.class public Lcom/lguplus/common_api/permission/PermissionsProxy;
.super Ljava/lang/Object;
.source "PermissionsProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lguplus/common_api/permission/PermissionsProxy$PermsClassLoader;
    }
.end annotation


# static fields
.field static final PERMS_CLS_NAME:Ljava/lang/String; = "com.lguplus.common_api_impl.permission.PermissionsImpl"

.field static final PERMS_PKG_NAME:Ljava/lang/String; = "com.lguplus.common_api_impl"


# instance fields
.field mPerms:Lcom/lguplus/common_api/permission/Permissions;

.field mPkgMgr:Landroid/content/pm/IPackageManager$Stub;


# direct methods
.method public constructor <init>(Landroid/content/pm/IPackageManager$Stub;)V
    .registers 3
    .parameter "pkgMgr"

    #@0
    .prologue
    .line 15
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 14
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@6
    .line 16
    iput-object p1, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPkgMgr:Landroid/content/pm/IPackageManager$Stub;

    #@8
    .line 17
    return-void
.end method

.method private getPerms()V
    .registers 8

    #@0
    .prologue
    .line 30
    :try_start_0
    iget-object v3, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPkgMgr:Landroid/content/pm/IPackageManager$Stub;

    #@2
    const-string v4, "com.lguplus.common_api_impl"

    #@4
    const/4 v5, 0x0

    #@5
    const/4 v6, 0x0

    #@6
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    #@9
    move-result-object v3

    #@a
    iget-object v0, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@c
    .line 31
    .local v0, apkPath:Ljava/lang/String;
    new-instance v3, Lcom/lguplus/common_api/permission/PermissionsProxy$PermsClassLoader;

    #@e
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    #@15
    move-result-object v4

    #@16
    invoke-direct {v3, v0, v4}, Lcom/lguplus/common_api/permission/PermissionsProxy$PermsClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@19
    const-string v4, "com.lguplus.common_api_impl.permission.PermissionsImpl"

    #@1b
    invoke-virtual {v3, v4}, Lcom/lguplus/common_api/permission/PermissionsProxy$PermsClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@1e
    move-result-object v1

    #@1f
    .line 33
    .local v1, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v3, 0x0

    #@20
    new-array v3, v3, [Ljava/lang/Class;

    #@22
    invoke-virtual {v1, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@25
    move-result-object v3

    #@26
    const/4 v4, 0x0

    #@27
    new-array v4, v4, [Ljava/lang/Object;

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Lcom/lguplus/common_api/permission/Permissions;

    #@2f
    iput-object v3, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_31} :catch_32

    #@31
    .line 37
    .end local v0           #apkPath:Ljava/lang/String;
    .end local v1           #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_31
    return-void

    #@32
    .line 34
    :catch_32
    move-exception v2

    #@33
    .line 35
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "uplus_common_api"

    #@35
    new-instance v4, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v5, "getPerms:"

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_31
.end method


# virtual methods
.method public afterAddPkg(Landroid/content/pm/PackageParser$Package;)V
    .registers 5
    .parameter "pkg"

    #@0
    .prologue
    .line 94
    iget-object v0, p1, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@2
    const-string v1, "com.lguplus.common_api_impl"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_d

    #@a
    invoke-direct {p0}, Lcom/lguplus/common_api/permission/PermissionsProxy;->getPerms()V

    #@d
    .line 95
    :cond_d
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@f
    if-eqz v0, :cond_1c

    #@11
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@13
    iget-object v1, p1, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@15
    iget-object v2, p1, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@17
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@19
    invoke-interface {v0, v1, v2}, Lcom/lguplus/common_api/permission/Permissions;->afterAdd(Ljava/lang/String;I)V

    #@1c
    .line 96
    :cond_1c
    return-void
.end method

.method public beforeAddPkg(Landroid/content/pm/PackageParser$Package;)V
    .registers 18
    .parameter "pkg"

    #@0
    .prologue
    .line 55
    :try_start_0
    move-object/from16 v0, p0

    #@2
    iget-object v12, v0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4} :catch_a3

    #@4
    if-eqz v12, :cond_84

    #@6
    .line 56
    const/4 v9, 0x0

    #@7
    .line 57
    .local v9, token:Ljava/lang/String;
    const/4 v1, 0x0

    #@8
    .line 59
    .local v1, am:Landroid/content/res/AssetManager;
    :try_start_8
    new-instance v2, Landroid/content/res/AssetManager;

    #@a
    invoke-direct {v2}, Landroid/content/res/AssetManager;-><init>()V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_d} :catch_de
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_d} :catch_85

    #@d
    .line 60
    .end local v1           #am:Landroid/content/res/AssetManager;
    .local v2, am:Landroid/content/res/AssetManager;
    :try_start_d
    move-object/from16 v0, p1

    #@f
    iget-object v12, v0, Landroid/content/pm/PackageParser$Package;->mScanPath:Ljava/lang/String;

    #@11
    invoke-virtual {v2, v12}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@14
    move-result v4

    #@15
    .line 61
    .local v4, cookie:I
    if-eqz v4, :cond_2f

    #@17
    .line 62
    const-string v12, "assets/uplus_common_api_perm_token.txt"

    #@19
    invoke-virtual {v2, v4, v12}, Landroid/content/res/AssetManager;->openNonAsset(ILjava/lang/String;)Ljava/io/InputStream;

    #@1c
    move-result-object v6

    #@1d
    .line 63
    .local v6, is:Ljava/io/InputStream;
    const/16 v12, 0x1000

    #@1f
    new-array v3, v12, [B

    #@21
    .local v3, bytes:[B
    invoke-virtual {v6, v3}, Ljava/io/InputStream;->read([B)I

    #@24
    move-result v8

    #@25
    .local v8, n:I
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    #@28
    .line 64
    new-instance v10, Ljava/lang/String;

    #@2a
    const/4 v12, 0x0

    #@2b
    invoke-direct {v10, v3, v12, v8}, Ljava/lang/String;-><init>([BII)V
    :try_end_2e
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_2e} :catch_e1
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_2e} :catch_db

    #@2e
    .end local v9           #token:Ljava/lang/String;
    .local v10, token:Ljava/lang/String;
    move-object v9, v10

    #@2f
    .end local v3           #bytes:[B
    .end local v6           #is:Ljava/io/InputStream;
    .end local v8           #n:I
    .end local v10           #token:Ljava/lang/String;
    .restart local v9       #token:Ljava/lang/String;
    :cond_2f
    move-object v1, v2

    #@30
    .line 72
    .end local v2           #am:Landroid/content/res/AssetManager;
    .end local v4           #cookie:I
    .restart local v1       #am:Landroid/content/res/AssetManager;
    :goto_30
    if-eqz v1, :cond_35

    #@32
    :try_start_32
    invoke-virtual {v1}, Landroid/content/res/AssetManager;->close()V

    #@35
    .line 73
    :cond_35
    move-object/from16 v0, p0

    #@37
    iget-object v12, v0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@39
    move-object/from16 v0, p1

    #@3b
    iget-object v13, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@3d
    move-object/from16 v0, p1

    #@3f
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@41
    const/4 v15, 0x0

    #@42
    aget-object v14, v14, v15

    #@44
    invoke-virtual {v14}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    #@47
    move-result-object v14

    #@48
    invoke-interface {v12, v13, v9, v14}, Lcom/lguplus/common_api/permission/Permissions;->beforeAdd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v11

    #@4c
    .line 75
    .local v11, userName:Ljava/lang/String;
    if-eqz v11, :cond_56

    #@4e
    const-string v12, "INVALID-TOKEN"

    #@50
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v12

    #@54
    if-nez v12, :cond_c1

    #@56
    :cond_56
    const/4 v7, 0x1

    #@57
    .line 76
    .local v7, isValidToken:Z
    :goto_57
    move-object/from16 v0, p1

    #@59
    iget-object v12, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@5b
    if-eqz v12, :cond_d4

    #@5d
    .line 77
    if-eqz v7, :cond_c3

    #@5f
    .line 78
    move-object/from16 v0, p1

    #@61
    iget-object v12, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@63
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v12

    #@67
    if-nez v12, :cond_84

    #@69
    .line 79
    new-instance v12, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    move-object/from16 v0, p1

    #@70
    iget-object v13, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@72
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v12

    #@76
    const-string v13, ".uplus_common_api"

    #@78
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v12

    #@7c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v12

    #@80
    move-object/from16 v0, p1

    #@82
    iput-object v12, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@84
    .line 91
    .end local v1           #am:Landroid/content/res/AssetManager;
    .end local v7           #isValidToken:Z
    .end local v9           #token:Ljava/lang/String;
    .end local v11           #userName:Ljava/lang/String;
    :cond_84
    :goto_84
    return-void

    #@85
    .line 69
    .restart local v1       #am:Landroid/content/res/AssetManager;
    .restart local v9       #token:Ljava/lang/String;
    :catch_85
    move-exception v5

    #@86
    .line 70
    .local v5, e:Ljava/lang/Exception;
    :goto_86
    const-string v12, "uplus_common_api"

    #@88
    new-instance v13, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v14, "beforeAddPkg:"

    #@8f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v13

    #@93
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@96
    move-result-object v14

    #@97
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v13

    #@9b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v13

    #@9f
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a2
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_a2} :catch_a3

    #@a2
    goto :goto_30

    #@a3
    .line 88
    .end local v1           #am:Landroid/content/res/AssetManager;
    .end local v5           #e:Ljava/lang/Exception;
    .end local v9           #token:Ljava/lang/String;
    :catch_a3
    move-exception v5

    #@a4
    .line 89
    .restart local v5       #e:Ljava/lang/Exception;
    const-string v12, "uplus_common_api"

    #@a6
    new-instance v13, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v14, "beforeAddPkg:"

    #@ad
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v13

    #@b1
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@b4
    move-result-object v14

    #@b5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v13

    #@b9
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v13

    #@bd
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    goto :goto_84

    #@c1
    .line 75
    .end local v5           #e:Ljava/lang/Exception;
    .restart local v1       #am:Landroid/content/res/AssetManager;
    .restart local v9       #token:Ljava/lang/String;
    .restart local v11       #userName:Ljava/lang/String;
    :cond_c1
    const/4 v7, 0x0

    #@c2
    goto :goto_57

    #@c3
    .line 81
    .restart local v7       #isValidToken:Z
    :cond_c3
    :try_start_c3
    move-object/from16 v0, p1

    #@c5
    iget-object v12, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@c7
    const-string v13, ".uplus_common_api$"

    #@c9
    const-string v14, ""

    #@cb
    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ce
    move-result-object v12

    #@cf
    move-object/from16 v0, p1

    #@d1
    iput-object v12, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@d3
    goto :goto_84

    #@d4
    .line 84
    :cond_d4
    if-eqz v7, :cond_84

    #@d6
    move-object/from16 v0, p1

    #@d8
    iput-object v11, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;
    :try_end_da
    .catch Ljava/lang/Exception; {:try_start_c3 .. :try_end_da} :catch_a3

    #@da
    goto :goto_84

    #@db
    .line 69
    .end local v1           #am:Landroid/content/res/AssetManager;
    .end local v7           #isValidToken:Z
    .end local v11           #userName:Ljava/lang/String;
    .restart local v2       #am:Landroid/content/res/AssetManager;
    :catch_db
    move-exception v5

    #@dc
    move-object v1, v2

    #@dd
    .end local v2           #am:Landroid/content/res/AssetManager;
    .restart local v1       #am:Landroid/content/res/AssetManager;
    goto :goto_86

    #@de
    .line 67
    :catch_de
    move-exception v12

    #@df
    goto/16 :goto_30

    #@e1
    .end local v1           #am:Landroid/content/res/AssetManager;
    .restart local v2       #am:Landroid/content/res/AssetManager;
    :catch_e1
    move-exception v12

    #@e2
    move-object v1, v2

    #@e3
    .end local v2           #am:Landroid/content/res/AssetManager;
    .restart local v1       #am:Landroid/content/res/AssetManager;
    goto/16 :goto_30
.end method

.method public checkByPkgName(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "pkgName"
    .parameter "permName"

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@2
    if-nez v0, :cond_7

    #@4
    invoke-direct {p0}, Lcom/lguplus/common_api/permission/PermissionsProxy;->getPerms()V

    #@7
    .line 44
    :cond_7
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@9
    if-eqz v0, :cond_15

    #@b
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@d
    invoke-interface {v0, p1, p2}, Lcom/lguplus/common_api/permission/Permissions;->checkByPkgName(Ljava/lang/String;Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method

.method public checkByUid(ILjava/lang/String;)Z
    .registers 4
    .parameter "uid"
    .parameter "permName"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@2
    if-nez v0, :cond_7

    #@4
    invoke-direct {p0}, Lcom/lguplus/common_api/permission/PermissionsProxy;->getPerms()V

    #@7
    .line 40
    :cond_7
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@9
    if-eqz v0, :cond_15

    #@b
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@d
    invoke-interface {v0, p1, p2}, Lcom/lguplus/common_api/permission/Permissions;->checkByUid(ILjava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method

.method public getGids(Ljava/lang/String;)[I
    .registers 3
    .parameter "pkgName"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@6
    invoke-interface {v0, p1}, Lcom/lguplus/common_api/permission/Permissions;->getGids(Ljava/lang/String;)[I

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isSystemUid(Ljava/lang/String;)Z
    .registers 3
    .parameter "pkgName"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@6
    invoke-interface {v0, p1}, Lcom/lguplus/common_api/permission/Permissions;->isSystemUid(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onDelPkg(Landroid/content/pm/PackageParser$Package;)V
    .registers 4
    .parameter "pkg"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/lguplus/common_api/permission/PermissionsProxy;->mPerms:Lcom/lguplus/common_api/permission/Permissions;

    #@6
    iget-object v1, p1, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@8
    invoke-interface {v0, v1}, Lcom/lguplus/common_api/permission/Permissions;->del(Ljava/lang/String;)Z

    #@b
    .line 100
    :cond_b
    return-void
.end method

.method public reorderApkFiles([Ljava/lang/String;)V
    .registers 7
    .parameter "files"

    #@0
    .prologue
    .line 102
    const/4 v1, 0x1

    #@1
    .local v1, i:I
    :goto_1
    array-length v3, p1

    #@2
    if-ge v1, v3, :cond_20

    #@4
    .line 103
    aget-object v3, p1, v1

    #@6
    const-string v4, "LGUPlusLinuxCommonApi.apk"

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_21

    #@e
    .line 104
    aget-object v0, p1, v1

    #@10
    .line 105
    .local v0, f:Ljava/lang/String;
    add-int/lit8 v2, v1, -0x1

    #@12
    .local v2, j:I
    :goto_12
    if-ltz v2, :cond_1d

    #@14
    .line 106
    add-int/lit8 v3, v2, 0x1

    #@16
    aget-object v4, p1, v2

    #@18
    aput-object v4, p1, v3

    #@1a
    .line 105
    add-int/lit8 v2, v2, -0x1

    #@1c
    goto :goto_12

    #@1d
    .line 107
    :cond_1d
    const/4 v3, 0x0

    #@1e
    aput-object v0, p1, v3

    #@20
    .line 111
    .end local v0           #f:Ljava/lang/String;
    .end local v2           #j:I
    :cond_20
    return-void

    #@21
    .line 102
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_1
.end method
