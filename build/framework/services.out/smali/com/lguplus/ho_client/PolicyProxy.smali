.class public Lcom/lguplus/ho_client/PolicyProxy;
.super Ljava/lang/Object;
.source "PolicyProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lguplus/ho_client/PolicyProxy$HOClientClassLoader;
    }
.end annotation


# static fields
.field public static final EVENT_HANDLE_CONNECT:I = 0x3e8

.field static final HO_CLS_NAME:Ljava/lang/String; = "com.lguplus.ho_client_impl.HOClientImpl"

.field static final HO_PKG_NAME:Ljava/lang/String; = "com.lguplus.ho_client_impl"

.field static final TAG:Ljava/lang/String; = "lguplusHandover"


# instance fields
.field hoClient:Lcom/lguplus/ho_client/HOClient;

.field isEnabled:Z

.field mContext:Landroid/content/Context;

.field mNetTrackers:[Landroid/net/NetworkStateTracker;

.field mPkgMgr:Landroid/content/pm/PackageManager;

.field private receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Landroid/net/NetworkStateTracker;Z)V
    .registers 7
    .parameter "context"
    .parameter "mNetTrackers"
    .parameter "isEnabled"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 27
    const/4 v1, 0x0

    #@5
    iput-object v1, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@7
    .line 28
    iput-boolean v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@9
    .line 38
    new-instance v1, Lcom/lguplus/ho_client/PolicyProxy$1;

    #@b
    invoke-direct {v1, p0}, Lcom/lguplus/ho_client/PolicyProxy$1;-><init>(Lcom/lguplus/ho_client/PolicyProxy;)V

    #@e
    iput-object v1, p0, Lcom/lguplus/ho_client/PolicyProxy;->receiver:Landroid/content/BroadcastReceiver;

    #@10
    .line 62
    const-string v1, "PolicyProxy()"

    #@12
    invoke-virtual {p0, v1}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@15
    .line 63
    iput-object p1, p0, Lcom/lguplus/ho_client/PolicyProxy;->mContext:Landroid/content/Context;

    #@17
    .line 64
    iput-boolean p3, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@19
    .line 65
    iput-object p2, p0, Lcom/lguplus/ho_client/PolicyProxy;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@1b
    .line 66
    iget-object v1, p0, Lcom/lguplus/ho_client/PolicyProxy;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@20
    move-result-object v1

    #@21
    iput-object v1, p0, Lcom/lguplus/ho_client/PolicyProxy;->mPkgMgr:Landroid/content/pm/PackageManager;

    #@23
    .line 67
    if-eqz p3, :cond_40

    #@25
    .line 68
    new-instance v0, Landroid/content/IntentFilter;

    #@27
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@2a
    .line 69
    .local v0, f:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    #@2c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2f
    .line 70
    const-string v1, "package"

    #@31
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@34
    .line 71
    const-string v1, "com.lguplus.ho_client_impl"

    #@36
    invoke-virtual {v0, v1, v2}, Landroid/content/IntentFilter;->addDataPath(Ljava/lang/String;I)V

    #@39
    .line 72
    iget-object v1, p0, Lcom/lguplus/ho_client/PolicyProxy;->mContext:Landroid/content/Context;

    #@3b
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->receiver:Landroid/content/BroadcastReceiver;

    #@3d
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@40
    .line 74
    .end local v0           #f:Landroid/content/IntentFilter;
    :cond_40
    return-void
.end method


# virtual methods
.method public delayHandleConnect(Landroid/os/Handler;I)Z
    .registers 5
    .parameter "handler"
    .parameter "type"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 178
    const-string v1, "delayHandleConnect"

    #@3
    invoke-virtual {p0, v1}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@6
    .line 179
    invoke-virtual {p0, v0}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@9
    .line 180
    const/4 v1, 0x1

    #@a
    if-ne p2, v1, :cond_10

    #@c
    iget-object v1, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@e
    if-nez v1, :cond_11

    #@10
    .line 181
    :cond_10
    :goto_10
    return v0

    #@11
    :cond_11
    iget-object v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@13
    invoke-interface {v0, p1}, Lcom/lguplus/ho_client/HOClient;->delayHandleConnect(Landroid/os/Handler;)Z

    #@16
    move-result v0

    #@17
    goto :goto_10
.end method

.method declared-synchronized getHoClient(Z)V
    .registers 13
    .parameter "isReplaced"

    #@0
    .prologue
    .line 86
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v7, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_e4

    #@3
    if-eqz v7, :cond_9

    #@5
    if-nez p1, :cond_9

    #@7
    .line 100
    :goto_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 88
    :cond_9
    :try_start_9
    const-string v7, "get HOClient, apk load"

    #@b
    invoke-virtual {p0, v7}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@e
    .line 89
    iget-object v7, p0, Lcom/lguplus/ho_client/PolicyProxy;->mPkgMgr:Landroid/content/pm/PackageManager;

    #@10
    const-string v8, "com.lguplus.ho_client_impl"

    #@12
    const/4 v9, 0x0

    #@13
    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@16
    move-result-object v7

    #@17
    iget-object v0, v7, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@19
    .line 90
    .local v0, apkPath:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v8, "get HOClient, apkPath : "

    #@20
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v7

    #@2c
    invoke-virtual {p0, v7}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@2f
    .line 91
    new-instance v7, Lcom/lguplus/ho_client/PolicyProxy$HOClientClassLoader;

    #@31
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@34
    move-result-object v8

    #@35
    invoke-virtual {v8}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    #@38
    move-result-object v8

    #@39
    invoke-direct {v7, v0, v8}, Lcom/lguplus/ho_client/PolicyProxy$HOClientClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@3c
    const-string v8, "com.lguplus.ho_client_impl.HOClientImpl"

    #@3e
    invoke-virtual {v7, v8}, Lcom/lguplus/ho_client/PolicyProxy$HOClientClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@41
    move-result-object v2

    #@42
    .line 92
    .local v2, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    new-instance v7, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v8, "get HOClient, cls : "

    #@49
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    #@50
    move-result-object v8

    #@51
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {p0, v7}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@5c
    .line 93
    const/4 v7, 0x3

    #@5d
    new-array v7, v7, [Ljava/lang/Class;

    #@5f
    const/4 v8, 0x0

    #@60
    const-class v9, Landroid/content/Context;

    #@62
    aput-object v9, v7, v8

    #@64
    const/4 v8, 0x1

    #@65
    const-class v9, [Landroid/net/NetworkStateTracker;

    #@67
    aput-object v9, v7, v8

    #@69
    const/4 v8, 0x2

    #@6a
    sget-object v9, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@6c
    aput-object v9, v7, v8

    #@6e
    invoke-virtual {v2, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@71
    move-result-object v7

    #@72
    const/4 v8, 0x3

    #@73
    new-array v8, v8, [Ljava/lang/Object;

    #@75
    const/4 v9, 0x0

    #@76
    iget-object v10, p0, Lcom/lguplus/ho_client/PolicyProxy;->mContext:Landroid/content/Context;

    #@78
    aput-object v10, v8, v9

    #@7a
    const/4 v9, 0x1

    #@7b
    iget-object v10, p0, Lcom/lguplus/ho_client/PolicyProxy;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@7d
    aput-object v10, v8, v9

    #@7f
    const/4 v9, 0x2

    #@80
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@83
    move-result-object v10

    #@84
    aput-object v10, v8, v9

    #@86
    invoke-virtual {v7, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    move-result-object v7

    #@8a
    check-cast v7, Lcom/lguplus/ho_client/HOClient;

    #@8c
    iput-object v7, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;
    :try_end_8e
    .catchall {:try_start_9 .. :try_end_8e} :catchall_e4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_8e} :catch_90

    #@8e
    goto/16 :goto_7

    #@90
    .line 94
    .end local v0           #apkPath:Ljava/lang/String;
    .end local v2           #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_90
    move-exception v3

    #@91
    .line 95
    .local v3, e:Ljava/lang/Exception;
    :try_start_91
    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@94
    move-result-object v1

    #@95
    .local v1, arr$:[Ljava/lang/StackTraceElement;
    array-length v6, v1

    #@96
    .local v6, len$:I
    const/4 v5, 0x0

    #@97
    .local v5, i$:I
    :goto_97
    if-ge v5, v6, :cond_c8

    #@99
    aget-object v4, v1, v5

    #@9b
    .line 96
    .local v4, es:Ljava/lang/StackTraceElement;
    const-string v7, "lguplusHandover"

    #@9d
    new-instance v8, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v9, ""

    #@a4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@ab
    move-result-object v9

    #@ac
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    const-string v9, ","

    #@b2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v8

    #@b6
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    #@b9
    move-result-object v9

    #@ba
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v8

    #@be
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v8

    #@c2
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 95
    add-int/lit8 v5, v5, 0x1

    #@c7
    goto :goto_97

    #@c8
    .line 98
    .end local v4           #es:Ljava/lang/StackTraceElement;
    :cond_c8
    new-instance v7, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v8, "getHOClient:"

    #@cf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v7

    #@d3
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@d6
    move-result-object v8

    #@d7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v7

    #@db
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v7

    #@df
    invoke-virtual {p0, v7}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V
    :try_end_e2
    .catchall {:try_start_91 .. :try_end_e2} :catchall_e4

    #@e2
    goto/16 :goto_7

    #@e4
    .line 86
    .end local v1           #arr$:[Ljava/lang/StackTraceElement;
    .end local v3           #e:Ljava/lang/Exception;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :catchall_e4
    move-exception v7

    #@e5
    monitor-exit p0

    #@e6
    throw v7
.end method

.method public isRequiredToRestoreDefaultNetwork(Landroid/os/Handler;ILjava/lang/String;I)Z
    .registers 7
    .parameter "handler"
    .parameter "type"
    .parameter "feature"
    .parameter "currentPid"

    #@0
    .prologue
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "isRequiredToRestoreDefaultNetwork, type : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@16
    .line 186
    const/4 v0, 0x0

    #@17
    invoke-virtual {p0, v0}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@1a
    .line 187
    iget-object v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@1c
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/lguplus/ho_client/HOClient;->isRequiredToRestoreDefaultNetwork(Landroid/os/Handler;ILjava/lang/String;I)Z

    #@1f
    move-result v0

    #@20
    return v0
.end method

.method public isTeardownRequiredOnConnect(II)Z
    .registers 7
    .parameter "type"
    .parameter "activeDefaultNetwork"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 103
    iget-boolean v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 112
    :cond_5
    :goto_5
    return v1

    #@6
    .line 105
    :cond_6
    :try_start_6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "HOClient isTeardownRequiredOnConnect, type : "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "activeDefaultNetwork:"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p0, v2}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@26
    .line 106
    const/4 v2, 0x0

    #@27
    invoke-virtual {p0, v2}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@2a
    .line 107
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@2c
    if-eqz v2, :cond_5

    #@2e
    if-eqz p1, :cond_33

    #@30
    const/4 v2, 0x1

    #@31
    if-ne p1, v2, :cond_5

    #@33
    .line 109
    :cond_33
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@35
    invoke-interface {v2, p1, p2}, Lcom/lguplus/ho_client/HOClient;->isTeardownRequiredOnConnect(II)Z
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_38} :catch_3a

    #@38
    move-result v1

    #@39
    goto :goto_5

    #@3a
    .line 110
    :catch_3a
    move-exception v0

    #@3b
    .line 111
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v3, "isTeardownRequiredOnConnect, e:"

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {p0, v2}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@55
    goto :goto_5
.end method

.method public isValidDefaultNetworkOnConnect(II)Z
    .registers 7
    .parameter "type"
    .parameter "activeDefaultNetwork"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 117
    iget-boolean v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 129
    :cond_5
    :goto_5
    return v1

    #@6
    .line 119
    :cond_6
    :try_start_6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "HOClient isValidDefaultNetworkOnConnect, type:"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, ", activeDefaultNetwork:"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p0, v2}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@26
    .line 120
    const/4 v2, 0x0

    #@27
    invoke-virtual {p0, v2}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@2a
    .line 121
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@2c
    if-eqz v2, :cond_5

    #@2e
    if-eqz p1, :cond_32

    #@30
    if-ne p1, v1, :cond_5

    #@32
    .line 122
    :cond_32
    invoke-virtual {p0, p1}, Lcom/lguplus/ho_client/PolicyProxy;->setNetworkInfo(I)V

    #@35
    .line 123
    if-ne p1, v1, :cond_3e

    #@37
    .line 124
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@39
    invoke-interface {v2, p1, p2}, Lcom/lguplus/ho_client/HOClient;->isValidDefaultNetworkOnConnect(II)Z

    #@3c
    move-result v1

    #@3d
    goto :goto_5

    #@3e
    .line 126
    :cond_3e
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@40
    invoke-interface {v2, p1, p2}, Lcom/lguplus/ho_client/HOClient;->isValidDefaultNetworkOnConnect(II)Z
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_43} :catch_45

    #@43
    move-result v1

    #@44
    goto :goto_5

    #@45
    .line 127
    :catch_45
    move-exception v0

    #@46
    .line 128
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v3, "isValidDefaultNetworkOnConnect,  e:"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {p0, v2}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@60
    goto :goto_5
.end method

.method public isValidDefaultNetworkOnFailover(I)Z
    .registers 7
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 134
    iget-boolean v3, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@4
    if-nez v3, :cond_7

    #@6
    .line 143
    :cond_6
    :goto_6
    return v2

    #@7
    .line 136
    :cond_7
    :try_start_7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "HOClient isValidDefaultNetworkOnFailover, type:"

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {p0, v3}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@1d
    .line 137
    const/4 v3, 0x0

    #@1e
    invoke-virtual {p0, v3}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@21
    .line 138
    iget-object v3, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@23
    if-eqz v3, :cond_6

    #@25
    if-eqz p1, :cond_29

    #@27
    if-ne p1, v1, :cond_6

    #@29
    .line 140
    :cond_29
    iget-object v3, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@2b
    invoke-interface {v3, p1}, Lcom/lguplus/ho_client/HOClient;->isValidDefaultNetworkOnFailover(I)Z

    #@2e
    move-result v3

    #@2f
    if-eqz v3, :cond_41

    #@31
    iget-object v3, p0, Lcom/lguplus/ho_client/PolicyProxy;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@33
    aget-object v3, v3, p1

    #@35
    invoke-interface {v3}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_3c} :catch_43

    #@3c
    move-result v3

    #@3d
    if-eqz v3, :cond_41

    #@3f
    :goto_3f
    move v2, v1

    #@40
    goto :goto_6

    #@41
    :cond_41
    move v1, v2

    #@42
    goto :goto_3f

    #@43
    .line 141
    :catch_43
    move-exception v0

    #@44
    .line 142
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v3, "isValidDefaultNetworkOnFailover, e:"

    #@4b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {p0, v1}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@5e
    goto :goto_6
.end method

.method log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 36
    const-string v0, "lguplusHandover"

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 37
    return-void
.end method

.method public setDefaultNetworkChange(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 171
    const-string v0, "get HOClient, setDefaultNetworkChange"

    #@2
    invoke-virtual {p0, v0}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@5
    .line 172
    const/4 v0, 0x0

    #@6
    invoke-virtual {p0, v0}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@9
    .line 173
    iget-boolean v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@b
    if-eqz v0, :cond_11

    #@d
    iget-object v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@f
    if-nez v0, :cond_12

    #@11
    .line 175
    :cond_11
    :goto_11
    return-void

    #@12
    .line 174
    :cond_12
    iget-object v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@14
    invoke-interface {v0, p1}, Lcom/lguplus/ho_client/HOClient;->setDefaultNetworkChange(I)V

    #@17
    goto :goto_11
.end method

.method public setMobileEnableWhenNSWOActived(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 164
    const-string v0, "policy proxy setMobileEnableWhenNSWOActived"

    #@2
    invoke-virtual {p0, v0}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@5
    .line 165
    const/4 v0, 0x0

    #@6
    invoke-virtual {p0, v0}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@9
    .line 166
    iget-boolean v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@b
    if-eqz v0, :cond_11

    #@d
    iget-object v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@f
    if-nez v0, :cond_12

    #@11
    .line 168
    :cond_11
    :goto_11
    return-void

    #@12
    .line 167
    :cond_12
    iget-object v0, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@14
    invoke-interface {v0, p1}, Lcom/lguplus/ho_client/HOClient;->setMobileEnableWhenNSWOActived(Z)V

    #@17
    goto :goto_11
.end method

.method public setNetworkInfo(I)V
    .registers 9
    .parameter "type"

    #@0
    .prologue
    .line 147
    iget-boolean v5, p0, Lcom/lguplus/ho_client/PolicyProxy;->isEnabled:Z

    #@2
    if-eqz v5, :cond_8

    #@4
    iget-object v5, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@6
    if-nez v5, :cond_9

    #@8
    .line 160
    :cond_8
    :goto_8
    return-void

    #@9
    .line 148
    :cond_9
    new-instance v5, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v6, "setNetworkInfo type :"

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {p0, v5}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@1f
    .line 150
    const/4 v5, 0x0

    #@20
    :try_start_20
    invoke-virtual {p0, v5}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@23
    .line 151
    iget-object v5, p0, Lcom/lguplus/ho_client/PolicyProxy;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@25
    aget-object v5, v5, p1

    #@27
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@2a
    move-result-object v3

    #@2b
    .line 152
    .local v3, lp:Landroid/net/LinkProperties;
    const/4 v1, 0x0

    #@2c
    .line 153
    .local v1, gateway:Ljava/lang/String;
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@2f
    move-result-object v5

    #@30
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@33
    move-result-object v2

    #@34
    .local v2, i$:Ljava/util/Iterator;
    :cond_34
    :goto_34
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@37
    move-result v5

    #@38
    if-eqz v5, :cond_4f

    #@3a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3d
    move-result-object v4

    #@3e
    check-cast v4, Landroid/net/RouteInfo;

    #@40
    .line 154
    .local v4, r:Landroid/net/RouteInfo;
    invoke-virtual {v4}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    #@43
    move-result v5

    #@44
    if-eqz v5, :cond_34

    #@46
    invoke-virtual {v4}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    goto :goto_34

    #@4f
    .line 156
    .end local v4           #r:Landroid/net/RouteInfo;
    :cond_4f
    iget-object v5, p0, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@51
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-interface {v5, p1, v6, v1}, Lcom/lguplus/ho_client/HOClient;->setNetworkInfo(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_58
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_58} :catch_59

    #@58
    goto :goto_8

    #@59
    .line 157
    .end local v1           #gateway:Ljava/lang/String;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #lp:Landroid/net/LinkProperties;
    :catch_59
    move-exception v0

    #@5a
    .line 158
    .local v0, e:Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v6, "setNetworkInfo error"

    #@61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {p0, v5}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@74
    goto :goto_8
.end method
