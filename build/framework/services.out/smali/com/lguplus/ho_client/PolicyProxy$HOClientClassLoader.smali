.class Lcom/lguplus/ho_client/PolicyProxy$HOClientClassLoader;
.super Ldalvik/system/PathClassLoader;
.source "PolicyProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lguplus/ho_client/PolicyProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HOClientClassLoader"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .registers 3
    .parameter "path"
    .parameter "parent"

    #@0
    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@3
    return-void
.end method


# virtual methods
.method public loadClass(Ljava/lang/String;)Ljava/lang/Class;
    .registers 3
    .parameter "clsName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    const-string v0, "com.lguplus.ho_client_impl.HOClientImpl"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_d

    #@8
    invoke-super {p0, p1}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@b
    move-result-object v0

    #@c
    .line 81
    :goto_c
    return-object v0

    #@d
    :cond_d
    invoke-virtual {p0, p1}, Lcom/lguplus/ho_client/PolicyProxy$HOClientClassLoader;->findClass(Ljava/lang/String;)Ljava/lang/Class;

    #@10
    move-result-object v0

    #@11
    goto :goto_c
.end method
