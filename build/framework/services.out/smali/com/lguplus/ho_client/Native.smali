.class public Lcom/lguplus/ho_client/Native;
.super Ljava/lang/Object;
.source "Native.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lguplus/ho_client/Native$OnReadLineHandler;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "lguplusHandover"

.field static isLibraryLoaded:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 12
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/lguplus/ho_client/Native;->isLibraryLoaded:Z

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    const-string v0, "lguplusHandover"

    #@5
    const-string v1, "Native()"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 29
    invoke-static {}, Lcom/lguplus/ho_client/Native;->loadLibrary()V

    #@d
    .line 30
    return-void
.end method

.method public static declared-synchronized loadLibrary()V
    .registers 5

    #@0
    .prologue
    .line 15
    const-class v2, Lcom/lguplus/ho_client/Native;

    #@2
    monitor-enter v2

    #@3
    :try_start_3
    const-string v1, "lguplusHandover"

    #@5
    const-string v3, "loadLibrary()"

    #@7
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 16
    sget-boolean v1, Lcom/lguplus/ho_client/Native;->isLibraryLoaded:Z
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_44

    #@c
    if-nez v1, :cond_24

    #@e
    .line 18
    :try_start_e
    const-string v1, "lguplusHandover"

    #@10
    const-string v3, "before loadLibrary"

    #@12
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 19
    const-string v1, "lguplusHOClient"

    #@17
    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@1a
    .line 20
    const/4 v1, 0x1

    #@1b
    sput-boolean v1, Lcom/lguplus/ho_client/Native;->isLibraryLoaded:Z

    #@1d
    .line 21
    const-string v1, "lguplusHandover"

    #@1f
    const-string v3, "after loadLibrary"

    #@21
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_24
    .catchall {:try_start_e .. :try_end_24} :catchall_44
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_24} :catch_26

    #@24
    .line 26
    .local v0, e:Ljava/lang/Exception;
    :cond_24
    :goto_24
    monitor-exit v2

    #@25
    return-void

    #@26
    .line 22
    .end local v0           #e:Ljava/lang/Exception;
    :catch_26
    move-exception v0

    #@27
    .line 23
    .restart local v0       #e:Ljava/lang/Exception;
    :try_start_27
    const-string v1, "lguplusHandover"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "error on loadLibrary"

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_27 .. :try_end_43} :catchall_44

    #@43
    goto :goto_24

    #@44
    .line 15
    :catchall_44
    move-exception v1

    #@45
    monitor-exit v2

    #@46
    throw v1
.end method

.method public static native readFile(Ljava/lang/String;Lcom/lguplus/ho_client/Native$OnReadLineHandler;)V
.end method

.method public static native readFileToStr(Ljava/lang/String;)Ljava/lang/String;
.end method


# virtual methods
.method public native changeNATTable(Ljava/lang/String;)Z
.end method

.method public native closeIptables(I)Z
.end method

.method public native getRoutes(Ljava/util/List;IZ)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZ)I"
        }
    .end annotation
.end method

.method public native modifyMaskRule(IIZ)Z
.end method

.method public native modifyRouteRule(Ljava/lang/String;IIZ)I
.end method

.method public native modifyRouteTable(Ljava/lang/String;ILjava/lang/String;IZ)I
.end method

.method public native openIptables()I
.end method

.method public native setIptables(I[Ljava/lang/String;)I
.end method
