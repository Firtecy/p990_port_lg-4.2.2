.class Lcom/lguplus/ho_client/PolicyProxy$1;
.super Landroid/content/BroadcastReceiver;
.source "PolicyProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lguplus/ho_client/PolicyProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lguplus/ho_client/PolicyProxy;


# direct methods
.method constructor <init>(Lcom/lguplus/ho_client/PolicyProxy;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 38
    iput-object p1, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 41
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 42
    .local v0, action:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 44
    .local v1, s:Ljava/lang/String;
    if-nez v1, :cond_b

    #@a
    .line 58
    :cond_a
    :goto_a
    return-void

    #@b
    .line 46
    :cond_b
    const-string v2, "com.lguplus.ho_client_impl"

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_a

    #@13
    const-string v2, "test"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@18
    move-result v2

    #@19
    if-nez v2, :cond_a

    #@1b
    .line 47
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@1d
    const-string v3, "package is replaced"

    #@1f
    invoke-virtual {v2, v3}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@22
    .line 48
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@24
    iget-object v2, v2, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@26
    if-eqz v2, :cond_42

    #@28
    .line 49
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@2a
    iget-object v2, v2, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@2c
    invoke-interface {v2}, Lcom/lguplus/ho_client/HOClient;->unRegisterReceiver()V

    #@2f
    .line 50
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@31
    const/4 v3, 0x0

    #@32
    iput-object v3, v2, Lcom/lguplus/ho_client/PolicyProxy;->hoClient:Lcom/lguplus/ho_client/HOClient;

    #@34
    .line 51
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@36
    const-string v3, "HOClient hoclient = null, replaced(true), getHoClient start"

    #@38
    invoke-virtual {v2, v3}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@3b
    .line 52
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@3d
    const/4 v3, 0x1

    #@3e
    invoke-virtual {v2, v3}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@41
    goto :goto_a

    #@42
    .line 54
    :cond_42
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@44
    const-string v3, "HOClient hoclient = null, replaced(false), getHoClient start"

    #@46
    invoke-virtual {v2, v3}, Lcom/lguplus/ho_client/PolicyProxy;->log(Ljava/lang/String;)V

    #@49
    .line 55
    iget-object v2, p0, Lcom/lguplus/ho_client/PolicyProxy$1;->this$0:Lcom/lguplus/ho_client/PolicyProxy;

    #@4b
    const/4 v3, 0x0

    #@4c
    invoke-virtual {v2, v3}, Lcom/lguplus/ho_client/PolicyProxy;->getHoClient(Z)V

    #@4f
    goto :goto_a
.end method
