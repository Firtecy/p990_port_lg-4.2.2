.class final Lcom/android/server/IntentResolver$1;
.super Ljava/lang/Object;
.source "IntentResolver.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/IntentResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 608
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 6
    .parameter "o1"
    .parameter "o2"

    #@0
    .prologue
    .line 610
    check-cast p1, Landroid/content/IntentFilter;

    #@2
    .end local p1
    invoke-virtual {p1}, Landroid/content/IntentFilter;->getPriority()I

    #@5
    move-result v0

    #@6
    .line 611
    .local v0, q1:I
    check-cast p2, Landroid/content/IntentFilter;

    #@8
    .end local p2
    invoke-virtual {p2}, Landroid/content/IntentFilter;->getPriority()I

    #@b
    move-result v1

    #@c
    .line 612
    .local v1, q2:I
    if-le v0, v1, :cond_10

    #@e
    const/4 v2, -0x1

    #@f
    :goto_f
    return v2

    #@10
    :cond_10
    if-ge v0, v1, :cond_14

    #@12
    const/4 v2, 0x1

    #@13
    goto :goto_f

    #@14
    :cond_14
    const/4 v2, 0x0

    #@15
    goto :goto_f
.end method
