.class public Lcom/android/server/ConnectivityService;
.super Landroid/net/IConnectivityManager$Stub;
.source "ConnectivityService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ConnectivityService$VpnCallback;,
        Lcom/android/server/ConnectivityService$SettingsObserver;,
        Lcom/android/server/ConnectivityService$InternalHandler;,
        Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;,
        Lcom/android/server/ConnectivityService$FeatureUser;,
        Lcom/android/server/ConnectivityService$DefaultNetworkFactory;,
        Lcom/android/server/ConnectivityService$NetworkFactory;,
        Lcom/android/server/ConnectivityService$RadioAttributes;
    }
.end annotation


# static fields
.field private static final ADD:Z = true

.field private static APN_INDEX:I = 0x0

.field private static AUTH_TYPE_INDEX:I = 0x0

.field private static BEARER_INDEX:I = 0x0

.field private static CARRIER_ENABLED:I = 0x0

.field private static final DBG:Z = true

.field private static final DISABLED:I = 0x0

.field private static final ENABLED:I = 0x1

.field private static final EVENT_APPLY_GLOBAL_HTTP_PROXY:I = 0x9

.field private static final EVENT_CHANGE_MOBILE_DATA_ENABLED:I = 0x2

.field private static final EVENT_CLEAR_NET_TRANSITION_WAKELOCK:I = 0x8

.field private static final EVENT_INET_CONDITION_CHANGE:I = 0x4

.field private static final EVENT_INET_CONDITION_HOLD_END:I = 0x5

.field private static final EVENT_RESTORE_APNS:I = 0xf

.field private static final EVENT_RESTORE_DEFAULT_NETWORK:I = 0x1

.field private static final EVENT_RESTORE_DNS:I = 0xb

.field private static final EVENT_SEND_STICKY_BROADCAST_INTENT:I = 0xc

.field private static final EVENT_SET_DEPENDENCY_MET:I = 0xa

.field private static final EVENT_SET_MOBILE_DATA:I = 0x7

.field private static final EVENT_SET_NETWORK_PREFERENCE:I = 0x3

.field private static final EVENT_SET_POLICY_DATA_ENABLE:I = 0xd

.field private static final EVENT_VPN_STATE_CHANGED:I = 0xe

.field private static ID_INDEX:I = 0x0

.field private static final INET_CONDITION_LOG_MAX_SIZE:I = 0xf

.field private static IP_INDEX:I = 0x0

.field private static final LOGD_RULES:Z = true

.field private static final MAX_HOSTROUTE_CYCLE_COUNT:I = 0xa

.field private static MCC_INDEX:I = 0x0

.field private static final MESSAGE_BLOCK_ACCESS_POINT:I = 0x64

.field private static MMSC_INDEX:I = 0x0

.field private static MMSPORT_INDEX:I = 0x0

.field private static MMSPROXY_INDEX:I = 0x0

.field private static MNC_INDEX:I = 0x0

.field private static NAME_INDEX:I = 0x0

.field private static final NETWORK_RESTORE_DELAY_PROP_NAME:Ljava/lang/String; = "android.telephony.apn-restore"

.field private static NUMERIC_INDEX:I = 0x0

.field private static PASSWORD_INDEX:I = 0x0

.field private static PORT_INDEX:I = 0x0

.field private static PROXY_INDEX:I = 0x0

.field private static final REMOVE:Z = false

.field private static final RESTORE_DEFAULT_NETWORK_DELAY:I = 0xea60

.field private static SERVER_INDEX:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ConnectivityService"

.field private static final TO_DEFAULT_TABLE:Z = true

.field private static final TO_SECONDARY_TABLE:Z = false

.field private static TYPE_INDEX:I = 0x0

.field private static USER_INDEX:I = 0x0

.field private static final VDBG:Z = true

.field private static final VZWAPPAPN_METATAG:Ljava/lang/String; = "com.verizon.VZWAPPAPN"

.field public static mJustIsBTConnected:Z

.field public static mJustIsWifiConnected:Z

.field private static sProjection:[Ljava/lang/String;

.field private static sServiceInstance:Lcom/android/server/ConnectivityService;


# instance fields
.field public featureset:Ljava/lang/String;

.field private mActiveDefaultNetwork:I

.field private mAddedRoutes:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCaptivePortalTracker:Landroid/net/CaptivePortalTracker;

.field private mContext:Landroid/content/Context;

.field private mCurrentLinkProperties:[Landroid/net/LinkProperties;

.field private mDataActivityObserver:Landroid/net/INetworkManagementEventObserver;

.field private mDefaultConnectionSequence:I

.field private mDefaultDns:Ljava/net/InetAddress;

.field private mDefaultInetCondition:I

.field private mDefaultInetConditionPublished:I

.field private mDefaultProxy:Landroid/net/ProxyProperties;

.field private mDefaultProxyDisabled:Z

.field private mDefaultProxyLock:Ljava/lang/Object;

.field private mDnsLock:Ljava/lang/Object;

.field private mDnsOverridden:Z

.field private mFeatureUsers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/ConnectivityService$FeatureUser;",
            ">;"
        }
    .end annotation
.end field

.field private mGlobalProxy:Landroid/net/ProxyProperties;

.field private final mGlobalProxyLock:Ljava/lang/Object;

.field private mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

.field private mInetConditionChangeInFlight:Z

.field private mInetLog:Ljava/util/ArrayList;

.field private mInitialBroadcast:Landroid/content/Intent;

.field private mIsWifiConnected:Z

.field private mKeyStore:Landroid/security/KeyStore;

.field public mLGDBControl:Lcom/android/internal/telephony/LGDBControl;

.field protected mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field private mLockdownEnabled:Z

.field private mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

.field private mMeteredIfaces:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mNetConfigs:[Landroid/net/NetworkConfig;

.field private mNetRequestAll:Ljava/util/List;

.field private mNetRequestersPids:[Ljava/util/List;

.field private mNetTrackers:[Landroid/net/NetworkStateTracker;

.field private mNetTransitionWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mNetTransitionWakeLockCausedBy:Ljava/lang/String;

.field private mNetTransitionWakeLockSerialNumber:I

.field private mNetTransitionWakeLockTimeout:I

.field private mNetd:Landroid/os/INetworkManagementService;

.field private mNetworkPreference:I

.field mNetworksDefined:I

.field private mNumDnsEntries:I

.field mPackageManager:Landroid/content/pm/PackageManager;

.field protected mPhone:Lcom/android/internal/telephony/PhoneBase;

.field private mPolicyListener:Landroid/net/INetworkPolicyListener;

.field private mPolicyManager:Landroid/net/INetworkPolicyManager;

.field private mPriorityList:[I

.field mProtectedNetworks:Ljava/util/List;

.field mRadioAttributes:[Lcom/android/server/ConnectivityService$RadioAttributes;

.field private mRoutingTableLock:Ljava/lang/Object;

.field private mRulesLock:Ljava/lang/Object;

.field private mSetDataEnableByUser:Z

.field private mSettingsObserver:Lcom/android/server/ConnectivityService$SettingsObserver;

.field private mSystemReady:Z

.field private mTestMode:Z

.field private mTethering:Lcom/android/server/connectivity/Tethering;

.field private mTetheringConfigValid:Z

.field private mTrackerHandler:Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;

.field private mUidRules:Landroid/util/SparseIntArray;

.field private mUserPresentReceiver:Landroid/content/BroadcastReceiver;

.field private mVpn:Lcom/android/server/connectivity/Vpn;

.field private mVpnCallback:Lcom/android/server/ConnectivityService$VpnCallback;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 448
    sput-boolean v2, Lcom/android/server/ConnectivityService;->mJustIsWifiConnected:Z

    #@7
    .line 449
    sput-boolean v2, Lcom/android/server/ConnectivityService;->mJustIsBTConnected:Z

    #@9
    .line 459
    sput v2, Lcom/android/server/ConnectivityService;->ID_INDEX:I

    #@b
    .line 460
    sput v3, Lcom/android/server/ConnectivityService;->NAME_INDEX:I

    #@d
    .line 461
    sput v4, Lcom/android/server/ConnectivityService;->APN_INDEX:I

    #@f
    .line 462
    sput v5, Lcom/android/server/ConnectivityService;->PROXY_INDEX:I

    #@11
    .line 463
    sput v6, Lcom/android/server/ConnectivityService;->PORT_INDEX:I

    #@13
    .line 464
    const/4 v0, 0x5

    #@14
    sput v0, Lcom/android/server/ConnectivityService;->USER_INDEX:I

    #@16
    .line 465
    const/4 v0, 0x6

    #@17
    sput v0, Lcom/android/server/ConnectivityService;->SERVER_INDEX:I

    #@19
    .line 466
    const/4 v0, 0x7

    #@1a
    sput v0, Lcom/android/server/ConnectivityService;->PASSWORD_INDEX:I

    #@1c
    .line 467
    const/16 v0, 0x8

    #@1e
    sput v0, Lcom/android/server/ConnectivityService;->MMSC_INDEX:I

    #@20
    .line 468
    const/16 v0, 0x9

    #@22
    sput v0, Lcom/android/server/ConnectivityService;->MCC_INDEX:I

    #@24
    .line 469
    const/16 v0, 0xa

    #@26
    sput v0, Lcom/android/server/ConnectivityService;->MNC_INDEX:I

    #@28
    .line 470
    const/16 v0, 0xb

    #@2a
    sput v0, Lcom/android/server/ConnectivityService;->NUMERIC_INDEX:I

    #@2c
    .line 471
    const/16 v0, 0xc

    #@2e
    sput v0, Lcom/android/server/ConnectivityService;->MMSPROXY_INDEX:I

    #@30
    .line 472
    const/16 v0, 0xd

    #@32
    sput v0, Lcom/android/server/ConnectivityService;->MMSPORT_INDEX:I

    #@34
    .line 473
    const/16 v0, 0xe

    #@36
    sput v0, Lcom/android/server/ConnectivityService;->AUTH_TYPE_INDEX:I

    #@38
    .line 474
    const/16 v0, 0xf

    #@3a
    sput v0, Lcom/android/server/ConnectivityService;->TYPE_INDEX:I

    #@3c
    .line 475
    const/16 v0, 0x10

    #@3e
    sput v0, Lcom/android/server/ConnectivityService;->IP_INDEX:I

    #@40
    .line 477
    const/16 v0, 0x11

    #@42
    sput v0, Lcom/android/server/ConnectivityService;->BEARER_INDEX:I

    #@44
    .line 478
    const/16 v0, 0x12

    #@46
    sput v0, Lcom/android/server/ConnectivityService;->CARRIER_ENABLED:I

    #@48
    .line 480
    const/16 v0, 0x13

    #@4a
    new-array v0, v0, [Ljava/lang/String;

    #@4c
    const-string v1, "_id"

    #@4e
    aput-object v1, v0, v2

    #@50
    const-string v1, "name"

    #@52
    aput-object v1, v0, v3

    #@54
    const-string v1, "apn"

    #@56
    aput-object v1, v0, v4

    #@58
    const-string v1, "proxy"

    #@5a
    aput-object v1, v0, v5

    #@5c
    const-string v1, "port"

    #@5e
    aput-object v1, v0, v6

    #@60
    const/4 v1, 0x5

    #@61
    const-string v2, "user"

    #@63
    aput-object v2, v0, v1

    #@65
    const/4 v1, 0x6

    #@66
    const-string v2, "server"

    #@68
    aput-object v2, v0, v1

    #@6a
    const/4 v1, 0x7

    #@6b
    const-string v2, "password"

    #@6d
    aput-object v2, v0, v1

    #@6f
    const/16 v1, 0x8

    #@71
    const-string v2, "mmsc"

    #@73
    aput-object v2, v0, v1

    #@75
    const/16 v1, 0x9

    #@77
    const-string v2, "mcc"

    #@79
    aput-object v2, v0, v1

    #@7b
    const/16 v1, 0xa

    #@7d
    const-string v2, "mnc"

    #@7f
    aput-object v2, v0, v1

    #@81
    const/16 v1, 0xb

    #@83
    const-string v2, "numeric"

    #@85
    aput-object v2, v0, v1

    #@87
    const/16 v1, 0xc

    #@89
    const-string v2, "mmsproxy"

    #@8b
    aput-object v2, v0, v1

    #@8d
    const/16 v1, 0xd

    #@8f
    const-string v2, "mmsport"

    #@91
    aput-object v2, v0, v1

    #@93
    const/16 v1, 0xe

    #@95
    const-string v2, "authtype"

    #@97
    aput-object v2, v0, v1

    #@99
    const/16 v1, 0xf

    #@9b
    const-string v2, "type"

    #@9d
    aput-object v2, v0, v1

    #@9f
    const/16 v1, 0x10

    #@a1
    const-string v2, "protocol"

    #@a3
    aput-object v2, v0, v1

    #@a5
    const/16 v1, 0x11

    #@a7
    const-string v2, "bearer"

    #@a9
    aput-object v2, v0, v1

    #@ab
    const/16 v1, 0x12

    #@ad
    const-string v2, "carrier_enabled"

    #@af
    aput-object v2, v0, v1

    #@b1
    sput-object v0, Lcom/android/server/ConnectivityService;->sProjection:[Ljava/lang/String;

    #@b3
    return-void
.end method

.method protected constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 520
    invoke-direct {p0}, Landroid/net/IConnectivityManager$Stub;-><init>()V

    #@5
    .line 229
    iput-boolean v2, p0, Lcom/android/server/ConnectivityService;->mTetheringConfigValid:Z

    #@7
    .line 234
    new-instance v0, Lcom/android/server/ConnectivityService$VpnCallback;

    #@9
    invoke-direct {v0, p0}, Lcom/android/server/ConnectivityService$VpnCallback;-><init>(Lcom/android/server/ConnectivityService;)V

    #@c
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mVpnCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@e
    .line 240
    new-instance v0, Ljava/lang/Object;

    #@10
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@13
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mRulesLock:Ljava/lang/Object;

    #@15
    .line 242
    new-instance v0, Landroid/util/SparseIntArray;

    #@17
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mUidRules:Landroid/util/SparseIntArray;

    #@1c
    .line 244
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mMeteredIfaces:Ljava/util/HashSet;

    #@22
    .line 278
    const/4 v0, -0x1

    #@23
    iput v0, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@25
    .line 280
    iput v2, p0, Lcom/android/server/ConnectivityService;->mDefaultInetCondition:I

    #@27
    .line 281
    iput v2, p0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@29
    .line 282
    iput-boolean v2, p0, Lcom/android/server/ConnectivityService;->mInetConditionChangeInFlight:Z

    #@2b
    .line 283
    iput v2, p0, Lcom/android/server/ConnectivityService;->mDefaultConnectionSequence:I

    #@2d
    .line 285
    new-instance v0, Ljava/lang/Object;

    #@2f
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@32
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mDnsLock:Ljava/lang/Object;

    #@34
    .line 287
    iput-boolean v2, p0, Lcom/android/server/ConnectivityService;->mDnsOverridden:Z

    #@36
    .line 395
    const-string v0, ""

    #@38
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockCausedBy:Ljava/lang/String;

    #@3a
    .line 403
    new-instance v0, Ljava/util/ArrayList;

    #@3c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3f
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mAddedRoutes:Ljava/util/Collection;

    #@41
    .line 413
    const-string v0, "ro.afwdata.LGfeatureset"

    #@43
    const-string v1, "none"

    #@45
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->featureset:Ljava/lang/String;

    #@4b
    .line 420
    iput-boolean v2, p0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@4d
    .line 429
    iput-object v3, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@4f
    .line 430
    new-instance v0, Ljava/lang/Object;

    #@51
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@54
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyLock:Ljava/lang/Object;

    #@56
    .line 431
    iput-boolean v2, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyDisabled:Z

    #@58
    .line 434
    iput-object v3, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@5a
    .line 435
    new-instance v0, Ljava/lang/Object;

    #@5c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5f
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mGlobalProxyLock:Ljava/lang/Object;

    #@61
    .line 442
    iput-boolean v2, p0, Lcom/android/server/ConnectivityService;->mSetDataEnableByUser:Z

    #@63
    .line 445
    new-instance v0, Ljava/lang/Object;

    #@65
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@68
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mRoutingTableLock:Ljava/lang/Object;

    #@6a
    .line 1386
    new-instance v0, Lcom/android/server/ConnectivityService$1;

    #@6c
    invoke-direct {v0, p0}, Lcom/android/server/ConnectivityService$1;-><init>(Lcom/android/server/ConnectivityService;)V

    #@6f
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mDataActivityObserver:Landroid/net/INetworkManagementEventObserver;

    #@71
    .line 2468
    new-instance v0, Lcom/android/server/ConnectivityService$2;

    #@73
    invoke-direct {v0, p0}, Lcom/android/server/ConnectivityService$2;-><init>(Lcom/android/server/ConnectivityService;)V

    #@76
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    #@78
    .line 3153
    new-instance v0, Lcom/android/server/ConnectivityService$3;

    #@7a
    invoke-direct {v0, p0}, Lcom/android/server/ConnectivityService$3;-><init>(Lcom/android/server/ConnectivityService;)V

    #@7d
    iput-object v0, p0, Lcom/android/server/ConnectivityService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@7f
    .line 520
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/net/INetworkStatsService;Landroid/net/INetworkPolicyManager;)V
    .registers 11
    .parameter "context"
    .parameter "netd"
    .parameter "statsService"
    .parameter "policyManager"

    #@0
    .prologue
    .line 526
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/server/ConnectivityService;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/net/INetworkStatsService;Landroid/net/INetworkPolicyManager;Lcom/android/server/ConnectivityService$NetworkFactory;)V

    #@9
    .line 527
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/net/INetworkStatsService;Landroid/net/INetworkPolicyManager;Lcom/android/server/ConnectivityService$NetworkFactory;)V
    .registers 43
    .parameter "context"
    .parameter "netManager"
    .parameter "statsService"
    .parameter "policyManager"
    .parameter "netFactory"

    #@0
    .prologue
    .line 531
    invoke-direct/range {p0 .. p0}, Landroid/net/IConnectivityManager$Stub;-><init>()V

    #@3
    .line 229
    const/4 v2, 0x0

    #@4
    move-object/from16 v0, p0

    #@6
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mTetheringConfigValid:Z

    #@8
    .line 234
    new-instance v2, Lcom/android/server/ConnectivityService$VpnCallback;

    #@a
    move-object/from16 v0, p0

    #@c
    invoke-direct {v2, v0}, Lcom/android/server/ConnectivityService$VpnCallback;-><init>(Lcom/android/server/ConnectivityService;)V

    #@f
    move-object/from16 v0, p0

    #@11
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mVpnCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@13
    .line 240
    new-instance v2, Ljava/lang/Object;

    #@15
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@18
    move-object/from16 v0, p0

    #@1a
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mRulesLock:Ljava/lang/Object;

    #@1c
    .line 242
    new-instance v2, Landroid/util/SparseIntArray;

    #@1e
    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    #@21
    move-object/from16 v0, p0

    #@23
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mUidRules:Landroid/util/SparseIntArray;

    #@25
    .line 244
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@28
    move-result-object v2

    #@29
    move-object/from16 v0, p0

    #@2b
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mMeteredIfaces:Ljava/util/HashSet;

    #@2d
    .line 278
    const/4 v2, -0x1

    #@2e
    move-object/from16 v0, p0

    #@30
    iput v2, v0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@32
    .line 280
    const/4 v2, 0x0

    #@33
    move-object/from16 v0, p0

    #@35
    iput v2, v0, Lcom/android/server/ConnectivityService;->mDefaultInetCondition:I

    #@37
    .line 281
    const/4 v2, 0x0

    #@38
    move-object/from16 v0, p0

    #@3a
    iput v2, v0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@3c
    .line 282
    const/4 v2, 0x0

    #@3d
    move-object/from16 v0, p0

    #@3f
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mInetConditionChangeInFlight:Z

    #@41
    .line 283
    const/4 v2, 0x0

    #@42
    move-object/from16 v0, p0

    #@44
    iput v2, v0, Lcom/android/server/ConnectivityService;->mDefaultConnectionSequence:I

    #@46
    .line 285
    new-instance v2, Ljava/lang/Object;

    #@48
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@4b
    move-object/from16 v0, p0

    #@4d
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mDnsLock:Ljava/lang/Object;

    #@4f
    .line 287
    const/4 v2, 0x0

    #@50
    move-object/from16 v0, p0

    #@52
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mDnsOverridden:Z

    #@54
    .line 395
    const-string v2, ""

    #@56
    move-object/from16 v0, p0

    #@58
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockCausedBy:Ljava/lang/String;

    #@5a
    .line 403
    new-instance v2, Ljava/util/ArrayList;

    #@5c
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5f
    move-object/from16 v0, p0

    #@61
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mAddedRoutes:Ljava/util/Collection;

    #@63
    .line 413
    const-string v2, "ro.afwdata.LGfeatureset"

    #@65
    const-string v3, "none"

    #@67
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    move-object/from16 v0, p0

    #@6d
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->featureset:Ljava/lang/String;

    #@6f
    .line 420
    const/4 v2, 0x0

    #@70
    move-object/from16 v0, p0

    #@72
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@74
    .line 429
    const/4 v2, 0x0

    #@75
    move-object/from16 v0, p0

    #@77
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@79
    .line 430
    new-instance v2, Ljava/lang/Object;

    #@7b
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@7e
    move-object/from16 v0, p0

    #@80
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mDefaultProxyLock:Ljava/lang/Object;

    #@82
    .line 431
    const/4 v2, 0x0

    #@83
    move-object/from16 v0, p0

    #@85
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mDefaultProxyDisabled:Z

    #@87
    .line 434
    const/4 v2, 0x0

    #@88
    move-object/from16 v0, p0

    #@8a
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@8c
    .line 435
    new-instance v2, Ljava/lang/Object;

    #@8e
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@91
    move-object/from16 v0, p0

    #@93
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mGlobalProxyLock:Ljava/lang/Object;

    #@95
    .line 442
    const/4 v2, 0x0

    #@96
    move-object/from16 v0, p0

    #@98
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mSetDataEnableByUser:Z

    #@9a
    .line 445
    new-instance v2, Ljava/lang/Object;

    #@9c
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@9f
    move-object/from16 v0, p0

    #@a1
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mRoutingTableLock:Ljava/lang/Object;

    #@a3
    .line 1386
    new-instance v2, Lcom/android/server/ConnectivityService$1;

    #@a5
    move-object/from16 v0, p0

    #@a7
    invoke-direct {v2, v0}, Lcom/android/server/ConnectivityService$1;-><init>(Lcom/android/server/ConnectivityService;)V

    #@aa
    move-object/from16 v0, p0

    #@ac
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mDataActivityObserver:Landroid/net/INetworkManagementEventObserver;

    #@ae
    .line 2468
    new-instance v2, Lcom/android/server/ConnectivityService$2;

    #@b0
    move-object/from16 v0, p0

    #@b2
    invoke-direct {v2, v0}, Lcom/android/server/ConnectivityService$2;-><init>(Lcom/android/server/ConnectivityService;)V

    #@b5
    move-object/from16 v0, p0

    #@b7
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    #@b9
    .line 3153
    new-instance v2, Lcom/android/server/ConnectivityService$3;

    #@bb
    move-object/from16 v0, p0

    #@bd
    invoke-direct {v2, v0}, Lcom/android/server/ConnectivityService$3;-><init>(Lcom/android/server/ConnectivityService;)V

    #@c0
    move-object/from16 v0, p0

    #@c2
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@c4
    .line 532
    const-string v2, "ConnectivityService starting up"

    #@c6
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@c9
    .line 534
    new-instance v15, Landroid/os/HandlerThread;

    #@cb
    const-string v2, "ConnectivityServiceThread"

    #@cd
    invoke-direct {v15, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@d0
    .line 535
    .local v15, handlerThread:Landroid/os/HandlerThread;
    invoke-virtual {v15}, Landroid/os/HandlerThread;->start()V

    #@d3
    .line 536
    new-instance v2, Lcom/android/server/ConnectivityService$InternalHandler;

    #@d5
    invoke-virtual {v15}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@d8
    move-result-object v3

    #@d9
    move-object/from16 v0, p0

    #@db
    invoke-direct {v2, v0, v3}, Lcom/android/server/ConnectivityService$InternalHandler;-><init>(Lcom/android/server/ConnectivityService;Landroid/os/Looper;)V

    #@de
    move-object/from16 v0, p0

    #@e0
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@e2
    .line 537
    new-instance v2, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;

    #@e4
    invoke-virtual {v15}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@e7
    move-result-object v3

    #@e8
    move-object/from16 v0, p0

    #@ea
    invoke-direct {v2, v0, v3}, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;-><init>(Lcom/android/server/ConnectivityService;Landroid/os/Looper;)V

    #@ed
    move-object/from16 v0, p0

    #@ef
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mTrackerHandler:Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;

    #@f1
    .line 542
    move-object/from16 v0, p0

    #@f3
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->featureset:Ljava/lang/String;

    #@f5
    move-object/from16 v0, p1

    #@f7
    invoke-static {v0, v2}, Lcom/android/internal/telephony/LGfeature;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/LGfeature;

    #@fa
    move-result-object v2

    #@fb
    move-object/from16 v0, p0

    #@fd
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@ff
    .line 544
    new-instance v2, Ljava/lang/StringBuilder;

    #@101
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v3, "[ConnectivityService]"

    #@106
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v2

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@10e
    invoke-virtual {v3}, Lcom/android/internal/telephony/LGfeature;->toString()Ljava/lang/String;

    #@111
    move-result-object v3

    #@112
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v2

    #@116
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@119
    move-result-object v2

    #@11a
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@11d
    .line 549
    new-instance v2, Lcom/android/internal/telephony/LGDBControl;

    #@11f
    move-object/from16 v0, p1

    #@121
    invoke-direct {v2, v0}, Lcom/android/internal/telephony/LGDBControl;-><init>(Landroid/content/Context;)V

    #@124
    move-object/from16 v0, p0

    #@126
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mLGDBControl:Lcom/android/internal/telephony/LGDBControl;

    #@128
    .line 550
    new-instance v2, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const-string v3, "[ConnectivityService]"

    #@12f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v2

    #@133
    move-object/from16 v0, p0

    #@135
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mLGDBControl:Lcom/android/internal/telephony/LGDBControl;

    #@137
    invoke-virtual {v3}, Lcom/android/internal/telephony/LGDBControl;->toString()Ljava/lang/String;

    #@13a
    move-result-object v3

    #@13b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v2

    #@13f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v2

    #@143
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@146
    .line 553
    if-nez p5, :cond_155

    #@148
    .line 554
    new-instance p5, Lcom/android/server/ConnectivityService$DefaultNetworkFactory;

    #@14a
    .end local p5
    move-object/from16 v0, p0

    #@14c
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mTrackerHandler:Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;

    #@14e
    move-object/from16 v0, p5

    #@150
    move-object/from16 v1, p1

    #@152
    invoke-direct {v0, v1, v2}, Lcom/android/server/ConnectivityService$DefaultNetworkFactory;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@155
    .line 557
    .restart local p5
    :cond_155
    const-string v2, "net.Is_phone_booted"

    #@157
    const-string v3, "true"

    #@159
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@15c
    .line 561
    const-string v2, "net.Is_LTERoaming_allowed"

    #@15e
    const-string v3, "true"

    #@160
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@163
    .line 565
    const-string v2, "net.hostname"

    #@165
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@168
    move-result-object v2

    #@169
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16c
    move-result v2

    #@16d
    if-eqz v2, :cond_195

    #@16f
    .line 566
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@172
    move-result-object v2

    #@173
    const-string v3, "android_id"

    #@175
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@178
    move-result-object v18

    #@179
    .line 568
    .local v18, id:Ljava/lang/String;
    if-eqz v18, :cond_195

    #@17b
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@17e
    move-result v2

    #@17f
    if-lez v2, :cond_195

    #@181
    .line 569
    new-instance v2, Ljava/lang/String;

    #@183
    const-string v3, "android-"

    #@185
    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@188
    move-object/from16 v0, v18

    #@18a
    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@18d
    move-result-object v27

    #@18e
    .line 570
    .local v27, name:Ljava/lang/String;
    const-string v2, "net.hostname"

    #@190
    move-object/from16 v0, v27

    #@192
    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@195
    .line 575
    .end local v18           #id:Ljava/lang/String;
    .end local v27           #name:Ljava/lang/String;
    :cond_195
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@198
    move-result-object v2

    #@199
    const-string v3, "default_dns_server"

    #@19b
    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@19e
    move-result-object v12

    #@19f
    .line 577
    .local v12, dns:Ljava/lang/String;
    if-eqz v12, :cond_1a7

    #@1a1
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@1a4
    move-result v2

    #@1a5
    if-nez v2, :cond_1b2

    #@1a7
    .line 578
    :cond_1a7
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1aa
    move-result-object v2

    #@1ab
    const v3, 0x104003b

    #@1ae
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b1
    move-result-object v12

    #@1b2
    .line 582
    :cond_1b2
    :try_start_1b2
    invoke-static {v12}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@1b5
    move-result-object v2

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mDefaultDns:Ljava/net/InetAddress;
    :try_end_1ba
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1b2 .. :try_end_1ba} :catch_299

    #@1ba
    .line 587
    :goto_1ba
    const-string v2, "missing Context"

    #@1bc
    move-object/from16 v0, p1

    #@1be
    invoke-static {v0, v2}, Lcom/android/server/ConnectivityService;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    #@1c1
    move-result-object v2

    #@1c2
    check-cast v2, Landroid/content/Context;

    #@1c4
    move-object/from16 v0, p0

    #@1c6
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@1c8
    .line 588
    const-string v2, "missing INetworkManagementService"

    #@1ca
    move-object/from16 v0, p2

    #@1cc
    invoke-static {v0, v2}, Lcom/android/server/ConnectivityService;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    #@1cf
    move-result-object v2

    #@1d0
    check-cast v2, Landroid/os/INetworkManagementService;

    #@1d2
    move-object/from16 v0, p0

    #@1d4
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@1d6
    .line 589
    const-string v2, "missing INetworkPolicyManager"

    #@1d8
    move-object/from16 v0, p4

    #@1da
    invoke-static {v0, v2}, Lcom/android/server/ConnectivityService;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    #@1dd
    move-result-object v2

    #@1de
    check-cast v2, Landroid/net/INetworkPolicyManager;

    #@1e0
    move-object/from16 v0, p0

    #@1e2
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mPolicyManager:Landroid/net/INetworkPolicyManager;

    #@1e4
    .line 590
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@1e7
    move-result-object v2

    #@1e8
    move-object/from16 v0, p0

    #@1ea
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mKeyStore:Landroid/security/KeyStore;

    #@1ec
    .line 593
    :try_start_1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mPolicyManager:Landroid/net/INetworkPolicyManager;

    #@1f0
    move-object/from16 v0, p0

    #@1f2
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    #@1f4
    invoke-interface {v2, v3}, Landroid/net/INetworkPolicyManager;->registerListener(Landroid/net/INetworkPolicyListener;)V
    :try_end_1f7
    .catch Landroid/os/RemoteException; {:try_start_1ec .. :try_end_1f7} :catch_2b2

    #@1f7
    .line 601
    :goto_1f7
    move-object/from16 v0, p0

    #@1f9
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@1fb
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1fe
    move-result-object v2

    #@1ff
    move-object/from16 v0, p0

    #@201
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@203
    .line 605
    const-string v2, "power"

    #@205
    move-object/from16 v0, p1

    #@207
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@20a
    move-result-object v30

    #@20b
    check-cast v30, Landroid/os/PowerManager;

    #@20d
    .line 607
    .local v30, powerManager:Landroid/os/PowerManager;
    const/4 v2, 0x1

    #@20e
    const-string v3, "ConnectivityService"

    #@210
    move-object/from16 v0, v30

    #@212
    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@215
    move-result-object v2

    #@216
    move-object/from16 v0, p0

    #@218
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@21a
    .line 608
    move-object/from16 v0, p0

    #@21c
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@21e
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@221
    move-result-object v2

    #@222
    const v3, 0x10e0009

    #@225
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@228
    move-result v2

    #@229
    move-object/from16 v0, p0

    #@22b
    iput v2, v0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockTimeout:I

    #@22d
    .line 611
    const/16 v2, 0x18

    #@22f
    new-array v2, v2, [Landroid/net/NetworkStateTracker;

    #@231
    move-object/from16 v0, p0

    #@233
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@235
    .line 613
    const/16 v2, 0x18

    #@237
    new-array v2, v2, [Landroid/net/LinkProperties;

    #@239
    move-object/from16 v0, p0

    #@23b
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mCurrentLinkProperties:[Landroid/net/LinkProperties;

    #@23d
    .line 615
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->getPersistedNetworkPreference()I

    #@240
    move-result v2

    #@241
    move-object/from16 v0, p0

    #@243
    iput v2, v0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@245
    .line 617
    const/16 v2, 0x18

    #@247
    new-array v2, v2, [Lcom/android/server/ConnectivityService$RadioAttributes;

    #@249
    move-object/from16 v0, p0

    #@24b
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mRadioAttributes:[Lcom/android/server/ConnectivityService$RadioAttributes;

    #@24d
    .line 618
    const/16 v2, 0x18

    #@24f
    new-array v2, v2, [Landroid/net/NetworkConfig;

    #@251
    move-object/from16 v0, p0

    #@253
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@255
    .line 621
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@258
    move-result-object v2

    #@259
    const v3, 0x107001a

    #@25c
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@25f
    move-result-object v34

    #@260
    .line 623
    .local v34, raStrings:[Ljava/lang/String;
    move-object/from16 v8, v34

    #@262
    .local v8, arr$:[Ljava/lang/String;
    array-length v0, v8

    #@263
    move/from16 v22, v0

    #@265
    .local v22, len$:I
    const/16 v17, 0x0

    #@267
    .local v17, i$:I
    :goto_267
    move/from16 v0, v17

    #@269
    move/from16 v1, v22

    #@26b
    if-ge v0, v1, :cond_301

    #@26d
    aget-object v33, v8, v17

    #@26f
    .line 624
    .local v33, raString:Ljava/lang/String;
    new-instance v32, Lcom/android/server/ConnectivityService$RadioAttributes;

    #@271
    invoke-direct/range {v32 .. v33}, Lcom/android/server/ConnectivityService$RadioAttributes;-><init>(Ljava/lang/String;)V

    #@274
    .line 625
    .local v32, r:Lcom/android/server/ConnectivityService$RadioAttributes;
    move-object/from16 v0, v32

    #@276
    iget v2, v0, Lcom/android/server/ConnectivityService$RadioAttributes;->mType:I

    #@278
    const/16 v3, 0x17

    #@27a
    if-le v2, v3, :cond_2cf

    #@27c
    .line 626
    new-instance v2, Ljava/lang/StringBuilder;

    #@27e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@281
    const-string v3, "Error in radioAttributes - ignoring attempt to define type "

    #@283
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@286
    move-result-object v2

    #@287
    move-object/from16 v0, v32

    #@289
    iget v3, v0, Lcom/android/server/ConnectivityService$RadioAttributes;->mType:I

    #@28b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28e
    move-result-object v2

    #@28f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@292
    move-result-object v2

    #@293
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@296
    .line 623
    :goto_296
    add-int/lit8 v17, v17, 0x1

    #@298
    goto :goto_267

    #@299
    .line 583
    .end local v8           #arr$:[Ljava/lang/String;
    .end local v17           #i$:I
    .end local v22           #len$:I
    .end local v30           #powerManager:Landroid/os/PowerManager;
    .end local v32           #r:Lcom/android/server/ConnectivityService$RadioAttributes;
    .end local v33           #raString:Ljava/lang/String;
    .end local v34           #raStrings:[Ljava/lang/String;
    :catch_299
    move-exception v13

    #@29a
    .line 584
    .local v13, e:Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/StringBuilder;

    #@29c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29f
    const-string v3, "Error setting defaultDns using "

    #@2a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a4
    move-result-object v2

    #@2a5
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a8
    move-result-object v2

    #@2a9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ac
    move-result-object v2

    #@2ad
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@2b0
    goto/16 :goto_1ba

    #@2b2
    .line 594
    .end local v13           #e:Ljava/lang/IllegalArgumentException;
    :catch_2b2
    move-exception v13

    #@2b3
    .line 596
    .local v13, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b8
    const-string v3, "unable to register INetworkPolicyListener"

    #@2ba
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bd
    move-result-object v2

    #@2be
    invoke-virtual {v13}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@2c1
    move-result-object v3

    #@2c2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v2

    #@2c6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c9
    move-result-object v2

    #@2ca
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@2cd
    goto/16 :goto_1f7

    #@2cf
    .line 629
    .end local v13           #e:Landroid/os/RemoteException;
    .restart local v8       #arr$:[Ljava/lang/String;
    .restart local v17       #i$:I
    .restart local v22       #len$:I
    .restart local v30       #powerManager:Landroid/os/PowerManager;
    .restart local v32       #r:Lcom/android/server/ConnectivityService$RadioAttributes;
    .restart local v33       #raString:Ljava/lang/String;
    .restart local v34       #raStrings:[Ljava/lang/String;
    :cond_2cf
    move-object/from16 v0, p0

    #@2d1
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mRadioAttributes:[Lcom/android/server/ConnectivityService$RadioAttributes;

    #@2d3
    move-object/from16 v0, v32

    #@2d5
    iget v3, v0, Lcom/android/server/ConnectivityService$RadioAttributes;->mType:I

    #@2d7
    aget-object v2, v2, v3

    #@2d9
    if-eqz v2, :cond_2f6

    #@2db
    .line 630
    new-instance v2, Ljava/lang/StringBuilder;

    #@2dd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e0
    const-string v3, "Error in radioAttributes - ignoring attempt to redefine type "

    #@2e2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e5
    move-result-object v2

    #@2e6
    move-object/from16 v0, v32

    #@2e8
    iget v3, v0, Lcom/android/server/ConnectivityService$RadioAttributes;->mType:I

    #@2ea
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ed
    move-result-object v2

    #@2ee
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f1
    move-result-object v2

    #@2f2
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@2f5
    goto :goto_296

    #@2f6
    .line 634
    :cond_2f6
    move-object/from16 v0, p0

    #@2f8
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mRadioAttributes:[Lcom/android/server/ConnectivityService$RadioAttributes;

    #@2fa
    move-object/from16 v0, v32

    #@2fc
    iget v3, v0, Lcom/android/server/ConnectivityService$RadioAttributes;->mType:I

    #@2fe
    aput-object v32, v2, v3

    #@300
    goto :goto_296

    #@301
    .line 637
    .end local v32           #r:Lcom/android/server/ConnectivityService$RadioAttributes;
    .end local v33           #raString:Ljava/lang/String;
    :cond_301
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@304
    move-result-object v2

    #@305
    const v3, 0x1070018

    #@308
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@30b
    move-result-object v26

    #@30c
    .line 639
    .local v26, naStrings:[Ljava/lang/String;
    move-object/from16 v8, v26

    #@30e
    array-length v0, v8

    #@30f
    move/from16 v22, v0

    #@311
    const/16 v17, 0x0

    #@313
    :goto_313
    move/from16 v0, v17

    #@315
    move/from16 v1, v22

    #@317
    if-ge v0, v1, :cond_3bc

    #@319
    aget-object v25, v8, v17

    #@31b
    .line 641
    .local v25, naString:Ljava/lang/String;
    :try_start_31b
    new-instance v23, Landroid/net/NetworkConfig;

    #@31d
    move-object/from16 v0, v23

    #@31f
    move-object/from16 v1, v25

    #@321
    invoke-direct {v0, v1}, Landroid/net/NetworkConfig;-><init>(Ljava/lang/String;)V

    #@324
    .line 642
    .local v23, n:Landroid/net/NetworkConfig;
    move-object/from16 v0, v23

    #@326
    iget v2, v0, Landroid/net/NetworkConfig;->type:I

    #@328
    const/16 v3, 0x17

    #@32a
    if-le v2, v3, :cond_349

    #@32c
    .line 643
    new-instance v2, Ljava/lang/StringBuilder;

    #@32e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@331
    const-string v3, "Error in networkAttributes - ignoring attempt to define type "

    #@333
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@336
    move-result-object v2

    #@337
    move-object/from16 v0, v23

    #@339
    iget v3, v0, Landroid/net/NetworkConfig;->type:I

    #@33b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33e
    move-result-object v2

    #@33f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@342
    move-result-object v2

    #@343
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@346
    .line 639
    .end local v23           #n:Landroid/net/NetworkConfig;
    :goto_346
    add-int/lit8 v17, v17, 0x1

    #@348
    goto :goto_313

    #@349
    .line 647
    .restart local v23       #n:Landroid/net/NetworkConfig;
    :cond_349
    move-object/from16 v0, p0

    #@34b
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@34d
    move-object/from16 v0, v23

    #@34f
    iget v3, v0, Landroid/net/NetworkConfig;->type:I

    #@351
    aget-object v2, v2, v3

    #@353
    if-eqz v2, :cond_372

    #@355
    .line 648
    new-instance v2, Ljava/lang/StringBuilder;

    #@357
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@35a
    const-string v3, "Error in networkAttributes - ignoring attempt to redefine type "

    #@35c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35f
    move-result-object v2

    #@360
    move-object/from16 v0, v23

    #@362
    iget v3, v0, Landroid/net/NetworkConfig;->type:I

    #@364
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@367
    move-result-object v2

    #@368
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36b
    move-result-object v2

    #@36c
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@36f
    goto :goto_346

    #@370
    .line 659
    .end local v23           #n:Landroid/net/NetworkConfig;
    :catch_370
    move-exception v2

    #@371
    goto :goto_346

    #@372
    .line 652
    .restart local v23       #n:Landroid/net/NetworkConfig;
    :cond_372
    move-object/from16 v0, p0

    #@374
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mRadioAttributes:[Lcom/android/server/ConnectivityService$RadioAttributes;

    #@376
    move-object/from16 v0, v23

    #@378
    iget v3, v0, Landroid/net/NetworkConfig;->radio:I

    #@37a
    aget-object v2, v2, v3

    #@37c
    if-nez v2, :cond_3a7

    #@37e
    .line 653
    new-instance v2, Ljava/lang/StringBuilder;

    #@380
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@383
    const-string v3, "Error in networkAttributes - ignoring attempt to use undefined radio "

    #@385
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@388
    move-result-object v2

    #@389
    move-object/from16 v0, v23

    #@38b
    iget v3, v0, Landroid/net/NetworkConfig;->radio:I

    #@38d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@390
    move-result-object v2

    #@391
    const-string v3, " in network type "

    #@393
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@396
    move-result-object v2

    #@397
    move-object/from16 v0, v23

    #@399
    iget v3, v0, Landroid/net/NetworkConfig;->type:I

    #@39b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39e
    move-result-object v2

    #@39f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a2
    move-result-object v2

    #@3a3
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@3a6
    goto :goto_346

    #@3a7
    .line 657
    :cond_3a7
    move-object/from16 v0, p0

    #@3a9
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@3ab
    move-object/from16 v0, v23

    #@3ad
    iget v3, v0, Landroid/net/NetworkConfig;->type:I

    #@3af
    aput-object v23, v2, v3

    #@3b1
    .line 658
    move-object/from16 v0, p0

    #@3b3
    iget v2, v0, Lcom/android/server/ConnectivityService;->mNetworksDefined:I

    #@3b5
    add-int/lit8 v2, v2, 0x1

    #@3b7
    move-object/from16 v0, p0

    #@3b9
    iput v2, v0, Lcom/android/server/ConnectivityService;->mNetworksDefined:I
    :try_end_3bb
    .catch Ljava/lang/Exception; {:try_start_31b .. :try_end_3bb} :catch_370

    #@3bb
    goto :goto_346

    #@3bc
    .line 664
    .end local v23           #n:Landroid/net/NetworkConfig;
    .end local v25           #naString:Ljava/lang/String;
    :cond_3bc
    new-instance v2, Ljava/util/ArrayList;

    #@3be
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@3c1
    move-object/from16 v0, p0

    #@3c3
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mProtectedNetworks:Ljava/util/List;

    #@3c5
    .line 665
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3c8
    move-result-object v2

    #@3c9
    const v3, 0x1070019

    #@3cc
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@3cf
    move-result-object v31

    #@3d0
    .line 667
    .local v31, protectedNetworks:[I
    move-object/from16 v8, v31

    #@3d2
    .local v8, arr$:[I
    array-length v0, v8

    #@3d3
    move/from16 v22, v0

    #@3d5
    const/16 v17, 0x0

    #@3d7
    :goto_3d7
    move/from16 v0, v17

    #@3d9
    move/from16 v1, v22

    #@3db
    if-ge v0, v1, :cond_41c

    #@3dd
    aget v29, v8, v17

    #@3df
    .line 668
    .local v29, p:I
    move-object/from16 v0, p0

    #@3e1
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@3e3
    aget-object v2, v2, v29

    #@3e5
    if-eqz v2, :cond_403

    #@3e7
    move-object/from16 v0, p0

    #@3e9
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mProtectedNetworks:Ljava/util/List;

    #@3eb
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3ee
    move-result-object v3

    #@3ef
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@3f2
    move-result v2

    #@3f3
    if-nez v2, :cond_403

    #@3f5
    .line 669
    move-object/from16 v0, p0

    #@3f7
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mProtectedNetworks:Ljava/util/List;

    #@3f9
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3fc
    move-result-object v3

    #@3fd
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@400
    .line 667
    :goto_400
    add-int/lit8 v17, v17, 0x1

    #@402
    goto :goto_3d7

    #@403
    .line 671
    :cond_403
    new-instance v2, Ljava/lang/StringBuilder;

    #@405
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@408
    const-string v3, "Ignoring protectedNetwork "

    #@40a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40d
    move-result-object v2

    #@40e
    move/from16 v0, v29

    #@410
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@413
    move-result-object v2

    #@414
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@417
    move-result-object v2

    #@418
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@41b
    goto :goto_400

    #@41c
    .line 676
    .end local v29           #p:I
    :cond_41c
    move-object/from16 v0, p0

    #@41e
    iget v2, v0, Lcom/android/server/ConnectivityService;->mNetworksDefined:I

    #@420
    new-array v2, v2, [I

    #@422
    move-object/from16 v0, p0

    #@424
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mPriorityList:[I

    #@426
    .line 678
    move-object/from16 v0, p0

    #@428
    iget v2, v0, Lcom/android/server/ConnectivityService;->mNetworksDefined:I

    #@42a
    add-int/lit8 v20, v2, -0x1

    #@42c
    .line 679
    .local v20, insertionPoint:I
    const/4 v11, 0x0

    #@42d
    .line 680
    .local v11, currentLowest:I
    const/16 v28, 0x0

    #@42f
    .line 681
    .end local v8           #arr$:[I
    .local v28, nextLowest:I
    :goto_42f
    const/4 v2, -0x1

    #@430
    move/from16 v0, v20

    #@432
    if-le v0, v2, :cond_486

    #@434
    .line 682
    move-object/from16 v0, p0

    #@436
    iget-object v8, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@438
    .local v8, arr$:[Landroid/net/NetworkConfig;
    array-length v0, v8

    #@439
    move/from16 v22, v0

    #@43b
    const/16 v17, 0x0

    #@43d
    move/from16 v21, v20

    #@43f
    .end local v20           #insertionPoint:I
    .local v21, insertionPoint:I
    :goto_43f
    move/from16 v0, v17

    #@441
    move/from16 v1, v22

    #@443
    if-ge v0, v1, :cond_47f

    #@445
    aget-object v24, v8, v17

    #@447
    .line 683
    .local v24, na:Landroid/net/NetworkConfig;
    if-nez v24, :cond_450

    #@449
    move/from16 v20, v21

    #@44b
    .line 682
    .end local v21           #insertionPoint:I
    .restart local v20       #insertionPoint:I
    :goto_44b
    add-int/lit8 v17, v17, 0x1

    #@44d
    move/from16 v21, v20

    #@44f
    .end local v20           #insertionPoint:I
    .restart local v21       #insertionPoint:I
    goto :goto_43f

    #@450
    .line 684
    :cond_450
    move-object/from16 v0, v24

    #@452
    iget v2, v0, Landroid/net/NetworkConfig;->priority:I

    #@454
    if-ge v2, v11, :cond_459

    #@456
    move/from16 v20, v21

    #@458
    .end local v21           #insertionPoint:I
    .restart local v20       #insertionPoint:I
    goto :goto_44b

    #@459
    .line 685
    .end local v20           #insertionPoint:I
    .restart local v21       #insertionPoint:I
    :cond_459
    move-object/from16 v0, v24

    #@45b
    iget v2, v0, Landroid/net/NetworkConfig;->priority:I

    #@45d
    if-le v2, v11, :cond_472

    #@45f
    .line 686
    move-object/from16 v0, v24

    #@461
    iget v2, v0, Landroid/net/NetworkConfig;->priority:I

    #@463
    move/from16 v0, v28

    #@465
    if-lt v2, v0, :cond_469

    #@467
    if-nez v28, :cond_6e7

    #@469
    .line 687
    :cond_469
    move-object/from16 v0, v24

    #@46b
    iget v0, v0, Landroid/net/NetworkConfig;->priority:I

    #@46d
    move/from16 v28, v0

    #@46f
    move/from16 v20, v21

    #@471
    .end local v21           #insertionPoint:I
    .restart local v20       #insertionPoint:I
    goto :goto_44b

    #@472
    .line 691
    .end local v20           #insertionPoint:I
    .restart local v21       #insertionPoint:I
    :cond_472
    move-object/from16 v0, p0

    #@474
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mPriorityList:[I

    #@476
    add-int/lit8 v20, v21, -0x1

    #@478
    .end local v21           #insertionPoint:I
    .restart local v20       #insertionPoint:I
    move-object/from16 v0, v24

    #@47a
    iget v3, v0, Landroid/net/NetworkConfig;->type:I

    #@47c
    aput v3, v2, v21

    #@47e
    goto :goto_44b

    #@47f
    .line 693
    .end local v20           #insertionPoint:I
    .end local v24           #na:Landroid/net/NetworkConfig;
    .restart local v21       #insertionPoint:I
    :cond_47f
    move/from16 v11, v28

    #@481
    .line 694
    const/16 v28, 0x0

    #@483
    move/from16 v20, v21

    #@485
    .end local v21           #insertionPoint:I
    .restart local v20       #insertionPoint:I
    goto :goto_42f

    #@486
    .line 698
    .end local v8           #arr$:[Landroid/net/NetworkConfig;
    :cond_486
    const/16 v2, 0x18

    #@488
    new-array v2, v2, [Ljava/util/ArrayList;

    #@48a
    move-object/from16 v0, p0

    #@48c
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@48e
    .line 699
    move-object/from16 v0, p0

    #@490
    iget-object v8, v0, Lcom/android/server/ConnectivityService;->mPriorityList:[I

    #@492
    .local v8, arr$:[I
    array-length v0, v8

    #@493
    move/from16 v22, v0

    #@495
    const/16 v17, 0x0

    #@497
    :goto_497
    move/from16 v0, v17

    #@499
    move/from16 v1, v22

    #@49b
    if-ge v0, v1, :cond_4ad

    #@49d
    aget v16, v8, v17

    #@49f
    .line 700
    .local v16, i:I
    move-object/from16 v0, p0

    #@4a1
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@4a3
    new-instance v3, Ljava/util/ArrayList;

    #@4a5
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@4a8
    aput-object v3, v2, v16

    #@4aa
    .line 699
    add-int/lit8 v17, v17, 0x1

    #@4ac
    goto :goto_497

    #@4ad
    .line 703
    .end local v16           #i:I
    :cond_4ad
    new-instance v2, Ljava/util/ArrayList;

    #@4af
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@4b2
    move-object/from16 v0, p0

    #@4b4
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@4b6
    .line 706
    new-instance v2, Ljava/util/ArrayList;

    #@4b8
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@4bb
    move-object/from16 v0, p0

    #@4bd
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mNetRequestAll:Ljava/util/List;

    #@4bf
    .line 709
    const/4 v2, 0x0

    #@4c0
    move-object/from16 v0, p0

    #@4c2
    iput v2, v0, Lcom/android/server/ConnectivityService;->mNumDnsEntries:I

    #@4c4
    .line 711
    const-string v2, "cm.test.mode"

    #@4c6
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4c9
    move-result-object v2

    #@4ca
    const-string v3, "true"

    #@4cc
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4cf
    move-result v2

    #@4d0
    if-eqz v2, :cond_521

    #@4d2
    const-string v2, "ro.build.type"

    #@4d4
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4d7
    move-result-object v2

    #@4d8
    const-string v3, "eng"

    #@4da
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4dd
    move-result v2

    #@4de
    if-eqz v2, :cond_521

    #@4e0
    const/4 v2, 0x1

    #@4e1
    :goto_4e1
    move-object/from16 v0, p0

    #@4e3
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mTestMode:Z

    #@4e5
    .line 715
    move-object/from16 v0, p0

    #@4e7
    iget-object v8, v0, Lcom/android/server/ConnectivityService;->mPriorityList:[I

    #@4e9
    array-length v0, v8

    #@4ea
    move/from16 v22, v0

    #@4ec
    const/16 v17, 0x0

    #@4ee
    :goto_4ee
    move/from16 v0, v17

    #@4f0
    move/from16 v1, v22

    #@4f2
    if-ge v0, v1, :cond_54b

    #@4f4
    aget v35, v8, v17

    #@4f6
    .line 716
    .local v35, targetNetworkType:I
    move-object/from16 v0, p0

    #@4f8
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@4fa
    aget-object v10, v2, v35

    #@4fc
    .line 719
    .local v10, config:Landroid/net/NetworkConfig;
    :try_start_4fc
    move-object/from16 v0, p5

    #@4fe
    move/from16 v1, v35

    #@500
    invoke-interface {v0, v1, v10}, Lcom/android/server/ConnectivityService$NetworkFactory;->createTracker(ILandroid/net/NetworkConfig;)Landroid/net/NetworkStateTracker;

    #@503
    move-result-object v36

    #@504
    .line 720
    .local v36, tracker:Landroid/net/NetworkStateTracker;
    move-object/from16 v0, p0

    #@506
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@508
    aput-object v36, v2, v35
    :try_end_50a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4fc .. :try_end_50a} :catch_523

    #@50a
    .line 727
    move-object/from16 v0, p0

    #@50c
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mTrackerHandler:Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;

    #@50e
    move-object/from16 v0, v36

    #@510
    move-object/from16 v1, p1

    #@512
    invoke-interface {v0, v1, v2}, Landroid/net/NetworkStateTracker;->startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V

    #@515
    .line 728
    invoke-virtual {v10}, Landroid/net/NetworkConfig;->isDefault()Z

    #@518
    move-result v2

    #@519
    if-eqz v2, :cond_51e

    #@51b
    .line 729
    invoke-interface/range {v36 .. v36}, Landroid/net/NetworkStateTracker;->reconnect()Z

    #@51e
    .line 715
    .end local v36           #tracker:Landroid/net/NetworkStateTracker;
    :cond_51e
    :goto_51e
    add-int/lit8 v17, v17, 0x1

    #@520
    goto :goto_4ee

    #@521
    .line 711
    .end local v10           #config:Landroid/net/NetworkConfig;
    .end local v35           #targetNetworkType:I
    :cond_521
    const/4 v2, 0x0

    #@522
    goto :goto_4e1

    #@523
    .line 721
    .restart local v10       #config:Landroid/net/NetworkConfig;
    .restart local v35       #targetNetworkType:I
    :catch_523
    move-exception v13

    #@524
    .line 722
    .local v13, e:Ljava/lang/IllegalArgumentException;
    const-string v2, "ConnectivityService"

    #@526
    new-instance v3, Ljava/lang/StringBuilder;

    #@528
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@52b
    const-string v4, "Problem creating "

    #@52d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@530
    move-result-object v3

    #@531
    invoke-static/range {v35 .. v35}, Landroid/net/ConnectivityManager;->getNetworkTypeName(I)Ljava/lang/String;

    #@534
    move-result-object v4

    #@535
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@538
    move-result-object v3

    #@539
    const-string v4, " tracker: "

    #@53b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53e
    move-result-object v3

    #@53f
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@542
    move-result-object v3

    #@543
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@546
    move-result-object v3

    #@547
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54a
    goto :goto_51e

    #@54b
    .line 733
    .end local v10           #config:Landroid/net/NetworkConfig;
    .end local v13           #e:Ljava/lang/IllegalArgumentException;
    .end local v35           #targetNetworkType:I
    :cond_54b
    move-object/from16 v0, p0

    #@54d
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@54f
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APN2_ENABLE_BACKUP_RESTORE_VZW:Z

    #@551
    if-eqz v2, :cond_5d2

    #@553
    .line 736
    :try_start_553
    new-instance v19, Ljava/io/BufferedReader;

    #@555
    new-instance v2, Ljava/io/FileReader;

    #@557
    const-string v3, "/persist-lg/apn2/admin_apn_backup"

    #@559
    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@55c
    move-object/from16 v0, v19

    #@55e
    invoke-direct {v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@561
    .line 737
    .local v19, inApnFile:Ljava/io/BufferedReader;
    const/4 v9, 0x0

    #@562
    .line 740
    .local v9, buffer:Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@565
    move-result-object v9

    #@566
    if-eqz v9, :cond_6b8

    #@568
    .line 741
    const-string v2, "ConnectivityService"

    #@56a
    new-instance v3, Ljava/lang/StringBuilder;

    #@56c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@56f
    const-string v4, "!!!!!restoreAPN2Disable: apn2-disable = "

    #@571
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@574
    move-result-object v3

    #@575
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@578
    move-result-object v3

    #@579
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57c
    move-result-object v3

    #@57d
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@580
    .line 742
    const-string v2, "ConnectivityService"

    #@582
    new-instance v3, Ljava/lang/StringBuilder;

    #@584
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@587
    const-string v4, "!!!!!restoreAPN2Disable: apn2-disable len = "

    #@589
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58c
    move-result-object v3

    #@58d
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@590
    move-result v4

    #@591
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@594
    move-result-object v3

    #@595
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@598
    move-result-object v3

    #@599
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59c
    .line 744
    new-instance v14, Ljava/io/File;

    #@59e
    const-string v2, "/persist-lg/apn2/admin_apn_backup"

    #@5a0
    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5a3
    .line 746
    .local v14, f:Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->delete()Z

    #@5a6
    .line 748
    const-string v2, "1"

    #@5a8
    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5ab
    move-result v2

    #@5ac
    if-eqz v2, :cond_5d2

    #@5ae
    .line 750
    const-string v2, "ConnectivityService"

    #@5b0
    const-string v3, "!!!!!restoreAPN2Disable: apn2-disable 1 send message with 11s delay"

    #@5b2
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b5
    .line 751
    move-object/from16 v0, p0

    #@5b7
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@5b9
    move-object/from16 v0, p0

    #@5bb
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@5bd
    const/16 v4, 0xf

    #@5bf
    invoke-virtual {v3, v4}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(I)Landroid/os/Message;

    #@5c2
    move-result-object v3

    #@5c3
    const-wide/16 v4, 0x2af8

    #@5c5
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@5c8
    .line 752
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5cb
    move-result-object v2

    #@5cc
    const-string v3, "apn2_disable"

    #@5ce
    const/4 v4, 0x1

    #@5cf
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_5d2
    .catch Ljava/lang/Exception; {:try_start_553 .. :try_end_5d2} :catch_6c1

    #@5d2
    .line 763
    .end local v9           #buffer:Ljava/lang/String;
    .end local v14           #f:Ljava/io/File;
    .end local v19           #inApnFile:Ljava/io/BufferedReader;
    :cond_5d2
    :goto_5d2
    new-instance v2, Lcom/android/server/connectivity/Tethering;

    #@5d4
    move-object/from16 v0, p0

    #@5d6
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@5d8
    move-object/from16 v0, p0

    #@5da
    iget-object v4, v0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@5dc
    move-object/from16 v0, p0

    #@5de
    iget-object v5, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@5e0
    invoke-virtual {v5}, Lcom/android/server/ConnectivityService$InternalHandler;->getLooper()Landroid/os/Looper;

    #@5e3
    move-result-object v7

    #@5e4
    move-object/from16 v5, p3

    #@5e6
    move-object/from16 v6, p0

    #@5e8
    invoke-direct/range {v2 .. v7}, Lcom/android/server/connectivity/Tethering;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/net/INetworkStatsService;Landroid/net/IConnectivityManager;Landroid/os/Looper;)V

    #@5eb
    move-object/from16 v0, p0

    #@5ed
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@5ef
    .line 764
    move-object/from16 v0, p0

    #@5f1
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@5f3
    invoke-virtual {v2}, Lcom/android/server/connectivity/Tethering;->getTetherableUsbRegexs()[Ljava/lang/String;

    #@5f6
    move-result-object v2

    #@5f7
    array-length v2, v2

    #@5f8
    if-nez v2, :cond_610

    #@5fa
    move-object/from16 v0, p0

    #@5fc
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@5fe
    invoke-virtual {v2}, Lcom/android/server/connectivity/Tethering;->getTetherableWifiRegexs()[Ljava/lang/String;

    #@601
    move-result-object v2

    #@602
    array-length v2, v2

    #@603
    if-nez v2, :cond_610

    #@605
    move-object/from16 v0, p0

    #@607
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@609
    invoke-virtual {v2}, Lcom/android/server/connectivity/Tethering;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    #@60c
    move-result-object v2

    #@60d
    array-length v2, v2

    #@60e
    if-eqz v2, :cond_6cb

    #@610
    :cond_610
    move-object/from16 v0, p0

    #@612
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@614
    invoke-virtual {v2}, Lcom/android/server/connectivity/Tethering;->getUpstreamIfaceTypes()[I

    #@617
    move-result-object v2

    #@618
    array-length v2, v2

    #@619
    if-eqz v2, :cond_6cb

    #@61b
    const/4 v2, 0x1

    #@61c
    :goto_61c
    move-object/from16 v0, p0

    #@61e
    iput-boolean v2, v0, Lcom/android/server/ConnectivityService;->mTetheringConfigValid:Z

    #@620
    .line 769
    new-instance v2, Lcom/android/server/connectivity/Vpn;

    #@622
    move-object/from16 v0, p0

    #@624
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@626
    move-object/from16 v0, p0

    #@628
    iget-object v4, v0, Lcom/android/server/ConnectivityService;->mVpnCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@62a
    move-object/from16 v0, p0

    #@62c
    iget-object v5, v0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@62e
    invoke-direct {v2, v3, v4, v5}, Lcom/android/server/connectivity/Vpn;-><init>(Landroid/content/Context;Lcom/android/server/ConnectivityService$VpnCallback;Landroid/os/INetworkManagementService;)V

    #@631
    move-object/from16 v0, p0

    #@633
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@635
    .line 770
    move-object/from16 v0, p0

    #@637
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@639
    move-object/from16 v0, p0

    #@63b
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@63d
    move-object/from16 v0, p0

    #@63f
    iget-object v4, v0, Lcom/android/server/ConnectivityService;->mTrackerHandler:Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;

    #@641
    invoke-virtual {v2, v3, v4}, Lcom/android/server/connectivity/Vpn;->startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V

    #@644
    .line 773
    :try_start_644
    move-object/from16 v0, p0

    #@646
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@648
    move-object/from16 v0, p0

    #@64a
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@64c
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V

    #@64f
    .line 774
    move-object/from16 v0, p0

    #@651
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@653
    move-object/from16 v0, p0

    #@655
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mDataActivityObserver:Landroid/net/INetworkManagementEventObserver;

    #@657
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    :try_end_65a
    .catch Landroid/os/RemoteException; {:try_start_644 .. :try_end_65a} :catch_6ce

    #@65a
    .line 780
    :goto_65a
    new-instance v2, Ljava/util/ArrayList;

    #@65c
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@65f
    move-object/from16 v0, p0

    #@661
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mInetLog:Ljava/util/ArrayList;

    #@663
    .line 785
    move-object/from16 v0, p0

    #@665
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@667
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->apnbackup:Z

    #@669
    if-eqz v2, :cond_68c

    #@66b
    move-object/from16 v0, p0

    #@66d
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@66f
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@671
    const-string v3, "SPCSBASE"

    #@673
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@676
    move-result v2

    #@677
    if-eqz v2, :cond_68c

    #@679
    .line 787
    move-object/from16 v0, p0

    #@67b
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@67d
    move-object/from16 v0, p0

    #@67f
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@681
    const/16 v4, 0xf

    #@683
    invoke-virtual {v3, v4}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(I)Landroid/os/Message;

    #@686
    move-result-object v3

    #@687
    const-wide/16 v4, 0x1388

    #@689
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@68c
    .line 791
    :cond_68c
    new-instance v2, Lcom/android/server/ConnectivityService$SettingsObserver;

    #@68e
    move-object/from16 v0, p0

    #@690
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@692
    const/16 v4, 0x9

    #@694
    invoke-direct {v2, v3, v4}, Lcom/android/server/ConnectivityService$SettingsObserver;-><init>(Landroid/os/Handler;I)V

    #@697
    move-object/from16 v0, p0

    #@699
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mSettingsObserver:Lcom/android/server/ConnectivityService$SettingsObserver;

    #@69b
    .line 792
    move-object/from16 v0, p0

    #@69d
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mSettingsObserver:Lcom/android/server/ConnectivityService$SettingsObserver;

    #@69f
    move-object/from16 v0, p0

    #@6a1
    iget-object v3, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@6a3
    invoke-virtual {v2, v3}, Lcom/android/server/ConnectivityService$SettingsObserver;->observe(Landroid/content/Context;)V

    #@6a6
    .line 794
    move-object/from16 v0, p0

    #@6a8
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@6aa
    move-object/from16 v0, p0

    #@6ac
    invoke-static {v2, v0}, Landroid/net/CaptivePortalTracker;->makeCaptivePortalTracker(Landroid/content/Context;Landroid/net/IConnectivityManager;)Landroid/net/CaptivePortalTracker;

    #@6af
    move-result-object v2

    #@6b0
    move-object/from16 v0, p0

    #@6b2
    iput-object v2, v0, Lcom/android/server/ConnectivityService;->mCaptivePortalTracker:Landroid/net/CaptivePortalTracker;

    #@6b4
    .line 795
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->loadGlobalProxy()V

    #@6b7
    .line 796
    return-void

    #@6b8
    .line 756
    .restart local v9       #buffer:Ljava/lang/String;
    .restart local v19       #inApnFile:Ljava/io/BufferedReader;
    :cond_6b8
    :try_start_6b8
    const-string v2, "ConnectivityService"

    #@6ba
    const-string v3, "!!!!!restoreAPN2Disable file EMPTY "

    #@6bc
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6bf
    .catch Ljava/lang/Exception; {:try_start_6b8 .. :try_end_6bf} :catch_6c1

    #@6bf
    goto/16 :goto_5d2

    #@6c1
    .line 758
    .end local v9           #buffer:Ljava/lang/String;
    .end local v19           #inApnFile:Ljava/io/BufferedReader;
    :catch_6c1
    move-exception v13

    #@6c2
    .line 759
    .local v13, e:Ljava/lang/Exception;
    const-string v2, "ConnectivityService"

    #@6c4
    const-string v3, "!!!!!!No Backup APNs: read error"

    #@6c6
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c9
    goto/16 :goto_5d2

    #@6cb
    .line 764
    .end local v13           #e:Ljava/lang/Exception;
    :cond_6cb
    const/4 v2, 0x0

    #@6cc
    goto/16 :goto_61c

    #@6ce
    .line 775
    :catch_6ce
    move-exception v13

    #@6cf
    .line 776
    .local v13, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    #@6d1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6d4
    const-string v3, "Error registering observer :"

    #@6d6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d9
    move-result-object v2

    #@6da
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6dd
    move-result-object v2

    #@6de
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e1
    move-result-object v2

    #@6e2
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@6e5
    goto/16 :goto_65a

    #@6e7
    .end local v13           #e:Landroid/os/RemoteException;
    .end local v20           #insertionPoint:I
    .local v8, arr$:[Landroid/net/NetworkConfig;
    .restart local v21       #insertionPoint:I
    .restart local v24       #na:Landroid/net/NetworkConfig;
    :cond_6e7
    move/from16 v20, v21

    #@6e9
    .end local v21           #insertionPoint:I
    .restart local v20       #insertionPoint:I
    goto/16 :goto_44b
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/os/Handler;)Landroid/net/NetworkStateTracker;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-static {p0, p1}, Lcom/android/server/ConnectivityService;->makeWimaxStateTracker(Landroid/content/Context;Landroid/os/Handler;)Landroid/net/NetworkStateTracker;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/ConnectivityService;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->sendDataActivityBroadcast(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->handleConnectionFailure(Landroid/net/NetworkInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->handleCaptivePortalTrackerCheck(Landroid/net/NetworkInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->handleDisconnect(Landroid/net/NetworkInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->handleConnect(Landroid/net/NetworkInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/ConnectivityService;)Lcom/android/server/net/LockdownVpnTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/ConnectivityService;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->handleConnectivityChange(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/server/ConnectivityService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->getConnectivityChangeDelay()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->sendConnectedBroadcastDelayed(Landroid/net/NetworkInfo;I)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/server/ConnectivityService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockSerialNumber:I

    #@2
    return v0
.end method

.method static synthetic access$1900(Lcom/android/server/ConnectivityService;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 204
    invoke-static {p0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Lcom/android/server/ConnectivityService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockCausedBy:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/server/ConnectivityService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->handleInetConditionChange(II)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/server/ConnectivityService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->handleInetConditionHoldEnd(II)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/server/ConnectivityService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->handleSetNetworkPreference(I)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Lcom/android/server/ConnectivityService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->handleSetMobileData(Z)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/server/ConnectivityService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 204
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->handleDeprecatedGlobalHttpProxy()V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/server/ConnectivityService;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->handleSetDependencyMet(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/android/server/ConnectivityService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->handleDnsConfigurationChange(I)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/server/ConnectivityService;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$2900(Lcom/android/server/ConnectivityService;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->handleSetPolicyDataEnable(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/ConnectivityService;Lcom/android/server/ConnectivityService$FeatureUser;Z)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->stopUsingNetworkFeature(Lcom/android/server/ConnectivityService$FeatureUser;Z)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3000(Lcom/android/server/ConnectivityService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 204
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->restoreAPN2Disable()V

    #@3
    return-void
.end method

.method static synthetic access$3100(Lcom/android/server/ConnectivityService;)Lcom/android/server/ConnectivityService$InternalHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$3200(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mDnsLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/server/ConnectivityService;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/ConnectivityService;->updateDns(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3400(Lcom/android/server/ConnectivityService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/android/server/ConnectivityService;->mDnsOverridden:Z

    #@2
    return v0
.end method

.method static synthetic access$3402(Lcom/android/server/ConnectivityService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/android/server/ConnectivityService;->mDnsOverridden:Z

    #@2
    return p1
.end method

.method static synthetic access$3500(Lcom/android/server/ConnectivityService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 204
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->bumpDns()V

    #@3
    return-void
.end method

.method static synthetic access$3600(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$3702(Lcom/android/server/ConnectivityService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyDisabled:Z

    #@2
    return p1
.end method

.method static synthetic access$3800(Lcom/android/server/ConnectivityService;)Landroid/net/ProxyProperties;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@2
    return-object v0
.end method

.method static synthetic access$3900(Lcom/android/server/ConnectivityService;Landroid/net/ProxyProperties;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->sendProxyBroadcast(Landroid/net/ProxyProperties;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mRulesLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/ConnectivityService;)Landroid/util/SparseIntArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mUidRules:Landroid/util/SparseIntArray;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/ConnectivityService;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mMeteredIfaces:Ljava/util/HashSet;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/ConnectivityService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget v0, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/android/server/ConnectivityService;)[Landroid/net/NetworkStateTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/ConnectivityService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private addRoute(Landroid/net/LinkProperties;Landroid/net/RouteInfo;Z)Z
    .registers 11
    .parameter "p"
    .parameter "r"
    .parameter "toDefaultTable"

    #@0
    .prologue
    .line 2305
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    const/4 v4, 0x0

    #@5
    const/4 v5, 0x1

    #@6
    move-object v0, p0

    #@7
    move-object v2, p1

    #@8
    move-object v3, p2

    #@9
    move v6, p3

    #@a
    invoke-direct/range {v0 .. v6}, Lcom/android/server/ConnectivityService;->modifyRoute(Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/RouteInfo;IZZ)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method private addRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z
    .registers 4
    .parameter "lp"
    .parameter "addr"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2313
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/android/server/ConnectivityService;->modifyRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;ZZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method private bumpDns()V
    .registers 9

    #@0
    .prologue
    .line 4009
    const-string v5, "net.dnschange"

    #@2
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    .line 4010
    .local v4, propVal:Ljava/lang/String;
    const/4 v3, 0x0

    #@7
    .line 4011
    .local v3, n:I
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_11

    #@d
    .line 4013
    :try_start_d
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_10} :catch_50

    #@10
    move-result v3

    #@11
    .line 4016
    :cond_11
    :goto_11
    const-string v5, "net.dnschange"

    #@13
    new-instance v6, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v7, ""

    #@1a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v6

    #@1e
    add-int/lit8 v7, v3, 0x1

    #@20
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 4020
    new-instance v2, Landroid/content/Intent;

    #@2d
    const-string v5, "android.intent.action.CLEAR_DNS_CACHE"

    #@2f
    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    .line 4021
    .local v2, intent:Landroid/content/Intent;
    const/high16 v5, 0x2000

    #@34
    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@37
    .line 4025
    const/high16 v5, 0x800

    #@39
    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@3c
    .line 4026
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3f
    move-result-wide v0

    #@40
    .line 4028
    .local v0, ident:J
    :try_start_40
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@42
    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@44
    invoke-virtual {v5, v2, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_47
    .catchall {:try_start_40 .. :try_end_47} :catchall_4b

    #@47
    .line 4030
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4a
    .line 4032
    return-void

    #@4b
    .line 4030
    :catchall_4b
    move-exception v5

    #@4c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4f
    throw v5

    #@50
    .line 4014
    .end local v0           #ident:J
    .end local v2           #intent:Landroid/content/Intent;
    :catch_50
    move-exception v5

    #@51
    goto :goto_11
.end method

.method private static checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter
    .parameter "message"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 4888
    .local p0, value:Ljava/lang/Object;,"TT;"
    if-nez p0, :cond_8

    #@2
    .line 4889
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0

    #@8
    .line 4891
    :cond_8
    return-object p0
.end method

.method private checkVzwNetType(I)I
    .registers 11
    .parameter "networkType"

    #@0
    .prologue
    const/16 v8, 0xf

    #@2
    const/4 v7, 0x5

    #@3
    const/4 v6, 0x1

    #@4
    .line 2148
    if-eq p1, v7, :cond_8

    #@6
    if-ne p1, v8, :cond_68

    #@8
    .line 2150
    :cond_8
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->isSystemImage()Z

    #@b
    move-result v2

    #@c
    .line 2151
    .local v2, mSystemImage:Z
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->isSignedFromVZW()Z

    #@f
    move-result v1

    #@10
    .line 2152
    .local v1, mSignedFromVZW:Z
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->isContainVzwAppApn_MetaTag()Z

    #@13
    move-result v0

    #@14
    .line 2154
    .local v0, mContainVzwAppApn_MetaTag:Z
    const-string v3, "ConnectivityService"

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "mSystemImage : "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, " mSignedFromVZW : "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    const-string v5, " mContainVzwAppApn_MetaTag : "

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 2156
    if-nez v2, :cond_69

    #@42
    if-ne v1, v6, :cond_69

    #@44
    .line 2158
    const-string v3, "ConnectivityService"

    #@46
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, "checkVzwNetType set from "

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    const-string v5, " to "

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 2159
    const/16 p1, 0xf

    #@68
    .line 2178
    .end local v0           #mContainVzwAppApn_MetaTag:Z
    .end local v1           #mSignedFromVZW:Z
    .end local v2           #mSystemImage:Z
    :cond_68
    :goto_68
    return p1

    #@69
    .line 2161
    .restart local v0       #mContainVzwAppApn_MetaTag:Z
    .restart local v1       #mSignedFromVZW:Z
    .restart local v2       #mSystemImage:Z
    :cond_69
    if-nez v2, :cond_91

    #@6b
    if-nez v1, :cond_91

    #@6d
    .line 2163
    const-string v3, "ConnectivityService"

    #@6f
    new-instance v4, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v5, "checkVzwNetType set from "

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    const-string v5, " to "

    #@80
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v4

    #@88
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v4

    #@8c
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 2164
    const/4 p1, 0x5

    #@90
    goto :goto_68

    #@91
    .line 2166
    :cond_91
    if-ne v2, v6, :cond_bc

    #@93
    if-eq v1, v6, :cond_97

    #@95
    if-ne v0, v6, :cond_bc

    #@97
    .line 2168
    :cond_97
    const-string v3, "ConnectivityService"

    #@99
    new-instance v4, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v5, "checkVzwNetType set from "

    #@a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v4

    #@a4
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v4

    #@a8
    const-string v5, " to "

    #@aa
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v4

    #@ae
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v4

    #@b2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v4

    #@b6
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 2169
    const/16 p1, 0xf

    #@bb
    goto :goto_68

    #@bc
    .line 2173
    :cond_bc
    const-string v3, "ConnectivityService"

    #@be
    new-instance v4, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v5, "checkVzwNetType set from "

    #@c5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v4

    #@c9
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v4

    #@cd
    const-string v5, " to "

    #@cf
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v4

    #@d3
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v4

    #@d7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v4

    #@db
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 2174
    const/4 p1, 0x5

    #@df
    goto :goto_68
.end method

.method private convertFeatureToNetworkType(ILjava/lang/String;)I
    .registers 6
    .parameter "networkType"
    .parameter "feature"

    #@0
    .prologue
    .line 4815
    move v0, p1

    #@1
    .line 4817
    .local v0, usedNetworkType:I
    if-nez p1, :cond_d0

    #@3
    .line 4818
    const-string v1, "enableMMS"

    #@5
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_d

    #@b
    .line 4819
    const/4 v0, 0x2

    #@c
    .line 4884
    :goto_c
    return v0

    #@d
    .line 4820
    :cond_d
    const-string v1, "enableSUPL"

    #@f
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_17

    #@15
    .line 4821
    const/4 v0, 0x3

    #@16
    goto :goto_c

    #@17
    .line 4822
    :cond_17
    const-string v1, "enableDUN"

    #@19
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_27

    #@1f
    const-string v1, "enableDUNAlways"

    #@21
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_29

    #@27
    .line 4824
    :cond_27
    const/4 v0, 0x4

    #@28
    goto :goto_c

    #@29
    .line 4825
    :cond_29
    const-string v1, "enableHIPRI"

    #@2b
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_33

    #@31
    .line 4826
    const/4 v0, 0x5

    #@32
    goto :goto_c

    #@33
    .line 4827
    :cond_33
    const-string v1, "enableFOTA"

    #@35
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_3e

    #@3b
    .line 4828
    const/16 v0, 0xa

    #@3d
    goto :goto_c

    #@3e
    .line 4829
    :cond_3e
    const-string v1, "enableIMS"

    #@40
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@43
    move-result v1

    #@44
    if-eqz v1, :cond_49

    #@46
    .line 4830
    const/16 v0, 0xb

    #@48
    goto :goto_c

    #@49
    .line 4831
    :cond_49
    const-string v1, "enableCBS"

    #@4b
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_54

    #@51
    .line 4832
    const/16 v0, 0xc

    #@53
    goto :goto_c

    #@54
    .line 4835
    :cond_54
    const-string v1, "enableADMIN"

    #@56
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@59
    move-result v1

    #@5a
    if-eqz v1, :cond_5f

    #@5c
    .line 4836
    const/16 v0, 0xe

    #@5e
    goto :goto_c

    #@5f
    .line 4837
    :cond_5f
    const-string v1, "enableVZWAPP"

    #@61
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@64
    move-result v1

    #@65
    if-eqz v1, :cond_6a

    #@67
    .line 4838
    const/16 v0, 0xf

    #@69
    goto :goto_c

    #@6a
    .line 4839
    :cond_6a
    const-string v1, "enable800APN"

    #@6c
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@6f
    move-result v1

    #@70
    if-eqz v1, :cond_75

    #@72
    .line 4840
    const/16 v0, 0x11

    #@74
    goto :goto_c

    #@75
    .line 4844
    :cond_75
    const-string v1, "enableTETHERING"

    #@77
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@7a
    move-result v1

    #@7b
    if-eqz v1, :cond_80

    #@7d
    .line 4845
    const/16 v0, 0x12

    #@7f
    goto :goto_c

    #@80
    .line 4849
    :cond_80
    const-string v1, "enableKTMULTIRAB1"

    #@82
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@85
    move-result v1

    #@86
    if-eqz v1, :cond_8b

    #@88
    .line 4850
    const/16 v0, 0x13

    #@8a
    goto :goto_c

    #@8b
    .line 4851
    :cond_8b
    const-string v1, "enableKTMULTIRAB2"

    #@8d
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@90
    move-result v1

    #@91
    if-eqz v1, :cond_97

    #@93
    .line 4852
    const/16 v0, 0x14

    #@95
    goto/16 :goto_c

    #@97
    .line 4855
    :cond_97
    const-string v1, "enableENTITLEMENT"

    #@99
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@9c
    move-result v1

    #@9d
    if-eqz v1, :cond_a3

    #@9f
    .line 4856
    const/16 v0, 0x10

    #@a1
    goto/16 :goto_c

    #@a3
    .line 4859
    :cond_a3
    const-string v1, "enableBIP"

    #@a5
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@a8
    move-result v1

    #@a9
    if-eqz v1, :cond_af

    #@ab
    .line 4860
    const/16 v0, 0x16

    #@ad
    goto/16 :goto_c

    #@af
    .line 4863
    :cond_af
    const-string v1, "enableRCS"

    #@b1
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@b4
    move-result v1

    #@b5
    if-eqz v1, :cond_bb

    #@b7
    .line 4864
    const/16 v0, 0x15

    #@b9
    goto/16 :goto_c

    #@bb
    .line 4868
    :cond_bb
    const-string v1, "enableEMERGENCY"

    #@bd
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@c0
    move-result v1

    #@c1
    if-eqz v1, :cond_c7

    #@c3
    .line 4869
    const/16 v0, 0x17

    #@c5
    goto/16 :goto_c

    #@c7
    .line 4873
    :cond_c7
    const-string v1, "ConnectivityService"

    #@c9
    const-string v2, "Can\'t match any mobile netTracker!"

    #@cb
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    goto/16 :goto_c

    #@d0
    .line 4875
    :cond_d0
    const/4 v1, 0x1

    #@d1
    if-ne p1, v1, :cond_e8

    #@d3
    .line 4876
    const-string v1, "p2p"

    #@d5
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@d8
    move-result v1

    #@d9
    if-eqz v1, :cond_df

    #@db
    .line 4877
    const/16 v0, 0xd

    #@dd
    goto/16 :goto_c

    #@df
    .line 4879
    :cond_df
    const-string v1, "ConnectivityService"

    #@e1
    const-string v2, "Can\'t match any wifi netTracker!"

    #@e3
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    goto/16 :goto_c

    #@e8
    .line 4882
    :cond_e8
    const-string v1, "ConnectivityService"

    #@ea
    const-string v2, "Unexpected network type"

    #@ec
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    goto/16 :goto_c
.end method

.method private deleteDefualt_DNS_forIMS()V
    .registers 11

    #@0
    .prologue
    .line 1608
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    const/16 v8, 0xb

    #@4
    aget-object v5, v7, v8

    #@6
    .line 1610
    .local v5, nt:Landroid/net/NetworkStateTracker;
    if-eqz v5, :cond_18

    #@8
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@b
    move-result-object v7

    #@c
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    #@f
    move-result v7

    #@10
    if-eqz v7, :cond_18

    #@12
    .line 1612
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@15
    move-result-object v6

    #@16
    .line 1614
    .local v6, p:Landroid/net/LinkProperties;
    if-nez v6, :cond_19

    #@18
    .line 1632
    .end local v6           #p:Landroid/net/LinkProperties;
    :cond_18
    return-void

    #@19
    .line 1616
    .restart local v6       #p:Landroid/net/LinkProperties;
    :cond_19
    invoke-virtual {v6}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@1c
    move-result-object v1

    #@1d
    .line 1618
    .local v1, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@20
    move-result v7

    #@21
    if-eqz v7, :cond_18

    #@23
    .line 1620
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@26
    move-result-object v4

    #@27
    .local v4, i$:Ljava/util/Iterator;
    :cond_27
    :goto_27
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@2a
    move-result v7

    #@2b
    if-eqz v7, :cond_18

    #@2d
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Ljava/net/InetAddress;

    #@33
    .line 1622
    .local v0, dns:Ljava/net/InetAddress;
    const/4 v2, 0x1

    #@34
    .line 1624
    .local v2, i:I
    if-eqz v0, :cond_27

    #@36
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@39
    move-result-object v7

    #@3a
    const-string v8, "0.0.0.0"

    #@3c
    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3f
    move-result v7

    #@40
    if-nez v7, :cond_27

    #@42
    .line 1626
    const-string v7, "ConnectivityService"

    #@44
    new-instance v8, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v9, "delete dns "

    #@4b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v8

    #@53
    const-string v9, " for "

    #@55
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v8

    #@59
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@5c
    move-result-object v9

    #@5d
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@60
    move-result-object v9

    #@61
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v8

    #@65
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v8

    #@69
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 1627
    new-instance v7, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v8, "net.dns"

    #@73
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v7

    #@77
    add-int/lit8 v3, v2, 0x1

    #@79
    .end local v2           #i:I
    .local v3, i:I
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v7

    #@7d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v7

    #@81
    const-string v8, ""

    #@83
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    move v2, v3

    #@87
    .end local v3           #i:I
    .restart local v2       #i:I
    goto :goto_27
.end method

.method private enforceAccessPermission()V
    .registers 4

    #@0
    .prologue
    .line 2699
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    #@4
    const-string v2, "ConnectivityService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2702
    return-void
.end method

.method private enforceChangePermission()V
    .registers 4

    #@0
    .prologue
    .line 2705
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v2, "ConnectivityService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2708
    return-void
.end method

.method private enforceConnectivityInternalPermission()V
    .registers 4

    #@0
    .prologue
    .line 2724
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "ConnectivityService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2727
    return-void
.end method

.method private enforcePreference()V
    .registers 4

    #@0
    .prologue
    .line 1126
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    iget v2, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@4
    aget-object v1, v1, v2

    #@6
    invoke-interface {v1}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    .line 1142
    :cond_10
    return-void

    #@11
    .line 1129
    :cond_11
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@13
    iget v2, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@15
    aget-object v1, v1, v2

    #@17
    invoke-interface {v1}, Landroid/net/NetworkStateTracker;->isAvailable()Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_10

    #@1d
    .line 1132
    const/4 v0, 0x0

    #@1e
    .local v0, t:I
    :goto_1e
    const/16 v1, 0x17

    #@20
    if-gt v0, v1, :cond_10

    #@22
    .line 1133
    iget v1, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@24
    if-eq v0, v1, :cond_65

    #@26
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@28
    aget-object v1, v1, v0

    #@2a
    if-eqz v1, :cond_65

    #@2c
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2e
    aget-object v1, v1, v0

    #@30
    invoke-interface {v1}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_65

    #@3a
    .line 1136
    new-instance v1, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v2, "tearing down "

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@47
    aget-object v2, v2, v0

    #@49
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    const-string v2, " in enforcePreference"

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@5e
    .line 1139
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@60
    aget-object v1, v1, v0

    #@62
    invoke-direct {p0, v1}, Lcom/android/server/ConnectivityService;->teardown(Landroid/net/NetworkStateTracker;)Z

    #@65
    .line 1132
    :cond_65
    add-int/lit8 v0, v0, 0x1

    #@67
    goto :goto_1e
.end method

.method private static enforceSystemUid()V
    .registers 3

    #@0
    .prologue
    .line 5143
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 5144
    .local v0, uid:I
    const/16 v1, 0x3e8

    #@6
    if-eq v0, v1, :cond_10

    #@8
    .line 5145
    new-instance v1, Ljava/lang/SecurityException;

    #@a
    const-string v2, "Only available to AID_SYSTEM"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 5147
    :cond_10
    return-void
.end method

.method private enforceTetherAccessPermission()V
    .registers 4

    #@0
    .prologue
    .line 2718
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    #@4
    const-string v2, "ConnectivityService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2721
    return-void
.end method

.method private enforceTetherChangePermission()V
    .registers 4

    #@0
    .prologue
    .line 2712
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v2, "ConnectivityService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2715
    return-void
.end method

.method private getConnectivityChangeDelay()I
    .registers 5

    #@0
    .prologue
    .line 1096
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    .line 1099
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v2, "conn.connectivity_change_delay"

    #@8
    const/16 v3, 0xbb8

    #@a
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@d
    move-result v1

    #@e
    .line 1102
    .local v1, defaultDelay:I
    const-string v2, "connectivity_change_delay"

    #@10
    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@13
    move-result v2

    #@14
    return v2
.end method

.method private getFilteredNetworkInfo(Landroid/net/NetworkStateTracker;I)Landroid/net/NetworkInfo;
    .registers 7
    .parameter "tracker"
    .parameter "uid"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1181
    invoke-interface {p1}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@4
    move-result-object v0

    #@5
    .line 1182
    .local v0, info:Landroid/net/NetworkInfo;
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->isNetworkBlocked(Landroid/net/NetworkStateTracker;I)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_16

    #@b
    .line 1184
    new-instance v1, Landroid/net/NetworkInfo;

    #@d
    invoke-direct {v1, v0}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@10
    .line 1185
    .end local v0           #info:Landroid/net/NetworkInfo;
    .local v1, info:Landroid/net/NetworkInfo;
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->BLOCKED:Landroid/net/NetworkInfo$DetailedState;

    #@12
    invoke-virtual {v1, v2, v3, v3}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@15
    move-object v0, v1

    #@16
    .line 1187
    .end local v1           #info:Landroid/net/NetworkInfo;
    .restart local v0       #info:Landroid/net/NetworkInfo;
    :cond_16
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@18
    if-eqz v2, :cond_20

    #@1a
    .line 1188
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@1c
    invoke-virtual {v2, v0}, Lcom/android/server/net/LockdownVpnTracker;->augmentNetworkInfo(Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    #@1f
    move-result-object v0

    #@20
    .line 1190
    :cond_20
    return-object v0
.end method

.method private getIPtype(Ljava/util/Collection;)I
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 3983
    .local p1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    const/4 v4, 0x0

    #@1
    .line 3984
    .local v4, ipv6:I
    const/4 v3, 0x0

    #@2
    .line 3985
    .local v3, ipv4:I
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_43

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Ljava/net/InetAddress;

    #@12
    .line 3986
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet4Address;

    #@14
    if-eqz v5, :cond_1d

    #@16
    .line 3987
    const/4 v3, 0x1

    #@17
    .line 3988
    const-string v5, "getIPtype() this address list has ipv4"

    #@19
    invoke-static {v5}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@1c
    goto :goto_6

    #@1d
    .line 3990
    :cond_1d
    instance-of v5, v0, Ljava/net/Inet6Address;

    #@1f
    if-eqz v5, :cond_6

    #@21
    move-object v2, v0

    #@22
    .line 3991
    check-cast v2, Ljava/net/Inet6Address;

    #@24
    .line 3992
    .local v2, i6addr:Ljava/net/Inet6Address;
    invoke-virtual {v2}, Ljava/net/Inet6Address;->isAnyLocalAddress()Z

    #@27
    move-result v5

    #@28
    if-nez v5, :cond_6

    #@2a
    invoke-virtual {v2}, Ljava/net/Inet6Address;->isLinkLocalAddress()Z

    #@2d
    move-result v5

    #@2e
    if-nez v5, :cond_6

    #@30
    invoke-virtual {v2}, Ljava/net/Inet6Address;->isLoopbackAddress()Z

    #@33
    move-result v5

    #@34
    if-nez v5, :cond_6

    #@36
    invoke-virtual {v2}, Ljava/net/Inet6Address;->isMulticastAddress()Z

    #@39
    move-result v5

    #@3a
    if-nez v5, :cond_6

    #@3c
    .line 3994
    const/4 v4, 0x2

    #@3d
    .line 3995
    const-string v5, "getIPtype() this address list has ipv6"

    #@3f
    invoke-static {v5}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@42
    goto :goto_6

    #@43
    .line 3999
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v2           #i6addr:Ljava/net/Inet6Address;
    :cond_43
    add-int v5, v3, v4

    #@45
    return v5
.end method

.method private getNetworkInfo(II)Landroid/net/NetworkInfo;
    .registers 6
    .parameter "networkType"
    .parameter "uid"

    #@0
    .prologue
    .line 1243
    const/4 v0, 0x0

    #@1
    .line 1244
    .local v0, info:Landroid/net/NetworkInfo;
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_11

    #@7
    .line 1245
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@9
    aget-object v1, v2, p1

    #@b
    .line 1246
    .local v1, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v1, :cond_11

    #@d
    .line 1247
    invoke-direct {p0, v1, p2}, Lcom/android/server/ConnectivityService;->getFilteredNetworkInfo(Landroid/net/NetworkStateTracker;I)Landroid/net/NetworkInfo;

    #@10
    move-result-object v0

    #@11
    .line 1250
    .end local v1           #tracker:Landroid/net/NetworkStateTracker;
    :cond_11
    return-object v0
.end method

.method private getNetworkStateUnchecked(I)Landroid/net/NetworkState;
    .registers 7
    .parameter "networkType"

    #@0
    .prologue
    .line 1317
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1e

    #@6
    .line 1318
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@8
    aget-object v0, v1, p1

    #@a
    .line 1319
    .local v0, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v0, :cond_1e

    #@c
    .line 1320
    new-instance v1, Landroid/net/NetworkState;

    #@e
    invoke-interface {v0}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@11
    move-result-object v2

    #@12
    invoke-interface {v0}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@15
    move-result-object v3

    #@16
    invoke-interface {v0}, Landroid/net/NetworkStateTracker;->getLinkCapabilities()Landroid/net/LinkCapabilities;

    #@19
    move-result-object v4

    #@1a
    invoke-direct {v1, v2, v3, v4}, Landroid/net/NetworkState;-><init>(Landroid/net/NetworkInfo;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;)V

    #@1d
    .line 1324
    .end local v0           #tracker:Landroid/net/NetworkStateTracker;
    :goto_1d
    return-object v1

    #@1e
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_1d
.end method

.method private getPersistedNetworkPreference()I
    .registers 5

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 1107
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    .line 1109
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v2, "network_preference"

    #@9
    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v1

    #@d
    .line 1111
    .local v1, networkPrefSetting:I
    if-eq v1, v3, :cond_10

    #@f
    .line 1115
    .end local v1           #networkPrefSetting:I
    :goto_f
    return v1

    #@10
    .restart local v1       #networkPrefSetting:I
    :cond_10
    const/4 v1, 0x1

    #@11
    goto :goto_f
.end method

.method private getRestoreDefaultNetworkDelay(I)I
    .registers 5
    .parameter "networkType"

    #@0
    .prologue
    .line 4134
    const-string v2, "android.telephony.apn-restore"

    #@2
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4136
    .local v0, restoreDefaultNetworkDelayStr:Ljava/lang/String;
    if-eqz v0, :cond_18

    #@8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_18

    #@e
    .line 4139
    :try_start_e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_15
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_15} :catch_17

    #@15
    move-result v1

    #@16
    .line 4150
    :cond_16
    :goto_16
    return v1

    #@17
    .line 4140
    :catch_17
    move-exception v2

    #@18
    .line 4144
    :cond_18
    const v1, 0xea60

    #@1b
    .line 4146
    .local v1, ret:I
    const/16 v2, 0x17

    #@1d
    if-gt p1, v2, :cond_16

    #@1f
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@21
    aget-object v2, v2, p1

    #@23
    if-eqz v2, :cond_16

    #@25
    .line 4148
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@27
    aget-object v2, v2, p1

    #@29
    iget v1, v2, Landroid/net/NetworkConfig;->restoreTime:I

    #@2b
    goto :goto_16
.end method

.method private getRestoreVZWAPPNetworkDelay()I
    .registers 3

    #@0
    .prologue
    .line 4155
    const-string v1, "verizon.telephony.appsapn-restore"

    #@2
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4157
    .local v0, restoreVZWAPPNetworkDelayStr:Ljava/lang/String;
    if-eqz v0, :cond_18

    #@8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_18

    #@e
    .line 4160
    :try_start_e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_15
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_15} :catch_17

    #@15
    move-result v1

    #@16
    .line 4165
    :goto_16
    return v1

    #@17
    .line 4161
    :catch_17
    move-exception v1

    #@18
    .line 4165
    :cond_18
    const v1, 0xea60

    #@1b
    goto :goto_16
.end method

.method private getSysid(I)I
    .registers 7
    .parameter "pid"

    #@0
    .prologue
    .line 3860
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetRequestAll:Ljava/util/List;

    #@2
    .line 3861
    .local v2, pids:Ljava/util/List;
    const/16 v3, 0x63

    #@4
    .line 3863
    .local v3, sysid:I
    const/4 v0, 0x0

    #@5
    .local v0, jj:I
    :goto_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@8
    move-result v4

    #@9
    if-ge v0, v4, :cond_18

    #@b
    .line 3865
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/lang/Integer;

    #@11
    .line 3866
    .local v1, pid_inlist:Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@14
    move-result v4

    #@15
    if-ne v4, p1, :cond_19

    #@17
    .line 3868
    move v3, v0

    #@18
    .line 3872
    .end local v1           #pid_inlist:Ljava/lang/Integer;
    :cond_18
    return v3

    #@19
    .line 3863
    .restart local v1       #pid_inlist:Ljava/lang/Integer;
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_5
.end method

.method private handleApplyDefaultProxy(Landroid/net/ProxyProperties;)V
    .registers 4
    .parameter "proxy"

    #@0
    .prologue
    .line 4737
    if-eqz p1, :cond_d

    #@2
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_d

    #@c
    .line 4738
    const/4 p1, 0x0

    #@d
    .line 4740
    :cond_d
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyLock:Ljava/lang/Object;

    #@f
    monitor-enter v1

    #@10
    .line 4741
    :try_start_10
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@12
    if-eqz v0, :cond_1e

    #@14
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@16
    invoke-virtual {v0, p1}, Landroid/net/ProxyProperties;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_1e

    #@1c
    monitor-exit v1

    #@1d
    .line 4749
    :goto_1d
    return-void

    #@1e
    .line 4742
    :cond_1e
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@20
    if-ne v0, p1, :cond_27

    #@22
    monitor-exit v1

    #@23
    goto :goto_1d

    #@24
    .line 4748
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_10 .. :try_end_26} :catchall_24

    #@26
    throw v0

    #@27
    .line 4743
    :cond_27
    :try_start_27
    iput-object p1, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@29
    .line 4745
    iget-boolean v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyDisabled:Z

    #@2b
    if-nez v0, :cond_30

    #@2d
    .line 4746
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->sendProxyBroadcast(Landroid/net/ProxyProperties;)V

    #@30
    .line 4748
    :cond_30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_27 .. :try_end_31} :catchall_24

    #@31
    goto :goto_1d
.end method

.method private handleCaptivePortalTrackerCheck(Landroid/net/NetworkInfo;)V
    .registers 6
    .parameter "info"

    #@0
    .prologue
    .line 3343
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Captive portal check "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@16
    .line 3344
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@19
    move-result v1

    #@1a
    .line 3345
    .local v1, type:I
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@1c
    aget-object v0, v2, v1

    #@1e
    .line 3346
    .local v0, thisNet:Landroid/net/NetworkStateTracker;
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@20
    aget-object v2, v2, v1

    #@22
    invoke-virtual {v2}, Landroid/net/NetworkConfig;->isDefault()Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_7a

    #@28
    .line 3347
    iget v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@2a
    const/4 v3, -0x1

    #@2b
    if-eq v2, v3, :cond_7a

    #@2d
    iget v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@2f
    if-eq v2, v1, :cond_7a

    #@31
    .line 3348
    invoke-direct {p0, v1}, Lcom/android/server/ConnectivityService;->isNewNetTypePreferredOverCurrentNetType(I)Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_5c

    #@37
    .line 3349
    new-instance v2, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v3, "Captive check on "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@51
    .line 3350
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mCaptivePortalTracker:Landroid/net/CaptivePortalTracker;

    #@53
    new-instance v3, Landroid/net/NetworkInfo;

    #@55
    invoke-direct {v3, p1}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@58
    invoke-virtual {v2, v3}, Landroid/net/CaptivePortalTracker;->detectCaptivePortal(Landroid/net/NetworkInfo;)V

    #@5b
    .line 3361
    :goto_5b
    return-void

    #@5c
    .line 3353
    :cond_5c
    new-instance v2, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v3, "Tear down low priority net "

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v2

    #@73
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@76
    .line 3354
    invoke-direct {p0, v0}, Lcom/android/server/ConnectivityService;->teardown(Landroid/net/NetworkStateTracker;)Z

    #@79
    goto :goto_5b

    #@7a
    .line 3360
    :cond_7a
    invoke-interface {v0}, Landroid/net/NetworkStateTracker;->captivePortalCheckComplete()V

    #@7d
    goto :goto_5b
.end method

.method private handleConnect(Landroid/net/NetworkInfo;)V
    .registers 14
    .parameter "info"

    #@0
    .prologue
    .line 3173
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@3
    move-result v2

    #@4
    .line 3175
    .local v2, newNetType:I
    invoke-direct {p0, v2}, Lcom/android/server/ConnectivityService;->setupDataActivityTracking(I)V

    #@7
    .line 3178
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isFailover()Z

    #@a
    move-result v1

    #@b
    .line 3179
    .local v1, isFailover:Z
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@d
    aget-object v6, v7, v2

    #@f
    .line 3180
    .local v6, thisNet:Landroid/net/NetworkStateTracker;
    invoke-interface {v6}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    .line 3182
    .local v5, thisIface:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@19
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_TOAST_ON_WIFI_OFF_UPLUS:Z

    #@1b
    const/4 v8, 0x1

    #@1c
    if-eq v7, v8, :cond_25

    #@1e
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@20
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_PAYPOPUP_KT:Z

    #@22
    const/4 v8, 0x1

    #@23
    if-ne v7, v8, :cond_30

    #@25
    .line 3183
    :cond_25
    const/4 v7, 0x1

    #@26
    if-ne v2, v7, :cond_30

    #@28
    .line 3184
    const/4 v7, 0x1

    #@29
    iput-boolean v7, p0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@2b
    .line 3185
    const-string v7, "[LG_DATA]WIFI IS CONNECTED"

    #@2d
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@30
    .line 3192
    :cond_30
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@32
    aget-object v7, v7, v2

    #@34
    invoke-virtual {v7}, Landroid/net/NetworkConfig;->isDefault()Z

    #@37
    move-result v7

    #@38
    if-eqz v7, :cond_12a

    #@3a
    .line 3193
    iget v7, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@3c
    const/4 v8, -0x1

    #@3d
    if-eq v7, v8, :cond_a6

    #@3f
    iget v7, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@41
    if-eq v7, v2, :cond_a6

    #@43
    .line 3194
    invoke-direct {p0, v2}, Lcom/android/server/ConnectivityService;->isNewNetTypePreferredOverCurrentNetType(I)Z

    #@46
    move-result v7

    #@47
    if-eqz v7, :cond_155

    #@49
    .line 3196
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@4b
    iget v8, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@4d
    aget-object v4, v7, v8

    #@4f
    .line 3199
    .local v4, otherNet:Landroid/net/NetworkStateTracker;
    new-instance v7, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v8, "Policy requires "

    #@56
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v7

    #@5a
    invoke-interface {v4}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@5d
    move-result-object v8

    #@5e
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@61
    move-result-object v8

    #@62
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v7

    #@66
    const-string v8, " teardown"

    #@68
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v7

    #@6c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v7

    #@70
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@73
    .line 3203
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@75
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@77
    if-eqz v7, :cond_146

    #@79
    iget v7, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@7b
    const/4 v8, 0x1

    #@7c
    if-ne v7, v8, :cond_146

    #@7e
    iget v7, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@80
    if-nez v7, :cond_146

    #@82
    const/4 v7, 0x1

    #@83
    if-ne v2, v7, :cond_146

    #@85
    .line 3208
    new-instance v7, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v8, "minjeon] SKIP mobile tear down during WiFi"

    #@8c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v7

    #@90
    invoke-interface {v4}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@93
    move-result-object v8

    #@94
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@97
    move-result-object v8

    #@98
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v7

    #@9c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v7

    #@a0
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@a3
    .line 3209
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->notifyFakeDataStateChangedToDisconnect()V

    #@a6
    .line 3237
    .end local v4           #otherNet:Landroid/net/NetworkStateTracker;
    :cond_a6
    monitor-enter p0

    #@a7
    .line 3241
    :try_start_a7
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@a9
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@ac
    move-result v7

    #@ad
    if-eqz v7, :cond_c1

    #@af
    .line 3242
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@b1
    iget-object v8, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@b3
    const/16 v9, 0x8

    #@b5
    iget v10, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockSerialNumber:I

    #@b7
    const/4 v11, 0x0

    #@b8
    invoke-virtual {v8, v9, v10, v11}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@bb
    move-result-object v8

    #@bc
    const-wide/16 v9, 0x3e8

    #@be
    invoke-virtual {v7, v8, v9, v10}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c1
    .line 3247
    :cond_c1
    monitor-exit p0
    :try_end_c2
    .catchall {:try_start_a7 .. :try_end_c2} :catchall_189

    #@c2
    .line 3248
    iput v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@c4
    .line 3254
    const/16 v0, 0x63

    #@c6
    .line 3255
    .local v0, activenetwork:I
    const-string v7, "ro.build.target_operator"

    #@c8
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@cb
    move-result-object v3

    #@cc
    .line 3256
    .local v3, operator:Ljava/lang/String;
    if-eqz v3, :cond_ea

    #@ce
    const-string v7, "SPR"

    #@d0
    invoke-virtual {v3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d3
    move-result v7

    #@d4
    if-eqz v7, :cond_ea

    #@d6
    .line 3257
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@d9
    move-result v7

    #@da
    const/4 v8, 0x1

    #@db
    if-eq v7, v8, :cond_e1

    #@dd
    .line 3258
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtype()I

    #@e0
    move-result v0

    #@e1
    .line 3260
    :cond_e1
    const-string v7, "net.activenetwork"

    #@e3
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e6
    move-result-object v8

    #@e7
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@ea
    .line 3262
    :cond_ea
    const-string v7, "ConnectivityService"

    #@ec
    new-instance v8, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v9, "now we set activenetwork : "

    #@f3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v8

    #@f7
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v8

    #@fb
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v8

    #@ff
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@102
    .line 3265
    const/4 v7, 0x0

    #@103
    iput v7, p0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@105
    .line 3266
    iget v7, p0, Lcom/android/server/ConnectivityService;->mDefaultConnectionSequence:I

    #@107
    add-int/lit8 v7, v7, 0x1

    #@109
    iput v7, p0, Lcom/android/server/ConnectivityService;->mDefaultConnectionSequence:I

    #@10b
    .line 3267
    const/4 v7, 0x0

    #@10c
    iput-boolean v7, p0, Lcom/android/server/ConnectivityService;->mInetConditionChangeInFlight:Z

    #@10e
    .line 3271
    iget v7, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@110
    const/4 v8, -0x1

    #@111
    if-eq v7, v8, :cond_12a

    #@113
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->isRCSeWorking()Z

    #@116
    move-result v7

    #@117
    if-eqz v7, :cond_12a

    #@119
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@11b
    const/16 v8, 0x15

    #@11d
    aget-object v7, v7, v8

    #@11f
    if-eqz v7, :cond_12a

    #@121
    .line 3273
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@123
    const/16 v8, 0x15

    #@125
    aget-object v7, v7, v8

    #@127
    invoke-direct {p0, v7}, Lcom/android/server/ConnectivityService;->teardown(Landroid/net/NetworkStateTracker;)Z

    #@12a
    .line 3277
    .end local v0           #activenetwork:I
    .end local v3           #operator:Ljava/lang/String;
    :cond_12a
    const/4 v7, 0x0

    #@12b
    invoke-interface {v6, v7}, Landroid/net/NetworkStateTracker;->setTeardownRequested(Z)V

    #@12e
    .line 3278
    invoke-virtual {p0, v6}, Lcom/android/server/ConnectivityService;->updateNetworkSettings(Landroid/net/NetworkStateTracker;)V

    #@131
    .line 3279
    const/4 v7, 0x0

    #@132
    invoke-direct {p0, v2, v7}, Lcom/android/server/ConnectivityService;->handleConnectivityChange(IZ)V

    #@135
    .line 3280
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->getConnectivityChangeDelay()I

    #@138
    move-result v7

    #@139
    invoke-direct {p0, p1, v7}, Lcom/android/server/ConnectivityService;->sendConnectedBroadcastDelayed(Landroid/net/NetworkInfo;I)V

    #@13c
    .line 3283
    if-eqz v5, :cond_145

    #@13e
    .line 3285
    :try_start_13e
    invoke-static {}, Lcom/android/server/am/BatteryStatsService;->getService()Lcom/android/internal/app/IBatteryStats;

    #@141
    move-result-object v7

    #@142
    invoke-interface {v7, v5, v2}, Lcom/android/internal/app/IBatteryStats;->noteNetworkInterfaceType(Ljava/lang/String;I)V
    :try_end_145
    .catch Landroid/os/RemoteException; {:try_start_13e .. :try_end_145} :catch_18c

    #@145
    .line 3290
    :cond_145
    :goto_145
    return-void

    #@146
    .line 3212
    .restart local v4       #otherNet:Landroid/net/NetworkStateTracker;
    :cond_146
    invoke-direct {p0, v4}, Lcom/android/server/ConnectivityService;->teardown(Landroid/net/NetworkStateTracker;)Z

    #@149
    move-result v7

    #@14a
    if-nez v7, :cond_a6

    #@14c
    .line 3213
    const-string v7, "Network declined teardown request"

    #@14e
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@151
    .line 3214
    invoke-direct {p0, v6}, Lcom/android/server/ConnectivityService;->teardown(Landroid/net/NetworkStateTracker;)Z

    #@154
    goto :goto_145

    #@155
    .line 3221
    .end local v4           #otherNet:Landroid/net/NetworkStateTracker;
    :cond_155
    new-instance v7, Ljava/lang/StringBuilder;

    #@157
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@15a
    const-string v8, "Not broadcasting CONNECT_ACTION to torn down network "

    #@15c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v7

    #@160
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@163
    move-result-object v8

    #@164
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v7

    #@168
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v7

    #@16c
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@16f
    .line 3225
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@171
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@173
    if-eqz v7, :cond_185

    #@175
    iget v7, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@177
    const/4 v8, 0x1

    #@178
    if-ne v7, v8, :cond_185

    #@17a
    iget v7, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@17c
    const/4 v8, 0x1

    #@17d
    if-ne v7, v8, :cond_185

    #@17f
    if-nez v2, :cond_185

    #@181
    .line 3229
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->notifyFakeDataStateChangedToDisconnect()V

    #@184
    goto :goto_145

    #@185
    .line 3233
    :cond_185
    invoke-direct {p0, v6}, Lcom/android/server/ConnectivityService;->teardown(Landroid/net/NetworkStateTracker;)Z

    #@188
    goto :goto_145

    #@189
    .line 3247
    :catchall_189
    move-exception v7

    #@18a
    :try_start_18a
    monitor-exit p0
    :try_end_18b
    .catchall {:try_start_18a .. :try_end_18b} :catchall_189

    #@18b
    throw v7

    #@18c
    .line 3286
    :catch_18c
    move-exception v7

    #@18d
    goto :goto_145
.end method

.method private handleConnectionFailure(Landroid/net/NetworkInfo;)V
    .registers 14
    .parameter "info"

    #@0
    .prologue
    const/4 v11, -0x1

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 3031
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@5
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@8
    move-result v8

    #@9
    aget-object v7, v7, v8

    #@b
    invoke-interface {v7, v9}, Landroid/net/NetworkStateTracker;->setTeardownRequested(Z)V

    #@e
    .line 3033
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@11
    move-result v3

    #@12
    .line 3034
    .local v3, netType:I
    if-ne v3, v10, :cond_1d

    #@14
    .line 3035
    const-string v7, "ConnectivityService"

    #@16
    const-string v8, "[LGE_DATA]handleConnectionFailure: mIsWifiConnected = false"

    #@18
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 3036
    iput-boolean v9, p0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@1d
    .line 3040
    :cond_1d
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    .line 3041
    .local v4, reason:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    .line 3044
    .local v0, extraInfo:Ljava/lang/String;
    if-nez v4, :cond_116

    #@27
    .line 3045
    const-string v5, "."

    #@29
    .line 3049
    .local v5, reasonText:Ljava/lang/String;
    :goto_29
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v8, "Attempt to connect to "

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v7

    #@3c
    const-string v8, " failed"

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v7

    #@4a
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@4d
    .line 3051
    new-instance v2, Landroid/content/Intent;

    #@4f
    const-string v7, "android.net.conn.CONNECTIVITY_CHANGE"

    #@51
    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@54
    .line 3052
    .local v2, intent:Landroid/content/Intent;
    const-string v7, "networkInfo"

    #@56
    new-instance v8, Landroid/net/NetworkInfo;

    #@58
    invoke-direct {v8, p1}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@5b
    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@5e
    .line 3053
    const-string v7, "networkType"

    #@60
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@63
    move-result v8

    #@64
    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@67
    .line 3054
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@6a
    move-result-object v7

    #@6b
    if-nez v7, :cond_72

    #@6d
    .line 3055
    const-string v7, "noConnectivity"

    #@6f
    invoke-virtual {v2, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@72
    .line 3057
    :cond_72
    if-eqz v4, :cond_79

    #@74
    .line 3058
    const-string v7, "reason"

    #@76
    invoke-virtual {v2, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@79
    .line 3060
    :cond_79
    if-eqz v0, :cond_80

    #@7b
    .line 3061
    const-string v7, "extraInfo"

    #@7d
    invoke-virtual {v2, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@80
    .line 3063
    :cond_80
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isFailover()Z

    #@83
    move-result v7

    #@84
    if-eqz v7, :cond_8e

    #@86
    .line 3064
    const-string v7, "isFailover"

    #@88
    invoke-virtual {v2, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@8b
    .line 3065
    invoke-virtual {p1, v9}, Landroid/net/NetworkInfo;->setFailover(Z)V

    #@8e
    .line 3068
    :cond_8e
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@90
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@93
    move-result v8

    #@94
    aget-object v7, v7, v8

    #@96
    invoke-virtual {v7}, Landroid/net/NetworkConfig;->isDefault()Z

    #@99
    move-result v7

    #@9a
    if-eqz v7, :cond_b6

    #@9c
    .line 3069
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@9f
    move-result v7

    #@a0
    invoke-direct {p0, v7}, Lcom/android/server/ConnectivityService;->tryFailover(I)V

    #@a3
    .line 3070
    iget v7, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@a5
    if-eq v7, v11, :cond_131

    #@a7
    .line 3071
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@a9
    iget v8, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@ab
    aget-object v7, v7, v8

    #@ad
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@b0
    move-result-object v6

    #@b1
    .line 3072
    .local v6, switchTo:Landroid/net/NetworkInfo;
    const-string v7, "otherNetwork"

    #@b3
    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@b6
    .line 3079
    .end local v6           #switchTo:Landroid/net/NetworkInfo;
    :cond_b6
    :goto_b6
    const-string v7, "inetCondition"

    #@b8
    iget v8, p0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@ba
    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@bd
    .line 3082
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@bf
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SMCAUSE_NOTIFY:Z

    #@c1
    if-eqz v7, :cond_f4

    #@c3
    .line 3084
    const-string v7, "smCause"

    #@c5
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSmCause()I

    #@c8
    move-result v8

    #@c9
    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@cc
    .line 3085
    new-instance v7, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v8, "handleConnectionFailure(): SM Cause is added  to CONNECTIVITY_ACTION intent for ["

    #@d3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v7

    #@d7
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@da
    move-result-object v8

    #@db
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v7

    #@df
    const-string v8, "], SM cause : "

    #@e1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v7

    #@e5
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSmCause()I

    #@e8
    move-result v8

    #@e9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v7

    #@ed
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v7

    #@f1
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@f4
    .line 3089
    :cond_f4
    new-instance v1, Landroid/content/Intent;

    #@f6
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@f9
    .line 3090
    .local v1, immediateIntent:Landroid/content/Intent;
    const-string v7, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@fb
    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@fe
    .line 3091
    invoke-direct {p0, v1}, Lcom/android/server/ConnectivityService;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@101
    .line 3092
    invoke-direct {p0, v2}, Lcom/android/server/ConnectivityService;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@104
    .line 3097
    iget v7, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@106
    if-eq v7, v11, :cond_115

    #@108
    .line 3098
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@10a
    iget v8, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@10c
    aget-object v7, v7, v8

    #@10e
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@111
    move-result-object v7

    #@112
    invoke-virtual {p0, v7}, Lcom/android/server/ConnectivityService;->sendConnectedBroadcast(Landroid/net/NetworkInfo;)V

    #@115
    .line 3100
    :cond_115
    return-void

    #@116
    .line 3047
    .end local v1           #immediateIntent:Landroid/content/Intent;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v5           #reasonText:Ljava/lang/String;
    :cond_116
    new-instance v7, Ljava/lang/StringBuilder;

    #@118
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    const-string v8, " ("

    #@11d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v7

    #@121
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v7

    #@125
    const-string v8, ")."

    #@127
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v7

    #@12b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12e
    move-result-object v5

    #@12f
    .restart local v5       #reasonText:Ljava/lang/String;
    goto/16 :goto_29

    #@131
    .line 3074
    .restart local v2       #intent:Landroid/content/Intent;
    :cond_131
    iput v9, p0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@133
    .line 3075
    const-string v7, "noConnectivity"

    #@135
    invoke-virtual {v2, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@138
    goto/16 :goto_b6
.end method

.method private handleConnectivityChange(IZ)V
    .registers 18
    .parameter "netType"
    .parameter "doReset"

    #@0
    .prologue
    .line 3469
    if-eqz p2, :cond_118

    #@2
    const/4 v12, 0x3

    #@3
    .line 3476
    .local v12, resetMask:I
    :goto_3
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@5
    iget-boolean v13, v13, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@7
    if-eqz v13, :cond_11b

    #@9
    if-nez p1, :cond_11b

    #@b
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@d
    aget-object v13, v13, p1

    #@f
    invoke-interface {v13}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@12
    move-result-object v13

    #@13
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->isConnected()Z

    #@16
    move-result v13

    #@17
    if-eqz v13, :cond_11b

    #@19
    iget v13, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@1b
    const/4 v14, 0x1

    #@1c
    if-ne v13, v14, :cond_11b

    #@1e
    .line 3480
    const-string v13, "handleConnectivityChange:[DOCOMO_DUAL_CONNECTIVITY] skip setting mobile DNS during wifi connected"

    #@20
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@23
    .line 3488
    :goto_23
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mCurrentLinkProperties:[Landroid/net/LinkProperties;

    #@25
    aget-object v4, v13, p1

    #@27
    .line 3489
    .local v4, curLp:Landroid/net/LinkProperties;
    const/4 v10, 0x0

    #@28
    .line 3493
    .local v10, newLp:Landroid/net/LinkProperties;
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2a
    aget-object v13, v13, p1

    #@2c
    invoke-interface {v13}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@2f
    move-result-object v13

    #@30
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->isConnected()Z

    #@33
    move-result v13

    #@34
    if-nez v13, :cond_4a

    #@36
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@38
    iget-boolean v13, v13, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_KEEP_ROUTE_INFO_ON_SUSPEND_VZW:Z

    #@3a
    if-eqz v13, :cond_274

    #@3c
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@3e
    aget-object v13, v13, p1

    #@40
    invoke-interface {v13}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@43
    move-result-object v13

    #@44
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->isConnectedOrSuspended()Z

    #@47
    move-result v13

    #@48
    if-eqz v13, :cond_274

    #@4a
    .line 3497
    :cond_4a
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@4c
    aget-object v13, v13, p1

    #@4e
    invoke-interface {v13}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@51
    move-result-object v10

    #@52
    .line 3499
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@54
    iget-boolean v13, v13, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@56
    if-eqz v13, :cond_8d

    #@58
    const/4 v13, 0x4

    #@59
    move/from16 v0, p1

    #@5b
    if-ne v0, v13, :cond_8d

    #@5d
    .line 3502
    invoke-virtual {p0, v10}, Lcom/android/server/ConnectivityService;->isIpv4Connected(Landroid/net/LinkProperties;)Z

    #@60
    move-result v13

    #@61
    if-eqz v13, :cond_75

    #@63
    .line 3504
    const-string v1, "8.8.8.8"

    #@65
    .line 3505
    .local v1, DNS_DEFAULT_SERVER1:Ljava/lang/String;
    const-string v2, "8.8.4.4"

    #@67
    .line 3506
    .local v2, DNS_DEFAULT_SERVER2:Ljava/lang/String;
    invoke-static {v1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@6a
    move-result-object v13

    #@6b
    invoke-virtual {v10, v13}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@6e
    .line 3507
    invoke-static {v2}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@71
    move-result-object v13

    #@72
    invoke-virtual {v10, v13}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@75
    .line 3510
    .end local v1           #DNS_DEFAULT_SERVER1:Ljava/lang/String;
    .end local v2           #DNS_DEFAULT_SERVER2:Ljava/lang/String;
    :cond_75
    invoke-virtual {p0, v10}, Lcom/android/server/ConnectivityService;->isIpv6Connected(Landroid/net/LinkProperties;)Z

    #@78
    move-result v13

    #@79
    if-eqz v13, :cond_8d

    #@7b
    .line 3512
    const-string v1, "2001:4860:4860::8888"

    #@7d
    .line 3513
    .restart local v1       #DNS_DEFAULT_SERVER1:Ljava/lang/String;
    const-string v2, "2001:4860:4860::8844"

    #@7f
    .line 3514
    .restart local v2       #DNS_DEFAULT_SERVER2:Ljava/lang/String;
    invoke-static {v1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@82
    move-result-object v13

    #@83
    invoke-virtual {v10, v13}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@86
    .line 3515
    invoke-static {v2}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@89
    move-result-object v13

    #@8a
    invoke-virtual {v10, v13}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@8d
    .line 3521
    .end local v1           #DNS_DEFAULT_SERVER1:Ljava/lang/String;
    .end local v2           #DNS_DEFAULT_SERVER2:Ljava/lang/String;
    :cond_8d
    new-instance v13, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v14, "handleConnectivityChange: changed linkProperty["

    #@94
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v13

    #@98
    move/from16 v0, p1

    #@9a
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v13

    #@9e
    const-string v14, "]:"

    #@a0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v13

    #@a4
    const-string v14, " doReset="

    #@a6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v13

    #@aa
    move/from16 v0, p2

    #@ac
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@af
    move-result-object v13

    #@b0
    const-string v14, " resetMask="

    #@b2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v13

    #@b6
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v13

    #@ba
    const-string v14, "\n   curLp="

    #@bc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v13

    #@c0
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v13

    #@c4
    const-string v14, "\n   newLp="

    #@c6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v13

    #@ca
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v13

    #@ce
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v13

    #@d2
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@d5
    .line 3527
    if-eqz v4, :cond_16c

    #@d7
    .line 3528
    invoke-virtual {v4, v10}, Landroid/net/LinkProperties;->isIdenticalInterfaceName(Landroid/net/LinkProperties;)Z

    #@da
    move-result v13

    #@db
    if-eqz v13, :cond_249

    #@dd
    .line 3529
    invoke-virtual {v4, v10}, Landroid/net/LinkProperties;->compareAddresses(Landroid/net/LinkProperties;)Landroid/net/LinkProperties$CompareResult;

    #@e0
    move-result-object v3

    #@e1
    .line 3530
    .local v3, car:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/LinkAddress;>;"
    iget-object v13, v3, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@e3
    invoke-interface {v13}, Ljava/util/Collection;->size()I

    #@e6
    move-result v13

    #@e7
    if-nez v13, :cond_f1

    #@e9
    iget-object v13, v3, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@eb
    invoke-interface {v13}, Ljava/util/Collection;->size()I

    #@ee
    move-result v13

    #@ef
    if-eqz v13, :cond_21f

    #@f1
    .line 3531
    :cond_f1
    iget-object v13, v3, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@f3
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f6
    move-result-object v6

    #@f7
    .local v6, i$:Ljava/util/Iterator;
    :cond_f7
    :goto_f7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@fa
    move-result v13

    #@fb
    if-eqz v13, :cond_13a

    #@fd
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@100
    move-result-object v8

    #@101
    check-cast v8, Landroid/net/LinkAddress;

    #@103
    .line 3532
    .local v8, linkAddr:Landroid/net/LinkAddress;
    invoke-virtual {v8}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@106
    move-result-object v13

    #@107
    instance-of v13, v13, Ljava/net/Inet4Address;

    #@109
    if-eqz v13, :cond_10d

    #@10b
    .line 3533
    or-int/lit8 v12, v12, 0x1

    #@10d
    .line 3535
    :cond_10d
    invoke-virtual {v8}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@110
    move-result-object v13

    #@111
    instance-of v13, v13, Ljava/net/Inet6Address;

    #@113
    if-eqz v13, :cond_f7

    #@115
    .line 3536
    or-int/lit8 v12, v12, 0x2

    #@117
    goto :goto_f7

    #@118
    .line 3469
    .end local v3           #car:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/LinkAddress;>;"
    .end local v4           #curLp:Landroid/net/LinkProperties;
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v8           #linkAddr:Landroid/net/LinkAddress;
    .end local v10           #newLp:Landroid/net/LinkProperties;
    .end local v12           #resetMask:I
    :cond_118
    const/4 v12, 0x0

    #@119
    goto/16 :goto_3

    #@11b
    .line 3482
    .restart local v12       #resetMask:I
    :cond_11b
    new-instance v13, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v14, "handleConnectivityChange: set DNS for "

    #@122
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v13

    #@126
    invoke-static/range {p1 .. p1}, Landroid/net/ConnectivityManager;->getNetworkTypeName(I)Ljava/lang/String;

    #@129
    move-result-object v14

    #@12a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v13

    #@12e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@131
    move-result-object v13

    #@132
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@135
    .line 3484
    invoke-direct/range {p0 .. p1}, Lcom/android/server/ConnectivityService;->handleDnsConfigurationChange(I)V

    #@138
    goto/16 :goto_23

    #@13a
    .line 3540
    .restart local v3       #car:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/LinkAddress;>;"
    .restart local v4       #curLp:Landroid/net/LinkProperties;
    .restart local v6       #i$:Ljava/util/Iterator;
    .restart local v10       #newLp:Landroid/net/LinkProperties;
    :cond_13a
    new-instance v13, Ljava/lang/StringBuilder;

    #@13c
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@13f
    const-string v14, "handleConnectivityChange: addresses changed linkProperty["

    #@141
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v13

    #@145
    move/from16 v0, p1

    #@147
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v13

    #@14b
    const-string v14, "]:"

    #@14d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v13

    #@151
    const-string v14, " resetMask="

    #@153
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v13

    #@157
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v13

    #@15b
    const-string v14, "\n   car="

    #@15d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v13

    #@161
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v13

    #@165
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v13

    #@169
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@16c
    .line 3560
    .end local v3           #car:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/LinkAddress;>;"
    .end local v6           #i$:Ljava/util/Iterator;
    :cond_16c
    :goto_16c
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@16e
    aget-object v13, v13, p1

    #@170
    invoke-virtual {v13}, Landroid/net/NetworkConfig;->isDefault()Z

    #@173
    move-result v13

    #@174
    if-eqz v13, :cond_17d

    #@176
    .line 3561
    invoke-virtual {v10}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@179
    move-result-object v13

    #@17a
    invoke-direct {p0, v13}, Lcom/android/server/ConnectivityService;->handleApplyDefaultProxy(Landroid/net/ProxyProperties;)V

    #@17d
    .line 3571
    :cond_17d
    :goto_17d
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mCurrentLinkProperties:[Landroid/net/LinkProperties;

    #@17f
    aput-object v10, v13, p1

    #@181
    .line 3572
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@183
    aget-object v13, v13, p1

    #@185
    invoke-virtual {v13}, Landroid/net/NetworkConfig;->isDefault()Z

    #@188
    move-result v13

    #@189
    invoke-direct {p0, v10, v4, v13}, Lcom/android/server/ConnectivityService;->updateRoutes(Landroid/net/LinkProperties;Landroid/net/LinkProperties;Z)Z

    #@18c
    move-result v11

    #@18d
    .line 3574
    .local v11, resetDns:Z
    if-nez v12, :cond_191

    #@18f
    if-eqz v11, :cond_1f7

    #@191
    .line 3575
    :cond_191
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@193
    aget-object v13, v13, p1

    #@195
    invoke-interface {v13}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@198
    move-result-object v9

    #@199
    .line 3576
    .local v9, linkProperties:Landroid/net/LinkProperties;
    if-eqz v9, :cond_1f7

    #@19b
    .line 3577
    invoke-virtual {v9}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@19e
    move-result-object v7

    #@19f
    .line 3578
    .local v7, iface:Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a2
    move-result v13

    #@1a3
    if-nez v13, :cond_1f7

    #@1a5
    .line 3579
    if-eqz v12, :cond_1da

    #@1a7
    .line 3580
    new-instance v13, Ljava/lang/StringBuilder;

    #@1a9
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1ac
    const-string v14, "resetConnections("

    #@1ae
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v13

    #@1b2
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v13

    #@1b6
    const-string v14, ", "

    #@1b8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v13

    #@1bc
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v13

    #@1c0
    const-string v14, ")"

    #@1c2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v13

    #@1c6
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c9
    move-result-object v13

    #@1ca
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@1cd
    .line 3581
    invoke-static {v7, v12}, Landroid/net/NetworkUtils;->resetConnections(Ljava/lang/String;I)I

    #@1d0
    .line 3585
    and-int/lit8 v13, v12, 0x1

    #@1d2
    if-eqz v13, :cond_1da

    #@1d4
    .line 3586
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@1d6
    const/4 v14, 0x0

    #@1d7
    invoke-virtual {v13, v7, v14}, Lcom/android/server/connectivity/Vpn;->interfaceStatusChanged(Ljava/lang/String;Z)V

    #@1da
    .line 3589
    :cond_1da
    if-eqz v11, :cond_1f7

    #@1dc
    .line 3590
    new-instance v13, Ljava/lang/StringBuilder;

    #@1de
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1e1
    const-string v14, "resetting DNS cache for "

    #@1e3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v13

    #@1e7
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ea
    move-result-object v13

    #@1eb
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ee
    move-result-object v13

    #@1ef
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@1f2
    .line 3592
    :try_start_1f2
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@1f4
    invoke-interface {v13, v7}, Landroid/os/INetworkManagementService;->flushInterfaceDnsCache(Ljava/lang/String;)V
    :try_end_1f7
    .catch Ljava/lang/Exception; {:try_start_1f2 .. :try_end_1f7} :catch_2ba

    #@1f7
    .line 3605
    .end local v7           #iface:Ljava/lang/String;
    .end local v9           #linkProperties:Landroid/net/LinkProperties;
    :cond_1f7
    :goto_1f7
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@1f9
    aget-object v13, v13, p1

    #@1fb
    invoke-interface {v13}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@1fe
    move-result-object v13

    #@1ff
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@202
    move-result-object v13

    #@203
    const-string v14, "linkPropertiesChanged"

    #@205
    invoke-static {v13, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@208
    move-result v13

    #@209
    if-eqz v13, :cond_21e

    #@20b
    .line 3607
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@20e
    move-result v13

    #@20f
    if-eqz v13, :cond_21e

    #@211
    .line 3608
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@213
    iget-object v14, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@215
    aget-object v14, v14, p1

    #@217
    invoke-interface {v14}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@21a
    move-result-object v14

    #@21b
    invoke-virtual {v13, v14}, Lcom/android/server/connectivity/Tethering;->handleTetherIfaceChange(Landroid/net/NetworkInfo;)V

    #@21e
    .line 3611
    :cond_21e
    return-void

    #@21f
    .line 3546
    .end local v11           #resetDns:Z
    .restart local v3       #car:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/LinkAddress;>;"
    :cond_21f
    new-instance v13, Ljava/lang/StringBuilder;

    #@221
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@224
    const-string v14, "handleConnectivityChange: address are the same reset per doReset linkProperty["

    #@226
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v13

    #@22a
    move/from16 v0, p1

    #@22c
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v13

    #@230
    const-string v14, "]:"

    #@232
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@235
    move-result-object v13

    #@236
    const-string v14, " resetMask="

    #@238
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v13

    #@23c
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23f
    move-result-object v13

    #@240
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@243
    move-result-object v13

    #@244
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@247
    goto/16 :goto_16c

    #@249
    .line 3552
    .end local v3           #car:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/LinkAddress;>;"
    :cond_249
    const/4 v12, 0x3

    #@24a
    .line 3554
    new-instance v13, Ljava/lang/StringBuilder;

    #@24c
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@24f
    const-string v14, "handleConnectivityChange: interface not not equivalent reset both linkProperty["

    #@251
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@254
    move-result-object v13

    #@255
    move/from16 v0, p1

    #@257
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v13

    #@25b
    const-string v14, "]:"

    #@25d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@260
    move-result-object v13

    #@261
    const-string v14, " resetMask="

    #@263
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@266
    move-result-object v13

    #@267
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v13

    #@26b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26e
    move-result-object v13

    #@26f
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@272
    goto/16 :goto_16c

    #@274
    .line 3565
    :cond_274
    new-instance v13, Ljava/lang/StringBuilder;

    #@276
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@279
    const-string v14, "handleConnectivityChange: changed linkProperty["

    #@27b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v13

    #@27f
    move/from16 v0, p1

    #@281
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@284
    move-result-object v13

    #@285
    const-string v14, "]:"

    #@287
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28a
    move-result-object v13

    #@28b
    const-string v14, " doReset="

    #@28d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@290
    move-result-object v13

    #@291
    move/from16 v0, p2

    #@293
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@296
    move-result-object v13

    #@297
    const-string v14, " resetMask="

    #@299
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29c
    move-result-object v13

    #@29d
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a0
    move-result-object v13

    #@2a1
    const-string v14, "\n  curLp="

    #@2a3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a6
    move-result-object v13

    #@2a7
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2aa
    move-result-object v13

    #@2ab
    const-string v14, "\n  newLp= null"

    #@2ad
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b0
    move-result-object v13

    #@2b1
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b4
    move-result-object v13

    #@2b5
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2b8
    goto/16 :goto_17d

    #@2ba
    .line 3593
    .restart local v7       #iface:Ljava/lang/String;
    .restart local v9       #linkProperties:Landroid/net/LinkProperties;
    .restart local v11       #resetDns:Z
    :catch_2ba
    move-exception v5

    #@2bb
    .line 3595
    .local v5, e:Ljava/lang/Exception;
    new-instance v13, Ljava/lang/StringBuilder;

    #@2bd
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@2c0
    const-string v14, "Exception resetting dns cache: "

    #@2c2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v13

    #@2c6
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c9
    move-result-object v13

    #@2ca
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2cd
    move-result-object v13

    #@2ce
    invoke-static {v13}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@2d1
    goto/16 :goto_1f7
.end method

.method private handleDeprecatedGlobalHttpProxy()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 4752
    iget-object v6, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v6

    #@8
    const-string v7, "http_proxy"

    #@a
    invoke-static {v6, v7}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    .line 4754
    .local v3, proxy:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v6

    #@12
    if-nez v6, :cond_34

    #@14
    .line 4755
    const-string v6, ":"

    #@16
    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 4756
    .local v0, data:[Ljava/lang/String;
    aget-object v4, v0, v8

    #@1c
    .line 4757
    .local v4, proxyHost:Ljava/lang/String;
    const/16 v5, 0x1f90

    #@1e
    .line 4758
    .local v5, proxyPort:I
    array-length v6, v0

    #@1f
    if-le v6, v9, :cond_28

    #@21
    .line 4760
    const/4 v6, 0x1

    #@22
    :try_start_22
    aget-object v6, v0, v6

    #@24
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_27
    .catch Ljava/lang/NumberFormatException; {:try_start_22 .. :try_end_27} :catch_35

    #@27
    move-result v5

    #@28
    .line 4765
    :cond_28
    new-instance v2, Landroid/net/ProxyProperties;

    #@2a
    aget-object v6, v0, v8

    #@2c
    const-string v7, ""

    #@2e
    invoke-direct {v2, v6, v5, v7}, Landroid/net/ProxyProperties;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@31
    .line 4766
    .local v2, p:Landroid/net/ProxyProperties;
    invoke-virtual {p0, v2}, Lcom/android/server/ConnectivityService;->setGlobalProxy(Landroid/net/ProxyProperties;)V

    #@34
    .line 4768
    .end local v0           #data:[Ljava/lang/String;
    .end local v2           #p:Landroid/net/ProxyProperties;
    .end local v4           #proxyHost:Ljava/lang/String;
    .end local v5           #proxyPort:I
    :cond_34
    :goto_34
    return-void

    #@35
    .line 4761
    .restart local v0       #data:[Ljava/lang/String;
    .restart local v4       #proxyHost:Ljava/lang/String;
    .restart local v5       #proxyPort:I
    :catch_35
    move-exception v1

    #@36
    .line 4762
    .local v1, e:Ljava/lang/NumberFormatException;
    goto :goto_34
.end method

.method private handleDisconnect(Landroid/net/NetworkInfo;)V
    .registers 29
    .parameter "info"

    #@0
    .prologue
    .line 2738
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getType()I

    #@3
    move-result v25

    #@4
    .line 2740
    .local v25, prevNetType:I
    move-object/from16 v0, p0

    #@6
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@8
    aget-object v2, v2, v25

    #@a
    const/4 v3, 0x0

    #@b
    invoke-interface {v2, v3}, Landroid/net/NetworkStateTracker;->setTeardownRequested(Z)V

    #@e
    .line 2743
    move-object/from16 v0, p0

    #@10
    move/from16 v1, v25

    #@12
    invoke-direct {v0, v1}, Lcom/android/server/ConnectivityService;->removeDataActivityTracking(I)V

    #@15
    .line 2751
    move-object/from16 v0, p0

    #@17
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@19
    aget-object v2, v2, v25

    #@1b
    invoke-virtual {v2}, Landroid/net/NetworkConfig;->isDefault()Z

    #@1e
    move-result v2

    #@1f
    if-nez v2, :cond_43

    #@21
    .line 2752
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@25
    aget-object v24, v2, v25

    #@27
    .line 2753
    .local v24, pids:Ljava/util/List;
    const/4 v12, 0x0

    #@28
    .local v12, i:I
    :goto_28
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    #@2b
    move-result v2

    #@2c
    if-ge v12, v2, :cond_43

    #@2e
    .line 2754
    move-object/from16 v0, v24

    #@30
    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v23

    #@34
    check-cast v23, Ljava/lang/Integer;

    #@36
    .line 2758
    .local v23, pid:Ljava/lang/Integer;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    #@39
    move-result v2

    #@3a
    const/4 v3, 0x0

    #@3b
    move-object/from16 v0, p0

    #@3d
    invoke-direct {v0, v2, v3}, Lcom/android/server/ConnectivityService;->reassessPidDns(IZ)V

    #@40
    .line 2753
    add-int/lit8 v12, v12, 0x1

    #@42
    goto :goto_28

    #@43
    .line 2762
    .end local v12           #i:I
    .end local v23           #pid:Ljava/lang/Integer;
    .end local v24           #pids:Ljava/util/List;
    :cond_43
    new-instance v16, Landroid/content/Intent;

    #@45
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    #@47
    move-object/from16 v0, v16

    #@49
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@4c
    .line 2763
    .local v16, intent:Landroid/content/Intent;
    const-string v2, "networkInfo"

    #@4e
    new-instance v3, Landroid/net/NetworkInfo;

    #@50
    move-object/from16 v0, p1

    #@52
    invoke-direct {v3, v0}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@55
    move-object/from16 v0, v16

    #@57
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@5a
    .line 2764
    const-string v2, "networkType"

    #@5c
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getType()I

    #@5f
    move-result v3

    #@60
    move-object/from16 v0, v16

    #@62
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@65
    .line 2765
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->isFailover()Z

    #@68
    move-result v2

    #@69
    if-eqz v2, :cond_79

    #@6b
    .line 2766
    const-string v2, "isFailover"

    #@6d
    const/4 v3, 0x1

    #@6e
    move-object/from16 v0, v16

    #@70
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@73
    .line 2767
    const/4 v2, 0x0

    #@74
    move-object/from16 v0, p1

    #@76
    invoke-virtual {v0, v2}, Landroid/net/NetworkInfo;->setFailover(Z)V

    #@79
    .line 2769
    :cond_79
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    if-eqz v2, :cond_8a

    #@7f
    .line 2770
    const-string v2, "reason"

    #@81
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    move-object/from16 v0, v16

    #@87
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@8a
    .line 2772
    :cond_8a
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@8d
    move-result-object v2

    #@8e
    if-eqz v2, :cond_9b

    #@90
    .line 2773
    const-string v2, "extraInfo"

    #@92
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@95
    move-result-object v3

    #@96
    move-object/from16 v0, v16

    #@98
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@9b
    .line 2777
    :cond_9b
    move-object/from16 v0, p0

    #@9d
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@9f
    aget-object v2, v2, v25

    #@a1
    invoke-virtual {v2}, Landroid/net/NetworkConfig;->isDefault()Z

    #@a4
    move-result v2

    #@a5
    if-eqz v2, :cond_cc

    #@a7
    .line 2778
    move-object/from16 v0, p0

    #@a9
    move/from16 v1, v25

    #@ab
    invoke-direct {v0, v1}, Lcom/android/server/ConnectivityService;->tryFailover(I)V

    #@ae
    .line 2779
    move-object/from16 v0, p0

    #@b0
    iget v2, v0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@b2
    const/4 v3, -0x1

    #@b3
    if-eq v2, v3, :cond_13c

    #@b5
    .line 2780
    move-object/from16 v0, p0

    #@b7
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@b9
    move-object/from16 v0, p0

    #@bb
    iget v3, v0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@bd
    aget-object v2, v2, v3

    #@bf
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@c2
    move-result-object v26

    #@c3
    .line 2781
    .local v26, switchTo:Landroid/net/NetworkInfo;
    const-string v2, "otherNetwork"

    #@c5
    move-object/from16 v0, v16

    #@c7
    move-object/from16 v1, v26

    #@c9
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@cc
    .line 2787
    .end local v26           #switchTo:Landroid/net/NetworkInfo;
    :cond_cc
    :goto_cc
    const-string v2, "inetCondition"

    #@ce
    move-object/from16 v0, p0

    #@d0
    iget v3, v0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@d2
    move-object/from16 v0, v16

    #@d4
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@d7
    .line 2790
    move-object/from16 v0, p0

    #@d9
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@db
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SMCAUSE_NOTIFY:Z

    #@dd
    if-eqz v2, :cond_112

    #@df
    .line 2792
    const-string v2, "smCause"

    #@e1
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getSmCause()I

    #@e4
    move-result v3

    #@e5
    move-object/from16 v0, v16

    #@e7
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@ea
    .line 2793
    new-instance v2, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v3, "handleDisconnect(): SM Cause is added  to CONNECTIVITY_ACTION intent for ["

    #@f1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v2

    #@f5
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@f8
    move-result-object v3

    #@f9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v2

    #@fd
    const-string v3, "], SM cause : "

    #@ff
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v2

    #@103
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getSmCause()I

    #@106
    move-result v3

    #@107
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v2

    #@10b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v2

    #@10f
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@112
    .line 2798
    :cond_112
    const/4 v11, 0x1

    #@113
    .line 2799
    .local v11, doReset:Z
    move-object/from16 v0, p0

    #@115
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@117
    aget-object v2, v2, v25

    #@119
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@11c
    move-result-object v4

    #@11d
    .line 2800
    .local v4, linkProperties:Landroid/net/LinkProperties;
    if-eqz v4, :cond_16f

    #@11f
    .line 2801
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@122
    move-result-object v22

    #@123
    .line 2802
    .local v22, oldIface:Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@126
    move-result v2

    #@127
    if-nez v2, :cond_16f

    #@129
    .line 2803
    move-object/from16 v0, p0

    #@12b
    iget-object v9, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@12d
    .local v9, arr$:[Landroid/net/NetworkStateTracker;
    array-length v0, v9

    #@12e
    move/from16 v18, v0

    #@130
    .local v18, len$:I
    const/4 v13, 0x0

    #@131
    .local v13, i$:I
    :goto_131
    move/from16 v0, v18

    #@133
    if-ge v13, v0, :cond_16f

    #@135
    aget-object v21, v9, v13

    #@137
    .line 2804
    .local v21, networkStateTracker:Landroid/net/NetworkStateTracker;
    if-nez v21, :cond_14a

    #@139
    .line 2803
    :cond_139
    add-int/lit8 v13, v13, 0x1

    #@13b
    goto :goto_131

    #@13c
    .line 2783
    .end local v4           #linkProperties:Landroid/net/LinkProperties;
    .end local v9           #arr$:[Landroid/net/NetworkStateTracker;
    .end local v11           #doReset:Z
    .end local v13           #i$:I
    .end local v18           #len$:I
    .end local v21           #networkStateTracker:Landroid/net/NetworkStateTracker;
    .end local v22           #oldIface:Ljava/lang/String;
    :cond_13c
    const/4 v2, 0x0

    #@13d
    move-object/from16 v0, p0

    #@13f
    iput v2, v0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@141
    .line 2784
    const-string v2, "noConnectivity"

    #@143
    const/4 v3, 0x1

    #@144
    move-object/from16 v0, v16

    #@146
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@149
    goto :goto_cc

    #@14a
    .line 2805
    .restart local v4       #linkProperties:Landroid/net/LinkProperties;
    .restart local v9       #arr$:[Landroid/net/NetworkStateTracker;
    .restart local v11       #doReset:Z
    .restart local v13       #i$:I
    .restart local v18       #len$:I
    .restart local v21       #networkStateTracker:Landroid/net/NetworkStateTracker;
    .restart local v22       #oldIface:Ljava/lang/String;
    :cond_14a
    invoke-interface/range {v21 .. v21}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@14d
    move-result-object v20

    #@14e
    .line 2806
    .local v20, networkInfo:Landroid/net/NetworkInfo;
    invoke-virtual/range {v20 .. v20}, Landroid/net/NetworkInfo;->isConnected()Z

    #@151
    move-result v2

    #@152
    if-eqz v2, :cond_139

    #@154
    invoke-virtual/range {v20 .. v20}, Landroid/net/NetworkInfo;->getType()I

    #@157
    move-result v2

    #@158
    move/from16 v0, v25

    #@15a
    if-eq v2, v0, :cond_139

    #@15c
    .line 2807
    invoke-interface/range {v21 .. v21}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@15f
    move-result-object v17

    #@160
    .line 2808
    .local v17, l:Landroid/net/LinkProperties;
    if-eqz v17, :cond_139

    #@162
    .line 2809
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@165
    move-result-object v2

    #@166
    move-object/from16 v0, v22

    #@168
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16b
    move-result v2

    #@16c
    if-eqz v2, :cond_139

    #@16e
    .line 2810
    const/4 v11, 0x0

    #@16f
    .line 2818
    .end local v9           #arr$:[Landroid/net/NetworkStateTracker;
    .end local v13           #i$:I
    .end local v17           #l:Landroid/net/LinkProperties;
    .end local v18           #len$:I
    .end local v20           #networkInfo:Landroid/net/NetworkInfo;
    .end local v21           #networkStateTracker:Landroid/net/NetworkStateTracker;
    .end local v22           #oldIface:Ljava/lang/String;
    :cond_16f
    new-instance v2, Ljava/lang/StringBuilder;

    #@171
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@174
    const-string v3, "[LGE_DATA][handleDisconnect] Reason:"

    #@176
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v2

    #@17a
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@17d
    move-result-object v3

    #@17e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v2

    #@182
    const-string v3, ", networkType:"

    #@184
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v2

    #@188
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getSubtype()I

    #@18b
    move-result v3

    #@18c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v2

    #@190
    const-string v3, ", networkTypeName:"

    #@192
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v2

    #@196
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    #@199
    move-result-object v3

    #@19a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v2

    #@19e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a1
    move-result-object v2

    #@1a2
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@1a5
    .line 2820
    move-object/from16 v0, p0

    #@1a7
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1a9
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_TCP_SOCKET_CONN_IN_OOS_DCM:Z

    #@1ab
    if-eqz v2, :cond_1c2

    #@1ad
    if-nez v25, :cond_1c2

    #@1af
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@1b2
    move-result-object v2

    #@1b3
    const-string v3, "nwTypeChanged"

    #@1b5
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1b8
    move-result v2

    #@1b9
    if-eqz v2, :cond_1c2

    #@1bb
    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getSubtype()I

    #@1be
    move-result v2

    #@1bf
    if-nez v2, :cond_1c2

    #@1c1
    .line 2824
    const/4 v11, 0x0

    #@1c2
    .line 2829
    :cond_1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1c6
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_KEEP_ROUTE_INFO_ON_SUSPEND_VZW:Z

    #@1c8
    if-eqz v2, :cond_1db

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@1ce
    aget-object v2, v2, v25

    #@1d0
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@1d3
    move-result-object v2

    #@1d4
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isSuspended()Z

    #@1d7
    move-result v2

    #@1d8
    if-eqz v2, :cond_1db

    #@1da
    .line 2830
    const/4 v11, 0x0

    #@1db
    .line 2835
    :cond_1db
    move-object/from16 v0, p0

    #@1dd
    move/from16 v1, v25

    #@1df
    invoke-direct {v0, v1, v11}, Lcom/android/server/ConnectivityService;->handleConnectivityChange(IZ)V

    #@1e2
    .line 2837
    new-instance v15, Landroid/content/Intent;

    #@1e4
    invoke-direct/range {v15 .. v16}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@1e7
    .line 2838
    .local v15, immediateIntent:Landroid/content/Intent;
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@1e9
    invoke-virtual {v15, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@1ec
    .line 2839
    move-object/from16 v0, p0

    #@1ee
    invoke-direct {v0, v15}, Lcom/android/server/ConnectivityService;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@1f1
    .line 2840
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->getConnectivityChangeDelay()I

    #@1f4
    move-result v2

    #@1f5
    move-object/from16 v0, p0

    #@1f7
    move-object/from16 v1, v16

    #@1f9
    invoke-direct {v0, v1, v2}, Lcom/android/server/ConnectivityService;->sendStickyBroadcastDelayed(Landroid/content/Intent;I)V

    #@1fc
    .line 2845
    move-object/from16 v0, p0

    #@1fe
    iget v2, v0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@200
    const/4 v3, -0x1

    #@201
    if-eq v2, v3, :cond_21a

    #@203
    .line 2846
    move-object/from16 v0, p0

    #@205
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@207
    move-object/from16 v0, p0

    #@209
    iget v3, v0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@20b
    aget-object v2, v2, v3

    #@20d
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@210
    move-result-object v2

    #@211
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->getConnectivityChangeDelay()I

    #@214
    move-result v3

    #@215
    move-object/from16 v0, p0

    #@217
    invoke-direct {v0, v2, v3}, Lcom/android/server/ConnectivityService;->sendConnectedBroadcastDelayed(Landroid/net/NetworkInfo;I)V

    #@21a
    .line 2850
    :cond_21a
    move-object/from16 v0, p0

    #@21c
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@21e
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@220
    if-eqz v2, :cond_27c

    #@222
    move-object/from16 v0, p0

    #@224
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@226
    aget-object v2, v2, v25

    #@228
    iget v2, v2, Landroid/net/NetworkConfig;->radio:I

    #@22a
    if-nez v2, :cond_27c

    #@22c
    move-object/from16 v0, p0

    #@22e
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@230
    const/4 v3, 0x1

    #@231
    aget-object v2, v2, v3

    #@233
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@236
    move-result-object v2

    #@237
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    #@23a
    move-result v2

    #@23b
    if-eqz v2, :cond_27c

    #@23d
    const/4 v2, 0x1

    #@23e
    if-ne v11, v2, :cond_27c

    #@240
    .line 2856
    const/4 v10, 0x0

    #@241
    .line 2858
    .local v10, defaultGateway:Ljava/net/InetAddress;
    if-eqz v4, :cond_27c

    #@243
    .line 2859
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@246
    move-result-object v14

    #@247
    .line 2861
    .local v14, ifaceName:Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@24a
    move-result v2

    #@24b
    if-nez v2, :cond_27a

    #@24d
    const-string v2, "rmnet_usb0"

    #@24f
    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@252
    move-result v2

    #@253
    if-eqz v2, :cond_27a

    #@255
    .line 2862
    const-string v2, "minjeon] Force to Remove default routing table route "

    #@257
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@25a
    .line 2864
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@25d
    move-result-object v2

    #@25e
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@261
    move-result-object v13

    #@262
    .local v13, i$:Ljava/util/Iterator;
    :cond_262
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@265
    move-result v2

    #@266
    if-eqz v2, :cond_27a

    #@268
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26b
    move-result-object v19

    #@26c
    check-cast v19, Landroid/net/RouteInfo;

    #@26e
    .line 2865
    .local v19, linkroute:Landroid/net/RouteInfo;
    if-eqz v19, :cond_262

    #@270
    invoke-virtual/range {v19 .. v19}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    #@273
    move-result v2

    #@274
    if-eqz v2, :cond_262

    #@276
    .line 2867
    invoke-virtual/range {v19 .. v19}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@279
    move-result-object v10

    #@27a
    .line 2873
    .end local v13           #i$:Ljava/util/Iterator;
    .end local v19           #linkroute:Landroid/net/RouteInfo;
    :cond_27a
    if-nez v10, :cond_27d

    #@27c
    .line 2887
    .end local v10           #defaultGateway:Ljava/net/InetAddress;
    .end local v14           #ifaceName:Ljava/lang/String;
    :cond_27c
    return-void

    #@27d
    .line 2876
    .restart local v10       #defaultGateway:Ljava/net/InetAddress;
    .restart local v14       #ifaceName:Ljava/lang/String;
    :cond_27d
    move-object/from16 v0, p0

    #@27f
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mAddedRoutes:Ljava/util/Collection;

    #@281
    check-cast v2, Ljava/util/ArrayList;

    #@283
    invoke-virtual {v2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@286
    move-result-object v2

    #@287
    check-cast v2, Ljava/util/ArrayList;

    #@289
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@28c
    move-result-object v13

    #@28d
    .restart local v13       #i$:Ljava/util/Iterator;
    :cond_28d
    :goto_28d
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@290
    move-result v2

    #@291
    if-eqz v2, :cond_27c

    #@293
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@296
    move-result-object v5

    #@297
    check-cast v5, Landroid/net/RouteInfo;

    #@299
    .line 2877
    .local v5, r:Landroid/net/RouteInfo;
    if-eqz v5, :cond_27c

    #@29b
    .line 2879
    invoke-virtual {v5}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@29e
    move-result-object v2

    #@29f
    if-eqz v2, :cond_28d

    #@2a1
    invoke-virtual {v5}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@2a4
    move-result-object v2

    #@2a5
    invoke-virtual {v2, v10}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@2a8
    move-result v2

    #@2a9
    if-eqz v2, :cond_28d

    #@2ab
    .line 2880
    new-instance v2, Ljava/lang/StringBuilder;

    #@2ad
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b0
    const-string v3, "minjeon] force to remove [ "

    #@2b2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b5
    move-result-object v2

    #@2b6
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b9
    move-result-object v2

    #@2ba
    const-string v3, " ] "

    #@2bc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bf
    move-result-object v2

    #@2c0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c3
    move-result-object v2

    #@2c4
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@2c7
    .line 2881
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@2ca
    move-result-object v3

    #@2cb
    const/4 v6, 0x0

    #@2cc
    const/4 v7, 0x0

    #@2cd
    const/4 v8, 0x1

    #@2ce
    move-object/from16 v2, p0

    #@2d0
    invoke-direct/range {v2 .. v8}, Lcom/android/server/ConnectivityService;->modifyRoute(Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/RouteInfo;IZZ)Z

    #@2d3
    goto :goto_28d
.end method

.method private handleDnsConfigurationChange(I)V
    .registers 17
    .parameter "netType"

    #@0
    .prologue
    .line 4094
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    aget-object v7, v12, p1

    #@4
    .line 4095
    .local v7, nt:Landroid/net/NetworkStateTracker;
    if-eqz v7, :cond_1c

    #@6
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@9
    move-result-object v12

    #@a
    invoke-virtual {v12}, Landroid/net/NetworkInfo;->isConnected()Z

    #@d
    move-result v12

    #@e
    if-eqz v12, :cond_1c

    #@10
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@13
    move-result v12

    #@14
    if-nez v12, :cond_1c

    #@16
    .line 4096
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@19
    move-result-object v8

    #@1a
    .line 4097
    .local v8, p:Landroid/net/LinkProperties;
    if-nez v8, :cond_1d

    #@1c
    .line 4131
    .end local v8           #p:Landroid/net/LinkProperties;
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 4098
    .restart local v8       #p:Landroid/net/LinkProperties;
    :cond_1d
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@20
    move-result-object v2

    #@21
    .line 4099
    .local v2, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    const/4 v1, 0x0

    #@22
    .line 4100
    .local v1, changed:Z
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@24
    aget-object v12, v12, p1

    #@26
    invoke-virtual {v12}, Landroid/net/NetworkConfig;->isDefault()Z

    #@29
    move-result v12

    #@2a
    if-eqz v12, :cond_5d

    #@2c
    .line 4101
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@2f
    move-result-object v12

    #@30
    invoke-virtual {v12}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@33
    move-result-object v6

    #@34
    .line 4102
    .local v6, network:Ljava/lang/String;
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mDnsLock:Ljava/lang/Object;

    #@36
    monitor-enter v13

    #@37
    .line 4103
    :try_start_37
    iget-boolean v12, p0, Lcom/android/server/ConnectivityService;->mDnsOverridden:Z

    #@39
    if-nez v12, :cond_53

    #@3b
    .line 4104
    const-string v3, ""

    #@3d
    .line 4105
    .local v3, domain:Ljava/lang/String;
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getDomainName()Ljava/lang/String;

    #@40
    move-result-object v12

    #@41
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@44
    move-result v12

    #@45
    if-nez v12, :cond_4b

    #@47
    .line 4106
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getDomainName()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    .line 4108
    :cond_4b
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@4e
    move-result-object v12

    #@4f
    invoke-direct {p0, v6, v12, v2, v3}, Lcom/android/server/ConnectivityService;->updateDns(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)Z

    #@52
    move-result v1

    #@53
    .line 4110
    .end local v3           #domain:Ljava/lang/String;
    :cond_53
    monitor-exit v13
    :try_end_54
    .catchall {:try_start_37 .. :try_end_54} :catchall_5a

    #@54
    .line 4129
    .end local v6           #network:Ljava/lang/String;
    :cond_54
    if-eqz v1, :cond_1c

    #@56
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->bumpDns()V

    #@59
    goto :goto_1c

    #@5a
    .line 4110
    .restart local v6       #network:Ljava/lang/String;
    :catchall_5a
    move-exception v12

    #@5b
    :try_start_5b
    monitor-exit v13
    :try_end_5c
    .catchall {:try_start_5b .. :try_end_5c} :catchall_5a

    #@5c
    throw v12

    #@5d
    .line 4113
    .end local v6           #network:Ljava/lang/String;
    :cond_5d
    :try_start_5d
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@5f
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@62
    move-result-object v13

    #@63
    invoke-static {v2}, Landroid/net/NetworkUtils;->makeStrings(Ljava/util/Collection;)[Ljava/lang/String;

    #@66
    move-result-object v14

    #@67
    invoke-interface {v12, v13, v14}, Landroid/os/INetworkManagementService;->setDnsServersForInterface(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_6a
    .catch Ljava/lang/Exception; {:try_start_5d .. :try_end_6a} :catch_8e

    #@6a
    .line 4119
    :goto_6a
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@6c
    aget-object v10, v12, p1

    #@6e
    .line 4120
    .local v10, pids:Ljava/util/List;
    const/4 v11, 0x0

    #@6f
    .local v11, y:I
    :goto_6f
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@72
    move-result v12

    #@73
    if-ge v11, v12, :cond_54

    #@75
    .line 4121
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@78
    move-result-object v9

    #@79
    check-cast v9, Ljava/lang/Integer;

    #@7b
    .line 4123
    .local v9, pid:Ljava/lang/Integer;
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@7e
    move-result-object v0

    #@7f
    .line 4124
    .local v0, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-direct {p0, v0}, Lcom/android/server/ConnectivityService;->getIPtype(Ljava/util/Collection;)I

    #@82
    move-result v5

    #@83
    .line 4125
    .local v5, iptype:I
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@86
    move-result v12

    #@87
    invoke-direct {p0, v2, v12, v5}, Lcom/android/server/ConnectivityService;->writePidDns(Ljava/util/Collection;II)Z

    #@8a
    move-result v1

    #@8b
    .line 4120
    add-int/lit8 v11, v11, 0x1

    #@8d
    goto :goto_6f

    #@8e
    .line 4115
    .end local v0           #addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v5           #iptype:I
    .end local v9           #pid:Ljava/lang/Integer;
    .end local v10           #pids:Ljava/util/List;
    .end local v11           #y:I
    :catch_8e
    move-exception v4

    #@8f
    .line 4116
    .local v4, e:Ljava/lang/Exception;
    new-instance v12, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v13, "exception setting dns servers: "

    #@96
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v12

    #@9a
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v12

    #@9e
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v12

    #@a2
    invoke-static {v12}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@a5
    goto :goto_6a
.end method

.method private handleInetConditionChange(II)V
    .registers 9
    .parameter "netType"
    .parameter "condition"

    #@0
    .prologue
    .line 4611
    iget v1, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@2
    const/4 v2, -0x1

    #@3
    if-ne v1, v2, :cond_b

    #@5
    .line 4612
    const-string v1, "handleInetConditionChange: no active default network - ignore"

    #@7
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@a
    .line 4644
    :goto_a
    return-void

    #@b
    .line 4615
    :cond_b
    iget v1, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@d
    if-eq v1, p1, :cond_38

    #@f
    .line 4616
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "handleInetConditionChange: net="

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, " != default="

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, " - ignore"

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@37
    goto :goto_a

    #@38
    .line 4621
    :cond_38
    new-instance v1, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v2, "handleInetConditionChange: net="

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    const-string v2, ", condition="

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    const-string v2, ",mActiveDefaultNetwork="

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    iget v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@64
    .line 4625
    iput p2, p0, Lcom/android/server/ConnectivityService;->mDefaultInetCondition:I

    #@66
    .line 4627
    iget-boolean v1, p0, Lcom/android/server/ConnectivityService;->mInetConditionChangeInFlight:Z

    #@68
    if-nez v1, :cond_a8

    #@6a
    .line 4628
    const-string v1, "handleInetConditionChange: starting a change hold"

    #@6c
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@6f
    .line 4630
    iget v1, p0, Lcom/android/server/ConnectivityService;->mDefaultInetCondition:I

    #@71
    const/16 v2, 0x32

    #@73
    if-le v1, v2, :cond_99

    #@75
    .line 4631
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@77
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7a
    move-result-object v1

    #@7b
    const-string v2, "inet_condition_debounce_up_delay"

    #@7d
    const/16 v3, 0x1f4

    #@7f
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@82
    move-result v0

    #@83
    .line 4637
    .local v0, delay:I
    :goto_83
    const/4 v1, 0x1

    #@84
    iput-boolean v1, p0, Lcom/android/server/ConnectivityService;->mInetConditionChangeInFlight:Z

    #@86
    .line 4638
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@88
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@8a
    const/4 v3, 0x5

    #@8b
    iget v4, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@8d
    iget v5, p0, Lcom/android/server/ConnectivityService;->mDefaultConnectionSequence:I

    #@8f
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@92
    move-result-object v2

    #@93
    int-to-long v3, v0

    #@94
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@97
    goto/16 :goto_a

    #@99
    .line 4634
    .end local v0           #delay:I
    :cond_99
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@9b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9e
    move-result-object v1

    #@9f
    const-string v2, "inet_condition_debounce_down_delay"

    #@a1
    const/16 v3, 0xbb8

    #@a3
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a6
    move-result v0

    #@a7
    .restart local v0       #delay:I
    goto :goto_83

    #@a8
    .line 4642
    .end local v0           #delay:I
    :cond_a8
    const-string v1, "handleInetConditionChange: currently in hold - not setting new end evt"

    #@aa
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@ad
    goto/16 :goto_a
.end method

.method private handleInetConditionHoldEnd(II)V
    .registers 6
    .parameter "netType"
    .parameter "sequence"

    #@0
    .prologue
    .line 4648
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "handleInetConditionHoldEnd: net="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ", condition="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p0, Lcom/android/server/ConnectivityService;->mDefaultInetCondition:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", published condition="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget v2, p0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2e
    .line 4652
    const/4 v1, 0x0

    #@2f
    iput-boolean v1, p0, Lcom/android/server/ConnectivityService;->mInetConditionChangeInFlight:Z

    #@31
    .line 4654
    iget v1, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@33
    const/4 v2, -0x1

    #@34
    if-ne v1, v2, :cond_3c

    #@36
    .line 4655
    const-string v1, "handleInetConditionHoldEnd: no active default network - ignoring"

    #@38
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@3b
    .line 4676
    :goto_3b
    return-void

    #@3c
    .line 4658
    :cond_3c
    iget v1, p0, Lcom/android/server/ConnectivityService;->mDefaultConnectionSequence:I

    #@3e
    if-eq v1, p2, :cond_46

    #@40
    .line 4659
    const-string v1, "handleInetConditionHoldEnd: event hold for obsolete network - ignoring"

    #@42
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@45
    goto :goto_3b

    #@46
    .line 4669
    :cond_46
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@48
    iget v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@4a
    aget-object v1, v1, v2

    #@4c
    invoke-interface {v1}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@4f
    move-result-object v0

    #@50
    .line 4670
    .local v0, networkInfo:Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@53
    move-result v1

    #@54
    if-nez v1, :cond_5c

    #@56
    .line 4671
    const-string v1, "handleInetConditionHoldEnd: default network not connected - ignoring"

    #@58
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@5b
    goto :goto_3b

    #@5c
    .line 4674
    :cond_5c
    iget v1, p0, Lcom/android/server/ConnectivityService;->mDefaultInetCondition:I

    #@5e
    iput v1, p0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@60
    .line 4675
    invoke-direct {p0, v0}, Lcom/android/server/ConnectivityService;->sendInetConditionBroadcast(Landroid/net/NetworkInfo;)V

    #@63
    goto :goto_3b
.end method

.method private handleSetDependencyMet(IZ)V
    .registers 5
    .parameter "networkType"
    .parameter "met"

    #@0
    .prologue
    .line 2460
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    aget-object v0, v0, p1

    #@4
    if-eqz v0, :cond_33

    #@6
    .line 2462
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "handleSetDependencyMet("

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ", "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, ")"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2c
    .line 2464
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2e
    aget-object v0, v0, p1

    #@30
    invoke-interface {v0, p2}, Landroid/net/NetworkStateTracker;->setDependencyMet(Z)V

    #@33
    .line 2466
    :cond_33
    return-void
.end method

.method private handleSetMobileData(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    const/4 v3, 0x6

    #@1
    const/4 v2, 0x0

    #@2
    .line 2666
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@4
    aget-object v0, v0, v2

    #@6
    if-eqz v0, :cond_2b

    #@8
    .line 2668
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@f
    aget-object v1, v1, v2

    #@11
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@24
    .line 2670
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@26
    aget-object v0, v0, v2

    #@28
    invoke-interface {v0, p1}, Landroid/net/NetworkStateTracker;->setUserDataEnable(Z)V

    #@2b
    .line 2672
    :cond_2b
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2d
    aget-object v0, v0, v3

    #@2f
    if-eqz v0, :cond_54

    #@31
    .line 2674
    new-instance v0, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@38
    aget-object v1, v1, v3

    #@3a
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@4d
    .line 2676
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@4f
    aget-object v0, v0, v3

    #@51
    invoke-interface {v0, p1}, Landroid/net/NetworkStateTracker;->setUserDataEnable(Z)V

    #@54
    .line 2678
    :cond_54
    return-void
.end method

.method private handleSetNetworkPreference(I)V
    .registers 4
    .parameter "preference"

    #@0
    .prologue
    .line 1081
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_2c

    #@6
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@8
    aget-object v1, v1, p1

    #@a
    if-eqz v1, :cond_2c

    #@c
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@e
    aget-object v1, v1, p1

    #@10
    invoke-virtual {v1}, Landroid/net/NetworkConfig;->isDefault()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_2c

    #@16
    .line 1084
    iget v1, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@18
    if-eq v1, p1, :cond_2c

    #@1a
    .line 1085
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1f
    move-result-object v0

    #@20
    .line 1086
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v1, "network_preference"

    #@22
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@25
    .line 1087
    monitor-enter p0

    #@26
    .line 1088
    :try_start_26
    iput p1, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@28
    .line 1089
    monitor-exit p0
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_2d

    #@29
    .line 1090
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforcePreference()V

    #@2c
    .line 1093
    .end local v0           #cr:Landroid/content/ContentResolver;
    :cond_2c
    return-void

    #@2d
    .line 1089
    .restart local v0       #cr:Landroid/content/ContentResolver;
    :catchall_2d
    move-exception v1

    #@2e
    :try_start_2e
    monitor-exit p0
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    throw v1
.end method

.method private handleSetPolicyDataEnable(IZ)V
    .registers 5
    .parameter "networkType"
    .parameter "enabled"

    #@0
    .prologue
    .line 2690
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_f

    #@6
    .line 2691
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@8
    aget-object v0, v1, p1

    #@a
    .line 2692
    .local v0, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v0, :cond_f

    #@c
    .line 2693
    invoke-interface {v0, p2}, Landroid/net/NetworkStateTracker;->setPolicyDataEnable(Z)V

    #@f
    .line 2696
    .end local v0           #tracker:Landroid/net/NetworkStateTracker;
    :cond_f
    return-void
.end method

.method private isContainVzwAppApn_MetaTag()Z
    .registers 14

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1484
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingUid()I

    #@4
    move-result v2

    #@5
    .line 1485
    .local v2, callerUid:I
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@7
    invoke-virtual {v10, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@a
    move-result-object v8

    #@b
    .line 1487
    .local v8, packages:[Ljava/lang/String;
    move-object v1, v8

    #@c
    .local v1, arr$:[Ljava/lang/String;
    array-length v5, v1

    #@d
    .local v5, len$:I
    const/4 v4, 0x0

    #@e
    .local v4, i$:I
    :goto_e
    if-ge v4, v5, :cond_36

    #@10
    aget-object v6, v1, v4

    #@12
    .line 1489
    .local v6, name:Ljava/lang/String;
    :try_start_12
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@14
    const/4 v11, 0x0

    #@15
    invoke-virtual {v10, v6, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@18
    move-result-object v7

    #@19
    .line 1490
    .local v7, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@1b
    iget-object v11, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@1d
    const/16 v12, 0x80

    #@1f
    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@22
    move-result-object v0

    #@23
    .line 1492
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    if-eqz v7, :cond_39

    #@25
    if-eqz v0, :cond_39

    #@27
    iget-object v10, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@29
    if-eqz v10, :cond_39

    #@2b
    .line 1493
    iget-object v10, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@2d
    const-string v11, "com.verizon.VZWAPPAPN"

    #@2f
    invoke-virtual {v10, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
    :try_end_32
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12 .. :try_end_32} :catch_37

    #@32
    move-result v10

    #@33
    if-eqz v10, :cond_36

    #@35
    .line 1494
    const/4 v9, 0x1

    #@36
    .line 1502
    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v6           #name:Ljava/lang/String;
    .end local v7           #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_36
    :goto_36
    return v9

    #@37
    .line 1498
    .restart local v6       #name:Ljava/lang/String;
    :catch_37
    move-exception v3

    #@38
    .line 1499
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_36

    #@39
    .line 1487
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0       #appInfo:Landroid/content/pm/ApplicationInfo;
    .restart local v7       #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_39
    add-int/lit8 v4, v4, 0x1

    #@3b
    goto :goto_e
.end method

.method private isNetworkBlocked(Landroid/net/NetworkStateTracker;I)Z
    .registers 10
    .parameter "tracker"
    .parameter "uid"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1158
    invoke-interface {p1}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@4
    move-result-object v4

    #@5
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 1162
    .local v0, iface:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mRulesLock:Ljava/lang/Object;

    #@b
    monitor-enter v4

    #@c
    .line 1163
    :try_start_c
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mMeteredIfaces:Ljava/util/HashSet;

    #@e
    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    .line 1164
    .local v1, networkCostly:Z
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mUidRules:Landroid/util/SparseIntArray;

    #@14
    const/4 v6, 0x0

    #@15
    invoke-virtual {v5, p2, v6}, Landroid/util/SparseIntArray;->get(II)I

    #@18
    move-result v2

    #@19
    .line 1165
    .local v2, uidRules:I
    monitor-exit v4

    #@1a
    .line 1167
    if-eqz v1, :cond_21

    #@1c
    and-int/lit8 v4, v2, 0x1

    #@1e
    if-eqz v4, :cond_21

    #@20
    .line 1168
    const/4 v3, 0x1

    #@21
    .line 1172
    :cond_21
    return v3

    #@22
    .line 1165
    .end local v1           #networkCostly:Z
    .end local v2           #uidRules:I
    :catchall_22
    move-exception v3

    #@23
    monitor-exit v4
    :try_end_24
    .catchall {:try_start_c .. :try_end_24} :catchall_22

    #@24
    throw v3
.end method

.method private isNetworkMeteredUnchecked(I)Z
    .registers 4
    .parameter "networkType"

    #@0
    .prologue
    .line 1358
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->getNetworkStateUnchecked(I)Landroid/net/NetworkState;

    #@3
    move-result-object v0

    #@4
    .line 1359
    .local v0, state:Landroid/net/NetworkState;
    if-eqz v0, :cond_e

    #@6
    .line 1361
    :try_start_6
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mPolicyManager:Landroid/net/INetworkPolicyManager;

    #@8
    invoke-interface {v1, v0}, Landroid/net/INetworkPolicyManager;->isNetworkMetered(Landroid/net/NetworkState;)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 1365
    :goto_c
    return v1

    #@d
    .line 1362
    :catch_d
    move-exception v1

    #@e
    .line 1365
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_c
.end method

.method private isNewNetTypePreferredOverCurrentNetType(I)Z
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 3165
    iget v0, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@2
    if-eq p1, v0, :cond_14

    #@4
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@6
    iget v1, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@8
    aget-object v0, v0, v1

    #@a
    iget v0, v0, Landroid/net/NetworkConfig;->priority:I

    #@c
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@e
    aget-object v1, v1, p1

    #@10
    iget v1, v1, Landroid/net/NetworkConfig;->priority:I

    #@12
    if-gt v0, v1, :cond_1a

    #@14
    :cond_14
    iget v0, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@16
    iget v1, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@18
    if-ne v0, v1, :cond_1c

    #@1a
    .line 3168
    :cond_1a
    const/4 v0, 0x0

    #@1b
    .line 3169
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x1

    #@1d
    goto :goto_1b
.end method

.method private isRCSeWorking()Z
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 3295
    const/4 v8, 0x0

    #@2
    .line 3298
    .local v8, nRCSeWorking:I
    :try_start_2
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    .line 3299
    .local v0, contentResolver:Landroid/content/ContentResolver;
    const-string v1, "content://com.lge.ims.provisioning/workings"

    #@a
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v1

    #@e
    const/4 v2, 0x0

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x0

    #@11
    const/4 v5, 0x0

    #@12
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@15
    move-result-object v6

    #@16
    .line 3301
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_2c

    #@18
    .line 3303
    :goto_18
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_29

    #@1e
    .line 3305
    const-string v1, "rcs_e_working"

    #@20
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@23
    move-result v1

    #@24
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    #@27
    move-result v8

    #@28
    goto :goto_18

    #@29
    .line 3307
    :cond_29
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2c} :catch_30

    #@2c
    .line 3313
    .end local v0           #contentResolver:Landroid/content/ContentResolver;
    .end local v6           #cursor:Landroid/database/Cursor;
    :cond_2c
    :goto_2c
    if-ne v8, v9, :cond_37

    #@2e
    move v1, v9

    #@2f
    .line 3315
    :goto_2f
    return v1

    #@30
    .line 3309
    :catch_30
    move-exception v7

    #@31
    .line 3310
    .local v7, e:Ljava/lang/Exception;
    const-string v1, "Fail to query content://com.lge.ims.provisioning/workings"

    #@33
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@36
    goto :goto_2c

    #@37
    .line 3315
    .end local v7           #e:Ljava/lang/Exception;
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_2f
.end method

.method private isSignedFromVZW()Z
    .registers 21

    #@0
    .prologue
    .line 1510
    :try_start_0
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingUid()I

    #@3
    move-result v4

    #@4
    .line 1512
    .local v4, caller:I
    const-string v17, "ConnectivityService"

    #@6
    new-instance v18, Ljava/lang/StringBuilder;

    #@8
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v19, "SecureSetting caller UID: "

    #@d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v18

    #@11
    move-object/from16 v0, v18

    #@13
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v18

    #@17
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v18

    #@1b
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1513
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@22
    move-object/from16 v17, v0

    #@24
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@27
    move-result-object v14

    #@28
    .line 1514
    .local v14, pm:Landroid/content/pm/PackageManager;
    const-string v17, "com.verizon.permissions.vzwappapn"

    #@2a
    const/16 v18, 0x40

    #@2c
    move-object/from16 v0, v17

    #@2e
    move/from16 v1, v18

    #@30
    invoke-virtual {v14, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@33
    move-result-object v16

    #@34
    .line 1516
    .local v16, sigpi:Landroid/content/pm/PackageInfo;
    invoke-virtual {v14, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    .line 1517
    .local v6, callerpkgs:[Ljava/lang/String;
    if-nez v6, :cond_3d

    #@3a
    const/16 v17, 0x0

    #@3c
    .line 1557
    .end local v4           #caller:I
    .end local v6           #callerpkgs:[Ljava/lang/String;
    .end local v14           #pm:Landroid/content/pm/PackageManager;
    .end local v16           #sigpi:Landroid/content/pm/PackageInfo;
    :goto_3c
    return v17

    #@3d
    .line 1519
    .restart local v4       #caller:I
    .restart local v6       #callerpkgs:[Ljava/lang/String;
    .restart local v14       #pm:Landroid/content/pm/PackageManager;
    .restart local v16       #sigpi:Landroid/content/pm/PackageInfo;
    :cond_3d
    move-object v2, v6

    #@3e
    .local v2, arr$:[Ljava/lang/String;
    array-length v11, v2

    #@3f
    .local v11, len$:I
    const/4 v9, 0x0

    #@40
    .local v9, i$:I
    move v10, v9

    #@41
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v9           #i$:I
    .end local v11           #len$:I
    .local v10, i$:I
    :goto_41
    if-ge v10, v11, :cond_dc

    #@43
    aget-object v13, v2, v10

    #@45
    .line 1521
    .local v13, pkgname:Ljava/lang/String;
    const-string v17, "ConnectivityService"

    #@47
    new-instance v18, Ljava/lang/StringBuilder;

    #@49
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v19, "SecureSetting pkg: "

    #@4e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v18

    #@52
    move-object/from16 v0, v18

    #@54
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v18

    #@58
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v18

    #@5c
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 1523
    const-string v17, "cequint"

    #@61
    move-object/from16 v0, v17

    #@63
    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@66
    move-result v17

    #@67
    if-eqz v17, :cond_73

    #@69
    .line 1524
    const-string v17, "ConnectivityService"

    #@6b
    const-string v18, "cequint is only  false"

    #@6d
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1525
    const/16 v17, 0x0

    #@72
    goto :goto_3c

    #@73
    .line 1535
    :cond_73
    const/16 v17, 0x40

    #@75
    move/from16 v0, v17

    #@77
    invoke-virtual {v14, v13, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@7a
    move-result-object v5

    #@7b
    .line 1537
    .local v5, callerpi:Landroid/content/pm/PackageInfo;
    iget-object v3, v5, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@7d
    .local v3, arr$:[Landroid/content/pm/Signature;
    array-length v12, v3

    #@7e
    .local v12, len$:I
    const/4 v9, 0x0

    #@7f
    .end local v10           #i$:I
    .restart local v9       #i$:I
    :goto_7f
    if-ge v9, v12, :cond_cf

    #@81
    aget-object v15, v3, v9

    #@83
    .line 1541
    .local v15, sig:Landroid/content/pm/Signature;
    const/4 v8, 0x0

    #@84
    .local v8, i:I
    :goto_84
    move-object/from16 v0, v16

    #@86
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@88
    move-object/from16 v17, v0

    #@8a
    move-object/from16 v0, v17

    #@8c
    array-length v0, v0

    #@8d
    move/from16 v17, v0

    #@8f
    move/from16 v0, v17

    #@91
    if-ge v8, v0, :cond_cc

    #@93
    .line 1542
    if-eqz v15, :cond_c9

    #@95
    move-object/from16 v0, v16

    #@97
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@99
    move-object/from16 v17, v0

    #@9b
    aget-object v17, v17, v8

    #@9d
    move-object/from16 v0, v17

    #@9f
    invoke-virtual {v15, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    #@a2
    move-result v17

    #@a3
    if-eqz v17, :cond_c9

    #@a5
    .line 1543
    const-string v17, "ConnectivityService"

    #@a7
    new-instance v18, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v19, "SecureSetting it\'s a match : sigpi.signatures ["

    #@ae
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v18

    #@b2
    move-object/from16 v0, v18

    #@b4
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v18

    #@b8
    const-string v19, "]"

    #@ba
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v18

    #@be
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v18

    #@c2
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_c5} :catch_d4

    #@c5
    .line 1544
    const/16 v17, 0x1

    #@c7
    goto/16 :goto_3c

    #@c9
    .line 1541
    :cond_c9
    add-int/lit8 v8, v8, 0x1

    #@cb
    goto :goto_84

    #@cc
    .line 1537
    :cond_cc
    add-int/lit8 v9, v9, 0x1

    #@ce
    goto :goto_7f

    #@cf
    .line 1519
    .end local v8           #i:I
    .end local v15           #sig:Landroid/content/pm/Signature;
    :cond_cf
    add-int/lit8 v9, v10, 0x1

    #@d1
    move v10, v9

    #@d2
    .end local v9           #i$:I
    .restart local v10       #i$:I
    goto/16 :goto_41

    #@d4
    .line 1550
    .end local v3           #arr$:[Landroid/content/pm/Signature;
    .end local v4           #caller:I
    .end local v5           #callerpi:Landroid/content/pm/PackageInfo;
    .end local v6           #callerpkgs:[Ljava/lang/String;
    .end local v10           #i$:I
    .end local v12           #len$:I
    .end local v13           #pkgname:Ljava/lang/String;
    .end local v14           #pm:Landroid/content/pm/PackageManager;
    .end local v16           #sigpi:Landroid/content/pm/PackageInfo;
    :catch_d4
    move-exception v7

    #@d5
    .line 1551
    .local v7, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v17, "ConnectivityService"

    #@d7
    const-string v18, "SecureSetting, No match"

    #@d9
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 1556
    .end local v7           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_dc
    const-string v17, "ConnectivityService"

    #@de
    const-string v18, "SecureSetting oops didn\'t find a match"

    #@e0
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 1557
    const/16 v17, 0x0

    #@e5
    goto/16 :goto_3c
.end method

.method private isSystemImage()Z
    .registers 12

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1467
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingUid()I

    #@4
    move-result v1

    #@5
    .line 1468
    .local v1, callerUid:I
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@7
    invoke-virtual {v9, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@a
    move-result-object v7

    #@b
    .line 1470
    .local v7, packages:[Ljava/lang/String;
    move-object v0, v7

    #@c
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@d
    .local v4, len$:I
    const/4 v3, 0x0

    #@e
    .local v3, i$:I
    :goto_e
    if-ge v3, v4, :cond_24

    #@10
    aget-object v5, v0, v3

    #@12
    .line 1472
    .local v5, name:Ljava/lang/String;
    :try_start_12
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@14
    const/4 v10, 0x0

    #@15
    invoke-virtual {v9, v5, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@18
    move-result-object v6

    #@19
    .line 1473
    .local v6, packageInfo:Landroid/content/pm/PackageInfo;
    if-eqz v6, :cond_27

    #@1b
    iget-object v9, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1d
    iget v9, v9, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_1f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12 .. :try_end_1f} :catch_25

    #@1f
    and-int/lit8 v9, v9, 0x1

    #@21
    if-eqz v9, :cond_27

    #@23
    .line 1474
    const/4 v8, 0x1

    #@24
    .line 1480
    .end local v5           #name:Ljava/lang/String;
    .end local v6           #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_24
    :goto_24
    return v8

    #@25
    .line 1476
    .restart local v5       #name:Ljava/lang/String;
    :catch_25
    move-exception v2

    #@26
    .line 1477
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_24

    #@27
    .line 1470
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6       #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_27
    add-int/lit8 v3, v3, 0x1

    #@29
    goto :goto_e
.end method

.method private loadGlobalProxy()V
    .registers 8

    #@0
    .prologue
    .line 4717
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v4

    #@6
    .line 4718
    .local v4, res:Landroid/content/ContentResolver;
    const-string v5, "global_http_proxy_host"

    #@8
    invoke-static {v4, v5}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 4719
    .local v1, host:Ljava/lang/String;
    const-string v5, "global_http_proxy_port"

    #@e
    const/4 v6, 0x0

    #@f
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@12
    move-result v2

    #@13
    .line 4720
    .local v2, port:I
    const-string v5, "global_http_proxy_exclusion_list"

    #@15
    invoke-static {v4, v5}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 4722
    .local v0, exclList:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c
    move-result v5

    #@1d
    if-nez v5, :cond_2a

    #@1f
    .line 4723
    new-instance v3, Landroid/net/ProxyProperties;

    #@21
    invoke-direct {v3, v1, v2, v0}, Landroid/net/ProxyProperties;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@24
    .line 4724
    .local v3, proxyProperties:Landroid/net/ProxyProperties;
    iget-object v6, p0, Lcom/android/server/ConnectivityService;->mGlobalProxyLock:Ljava/lang/Object;

    #@26
    monitor-enter v6

    #@27
    .line 4725
    :try_start_27
    iput-object v3, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@29
    .line 4726
    monitor-exit v6

    #@2a
    .line 4728
    .end local v3           #proxyProperties:Landroid/net/ProxyProperties;
    :cond_2a
    return-void

    #@2b
    .line 4726
    .restart local v3       #proxyProperties:Landroid/net/ProxyProperties;
    :catchall_2b
    move-exception v5

    #@2c
    monitor-exit v6
    :try_end_2d
    .catchall {:try_start_27 .. :try_end_2d} :catchall_2b

    #@2d
    throw v5
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 4807
    const-string v0, "ConnectivityService"

    #@2
    invoke-static {v0, p0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 4808
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 4811
    const-string v0, "ConnectivityService"

    #@2
    invoke-static {v0, p0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 4812
    return-void
.end method

.method private makeGeneralIntent(Landroid/net/NetworkInfo;Ljava/lang/String;)Landroid/content/Intent;
    .registers 6
    .parameter "info"
    .parameter "bcastType"

    #@0
    .prologue
    .line 2974
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@2
    if-eqz v1, :cond_a

    #@4
    .line 2975
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@6
    invoke-virtual {v1, p1}, Lcom/android/server/net/LockdownVpnTracker;->augmentNetworkInfo(Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    #@9
    move-result-object p1

    #@a
    .line 2978
    :cond_a
    new-instance v0, Landroid/content/Intent;

    #@c
    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@f
    .line 2979
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "networkInfo"

    #@11
    new-instance v2, Landroid/net/NetworkInfo;

    #@13
    invoke-direct {v2, p1}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@19
    .line 2980
    const-string v1, "networkType"

    #@1b
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@22
    .line 2981
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isFailover()Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_32

    #@28
    .line 2982
    const-string v1, "isFailover"

    #@2a
    const/4 v2, 0x1

    #@2b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@2e
    .line 2983
    const/4 v1, 0x0

    #@2f
    invoke-virtual {p1, v1}, Landroid/net/NetworkInfo;->setFailover(Z)V

    #@32
    .line 2985
    :cond_32
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    if-eqz v1, :cond_41

    #@38
    .line 2986
    const-string v1, "reason"

    #@3a
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@41
    .line 2988
    :cond_41
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    if-eqz v1, :cond_50

    #@47
    .line 2989
    const-string v1, "extraInfo"

    #@49
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@50
    .line 2992
    :cond_50
    const-string v1, "inetCondition"

    #@52
    iget v2, p0, Lcom/android/server/ConnectivityService;->mDefaultInetConditionPublished:I

    #@54
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@57
    .line 2995
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@59
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_SMCAUSE_NOTIFY:Z

    #@5b
    if-eqz v1, :cond_8e

    #@5d
    .line 2997
    const-string v1, "smCause"

    #@5f
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSmCause()I

    #@62
    move-result v2

    #@63
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@66
    .line 2998
    new-instance v1, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v2, "makeGeneralIntent(): SM Cause is added  to CONNECTIVITY_ACTION intent for ["

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v1

    #@79
    const-string v2, "], SM cause : "

    #@7b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSmCause()I

    #@82
    move-result v2

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@8e
    .line 3002
    :cond_8e
    return-object v0
.end method

.method private static makeWimaxStateTracker(Landroid/content/Context;Landroid/os/Handler;)Landroid/net/NetworkStateTracker;
    .registers 22
    .parameter "context"
    .parameter "trackerHandler"

    #@0
    .prologue
    .line 978
    const/4 v13, 0x0

    #@1
    .line 979
    .local v13, wimaxStateTrackerClass:Ljava/lang/Class;
    const/4 v10, 0x0

    #@2
    .line 987
    .local v10, wimaxServiceClass:Ljava/lang/Class;
    const/4 v12, 0x0

    #@3
    .line 989
    .local v12, wimaxStateTracker:Landroid/net/NetworkStateTracker;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v17

    #@7
    const v18, 0x111003f

    #@a
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@d
    move-result v3

    #@e
    .line 992
    .local v3, isWimaxEnabled:Z
    if-eqz v3, :cond_14e

    #@10
    .line 994
    :try_start_10
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@13
    move-result-object v17

    #@14
    const v18, 0x1040043

    #@17
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    .line 996
    .local v6, wimaxJarLocation:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v17

    #@1f
    const v18, 0x1040044

    #@22
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@25
    move-result-object v7

    #@26
    .line 998
    .local v7, wimaxLibLocation:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29
    move-result-object v17

    #@2a
    const v18, 0x1040045

    #@2d
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@30
    move-result-object v9

    #@31
    .line 1000
    .local v9, wimaxManagerClassName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@34
    move-result-object v17

    #@35
    const v18, 0x1040046

    #@38
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3b
    move-result-object v11

    #@3c
    .line 1002
    .local v11, wimaxServiceClassName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3f
    move-result-object v17

    #@40
    const v18, 0x1040047

    #@43
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v14

    #@47
    .line 1005
    .local v14, wimaxStateTrackerClassName:Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    #@49
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v18, "wimaxJarLocation: "

    #@4e
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v17

    #@52
    move-object/from16 v0, v17

    #@54
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v17

    #@58
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v17

    #@5c
    invoke-static/range {v17 .. v17}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@5f
    .line 1006
    new-instance v5, Ldalvik/system/DexClassLoader;

    #@61
    new-instance v17, Landroid/content/ContextWrapper;

    #@63
    move-object/from16 v0, v17

    #@65
    move-object/from16 v1, p0

    #@67
    invoke-direct {v0, v1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    #@6a
    invoke-virtual/range {v17 .. v17}, Landroid/content/ContextWrapper;->getCacheDir()Ljava/io/File;

    #@6d
    move-result-object v17

    #@6e
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@71
    move-result-object v17

    #@72
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@75
    move-result-object v18

    #@76
    move-object/from16 v0, v17

    #@78
    move-object/from16 v1, v18

    #@7a
    invoke-direct {v5, v6, v0, v7, v1}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V
    :try_end_7d
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_10 .. :try_end_7d} :catch_127

    #@7d
    .line 1011
    .local v5, wimaxClassLoader:Ldalvik/system/DexClassLoader;
    :try_start_7d
    invoke-virtual {v5, v9}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@80
    move-result-object v8

    #@81
    .line 1012
    .local v8, wimaxManagerClass:Ljava/lang/Class;
    invoke-virtual {v5, v14}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@84
    move-result-object v13

    #@85
    .line 1013
    invoke-virtual {v5, v11}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_88
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7d .. :try_end_88} :catch_109
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7d .. :try_end_88} :catch_127

    #@88
    move-result-object v10

    #@89
    .line 1024
    :try_start_89
    const-string v17, "Starting Wimax Service... "

    #@8b
    invoke-static/range {v17 .. v17}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@8e
    .line 1026
    const/16 v17, 0x2

    #@90
    move/from16 v0, v17

    #@92
    new-array v0, v0, [Ljava/lang/Class;

    #@94
    move-object/from16 v17, v0

    #@96
    const/16 v18, 0x0

    #@98
    const-class v19, Landroid/content/Context;

    #@9a
    aput-object v19, v17, v18

    #@9c
    const/16 v18, 0x1

    #@9e
    const-class v19, Landroid/os/Handler;

    #@a0
    aput-object v19, v17, v18

    #@a2
    move-object/from16 v0, v17

    #@a4
    invoke-virtual {v13, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@a7
    move-result-object v16

    #@a8
    .line 1028
    .local v16, wmxStTrkrConst:Ljava/lang/reflect/Constructor;
    const/16 v17, 0x2

    #@aa
    move/from16 v0, v17

    #@ac
    new-array v0, v0, [Ljava/lang/Object;

    #@ae
    move-object/from16 v17, v0

    #@b0
    const/16 v18, 0x0

    #@b2
    aput-object p0, v17, v18

    #@b4
    const/16 v18, 0x1

    #@b6
    aput-object p1, v17, v18

    #@b8
    invoke-virtual/range {v16 .. v17}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@bb
    move-result-object v17

    #@bc
    move-object/from16 v0, v17

    #@be
    check-cast v0, Landroid/net/NetworkStateTracker;

    #@c0
    move-object v12, v0

    #@c1
    .line 1031
    const/16 v17, 0x2

    #@c3
    move/from16 v0, v17

    #@c5
    new-array v0, v0, [Ljava/lang/Class;

    #@c7
    move-object/from16 v17, v0

    #@c9
    const/16 v18, 0x0

    #@cb
    const-class v19, Landroid/content/Context;

    #@cd
    aput-object v19, v17, v18

    #@cf
    const/16 v18, 0x1

    #@d1
    aput-object v13, v17, v18

    #@d3
    move-object/from16 v0, v17

    #@d5
    invoke-virtual {v10, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@d8
    move-result-object v15

    #@d9
    .line 1033
    .local v15, wmxSrvConst:Ljava/lang/reflect/Constructor;
    const/16 v17, 0x1

    #@db
    move/from16 v0, v17

    #@dd
    invoke-virtual {v15, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    #@e0
    .line 1034
    const/16 v17, 0x2

    #@e2
    move/from16 v0, v17

    #@e4
    new-array v0, v0, [Ljava/lang/Object;

    #@e6
    move-object/from16 v17, v0

    #@e8
    const/16 v18, 0x0

    #@ea
    aput-object p0, v17, v18

    #@ec
    const/16 v18, 0x1

    #@ee
    aput-object v12, v17, v18

    #@f0
    move-object/from16 v0, v17

    #@f2
    invoke-virtual {v15, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@f5
    move-result-object v4

    #@f6
    check-cast v4, Landroid/os/IBinder;

    #@f8
    .line 1035
    .local v4, svcInvoker:Landroid/os/IBinder;
    const/16 v17, 0x0

    #@fa
    move/from16 v0, v17

    #@fc
    invoke-virtual {v15, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    #@ff
    .line 1037
    const-string v17, "WiMax"

    #@101
    move-object/from16 v0, v17

    #@103
    invoke-static {v0, v4}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_106
    .catch Ljava/lang/Exception; {:try_start_89 .. :try_end_106} :catch_130

    #@106
    move-object/from16 v17, v12

    #@108
    .line 1048
    .end local v4           #svcInvoker:Landroid/os/IBinder;
    .end local v5           #wimaxClassLoader:Ldalvik/system/DexClassLoader;
    .end local v6           #wimaxJarLocation:Ljava/lang/String;
    .end local v7           #wimaxLibLocation:Ljava/lang/String;
    .end local v8           #wimaxManagerClass:Ljava/lang/Class;
    .end local v9           #wimaxManagerClassName:Ljava/lang/String;
    .end local v11           #wimaxServiceClassName:Ljava/lang/String;
    .end local v14           #wimaxStateTrackerClassName:Ljava/lang/String;
    .end local v15           #wmxSrvConst:Ljava/lang/reflect/Constructor;
    .end local v16           #wmxStTrkrConst:Ljava/lang/reflect/Constructor;
    :goto_108
    return-object v17

    #@109
    .line 1014
    .restart local v5       #wimaxClassLoader:Ldalvik/system/DexClassLoader;
    .restart local v6       #wimaxJarLocation:Ljava/lang/String;
    .restart local v7       #wimaxLibLocation:Ljava/lang/String;
    .restart local v9       #wimaxManagerClassName:Ljava/lang/String;
    .restart local v11       #wimaxServiceClassName:Ljava/lang/String;
    .restart local v14       #wimaxStateTrackerClassName:Ljava/lang/String;
    :catch_109
    move-exception v2

    #@10a
    .line 1015
    .local v2, ex:Ljava/lang/ClassNotFoundException;
    :try_start_10a
    new-instance v17, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v18, "Exception finding Wimax classes: "

    #@111
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v17

    #@115
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->toString()Ljava/lang/String;

    #@118
    move-result-object v18

    #@119
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v17

    #@11d
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v17

    #@121
    invoke-static/range {v17 .. v17}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V
    :try_end_124
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_10a .. :try_end_124} :catch_127

    #@124
    .line 1016
    const/16 v17, 0x0

    #@126
    goto :goto_108

    #@127
    .line 1018
    .end local v2           #ex:Ljava/lang/ClassNotFoundException;
    .end local v5           #wimaxClassLoader:Ldalvik/system/DexClassLoader;
    .end local v6           #wimaxJarLocation:Ljava/lang/String;
    .end local v7           #wimaxLibLocation:Ljava/lang/String;
    .end local v9           #wimaxManagerClassName:Ljava/lang/String;
    .end local v11           #wimaxServiceClassName:Ljava/lang/String;
    .end local v14           #wimaxStateTrackerClassName:Ljava/lang/String;
    :catch_127
    move-exception v2

    #@128
    .line 1019
    .local v2, ex:Landroid/content/res/Resources$NotFoundException;
    const-string v17, "Wimax Resources does not exist!!! "

    #@12a
    invoke-static/range {v17 .. v17}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@12d
    .line 1020
    const/16 v17, 0x0

    #@12f
    goto :goto_108

    #@130
    .line 1039
    .end local v2           #ex:Landroid/content/res/Resources$NotFoundException;
    .restart local v5       #wimaxClassLoader:Ldalvik/system/DexClassLoader;
    .restart local v6       #wimaxJarLocation:Ljava/lang/String;
    .restart local v7       #wimaxLibLocation:Ljava/lang/String;
    .restart local v8       #wimaxManagerClass:Ljava/lang/Class;
    .restart local v9       #wimaxManagerClassName:Ljava/lang/String;
    .restart local v11       #wimaxServiceClassName:Ljava/lang/String;
    .restart local v14       #wimaxStateTrackerClassName:Ljava/lang/String;
    :catch_130
    move-exception v2

    #@131
    .line 1040
    .local v2, ex:Ljava/lang/Exception;
    new-instance v17, Ljava/lang/StringBuilder;

    #@133
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v18, "Exception creating Wimax classes: "

    #@138
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v17

    #@13c
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@13f
    move-result-object v18

    #@140
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v17

    #@144
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@147
    move-result-object v17

    #@148
    invoke-static/range {v17 .. v17}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@14b
    .line 1041
    const/16 v17, 0x0

    #@14d
    goto :goto_108

    #@14e
    .line 1044
    .end local v2           #ex:Ljava/lang/Exception;
    .end local v5           #wimaxClassLoader:Ldalvik/system/DexClassLoader;
    .end local v6           #wimaxJarLocation:Ljava/lang/String;
    .end local v7           #wimaxLibLocation:Ljava/lang/String;
    .end local v8           #wimaxManagerClass:Ljava/lang/Class;
    .end local v9           #wimaxManagerClassName:Ljava/lang/String;
    .end local v11           #wimaxServiceClassName:Ljava/lang/String;
    .end local v14           #wimaxStateTrackerClassName:Ljava/lang/String;
    :cond_14e
    const-string v17, "Wimax is not enabled or not added to the network attributes!!! "

    #@150
    invoke-static/range {v17 .. v17}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@153
    .line 1045
    const/16 v17, 0x0

    #@155
    goto :goto_108
.end method

.method private modifyRoute(Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/RouteInfo;IZZ)Z
    .registers 16
    .parameter "ifaceName"
    .parameter "lp"
    .parameter "r"
    .parameter "cycleCount"
    .parameter "doAdd"
    .parameter "toDefaultTable"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2340
    if-eqz p1, :cond_7

    #@3
    if-eqz p2, :cond_7

    #@5
    if-nez p3, :cond_33

    #@7
    .line 2341
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v1, "modifyRoute got unexpected null: "

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    const-string v1, ", "

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    const-string v1, ", "

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@31
    move v0, v8

    #@32
    .line 2424
    :goto_32
    return v0

    #@33
    .line 2345
    :cond_33
    const/16 v0, 0xa

    #@35
    if-le p4, v0, :cond_3e

    #@37
    .line 2346
    const-string v0, "Error modifying route - too much recursion"

    #@39
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@3c
    move v0, v8

    #@3d
    .line 2347
    goto :goto_32

    #@3e
    .line 2350
    :cond_3e
    invoke-virtual {p3}, Landroid/net/RouteInfo;->isHostRoute()Z

    #@41
    move-result v0

    #@42
    if-nez v0, :cond_72

    #@44
    .line 2351
    invoke-virtual {p2}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@47
    move-result-object v0

    #@48
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v0, v1}, Landroid/net/RouteInfo;->selectBestRoute(Ljava/util/Collection;Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@4f
    move-result-object v3

    #@50
    .line 2352
    .local v3, bestRoute:Landroid/net/RouteInfo;
    if-eqz v3, :cond_72

    #@52
    .line 2353
    invoke-virtual {v3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v0

    #@5e
    if-eqz v0, :cond_a6

    #@60
    .line 2355
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@63
    move-result-object v0

    #@64
    invoke-static {v0}, Landroid/net/RouteInfo;->makeHostRoute(Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@67
    move-result-object v3

    #@68
    .line 2361
    :goto_68
    add-int/lit8 v4, p4, 0x1

    #@6a
    move-object v0, p0

    #@6b
    move-object v1, p1

    #@6c
    move-object v2, p2

    #@6d
    move v5, p5

    #@6e
    move v6, p6

    #@6f
    invoke-direct/range {v0 .. v6}, Lcom/android/server/ConnectivityService;->modifyRoute(Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/RouteInfo;IZZ)Z

    #@72
    .line 2364
    .end local v3           #bestRoute:Landroid/net/RouteInfo;
    :cond_72
    if-eqz p5, :cond_d6

    #@74
    .line 2365
    new-instance v0, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v1, "Adding "

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v0

    #@83
    const-string v1, " for interface "

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@94
    .line 2367
    if-eqz p6, :cond_d0

    #@96
    .line 2369
    :try_start_96
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mRoutingTableLock:Ljava/lang/Object;

    #@98
    monitor-enter v1
    :try_end_99
    .catch Ljava/lang/Exception; {:try_start_96 .. :try_end_99} :catch_b6

    #@99
    .line 2371
    :try_start_99
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mAddedRoutes:Ljava/util/Collection;

    #@9b
    invoke-interface {v0, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@9e
    .line 2376
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@a0
    invoke-interface {v0, p1, p3}, Landroid/os/INetworkManagementService;->addRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V

    #@a3
    .line 2378
    monitor-exit v1
    :try_end_a4
    .catchall {:try_start_99 .. :try_end_a4} :catchall_b3

    #@a4
    .line 2424
    :goto_a4
    const/4 v0, 0x1

    #@a5
    goto :goto_32

    #@a6
    .line 2359
    .restart local v3       #bestRoute:Landroid/net/RouteInfo;
    :cond_a6
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@a9
    move-result-object v0

    #@aa
    invoke-virtual {v3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@ad
    move-result-object v1

    #@ae
    invoke-static {v0, v1}, Landroid/net/RouteInfo;->makeHostRoute(Ljava/net/InetAddress;Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@b1
    move-result-object v3

    #@b2
    goto :goto_68

    #@b3
    .line 2378
    .end local v3           #bestRoute:Landroid/net/RouteInfo;
    :catchall_b3
    move-exception v0

    #@b4
    :try_start_b4
    monitor-exit v1
    :try_end_b5
    .catchall {:try_start_b4 .. :try_end_b5} :catchall_b3

    #@b5
    :try_start_b5
    throw v0
    :try_end_b6
    .catch Ljava/lang/Exception; {:try_start_b5 .. :try_end_b6} :catch_b6

    #@b6
    .line 2383
    :catch_b6
    move-exception v7

    #@b7
    .line 2385
    .local v7, e:Ljava/lang/Exception;
    new-instance v0, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v1, "Exception trying to add a route: "

    #@be
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v0

    #@c2
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v0

    #@c6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v0

    #@ca
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@cd
    move v0, v8

    #@ce
    .line 2386
    goto/16 :goto_32

    #@d0
    .line 2381
    .end local v7           #e:Ljava/lang/Exception;
    :cond_d0
    :try_start_d0
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@d2
    invoke-interface {v0, p1, p3}, Landroid/os/INetworkManagementService;->addSecondaryRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    :try_end_d5
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_d5} :catch_b6

    #@d5
    goto :goto_a4

    #@d6
    .line 2391
    :cond_d6
    if-eqz p6, :cond_14a

    #@d8
    .line 2393
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mRoutingTableLock:Ljava/lang/Object;

    #@da
    monitor-enter v1

    #@db
    .line 2395
    :try_start_db
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mAddedRoutes:Ljava/util/Collection;

    #@dd
    invoke-interface {v0, p3}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@e0
    .line 2398
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mAddedRoutes:Ljava/util/Collection;

    #@e2
    invoke-interface {v0, p3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@e5
    move-result v0

    #@e6
    if-nez v0, :cond_12d

    #@e8
    .line 2399
    new-instance v0, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v2, "Removing "

    #@ef
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v0

    #@f3
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v0

    #@f7
    const-string v2, " for interface "

    #@f9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v0

    #@fd
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v0

    #@101
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v0

    #@105
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_108
    .catchall {:try_start_db .. :try_end_108} :catchall_10f

    #@108
    .line 2401
    :try_start_108
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@10a
    invoke-interface {v0, p1, p3}, Landroid/os/INetworkManagementService;->removeRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    :try_end_10d
    .catchall {:try_start_108 .. :try_end_10d} :catchall_10f
    .catch Ljava/lang/Exception; {:try_start_108 .. :try_end_10d} :catch_112

    #@10d
    .line 2411
    :goto_10d
    :try_start_10d
    monitor-exit v1

    #@10e
    goto :goto_a4

    #@10f
    :catchall_10f
    move-exception v0

    #@110
    monitor-exit v1
    :try_end_111
    .catchall {:try_start_10d .. :try_end_111} :catchall_10f

    #@111
    throw v0

    #@112
    .line 2402
    :catch_112
    move-exception v7

    #@113
    .line 2404
    .restart local v7       #e:Ljava/lang/Exception;
    :try_start_113
    new-instance v0, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    const-string v2, "Exception trying to remove a route: "

    #@11a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v0

    #@11e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v0

    #@122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@125
    move-result-object v0

    #@126
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@129
    .line 2405
    monitor-exit v1

    #@12a
    move v0, v8

    #@12b
    goto/16 :goto_32

    #@12d
    .line 2408
    .end local v7           #e:Ljava/lang/Exception;
    :cond_12d
    new-instance v0, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v2, "not removing "

    #@134
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v0

    #@138
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v0

    #@13c
    const-string v2, " as it\'s still in use"

    #@13e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v0

    #@142
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v0

    #@146
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_149
    .catchall {:try_start_113 .. :try_end_149} :catchall_10f

    #@149
    goto :goto_10d

    #@14a
    .line 2414
    :cond_14a
    new-instance v0, Ljava/lang/StringBuilder;

    #@14c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14f
    const-string v1, "Removing "

    #@151
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v0

    #@155
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v0

    #@159
    const-string v1, " for interface "

    #@15b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v0

    #@15f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v0

    #@163
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v0

    #@167
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@16a
    .line 2416
    :try_start_16a
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@16c
    invoke-interface {v0, p1, p3}, Landroid/os/INetworkManagementService;->removeSecondaryRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    :try_end_16f
    .catch Ljava/lang/Exception; {:try_start_16a .. :try_end_16f} :catch_171

    #@16f
    goto/16 :goto_a4

    #@171
    .line 2417
    :catch_171
    move-exception v7

    #@172
    .line 2419
    .restart local v7       #e:Ljava/lang/Exception;
    new-instance v0, Ljava/lang/StringBuilder;

    #@174
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v1, "Exception trying to remove a route: "

    #@179
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v0

    #@17d
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v0

    #@181
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@184
    move-result-object v0

    #@185
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@188
    move v0, v8

    #@189
    .line 2420
    goto/16 :goto_32
.end method

.method private modifyRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;ZZ)Z
    .registers 12
    .parameter "lp"
    .parameter "addr"
    .parameter "doAdd"
    .parameter "toDefaultTable"

    #@0
    .prologue
    .line 2322
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p2}, Landroid/net/RouteInfo;->selectBestRoute(Ljava/util/Collection;Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@7
    move-result-object v3

    #@8
    .line 2323
    .local v3, bestRoute:Landroid/net/RouteInfo;
    if-nez v3, :cond_1c

    #@a
    .line 2324
    invoke-static {p2}, Landroid/net/RouteInfo;->makeHostRoute(Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@d
    move-result-object v3

    #@e
    .line 2335
    :goto_e
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    const/4 v4, 0x0

    #@13
    move-object v0, p0

    #@14
    move-object v2, p1

    #@15
    move v5, p3

    #@16
    move v6, p4

    #@17
    invoke-direct/range {v0 .. v6}, Lcom/android/server/ConnectivityService;->modifyRoute(Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/RouteInfo;IZZ)Z

    #@1a
    move-result v0

    #@1b
    return v0

    #@1c
    .line 2326
    :cond_1c
    invoke-virtual {v3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0, p2}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2b

    #@26
    .line 2328
    invoke-static {p2}, Landroid/net/RouteInfo;->makeHostRoute(Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@29
    move-result-object v3

    #@2a
    goto :goto_e

    #@2b
    .line 2332
    :cond_2b
    invoke-virtual {v3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@2e
    move-result-object v0

    #@2f
    invoke-static {p2, v0}, Landroid/net/RouteInfo;->makeHostRoute(Ljava/net/InetAddress;Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@32
    move-result-object v3

    #@33
    goto :goto_e
.end method

.method private notifyFakeDataStateChangedToDisconnect()V
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3321
    new-instance v0, Landroid/content/Intent;

    #@3
    const-string v3, "android.intent.action.ANY_DATA_STATE"

    #@5
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8
    .line 3322
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "state"

    #@a
    const-string v4, "DISCONNECTED"

    #@c
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@f
    .line 3324
    const-string v3, "networkUnvailable"

    #@11
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@14
    .line 3325
    const-string v3, "reason"

    #@16
    const-string v4, "WIFI_CONNECTED"

    #@18
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b
    .line 3329
    invoke-virtual {p0, v2}, Lcom/android/server/ConnectivityService;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@1e
    move-result-object v1

    #@1f
    .line 3330
    .local v1, mobile:Landroid/net/NetworkInfo;
    if-eqz v1, :cond_6c

    #@21
    .line 3331
    const-string v3, "networkRoaming"

    #@23
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isRoaming()Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_2a

    #@29
    const/4 v2, 0x1

    #@2a
    :cond_2a
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@2d
    .line 3332
    const-string v2, "apn"

    #@2f
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@36
    .line 3333
    const-string v2, "apnType"

    #@38
    const-string v3, "default"

    #@3a
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3d
    .line 3335
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@3f
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@41
    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@44
    .line 3337
    new-instance v2, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v3, "minjeon] SKIP mobile make disconnect intent => "

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    const-string v3, " : type "

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    #@60
    move-result v3

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v2

    #@69
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@6c
    .line 3339
    :cond_6c
    return-void
.end method

.method private reassessPidDns(IZ)V
    .registers 20
    .parameter "myPid"
    .parameter "doBump"

    #@0
    .prologue
    .line 3764
    new-instance v15, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v16, "reassessPidDns for pid "

    #@7
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v15

    #@b
    move/from16 v0, p1

    #@d
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v15

    #@11
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v15

    #@15
    invoke-static {v15}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@18
    .line 3765
    move-object/from16 v0, p0

    #@1a
    iget-object v2, v0, Lcom/android/server/ConnectivityService;->mPriorityList:[I

    #@1c
    .local v2, arr$:[I
    array-length v8, v2

    #@1d
    .local v8, len$:I
    const/4 v5, 0x0

    #@1e
    .local v5, i$:I
    :goto_1e
    if-ge v5, v8, :cond_8a

    #@20
    aget v4, v2, v5

    #@22
    .line 3766
    .local v4, i:I
    move-object/from16 v0, p0

    #@24
    iget-object v15, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@26
    aget-object v15, v15, v4

    #@28
    invoke-virtual {v15}, Landroid/net/NetworkConfig;->isDefault()Z

    #@2b
    move-result v15

    #@2c
    if-eqz v15, :cond_31

    #@2e
    .line 3765
    :cond_2e
    add-int/lit8 v5, v5, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 3769
    :cond_31
    move-object/from16 v0, p0

    #@33
    iget-object v15, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@35
    aget-object v9, v15, v4

    #@37
    .line 3773
    .local v9, nt:Landroid/net/NetworkStateTracker;
    if-eqz v9, :cond_2e

    #@39
    .line 3778
    invoke-interface {v9}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@3c
    move-result-object v15

    #@3d
    invoke-virtual {v15}, Landroid/net/NetworkInfo;->isConnected()Z

    #@40
    move-result v15

    #@41
    if-eqz v15, :cond_2e

    #@43
    invoke-interface {v9}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@46
    move-result v15

    #@47
    if-nez v15, :cond_2e

    #@49
    .line 3780
    invoke-interface {v9}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@4c
    move-result-object v10

    #@4d
    .line 3781
    .local v10, p:Landroid/net/LinkProperties;
    if-eqz v10, :cond_2e

    #@4f
    .line 3782
    move-object/from16 v0, p0

    #@51
    iget-object v15, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@53
    aget-object v12, v15, v4

    #@55
    .line 3783
    .local v12, pids:Ljava/util/List;
    const/4 v7, 0x0

    #@56
    .local v7, j:I
    :goto_56
    invoke-interface {v12}, Ljava/util/List;->size()I

    #@59
    move-result v15

    #@5a
    if-ge v7, v15, :cond_2e

    #@5c
    .line 3784
    invoke-interface {v12, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5f
    move-result-object v11

    #@60
    check-cast v11, Ljava/lang/Integer;

    #@62
    .line 3785
    .local v11, pid:Ljava/lang/Integer;
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@65
    move-result v15

    #@66
    move/from16 v0, p1

    #@68
    if-ne v15, v0, :cond_87

    #@6a
    .line 3786
    invoke-virtual {v10}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@6d
    move-result-object v3

    #@6e
    .line 3791
    .local v3, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-virtual {v10}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@71
    move-result-object v1

    #@72
    .line 3792
    .local v1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    move-object/from16 v0, p0

    #@74
    invoke-direct {v0, v1}, Lcom/android/server/ConnectivityService;->getIPtype(Ljava/util/Collection;)I

    #@77
    move-result v6

    #@78
    .line 3793
    .local v6, iptype:I
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@7b
    move-result v15

    #@7c
    move-object/from16 v0, p0

    #@7e
    invoke-direct {v0, v3, v15, v6}, Lcom/android/server/ConnectivityService;->writePidDns(Ljava/util/Collection;II)Z

    #@81
    .line 3795
    if-eqz p2, :cond_86

    #@83
    .line 3796
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->bumpDns()V

    #@86
    .line 3823
    .end local v1           #addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v3           #dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v6           #iptype:I
    .end local v7           #j:I
    .end local v9           #nt:Landroid/net/NetworkStateTracker;
    .end local v10           #p:Landroid/net/LinkProperties;
    .end local v11           #pid:Ljava/lang/Integer;
    .end local v12           #pids:Ljava/util/List;
    :cond_86
    :goto_86
    return-void

    #@87
    .line 3783
    .restart local v7       #j:I
    .restart local v9       #nt:Landroid/net/NetworkStateTracker;
    .restart local v10       #p:Landroid/net/LinkProperties;
    .restart local v11       #pid:Ljava/lang/Integer;
    .restart local v12       #pids:Ljava/util/List;
    :cond_87
    add-int/lit8 v7, v7, 0x1

    #@89
    goto :goto_56

    #@8a
    .line 3808
    .end local v4           #i:I
    .end local v7           #j:I
    .end local v9           #nt:Landroid/net/NetworkStateTracker;
    .end local v10           #p:Landroid/net/LinkProperties;
    .end local v11           #pid:Ljava/lang/Integer;
    .end local v12           #pids:Ljava/util/List;
    :cond_8a
    move-object/from16 v0, p0

    #@8c
    iget-object v15, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@8e
    iget-boolean v15, v15, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@90
    if-eqz v15, :cond_c4

    #@92
    .line 3810
    invoke-direct/range {p0 .. p1}, Lcom/android/server/ConnectivityService;->getSysid(I)I

    #@95
    move-result v14

    #@96
    .line 3817
    .local v14, sysid:I
    :goto_96
    const/4 v4, 0x1

    #@97
    .line 3818
    .restart local v4       #i:I
    :goto_97
    new-instance v15, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v16, "net.dns"

    #@9e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v15

    #@a2
    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v15

    #@a6
    const-string v16, "."

    #@a8
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v15

    #@ac
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v15

    #@b0
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v13

    #@b4
    .line 3819
    .local v13, prop:Ljava/lang/String;
    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@b7
    move-result-object v15

    #@b8
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    #@bb
    move-result v15

    #@bc
    if-nez v15, :cond_c7

    #@be
    .line 3820
    if-eqz p2, :cond_86

    #@c0
    .line 3821
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->bumpDns()V

    #@c3
    goto :goto_86

    #@c4
    .line 3814
    .end local v4           #i:I
    .end local v13           #prop:Ljava/lang/String;
    .end local v14           #sysid:I
    :cond_c4
    move/from16 v14, p1

    #@c6
    .restart local v14       #sysid:I
    goto :goto_96

    #@c7
    .line 3825
    .restart local v4       #i:I
    .restart local v13       #prop:Ljava/lang/String;
    :cond_c7
    const-string v15, ""

    #@c9
    invoke-static {v13, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@cc
    .line 3817
    add-int/lit8 v4, v4, 0x1

    #@ce
    goto :goto_97
.end method

.method private removeDataActivityTracking(I)V
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 3407
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    aget-object v1, v2, p1

    #@4
    .line 3408
    .local v1, net:Landroid/net/NetworkStateTracker;
    invoke-interface {v1}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 3410
    .local v0, iface:Ljava/lang/String;
    if-eqz v0, :cond_1c

    #@e
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeMobile(I)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_17

    #@14
    const/4 v2, 0x1

    #@15
    if-ne v2, p1, :cond_1c

    #@17
    .line 3414
    :cond_17
    :try_start_17
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@19
    invoke-interface {v2, v0}, Landroid/os/INetworkManagementService;->removeIdleTimer(Ljava/lang/String;)V
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_1c} :catch_1d

    #@1c
    .line 3418
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 3415
    :catch_1d
    move-exception v2

    #@1e
    goto :goto_1c
.end method

.method private removeRoute(Landroid/net/LinkProperties;Landroid/net/RouteInfo;Z)Z
    .registers 11
    .parameter "p"
    .parameter "r"
    .parameter "toDefaultTable"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2309
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    move-object v0, p0

    #@6
    move-object v2, p1

    #@7
    move-object v3, p2

    #@8
    move v5, v4

    #@9
    move v6, p3

    #@a
    invoke-direct/range {v0 .. v6}, Lcom/android/server/ConnectivityService;->modifyRoute(Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/RouteInfo;IZZ)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method private removeRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z
    .registers 5
    .parameter "lp"
    .parameter "addr"

    #@0
    .prologue
    .line 2317
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/ConnectivityService;->modifyRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;ZZ)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private restoreAPN2Disable()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 5169
    const-string v0, "ConnectivityService"

    #@3
    const-string v1, "restoreAPN2Disable"

    #@5
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 5171
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    const-string v1, "apn2_disable"

    #@10
    const/4 v2, 0x0

    #@11
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@14
    move-result v0

    #@15
    if-ne v0, v3, :cond_47

    #@17
    .line 5173
    const-string v0, "ConnectivityService"

    #@19
    const-string v1, "[APN2 Disable]!!!!!restoreAPN2Disable: apn2-disable"

    #@1b
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 5175
    const-string v0, "ril.current.apn2-disable"

    #@20
    const-string v1, "1"

    #@22
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 5176
    const/4 v0, 0x0

    #@26
    invoke-direct {p0, v0, v3}, Lcom/android/server/ConnectivityService;->updateApnDB(Ljava/lang/String;I)Z

    #@29
    .line 5179
    const-string v0, "ConnectivityService"

    #@2b
    new-instance v1, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v2, "[APN2 Disable]!!!!! ril.current.apn2-disable : "

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, "ril.current.apn2-disable"

    #@38
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 5185
    :cond_47
    return-void
.end method

.method private sendConnectedBroadcastDelayed(Landroid/net/NetworkInfo;I)V
    .registers 4
    .parameter "info"
    .parameter "delayMs"

    #@0
    .prologue
    .line 2965
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/server/ConnectivityService;->sendGeneralBroadcast(Landroid/net/NetworkInfo;Ljava/lang/String;)V

    #@5
    .line 2966
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    #@7
    invoke-direct {p0, p1, v0, p2}, Lcom/android/server/ConnectivityService;->sendGeneralBroadcastDelayed(Landroid/net/NetworkInfo;Ljava/lang/String;I)V

    #@a
    .line 2967
    return-void
.end method

.method private sendDataActivityBroadcast(IZ)V
    .registers 14
    .parameter "deviceType"
    .parameter "active"

    #@0
    .prologue
    .line 3014
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v0, "android.net.conn.DATA_ACTIVITY_CHANGE"

    #@4
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 3015
    .local v1, intent:Landroid/content/Intent;
    const-string v0, "deviceType"

    #@9
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c
    .line 3016
    const-string v0, "isActive"

    #@e
    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@11
    .line 3017
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@14
    move-result-wide v9

    #@15
    .line 3019
    .local v9, ident:J
    :try_start_15
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@17
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@19
    const-string v3, "android.permission.RECEIVE_DATA_ACTIVITY_CHANGE"

    #@1b
    const/4 v4, 0x0

    #@1c
    const/4 v5, 0x0

    #@1d
    const/4 v6, 0x0

    #@1e
    const/4 v7, 0x0

    #@1f
    const/4 v8, 0x0

    #@20
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_23
    .catchall {:try_start_15 .. :try_end_23} :catchall_27

    #@23
    .line 3022
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@26
    .line 3024
    return-void

    #@27
    .line 3022
    :catchall_27
    move-exception v0

    #@28
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2b
    throw v0
.end method

.method private sendGeneralBroadcast(Landroid/net/NetworkInfo;Ljava/lang/String;)V
    .registers 4
    .parameter "info"
    .parameter "bcastType"

    #@0
    .prologue
    .line 3006
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->makeGeneralIntent(Landroid/net/NetworkInfo;Ljava/lang/String;)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Lcom/android/server/ConnectivityService;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@7
    .line 3007
    return-void
.end method

.method private sendGeneralBroadcastDelayed(Landroid/net/NetworkInfo;Ljava/lang/String;I)V
    .registers 5
    .parameter "info"
    .parameter "bcastType"
    .parameter "delayMs"

    #@0
    .prologue
    .line 3010
    invoke-direct {p0, p1, p2}, Lcom/android/server/ConnectivityService;->makeGeneralIntent(Landroid/net/NetworkInfo;Ljava/lang/String;)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p3}, Lcom/android/server/ConnectivityService;->sendStickyBroadcastDelayed(Landroid/content/Intent;I)V

    #@7
    .line 3011
    return-void
.end method

.method private sendInetConditionBroadcast(Landroid/net/NetworkInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 2970
    const-string v0, "android.net.conn.INET_CONDITION_ACTION"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/server/ConnectivityService;->sendGeneralBroadcast(Landroid/net/NetworkInfo;Ljava/lang/String;)V

    #@5
    .line 2971
    return-void
.end method

.method private sendProxyBroadcast(Landroid/net/ProxyProperties;)V
    .registers 8
    .parameter "proxy"

    #@0
    .prologue
    .line 4771
    if-nez p1, :cond_c

    #@2
    new-instance p1, Landroid/net/ProxyProperties;

    #@4
    .end local p1
    const-string v3, ""

    #@6
    const/4 v4, 0x0

    #@7
    const-string v5, ""

    #@9
    invoke-direct {p1, v3, v4, v5}, Landroid/net/ProxyProperties;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@c
    .line 4772
    .restart local p1
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "sending Proxy Broadcast for "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@22
    .line 4773
    new-instance v2, Landroid/content/Intent;

    #@24
    const-string v3, "android.intent.action.PROXY_CHANGE"

    #@26
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@29
    .line 4774
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x2800

    #@2b
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2e
    .line 4776
    const-string v3, "proxy"

    #@30
    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@33
    .line 4777
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@36
    move-result-wide v0

    #@37
    .line 4779
    .local v0, ident:J
    :try_start_37
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@39
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@3b
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_3e
    .catchall {:try_start_37 .. :try_end_3e} :catchall_42

    #@3e
    .line 4781
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@41
    .line 4783
    return-void

    #@42
    .line 4781
    :catchall_42
    move-exception v3

    #@43
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@46
    throw v3
.end method

.method private sendStickyBroadcast(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 3103
    monitor-enter p0

    #@1
    .line 3104
    :try_start_1
    iget-boolean v2, p0, Lcom/android/server/ConnectivityService;->mSystemReady:Z

    #@3
    if-nez v2, :cond_c

    #@5
    .line 3105
    new-instance v2, Landroid/content/Intent;

    #@7
    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@a
    iput-object v2, p0, Lcom/android/server/ConnectivityService;->mInitialBroadcast:Landroid/content/Intent;

    #@c
    .line 3107
    :cond_c
    const/high16 v2, 0x800

    #@e
    invoke-virtual {p1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11
    .line 3109
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "sendStickyBroadcast: action="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2b
    .line 3112
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_2e
    .catchall {:try_start_1 .. :try_end_2e} :catchall_40

    #@2e
    move-result-wide v0

    #@2f
    .line 3114
    .local v0, ident:J
    :try_start_2f
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@31
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@33
    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_36
    .catchall {:try_start_2f .. :try_end_36} :catchall_3b

    #@36
    .line 3116
    :try_start_36
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@39
    .line 3118
    monitor-exit p0

    #@3a
    .line 3119
    return-void

    #@3b
    .line 3116
    :catchall_3b
    move-exception v2

    #@3c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3f
    throw v2

    #@40
    .line 3118
    .end local v0           #ident:J
    :catchall_40
    move-exception v2

    #@41
    monitor-exit p0
    :try_end_42
    .catchall {:try_start_36 .. :try_end_42} :catchall_40

    #@42
    throw v2
.end method

.method private sendStickyBroadcastDelayed(Landroid/content/Intent;I)V
    .registers 7
    .parameter "intent"
    .parameter "delayMs"

    #@0
    .prologue
    .line 3122
    if-gtz p2, :cond_6

    #@2
    .line 3123
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@5
    .line 3132
    :goto_5
    return-void

    #@6
    .line 3126
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "sendStickyBroadcastDelayed: delayMs="

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ", action="

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2a
    .line 3129
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@2c
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@2e
    const/16 v2, 0xc

    #@30
    invoke-virtual {v1, v2, p1}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@33
    move-result-object v1

    #@34
    int-to-long v2, p2

    #@35
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@38
    goto :goto_5
.end method

.method private setBufferSize(Ljava/lang/String;)V
    .registers 7
    .parameter "bufferSizes"

    #@0
    .prologue
    .line 3739
    :try_start_0
    const-string v3, ","

    #@2
    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    .line 3741
    .local v2, values:[Ljava/lang/String;
    array-length v3, v2

    #@7
    const/4 v4, 0x6

    #@8
    if-ne v3, v4, :cond_3d

    #@a
    .line 3742
    const-string v1, "/sys/kernel/ipv4/tcp_"

    #@c
    .line 3743
    .local v1, prefix:Ljava/lang/String;
    const-string v3, "/sys/kernel/ipv4/tcp_rmem_min"

    #@e
    const/4 v4, 0x0

    #@f
    aget-object v4, v2, v4

    #@11
    invoke-static {v3, v4}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 3744
    const-string v3, "/sys/kernel/ipv4/tcp_rmem_def"

    #@16
    const/4 v4, 0x1

    #@17
    aget-object v4, v2, v4

    #@19
    invoke-static {v3, v4}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 3745
    const-string v3, "/sys/kernel/ipv4/tcp_rmem_max"

    #@1e
    const/4 v4, 0x2

    #@1f
    aget-object v4, v2, v4

    #@21
    invoke-static {v3, v4}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 3746
    const-string v3, "/sys/kernel/ipv4/tcp_wmem_min"

    #@26
    const/4 v4, 0x3

    #@27
    aget-object v4, v2, v4

    #@29
    invoke-static {v3, v4}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 3747
    const-string v3, "/sys/kernel/ipv4/tcp_wmem_def"

    #@2e
    const/4 v4, 0x4

    #@2f
    aget-object v4, v2, v4

    #@31
    invoke-static {v3, v4}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 3748
    const-string v3, "/sys/kernel/ipv4/tcp_wmem_max"

    #@36
    const/4 v4, 0x5

    #@37
    aget-object v4, v2, v4

    #@39
    invoke-static {v3, v4}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 3755
    .end local v1           #prefix:Ljava/lang/String;
    .end local v2           #values:[Ljava/lang/String;
    :goto_3c
    return-void

    #@3d
    .line 3750
    .restart local v2       #values:[Ljava/lang/String;
    :cond_3d
    new-instance v3, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v4, "Invalid buffersize string: "

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V
    :try_end_53
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_53} :catch_54

    #@53
    goto :goto_3c

    #@54
    .line 3752
    .end local v2           #values:[Ljava/lang/String;
    :catch_54
    move-exception v0

    #@55
    .line 3753
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v4, "Can\'t set tcp buffer sizes:"

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@6b
    goto :goto_3c
.end method

.method private setLockdownTracker(Lcom/android/server/net/LockdownVpnTracker;)V
    .registers 5
    .parameter "tracker"

    #@0
    .prologue
    .line 5117
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@2
    .line 5118
    .local v0, existing:Lcom/android/server/net/LockdownVpnTracker;
    const/4 v1, 0x0

    #@3
    iput-object v1, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@5
    .line 5119
    if-eqz v0, :cond_a

    #@7
    .line 5120
    invoke-virtual {v0}, Lcom/android/server/net/LockdownVpnTracker;->shutdown()V

    #@a
    .line 5124
    :cond_a
    if-eqz p1, :cond_1a

    #@c
    .line 5125
    :try_start_c
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@e
    const/4 v2, 0x1

    #@f
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->setFirewallEnabled(Z)V

    #@12
    .line 5126
    iput-object p1, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@14
    .line 5127
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mLockdownTracker:Lcom/android/server/net/LockdownVpnTracker;

    #@16
    invoke-virtual {v1}, Lcom/android/server/net/LockdownVpnTracker;->init()V

    #@19
    .line 5134
    :goto_19
    return-void

    #@1a
    .line 5129
    :cond_1a
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@1c
    const/4 v2, 0x0

    #@1d
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->setFirewallEnabled(Z)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_20} :catch_21

    #@20
    goto :goto_19

    #@21
    .line 5131
    :catch_21
    move-exception v1

    #@22
    goto :goto_19
.end method

.method private setPidlist()V
    .registers 8

    #@0
    .prologue
    .line 3834
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mNetRequestAll:Ljava/util/List;

    #@2
    .line 3836
    .local v4, pids:Ljava/util/List;
    const-string v2, ""

    #@4
    .line 3837
    .local v2, list:Ljava/lang/String;
    const/4 v0, 0x1

    #@5
    .line 3839
    .local v0, isfirst:Z
    const/4 v1, 0x0

    #@6
    .local v1, jj:I
    :goto_6
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@9
    move-result v5

    #@a
    if-ge v1, v5, :cond_35

    #@c
    .line 3841
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v5

    #@10
    check-cast v5, Ljava/lang/Integer;

    #@12
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    .line 3843
    .local v3, pid:Ljava/lang/String;
    if-eqz v0, :cond_1d

    #@18
    .line 3845
    move-object v2, v3

    #@19
    .line 3846
    const/4 v0, 0x0

    #@1a
    .line 3839
    :goto_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_6

    #@1d
    .line 3850
    :cond_1d
    new-instance v5, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    const-string v6, ","

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    goto :goto_1a

    #@35
    .line 3854
    .end local v3           #pid:Ljava/lang/String;
    :cond_35
    new-instance v5, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v6, "pid list is : "

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    invoke-static {v5}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@4b
    .line 3855
    const-string v5, "net.pdnlist"

    #@4d
    invoke-static {v5, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 3857
    return-void
.end method

.method private setupDataActivityTracking(I)V
    .registers 8
    .parameter "type"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3375
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@3
    aget-object v1, v3, p1

    #@5
    .line 3376
    .local v1, thisNet:Landroid/net/NetworkStateTracker;
    invoke-interface {v1}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 3380
    .local v0, iface:Ljava/lang/String;
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeMobile(I)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_2e

    #@13
    .line 3381
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18
    move-result-object v3

    #@19
    const-string v4, "data_activity_timeout_mobile"

    #@1b
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1e
    move-result v2

    #@1f
    .line 3385
    .local v2, timeout:I
    const/4 p1, 0x0

    #@20
    .line 3395
    :goto_20
    if-lez v2, :cond_2d

    #@22
    if-eqz v0, :cond_2d

    #@24
    .line 3397
    :try_start_24
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@26
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-interface {v3, v0, v2, v4}, Landroid/os/INetworkManagementService;->addIdleTimer(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_2d
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2d} :catch_40

    #@2d
    .line 3401
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 3386
    .end local v2           #timeout:I
    :cond_2e
    const/4 v3, 0x1

    #@2f
    if-ne v3, p1, :cond_3e

    #@31
    .line 3387
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@33
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v3

    #@37
    const-string v4, "data_activity_timeout_wifi"

    #@39
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3c
    move-result v2

    #@3d
    .restart local v2       #timeout:I
    goto :goto_20

    #@3e
    .line 3392
    .end local v2           #timeout:I
    :cond_3e
    const/4 v2, 0x0

    #@3f
    .restart local v2       #timeout:I
    goto :goto_20

    #@40
    .line 3398
    :catch_40
    move-exception v3

    #@41
    goto :goto_2d
.end method

.method private stopUsingNetworkFeature(Lcom/android/server/ConnectivityService$FeatureUser;Z)I
    .registers 15
    .parameter "u"
    .parameter "ignoreDups"

    #@0
    .prologue
    .line 2043
    iget v4, p1, Lcom/android/server/ConnectivityService$FeatureUser;->mNetworkType:I

    #@2
    .line 2044
    .local v4, networkType:I
    iget-object v2, p1, Lcom/android/server/ConnectivityService$FeatureUser;->mFeature:Ljava/lang/String;

    #@4
    .line 2045
    .local v2, feature:Ljava/lang/String;
    iget v5, p1, Lcom/android/server/ConnectivityService$FeatureUser;->mPid:I

    #@6
    .line 2046
    .local v5, pid:I
    iget v7, p1, Lcom/android/server/ConnectivityService$FeatureUser;->mUid:I

    #@8
    .line 2048
    .local v7, uid:I
    const/4 v6, 0x0

    #@9
    .line 2049
    .local v6, tracker:Landroid/net/NetworkStateTracker;
    const/4 v0, 0x0

    #@a
    .line 2052
    .local v0, callTeardown:Z
    new-instance v10, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v11, "stopUsingNetworkFeature: net "

    #@11
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v10

    #@15
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v10

    #@19
    const-string v11, ": "

    #@1b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v10

    #@1f
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v10

    #@23
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v10

    #@27
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2a
    .line 2055
    invoke-static {v4}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@2d
    move-result v10

    #@2e
    if-nez v10, :cond_58

    #@30
    .line 2057
    new-instance v10, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v11, "stopUsingNetworkFeature: net "

    #@37
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v10

    #@3f
    const-string v11, ": "

    #@41
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v10

    #@45
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v10

    #@49
    const-string v11, ", net is invalid"

    #@4b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v10

    #@4f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v10

    #@53
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@56
    .line 2060
    const/4 v10, -0x1

    #@57
    .line 2141
    :goto_57
    return v10

    #@58
    .line 2065
    :cond_58
    monitor-enter p0

    #@59
    .line 2067
    :try_start_59
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@5b
    invoke-interface {v10, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@5e
    move-result v10

    #@5f
    if-nez v10, :cond_6c

    #@61
    .line 2069
    const-string v10, "stopUsingNetworkFeature: this process has no outstanding requests, ignoring"

    #@63
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@66
    .line 2072
    const/4 v10, 0x1

    #@67
    monitor-exit p0

    #@68
    goto :goto_57

    #@69
    .line 2132
    :catchall_69
    move-exception v10

    #@6a
    monitor-exit p0
    :try_end_6b
    .catchall {:try_start_59 .. :try_end_6b} :catchall_69

    #@6b
    throw v10

    #@6c
    .line 2074
    :cond_6c
    :try_start_6c
    invoke-virtual {p1}, Lcom/android/server/ConnectivityService$FeatureUser;->unlinkDeathRecipient()V

    #@6f
    .line 2075
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@71
    iget-object v11, p0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@73
    invoke-interface {v11, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    #@76
    move-result v11

    #@77
    invoke-interface {v10, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@7a
    .line 2083
    if-nez p2, :cond_9c

    #@7c
    .line 2084
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@7e
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@81
    move-result-object v3

    #@82
    .local v3, i$:Ljava/util/Iterator;
    :cond_82
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@85
    move-result v10

    #@86
    if-eqz v10, :cond_9c

    #@88
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8b
    move-result-object v9

    #@8c
    check-cast v9, Lcom/android/server/ConnectivityService$FeatureUser;

    #@8e
    .line 2085
    .local v9, x:Lcom/android/server/ConnectivityService$FeatureUser;
    invoke-virtual {v9, p1}, Lcom/android/server/ConnectivityService$FeatureUser;->isSameUser(Lcom/android/server/ConnectivityService$FeatureUser;)Z

    #@91
    move-result v10

    #@92
    if-eqz v10, :cond_82

    #@94
    .line 2086
    const-string v10, "stopUsingNetworkFeature: dup is found, ignoring"

    #@96
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@99
    .line 2087
    const/4 v10, 0x1

    #@9a
    monitor-exit p0

    #@9b
    goto :goto_57

    #@9c
    .line 2093
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v9           #x:Lcom/android/server/ConnectivityService$FeatureUser;
    :cond_9c
    invoke-direct {p0, v4, v2}, Lcom/android/server/ConnectivityService;->convertFeatureToNetworkType(ILjava/lang/String;)I

    #@9f
    move-result v8

    #@a0
    .line 2095
    .local v8, usedNetworkType:I
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@a2
    aget-object v6, v10, v8

    #@a4
    .line 2096
    if-nez v6, :cond_d3

    #@a6
    .line 2098
    new-instance v10, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v11, "stopUsingNetworkFeature: net "

    #@ad
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v10

    #@b1
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v10

    #@b5
    const-string v11, ": "

    #@b7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v10

    #@bb
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v10

    #@bf
    const-string v11, " no known tracker for used net type "

    #@c1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v10

    #@c5
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v10

    #@c9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v10

    #@cd
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@d0
    .line 2101
    const/4 v10, -0x1

    #@d1
    monitor-exit p0

    #@d2
    goto :goto_57

    #@d3
    .line 2103
    :cond_d3
    if-eq v8, v4, :cond_157

    #@d5
    .line 2104
    new-instance v1, Ljava/lang/Integer;

    #@d7
    invoke-direct {v1, v5}, Ljava/lang/Integer;-><init>(I)V

    #@da
    .line 2105
    .local v1, currentPid:Ljava/lang/Integer;
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@dc
    aget-object v10, v10, v8

    #@de
    invoke-interface {v10, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@e1
    .line 2109
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@e3
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@e5
    if-nez v10, :cond_ed

    #@e7
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@e9
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

    #@eb
    if-eqz v10, :cond_f5

    #@ed
    .line 2111
    :cond_ed
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mNetRequestAll:Ljava/util/List;

    #@ef
    invoke-interface {v10, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@f2
    .line 2112
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->setPidlist()V

    #@f5
    .line 2117
    :cond_f5
    const/4 v10, 0x1

    #@f6
    invoke-direct {p0, v5, v10}, Lcom/android/server/ConnectivityService;->reassessPidDns(IZ)V

    #@f9
    .line 2118
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@fb
    aget-object v10, v10, v8

    #@fd
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@100
    move-result v10

    #@101
    if-eqz v10, :cond_12d

    #@103
    .line 2120
    new-instance v10, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v11, "stopUsingNetworkFeature: net "

    #@10a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v10

    #@10e
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@111
    move-result-object v10

    #@112
    const-string v11, ": "

    #@114
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v10

    #@118
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v10

    #@11c
    const-string v11, " others still using it"

    #@11e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v10

    #@122
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@125
    move-result-object v10

    #@126
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@129
    .line 2123
    const/4 v10, 0x1

    #@12a
    monitor-exit p0

    #@12b
    goto/16 :goto_57

    #@12d
    .line 2125
    :cond_12d
    const/4 v0, 0x1

    #@12e
    .line 2132
    .end local v1           #currentPid:Ljava/lang/Integer;
    :goto_12e
    monitor-exit p0
    :try_end_12f
    .catchall {:try_start_6c .. :try_end_12f} :catchall_69

    #@12f
    .line 2134
    if-eqz v0, :cond_17e

    #@131
    .line 2136
    new-instance v10, Ljava/lang/StringBuilder;

    #@133
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v11, "stopUsingNetworkFeature: teardown net "

    #@138
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v10

    #@13c
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v10

    #@140
    const-string v11, ": "

    #@142
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v10

    #@146
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v10

    #@14a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14d
    move-result-object v10

    #@14e
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@151
    .line 2138
    invoke-interface {v6}, Landroid/net/NetworkStateTracker;->teardown()Z

    #@154
    .line 2139
    const/4 v10, 0x1

    #@155
    goto/16 :goto_57

    #@157
    .line 2128
    :cond_157
    :try_start_157
    new-instance v10, Ljava/lang/StringBuilder;

    #@159
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@15c
    const-string v11, "stopUsingNetworkFeature: net "

    #@15e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v10

    #@162
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@165
    move-result-object v10

    #@166
    const-string v11, ": "

    #@168
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v10

    #@16c
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v10

    #@170
    const-string v11, " not a known feature - dropping"

    #@172
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@175
    move-result-object v10

    #@176
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@179
    move-result-object v10

    #@17a
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_17d
    .catchall {:try_start_157 .. :try_end_17d} :catchall_69

    #@17d
    goto :goto_12e

    #@17e
    .line 2141
    :cond_17e
    const/4 v10, -0x1

    #@17f
    goto/16 :goto_57
.end method

.method private teardown(Landroid/net/NetworkStateTracker;)Z
    .registers 4
    .parameter "netTracker"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1145
    invoke-interface {p1}, Landroid/net/NetworkStateTracker;->teardown()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_b

    #@7
    .line 1146
    invoke-interface {p1, v0}, Landroid/net/NetworkStateTracker;->setTeardownRequested(Z)V

    #@a
    .line 1149
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method private throwIfLockdownEnabled()V
    .registers 3

    #@0
    .prologue
    .line 5137
    iget-boolean v0, p0, Lcom/android/server/ConnectivityService;->mLockdownEnabled:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 5138
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Unavailable in lockdown mode"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 5140
    :cond_c
    return-void
.end method

.method private tryFailover(I)V
    .registers 10
    .parameter "prevNetType"

    #@0
    .prologue
    .line 2895
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@2
    aget-object v5, v5, p1

    #@4
    invoke-virtual {v5}, Landroid/net/NetworkConfig;->isDefault()Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_ca

    #@a
    .line 2896
    iget v5, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@c
    if-ne v5, p1, :cond_43

    #@e
    .line 2897
    const/4 v5, -0x1

    #@f
    iput v5, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@11
    .line 2899
    const-string v5, "ro.build.target_operator"

    #@13
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    .line 2900
    .local v4, operator:Ljava/lang/String;
    if-eqz v4, :cond_43

    #@19
    const-string v5, "SPR"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_43

    #@21
    .line 2901
    const/4 v0, 0x0

    #@22
    .line 2902
    .local v0, activenetwork:I
    const-string v5, "net.activenetwork"

    #@24
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 2903
    const-string v5, "ConnectivityService"

    #@2d
    new-instance v6, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v7, "now we set activenetwork : "

    #@34
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v6

    #@40
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 2915
    .end local v0           #activenetwork:I
    .end local v4           #operator:Ljava/lang/String;
    :cond_43
    const/4 v3, 0x0

    #@44
    .local v3, checkType:I
    :goto_44
    const/16 v5, 0x17

    #@46
    if-gt v3, v5, :cond_ca

    #@48
    .line 2916
    if-ne v3, p1, :cond_4d

    #@4a
    .line 2915
    :cond_4a
    :goto_4a
    add-int/lit8 v3, v3, 0x1

    #@4c
    goto :goto_44

    #@4d
    .line 2917
    :cond_4d
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@4f
    aget-object v5, v5, v3

    #@51
    if-eqz v5, :cond_4a

    #@53
    .line 2918
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@55
    aget-object v5, v5, v3

    #@57
    invoke-virtual {v5}, Landroid/net/NetworkConfig;->isDefault()Z

    #@5a
    move-result v5

    #@5b
    if-eqz v5, :cond_4a

    #@5d
    .line 2919
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@5f
    aget-object v5, v5, v3

    #@61
    if-eqz v5, :cond_4a

    #@63
    .line 2933
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@65
    aget-object v2, v5, v3

    #@67
    .line 2937
    .local v2, checkTracker:Landroid/net/NetworkStateTracker;
    if-eqz v2, :cond_4a

    #@69
    .line 2941
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@6c
    move-result-object v1

    #@6d
    .line 2942
    .local v1, checkInfo:Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    #@70
    move-result v5

    #@71
    if-eqz v5, :cond_79

    #@73
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@76
    move-result v5

    #@77
    if-eqz v5, :cond_9b

    #@79
    .line 2943
    :cond_79
    const/4 v5, 0x1

    #@7a
    invoke-virtual {v1, v5}, Landroid/net/NetworkInfo;->setFailover(Z)V

    #@7d
    .line 2944
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->reconnect()Z

    #@80
    .line 2954
    :cond_80
    :goto_80
    new-instance v5, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v6, "Attempting to switch to "

    #@87
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@8e
    move-result-object v6

    #@8f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v5

    #@93
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v5

    #@97
    invoke-static {v5}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@9a
    goto :goto_4a

    #@9b
    .line 2947
    :cond_9b
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@9d
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@9f
    if-eqz v5, :cond_80

    #@a1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    #@a4
    move-result v5

    #@a5
    if-eqz v5, :cond_80

    #@a7
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@aa
    move-result v5

    #@ab
    if-nez v5, :cond_80

    #@ad
    if-nez v3, :cond_80

    #@af
    .line 2950
    new-instance v5, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v6, "abnormal state ] checkinfo state ==> "

    #@b6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v5

    #@ba
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v5

    #@c2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v5

    #@c6
    invoke-static {v5}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@c9
    goto :goto_80

    #@ca
    .line 2957
    .end local v1           #checkInfo:Landroid/net/NetworkInfo;
    .end local v2           #checkTracker:Landroid/net/NetworkStateTracker;
    .end local v3           #checkType:I
    :cond_ca
    return-void
.end method

.method private updateApnDB(Ljava/lang/String;I)Z
    .registers 20
    .parameter "s"
    .parameter "Set_id"

    #@0
    .prologue
    .line 5189
    const-string v12, "311480"

    #@2
    .line 5191
    .local v12, networkOperator:Ljava/lang/String;
    const-string v10, "311"

    #@4
    .line 5192
    .local v10, mcc:Ljava/lang/String;
    const-string v11, "480"

    #@6
    .line 5196
    .local v11, mnc:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "numeric=\""

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, "\""

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    .line 5200
    .local v4, where:Ljava/lang/String;
    move-object/from16 v0, p0

    #@21
    iget-object v1, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@23
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@26
    move-result-object v1

    #@27
    sget-object v2, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@29
    sget-object v3, Lcom/android/server/ConnectivityService;->sProjection:[Ljava/lang/String;

    #@2b
    const/4 v5, 0x0

    #@2c
    const-string v6, "_id"

    #@2e
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@31
    move-result-object v9

    #@32
    .line 5203
    .local v9, mCursor:Landroid/database/Cursor;
    if-nez v9, :cond_3d

    #@34
    .line 5205
    const-string v1, "ConnectivityService"

    #@36
    const-string v2, " Cursor is null"

    #@38
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 5206
    const/4 v1, 0x0

    #@3c
    .line 5248
    :goto_3c
    return v1

    #@3d
    .line 5209
    :cond_3d
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@40
    .line 5210
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@43
    move-result v7

    #@44
    .line 5213
    .local v7, count:I
    move/from16 v0, p2

    #@46
    if-ge v7, v0, :cond_64

    #@48
    .line 5215
    const-string v1, "ConnectivityService"

    #@4a
    new-instance v2, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v3, "set id is bad id : "

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    move/from16 v0, p2

    #@57
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 5216
    const/4 v1, 0x0

    #@63
    goto :goto_3c

    #@64
    .line 5219
    :cond_64
    move/from16 v0, p2

    #@66
    invoke-interface {v9, v0}, Landroid/database/Cursor;->move(I)Z

    #@69
    .line 5222
    sget v1, Lcom/android/server/ConnectivityService;->ID_INDEX:I

    #@6b
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@6e
    move-result-object v8

    #@6f
    .line 5223
    .local v8, key:Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@72
    move-result v13

    #@73
    .line 5224
    .local v13, pos:I
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@75
    int-to-long v2, v13

    #@76
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@79
    move-result-object v15

    #@7a
    .line 5225
    .local v15, url:Landroid/net/Uri;
    const-string v1, "ConnectivityService"

    #@7c
    new-instance v2, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v3, "your pos"

    #@83
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v2

    #@87
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v2

    #@8b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v2

    #@8f
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 5227
    new-instance v16, Landroid/content/ContentValues;

    #@94
    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    #@97
    .line 5229
    .local v16, values:Landroid/content/ContentValues;
    const-string v1, "type"

    #@99
    const-string v2, "admin"

    #@9b
    move-object/from16 v0, v16

    #@9d
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 5230
    const-string v1, "apn"

    #@a2
    const-string v2, "VZWADMIN"

    #@a4
    move-object/from16 v0, v16

    #@a6
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a9
    .line 5232
    const-string v1, "protocol"

    #@ab
    const-string v2, "IPV4V6"

    #@ad
    move-object/from16 v0, v16

    #@af
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b2
    .line 5235
    const-string v1, "bearer"

    #@b4
    const-string v2, "0"

    #@b6
    move-object/from16 v0, v16

    #@b8
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@bb
    .line 5236
    const-string v1, "authtype"

    #@bd
    const-string v2, "0"

    #@bf
    move-object/from16 v0, v16

    #@c1
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c4
    .line 5237
    const-string v1, "user"

    #@c6
    const-string v2, "ncc"

    #@c8
    move-object/from16 v0, v16

    #@ca
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@cd
    .line 5238
    const-string v1, "password"

    #@cf
    const-string v2, "ncc"

    #@d1
    move-object/from16 v0, v16

    #@d3
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d6
    .line 5239
    const-string v1, "name"

    #@d8
    const-string v2, "VZWADMIN"

    #@da
    move-object/from16 v0, v16

    #@dc
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@df
    .line 5240
    const-string v1, "mcc"

    #@e1
    move-object/from16 v0, v16

    #@e3
    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e6
    .line 5241
    const-string v1, "mnc"

    #@e8
    move-object/from16 v0, v16

    #@ea
    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ed
    .line 5242
    const-string v1, "numeric"

    #@ef
    move-object/from16 v0, v16

    #@f1
    invoke-virtual {v0, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f4
    .line 5243
    const-string v1, "carrier_enabled"

    #@f6
    const-string v2, "0"

    #@f8
    move-object/from16 v0, v16

    #@fa
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@fd
    .line 5245
    move-object/from16 v0, p0

    #@ff
    iget-object v1, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@101
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@104
    move-result-object v1

    #@105
    const/4 v2, 0x0

    #@106
    const/4 v3, 0x0

    #@107
    move-object/from16 v0, v16

    #@109
    invoke-virtual {v1, v15, v0, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@10c
    move-result v14

    #@10d
    .line 5247
    .local v14, result:I
    const-string v1, "ConnectivityService"

    #@10f
    const-string v2, "updata success : "

    #@111
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 5248
    const/4 v1, 0x1

    #@115
    goto/16 :goto_3c
.end method

.method private updateDns(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)Z
    .registers 18
    .parameter "network"
    .parameter "iface"
    .parameter
    .parameter "domains"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    #@0
    .prologue
    .line 4037
    .local p3, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    const/4 v1, 0x0

    #@1
    .line 4038
    .local v1, changed:Z
    const/4 v7, 0x0

    #@2
    .line 4039
    .local v7, last:I
    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->size()I

    #@5
    move-result v9

    #@6
    if-nez v9, :cond_7e

    #@8
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mDefaultDns:Ljava/net/InetAddress;

    #@a
    if-eqz v9, :cond_7e

    #@c
    .line 4040
    add-int/lit8 v7, v7, 0x1

    #@e
    .line 4041
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mDefaultDns:Ljava/net/InetAddress;

    #@10
    invoke-virtual {v9}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@13
    move-result-object v8

    #@14
    .line 4042
    .local v8, value:Ljava/lang/String;
    const-string v9, "net.dns1"

    #@16
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v9

    #@1a
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v9

    #@1e
    if-nez v9, :cond_46

    #@20
    .line 4044
    new-instance v9, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v10, "no dns provided for "

    #@27
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v9

    #@2b
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v9

    #@2f
    const-string v10, " - using "

    #@31
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v9

    #@39
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v9

    #@3d
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@40
    .line 4046
    const/4 v1, 0x1

    #@41
    .line 4047
    const-string v9, "net.dns1"

    #@43
    invoke-static {v9, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 4064
    .end local v8           #value:Ljava/lang/String;
    :cond_46
    add-int/lit8 v4, v7, 0x1

    #@48
    .local v4, i:I
    :goto_48
    iget v9, p0, Lcom/android/server/ConnectivityService;->mNumDnsEntries:I

    #@4a
    if-gt v4, v9, :cond_d8

    #@4c
    .line 4065
    new-instance v9, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v10, "net.dns"

    #@53
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v9

    #@57
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v9

    #@5b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v6

    #@5f
    .line 4066
    .local v6, key:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v10, "erasing "

    #@66
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v9

    #@6e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v9

    #@72
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@75
    .line 4067
    const/4 v1, 0x1

    #@76
    .line 4068
    const-string v9, ""

    #@78
    invoke-static {v6, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    .line 4064
    add-int/lit8 v4, v4, 0x1

    #@7d
    goto :goto_48

    #@7e
    .line 4050
    .end local v4           #i:I
    .end local v6           #key:Ljava/lang/String;
    :cond_7e
    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@81
    move-result-object v5

    #@82
    .local v5, i$:Ljava/util/Iterator;
    :cond_82
    :goto_82
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@85
    move-result v9

    #@86
    if-eqz v9, :cond_46

    #@88
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8b
    move-result-object v2

    #@8c
    check-cast v2, Ljava/net/InetAddress;

    #@8e
    .line 4051
    .local v2, dns:Ljava/net/InetAddress;
    add-int/lit8 v7, v7, 0x1

    #@90
    .line 4052
    new-instance v9, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v10, "net.dns"

    #@97
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v9

    #@9b
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v9

    #@9f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v6

    #@a3
    .line 4053
    .restart local v6       #key:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@a6
    move-result-object v8

    #@a7
    .line 4054
    .restart local v8       #value:Ljava/lang/String;
    if-nez v1, :cond_b3

    #@a9
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@ac
    move-result-object v9

    #@ad
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b0
    move-result v9

    #@b1
    if-nez v9, :cond_82

    #@b3
    .line 4058
    :cond_b3
    new-instance v9, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v10, "adding dns "

    #@ba
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v9

    #@be
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v9

    #@c2
    const-string v10, " for "

    #@c4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v9

    #@c8
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v9

    #@cc
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cf
    move-result-object v9

    #@d0
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@d3
    .line 4060
    const/4 v1, 0x1

    #@d4
    .line 4061
    invoke-static {v6, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@d7
    goto :goto_82

    #@d8
    .line 4070
    .end local v2           #dns:Ljava/net/InetAddress;
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v6           #key:Ljava/lang/String;
    .end local v8           #value:Ljava/lang/String;
    .restart local v4       #i:I
    :cond_d8
    iput v7, p0, Lcom/android/server/ConnectivityService;->mNumDnsEntries:I

    #@da
    .line 4072
    if-eqz v1, :cond_fc

    #@dc
    .line 4075
    :try_start_dc
    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->size()I

    #@df
    move-result v9

    #@e0
    if-nez v9, :cond_113

    #@e2
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mDefaultDns:Ljava/net/InetAddress;

    #@e4
    if-eqz v9, :cond_113

    #@e6
    .line 4076
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@e8
    const/4 v10, 0x1

    #@e9
    new-array v10, v10, [Ljava/lang/String;

    #@eb
    const/4 v11, 0x0

    #@ec
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mDefaultDns:Ljava/net/InetAddress;

    #@ee
    invoke-virtual {v12}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@f1
    move-result-object v12

    #@f2
    aput-object v12, v10, v11

    #@f4
    invoke-interface {v9, p2, v10}, Landroid/os/INetworkManagementService;->setDnsServersForInterface(Ljava/lang/String;[Ljava/lang/String;)V

    #@f7
    .line 4080
    :goto_f7
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@f9
    invoke-interface {v9, p2}, Landroid/os/INetworkManagementService;->setDefaultInterfaceForDns(Ljava/lang/String;)V
    :try_end_fc
    .catch Ljava/lang/Exception; {:try_start_dc .. :try_end_fc} :catch_11d

    #@fc
    .line 4085
    :cond_fc
    :goto_fc
    const-string v9, "net.dns.search"

    #@fe
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@101
    move-result-object v9

    #@102
    move-object/from16 v0, p4

    #@104
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@107
    move-result v9

    #@108
    if-nez v9, :cond_112

    #@10a
    .line 4086
    const-string v9, "net.dns.search"

    #@10c
    move-object/from16 v0, p4

    #@10e
    invoke-static {v9, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@111
    .line 4087
    const/4 v1, 0x1

    #@112
    .line 4089
    :cond_112
    return v1

    #@113
    .line 4079
    :cond_113
    :try_start_113
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@115
    invoke-static/range {p3 .. p3}, Landroid/net/NetworkUtils;->makeStrings(Ljava/util/Collection;)[Ljava/lang/String;

    #@118
    move-result-object v10

    #@119
    invoke-interface {v9, p2, v10}, Landroid/os/INetworkManagementService;->setDnsServersForInterface(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_11c
    .catch Ljava/lang/Exception; {:try_start_113 .. :try_end_11c} :catch_11d

    #@11c
    goto :goto_f7

    #@11d
    .line 4081
    :catch_11d
    move-exception v3

    #@11e
    .line 4082
    .local v3, e:Ljava/lang/Exception;
    new-instance v9, Ljava/lang/StringBuilder;

    #@120
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@123
    const-string v10, "exception setting default dns interface: "

    #@125
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v9

    #@129
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v9

    #@12d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v9

    #@131
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@134
    goto :goto_fc
.end method

.method private updateRoutes(Landroid/net/LinkProperties;Landroid/net/LinkProperties;Z)Z
    .registers 17
    .parameter "newLp"
    .parameter "curLp"
    .parameter "isLinkDefault"

    #@0
    .prologue
    .line 3623
    const/4 v9, 0x0

    #@1
    .line 3624
    .local v9, routesToAdd:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/RouteInfo;>;"
    new-instance v0, Landroid/net/LinkProperties$CompareResult;

    #@3
    invoke-direct {v0}, Landroid/net/LinkProperties$CompareResult;-><init>()V

    #@6
    .line 3625
    .local v0, dnsDiff:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Ljava/net/InetAddress;>;"
    new-instance v7, Landroid/net/LinkProperties$CompareResult;

    #@8
    invoke-direct {v7}, Landroid/net/LinkProperties$CompareResult;-><init>()V

    #@b
    .line 3626
    .local v7, routeDiff:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/RouteInfo;>;"
    if-eqz p2, :cond_4b

    #@d
    .line 3628
    invoke-virtual {p2, p1}, Landroid/net/LinkProperties;->compareRoutes(Landroid/net/LinkProperties;)Landroid/net/LinkProperties$CompareResult;

    #@10
    move-result-object v7

    #@11
    .line 3629
    invoke-virtual {p2, p1}, Landroid/net/LinkProperties;->compareDnses(Landroid/net/LinkProperties;)Landroid/net/LinkProperties$CompareResult;

    #@14
    move-result-object v0

    #@15
    .line 3635
    :cond_15
    :goto_15
    iget-object v10, v7, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@17
    invoke-interface {v10}, Ljava/util/Collection;->size()I

    #@1a
    move-result v10

    #@1b
    if-nez v10, :cond_25

    #@1d
    iget-object v10, v7, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@1f
    invoke-interface {v10}, Ljava/util/Collection;->size()I

    #@22
    move-result v10

    #@23
    if-eqz v10, :cond_5a

    #@25
    :cond_25
    const/4 v8, 0x1

    #@26
    .line 3637
    .local v8, routesChanged:Z
    :goto_26
    iget-object v10, v7, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@28
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v2

    #@2c
    .local v2, i$:Ljava/util/Iterator;
    :cond_2c
    :goto_2c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v10

    #@30
    if-eqz v10, :cond_5c

    #@32
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v6

    #@36
    check-cast v6, Landroid/net/RouteInfo;

    #@38
    .line 3638
    .local v6, r:Landroid/net/RouteInfo;
    if-nez p3, :cond_40

    #@3a
    invoke-virtual {v6}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    #@3d
    move-result v10

    #@3e
    if-nez v10, :cond_44

    #@40
    .line 3639
    :cond_40
    const/4 v10, 0x1

    #@41
    invoke-direct {p0, p2, v6, v10}, Lcom/android/server/ConnectivityService;->removeRoute(Landroid/net/LinkProperties;Landroid/net/RouteInfo;Z)Z

    #@44
    .line 3641
    :cond_44
    if-nez p3, :cond_2c

    #@46
    .line 3643
    const/4 v10, 0x0

    #@47
    invoke-direct {p0, p2, v6, v10}, Lcom/android/server/ConnectivityService;->removeRoute(Landroid/net/LinkProperties;Landroid/net/RouteInfo;Z)Z

    #@4a
    goto :goto_2c

    #@4b
    .line 3630
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v6           #r:Landroid/net/RouteInfo;
    .end local v8           #routesChanged:Z
    :cond_4b
    if-eqz p1, :cond_15

    #@4d
    .line 3631
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@50
    move-result-object v10

    #@51
    iput-object v10, v7, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@53
    .line 3632
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@56
    move-result-object v10

    #@57
    iput-object v10, v0, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@59
    goto :goto_15

    #@5a
    .line 3635
    :cond_5a
    const/4 v8, 0x0

    #@5b
    goto :goto_26

    #@5c
    .line 3647
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v8       #routesChanged:Z
    :cond_5c
    iget-object v10, v7, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@5e
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@61
    move-result-object v2

    #@62
    :goto_62
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@65
    move-result v10

    #@66
    if-eqz v10, :cond_d6

    #@68
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6b
    move-result-object v6

    #@6c
    check-cast v6, Landroid/net/RouteInfo;

    #@6e
    .line 3648
    .restart local v6       #r:Landroid/net/RouteInfo;
    if-nez p3, :cond_76

    #@70
    invoke-virtual {v6}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    #@73
    move-result v10

    #@74
    if-nez v10, :cond_7b

    #@76
    .line 3649
    :cond_76
    const/4 v10, 0x1

    #@77
    invoke-direct {p0, p1, v6, v10}, Lcom/android/server/ConnectivityService;->addRoute(Landroid/net/LinkProperties;Landroid/net/RouteInfo;Z)Z

    #@7a
    goto :goto_62

    #@7b
    .line 3652
    :cond_7b
    const/4 v10, 0x0

    #@7c
    invoke-direct {p0, p1, v6, v10}, Lcom/android/server/ConnectivityService;->addRoute(Landroid/net/LinkProperties;Landroid/net/RouteInfo;Z)Z

    #@7f
    .line 3656
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@82
    move-result-object v3

    #@83
    .line 3658
    .local v3, ifaceName:Ljava/lang/String;
    iget-object v11, p0, Lcom/android/server/ConnectivityService;->mRoutingTableLock:Ljava/lang/Object;

    #@85
    monitor-enter v11

    #@86
    .line 3660
    :try_start_86
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@89
    move-result v10

    #@8a
    if-nez v10, :cond_b9

    #@8c
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mAddedRoutes:Ljava/util/Collection;

    #@8e
    invoke-interface {v10, v6}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@91
    move-result v10

    #@92
    if-nez v10, :cond_b9

    #@94
    .line 3661
    new-instance v10, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v12, "Removing "

    #@9b
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v10

    #@9f
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v10

    #@a3
    const-string v12, " for interface "

    #@a5
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v10

    #@a9
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v10

    #@ad
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v10

    #@b1
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_b4
    .catchall {:try_start_86 .. :try_end_b4} :catchall_bb

    #@b4
    .line 3663
    :try_start_b4
    iget-object v10, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@b6
    invoke-interface {v10, v3, v6}, Landroid/os/INetworkManagementService;->removeRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    :try_end_b9
    .catchall {:try_start_b4 .. :try_end_b9} :catchall_bb
    .catch Ljava/lang/Exception; {:try_start_b4 .. :try_end_b9} :catch_be

    #@b9
    .line 3670
    :cond_b9
    :goto_b9
    :try_start_b9
    monitor-exit v11

    #@ba
    goto :goto_62

    #@bb
    :catchall_bb
    move-exception v10

    #@bc
    monitor-exit v11
    :try_end_bd
    .catchall {:try_start_b9 .. :try_end_bd} :catchall_bb

    #@bd
    throw v10

    #@be
    .line 3664
    :catch_be
    move-exception v1

    #@bf
    .line 3666
    .local v1, e:Ljava/lang/Exception;
    :try_start_bf
    new-instance v10, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v12, "Exception trying to remove a route: "

    #@c6
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v10

    #@ca
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v10

    #@ce
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v10

    #@d2
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V
    :try_end_d5
    .catchall {:try_start_bf .. :try_end_d5} :catchall_bb

    #@d5
    goto :goto_b9

    #@d6
    .line 3675
    .end local v1           #e:Ljava/lang/Exception;
    .end local v3           #ifaceName:Ljava/lang/String;
    .end local v6           #r:Landroid/net/RouteInfo;
    :cond_d6
    if-nez p3, :cond_13a

    #@d8
    .line 3677
    if-eqz v8, :cond_10e

    #@da
    .line 3679
    if-eqz p2, :cond_f4

    #@dc
    .line 3680
    invoke-virtual {p2}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@df
    move-result-object v10

    #@e0
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e3
    move-result-object v2

    #@e4
    :goto_e4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e7
    move-result v10

    #@e8
    if-eqz v10, :cond_f4

    #@ea
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ed
    move-result-object v5

    #@ee
    check-cast v5, Ljava/net/InetAddress;

    #@f0
    .line 3681
    .local v5, oldDns:Ljava/net/InetAddress;
    invoke-direct {p0, p2, v5}, Lcom/android/server/ConnectivityService;->removeRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z

    #@f3
    goto :goto_e4

    #@f4
    .line 3684
    .end local v5           #oldDns:Ljava/net/InetAddress;
    :cond_f4
    if-eqz p1, :cond_13a

    #@f6
    .line 3685
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@f9
    move-result-object v10

    #@fa
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@fd
    move-result-object v2

    #@fe
    :goto_fe
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@101
    move-result v10

    #@102
    if-eqz v10, :cond_13a

    #@104
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@107
    move-result-object v4

    #@108
    check-cast v4, Ljava/net/InetAddress;

    #@10a
    .line 3686
    .local v4, newDns:Ljava/net/InetAddress;
    invoke-direct {p0, p1, v4}, Lcom/android/server/ConnectivityService;->addRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z

    #@10d
    goto :goto_fe

    #@10e
    .line 3691
    .end local v4           #newDns:Ljava/net/InetAddress;
    :cond_10e
    iget-object v10, v0, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@110
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@113
    move-result-object v2

    #@114
    :goto_114
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@117
    move-result v10

    #@118
    if-eqz v10, :cond_124

    #@11a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11d
    move-result-object v5

    #@11e
    check-cast v5, Ljava/net/InetAddress;

    #@120
    .line 3692
    .restart local v5       #oldDns:Ljava/net/InetAddress;
    invoke-direct {p0, p2, v5}, Lcom/android/server/ConnectivityService;->removeRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z

    #@123
    goto :goto_114

    #@124
    .line 3694
    .end local v5           #oldDns:Ljava/net/InetAddress;
    :cond_124
    iget-object v10, v0, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@126
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@129
    move-result-object v2

    #@12a
    :goto_12a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@12d
    move-result v10

    #@12e
    if-eqz v10, :cond_13a

    #@130
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@133
    move-result-object v4

    #@134
    check-cast v4, Ljava/net/InetAddress;

    #@136
    .line 3695
    .restart local v4       #newDns:Ljava/net/InetAddress;
    invoke-direct {p0, p1, v4}, Lcom/android/server/ConnectivityService;->addRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z

    #@139
    goto :goto_12a

    #@13a
    .line 3699
    .end local v4           #newDns:Ljava/net/InetAddress;
    :cond_13a
    return v8
.end method

.method private writePidDns(Ljava/util/Collection;II)Z
    .registers 15
    .parameter
    .parameter "pid"
    .parameter "IPtype"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;II)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    const/4 v8, 0x0

    #@1
    .line 3893
    const/4 v5, 0x1

    #@2
    .line 3894
    .local v5, j:I
    const/4 v1, 0x0

    #@3
    .line 3899
    .local v1, changed:Z
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@5
    iget-boolean v9, v9, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@7
    if-eqz v9, :cond_28

    #@9
    .line 3901
    invoke-direct {p0, p2}, Lcom/android/server/ConnectivityService;->getSysid(I)I

    #@c
    move-result v7

    #@d
    .line 3903
    .local v7, sysid:I
    const/16 v9, 0x9

    #@f
    if-le v7, v9, :cond_29

    #@11
    .line 3905
    new-instance v9, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v10, "[CS]using pid is over !!! qns will not work num of pids "

    #@18
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v9

    #@20
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v9

    #@24
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@27
    .line 3961
    :cond_27
    :goto_27
    return v8

    #@28
    .line 3911
    .end local v7           #sysid:I
    :cond_28
    move v7, p2

    #@29
    .line 3914
    .restart local v7       #sysid:I
    :cond_29
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2c
    move-result-object v4

    #@2d
    .local v4, i$:Ljava/util/Iterator;
    :goto_2d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@30
    move-result v9

    #@31
    if-eqz v9, :cond_bf

    #@33
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36
    move-result-object v2

    #@37
    check-cast v2, Ljava/net/InetAddress;

    #@39
    .line 3915
    .local v2, dns:Ljava/net/InetAddress;
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    .line 3916
    .local v3, dnsString:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v10, "net.dns"

    #@44
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v9

    #@48
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v9

    #@4c
    const-string v10, "."

    #@4e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v9

    #@52
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v9

    #@56
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v9

    #@5a
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v6

    #@5e
    .line 3917
    .local v6, propvalue:Ljava/lang/String;
    const-string v9, ";"

    #@60
    invoke-virtual {v6, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@63
    move-result v0

    #@64
    .line 3918
    .local v0, a:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v10, "writePidDns() : DNS from property = "

    #@6b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v9

    #@6f
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v9

    #@73
    const-string v10, " index of ; is "

    #@75
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v9

    #@79
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v9

    #@7d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v9

    #@81
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@84
    .line 3919
    const/4 v9, -0x1

    #@85
    if-ne v0, v9, :cond_b3

    #@87
    .line 3920
    new-instance v9, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v10, "net.dns"

    #@8e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v9

    #@92
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@95
    move-result-object v9

    #@96
    const-string v10, "."

    #@98
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v9

    #@9c
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v9

    #@a4
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a7
    move-result-object v9

    #@a8
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ab
    move-result v9

    #@ac
    if-nez v9, :cond_af

    #@ae
    .line 3921
    const/4 v1, 0x1

    #@af
    .line 3926
    :cond_af
    :goto_af
    add-int/lit8 v5, v5, 0x1

    #@b1
    .line 3927
    goto/16 :goto_2d

    #@b3
    .line 3923
    :cond_b3
    invoke-virtual {v6, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b6
    move-result-object v9

    #@b7
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ba
    move-result v9

    #@bb
    if-nez v9, :cond_af

    #@bd
    .line 3924
    const/4 v1, 0x1

    #@be
    goto :goto_af

    #@bf
    .line 3929
    .end local v0           #a:I
    .end local v2           #dns:Ljava/net/InetAddress;
    .end local v3           #dnsString:Ljava/lang/String;
    .end local v6           #propvalue:Ljava/lang/String;
    :cond_bf
    if-eqz v1, :cond_27

    #@c1
    .line 3932
    const/4 v5, 0x1

    #@c2
    .line 3933
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@c5
    move-result-object v4

    #@c6
    :goto_c6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@c9
    move-result v8

    #@ca
    if-eqz v8, :cond_149

    #@cc
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@cf
    move-result-object v2

    #@d0
    check-cast v2, Ljava/net/InetAddress;

    #@d2
    .line 3934
    .restart local v2       #dns:Ljava/net/InetAddress;
    new-instance v8, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v9, "net.dns"

    #@d9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v8

    #@dd
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v8

    #@e1
    const-string v9, "."

    #@e3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v8

    #@e7
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v8

    #@eb
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v8

    #@ef
    new-instance v9, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@f7
    move-result-object v10

    #@f8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v9

    #@fc
    const-string v10, ";"

    #@fe
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v9

    #@102
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v9

    #@106
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v9

    #@10a
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@10d
    .line 3935
    new-instance v8, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v9, "!!! net.dns"

    #@114
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v8

    #@118
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v8

    #@11c
    const-string v9, "."

    #@11e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v8

    #@122
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@125
    move-result-object v8

    #@126
    const-string v9, " :"

    #@128
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v8

    #@12c
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@12f
    move-result-object v9

    #@130
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v8

    #@134
    const-string v9, " IPtype : "

    #@136
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v8

    #@13a
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v8

    #@13e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v8

    #@142
    invoke-static {v8}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@145
    .line 3936
    add-int/lit8 v5, v5, 0x1

    #@147
    goto/16 :goto_c6

    #@149
    .line 3961
    .end local v2           #dns:Ljava/net/InetAddress;
    :cond_149
    const/4 v8, 0x1

    #@14a
    goto/16 :goto_27
.end method


# virtual methods
.method public captivePortalCheckComplete(Landroid/net/NetworkInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 3365
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@5
    move-result v1

    #@6
    aget-object v0, v0, v1

    #@8
    invoke-interface {v0}, Landroid/net/NetworkStateTracker;->captivePortalCheckComplete()V

    #@b
    .line 3366
    return-void
.end method

.method public clearTetheringBlockNotification()V
    .registers 3

    #@0
    .prologue
    .line 1600
    const-string v0, "ConnectivityService"

    #@2
    const-string v1, "[ConnectivityService] clearTetheringBlockNotification"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1601
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@9
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->clearTetheredBlockNotification()V

    #@c
    .line 1602
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 18
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 4171
    new-instance v10, Lcom/android/internal/util/IndentingPrintWriter;

    #@2
    const-string v12, "  "

    #@4
    move-object/from16 v0, p2

    #@6
    invoke-direct {v10, v0, v12}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@9
    .line 4172
    .local v10, pw:Lcom/android/internal/util/IndentingPrintWriter;
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@b
    const-string v13, "android.permission.DUMP"

    #@d
    invoke-virtual {v12, v13}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@10
    move-result v12

    #@11
    if-eqz v12, :cond_3c

    #@13
    .line 4175
    new-instance v12, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v13, "Permission Denial: can\'t dump ConnectivityService from from pid="

    #@1a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v12

    #@1e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@21
    move-result v13

    #@22
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v12

    #@26
    const-string v13, ", uid="

    #@28
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v12

    #@2c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2f
    move-result v13

    #@30
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v12

    #@34
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v12

    #@38
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 4238
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 4182
    :cond_3c
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@3f
    .line 4183
    const/4 v2, 0x0

    #@40
    .local v2, i:I
    :goto_40
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@42
    array-length v12, v12

    #@43
    if-ge v2, v12, :cond_b0

    #@45
    .line 4184
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@47
    aget-object v7, v12, v2

    #@49
    .line 4185
    .local v7, nst:Landroid/net/NetworkStateTracker;
    if-eqz v7, :cond_ad

    #@4b
    .line 4186
    new-instance v12, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v13, "NetworkStateTracker for "

    #@52
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v12

    #@56
    invoke-static {v2}, Landroid/net/ConnectivityManager;->getNetworkTypeName(I)Ljava/lang/String;

    #@59
    move-result-object v13

    #@5a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v12

    #@5e
    const-string v13, ":"

    #@60
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v12

    #@64
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v12

    #@68
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@6b
    .line 4187
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@6e
    .line 4188
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@71
    move-result-object v12

    #@72
    invoke-virtual {v12}, Landroid/net/NetworkInfo;->isConnected()Z

    #@75
    move-result v12

    #@76
    if-eqz v12, :cond_96

    #@78
    .line 4189
    new-instance v12, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v13, "Active network: "

    #@7f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v12

    #@83
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@86
    move-result-object v13

    #@87
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@8a
    move-result-object v13

    #@8b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v12

    #@8f
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v12

    #@93
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@96
    .line 4192
    :cond_96
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@99
    move-result-object v12

    #@9a
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/Object;)V

    #@9d
    .line 4193
    invoke-interface {v7}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@a0
    move-result-object v12

    #@a1
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/Object;)V

    #@a4
    .line 4194
    invoke-virtual {v10, v7}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/Object;)V

    #@a7
    .line 4195
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@aa
    .line 4196
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@ad
    .line 4183
    :cond_ad
    add-int/lit8 v2, v2, 0x1

    #@af
    goto :goto_40

    #@b0
    .line 4200
    .end local v7           #nst:Landroid/net/NetworkStateTracker;
    :cond_b0
    const-string v12, "Network Requester Pids:"

    #@b2
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@b5
    .line 4201
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@b8
    .line 4202
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mPriorityList:[I

    #@ba
    .local v1, arr$:[I
    array-length v5, v1

    #@bb
    .local v5, len$:I
    const/4 v3, 0x0

    #@bc
    .local v3, i$:I
    move v4, v3

    #@bd
    .end local v3           #i$:I
    .local v4, i$:I
    :goto_bd
    if-ge v4, v5, :cond_109

    #@bf
    aget v6, v1, v4

    #@c1
    .line 4203
    .local v6, net:I
    new-instance v12, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v12

    #@ca
    const-string v13, ": "

    #@cc
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v12

    #@d0
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v9

    #@d4
    .line 4204
    .local v9, pidString:Ljava/lang/String;
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@d6
    aget-object v12, v12, v6

    #@d8
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@db
    move-result-object v3

    #@dc
    .end local v4           #i$:I
    .local v3, i$:Ljava/util/Iterator;
    :goto_dc
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@df
    move-result v12

    #@e0
    if-eqz v12, :cond_102

    #@e2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e5
    move-result-object v8

    #@e6
    .line 4205
    .local v8, pid:Ljava/lang/Object;
    new-instance v12, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v12

    #@ef
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f2
    move-result-object v13

    #@f3
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v12

    #@f7
    const-string v13, ", "

    #@f9
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v12

    #@fd
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v9

    #@101
    goto :goto_dc

    #@102
    .line 4207
    .end local v8           #pid:Ljava/lang/Object;
    :cond_102
    invoke-virtual {v10, v9}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@105
    .line 4202
    add-int/lit8 v3, v4, 0x1

    #@107
    .local v3, i$:I
    move v4, v3

    #@108
    .end local v3           #i$:I
    .restart local v4       #i$:I
    goto :goto_bd

    #@109
    .line 4209
    .end local v6           #net:I
    .end local v9           #pidString:Ljava/lang/String;
    :cond_109
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@10c
    .line 4210
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@10f
    .line 4212
    const-string v12, "FeatureUsers:"

    #@111
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@114
    .line 4213
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@117
    .line 4214
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@119
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@11c
    move-result-object v3

    #@11d
    .end local v4           #i$:I
    .local v3, i$:Ljava/util/Iterator;
    :goto_11d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@120
    move-result v12

    #@121
    if-eqz v12, :cond_131

    #@123
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@126
    move-result-object v11

    #@127
    check-cast v11, Lcom/android/server/ConnectivityService$FeatureUser;

    #@129
    .line 4215
    .local v11, requester:Lcom/android/server/ConnectivityService$FeatureUser;
    invoke-virtual {v11}, Lcom/android/server/ConnectivityService$FeatureUser;->toString()Ljava/lang/String;

    #@12c
    move-result-object v12

    #@12d
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@130
    goto :goto_11d

    #@131
    .line 4217
    .end local v11           #requester:Lcom/android/server/ConnectivityService$FeatureUser;
    :cond_131
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@134
    .line 4218
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@137
    .line 4220
    monitor-enter p0

    #@138
    .line 4221
    :try_start_138
    new-instance v12, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v13, "NetworkTranstionWakeLock is currently "

    #@13f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v13

    #@143
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@145
    invoke-virtual {v12}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@148
    move-result v12

    #@149
    if-eqz v12, :cond_1a5

    #@14b
    const-string v12, ""

    #@14d
    :goto_14d
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v12

    #@151
    const-string v13, "held."

    #@153
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v12

    #@157
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v12

    #@15b
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@15e
    .line 4223
    new-instance v12, Ljava/lang/StringBuilder;

    #@160
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@163
    const-string v13, "It was last requested for "

    #@165
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v12

    #@169
    iget-object v13, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockCausedBy:Ljava/lang/String;

    #@16b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v12

    #@16f
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@172
    move-result-object v12

    #@173
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@176
    .line 4224
    monitor-exit p0
    :try_end_177
    .catchall {:try_start_138 .. :try_end_177} :catchall_1a8

    #@177
    .line 4225
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@17a
    .line 4227
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@17c
    move-object/from16 v0, p3

    #@17e
    invoke-virtual {v12, p1, v10, v0}, Lcom/android/server/connectivity/Tethering;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@181
    .line 4229
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mInetLog:Ljava/util/ArrayList;

    #@183
    if-eqz v12, :cond_3b

    #@185
    .line 4230
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@188
    .line 4231
    const-string v12, "Inet condition reports:"

    #@18a
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@18d
    .line 4232
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@190
    .line 4233
    const/4 v2, 0x0

    #@191
    :goto_191
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mInetLog:Ljava/util/ArrayList;

    #@193
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@196
    move-result v12

    #@197
    if-ge v2, v12, :cond_1ab

    #@199
    .line 4234
    iget-object v12, p0, Lcom/android/server/ConnectivityService;->mInetLog:Ljava/util/ArrayList;

    #@19b
    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19e
    move-result-object v12

    #@19f
    invoke-virtual {v10, v12}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/Object;)V

    #@1a2
    .line 4233
    add-int/lit8 v2, v2, 0x1

    #@1a4
    goto :goto_191

    #@1a5
    .line 4221
    :cond_1a5
    :try_start_1a5
    const-string v12, "not "

    #@1a7
    goto :goto_14d

    #@1a8
    .line 4224
    :catchall_1a8
    move-exception v12

    #@1a9
    monitor-exit p0
    :try_end_1aa
    .catchall {:try_start_1a5 .. :try_end_1aa} :catchall_1a8

    #@1aa
    throw v12

    #@1ab
    .line 4236
    :cond_1ab
    invoke-virtual {v10}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@1ae
    goto/16 :goto_3b
.end method

.method public establishVpn(Lcom/android/internal/net/VpnConfig;)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 4965
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->throwIfLockdownEnabled()V

    #@3
    .line 4966
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@5
    invoke-virtual {v0, p1}, Lcom/android/server/connectivity/Vpn;->establish(Lcom/android/internal/net/VpnConfig;)Landroid/os/ParcelFileDescriptor;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getActiveLinkProperties()Landroid/net/LinkProperties;
    .registers 2

    #@0
    .prologue
    .line 1284
    iget v0, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@2
    invoke-virtual {p0, v0}, Lcom/android/server/ConnectivityService;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getActiveNetworkInfo()Landroid/net/NetworkInfo;
    .registers 3

    #@0
    .prologue
    .line 1202
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1203
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v0

    #@7
    .line 1204
    .local v0, uid:I
    iget v1, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@9
    invoke-direct {p0, v1, v0}, Lcom/android/server/ConnectivityService;->getNetworkInfo(II)Landroid/net/NetworkInfo;

    #@c
    move-result-object v1

    #@d
    return-object v1
.end method

.method public getActiveNetworkInfoForUid(I)Landroid/net/NetworkInfo;
    .registers 3
    .parameter "uid"

    #@0
    .prologue
    .line 1220
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceConnectivityInternalPermission()V

    #@3
    .line 1221
    iget v0, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@5
    invoke-direct {p0, v0, p1}, Lcom/android/server/ConnectivityService;->getNetworkInfo(II)Landroid/net/NetworkInfo;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getActiveNetworkInfoUnfiltered()Landroid/net/NetworkInfo;
    .registers 4

    #@0
    .prologue
    .line 1208
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1209
    iget v1, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@5
    invoke-static {v1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_18

    #@b
    .line 1210
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@d
    iget v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@f
    aget-object v0, v1, v2

    #@11
    .line 1211
    .local v0, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v0, :cond_18

    #@13
    .line 1212
    invoke-interface {v0}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@16
    move-result-object v1

    #@17
    .line 1215
    .end local v0           #tracker:Landroid/net/NetworkStateTracker;
    :goto_17
    return-object v1

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17
.end method

.method public getActiveNetworkQuotaInfo()Landroid/net/NetworkQuotaInfo;
    .registers 5

    #@0
    .prologue
    .line 1329
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1331
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@6
    move-result-wide v1

    #@7
    .line 1333
    .local v1, token:J
    :try_start_7
    iget v3, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@9
    invoke-direct {p0, v3}, Lcom/android/server/ConnectivityService;->getNetworkStateUnchecked(I)Landroid/net/NetworkState;
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_1c

    #@c
    move-result-object v0

    #@d
    .line 1334
    .local v0, state:Landroid/net/NetworkState;
    if-eqz v0, :cond_1a

    #@f
    .line 1336
    :try_start_f
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mPolicyManager:Landroid/net/INetworkPolicyManager;

    #@11
    invoke-interface {v3, v0}, Landroid/net/INetworkPolicyManager;->getNetworkQuotaInfo(Landroid/net/NetworkState;)Landroid/net/NetworkQuotaInfo;
    :try_end_14
    .catchall {:try_start_f .. :try_end_14} :catchall_1c
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_19

    #@14
    move-result-object v3

    #@15
    .line 1342
    :goto_15
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@18
    .line 1340
    return-object v3

    #@19
    .line 1337
    :catch_19
    move-exception v3

    #@1a
    .line 1340
    :cond_1a
    const/4 v3, 0x0

    #@1b
    goto :goto_15

    #@1c
    .line 1342
    .end local v0           #state:Landroid/net/NetworkState;
    :catchall_1c
    move-exception v3

    #@1d
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@20
    throw v3
.end method

.method public getAllNetworkInfo()[Landroid/net/NetworkInfo;
    .registers 9

    #@0
    .prologue
    .line 1255
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1256
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v5

    #@7
    .line 1257
    .local v5, uid:I
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@a
    move-result-object v3

    #@b
    .line 1258
    .local v3, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/NetworkInfo;>;"
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mRulesLock:Ljava/lang/Object;

    #@d
    monitor-enter v7

    #@e
    .line 1259
    :try_start_e
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@10
    .local v0, arr$:[Landroid/net/NetworkStateTracker;
    array-length v2, v0

    #@11
    .local v2, len$:I
    const/4 v1, 0x0

    #@12
    .local v1, i$:I
    :goto_12
    if-ge v1, v2, :cond_22

    #@14
    aget-object v4, v0, v1

    #@16
    .line 1260
    .local v4, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v4, :cond_1f

    #@18
    .line 1261
    invoke-direct {p0, v4, v5}, Lcom/android/server/ConnectivityService;->getFilteredNetworkInfo(Landroid/net/NetworkStateTracker;I)Landroid/net/NetworkInfo;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 1259
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_12

    #@22
    .line 1264
    .end local v4           #tracker:Landroid/net/NetworkStateTracker;
    :cond_22
    monitor-exit v7
    :try_end_23
    .catchall {:try_start_e .. :try_end_23} :catchall_30

    #@23
    .line 1265
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v6

    #@27
    new-array v6, v6, [Landroid/net/NetworkInfo;

    #@29
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@2c
    move-result-object v6

    #@2d
    check-cast v6, [Landroid/net/NetworkInfo;

    #@2f
    return-object v6

    #@30
    .line 1264
    .end local v0           #arr$:[Landroid/net/NetworkStateTracker;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_30
    move-exception v6

    #@31
    :try_start_31
    monitor-exit v7
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    #@32
    throw v6
.end method

.method public getAllNetworkState()[Landroid/net/NetworkState;
    .registers 12

    #@0
    .prologue
    .line 1301
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1302
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v6

    #@7
    .line 1303
    .local v6, uid:I
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@a
    move-result-object v4

    #@b
    .line 1304
    .local v4, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/NetworkState;>;"
    iget-object v8, p0, Lcom/android/server/ConnectivityService;->mRulesLock:Ljava/lang/Object;

    #@d
    monitor-enter v8

    #@e
    .line 1305
    :try_start_e
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@10
    .local v0, arr$:[Landroid/net/NetworkStateTracker;
    array-length v3, v0

    #@11
    .local v3, len$:I
    const/4 v1, 0x0

    #@12
    .local v1, i$:I
    :goto_12
    if-ge v1, v3, :cond_2f

    #@14
    aget-object v5, v0, v1

    #@16
    .line 1306
    .local v5, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v5, :cond_2c

    #@18
    .line 1307
    invoke-direct {p0, v5, v6}, Lcom/android/server/ConnectivityService;->getFilteredNetworkInfo(Landroid/net/NetworkStateTracker;I)Landroid/net/NetworkInfo;

    #@1b
    move-result-object v2

    #@1c
    .line 1308
    .local v2, info:Landroid/net/NetworkInfo;
    new-instance v7, Landroid/net/NetworkState;

    #@1e
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@21
    move-result-object v9

    #@22
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getLinkCapabilities()Landroid/net/LinkCapabilities;

    #@25
    move-result-object v10

    #@26
    invoke-direct {v7, v2, v9, v10}, Landroid/net/NetworkState;-><init>(Landroid/net/NetworkInfo;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;)V

    #@29
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    .line 1305
    .end local v2           #info:Landroid/net/NetworkInfo;
    :cond_2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_12

    #@2f
    .line 1312
    .end local v5           #tracker:Landroid/net/NetworkStateTracker;
    :cond_2f
    monitor-exit v8
    :try_end_30
    .catchall {:try_start_e .. :try_end_30} :catchall_3d

    #@30
    .line 1313
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@33
    move-result v7

    #@34
    new-array v7, v7, [Landroid/net/NetworkState;

    #@36
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@39
    move-result-object v7

    #@3a
    check-cast v7, [Landroid/net/NetworkState;

    #@3c
    return-object v7

    #@3d
    .line 1312
    .end local v0           #arr$:[Landroid/net/NetworkStateTracker;
    .end local v1           #i$:I
    .end local v3           #len$:I
    :catchall_3d
    move-exception v7

    #@3e
    :try_start_3e
    monitor-exit v8
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    throw v7
.end method

.method public getGlobalProxy()Landroid/net/ProxyProperties;
    .registers 3

    #@0
    .prologue
    .line 4731
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mGlobalProxyLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 4732
    :try_start_3
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 4733
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getLastTetherError(Ljava/lang/String;)I
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 4476
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4478
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 4479
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/server/connectivity/Tethering;->getLastTetherError(Ljava/lang/String;)I

    #@e
    move-result v0

    #@f
    .line 4481
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x3

    #@11
    goto :goto_f
.end method

.method public getLegacyVpnInfo()Lcom/android/internal/net/LegacyVpnInfo;
    .registers 2

    #@0
    .prologue
    .line 4991
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->throwIfLockdownEnabled()V

    #@3
    .line 4992
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@5
    invoke-virtual {v0}, Lcom/android/server/connectivity/Vpn;->getLegacyVpnInfo()Lcom/android/internal/net/LegacyVpnInfo;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getLinkProperties(I)Landroid/net/LinkProperties;
    .registers 4
    .parameter "networkType"

    #@0
    .prologue
    .line 1289
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1290
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_14

    #@9
    .line 1291
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@b
    aget-object v0, v1, p1

    #@d
    .line 1292
    .local v0, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v0, :cond_14

    #@f
    .line 1293
    invoke-interface {v0}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@12
    move-result-object v1

    #@13
    .line 1296
    .end local v0           #tracker:Landroid/net/NetworkStateTracker;
    :goto_13
    return-object v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method public getMobileDataEnabled()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2434
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@4
    .line 2435
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    const-string v2, "mobile_data"

    #@c
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@f
    move-result v1

    #@10
    if-ne v1, v0, :cond_29

    #@12
    .line 2437
    .local v0, retVal:Z
    :goto_12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "getMobileDataEnabled returning "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@28
    .line 2438
    return v0

    #@29
    .line 2435
    .end local v0           #retVal:Z
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_12
.end method

.method public getMobileDataEnabledByUser()Z
    .registers 3

    #@0
    .prologue
    .line 2447
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getMobileDataEnabledByUser returning "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/server/ConnectivityService;->mSetDataEnableByUser:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@18
    .line 2448
    iget-boolean v0, p0, Lcom/android/server/ConnectivityService;->mSetDataEnableByUser:Z

    #@1a
    return v0
.end method

.method public getNetworkInfo(I)Landroid/net/NetworkInfo;
    .registers 5
    .parameter "networkType"

    #@0
    .prologue
    .line 1226
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1227
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v0

    #@7
    .line 1230
    .local v0, uid:I
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@9
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VZWAPP_CHECK_PERMISSION_VZW:Z

    #@b
    if-eqz v2, :cond_17

    #@d
    .line 1232
    move v1, p1

    #@e
    .line 1234
    .local v1, usedNetType:I
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->checkVzwNetType(I)I

    #@11
    move-result v1

    #@12
    .line 1235
    invoke-direct {p0, v1, v0}, Lcom/android/server/ConnectivityService;->getNetworkInfo(II)Landroid/net/NetworkInfo;

    #@15
    move-result-object v2

    #@16
    .line 1239
    .end local v1           #usedNetType:I
    :goto_16
    return-object v2

    #@17
    :cond_17
    invoke-direct {p0, p1, v0}, Lcom/android/server/ConnectivityService;->getNetworkInfo(II)Landroid/net/NetworkInfo;

    #@1a
    move-result-object v2

    #@1b
    goto :goto_16
.end method

.method public getNetworkPreference()I
    .registers 3

    #@0
    .prologue
    .line 1072
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1074
    monitor-enter p0

    #@4
    .line 1075
    :try_start_4
    iget v0, p0, Lcom/android/server/ConnectivityService;->mNetworkPreference:I

    #@6
    .line 1076
    .local v0, preference:I
    monitor-exit p0

    #@7
    .line 1077
    return v0

    #@8
    .line 1076
    .end local v0           #preference:I
    :catchall_8
    move-exception v1

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_8

    #@a
    throw v1
.end method

.method public getProxy()Landroid/net/ProxyProperties;
    .registers 3

    #@0
    .prologue
    .line 4680
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 4681
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxyDisabled:Z

    #@5
    if-eqz v0, :cond_a

    #@7
    const/4 v0, 0x0

    #@8
    :goto_8
    monitor-exit v1

    #@9
    return-object v0

    #@a
    :cond_a
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@c
    goto :goto_8

    #@d
    .line 4682
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public getTetherableBluetoothRegexs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4505
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4506
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 4507
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@b
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 4509
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    new-array v0, v0, [Ljava/lang/String;

    #@13
    goto :goto_f
.end method

.method public getTetherableIfaces()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4530
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4531
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@5
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->getTetherableIfaces()[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getTetherableUsbRegexs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4487
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4488
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 4489
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@b
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->getTetherableUsbRegexs()[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 4491
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    new-array v0, v0, [Ljava/lang/String;

    #@13
    goto :goto_f
.end method

.method public getTetherableWifiRegexs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4496
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4497
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 4498
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@b
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->getTetherableWifiRegexs()[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 4500
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    new-array v0, v0, [Ljava/lang/String;

    #@13
    goto :goto_f
.end method

.method public getTetheredIfacePairs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4541
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4542
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@5
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->getTetheredIfacePairs()[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getTetheredIfaces()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4535
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4536
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@5
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->getTetheredIfaces()[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getTetheringErroredIfaces()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4546
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4547
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@5
    invoke-virtual {v0}, Lcom/android/server/connectivity/Tethering;->getErroredIfaces()[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public handleConnectMobile()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 2570
    const-string v3, "phone"

    #@4
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v3

    #@8
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@b
    move-result-object v1

    #@c
    .line 2571
    .local v1, mPhoneService:Lcom/android/internal/telephony/ITelephony;
    const-string v3, "ro.support_mpdn"

    #@e
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    const-string v4, "true"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    .line 2573
    .local v0, is_mpdn:Z
    const-string v3, "ConnectivityService"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "[LGE_DATA] handleConnectMobile !! mobile = "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->getMobileDataEnabled()Z

    #@28
    move-result v5

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, "is_mpdn ="

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, "isroaming = "

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isNetworkRoaming()Z

    #@40
    move-result v5

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 2578
    if-eqz v1, :cond_60

    #@4e
    .line 2579
    :try_start_4e
    const-string v3, "enable_mUserDataEnabled"

    #@50
    invoke-interface {v1, v3}, Lcom/android/internal/telephony/ITelephony;->enableApnType(Ljava/lang/String;)I

    #@53
    .line 2581
    if-eqz v0, :cond_60

    #@55
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isNetworkRoaming()Z

    #@58
    move-result v3

    #@59
    if-eq v3, v6, :cond_60

    #@5b
    .line 2582
    const-string v3, "ims"

    #@5d
    invoke-interface {v1, v3}, Lcom/android/internal/telephony/ITelephony;->enableApnType(Ljava/lang/String;)I
    :try_end_60
    .catchall {:try_start_4e .. :try_end_60} :catchall_7a
    .catch Landroid/os/RemoteException; {:try_start_4e .. :try_end_60} :catch_95

    #@60
    .line 2589
    :cond_60
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@62
    aget-object v2, v3, v7

    #@64
    .line 2590
    .local v2, network:Landroid/net/NetworkStateTracker;
    const-string v3, "ConnectivityService"

    #@66
    const-string v4, "[LGE_DATA] reconnect the TYPE_MOBILE"

    #@68
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 2591
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->reconnect()Z

    #@6e
    .line 2593
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@70
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@73
    move-result-object v3

    #@74
    const-string v4, "mobile_data"

    #@76
    :goto_76
    invoke-static {v3, v4, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@79
    .line 2596
    return-void

    #@7a
    .line 2589
    .end local v2           #network:Landroid/net/NetworkStateTracker;
    :catchall_7a
    move-exception v3

    #@7b
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@7d
    aget-object v2, v4, v7

    #@7f
    .line 2590
    .restart local v2       #network:Landroid/net/NetworkStateTracker;
    const-string v4, "ConnectivityService"

    #@81
    const-string v5, "[LGE_DATA] reconnect the TYPE_MOBILE"

    #@83
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 2591
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->reconnect()Z

    #@89
    .line 2593
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@8b
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8e
    move-result-object v4

    #@8f
    const-string v5, "mobile_data"

    #@91
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@94
    .line 2589
    throw v3

    #@95
    .line 2586
    .end local v2           #network:Landroid/net/NetworkStateTracker;
    :catch_95
    move-exception v3

    #@96
    .line 2589
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@98
    aget-object v2, v3, v7

    #@9a
    .line 2590
    .restart local v2       #network:Landroid/net/NetworkStateTracker;
    const-string v3, "ConnectivityService"

    #@9c
    const-string v4, "[LGE_DATA] reconnect the TYPE_MOBILE"

    #@9e
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 2591
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->reconnect()Z

    #@a4
    .line 2593
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@a6
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a9
    move-result-object v3

    #@aa
    const-string v4, "mobile_data"

    #@ac
    goto :goto_76
.end method

.method public handleDisconnectMobile()V
    .registers 7

    #@0
    .prologue
    .line 2532
    const-string v3, "phone"

    #@2
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v3

    #@6
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v0

    #@a
    .line 2533
    .local v0, mPhoneService:Lcom/android/internal/telephony/ITelephony;
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@c
    .line 2535
    .local v1, mobile_state:Landroid/net/NetworkInfo$State;
    const-string v3, "ConnectivityService"

    #@e
    const-string v4, "[LGE_DATA]teardown the TYPE_MOBILE"

    #@10
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 2537
    const/4 v2, 0x0

    #@14
    .local v2, t:I
    :goto_14
    const/16 v3, 0x17

    #@16
    if-gt v2, v3, :cond_84

    #@18
    .line 2539
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@1a
    .line 2541
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@1c
    aget-object v3, v3, v2

    #@1e
    if-eqz v3, :cond_2c

    #@20
    .line 2542
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@22
    aget-object v3, v3, v2

    #@24
    invoke-interface {v3}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@2b
    move-result-object v1

    #@2c
    .line 2544
    :cond_2c
    const-string v3, "ConnectivityService"

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v5, "[LGE_DATA]teardown the TYPE_MOBILE state ::"

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 2546
    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@46
    if-eq v1, v3, :cond_50

    #@48
    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@4a
    if-eq v1, v3, :cond_50

    #@4c
    sget-object v3, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@4e
    if-ne v1, v3, :cond_81

    #@50
    .line 2551
    :cond_50
    const-string v3, "ConnectivityService"

    #@52
    new-instance v4, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v5, "[LGE_DATA] teardown : "

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@5f
    aget-object v5, v5, v2

    #@61
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v4

    #@71
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 2553
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@76
    aget-object v3, v3, v2

    #@78
    if-eqz v3, :cond_81

    #@7a
    .line 2554
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@7c
    aget-object v3, v3, v2

    #@7e
    invoke-direct {p0, v3}, Lcom/android/server/ConnectivityService;->teardown(Landroid/net/NetworkStateTracker;)Z

    #@81
    .line 2537
    :cond_81
    add-int/lit8 v2, v2, 0x1

    #@83
    goto :goto_14

    #@84
    .line 2558
    :cond_84
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@86
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@89
    move-result-object v3

    #@8a
    const-string v4, "mobile_data"

    #@8c
    const/4 v5, 0x0

    #@8d
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@90
    .line 2561
    if-eqz v0, :cond_97

    #@92
    .line 2562
    :try_start_92
    const-string v3, "disable_mUserDataEnabled"

    #@94
    invoke-interface {v0, v3}, Lcom/android/internal/telephony/ITelephony;->enableApnType(Ljava/lang/String;)I
    :try_end_97
    .catch Landroid/os/RemoteException; {:try_start_92 .. :try_end_97} :catch_98

    #@97
    .line 2567
    :cond_97
    :goto_97
    return-void

    #@98
    .line 2565
    :catch_98
    move-exception v3

    #@99
    goto :goto_97
.end method

.method public isActiveNetworkMetered()Z
    .registers 4

    #@0
    .prologue
    .line 1348
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1349
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@6
    move-result-wide v0

    #@7
    .line 1351
    .local v0, token:J
    :try_start_7
    iget v2, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@9
    invoke-direct {p0, v2}, Lcom/android/server/ConnectivityService;->isNetworkMeteredUnchecked(I)Z
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_11

    #@c
    move-result v2

    #@d
    .line 1353
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@10
    .line 1351
    return v2

    #@11
    .line 1353
    :catchall_11
    move-exception v2

    #@12
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@15
    throw v2
.end method

.method public isAllowRoaming()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1582
    :try_start_1
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    const-string v3, "data_roaming"

    #@9
    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_c} :catch_11

    #@c
    move-result v2

    #@d
    if-lez v2, :cond_10

    #@f
    const/4 v1, 0x1

    #@10
    .line 1584
    :cond_10
    :goto_10
    return v1

    #@11
    .line 1583
    :catch_11
    move-exception v0

    #@12
    .line 1584
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_10
.end method

.method isIpv4Connected(Landroid/net/LinkProperties;)Z
    .registers 8
    .parameter "linkProps"

    #@0
    .prologue
    .line 3422
    const/4 v4, 0x0

    #@1
    .line 3423
    .local v4, ret:Z
    const/4 v1, 0x0

    #@2
    .line 3425
    .local v1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    if-nez p1, :cond_6

    #@4
    const/4 v5, 0x0

    #@5
    .line 3438
    :goto_5
    return v5

    #@6
    .line 3427
    :cond_6
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@9
    move-result-object v1

    #@a
    .line 3428
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .local v2, i$:Ljava/util/Iterator;
    :cond_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v5

    #@12
    if-eqz v5, :cond_3a

    #@14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Ljava/net/InetAddress;

    #@1a
    .line 3429
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet4Address;

    #@1c
    if-eqz v5, :cond_e

    #@1e
    move-object v3, v0

    #@1f
    .line 3430
    check-cast v3, Ljava/net/Inet4Address;

    #@21
    .line 3431
    .local v3, i4addr:Ljava/net/Inet4Address;
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isAnyLocalAddress()Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_e

    #@27
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isLinkLocalAddress()Z

    #@2a
    move-result v5

    #@2b
    if-nez v5, :cond_e

    #@2d
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isLoopbackAddress()Z

    #@30
    move-result v5

    #@31
    if-nez v5, :cond_e

    #@33
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isMulticastAddress()Z

    #@36
    move-result v5

    #@37
    if-nez v5, :cond_e

    #@39
    .line 3433
    const/4 v4, 0x1

    #@3a
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #i4addr:Ljava/net/Inet4Address;
    :cond_3a
    move v5, v4

    #@3b
    .line 3438
    goto :goto_5
.end method

.method isIpv6Connected(Landroid/net/LinkProperties;)Z
    .registers 8
    .parameter "linkProps"

    #@0
    .prologue
    .line 3442
    const/4 v4, 0x0

    #@1
    .line 3443
    .local v4, ret:Z
    const/4 v1, 0x0

    #@2
    .line 3445
    .local v1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    if-nez p1, :cond_6

    #@4
    const/4 v5, 0x0

    #@5
    .line 3458
    :goto_5
    return v5

    #@6
    .line 3447
    :cond_6
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@9
    move-result-object v1

    #@a
    .line 3448
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .local v2, i$:Ljava/util/Iterator;
    :cond_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v5

    #@12
    if-eqz v5, :cond_3a

    #@14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Ljava/net/InetAddress;

    #@1a
    .line 3449
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet6Address;

    #@1c
    if-eqz v5, :cond_e

    #@1e
    move-object v3, v0

    #@1f
    .line 3450
    check-cast v3, Ljava/net/Inet6Address;

    #@21
    .line 3451
    .local v3, i6addr:Ljava/net/Inet6Address;
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isAnyLocalAddress()Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_e

    #@27
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isLinkLocalAddress()Z

    #@2a
    move-result v5

    #@2b
    if-nez v5, :cond_e

    #@2d
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isLoopbackAddress()Z

    #@30
    move-result v5

    #@31
    if-nez v5, :cond_e

    #@33
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isMulticastAddress()Z

    #@36
    move-result v5

    #@37
    if-nez v5, :cond_e

    #@39
    .line 3453
    const/4 v4, 0x1

    #@3a
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #i6addr:Ljava/net/Inet6Address;
    :cond_3a
    move v5, v4

    #@3b
    .line 3458
    goto :goto_5
.end method

.method public isNetworkRoaming()Z
    .registers 3

    #@0
    .prologue
    .line 1574
    const-string v0, "true"

    #@2
    const-string v1, "gsm.operator.isroaming"

    #@4
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public isNetworkSKT_formccmnc()Z
    .registers 5

    #@0
    .prologue
    .line 1566
    const-string v1, "persist.radio.camped_mccmnc"

    #@2
    const-string v2, "false"

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 1568
    .local v0, prop_mccmnc:Ljava/lang/String;
    const-string v1, "ConnectivityService"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "[LGE_DATA] USIM_MCC_MNC = "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1570
    const-string v1, "45005"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v1

    #@26
    return v1
.end method

.method public isNetworkSupported(I)Z
    .registers 3
    .parameter "networkType"

    #@0
    .prologue
    .line 1270
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceAccessPermission()V

    #@3
    .line 1271
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_11

    #@9
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@b
    aget-object v0, v0, p1

    #@d
    if-eqz v0, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public isSupportType(II)Z
    .registers 5
    .parameter "mdpnset"
    .parameter "netType"

    #@0
    .prologue
    .line 861
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@2
    aget-object v0, v0, p2

    #@4
    if-eqz v0, :cond_1e

    #@6
    .line 862
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "isSupportType , Here is supported, your netType : "

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@1c
    .line 863
    const/4 v0, 0x1

    #@1d
    .line 867
    :goto_1d
    return v0

    #@1e
    .line 866
    :cond_1e
    new-instance v0, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v1, "isSupportType , Here is null, your netType : "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@34
    .line 867
    const/4 v0, 0x0

    #@35
    goto :goto_1d
.end method

.method public isTetheringSupported()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 4554
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@5
    .line 4555
    const-string v4, "ro.tether.denied"

    #@7
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v4

    #@b
    const-string v5, "true"

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v4

    #@11
    if-eqz v4, :cond_4b

    #@13
    move v0, v2

    #@14
    .line 4556
    .local v0, defaultVal:I
    :goto_14
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@19
    move-result-object v4

    #@1a
    const-string v5, "tether_supported"

    #@1c
    invoke-static {v4, v5, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_4d

    #@22
    move v1, v3

    #@23
    .line 4560
    .local v1, tetherEnabledInSettings:Z
    :goto_23
    const-string v4, "ACG"

    #@25
    const-string v5, "ro.build.target_operator"

    #@27
    const-string v6, "OPEN"

    #@29
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v4

    #@31
    if-nez v4, :cond_39

    #@33
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_44

    #@39
    .line 4562
    :cond_39
    if-eqz v1, :cond_4f

    #@3b
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@3d
    invoke-virtual {v4}, Lcom/android/server/connectivity/Tethering;->isHotspotSupported()Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_4f

    #@43
    move v1, v3

    #@44
    .line 4566
    :cond_44
    :goto_44
    if-eqz v1, :cond_51

    #@46
    iget-boolean v4, p0, Lcom/android/server/ConnectivityService;->mTetheringConfigValid:Z

    #@48
    if-eqz v4, :cond_51

    #@4a
    :goto_4a
    return v3

    #@4b
    .end local v0           #defaultVal:I
    .end local v1           #tetherEnabledInSettings:Z
    :cond_4b
    move v0, v3

    #@4c
    .line 4555
    goto :goto_14

    #@4d
    .restart local v0       #defaultVal:I
    :cond_4d
    move v1, v2

    #@4e
    .line 4556
    goto :goto_23

    #@4f
    .restart local v1       #tetherEnabledInSettings:Z
    :cond_4f
    move v1, v2

    #@50
    .line 4562
    goto :goto_44

    #@51
    :cond_51
    move v3, v2

    #@52
    .line 4566
    goto :goto_4a
.end method

.method public mobileDataPdpReset()V
    .registers 4

    #@0
    .prologue
    .line 2619
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isNetworkRoaming()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_e

    #@6
    .line 2621
    const-string v1, "ConnectivityService"

    #@8
    const-string v2, "[LGE_DATA] mobileDataPdpReset is not allowed when it is not in network roaming."

    #@a
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 2634
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2625
    :cond_e
    const-string v1, "phone"

    #@10
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@13
    move-result-object v1

    #@14
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@17
    move-result-object v0

    #@18
    .line 2626
    .local v0, mPhoneService:Lcom/android/internal/telephony/ITelephony;
    const-string v1, "ConnectivityService"

    #@1a
    const-string v2, "[LGE_DATA] mobileDataPdpReset !!"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 2629
    if-eqz v0, :cond_d

    #@21
    .line 2630
    :try_start_21
    const-string v1, "mobileData_PdpReset"

    #@23
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephony;->enableApnType(Ljava/lang/String;)I
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_26} :catch_27

    #@26
    goto :goto_d

    #@27
    .line 2632
    :catch_27
    move-exception v1

    #@28
    goto :goto_d
.end method

.method public prepareVpn(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "oldPackage"
    .parameter "newPackage"

    #@0
    .prologue
    .line 4929
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->throwIfLockdownEnabled()V

    #@3
    .line 4930
    sget-boolean v4, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@5
    if-eqz v4, :cond_2c

    #@7
    .line 4931
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@9
    invoke-virtual {v4, p1, p2}, Lcom/android/server/connectivity/Vpn;->prepare(Ljava/lang/String;Ljava/lang/String;)Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_11

    #@f
    const/4 v3, 0x1

    #@10
    .line 4941
    :goto_10
    return v3

    #@11
    .line 4932
    :cond_11
    invoke-static {}, Lcom/android/server/DeviceManager3LMService;->getInstance()Lcom/android/server/DeviceManager3LMService;

    #@14
    move-result-object v0

    #@15
    .line 4933
    .local v0, dm:Lcom/android/server/DeviceManager3LMService;
    invoke-virtual {v0, p1}, Lcom/android/server/DeviceManager3LMService;->isPackage3LM(Ljava/lang/String;)Z

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_2a

    #@1b
    .line 4934
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1e
    move-result-wide v1

    #@1f
    .line 4935
    .local v1, ident:J
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@21
    const/4 v5, 0x0

    #@22
    invoke-virtual {v4, v5, p1}, Lcom/android/server/connectivity/Vpn;->prepare(Ljava/lang/String;Ljava/lang/String;)Z

    #@25
    move-result v3

    #@26
    .line 4936
    .local v3, result:Z
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@29
    goto :goto_10

    #@2a
    .line 4939
    .end local v1           #ident:J
    .end local v3           #result:Z
    :cond_2a
    const/4 v3, 0x0

    #@2b
    goto :goto_10

    #@2c
    .line 4941
    .end local v0           #dm:Lcom/android/server/DeviceManager3LMService;
    :cond_2c
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@2e
    invoke-virtual {v4, p1, p2}, Lcom/android/server/connectivity/Vpn;->prepare(Ljava/lang/String;Ljava/lang/String;)Z

    #@31
    move-result v3

    #@32
    goto :goto_10
.end method

.method public prepareVpnEx(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 5
    .parameter "oldPackage"
    .parameter "newPackage"
    .parameter "mvpOn"

    #@0
    .prologue
    .line 4951
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->throwIfLockdownEnabled()V

    #@3
    .line 4952
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/connectivity/Vpn;->prepare(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public protectVpn(Landroid/os/ParcelFileDescriptor;)Z
    .registers 5
    .parameter "socket"

    #@0
    .prologue
    .line 4902
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->throwIfLockdownEnabled()V

    #@3
    .line 4904
    :try_start_3
    iget v0, p0, Lcom/android/server/ConnectivityService;->mActiveDefaultNetwork:I

    #@5
    .line 4905
    .local v0, type:I
    invoke-static {v0}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_32

    #@b
    .line 4906
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@d
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@f
    aget-object v2, v2, v0

    #@11
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, p1, v2}, Lcom/android/server/connectivity/Vpn;->protect(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_21
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_1c} :catch_26

    #@1c
    .line 4907
    const/4 v1, 0x1

    #@1d
    .line 4913
    :try_start_1d
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_20} :catch_30

    #@20
    .line 4918
    .end local v0           #type:I
    :goto_20
    return v1

    #@21
    .line 4912
    :catchall_21
    move-exception v1

    #@22
    .line 4913
    :try_start_22
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_25} :catch_2e

    #@25
    .line 4912
    :goto_25
    throw v1

    #@26
    .line 4909
    :catch_26
    move-exception v1

    #@27
    .line 4913
    :try_start_27
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V

    #@2a
    .line 4918
    :goto_2a
    const/4 v1, 0x0

    #@2b
    goto :goto_20

    #@2c
    .line 4914
    :catch_2c
    move-exception v1

    #@2d
    goto :goto_2a

    #@2e
    :catch_2e
    move-exception v2

    #@2f
    goto :goto_25

    #@30
    .restart local v0       #type:I
    :catch_30
    move-exception v2

    #@31
    goto :goto_20

    #@32
    .line 4913
    :cond_32
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_35} :catch_2c

    #@35
    goto :goto_2a
.end method

.method public reportInetCondition(II)V
    .registers 9
    .parameter "networkType"
    .parameter "percentage"

    #@0
    .prologue
    .line 4590
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "reportNetworkCondition("

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    const-string v4, ", "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, ")"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@26
    .line 4591
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@28
    const-string v4, "android.permission.STATUS_BAR"

    #@2a
    const-string v5, "ConnectivityService"

    #@2c
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@2f
    .line 4596
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingPid()I

    #@32
    move-result v0

    #@33
    .line 4597
    .local v0, pid:I
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingUid()I

    #@36
    move-result v2

    #@37
    .line 4598
    .local v2, uid:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    const-string v4, "("

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    const-string v4, ") reports inet is "

    #@4c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    const/16 v3, 0x32

    #@52
    if-le p2, v3, :cond_a0

    #@54
    const-string v3, "connected"

    #@56
    :goto_56
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    const-string v4, " ("

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    const-string v4, ") on "

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    const-string v4, "network Type "

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    const-string v4, " at "

    #@76
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v1

    #@8a
    .line 4601
    .local v1, s:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mInetLog:Ljava/util/ArrayList;

    #@8c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8f
    .line 4602
    :goto_8f
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mInetLog:Ljava/util/ArrayList;

    #@91
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@94
    move-result v3

    #@95
    const/16 v4, 0xf

    #@97
    if-le v3, v4, :cond_a3

    #@99
    .line 4603
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mInetLog:Ljava/util/ArrayList;

    #@9b
    const/4 v4, 0x0

    #@9c
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@9f
    goto :goto_8f

    #@a0
    .line 4598
    .end local v1           #s:Ljava/lang/String;
    :cond_a0
    const-string v3, "disconnected"

    #@a2
    goto :goto_56

    #@a3
    .line 4606
    .restart local v1       #s:Ljava/lang/String;
    :cond_a3
    iget-object v3, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@a5
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@a7
    const/4 v5, 0x4

    #@a8
    invoke-virtual {v4, v5, p1, p2}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@ab
    move-result-object v4

    #@ac
    invoke-virtual {v3, v4}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessage(Landroid/os/Message;)Z

    #@af
    .line 4608
    return-void
.end method

.method public requestNetworkTransitionWakelock(Ljava/lang/String;)V
    .registers 7
    .parameter "forWhom"

    #@0
    .prologue
    .line 4574
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceConnectivityInternalPermission()V

    #@3
    .line 4575
    monitor-enter p0

    #@4
    .line 4576
    :try_start_4
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@6
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    monitor-exit p0

    #@d
    .line 4585
    :goto_d
    return-void

    #@e
    .line 4577
    :cond_e
    iget v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockSerialNumber:I

    #@10
    add-int/lit8 v0, v0, 0x1

    #@12
    iput v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockSerialNumber:I

    #@14
    .line 4578
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@16
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@19
    .line 4579
    iput-object p1, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockCausedBy:Ljava/lang/String;

    #@1b
    .line 4580
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_30

    #@1c
    .line 4581
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@1e
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@20
    const/16 v2, 0x8

    #@22
    iget v3, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockSerialNumber:I

    #@24
    const/4 v4, 0x0

    #@25
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@28
    move-result-object v1

    #@29
    iget v2, p0, Lcom/android/server/ConnectivityService;->mNetTransitionWakeLockTimeout:I

    #@2b
    int-to-long v2, v2

    #@2c
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2f
    goto :goto_d

    #@30
    .line 4580
    :catchall_30
    move-exception v0

    #@31
    :try_start_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    #@32
    throw v0
.end method

.method public requestRemRouteToHostAddress(I[B)Z
    .registers 12
    .parameter "networkType"
    .parameter "hostAddress"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2188
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@4
    .line 2189
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mProtectedNetworks:Ljava/util/List;

    #@6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v8

    #@a
    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@d
    move-result v7

    #@e
    if-eqz v7, :cond_13

    #@10
    .line 2190
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceConnectivityInternalPermission()V

    #@13
    .line 2193
    :cond_13
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@16
    move-result v7

    #@17
    if-nez v7, :cond_30

    #@19
    .line 2194
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v8, "requestRouteToHostAddress on invalid network: "

    #@20
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v7

    #@2c
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2f
    .line 2217
    :goto_2f
    return v6

    #@30
    .line 2197
    :cond_30
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@32
    aget-object v5, v7, p1

    #@34
    .line 2199
    .local v5, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v5, :cond_46

    #@36
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@39
    move-result-object v7

    #@3a
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_46

    #@40
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@43
    move-result v7

    #@44
    if-eqz v7, :cond_63

    #@46
    .line 2202
    :cond_46
    new-instance v7, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v8, "requestRouteToHostAddress on down network ("

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    const-string v8, ") - dropped"

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v7

    #@5f
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@62
    goto :goto_2f

    #@63
    .line 2207
    :cond_63
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@66
    move-result-wide v3

    #@67
    .line 2209
    .local v3, token:J
    :try_start_67
    invoke-static {p2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@6a
    move-result-object v0

    #@6b
    .line 2210
    .local v0, addr:Ljava/net/InetAddress;
    invoke-interface {v5}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@6e
    move-result-object v2

    #@6f
    .line 2211
    .local v2, lp:Landroid/net/LinkProperties;
    invoke-direct {p0, v2, v0}, Lcom/android/server/ConnectivityService;->removeRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z
    :try_end_72
    .catchall {:try_start_67 .. :try_end_72} :catchall_93
    .catch Ljava/net/UnknownHostException; {:try_start_67 .. :try_end_72} :catch_77

    #@72
    move-result v6

    #@73
    .line 2215
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v2           #lp:Landroid/net/LinkProperties;
    :goto_73
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@76
    goto :goto_2f

    #@77
    .line 2212
    :catch_77
    move-exception v1

    #@78
    .line 2213
    .local v1, e:Ljava/net/UnknownHostException;
    :try_start_78
    new-instance v7, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v8, "requestAddressAPI got "

    #@7f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v7

    #@83
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v7

    #@8b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v7

    #@8f
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_92
    .catchall {:try_start_78 .. :try_end_92} :catchall_93

    #@92
    goto :goto_73

    #@93
    .line 2215
    .end local v1           #e:Ljava/net/UnknownHostException;
    :catchall_93
    move-exception v6

    #@94
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@97
    throw v6
.end method

.method public requestRouteToHost(II)Z
    .registers 7
    .parameter "networkType"
    .parameter "hostAddress"

    #@0
    .prologue
    .line 2233
    invoke-static {p2}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    .line 2235
    .local v0, inetAddress:Ljava/net/InetAddress;
    if-nez v0, :cond_8

    #@6
    .line 2236
    const/4 v2, 0x0

    #@7
    .line 2248
    :goto_7
    return v2

    #@8
    .line 2239
    :cond_8
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@a
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VZWAPP_CHECK_PERMISSION_VZW:Z

    #@c
    if-eqz v2, :cond_23

    #@e
    .line 2241
    move v1, p1

    #@f
    .line 2242
    .local v1, usedNetType:I
    const-string v2, "ConnectivityService"

    #@11
    const-string v3, "requestRouteToHost is called, so check type"

    #@13
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 2243
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->checkVzwNetType(I)I

    #@19
    move-result v1

    #@1a
    .line 2244
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {p0, v1, v2}, Lcom/android/server/ConnectivityService;->requestRouteToHostAddress(I[B)Z

    #@21
    move-result v2

    #@22
    goto :goto_7

    #@23
    .line 2248
    .end local v1           #usedNetType:I
    :cond_23
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@26
    move-result-object v2

    #@27
    invoke-virtual {p0, p1, v2}, Lcom/android/server/ConnectivityService;->requestRouteToHostAddress(I[B)Z

    #@2a
    move-result v2

    #@2b
    goto :goto_7
.end method

.method public requestRouteToHostAddress(I[B)Z
    .registers 14
    .parameter "networkType"
    .parameter "hostAddress"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2261
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@4
    .line 2262
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mProtectedNetworks:Ljava/util/List;

    #@6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v10

    #@a
    invoke-interface {v9, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@d
    move-result v9

    #@e
    if-eqz v9, :cond_13

    #@10
    .line 2263
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceConnectivityInternalPermission()V

    #@13
    .line 2266
    :cond_13
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@16
    move-result v9

    #@17
    if-nez v9, :cond_30

    #@19
    .line 2267
    new-instance v9, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v10, "requestRouteToHostAddress on invalid network: "

    #@20
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v9

    #@24
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v9

    #@28
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v9

    #@2c
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@2f
    .line 2301
    :goto_2f
    return v8

    #@30
    .line 2271
    :cond_30
    move v7, p1

    #@31
    .line 2272
    .local v7, usedNetType:I
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@33
    iget-boolean v9, v9, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VZWAPP_CHECK_PERMISSION_VZW:Z

    #@35
    if-eqz v9, :cond_42

    #@37
    .line 2273
    const-string v9, "ConnectivityService"

    #@39
    const-string v10, "requestRouteToHostAddress is called, so check type"

    #@3b
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 2274
    invoke-direct {p0, p1}, Lcom/android/server/ConnectivityService;->checkVzwNetType(I)I

    #@41
    move-result v7

    #@42
    .line 2277
    :cond_42
    iget-object v9, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@44
    aget-object v6, v9, v7

    #@46
    .line 2278
    .local v6, tracker:Landroid/net/NetworkStateTracker;
    invoke-interface {v6}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@49
    move-result-object v9

    #@4a
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@4d
    move-result-object v3

    #@4e
    .line 2280
    .local v3, netState:Landroid/net/NetworkInfo$DetailedState;
    if-eqz v6, :cond_5e

    #@50
    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@52
    if-eq v3, v9, :cond_58

    #@54
    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    #@56
    if-ne v3, v9, :cond_5e

    #@58
    :cond_58
    invoke-interface {v6}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@5b
    move-result v9

    #@5c
    if-eqz v9, :cond_7b

    #@5e
    .line 2284
    :cond_5e
    new-instance v9, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v10, "requestRouteToHostAddress on down network ("

    #@65
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v9

    #@69
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v9

    #@6d
    const-string v10, ") - dropped"

    #@6f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v9

    #@73
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v9

    #@77
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@7a
    goto :goto_2f

    #@7b
    .line 2291
    :cond_7b
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@7e
    move-result-wide v4

    #@7f
    .line 2293
    .local v4, token:J
    :try_start_7f
    invoke-static {p2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@82
    move-result-object v0

    #@83
    .line 2294
    .local v0, addr:Ljava/net/InetAddress;
    invoke-interface {v6}, Landroid/net/NetworkStateTracker;->getLinkProperties()Landroid/net/LinkProperties;

    #@86
    move-result-object v2

    #@87
    .line 2295
    .local v2, lp:Landroid/net/LinkProperties;
    invoke-direct {p0, v2, v0}, Lcom/android/server/ConnectivityService;->addRouteToAddress(Landroid/net/LinkProperties;Ljava/net/InetAddress;)Z
    :try_end_8a
    .catchall {:try_start_7f .. :try_end_8a} :catchall_ab
    .catch Ljava/net/UnknownHostException; {:try_start_7f .. :try_end_8a} :catch_8f

    #@8a
    move-result v8

    #@8b
    .line 2299
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v2           #lp:Landroid/net/LinkProperties;
    :goto_8b
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@8e
    goto :goto_2f

    #@8f
    .line 2296
    :catch_8f
    move-exception v1

    #@90
    .line 2297
    .local v1, e:Ljava/net/UnknownHostException;
    :try_start_90
    new-instance v9, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v10, "requestRouteToHostAddress got "

    #@97
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v9

    #@9b
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;

    #@9e
    move-result-object v10

    #@9f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v9

    #@a3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v9

    #@a7
    invoke-static {v9}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_aa
    .catchall {:try_start_90 .. :try_end_aa} :catchall_ab

    #@aa
    goto :goto_8b

    #@ab
    .line 2299
    .end local v1           #e:Ljava/net/UnknownHostException;
    :catchall_ab
    move-exception v8

    #@ac
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@af
    throw v8
.end method

.method public sendConnectedBroadcast(Landroid/net/NetworkInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 2960
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/server/ConnectivityService;->sendGeneralBroadcast(Landroid/net/NetworkInfo;Ljava/lang/String;)V

    #@5
    .line 2961
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    #@7
    invoke-direct {p0, p1, v0}, Lcom/android/server/ConnectivityService;->sendGeneralBroadcast(Landroid/net/NetworkInfo;Ljava/lang/String;)V

    #@a
    .line 2962
    return-void
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 7
    .parameter "msger"

    #@0
    .prologue
    .line 2601
    const-string v4, "setDataConnectionMessanger_byCS "

    #@2
    invoke-static {v4}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@5
    .line 2603
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mPriorityList:[I

    #@7
    .local v0, arr$:[I
    array-length v2, v0

    #@8
    .local v2, len$:I
    const/4 v1, 0x0

    #@9
    .local v1, i$:I
    :goto_9
    if-ge v1, v2, :cond_2b

    #@b
    aget v3, v0, v1

    #@d
    .line 2604
    .local v3, netType:I
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@f
    aget-object v4, v4, v3

    #@11
    iget v4, v4, Landroid/net/NetworkConfig;->radio:I

    #@13
    packed-switch v4, :pswitch_data_2c

    #@16
    .line 2603
    :cond_16
    :goto_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_9

    #@19
    .line 2606
    :pswitch_19
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1b
    iget v4, v4, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@1d
    invoke-virtual {p0, v4, v3}, Lcom/android/server/ConnectivityService;->isSupportType(II)Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_16

    #@23
    .line 2609
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@25
    aget-object v4, v4, v3

    #@27
    invoke-interface {v4, p1}, Landroid/net/NetworkStateTracker;->setDataConnectionMessanger(Landroid/os/Messenger;)V

    #@2a
    goto :goto_16

    #@2b
    .line 2612
    .end local v3           #netType:I
    :cond_2b
    return-void

    #@2c
    .line 2604
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_19
    .end packed-switch
.end method

.method public setDataDependency(IZ)V
    .registers 7
    .parameter "networkType"
    .parameter "met"

    #@0
    .prologue
    .line 2453
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceConnectivityInternalPermission()V

    #@3
    .line 2455
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@5
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@7
    const/16 v3, 0xa

    #@9
    if-eqz p2, :cond_14

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    invoke-virtual {v2, v3, v0, p1}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v1, v0}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 2457
    return-void

    #@14
    .line 2455
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_c
.end method

.method public setGlobalProxy(Landroid/net/ProxyProperties;)V
    .registers 8
    .parameter "proxyProperties"

    #@0
    .prologue
    .line 4686
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@3
    .line 4687
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mGlobalProxyLock:Ljava/lang/Object;

    #@5
    monitor-enter v5

    #@6
    .line 4688
    :try_start_6
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@8
    if-ne p1, v4, :cond_c

    #@a
    monitor-exit v5

    #@b
    .line 4714
    :cond_b
    :goto_b
    return-void

    #@c
    .line 4689
    :cond_c
    if-eqz p1, :cond_1b

    #@e
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@10
    invoke-virtual {p1, v4}, Landroid/net/ProxyProperties;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_1b

    #@16
    monitor-exit v5

    #@17
    goto :goto_b

    #@18
    .line 4708
    :catchall_18
    move-exception v4

    #@19
    monitor-exit v5
    :try_end_1a
    .catchall {:try_start_6 .. :try_end_1a} :catchall_18

    #@1a
    throw v4

    #@1b
    .line 4690
    :cond_1b
    :try_start_1b
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@1d
    if-eqz v4, :cond_29

    #@1f
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@21
    invoke-virtual {v4, p1}, Landroid/net/ProxyProperties;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_29

    #@27
    monitor-exit v5

    #@28
    goto :goto_b

    #@29
    .line 4692
    :cond_29
    const-string v1, ""

    #@2b
    .line 4693
    .local v1, host:Ljava/lang/String;
    const/4 v2, 0x0

    #@2c
    .line 4694
    .local v2, port:I
    const-string v0, ""

    #@2e
    .line 4695
    .local v0, exclList:Ljava/lang/String;
    if-eqz p1, :cond_70

    #@30
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@37
    move-result v4

    #@38
    if-nez v4, :cond_70

    #@3a
    .line 4696
    new-instance v4, Landroid/net/ProxyProperties;

    #@3c
    invoke-direct {v4, p1}, Landroid/net/ProxyProperties;-><init>(Landroid/net/ProxyProperties;)V

    #@3f
    iput-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@41
    .line 4697
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@43
    invoke-virtual {v4}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    .line 4698
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@49
    invoke-virtual {v4}, Landroid/net/ProxyProperties;->getPort()I

    #@4c
    move-result v2

    #@4d
    .line 4699
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@4f
    invoke-virtual {v4}, Landroid/net/ProxyProperties;->getExclusionList()Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    .line 4703
    :goto_53
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@55
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@58
    move-result-object v3

    #@59
    .line 4704
    .local v3, res:Landroid/content/ContentResolver;
    const-string v4, "global_http_proxy_host"

    #@5b
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@5e
    .line 4705
    const-string v4, "global_http_proxy_port"

    #@60
    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@63
    .line 4706
    const-string v4, "global_http_proxy_exclusion_list"

    #@65
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@68
    .line 4708
    monitor-exit v5
    :try_end_69
    .catchall {:try_start_1b .. :try_end_69} :catchall_18

    #@69
    .line 4710
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;

    #@6b
    if-nez v4, :cond_b

    #@6d
    .line 4711
    iget-object p1, p0, Lcom/android/server/ConnectivityService;->mDefaultProxy:Landroid/net/ProxyProperties;

    #@6f
    goto :goto_b

    #@70
    .line 4701
    .end local v3           #res:Landroid/content/ContentResolver;
    :cond_70
    const/4 v4, 0x0

    #@71
    :try_start_71
    iput-object v4, p0, Lcom/android/server/ConnectivityService;->mGlobalProxy:Landroid/net/ProxyProperties;
    :try_end_73
    .catchall {:try_start_71 .. :try_end_73} :catchall_18

    #@73
    goto :goto_53
.end method

.method public setMobileDataEnabled(Z)V
    .registers 9
    .parameter "enabled"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 2641
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@5
    .line 2642
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "setMobileDataEnabled("

    #@c
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v4, ")"

    #@16
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@21
    .line 2645
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@23
    if-eqz v1, :cond_3b

    #@25
    .line 2646
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@28
    move-result-object v0

    #@29
    .line 2647
    .local v0, mdm:Lcom/lge/cappuccino/IMdm;
    if-eqz v0, :cond_3b

    #@2b
    .line 2648
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@2e
    move-result v1

    #@2f
    invoke-interface {v0, p1, v1}, Lcom/lge/cappuccino/IMdm;->checkMobileNetwork(ZI)Z

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_3b

    #@35
    .line 2649
    const-string v1, "Mobile Network cannot be changed by Server Policy."

    #@37
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@3a
    .line 2663
    .end local v0           #mdm:Lcom/lge/cappuccino/IMdm;
    :cond_3a
    :goto_3a
    return-void

    #@3b
    .line 2655
    :cond_3b
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@3d
    iget-object v5, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@3f
    const/4 v6, 0x7

    #@40
    if-eqz p1, :cond_53

    #@42
    move v1, v2

    #@43
    :goto_43
    invoke-virtual {v5, v6, v1, v3}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v4, v1}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessage(Landroid/os/Message;)Z

    #@4a
    .line 2659
    iget-boolean v1, p0, Lcom/android/server/ConnectivityService;->mSetDataEnableByUser:Z

    #@4c
    if-nez v1, :cond_3a

    #@4e
    if-ne p1, v2, :cond_3a

    #@50
    .line 2660
    iput-boolean v2, p0, Lcom/android/server/ConnectivityService;->mSetDataEnableByUser:Z

    #@52
    goto :goto_3a

    #@53
    :cond_53
    move v1, v3

    #@54
    .line 2655
    goto :goto_43
.end method

.method public setNetworkPreference(I)V
    .registers 6
    .parameter "preference"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1056
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@5
    .line 1059
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@7
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@9
    const-string v1, "DCMBASE"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 1060
    if-eq p1, v2, :cond_24

    #@13
    .line 1061
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@15
    iput-boolean v3, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@17
    .line 1067
    :cond_17
    :goto_17
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@19
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@1b
    const/4 v2, 0x3

    #@1c
    invoke-virtual {v1, v2, p1, v3}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessage(Landroid/os/Message;)Z

    #@23
    .line 1069
    return-void

    #@24
    .line 1063
    :cond_24
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@26
    iput-boolean v2, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_DUAL_CONNECTIVITY_DCM:Z

    #@28
    goto :goto_17
.end method

.method public setPolicyDataEnable(IZ)V
    .registers 7
    .parameter "networkType"
    .parameter "enabled"

    #@0
    .prologue
    .line 2683
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v2, "ConnectivityService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2685
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@b
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@d
    const/16 v3, 0xd

    #@f
    if-eqz p2, :cond_1a

    #@11
    const/4 v0, 0x1

    #@12
    :goto_12
    invoke-virtual {v2, v3, p1, v0}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(III)Landroid/os/Message;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v1, v0}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessage(Landroid/os/Message;)Z

    #@19
    .line 2687
    return-void

    #@1a
    .line 2685
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_12
.end method

.method public setRadio(IZ)Z
    .registers 6
    .parameter "netType"
    .parameter "turnOn"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1378
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@4
    .line 1379
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_b

    #@a
    .line 1383
    :cond_a
    :goto_a
    return v1

    #@b
    .line 1382
    :cond_b
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@d
    aget-object v0, v2, p1

    #@f
    .line 1383
    .local v0, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v0, :cond_a

    #@11
    invoke-interface {v0, p2}, Landroid/net/NetworkStateTracker;->setRadio(Z)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_a

    #@17
    const/4 v1, 0x1

    #@18
    goto :goto_a
.end method

.method public setRadios(Z)Z
    .registers 8
    .parameter "turnOn"

    #@0
    .prologue
    .line 1369
    const/4 v3, 0x1

    #@1
    .line 1370
    .local v3, result:Z
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@4
    .line 1371
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@6
    .local v0, arr$:[Landroid/net/NetworkStateTracker;
    array-length v2, v0

    #@7
    .local v2, len$:I
    const/4 v1, 0x0

    #@8
    .local v1, i$:I
    :goto_8
    if-ge v1, v2, :cond_1c

    #@a
    aget-object v4, v0, v1

    #@c
    .line 1372
    .local v4, t:Landroid/net/NetworkStateTracker;
    if-eqz v4, :cond_17

    #@e
    invoke-interface {v4, p1}, Landroid/net/NetworkStateTracker;->setRadio(Z)Z

    #@11
    move-result v5

    #@12
    if-eqz v5, :cond_1a

    #@14
    if-eqz v3, :cond_1a

    #@16
    const/4 v3, 0x1

    #@17
    .line 1371
    :cond_17
    :goto_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 1372
    :cond_1a
    const/4 v3, 0x0

    #@1b
    goto :goto_17

    #@1c
    .line 1374
    .end local v4           #t:Landroid/net/NetworkStateTracker;
    :cond_1c
    return v3
.end method

.method public setUsbTethering(Z)I
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    .line 4514
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherAccessPermission()V

    #@3
    .line 4516
    if-eqz p1, :cond_17

    #@5
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    const-string v1, "tethering_blocked"

    #@d
    const/4 v2, 0x0

    #@e
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    const/4 v1, 0x1

    #@13
    if-ne v0, v1, :cond_17

    #@15
    .line 4518
    const/4 v0, 0x2

    #@16
    .line 4523
    :goto_16
    return v0

    #@17
    .line 4520
    :cond_17
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_24

    #@1d
    .line 4521
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@1f
    invoke-virtual {v0, p1}, Lcom/android/server/connectivity/Tethering;->setUsbTethering(Z)I

    #@22
    move-result v0

    #@23
    goto :goto_16

    #@24
    .line 4523
    :cond_24
    const/4 v0, 0x3

    #@25
    goto :goto_16
.end method

.method public showTetheringBlockNotification(I)V
    .registers 4
    .parameter "icon"

    #@0
    .prologue
    .line 1594
    const-string v0, "ConnectivityService"

    #@2
    const-string v1, "[ConnectivityService] showTetheringBlockNotification"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1595
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/server/connectivity/Tethering;->showTetheredBlockNotification(I)V

    #@c
    .line 1596
    return-void
.end method

.method public startLegacyVpn(Lcom/android/internal/net/VpnProfile;)V
    .registers 5
    .parameter "profile"

    #@0
    .prologue
    .line 4975
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->throwIfLockdownEnabled()V

    #@3
    .line 4976
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->getActiveLinkProperties()Landroid/net/LinkProperties;

    #@6
    move-result-object v0

    #@7
    .line 4977
    .local v0, egress:Landroid/net/LinkProperties;
    if-nez v0, :cond_11

    #@9
    .line 4978
    new-instance v1, Ljava/lang/IllegalStateException;

    #@b
    const-string v2, "Missing active network connection"

    #@d
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 4980
    :cond_11
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@13
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mKeyStore:Landroid/security/KeyStore;

    #@15
    invoke-virtual {v1, p1, v2, v0}, Lcom/android/server/connectivity/Vpn;->startLegacyVpn(Lcom/android/internal/net/VpnProfile;Landroid/security/KeyStore;Landroid/net/LinkProperties;)V

    #@18
    .line 4981
    return-void
.end method

.method public startUsingNetworkFeature(ILjava/lang/String;Landroid/os/IBinder;)I
    .registers 39
    .parameter "networkType"
    .parameter "feature"
    .parameter "binder"

    #@0
    .prologue
    .line 1637
    const-wide/16 v23, 0x0

    #@2
    .line 1639
    .local v23, startTime:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5
    move-result-wide v23

    #@6
    .line 1643
    move-object/from16 v0, p0

    #@8
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@a
    move-object/from16 v31, v0

    #@c
    invoke-static/range {v31 .. v31}, Lcom/android/internal/telephony/DataConnectionManager;->getInstance(Landroid/content/Context;)Lcom/android/internal/telephony/DataConnectionManager;

    #@f
    move-result-object v6

    #@10
    .line 1647
    .local v6, dataMgr:Lcom/android/internal/telephony/DataConnectionManager;
    new-instance v31, Ljava/lang/StringBuilder;

    #@12
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v32, "startUsingNetworkFeature for net "

    #@17
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v31

    #@1b
    move-object/from16 v0, v31

    #@1d
    move/from16 v1, p1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v31

    #@23
    const-string v32, ": "

    #@25
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v31

    #@29
    move-object/from16 v0, v31

    #@2b
    move-object/from16 v1, p2

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v31

    #@31
    const-string v32, ", uid="

    #@33
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v31

    #@37
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3a
    move-result v32

    #@3b
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v31

    #@3f
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v31

    #@43
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@46
    .line 1652
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@49
    move-result-object v31

    #@4a
    if-eqz v31, :cond_7f

    #@4c
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4f
    move-result-object v31

    #@50
    const/16 v32, 0x0

    #@52
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@55
    move-result v33

    #@56
    invoke-interface/range {v31 .. v33}, Lcom/lge/cappuccino/IMdm;->getAllowSendingSms(Landroid/content/ComponentName;I)Z

    #@59
    move-result v31

    #@5a
    if-nez v31, :cond_7f

    #@5c
    const-string v31, "enableMMS"

    #@5e
    move-object/from16 v0, p2

    #@60
    move-object/from16 v1, v31

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v31

    #@66
    if-eqz v31, :cond_7f

    #@68
    .line 1655
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6b
    move-result-object v31

    #@6c
    const/16 v32, 0x0

    #@6e
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@71
    move-result v33

    #@72
    invoke-interface/range {v31 .. v33}, Lcom/lge/cappuccino/IMdm;->isAllowSendMMS(Landroid/content/ComponentName;I)Z

    #@75
    .line 1657
    const-string v31, "ConnectivityService"

    #@77
    const-string v32, "Block Sending SMS in ConnectivityService"

    #@79
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 1658
    const/16 v22, 0x3

    #@7e
    .line 2000
    :goto_7e
    return v22

    #@7f
    .line 1662
    :cond_7f
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@82
    .line 1664
    :try_start_82
    invoke-static/range {p1 .. p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@85
    move-result v31

    #@86
    if-eqz v31, :cond_92

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetConfigs:[Landroid/net/NetworkConfig;

    #@8c
    move-object/from16 v31, v0

    #@8e
    aget-object v31, v31, p1
    :try_end_90
    .catchall {:try_start_82 .. :try_end_90} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_82 .. :try_end_90} :catch_319

    #@90
    if-nez v31, :cond_bf

    #@92
    .line 1666
    :cond_92
    const/16 v22, 0x3

    #@94
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@97
    move-result-wide v31

    #@98
    sub-long v8, v31, v23

    #@9a
    .line 2004
    .local v8, execTime:J
    const-wide/16 v31, 0xfa

    #@9c
    cmp-long v31, v8, v31

    #@9e
    if-lez v31, :cond_acc

    #@a0
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v32, "startUsingNetworkFeature took too long: "

    #@a7
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v31

    #@ab
    move-object/from16 v0, v31

    #@ad
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v31

    #@b1
    const-string v32, "ms"

    #@b3
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v31

    #@b7
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v31

    #@bb
    :goto_bb
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@be
    goto :goto_7e

    #@bf
    .line 1669
    .end local v8           #execTime:J
    :cond_bf
    :try_start_bf
    move-object/from16 v0, p0

    #@c1
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@c3
    move-object/from16 v31, v0

    #@c5
    move-object/from16 v0, v31

    #@c7
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_KAF_KT:Z

    #@c9
    move/from16 v31, v0

    #@cb
    if-eqz v31, :cond_132

    #@cd
    .line 1670
    const-string v31, "KT_Internet"

    #@cf
    move-object/from16 v0, p2

    #@d1
    move-object/from16 v1, v31

    #@d3
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d6
    move-result v31

    #@d7
    const/16 v32, 0x1

    #@d9
    move/from16 v0, v31

    #@db
    move/from16 v1, v32

    #@dd
    if-ne v0, v1, :cond_132

    #@df
    .line 1672
    sget-object v31, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->KT_KAF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@e1
    new-instance v32, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    move-object/from16 v0, p0

    #@e8
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@ea
    move-object/from16 v33, v0

    #@ec
    aget-object v33, v33, p1

    #@ee
    invoke-interface/range {v33 .. v33}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@f1
    move-result v33

    #@f2
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v32

    #@f6
    const-string v33, ""

    #@f8
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v32

    #@fc
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ff
    move-result-object v32

    #@100
    move-object/from16 v0, v31

    #@102
    move-object/from16 v1, v32

    #@104
    move/from16 v2, p1

    #@106
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I
    :try_end_109
    .catchall {:try_start_bf .. :try_end_109} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_bf .. :try_end_109} :catch_319

    #@109
    move-result v22

    #@10a
    .line 2003
    .local v22, ret:I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10d
    move-result-wide v31

    #@10e
    sub-long v8, v31, v23

    #@110
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@112
    cmp-long v31, v8, v31

    #@114
    if-lez v31, :cond_ae8

    #@116
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@118
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    const-string v32, "startUsingNetworkFeature took too long: "

    #@11d
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v31

    #@121
    move-object/from16 v0, v31

    #@123
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@126
    move-result-object v31

    #@127
    const-string v32, "ms"

    #@129
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v31

    #@12d
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v31

    #@131
    goto :goto_bb

    #@132
    .line 1680
    .end local v8           #execTime:J
    .end local v22           #ret:I
    :cond_132
    :try_start_132
    move-object/from16 v0, p0

    #@134
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@136
    move-object/from16 v31, v0

    #@138
    move-object/from16 v0, v31

    #@13a
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@13c
    move/from16 v31, v0

    #@13e
    if-eqz v31, :cond_181

    #@140
    .line 1682
    sget-object v31, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->MMS_MOBILE_OFF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@142
    const/16 v32, 0x0

    #@144
    move-object/from16 v0, v31

    #@146
    move-object/from16 v1, p2

    #@148
    move/from16 v2, v32

    #@14a
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I
    :try_end_14d
    .catchall {:try_start_132 .. :try_end_14d} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_132 .. :try_end_14d} :catch_319

    #@14d
    move-result v22

    #@14e
    .line 1683
    .restart local v22       #ret:I
    const/16 v31, 0x2

    #@150
    move/from16 v0, v22

    #@152
    move/from16 v1, v31

    #@154
    if-ne v0, v1, :cond_181

    #@156
    .line 1684
    const/16 v22, 0x2

    #@158
    .line 2003
    .end local v22           #ret:I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@15b
    move-result-wide v31

    #@15c
    sub-long v8, v31, v23

    #@15e
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@160
    cmp-long v31, v8, v31

    #@162
    if-lez v31, :cond_b04

    #@164
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@166
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@169
    const-string v32, "startUsingNetworkFeature took too long: "

    #@16b
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v31

    #@16f
    move-object/from16 v0, v31

    #@171
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@174
    move-result-object v31

    #@175
    const-string v32, "ms"

    #@177
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v31

    #@17b
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v31

    #@17f
    goto/16 :goto_bb

    #@181
    .line 1689
    .end local v8           #execTime:J
    :cond_181
    :try_start_181
    invoke-direct/range {p0 .. p2}, Lcom/android/server/ConnectivityService;->convertFeatureToNetworkType(ILjava/lang/String;)I

    #@184
    move-result v30

    #@185
    .line 1690
    .local v30, usedNetworkType_t:I
    move-object/from16 v0, p0

    #@187
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@189
    move-object/from16 v31, v0

    #@18b
    aget-object v18, v31, v30

    #@18d
    .line 1691
    .local v18, network_t:Landroid/net/NetworkStateTracker;
    invoke-interface/range {v18 .. v18}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@190
    move-result-object v20

    #@191
    .line 1694
    .local v20, ni_t:Landroid/net/NetworkInfo;
    move-object/from16 v0, p0

    #@193
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@195
    move-object/from16 v31, v0

    #@197
    move-object/from16 v0, v31

    #@199
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_KR:Z

    #@19b
    move/from16 v31, v0

    #@19d
    const/16 v32, 0x1

    #@19f
    move/from16 v0, v31

    #@1a1
    move/from16 v1, v32

    #@1a3
    if-ne v0, v1, :cond_1e6

    #@1a5
    .line 1696
    sget-object v31, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->WHEN_DATA_DISALLOWED:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@1a7
    const-string v32, ""

    #@1a9
    move-object/from16 v0, v31

    #@1ab
    move-object/from16 v1, v32

    #@1ad
    move/from16 v2, v30

    #@1af
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I
    :try_end_1b2
    .catchall {:try_start_181 .. :try_end_1b2} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_181 .. :try_end_1b2} :catch_319

    #@1b2
    move-result v22

    #@1b3
    .line 1697
    .restart local v22       #ret:I
    const/16 v31, 0x2

    #@1b5
    move/from16 v0, v22

    #@1b7
    move/from16 v1, v31

    #@1b9
    if-ne v0, v1, :cond_1e6

    #@1bb
    .line 1698
    const/16 v22, 0x2

    #@1bd
    .line 2003
    .end local v22           #ret:I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1c0
    move-result-wide v31

    #@1c1
    sub-long v8, v31, v23

    #@1c3
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@1c5
    cmp-long v31, v8, v31

    #@1c7
    if-lez v31, :cond_b20

    #@1c9
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@1cb
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@1ce
    const-string v32, "startUsingNetworkFeature took too long: "

    #@1d0
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d3
    move-result-object v31

    #@1d4
    move-object/from16 v0, v31

    #@1d6
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v31

    #@1da
    const-string v32, "ms"

    #@1dc
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v31

    #@1e0
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v31

    #@1e4
    goto/16 :goto_bb

    #@1e6
    .line 1703
    .end local v8           #execTime:J
    :cond_1e6
    :try_start_1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1ea
    move-object/from16 v31, v0

    #@1ec
    move-object/from16 v0, v31

    #@1ee
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_MMS_ON_DATA_DISABLED_SKT:Z

    #@1f0
    move/from16 v31, v0

    #@1f2
    if-eqz v31, :cond_2f8

    #@1f4
    const-string v31, "enableMMS"

    #@1f6
    move-object/from16 v0, p2

    #@1f8
    move-object/from16 v1, v31

    #@1fa
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1fd
    move-result v31

    #@1fe
    if-eqz v31, :cond_2f8

    #@200
    .line 1706
    const-string v31, "ConnectivityService"

    #@202
    new-instance v32, Ljava/lang/StringBuilder;

    #@204
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@207
    const-string v33, "[LGE_DATA] just pass MMS when 3G is blocked, mode = "

    #@209
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v32

    #@20d
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->getMobileDataEnabled()Z

    #@210
    move-result v33

    #@211
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@214
    move-result-object v32

    #@215
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v32

    #@219
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21c
    .line 1749
    :cond_21c
    :goto_21c
    if-nez p1, :cond_2b4

    #@21e
    move-object/from16 v0, p0

    #@220
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@222
    move-object/from16 v31, v0

    #@224
    move-object/from16 v0, v31

    #@226
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_VZWAPP_CHECK_PERMISSION_VZW:Z

    #@228
    move/from16 v31, v0

    #@22a
    if-eqz v31, :cond_2b4

    #@22c
    .line 1750
    const-string v31, "enableHIPRI"

    #@22e
    move-object/from16 v0, p2

    #@230
    move-object/from16 v1, v31

    #@232
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@235
    move-result v31

    #@236
    if-nez v31, :cond_244

    #@238
    const-string v31, "enableVZWAPP"

    #@23a
    move-object/from16 v0, p2

    #@23c
    move-object/from16 v1, v31

    #@23e
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@241
    move-result v31

    #@242
    if-eqz v31, :cond_2b4

    #@244
    .line 1752
    :cond_244
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->isSystemImage()Z

    #@247
    move-result v14

    #@248
    .line 1753
    .local v14, mSystemImage:Z
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->isSignedFromVZW()Z

    #@24b
    move-result v13

    #@24c
    .line 1754
    .local v13, mSignedFromVZW:Z
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->isContainVzwAppApn_MetaTag()Z

    #@24f
    move-result v12

    #@250
    .line 1756
    .local v12, mContainVzwAppApn_MetaTag:Z
    const-string v31, "ConnectivityService"

    #@252
    new-instance v32, Ljava/lang/StringBuilder;

    #@254
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@257
    const-string v33, "mSystemImage : "

    #@259
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25c
    move-result-object v32

    #@25d
    move-object/from16 v0, v32

    #@25f
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@262
    move-result-object v32

    #@263
    const-string v33, " mSignedFromVZW : "

    #@265
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@268
    move-result-object v32

    #@269
    move-object/from16 v0, v32

    #@26b
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26e
    move-result-object v32

    #@26f
    const-string v33, " mContainVzwAppApn_MetaTag : "

    #@271
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v32

    #@275
    move-object/from16 v0, v32

    #@277
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v32

    #@27b
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27e
    move-result-object v32

    #@27f
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@282
    .line 1758
    if-nez v14, :cond_44c

    #@284
    const/16 v31, 0x1

    #@286
    move/from16 v0, v31

    #@288
    if-ne v13, v0, :cond_44c

    #@28a
    .line 1760
    const-string v31, "ConnectivityService"

    #@28c
    new-instance v32, Ljava/lang/StringBuilder;

    #@28e
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@291
    const-string v33, "feature set from "

    #@293
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@296
    move-result-object v32

    #@297
    move-object/from16 v0, v32

    #@299
    move-object/from16 v1, p2

    #@29b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29e
    move-result-object v32

    #@29f
    const-string v33, " to "

    #@2a1
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a4
    move-result-object v32

    #@2a5
    const-string v33, "enableVZWAPP"

    #@2a7
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2aa
    move-result-object v32

    #@2ab
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ae
    move-result-object v32

    #@2af
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b2
    .line 1761
    const-string p2, "enableVZWAPP"

    #@2b4
    .line 1782
    .end local v12           #mContainVzwAppApn_MetaTag:Z
    .end local v13           #mSignedFromVZW:Z
    .end local v14           #mSystemImage:Z
    :cond_2b4
    :goto_2b4
    new-instance v10, Lcom/android/server/ConnectivityService$FeatureUser;

    #@2b6
    move-object/from16 v0, p0

    #@2b8
    move/from16 v1, p1

    #@2ba
    move-object/from16 v2, p2

    #@2bc
    move-object/from16 v3, p3

    #@2be
    invoke-direct {v10, v0, v1, v2, v3}, Lcom/android/server/ConnectivityService$FeatureUser;-><init>(Lcom/android/server/ConnectivityService;ILjava/lang/String;Landroid/os/IBinder;)V

    #@2c1
    .line 1785
    .local v10, f:Lcom/android/server/ConnectivityService$FeatureUser;
    invoke-direct/range {p0 .. p2}, Lcom/android/server/ConnectivityService;->convertFeatureToNetworkType(ILjava/lang/String;)I

    #@2c4
    move-result v29

    #@2c5
    .line 1787
    .local v29, usedNetworkType:I
    move-object/from16 v0, p0

    #@2c7
    iget-boolean v0, v0, Lcom/android/server/ConnectivityService;->mLockdownEnabled:Z

    #@2c9
    move/from16 v31, v0
    :try_end_2cb
    .catchall {:try_start_1e6 .. :try_end_2cb} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_1e6 .. :try_end_2cb} :catch_319

    #@2cb
    if-eqz v31, :cond_4e6

    #@2cd
    .line 1790
    const/16 v22, 0x2

    #@2cf
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2d2
    move-result-wide v31

    #@2d3
    sub-long v8, v31, v23

    #@2d5
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@2d7
    cmp-long v31, v8, v31

    #@2d9
    if-lez v31, :cond_b3c

    #@2db
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@2dd
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@2e0
    const-string v32, "startUsingNetworkFeature took too long: "

    #@2e2
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e5
    move-result-object v31

    #@2e6
    move-object/from16 v0, v31

    #@2e8
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2eb
    move-result-object v31

    #@2ec
    const-string v32, "ms"

    #@2ee
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f1
    move-result-object v31

    #@2f2
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f5
    move-result-object v31

    #@2f6
    goto/16 :goto_bb

    #@2f8
    .line 1710
    .end local v8           #execTime:J
    .end local v10           #f:Lcom/android/server/ConnectivityService$FeatureUser;
    .end local v29           #usedNetworkType:I
    :cond_2f8
    :try_start_2f8
    move-object/from16 v0, p0

    #@2fa
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@2fc
    move-object/from16 v31, v0

    #@2fe
    move-object/from16 v0, v31

    #@300
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@302
    move/from16 v31, v0

    #@304
    if-eqz v31, :cond_348

    #@306
    const-string v31, "enableEMERGENCY"

    #@308
    move-object/from16 v0, p2

    #@30a
    move-object/from16 v1, v31

    #@30c
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@30f
    move-result v31

    #@310
    if-eqz v31, :cond_348

    #@312
    .line 1711
    const-string v31, "[EPDN] startUsingNetworkFeature: bypass emergency type for isavailable"

    #@314
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_317
    .catchall {:try_start_2f8 .. :try_end_317} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_2f8 .. :try_end_317} :catch_319

    #@317
    goto/16 :goto_21c

    #@319
    .line 1998
    .end local v18           #network_t:Landroid/net/NetworkStateTracker;
    .end local v20           #ni_t:Landroid/net/NetworkInfo;
    .end local v30           #usedNetworkType_t:I
    :catch_319
    move-exception v7

    #@31a
    .line 1999
    .local v7, e:Ljava/lang/Exception;
    :try_start_31a
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_31d
    .catchall {:try_start_31a .. :try_end_31d} :catchall_3b0

    #@31d
    .line 2000
    const/16 v22, -0x1

    #@31f
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@322
    move-result-wide v31

    #@323
    sub-long v8, v31, v23

    #@325
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@327
    cmp-long v31, v8, v31

    #@329
    if-lez v31, :cond_aac

    #@32b
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@32d
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@330
    const-string v32, "startUsingNetworkFeature took too long: "

    #@332
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@335
    move-result-object v31

    #@336
    move-object/from16 v0, v31

    #@338
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@33b
    move-result-object v31

    #@33c
    const-string v32, "ms"

    #@33e
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@341
    move-result-object v31

    #@342
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@345
    move-result-object v31

    #@346
    goto/16 :goto_bb

    #@348
    .line 1718
    .end local v7           #e:Ljava/lang/Exception;
    .end local v8           #execTime:J
    .restart local v18       #network_t:Landroid/net/NetworkStateTracker;
    .restart local v20       #ni_t:Landroid/net/NetworkInfo;
    .restart local v30       #usedNetworkType_t:I
    :cond_348
    :try_start_348
    new-instance v31, Ljava/lang/StringBuilder;

    #@34a
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@34d
    const-string v32, "usedNetworkType_t : "

    #@34f
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@352
    move-result-object v31

    #@353
    move-object/from16 v0, v31

    #@355
    move/from16 v1, v30

    #@357
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35a
    move-result-object v31

    #@35b
    const-string v32, "networkType : "

    #@35d
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@360
    move-result-object v31

    #@361
    move-object/from16 v0, v31

    #@363
    move/from16 v1, p1

    #@365
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@368
    move-result-object v31

    #@369
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36c
    move-result-object v31

    #@36d
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@370
    .line 1720
    move/from16 v0, v30

    #@372
    move/from16 v1, p1

    #@374
    if-eq v0, v1, :cond_21c

    #@376
    .line 1721
    if-eqz v20, :cond_21c

    #@378
    invoke-virtual/range {v20 .. v20}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@37b
    move-result v31

    #@37c
    if-nez v31, :cond_21c

    #@37e
    .line 1722
    const-string v31, "special network not available"

    #@380
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@383
    .line 1723
    const-string v31, "enableDUNAlways"

    #@385
    move-object/from16 v0, p2

    #@387
    move-object/from16 v1, v31

    #@389
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@38c
    move-result v31

    #@38d
    if-nez v31, :cond_407

    #@38f
    .line 1725
    move-object/from16 v0, p0

    #@391
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@393
    move-object/from16 v31, v0

    #@395
    move-object/from16 v0, v31

    #@397
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@399
    move/from16 v31, v0

    #@39b
    if-eqz v31, :cond_3dc

    #@39d
    const-string v31, "enableTETHERING"

    #@39f
    move-object/from16 v0, p2

    #@3a1
    move-object/from16 v1, v31

    #@3a3
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3a6
    move-result v31

    #@3a7
    if-eqz v31, :cond_3dc

    #@3a9
    .line 1728
    const-string v31, "[LGU_MPDN] bypass tethering type for isavailable"

    #@3ab
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_3ae
    .catchall {:try_start_348 .. :try_end_3ae} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_348 .. :try_end_3ae} :catch_319

    #@3ae
    goto/16 :goto_21c

    #@3b0
    .line 2002
    .end local v18           #network_t:Landroid/net/NetworkStateTracker;
    .end local v20           #ni_t:Landroid/net/NetworkInfo;
    .end local v30           #usedNetworkType_t:I
    :catchall_3b0
    move-exception v31

    #@3b1
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3b4
    move-result-wide v32

    #@3b5
    sub-long v8, v32, v23

    #@3b7
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v32, 0xfa

    #@3b9
    cmp-long v32, v8, v32

    #@3bb
    if-lez v32, :cond_a8c

    #@3bd
    .line 2005
    new-instance v32, Ljava/lang/StringBuilder;

    #@3bf
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@3c2
    const-string v33, "startUsingNetworkFeature took too long: "

    #@3c4
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c7
    move-result-object v32

    #@3c8
    move-object/from16 v0, v32

    #@3ca
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3cd
    move-result-object v32

    #@3ce
    const-string v33, "ms"

    #@3d0
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d3
    move-result-object v32

    #@3d4
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d7
    move-result-object v32

    #@3d8
    invoke-static/range {v32 .. v32}, Lcom/android/server/ConnectivityService;->loge(Ljava/lang/String;)V

    #@3db
    .line 2002
    :goto_3db
    throw v31

    #@3dc
    .line 1732
    .end local v8           #execTime:J
    .restart local v18       #network_t:Landroid/net/NetworkStateTracker;
    .restart local v20       #ni_t:Landroid/net/NetworkInfo;
    .restart local v30       #usedNetworkType_t:I
    :cond_3dc
    const/16 v22, 0x2

    #@3de
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3e1
    move-result-wide v31

    #@3e2
    sub-long v8, v31, v23

    #@3e4
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@3e6
    cmp-long v31, v8, v31

    #@3e8
    if-lez v31, :cond_c5e

    #@3ea
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@3ec
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@3ef
    const-string v32, "startUsingNetworkFeature took too long: "

    #@3f1
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f4
    move-result-object v31

    #@3f5
    move-object/from16 v0, v31

    #@3f7
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3fa
    move-result-object v31

    #@3fb
    const-string v32, "ms"

    #@3fd
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@400
    move-result-object v31

    #@401
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@404
    move-result-object v31

    #@405
    goto/16 :goto_bb

    #@407
    .line 1736
    .end local v8           #execTime:J
    :cond_407
    :try_start_407
    move-object/from16 v0, p0

    #@409
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@40b
    move-object/from16 v31, v0

    #@40d
    move-object/from16 v0, v31

    #@40f
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_APN_CHANGE_DCM:Z

    #@411
    move/from16 v31, v0

    #@413
    if-eqz v31, :cond_21c

    #@415
    const-string v31, "enableDUNAlways"

    #@417
    move-object/from16 v0, p2

    #@419
    move-object/from16 v1, v31

    #@41b
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_41e
    .catchall {:try_start_407 .. :try_end_41e} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_407 .. :try_end_41e} :catch_319

    #@41e
    move-result v31

    #@41f
    if-eqz v31, :cond_21c

    #@421
    .line 1738
    const/16 v22, 0x2

    #@423
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@426
    move-result-wide v31

    #@427
    sub-long v8, v31, v23

    #@429
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@42b
    cmp-long v31, v8, v31

    #@42d
    if-lez v31, :cond_c7b

    #@42f
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@431
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@434
    const-string v32, "startUsingNetworkFeature took too long: "

    #@436
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@439
    move-result-object v31

    #@43a
    move-object/from16 v0, v31

    #@43c
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@43f
    move-result-object v31

    #@440
    const-string v32, "ms"

    #@442
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@445
    move-result-object v31

    #@446
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@449
    move-result-object v31

    #@44a
    goto/16 :goto_bb

    #@44c
    .line 1763
    .end local v8           #execTime:J
    .restart local v12       #mContainVzwAppApn_MetaTag:Z
    .restart local v13       #mSignedFromVZW:Z
    .restart local v14       #mSystemImage:Z
    :cond_44c
    if-nez v14, :cond_47c

    #@44e
    if-nez v13, :cond_47c

    #@450
    .line 1765
    :try_start_450
    const-string v31, "ConnectivityService"

    #@452
    new-instance v32, Ljava/lang/StringBuilder;

    #@454
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@457
    const-string v33, "feature set from "

    #@459
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45c
    move-result-object v32

    #@45d
    move-object/from16 v0, v32

    #@45f
    move-object/from16 v1, p2

    #@461
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@464
    move-result-object v32

    #@465
    const-string v33, " to "

    #@467
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46a
    move-result-object v32

    #@46b
    const-string v33, "enableHIPRI"

    #@46d
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@470
    move-result-object v32

    #@471
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@474
    move-result-object v32

    #@475
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@478
    .line 1766
    const-string p2, "enableHIPRI"

    #@47a
    goto/16 :goto_2b4

    #@47c
    .line 1768
    :cond_47c
    const/16 v31, 0x1

    #@47e
    move/from16 v0, v31

    #@480
    if-ne v14, v0, :cond_4ba

    #@482
    const/16 v31, 0x1

    #@484
    move/from16 v0, v31

    #@486
    if-eq v13, v0, :cond_48e

    #@488
    const/16 v31, 0x1

    #@48a
    move/from16 v0, v31

    #@48c
    if-ne v12, v0, :cond_4ba

    #@48e
    .line 1770
    :cond_48e
    const-string v31, "ConnectivityService"

    #@490
    new-instance v32, Ljava/lang/StringBuilder;

    #@492
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@495
    const-string v33, "feature set from "

    #@497
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49a
    move-result-object v32

    #@49b
    move-object/from16 v0, v32

    #@49d
    move-object/from16 v1, p2

    #@49f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a2
    move-result-object v32

    #@4a3
    const-string v33, " to "

    #@4a5
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a8
    move-result-object v32

    #@4a9
    const-string v33, "enableVZWAPP"

    #@4ab
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ae
    move-result-object v32

    #@4af
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b2
    move-result-object v32

    #@4b3
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b6
    .line 1771
    const-string p2, "enableVZWAPP"

    #@4b8
    goto/16 :goto_2b4

    #@4ba
    .line 1775
    :cond_4ba
    const-string v31, "ConnectivityService"

    #@4bc
    new-instance v32, Ljava/lang/StringBuilder;

    #@4be
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@4c1
    const-string v33, "feature set from "

    #@4c3
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c6
    move-result-object v32

    #@4c7
    move-object/from16 v0, v32

    #@4c9
    move-object/from16 v1, p2

    #@4cb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ce
    move-result-object v32

    #@4cf
    const-string v33, " to "

    #@4d1
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d4
    move-result-object v32

    #@4d5
    const-string v33, "enableHIPRI"

    #@4d7
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4da
    move-result-object v32

    #@4db
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4de
    move-result-object v32

    #@4df
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e2
    .line 1776
    const-string p2, "enableHIPRI"

    #@4e4
    goto/16 :goto_2b4

    #@4e6
    .line 1793
    .end local v12           #mContainVzwAppApn_MetaTag:Z
    .end local v13           #mSignedFromVZW:Z
    .end local v14           #mSystemImage:Z
    .restart local v10       #f:Lcom/android/server/ConnectivityService$FeatureUser;
    .restart local v29       #usedNetworkType:I
    :cond_4e6
    move-object/from16 v0, p0

    #@4e8
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mProtectedNetworks:Ljava/util/List;

    #@4ea
    move-object/from16 v31, v0

    #@4ec
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ef
    move-result-object v32

    #@4f0
    invoke-interface/range {v31 .. v32}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@4f3
    move-result v31

    #@4f4
    if-eqz v31, :cond_4f9

    #@4f6
    .line 1794
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->enforceConnectivityInternalPermission()V

    #@4f9
    .line 1798
    :cond_4f9
    move-object/from16 v0, p0

    #@4fb
    move/from16 v1, v29

    #@4fd
    invoke-direct {v0, v1}, Lcom/android/server/ConnectivityService;->isNetworkMeteredUnchecked(I)Z

    #@500
    move-result v17

    #@501
    .line 1800
    .local v17, networkMetered:Z
    move-object/from16 v0, p0

    #@503
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mRulesLock:Ljava/lang/Object;

    #@505
    move-object/from16 v32, v0

    #@507
    monitor-enter v32
    :try_end_508
    .catchall {:try_start_450 .. :try_end_508} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_450 .. :try_end_508} :catch_319

    #@508
    .line 1801
    :try_start_508
    move-object/from16 v0, p0

    #@50a
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mUidRules:Landroid/util/SparseIntArray;

    #@50c
    move-object/from16 v31, v0

    #@50e
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@511
    move-result v33

    #@512
    const/16 v34, 0x0

    #@514
    move-object/from16 v0, v31

    #@516
    move/from16 v1, v33

    #@518
    move/from16 v2, v34

    #@51a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@51d
    move-result v28

    #@51e
    .line 1802
    .local v28, uidRules:I
    monitor-exit v32
    :try_end_51f
    .catchall {:try_start_508 .. :try_end_51f} :catchall_550

    #@51f
    .line 1803
    if-eqz v17, :cond_553

    #@521
    and-int/lit8 v31, v28, 0x1

    #@523
    if-eqz v31, :cond_553

    #@525
    .line 1804
    const/16 v22, 0x3

    #@527
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@52a
    move-result-wide v31

    #@52b
    sub-long v8, v31, v23

    #@52d
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@52f
    cmp-long v31, v8, v31

    #@531
    if-lez v31, :cond_b59

    #@533
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@535
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@538
    const-string v32, "startUsingNetworkFeature took too long: "

    #@53a
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53d
    move-result-object v31

    #@53e
    move-object/from16 v0, v31

    #@540
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@543
    move-result-object v31

    #@544
    const-string v32, "ms"

    #@546
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@549
    move-result-object v31

    #@54a
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54d
    move-result-object v31

    #@54e
    goto/16 :goto_bb

    #@550
    .line 1802
    .end local v8           #execTime:J
    .end local v28           #uidRules:I
    :catchall_550
    move-exception v31

    #@551
    :try_start_551
    monitor-exit v32
    :try_end_552
    .catchall {:try_start_551 .. :try_end_552} :catchall_550

    #@552
    :try_start_552
    throw v31

    #@553
    .line 1807
    .restart local v28       #uidRules:I
    :cond_553
    move-object/from16 v0, p0

    #@555
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@557
    move-object/from16 v31, v0

    #@559
    aget-object v16, v31, v29

    #@55b
    .line 1809
    .local v16, network:Landroid/net/NetworkStateTracker;
    move-object/from16 v0, p0

    #@55d
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@55f
    move-object/from16 v31, v0

    #@561
    move-object/from16 v0, v31

    #@563
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->imsstarttiming:Z

    #@565
    move/from16 v31, v0

    #@567
    if-eqz v31, :cond_61e

    #@569
    .line 1810
    const/16 v31, 0xb

    #@56b
    move/from16 v0, v29

    #@56d
    move/from16 v1, v31

    #@56f
    if-ne v0, v1, :cond_61e

    #@571
    .line 1812
    move-object/from16 v0, p0

    #@573
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetTrackers:[Landroid/net/NetworkStateTracker;

    #@575
    move-object/from16 v31, v0

    #@577
    const/16 v32, 0x0

    #@579
    aget-object v31, v31, v32

    #@57b
    invoke-interface/range {v31 .. v31}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@57e
    move-result-object v31

    #@57f
    invoke-virtual/range {v31 .. v31}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@582
    move-result-object v15

    #@583
    .line 1813
    .local v15, mobile_state:Landroid/net/NetworkInfo$State;
    const-string v31, "ConnectivityService"

    #@585
    new-instance v32, Ljava/lang/StringBuilder;

    #@587
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@58a
    const-string v33, "call StartUsnigNetworkFeature by IMS, mIsWifiConnected = "

    #@58c
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58f
    move-result-object v32

    #@590
    move-object/from16 v0, p0

    #@592
    iget-boolean v0, v0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@594
    move/from16 v33, v0

    #@596
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@599
    move-result-object v32

    #@59a
    const-string v33, ", mobile_state= "

    #@59c
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59f
    move-result-object v32

    #@5a0
    move-object/from16 v0, v32

    #@5a2
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a5
    move-result-object v32

    #@5a6
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a9
    move-result-object v32

    #@5aa
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5ad
    .line 1814
    move-object/from16 v0, p0

    #@5af
    iget-boolean v0, v0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@5b1
    move/from16 v31, v0

    #@5b3
    if-eqz v31, :cond_5bb

    #@5b5
    sget-object v31, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@5b7
    move-object/from16 v0, v31

    #@5b9
    if-eq v15, v0, :cond_61e

    #@5bb
    :cond_5bb
    move-object/from16 v0, p0

    #@5bd
    iget-boolean v0, v0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@5bf
    move/from16 v31, v0

    #@5c1
    if-nez v31, :cond_5c9

    #@5c3
    sget-object v31, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@5c5
    move-object/from16 v0, v31

    #@5c7
    if-eq v15, v0, :cond_61e

    #@5c9
    .line 1817
    :cond_5c9
    const-string v31, "ConnectivityService"

    #@5cb
    new-instance v32, Ljava/lang/StringBuilder;

    #@5cd
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@5d0
    const-string v33, "Can\'t call StartUsnigNetworkFeature because of, mIsWifiConnected = "

    #@5d2
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d5
    move-result-object v32

    #@5d6
    move-object/from16 v0, p0

    #@5d8
    iget-boolean v0, v0, Lcom/android/server/ConnectivityService;->mIsWifiConnected:Z

    #@5da
    move/from16 v33, v0

    #@5dc
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5df
    move-result-object v32

    #@5e0
    const-string v33, ", mobile_state= "

    #@5e2
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e5
    move-result-object v32

    #@5e6
    move-object/from16 v0, v32

    #@5e8
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5eb
    move-result-object v32

    #@5ec
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5ef
    move-result-object v32

    #@5f0
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5f3
    .catchall {:try_start_552 .. :try_end_5f3} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_552 .. :try_end_5f3} :catch_319

    #@5f3
    .line 1818
    const/16 v22, 0x3

    #@5f5
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5f8
    move-result-wide v31

    #@5f9
    sub-long v8, v31, v23

    #@5fb
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@5fd
    cmp-long v31, v8, v31

    #@5ff
    if-lez v31, :cond_b76

    #@601
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@603
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@606
    const-string v32, "startUsingNetworkFeature took too long: "

    #@608
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60b
    move-result-object v31

    #@60c
    move-object/from16 v0, v31

    #@60e
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@611
    move-result-object v31

    #@612
    const-string v32, "ms"

    #@614
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@617
    move-result-object v31

    #@618
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61b
    move-result-object v31

    #@61c
    goto/16 :goto_bb

    #@61e
    .line 1823
    .end local v8           #execTime:J
    .end local v15           #mobile_state:Landroid/net/NetworkInfo$State;
    :cond_61e
    if-eqz v16, :cond_a61

    #@620
    .line 1824
    :try_start_620
    new-instance v5, Ljava/lang/Integer;

    #@622
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingPid()I

    #@625
    move-result v31

    #@626
    move/from16 v0, v31

    #@628
    invoke-direct {v5, v0}, Ljava/lang/Integer;-><init>(I)V

    #@62b
    .line 1825
    .local v5, currentPid:Ljava/lang/Integer;
    move/from16 v0, v29

    #@62d
    move/from16 v1, p1

    #@62f
    if-eq v0, v1, :cond_9ed

    #@631
    .line 1826
    invoke-interface/range {v16 .. v16}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@634
    move-result-object v19

    #@635
    .line 1828
    .local v19, ni:Landroid/net/NetworkInfo;
    invoke-virtual/range {v19 .. v19}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@638
    move-result v31

    #@639
    if-nez v31, :cond_680

    #@63b
    .line 1829
    const-string v31, "enableDUNAlways"

    #@63d
    move-object/from16 v0, p2

    #@63f
    move-object/from16 v1, v31

    #@641
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@644
    move-result v31

    #@645
    if-nez v31, :cond_7df

    #@647
    .line 1830
    new-instance v31, Ljava/lang/StringBuilder;

    #@649
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@64c
    const-string v32, "special network not available ni="

    #@64e
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@651
    move-result-object v31

    #@652
    invoke-virtual/range {v19 .. v19}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@655
    move-result-object v32

    #@656
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@659
    move-result-object v31

    #@65a
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65d
    move-result-object v31

    #@65e
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@661
    .line 1832
    move-object/from16 v0, p0

    #@663
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@665
    move-object/from16 v31, v0

    #@667
    move-object/from16 v0, v31

    #@669
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@66b
    move/from16 v31, v0

    #@66d
    if-eqz v31, :cond_793

    #@66f
    const-string v31, "enableTETHERING"

    #@671
    move-object/from16 v0, p2

    #@673
    move-object/from16 v1, v31

    #@675
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@678
    move-result v31

    #@679
    if-eqz v31, :cond_793

    #@67b
    .line 1834
    const-string v31, "[LGU_MPDN] bypass tethering type for isavailable"

    #@67d
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@680
    .line 1854
    :cond_680
    :goto_680
    move-object/from16 v0, p0

    #@682
    move/from16 v1, v29

    #@684
    invoke-direct {v0, v1}, Lcom/android/server/ConnectivityService;->getRestoreDefaultNetworkDelay(I)I

    #@687
    move-result v21

    #@688
    .line 1856
    .local v21, restoreTimer:I
    monitor-enter p0
    :try_end_689
    .catchall {:try_start_620 .. :try_end_689} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_620 .. :try_end_689} :catch_319

    #@689
    .line 1857
    const/4 v4, 0x1

    #@68a
    .line 1858
    .local v4, addToList:Z
    if-gez v21, :cond_6ab

    #@68c
    .line 1861
    :try_start_68c
    move-object/from16 v0, p0

    #@68e
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@690
    move-object/from16 v31, v0

    #@692
    invoke-interface/range {v31 .. v31}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@695
    move-result-object v11

    #@696
    .local v11, i$:Ljava/util/Iterator;
    :cond_696
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@699
    move-result v31

    #@69a
    if-eqz v31, :cond_6ab

    #@69c
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@69f
    move-result-object v27

    #@6a0
    check-cast v27, Lcom/android/server/ConnectivityService$FeatureUser;

    #@6a2
    .line 1862
    .local v27, u:Lcom/android/server/ConnectivityService$FeatureUser;
    move-object/from16 v0, v27

    #@6a4
    invoke-virtual {v0, v10}, Lcom/android/server/ConnectivityService$FeatureUser;->isSameUser(Lcom/android/server/ConnectivityService$FeatureUser;)Z

    #@6a7
    move-result v31

    #@6a8
    if-eqz v31, :cond_696

    #@6aa
    .line 1864
    const/4 v4, 0x0

    #@6ab
    .line 1870
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v27           #u:Lcom/android/server/ConnectivityService$FeatureUser;
    :cond_6ab
    if-eqz v4, :cond_6b8

    #@6ad
    move-object/from16 v0, p0

    #@6af
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@6b1
    move-object/from16 v31, v0

    #@6b3
    move-object/from16 v0, v31

    #@6b5
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@6b8
    .line 1871
    :cond_6b8
    move-object/from16 v0, p0

    #@6ba
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@6bc
    move-object/from16 v31, v0

    #@6be
    aget-object v31, v31, v29

    #@6c0
    move-object/from16 v0, v31

    #@6c2
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@6c5
    move-result v31

    #@6c6
    if-nez v31, :cond_6ff

    #@6c8
    .line 1873
    move-object/from16 v0, p0

    #@6ca
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@6cc
    move-object/from16 v31, v0

    #@6ce
    aget-object v31, v31, v29

    #@6d0
    move-object/from16 v0, v31

    #@6d2
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@6d5
    .line 1876
    move-object/from16 v0, p0

    #@6d7
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@6d9
    move-object/from16 v31, v0

    #@6db
    move-object/from16 v0, v31

    #@6dd
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@6df
    move/from16 v31, v0

    #@6e1
    if-nez v31, :cond_6f1

    #@6e3
    move-object/from16 v0, p0

    #@6e5
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@6e7
    move-object/from16 v31, v0

    #@6e9
    move-object/from16 v0, v31

    #@6eb
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

    #@6ed
    move/from16 v31, v0

    #@6ef
    if-eqz v31, :cond_6ff

    #@6f1
    .line 1878
    :cond_6f1
    move-object/from16 v0, p0

    #@6f3
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetRequestAll:Ljava/util/List;

    #@6f5
    move-object/from16 v31, v0

    #@6f7
    move-object/from16 v0, v31

    #@6f9
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@6fc
    .line 1879
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->setPidlist()V

    #@6ff
    .line 1883
    :cond_6ff
    monitor-exit p0
    :try_end_700
    .catchall {:try_start_68c .. :try_end_700} :catchall_7fb

    #@700
    .line 1885
    if-ltz v21, :cond_737

    #@702
    .line 1887
    :try_start_702
    move-object/from16 v0, p0

    #@704
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@706
    move-object/from16 v31, v0

    #@708
    move-object/from16 v0, v31

    #@70a
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_HIPRI_TYPE_TIMER_UPLUS:Z

    #@70c
    move/from16 v31, v0

    #@70e
    if-eqz v31, :cond_823

    #@710
    .line 1888
    new-instance v31, Ljava/lang/StringBuilder;

    #@712
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@715
    const-string v32, "restoreTimer = "

    #@717
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71a
    move-result-object v31

    #@71b
    move-object/from16 v0, v31

    #@71d
    move/from16 v1, v21

    #@71f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@722
    move-result-object v31

    #@723
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@726
    move-result-object v31

    #@727
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@72a
    .line 1889
    const/16 v31, 0x5

    #@72c
    move/from16 v0, v29

    #@72e
    move/from16 v1, v31

    #@730
    if-ne v0, v1, :cond_7fe

    #@732
    .line 1891
    const-string v31, "if TYPE_MOBILE_HIPRI , 60sec timer is not set"

    #@734
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@737
    .line 1943
    :cond_737
    :goto_737
    invoke-virtual/range {v19 .. v19}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    #@73a
    move-result v31

    #@73b
    const/16 v32, 0x1

    #@73d
    move/from16 v0, v31

    #@73f
    move/from16 v1, v32

    #@741
    if-ne v0, v1, :cond_964

    #@743
    invoke-interface/range {v16 .. v16}, Landroid/net/NetworkStateTracker;->isTeardownRequested()Z

    #@746
    move-result v31

    #@747
    if-nez v31, :cond_964

    #@749
    .line 1945
    invoke-virtual/range {v19 .. v19}, Landroid/net/NetworkInfo;->isConnected()Z

    #@74c
    move-result v31

    #@74d
    const/16 v32, 0x1

    #@74f
    move/from16 v0, v31

    #@751
    move/from16 v1, v32

    #@753
    if-ne v0, v1, :cond_934

    #@755
    .line 1946
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_758
    .catchall {:try_start_702 .. :try_end_758} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_702 .. :try_end_758} :catch_319

    #@758
    move-result-wide v25

    #@759
    .line 1949
    .local v25, token:J
    :try_start_759
    move-object/from16 v0, p0

    #@75b
    move/from16 v1, v29

    #@75d
    invoke-direct {v0, v1}, Lcom/android/server/ConnectivityService;->handleDnsConfigurationChange(I)V

    #@760
    .line 1950
    const-string v31, "special network already active"

    #@762
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_765
    .catchall {:try_start_759 .. :try_end_765} :catchall_92f

    #@765
    .line 1952
    :try_start_765
    invoke-static/range {v25 .. v26}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_768
    .catchall {:try_start_765 .. :try_end_768} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_765 .. :try_end_768} :catch_319

    #@768
    .line 1954
    const/16 v22, 0x0

    #@76a
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@76d
    move-result-wide v31

    #@76e
    sub-long v8, v31, v23

    #@770
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@772
    cmp-long v31, v8, v31

    #@774
    if-lez v31, :cond_b93

    #@776
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@778
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@77b
    const-string v32, "startUsingNetworkFeature took too long: "

    #@77d
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@780
    move-result-object v31

    #@781
    move-object/from16 v0, v31

    #@783
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@786
    move-result-object v31

    #@787
    const-string v32, "ms"

    #@789
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78c
    move-result-object v31

    #@78d
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@790
    move-result-object v31

    #@791
    goto/16 :goto_bb

    #@793
    .line 1838
    .end local v4           #addToList:Z
    .end local v8           #execTime:J
    .end local v21           #restoreTimer:I
    .end local v25           #token:J
    :cond_793
    :try_start_793
    move-object/from16 v0, p0

    #@795
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@797
    move-object/from16 v31, v0

    #@799
    move-object/from16 v0, v31

    #@79b
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->EMERGENCY_SUPPORT:Z

    #@79d
    move/from16 v31, v0

    #@79f
    if-eqz v31, :cond_7b4

    #@7a1
    const-string v31, "enableEMERGENCY"

    #@7a3
    move-object/from16 v0, p2

    #@7a5
    move-object/from16 v1, v31

    #@7a7
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@7aa
    move-result v31

    #@7ab
    if-eqz v31, :cond_7b4

    #@7ad
    .line 1839
    const-string v31, "[EPDN] startUsingNetworkFeature: bypass emergency type for isavailable"

    #@7af
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_7b2
    .catchall {:try_start_793 .. :try_end_7b2} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_793 .. :try_end_7b2} :catch_319

    #@7b2
    goto/16 :goto_680

    #@7b4
    .line 1843
    :cond_7b4
    const/16 v22, 0x2

    #@7b6
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@7b9
    move-result-wide v31

    #@7ba
    sub-long v8, v31, v23

    #@7bc
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@7be
    cmp-long v31, v8, v31

    #@7c0
    if-lez v31, :cond_c07

    #@7c2
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@7c4
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@7c7
    const-string v32, "startUsingNetworkFeature took too long: "

    #@7c9
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7cc
    move-result-object v31

    #@7cd
    move-object/from16 v0, v31

    #@7cf
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@7d2
    move-result-object v31

    #@7d3
    const-string v32, "ms"

    #@7d5
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d8
    move-result-object v31

    #@7d9
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7dc
    move-result-object v31

    #@7dd
    goto/16 :goto_bb

    #@7df
    .line 1848
    .end local v8           #execTime:J
    :cond_7df
    :try_start_7df
    new-instance v31, Ljava/lang/StringBuilder;

    #@7e1
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@7e4
    const-string v32, "special network not available, but try anyway ni="

    #@7e6
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e9
    move-result-object v31

    #@7ea
    invoke-virtual/range {v19 .. v19}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@7ed
    move-result-object v32

    #@7ee
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f1
    move-result-object v31

    #@7f2
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f5
    move-result-object v31

    #@7f6
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_7f9
    .catchall {:try_start_7df .. :try_end_7f9} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_7df .. :try_end_7f9} :catch_319

    #@7f9
    goto/16 :goto_680

    #@7fb
    .line 1883
    .restart local v4       #addToList:Z
    .restart local v21       #restoreTimer:I
    :catchall_7fb
    move-exception v31

    #@7fc
    :try_start_7fc
    monitor-exit p0
    :try_end_7fd
    .catchall {:try_start_7fc .. :try_end_7fd} :catchall_7fb

    #@7fd
    :try_start_7fd
    throw v31

    #@7fe
    .line 1900
    :cond_7fe
    const-string v31, "60sec timer is set!!!"

    #@800
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@803
    .line 1901
    move-object/from16 v0, p0

    #@805
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@807
    move-object/from16 v31, v0

    #@809
    move-object/from16 v0, p0

    #@80b
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@80d
    move-object/from16 v32, v0

    #@80f
    const/16 v33, 0x1

    #@811
    move-object/from16 v0, v32

    #@813
    move/from16 v1, v33

    #@815
    invoke-virtual {v0, v1, v10}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@818
    move-result-object v32

    #@819
    move/from16 v0, v21

    #@81b
    int-to-long v0, v0

    #@81c
    move-wide/from16 v33, v0

    #@81e
    invoke-virtual/range {v31 .. v34}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@821
    goto/16 :goto_737

    #@823
    .line 1905
    :cond_823
    const-string v31, "ro.afwdata.LGfeatureset"

    #@825
    const-string v32, "none"

    #@827
    invoke-static/range {v31 .. v32}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@82a
    move-result-object v31

    #@82b
    const-string v32, "SKTBASE"

    #@82d
    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@830
    move-result v31

    #@831
    if-eqz v31, :cond_881

    #@833
    .line 1907
    new-instance v31, Ljava/lang/StringBuilder;

    #@835
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@838
    const-string v32, "restoreTimer = "

    #@83a
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83d
    move-result-object v31

    #@83e
    move-object/from16 v0, v31

    #@840
    move/from16 v1, v21

    #@842
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@845
    move-result-object v31

    #@846
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@849
    move-result-object v31

    #@84a
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@84d
    .line 1909
    const/16 v31, 0x4

    #@84f
    move/from16 v0, v29

    #@851
    move/from16 v1, v31

    #@853
    if-ne v0, v1, :cond_85c

    #@855
    .line 1911
    const-string v31, "if TYPE_MOBILE_DUN , 60sec timer is not set"

    #@857
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@85a
    goto/16 :goto_737

    #@85c
    .line 1915
    :cond_85c
    const-string v31, "60sec timer is set!!!"

    #@85e
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@861
    .line 1916
    move-object/from16 v0, p0

    #@863
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@865
    move-object/from16 v31, v0

    #@867
    move-object/from16 v0, p0

    #@869
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@86b
    move-object/from16 v32, v0

    #@86d
    const/16 v33, 0x1

    #@86f
    move-object/from16 v0, v32

    #@871
    move/from16 v1, v33

    #@873
    invoke-virtual {v0, v1, v10}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@876
    move-result-object v32

    #@877
    move/from16 v0, v21

    #@879
    int-to-long v0, v0

    #@87a
    move-wide/from16 v33, v0

    #@87c
    invoke-virtual/range {v31 .. v34}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@87f
    goto/16 :goto_737

    #@881
    .line 1921
    :cond_881
    move-object/from16 v0, p0

    #@883
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@885
    move-object/from16 v31, v0

    #@887
    move-object/from16 v0, v31

    #@889
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_VZW_APN_RESTORE_TIME_SET_VZW:Z

    #@88b
    move/from16 v31, v0

    #@88d
    if-eqz v31, :cond_90f

    #@88f
    .line 1923
    const/16 v31, 0xf

    #@891
    move/from16 v0, v29

    #@893
    move/from16 v1, v31

    #@895
    if-ne v0, v1, :cond_8d5

    #@897
    .line 1925
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->getRestoreVZWAPPNetworkDelay()I

    #@89a
    move-result v21

    #@89b
    .line 1926
    new-instance v31, Ljava/lang/StringBuilder;

    #@89d
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@8a0
    const-string v32, "if TYPE_MOBILE_VZWAPP , use VZW property. RestoreTimer = "

    #@8a2
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a5
    move-result-object v31

    #@8a6
    move-object/from16 v0, v31

    #@8a8
    move/from16 v1, v21

    #@8aa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8ad
    move-result-object v31

    #@8ae
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b1
    move-result-object v31

    #@8b2
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@8b5
    .line 1927
    move-object/from16 v0, p0

    #@8b7
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@8b9
    move-object/from16 v31, v0

    #@8bb
    move-object/from16 v0, p0

    #@8bd
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@8bf
    move-object/from16 v32, v0

    #@8c1
    const/16 v33, 0x1

    #@8c3
    move-object/from16 v0, v32

    #@8c5
    move/from16 v1, v33

    #@8c7
    invoke-virtual {v0, v1, v10}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@8ca
    move-result-object v32

    #@8cb
    move/from16 v0, v21

    #@8cd
    int-to-long v0, v0

    #@8ce
    move-wide/from16 v33, v0

    #@8d0
    invoke-virtual/range {v31 .. v34}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@8d3
    goto/16 :goto_737

    #@8d5
    .line 1931
    :cond_8d5
    new-instance v31, Ljava/lang/StringBuilder;

    #@8d7
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@8da
    const-string v32, "RestoreTimer = "

    #@8dc
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8df
    move-result-object v31

    #@8e0
    move-object/from16 v0, v31

    #@8e2
    move/from16 v1, v21

    #@8e4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e7
    move-result-object v31

    #@8e8
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8eb
    move-result-object v31

    #@8ec
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@8ef
    .line 1932
    move-object/from16 v0, p0

    #@8f1
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@8f3
    move-object/from16 v31, v0

    #@8f5
    move-object/from16 v0, p0

    #@8f7
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@8f9
    move-object/from16 v32, v0

    #@8fb
    const/16 v33, 0x1

    #@8fd
    move-object/from16 v0, v32

    #@8ff
    move/from16 v1, v33

    #@901
    invoke-virtual {v0, v1, v10}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@904
    move-result-object v32

    #@905
    move/from16 v0, v21

    #@907
    int-to-long v0, v0

    #@908
    move-wide/from16 v33, v0

    #@90a
    invoke-virtual/range {v31 .. v34}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@90d
    goto/16 :goto_737

    #@90f
    .line 1938
    :cond_90f
    move-object/from16 v0, p0

    #@911
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@913
    move-object/from16 v31, v0

    #@915
    move-object/from16 v0, p0

    #@917
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@919
    move-object/from16 v32, v0

    #@91b
    const/16 v33, 0x1

    #@91d
    move-object/from16 v0, v32

    #@91f
    move/from16 v1, v33

    #@921
    invoke-virtual {v0, v1, v10}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@924
    move-result-object v32

    #@925
    move/from16 v0, v21

    #@927
    int-to-long v0, v0

    #@928
    move-wide/from16 v33, v0

    #@92a
    invoke-virtual/range {v31 .. v34}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@92d
    goto/16 :goto_737

    #@92f
    .line 1952
    .restart local v25       #token:J
    :catchall_92f
    move-exception v31

    #@930
    invoke-static/range {v25 .. v26}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@933
    throw v31

    #@934
    .line 1956
    .end local v25           #token:J
    :cond_934
    const-string v31, "special network already connecting"

    #@936
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_939
    .catchall {:try_start_7fd .. :try_end_939} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_7fd .. :try_end_939} :catch_319

    #@939
    .line 1957
    const/16 v22, 0x1

    #@93b
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@93e
    move-result-wide v31

    #@93f
    sub-long v8, v31, v23

    #@941
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@943
    cmp-long v31, v8, v31

    #@945
    if-lez v31, :cond_bb0

    #@947
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@949
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@94c
    const-string v32, "startUsingNetworkFeature took too long: "

    #@94e
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@951
    move-result-object v31

    #@952
    move-object/from16 v0, v31

    #@954
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@957
    move-result-object v31

    #@958
    const-string v32, "ms"

    #@95a
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95d
    move-result-object v31

    #@95e
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@961
    move-result-object v31

    #@962
    goto/16 :goto_bb

    #@964
    .line 1964
    .end local v8           #execTime:J
    :cond_964
    :try_start_964
    new-instance v31, Ljava/lang/StringBuilder;

    #@966
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@969
    const-string v32, "startUsingNetworkFeature reconnecting to "

    #@96b
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96e
    move-result-object v31

    #@96f
    move-object/from16 v0, v31

    #@971
    move/from16 v1, p1

    #@973
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@976
    move-result-object v31

    #@977
    const-string v32, ": "

    #@979
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97c
    move-result-object v31

    #@97d
    move-object/from16 v0, v31

    #@97f
    move-object/from16 v1, p2

    #@981
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@984
    move-result-object v31

    #@985
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@988
    move-result-object v31

    #@989
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@98c
    .line 1968
    if-nez p1, :cond_9bf

    #@98e
    .line 1969
    invoke-interface/range {v16 .. v16}, Landroid/net/NetworkStateTracker;->reconnect()Z
    :try_end_991
    .catchall {:try_start_964 .. :try_end_991} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_964 .. :try_end_991} :catch_319

    #@991
    move-result v31

    #@992
    if-nez v31, :cond_9c2

    #@994
    .line 1970
    const/16 v22, 0x3

    #@996
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@999
    move-result-wide v31

    #@99a
    sub-long v8, v31, v23

    #@99c
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@99e
    cmp-long v31, v8, v31

    #@9a0
    if-lez v31, :cond_bcd

    #@9a2
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@9a4
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@9a7
    const-string v32, "startUsingNetworkFeature took too long: "

    #@9a9
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ac
    move-result-object v31

    #@9ad
    move-object/from16 v0, v31

    #@9af
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9b2
    move-result-object v31

    #@9b3
    const-string v32, "ms"

    #@9b5
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b8
    move-result-object v31

    #@9b9
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9bc
    move-result-object v31

    #@9bd
    goto/16 :goto_bb

    #@9bf
    .line 1973
    .end local v8           #execTime:J
    :cond_9bf
    :try_start_9bf
    invoke-interface/range {v16 .. v16}, Landroid/net/NetworkStateTracker;->reconnect()Z
    :try_end_9c2
    .catchall {:try_start_9bf .. :try_end_9c2} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_9bf .. :try_end_9c2} :catch_319

    #@9c2
    .line 1976
    :cond_9c2
    const/16 v22, 0x1

    #@9c4
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@9c7
    move-result-wide v31

    #@9c8
    sub-long v8, v31, v23

    #@9ca
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@9cc
    cmp-long v31, v8, v31

    #@9ce
    if-lez v31, :cond_bea

    #@9d0
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@9d2
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@9d5
    const-string v32, "startUsingNetworkFeature took too long: "

    #@9d7
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9da
    move-result-object v31

    #@9db
    move-object/from16 v0, v31

    #@9dd
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9e0
    move-result-object v31

    #@9e1
    const-string v32, "ms"

    #@9e3
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e6
    move-result-object v31

    #@9e7
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9ea
    move-result-object v31

    #@9eb
    goto/16 :goto_bb

    #@9ed
    .line 1979
    .end local v4           #addToList:Z
    .end local v8           #execTime:J
    .end local v19           #ni:Landroid/net/NetworkInfo;
    .end local v21           #restoreTimer:I
    :cond_9ed
    :try_start_9ed
    monitor-enter p0
    :try_end_9ee
    .catchall {:try_start_9ed .. :try_end_9ee} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_9ed .. :try_end_9ee} :catch_319

    #@9ee
    .line 1980
    :try_start_9ee
    move-object/from16 v0, p0

    #@9f0
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@9f2
    move-object/from16 v31, v0

    #@9f4
    move-object/from16 v0, v31

    #@9f6
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@9f9
    .line 1981
    move-object/from16 v0, p0

    #@9fb
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@9fd
    move-object/from16 v31, v0

    #@9ff
    aget-object v31, v31, v29

    #@a01
    move-object/from16 v0, v31

    #@a03
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@a06
    move-result v31

    #@a07
    if-nez v31, :cond_a32

    #@a09
    .line 1983
    move-object/from16 v0, p0

    #@a0b
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetRequestersPids:[Ljava/util/List;

    #@a0d
    move-object/from16 v31, v0

    #@a0f
    aget-object v31, v31, v29

    #@a11
    move-object/from16 v0, v31

    #@a13
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a16
    .line 1986
    move-object/from16 v0, p0

    #@a18
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@a1a
    move-object/from16 v31, v0

    #@a1c
    move-object/from16 v0, v31

    #@a1e
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_DNS_SYSPROP_ENHANCE:Z

    #@a20
    move/from16 v31, v0

    #@a22
    if-eqz v31, :cond_a32

    #@a24
    .line 1988
    move-object/from16 v0, p0

    #@a26
    iget-object v0, v0, Lcom/android/server/ConnectivityService;->mNetRequestAll:Ljava/util/List;

    #@a28
    move-object/from16 v31, v0

    #@a2a
    move-object/from16 v0, v31

    #@a2c
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a2f
    .line 1989
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ConnectivityService;->setPidlist()V

    #@a32
    .line 1993
    :cond_a32
    monitor-exit p0
    :try_end_a33
    .catchall {:try_start_9ee .. :try_end_a33} :catchall_a5e

    #@a33
    .line 1994
    const/16 v22, -0x1

    #@a35
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a38
    move-result-wide v31

    #@a39
    sub-long v8, v31, v23

    #@a3b
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@a3d
    cmp-long v31, v8, v31

    #@a3f
    if-lez v31, :cond_c24

    #@a41
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@a43
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@a46
    const-string v32, "startUsingNetworkFeature took too long: "

    #@a48
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4b
    move-result-object v31

    #@a4c
    move-object/from16 v0, v31

    #@a4e
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a51
    move-result-object v31

    #@a52
    const-string v32, "ms"

    #@a54
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a57
    move-result-object v31

    #@a58
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5b
    move-result-object v31

    #@a5c
    goto/16 :goto_bb

    #@a5e
    .line 1993
    .end local v8           #execTime:J
    :catchall_a5e
    move-exception v31

    #@a5f
    :try_start_a5f
    monitor-exit p0
    :try_end_a60
    .catchall {:try_start_a5f .. :try_end_a60} :catchall_a5e

    #@a60
    :try_start_a60
    throw v31
    :try_end_a61
    .catchall {:try_start_a60 .. :try_end_a61} :catchall_3b0
    .catch Ljava/lang/Exception; {:try_start_a60 .. :try_end_a61} :catch_319

    #@a61
    .line 1997
    .end local v5           #currentPid:Ljava/lang/Integer;
    :cond_a61
    const/16 v22, 0x2

    #@a63
    .line 2003
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a66
    move-result-wide v31

    #@a67
    sub-long v8, v31, v23

    #@a69
    .line 2004
    .restart local v8       #execTime:J
    const-wide/16 v31, 0xfa

    #@a6b
    cmp-long v31, v8, v31

    #@a6d
    if-lez v31, :cond_c41

    #@a6f
    .line 2005
    new-instance v31, Ljava/lang/StringBuilder;

    #@a71
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@a74
    const-string v32, "startUsingNetworkFeature took too long: "

    #@a76
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a79
    move-result-object v31

    #@a7a
    move-object/from16 v0, v31

    #@a7c
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a7f
    move-result-object v31

    #@a80
    const-string v32, "ms"

    #@a82
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a85
    move-result-object v31

    #@a86
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a89
    move-result-object v31

    #@a8a
    goto/16 :goto_bb

    #@a8c
    .line 2007
    .end local v10           #f:Lcom/android/server/ConnectivityService$FeatureUser;
    .end local v16           #network:Landroid/net/NetworkStateTracker;
    .end local v17           #networkMetered:Z
    .end local v18           #network_t:Landroid/net/NetworkStateTracker;
    .end local v20           #ni_t:Landroid/net/NetworkInfo;
    .end local v28           #uidRules:I
    .end local v29           #usedNetworkType:I
    .end local v30           #usedNetworkType_t:I
    :cond_a8c
    new-instance v32, Ljava/lang/StringBuilder;

    #@a8e
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@a91
    const-string v33, "startUsingNetworkFeature took "

    #@a93
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a96
    move-result-object v32

    #@a97
    move-object/from16 v0, v32

    #@a99
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a9c
    move-result-object v32

    #@a9d
    const-string v33, "ms"

    #@a9f
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa2
    move-result-object v32

    #@aa3
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa6
    move-result-object v32

    #@aa7
    invoke-static/range {v32 .. v32}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@aaa
    goto/16 :goto_3db

    #@aac
    .restart local v7       #e:Ljava/lang/Exception;
    :cond_aac
    new-instance v31, Ljava/lang/StringBuilder;

    #@aae
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@ab1
    const-string v32, "startUsingNetworkFeature took "

    #@ab3
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab6
    move-result-object v31

    #@ab7
    move-object/from16 v0, v31

    #@ab9
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@abc
    move-result-object v31

    #@abd
    const-string v32, "ms"

    #@abf
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac2
    move-result-object v31

    #@ac3
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac6
    move-result-object v31

    #@ac7
    .end local v7           #e:Ljava/lang/Exception;
    :goto_ac7
    invoke-static/range {v31 .. v31}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@aca
    goto/16 :goto_7e

    #@acc
    :cond_acc
    new-instance v31, Ljava/lang/StringBuilder;

    #@ace
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@ad1
    const-string v32, "startUsingNetworkFeature took "

    #@ad3
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad6
    move-result-object v31

    #@ad7
    move-object/from16 v0, v31

    #@ad9
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@adc
    move-result-object v31

    #@add
    const-string v32, "ms"

    #@adf
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae2
    move-result-object v31

    #@ae3
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae6
    move-result-object v31

    #@ae7
    goto :goto_ac7

    #@ae8
    .restart local v22       #ret:I
    :cond_ae8
    new-instance v31, Ljava/lang/StringBuilder;

    #@aea
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@aed
    const-string v32, "startUsingNetworkFeature took "

    #@aef
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af2
    move-result-object v31

    #@af3
    move-object/from16 v0, v31

    #@af5
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@af8
    move-result-object v31

    #@af9
    const-string v32, "ms"

    #@afb
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@afe
    move-result-object v31

    #@aff
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b02
    move-result-object v31

    #@b03
    goto :goto_ac7

    #@b04
    .end local v22           #ret:I
    :cond_b04
    new-instance v31, Ljava/lang/StringBuilder;

    #@b06
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@b09
    const-string v32, "startUsingNetworkFeature took "

    #@b0b
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0e
    move-result-object v31

    #@b0f
    move-object/from16 v0, v31

    #@b11
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b14
    move-result-object v31

    #@b15
    const-string v32, "ms"

    #@b17
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1a
    move-result-object v31

    #@b1b
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1e
    move-result-object v31

    #@b1f
    goto :goto_ac7

    #@b20
    .restart local v18       #network_t:Landroid/net/NetworkStateTracker;
    .restart local v20       #ni_t:Landroid/net/NetworkInfo;
    .restart local v30       #usedNetworkType_t:I
    :cond_b20
    new-instance v31, Ljava/lang/StringBuilder;

    #@b22
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@b25
    const-string v32, "startUsingNetworkFeature took "

    #@b27
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2a
    move-result-object v31

    #@b2b
    move-object/from16 v0, v31

    #@b2d
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b30
    move-result-object v31

    #@b31
    const-string v32, "ms"

    #@b33
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b36
    move-result-object v31

    #@b37
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3a
    move-result-object v31

    #@b3b
    goto :goto_ac7

    #@b3c
    .restart local v10       #f:Lcom/android/server/ConnectivityService$FeatureUser;
    .restart local v29       #usedNetworkType:I
    :cond_b3c
    new-instance v31, Ljava/lang/StringBuilder;

    #@b3e
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@b41
    const-string v32, "startUsingNetworkFeature took "

    #@b43
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b46
    move-result-object v31

    #@b47
    move-object/from16 v0, v31

    #@b49
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b4c
    move-result-object v31

    #@b4d
    const-string v32, "ms"

    #@b4f
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b52
    move-result-object v31

    #@b53
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b56
    move-result-object v31

    #@b57
    goto/16 :goto_ac7

    #@b59
    .restart local v17       #networkMetered:Z
    .restart local v28       #uidRules:I
    :cond_b59
    new-instance v31, Ljava/lang/StringBuilder;

    #@b5b
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@b5e
    const-string v32, "startUsingNetworkFeature took "

    #@b60
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b63
    move-result-object v31

    #@b64
    move-object/from16 v0, v31

    #@b66
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b69
    move-result-object v31

    #@b6a
    const-string v32, "ms"

    #@b6c
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6f
    move-result-object v31

    #@b70
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b73
    move-result-object v31

    #@b74
    goto/16 :goto_ac7

    #@b76
    .restart local v15       #mobile_state:Landroid/net/NetworkInfo$State;
    .restart local v16       #network:Landroid/net/NetworkStateTracker;
    :cond_b76
    new-instance v31, Ljava/lang/StringBuilder;

    #@b78
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@b7b
    const-string v32, "startUsingNetworkFeature took "

    #@b7d
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b80
    move-result-object v31

    #@b81
    move-object/from16 v0, v31

    #@b83
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b86
    move-result-object v31

    #@b87
    const-string v32, "ms"

    #@b89
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8c
    move-result-object v31

    #@b8d
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b90
    move-result-object v31

    #@b91
    goto/16 :goto_ac7

    #@b93
    .end local v15           #mobile_state:Landroid/net/NetworkInfo$State;
    .restart local v4       #addToList:Z
    .restart local v5       #currentPid:Ljava/lang/Integer;
    .restart local v19       #ni:Landroid/net/NetworkInfo;
    .restart local v21       #restoreTimer:I
    .restart local v25       #token:J
    :cond_b93
    new-instance v31, Ljava/lang/StringBuilder;

    #@b95
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@b98
    const-string v32, "startUsingNetworkFeature took "

    #@b9a
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9d
    move-result-object v31

    #@b9e
    move-object/from16 v0, v31

    #@ba0
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ba3
    move-result-object v31

    #@ba4
    const-string v32, "ms"

    #@ba6
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba9
    move-result-object v31

    #@baa
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bad
    move-result-object v31

    #@bae
    goto/16 :goto_ac7

    #@bb0
    .end local v25           #token:J
    :cond_bb0
    new-instance v31, Ljava/lang/StringBuilder;

    #@bb2
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@bb5
    const-string v32, "startUsingNetworkFeature took "

    #@bb7
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bba
    move-result-object v31

    #@bbb
    move-object/from16 v0, v31

    #@bbd
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@bc0
    move-result-object v31

    #@bc1
    const-string v32, "ms"

    #@bc3
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc6
    move-result-object v31

    #@bc7
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bca
    move-result-object v31

    #@bcb
    goto/16 :goto_ac7

    #@bcd
    :cond_bcd
    new-instance v31, Ljava/lang/StringBuilder;

    #@bcf
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@bd2
    const-string v32, "startUsingNetworkFeature took "

    #@bd4
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd7
    move-result-object v31

    #@bd8
    move-object/from16 v0, v31

    #@bda
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@bdd
    move-result-object v31

    #@bde
    const-string v32, "ms"

    #@be0
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be3
    move-result-object v31

    #@be4
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be7
    move-result-object v31

    #@be8
    goto/16 :goto_ac7

    #@bea
    :cond_bea
    new-instance v31, Ljava/lang/StringBuilder;

    #@bec
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@bef
    const-string v32, "startUsingNetworkFeature took "

    #@bf1
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf4
    move-result-object v31

    #@bf5
    move-object/from16 v0, v31

    #@bf7
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@bfa
    move-result-object v31

    #@bfb
    const-string v32, "ms"

    #@bfd
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c00
    move-result-object v31

    #@c01
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c04
    move-result-object v31

    #@c05
    goto/16 :goto_ac7

    #@c07
    .end local v4           #addToList:Z
    .end local v21           #restoreTimer:I
    :cond_c07
    new-instance v31, Ljava/lang/StringBuilder;

    #@c09
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@c0c
    const-string v32, "startUsingNetworkFeature took "

    #@c0e
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c11
    move-result-object v31

    #@c12
    move-object/from16 v0, v31

    #@c14
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c17
    move-result-object v31

    #@c18
    const-string v32, "ms"

    #@c1a
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1d
    move-result-object v31

    #@c1e
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c21
    move-result-object v31

    #@c22
    goto/16 :goto_ac7

    #@c24
    .end local v19           #ni:Landroid/net/NetworkInfo;
    :cond_c24
    new-instance v31, Ljava/lang/StringBuilder;

    #@c26
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@c29
    const-string v32, "startUsingNetworkFeature took "

    #@c2b
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2e
    move-result-object v31

    #@c2f
    move-object/from16 v0, v31

    #@c31
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c34
    move-result-object v31

    #@c35
    const-string v32, "ms"

    #@c37
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3a
    move-result-object v31

    #@c3b
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3e
    move-result-object v31

    #@c3f
    goto/16 :goto_ac7

    #@c41
    .end local v5           #currentPid:Ljava/lang/Integer;
    :cond_c41
    new-instance v31, Ljava/lang/StringBuilder;

    #@c43
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@c46
    const-string v32, "startUsingNetworkFeature took "

    #@c48
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4b
    move-result-object v31

    #@c4c
    move-object/from16 v0, v31

    #@c4e
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c51
    move-result-object v31

    #@c52
    const-string v32, "ms"

    #@c54
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c57
    move-result-object v31

    #@c58
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5b
    move-result-object v31

    #@c5c
    goto/16 :goto_ac7

    #@c5e
    .end local v10           #f:Lcom/android/server/ConnectivityService$FeatureUser;
    .end local v16           #network:Landroid/net/NetworkStateTracker;
    .end local v17           #networkMetered:Z
    .end local v28           #uidRules:I
    .end local v29           #usedNetworkType:I
    :cond_c5e
    new-instance v31, Ljava/lang/StringBuilder;

    #@c60
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@c63
    const-string v32, "startUsingNetworkFeature took "

    #@c65
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c68
    move-result-object v31

    #@c69
    move-object/from16 v0, v31

    #@c6b
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c6e
    move-result-object v31

    #@c6f
    const-string v32, "ms"

    #@c71
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c74
    move-result-object v31

    #@c75
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c78
    move-result-object v31

    #@c79
    goto/16 :goto_ac7

    #@c7b
    :cond_c7b
    new-instance v31, Ljava/lang/StringBuilder;

    #@c7d
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@c80
    const-string v32, "startUsingNetworkFeature took "

    #@c82
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c85
    move-result-object v31

    #@c86
    move-object/from16 v0, v31

    #@c88
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c8b
    move-result-object v31

    #@c8c
    const-string v32, "ms"

    #@c8e
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c91
    move-result-object v31

    #@c92
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c95
    move-result-object v31

    #@c96
    goto/16 :goto_ac7
.end method

.method public stopUsingNetworkFeature(ILjava/lang/String;)I
    .registers 11
    .parameter "networkType"
    .parameter "feature"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 2015
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceChangePermission()V

    #@4
    .line 2017
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingPid()I

    #@7
    move-result v2

    #@8
    .line 2018
    .local v2, pid:I
    invoke-static {}, Lcom/android/server/ConnectivityService;->getCallingUid()I

    #@b
    move-result v4

    #@c
    .line 2020
    .local v4, uid:I
    const/4 v3, 0x0

    #@d
    .line 2021
    .local v3, u:Lcom/android/server/ConnectivityService$FeatureUser;
    const/4 v0, 0x0

    #@e
    .line 2023
    .local v0, found:Z
    monitor-enter p0

    #@f
    .line 2024
    :try_start_f
    iget-object v7, p0, Lcom/android/server/ConnectivityService;->mFeatureUsers:Ljava/util/List;

    #@11
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v1

    #@15
    .local v1, i$:Ljava/util/Iterator;
    :cond_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v7

    #@19
    if-eqz v7, :cond_29

    #@1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v5

    #@1f
    check-cast v5, Lcom/android/server/ConnectivityService$FeatureUser;

    #@21
    .line 2025
    .local v5, x:Lcom/android/server/ConnectivityService$FeatureUser;
    invoke-virtual {v5, v2, v4, p1, p2}, Lcom/android/server/ConnectivityService$FeatureUser;->isSameUser(IIILjava/lang/String;)Z

    #@24
    move-result v7

    #@25
    if-eqz v7, :cond_15

    #@27
    .line 2026
    move-object v3, v5

    #@28
    .line 2027
    const/4 v0, 0x1

    #@29
    .line 2031
    .end local v5           #x:Lcom/android/server/ConnectivityService$FeatureUser;
    :cond_29
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_f .. :try_end_2a} :catchall_33

    #@2a
    .line 2032
    if-eqz v0, :cond_36

    #@2c
    if-eqz v3, :cond_36

    #@2e
    .line 2034
    invoke-direct {p0, v3, v6}, Lcom/android/server/ConnectivityService;->stopUsingNetworkFeature(Lcom/android/server/ConnectivityService$FeatureUser;Z)I

    #@31
    move-result v6

    #@32
    .line 2038
    :goto_32
    return v6

    #@33
    .line 2031
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_33
    move-exception v6

    #@34
    :try_start_34
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_33

    #@35
    throw v6

    #@36
    .line 2037
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_36
    const-string v7, "stopUsingNetworkFeature - not a live request, ignoring"

    #@38
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@3b
    goto :goto_32
.end method

.method public systemReady()V
    .registers 5

    #@0
    .prologue
    .line 3135
    monitor-enter p0

    #@1
    .line 3136
    const/4 v1, 0x1

    #@2
    :try_start_2
    iput-boolean v1, p0, Lcom/android/server/ConnectivityService;->mSystemReady:Z

    #@4
    .line 3137
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mInitialBroadcast:Landroid/content/Intent;

    #@6
    if-eqz v1, :cond_14

    #@8
    .line 3138
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@a
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mInitialBroadcast:Landroid/content/Intent;

    #@c
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@e
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@11
    .line 3139
    const/4 v1, 0x0

    #@12
    iput-object v1, p0, Lcom/android/server/ConnectivityService;->mInitialBroadcast:Landroid/content/Intent;

    #@14
    .line 3141
    :cond_14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_2 .. :try_end_15} :catchall_37

    #@15
    .line 3143
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@17
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@19
    const/16 v3, 0x9

    #@1b
    invoke-virtual {v2, v3}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Lcom/android/server/ConnectivityService$InternalHandler;->sendMessage(Landroid/os/Message;)Z

    #@22
    .line 3147
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->updateLockdownVpn()Z

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_36

    #@28
    .line 3148
    new-instance v0, Landroid/content/IntentFilter;

    #@2a
    const-string v1, "android.intent.action.USER_PRESENT"

    #@2c
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2f
    .line 3149
    .local v0, filter:Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@31
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@33
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@36
    .line 3151
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_36
    return-void

    #@37
    .line 3141
    :catchall_37
    move-exception v1

    #@38
    :try_start_38
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_37

    #@39
    throw v1
.end method

.method public tether(Ljava/lang/String;)I
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    const/4 v0, 0x3

    #@1
    .line 4443
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherChangePermission()V

    #@4
    .line 4445
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const-string v2, "ATT"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_4c

    #@10
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    const-string v2, "US"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_4c

    #@1c
    .line 4446
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@1e
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@21
    move-result-object v1

    #@22
    const-string v2, "tether_entitlement_check_state"

    #@24
    const/4 v3, 0x1

    #@25
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@28
    move-result v1

    #@29
    if-lez v1, :cond_4c

    #@2b
    .line 4447
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v1

    #@2f
    const/16 v2, 0x2710

    #@31
    if-le v1, v2, :cond_4c

    #@33
    .line 4448
    const-string v1, "ConnectivityService"

    #@35
    const-string v2, "[ConnectivityService] Tethering Permission denied"

    #@37
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 4449
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mHandler:Lcom/android/server/ConnectivityService$InternalHandler;

    #@3c
    const/16 v2, 0x64

    #@3e
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@41
    move-result v3

    #@42
    const/4 v4, 0x0

    #@43
    const/4 v5, 0x0

    #@44
    invoke-static {v1, v2, v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@4b
    .line 4459
    :cond_4b
    :goto_4b
    return v0

    #@4c
    .line 4456
    :cond_4c
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_4b

    #@52
    .line 4457
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@54
    invoke-virtual {v0, p1}, Lcom/android/server/connectivity/Tethering;->tether(Ljava/lang/String;)I

    #@57
    move-result v0

    #@58
    goto :goto_4b
.end method

.method public untether(Ljava/lang/String;)I
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 4465
    invoke-direct {p0}, Lcom/android/server/ConnectivityService;->enforceTetherChangePermission()V

    #@3
    .line 4467
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService;->isTetheringSupported()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 4468
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mTethering:Lcom/android/server/connectivity/Tethering;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/server/connectivity/Tethering;->untether(Ljava/lang/String;)I

    #@e
    move-result v0

    #@f
    .line 4470
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x3

    #@11
    goto :goto_f
.end method

.method protected updateBlockedUids(IZ)V
    .registers 9
    .parameter "uid"
    .parameter "isBlocked"

    #@0
    .prologue
    .line 5151
    :try_start_0
    const-string v3, "alarm"

    #@2
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Lcom/android/server/AlarmManagerService;

    #@8
    .line 5153
    .local v1, mAlarmMgrSvc:Lcom/android/server/AlarmManagerService;
    invoke-virtual {v1, p1, p2}, Lcom/android/server/AlarmManagerService;->updateBlockedUids(IZ)V
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_b} :catch_17

    #@b
    .line 5158
    .end local v1           #mAlarmMgrSvc:Lcom/android/server/AlarmManagerService;
    :goto_b
    :try_start_b
    const-string v3, "power"

    #@d
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    check-cast v2, Lcom/android/server/power/PowerManagerService;

    #@13
    .line 5160
    .local v2, mPowerMgrSvc:Lcom/android/server/power/PowerManagerService;
    invoke-virtual {v2, p1, p2}, Lcom/android/server/power/PowerManagerService;->updateBlockedUids(IZ)V
    :try_end_16
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_16} :catch_31

    #@16
    .line 5164
    .end local v2           #mPowerMgrSvc:Lcom/android/server/power/PowerManagerService;
    :goto_16
    return-void

    #@17
    .line 5154
    :catch_17
    move-exception v0

    #@18
    .line 5155
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v3, "ConnectivityService"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "Could Not Update blocked Uids with alarmManager"

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_b

    #@31
    .line 5161
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catch_31
    move-exception v0

    #@32
    .line 5162
    .restart local v0       #e:Ljava/lang/NullPointerException;
    const-string v3, "ConnectivityService"

    #@34
    new-instance v4, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v5, "Could Not Update blocked Uids with powerManager"

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_16
.end method

.method public updateLockdownVpn()Z
    .registers 8

    #@0
    .prologue
    .line 5090
    invoke-static {}, Lcom/android/server/ConnectivityService;->enforceSystemUid()V

    #@3
    .line 5093
    invoke-static {}, Lcom/android/server/net/LockdownVpnTracker;->isEnabled()Z

    #@6
    move-result v0

    #@7
    iput-boolean v0, p0, Lcom/android/server/ConnectivityService;->mLockdownEnabled:Z

    #@9
    .line 5094
    iget-boolean v0, p0, Lcom/android/server/ConnectivityService;->mLockdownEnabled:Z

    #@b
    if-eqz v0, :cond_5b

    #@d
    .line 5095
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mKeyStore:Landroid/security/KeyStore;

    #@f
    invoke-virtual {v0}, Landroid/security/KeyStore;->state()Landroid/security/KeyStore$State;

    #@12
    move-result-object v0

    #@13
    sget-object v1, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@15
    if-eq v0, v1, :cond_20

    #@17
    .line 5096
    const-string v0, "ConnectivityService"

    #@19
    const-string v1, "KeyStore locked; unable to create LockdownTracker"

    #@1b
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 5097
    const/4 v0, 0x0

    #@1f
    .line 5108
    :goto_1f
    return v0

    #@20
    .line 5100
    :cond_20
    new-instance v6, Ljava/lang/String;

    #@22
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mKeyStore:Landroid/security/KeyStore;

    #@24
    const-string v1, "LOCKDOWN_VPN"

    #@26
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@29
    move-result-object v0

    #@2a
    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    #@2d
    .line 5101
    .local v6, profileName:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/ConnectivityService;->mKeyStore:Landroid/security/KeyStore;

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "VPN_"

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@45
    move-result-object v0

    #@46
    invoke-static {v6, v0}, Lcom/android/internal/net/VpnProfile;->decode(Ljava/lang/String;[B)Lcom/android/internal/net/VpnProfile;

    #@49
    move-result-object v5

    #@4a
    .line 5103
    .local v5, profile:Lcom/android/internal/net/VpnProfile;
    new-instance v0, Lcom/android/server/net/LockdownVpnTracker;

    #@4c
    iget-object v1, p0, Lcom/android/server/ConnectivityService;->mContext:Landroid/content/Context;

    #@4e
    iget-object v2, p0, Lcom/android/server/ConnectivityService;->mNetd:Landroid/os/INetworkManagementService;

    #@50
    iget-object v4, p0, Lcom/android/server/ConnectivityService;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@52
    move-object v3, p0

    #@53
    invoke-direct/range {v0 .. v5}, Lcom/android/server/net/LockdownVpnTracker;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Lcom/android/server/ConnectivityService;Lcom/android/server/connectivity/Vpn;Lcom/android/internal/net/VpnProfile;)V

    #@56
    invoke-direct {p0, v0}, Lcom/android/server/ConnectivityService;->setLockdownTracker(Lcom/android/server/net/LockdownVpnTracker;)V

    #@59
    .line 5108
    .end local v5           #profile:Lcom/android/internal/net/VpnProfile;
    .end local v6           #profileName:Ljava/lang/String;
    :goto_59
    const/4 v0, 0x1

    #@5a
    goto :goto_1f

    #@5b
    .line 5105
    :cond_5b
    const/4 v0, 0x0

    #@5c
    invoke-direct {p0, v0}, Lcom/android/server/ConnectivityService;->setLockdownTracker(Lcom/android/server/net/LockdownVpnTracker;)V

    #@5f
    goto :goto_59
.end method

.method public updateNetworkSettings(Landroid/net/NetworkStateTracker;)V
    .registers 6
    .parameter "nt"

    #@0
    .prologue
    .line 3709
    invoke-interface {p1}, Landroid/net/NetworkStateTracker;->getTcpBufferSizesPropName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3710
    .local v1, key:Ljava/lang/String;
    if-nez v1, :cond_59

    #@6
    const/4 v0, 0x0

    #@7
    .line 3712
    .local v0, bufferSizes:Ljava/lang/String;
    :goto_7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_29

    #@d
    .line 3713
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, " not found in system properties. Using defaults"

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@23
    .line 3716
    const-string v1, "net.tcp.buffersize.default"

    #@25
    .line 3717
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 3721
    :cond_29
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_58

    #@2f
    .line 3723
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "Setting TCP values: ["

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    const-string v3, "] which comes from ["

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    const-string v3, "]"

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->log(Ljava/lang/String;)V

    #@55
    .line 3726
    invoke-direct {p0, v0}, Lcom/android/server/ConnectivityService;->setBufferSize(Ljava/lang/String;)V

    #@58
    .line 3728
    :cond_58
    return-void

    #@59
    .line 3710
    .end local v0           #bufferSizes:Ljava/lang/String;
    :cond_59
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    goto :goto_7
.end method
