.class Lcom/android/server/WallpaperManagerService$MyPackageMonitor;
.super Lcom/android/internal/content/PackageMonitor;
.source "WallpaperManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WallpaperManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyPackageMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/WallpaperManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/WallpaperManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 324
    iput-object p1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method doPackagesChangedLocked(ZLcom/android/server/WallpaperManagerService$WallpaperData;)Z
    .registers 13
    .parameter "doit"
    .parameter "wallpaper"

    #@0
    .prologue
    const/4 v9, 0x3

    #@1
    const/4 v8, 0x2

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v6, 0x0

    #@4
    .line 411
    const/4 v1, 0x0

    #@5
    .line 412
    .local v1, changed:Z
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@7
    if-eqz v3, :cond_3b

    #@9
    .line 413
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@b
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {p0, v3}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->isPackageDisappearing(Ljava/lang/String;)I

    #@12
    move-result v0

    #@13
    .line 415
    .local v0, change:I
    if-eq v0, v9, :cond_17

    #@15
    if-ne v0, v8, :cond_3b

    #@17
    .line 417
    :cond_17
    const/4 v1, 0x1

    #@18
    .line 418
    if-eqz p1, :cond_3b

    #@1a
    .line 419
    const-string v3, "WallpaperService"

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "Wallpaper uninstalled, removing: "

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    iget-object v5, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 421
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@36
    iget v4, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@38
    invoke-virtual {v3, v6, v4, v7}, Lcom/android/server/WallpaperManagerService;->clearWallpaperLocked(ZILandroid/os/IRemoteCallback;)V

    #@3b
    .line 425
    .end local v0           #change:I
    :cond_3b
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@3d
    if-eqz v3, :cond_4f

    #@3f
    .line 426
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@41
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {p0, v3}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->isPackageDisappearing(Ljava/lang/String;)I

    #@48
    move-result v0

    #@49
    .line 428
    .restart local v0       #change:I
    if-eq v0, v9, :cond_4d

    #@4b
    if-ne v0, v8, :cond_4f

    #@4d
    .line 430
    :cond_4d
    iput-object v7, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@4f
    .line 433
    .end local v0           #change:I
    :cond_4f
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@51
    if-eqz v3, :cond_6d

    #@53
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@55
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {p0, v3}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->isPackageModified(Ljava/lang/String;)Z

    #@5c
    move-result v3

    #@5d
    if-eqz v3, :cond_6d

    #@5f
    .line 436
    :try_start_5f
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@61
    iget-object v3, v3, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@63
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@66
    move-result-object v3

    #@67
    iget-object v4, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@69
    const/4 v5, 0x0

    #@6a
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;
    :try_end_6d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5f .. :try_end_6d} :catch_8c

    #@6d
    .line 444
    :cond_6d
    :goto_6d
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@6f
    if-eqz v3, :cond_8b

    #@71
    iget-object v3, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@73
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {p0, v3}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->isPackageModified(Ljava/lang/String;)Z

    #@7a
    move-result v3

    #@7b
    if-eqz v3, :cond_8b

    #@7d
    .line 447
    :try_start_7d
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@7f
    iget-object v3, v3, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@81
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@84
    move-result-object v3

    #@85
    iget-object v4, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@87
    const/4 v5, 0x0

    #@88
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;
    :try_end_8b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7d .. :try_end_8b} :catch_af

    #@8b
    .line 453
    :cond_8b
    :goto_8b
    return v1

    #@8c
    .line 438
    :catch_8c
    move-exception v2

    #@8d
    .line 439
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "WallpaperService"

    #@8f
    new-instance v4, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v5, "Wallpaper component gone, removing: "

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    iget-object v5, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@9c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v4

    #@a4
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 441
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@a9
    iget v4, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@ab
    invoke-virtual {v3, v6, v4, v7}, Lcom/android/server/WallpaperManagerService;->clearWallpaperLocked(ZILandroid/os/IRemoteCallback;)V

    #@ae
    goto :goto_6d

    #@af
    .line 449
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_af
    move-exception v2

    #@b0
    .line 450
    .restart local v2       #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    iput-object v7, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@b2
    goto :goto_8b
.end method

.method public onHandleForceStop(Landroid/content/Intent;[Ljava/lang/String;IZ)Z
    .registers 11
    .parameter "intent"
    .parameter "packages"
    .parameter "uid"
    .parameter "doit"

    #@0
    .prologue
    .line 383
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    iget-object v4, v3, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v4

    #@5
    .line 384
    const/4 v0, 0x0

    #@6
    .line 385
    .local v0, changed:Z
    :try_start_6
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@8
    iget v3, v3, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@a
    invoke-virtual {p0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->getChangingUserId()I

    #@d
    move-result v5

    #@e
    if-eq v3, v5, :cond_13

    #@10
    .line 386
    const/4 v3, 0x0

    #@11
    monitor-exit v4

    #@12
    .line 393
    :goto_12
    return v3

    #@13
    .line 388
    :cond_13
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@15
    iget-object v3, v3, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@17
    iget-object v5, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@19
    iget v5, v5, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@1b
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@21
    .line 389
    .local v2, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-eqz v2, :cond_28

    #@23
    .line 390
    invoke-virtual {p0, p4, v2}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->doPackagesChangedLocked(ZLcom/android/server/WallpaperManagerService$WallpaperData;)Z

    #@26
    move-result v1

    #@27
    .line 391
    .local v1, res:Z
    or-int/2addr v0, v1

    #@28
    .line 393
    .end local v1           #res:Z
    :cond_28
    monitor-exit v4

    #@29
    move v3, v0

    #@2a
    goto :goto_12

    #@2b
    .line 394
    .end local v2           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_2b
    move-exception v3

    #@2c
    monitor-exit v4
    :try_end_2d
    .catchall {:try_start_6 .. :try_end_2d} :catchall_2b

    #@2d
    throw v3
.end method

.method public onPackageModified(Ljava/lang/String;)V
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 350
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v2

    #@5
    .line 351
    :try_start_5
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@7
    iget v1, v1, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@9
    invoke-virtual {p0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->getChangingUserId()I

    #@c
    move-result v3

    #@d
    if-eq v1, v3, :cond_11

    #@f
    .line 352
    monitor-exit v2

    #@10
    .line 363
    :goto_10
    return-void

    #@11
    .line 354
    :cond_11
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@13
    iget-object v1, v1, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@15
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@17
    iget v3, v3, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@19
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@1f
    .line 355
    .local v0, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-eqz v0, :cond_3a

    #@21
    .line 356
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@23
    if-eqz v1, :cond_31

    #@25
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@27
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_36

    #@31
    .line 358
    :cond_31
    monitor-exit v2

    #@32
    goto :goto_10

    #@33
    .line 362
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_33
    move-exception v1

    #@34
    monitor-exit v2
    :try_end_35
    .catchall {:try_start_5 .. :try_end_35} :catchall_33

    #@35
    throw v1

    #@36
    .line 360
    .restart local v0       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_36
    const/4 v1, 0x1

    #@37
    :try_start_37
    invoke-virtual {p0, v1, v0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->doPackagesChangedLocked(ZLcom/android/server/WallpaperManagerService$WallpaperData;)Z

    #@3a
    .line 362
    :cond_3a
    monitor-exit v2
    :try_end_3b
    .catchall {:try_start_37 .. :try_end_3b} :catchall_33

    #@3b
    goto :goto_10
.end method

.method public onPackageUpdateFinished(Ljava/lang/String;I)V
    .registers 10
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 327
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    iget-object v6, v0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v6

    #@5
    .line 328
    :try_start_5
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@7
    iget v0, v0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@9
    invoke-virtual {p0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->getChangingUserId()I

    #@c
    move-result v2

    #@d
    if-eq v0, v2, :cond_11

    #@f
    .line 329
    monitor-exit v6

    #@10
    .line 346
    :goto_10
    return-void

    #@11
    .line 331
    :cond_11
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@13
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@15
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@17
    iget v2, v2, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@19
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v4

    #@1d
    check-cast v4, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@1f
    .line 332
    .local v4, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-eqz v4, :cond_56

    #@21
    .line 333
    iget-object v0, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@23
    if-eqz v0, :cond_56

    #@25
    iget-object v0, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@27
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_56

    #@31
    .line 335
    const/4 v0, 0x0

    #@32
    iput-boolean v0, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperUpdating:Z

    #@34
    .line 336
    iget-object v1, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@36
    .line 337
    .local v1, comp:Landroid/content/ComponentName;
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@38
    invoke-virtual {v0, v4}, Lcom/android/server/WallpaperManagerService;->clearWallpaperComponentLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@3b
    .line 338
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@3d
    const/4 v2, 0x0

    #@3e
    const/4 v3, 0x0

    #@3f
    const/4 v5, 0x0

    #@40
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z

    #@43
    move-result v0

    #@44
    if-nez v0, :cond_56

    #@46
    .line 340
    const-string v0, "WallpaperService"

    #@48
    const-string v2, "Wallpaper no longer available; reverting to default"

    #@4a
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 341
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@4f
    const/4 v2, 0x0

    #@50
    iget v3, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@52
    const/4 v5, 0x0

    #@53
    invoke-virtual {v0, v2, v3, v5}, Lcom/android/server/WallpaperManagerService;->clearWallpaperLocked(ZILandroid/os/IRemoteCallback;)V

    #@56
    .line 345
    .end local v1           #comp:Landroid/content/ComponentName;
    :cond_56
    monitor-exit v6

    #@57
    goto :goto_10

    #@58
    .end local v4           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_58
    move-exception v0

    #@59
    monitor-exit v6
    :try_end_5a
    .catchall {:try_start_5 .. :try_end_5a} :catchall_58

    #@5a
    throw v0
.end method

.method public onPackageUpdateStarted(Ljava/lang/String;I)V
    .registers 7
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 367
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v2

    #@5
    .line 368
    :try_start_5
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@7
    iget v1, v1, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@9
    invoke-virtual {p0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->getChangingUserId()I

    #@c
    move-result v3

    #@d
    if-eq v1, v3, :cond_11

    #@f
    .line 369
    monitor-exit v2

    #@10
    .line 379
    :goto_10
    return-void

    #@11
    .line 371
    :cond_11
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@13
    iget-object v1, v1, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@15
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@17
    iget v3, v3, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@19
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@1f
    .line 372
    .local v0, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-eqz v0, :cond_34

    #@21
    .line 373
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@23
    if-eqz v1, :cond_34

    #@25
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@27
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_34

    #@31
    .line 375
    const/4 v1, 0x1

    #@32
    iput-boolean v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperUpdating:Z

    #@34
    .line 378
    :cond_34
    monitor-exit v2

    #@35
    goto :goto_10

    #@36
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_36
    move-exception v1

    #@37
    monitor-exit v2
    :try_end_38
    .catchall {:try_start_5 .. :try_end_38} :catchall_36

    #@38
    throw v1
.end method

.method public onSomePackagesChanged()V
    .registers 5

    #@0
    .prologue
    .line 399
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v2

    #@5
    .line 400
    :try_start_5
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@7
    iget v1, v1, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@9
    invoke-virtual {p0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->getChangingUserId()I

    #@c
    move-result v3

    #@d
    if-eq v1, v3, :cond_11

    #@f
    .line 401
    monitor-exit v2

    #@10
    .line 408
    :goto_10
    return-void

    #@11
    .line 403
    :cond_11
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@13
    iget-object v1, v1, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@15
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->this$0:Lcom/android/server/WallpaperManagerService;

    #@17
    iget v3, v3, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@19
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@1f
    .line 404
    .local v0, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-eqz v0, :cond_25

    #@21
    .line 405
    const/4 v1, 0x1

    #@22
    invoke-virtual {p0, v1, v0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->doPackagesChangedLocked(ZLcom/android/server/WallpaperManagerService$WallpaperData;)Z

    #@25
    .line 407
    :cond_25
    monitor-exit v2

    #@26
    goto :goto_10

    #@27
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_27
    move-exception v1

    #@28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_5 .. :try_end_29} :catchall_27

    #@29
    throw v1
.end method
