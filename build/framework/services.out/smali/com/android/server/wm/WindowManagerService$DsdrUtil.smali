.class final Lcom/android/server/wm/WindowManagerService$DsdrUtil;
.super Ljava/lang/Object;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "DsdrUtil"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 13225
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$DsdrUtil;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 13227
    return-void
.end method


# virtual methods
.method public destroyExternalApp()V
    .registers 11

    #@0
    .prologue
    .line 13236
    :try_start_0
    const-string v7, "WindowManager"

    #@2
    const-string v8, "[DSDR] destroyExternalWindows started"

    #@4
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 13237
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DsdrUtil;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@9
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mAppTokens:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v7

    #@f
    if-lez v7, :cond_7c

    #@11
    .line 13238
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DsdrUtil;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@13
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mAppTokens:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v7

    #@19
    add-int/lit8 v3, v7, -0x1

    #@1b
    .local v3, index:I
    :goto_1b
    if-ltz v3, :cond_7c

    #@1d
    .line 13239
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DsdrUtil;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@1f
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mAppTokens:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    check-cast v5, Lcom/android/server/wm/AppWindowToken;

    #@27
    .line 13240
    .local v5, token:Lcom/android/server/wm/AppWindowToken;
    const/4 v4, 0x0

    #@28
    .line 13241
    .local v4, rv:I
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DsdrUtil;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2a
    invoke-virtual {v7, v5}, Lcom/android/server/wm/WindowManagerService;->getExternalWindowStatus(Lcom/android/server/wm/AppWindowToken;)I

    #@2d
    move-result v4

    #@2e
    if-lez v4, :cond_73

    #@30
    .line 13242
    invoke-virtual {v5}, Lcom/android/server/wm/AppWindowToken;->findMainWindow()Lcom/android/server/wm/WindowState;

    #@33
    move-result-object v6

    #@34
    .line 13243
    .local v6, w:Lcom/android/server/wm/WindowState;
    if-eqz v6, :cond_73

    #@36
    .line 13244
    iget-object v1, v6, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;
    :try_end_38
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_38} :catch_7b

    #@38
    .line 13246
    .local v1, attrs:Landroid/view/WindowManager$LayoutParams;
    :try_start_38
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DsdrUtil;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@3a
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@3c
    const-string v8, "activity"

    #@3e
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@41
    move-result-object v0

    #@42
    check-cast v0, Landroid/app/ActivityManager;

    #@44
    .line 13247
    .local v0, am:Landroid/app/ActivityManager;
    iget-object v7, v1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@46
    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    #@49
    .line 13248
    const-string v7, "WindowManager"

    #@4b
    new-instance v8, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v9, "[DSDR] Destroy external activity - "

    #@52
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v8

    #@56
    iget-object v9, v1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@58
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v8

    #@5c
    const-string v9, " (status="

    #@5e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v8

    #@62
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v8

    #@66
    const-string v9, ")"

    #@68
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v8

    #@70
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_73} :catch_76
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_38 .. :try_end_73} :catch_7b

    #@73
    .line 13238
    .end local v0           #am:Landroid/app/ActivityManager;
    .end local v1           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v6           #w:Lcom/android/server/wm/WindowState;
    :cond_73
    :goto_73
    add-int/lit8 v3, v3, -0x1

    #@75
    goto :goto_1b

    #@76
    .line 13249
    .restart local v1       #attrs:Landroid/view/WindowManager$LayoutParams;
    .restart local v6       #w:Lcom/android/server/wm/WindowState;
    :catch_76
    move-exception v2

    #@77
    .local v2, e:Ljava/lang/Exception;
    :try_start_77
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_77 .. :try_end_7a} :catch_7b

    #@7a
    goto :goto_73

    #@7b
    .line 13254
    .end local v1           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v3           #index:I
    .end local v4           #rv:I
    .end local v5           #token:Lcom/android/server/wm/AppWindowToken;
    .end local v6           #w:Lcom/android/server/wm/WindowState;
    :catch_7b
    move-exception v7

    #@7c
    .line 13256
    :cond_7c
    return-void
.end method
