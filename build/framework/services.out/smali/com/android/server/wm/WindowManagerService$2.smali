.class Lcom/android/server/wm/WindowManagerService$2;
.super Landroid/content/BroadcastReceiver;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 370
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$2;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 373
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v6

    #@4
    .line 375
    .local v6, action:Ljava/lang/String;
    :try_start_4
    const-string v1, "com.lge.intent.action.LG_DUAL_SCREEN_UPDATE"

    #@6
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_65

    #@c
    .line 376
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$2;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@e
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$2;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@10
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@12
    const-string v3, ""

    #@14
    const/4 v4, 0x0

    #@15
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@18
    move-result-object v2

    #@19
    iput-object v2, v1, Lcom/android/server/wm/WindowManagerService;->mToastHidden:Landroid/widget/Toast;

    #@1b
    .line 377
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@1d
    const/4 v1, -0x1

    #@1e
    const/4 v2, -0x1

    #@1f
    const/16 v3, 0x7d6

    #@21
    const v4, 0x40028

    #@24
    const/4 v5, -0x1

    #@25
    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    #@28
    .line 382
    .local v0, params:Landroid/view/WindowManager$LayoutParams;
    const/4 v1, 0x1

    #@29
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2b
    .line 383
    const/4 v1, 0x1

    #@2c
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2e
    .line 384
    const/4 v1, 0x0

    #@2f
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@31
    .line 385
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$2;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@33
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mToastHidden:Landroid/widget/Toast;

    #@35
    new-instance v2, Landroid/view/View;

    #@37
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerService$2;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@39
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@3b
    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@3e
    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    #@41
    .line 386
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$2;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@43
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mToastHidden:Landroid/widget/Toast;

    #@45
    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@4c
    .line 387
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$2;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4e
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mToastHidden:Landroid/widget/Toast;

    #@50
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    #@53
    .line 388
    const-string v1, "WindowManager"

    #@55
    const-string v2, "[DSDR][WMS.java] LG_DUAL_SCREEN_UPDATE"

    #@57
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 389
    new-instance v8, Lcom/android/server/wm/WindowManagerService$2$1;

    #@5c
    invoke-direct {v8, p0}, Lcom/android/server/wm/WindowManagerService$2$1;-><init>(Lcom/android/server/wm/WindowManagerService$2;)V

    #@5f
    .line 395
    .local v8, h:Landroid/os/Handler;
    const/4 v1, 0x0

    #@60
    const-wide/16 v2, 0x64

    #@62
    invoke-virtual {v8, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_65} :catch_66

    #@65
    .line 400
    .end local v0           #params:Landroid/view/WindowManager$LayoutParams;
    .end local v8           #h:Landroid/os/Handler;
    :cond_65
    :goto_65
    return-void

    #@66
    .line 397
    :catch_66
    move-exception v7

    #@67
    .line 398
    .local v7, e:Ljava/lang/Exception;
    const-string v1, "WindowManager"

    #@69
    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@70
    goto :goto_65
.end method
