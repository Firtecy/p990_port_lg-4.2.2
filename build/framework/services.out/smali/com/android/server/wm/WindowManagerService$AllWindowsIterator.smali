.class Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;
.super Ljava/lang/Object;
.source "WindowManagerService.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AllWindowsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/android/server/wm/WindowState;",
        ">;"
    }
.end annotation


# instance fields
.field private mDisplayContent:Lcom/android/server/wm/DisplayContent;

.field private mDisplayContentsIterator:Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

.field private mReverse:Z

.field private mWindowList:Lcom/android/server/wm/WindowList;

.field private mWindowListIndex:I

.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 12706
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 12707
    new-instance v0, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

    #@7
    invoke-direct {v0, p1}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;-><init>(Lcom/android/server/wm/WindowManagerService;)V

    #@a
    iput-object v0, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContentsIterator:Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

    #@c
    .line 12708
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContentsIterator:Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

    #@e
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->next()Lcom/android/server/wm/DisplayContent;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@14
    .line 12709
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@16
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getWindowList()Lcom/android/server/wm/WindowList;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@1c
    .line 12710
    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/WindowManagerService;Z)V
    .registers 4
    .parameter
    .parameter "reverse"

    #@0
    .prologue
    .line 12713
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;-><init>(Lcom/android/server/wm/WindowManagerService;)V

    #@3
    .line 12714
    iput-boolean p2, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mReverse:Z

    #@5
    .line 12715
    if-eqz p2, :cond_12

    #@7
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@9
    invoke-virtual {v0}, Lcom/android/server/wm/WindowList;->size()I

    #@c
    move-result v0

    #@d
    add-int/lit8 v0, v0, -0x1

    #@f
    :goto_f
    iput v0, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@11
    .line 12716
    return-void

    #@12
    .line 12715
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_f
.end method


# virtual methods
.method public hasNext()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 12720
    iget-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mReverse:Z

    #@4
    if-eqz v2, :cond_d

    #@6
    .line 12721
    iget v2, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@8
    if-ltz v2, :cond_b

    #@a
    .line 12723
    :cond_a
    :goto_a
    return v0

    #@b
    :cond_b
    move v0, v1

    #@c
    .line 12721
    goto :goto_a

    #@d
    .line 12723
    :cond_d
    iget v2, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@f
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@11
    invoke-virtual {v3}, Lcom/android/server/wm/WindowList;->size()I

    #@14
    move-result v3

    #@15
    if-lt v2, v3, :cond_a

    #@17
    move v0, v1

    #@18
    goto :goto_a
.end method

.method public next()Lcom/android/server/wm/WindowState;
    .registers 4

    #@0
    .prologue
    .line 12728
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->hasNext()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_6d

    #@6
    .line 12729
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@8
    iget v2, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@a
    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/server/wm/WindowState;

    #@10
    .line 12730
    .local v0, win:Lcom/android/server/wm/WindowState;
    iget-boolean v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mReverse:Z

    #@12
    if-eqz v1, :cond_41

    #@14
    .line 12731
    iget v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@16
    add-int/lit8 v1, v1, -0x1

    #@18
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@1a
    .line 12732
    iget v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@1c
    if-gez v1, :cond_40

    #@1e
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContentsIterator:Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

    #@20
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->hasNext()Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_40

    #@26
    .line 12733
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContentsIterator:Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

    #@28
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->next()Lcom/android/server/wm/DisplayContent;

    #@2b
    move-result-object v1

    #@2c
    iput-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@2e
    .line 12734
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@30
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getWindowList()Lcom/android/server/wm/WindowList;

    #@33
    move-result-object v1

    #@34
    iput-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@36
    .line 12735
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@38
    invoke-virtual {v1}, Lcom/android/server/wm/WindowList;->size()I

    #@3b
    move-result v1

    #@3c
    add-int/lit8 v1, v1, -0x1

    #@3e
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@40
    .line 12746
    :cond_40
    :goto_40
    return-object v0

    #@41
    .line 12738
    :cond_41
    iget v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@43
    add-int/lit8 v1, v1, 0x1

    #@45
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@47
    .line 12739
    iget v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@49
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@4b
    invoke-virtual {v2}, Lcom/android/server/wm/WindowList;->size()I

    #@4e
    move-result v2

    #@4f
    if-lt v1, v2, :cond_40

    #@51
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContentsIterator:Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

    #@53
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->hasNext()Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_40

    #@59
    .line 12741
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContentsIterator:Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;

    #@5b
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->next()Lcom/android/server/wm/DisplayContent;

    #@5e
    move-result-object v1

    #@5f
    iput-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@61
    .line 12742
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@63
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getWindowList()Lcom/android/server/wm/WindowList;

    #@66
    move-result-object v1

    #@67
    iput-object v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowList:Lcom/android/server/wm/WindowList;

    #@69
    .line 12743
    const/4 v1, 0x0

    #@6a
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->mWindowListIndex:I

    #@6c
    goto :goto_40

    #@6d
    .line 12748
    .end local v0           #win:Lcom/android/server/wm/WindowState;
    :cond_6d
    new-instance v1, Ljava/util/NoSuchElementException;

    #@6f
    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    #@72
    throw v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 12699
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->next()Lcom/android/server/wm/WindowState;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public remove()V
    .registers 3

    #@0
    .prologue
    .line 12753
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2
    const-string v1, "AllWindowsIterator.remove not implemented"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method
