.class public Lcom/android/server/wm/BlackFrame;
.super Ljava/lang/Object;
.source "BlackFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/BlackFrame$BlackSurface;
    }
.end annotation


# instance fields
.field final mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

.field final mInnerRect:Landroid/graphics/Rect;

.field final mOuterRect:Landroid/graphics/Rect;

.field final mTmpFloats:[F

.field final mTmpMatrix:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/view/SurfaceSession;Landroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .registers 18
    .parameter "session"
    .parameter "outer"
    .parameter "inner"
    .parameter "layer"
    .parameter "layerStack"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    #@0
    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 89
    new-instance v0, Landroid/graphics/Matrix;

    #@5
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/wm/BlackFrame;->mTmpMatrix:Landroid/graphics/Matrix;

    #@a
    .line 90
    const/16 v0, 0x9

    #@c
    new-array v0, v0, [F

    #@e
    iput-object v0, p0, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@10
    .line 91
    const/4 v0, 0x4

    #@11
    new-array v0, v0, [Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@13
    iput-object v0, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@15
    .line 108
    const/4 v9, 0x0

    #@16
    .line 110
    .local v9, success:Z
    new-instance v0, Landroid/graphics/Rect;

    #@18
    invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@1b
    iput-object v0, p0, Lcom/android/server/wm/BlackFrame;->mOuterRect:Landroid/graphics/Rect;

    #@1d
    .line 111
    new-instance v0, Landroid/graphics/Rect;

    #@1f
    invoke-direct {v0, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@22
    iput-object v0, p0, Lcom/android/server/wm/BlackFrame;->mInnerRect:Landroid/graphics/Rect;

    #@24
    .line 113
    :try_start_24
    iget v0, p2, Landroid/graphics/Rect;->top:I

    #@26
    iget v1, p3, Landroid/graphics/Rect;->top:I

    #@28
    if-ge v0, v1, :cond_42

    #@2a
    .line 114
    iget-object v10, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@2c
    const/4 v11, 0x0

    #@2d
    new-instance v0, Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@2f
    iget v4, p2, Landroid/graphics/Rect;->left:I

    #@31
    iget v5, p2, Landroid/graphics/Rect;->top:I

    #@33
    iget v6, p3, Landroid/graphics/Rect;->right:I

    #@35
    iget v7, p3, Landroid/graphics/Rect;->top:I

    #@37
    move-object v1, p0

    #@38
    move-object v2, p1

    #@39
    move/from16 v3, p4

    #@3b
    move/from16 v8, p5

    #@3d
    invoke-direct/range {v0 .. v8}, Lcom/android/server/wm/BlackFrame$BlackSurface;-><init>(Lcom/android/server/wm/BlackFrame;Landroid/view/SurfaceSession;IIIIII)V

    #@40
    aput-object v0, v10, v11

    #@42
    .line 117
    :cond_42
    iget v0, p2, Landroid/graphics/Rect;->left:I

    #@44
    iget v1, p3, Landroid/graphics/Rect;->left:I

    #@46
    if-ge v0, v1, :cond_60

    #@48
    .line 118
    iget-object v10, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@4a
    const/4 v11, 0x1

    #@4b
    new-instance v0, Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@4d
    iget v4, p2, Landroid/graphics/Rect;->left:I

    #@4f
    iget v5, p3, Landroid/graphics/Rect;->top:I

    #@51
    iget v6, p3, Landroid/graphics/Rect;->left:I

    #@53
    iget v7, p2, Landroid/graphics/Rect;->bottom:I

    #@55
    move-object v1, p0

    #@56
    move-object v2, p1

    #@57
    move/from16 v3, p4

    #@59
    move/from16 v8, p5

    #@5b
    invoke-direct/range {v0 .. v8}, Lcom/android/server/wm/BlackFrame$BlackSurface;-><init>(Lcom/android/server/wm/BlackFrame;Landroid/view/SurfaceSession;IIIIII)V

    #@5e
    aput-object v0, v10, v11

    #@60
    .line 121
    :cond_60
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    #@62
    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    #@64
    if-le v0, v1, :cond_7e

    #@66
    .line 122
    iget-object v10, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@68
    const/4 v11, 0x2

    #@69
    new-instance v0, Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@6b
    iget v4, p3, Landroid/graphics/Rect;->left:I

    #@6d
    iget v5, p3, Landroid/graphics/Rect;->bottom:I

    #@6f
    iget v6, p2, Landroid/graphics/Rect;->right:I

    #@71
    iget v7, p2, Landroid/graphics/Rect;->bottom:I

    #@73
    move-object v1, p0

    #@74
    move-object v2, p1

    #@75
    move/from16 v3, p4

    #@77
    move/from16 v8, p5

    #@79
    invoke-direct/range {v0 .. v8}, Lcom/android/server/wm/BlackFrame$BlackSurface;-><init>(Lcom/android/server/wm/BlackFrame;Landroid/view/SurfaceSession;IIIIII)V

    #@7c
    aput-object v0, v10, v11

    #@7e
    .line 125
    :cond_7e
    iget v0, p2, Landroid/graphics/Rect;->right:I

    #@80
    iget v1, p3, Landroid/graphics/Rect;->right:I

    #@82
    if-le v0, v1, :cond_9c

    #@84
    .line 126
    iget-object v10, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@86
    const/4 v11, 0x3

    #@87
    new-instance v0, Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@89
    iget v4, p3, Landroid/graphics/Rect;->right:I

    #@8b
    iget v5, p2, Landroid/graphics/Rect;->top:I

    #@8d
    iget v6, p2, Landroid/graphics/Rect;->right:I

    #@8f
    iget v7, p3, Landroid/graphics/Rect;->bottom:I

    #@91
    move-object v1, p0

    #@92
    move-object v2, p1

    #@93
    move/from16 v3, p4

    #@95
    move/from16 v8, p5

    #@97
    invoke-direct/range {v0 .. v8}, Lcom/android/server/wm/BlackFrame$BlackSurface;-><init>(Lcom/android/server/wm/BlackFrame;Landroid/view/SurfaceSession;IIIIII)V

    #@9a
    aput-object v0, v10, v11
    :try_end_9c
    .catchall {:try_start_24 .. :try_end_9c} :catchall_a3

    #@9c
    .line 129
    :cond_9c
    const/4 v9, 0x1

    #@9d
    .line 131
    if-nez v9, :cond_a2

    #@9f
    .line 132
    invoke-virtual {p0}, Lcom/android/server/wm/BlackFrame;->kill()V

    #@a2
    .line 135
    :cond_a2
    return-void

    #@a3
    .line 131
    :catchall_a3
    move-exception v0

    #@a4
    if-nez v9, :cond_a9

    #@a6
    .line 132
    invoke-virtual {p0}, Lcom/android/server/wm/BlackFrame;->kill()V

    #@a9
    :cond_a9
    throw v0
.end method


# virtual methods
.method public clearMatrix()V
    .registers 3

    #@0
    .prologue
    .line 171
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_16

    #@6
    .line 172
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@8
    aget-object v1, v1, v0

    #@a
    if-eqz v1, :cond_13

    #@c
    .line 173
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@e
    aget-object v1, v1, v0

    #@10
    invoke-virtual {v1}, Lcom/android/server/wm/BlackFrame$BlackSurface;->clearMatrix()V

    #@13
    .line 171
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_1

    #@16
    .line 176
    :cond_16
    return-void
.end method

.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 153
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@2
    if-eqz v1, :cond_1c

    #@4
    .line 154
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@7
    array-length v1, v1

    #@8
    if-ge v0, v1, :cond_1c

    #@a
    .line 155
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@c
    aget-object v1, v1, v0

    #@e
    if-eqz v1, :cond_19

    #@10
    .line 156
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@12
    aget-object v1, v1, v0

    #@14
    iget-object v1, v1, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@16
    invoke-virtual {v1}, Landroid/view/Surface;->hide()V

    #@19
    .line 154
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_5

    #@1c
    .line 160
    .end local v0           #i:I
    :cond_1c
    return-void
.end method

.method public kill()V
    .registers 4

    #@0
    .prologue
    .line 138
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@2
    if-eqz v1, :cond_21

    #@4
    .line 139
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@7
    array-length v1, v1

    #@8
    if-ge v0, v1, :cond_21

    #@a
    .line 140
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@c
    aget-object v1, v1, v0

    #@e
    if-eqz v1, :cond_1e

    #@10
    .line 145
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@12
    aget-object v1, v1, v0

    #@14
    iget-object v1, v1, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@16
    invoke-virtual {v1}, Landroid/view/Surface;->destroy()V

    #@19
    .line 146
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@1b
    const/4 v2, 0x0

    #@1c
    aput-object v2, v1, v0

    #@1e
    .line 139
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_5

    #@21
    .line 150
    .end local v0           #i:I
    :cond_21
    return-void
.end method

.method public printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 94
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v2, "Outer: "

    #@5
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v2, p0, Lcom/android/server/wm/BlackFrame;->mOuterRect:Landroid/graphics/Rect;

    #@a
    invoke-virtual {v2, p2}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@d
    .line 95
    const-string v2, " / Inner: "

    #@f
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    iget-object v2, p0, Lcom/android/server/wm/BlackFrame;->mInnerRect:Landroid/graphics/Rect;

    #@14
    invoke-virtual {v2, p2}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@17
    .line 96
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1a
    .line 97
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    iget-object v2, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@1d
    array-length v2, v2

    #@1e
    if-ge v1, v2, :cond_50

    #@20
    .line 98
    iget-object v2, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@22
    aget-object v0, v2, v1

    #@24
    .line 99
    .local v0, bs:Lcom/android/server/wm/BlackFrame$BlackSurface;
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27
    const-string v2, "#"

    #@29
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@2f
    .line 100
    const-string v2, ": "

    #@31
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@34
    iget-object v2, v0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@36
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@39
    .line 101
    const-string v2, " left="

    #@3b
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3e
    iget v2, v0, Lcom/android/server/wm/BlackFrame$BlackSurface;->left:I

    #@40
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    #@43
    .line 102
    const-string v2, " top="

    #@45
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@48
    iget v2, v0, Lcom/android/server/wm/BlackFrame$BlackSurface;->top:I

    #@4a
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(I)V

    #@4d
    .line 97
    add-int/lit8 v1, v1, 0x1

    #@4f
    goto :goto_1b

    #@50
    .line 104
    .end local v0           #bs:Lcom/android/server/wm/BlackFrame$BlackSurface;
    :cond_50
    return-void
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 163
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_16

    #@6
    .line 164
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@8
    aget-object v1, v1, v0

    #@a
    if-eqz v1, :cond_13

    #@c
    .line 165
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame;->mBlackSurfaces:[Lcom/android/server/wm/BlackFrame$BlackSurface;

    #@e
    aget-object v1, v1, v0

    #@10
    invoke-virtual {v1, p1}, Lcom/android/server/wm/BlackFrame$BlackSurface;->setMatrix(Landroid/graphics/Matrix;)V

    #@13
    .line 163
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_1

    #@16
    .line 168
    :cond_16
    return-void
.end method
