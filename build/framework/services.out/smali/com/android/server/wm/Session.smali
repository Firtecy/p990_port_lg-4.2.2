.class final Lcom/android/server/wm/Session;
.super Landroid/view/IWindowSession$Stub;
.source "Session.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final mClient:Lcom/android/internal/view/IInputMethodClient;

.field mClientDead:Z

.field final mInputContext:Lcom/android/internal/view/IInputContext;

.field mNumWindow:I

.field final mPid:I

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field final mStringName:Ljava/lang/String;

.field mSurfaceSession:Landroid/view/SurfaceSession;

.field final mUid:I


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;)V
    .registers 12
    .parameter "service"
    .parameter "client"
    .parameter "inputContext"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 65
    invoke-direct {p0}, Landroid/view/IWindowSession$Stub;-><init>()V

    #@4
    .line 61
    iput v5, p0, Lcom/android/server/wm/Session;->mNumWindow:I

    #@6
    .line 62
    iput-boolean v5, p0, Lcom/android/server/wm/Session;->mClientDead:Z

    #@8
    .line 66
    iput-object p1, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@a
    .line 67
    iput-object p2, p0, Lcom/android/server/wm/Session;->mClient:Lcom/android/internal/view/IInputMethodClient;

    #@c
    .line 68
    iput-object p3, p0, Lcom/android/server/wm/Session;->mInputContext:Lcom/android/internal/view/IInputContext;

    #@e
    .line 69
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@11
    move-result v5

    #@12
    iput v5, p0, Lcom/android/server/wm/Session;->mUid:I

    #@14
    .line 70
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@17
    move-result v5

    #@18
    iput v5, p0, Lcom/android/server/wm/Session;->mPid:I

    #@1a
    .line 71
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    .line 72
    .local v4, sb:Ljava/lang/StringBuilder;
    const-string v5, "Session{"

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 73
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@27
    move-result v5

    #@28
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 74
    const-string v5, " "

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 75
    iget v5, p0, Lcom/android/server/wm/Session;->mPid:I

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    .line 76
    iget v5, p0, Lcom/android/server/wm/Session;->mUid:I

    #@3b
    const/16 v6, 0x2710

    #@3d
    if-ge v5, v6, :cond_95

    #@3f
    .line 77
    const-string v5, ":"

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    .line 78
    iget v5, p0, Lcom/android/server/wm/Session;->mUid:I

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    .line 85
    :goto_49
    const-string v5, "}"

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    iput-object v5, p0, Lcom/android/server/wm/Session;->mStringName:Ljava/lang/String;

    #@54
    .line 88
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@56
    iget-object v6, v5, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@58
    monitor-enter v6

    #@59
    .line 89
    :try_start_59
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@5b
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@5d
    if-nez v5, :cond_73

    #@5f
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@61
    iget-boolean v5, v5, Lcom/android/server/wm/WindowManagerService;->mHaveInputMethods:Z

    #@63
    if-eqz v5, :cond_73

    #@65
    .line 90
    const-string v5, "input_method"

    #@67
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6a
    move-result-object v0

    #@6b
    .line 92
    .local v0, b:Landroid/os/IBinder;
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@6d
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodManager;

    #@70
    move-result-object v7

    #@71
    iput-object v7, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@73
    .line 94
    .end local v0           #b:Landroid/os/IBinder;
    :cond_73
    monitor-exit v6
    :try_end_74
    .catchall {:try_start_59 .. :try_end_74} :catchall_b2

    #@74
    .line 95
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@77
    move-result-wide v2

    #@78
    .line 99
    .local v2, ident:J
    :try_start_78
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7a
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@7c
    if-eqz v5, :cond_b5

    #@7e
    .line 100
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@80
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@82
    iget v6, p0, Lcom/android/server/wm/Session;->mUid:I

    #@84
    iget v7, p0, Lcom/android/server/wm/Session;->mPid:I

    #@86
    invoke-interface {v5, p2, p3, v6, v7}, Lcom/android/internal/view/IInputMethodManager;->addClient(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;II)V

    #@89
    .line 105
    :goto_89
    invoke-interface {p2}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@8c
    move-result-object v5

    #@8d
    const/4 v6, 0x0

    #@8e
    invoke-interface {v5, p0, v6}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_91
    .catchall {:try_start_78 .. :try_end_91} :catchall_cc
    .catch Landroid/os/RemoteException; {:try_start_78 .. :try_end_91} :catch_ba

    #@91
    .line 115
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@94
    .line 117
    :goto_94
    return-void

    #@95
    .line 80
    .end local v2           #ident:J
    :cond_95
    const-string v5, ":u"

    #@97
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    .line 81
    iget v5, p0, Lcom/android/server/wm/Session;->mUid:I

    #@9c
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    #@9f
    move-result v5

    #@a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a3
    .line 82
    const/16 v5, 0x61

    #@a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@a8
    .line 83
    iget v5, p0, Lcom/android/server/wm/Session;->mUid:I

    #@aa
    invoke-static {v5}, Landroid/os/UserHandle;->getAppId(I)I

    #@ad
    move-result v5

    #@ae
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    goto :goto_49

    #@b2
    .line 94
    :catchall_b2
    move-exception v5

    #@b3
    :try_start_b3
    monitor-exit v6
    :try_end_b4
    .catchall {:try_start_b3 .. :try_end_b4} :catchall_b2

    #@b4
    throw v5

    #@b5
    .line 103
    .restart local v2       #ident:J
    :cond_b5
    const/4 v5, 0x0

    #@b6
    :try_start_b6
    invoke-interface {p2, v5}, Lcom/android/internal/view/IInputMethodClient;->setUsingInputMethod(Z)V
    :try_end_b9
    .catchall {:try_start_b6 .. :try_end_b9} :catchall_cc
    .catch Landroid/os/RemoteException; {:try_start_b6 .. :try_end_b9} :catch_ba

    #@b9
    goto :goto_89

    #@ba
    .line 106
    :catch_ba
    move-exception v1

    #@bb
    .line 109
    .local v1, e:Landroid/os/RemoteException;
    :try_start_bb
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@bd
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@bf
    if-eqz v5, :cond_c8

    #@c1
    .line 110
    iget-object v5, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@c3
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@c5
    invoke-interface {v5, p2}, Lcom/android/internal/view/IInputMethodManager;->removeClient(Lcom/android/internal/view/IInputMethodClient;)V
    :try_end_c8
    .catchall {:try_start_bb .. :try_end_c8} :catchall_cc
    .catch Landroid/os/RemoteException; {:try_start_bb .. :try_end_c8} :catch_d1

    #@c8
    .line 115
    :cond_c8
    :goto_c8
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@cb
    goto :goto_94

    #@cc
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_cc
    move-exception v5

    #@cd
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@d0
    throw v5

    #@d1
    .line 112
    .restart local v1       #e:Landroid/os/RemoteException;
    :catch_d1
    move-exception v5

    #@d2
    goto :goto_c8
.end method


# virtual methods
.method public add(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;ILandroid/graphics/Rect;Landroid/view/InputChannel;)I
    .registers 15
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "outContentInsets"
    .parameter "outInputChannel"

    #@0
    .prologue
    .line 152
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move-object v6, p5

    #@7
    move-object v7, p6

    #@8
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/wm/Session;->addToDisplay(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public addToDisplay(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I
    .registers 17
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "displayId"
    .parameter "outContentInsets"
    .parameter "outInputChannel"

    #@0
    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p1

    #@4
    move v3, p2

    #@5
    move-object v4, p3

    #@6
    move v5, p4

    #@7
    move v6, p5

    #@8
    move-object v7, p6

    #@9
    move-object/from16 v8, p7

    #@b
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/wm/WindowManagerService;->addWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I

    #@e
    move-result v0

    #@f
    return v0
.end method

.method public addToDisplayWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;)I
    .registers 16
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "displayId"
    .parameter "outContentInsets"

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    const/4 v8, 0x0

    #@3
    move-object v1, p0

    #@4
    move-object v2, p1

    #@5
    move v3, p2

    #@6
    move-object v4, p3

    #@7
    move v5, p4

    #@8
    move v6, p5

    #@9
    move-object v7, p6

    #@a
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/wm/WindowManagerService;->addWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public addWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;ILandroid/graphics/Rect;)I
    .registers 13
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "outContentInsets"

    #@0
    .prologue
    .line 167
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move-object v6, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/wm/Session;->addToDisplayWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public binderDied()V
    .registers 4

    #@0
    .prologue
    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 138
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@8
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputMethodManager:Lcom/android/internal/view/IInputMethodManager;

    #@a
    iget-object v1, p0, Lcom/android/server/wm/Session;->mClient:Lcom/android/internal/view/IInputMethodClient;

    #@c
    invoke-interface {v0, v1}, Lcom/android/internal/view/IInputMethodManager;->removeClient(Lcom/android/internal/view/IInputMethodClient;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f} :catch_29

    #@f
    .line 142
    :cond_f
    :goto_f
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@11
    iget-object v1, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@13
    monitor-enter v1

    #@14
    .line 143
    :try_start_14
    iget-object v0, p0, Lcom/android/server/wm/Session;->mClient:Lcom/android/internal/view/IInputMethodClient;

    #@16
    invoke-interface {v0}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@19
    move-result-object v0

    #@1a
    const/4 v2, 0x0

    #@1b
    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@1e
    .line 144
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Lcom/android/server/wm/Session;->mClientDead:Z

    #@21
    .line 145
    invoke-virtual {p0}, Lcom/android/server/wm/Session;->killSessionLocked()V

    #@24
    .line 146
    monitor-exit v1

    #@25
    .line 147
    return-void

    #@26
    .line 146
    :catchall_26
    move-exception v0

    #@27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_14 .. :try_end_28} :catchall_26

    #@28
    throw v0

    #@29
    .line 140
    :catch_29
    move-exception v0

    #@2a
    goto :goto_f
.end method

.method public dragRecipientEntered(Landroid/view/IWindow;)V
    .registers 2
    .parameter "window"

    #@0
    .prologue
    .line 386
    return-void
.end method

.method public dragRecipientExited(Landroid/view/IWindow;)V
    .registers 2
    .parameter "window"

    #@0
    .prologue
    .line 392
    return-void
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 4
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 493
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "mNumWindow="

    #@5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget v0, p0, Lcom/android/server/wm/Session;->mNumWindow:I

    #@a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@d
    .line 494
    const-string v0, " mClientDead="

    #@f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    iget-boolean v0, p0, Lcom/android/server/wm/Session;->mClientDead:Z

    #@14
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@17
    .line 495
    const-string v0, " mSurfaceSession="

    #@19
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c
    iget-object v0, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@1e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@21
    .line 496
    return-void
.end method

.method public finishDrawing(Landroid/view/IWindow;)V
    .registers 3
    .parameter "window"

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p0, p1}, Lcom/android/server/wm/WindowManagerService;->finishDrawingWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;)V

    #@5
    .line 228
    return-void
.end method

.method public getDisplayFrame(Landroid/view/IWindow;Landroid/graphics/Rect;)V
    .registers 4
    .parameter "window"
    .parameter "outDisplayFrame"

    #@0
    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p0, p1, p2}, Lcom/android/server/wm/WindowManagerService;->getWindowDisplayFrame(Lcom/android/server/wm/Session;Landroid/view/IWindow;Landroid/graphics/Rect;)V

    #@5
    .line 217
    return-void
.end method

.method public getInTouchMode()Z
    .registers 3

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v1, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v1

    #@5
    .line 238
    :try_start_5
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7
    iget-boolean v0, v0, Lcom/android/server/wm/WindowManagerService;->mInTouchMode:Z

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 239
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public isWindowSplit(Landroid/view/IWindow;Landroid/graphics/Rect;)Z
    .registers 4
    .parameter "window"
    .parameter "outSplitWindowFrame"

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p0, p1, p2}, Lcom/android/server/wm/WindowManagerService;->isWindowSplit(Lcom/android/server/wm/Session;Landroid/view/IWindow;Landroid/graphics/Rect;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method killSessionLocked()V
    .registers 5

    #@0
    .prologue
    .line 472
    iget v1, p0, Lcom/android/server/wm/Session;->mNumWindow:I

    #@2
    if-gtz v1, :cond_1b

    #@4
    iget-boolean v1, p0, Lcom/android/server/wm/Session;->mClientDead:Z

    #@6
    if-eqz v1, :cond_1b

    #@8
    .line 473
    iget-object v1, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@a
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mSessions:Ljava/util/HashSet;

    #@c
    invoke-virtual {v1, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@f
    .line 474
    iget-object v1, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@11
    if-eqz v1, :cond_1b

    #@13
    .line 481
    :try_start_13
    iget-object v1, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@15
    invoke-virtual {v1}, Landroid/view/SurfaceSession;->kill()V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_18} :catch_1c

    #@18
    .line 487
    :goto_18
    const/4 v1, 0x0

    #@19
    iput-object v1, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@1b
    .line 490
    :cond_1b
    return-void

    #@1c
    .line 482
    :catch_1c
    move-exception v0

    #@1d
    .line 483
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "WindowManager"

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "Exception thrown when killing surface session "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    iget-object v3, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, " in session "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    const-string v3, ": "

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_18
.end method

.method public onRectangleOnScreenRequested(Landroid/os/IBinder;Landroid/graphics/Rect;Z)V
    .registers 8
    .parameter "token"
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 444
    iget-object v2, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v3

    #@5
    .line 445
    :try_start_5
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_18

    #@8
    move-result-wide v0

    #@9
    .line 447
    .local v0, identity:J
    :try_start_9
    iget-object v2, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@b
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/wm/WindowManagerService;->onRectangleOnScreenRequested(Landroid/os/IBinder;Landroid/graphics/Rect;Z)V
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_13

    #@e
    .line 449
    :try_start_e
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@11
    .line 451
    monitor-exit v3

    #@12
    .line 452
    return-void

    #@13
    .line 449
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2

    #@18
    .line 451
    .end local v0           #identity:J
    :catchall_18
    move-exception v2

    #@19
    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_e .. :try_end_1a} :catchall_18

    #@1a
    throw v2
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 123
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/IWindowSession$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result v1

    #@4
    return v1

    #@5
    .line 124
    :catch_5
    move-exception v0

    #@6
    .line 126
    .local v0, e:Ljava/lang/RuntimeException;
    instance-of v1, v0, Ljava/lang/SecurityException;

    #@8
    if-nez v1, :cond_11

    #@a
    .line 127
    const-string v1, "WindowManager"

    #@c
    const-string v2, "Window Session Crash"

    #@e
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 129
    :cond_11
    throw v0
.end method

.method public outOfMemory(Landroid/view/IWindow;)Z
    .registers 3
    .parameter "window"

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p0, p1}, Lcom/android/server/wm/WindowManagerService;->outOfMemoryWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public performDeferredDestroy(Landroid/view/IWindow;)V
    .registers 3
    .parameter "window"

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p0, p1}, Lcom/android/server/wm/WindowManagerService;->performDeferredDestroyWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;)V

    #@5
    .line 199
    return-void
.end method

.method public performDrag(Landroid/view/IWindow;Landroid/os/IBinder;FFFFLandroid/content/ClipData;)Z
    .registers 15
    .parameter "window"
    .parameter "dragToken"
    .parameter "touchX"
    .parameter "touchY"
    .parameter "thumbCenterX"
    .parameter "thumbCenterY"
    .parameter "data"

    #@0
    .prologue
    .line 270
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v4, v3, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v4

    #@5
    .line 271
    :try_start_5
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@9
    if-nez v3, :cond_1d

    #@b
    .line 272
    const-string v3, "WindowManager"

    #@d
    const-string v5, "No drag prepared"

    #@f
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 273
    new-instance v3, Ljava/lang/IllegalStateException;

    #@14
    const-string v5, "performDrag() without prepareDrag()"

    #@16
    invoke-direct {v3, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v3

    #@1a
    .line 337
    :catchall_1a
    move-exception v3

    #@1b
    monitor-exit v4
    :try_end_1c
    .catchall {:try_start_5 .. :try_end_1c} :catchall_1a

    #@1c
    throw v3

    #@1d
    .line 276
    :cond_1d
    :try_start_1d
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1f
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@21
    iget-object v3, v3, Lcom/android/server/wm/DragState;->mToken:Landroid/os/IBinder;

    #@23
    if-eq p2, v3, :cond_34

    #@25
    .line 277
    const-string v3, "WindowManager"

    #@27
    const-string v5, "Performing mismatched drag"

    #@29
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 278
    new-instance v3, Ljava/lang/IllegalStateException;

    #@2e
    const-string v5, "performDrag() does not match prepareDrag()"

    #@30
    invoke-direct {v3, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@33
    throw v3

    #@34
    .line 281
    :cond_34
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@36
    const/4 v5, 0x0

    #@37
    const/4 v6, 0x0

    #@38
    invoke-virtual {v3, v5, p1, v6}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;

    #@3b
    move-result-object v0

    #@3c
    .line 282
    .local v0, callingWin:Lcom/android/server/wm/WindowState;
    if-nez v0, :cond_59

    #@3e
    .line 283
    const-string v3, "WindowManager"

    #@40
    new-instance v5, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v6, "Bad requesting window "

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 284
    const/4 v3, 0x0

    #@57
    monitor-exit v4

    #@58
    .line 339
    :goto_58
    return v3

    #@59
    .line 291
    :cond_59
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@5b
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@5d
    const/16 v5, 0x14

    #@5f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@62
    move-result-object v6

    #@63
    invoke-virtual {v3, v5, v6}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(ILjava/lang/Object;)V

    #@66
    .line 299
    iget-object v3, v0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@68
    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;

    #@6b
    move-result-object v1

    #@6c
    .line 300
    .local v1, display:Landroid/view/Display;
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@6e
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@70
    invoke-virtual {v3, v1}, Lcom/android/server/wm/DragState;->register(Landroid/view/Display;)V

    #@73
    .line 301
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@75
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mInputMonitor:Lcom/android/server/wm/InputMonitor;

    #@77
    const/4 v5, 0x1

    #@78
    invoke-virtual {v3, v5}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@7b
    .line 302
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7d
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@7f
    iget-object v5, v0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@81
    iget-object v6, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@83
    iget-object v6, v6, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@85
    iget-object v6, v6, Lcom/android/server/wm/DragState;->mServerChannel:Landroid/view/InputChannel;

    #@87
    invoke-virtual {v3, v5, v6}, Lcom/android/server/input/InputManagerService;->transferTouchFocus(Landroid/view/InputChannel;Landroid/view/InputChannel;)Z

    #@8a
    move-result v3

    #@8b
    if-nez v3, :cond_ab

    #@8d
    .line 304
    const-string v3, "WindowManager"

    #@8f
    const-string v5, "Unable to transfer touch focus"

    #@91
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 305
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@96
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@98
    invoke-virtual {v3}, Lcom/android/server/wm/DragState;->unregister()V

    #@9b
    .line 306
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9d
    const/4 v5, 0x0

    #@9e
    iput-object v5, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@a0
    .line 307
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@a2
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mInputMonitor:Lcom/android/server/wm/InputMonitor;

    #@a4
    const/4 v5, 0x1

    #@a5
    invoke-virtual {v3, v5}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@a8
    .line 308
    const/4 v3, 0x0

    #@a9
    monitor-exit v4

    #@aa
    goto :goto_58

    #@ab
    .line 311
    :cond_ab
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@ad
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@af
    iput-object p7, v3, Lcom/android/server/wm/DragState;->mData:Landroid/content/ClipData;

    #@b1
    .line 312
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@b3
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@b5
    iput p3, v3, Lcom/android/server/wm/DragState;->mCurrentX:F

    #@b7
    .line 313
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@b9
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@bb
    iput p4, v3, Lcom/android/server/wm/DragState;->mCurrentY:F

    #@bd
    .line 314
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@bf
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@c1
    invoke-virtual {v3, p3, p4}, Lcom/android/server/wm/DragState;->broadcastDragStartedLw(FF)V

    #@c4
    .line 317
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@c6
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@c8
    iput p5, v3, Lcom/android/server/wm/DragState;->mThumbOffsetX:F

    #@ca
    .line 318
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@cc
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@ce
    iput p6, v3, Lcom/android/server/wm/DragState;->mThumbOffsetY:F

    #@d0
    .line 321
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@d2
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@d4
    iget-object v2, v3, Lcom/android/server/wm/DragState;->mSurface:Landroid/view/Surface;

    #@d6
    .line 324
    .local v2, surface:Landroid/view/Surface;
    invoke-static {}, Landroid/view/Surface;->openTransaction()V
    :try_end_d9
    .catchall {:try_start_1d .. :try_end_d9} :catchall_1a

    #@d9
    .line 326
    sub-float v3, p3, p5

    #@db
    sub-float v5, p4, p6

    #@dd
    :try_start_dd
    invoke-virtual {v2, v3, v5}, Landroid/view/Surface;->setPosition(FF)V

    #@e0
    .line 328
    const v3, 0x3f350481

    #@e3
    invoke-virtual {v2, v3}, Landroid/view/Surface;->setAlpha(F)V

    #@e6
    .line 329
    iget-object v3, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@e8
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@ea
    invoke-virtual {v3}, Lcom/android/server/wm/DragState;->getDragLayerLw()I

    #@ed
    move-result v3

    #@ee
    invoke-virtual {v2, v3}, Landroid/view/Surface;->setLayer(I)V

    #@f1
    .line 330
    invoke-virtual {v1}, Landroid/view/Display;->getLayerStack()I

    #@f4
    move-result v3

    #@f5
    invoke-virtual {v2, v3}, Landroid/view/Surface;->setLayerStack(I)V

    #@f8
    .line 331
    invoke-virtual {v2}, Landroid/view/Surface;->show()V
    :try_end_fb
    .catchall {:try_start_dd .. :try_end_fb} :catchall_102

    #@fb
    .line 333
    :try_start_fb
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@fe
    .line 337
    monitor-exit v4

    #@ff
    .line 339
    const/4 v3, 0x1

    #@100
    goto/16 :goto_58

    #@102
    .line 333
    :catchall_102
    move-exception v3

    #@103
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@106
    throw v3
    :try_end_107
    .catchall {:try_start_fb .. :try_end_107} :catchall_1a
.end method

.method public performHapticFeedback(Landroid/view/IWindow;IZ)Z
    .registers 10
    .parameter "window"
    .parameter "effectId"
    .parameter "always"

    #@0
    .prologue
    .line 244
    iget-object v2, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v3

    #@5
    .line 245
    :try_start_5
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_22

    #@8
    move-result-wide v0

    #@9
    .line 247
    .local v0, ident:J
    :try_start_9
    iget-object v2, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@b
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@d
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f
    const/4 v5, 0x1

    #@10
    invoke-virtual {v4, p0, p1, v5}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;

    #@13
    move-result-object v4

    #@14
    invoke-interface {v2, v4, p2, p3}, Landroid/view/WindowManagerPolicy;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z
    :try_end_17
    .catchall {:try_start_9 .. :try_end_17} :catchall_1d

    #@17
    move-result v2

    #@18
    .line 251
    :try_start_18
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1b
    monitor-exit v3

    #@1c
    return v2

    #@1d
    :catchall_1d
    move-exception v2

    #@1e
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@21
    throw v2

    #@22
    .line 253
    .end local v0           #ident:J
    :catchall_22
    move-exception v2

    #@23
    monitor-exit v3
    :try_end_24
    .catchall {:try_start_18 .. :try_end_24} :catchall_22

    #@24
    throw v2
.end method

.method public prepareDrag(Landroid/view/IWindow;IIILandroid/view/Surface;)Landroid/os/IBinder;
    .registers 13
    .parameter "window"
    .parameter "flags"
    .parameter "width"
    .parameter "height"
    .parameter "outSurface"

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v2, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@4
    move-object v1, p1

    #@5
    move v3, p2

    #@6
    move v4, p3

    #@7
    move v5, p4

    #@8
    move-object v6, p5

    #@9
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/wm/WindowManagerService;->prepareDragSurface(Landroid/view/IWindow;Landroid/view/SurfaceSession;IIILandroid/view/Surface;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public relayout(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I
    .registers 28
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "requestedWidth"
    .parameter "requestedHeight"
    .parameter "viewFlags"
    .parameter "flags"
    .parameter "outFrame"
    .parameter "outContentInsets"
    .parameter "outVisibleInsets"
    .parameter "outConfig"
    .parameter "outSurface"

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    move-object v1, p0

    #@3
    move-object/from16 v2, p1

    #@5
    move/from16 v3, p2

    #@7
    move-object/from16 v4, p3

    #@9
    move/from16 v5, p4

    #@b
    move/from16 v6, p5

    #@d
    move/from16 v7, p6

    #@f
    move/from16 v8, p7

    #@11
    move-object/from16 v9, p8

    #@13
    move-object/from16 v10, p9

    #@15
    move-object/from16 v11, p10

    #@17
    move-object/from16 v12, p11

    #@19
    move-object/from16 v13, p12

    #@1b
    invoke-virtual/range {v0 .. v13}, Lcom/android/server/wm/WindowManagerService;->relayoutWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I

    #@1e
    move-result v14

    #@1f
    .line 194
    .local v14, res:I
    return v14
.end method

.method public remove(Landroid/view/IWindow;)V
    .registers 3
    .parameter "window"

    #@0
    .prologue
    .line 179
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p0, p1}, Lcom/android/server/wm/WindowManagerService;->removeWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;)V

    #@5
    .line 180
    return-void
.end method

.method public reportDropResult(Landroid/view/IWindow;Z)V
    .registers 11
    .parameter "window"
    .parameter "consumed"

    #@0
    .prologue
    .line 343
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@3
    move-result-object v3

    #@4
    .line 348
    .local v3, token:Landroid/os/IBinder;
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@6
    iget-object v5, v4, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@8
    monitor-enter v5

    #@9
    .line 349
    :try_start_9
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_4c

    #@c
    move-result-wide v1

    #@d
    .line 351
    .local v1, ident:J
    :try_start_d
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@11
    if-nez v4, :cond_1f

    #@13
    .line 354
    const-string v4, "WindowManager"

    #@15
    const-string v6, "Drop result given but no drag in progress"

    #@17
    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a
    .catchall {:try_start_d .. :try_end_1a} :catchall_47

    #@1a
    .line 377
    :try_start_1a
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1d
    monitor-exit v5
    :try_end_1e
    .catchall {:try_start_1a .. :try_end_1e} :catchall_4c

    #@1e
    .line 380
    :goto_1e
    return-void

    #@1f
    .line 358
    :cond_1f
    :try_start_1f
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@21
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@23
    iget-object v4, v4, Lcom/android/server/wm/DragState;->mToken:Landroid/os/IBinder;

    #@25
    if-eq v4, v3, :cond_4f

    #@27
    .line 360
    const-string v4, "WindowManager"

    #@29
    new-instance v6, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v7, "Invalid drop-result claim by "

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 361
    new-instance v4, Ljava/lang/IllegalStateException;

    #@41
    const-string v6, "reportDropResult() by non-recipient"

    #@43
    invoke-direct {v4, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@46
    throw v4
    :try_end_47
    .catchall {:try_start_1f .. :try_end_47} :catchall_47

    #@47
    .line 377
    :catchall_47
    move-exception v4

    #@48
    :try_start_48
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b
    throw v4

    #@4c
    .line 379
    .end local v1           #ident:J
    :catchall_4c
    move-exception v4

    #@4d
    monitor-exit v5
    :try_end_4e
    .catchall {:try_start_48 .. :try_end_4e} :catchall_4c

    #@4e
    throw v4

    #@4f
    .line 367
    .restart local v1       #ident:J
    :cond_4f
    :try_start_4f
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@51
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@53
    const/16 v6, 0x15

    #@55
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v4, v6, v7}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(ILjava/lang/Object;)V

    #@5c
    .line 368
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@5e
    const/4 v6, 0x0

    #@5f
    const/4 v7, 0x0

    #@60
    invoke-virtual {v4, v6, p1, v7}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;

    #@63
    move-result-object v0

    #@64
    .line 369
    .local v0, callingWin:Lcom/android/server/wm/WindowState;
    if-nez v0, :cond_83

    #@66
    .line 370
    const-string v4, "WindowManager"

    #@68
    new-instance v6, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v7, "Bad result-reporting window "

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v6

    #@77
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v6

    #@7b
    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7e
    .catchall {:try_start_4f .. :try_end_7e} :catchall_47

    #@7e
    .line 377
    :try_start_7e
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@81
    monitor-exit v5
    :try_end_82
    .catchall {:try_start_7e .. :try_end_82} :catchall_4c

    #@82
    goto :goto_1e

    #@83
    .line 374
    :cond_83
    :try_start_83
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@85
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@87
    iput-boolean p2, v4, Lcom/android/server/wm/DragState;->mDragResult:Z

    #@89
    .line 375
    iget-object v4, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@8b
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@8d
    invoke-virtual {v4, v0}, Lcom/android/server/wm/DragState;->endDragLw(Lcom/android/server/wm/WindowState;)V
    :try_end_90
    .catchall {:try_start_83 .. :try_end_90} :catchall_47

    #@90
    .line 377
    :try_start_90
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@93
    .line 379
    monitor-exit v5
    :try_end_94
    .catchall {:try_start_90 .. :try_end_94} :catchall_4c

    #@94
    goto :goto_1e
.end method

.method public sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .registers 19
    .parameter "window"
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"
    .parameter "sync"

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v10, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v10

    #@5
    .line 414
    :try_start_5
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_29

    #@8
    move-result-wide v8

    #@9
    .line 416
    .local v8, ident:J
    :try_start_9
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@b
    iget-object v1, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@d
    const/4 v2, 0x1

    #@e
    invoke-virtual {v1, p0, p1, v2}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/os/IBinder;Z)Lcom/android/server/wm/WindowState;

    #@11
    move-result-object v1

    #@12
    move-object v2, p2

    #@13
    move v3, p3

    #@14
    move v4, p4

    #@15
    move/from16 v5, p5

    #@17
    move-object/from16 v6, p6

    #@19
    move/from16 v7, p7

    #@1b
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/wm/WindowManagerService;->sendWindowWallpaperCommandLocked(Lcom/android/server/wm/WindowState;Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_24

    #@1e
    move-result-object v0

    #@1f
    .line 420
    :try_start_1f
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    monitor-exit v10

    #@23
    return-object v0

    #@24
    :catchall_24
    move-exception v0

    #@25
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@28
    throw v0

    #@29
    .line 422
    .end local v8           #ident:J
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v10
    :try_end_2b
    .catchall {:try_start_1f .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method public setInTouchMode(Z)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v1, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v1

    #@5
    .line 232
    :try_start_5
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7
    iput-boolean p1, v0, Lcom/android/server/wm/WindowManagerService;->mInTouchMode:Z

    #@9
    .line 233
    monitor-exit v1

    #@a
    .line 234
    return-void

    #@b
    .line 233
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public setInsets(Landroid/view/IWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Region;)V
    .registers 13
    .parameter "window"
    .parameter "touchableInsets"
    .parameter "contentInsets"
    .parameter "visibleInsets"
    .parameter "touchableArea"

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p1

    #@4
    move v3, p2

    #@5
    move-object v4, p3

    #@6
    move-object v5, p4

    #@7
    move-object v6, p5

    #@8
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/wm/WindowManagerService;->setInsetsWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Region;)V

    #@b
    .line 213
    return-void
.end method

.method public setTransparentRegion(Landroid/view/IWindow;Landroid/graphics/Region;)V
    .registers 4
    .parameter "window"
    .parameter "region"

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p0, p1, p2}, Lcom/android/server/wm/WindowManagerService;->setTransparentRegionWindow(Lcom/android/server/wm/Session;Landroid/view/IWindow;Landroid/graphics/Region;)V

    #@5
    .line 207
    return-void
.end method

.method public setUniverseTransform(Landroid/os/IBinder;FFFFFFF)V
    .registers 21
    .parameter "window"
    .parameter "alpha"
    .parameter "offx"
    .parameter "offy"
    .parameter "dsdx"
    .parameter "dtdx"
    .parameter "dsdy"
    .parameter "dtdy"

    #@0
    .prologue
    .line 431
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v11, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v11

    #@5
    .line 432
    :try_start_5
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_2b

    #@8
    move-result-wide v9

    #@9
    .line 434
    .local v9, ident:J
    :try_start_9
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@b
    iget-object v1, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@d
    const/4 v2, 0x1

    #@e
    invoke-virtual {v1, p0, p1, v2}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/os/IBinder;Z)Lcom/android/server/wm/WindowState;

    #@11
    move-result-object v1

    #@12
    move v2, p2

    #@13
    move v3, p3

    #@14
    move/from16 v4, p4

    #@16
    move/from16 v5, p5

    #@18
    move/from16 v6, p6

    #@1a
    move/from16 v7, p7

    #@1c
    move/from16 v8, p8

    #@1e
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/wm/WindowManagerService;->setUniverseTransformLocked(Lcom/android/server/wm/WindowState;FFFFFFF)V
    :try_end_21
    .catchall {:try_start_9 .. :try_end_21} :catchall_26

    #@21
    .line 438
    :try_start_21
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@24
    .line 440
    monitor-exit v11

    #@25
    .line 441
    return-void

    #@26
    .line 438
    :catchall_26
    move-exception v0

    #@27
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2a
    throw v0

    #@2b
    .line 440
    .end local v9           #ident:J
    :catchall_2b
    move-exception v0

    #@2c
    monitor-exit v11
    :try_end_2d
    .catchall {:try_start_21 .. :try_end_2d} :catchall_2b

    #@2d
    throw v0
.end method

.method public setWallpaperPosition(Landroid/os/IBinder;FFFF)V
    .registers 15
    .parameter "window"
    .parameter "x"
    .parameter "y"
    .parameter "xStep"
    .parameter "yStep"

    #@0
    .prologue
    .line 395
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v8, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v8

    #@5
    .line 396
    :try_start_5
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_23

    #@8
    move-result-wide v6

    #@9
    .line 398
    .local v6, ident:J
    :try_start_9
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@b
    iget-object v1, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@d
    const/4 v2, 0x1

    #@e
    invoke-virtual {v1, p0, p1, v2}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/os/IBinder;Z)Lcom/android/server/wm/WindowState;

    #@11
    move-result-object v1

    #@12
    move v2, p2

    #@13
    move v3, p3

    #@14
    move v4, p4

    #@15
    move v5, p5

    #@16
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/wm/WindowManagerService;->setWindowWallpaperPositionLocked(Lcom/android/server/wm/WindowState;FFFF)V
    :try_end_19
    .catchall {:try_start_9 .. :try_end_19} :catchall_1e

    #@19
    .line 402
    :try_start_19
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    .line 404
    monitor-exit v8

    #@1d
    .line 405
    return-void

    #@1e
    .line 402
    :catchall_1e
    move-exception v0

    #@1f
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    throw v0

    #@23
    .line 404
    .end local v6           #ident:J
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v8
    :try_end_25
    .catchall {:try_start_19 .. :try_end_25} :catchall_23

    #@25
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 500
    iget-object v0, p0, Lcom/android/server/wm/Session;->mStringName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .registers 4
    .parameter "window"
    .parameter "result"

    #@0
    .prologue
    .line 426
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/WindowManagerService;->wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V

    #@5
    .line 427
    return-void
.end method

.method public wallpaperOffsetsComplete(Landroid/os/IBinder;)V
    .registers 3
    .parameter "window"

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/wm/WindowManagerService;->wallpaperOffsetsComplete(Landroid/os/IBinder;)V

    #@5
    .line 409
    return-void
.end method

.method windowAddedLocked()V
    .registers 2

    #@0
    .prologue
    .line 455
    iget-object v0, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@2
    if-nez v0, :cond_12

    #@4
    .line 458
    new-instance v0, Landroid/view/SurfaceSession;

    #@6
    invoke-direct {v0}, Landroid/view/SurfaceSession;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@b
    .line 461
    iget-object v0, p0, Lcom/android/server/wm/Session;->mService:Lcom/android/server/wm/WindowManagerService;

    #@d
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mSessions:Ljava/util/HashSet;

    #@f
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@12
    .line 463
    :cond_12
    iget v0, p0, Lcom/android/server/wm/Session;->mNumWindow:I

    #@14
    add-int/lit8 v0, v0, 0x1

    #@16
    iput v0, p0, Lcom/android/server/wm/Session;->mNumWindow:I

    #@18
    .line 464
    return-void
.end method

.method windowRemovedLocked()V
    .registers 2

    #@0
    .prologue
    .line 467
    iget v0, p0, Lcom/android/server/wm/Session;->mNumWindow:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/server/wm/Session;->mNumWindow:I

    #@6
    .line 468
    invoke-virtual {p0}, Lcom/android/server/wm/Session;->killSessionLocked()V

    #@9
    .line 469
    return-void
.end method
