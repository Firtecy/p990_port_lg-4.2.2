.class Lcom/android/server/wm/DragState$1;
.super Ljava/lang/Thread;
.source "DragState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/DragState;->dropFailAnimation(Landroid/graphics/PointF;Landroid/graphics/PointF;FFLandroid/view/Surface;Lcom/android/server/wm/WindowManagerService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/DragState;

.field final synthetic val$endP:Landroid/graphics/PointF;

.field final synthetic val$offsetX:F

.field final synthetic val$offsetY:F

.field final synthetic val$service:Lcom/android/server/wm/WindowManagerService;

.field final synthetic val$startP:Landroid/graphics/PointF;

.field final synthetic val$surface:Landroid/view/Surface;


# direct methods
.method constructor <init>(Lcom/android/server/wm/DragState;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/view/Surface;FFLcom/android/server/wm/WindowManagerService;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 330
    iput-object p1, p0, Lcom/android/server/wm/DragState$1;->this$0:Lcom/android/server/wm/DragState;

    #@2
    iput-object p2, p0, Lcom/android/server/wm/DragState$1;->val$endP:Landroid/graphics/PointF;

    #@4
    iput-object p3, p0, Lcom/android/server/wm/DragState$1;->val$startP:Landroid/graphics/PointF;

    #@6
    iput-object p4, p0, Lcom/android/server/wm/DragState$1;->val$surface:Landroid/view/Surface;

    #@8
    iput p5, p0, Lcom/android/server/wm/DragState$1;->val$offsetX:F

    #@a
    iput p6, p0, Lcom/android/server/wm/DragState$1;->val$offsetY:F

    #@c
    iput-object p7, p0, Lcom/android/server/wm/DragState$1;->val$service:Lcom/android/server/wm/WindowManagerService;

    #@e
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@11
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    const v8, 0x461c4000

    #@3
    .line 333
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$endP:Landroid/graphics/PointF;

    #@5
    iget v6, v6, Landroid/graphics/PointF;->x:F

    #@7
    iget-object v7, p0, Lcom/android/server/wm/DragState$1;->val$startP:Landroid/graphics/PointF;

    #@9
    iget v7, v7, Landroid/graphics/PointF;->x:F

    #@b
    sub-float v2, v6, v7

    #@d
    .line 334
    .local v2, varX:F
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$endP:Landroid/graphics/PointF;

    #@f
    iget v6, v6, Landroid/graphics/PointF;->y:F

    #@11
    iget-object v7, p0, Lcom/android/server/wm/DragState$1;->val$startP:Landroid/graphics/PointF;

    #@13
    iget v7, v7, Landroid/graphics/PointF;->y:F

    #@15
    sub-float v3, v6, v7

    #@17
    .line 335
    .local v3, varY:F
    div-float/2addr v2, v8

    #@18
    .line 336
    div-float/2addr v3, v8

    #@19
    .line 338
    const/4 v1, 0x2

    #@1a
    .local v1, i:I
    :goto_1a
    const/16 v6, 0x64

    #@1c
    if-ge v1, v6, :cond_59

    #@1e
    .line 339
    mul-int v6, v1, v1

    #@20
    int-to-float v0, v6

    #@21
    .line 340
    .local v0, dxy:F
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$endP:Landroid/graphics/PointF;

    #@23
    iget v6, v6, Landroid/graphics/PointF;->x:F

    #@25
    mul-float v7, v2, v0

    #@27
    sub-float v4, v6, v7

    #@29
    .line 341
    .local v4, x:F
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$endP:Landroid/graphics/PointF;

    #@2b
    iget v6, v6, Landroid/graphics/PointF;->y:F

    #@2d
    mul-float v7, v3, v0

    #@2f
    sub-float v5, v6, v7

    #@31
    .line 343
    .local v5, y:F
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@34
    .line 345
    :try_start_34
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$surface:Landroid/view/Surface;

    #@36
    iget v7, p0, Lcom/android/server/wm/DragState$1;->val$offsetX:F

    #@38
    sub-float v7, v4, v7

    #@3a
    iget v8, p0, Lcom/android/server/wm/DragState$1;->val$offsetY:F

    #@3c
    sub-float v8, v5, v8

    #@3e
    invoke-virtual {v6, v7, v8}, Landroid/view/Surface;->setPosition(FF)V

    #@41
    .line 346
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$surface:Landroid/view/Surface;

    #@43
    const v7, 0x3e960419

    #@46
    invoke-virtual {v6, v7}, Landroid/view/Surface;->setAlpha(F)V
    :try_end_49
    .catchall {:try_start_34 .. :try_end_49} :catchall_54

    #@49
    .line 348
    const-wide/16 v6, 0x2

    #@4b
    :try_start_4b
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_54
    .catch Ljava/lang/InterruptedException; {:try_start_4b .. :try_end_4e} :catch_66

    #@4e
    .line 352
    :goto_4e
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@51
    .line 338
    add-int/lit8 v1, v1, 0x1

    #@53
    goto :goto_1a

    #@54
    .line 352
    :catchall_54
    move-exception v6

    #@55
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@58
    throw v6

    #@59
    .line 355
    .end local v0           #dxy:F
    .end local v4           #x:F
    .end local v5           #y:F
    :cond_59
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$service:Lcom/android/server/wm/WindowManagerService;

    #@5b
    iget-object v6, v6, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@5d
    invoke-virtual {v6}, Lcom/android/server/wm/DragState;->reset()V

    #@60
    .line 356
    iget-object v6, p0, Lcom/android/server/wm/DragState$1;->val$service:Lcom/android/server/wm/WindowManagerService;

    #@62
    const/4 v7, 0x0

    #@63
    iput-object v7, v6, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@65
    .line 357
    return-void

    #@66
    .line 349
    .restart local v0       #dxy:F
    .restart local v4       #x:F
    .restart local v5       #y:F
    :catch_66
    move-exception v6

    #@67
    goto :goto_4e
.end method
