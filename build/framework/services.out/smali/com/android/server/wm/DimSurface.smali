.class Lcom/android/server/wm/DimSurface;
.super Ljava/lang/Object;
.source "DimSurface.java"


# static fields
.field static final TAG:Ljava/lang/String; = "DimSurface"


# instance fields
.field mDimColor:I

.field mDimShown:Z

.field mDimSurface:Landroid/view/Surface;

.field mLastDimHeight:I

.field mLastDimWidth:I

.field mLayer:I


# direct methods
.method constructor <init>(Landroid/view/SurfaceSession;I)V
    .registers 11
    .parameter "session"
    .parameter "layerStack"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, -0x1

    #@2
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 30
    iput-boolean v1, p0, Lcom/android/server/wm/DimSurface;->mDimShown:Z

    #@7
    .line 31
    iput v1, p0, Lcom/android/server/wm/DimSurface;->mDimColor:I

    #@9
    .line 32
    iput v0, p0, Lcom/android/server/wm/DimSurface;->mLayer:I

    #@b
    .line 43
    :try_start_b
    new-instance v0, Landroid/view/Surface;

    #@d
    const-string v2, "DimSurface"

    #@f
    const/16 v3, 0x10

    #@11
    const/16 v4, 0x10

    #@13
    const/4 v5, -0x1

    #@14
    const v6, 0x20004

    #@17
    move-object v1, p1

    #@18
    invoke-direct/range {v0 .. v6}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@1b
    iput-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@1d
    .line 50
    iget-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@1f
    invoke-virtual {v0, p2}, Landroid/view/Surface;->setLayerStack(I)V

    #@22
    .line 51
    iget-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@24
    const/4 v1, 0x0

    #@25
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setAlpha(F)V

    #@28
    .line 52
    iget-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@2a
    invoke-virtual {v0}, Landroid/view/Surface;->show()V
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_2d} :catch_2e

    #@2d
    .line 56
    :goto_2d
    return-void

    #@2e
    .line 53
    :catch_2e
    move-exception v7

    #@2f
    .line 54
    .local v7, e:Ljava/lang/Exception;
    const-string v0, "WindowManager"

    #@31
    const-string v1, "Exception creating Dim surface"

    #@33
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    goto :goto_2d
.end method


# virtual methods
.method hide()V
    .registers 4

    #@0
    .prologue
    .line 96
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 97
    const-string v1, "DimSurface"

    #@6
    const-string v2, "hide: no Surface"

    #@8
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 110
    :cond_b
    :goto_b
    return-void

    #@c
    .line 101
    :cond_c
    iget-boolean v1, p0, Lcom/android/server/wm/DimSurface;->mDimShown:Z

    #@e
    if-eqz v1, :cond_b

    #@10
    .line 102
    const/4 v1, 0x0

    #@11
    iput-boolean v1, p0, Lcom/android/server/wm/DimSurface;->mDimShown:Z

    #@13
    .line 105
    :try_start_13
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@15
    invoke-virtual {v1}, Landroid/view/Surface;->hide()V
    :try_end_18
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_18} :catch_19

    #@18
    goto :goto_b

    #@19
    .line 106
    :catch_19
    move-exception v0

    #@1a
    .line 107
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "WindowManager"

    #@1c
    const-string v2, "Illegal argument exception hiding dim surface"

    #@1e
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_b
.end method

.method kill()V
    .registers 2

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 114
    iget-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@6
    invoke-virtual {v0}, Landroid/view/Surface;->destroy()V

    #@9
    .line 115
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@c
    .line 117
    :cond_c
    return-void
.end method

.method public printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 120
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "mDimSurface="

    #@5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v0, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@d
    .line 121
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    const-string v0, "mDimShown="

    #@12
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    iget-boolean v0, p0, Lcom/android/server/wm/DimSurface;->mDimShown:Z

    #@17
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@1a
    .line 122
    const-string v0, " mLayer="

    #@1c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    iget v0, p0, Lcom/android/server/wm/DimSurface;->mLayer:I

    #@21
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@24
    .line 123
    const-string v0, " mDimColor=0x"

    #@26
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    iget v0, p0, Lcom/android/server/wm/DimSurface;->mDimColor:I

    #@2b
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 124
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@35
    const-string v0, "mLastDimWidth="

    #@37
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    iget v0, p0, Lcom/android/server/wm/DimSurface;->mLastDimWidth:I

    #@3c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@3f
    .line 125
    const-string v0, " mLastDimWidth="

    #@41
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@44
    iget v0, p0, Lcom/android/server/wm/DimSurface;->mLastDimWidth:I

    #@46
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@49
    .line 126
    return-void
.end method

.method show(IIII)V
    .registers 9
    .parameter "dw"
    .parameter "dh"
    .parameter "layer"
    .parameter "color"

    #@0
    .prologue
    .line 62
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 63
    const-string v1, "DimSurface"

    #@6
    const-string v2, "show: no Surface"

    #@8
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 93
    :cond_b
    :goto_b
    return-void

    #@c
    .line 67
    :cond_c
    iget-boolean v1, p0, Lcom/android/server/wm/DimSurface;->mDimShown:Z

    #@e
    if-nez v1, :cond_37

    #@10
    .line 70
    const/4 v1, 0x1

    #@11
    iput-boolean v1, p0, Lcom/android/server/wm/DimSurface;->mDimShown:Z

    #@13
    .line 72
    :try_start_13
    iput p1, p0, Lcom/android/server/wm/DimSurface;->mLastDimWidth:I

    #@15
    .line 73
    iput p2, p0, Lcom/android/server/wm/DimSurface;->mLastDimHeight:I

    #@17
    .line 74
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@19
    const/4 v2, 0x0

    #@1a
    const/4 v3, 0x0

    #@1b
    invoke-virtual {v1, v2, v3}, Landroid/view/Surface;->setPosition(II)V

    #@1e
    .line 75
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@20
    invoke-virtual {v1, p1, p2}, Landroid/view/Surface;->setSize(II)V

    #@23
    .line 76
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@25
    invoke-virtual {v1, p3}, Landroid/view/Surface;->setLayer(I)V

    #@28
    .line 77
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@2a
    invoke-virtual {v1}, Landroid/view/Surface;->show()V
    :try_end_2d
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_2d} :catch_2e

    #@2d
    goto :goto_b

    #@2e
    .line 78
    :catch_2e
    move-exception v0

    #@2f
    .line 79
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "WindowManager"

    #@31
    const-string v2, "Failure showing dim surface"

    #@33
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    goto :goto_b

    #@37
    .line 81
    .end local v0           #e:Ljava/lang/RuntimeException;
    :cond_37
    iget v1, p0, Lcom/android/server/wm/DimSurface;->mLastDimWidth:I

    #@39
    if-ne v1, p1, :cond_47

    #@3b
    iget v1, p0, Lcom/android/server/wm/DimSurface;->mLastDimHeight:I

    #@3d
    if-ne v1, p2, :cond_47

    #@3f
    iget v1, p0, Lcom/android/server/wm/DimSurface;->mDimColor:I

    #@41
    if-ne v1, p4, :cond_47

    #@43
    iget v1, p0, Lcom/android/server/wm/DimSurface;->mLayer:I

    #@45
    if-eq v1, p3, :cond_b

    #@47
    .line 85
    :cond_47
    iput p1, p0, Lcom/android/server/wm/DimSurface;->mLastDimWidth:I

    #@49
    .line 86
    iput p2, p0, Lcom/android/server/wm/DimSurface;->mLastDimHeight:I

    #@4b
    .line 87
    iput p3, p0, Lcom/android/server/wm/DimSurface;->mLayer:I

    #@4d
    .line 88
    iput p4, p0, Lcom/android/server/wm/DimSurface;->mDimColor:I

    #@4f
    .line 89
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@51
    invoke-virtual {v1, p1, p2}, Landroid/view/Surface;->setSize(II)V

    #@54
    .line 90
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@56
    invoke-virtual {v1, p3}, Landroid/view/Surface;->setLayer(I)V

    #@59
    .line 91
    iget-object v1, p0, Lcom/android/server/wm/DimSurface;->mDimSurface:Landroid/view/Surface;

    #@5b
    shr-int/lit8 v2, p4, 0x18

    #@5d
    and-int/lit16 v2, v2, 0xff

    #@5f
    int-to-float v2, v2

    #@60
    const/high16 v3, 0x437f

    #@62
    div-float/2addr v2, v3

    #@63
    invoke-virtual {v1, v2}, Landroid/view/Surface;->setAlpha(F)V

    #@66
    goto :goto_b
.end method
