.class Lcom/android/server/wm/BlackFrame$BlackSurface;
.super Ljava/lang/Object;
.source "BlackFrame.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/BlackFrame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BlackSurface"
.end annotation


# instance fields
.field final layer:I

.field final left:I

.field final surface:Landroid/view/Surface;

.field final synthetic this$0:Lcom/android/server/wm/BlackFrame;

.field final top:I


# direct methods
.method constructor <init>(Lcom/android/server/wm/BlackFrame;Landroid/view/SurfaceSession;IIIIII)V
    .registers 16
    .parameter
    .parameter "session"
    .parameter "layer"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"
    .parameter "layerStack"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    #@0
    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 40
    iput p4, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->left:I

    #@7
    .line 41
    iput p5, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->top:I

    #@9
    .line 42
    iput p3, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->layer:I

    #@b
    .line 43
    sub-int v3, p6, p4

    #@d
    .line 44
    .local v3, w:I
    sub-int v4, p7, p5

    #@f
    .line 50
    .local v4, h:I
    new-instance v0, Landroid/view/Surface;

    #@11
    const-string v2, "BlackSurface"

    #@13
    const/4 v5, -0x1

    #@14
    const v6, 0x20004

    #@17
    move-object v1, p2

    #@18
    invoke-direct/range {v0 .. v6}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@1b
    iput-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@1d
    .line 53
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@1f
    const/high16 v1, 0x3f80

    #@21
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setAlpha(F)V

    #@24
    .line 54
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@26
    invoke-virtual {v0, p8}, Landroid/view/Surface;->setLayerStack(I)V

    #@29
    .line 55
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@2b
    invoke-virtual {v0, p3}, Landroid/view/Surface;->setLayer(I)V

    #@2e
    .line 56
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@30
    invoke-virtual {v0}, Landroid/view/Surface;->show()V

    #@33
    .line 60
    return-void
.end method


# virtual methods
.method clearMatrix()V
    .registers 4

    #@0
    .prologue
    const/high16 v2, 0x3f80

    #@2
    const/4 v1, 0x0

    #@3
    .line 83
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@5
    invoke-virtual {v0, v2, v1, v1, v2}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@8
    .line 84
    return-void
.end method

.method setMatrix(Landroid/graphics/Matrix;)V
    .registers 8
    .parameter "matrix"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/BlackFrame;->mTmpMatrix:Landroid/graphics/Matrix;

    #@4
    iget v1, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->left:I

    #@6
    int-to-float v1, v1

    #@7
    iget v2, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->top:I

    #@9
    int-to-float v2, v2

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@d
    .line 64
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@f
    iget-object v0, v0, Lcom/android/server/wm/BlackFrame;->mTmpMatrix:Landroid/graphics/Matrix;

    #@11
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@14
    .line 65
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@16
    iget-object v0, v0, Lcom/android/server/wm/BlackFrame;->mTmpMatrix:Landroid/graphics/Matrix;

    #@18
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@1a
    iget-object v1, v1, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@1c
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    #@1f
    .line 66
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@21
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@23
    iget-object v1, v1, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@25
    const/4 v2, 0x2

    #@26
    aget v1, v1, v2

    #@28
    iget-object v2, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@2a
    iget-object v2, v2, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@2c
    const/4 v3, 0x5

    #@2d
    aget v2, v2, v3

    #@2f
    invoke-virtual {v0, v1, v2}, Landroid/view/Surface;->setPosition(FF)V

    #@32
    .line 68
    iget-object v0, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->surface:Landroid/view/Surface;

    #@34
    iget-object v1, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@36
    iget-object v1, v1, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@38
    const/4 v2, 0x0

    #@39
    aget v1, v1, v2

    #@3b
    iget-object v2, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@3d
    iget-object v2, v2, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@3f
    const/4 v3, 0x3

    #@40
    aget v2, v2, v3

    #@42
    iget-object v3, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@44
    iget-object v3, v3, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@46
    const/4 v4, 0x1

    #@47
    aget v3, v3, v4

    #@49
    iget-object v4, p0, Lcom/android/server/wm/BlackFrame$BlackSurface;->this$0:Lcom/android/server/wm/BlackFrame;

    #@4b
    iget-object v4, v4, Lcom/android/server/wm/BlackFrame;->mTmpFloats:[F

    #@4d
    const/4 v5, 0x4

    #@4e
    aget v4, v4, v5

    #@50
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@53
    .line 80
    return-void
.end method
