.class public Lcom/android/server/wm/MagnificationSpec;
.super Ljava/lang/Object;
.source "MagnificationSpec.java"


# instance fields
.field public mOffsetX:F

.field public mOffsetY:F

.field public mScale:F


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 20
    const/high16 v0, 0x3f80

    #@5
    iput v0, p0, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@7
    return-void
.end method


# virtual methods
.method public initialize(FFF)V
    .registers 4
    .parameter "scale"
    .parameter "offsetX"
    .parameter "offsetY"

    #@0
    .prologue
    .line 25
    iput p1, p0, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@2
    .line 26
    iput p2, p0, Lcom/android/server/wm/MagnificationSpec;->mOffsetX:F

    #@4
    .line 27
    iput p3, p0, Lcom/android/server/wm/MagnificationSpec;->mOffsetY:F

    #@6
    .line 28
    return-void
.end method

.method public isNop()Z
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 31
    iget v0, p0, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@3
    const/high16 v1, 0x3f80

    #@5
    cmpl-float v0, v0, v1

    #@7
    if-nez v0, :cond_17

    #@9
    iget v0, p0, Lcom/android/server/wm/MagnificationSpec;->mOffsetX:F

    #@b
    cmpl-float v0, v0, v2

    #@d
    if-nez v0, :cond_17

    #@f
    iget v0, p0, Lcom/android/server/wm/MagnificationSpec;->mOffsetY:F

    #@11
    cmpl-float v0, v0, v2

    #@13
    if-nez v0, :cond_17

    #@15
    const/4 v0, 0x1

    #@16
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 37
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "<scale:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 38
    iget v1, p0, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@f
    .line 39
    const-string v1, ",offsetX:"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 40
    iget v1, p0, Lcom/android/server/wm/MagnificationSpec;->mOffsetX:F

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@19
    .line 41
    const-string v1, ",offsetY:"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 42
    iget v1, p0, Lcom/android/server/wm/MagnificationSpec;->mOffsetY:F

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@23
    .line 43
    const-string v1, ">"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    return-object v1
.end method
