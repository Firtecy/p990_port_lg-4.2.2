.class Lcom/android/server/wm/AppWindowToken;
.super Lcom/android/server/wm/WindowToken;
.source "AppWindowToken.java"


# instance fields
.field final allAppWindows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/wm/WindowState;",
            ">;"
        }
    .end annotation
.end field

.field allDrawn:Z

.field appFullscreen:Z

.field final appToken:Landroid/view/IApplicationToken;

.field clientHidden:Z

.field firstWindowDrawn:Z

.field groupId:I

.field hiddenRequested:Z

.field inPendingTransaction:Z

.field inputDispatchingTimeoutNanos:J

.field isFullscreen:Z

.field lastTransactionSequence:J

.field final mAnimator:Lcom/android/server/wm/WindowAnimator;

.field final mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

.field final mInputApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

.field numDrawnWindows:I

.field numInterestingWindows:I

.field removed:Z

.field reportedDrawn:Z

.field reportedVisible:Z

.field requestedOrientation:I

.field screenId:I

.field showWhenLocked:Z

.field startingData:Lcom/android/server/wm/StartingData;

.field startingDisplayed:Z

.field startingMoved:Z

.field startingView:Landroid/view/View;

.field startingWindow:Lcom/android/server/wm/WindowState;

.field final userId:I

.field willBeHidden:Z


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;ILandroid/view/IApplicationToken;)V
    .registers 9
    .parameter "_service"
    .parameter "_userId"
    .parameter "_token"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 112
    invoke-interface {p3}, Landroid/view/IApplicationToken;->asBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x2

    #@7
    const/4 v2, 0x1

    #@8
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/wm/WindowToken;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;IZ)V

    #@b
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    #@d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@12
    .line 56
    iput v3, p0, Lcom/android/server/wm/AppWindowToken;->groupId:I

    #@14
    .line 58
    iput v3, p0, Lcom/android/server/wm/AppWindowToken;->requestedOrientation:I

    #@16
    .line 68
    const-wide/high16 v0, -0x8000

    #@18
    iput-wide v0, p0, Lcom/android/server/wm/AppWindowToken;->lastTransactionSequence:J

    #@1a
    .line 105
    iput v4, p0, Lcom/android/server/wm/AppWindowToken;->screenId:I

    #@1c
    .line 106
    iput-boolean v4, p0, Lcom/android/server/wm/AppWindowToken;->isFullscreen:Z

    #@1e
    .line 114
    iput p2, p0, Lcom/android/server/wm/AppWindowToken;->userId:I

    #@20
    .line 115
    iput-object p0, p0, Lcom/android/server/wm/WindowToken;->appWindowToken:Lcom/android/server/wm/AppWindowToken;

    #@22
    .line 116
    iput-object p3, p0, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@24
    .line 117
    new-instance v0, Lcom/android/server/input/InputApplicationHandle;

    #@26
    invoke-direct {v0, p0}, Lcom/android/server/input/InputApplicationHandle;-><init>(Ljava/lang/Object;)V

    #@29
    iput-object v0, p0, Lcom/android/server/wm/AppWindowToken;->mInputApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@2b
    .line 118
    iget-object v0, p0, Lcom/android/server/wm/WindowToken;->service:Lcom/android/server/wm/WindowManagerService;

    #@2d
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@2f
    iput-object v0, p0, Lcom/android/server/wm/AppWindowToken;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@31
    .line 119
    new-instance v0, Lcom/android/server/wm/AppWindowAnimator;

    #@33
    invoke-direct {v0, p0}, Lcom/android/server/wm/AppWindowAnimator;-><init>(Lcom/android/server/wm/AppWindowToken;)V

    #@36
    iput-object v0, p0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@38
    .line 120
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 4
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 237
    invoke-super {p0, p1, p2}, Lcom/android/server/wm/WindowToken;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@3
    .line 238
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@5
    if-eqz v0, :cond_19

    #@7
    .line 239
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a
    const-string v0, "app=true"

    #@c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f
    .line 240
    const-string v0, " userId="

    #@11
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14
    iget v0, p0, Lcom/android/server/wm/AppWindowToken;->userId:I

    #@16
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(I)V

    #@19
    .line 242
    :cond_19
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v0

    #@1f
    if-lez v0, :cond_2e

    #@21
    .line 243
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    const-string v0, "allAppWindows="

    #@26
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@2e
    .line 245
    :cond_2e
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31
    const-string v0, "groupId="

    #@33
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36
    iget v0, p0, Lcom/android/server/wm/AppWindowToken;->groupId:I

    #@38
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@3b
    .line 246
    const-string v0, " appFullscreen="

    #@3d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@40
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->appFullscreen:Z

    #@42
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@45
    .line 247
    const-string v0, " requestedOrientation="

    #@47
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    iget v0, p0, Lcom/android/server/wm/AppWindowToken;->requestedOrientation:I

    #@4c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(I)V

    #@4f
    .line 248
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@52
    const-string v0, "hiddenRequested="

    #@54
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@57
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@59
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@5c
    .line 249
    const-string v0, " clientHidden="

    #@5e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@61
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->clientHidden:Z

    #@63
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@66
    .line 250
    const-string v0, " willBeHidden="

    #@68
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6b
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->willBeHidden:Z

    #@6d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@70
    .line 251
    const-string v0, " reportedDrawn="

    #@72
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@75
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->reportedDrawn:Z

    #@77
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@7a
    .line 252
    const-string v0, " reportedVisible="

    #@7c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7f
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->reportedVisible:Z

    #@81
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@84
    .line 253
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->paused:Z

    #@86
    if-eqz v0, :cond_95

    #@88
    .line 254
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8b
    const-string v0, "paused="

    #@8d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@90
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->paused:Z

    #@92
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@95
    .line 256
    :cond_95
    iget v0, p0, Lcom/android/server/wm/AppWindowToken;->numInterestingWindows:I

    #@97
    if-nez v0, :cond_a7

    #@99
    iget v0, p0, Lcom/android/server/wm/AppWindowToken;->numDrawnWindows:I

    #@9b
    if-nez v0, :cond_a7

    #@9d
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->allDrawn:Z

    #@9f
    if-nez v0, :cond_a7

    #@a1
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@a3
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowAnimator;->allDrawn:Z

    #@a5
    if-eqz v0, :cond_e3

    #@a7
    .line 258
    :cond_a7
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@aa
    const-string v0, "numInterestingWindows="

    #@ac
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@af
    .line 259
    iget v0, p0, Lcom/android/server/wm/AppWindowToken;->numInterestingWindows:I

    #@b1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@b4
    .line 260
    const-string v0, " numDrawnWindows="

    #@b6
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b9
    iget v0, p0, Lcom/android/server/wm/AppWindowToken;->numDrawnWindows:I

    #@bb
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@be
    .line 261
    const-string v0, " inPendingTransaction="

    #@c0
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c3
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->inPendingTransaction:Z

    #@c5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@c8
    .line 262
    const-string v0, " allDrawn="

    #@ca
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cd
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->allDrawn:Z

    #@cf
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@d2
    .line 263
    const-string v0, " (animator="

    #@d4
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d7
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@d9
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowAnimator;->allDrawn:Z

    #@db
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@de
    .line 264
    const-string v0, ")"

    #@e0
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e3
    .line 266
    :cond_e3
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->inPendingTransaction:Z

    #@e5
    if-eqz v0, :cond_f4

    #@e7
    .line 267
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ea
    const-string v0, "inPendingTransaction="

    #@ec
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ef
    .line 268
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->inPendingTransaction:Z

    #@f1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@f4
    .line 270
    :cond_f4
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@f6
    if-nez v0, :cond_100

    #@f8
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->removed:Z

    #@fa
    if-nez v0, :cond_100

    #@fc
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->firstWindowDrawn:Z

    #@fe
    if-eqz v0, :cond_121

    #@100
    .line 271
    :cond_100
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@103
    const-string v0, "startingData="

    #@105
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@108
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@10a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@10d
    .line 272
    const-string v0, " removed="

    #@10f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@112
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->removed:Z

    #@114
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@117
    .line 273
    const-string v0, " firstWindowDrawn="

    #@119
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11c
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->firstWindowDrawn:Z

    #@11e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@121
    .line 275
    :cond_121
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@123
    if-nez v0, :cond_131

    #@125
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->startingView:Landroid/view/View;

    #@127
    if-nez v0, :cond_131

    #@129
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->startingDisplayed:Z

    #@12b
    if-nez v0, :cond_131

    #@12d
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->startingMoved:Z

    #@12f
    if-eqz v0, :cond_15c

    #@131
    .line 277
    :cond_131
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@134
    const-string v0, "startingWindow="

    #@136
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@139
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@13b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@13e
    .line 278
    const-string v0, " startingView="

    #@140
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@143
    iget-object v0, p0, Lcom/android/server/wm/AppWindowToken;->startingView:Landroid/view/View;

    #@145
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@148
    .line 279
    const-string v0, " startingDisplayed="

    #@14a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14d
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->startingDisplayed:Z

    #@14f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@152
    .line 280
    const-string v0, " startingMoved"

    #@154
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@157
    iget-boolean v0, p0, Lcom/android/server/wm/AppWindowToken;->startingMoved:Z

    #@159
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@15c
    .line 282
    :cond_15c
    return-void
.end method

.method findMainWindow()Lcom/android/server/wm/WindowState;
    .registers 5

    #@0
    .prologue
    .line 223
    iget-object v2, p0, Lcom/android/server/wm/WindowToken;->windows:Lcom/android/server/wm/WindowList;

    #@2
    invoke-virtual {v2}, Lcom/android/server/wm/WindowList;->size()I

    #@5
    move-result v0

    #@6
    .line 224
    .local v0, j:I
    :cond_6
    if-lez v0, :cond_21

    #@8
    .line 225
    add-int/lit8 v0, v0, -0x1

    #@a
    .line 226
    iget-object v2, p0, Lcom/android/server/wm/WindowToken;->windows:Lcom/android/server/wm/WindowList;

    #@c
    invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/wm/WindowState;

    #@12
    .line 227
    .local v1, win:Lcom/android/server/wm/WindowState;
    iget-object v2, v1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@14
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@16
    const/4 v3, 0x1

    #@17
    if-eq v2, v3, :cond_20

    #@19
    iget-object v2, v1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@1b
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1d
    const/4 v3, 0x3

    #@1e
    if-ne v2, v3, :cond_6

    #@20
    .line 232
    .end local v1           #win:Lcom/android/server/wm/WindowState;
    :cond_20
    :goto_20
    return-object v1

    #@21
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_20
.end method

.method sendAppVisibilityToClients()V
    .registers 6

    #@0
    .prologue
    .line 123
    iget-object v3, p0, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 124
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_2b

    #@9
    .line 125
    iget-object v3, p0, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/server/wm/WindowState;

    #@11
    .line 126
    .local v2, win:Lcom/android/server/wm/WindowState;
    iget-object v3, p0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@13
    if-ne v2, v3, :cond_1c

    #@15
    iget-boolean v3, p0, Lcom/android/server/wm/AppWindowToken;->clientHidden:Z

    #@17
    if-eqz v3, :cond_1c

    #@19
    .line 124
    :goto_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_7

    #@1c
    .line 133
    :cond_1c
    :try_start_1c
    iget-object v4, v2, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@1e
    iget-boolean v3, p0, Lcom/android/server/wm/AppWindowToken;->clientHidden:Z

    #@20
    if-nez v3, :cond_29

    #@22
    const/4 v3, 0x1

    #@23
    :goto_23
    invoke-interface {v4, v3}, Landroid/view/IWindow;->dispatchAppVisibility(Z)V
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_26} :catch_27

    #@26
    goto :goto_19

    #@27
    .line 134
    :catch_27
    move-exception v3

    #@28
    goto :goto_19

    #@29
    .line 133
    :cond_29
    const/4 v3, 0x0

    #@2a
    goto :goto_23

    #@2b
    .line 137
    .end local v2           #win:Lcom/android/server/wm/WindowState;
    :cond_2b
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 286
    iget-object v1, p0, Lcom/android/server/wm/WindowToken;->stringName:Ljava/lang/String;

    #@2
    if-nez v1, :cond_2e

    #@4
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 288
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "AppWindowToken{"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 289
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@11
    move-result v1

    #@12
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 290
    const-string v1, " token="

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    iget-object v1, p0, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    const/16 v1, 0x7d

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    .line 291
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    iput-object v1, p0, Lcom/android/server/wm/WindowToken;->stringName:Ljava/lang/String;

    #@2e
    .line 293
    .end local v0           #sb:Ljava/lang/StringBuilder;
    :cond_2e
    iget-object v1, p0, Lcom/android/server/wm/WindowToken;->stringName:Ljava/lang/String;

    #@30
    return-object v1
.end method

.method updateReportedVisibilityLocked()V
    .registers 16

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 140
    iget-object v12, p0, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@4
    if-nez v12, :cond_7

    #@6
    .line 220
    :cond_6
    :goto_6
    return-void

    #@7
    .line 144
    :cond_7
    const/4 v7, 0x0

    #@8
    .line 145
    .local v7, numInteresting:I
    const/4 v8, 0x0

    #@9
    .line 146
    .local v8, numVisible:I
    const/4 v6, 0x0

    #@a
    .line 147
    .local v6, numDrawn:I
    const/4 v4, 0x1

    #@b
    .line 151
    .local v4, nowGone:Z
    iget-object v12, p0, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v0

    #@11
    .line 152
    .local v0, N:I
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v0, :cond_56

    #@14
    .line 153
    iget-object v12, p0, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v9

    #@1a
    check-cast v9, Lcom/android/server/wm/WindowState;

    #@1c
    .line 154
    .local v9, win:Lcom/android/server/wm/WindowState;
    iget-object v12, p0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@1e
    if-eq v9, v12, :cond_33

    #@20
    iget-boolean v12, v9, Lcom/android/server/wm/WindowState;->mAppFreezing:Z

    #@22
    if-nez v12, :cond_33

    #@24
    iget v12, v9, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@26
    if-nez v12, :cond_33

    #@28
    iget-object v12, v9, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@2a
    iget v12, v12, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2c
    const/4 v13, 0x3

    #@2d
    if-eq v12, v13, :cond_33

    #@2f
    iget-boolean v12, v9, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@31
    if-eqz v12, :cond_36

    #@33
    .line 152
    :cond_33
    :goto_33
    add-int/lit8 v1, v1, 0x1

    #@35
    goto :goto_12

    #@36
    .line 175
    :cond_36
    add-int/lit8 v7, v7, 0x1

    #@38
    .line 176
    invoke-virtual {v9}, Lcom/android/server/wm/WindowState;->isDrawnLw()Z

    #@3b
    move-result v12

    #@3c
    if-eqz v12, :cond_4c

    #@3e
    .line 177
    add-int/lit8 v6, v6, 0x1

    #@40
    .line 178
    iget-object v12, v9, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@42
    invoke-virtual {v12}, Lcom/android/server/wm/WindowStateAnimator;->isAnimating()Z

    #@45
    move-result v12

    #@46
    if-nez v12, :cond_4a

    #@48
    .line 179
    add-int/lit8 v8, v8, 0x1

    #@4a
    .line 181
    :cond_4a
    const/4 v4, 0x0

    #@4b
    goto :goto_33

    #@4c
    .line 182
    :cond_4c
    iget-object v12, v9, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@4e
    invoke-virtual {v12}, Lcom/android/server/wm/WindowStateAnimator;->isAnimating()Z

    #@51
    move-result v12

    #@52
    if-eqz v12, :cond_33

    #@54
    .line 183
    const/4 v4, 0x0

    #@55
    goto :goto_33

    #@56
    .line 187
    .end local v9           #win:Lcom/android/server/wm/WindowState;
    :cond_56
    if-lez v7, :cond_a1

    #@58
    if-lt v6, v7, :cond_a1

    #@5a
    move v3, v10

    #@5b
    .line 188
    .local v3, nowDrawn:Z
    :goto_5b
    if-lez v7, :cond_a3

    #@5d
    if-lt v8, v7, :cond_a3

    #@5f
    move v5, v10

    #@60
    .line 189
    .local v5, nowVisible:Z
    :goto_60
    if-nez v4, :cond_6a

    #@62
    .line 191
    if-nez v3, :cond_66

    #@64
    .line 192
    iget-boolean v3, p0, Lcom/android/server/wm/AppWindowToken;->reportedDrawn:Z

    #@66
    .line 194
    :cond_66
    if-nez v5, :cond_6a

    #@68
    .line 195
    iget-boolean v5, p0, Lcom/android/server/wm/AppWindowToken;->reportedVisible:Z

    #@6a
    .line 200
    :cond_6a
    iget-boolean v12, p0, Lcom/android/server/wm/AppWindowToken;->reportedDrawn:Z

    #@6c
    if-eq v3, v12, :cond_83

    #@6e
    .line 201
    if-eqz v3, :cond_81

    #@70
    .line 202
    iget-object v12, p0, Lcom/android/server/wm/WindowToken;->service:Lcom/android/server/wm/WindowManagerService;

    #@72
    iget-object v12, v12, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@74
    const/16 v13, 0x9

    #@76
    invoke-virtual {v12, v13, p0}, Lcom/android/server/wm/WindowManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@79
    move-result-object v2

    #@7a
    .line 204
    .local v2, m:Landroid/os/Message;
    iget-object v12, p0, Lcom/android/server/wm/WindowToken;->service:Lcom/android/server/wm/WindowManagerService;

    #@7c
    iget-object v12, v12, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@7e
    invoke-virtual {v12, v2}, Lcom/android/server/wm/WindowManagerService$H;->sendMessage(Landroid/os/Message;)Z

    #@81
    .line 206
    .end local v2           #m:Landroid/os/Message;
    :cond_81
    iput-boolean v3, p0, Lcom/android/server/wm/AppWindowToken;->reportedDrawn:Z

    #@83
    .line 208
    :cond_83
    iget-boolean v12, p0, Lcom/android/server/wm/AppWindowToken;->reportedVisible:Z

    #@85
    if-eq v5, v12, :cond_6

    #@87
    .line 212
    iput-boolean v5, p0, Lcom/android/server/wm/AppWindowToken;->reportedVisible:Z

    #@89
    .line 213
    iget-object v12, p0, Lcom/android/server/wm/WindowToken;->service:Lcom/android/server/wm/WindowManagerService;

    #@8b
    iget-object v13, v12, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@8d
    const/16 v14, 0x8

    #@8f
    if-eqz v5, :cond_a5

    #@91
    move v12, v10

    #@92
    :goto_92
    if-eqz v4, :cond_a7

    #@94
    :goto_94
    invoke-virtual {v13, v14, v12, v10, p0}, Lcom/android/server/wm/WindowManagerService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@97
    move-result-object v2

    #@98
    .line 218
    .restart local v2       #m:Landroid/os/Message;
    iget-object v10, p0, Lcom/android/server/wm/WindowToken;->service:Lcom/android/server/wm/WindowManagerService;

    #@9a
    iget-object v10, v10, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@9c
    invoke-virtual {v10, v2}, Lcom/android/server/wm/WindowManagerService$H;->sendMessage(Landroid/os/Message;)Z

    #@9f
    goto/16 :goto_6

    #@a1
    .end local v2           #m:Landroid/os/Message;
    .end local v3           #nowDrawn:Z
    .end local v5           #nowVisible:Z
    :cond_a1
    move v3, v11

    #@a2
    .line 187
    goto :goto_5b

    #@a3
    .restart local v3       #nowDrawn:Z
    :cond_a3
    move v5, v11

    #@a4
    .line 188
    goto :goto_60

    #@a5
    .restart local v5       #nowVisible:Z
    :cond_a5
    move v12, v11

    #@a6
    .line 213
    goto :goto_92

    #@a7
    :cond_a7
    move v10, v11

    #@a8
    goto :goto_94
.end method
