.class Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;
.super Ljava/lang/Object;
.source "WindowManagerService.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisplayContentsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/android/server/wm/DisplayContent;",
        ">;"
    }
.end annotation


# instance fields
.field private cur:I

.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 12676
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .registers 3

    #@0
    .prologue
    .line 12681
    iget v0, p0, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->cur:I

    #@2
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4
    invoke-static {v1}, Lcom/android/server/wm/WindowManagerService;->access$3200(Lcom/android/server/wm/WindowManagerService;)Landroid/util/SparseArray;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@b
    move-result v1

    #@c
    if-ge v0, v1, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public next()Lcom/android/server/wm/DisplayContent;
    .registers 4

    #@0
    .prologue
    .line 12686
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->hasNext()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_19

    #@6
    .line 12687
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@8
    invoke-static {v0}, Lcom/android/server/wm/WindowManagerService;->access$3200(Lcom/android/server/wm/WindowManagerService;)Landroid/util/SparseArray;

    #@b
    move-result-object v0

    #@c
    iget v1, p0, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->cur:I

    #@e
    add-int/lit8 v2, v1, 0x1

    #@10
    iput v2, p0, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->cur:I

    #@12
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Lcom/android/server/wm/DisplayContent;

    #@18
    return-object v0

    #@19
    .line 12689
    :cond_19
    new-instance v0, Ljava/util/NoSuchElementException;

    #@1b
    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    #@1e
    throw v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 12676
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerService$DisplayContentsIterator;->next()Lcom/android/server/wm/DisplayContent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public remove()V
    .registers 3

    #@0
    .prologue
    .line 12694
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2
    const-string v1, "AllDisplayContentIterator.remove not implemented"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method
