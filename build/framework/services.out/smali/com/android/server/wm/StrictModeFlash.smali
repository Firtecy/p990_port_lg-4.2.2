.class Lcom/android/server/wm/StrictModeFlash;
.super Ljava/lang/Object;
.source "StrictModeFlash.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "StrictModeFlash"


# instance fields
.field mDrawNeeded:Z

.field mLastDH:I

.field mLastDW:I

.field mSurface:Landroid/view/Surface;

.field final mThickness:I


# direct methods
.method public constructor <init>(Landroid/view/Display;Landroid/view/SurfaceSession;)V
    .registers 13
    .parameter "display"
    .parameter "session"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 36
    const/16 v0, 0x14

    #@7
    iput v0, p0, Lcom/android/server/wm/StrictModeFlash;->mThickness:I

    #@9
    .line 40
    :try_start_9
    new-instance v0, Landroid/view/Surface;

    #@b
    const-string v2, "StrictModeFlash"

    #@d
    const/4 v3, 0x1

    #@e
    const/4 v4, 0x1

    #@f
    const/4 v5, -0x3

    #@10
    const/4 v6, 0x4

    #@11
    move-object v1, p2

    #@12
    invoke-direct/range {v0 .. v6}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@15
    iput-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;
    :try_end_17
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_9 .. :try_end_17} :catch_35

    #@17
    .line 46
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@19
    invoke-virtual {p1}, Landroid/view/Display;->getLayerStack()I

    #@1c
    move-result v1

    #@1d
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setLayerStack(I)V

    #@20
    .line 47
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@22
    const v1, 0xf6950

    #@25
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setLayer(I)V

    #@28
    .line 48
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@2a
    invoke-virtual {v0, v9, v9}, Landroid/view/Surface;->setPosition(II)V

    #@2d
    .line 49
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@2f
    invoke-virtual {v0}, Landroid/view/Surface;->show()V

    #@32
    .line 50
    iput-boolean v8, p0, Lcom/android/server/wm/StrictModeFlash;->mDrawNeeded:Z

    #@34
    .line 51
    :goto_34
    return-void

    #@35
    .line 42
    :catch_35
    move-exception v7

    #@36
    .line 43
    .local v7, e:Landroid/view/Surface$OutOfResourcesException;
    goto :goto_34
.end method

.method private drawIfNeeded()V
    .registers 10

    #@0
    .prologue
    const/16 v8, 0x14

    #@2
    const/high16 v7, -0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 54
    iget-boolean v4, p0, Lcom/android/server/wm/StrictModeFlash;->mDrawNeeded:Z

    #@7
    if-nez v4, :cond_a

    #@9
    .line 86
    :cond_9
    :goto_9
    return-void

    #@a
    .line 57
    :cond_a
    iput-boolean v6, p0, Lcom/android/server/wm/StrictModeFlash;->mDrawNeeded:Z

    #@c
    .line 58
    iget v3, p0, Lcom/android/server/wm/StrictModeFlash;->mLastDW:I

    #@e
    .line 59
    .local v3, dw:I
    iget v1, p0, Lcom/android/server/wm/StrictModeFlash;->mLastDH:I

    #@10
    .line 61
    .local v1, dh:I
    new-instance v2, Landroid/graphics/Rect;

    #@12
    invoke-direct {v2, v6, v6, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@15
    .line 62
    .local v2, dirty:Landroid/graphics/Rect;
    const/4 v0, 0x0

    #@16
    .line 64
    .local v0, c:Landroid/graphics/Canvas;
    :try_start_16
    iget-object v4, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@18
    invoke-virtual {v4, v2}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    :try_end_1b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_16 .. :try_end_1b} :catch_5e
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_16 .. :try_end_1b} :catch_5c

    #@1b
    move-result-object v0

    #@1c
    .line 68
    :goto_1c
    if-eqz v0, :cond_9

    #@1e
    .line 73
    new-instance v4, Landroid/graphics/Rect;

    #@20
    invoke-direct {v4, v6, v6, v3, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    #@23
    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    #@25
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    #@28
    .line 74
    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    #@2b
    .line 76
    new-instance v4, Landroid/graphics/Rect;

    #@2d
    invoke-direct {v4, v6, v6, v8, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@30
    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    #@32
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    #@35
    .line 77
    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    #@38
    .line 79
    new-instance v4, Landroid/graphics/Rect;

    #@3a
    add-int/lit8 v5, v3, -0x14

    #@3c
    invoke-direct {v4, v5, v6, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@3f
    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    #@41
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    #@44
    .line 80
    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    #@47
    .line 82
    new-instance v4, Landroid/graphics/Rect;

    #@49
    add-int/lit8 v5, v1, -0x14

    #@4b
    invoke-direct {v4, v6, v5, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@4e
    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    #@50
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    #@53
    .line 83
    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    #@56
    .line 85
    iget-object v4, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@58
    invoke-virtual {v4, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    #@5b
    goto :goto_9

    #@5c
    .line 66
    :catch_5c
    move-exception v4

    #@5d
    goto :goto_1c

    #@5e
    .line 65
    :catch_5e
    move-exception v4

    #@5f
    goto :goto_1c
.end method


# virtual methods
.method positionSurface(II)V
    .registers 4
    .parameter "dw"
    .parameter "dh"

    #@0
    .prologue
    .line 103
    iget v0, p0, Lcom/android/server/wm/StrictModeFlash;->mLastDW:I

    #@2
    if-ne v0, p1, :cond_9

    #@4
    iget v0, p0, Lcom/android/server/wm/StrictModeFlash;->mLastDH:I

    #@6
    if-ne v0, p2, :cond_9

    #@8
    .line 110
    :goto_8
    return-void

    #@9
    .line 106
    :cond_9
    iput p1, p0, Lcom/android/server/wm/StrictModeFlash;->mLastDW:I

    #@b
    .line 107
    iput p2, p0, Lcom/android/server/wm/StrictModeFlash;->mLastDH:I

    #@d
    .line 108
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@f
    invoke-virtual {v0, p1, p2}, Landroid/view/Surface;->setSize(II)V

    #@12
    .line 109
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/server/wm/StrictModeFlash;->mDrawNeeded:Z

    #@15
    goto :goto_8
.end method

.method public setVisibility(Z)V
    .registers 3
    .parameter "on"

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 100
    :goto_4
    return-void

    #@5
    .line 94
    :cond_5
    invoke-direct {p0}, Lcom/android/server/wm/StrictModeFlash;->drawIfNeeded()V

    #@8
    .line 95
    if-eqz p1, :cond_10

    #@a
    .line 96
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@c
    invoke-virtual {v0}, Landroid/view/Surface;->show()V

    #@f
    goto :goto_4

    #@10
    .line 98
    :cond_10
    iget-object v0, p0, Lcom/android/server/wm/StrictModeFlash;->mSurface:Landroid/view/Surface;

    #@12
    invoke-virtual {v0}, Landroid/view/Surface;->hide()V

    #@15
    goto :goto_4
.end method
