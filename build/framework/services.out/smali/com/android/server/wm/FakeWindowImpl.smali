.class public final Lcom/android/server/wm/FakeWindowImpl;
.super Ljava/lang/Object;
.source "FakeWindowImpl.java"

# interfaces
.implements Landroid/view/WindowManagerPolicy$FakeWindow;


# instance fields
.field final mApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

.field final mClientChannel:Landroid/view/InputChannel;

.field final mInputEventReceiver:Landroid/view/InputEventReceiver;

.field final mServerChannel:Landroid/view/InputChannel;

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field mTouchFullscreen:Z

.field final mWindowHandle:Lcom/android/server/input/InputWindowHandle;

.field final mWindowLayer:I


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;Landroid/os/Looper;Landroid/view/InputEventReceiver$Factory;Ljava/lang/String;IIZZZ)V
    .registers 15
    .parameter "service"
    .parameter "looper"
    .parameter "inputEventReceiverFactory"
    .parameter "name"
    .parameter "windowType"
    .parameter "layoutParamsFlags"
    .parameter "canReceiveKeys"
    .parameter "hasFocus"
    .parameter "touchFullscreen"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    iput-object p1, p0, Lcom/android/server/wm/FakeWindowImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    #@5
    .line 47
    invoke-static {p4}, Landroid/view/InputChannel;->openInputChannelPair(Ljava/lang/String;)[Landroid/view/InputChannel;

    #@8
    move-result-object v0

    #@9
    .line 48
    .local v0, channels:[Landroid/view/InputChannel;
    const/4 v1, 0x0

    #@a
    aget-object v1, v0, v1

    #@c
    iput-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mServerChannel:Landroid/view/InputChannel;

    #@e
    .line 49
    const/4 v1, 0x1

    #@f
    aget-object v1, v0, v1

    #@11
    iput-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mClientChannel:Landroid/view/InputChannel;

    #@13
    .line 50
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    #@15
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@17
    iget-object v2, p0, Lcom/android/server/wm/FakeWindowImpl;->mServerChannel:Landroid/view/InputChannel;

    #@19
    const/4 v3, 0x0

    #@1a
    invoke-virtual {v1, v2, v3}, Lcom/android/server/input/InputManagerService;->registerInputChannel(Landroid/view/InputChannel;Lcom/android/server/input/InputWindowHandle;)V

    #@1d
    .line 52
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mClientChannel:Landroid/view/InputChannel;

    #@1f
    invoke-interface {p3, v1, p2}, Landroid/view/InputEventReceiver$Factory;->createInputEventReceiver(Landroid/view/InputChannel;Landroid/os/Looper;)Landroid/view/InputEventReceiver;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mInputEventReceiver:Landroid/view/InputEventReceiver;

    #@25
    .line 55
    new-instance v1, Lcom/android/server/input/InputApplicationHandle;

    #@27
    const/4 v2, 0x0

    #@28
    invoke-direct {v1, v2}, Lcom/android/server/input/InputApplicationHandle;-><init>(Ljava/lang/Object;)V

    #@2b
    iput-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@2d
    .line 56
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@2f
    iput-object p4, v1, Lcom/android/server/input/InputApplicationHandle;->name:Ljava/lang/String;

    #@31
    .line 57
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@33
    const-wide v2, 0x12a05f200L

    #@38
    iput-wide v2, v1, Lcom/android/server/input/InputApplicationHandle;->dispatchingTimeoutNanos:J

    #@3a
    .line 60
    new-instance v1, Lcom/android/server/input/InputWindowHandle;

    #@3c
    iget-object v2, p0, Lcom/android/server/wm/FakeWindowImpl;->mApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@3e
    const/4 v3, 0x0

    #@3f
    const/4 v4, 0x0

    #@40
    invoke-direct {v1, v2, v3, v4}, Lcom/android/server/input/InputWindowHandle;-><init>(Lcom/android/server/input/InputApplicationHandle;Ljava/lang/Object;I)V

    #@43
    iput-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@45
    .line 61
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@47
    iput-object p4, v1, Lcom/android/server/input/InputWindowHandle;->name:Ljava/lang/String;

    #@49
    .line 62
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@4b
    iget-object v2, p0, Lcom/android/server/wm/FakeWindowImpl;->mServerChannel:Landroid/view/InputChannel;

    #@4d
    iput-object v2, v1, Lcom/android/server/input/InputWindowHandle;->inputChannel:Landroid/view/InputChannel;

    #@4f
    .line 63
    invoke-direct {p0, p5}, Lcom/android/server/wm/FakeWindowImpl;->getLayerLw(I)I

    #@52
    move-result v1

    #@53
    iput v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowLayer:I

    #@55
    .line 64
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@57
    iget v2, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowLayer:I

    #@59
    iput v2, v1, Lcom/android/server/input/InputWindowHandle;->layer:I

    #@5b
    .line 65
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@5d
    iput p6, v1, Lcom/android/server/input/InputWindowHandle;->layoutParamsFlags:I

    #@5f
    .line 66
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@61
    iput p5, v1, Lcom/android/server/input/InputWindowHandle;->layoutParamsType:I

    #@63
    .line 67
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@65
    const-wide v2, 0x12a05f200L

    #@6a
    iput-wide v2, v1, Lcom/android/server/input/InputWindowHandle;->dispatchingTimeoutNanos:J

    #@6c
    .line 69
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@6e
    const/4 v2, 0x1

    #@6f
    iput-boolean v2, v1, Lcom/android/server/input/InputWindowHandle;->visible:Z

    #@71
    .line 70
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@73
    iput-boolean p7, v1, Lcom/android/server/input/InputWindowHandle;->canReceiveKeys:Z

    #@75
    .line 71
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@77
    iput-boolean p8, v1, Lcom/android/server/input/InputWindowHandle;->hasFocus:Z

    #@79
    .line 72
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@7b
    const/4 v2, 0x0

    #@7c
    iput-boolean v2, v1, Lcom/android/server/input/InputWindowHandle;->hasWallpaper:Z

    #@7e
    .line 73
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@80
    const/4 v2, 0x0

    #@81
    iput-boolean v2, v1, Lcom/android/server/input/InputWindowHandle;->paused:Z

    #@83
    .line 74
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@85
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@88
    move-result v2

    #@89
    iput v2, v1, Lcom/android/server/input/InputWindowHandle;->ownerPid:I

    #@8b
    .line 75
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@8d
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@90
    move-result v2

    #@91
    iput v2, v1, Lcom/android/server/input/InputWindowHandle;->ownerUid:I

    #@93
    .line 76
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@95
    const/4 v2, 0x0

    #@96
    iput v2, v1, Lcom/android/server/input/InputWindowHandle;->inputFeatures:I

    #@98
    .line 77
    iget-object v1, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@9a
    const/high16 v2, 0x3f80

    #@9c
    iput v2, v1, Lcom/android/server/input/InputWindowHandle;->scaleFactor:F

    #@9e
    .line 79
    iput-boolean p9, p0, Lcom/android/server/wm/FakeWindowImpl;->mTouchFullscreen:Z

    #@a0
    .line 80
    return-void
.end method

.method private getLayerLw(I)I
    .registers 3
    .parameter "windowType"

    #@0
    .prologue
    .line 107
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    invoke-interface {v0, p1}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    #@7
    move-result v0

    #@8
    mul-int/lit16 v0, v0, 0x2710

    #@a
    add-int/lit16 v0, v0, 0x3e8

    #@c
    return v0
.end method


# virtual methods
.method public dismiss()V
    .registers 4

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v1, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v1

    #@5
    .line 97
    :try_start_5
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7
    invoke-virtual {v0, p0}, Lcom/android/server/wm/WindowManagerService;->removeFakeWindowLocked(Landroid/view/WindowManagerPolicy$FakeWindow;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_25

    #@d
    .line 98
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mInputEventReceiver:Landroid/view/InputEventReceiver;

    #@f
    invoke-virtual {v0}, Landroid/view/InputEventReceiver;->dispose()V

    #@12
    .line 99
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    #@14
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@16
    iget-object v2, p0, Lcom/android/server/wm/FakeWindowImpl;->mServerChannel:Landroid/view/InputChannel;

    #@18
    invoke-virtual {v0, v2}, Lcom/android/server/input/InputManagerService;->unregisterInputChannel(Landroid/view/InputChannel;)V

    #@1b
    .line 100
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mClientChannel:Landroid/view/InputChannel;

    #@1d
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@20
    .line 101
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mServerChannel:Landroid/view/InputChannel;

    #@22
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@25
    .line 103
    :cond_25
    monitor-exit v1

    #@26
    .line 104
    return-void

    #@27
    .line 103
    :catchall_27
    move-exception v0

    #@28
    monitor-exit v1
    :try_end_29
    .catchall {:try_start_5 .. :try_end_29} :catchall_27

    #@29
    throw v0
.end method

.method layout(II)V
    .registers 5
    .parameter "dw"
    .parameter "dh"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 83
    iget-boolean v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mTouchFullscreen:Z

    #@3
    if-eqz v0, :cond_1d

    #@5
    .line 84
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@7
    iget-object v0, v0, Lcom/android/server/input/InputWindowHandle;->touchableRegion:Landroid/graphics/Region;

    #@9
    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/Region;->set(IIII)Z

    #@c
    .line 88
    :goto_c
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@e
    iput v1, v0, Lcom/android/server/input/InputWindowHandle;->frameLeft:I

    #@10
    .line 89
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@12
    iput v1, v0, Lcom/android/server/input/InputWindowHandle;->frameTop:I

    #@14
    .line 90
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@16
    iput p1, v0, Lcom/android/server/input/InputWindowHandle;->frameRight:I

    #@18
    .line 91
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@1a
    iput p2, v0, Lcom/android/server/input/InputWindowHandle;->frameBottom:I

    #@1c
    .line 92
    return-void

    #@1d
    .line 86
    :cond_1d
    iget-object v0, p0, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@1f
    iget-object v0, v0, Lcom/android/server/input/InputWindowHandle;->touchableRegion:Landroid/graphics/Region;

    #@21
    invoke-virtual {v0}, Landroid/graphics/Region;->setEmpty()V

    #@24
    goto :goto_c
.end method
