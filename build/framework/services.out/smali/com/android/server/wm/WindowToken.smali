.class Lcom/android/server/wm/WindowToken;
.super Ljava/lang/Object;
.source "WindowToken.java"


# instance fields
.field appWindowToken:Lcom/android/server/wm/AppWindowToken;

.field final explicit:Z

.field hasVisible:Z

.field hidden:Z

.field paused:Z

.field sendingToBottom:Z

.field final service:Lcom/android/server/wm/WindowManagerService;

.field stringName:Ljava/lang/String;

.field final token:Landroid/os/IBinder;

.field waitingToHide:Z

.field waitingToShow:Z

.field final windowType:I

.field final windows:Lcom/android/server/wm/WindowList;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;IZ)V
    .registers 6
    .parameter "_service"
    .parameter "_token"
    .parameter "type"
    .parameter "_explicit"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    new-instance v0, Lcom/android/server/wm/WindowList;

    #@5
    invoke-direct {v0}, Lcom/android/server/wm/WindowList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/wm/WindowToken;->windows:Lcom/android/server/wm/WindowList;

    #@a
    .line 54
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/server/wm/WindowToken;->paused:Z

    #@d
    .line 75
    iput-object p1, p0, Lcom/android/server/wm/WindowToken;->service:Lcom/android/server/wm/WindowManagerService;

    #@f
    .line 76
    iput-object p2, p0, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@11
    .line 77
    iput p3, p0, Lcom/android/server/wm/WindowToken;->windowType:I

    #@13
    .line 78
    iput-boolean p4, p0, Lcom/android/server/wm/WindowToken;->explicit:Z

    #@15
    .line 79
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 4
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 82
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "windows="

    #@5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v0, p0, Lcom/android/server/wm/WindowToken;->windows:Lcom/android/server/wm/WindowList;

    #@a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@d
    .line 83
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    const-string v0, "windowType="

    #@12
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    iget v0, p0, Lcom/android/server/wm/WindowToken;->windowType:I

    #@17
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1a
    .line 84
    const-string v0, " hidden="

    #@1c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@21
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@24
    .line 85
    const-string v0, " hasVisible="

    #@26
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->hasVisible:Z

    #@2b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@2e
    .line 86
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->waitingToShow:Z

    #@30
    if-nez v0, :cond_3a

    #@32
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->waitingToHide:Z

    #@34
    if-nez v0, :cond_3a

    #@36
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->sendingToBottom:Z

    #@38
    if-eqz v0, :cond_5b

    #@3a
    .line 87
    :cond_3a
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d
    const-string v0, "waitingToShow="

    #@3f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->waitingToShow:Z

    #@44
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@47
    .line 88
    const-string v0, " waitingToHide="

    #@49
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->waitingToHide:Z

    #@4e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@51
    .line 89
    const-string v0, " sendingToBottom="

    #@53
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56
    iget-boolean v0, p0, Lcom/android/server/wm/WindowToken;->sendingToBottom:Z

    #@58
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@5b
    .line 91
    :cond_5b
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 95
    iget-object v1, p0, Lcom/android/server/wm/WindowToken;->stringName:Ljava/lang/String;

    #@2
    if-nez v1, :cond_2e

    #@4
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 97
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "WindowToken{"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 98
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@11
    move-result v1

    #@12
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 99
    const-string v1, " "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    iget-object v1, p0, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    const/16 v1, 0x7d

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    iput-object v1, p0, Lcom/android/server/wm/WindowToken;->stringName:Ljava/lang/String;

    #@2e
    .line 102
    .end local v0           #sb:Ljava/lang/StringBuilder;
    :cond_2e
    iget-object v1, p0, Lcom/android/server/wm/WindowToken;->stringName:Ljava/lang/String;

    #@30
    return-object v1
.end method
