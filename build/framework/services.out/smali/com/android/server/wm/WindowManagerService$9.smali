.class Lcom/android/server/wm/WindowManagerService$9;
.super Ljava/lang/Object;
.source "WindowManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/WindowManagerService;->watchRotation(Landroid/view/IRotationWatcher;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;

.field final synthetic val$watcherBinder:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 7514
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$9;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    iput-object p2, p0, Lcom/android/server/wm/WindowManagerService$9;->val$watcherBinder:Landroid/os/IBinder;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 6

    #@0
    .prologue
    .line 7516
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$9;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4
    monitor-enter v3

    #@5
    .line 7517
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    :try_start_6
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$9;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@8
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mRotationWatchers:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v2

    #@e
    if-ge v0, v2, :cond_3b

    #@10
    .line 7518
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerService$9;->val$watcherBinder:Landroid/os/IBinder;

    #@12
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$9;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@14
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mRotationWatchers:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/view/IRotationWatcher;

    #@1c
    invoke-interface {v2}, Landroid/view/IRotationWatcher;->asBinder()Landroid/os/IBinder;

    #@1f
    move-result-object v2

    #@20
    if-ne v4, v2, :cond_38

    #@22
    .line 7519
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$9;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@24
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mRotationWatchers:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@29
    move-result-object v1

    #@2a
    check-cast v1, Landroid/view/IRotationWatcher;

    #@2c
    .line 7520
    .local v1, removed:Landroid/view/IRotationWatcher;
    if-eqz v1, :cond_36

    #@2e
    .line 7521
    invoke-interface {v1}, Landroid/view/IRotationWatcher;->asBinder()Landroid/os/IBinder;

    #@31
    move-result-object v2

    #@32
    const/4 v4, 0x0

    #@33
    invoke-interface {v2, p0, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@36
    .line 7523
    :cond_36
    add-int/lit8 v0, v0, -0x1

    #@38
    .line 7517
    .end local v1           #removed:Landroid/view/IRotationWatcher;
    :cond_38
    add-int/lit8 v0, v0, 0x1

    #@3a
    goto :goto_6

    #@3b
    .line 7526
    :cond_3b
    monitor-exit v3

    #@3c
    .line 7527
    return-void

    #@3d
    .line 7526
    :catchall_3d
    move-exception v2

    #@3e
    monitor-exit v3
    :try_end_3f
    .catchall {:try_start_6 .. :try_end_3f} :catchall_3d

    #@3f
    throw v2
.end method
