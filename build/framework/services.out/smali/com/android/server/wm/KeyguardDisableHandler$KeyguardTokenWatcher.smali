.class Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;
.super Landroid/os/TokenWatcher;
.source "KeyguardDisableHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/KeyguardDisableHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KeyguardTokenWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/KeyguardDisableHandler;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/KeyguardDisableHandler;Landroid/os/Handler;)V
    .registers 4
    .parameter
    .parameter "handler"

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->this$0:Lcom/android/server/wm/KeyguardDisableHandler;

    #@2
    .line 82
    const-string v0, "KeyguardDisableHandler"

    #@4
    invoke-direct {p0, p2, v0}, Landroid/os/TokenWatcher;-><init>(Landroid/os/Handler;Ljava/lang/String;)V

    #@7
    .line 83
    return-void
.end method


# virtual methods
.method public acquired()V
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 89
    iget-object v1, p0, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->this$0:Lcom/android/server/wm/KeyguardDisableHandler;

    #@4
    invoke-static {v1}, Lcom/android/server/wm/KeyguardDisableHandler;->access$000(Lcom/android/server/wm/KeyguardDisableHandler;)I

    #@7
    move-result v1

    #@8
    const/4 v4, -0x1

    #@9
    if-ne v1, v4, :cond_30

    #@b
    .line 90
    iget-object v1, p0, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->this$0:Lcom/android/server/wm/KeyguardDisableHandler;

    #@d
    iget-object v1, v1, Lcom/android/server/wm/KeyguardDisableHandler;->mContext:Landroid/content/Context;

    #@f
    const-string v4, "device_policy"

    #@11
    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    #@17
    .line 92
    .local v0, dpm:Landroid/app/admin/DevicePolicyManager;
    if-eqz v0, :cond_30

    #@19
    .line 94
    :try_start_19
    iget-object v4, p0, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->this$0:Lcom/android/server/wm/KeyguardDisableHandler;

    #@1b
    const/4 v1, 0x0

    #@1c
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1f
    move-result-object v5

    #@20
    invoke-interface {v5}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    #@23
    move-result-object v5

    #@24
    iget v5, v5, Landroid/content/pm/UserInfo;->id:I

    #@26
    invoke-virtual {v0, v1, v5}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I

    #@29
    move-result v1

    #@2a
    if-nez v1, :cond_40

    #@2c
    move v1, v2

    #@2d
    :goto_2d
    invoke-static {v4, v1}, Lcom/android/server/wm/KeyguardDisableHandler;->access$002(Lcom/android/server/wm/KeyguardDisableHandler;I)I
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_30} :catch_4a

    #@30
    .line 103
    .end local v0           #dpm:Landroid/app/admin/DevicePolicyManager;
    :cond_30
    :goto_30
    iget-object v1, p0, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->this$0:Lcom/android/server/wm/KeyguardDisableHandler;

    #@32
    invoke-static {v1}, Lcom/android/server/wm/KeyguardDisableHandler;->access$000(Lcom/android/server/wm/KeyguardDisableHandler;)I

    #@35
    move-result v1

    #@36
    if-ne v1, v2, :cond_42

    #@38
    .line 104
    iget-object v1, p0, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->this$0:Lcom/android/server/wm/KeyguardDisableHandler;

    #@3a
    iget-object v1, v1, Lcom/android/server/wm/KeyguardDisableHandler;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@3c
    invoke-interface {v1, v3}, Landroid/view/WindowManagerPolicy;->enableKeyguard(Z)V

    #@3f
    .line 108
    :goto_3f
    return-void

    #@40
    .restart local v0       #dpm:Landroid/app/admin/DevicePolicyManager;
    :cond_40
    move v1, v3

    #@41
    .line 94
    goto :goto_2d

    #@42
    .line 106
    .end local v0           #dpm:Landroid/app/admin/DevicePolicyManager;
    :cond_42
    const-string v1, "KeyguardDisableHandler"

    #@44
    const-string v2, "Not disabling keyguard since device policy is enforced"

    #@46
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_3f

    #@4a
    .line 98
    .restart local v0       #dpm:Landroid/app/admin/DevicePolicyManager;
    :catch_4a
    move-exception v1

    #@4b
    goto :goto_30
.end method

.method public released()V
    .registers 3

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->this$0:Lcom/android/server/wm/KeyguardDisableHandler;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/KeyguardDisableHandler;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    const/4 v1, 0x1

    #@5
    invoke-interface {v0, v1}, Landroid/view/WindowManagerPolicy;->enableKeyguard(Z)V

    #@8
    .line 113
    return-void
.end method
