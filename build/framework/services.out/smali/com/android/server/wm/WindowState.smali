.class final Lcom/android/server/wm/WindowState;
.super Ljava/lang/Object;
.source "WindowState.java"

# interfaces
.implements Landroid/view/WindowManagerPolicy$WindowState;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/WindowState$1;,
        Lcom/android/server/wm/WindowState$DeathRecipient;
    }
.end annotation


# static fields
.field static final DEBUG_VISIBILITY:Z = false

.field static final SHOW_LIGHT_TRANSACTIONS:Z = false

.field static final SHOW_SURFACE_ALLOC:Z = false

.field static final SHOW_TRANSACTIONS:Z = false

.field static final TAG:Ljava/lang/String; = "WindowState"


# instance fields
.field mAppFreezing:Z

.field mAppToken:Lcom/android/server/wm/AppWindowToken;

.field mAttachedHidden:Z

.field final mAttachedWindow:Lcom/android/server/wm/WindowState;

.field final mAttrs:Landroid/view/WindowManager$LayoutParams;

.field final mBaseLayer:I

.field final mChildWindows:Lcom/android/server/wm/WindowList;

.field final mClient:Landroid/view/IWindow;

.field final mCompatFrame:Landroid/graphics/Rect;

.field private mConfigHasChanged:Z

.field mConfiguration:Landroid/content/res/Configuration;

.field final mContainingFrame:Landroid/graphics/Rect;

.field mContentChanged:Z

.field final mContentFrame:Landroid/graphics/Rect;

.field final mContentInsets:Landroid/graphics/Rect;

.field mContentInsetsChanged:Z

.field final mContext:Landroid/content/Context;

.field final mDeathRecipient:Lcom/android/server/wm/WindowState$DeathRecipient;

.field mDestroying:Z

.field mDisplayContent:Lcom/android/server/wm/DisplayContent;

.field final mDisplayFrame:Landroid/graphics/Rect;

.field mEnforceSizeCompat:Z

.field mExiting:Z

.field final mFrame:Landroid/graphics/Rect;

.field final mGivenContentInsets:Landroid/graphics/Rect;

.field mGivenInsetsPending:Z

.field final mGivenTouchableRegion:Landroid/graphics/Region;

.field final mGivenVisibleInsets:Landroid/graphics/Rect;

.field mGlobalScale:F

.field mHScale:F

.field mHasSurface:Z

.field mHaveFrame:Z

.field mInputChannel:Landroid/view/InputChannel;

.field final mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

.field mInvGlobalScale:F

.field final mIsFloatingLayer:Z

.field final mIsImWindow:Z

.field mIsSplitFullScreen:Z

.field final mIsWallpaper:Z

.field final mLastContentInsets:Landroid/graphics/Rect;

.field final mLastFrame:Landroid/graphics/Rect;

.field mLastHScale:F

.field mLastRequestedHeight:I

.field mLastRequestedWidth:I

.field final mLastSystemDecorRect:Landroid/graphics/Rect;

.field mLastTitle:Ljava/lang/CharSequence;

.field mLastVScale:F

.field final mLastVisibleInsets:Landroid/graphics/Rect;

.field mLayer:I

.field final mLayoutAttached:Z

.field mLayoutNeeded:Z

.field mLayoutSeq:I

.field mObscured:Z

.field mOrientationChanging:Z

.field mOwnerUid:I

.field final mParentFrame:Landroid/graphics/Rect;

.field final mPolicy:Landroid/view/WindowManagerPolicy;

.field mPolicyVisibility:Z

.field mPolicyVisibilityAfterAnim:Z

.field mRebuilding:Z

.field mRelayoutCalled:Z

.field mRemoveOnExit:Z

.field mRemoved:Z

.field mRequestedHeight:I

.field mRequestedWidth:I

.field mRootToken:Lcom/android/server/wm/WindowToken;

.field mScreenId:I

.field mSeq:I

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field final mSession:Lcom/android/server/wm/Session;

.field private mShowToOwnerOnly:Z

.field final mShownFrame:Landroid/graphics/RectF;

.field mStringNameCache:Ljava/lang/String;

.field final mSubLayer:I

.field final mSystemDecorRect:Landroid/graphics/Rect;

.field mSystemUiVisibility:I

.field mTargetAppToken:Lcom/android/server/wm/AppWindowToken;

.field final mTmpMatrix:Landroid/graphics/Matrix;

.field mToken:Lcom/android/server/wm/WindowToken;

.field mTouchableInsets:I

.field mTurnOnScreen:Z

.field mUseWVGACompat:Z

.field mVScale:F

.field mViewVisibility:I

.field final mVisibleFrame:Landroid/graphics/Rect;

.field final mVisibleInsets:Landroid/graphics/Rect;

.field mVisibleInsetsChanged:Z

.field mWallpaperVisible:Z

.field mWallpaperX:F

.field mWallpaperXStep:F

.field mWallpaperY:F

.field mWallpaperYStep:F

.field mWasExiting:Z

.field final mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

.field mXOffset:I

.field mYOffset:I


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/Session;Landroid/view/IWindow;Lcom/android/server/wm/WindowToken;Lcom/android/server/wm/WindowState;ILandroid/view/WindowManager$LayoutParams;ILcom/android/server/wm/DisplayContent;)V
    .registers 21
    .parameter "service"
    .parameter "s"
    .parameter "c"
    .parameter "token"
    .parameter "attachedWindow"
    .parameter "seq"
    .parameter "a"
    .parameter "viewVisibility"
    .parameter "displayContent"

    #@0
    .prologue
    .line 289
    const/4 v10, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object/from16 v5, p5

    #@8
    move/from16 v6, p6

    #@a
    move-object/from16 v7, p7

    #@c
    move/from16 v8, p8

    #@e
    move-object/from16 v9, p9

    #@10
    invoke-direct/range {v0 .. v10}, Lcom/android/server/wm/WindowState;-><init>(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/Session;Landroid/view/IWindow;Lcom/android/server/wm/WindowToken;Lcom/android/server/wm/WindowState;ILandroid/view/WindowManager$LayoutParams;ILcom/android/server/wm/DisplayContent;Z)V

    #@13
    .line 290
    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/Session;Landroid/view/IWindow;Lcom/android/server/wm/WindowToken;Lcom/android/server/wm/WindowState;ILandroid/view/WindowManager$LayoutParams;ILcom/android/server/wm/DisplayContent;Z)V
    .registers 20
    .parameter "service"
    .parameter "s"
    .parameter "c"
    .parameter "token"
    .parameter "attachedWindow"
    .parameter "seq"
    .parameter "a"
    .parameter "viewVisibility"
    .parameter "displayContent"
    .parameter "mvpWin"

    #@0
    .prologue
    .line 294
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 85
    new-instance v6, Landroid/view/WindowManager$LayoutParams;

    #@5
    invoke-direct {v6}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@8
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@a
    .line 88
    new-instance v6, Lcom/android/server/wm/WindowList;

    #@c
    invoke-direct {v6}, Lcom/android/server/wm/WindowList;-><init>()V

    #@f
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@11
    .line 97
    const/4 v6, 0x0

    #@12
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mUseWVGACompat:Z

    #@14
    .line 100
    const/4 v6, 0x1

    #@15
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@17
    .line 101
    const/4 v6, 0x1

    #@18
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@1a
    .line 120
    const/4 v6, -0x1

    #@1b
    iput v6, p0, Lcom/android/server/wm/WindowState;->mLayoutSeq:I

    #@1d
    .line 122
    const/4 v6, 0x0

    #@1e
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@20
    .line 132
    new-instance v6, Landroid/graphics/RectF;

    #@22
    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    #@25
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@27
    .line 138
    new-instance v6, Landroid/graphics/Rect;

    #@29
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@2c
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mVisibleInsets:Landroid/graphics/Rect;

    #@2e
    .line 139
    new-instance v6, Landroid/graphics/Rect;

    #@30
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@33
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mLastVisibleInsets:Landroid/graphics/Rect;

    #@35
    .line 147
    new-instance v6, Landroid/graphics/Rect;

    #@37
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@3a
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mContentInsets:Landroid/graphics/Rect;

    #@3c
    .line 148
    new-instance v6, Landroid/graphics/Rect;

    #@3e
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@41
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mLastContentInsets:Landroid/graphics/Rect;

    #@43
    .line 161
    new-instance v6, Landroid/graphics/Rect;

    #@45
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@48
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mGivenContentInsets:Landroid/graphics/Rect;

    #@4a
    .line 167
    new-instance v6, Landroid/graphics/Rect;

    #@4c
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@4f
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mGivenVisibleInsets:Landroid/graphics/Rect;

    #@51
    .line 172
    new-instance v6, Landroid/graphics/Region;

    #@53
    invoke-direct {v6}, Landroid/graphics/Region;-><init>()V

    #@56
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mGivenTouchableRegion:Landroid/graphics/Region;

    #@58
    .line 180
    const/4 v6, 0x0

    #@59
    iput v6, p0, Lcom/android/server/wm/WindowState;->mTouchableInsets:I

    #@5b
    .line 186
    new-instance v6, Landroid/graphics/Rect;

    #@5d
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@60
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@62
    .line 187
    new-instance v6, Landroid/graphics/Rect;

    #@64
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@67
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mLastSystemDecorRect:Landroid/graphics/Rect;

    #@69
    .line 190
    const/high16 v6, 0x3f80

    #@6b
    iput v6, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@6d
    .line 191
    const/high16 v6, 0x3f80

    #@6f
    iput v6, p0, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@71
    .line 192
    const/high16 v6, 0x3f80

    #@73
    iput v6, p0, Lcom/android/server/wm/WindowState;->mHScale:F

    #@75
    const/high16 v6, 0x3f80

    #@77
    iput v6, p0, Lcom/android/server/wm/WindowState;->mVScale:F

    #@79
    .line 193
    const/high16 v6, 0x3f80

    #@7b
    iput v6, p0, Lcom/android/server/wm/WindowState;->mLastHScale:F

    #@7d
    const/high16 v6, 0x3f80

    #@7f
    iput v6, p0, Lcom/android/server/wm/WindowState;->mLastVScale:F

    #@81
    .line 194
    new-instance v6, Landroid/graphics/Matrix;

    #@83
    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    #@86
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mTmpMatrix:Landroid/graphics/Matrix;

    #@88
    .line 197
    new-instance v6, Landroid/graphics/Rect;

    #@8a
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@8d
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@8f
    .line 198
    new-instance v6, Landroid/graphics/Rect;

    #@91
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@94
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mLastFrame:Landroid/graphics/Rect;

    #@96
    .line 201
    new-instance v6, Landroid/graphics/Rect;

    #@98
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@9b
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@9d
    .line 203
    new-instance v6, Landroid/graphics/Rect;

    #@9f
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@a2
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mContainingFrame:Landroid/graphics/Rect;

    #@a4
    .line 204
    new-instance v6, Landroid/graphics/Rect;

    #@a6
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@a9
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mDisplayFrame:Landroid/graphics/Rect;

    #@ab
    .line 205
    new-instance v6, Landroid/graphics/Rect;

    #@ad
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@b0
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mContentFrame:Landroid/graphics/Rect;

    #@b2
    .line 206
    new-instance v6, Landroid/graphics/Rect;

    #@b4
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@b7
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mParentFrame:Landroid/graphics/Rect;

    #@b9
    .line 207
    new-instance v6, Landroid/graphics/Rect;

    #@bb
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@be
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mVisibleFrame:Landroid/graphics/Rect;

    #@c0
    .line 213
    const/high16 v6, -0x4080

    #@c2
    iput v6, p0, Lcom/android/server/wm/WindowState;->mWallpaperX:F

    #@c4
    .line 214
    const/high16 v6, -0x4080

    #@c6
    iput v6, p0, Lcom/android/server/wm/WindowState;->mWallpaperY:F

    #@c8
    .line 218
    const/high16 v6, -0x4080

    #@ca
    iput v6, p0, Lcom/android/server/wm/WindowState;->mWallpaperXStep:F

    #@cc
    .line 219
    const/high16 v6, -0x4080

    #@ce
    iput v6, p0, Lcom/android/server/wm/WindowState;->mWallpaperYStep:F

    #@d0
    .line 268
    const/4 v6, 0x0

    #@d1
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@d3
    .line 280
    const/4 v6, 0x0

    #@d4
    iput v6, p0, Lcom/android/server/wm/WindowState;->mScreenId:I

    #@d6
    .line 283
    const/4 v6, 0x0

    #@d7
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsSplitFullScreen:Z

    #@d9
    .line 296
    iput-object p1, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@db
    .line 297
    iput-object p2, p0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@dd
    .line 298
    iput-object p3, p0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@df
    .line 299
    iput-object p4, p0, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    #@e1
    .line 300
    iget v6, p2, Lcom/android/server/wm/Session;->mUid:I

    #@e3
    iput v6, p0, Lcom/android/server/wm/WindowState;->mOwnerUid:I

    #@e5
    .line 301
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@e7
    move-object/from16 v0, p7

    #@e9
    invoke-virtual {v6, v0}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    #@ec
    .line 302
    move/from16 v0, p8

    #@ee
    iput v0, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@f0
    .line 303
    move-object/from16 v0, p9

    #@f2
    iput-object v0, p0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@f4
    .line 304
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f6
    iget-object v6, v6, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@f8
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@fa
    .line 305
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@fc
    iget-object v6, v6, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@fe
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mContext:Landroid/content/Context;

    #@100
    .line 306
    new-instance v3, Lcom/android/server/wm/WindowState$DeathRecipient;

    #@102
    const/4 v6, 0x0

    #@103
    invoke-direct {v3, p0, v6}, Lcom/android/server/wm/WindowState$DeathRecipient;-><init>(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState$1;)V

    #@106
    .line 307
    .local v3, deathRecipient:Lcom/android/server/wm/WindowState$DeathRecipient;
    iput p6, p0, Lcom/android/server/wm/WindowState;->mSeq:I

    #@108
    .line 308
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@10a
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@10c
    const/high16 v7, 0x2000

    #@10e
    and-int/2addr v6, v7

    #@10f
    if-eqz v6, :cond_1dd

    #@111
    const/4 v6, 0x1

    #@112
    :goto_112
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@114
    .line 309
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@116
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@118
    and-int/lit16 v6, v6, 0x1000

    #@11a
    if-eqz v6, :cond_1e0

    #@11c
    const/4 v6, 0x1

    #@11d
    :goto_11d
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mUseWVGACompat:Z

    #@11f
    .line 314
    :try_start_11f
    invoke-interface {p3}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@122
    move-result-object v6

    #@123
    const/4 v7, 0x0

    #@124
    invoke-interface {v6, v3, v7}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_127
    .catch Landroid/os/RemoteException; {:try_start_11f .. :try_end_127} :catch_1e3

    #@127
    .line 328
    iput-object v3, p0, Lcom/android/server/wm/WindowState;->mDeathRecipient:Lcom/android/server/wm/WindowState$DeathRecipient;

    #@129
    .line 330
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@12b
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@12d
    const/16 v7, 0x3e8

    #@12f
    if-lt v6, v7, :cond_217

    #@131
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@133
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@135
    const/16 v7, 0x7cf

    #@137
    if-gt v6, v7, :cond_217

    #@139
    .line 334
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@13b
    iget-object v7, p5, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@13d
    iget v7, v7, Landroid/view/WindowManager$LayoutParams;->type:I

    #@13f
    invoke-interface {v6, v7}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    #@142
    move-result v6

    #@143
    mul-int/lit16 v6, v6, 0x2710

    #@145
    add-int/lit16 v6, v6, 0x3e8

    #@147
    iget-object v7, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@149
    iget-object v8, p5, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@14b
    invoke-virtual {v7, v8}, Lcom/android/server/wm/WindowManagerService;->getSecondaryWindowOffset(Landroid/view/WindowManager$LayoutParams;)I

    #@14e
    move-result v7

    #@14f
    add-int/2addr v6, v7

    #@150
    iput v6, p0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@152
    .line 338
    const-string v6, "SplitWidow"

    #@154
    new-instance v7, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v8, "This window is type phone.. check mBaseLayer after modification.. mBaseLayer = "

    #@15b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v7

    #@15f
    iget v8, p0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@161
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@164
    move-result-object v7

    #@165
    const-string v8, " of win "

    #@167
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v7

    #@16b
    iget-object v8, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@16d
    invoke-virtual {v8}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@170
    move-result-object v8

    #@171
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v7

    #@175
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@178
    move-result-object v7

    #@179
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@17c
    .line 339
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@17e
    move-object/from16 v0, p7

    #@180
    iget v7, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@182
    invoke-interface {v6, v7}, Landroid/view/WindowManagerPolicy;->subWindowTypeToLayerLw(I)I

    #@185
    move-result v6

    #@186
    iput v6, p0, Lcom/android/server/wm/WindowState;->mSubLayer:I

    #@188
    .line 340
    iput-object p5, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@18a
    .line 342
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@18c
    iget-object v6, v6, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@18e
    invoke-virtual {v6, p0}, Lcom/android/server/wm/WindowList;->add(Ljava/lang/Object;)Z

    #@191
    .line 343
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@193
    if-eqz v6, :cond_203

    #@195
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@197
    if-eqz v6, :cond_203

    #@199
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@19b
    invoke-virtual {v6}, Lcom/android/server/wm/WindowState;->getSplitScreenId()I

    #@19e
    move-result v6

    #@19f
    if-eqz v6, :cond_203

    #@1a1
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@1a3
    invoke-virtual {v6}, Lcom/android/server/wm/WindowState;->isSplitFullScreen()Z

    #@1a6
    move-result v6

    #@1a7
    if-nez v6, :cond_203

    #@1a9
    .line 344
    const/4 v6, 0x1

    #@1aa
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mLayoutAttached:Z

    #@1ac
    .line 349
    :goto_1ac
    iget-object v6, p5, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@1ae
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1b0
    const/16 v7, 0x7db

    #@1b2
    if-eq v6, v7, :cond_1bc

    #@1b4
    iget-object v6, p5, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@1b6
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1b8
    const/16 v7, 0x7dc

    #@1ba
    if-ne v6, v7, :cond_211

    #@1bc
    :cond_1bc
    const/4 v6, 0x1

    #@1bd
    :goto_1bd
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@1bf
    .line 351
    iget-object v6, p5, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@1c1
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1c3
    const/16 v7, 0x7dd

    #@1c5
    if-ne v6, v7, :cond_213

    #@1c7
    const/4 v6, 0x1

    #@1c8
    :goto_1c8
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@1ca
    .line 352
    iget-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@1cc
    if-nez v6, :cond_1d2

    #@1ce
    iget-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@1d0
    if-eqz v6, :cond_215

    #@1d2
    :cond_1d2
    const/4 v6, 0x1

    #@1d3
    :goto_1d3
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsFloatingLayer:Z

    #@1d5
    .line 376
    :goto_1d5
    move-object v2, p0

    #@1d6
    .line 377
    .local v2, appWin:Lcom/android/server/wm/WindowState;
    :goto_1d6
    iget-object v6, v2, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@1d8
    if-eqz v6, :cond_2ad

    #@1da
    .line 378
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@1dc
    goto :goto_1d6

    #@1dd
    .line 308
    .end local v2           #appWin:Lcom/android/server/wm/WindowState;
    :cond_1dd
    const/4 v6, 0x0

    #@1de
    goto/16 :goto_112

    #@1e0
    .line 309
    :cond_1e0
    const/4 v6, 0x0

    #@1e1
    goto/16 :goto_11d

    #@1e3
    .line 315
    :catch_1e3
    move-exception v4

    #@1e4
    .line 316
    .local v4, e:Landroid/os/RemoteException;
    const/4 v6, 0x0

    #@1e5
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mDeathRecipient:Lcom/android/server/wm/WindowState$DeathRecipient;

    #@1e7
    .line 317
    const/4 v6, 0x0

    #@1e8
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@1ea
    .line 318
    const/4 v6, 0x0

    #@1eb
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mLayoutAttached:Z

    #@1ed
    .line 319
    const/4 v6, 0x0

    #@1ee
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@1f0
    .line 320
    const/4 v6, 0x0

    #@1f1
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@1f3
    .line 321
    const/4 v6, 0x0

    #@1f4
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsFloatingLayer:Z

    #@1f6
    .line 322
    const/4 v6, 0x0

    #@1f7
    iput v6, p0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@1f9
    .line 323
    const/4 v6, 0x0

    #@1fa
    iput v6, p0, Lcom/android/server/wm/WindowState;->mSubLayer:I

    #@1fc
    .line 324
    const/4 v6, 0x0

    #@1fd
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@1ff
    .line 325
    const/4 v6, 0x0

    #@200
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@202
    .line 414
    .end local v4           #e:Landroid/os/RemoteException;
    :goto_202
    return-void

    #@203
    .line 346
    :cond_203
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@205
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@207
    const/16 v7, 0x3eb

    #@209
    if-eq v6, v7, :cond_20f

    #@20b
    const/4 v6, 0x1

    #@20c
    :goto_20c
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mLayoutAttached:Z

    #@20e
    goto :goto_1ac

    #@20f
    :cond_20f
    const/4 v6, 0x0

    #@210
    goto :goto_20c

    #@211
    .line 349
    :cond_211
    const/4 v6, 0x0

    #@212
    goto :goto_1bd

    #@213
    .line 351
    :cond_213
    const/4 v6, 0x0

    #@214
    goto :goto_1c8

    #@215
    .line 352
    :cond_215
    const/4 v6, 0x0

    #@216
    goto :goto_1d3

    #@217
    .line 356
    :cond_217
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@219
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@21b
    const/16 v7, 0x7d2

    #@21d
    if-ne v6, v7, :cond_296

    #@21f
    .line 357
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@221
    move-object/from16 v0, p7

    #@223
    iget v7, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@225
    invoke-interface {v6, v7}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    #@228
    move-result v6

    #@229
    mul-int/lit16 v6, v6, 0x2710

    #@22b
    add-int/lit16 v6, v6, 0x3e8

    #@22d
    iget-object v7, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@22f
    iget-object v8, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@231
    invoke-virtual {v7, v8}, Lcom/android/server/wm/WindowManagerService;->getSecondaryWindowOffset(Landroid/view/WindowManager$LayoutParams;)I

    #@234
    move-result v7

    #@235
    add-int/2addr v6, v7

    #@236
    iput v6, p0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@238
    .line 361
    const-string v6, "SplitWidow"

    #@23a
    new-instance v7, Ljava/lang/StringBuilder;

    #@23c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23f
    const-string v8, "This window is type phone.. check mBaseLayer after modification.. mBaseLayer = "

    #@241
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v7

    #@245
    iget v8, p0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@247
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v7

    #@24b
    const-string v8, " of win "

    #@24d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v7

    #@251
    iget-object v8, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@253
    invoke-virtual {v8}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@256
    move-result-object v8

    #@257
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v7

    #@25b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25e
    move-result-object v7

    #@25f
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@262
    .line 367
    :goto_262
    const/4 v6, 0x0

    #@263
    iput v6, p0, Lcom/android/server/wm/WindowState;->mSubLayer:I

    #@265
    .line 368
    const/4 v6, 0x0

    #@266
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@268
    .line 369
    const/4 v6, 0x0

    #@269
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mLayoutAttached:Z

    #@26b
    .line 370
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@26d
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@26f
    const/16 v7, 0x7db

    #@271
    if-eq v6, v7, :cond_27b

    #@273
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@275
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@277
    const/16 v7, 0x7dc

    #@279
    if-ne v6, v7, :cond_2a7

    #@27b
    :cond_27b
    const/4 v6, 0x1

    #@27c
    :goto_27c
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@27e
    .line 372
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@280
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@282
    const/16 v7, 0x7dd

    #@284
    if-ne v6, v7, :cond_2a9

    #@286
    const/4 v6, 0x1

    #@287
    :goto_287
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@289
    .line 373
    iget-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@28b
    if-nez v6, :cond_291

    #@28d
    iget-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@28f
    if-eqz v6, :cond_2ab

    #@291
    :cond_291
    const/4 v6, 0x1

    #@292
    :goto_292
    iput-boolean v6, p0, Lcom/android/server/wm/WindowState;->mIsFloatingLayer:Z

    #@294
    goto/16 :goto_1d5

    #@296
    .line 363
    :cond_296
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@298
    move-object/from16 v0, p7

    #@29a
    iget v7, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@29c
    invoke-interface {v6, v7}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    #@29f
    move-result v6

    #@2a0
    mul-int/lit16 v6, v6, 0x2710

    #@2a2
    add-int/lit16 v6, v6, 0x3e8

    #@2a4
    iput v6, p0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@2a6
    goto :goto_262

    #@2a7
    .line 370
    :cond_2a7
    const/4 v6, 0x0

    #@2a8
    goto :goto_27c

    #@2a9
    .line 372
    :cond_2a9
    const/4 v6, 0x0

    #@2aa
    goto :goto_287

    #@2ab
    .line 373
    :cond_2ab
    const/4 v6, 0x0

    #@2ac
    goto :goto_292

    #@2ad
    .line 380
    .restart local v2       #appWin:Lcom/android/server/wm/WindowState;
    :cond_2ad
    iget-object v1, v2, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    #@2af
    .line 381
    .local v1, appToken:Lcom/android/server/wm/WindowToken;
    :goto_2af
    iget-object v6, v1, Lcom/android/server/wm/WindowToken;->appWindowToken:Lcom/android/server/wm/AppWindowToken;

    #@2b1
    if-nez v6, :cond_2c3

    #@2b3
    .line 382
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2b5
    iget-object v6, v6, Lcom/android/server/wm/WindowManagerService;->mTokenMap:Ljava/util/HashMap;

    #@2b7
    iget-object v7, v1, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@2b9
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2bc
    move-result-object v5

    #@2bd
    check-cast v5, Lcom/android/server/wm/WindowToken;

    #@2bf
    .line 383
    .local v5, parent:Lcom/android/server/wm/WindowToken;
    if-eqz v5, :cond_2c3

    #@2c1
    if-ne v1, v5, :cond_30d

    #@2c3
    .line 388
    .end local v5           #parent:Lcom/android/server/wm/WindowToken;
    :cond_2c3
    iput-object v1, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@2c5
    .line 389
    iget-object v6, v1, Lcom/android/server/wm/WindowToken;->appWindowToken:Lcom/android/server/wm/AppWindowToken;

    #@2c7
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2c9
    .line 391
    new-instance v6, Lcom/android/server/wm/WindowStateAnimator;

    #@2cb
    invoke-direct {v6, p0}, Lcom/android/server/wm/WindowStateAnimator;-><init>(Lcom/android/server/wm/WindowState;)V

    #@2ce
    iput-object v6, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@2d0
    .line 392
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@2d2
    move-object/from16 v0, p7

    #@2d4
    iget v7, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@2d6
    iput v7, v6, Lcom/android/server/wm/WindowStateAnimator;->mAlpha:F

    #@2d8
    .line 394
    const/4 v6, 0x0

    #@2d9
    iput v6, p0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@2db
    .line 395
    const/4 v6, 0x0

    #@2dc
    iput v6, p0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@2de
    .line 396
    const/4 v6, 0x0

    #@2df
    iput v6, p0, Lcom/android/server/wm/WindowState;->mLastRequestedWidth:I

    #@2e1
    .line 397
    const/4 v6, 0x0

    #@2e2
    iput v6, p0, Lcom/android/server/wm/WindowState;->mLastRequestedHeight:I

    #@2e4
    .line 398
    const/4 v6, 0x0

    #@2e5
    iput v6, p0, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@2e7
    .line 399
    const/4 v6, 0x0

    #@2e8
    iput v6, p0, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@2ea
    .line 400
    const/4 v6, 0x0

    #@2eb
    iput v6, p0, Lcom/android/server/wm/WindowState;->mLayer:I

    #@2ed
    .line 403
    const-string v6, "ro.lge.b2b.vmware"

    #@2ef
    const/4 v7, 0x0

    #@2f0
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2f3
    move-result v6

    #@2f4
    if-eqz v6, :cond_311

    #@2f6
    .line 404
    new-instance v7, Lcom/android/server/input/InputWindowHandle;

    #@2f8
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2fa
    if-eqz v6, :cond_30f

    #@2fc
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2fe
    iget-object v6, v6, Lcom/android/server/wm/AppWindowToken;->mInputApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@300
    :goto_300
    invoke-virtual/range {p9 .. p9}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    #@303
    move-result v8

    #@304
    move/from16 v0, p10

    #@306
    invoke-direct {v7, v6, p0, v8, v0}, Lcom/android/server/input/InputWindowHandle;-><init>(Lcom/android/server/input/InputApplicationHandle;Ljava/lang/Object;IZ)V

    #@309
    iput-object v7, p0, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@30b
    goto/16 :goto_202

    #@30d
    .line 386
    .restart local v5       #parent:Lcom/android/server/wm/WindowToken;
    :cond_30d
    move-object v1, v5

    #@30e
    .line 387
    goto :goto_2af

    #@30f
    .line 404
    .end local v5           #parent:Lcom/android/server/wm/WindowToken;
    :cond_30f
    const/4 v6, 0x0

    #@310
    goto :goto_300

    #@311
    .line 410
    :cond_311
    new-instance v7, Lcom/android/server/input/InputWindowHandle;

    #@313
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@315
    if-eqz v6, :cond_326

    #@317
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@319
    iget-object v6, v6, Lcom/android/server/wm/AppWindowToken;->mInputApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@31b
    :goto_31b
    invoke-virtual/range {p9 .. p9}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    #@31e
    move-result v8

    #@31f
    invoke-direct {v7, v6, p0, v8}, Lcom/android/server/input/InputWindowHandle;-><init>(Lcom/android/server/input/InputApplicationHandle;Ljava/lang/Object;I)V

    #@322
    iput-object v7, p0, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@324
    goto/16 :goto_202

    #@326
    :cond_326
    const/4 v6, 0x0

    #@327
    goto :goto_31b
.end method

.method private static applyInsets(Landroid/graphics/Region;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 8
    .parameter "outRegion"
    .parameter "frame"
    .parameter "inset"

    #@0
    .prologue
    .line 1241
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@4
    add-int/2addr v0, v1

    #@5
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@7
    iget v2, p2, Landroid/graphics/Rect;->top:I

    #@9
    add-int/2addr v1, v2

    #@a
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@c
    iget v3, p2, Landroid/graphics/Rect;->right:I

    #@e
    sub-int/2addr v2, v3

    #@f
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@11
    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    #@13
    sub-int/2addr v3, v4

    #@14
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/Region;->set(IIII)Z

    #@17
    .line 1244
    return-void
.end method


# virtual methods
.method attach()V
    .registers 2

    #@0
    .prologue
    .line 420
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@2
    invoke-virtual {v0}, Lcom/android/server/wm/Session;->windowAddedLocked()V

    #@5
    .line 421
    return-void
.end method

.method public final canReceiveKeys()Z
    .registers 2

    #@0
    .prologue
    .line 1078
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isVisibleOrAdding()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_14

    #@6
    iget v0, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@8
    if-nez v0, :cond_14

    #@a
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@c
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@e
    and-int/lit8 v0, v0, 0x8

    #@10
    if-nez v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public computeFrameLw(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 41
    .parameter "pf"
    .parameter "df"
    .parameter "cf"
    .parameter "vf"

    #@0
    .prologue
    .line 425
    const/4 v2, 0x1

    #@1
    move-object/from16 v0, p0

    #@3
    iput-boolean v2, v0, Lcom/android/server/wm/WindowState;->mHaveFrame:Z

    #@5
    .line 428
    move-object/from16 v0, p0

    #@7
    iget-object v5, v0, Lcom/android/server/wm/WindowState;->mContainingFrame:Landroid/graphics/Rect;

    #@9
    .line 429
    .local v5, container:Landroid/graphics/Rect;
    move-object/from16 v0, p1

    #@b
    invoke-virtual {v5, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@e
    .line 431
    move-object/from16 v0, p0

    #@10
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mDisplayFrame:Landroid/graphics/Rect;

    #@12
    move-object/from16 v20, v0

    #@14
    .line 432
    .local v20, display:Landroid/graphics/Rect;
    move-object/from16 v0, v20

    #@16
    move-object/from16 v1, p2

    #@18
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@1b
    .line 436
    move-object/from16 v0, p0

    #@1d
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@1f
    if-eqz v2, :cond_73

    #@21
    move-object/from16 v0, p0

    #@23
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mUseWVGACompat:Z

    #@25
    if-eqz v2, :cond_73

    #@27
    .line 437
    move-object/from16 v0, p0

    #@29
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@2b
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@2e
    move-result-object v2

    #@2f
    iget v3, v2, Landroid/view/DisplayInfo;->appWidth:I

    #@31
    .line 438
    .local v3, w:I
    move-object/from16 v0, p0

    #@33
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@35
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@38
    move-result-object v2

    #@39
    iget v4, v2, Landroid/view/DisplayInfo;->appHeight:I

    #@3b
    .line 439
    .local v4, h:I
    move v10, v3

    #@3c
    .line 440
    .local v10, adjustedWidth:I
    move v9, v4

    #@3d
    .line 441
    .local v9, adjustedHeight:I
    int-to-float v2, v4

    #@3e
    int-to-float v6, v3

    #@3f
    div-float v12, v2, v6

    #@41
    .line 442
    .local v12, aspect:F
    float-to-double v6, v12

    #@42
    const-wide/high16 v34, 0x3ff0

    #@44
    cmpl-double v2, v6, v34

    #@46
    if-lez v2, :cond_363

    #@48
    const v14, 0x3fd55555

    #@4b
    .line 444
    .local v14, compatAspect:F
    :goto_4b
    cmpl-float v2, v12, v14

    #@4d
    if-lez v2, :cond_368

    #@4f
    .line 445
    int-to-float v2, v3

    #@50
    mul-float/2addr v2, v14

    #@51
    const/high16 v6, 0x3f00

    #@53
    add-float/2addr v2, v6

    #@54
    float-to-int v9, v2

    #@55
    .line 452
    :goto_55
    new-instance v15, Landroid/graphics/Rect;

    #@57
    const/4 v2, 0x0

    #@58
    const/4 v6, 0x0

    #@59
    invoke-direct {v15, v2, v6, v10, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    #@5c
    .line 453
    .local v15, compatRect:Landroid/graphics/Rect;
    if-ge v10, v3, :cond_66

    #@5e
    .line 454
    sub-int v2, v3, v10

    #@60
    div-int/lit8 v2, v2, 0x2

    #@62
    const/4 v6, 0x0

    #@63
    invoke-virtual {v15, v2, v6}, Landroid/graphics/Rect;->offset(II)V

    #@66
    .line 456
    :cond_66
    if-ge v9, v4, :cond_70

    #@68
    .line 457
    const/4 v2, 0x0

    #@69
    sub-int v6, v4, v9

    #@6b
    div-int/lit8 v6, v6, 0x2

    #@6d
    invoke-virtual {v15, v2, v6}, Landroid/graphics/Rect;->offset(II)V

    #@70
    .line 459
    :cond_70
    invoke-virtual {v5, v15}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    #@73
    .line 463
    .end local v3           #w:I
    .end local v4           #h:I
    .end local v9           #adjustedHeight:I
    .end local v10           #adjustedWidth:I
    .end local v12           #aspect:F
    .end local v14           #compatAspect:F
    .end local v15           #compatRect:Landroid/graphics/Rect;
    :cond_73
    iget v2, v5, Landroid/graphics/Rect;->right:I

    #@75
    iget v6, v5, Landroid/graphics/Rect;->left:I

    #@77
    sub-int v29, v2, v6

    #@79
    .line 464
    .local v29, pw:I
    iget v2, v5, Landroid/graphics/Rect;->bottom:I

    #@7b
    iget v6, v5, Landroid/graphics/Rect;->top:I

    #@7d
    sub-int v28, v2, v6

    #@7f
    .line 467
    .local v28, ph:I
    move-object/from16 v0, p0

    #@81
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@83
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@85
    and-int/lit16 v2, v2, 0x4000

    #@87
    if-eqz v2, :cond_3b0

    #@89
    .line 468
    move-object/from16 v0, p0

    #@8b
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@8d
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@8f
    if-gez v2, :cond_370

    #@91
    .line 469
    move/from16 v3, v29

    #@93
    .line 475
    .restart local v3       #w:I
    :goto_93
    move-object/from16 v0, p0

    #@95
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@97
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@99
    if-gez v2, :cond_390

    #@9b
    .line 476
    move/from16 v4, v28

    #@9d
    .line 499
    .restart local v4       #h:I
    :goto_9d
    move-object/from16 v0, p0

    #@9f
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mParentFrame:Landroid/graphics/Rect;

    #@a1
    move-object/from16 v0, p1

    #@a3
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@a6
    move-result v2

    #@a7
    if-nez v2, :cond_b7

    #@a9
    .line 502
    move-object/from16 v0, p0

    #@ab
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mParentFrame:Landroid/graphics/Rect;

    #@ad
    move-object/from16 v0, p1

    #@af
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@b2
    .line 503
    const/4 v2, 0x1

    #@b3
    move-object/from16 v0, p0

    #@b5
    iput-boolean v2, v0, Lcom/android/server/wm/WindowState;->mContentChanged:Z

    #@b7
    .line 505
    :cond_b7
    move-object/from16 v0, p0

    #@b9
    iget v2, v0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@bb
    move-object/from16 v0, p0

    #@bd
    iget v6, v0, Lcom/android/server/wm/WindowState;->mLastRequestedWidth:I

    #@bf
    if-ne v2, v6, :cond_cb

    #@c1
    move-object/from16 v0, p0

    #@c3
    iget v2, v0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@c5
    move-object/from16 v0, p0

    #@c7
    iget v6, v0, Lcom/android/server/wm/WindowState;->mLastRequestedHeight:I

    #@c9
    if-eq v2, v6, :cond_e0

    #@cb
    .line 506
    :cond_cb
    move-object/from16 v0, p0

    #@cd
    iget v2, v0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@cf
    move-object/from16 v0, p0

    #@d1
    iput v2, v0, Lcom/android/server/wm/WindowState;->mLastRequestedWidth:I

    #@d3
    .line 507
    move-object/from16 v0, p0

    #@d5
    iget v2, v0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@d7
    move-object/from16 v0, p0

    #@d9
    iput v2, v0, Lcom/android/server/wm/WindowState;->mLastRequestedHeight:I

    #@db
    .line 508
    const/4 v2, 0x1

    #@dc
    move-object/from16 v0, p0

    #@de
    iput-boolean v2, v0, Lcom/android/server/wm/WindowState;->mContentChanged:Z

    #@e0
    .line 511
    :cond_e0
    move-object/from16 v0, p0

    #@e2
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mContentFrame:Landroid/graphics/Rect;

    #@e4
    move-object/from16 v16, v0

    #@e6
    .line 512
    .local v16, content:Landroid/graphics/Rect;
    move-object/from16 v0, v16

    #@e8
    move-object/from16 v1, p3

    #@ea
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@ed
    .line 514
    move-object/from16 v0, p0

    #@ef
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mVisibleFrame:Landroid/graphics/Rect;

    #@f1
    move-object/from16 v30, v0

    #@f3
    .line 515
    .local v30, visible:Landroid/graphics/Rect;
    move-object/from16 v0, v30

    #@f5
    move-object/from16 v1, p4

    #@f7
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@fa
    .line 517
    move-object/from16 v0, p0

    #@fc
    iget-object v8, v0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@fe
    .line 518
    .local v8, frame:Landroid/graphics/Rect;
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    #@101
    move-result v24

    #@102
    .line 519
    .local v24, fw:I
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@105
    move-result v22

    #@106
    .line 525
    .local v22, fh:I
    move-object/from16 v0, p0

    #@108
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@10a
    if-eqz v2, :cond_3fe

    #@10c
    .line 526
    move-object/from16 v0, p0

    #@10e
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@110
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    #@112
    int-to-float v2, v2

    #@113
    move-object/from16 v0, p0

    #@115
    iget v6, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@117
    mul-float v32, v2, v6

    #@119
    .line 527
    .local v32, x:F
    move-object/from16 v0, p0

    #@11b
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@11d
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    #@11f
    int-to-float v2, v2

    #@120
    move-object/from16 v0, p0

    #@122
    iget v6, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@124
    mul-float v33, v2, v6

    #@126
    .line 533
    .local v33, y:F
    :goto_126
    move-object/from16 v0, p0

    #@128
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@12a
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@12c
    move-object/from16 v0, p0

    #@12e
    iget-object v6, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@130
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@132
    move/from16 v0, v29

    #@134
    int-to-float v7, v0

    #@135
    mul-float/2addr v6, v7

    #@136
    add-float v6, v6, v32

    #@138
    float-to-int v6, v6

    #@139
    move-object/from16 v0, p0

    #@13b
    iget-object v7, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@13d
    iget v7, v7, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@13f
    move/from16 v0, v28

    #@141
    int-to-float v0, v0

    #@142
    move/from16 v34, v0

    #@144
    mul-float v7, v7, v34

    #@146
    add-float v7, v7, v33

    #@148
    float-to-int v7, v7

    #@149
    invoke-static/range {v2 .. v8}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;IILandroid/graphics/Rect;)V

    #@14c
    .line 540
    move-object/from16 v0, p0

    #@14e
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@150
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@152
    move-object/from16 v0, p2

    #@154
    invoke-static {v2, v0, v8}, Landroid/view/Gravity;->applyDisplay(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@157
    .line 549
    const/16 v23, 0x1

    #@159
    .line 550
    .local v23, frmAdjustNeeded:Z
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@15b
    if-eqz v2, :cond_22f

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@161
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@163
    const/16 v6, 0x63

    #@165
    if-gt v2, v6, :cond_171

    #@167
    move-object/from16 v0, p0

    #@169
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@16b
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@16d
    and-int/lit16 v2, v2, 0x200

    #@16f
    if-eqz v2, :cond_19f

    #@171
    :cond_171
    move-object/from16 v0, p0

    #@173
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@175
    if-eqz v2, :cond_22f

    #@177
    move-object/from16 v0, p0

    #@179
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@17b
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getSplitScreenId()I

    #@17e
    move-result v2

    #@17f
    if-eqz v2, :cond_22f

    #@181
    move-object/from16 v0, p0

    #@183
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@185
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->isSplitFullScreen()Z

    #@188
    move-result v2

    #@189
    if-nez v2, :cond_22f

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@18f
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@191
    const/16 v6, 0x3e9

    #@193
    if-eq v2, v6, :cond_19f

    #@195
    move-object/from16 v0, p0

    #@197
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@199
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@19b
    const/16 v6, 0x3ec

    #@19d
    if-ne v2, v6, :cond_22f

    #@19f
    .line 558
    :cond_19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@1a3
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@1a6
    move-result-object v2

    #@1a7
    iget v13, v2, Landroid/view/DisplayInfo;->appWidth:I

    #@1a9
    .line 559
    .local v13, aw:I
    move-object/from16 v0, p0

    #@1ab
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@1ad
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@1b0
    move-result-object v2

    #@1b1
    iget v11, v2, Landroid/view/DisplayInfo;->appHeight:I

    #@1b3
    .line 561
    .local v11, ah:I
    if-ge v13, v11, :cond_412

    #@1b5
    const/16 v25, 0x1

    #@1b7
    .line 562
    .local v25, isPortrait:Z
    :goto_1b7
    move-object/from16 v0, v16

    #@1b9
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@1bb
    move-object/from16 v0, v16

    #@1bd
    iget v6, v0, Landroid/graphics/Rect;->left:I

    #@1bf
    sub-int v19, v2, v6

    #@1c1
    .line 563
    .local v19, contentWidth:I
    move-object/from16 v0, v16

    #@1c3
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    #@1c5
    move-object/from16 v0, v16

    #@1c7
    iget v6, v0, Landroid/graphics/Rect;->top:I

    #@1c9
    sub-int v17, v2, v6

    #@1cb
    .line 564
    .local v17, contentHeight:I
    move-object/from16 v0, p1

    #@1cd
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@1cf
    move-object/from16 v0, p1

    #@1d1
    iget v6, v0, Landroid/graphics/Rect;->left:I

    #@1d3
    sub-int v27, v2, v6

    #@1d5
    .line 565
    .local v27, parentWidth:I
    move-object/from16 v0, p1

    #@1d7
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    #@1d9
    move-object/from16 v0, p1

    #@1db
    iget v6, v0, Landroid/graphics/Rect;->top:I

    #@1dd
    sub-int v26, v2, v6

    #@1df
    .line 566
    .local v26, parentHeight:I
    if-eqz v25, :cond_41a

    #@1e1
    .line 567
    move/from16 v0, v26

    #@1e3
    move/from16 v1, v17

    #@1e5
    if-ge v0, v1, :cond_416

    #@1e7
    const/16 v23, 0x0

    #@1e9
    .line 574
    :goto_1e9
    if-nez v23, :cond_21b

    #@1eb
    .line 575
    const-string v2, "SplitWindow"

    #@1ed
    new-instance v6, Ljava/lang/StringBuilder;

    #@1ef
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1f2
    const-string v7, "Check frames for skipping adjust.. content = "

    #@1f4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v6

    #@1f8
    move-object/from16 v0, v16

    #@1fa
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v6

    #@1fe
    const-string v7, ", frame = "

    #@200
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v6

    #@204
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@207
    move-result-object v6

    #@208
    const-string v7, " of this win "

    #@20a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v6

    #@20e
    move-object/from16 v0, p0

    #@210
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v6

    #@214
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@217
    move-result-object v6

    #@218
    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21b
    .line 578
    :cond_21b
    move-object/from16 v0, p0

    #@21d
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@21f
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@221
    const/16 v6, 0x3e9

    #@223
    if-eq v2, v6, :cond_22f

    #@225
    move-object/from16 v0, p0

    #@227
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@229
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@22b
    const/16 v6, 0x3ec

    #@22d
    if-ne v2, v6, :cond_22f

    #@22f
    .line 583
    .end local v11           #ah:I
    .end local v13           #aw:I
    .end local v17           #contentHeight:I
    .end local v19           #contentWidth:I
    .end local v25           #isPortrait:Z
    .end local v26           #parentHeight:I
    .end local v27           #parentWidth:I
    :cond_22f
    if-eqz v23, :cond_269

    #@231
    .line 584
    move-object/from16 v0, v16

    #@233
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@235
    iget v6, v8, Landroid/graphics/Rect;->left:I

    #@237
    if-ge v2, v6, :cond_23f

    #@239
    iget v2, v8, Landroid/graphics/Rect;->left:I

    #@23b
    move-object/from16 v0, v16

    #@23d
    iput v2, v0, Landroid/graphics/Rect;->left:I

    #@23f
    .line 585
    :cond_23f
    move-object/from16 v0, v16

    #@241
    iget v2, v0, Landroid/graphics/Rect;->top:I

    #@243
    iget v6, v8, Landroid/graphics/Rect;->top:I

    #@245
    if-ge v2, v6, :cond_24d

    #@247
    iget v2, v8, Landroid/graphics/Rect;->top:I

    #@249
    move-object/from16 v0, v16

    #@24b
    iput v2, v0, Landroid/graphics/Rect;->top:I

    #@24d
    .line 586
    :cond_24d
    move-object/from16 v0, v16

    #@24f
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@251
    iget v6, v8, Landroid/graphics/Rect;->right:I

    #@253
    if-le v2, v6, :cond_25b

    #@255
    iget v2, v8, Landroid/graphics/Rect;->right:I

    #@257
    move-object/from16 v0, v16

    #@259
    iput v2, v0, Landroid/graphics/Rect;->right:I

    #@25b
    .line 587
    :cond_25b
    move-object/from16 v0, v16

    #@25d
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    #@25f
    iget v6, v8, Landroid/graphics/Rect;->bottom:I

    #@261
    if-le v2, v6, :cond_269

    #@263
    iget v2, v8, Landroid/graphics/Rect;->bottom:I

    #@265
    move-object/from16 v0, v16

    #@267
    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    #@269
    .line 589
    :cond_269
    move-object/from16 v0, v30

    #@26b
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@26d
    iget v6, v8, Landroid/graphics/Rect;->left:I

    #@26f
    if-ge v2, v6, :cond_277

    #@271
    iget v2, v8, Landroid/graphics/Rect;->left:I

    #@273
    move-object/from16 v0, v30

    #@275
    iput v2, v0, Landroid/graphics/Rect;->left:I

    #@277
    .line 590
    :cond_277
    move-object/from16 v0, v30

    #@279
    iget v2, v0, Landroid/graphics/Rect;->top:I

    #@27b
    iget v6, v8, Landroid/graphics/Rect;->top:I

    #@27d
    if-ge v2, v6, :cond_285

    #@27f
    iget v2, v8, Landroid/graphics/Rect;->top:I

    #@281
    move-object/from16 v0, v30

    #@283
    iput v2, v0, Landroid/graphics/Rect;->top:I

    #@285
    .line 591
    :cond_285
    move-object/from16 v0, v30

    #@287
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@289
    iget v6, v8, Landroid/graphics/Rect;->right:I

    #@28b
    if-le v2, v6, :cond_293

    #@28d
    iget v2, v8, Landroid/graphics/Rect;->right:I

    #@28f
    move-object/from16 v0, v30

    #@291
    iput v2, v0, Landroid/graphics/Rect;->right:I

    #@293
    .line 592
    :cond_293
    move-object/from16 v0, v30

    #@295
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    #@297
    iget v6, v8, Landroid/graphics/Rect;->bottom:I

    #@299
    if-le v2, v6, :cond_2a1

    #@29b
    iget v2, v8, Landroid/graphics/Rect;->bottom:I

    #@29d
    move-object/from16 v0, v30

    #@29f
    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    #@2a1
    .line 601
    :cond_2a1
    move-object/from16 v0, p0

    #@2a3
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mContentInsets:Landroid/graphics/Rect;

    #@2a5
    move-object/from16 v18, v0

    #@2a7
    .line 602
    .local v18, contentInsets:Landroid/graphics/Rect;
    move-object/from16 v0, v16

    #@2a9
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@2ab
    iget v6, v8, Landroid/graphics/Rect;->left:I

    #@2ad
    sub-int/2addr v2, v6

    #@2ae
    move-object/from16 v0, v18

    #@2b0
    iput v2, v0, Landroid/graphics/Rect;->left:I

    #@2b2
    .line 603
    move-object/from16 v0, v16

    #@2b4
    iget v2, v0, Landroid/graphics/Rect;->top:I

    #@2b6
    iget v6, v8, Landroid/graphics/Rect;->top:I

    #@2b8
    sub-int/2addr v2, v6

    #@2b9
    move-object/from16 v0, v18

    #@2bb
    iput v2, v0, Landroid/graphics/Rect;->top:I

    #@2bd
    .line 604
    iget v2, v8, Landroid/graphics/Rect;->right:I

    #@2bf
    move-object/from16 v0, v16

    #@2c1
    iget v6, v0, Landroid/graphics/Rect;->right:I

    #@2c3
    sub-int/2addr v2, v6

    #@2c4
    move-object/from16 v0, v18

    #@2c6
    iput v2, v0, Landroid/graphics/Rect;->right:I

    #@2c8
    .line 605
    iget v2, v8, Landroid/graphics/Rect;->bottom:I

    #@2ca
    move-object/from16 v0, v16

    #@2cc
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    #@2ce
    sub-int/2addr v2, v6

    #@2cf
    move-object/from16 v0, v18

    #@2d1
    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    #@2d3
    .line 607
    move-object/from16 v0, p0

    #@2d5
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mVisibleInsets:Landroid/graphics/Rect;

    #@2d7
    move-object/from16 v31, v0

    #@2d9
    .line 608
    .local v31, visibleInsets:Landroid/graphics/Rect;
    move-object/from16 v0, v30

    #@2db
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@2dd
    iget v6, v8, Landroid/graphics/Rect;->left:I

    #@2df
    sub-int/2addr v2, v6

    #@2e0
    move-object/from16 v0, v31

    #@2e2
    iput v2, v0, Landroid/graphics/Rect;->left:I

    #@2e4
    .line 609
    move-object/from16 v0, v30

    #@2e6
    iget v2, v0, Landroid/graphics/Rect;->top:I

    #@2e8
    iget v6, v8, Landroid/graphics/Rect;->top:I

    #@2ea
    sub-int/2addr v2, v6

    #@2eb
    move-object/from16 v0, v31

    #@2ed
    iput v2, v0, Landroid/graphics/Rect;->top:I

    #@2ef
    .line 610
    iget v2, v8, Landroid/graphics/Rect;->right:I

    #@2f1
    move-object/from16 v0, v30

    #@2f3
    iget v6, v0, Landroid/graphics/Rect;->right:I

    #@2f5
    sub-int/2addr v2, v6

    #@2f6
    move-object/from16 v0, v31

    #@2f8
    iput v2, v0, Landroid/graphics/Rect;->right:I

    #@2fa
    .line 611
    iget v2, v8, Landroid/graphics/Rect;->bottom:I

    #@2fc
    move-object/from16 v0, v30

    #@2fe
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    #@300
    sub-int/2addr v2, v6

    #@301
    move-object/from16 v0, v31

    #@303
    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    #@305
    .line 613
    move-object/from16 v0, p0

    #@307
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@309
    invoke-virtual {v2, v8}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@30c
    .line 614
    move-object/from16 v0, p0

    #@30e
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@310
    if-eqz v2, :cond_32f

    #@312
    .line 618
    move-object/from16 v0, p0

    #@314
    iget v2, v0, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@316
    move-object/from16 v0, v18

    #@318
    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->scale(F)V

    #@31b
    .line 619
    move-object/from16 v0, p0

    #@31d
    iget v2, v0, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@31f
    move-object/from16 v0, v31

    #@321
    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->scale(F)V

    #@324
    .line 623
    move-object/from16 v0, p0

    #@326
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@328
    move-object/from16 v0, p0

    #@32a
    iget v6, v0, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@32c
    invoke-virtual {v2, v6}, Landroid/graphics/Rect;->scale(F)V

    #@32f
    .line 626
    :cond_32f
    move-object/from16 v0, p0

    #@331
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@333
    if-eqz v2, :cond_362

    #@335
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    #@338
    move-result v2

    #@339
    move/from16 v0, v24

    #@33b
    if-ne v0, v2, :cond_345

    #@33d
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@340
    move-result v2

    #@341
    move/from16 v0, v22

    #@343
    if-eq v0, v2, :cond_362

    #@345
    .line 627
    :cond_345
    move-object/from16 v0, p0

    #@347
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@349
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@34c
    move-result-object v21

    #@34d
    .line 628
    .local v21, displayInfo:Landroid/view/DisplayInfo;
    move-object/from16 v0, p0

    #@34f
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@351
    move-object/from16 v0, v21

    #@353
    iget v6, v0, Landroid/view/DisplayInfo;->appWidth:I

    #@355
    move-object/from16 v0, v21

    #@357
    iget v7, v0, Landroid/view/DisplayInfo;->appHeight:I

    #@359
    const/16 v34, 0x0

    #@35b
    move-object/from16 v0, p0

    #@35d
    move/from16 v1, v34

    #@35f
    invoke-virtual {v2, v0, v6, v7, v1}, Lcom/android/server/wm/WindowManagerService;->updateWallpaperOffsetLocked(Lcom/android/server/wm/WindowState;IIZ)Z

    #@362
    .line 643
    .end local v21           #displayInfo:Landroid/view/DisplayInfo;
    :cond_362
    return-void

    #@363
    .line 442
    .end local v8           #frame:Landroid/graphics/Rect;
    .end local v16           #content:Landroid/graphics/Rect;
    .end local v18           #contentInsets:Landroid/graphics/Rect;
    .end local v22           #fh:I
    .end local v23           #frmAdjustNeeded:Z
    .end local v24           #fw:I
    .end local v28           #ph:I
    .end local v29           #pw:I
    .end local v30           #visible:Landroid/graphics/Rect;
    .end local v31           #visibleInsets:Landroid/graphics/Rect;
    .end local v32           #x:F
    .end local v33           #y:F
    .restart local v9       #adjustedHeight:I
    .restart local v10       #adjustedWidth:I
    .restart local v12       #aspect:F
    :cond_363
    const v14, 0x3f19999a

    #@366
    goto/16 :goto_4b

    #@368
    .line 449
    .restart local v14       #compatAspect:F
    :cond_368
    int-to-float v2, v4

    #@369
    div-float/2addr v2, v14

    #@36a
    const/high16 v6, 0x3f00

    #@36c
    add-float/2addr v2, v6

    #@36d
    float-to-int v10, v2

    #@36e
    goto/16 :goto_55

    #@370
    .line 470
    .end local v3           #w:I
    .end local v4           #h:I
    .end local v9           #adjustedHeight:I
    .end local v10           #adjustedWidth:I
    .end local v12           #aspect:F
    .end local v14           #compatAspect:F
    .restart local v28       #ph:I
    .restart local v29       #pw:I
    :cond_370
    move-object/from16 v0, p0

    #@372
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@374
    if-eqz v2, :cond_388

    #@376
    .line 471
    move-object/from16 v0, p0

    #@378
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@37a
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@37c
    int-to-float v2, v2

    #@37d
    move-object/from16 v0, p0

    #@37f
    iget v6, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@381
    mul-float/2addr v2, v6

    #@382
    const/high16 v6, 0x3f00

    #@384
    add-float/2addr v2, v6

    #@385
    float-to-int v3, v2

    #@386
    .restart local v3       #w:I
    goto/16 :goto_93

    #@388
    .line 473
    .end local v3           #w:I
    :cond_388
    move-object/from16 v0, p0

    #@38a
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@38c
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@38e
    .restart local v3       #w:I
    goto/16 :goto_93

    #@390
    .line 477
    :cond_390
    move-object/from16 v0, p0

    #@392
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@394
    if-eqz v2, :cond_3a8

    #@396
    .line 478
    move-object/from16 v0, p0

    #@398
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@39a
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@39c
    int-to-float v2, v2

    #@39d
    move-object/from16 v0, p0

    #@39f
    iget v6, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@3a1
    mul-float/2addr v2, v6

    #@3a2
    const/high16 v6, 0x3f00

    #@3a4
    add-float/2addr v2, v6

    #@3a5
    float-to-int v4, v2

    #@3a6
    .restart local v4       #h:I
    goto/16 :goto_9d

    #@3a8
    .line 480
    .end local v4           #h:I
    :cond_3a8
    move-object/from16 v0, p0

    #@3aa
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@3ac
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@3ae
    .restart local v4       #h:I
    goto/16 :goto_9d

    #@3b0
    .line 483
    .end local v3           #w:I
    .end local v4           #h:I
    :cond_3b0
    move-object/from16 v0, p0

    #@3b2
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@3b4
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3b6
    const/4 v6, -0x1

    #@3b7
    if-ne v2, v6, :cond_3c8

    #@3b9
    .line 484
    move/from16 v3, v29

    #@3bb
    .line 490
    .restart local v3       #w:I
    :goto_3bb
    move-object/from16 v0, p0

    #@3bd
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@3bf
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@3c1
    const/4 v6, -0x1

    #@3c2
    if-ne v2, v6, :cond_3e2

    #@3c4
    .line 491
    move/from16 v4, v28

    #@3c6
    .restart local v4       #h:I
    goto/16 :goto_9d

    #@3c8
    .line 485
    .end local v3           #w:I
    .end local v4           #h:I
    :cond_3c8
    move-object/from16 v0, p0

    #@3ca
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@3cc
    if-eqz v2, :cond_3dd

    #@3ce
    .line 486
    move-object/from16 v0, p0

    #@3d0
    iget v2, v0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@3d2
    int-to-float v2, v2

    #@3d3
    move-object/from16 v0, p0

    #@3d5
    iget v6, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@3d7
    mul-float/2addr v2, v6

    #@3d8
    const/high16 v6, 0x3f00

    #@3da
    add-float/2addr v2, v6

    #@3db
    float-to-int v3, v2

    #@3dc
    .restart local v3       #w:I
    goto :goto_3bb

    #@3dd
    .line 488
    .end local v3           #w:I
    :cond_3dd
    move-object/from16 v0, p0

    #@3df
    iget v3, v0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@3e1
    .restart local v3       #w:I
    goto :goto_3bb

    #@3e2
    .line 492
    :cond_3e2
    move-object/from16 v0, p0

    #@3e4
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@3e6
    if-eqz v2, :cond_3f8

    #@3e8
    .line 493
    move-object/from16 v0, p0

    #@3ea
    iget v2, v0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@3ec
    int-to-float v2, v2

    #@3ed
    move-object/from16 v0, p0

    #@3ef
    iget v6, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@3f1
    mul-float/2addr v2, v6

    #@3f2
    const/high16 v6, 0x3f00

    #@3f4
    add-float/2addr v2, v6

    #@3f5
    float-to-int v4, v2

    #@3f6
    .restart local v4       #h:I
    goto/16 :goto_9d

    #@3f8
    .line 495
    .end local v4           #h:I
    :cond_3f8
    move-object/from16 v0, p0

    #@3fa
    iget v4, v0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@3fc
    .restart local v4       #h:I
    goto/16 :goto_9d

    #@3fe
    .line 529
    .restart local v8       #frame:Landroid/graphics/Rect;
    .restart local v16       #content:Landroid/graphics/Rect;
    .restart local v22       #fh:I
    .restart local v24       #fw:I
    .restart local v30       #visible:Landroid/graphics/Rect;
    :cond_3fe
    move-object/from16 v0, p0

    #@400
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@402
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    #@404
    int-to-float v0, v2

    #@405
    move/from16 v32, v0

    #@407
    .line 530
    .restart local v32       #x:F
    move-object/from16 v0, p0

    #@409
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@40b
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    #@40d
    int-to-float v0, v2

    #@40e
    move/from16 v33, v0

    #@410
    .restart local v33       #y:F
    goto/16 :goto_126

    #@412
    .line 561
    .restart local v11       #ah:I
    .restart local v13       #aw:I
    .restart local v23       #frmAdjustNeeded:Z
    :cond_412
    const/16 v25, 0x0

    #@414
    goto/16 :goto_1b7

    #@416
    .line 567
    .restart local v17       #contentHeight:I
    .restart local v19       #contentWidth:I
    .restart local v25       #isPortrait:Z
    .restart local v26       #parentHeight:I
    .restart local v27       #parentWidth:I
    :cond_416
    const/16 v23, 0x1

    #@418
    goto/16 :goto_1e9

    #@41a
    .line 570
    :cond_41a
    move/from16 v0, v27

    #@41c
    move/from16 v1, v19

    #@41e
    if-ge v0, v1, :cond_424

    #@420
    const/16 v23, 0x0

    #@422
    :goto_422
    goto/16 :goto_1e9

    #@424
    :cond_424
    const/16 v23, 0x1

    #@426
    goto :goto_422
.end method

.method disposeInputChannel()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1046
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@3
    if-eqz v0, :cond_15

    #@5
    .line 1047
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@9
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/server/input/InputManagerService;->unregisterInputChannel(Landroid/view/InputChannel;)V

    #@e
    .line 1049
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@10
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@13
    .line 1050
    iput-object v2, p0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@15
    .line 1053
    :cond_15
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@17
    iput-object v2, v0, Lcom/android/server/input/InputWindowHandle;->inputChannel:Landroid/view/InputChannel;

    #@19
    .line 1054
    return-void
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V
    .registers 10
    .parameter "pw"
    .parameter "prefix"
    .parameter "dumpAll"

    #@0
    .prologue
    const/high16 v5, 0x3f80

    #@2
    const/high16 v4, -0x4080

    #@4
    .line 1273
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7
    const-string v1, "mDisplayId="

    #@9
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@e
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    #@11
    move-result v1

    #@12
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@15
    .line 1274
    const-string v1, " mSession="

    #@17
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@1c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@1f
    .line 1275
    const-string v1, " mClient="

    #@21
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@26
    invoke-interface {v1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@2d
    .line 1276
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@30
    const-string v1, "mOwnerUid="

    #@32
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@35
    iget v1, p0, Lcom/android/server/wm/WindowState;->mOwnerUid:I

    #@37
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@3a
    .line 1277
    const-string v1, " mShowToOwnerOnly="

    #@3c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mShowToOwnerOnly:Z

    #@41
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@44
    .line 1278
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@47
    const-string v1, "mAttrs="

    #@49
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@4e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@51
    .line 1279
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@54
    const-string v1, "Requested w="

    #@56
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@59
    iget v1, p0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@5b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@5e
    .line 1280
    const-string v1, " h="

    #@60
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@63
    iget v1, p0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@65
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@68
    .line 1281
    const-string v1, " mLayoutSeq="

    #@6a
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6d
    iget v1, p0, Lcom/android/server/wm/WindowState;->mLayoutSeq:I

    #@6f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    #@72
    .line 1282
    iget v1, p0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@74
    iget v2, p0, Lcom/android/server/wm/WindowState;->mLastRequestedWidth:I

    #@76
    if-ne v1, v2, :cond_7e

    #@78
    iget v1, p0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@7a
    iget v2, p0, Lcom/android/server/wm/WindowState;->mLastRequestedHeight:I

    #@7c
    if-eq v1, v2, :cond_95

    #@7e
    .line 1283
    :cond_7e
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@81
    const-string v1, "LastRequested w="

    #@83
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@86
    iget v1, p0, Lcom/android/server/wm/WindowState;->mLastRequestedWidth:I

    #@88
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@8b
    .line 1284
    const-string v1, " h="

    #@8d
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@90
    iget v1, p0, Lcom/android/server/wm/WindowState;->mLastRequestedHeight:I

    #@92
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    #@95
    .line 1286
    :cond_95
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@97
    if-nez v1, :cond_9d

    #@99
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mLayoutAttached:Z

    #@9b
    if-eqz v1, :cond_b4

    #@9d
    .line 1287
    :cond_9d
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a0
    const-string v1, "mAttachedWindow="

    #@a2
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a5
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@a7
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@aa
    .line 1288
    const-string v1, " mLayoutAttached="

    #@ac
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@af
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mLayoutAttached:Z

    #@b1
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@b4
    .line 1290
    :cond_b4
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@b6
    if-nez v1, :cond_c0

    #@b8
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@ba
    if-nez v1, :cond_c0

    #@bc
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mIsFloatingLayer:Z

    #@be
    if-eqz v1, :cond_eb

    #@c0
    .line 1291
    :cond_c0
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c3
    const-string v1, "mIsImWindow="

    #@c5
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c8
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@ca
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@cd
    .line 1292
    const-string v1, " mIsWallpaper="

    #@cf
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d2
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@d4
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@d7
    .line 1293
    const-string v1, " mIsFloatingLayer="

    #@d9
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@dc
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mIsFloatingLayer:Z

    #@de
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@e1
    .line 1294
    const-string v1, " mWallpaperVisible="

    #@e3
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e6
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperVisible:Z

    #@e8
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@eb
    .line 1296
    :cond_eb
    if-eqz p3, :cond_138

    #@ed
    .line 1297
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f0
    const-string v1, "mBaseLayer="

    #@f2
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f5
    iget v1, p0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@f7
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@fa
    .line 1298
    const-string v1, " mSubLayer="

    #@fc
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ff
    iget v1, p0, Lcom/android/server/wm/WindowState;->mSubLayer:I

    #@101
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@104
    .line 1299
    const-string v1, " mAnimLayer="

    #@106
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@109
    iget v1, p0, Lcom/android/server/wm/WindowState;->mLayer:I

    #@10b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@10e
    const-string v1, "+"

    #@110
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@113
    .line 1300
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mTargetAppToken:Lcom/android/server/wm/AppWindowToken;

    #@115
    if-eqz v1, :cond_454

    #@117
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mTargetAppToken:Lcom/android/server/wm/AppWindowToken;

    #@119
    iget-object v1, v1, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@11b
    iget v1, v1, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@11d
    :goto_11d
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@120
    .line 1303
    const-string v1, "="

    #@122
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@125
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@127
    iget v1, v1, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@129
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@12c
    .line 1304
    const-string v1, " mLastLayer="

    #@12e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@131
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@133
    iget v1, v1, Lcom/android/server/wm/WindowStateAnimator;->mLastLayer:I

    #@135
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    #@138
    .line 1306
    :cond_138
    if-eqz p3, :cond_1b6

    #@13a
    .line 1307
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13d
    const-string v1, "mToken="

    #@13f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@142
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    #@144
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@147
    .line 1308
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14a
    const-string v1, "mRootToken="

    #@14c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14f
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@151
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@154
    .line 1309
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@156
    if-eqz v1, :cond_165

    #@158
    .line 1310
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15b
    const-string v1, "mAppToken="

    #@15d
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@160
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@162
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@165
    .line 1312
    :cond_165
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mTargetAppToken:Lcom/android/server/wm/AppWindowToken;

    #@167
    if-eqz v1, :cond_176

    #@169
    .line 1313
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16c
    const-string v1, "mTargetAppToken="

    #@16e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@171
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mTargetAppToken:Lcom/android/server/wm/AppWindowToken;

    #@173
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@176
    .line 1315
    :cond_176
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@179
    const-string v1, "mViewVisibility=0x"

    #@17b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17e
    .line 1316
    iget v1, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@180
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@183
    move-result-object v1

    #@184
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@187
    .line 1317
    const-string v1, " mHaveFrame="

    #@189
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18c
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mHaveFrame:Z

    #@18e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@191
    .line 1318
    const-string v1, " mObscured="

    #@193
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@196
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mObscured:Z

    #@198
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@19b
    .line 1319
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19e
    const-string v1, "mSeq="

    #@1a0
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a3
    iget v1, p0, Lcom/android/server/wm/WindowState;->mSeq:I

    #@1a5
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1a8
    .line 1320
    const-string v1, " mSystemUiVisibility=0x"

    #@1aa
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ad
    .line 1321
    iget v1, p0, Lcom/android/server/wm/WindowState;->mSystemUiVisibility:I

    #@1af
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1b2
    move-result-object v1

    #@1b3
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b6
    .line 1323
    :cond_1b6
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@1b8
    if-eqz v1, :cond_1c2

    #@1ba
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@1bc
    if-eqz v1, :cond_1c2

    #@1be
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@1c0
    if-eqz v1, :cond_1e3

    #@1c2
    .line 1324
    :cond_1c2
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c5
    const-string v1, "mPolicyVisibility="

    #@1c7
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ca
    .line 1325
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@1cc
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@1cf
    .line 1326
    const-string v1, " mPolicyVisibilityAfterAnim="

    #@1d1
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d4
    .line 1327
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@1d6
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@1d9
    .line 1328
    const-string v1, " mAttachedHidden="

    #@1db
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1de
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@1e0
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@1e3
    .line 1330
    :cond_1e3
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRelayoutCalled:Z

    #@1e5
    if-eqz v1, :cond_1eb

    #@1e7
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mLayoutNeeded:Z

    #@1e9
    if-eqz v1, :cond_202

    #@1eb
    .line 1331
    :cond_1eb
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ee
    const-string v1, "mRelayoutCalled="

    #@1f0
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f3
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRelayoutCalled:Z

    #@1f5
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@1f8
    .line 1332
    const-string v1, " mLayoutNeeded="

    #@1fa
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1fd
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mLayoutNeeded:Z

    #@1ff
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@202
    .line 1334
    :cond_202
    iget v1, p0, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@204
    if-nez v1, :cond_20a

    #@206
    iget v1, p0, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@208
    if-eqz v1, :cond_221

    #@20a
    .line 1335
    :cond_20a
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20d
    const-string v1, "Offsets x="

    #@20f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@212
    iget v1, p0, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@214
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@217
    .line 1336
    const-string v1, " y="

    #@219
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21c
    iget v1, p0, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@21e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    #@221
    .line 1338
    :cond_221
    if-eqz p3, :cond_27c

    #@223
    .line 1339
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@226
    const-string v1, "mGivenContentInsets="

    #@228
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22b
    .line 1340
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mGivenContentInsets:Landroid/graphics/Rect;

    #@22d
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@230
    .line 1341
    const-string v1, " mGivenVisibleInsets="

    #@232
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@235
    .line 1342
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mGivenVisibleInsets:Landroid/graphics/Rect;

    #@237
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@23a
    .line 1343
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@23d
    .line 1344
    iget v1, p0, Lcom/android/server/wm/WindowState;->mTouchableInsets:I

    #@23f
    if-nez v1, :cond_245

    #@241
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mGivenInsetsPending:Z

    #@243
    if-eqz v1, :cond_26f

    #@245
    .line 1345
    :cond_245
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@248
    const-string v1, "mTouchableInsets="

    #@24a
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24d
    iget v1, p0, Lcom/android/server/wm/WindowState;->mTouchableInsets:I

    #@24f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@252
    .line 1346
    const-string v1, " mGivenInsetsPending="

    #@254
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@257
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mGivenInsetsPending:Z

    #@259
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@25c
    .line 1347
    new-instance v0, Landroid/graphics/Region;

    #@25e
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@261
    .line 1348
    .local v0, region:Landroid/graphics/Region;
    invoke-virtual {p0, v0}, Lcom/android/server/wm/WindowState;->getTouchableRegion(Landroid/graphics/Region;)V

    #@264
    .line 1349
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@267
    const-string v1, "touchable region="

    #@269
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@26f
    .line 1351
    .end local v0           #region:Landroid/graphics/Region;
    :cond_26f
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@272
    const-string v1, "mConfiguration="

    #@274
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@277
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@279
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@27c
    .line 1353
    :cond_27c
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27f
    const-string v1, "mHasSurface="

    #@281
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@284
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@286
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@289
    .line 1354
    const-string v1, " mShownFrame="

    #@28b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28e
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@290
    invoke-virtual {v1, p1}, Landroid/graphics/RectF;->printShortString(Ljava/io/PrintWriter;)V

    #@293
    .line 1355
    const-string v1, " isReadyForDisplay()="

    #@295
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@298
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isReadyForDisplay()Z

    #@29b
    move-result v1

    #@29c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@29f
    .line 1356
    if-eqz p3, :cond_2d5

    #@2a1
    .line 1357
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a4
    const-string v1, "mFrame="

    #@2a6
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a9
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@2ab
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@2ae
    .line 1358
    const-string v1, " last="

    #@2b0
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b3
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mLastFrame:Landroid/graphics/Rect;

    #@2b5
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@2b8
    .line 1359
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@2bb
    .line 1360
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2be
    const-string v1, "mSystemDecorRect="

    #@2c0
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c3
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@2c5
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@2c8
    .line 1361
    const-string v1, " last="

    #@2ca
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2cd
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mLastSystemDecorRect:Landroid/graphics/Rect;

    #@2cf
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@2d2
    .line 1362
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@2d5
    .line 1364
    :cond_2d5
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@2d7
    if-eqz v1, :cond_2e9

    #@2d9
    .line 1365
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2dc
    const-string v1, "mCompatFrame="

    #@2de
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e1
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@2e3
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@2e6
    .line 1366
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@2e9
    .line 1368
    :cond_2e9
    if-eqz p3, :cond_35d

    #@2eb
    .line 1369
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2ee
    const-string v1, "Frames: containing="

    #@2f0
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2f3
    .line 1370
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mContainingFrame:Landroid/graphics/Rect;

    #@2f5
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@2f8
    .line 1371
    const-string v1, " parent="

    #@2fa
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2fd
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mParentFrame:Landroid/graphics/Rect;

    #@2ff
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@302
    .line 1372
    const-string v1, " display="

    #@304
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@307
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mDisplayFrame:Landroid/graphics/Rect;

    #@309
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@30c
    .line 1373
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@30f
    .line 1374
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@312
    const-string v1, "    content="

    #@314
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@317
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mContentFrame:Landroid/graphics/Rect;

    #@319
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@31c
    .line 1375
    const-string v1, " visible="

    #@31e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@321
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mVisibleFrame:Landroid/graphics/Rect;

    #@323
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@326
    .line 1376
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@329
    .line 1377
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32c
    const-string v1, "Cur insets: content="

    #@32e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@331
    .line 1378
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mContentInsets:Landroid/graphics/Rect;

    #@333
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@336
    .line 1379
    const-string v1, " visible="

    #@338
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33b
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mVisibleInsets:Landroid/graphics/Rect;

    #@33d
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@340
    .line 1380
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@343
    .line 1381
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@346
    const-string v1, "Lst insets: content="

    #@348
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@34b
    .line 1382
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mLastContentInsets:Landroid/graphics/Rect;

    #@34d
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@350
    .line 1383
    const-string v1, " visible="

    #@352
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@355
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mLastVisibleInsets:Landroid/graphics/Rect;

    #@357
    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->printShortString(Ljava/io/PrintWriter;)V

    #@35a
    .line 1384
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@35d
    .line 1386
    :cond_35d
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@360
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@362
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@365
    const-string v1, ":"

    #@367
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@36a
    .line 1387
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@36c
    new-instance v2, Ljava/lang/StringBuilder;

    #@36e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@371
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@374
    move-result-object v2

    #@375
    const-string v3, "  "

    #@377
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37a
    move-result-object v2

    #@37b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37e
    move-result-object v2

    #@37f
    invoke-virtual {v1, p1, v2, p3}, Lcom/android/server/wm/WindowStateAnimator;->dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V

    #@382
    .line 1388
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@384
    if-nez v1, :cond_392

    #@386
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRemoveOnExit:Z

    #@388
    if-nez v1, :cond_392

    #@38a
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@38c
    if-nez v1, :cond_392

    #@38e
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRemoved:Z

    #@390
    if-eqz v1, :cond_3bd

    #@392
    .line 1389
    :cond_392
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@395
    const-string v1, "mExiting="

    #@397
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@39a
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@39c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@39f
    .line 1390
    const-string v1, " mRemoveOnExit="

    #@3a1
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a4
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRemoveOnExit:Z

    #@3a6
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@3a9
    .line 1391
    const-string v1, " mDestroying="

    #@3ab
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3ae
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@3b0
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@3b3
    .line 1392
    const-string v1, " mRemoved="

    #@3b5
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3b8
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRemoved:Z

    #@3ba
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@3bd
    .line 1394
    :cond_3bd
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@3bf
    if-nez v1, :cond_3c9

    #@3c1
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAppFreezing:Z

    #@3c3
    if-nez v1, :cond_3c9

    #@3c5
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mTurnOnScreen:Z

    #@3c7
    if-eqz v1, :cond_3ea

    #@3c9
    .line 1395
    :cond_3c9
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3cc
    const-string v1, "mOrientationChanging="

    #@3ce
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d1
    .line 1396
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@3d3
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@3d6
    .line 1397
    const-string v1, " mAppFreezing="

    #@3d8
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3db
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAppFreezing:Z

    #@3dd
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@3e0
    .line 1398
    const-string v1, " mTurnOnScreen="

    #@3e2
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3e5
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mTurnOnScreen:Z

    #@3e7
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@3ea
    .line 1400
    :cond_3ea
    iget v1, p0, Lcom/android/server/wm/WindowState;->mHScale:F

    #@3ec
    cmpl-float v1, v1, v5

    #@3ee
    if-nez v1, :cond_3f6

    #@3f0
    iget v1, p0, Lcom/android/server/wm/WindowState;->mVScale:F

    #@3f2
    cmpl-float v1, v1, v5

    #@3f4
    if-eqz v1, :cond_40d

    #@3f6
    .line 1401
    :cond_3f6
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f9
    const-string v1, "mHScale="

    #@3fb
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3fe
    iget v1, p0, Lcom/android/server/wm/WindowState;->mHScale:F

    #@400
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@403
    .line 1402
    const-string v1, " mVScale="

    #@405
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@408
    iget v1, p0, Lcom/android/server/wm/WindowState;->mVScale:F

    #@40a
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(F)V

    #@40d
    .line 1404
    :cond_40d
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperX:F

    #@40f
    cmpl-float v1, v1, v4

    #@411
    if-nez v1, :cond_419

    #@413
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperY:F

    #@415
    cmpl-float v1, v1, v4

    #@417
    if-eqz v1, :cond_430

    #@419
    .line 1405
    :cond_419
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@41c
    const-string v1, "mWallpaperX="

    #@41e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@421
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperX:F

    #@423
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@426
    .line 1406
    const-string v1, " mWallpaperY="

    #@428
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42b
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperY:F

    #@42d
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(F)V

    #@430
    .line 1408
    :cond_430
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperXStep:F

    #@432
    cmpl-float v1, v1, v4

    #@434
    if-nez v1, :cond_43c

    #@436
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperYStep:F

    #@438
    cmpl-float v1, v1, v4

    #@43a
    if-eqz v1, :cond_453

    #@43c
    .line 1409
    :cond_43c
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43f
    const-string v1, "mWallpaperXStep="

    #@441
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@444
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperXStep:F

    #@446
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@449
    .line 1410
    const-string v1, " mWallpaperYStep="

    #@44b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@44e
    iget v1, p0, Lcom/android/server/wm/WindowState;->mWallpaperYStep:F

    #@450
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(F)V

    #@453
    .line 1412
    :cond_453
    return-void

    #@454
    .line 1300
    :cond_454
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@456
    if-eqz v1, :cond_460

    #@458
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@45a
    iget-object v1, v1, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@45c
    iget v1, v1, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@45e
    goto/16 :goto_11d

    #@460
    :cond_460
    const/4 v1, 0x0

    #@461
    goto/16 :goto_11d
.end method

.method public getAppToken()Landroid/view/IApplicationToken;
    .registers 2

    #@0
    .prologue
    .line 746
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@6
    iget-object v0, v0, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getAttrs()Landroid/view/WindowManager$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 702
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@2
    return-object v0
.end method

.method public getContentFrameLw()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 677
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mContentFrame:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public getDisplayFrameLw()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 672
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mDisplayFrame:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public getDisplayId()I
    .registers 2

    #@0
    .prologue
    .line 750
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@2
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getFrameLw()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 662
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public getGivenContentInsetsLw()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 692
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mGivenContentInsets:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public getGivenInsetsPendingLw()Z
    .registers 2

    #@0
    .prologue
    .line 687
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mGivenInsetsPending:Z

    #@2
    return v0
.end method

.method public getGivenVisibleInsetsLw()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 697
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mGivenVisibleInsets:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public getInputDispatchingTimeoutNanos()J
    .registers 3

    #@0
    .prologue
    .line 754
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@6
    iget-wide v0, v0, Lcom/android/server/wm/AppWindowToken;->inputDispatchingTimeoutNanos:J

    #@8
    :goto_8
    return-wide v0

    #@9
    :cond_9
    const-wide v0, 0x12a05f200L

    #@e
    goto :goto_8
.end method

.method public getNeedsMenuLw(Landroid/view/WindowManagerPolicy$WindowState;)Z
    .registers 8
    .parameter "bottom"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 707
    const/4 v0, -0x1

    #@2
    .line 708
    .local v0, index:I
    move-object v2, p0

    #@3
    .line 709
    .local v2, ws:Lcom/android/server/wm/WindowState;
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getWindowList()Lcom/android/server/wm/WindowList;

    #@6
    move-result-object v1

    #@7
    .line 711
    .local v1, windows:Lcom/android/server/wm/WindowList;
    :goto_7
    iget-object v4, v2, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@9
    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@b
    and-int/lit8 v4, v4, 0x8

    #@d
    if-eqz v4, :cond_1a

    #@f
    .line 713
    iget-object v4, v2, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@11
    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@13
    const/high16 v5, 0x800

    #@15
    and-int/2addr v4, v5

    #@16
    if-eqz v4, :cond_19

    #@18
    const/4 v3, 0x1

    #@19
    .line 728
    :cond_19
    return v3

    #@1a
    .line 717
    :cond_1a
    if-eq v2, p1, :cond_19

    #@1c
    .line 723
    if-gez v0, :cond_22

    #@1e
    .line 724
    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowList;->indexOf(Ljava/lang/Object;)I

    #@21
    move-result v0

    #@22
    .line 726
    :cond_22
    add-int/lit8 v0, v0, -0x1

    #@24
    .line 727
    if-ltz v0, :cond_19

    #@26
    .line 730
    invoke-virtual {v1, v0}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v2

    #@2a
    .end local v2           #ws:Lcom/android/server/wm/WindowState;
    check-cast v2, Lcom/android/server/wm/WindowState;

    #@2c
    .restart local v2       #ws:Lcom/android/server/wm/WindowState;
    goto :goto_7
.end method

.method public getShownFrameLw()Landroid/graphics/RectF;
    .registers 2

    #@0
    .prologue
    .line 667
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@2
    return-object v0
.end method

.method public getSplitScreenId()I
    .registers 2

    #@0
    .prologue
    .line 1435
    iget v0, p0, Lcom/android/server/wm/WindowState;->mScreenId:I

    #@2
    return v0
.end method

.method public getSurfaceLayer()I
    .registers 2

    #@0
    .prologue
    .line 741
    iget v0, p0, Lcom/android/server/wm/WindowState;->mLayer:I

    #@2
    return v0
.end method

.method public getSystemUiVisibility()I
    .registers 2

    #@0
    .prologue
    .line 736
    iget v0, p0, Lcom/android/server/wm/WindowState;->mSystemUiVisibility:I

    #@2
    return v0
.end method

.method public getTouchableRegion(Landroid/graphics/Region;)V
    .registers 6
    .parameter "outRegion"

    #@0
    .prologue
    .line 1247
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@2
    .line 1248
    .local v0, frame:Landroid/graphics/Rect;
    iget v2, p0, Lcom/android/server/wm/WindowState;->mTouchableInsets:I

    #@4
    packed-switch v2, :pswitch_data_24

    #@7
    .line 1251
    invoke-virtual {p1, v0}, Landroid/graphics/Region;->set(Landroid/graphics/Rect;)Z

    #@a
    .line 1266
    :goto_a
    return-void

    #@b
    .line 1254
    :pswitch_b
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mGivenContentInsets:Landroid/graphics/Rect;

    #@d
    invoke-static {p1, v0, v2}, Lcom/android/server/wm/WindowState;->applyInsets(Landroid/graphics/Region;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@10
    goto :goto_a

    #@11
    .line 1257
    :pswitch_11
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mGivenVisibleInsets:Landroid/graphics/Rect;

    #@13
    invoke-static {p1, v0, v2}, Lcom/android/server/wm/WindowState;->applyInsets(Landroid/graphics/Region;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@16
    goto :goto_a

    #@17
    .line 1260
    :pswitch_17
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mGivenTouchableRegion:Landroid/graphics/Region;

    #@19
    .line 1261
    .local v1, givenTouchableRegion:Landroid/graphics/Region;
    invoke-virtual {p1, v1}, Landroid/graphics/Region;->set(Landroid/graphics/Region;)Z

    #@1c
    .line 1262
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@1e
    iget v3, v0, Landroid/graphics/Rect;->top:I

    #@20
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Region;->translate(II)V

    #@23
    goto :goto_a

    #@24
    .line 1248
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_b
        :pswitch_11
        :pswitch_17
    .end packed-switch
.end method

.method public getVisibleFrameLw()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 682
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mVisibleFrame:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method getWindowList()Lcom/android/server/wm/WindowList;
    .registers 2

    #@0
    .prologue
    .line 1269
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@2
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getWindowList()Lcom/android/server/wm/WindowList;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method getWindowMagnificationSpecLocked()Lcom/android/server/wm/MagnificationSpec;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 646
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@3
    iget-object v0, v2, Lcom/android/server/wm/DisplayContent;->mMagnificationSpec:Lcom/android/server/wm/MagnificationSpec;

    #@5
    .line 647
    .local v0, spec:Lcom/android/server/wm/MagnificationSpec;
    if-eqz v0, :cond_1e

    #@7
    invoke-virtual {v0}, Lcom/android/server/wm/MagnificationSpec;->isNop()Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_1e

    #@d
    .line 648
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@f
    if-eqz v2, :cond_1f

    #@11
    .line 649
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@13
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@15
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@17
    invoke-interface {v2, v3}, Landroid/view/WindowManagerPolicy;->canMagnifyWindowLw(Landroid/view/WindowManager$LayoutParams;)Z

    #@1a
    move-result v2

    #@1b
    if-nez v2, :cond_1f

    #@1d
    move-object v0, v1

    #@1e
    .line 657
    .end local v0           #spec:Lcom/android/server/wm/MagnificationSpec;
    :cond_1e
    :goto_1e
    return-object v0

    #@1f
    .line 653
    .restart local v0       #spec:Lcom/android/server/wm/MagnificationSpec;
    :cond_1f
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@21
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@23
    invoke-interface {v2, v3}, Landroid/view/WindowManagerPolicy;->canMagnifyWindowLw(Landroid/view/WindowManager$LayoutParams;)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_1e

    #@29
    move-object v0, v1

    #@2a
    .line 654
    goto :goto_1e
.end method

.method public hasAppShownWindows()Z
    .registers 2

    #@0
    .prologue
    .line 761
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@6
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowToken;->firstWindowDrawn:Z

    #@8
    if-nez v0, :cond_10

    #@a
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@c
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowToken;->startingDisplayed:Z

    #@e
    if-eqz v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public hasDrawnLw()Z
    .registers 3

    #@0
    .prologue
    .line 1085
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@2
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@4
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public hideLw(Z)Z
    .registers 3
    .parameter "doAnimation"

    #@0
    .prologue
    .line 1143
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/WindowState;->hideLw(ZZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method hideLw(ZZ)Z
    .registers 10
    .parameter "doAnimation"
    .parameter "requestAnim"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1147
    if-eqz p1, :cond_d

    #@4
    .line 1148
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@6
    invoke-virtual {v5}, Lcom/android/server/wm/WindowManagerService;->okToDisplay()Z

    #@9
    move-result v5

    #@a
    if-nez v5, :cond_d

    #@c
    .line 1149
    const/4 p1, 0x0

    #@d
    .line 1152
    :cond_d
    if-eqz p1, :cond_14

    #@f
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@11
    .line 1154
    .local v1, current:Z
    :goto_11
    if-nez v1, :cond_17

    #@13
    .line 1196
    :goto_13
    return v3

    #@14
    .line 1152
    .end local v1           #current:Z
    :cond_14
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@16
    goto :goto_11

    #@17
    .line 1162
    .restart local v1       #current:Z
    :cond_17
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@19
    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1b
    const/16 v6, 0x7d4

    #@1d
    if-ne v5, v6, :cond_3f

    #@1f
    .line 1163
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@21
    invoke-virtual {v5}, Lcom/android/server/wm/WindowList;->size()I

    #@24
    move-result v2

    #@25
    .line 1164
    .local v2, sizeOfChild:I
    :cond_25
    :goto_25
    if-lez v2, :cond_3f

    #@27
    .line 1165
    add-int/lit8 v2, v2, -0x1

    #@29
    .line 1166
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@2b
    invoke-virtual {v5, v2}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Lcom/android/server/wm/WindowState;

    #@31
    .line 1167
    .local v0, c:Lcom/android/server/wm/WindowState;
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@33
    iget-object v6, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@35
    invoke-interface {v5, v0, v6}, Landroid/view/WindowManagerPolicy;->canBeForceHidden(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    #@38
    move-result v5

    #@39
    if-eqz v5, :cond_25

    #@3b
    .line 1168
    invoke-virtual {v0, p1}, Lcom/android/server/wm/WindowState;->hideLw(Z)Z

    #@3e
    goto :goto_25

    #@3f
    .line 1173
    .end local v0           #c:Lcom/android/server/wm/WindowState;
    .end local v2           #sizeOfChild:I
    :cond_3f
    if-eqz p1, :cond_4f

    #@41
    .line 1174
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@43
    const/16 v6, 0x2002

    #@45
    invoke-virtual {v5, v6, v3}, Lcom/android/server/wm/WindowStateAnimator;->applyAnimationLocked(IZ)Z

    #@48
    .line 1175
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@4a
    iget-object v5, v5, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@4c
    if-nez v5, :cond_4f

    #@4e
    .line 1176
    const/4 p1, 0x0

    #@4f
    .line 1179
    :cond_4f
    if-eqz p1, :cond_5c

    #@51
    .line 1180
    iput-boolean v3, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@53
    .line 1193
    :cond_53
    :goto_53
    if-eqz p2, :cond_5a

    #@55
    .line 1194
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@57
    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->updateLayoutToAnimationLocked()V

    #@5a
    :cond_5a
    move v3, v4

    #@5b
    .line 1196
    goto :goto_13

    #@5c
    .line 1183
    :cond_5c
    iput-boolean v3, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@5e
    .line 1184
    iput-boolean v3, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@60
    .line 1188
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@62
    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->enableScreenIfNeededLocked()V

    #@65
    .line 1189
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@67
    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    #@69
    if-ne v3, p0, :cond_53

    #@6b
    .line 1190
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@6d
    iput-boolean v4, v3, Lcom/android/server/wm/WindowManagerService;->mFocusMayChange:Z

    #@6f
    goto :goto_53
.end method

.method public isAlive()Z
    .registers 2

    #@0
    .prologue
    .line 1201
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@2
    invoke-interface {v0}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public isAnimatingLw()Z
    .registers 2

    #@0
    .prologue
    .line 941
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method isClosing()Z
    .registers 3

    #@0
    .prologue
    .line 1205
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@2
    if-nez v0, :cond_10

    #@4
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@6
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mClosingApps:Ljava/util/ArrayList;

    #@8
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method isConfigChanged()Z
    .registers 4

    #@0
    .prologue
    .line 994
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@2
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mCurConfiguration:Landroid/content/res/Configuration;

    #@6
    if-eq v1, v2, :cond_29

    #@8
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@a
    if-eqz v1, :cond_18

    #@c
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@e
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@10
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mCurConfiguration:Landroid/content/res/Configuration;

    #@12
    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_29

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    .line 998
    .local v0, configChanged:Z
    :goto_19
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@1b
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1d
    const/16 v2, 0x7d4

    #@1f
    if-ne v1, v2, :cond_28

    #@21
    .line 1000
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mConfigHasChanged:Z

    #@23
    or-int/2addr v1, v0

    #@24
    iput-boolean v1, p0, Lcom/android/server/wm/WindowState;->mConfigHasChanged:Z

    #@26
    .line 1001
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mConfigHasChanged:Z

    #@28
    .line 1004
    :cond_28
    return v0

    #@29
    .line 994
    .end local v0           #configChanged:Z
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_19
.end method

.method isConfigDiff(I)Z
    .registers 4
    .parameter "mask"

    #@0
    .prologue
    .line 1008
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@2
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mCurConfiguration:Landroid/content/res/Configuration;

    #@6
    if-eq v0, v1, :cond_1b

    #@8
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@a
    if-eqz v0, :cond_1b

    #@c
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@e
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@10
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mCurConfiguration:Landroid/content/res/Configuration;

    #@12
    invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@15
    move-result v0

    #@16
    and-int/2addr v0, p1

    #@17
    if-eqz v0, :cond_1b

    #@19
    const/4 v0, 0x1

    #@1a
    :goto_1a
    return v0

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_1a
.end method

.method public isDefaultDisplay()Z
    .registers 2

    #@0
    .prologue
    .line 1210
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@2
    iget-boolean v0, v0, Lcom/android/server/wm/DisplayContent;->isDefaultDisplay:Z

    #@4
    return v0
.end method

.method public isDisplayedLw()Z
    .registers 3

    #@0
    .prologue
    .line 927
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    .line 928
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isDrawnLw()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_26

    #@8
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@a
    if-eqz v1, :cond_26

    #@c
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@e
    if-nez v1, :cond_16

    #@10
    if-eqz v0, :cond_24

    #@12
    iget-boolean v1, v0, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@14
    if-eqz v1, :cond_24

    #@16
    :cond_16
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@18
    iget-boolean v1, v1, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@1a
    if-nez v1, :cond_24

    #@1c
    if-eqz v0, :cond_26

    #@1e
    iget-object v1, v0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@20
    iget-object v1, v1, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@22
    if-eqz v1, :cond_26

    #@24
    :cond_24
    const/4 v1, 0x1

    #@25
    :goto_25
    return v1

    #@26
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_25
.end method

.method public isDrawnLw()Z
    .registers 3

    #@0
    .prologue
    .line 960
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@2
    if-eqz v0, :cond_18

    #@4
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@6
    if-nez v0, :cond_18

    #@8
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@a
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@c
    const/4 v1, 0x3

    #@d
    if-eq v0, v1, :cond_16

    #@f
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@11
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@13
    const/4 v1, 0x4

    #@14
    if-ne v0, v1, :cond_18

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method isFullscreen(II)Z
    .registers 4
    .parameter "screenWidth"
    .parameter "screenHeight"

    #@0
    .prologue
    .line 989
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4
    if-gtz v0, :cond_1a

    #@6
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@8
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@a
    if-gtz v0, :cond_1a

    #@c
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@e
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@10
    if-lt v0, p1, :cond_1a

    #@12
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@14
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@16
    if-lt v0, p2, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public isGoneForLayoutLw()Z
    .registers 4

    #@0
    .prologue
    .line 946
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    .line 947
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    iget v1, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@4
    const/16 v2, 0x8

    #@6
    if-eq v1, v2, :cond_2a

    #@8
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRelayoutCalled:Z

    #@a
    if-eqz v1, :cond_2a

    #@c
    if-nez v0, :cond_14

    #@e
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@10
    iget-boolean v1, v1, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@12
    if-nez v1, :cond_2a

    #@14
    :cond_14
    if-eqz v0, :cond_1e

    #@16
    iget-boolean v1, v0, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@18
    if-nez v1, :cond_2a

    #@1a
    iget-boolean v1, v0, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@1c
    if-nez v1, :cond_2a

    #@1e
    :cond_1e
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@20
    if-nez v1, :cond_2a

    #@22
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@24
    if-nez v1, :cond_2a

    #@26
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@28
    if-eqz v1, :cond_2c

    #@2a
    :cond_2a
    const/4 v1, 0x1

    #@2b
    :goto_2b
    return v1

    #@2c
    :cond_2c
    const/4 v1, 0x0

    #@2d
    goto :goto_2b
.end method

.method isHiddenFromUserLocked()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1219
    move-object v1, p0

    #@2
    .line 1220
    .local v1, win:Lcom/android/server/wm/WindowState;
    :goto_2
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@4
    if-eqz v3, :cond_9

    #@6
    .line 1221
    iget-object v1, v1, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@8
    goto :goto_2

    #@9
    .line 1223
    :cond_9
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@b
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@d
    const/16 v4, 0x7d0

    #@f
    if-ge v3, v4, :cond_3e

    #@11
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@13
    if-eqz v3, :cond_3e

    #@15
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@17
    iget-boolean v3, v3, Lcom/android/server/wm/AppWindowToken;->showWhenLocked:Z

    #@19
    if-eqz v3, :cond_3e

    #@1b
    .line 1227
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@1d
    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@20
    move-result-object v0

    #@21
    .line 1228
    .local v0, displayInfo:Landroid/view/DisplayInfo;
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@23
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@25
    if-gtz v3, :cond_3e

    #@27
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@29
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@2b
    if-gtz v3, :cond_3e

    #@2d
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@2f
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@31
    iget v4, v0, Landroid/view/DisplayInfo;->appWidth:I

    #@33
    if-lt v3, v4, :cond_3e

    #@35
    iget-object v3, v1, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@37
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@39
    iget v4, v0, Landroid/view/DisplayInfo;->appHeight:I

    #@3b
    if-lt v3, v4, :cond_3e

    #@3d
    .line 1236
    .end local v0           #displayInfo:Landroid/view/DisplayInfo;
    :cond_3d
    :goto_3d
    return v2

    #@3e
    :cond_3e
    iget-boolean v3, v1, Lcom/android/server/wm/WindowState;->mShowToOwnerOnly:Z

    #@40
    if-eqz v3, :cond_3d

    #@42
    iget v3, v1, Lcom/android/server/wm/WindowState;->mOwnerUid:I

    #@44
    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    #@47
    move-result v3

    #@48
    iget-object v4, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4a
    iget v4, v4, Lcom/android/server/wm/WindowManagerService;->mCurrentUserId:I

    #@4c
    if-eq v3, v4, :cond_3d

    #@4e
    const/4 v2, 0x1

    #@4f
    goto :goto_3d
.end method

.method isIdentityMatrix(FFFF)Z
    .registers 11
    .parameter "dsdx"
    .parameter "dtdx"
    .parameter "dsdy"
    .parameter "dtdy"

    #@0
    .prologue
    const v5, 0x3f800054

    #@3
    const v4, 0x3f7fff58

    #@6
    const v3, 0x358637bd

    #@9
    const v2, -0x4a79c843

    #@c
    const/4 v0, 0x0

    #@d
    .line 765
    cmpg-float v1, p1, v4

    #@f
    if-ltz v1, :cond_15

    #@11
    cmpl-float v1, p1, v5

    #@13
    if-lez v1, :cond_16

    #@15
    .line 769
    :cond_15
    :goto_15
    return v0

    #@16
    .line 766
    :cond_16
    cmpg-float v1, p4, v4

    #@18
    if-ltz v1, :cond_15

    #@1a
    cmpl-float v1, p4, v5

    #@1c
    if-gtz v1, :cond_15

    #@1e
    .line 767
    cmpg-float v1, p2, v2

    #@20
    if-ltz v1, :cond_15

    #@22
    cmpl-float v1, p2, v3

    #@24
    if-gtz v1, :cond_15

    #@26
    .line 768
    cmpg-float v1, p3, v2

    #@28
    if-ltz v1, :cond_15

    #@2a
    cmpl-float v1, p3, v3

    #@2c
    if-gtz v1, :cond_15

    #@2e
    .line 769
    const/4 v0, 0x1

    #@2f
    goto :goto_15
.end method

.method isOnScreen()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 871
    iget-boolean v3, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@4
    if-eqz v3, :cond_e

    #@6
    iget-boolean v3, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@8
    if-eqz v3, :cond_e

    #@a
    iget-boolean v3, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@c
    if-eqz v3, :cond_f

    #@e
    .line 879
    :cond_e
    :goto_e
    return v1

    #@f
    .line 874
    :cond_f
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@11
    .line 875
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    if-eqz v0, :cond_29

    #@13
    .line 876
    iget-boolean v3, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@15
    if-nez v3, :cond_1b

    #@17
    iget-boolean v3, v0, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@19
    if-eqz v3, :cond_27

    #@1b
    :cond_1b
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@1d
    iget-object v3, v3, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@1f
    if-nez v3, :cond_27

    #@21
    iget-object v3, v0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@23
    iget-object v3, v3, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@25
    if-eqz v3, :cond_e

    #@27
    :cond_27
    move v1, v2

    #@28
    goto :goto_e

    #@29
    .line 879
    :cond_29
    iget-boolean v3, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@2b
    if-eqz v3, :cond_33

    #@2d
    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@2f
    iget-object v3, v3, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@31
    if-eqz v3, :cond_e

    #@33
    :cond_33
    move v1, v2

    #@34
    goto :goto_e
.end method

.method isOpaqueDrawn()Z
    .registers 3

    #@0
    .prologue
    .line 970
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@2
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@4
    const/4 v1, -0x1

    #@5
    if-eq v0, v1, :cond_f

    #@7
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@9
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@b
    const/16 v1, 0x7dd

    #@d
    if-ne v0, v1, :cond_29

    #@f
    :cond_f
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isDrawnLw()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_29

    #@15
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@17
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@19
    if-nez v0, :cond_29

    #@1b
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@1d
    if-eqz v0, :cond_27

    #@1f
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@21
    iget-object v0, v0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@23
    iget-object v0, v0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@25
    if-nez v0, :cond_29

    #@27
    :cond_27
    const/4 v0, 0x1

    #@28
    :goto_28
    return v0

    #@29
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_28
.end method

.method isPotentialDragTarget()Z
    .registers 2

    #@0
    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isVisibleNow()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_14

    #@6
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mRemoved:Z

    #@8
    if-nez v0, :cond_14

    #@a
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@c
    if-eqz v0, :cond_14

    #@e
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@10
    if-eqz v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method isReadyForDisplay()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 887
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@3
    iget-boolean v1, v1, Lcom/android/server/wm/WindowToken;->waitingToShow:Z

    #@5
    if-eqz v1, :cond_f

    #@7
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9
    iget v1, v1, Lcom/android/server/wm/WindowManagerService;->mNextAppTransition:I

    #@b
    const/4 v2, -0x1

    #@c
    if-eq v1, v2, :cond_f

    #@e
    .line 891
    :cond_e
    :goto_e
    return v0

    #@f
    :cond_f
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@11
    if-eqz v1, :cond_e

    #@13
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@15
    if-eqz v1, :cond_e

    #@17
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@19
    if-nez v1, :cond_e

    #@1b
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@1d
    if-nez v1, :cond_29

    #@1f
    iget v1, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@21
    if-nez v1, :cond_29

    #@23
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@25
    iget-boolean v1, v1, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@27
    if-eqz v1, :cond_3b

    #@29
    :cond_29
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@2b
    iget-object v1, v1, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@2d
    if-nez v1, :cond_3b

    #@2f
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@31
    if-eqz v1, :cond_e

    #@33
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@35
    iget-object v1, v1, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@37
    iget-object v1, v1, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@39
    if-eqz v1, :cond_e

    #@3b
    :cond_3b
    const/4 v0, 0x1

    #@3c
    goto :goto_e
.end method

.method isReadyForDisplayIgnoringKeyguard()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 903
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@3
    iget-boolean v2, v2, Lcom/android/server/wm/WindowToken;->waitingToShow:Z

    #@5
    if-eqz v2, :cond_f

    #@7
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9
    iget v2, v2, Lcom/android/server/wm/WindowManagerService;->mNextAppTransition:I

    #@b
    const/4 v3, -0x1

    #@c
    if-eq v2, v3, :cond_f

    #@e
    .line 913
    :cond_e
    :goto_e
    return v1

    #@f
    .line 907
    :cond_f
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@11
    .line 908
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    if-nez v0, :cond_17

    #@13
    iget-boolean v2, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@15
    if-eqz v2, :cond_e

    #@17
    .line 913
    :cond_17
    iget-boolean v2, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@19
    if-eqz v2, :cond_e

    #@1b
    iget-boolean v2, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@1d
    if-nez v2, :cond_e

    #@1f
    iget-boolean v2, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@21
    if-nez v2, :cond_2d

    #@23
    iget v2, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@25
    if-nez v2, :cond_2d

    #@27
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@29
    iget-boolean v2, v2, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@2b
    if-eqz v2, :cond_43

    #@2d
    :cond_2d
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@2f
    iget-object v2, v2, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@31
    if-nez v2, :cond_43

    #@33
    if-eqz v0, :cond_e

    #@35
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@37
    iget-object v2, v2, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@39
    if-eqz v2, :cond_e

    #@3b
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@3d
    invoke-virtual {v2}, Lcom/android/server/wm/WindowStateAnimator;->isDummyAnimation()Z

    #@40
    move-result v2

    #@41
    if-nez v2, :cond_e

    #@43
    :cond_43
    const/4 v1, 0x1

    #@44
    goto :goto_e
.end method

.method public isSplitFullScreen()Z
    .registers 2

    #@0
    .prologue
    .line 1440
    iget v0, p0, Lcom/android/server/wm/WindowState;->mScreenId:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mIsSplitFullScreen:Z

    #@8
    goto :goto_5
.end method

.method public isVisibleLw()Z
    .registers 3

    #@0
    .prologue
    .line 792
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    .line 793
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@4
    if-eqz v1, :cond_1e

    #@6
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@8
    if-eqz v1, :cond_1e

    #@a
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@c
    if-nez v1, :cond_1e

    #@e
    if-eqz v0, :cond_14

    #@10
    iget-boolean v1, v0, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@12
    if-nez v1, :cond_1e

    #@14
    :cond_14
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@16
    if-nez v1, :cond_1e

    #@18
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@1a
    if-nez v1, :cond_1e

    #@1c
    const/4 v1, 0x1

    #@1d
    :goto_1d
    return v1

    #@1e
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_1d
.end method

.method isVisibleNow()Z
    .registers 2

    #@0
    .prologue
    .line 839
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@6
    if-eqz v0, :cond_1c

    #@8
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@a
    if-nez v0, :cond_1c

    #@c
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@e
    iget-boolean v0, v0, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@10
    if-nez v0, :cond_1c

    #@12
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@14
    if-nez v0, :cond_1c

    #@16
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@18
    if-nez v0, :cond_1c

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method isVisibleOrAdding()Z
    .registers 3

    #@0
    .prologue
    .line 858
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    .line 859
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@4
    if-nez v1, :cond_e

    #@6
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mRelayoutCalled:Z

    #@8
    if-nez v1, :cond_26

    #@a
    iget v1, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@c
    if-nez v1, :cond_26

    #@e
    :cond_e
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@10
    if-eqz v1, :cond_26

    #@12
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@14
    if-nez v1, :cond_26

    #@16
    if-eqz v0, :cond_1c

    #@18
    iget-boolean v1, v0, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@1a
    if-nez v1, :cond_26

    #@1c
    :cond_1c
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@1e
    if-nez v1, :cond_26

    #@20
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@22
    if-nez v1, :cond_26

    #@24
    const/4 v1, 0x1

    #@25
    :goto_25
    return v1

    #@26
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_25
.end method

.method public isVisibleOrBehindKeyguardLw()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 808
    iget-object v4, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@4
    iget-boolean v4, v4, Lcom/android/server/wm/WindowToken;->waitingToShow:Z

    #@6
    if-eqz v4, :cond_10

    #@8
    iget-object v4, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@a
    iget v4, v4, Lcom/android/server/wm/WindowManagerService;->mNextAppTransition:I

    #@c
    const/4 v5, -0x1

    #@d
    if-eq v4, v5, :cond_10

    #@f
    .line 815
    :goto_f
    return v3

    #@10
    .line 812
    :cond_10
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@12
    .line 813
    .local v1, atoken:Lcom/android/server/wm/AppWindowToken;
    if-eqz v1, :cond_47

    #@14
    iget-object v4, v1, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@16
    iget-object v4, v4, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@18
    if-eqz v4, :cond_45

    #@1a
    move v0, v2

    #@1b
    .line 815
    .local v0, animating:Z
    :goto_1b
    iget-boolean v4, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@1d
    if-eqz v4, :cond_4d

    #@1f
    iget-boolean v4, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@21
    if-nez v4, :cond_4d

    #@23
    iget-boolean v4, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@25
    if-nez v4, :cond_4d

    #@27
    if-nez v1, :cond_49

    #@29
    iget-boolean v4, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@2b
    if-eqz v4, :cond_4d

    #@2d
    :cond_2d
    iget-boolean v4, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@2f
    if-nez v4, :cond_3b

    #@31
    iget v4, p0, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    #@33
    if-nez v4, :cond_3b

    #@35
    iget-object v4, p0, Lcom/android/server/wm/WindowState;->mRootToken:Lcom/android/server/wm/WindowToken;

    #@37
    iget-boolean v4, v4, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@39
    if-eqz v4, :cond_43

    #@3b
    :cond_3b
    iget-object v4, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@3d
    iget-object v4, v4, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@3f
    if-nez v4, :cond_43

    #@41
    if-eqz v0, :cond_4d

    #@43
    :cond_43
    :goto_43
    move v3, v2

    #@44
    goto :goto_f

    #@45
    .end local v0           #animating:Z
    :cond_45
    move v0, v3

    #@46
    .line 813
    goto :goto_1b

    #@47
    :cond_47
    move v0, v3

    #@48
    goto :goto_1b

    #@49
    .line 815
    .restart local v0       #animating:Z
    :cond_49
    iget-boolean v4, v1, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@4b
    if-eqz v4, :cond_2d

    #@4d
    :cond_4d
    move v2, v3

    #@4e
    goto :goto_43
.end method

.method public isWinVisibleLw()Z
    .registers 3

    #@0
    .prologue
    .line 828
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    .line 829
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@4
    if-eqz v1, :cond_24

    #@6
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@8
    if-eqz v1, :cond_24

    #@a
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@c
    if-nez v1, :cond_24

    #@e
    if-eqz v0, :cond_1a

    #@10
    iget-boolean v1, v0, Lcom/android/server/wm/AppWindowToken;->hiddenRequested:Z

    #@12
    if-eqz v1, :cond_1a

    #@14
    iget-object v1, v0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@16
    iget-boolean v1, v1, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@18
    if-eqz v1, :cond_24

    #@1a
    :cond_1a
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@1c
    if-nez v1, :cond_24

    #@1e
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@20
    if-nez v1, :cond_24

    #@22
    const/4 v1, 0x1

    #@23
    :goto_23
    return v1

    #@24
    :cond_24
    const/4 v1, 0x0

    #@25
    goto :goto_23
.end method

.method makeInputChannelName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1415
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@8
    move-result v1

    #@9
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@19
    invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    return-object v0
.end method

.method prelayout()V
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    .line 773
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@4
    if-eqz v0, :cond_1d

    #@6
    .line 774
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@8
    iget v0, v0, Lcom/android/server/wm/WindowManagerService;->mCompatibleScreenScale:F

    #@a
    iput v0, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@c
    .line 776
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mUseWVGACompat:Z

    #@e
    if-eqz v0, :cond_16

    #@10
    .line 777
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@12
    iget v0, v0, Lcom/android/server/wm/WindowManagerService;->mCompatibleScreenScaleForWvga:F

    #@14
    iput v0, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@16
    .line 779
    :cond_16
    iget v0, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@18
    div-float v0, v1, v0

    #@1a
    iput v0, p0, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@1c
    .line 783
    :goto_1c
    return-void

    #@1d
    .line 781
    :cond_1d
    iput v1, p0, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@1f
    iput v1, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@21
    goto :goto_1c
.end method

.method removeLocked()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1014
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->disposeInputChannel()V

    #@4
    .line 1016
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 1018
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@a
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@c
    invoke-virtual {v0, p0}, Lcom/android/server/wm/WindowList;->remove(Ljava/lang/Object;)Z

    #@f
    .line 1020
    :cond_f
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@11
    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowStateAnimator;->destroyDeferredSurfaceLocked(Z)V

    #@14
    .line 1021
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@16
    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowStateAnimator;->destroySurfaceLocked(Z)V

    #@19
    .line 1022
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@1b
    invoke-virtual {v0}, Lcom/android/server/wm/Session;->windowRemovedLocked()V

    #@1e
    .line 1024
    :try_start_1e
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@20
    invoke-interface {v0}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@23
    move-result-object v0

    #@24
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mDeathRecipient:Lcom/android/server/wm/WindowState$DeathRecipient;

    #@26
    const/4 v2, 0x0

    #@27
    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_2a
    .catch Ljava/lang/RuntimeException; {:try_start_1e .. :try_end_2a} :catch_2b

    #@2a
    .line 1029
    :goto_2a
    return-void

    #@2b
    .line 1025
    :catch_2b
    move-exception v0

    #@2c
    goto :goto_2a
.end method

.method setConfiguration(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 1032
    iput-object p1, p0, Lcom/android/server/wm/WindowState;->mConfiguration:Landroid/content/res/Configuration;

    #@2
    .line 1033
    const/4 v0, 0x0

    #@3
    iput-boolean v0, p0, Lcom/android/server/wm/WindowState;->mConfigHasChanged:Z

    #@5
    .line 1034
    return-void
.end method

.method setInputChannel(Landroid/view/InputChannel;)V
    .registers 4
    .parameter "inputChannel"

    #@0
    .prologue
    .line 1037
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1038
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Window already has an input channel."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1041
    :cond_c
    iput-object p1, p0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@e
    .line 1042
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@10
    iput-object p1, v0, Lcom/android/server/input/InputWindowHandle;->inputChannel:Landroid/view/InputChannel;

    #@12
    .line 1043
    return-void
.end method

.method public setShowToOwnerOnlyLocked(Z)V
    .registers 2
    .parameter "showToOwnerOnly"

    #@0
    .prologue
    .line 1214
    iput-boolean p1, p0, Lcom/android/server/wm/WindowState;->mShowToOwnerOnly:Z

    #@2
    .line 1215
    return-void
.end method

.method public setSplitScreenId(IZ)V
    .registers 3
    .parameter "id"
    .parameter "isFullScreen"

    #@0
    .prologue
    .line 1445
    iput p1, p0, Lcom/android/server/wm/WindowState;->mScreenId:I

    #@2
    .line 1446
    iput-boolean p2, p0, Lcom/android/server/wm/WindowState;->mIsSplitFullScreen:Z

    #@4
    .line 1447
    return-void
.end method

.method shouldAnimateMove()Z
    .registers 3

    #@0
    .prologue
    .line 982
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mContentChanged:Z

    #@2
    if-eqz v0, :cond_38

    #@4
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@6
    if-nez v0, :cond_38

    #@8
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@a
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@c
    if-nez v0, :cond_38

    #@e
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@10
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->okToDisplay()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_38

    #@16
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@18
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@1a
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mLastFrame:Landroid/graphics/Rect;

    #@1c
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@1e
    if-ne v0, v1, :cond_2a

    #@20
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@22
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@24
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mLastFrame:Landroid/graphics/Rect;

    #@26
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@28
    if-eq v0, v1, :cond_38

    #@2a
    :cond_2a
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@2c
    if-eqz v0, :cond_36

    #@2e
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@30
    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->shouldAnimateMove()Z

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_38

    #@36
    :cond_36
    const/4 v0, 0x1

    #@37
    :goto_37
    return v0

    #@38
    :cond_38
    const/4 v0, 0x0

    #@39
    goto :goto_37
.end method

.method public showLw(Z)Z
    .registers 3
    .parameter "doAnimation"

    #@0
    .prologue
    .line 1090
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/WindowState;->showLw(ZZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method showLw(ZZ)Z
    .registers 9
    .parameter "doAnimation"
    .parameter "requestAnim"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1094
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isHiddenFromUserLocked()Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_49

    #@8
    .line 1095
    const-string v3, "WindowState"

    #@a
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, "current user violation "

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@17
    iget v5, v5, Lcom/android/server/wm/WindowManagerService;->mCurrentUserId:I

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    const-string v5, " trying to display "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, ", type "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    iget-object v5, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@2f
    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->type:I

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, ", belonging to "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    iget v5, p0, Lcom/android/server/wm/WindowState;->mOwnerUid:I

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1138
    :cond_48
    :goto_48
    return v2

    #@49
    .line 1099
    :cond_49
    iget-boolean v4, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@4b
    if-eqz v4, :cond_51

    #@4d
    iget-boolean v4, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@4f
    if-nez v4, :cond_48

    #@51
    .line 1107
    :cond_51
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@53
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@55
    const/16 v4, 0x7d4

    #@57
    if-ne v2, v4, :cond_79

    #@59
    .line 1108
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@5b
    invoke-virtual {v2}, Lcom/android/server/wm/WindowList;->size()I

    #@5e
    move-result v1

    #@5f
    .line 1109
    .local v1, sizeOfChild:I
    :cond_5f
    :goto_5f
    if-lez v1, :cond_79

    #@61
    .line 1110
    add-int/lit8 v1, v1, -0x1

    #@63
    .line 1111
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@65
    invoke-virtual {v2, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@68
    move-result-object v0

    #@69
    check-cast v0, Lcom/android/server/wm/WindowState;

    #@6b
    .line 1112
    .local v0, c:Lcom/android/server/wm/WindowState;
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@6d
    iget-object v4, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@6f
    invoke-interface {v2, v0, v4}, Landroid/view/WindowManagerPolicy;->canBeForceHidden(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    #@72
    move-result v2

    #@73
    if-eqz v2, :cond_5f

    #@75
    .line 1113
    invoke-virtual {v0, p1}, Lcom/android/server/wm/WindowState;->showLw(Z)Z

    #@78
    goto :goto_5f

    #@79
    .line 1118
    .end local v0           #c:Lcom/android/server/wm/WindowState;
    .end local v1           #sizeOfChild:I
    :cond_79
    if-eqz p1, :cond_84

    #@7b
    .line 1121
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7d
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->okToDisplay()Z

    #@80
    move-result v2

    #@81
    if-nez v2, :cond_9a

    #@83
    .line 1122
    const/4 p1, 0x0

    #@84
    .line 1130
    :cond_84
    :goto_84
    iput-boolean v3, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@86
    .line 1131
    iput-boolean v3, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@88
    .line 1132
    if-eqz p1, :cond_91

    #@8a
    .line 1133
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@8c
    const/16 v4, 0x1001

    #@8e
    invoke-virtual {v2, v4, v3}, Lcom/android/server/wm/WindowStateAnimator;->applyAnimationLocked(IZ)Z

    #@91
    .line 1135
    :cond_91
    if-eqz p2, :cond_98

    #@93
    .line 1136
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@95
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->updateLayoutToAnimationLocked()V

    #@98
    :cond_98
    move v2, v3

    #@99
    .line 1138
    goto :goto_48

    #@9a
    .line 1123
    :cond_9a
    iget-boolean v2, p0, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@9c
    if-eqz v2, :cond_84

    #@9e
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@a0
    iget-object v2, v2, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@a2
    if-nez v2, :cond_84

    #@a4
    .line 1127
    const/4 p1, 0x0

    #@a5
    goto :goto_84
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1421
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mStringNameCache:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_14

    #@4
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mLastTitle:Ljava/lang/CharSequence;

    #@6
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@8
    invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@b
    move-result-object v1

    #@c
    if-ne v0, v1, :cond_14

    #@e
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mWasExiting:Z

    #@10
    iget-boolean v1, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@12
    if-eq v0, v1, :cond_65

    #@14
    .line 1423
    :cond_14
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@16
    invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Lcom/android/server/wm/WindowState;->mLastTitle:Ljava/lang/CharSequence;

    #@1c
    .line 1424
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@1e
    iput-boolean v0, p0, Lcom/android/server/wm/WindowState;->mWasExiting:Z

    #@20
    .line 1425
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, "Window{"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@2e
    move-result v1

    #@2f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    const-string v1, " u"

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@3f
    iget v1, v1, Lcom/android/server/wm/Session;->mUid:I

    #@41
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    #@44
    move-result v1

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    const-string v1, " "

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    iget-object v1, p0, Lcom/android/server/wm/WindowState;->mLastTitle:Ljava/lang/CharSequence;

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@57
    if-eqz v0, :cond_68

    #@59
    const-string v0, " EXITING}"

    #@5b
    :goto_5b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    iput-object v0, p0, Lcom/android/server/wm/WindowState;->mStringNameCache:Ljava/lang/String;

    #@65
    .line 1429
    :cond_65
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mStringNameCache:Ljava/lang/String;

    #@67
    return-object v0

    #@68
    .line 1425
    :cond_68
    const-string v0, "}"

    #@6a
    goto :goto_5b
.end method
