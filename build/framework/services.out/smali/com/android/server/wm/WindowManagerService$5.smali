.class Lcom/android/server/wm/WindowManagerService$5;
.super Landroid/os/Handler;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3583
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$5;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 3585
    iget v10, p1, Landroid/os/Message;->what:I

    #@4
    if-nez v10, :cond_28

    #@6
    .line 3586
    iget-object v10, p0, Lcom/android/server/wm/WindowManagerService$5;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@8
    iget-object v10, v10, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@a
    const-string v11, "display"

    #@c
    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/hardware/display/DisplayManager;

    #@12
    .line 3587
    .local v1, displayManager:Landroid/hardware/display/DisplayManager;
    const-string v10, "android.hardware.display.category.PRESENTATION"

    #@14
    invoke-virtual {v1, v10}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    #@17
    move-result-object v6

    #@18
    .line 3588
    .local v6, presentationDisplays:[Landroid/view/Display;
    array-length v10, v6

    #@19
    sput v10, Lcom/android/server/wm/WindowManagerService;->m_MirroringDisplayNum:I

    #@1b
    .line 3590
    sget v10, Lcom/android/server/wm/WindowManagerService;->m_MirroringDisplayNum:I

    #@1d
    if-nez v10, :cond_29

    #@1f
    .line 3592
    sput v12, Lcom/android/server/wm/WindowManagerService;->mMirroringMode:I

    #@21
    .line 3593
    const-string v10, "sys.dsdp.mode"

    #@23
    const-string v11, "stop"

    #@25
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 3636
    .end local v1           #displayManager:Landroid/hardware/display/DisplayManager;
    .end local v6           #presentationDisplays:[Landroid/view/Display;
    :cond_28
    :goto_28
    return-void

    #@29
    .line 3595
    .restart local v1       #displayManager:Landroid/hardware/display/DisplayManager;
    .restart local v6       #presentationDisplays:[Landroid/view/Display;
    :cond_29
    const/4 v3, 0x1

    #@2a
    .line 3596
    .local v3, isMirroring:Z
    const/4 v5, 0x0

    #@2b
    .line 3598
    .local v5, mirroringMode:I
    const/4 v2, 0x0

    #@2c
    .local v2, i:I
    :goto_2c
    array-length v10, v6

    #@2d
    if-ge v2, v10, :cond_6e

    #@2f
    .line 3599
    iget-object v10, p0, Lcom/android/server/wm/WindowManagerService$5;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@31
    aget-object v11, v6, v2

    #@33
    invoke-virtual {v11}, Landroid/view/Display;->getDisplayId()I

    #@36
    move-result v11

    #@37
    invoke-virtual {v10, v11}, Lcom/android/server/wm/WindowManagerService;->getDisplayContentLocked(I)Lcom/android/server/wm/DisplayContent;

    #@3a
    move-result-object v0

    #@3b
    .line 3600
    .local v0, displayContent:Lcom/android/server/wm/DisplayContent;
    if-nez v0, :cond_48

    #@3d
    .line 3602
    const-string v10, "sys.dsdp.mode"

    #@3f
    const-string v11, "stop"

    #@41
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@44
    .line 3603
    const/4 v3, 0x1

    #@45
    .line 3604
    sput v12, Lcom/android/server/wm/WindowManagerService;->mMirroringMode:I

    #@47
    goto :goto_28

    #@48
    .line 3607
    :cond_48
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getWindowList()Lcom/android/server/wm/WindowList;

    #@4b
    move-result-object v9

    #@4c
    .line 3608
    .local v9, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual {v9}, Lcom/android/server/wm/WindowList;->size()I

    #@4f
    move-result v8

    #@50
    .line 3609
    .local v8, windowCount:I
    const/4 v4, 0x0

    #@51
    .local v4, j:I
    :goto_51
    if-ge v4, v8, :cond_68

    #@53
    .line 3610
    invoke-virtual {v9, v4}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@56
    move-result-object v7

    #@57
    check-cast v7, Lcom/android/server/wm/WindowState;

    #@59
    .line 3611
    .local v7, window:Lcom/android/server/wm/WindowState;
    invoke-virtual {v7}, Lcom/android/server/wm/WindowState;->isVisibleLw()Z

    #@5c
    move-result v10

    #@5d
    if-nez v10, :cond_67

    #@5f
    iget-object v10, v7, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@61
    iget v10, v10, Landroid/view/WindowManager$LayoutParams;->type:I

    #@63
    const/16 v11, 0x7e9

    #@65
    if-ne v10, v11, :cond_6b

    #@67
    .line 3612
    :cond_67
    const/4 v3, 0x0

    #@68
    .line 3598
    .end local v7           #window:Lcom/android/server/wm/WindowState;
    :cond_68
    add-int/lit8 v2, v2, 0x1

    #@6a
    goto :goto_2c

    #@6b
    .line 3609
    .restart local v7       #window:Lcom/android/server/wm/WindowState;
    :cond_6b
    add-int/lit8 v4, v4, 0x1

    #@6d
    goto :goto_51

    #@6e
    .line 3620
    .end local v0           #displayContent:Lcom/android/server/wm/DisplayContent;
    .end local v4           #j:I
    .end local v7           #window:Lcom/android/server/wm/WindowState;
    .end local v8           #windowCount:I
    .end local v9           #windows:Lcom/android/server/wm/WindowList;
    :cond_6e
    if-ne v3, v13, :cond_83

    #@70
    .line 3621
    const/4 v5, 0x1

    #@71
    .line 3626
    :goto_71
    sget v10, Lcom/android/server/wm/WindowManagerService;->mMirroringMode:I

    #@73
    if-eq v10, v5, :cond_28

    #@75
    .line 3628
    sput v5, Lcom/android/server/wm/WindowManagerService;->mMirroringMode:I

    #@77
    .line 3629
    sget v10, Lcom/android/server/wm/WindowManagerService;->mMirroringMode:I

    #@79
    if-ne v10, v13, :cond_85

    #@7b
    .line 3630
    const-string v10, "sys.dsdp.mode"

    #@7d
    const-string v11, "stop"

    #@7f
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    goto :goto_28

    #@83
    .line 3623
    :cond_83
    const/4 v5, 0x2

    #@84
    goto :goto_71

    #@85
    .line 3632
    :cond_85
    const-string v10, "sys.dsdp.mode"

    #@87
    const-string v11, "start"

    #@89
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@8c
    goto :goto_28
.end method
