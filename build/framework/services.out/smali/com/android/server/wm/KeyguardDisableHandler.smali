.class public Lcom/android/server/wm/KeyguardDisableHandler;
.super Landroid/os/Handler;
.source "KeyguardDisableHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;
    }
.end annotation


# static fields
.field private static final ALLOW_DISABLE_NO:I = 0x0

.field private static final ALLOW_DISABLE_UNKNOWN:I = -0x1

.field private static final ALLOW_DISABLE_YES:I = 0x1

.field static final KEYGUARD_DISABLE:I = 0x1

.field static final KEYGUARD_POLICY_CHANGED:I = 0x3

.field static final KEYGUARD_REENABLE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "KeyguardDisableHandler"


# instance fields
.field private mAllowDisableKeyguard:I

.field final mContext:Landroid/content/Context;

.field mKeyguardTokenWatcher:Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;

.field final mPolicy:Landroid/view/WindowManagerPolicy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManagerPolicy;)V
    .registers 4
    .parameter "context"
    .parameter "policy"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 38
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mAllowDisableKeyguard:I

    #@6
    .line 50
    iput-object p1, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mContext:Landroid/content/Context;

    #@8
    .line 51
    iput-object p2, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@a
    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/wm/KeyguardDisableHandler;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget v0, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mAllowDisableKeyguard:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/server/wm/KeyguardDisableHandler;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput p1, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mAllowDisableKeyguard:I

    #@2
    return p1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 57
    iget-object v1, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mKeyguardTokenWatcher:Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;

    #@2
    if-nez v1, :cond_b

    #@4
    .line 58
    new-instance v1, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;

    #@6
    invoke-direct {v1, p0, p0}, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;-><init>(Lcom/android/server/wm/KeyguardDisableHandler;Landroid/os/Handler;)V

    #@9
    iput-object v1, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mKeyguardTokenWatcher:Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;

    #@b
    .line 61
    :cond_b
    iget v1, p1, Landroid/os/Message;->what:I

    #@d
    packed-switch v1, :pswitch_data_38

    #@10
    .line 77
    :goto_10
    return-void

    #@11
    .line 63
    :pswitch_11
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    check-cast v0, Landroid/util/Pair;

    #@15
    .line 64
    .local v0, pair:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/IBinder;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mKeyguardTokenWatcher:Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;

    #@17
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@19
    check-cast v1, Landroid/os/IBinder;

    #@1b
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@1d
    check-cast v2, Ljava/lang/String;

    #@1f
    invoke-virtual {v3, v1, v2}, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->acquire(Landroid/os/IBinder;Ljava/lang/String;)V

    #@22
    goto :goto_10

    #@23
    .line 68
    .end local v0           #pair:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/IBinder;Ljava/lang/String;>;"
    :pswitch_23
    iget-object v2, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mKeyguardTokenWatcher:Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;

    #@25
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@27
    check-cast v1, Landroid/os/IBinder;

    #@29
    invoke-virtual {v2, v1}, Lcom/android/server/wm/KeyguardDisableHandler$KeyguardTokenWatcher;->release(Landroid/os/IBinder;)V

    #@2c
    goto :goto_10

    #@2d
    .line 72
    :pswitch_2d
    iget-object v1, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@2f
    const/4 v2, 0x1

    #@30
    invoke-interface {v1, v2}, Landroid/view/WindowManagerPolicy;->enableKeyguard(Z)V

    #@33
    .line 74
    const/4 v1, -0x1

    #@34
    iput v1, p0, Lcom/android/server/wm/KeyguardDisableHandler;->mAllowDisableKeyguard:I

    #@36
    goto :goto_10

    #@37
    .line 61
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_11
        :pswitch_23
        :pswitch_2d
    .end packed-switch
.end method
