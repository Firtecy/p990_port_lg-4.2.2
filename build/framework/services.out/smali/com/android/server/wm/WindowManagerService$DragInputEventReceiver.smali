.class final Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;
.super Landroid/view/InputEventReceiver;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "DragInputEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;Landroid/view/InputChannel;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "inputChannel"
    .parameter "looper"

    #@0
    .prologue
    .line 782
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    .line 783
    invoke-direct {p0, p2, p3}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@5
    .line 784
    return-void
.end method


# virtual methods
.method public onInputEvent(Landroid/view/InputEvent;)V
    .registers 11
    .parameter "event"

    #@0
    .prologue
    .line 788
    const/4 v3, 0x0

    #@1
    .line 790
    .local v3, handled:Z
    :try_start_1
    instance-of v7, p1, Landroid/view/MotionEvent;

    #@3
    if-eqz v7, :cond_37

    #@5
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I

    #@8
    move-result v7

    #@9
    and-int/lit8 v7, v7, 0x2

    #@b
    if-eqz v7, :cond_37

    #@d
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@f
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@11
    if-eqz v7, :cond_37

    #@13
    .line 793
    move-object v0, p1

    #@14
    check-cast v0, Landroid/view/MotionEvent;

    #@16
    move-object v4, v0

    #@17
    .line 794
    .local v4, motionEvent:Landroid/view/MotionEvent;
    const/4 v2, 0x0

    #@18
    .line 795
    .local v2, endDrag:Z
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getRawX()F

    #@1b
    move-result v5

    #@1c
    .line 796
    .local v5, newX:F
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getRawY()F

    #@1f
    move-result v6

    #@20
    .line 798
    .local v6, newY:F
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getAction()I

    #@23
    move-result v7

    #@24
    packed-switch v7, :pswitch_data_9e

    #@27
    .line 826
    :goto_27
    :pswitch_27
    if-eqz v2, :cond_36

    #@29
    .line 829
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2b
    iget-object v8, v7, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@2d
    monitor-enter v8
    :try_end_2e
    .catchall {:try_start_1 .. :try_end_2e} :catchall_86
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_2e} :catch_5a

    #@2e
    .line 830
    :try_start_2e
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@30
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@32
    invoke-virtual {v7}, Lcom/android/server/wm/DragState;->endDragLw()V

    #@35
    .line 831
    monitor-exit v8
    :try_end_36
    .catchall {:try_start_2e .. :try_end_36} :catchall_9b

    #@36
    .line 834
    :cond_36
    const/4 v3, 0x1

    #@37
    .line 839
    .end local v2           #endDrag:Z
    .end local v4           #motionEvent:Landroid/view/MotionEvent;
    .end local v5           #newX:F
    .end local v6           #newY:F
    :cond_37
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@39
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@3b
    if-eqz v7, :cond_48

    #@3d
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@3f
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@41
    iget-object v7, v7, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@43
    if-eqz v7, :cond_48

    #@45
    .line 840
    invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@48
    .line 842
    :cond_48
    :goto_48
    return-void

    #@49
    .line 806
    .restart local v2       #endDrag:Z
    .restart local v4       #motionEvent:Landroid/view/MotionEvent;
    .restart local v5       #newX:F
    .restart local v6       #newY:F
    :pswitch_49
    :try_start_49
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4b
    iget-object v8, v7, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4d
    monitor-enter v8
    :try_end_4e
    .catchall {:try_start_49 .. :try_end_4e} :catchall_86
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_4e} :catch_5a

    #@4e
    .line 808
    :try_start_4e
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@50
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@52
    invoke-virtual {v7, v5, v6}, Lcom/android/server/wm/DragState;->notifyMoveLw(FF)V

    #@55
    .line 809
    monitor-exit v8

    #@56
    goto :goto_27

    #@57
    :catchall_57
    move-exception v7

    #@58
    monitor-exit v8
    :try_end_59
    .catchall {:try_start_4e .. :try_end_59} :catchall_57

    #@59
    :try_start_59
    throw v7
    :try_end_5a
    .catchall {:try_start_59 .. :try_end_5a} :catchall_86
    .catch Ljava/lang/Exception; {:try_start_59 .. :try_end_5a} :catch_5a

    #@5a
    .line 836
    .end local v2           #endDrag:Z
    .end local v4           #motionEvent:Landroid/view/MotionEvent;
    .end local v5           #newX:F
    .end local v6           #newY:F
    :catch_5a
    move-exception v1

    #@5b
    .line 837
    .local v1, e:Ljava/lang/Exception;
    :try_start_5b
    const-string v7, "WindowManager"

    #@5d
    const-string v8, "Exception caught by drag handleMotion"

    #@5f
    invoke-static {v7, v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_62
    .catchall {:try_start_5b .. :try_end_62} :catchall_86

    #@62
    .line 839
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@64
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@66
    if-eqz v7, :cond_48

    #@68
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6a
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@6c
    iget-object v7, v7, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@6e
    if-eqz v7, :cond_48

    #@70
    .line 840
    invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@73
    goto :goto_48

    #@74
    .line 815
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v2       #endDrag:Z
    .restart local v4       #motionEvent:Landroid/view/MotionEvent;
    .restart local v5       #newX:F
    .restart local v6       #newY:F
    :pswitch_74
    :try_start_74
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@76
    iget-object v8, v7, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@78
    monitor-enter v8
    :try_end_79
    .catchall {:try_start_74 .. :try_end_79} :catchall_86
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_79} :catch_5a

    #@79
    .line 816
    :try_start_79
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@7b
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@7d
    invoke-virtual {v7, v5, v6}, Lcom/android/server/wm/DragState;->notifyDropLw(FF)Z

    #@80
    move-result v2

    #@81
    .line 817
    monitor-exit v8

    #@82
    goto :goto_27

    #@83
    :catchall_83
    move-exception v7

    #@84
    monitor-exit v8
    :try_end_85
    .catchall {:try_start_79 .. :try_end_85} :catchall_83

    #@85
    :try_start_85
    throw v7
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_86
    .catch Ljava/lang/Exception; {:try_start_85 .. :try_end_86} :catch_5a

    #@86
    .line 839
    .end local v2           #endDrag:Z
    .end local v4           #motionEvent:Landroid/view/MotionEvent;
    .end local v5           #newX:F
    .end local v6           #newY:F
    :catchall_86
    move-exception v7

    #@87
    iget-object v8, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@89
    iget-object v8, v8, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@8b
    if-eqz v8, :cond_98

    #@8d
    iget-object v8, p0, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@8f
    iget-object v8, v8, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@91
    iget-object v8, v8, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@93
    if-eqz v8, :cond_98

    #@95
    .line 840
    invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@98
    :cond_98
    throw v7

    #@99
    .line 822
    .restart local v2       #endDrag:Z
    .restart local v4       #motionEvent:Landroid/view/MotionEvent;
    .restart local v5       #newX:F
    .restart local v6       #newY:F
    :pswitch_99
    const/4 v2, 0x1

    #@9a
    goto :goto_27

    #@9b
    .line 831
    :catchall_9b
    move-exception v7

    #@9c
    :try_start_9c
    monitor-exit v8
    :try_end_9d
    .catchall {:try_start_9c .. :try_end_9d} :catchall_9b

    #@9d
    :try_start_9d
    throw v7
    :try_end_9e
    .catchall {:try_start_9d .. :try_end_9e} :catchall_86
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_9e} :catch_5a

    #@9e
    .line 798
    :pswitch_data_9e
    .packed-switch 0x0
        :pswitch_27
        :pswitch_74
        :pswitch_49
        :pswitch_99
    .end packed-switch
.end method
