.class Lcom/android/server/wm/ScreenRotationAnimation;
.super Ljava/lang/Object;
.source "ScreenRotationAnimation.java"


# static fields
.field static final DEBUG_STATE:Z = false

.field static final DEBUG_TRANSFORMS:Z = false

.field static final FREEZE_LAYER:I = 0x1e8480

.field static final TAG:Ljava/lang/String; = "ScreenRotationAnimation"

.field static final TWO_PHASE_ANIMATION:Z

.field static final USE_CUSTOM_BLACK_FRAME:Z


# instance fields
.field mAnimRunning:Z

.field final mContext:Landroid/content/Context;

.field mCurRotation:I

.field mCustomBlackFrame:Lcom/android/server/wm/BlackFrame;

.field final mDisplay:Landroid/view/Display;

.field mEnterAnimId:I

.field final mEnterTransformation:Landroid/view/animation/Transformation;

.field mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

.field mExitAnimId:I

.field final mExitFrameFinalMatrix:Landroid/graphics/Matrix;

.field final mExitTransformation:Landroid/view/animation/Transformation;

.field mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

.field mFinishAnimReady:Z

.field mFinishAnimStartTime:J

.field mFinishEnterAnimation:Landroid/view/animation/Animation;

.field final mFinishEnterTransformation:Landroid/view/animation/Transformation;

.field mFinishExitAnimation:Landroid/view/animation/Animation;

.field final mFinishExitTransformation:Landroid/view/animation/Transformation;

.field mFinishFrameAnimation:Landroid/view/animation/Animation;

.field final mFinishFrameTransformation:Landroid/view/animation/Transformation;

.field final mFrameInitialMatrix:Landroid/graphics/Matrix;

.field final mFrameTransformation:Landroid/view/animation/Transformation;

.field mHalfwayPoint:J

.field mHeight:I

.field mLastRotateEnterAnimation:Landroid/view/animation/Animation;

.field final mLastRotateEnterTransformation:Landroid/view/animation/Transformation;

.field mLastRotateExitAnimation:Landroid/view/animation/Animation;

.field final mLastRotateExitTransformation:Landroid/view/animation/Transformation;

.field mLastRotateFrameAnimation:Landroid/view/animation/Animation;

.field final mLastRotateFrameTransformation:Landroid/view/animation/Transformation;

.field private mMoreFinishEnter:Z

.field private mMoreFinishExit:Z

.field private mMoreFinishFrame:Z

.field private mMoreRotateEnter:Z

.field private mMoreRotateExit:Z

.field private mMoreRotateFrame:Z

.field private mMoreStartEnter:Z

.field private mMoreStartExit:Z

.field private mMoreStartFrame:Z

.field mOriginalHeight:I

.field mOriginalRotation:I

.field mOriginalWidth:I

.field mRotateEnterAnimation:Landroid/view/animation/Animation;

.field final mRotateEnterTransformation:Landroid/view/animation/Transformation;

.field mRotateExitAnimation:Landroid/view/animation/Animation;

.field final mRotateExitTransformation:Landroid/view/animation/Transformation;

.field mRotateFrameAnimation:Landroid/view/animation/Animation;

.field final mRotateFrameTransformation:Landroid/view/animation/Transformation;

.field final mSnapshotFinalMatrix:Landroid/graphics/Matrix;

.field final mSnapshotInitialMatrix:Landroid/graphics/Matrix;

.field mStartEnterAnimation:Landroid/view/animation/Animation;

.field final mStartEnterTransformation:Landroid/view/animation/Transformation;

.field mStartExitAnimation:Landroid/view/animation/Animation;

.field final mStartExitTransformation:Landroid/view/animation/Transformation;

.field mStartFrameAnimation:Landroid/view/animation/Animation;

.field final mStartFrameTransformation:Landroid/view/animation/Transformation;

.field mStarted:Z

.field mSurface:Landroid/view/Surface;

.field final mTmpFloats:[F

.field final mTmpMatrix:Landroid/graphics/Matrix;

.field mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;Landroid/view/SurfaceSession;ZIIIII)V
    .registers 19
    .parameter "context"
    .parameter "display"
    .parameter "session"
    .parameter "inTransaction"
    .parameter "originalWidth"
    .parameter "originalHeight"
    .parameter "originalRotation"
    .parameter "exitAnim"
    .parameter "enterAnim"

    #@0
    .prologue
    .line 193
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    new-instance v1, Landroid/view/animation/Transformation;

    #@5
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@8
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartExitTransformation:Landroid/view/animation/Transformation;

    #@a
    .line 68
    new-instance v1, Landroid/view/animation/Transformation;

    #@c
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@f
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartEnterTransformation:Landroid/view/animation/Transformation;

    #@11
    .line 70
    new-instance v1, Landroid/view/animation/Transformation;

    #@13
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@16
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartFrameTransformation:Landroid/view/animation/Transformation;

    #@18
    .line 77
    new-instance v1, Landroid/view/animation/Transformation;

    #@1a
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@1d
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishExitTransformation:Landroid/view/animation/Transformation;

    #@1f
    .line 79
    new-instance v1, Landroid/view/animation/Transformation;

    #@21
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@24
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishEnterTransformation:Landroid/view/animation/Transformation;

    #@26
    .line 81
    new-instance v1, Landroid/view/animation/Transformation;

    #@28
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@2b
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishFrameTransformation:Landroid/view/animation/Transformation;

    #@2d
    .line 87
    new-instance v1, Landroid/view/animation/Transformation;

    #@2f
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@32
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitTransformation:Landroid/view/animation/Transformation;

    #@34
    .line 89
    new-instance v1, Landroid/view/animation/Transformation;

    #@36
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@39
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterTransformation:Landroid/view/animation/Transformation;

    #@3b
    .line 91
    new-instance v1, Landroid/view/animation/Transformation;

    #@3d
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@40
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateFrameTransformation:Landroid/view/animation/Transformation;

    #@42
    .line 96
    new-instance v1, Landroid/view/animation/Transformation;

    #@44
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@47
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mLastRotateExitTransformation:Landroid/view/animation/Transformation;

    #@49
    .line 98
    new-instance v1, Landroid/view/animation/Transformation;

    #@4b
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@4e
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mLastRotateEnterTransformation:Landroid/view/animation/Transformation;

    #@50
    .line 100
    new-instance v1, Landroid/view/animation/Transformation;

    #@52
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@55
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mLastRotateFrameTransformation:Landroid/view/animation/Transformation;

    #@57
    .line 103
    new-instance v1, Landroid/view/animation/Transformation;

    #@59
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@5c
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitTransformation:Landroid/view/animation/Transformation;

    #@5e
    .line 104
    new-instance v1, Landroid/view/animation/Transformation;

    #@60
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@63
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterTransformation:Landroid/view/animation/Transformation;

    #@65
    .line 105
    new-instance v1, Landroid/view/animation/Transformation;

    #@67
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@6a
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFrameTransformation:Landroid/view/animation/Transformation;

    #@6c
    .line 112
    new-instance v1, Landroid/graphics/Matrix;

    #@6e
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@71
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFrameInitialMatrix:Landroid/graphics/Matrix;

    #@73
    .line 113
    new-instance v1, Landroid/graphics/Matrix;

    #@75
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@78
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotInitialMatrix:Landroid/graphics/Matrix;

    #@7a
    .line 114
    new-instance v1, Landroid/graphics/Matrix;

    #@7c
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@7f
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotFinalMatrix:Landroid/graphics/Matrix;

    #@81
    .line 115
    new-instance v1, Landroid/graphics/Matrix;

    #@83
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@86
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitFrameFinalMatrix:Landroid/graphics/Matrix;

    #@88
    .line 116
    new-instance v1, Landroid/graphics/Matrix;

    #@8a
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@8d
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpMatrix:Landroid/graphics/Matrix;

    #@8f
    .line 117
    const/16 v1, 0x9

    #@91
    new-array v1, v1, [F

    #@93
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@95
    .line 194
    iput-object p1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@97
    .line 195
    iput-object p2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mDisplay:Landroid/view/Display;

    #@99
    .line 196
    move/from16 v0, p8

    #@9b
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitAnimId:I

    #@9d
    .line 197
    move/from16 v0, p9

    #@9f
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterAnimId:I

    #@a1
    .line 200
    const/4 v1, 0x1

    #@a2
    move/from16 v0, p7

    #@a4
    if-eq v0, v1, :cond_ab

    #@a6
    const/4 v1, 0x3

    #@a7
    move/from16 v0, p7

    #@a9
    if-ne v0, v1, :cond_df

    #@ab
    .line 202
    :cond_ab
    iput p6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mWidth:I

    #@ad
    .line 203
    iput p5, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHeight:I

    #@af
    .line 209
    :goto_af
    move/from16 v0, p7

    #@b1
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalRotation:I

    #@b3
    .line 210
    iput p5, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@b5
    .line 211
    iput p6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@b7
    .line 213
    if-nez p4, :cond_bc

    #@b9
    .line 216
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@bc
    .line 226
    :cond_bc
    :try_start_bc
    new-instance v1, Landroid/view/Surface;

    #@be
    const-string v3, "FreezeSurface"

    #@c0
    iget v4, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mWidth:I

    #@c2
    iget v5, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHeight:I

    #@c4
    const/4 v6, -0x1

    #@c5
    const v7, 0x30004

    #@c8
    move-object v2, p3

    #@c9
    invoke-direct/range {v1 .. v7}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@cc
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@ce
    .line 230
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@d0
    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    #@d3
    move-result v1

    #@d4
    if-nez v1, :cond_e4

    #@d6
    .line 232
    const/4 v1, 0x0

    #@d7
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;
    :try_end_d9
    .catchall {:try_start_bc .. :try_end_d9} :catchall_116
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_bc .. :try_end_d9} :catch_10d

    #@d9
    .line 249
    if-nez p4, :cond_de

    #@db
    .line 250
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@de
    .line 255
    :cond_de
    :goto_de
    return-void

    #@df
    .line 205
    :cond_df
    iput p5, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mWidth:I

    #@e1
    .line 206
    iput p6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHeight:I

    #@e3
    goto :goto_af

    #@e4
    .line 235
    :cond_e4
    :try_start_e4
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@e6
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mDisplay:Landroid/view/Display;

    #@e8
    invoke-virtual {v2}, Landroid/view/Display;->getLayerStack()I

    #@eb
    move-result v2

    #@ec
    invoke-virtual {v1, v2}, Landroid/view/Surface;->setLayerStack(I)V

    #@ef
    .line 236
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@f1
    const v2, 0x1e8481

    #@f4
    invoke-virtual {v1, v2}, Landroid/view/Surface;->setLayer(I)V

    #@f7
    .line 237
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@f9
    const/4 v2, 0x0

    #@fa
    invoke-virtual {v1, v2}, Landroid/view/Surface;->setAlpha(F)V

    #@fd
    .line 238
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@ff
    invoke-virtual {v1}, Landroid/view/Surface;->show()V
    :try_end_102
    .catchall {:try_start_e4 .. :try_end_102} :catchall_116
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_e4 .. :try_end_102} :catch_10d

    #@102
    .line 247
    :goto_102
    :try_start_102
    move/from16 v0, p7

    #@104
    invoke-direct {p0, v0}, Lcom/android/server/wm/ScreenRotationAnimation;->setRotationInTransaction(I)V
    :try_end_107
    .catchall {:try_start_102 .. :try_end_107} :catchall_116

    #@107
    .line 249
    if-nez p4, :cond_de

    #@109
    .line 250
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@10c
    goto :goto_de

    #@10d
    .line 239
    :catch_10d
    move-exception v8

    #@10e
    .line 240
    .local v8, e:Landroid/view/Surface$OutOfResourcesException;
    :try_start_10e
    const-string v1, "ScreenRotationAnimation"

    #@110
    const-string v2, "Unable to allocate freeze surface"

    #@112
    invoke-static {v1, v2, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_115
    .catchall {:try_start_10e .. :try_end_115} :catchall_116

    #@115
    goto :goto_102

    #@116
    .line 249
    .end local v8           #e:Landroid/view/Surface$OutOfResourcesException;
    :catchall_116
    move-exception v1

    #@117
    if-nez p4, :cond_11c

    #@119
    .line 250
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@11c
    :cond_11c
    throw v1
.end method

.method public static createRotationMatrix(IIILandroid/graphics/Matrix;)V
    .registers 6
    .parameter "rotation"
    .parameter "width"
    .parameter "height"
    .parameter "outMatrix"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 290
    packed-switch p0, :pswitch_data_28

    #@4
    .line 307
    :goto_4
    return-void

    #@5
    .line 292
    :pswitch_5
    invoke-virtual {p3}, Landroid/graphics/Matrix;->reset()V

    #@8
    goto :goto_4

    #@9
    .line 295
    :pswitch_9
    const/high16 v0, 0x42b4

    #@b
    invoke-virtual {p3, v0, v1, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    #@e
    .line 296
    int-to-float v0, p2

    #@f
    invoke-virtual {p3, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@12
    goto :goto_4

    #@13
    .line 299
    :pswitch_13
    const/high16 v0, 0x4334

    #@15
    invoke-virtual {p3, v0, v1, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    #@18
    .line 300
    int-to-float v0, p1

    #@19
    int-to-float v1, p2

    #@1a
    invoke-virtual {p3, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@1d
    goto :goto_4

    #@1e
    .line 303
    :pswitch_1e
    const/high16 v0, 0x4387

    #@20
    invoke-virtual {p3, v0, v1, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    #@23
    .line 304
    int-to-float v0, p1

    #@24
    invoke-virtual {p3, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@27
    goto :goto_4

    #@28
    .line 290
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_5
        :pswitch_9
        :pswitch_13
        :pswitch_1e
    .end packed-switch
.end method

.method static deltaRotation(II)I
    .registers 3
    .parameter "oldRotation"
    .parameter "newRotation"

    #@0
    .prologue
    .line 262
    sub-int v0, p1, p0

    #@2
    .line 263
    .local v0, delta:I
    if-gez v0, :cond_6

    #@4
    add-int/lit8 v0, v0, 0x4

    #@6
    .line 264
    :cond_6
    return v0
.end method

.method private hasAnimations()Z
    .registers 2

    #@0
    .prologue
    .line 676
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@2
    if-nez v0, :cond_8

    #@4
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private setRotationInTransaction(I)V
    .registers 6
    .parameter "rotation"

    #@0
    .prologue
    .line 311
    iput p1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCurRotation:I

    #@2
    .line 316
    const/4 v1, 0x0

    #@3
    invoke-static {p1, v1}, Lcom/android/server/wm/ScreenRotationAnimation;->deltaRotation(II)I

    #@6
    move-result v0

    #@7
    .line 317
    .local v0, delta:I
    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mWidth:I

    #@9
    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHeight:I

    #@b
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotInitialMatrix:Landroid/graphics/Matrix;

    #@d
    invoke-static {v0, v1, v2, v3}, Lcom/android/server/wm/ScreenRotationAnimation;->createRotationMatrix(IIILandroid/graphics/Matrix;)V

    #@10
    .line 320
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotInitialMatrix:Landroid/graphics/Matrix;

    #@12
    const/high16 v2, 0x3f80

    #@14
    invoke-direct {p0, v1, v2}, Lcom/android/server/wm/ScreenRotationAnimation;->setSnapshotTransformInTransaction(Landroid/graphics/Matrix;F)V

    #@17
    .line 321
    return-void
.end method

.method private setSnapshotTransformInTransaction(Landroid/graphics/Matrix;F)V
    .registers 9
    .parameter "matrix"
    .parameter "alpha"

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@2
    if-eqz v0, :cond_36

    #@4
    .line 269
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@6
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    #@9
    .line 270
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@b
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@d
    const/4 v2, 0x2

    #@e
    aget v1, v1, v2

    #@10
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@12
    const/4 v3, 0x5

    #@13
    aget v2, v2, v3

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/view/Surface;->setPosition(FF)V

    #@18
    .line 272
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@1a
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@1c
    const/4 v2, 0x0

    #@1d
    aget v1, v1, v2

    #@1f
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@21
    const/4 v3, 0x3

    #@22
    aget v2, v2, v3

    #@24
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@26
    const/4 v4, 0x1

    #@27
    aget v3, v3, v4

    #@29
    iget-object v4, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mTmpFloats:[F

    #@2b
    const/4 v5, 0x4

    #@2c
    aget v4, v4, v5

    #@2e
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@31
    .line 275
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@33
    invoke-virtual {v0, p2}, Landroid/view/Surface;->setAlpha(F)V

    #@36
    .line 286
    :cond_36
    return-void
.end method

.method private startAnimation(Landroid/view/SurfaceSession;JFIIZ)Z
    .registers 21
    .parameter "session"
    .parameter "maxAnimationDuration"
    .parameter "animationScale"
    .parameter "finalWidth"
    .parameter "finalHeight"
    .parameter "dismissing"

    #@0
    .prologue
    .line 341
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@2
    if-nez v2, :cond_6

    #@4
    .line 343
    const/4 v2, 0x0

    #@5
    .line 579
    :goto_5
    return v2

    #@6
    .line 345
    :cond_6
    iget-boolean v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStarted:Z

    #@8
    if-eqz v2, :cond_c

    #@a
    .line 346
    const/4 v2, 0x1

    #@b
    goto :goto_5

    #@c
    .line 349
    :cond_c
    const/4 v2, 0x1

    #@d
    iput-boolean v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStarted:Z

    #@f
    .line 351
    const/4 v11, 0x0

    #@10
    .line 354
    .local v11, firstStart:Z
    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCurRotation:I

    #@12
    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalRotation:I

    #@14
    invoke-static {v2, v3}, Lcom/android/server/wm/ScreenRotationAnimation;->deltaRotation(II)I

    #@17
    move-result v9

    #@18
    .line 383
    .local v9, delta:I
    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitAnimId:I

    #@1a
    if-eqz v2, :cond_fa

    #@1c
    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterAnimId:I

    #@1e
    if-eqz v2, :cond_fa

    #@20
    .line 384
    const/4 v8, 0x1

    #@21
    .line 385
    .local v8, customAnim:Z
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@23
    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitAnimId:I

    #@25
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@28
    move-result-object v2

    #@29
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@2b
    .line 386
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@2d
    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterAnimId:I

    #@2f
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@32
    move-result-object v2

    #@33
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@35
    .line 460
    :goto_35
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@37
    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@39
    iget v6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@3b
    move/from16 v0, p5

    #@3d
    move/from16 v1, p6

    #@3f
    invoke-virtual {v2, v0, v1, v3, v6}, Landroid/view/animation/Animation;->initialize(IIII)V

    #@42
    .line 461
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@44
    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@46
    iget v6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@48
    move/from16 v0, p5

    #@4a
    move/from16 v1, p6

    #@4c
    invoke-virtual {v2, v0, v1, v3, v6}, Landroid/view/animation/Animation;->initialize(IIII)V

    #@4f
    .line 466
    const/4 v2, 0x0

    #@50
    iput-boolean v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mAnimRunning:Z

    #@52
    .line 467
    const/4 v2, 0x0

    #@53
    iput-boolean v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimReady:Z

    #@55
    .line 468
    const-wide/16 v2, -0x1

    #@57
    iput-wide v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimStartTime:J

    #@59
    .line 486
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@5b
    move-wide v0, p2

    #@5c
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->restrictDuration(J)V

    #@5f
    .line 487
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@61
    move/from16 v0, p4

    #@63
    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->scaleCurrentDuration(F)V

    #@66
    .line 488
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@68
    move-wide v0, p2

    #@69
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->restrictDuration(J)V

    #@6c
    .line 489
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@6e
    move/from16 v0, p4

    #@70
    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->scaleCurrentDuration(F)V

    #@73
    .line 495
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mDisplay:Landroid/view/Display;

    #@75
    invoke-virtual {v2}, Landroid/view/Display;->getLayerStack()I

    #@78
    move-result v7

    #@79
    .line 527
    .local v7, layerStack:I
    if-nez v8, :cond_c2

    #@7b
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@7d
    if-nez v2, :cond_c2

    #@7f
    .line 531
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@82
    .line 539
    :try_start_82
    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@84
    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@86
    iget-object v6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFrameInitialMatrix:Landroid/graphics/Matrix;

    #@88
    invoke-static {v9, v2, v3, v6}, Lcom/android/server/wm/ScreenRotationAnimation;->createRotationMatrix(IIILandroid/graphics/Matrix;)V

    #@8b
    .line 541
    new-instance v4, Landroid/graphics/Rect;

    #@8d
    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@8f
    neg-int v2, v2

    #@90
    mul-int/lit8 v2, v2, 0x1

    #@92
    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@94
    neg-int v3, v3

    #@95
    mul-int/lit8 v3, v3, 0x1

    #@97
    iget v6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@99
    mul-int/lit8 v6, v6, 0x2

    #@9b
    iget v12, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@9d
    mul-int/lit8 v12, v12, 0x2

    #@9f
    invoke-direct {v4, v2, v3, v6, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@a2
    .line 543
    .local v4, outer:Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/Rect;

    #@a4
    const/4 v2, 0x0

    #@a5
    const/4 v3, 0x0

    #@a6
    iget v6, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@a8
    iget v12, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@aa
    invoke-direct {v5, v2, v3, v6, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@ad
    .line 544
    .local v5, inner:Landroid/graphics/Rect;
    new-instance v2, Lcom/android/server/wm/BlackFrame;

    #@af
    const v6, 0x1e8482

    #@b2
    move-object v3, p1

    #@b3
    invoke-direct/range {v2 .. v7}, Lcom/android/server/wm/BlackFrame;-><init>(Landroid/view/SurfaceSession;Landroid/graphics/Rect;Landroid/graphics/Rect;II)V

    #@b6
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@b8
    .line 546
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@ba
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFrameInitialMatrix:Landroid/graphics/Matrix;

    #@bc
    invoke-virtual {v2, v3}, Lcom/android/server/wm/BlackFrame;->setMatrix(Landroid/graphics/Matrix;)V
    :try_end_bf
    .catchall {:try_start_82 .. :try_end_bf} :catchall_16d
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_82 .. :try_end_bf} :catch_160

    #@bf
    .line 550
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@c2
    .line 557
    .end local v4           #outer:Landroid/graphics/Rect;
    .end local v5           #inner:Landroid/graphics/Rect;
    :cond_c2
    :goto_c2
    if-eqz v8, :cond_f7

    #@c4
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@c6
    if-nez v2, :cond_f7

    #@c8
    .line 561
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@cb
    .line 564
    :try_start_cb
    new-instance v4, Landroid/graphics/Rect;

    #@cd
    move/from16 v0, p5

    #@cf
    neg-int v2, v0

    #@d0
    mul-int/lit8 v2, v2, 0x1

    #@d2
    move/from16 v0, p6

    #@d4
    neg-int v3, v0

    #@d5
    mul-int/lit8 v3, v3, 0x1

    #@d7
    mul-int/lit8 v6, p5, 0x2

    #@d9
    mul-int/lit8 v12, p6, 0x2

    #@db
    invoke-direct {v4, v2, v3, v6, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@de
    .line 566
    .restart local v4       #outer:Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/Rect;

    #@e0
    const/4 v2, 0x0

    #@e1
    const/4 v3, 0x0

    #@e2
    move/from16 v0, p5

    #@e4
    move/from16 v1, p6

    #@e6
    invoke-direct {v5, v2, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@e9
    .line 567
    .restart local v5       #inner:Landroid/graphics/Rect;
    new-instance v2, Lcom/android/server/wm/BlackFrame;

    #@eb
    const v6, 0x1e8480

    #@ee
    move-object v3, p1

    #@ef
    invoke-direct/range {v2 .. v7}, Lcom/android/server/wm/BlackFrame;-><init>(Landroid/view/SurfaceSession;Landroid/graphics/Rect;Landroid/graphics/Rect;II)V

    #@f2
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;
    :try_end_f4
    .catchall {:try_start_cb .. :try_end_f4} :catchall_17f
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_cb .. :try_end_f4} :catch_172

    #@f4
    .line 572
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@f7
    .line 579
    .end local v4           #outer:Landroid/graphics/Rect;
    .end local v5           #inner:Landroid/graphics/Rect;
    :cond_f7
    :goto_f7
    const/4 v2, 0x1

    #@f8
    goto/16 :goto_5

    #@fa
    .line 388
    .end local v7           #layerStack:I
    .end local v8           #customAnim:Z
    :cond_fa
    const/4 v8, 0x0

    #@fb
    .line 389
    .restart local v8       #customAnim:Z
    packed-switch v9, :pswitch_data_184

    #@fe
    goto/16 :goto_35

    #@100
    .line 391
    :pswitch_100
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@102
    const v3, 0x10a003e

    #@105
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@108
    move-result-object v2

    #@109
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@10b
    .line 393
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@10d
    const v3, 0x10a003d

    #@110
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@113
    move-result-object v2

    #@114
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@116
    goto/16 :goto_35

    #@118
    .line 401
    :pswitch_118
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@11a
    const v3, 0x10a004a

    #@11d
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@120
    move-result-object v2

    #@121
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@123
    .line 403
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@125
    const v3, 0x10a0049

    #@128
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@12b
    move-result-object v2

    #@12c
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@12e
    goto/16 :goto_35

    #@130
    .line 411
    :pswitch_130
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@132
    const v3, 0x10a0041

    #@135
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@138
    move-result-object v2

    #@139
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@13b
    .line 413
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@13d
    const v3, 0x10a0040

    #@140
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@143
    move-result-object v2

    #@144
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@146
    goto/16 :goto_35

    #@148
    .line 421
    :pswitch_148
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@14a
    const v3, 0x10a0047

    #@14d
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@150
    move-result-object v2

    #@151
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@153
    .line 423
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mContext:Landroid/content/Context;

    #@155
    const v3, 0x10a0046

    #@158
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@15b
    move-result-object v2

    #@15c
    iput-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@15e
    goto/16 :goto_35

    #@160
    .line 547
    .restart local v7       #layerStack:I
    :catch_160
    move-exception v10

    #@161
    .line 548
    .local v10, e:Landroid/view/Surface$OutOfResourcesException;
    :try_start_161
    const-string v2, "ScreenRotationAnimation"

    #@163
    const-string v3, "Unable to allocate black surface"

    #@165
    invoke-static {v2, v3, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_168
    .catchall {:try_start_161 .. :try_end_168} :catchall_16d

    #@168
    .line 550
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@16b
    goto/16 :goto_c2

    #@16d
    .end local v10           #e:Landroid/view/Surface$OutOfResourcesException;
    :catchall_16d
    move-exception v2

    #@16e
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@171
    throw v2

    #@172
    .line 569
    :catch_172
    move-exception v10

    #@173
    .line 570
    .restart local v10       #e:Landroid/view/Surface$OutOfResourcesException;
    :try_start_173
    const-string v2, "ScreenRotationAnimation"

    #@175
    const-string v3, "Unable to allocate black surface"

    #@177
    invoke-static {v2, v3, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_17a
    .catchall {:try_start_173 .. :try_end_17a} :catchall_17f

    #@17a
    .line 572
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@17d
    goto/16 :goto_f7

    #@17f
    .end local v10           #e:Landroid/view/Surface$OutOfResourcesException;
    :catchall_17f
    move-exception v2

    #@180
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@183
    throw v2

    #@184
    .line 389
    :pswitch_data_184
    .packed-switch 0x0
        :pswitch_100
        :pswitch_118
        :pswitch_130
        :pswitch_148
    .end packed-switch
.end method

.method private stepAnimation(J)Z
    .registers 9
    .parameter "now"

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    .line 686
    iget-wide v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHalfwayPoint:J

    #@6
    cmp-long v3, p1, v3

    #@8
    if-lez v3, :cond_11

    #@a
    .line 687
    const-wide v3, 0x7fffffffffffffffL

    #@f
    iput-wide v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHalfwayPoint:J

    #@11
    .line 689
    :cond_11
    iget-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimReady:Z

    #@13
    if-eqz v3, :cond_1d

    #@15
    iget-wide v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimStartTime:J

    #@17
    cmp-long v3, v3, v0

    #@19
    if-gez v3, :cond_1d

    #@1b
    .line 691
    iput-wide p1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimStartTime:J

    #@1d
    .line 715
    :cond_1d
    iget-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimReady:Z

    #@1f
    if-eqz v3, :cond_25

    #@21
    iget-wide v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimStartTime:J

    #@23
    sub-long v0, p1, v3

    #@25
    .line 739
    .local v0, finishNow:J
    :cond_25
    iput-boolean v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateExit:Z

    #@27
    .line 740
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@29
    if-eqz v3, :cond_35

    #@2b
    .line 741
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@2d
    iget-object v4, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitTransformation:Landroid/view/animation/Transformation;

    #@2f
    invoke-virtual {v3, p1, p2, v4}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    #@32
    move-result v3

    #@33
    iput-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateExit:Z

    #@35
    .line 745
    :cond_35
    iput-boolean v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateEnter:Z

    #@37
    .line 746
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@39
    if-eqz v3, :cond_45

    #@3b
    .line 747
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@3d
    iget-object v4, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterTransformation:Landroid/view/animation/Transformation;

    #@3f
    invoke-virtual {v3, p1, p2, v4}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    #@42
    move-result v3

    #@43
    iput-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateEnter:Z

    #@45
    .line 759
    :cond_45
    iget-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateExit:Z

    #@47
    if-nez v3, :cond_59

    #@49
    .line 774
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@4b
    if-eqz v3, :cond_59

    #@4d
    .line 776
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@4f
    invoke-virtual {v3}, Landroid/view/animation/Animation;->cancel()V

    #@52
    .line 777
    iput-object v5, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@54
    .line 778
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitTransformation:Landroid/view/animation/Transformation;

    #@56
    invoke-virtual {v3}, Landroid/view/animation/Transformation;->clear()V

    #@59
    .line 782
    :cond_59
    iget-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateEnter:Z

    #@5b
    if-nez v3, :cond_6d

    #@5d
    .line 797
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@5f
    if-eqz v3, :cond_6d

    #@61
    .line 799
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@63
    invoke-virtual {v3}, Landroid/view/animation/Animation;->cancel()V

    #@66
    .line 800
    iput-object v5, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@68
    .line 801
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterTransformation:Landroid/view/animation/Transformation;

    #@6a
    invoke-virtual {v3}, Landroid/view/animation/Transformation;->clear()V

    #@6d
    .line 826
    :cond_6d
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitTransformation:Landroid/view/animation/Transformation;

    #@6f
    iget-object v4, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitTransformation:Landroid/view/animation/Transformation;

    #@71
    invoke-virtual {v3, v4}, Landroid/view/animation/Transformation;->set(Landroid/view/animation/Transformation;)V

    #@74
    .line 827
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterTransformation:Landroid/view/animation/Transformation;

    #@76
    iget-object v4, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterTransformation:Landroid/view/animation/Transformation;

    #@78
    invoke-virtual {v3, v4}, Landroid/view/animation/Transformation;->set(Landroid/view/animation/Transformation;)V

    #@7b
    .line 850
    iget-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateEnter:Z

    #@7d
    if-nez v3, :cond_87

    #@7f
    iget-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateExit:Z

    #@81
    if-nez v3, :cond_87

    #@83
    iget-boolean v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimReady:Z

    #@85
    if-nez v3, :cond_88

    #@87
    :cond_87
    const/4 v2, 0x1

    #@88
    .line 857
    .local v2, more:Z
    :cond_88
    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotFinalMatrix:Landroid/graphics/Matrix;

    #@8a
    iget-object v4, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitTransformation:Landroid/view/animation/Transformation;

    #@8c
    invoke-virtual {v4}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@8f
    move-result-object v4

    #@90
    iget-object v5, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotInitialMatrix:Landroid/graphics/Matrix;

    #@92
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    #@95
    .line 861
    return v2
.end method


# virtual methods
.method public dismiss(Landroid/view/SurfaceSession;JFII)Z
    .registers 16
    .parameter "session"
    .parameter "maxAnimationDuration"
    .parameter "animationScale"
    .parameter "finalWidth"
    .parameter "finalHeight"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 588
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@4
    if-nez v0, :cond_8

    #@6
    move v7, v8

    #@7
    .line 601
    :goto_7
    return v7

    #@8
    .line 592
    :cond_8
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStarted:Z

    #@a
    if-nez v0, :cond_15

    #@c
    move-object v0, p0

    #@d
    move-object v1, p1

    #@e
    move-wide v2, p2

    #@f
    move v4, p4

    #@10
    move v5, p5

    #@11
    move v6, p6

    #@12
    .line 593
    invoke-direct/range {v0 .. v7}, Lcom/android/server/wm/ScreenRotationAnimation;->startAnimation(Landroid/view/SurfaceSession;JFIIZ)Z

    #@15
    .line 596
    :cond_15
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStarted:Z

    #@17
    if-nez v0, :cond_1b

    #@19
    move v7, v8

    #@1a
    .line 597
    goto :goto_7

    #@1b
    .line 600
    :cond_1b
    iput-boolean v7, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimReady:Z

    #@1d
    goto :goto_7
.end method

.method public getEnterTransformation()Landroid/view/animation/Transformation;
    .registers 2

    #@0
    .prologue
    .line 955
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterTransformation:Landroid/view/animation/Transformation;

    #@2
    return-object v0
.end method

.method hasScreenshot()Z
    .registers 2

    #@0
    .prologue
    .line 258
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isAnimating()Z
    .registers 2

    #@0
    .prologue
    .line 668
    invoke-direct {p0}, Lcom/android/server/wm/ScreenRotationAnimation;->hasAnimations()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    goto :goto_7
.end method

.method public isRotating()Z
    .registers 3

    #@0
    .prologue
    .line 672
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCurRotation:I

    #@2
    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalRotation:I

    #@4
    if-eq v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public kill()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 606
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 610
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@7
    invoke-virtual {v0}, Landroid/view/Surface;->destroy()V

    #@a
    .line 611
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@c
    .line 613
    :cond_c
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCustomBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 614
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCustomBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@12
    invoke-virtual {v0}, Lcom/android/server/wm/BlackFrame;->kill()V

    #@15
    .line 615
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCustomBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@17
    .line 617
    :cond_17
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@19
    if-eqz v0, :cond_22

    #@1b
    .line 618
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@1d
    invoke-virtual {v0}, Lcom/android/server/wm/BlackFrame;->kill()V

    #@20
    .line 619
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@22
    .line 621
    :cond_22
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@24
    if-eqz v0, :cond_2d

    #@26
    .line 622
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@28
    invoke-virtual {v0}, Lcom/android/server/wm/BlackFrame;->kill()V

    #@2b
    .line 623
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@2d
    .line 657
    :cond_2d
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@2f
    if-eqz v0, :cond_38

    #@31
    .line 658
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@33
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    #@36
    .line 659
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@38
    .line 661
    :cond_38
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@3a
    if-eqz v0, :cond_43

    #@3c
    .line 662
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@3e
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    #@41
    .line 663
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@43
    .line 665
    :cond_43
    return-void
.end method

.method public printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 130
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "mSurface="

    #@5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@d
    .line 131
    const-string v0, " mWidth="

    #@f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mWidth:I

    #@14
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@17
    .line 132
    const-string v0, " mHeight="

    #@19
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHeight:I

    #@1e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@21
    .line 139
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    const-string v0, "mExitingBlackFrame="

    #@26
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@2b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@2e
    .line 140
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@30
    if-eqz v0, :cond_4a

    #@32
    .line 141
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@34
    new-instance v1, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, "  "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v0, v1, p2}, Lcom/android/server/wm/BlackFrame;->printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@4a
    .line 143
    :cond_4a
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4d
    const-string v0, "mEnteringBlackFrame="

    #@4f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@52
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@54
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@57
    .line 144
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@59
    if-eqz v0, :cond_73

    #@5b
    .line 145
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@5d
    new-instance v1, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    const-string v2, "  "

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v0, v1, p2}, Lcom/android/server/wm/BlackFrame;->printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@73
    .line 147
    :cond_73
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@76
    const-string v0, "mCurRotation="

    #@78
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7b
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCurRotation:I

    #@7d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@80
    .line 148
    const-string v0, " mOriginalRotation="

    #@82
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@85
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalRotation:I

    #@87
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@8a
    .line 149
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8d
    const-string v0, "mOriginalWidth="

    #@8f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@92
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalWidth:I

    #@94
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@97
    .line 150
    const-string v0, " mOriginalHeight="

    #@99
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9c
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mOriginalHeight:I

    #@9e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@a1
    .line 151
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a4
    const-string v0, "mStarted="

    #@a6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a9
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStarted:Z

    #@ab
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@ae
    .line 152
    const-string v0, " mAnimRunning="

    #@b0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b3
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mAnimRunning:Z

    #@b5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@b8
    .line 153
    const-string v0, " mFinishAnimReady="

    #@ba
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bd
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimReady:Z

    #@bf
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@c2
    .line 154
    const-string v0, " mFinishAnimStartTime="

    #@c4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c7
    iget-wide v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimStartTime:J

    #@c9
    invoke-virtual {p2, v0, v1}, Ljava/io/PrintWriter;->println(J)V

    #@cc
    .line 155
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cf
    const-string v0, "mStartExitAnimation="

    #@d1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d4
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartExitAnimation:Landroid/view/animation/Animation;

    #@d6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@d9
    .line 156
    const-string v0, " "

    #@db
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@de
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartExitTransformation:Landroid/view/animation/Transformation;

    #@e0
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@e3
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@e6
    .line 157
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e9
    const-string v0, "mStartEnterAnimation="

    #@eb
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ee
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartEnterAnimation:Landroid/view/animation/Animation;

    #@f0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@f3
    .line 158
    const-string v0, " "

    #@f5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f8
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartEnterTransformation:Landroid/view/animation/Transformation;

    #@fa
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@fd
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@100
    .line 159
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@103
    const-string v0, "mStartFrameAnimation="

    #@105
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@108
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartFrameAnimation:Landroid/view/animation/Animation;

    #@10a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@10d
    .line 160
    const-string v0, " "

    #@10f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@112
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStartFrameTransformation:Landroid/view/animation/Transformation;

    #@114
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@117
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@11a
    .line 161
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11d
    const-string v0, "mFinishExitAnimation="

    #@11f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@122
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishExitAnimation:Landroid/view/animation/Animation;

    #@124
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@127
    .line 162
    const-string v0, " "

    #@129
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12c
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishExitTransformation:Landroid/view/animation/Transformation;

    #@12e
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@131
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@134
    .line 163
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@137
    const-string v0, "mFinishEnterAnimation="

    #@139
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13c
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishEnterAnimation:Landroid/view/animation/Animation;

    #@13e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@141
    .line 164
    const-string v0, " "

    #@143
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@146
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishEnterTransformation:Landroid/view/animation/Transformation;

    #@148
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@14b
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@14e
    .line 165
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@151
    const-string v0, "mFinishFrameAnimation="

    #@153
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@156
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishFrameAnimation:Landroid/view/animation/Animation;

    #@158
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@15b
    .line 166
    const-string v0, " "

    #@15d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@160
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishFrameTransformation:Landroid/view/animation/Transformation;

    #@162
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@165
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@168
    .line 167
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16b
    const-string v0, "mRotateExitAnimation="

    #@16d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@170
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@172
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@175
    .line 168
    const-string v0, " "

    #@177
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17a
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitTransformation:Landroid/view/animation/Transformation;

    #@17c
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@17f
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@182
    .line 169
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@185
    const-string v0, "mRotateEnterAnimation="

    #@187
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18a
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@18c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@18f
    .line 170
    const-string v0, " "

    #@191
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@194
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterTransformation:Landroid/view/animation/Transformation;

    #@196
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@199
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@19c
    .line 171
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19f
    const-string v0, "mRotateFrameAnimation="

    #@1a1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a4
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateFrameAnimation:Landroid/view/animation/Animation;

    #@1a6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@1a9
    .line 172
    const-string v0, " "

    #@1ab
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ae
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateFrameTransformation:Landroid/view/animation/Transformation;

    #@1b0
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@1b3
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1b6
    .line 173
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b9
    const-string v0, "mExitTransformation="

    #@1bb
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1be
    .line 174
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitTransformation:Landroid/view/animation/Transformation;

    #@1c0
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@1c3
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1c6
    .line 175
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c9
    const-string v0, "mEnterTransformation="

    #@1cb
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ce
    .line 176
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterTransformation:Landroid/view/animation/Transformation;

    #@1d0
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@1d3
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1d6
    .line 177
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d9
    const-string v0, "mFrameTransformation="

    #@1db
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1de
    .line 178
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterTransformation:Landroid/view/animation/Transformation;

    #@1e0
    invoke-virtual {v0, p2}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@1e3
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1e6
    .line 179
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e9
    const-string v0, "mFrameInitialMatrix="

    #@1eb
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ee
    .line 180
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFrameInitialMatrix:Landroid/graphics/Matrix;

    #@1f0
    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->printShortString(Ljava/io/PrintWriter;)V

    #@1f3
    .line 181
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1f6
    .line 182
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f9
    const-string v0, "mSnapshotInitialMatrix="

    #@1fb
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1fe
    .line 183
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotInitialMatrix:Landroid/graphics/Matrix;

    #@200
    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->printShortString(Ljava/io/PrintWriter;)V

    #@203
    .line 184
    const-string v0, " mSnapshotFinalMatrix="

    #@205
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@208
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotFinalMatrix:Landroid/graphics/Matrix;

    #@20a
    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->printShortString(Ljava/io/PrintWriter;)V

    #@20d
    .line 185
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@210
    .line 186
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@213
    const-string v0, "mExitFrameFinalMatrix="

    #@215
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@218
    .line 187
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitFrameFinalMatrix:Landroid/graphics/Matrix;

    #@21a
    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->printShortString(Ljava/io/PrintWriter;)V

    #@21d
    .line 188
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@220
    .line 189
    return-void
.end method

.method public setRotationInTransaction(ILandroid/view/SurfaceSession;JFII)Z
    .registers 9
    .parameter "rotation"
    .parameter "session"
    .parameter "maxAnimationDuration"
    .parameter "animationScale"
    .parameter "finalWidth"
    .parameter "finalHeight"

    #@0
    .prologue
    .line 326
    invoke-direct {p0, p1}, Lcom/android/server/wm/ScreenRotationAnimation;->setRotationInTransaction(I)V

    #@3
    .line 333
    const/4 v0, 0x0

    #@4
    return v0
.end method

.method public stepAnimationLocked(J)Z
    .registers 7
    .parameter "now"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 908
    invoke-direct {p0}, Lcom/android/server/wm/ScreenRotationAnimation;->hasAnimations()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_a

    #@7
    .line 910
    iput-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFinishAnimReady:Z

    #@9
    .line 951
    :goto_9
    return v0

    #@a
    .line 914
    :cond_a
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mAnimRunning:Z

    #@c
    if-nez v0, :cond_2f

    #@e
    .line 941
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 942
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@14
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@17
    .line 944
    :cond_17
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@19
    if-eqz v0, :cond_20

    #@1b
    .line 945
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateExitAnimation:Landroid/view/animation/Animation;

    #@1d
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@20
    .line 947
    :cond_20
    const/4 v0, 0x1

    #@21
    iput-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mAnimRunning:Z

    #@23
    .line 948
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mRotateEnterAnimation:Landroid/view/animation/Animation;

    #@25
    invoke-virtual {v0}, Landroid/view/animation/Animation;->getDuration()J

    #@28
    move-result-wide v0

    #@29
    const-wide/16 v2, 0x2

    #@2b
    div-long/2addr v0, v2

    #@2c
    add-long/2addr v0, p1

    #@2d
    iput-wide v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mHalfwayPoint:J

    #@2f
    .line 951
    :cond_2f
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/ScreenRotationAnimation;->stepAnimation(J)Z

    #@32
    move-result v0

    #@33
    goto :goto_9
.end method

.method updateSurfacesInTransaction()V
    .registers 4

    #@0
    .prologue
    .line 865
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mStarted:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 905
    :goto_4
    return-void

    #@5
    .line 869
    :cond_5
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@7
    if-eqz v0, :cond_1a

    #@9
    .line 870
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreStartExit:Z

    #@b
    if-nez v0, :cond_1a

    #@d
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreFinishExit:Z

    #@f
    if-nez v0, :cond_1a

    #@11
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateExit:Z

    #@13
    if-nez v0, :cond_1a

    #@15
    .line 872
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSurface:Landroid/view/Surface;

    #@17
    invoke-virtual {v0}, Landroid/view/Surface;->hide()V

    #@1a
    .line 876
    :cond_1a
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCustomBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@1c
    if-eqz v0, :cond_2f

    #@1e
    .line 877
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreStartFrame:Z

    #@20
    if-nez v0, :cond_65

    #@22
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreFinishFrame:Z

    #@24
    if-nez v0, :cond_65

    #@26
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateFrame:Z

    #@28
    if-nez v0, :cond_65

    #@2a
    .line 879
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCustomBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@2c
    invoke-virtual {v0}, Lcom/android/server/wm/BlackFrame;->hide()V

    #@2f
    .line 885
    :cond_2f
    :goto_2f
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@31
    if-eqz v0, :cond_44

    #@33
    .line 886
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreStartExit:Z

    #@35
    if-nez v0, :cond_71

    #@37
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreFinishExit:Z

    #@39
    if-nez v0, :cond_71

    #@3b
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateExit:Z

    #@3d
    if-nez v0, :cond_71

    #@3f
    .line 888
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@41
    invoke-virtual {v0}, Lcom/android/server/wm/BlackFrame;->hide()V

    #@44
    .line 895
    :cond_44
    :goto_44
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@46
    if-eqz v0, :cond_59

    #@48
    .line 896
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreStartEnter:Z

    #@4a
    if-nez v0, :cond_86

    #@4c
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreFinishEnter:Z

    #@4e
    if-nez v0, :cond_86

    #@50
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mMoreRotateEnter:Z

    #@52
    if-nez v0, :cond_86

    #@54
    .line 898
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@56
    invoke-virtual {v0}, Lcom/android/server/wm/BlackFrame;->hide()V

    #@59
    .line 904
    :cond_59
    :goto_59
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mSnapshotFinalMatrix:Landroid/graphics/Matrix;

    #@5b
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitTransformation:Landroid/view/animation/Transformation;

    #@5d
    invoke-virtual {v1}, Landroid/view/animation/Transformation;->getAlpha()F

    #@60
    move-result v1

    #@61
    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/ScreenRotationAnimation;->setSnapshotTransformInTransaction(Landroid/graphics/Matrix;F)V

    #@64
    goto :goto_4

    #@65
    .line 881
    :cond_65
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mCustomBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@67
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFrameTransformation:Landroid/view/animation/Transformation;

    #@69
    invoke-virtual {v1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {v0, v1}, Lcom/android/server/wm/BlackFrame;->setMatrix(Landroid/graphics/Matrix;)V

    #@70
    goto :goto_2f

    #@71
    .line 890
    :cond_71
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitFrameFinalMatrix:Landroid/graphics/Matrix;

    #@73
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitTransformation:Landroid/view/animation/Transformation;

    #@75
    invoke-virtual {v1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@78
    move-result-object v1

    #@79
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mFrameInitialMatrix:Landroid/graphics/Matrix;

    #@7b
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    #@7e
    .line 891
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitingBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@80
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mExitFrameFinalMatrix:Landroid/graphics/Matrix;

    #@82
    invoke-virtual {v0, v1}, Lcom/android/server/wm/BlackFrame;->setMatrix(Landroid/graphics/Matrix;)V

    #@85
    goto :goto_44

    #@86
    .line 900
    :cond_86
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnteringBlackFrame:Lcom/android/server/wm/BlackFrame;

    #@88
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimation;->mEnterTransformation:Landroid/view/animation/Transformation;

    #@8a
    invoke-virtual {v1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@8d
    move-result-object v1

    #@8e
    invoke-virtual {v0, v1}, Lcom/android/server/wm/BlackFrame;->setMatrix(Landroid/graphics/Matrix;)V

    #@91
    goto :goto_59
.end method
