.class public Lcom/android/server/wm/WindowAnimator;
.super Ljava/lang/Object;
.source "WindowAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;,
        Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;
    }
.end annotation


# static fields
.field static final KEYGUARD_ANIMATING_IN:I = 0x1

.field static final KEYGUARD_ANIMATING_OUT:I = 0x3

.field static final KEYGUARD_NOT_SHOWN:I = 0x0

.field static final KEYGUARD_SHOWN:I = 0x2

.field private static final TAG:Ljava/lang/String; = "WindowAnimator"

.field static final WALLPAPER_ACTION_PENDING:I = 0x1


# instance fields
.field mAboveUniverseLayer:I

.field mAdjResult:I

.field final mAnimToLayout:Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;

.field private mAnimTransactionSequence:I

.field mAnimating:Z

.field final mAnimationRunnable:Ljava/lang/Runnable;

.field mAppAnimators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/wm/AppWindowAnimator;",
            ">;"
        }
    .end annotation
.end field

.field mBulkUpdateParams:I

.field final mContext:Landroid/content/Context;

.field mCurrentFocus:Lcom/android/server/wm/WindowState;

.field mCurrentTime:J

.field mDh:I

.field mDisplayContentsAnimators:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;",
            ">;"
        }
    .end annotation
.end field

.field mDw:I

.field mForceHiding:I

.field mInitialized:Z

.field mInnerDh:I

.field mInnerDw:I

.field mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

.field mPendingActions:I

.field mPendingLayoutChanges:Landroid/util/SparseIntArray;

.field final mPolicy:Landroid/view/WindowManagerPolicy;

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

.field mUpperWallpaperTarget:Lcom/android/server/wm/WindowState;

.field mWallpaperTarget:Lcom/android/server/wm/WindowState;

.field mWallpaperTokens:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/wm/WindowToken;",
            ">;"
        }
    .end annotation
.end field

.field mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

.field mWpAppAnimator:Lcom/android/server/wm/AppWindowAnimator;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 5
    .parameter "service"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 127
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 56
    new-instance v0, Landroid/util/SparseIntArray;

    #@7
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@c
    .line 76
    iput-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@e
    .line 78
    iput-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@10
    .line 79
    iput v2, p0, Lcom/android/server/wm/WindowAnimator;->mAboveUniverseLayer:I

    #@12
    .line 81
    iput v2, p0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@14
    .line 83
    new-instance v0, Landroid/util/SparseArray;

    #@16
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@19
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@1b
    .line 89
    iput-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@1d
    .line 90
    iput-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mWpAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@1f
    .line 91
    iput-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@21
    .line 92
    iput-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mUpperWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@23
    .line 94
    new-instance v0, Ljava/util/ArrayList;

    #@25
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@28
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@2a
    .line 96
    new-instance v0, Ljava/util/ArrayList;

    #@2c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2f
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTokens:Ljava/util/ArrayList;

    #@31
    .line 106
    new-instance v0, Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;

    #@33
    invoke-direct {v0}, Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;-><init>()V

    #@36
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mAnimToLayout:Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;

    #@38
    .line 108
    iput-boolean v2, p0, Lcom/android/server/wm/WindowAnimator;->mInitialized:Z

    #@3a
    .line 115
    iput v2, p0, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@3c
    .line 128
    iput-object p1, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@3e
    .line 129
    iget-object v0, p1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@40
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mContext:Landroid/content/Context;

    #@42
    .line 130
    iget-object v0, p1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@44
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@46
    .line 132
    new-instance v0, Lcom/android/server/wm/WindowAnimator$1;

    #@48
    invoke-direct {v0, p0}, Lcom/android/server/wm/WindowAnimator$1;-><init>(Lcom/android/server/wm/WindowAnimator;)V

    #@4b
    iput-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mAnimationRunnable:Ljava/lang/Runnable;

    #@4d
    .line 145
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/wm/WindowAnimator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/server/wm/WindowAnimator;->copyLayoutToAnimParamsLocked()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/wm/WindowAnimator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/server/wm/WindowAnimator;->animateLocked()V

    #@3
    return-void
.end method

.method private animateLocked()V
    .registers 21

    #@0
    .prologue
    .line 673
    move-object/from16 v0, p0

    #@2
    iget-boolean v15, v0, Lcom/android/server/wm/WindowAnimator;->mInitialized:Z

    #@4
    if-nez v15, :cond_7

    #@6
    .line 779
    :cond_6
    :goto_6
    return-void

    #@7
    .line 677
    :cond_7
    move-object/from16 v0, p0

    #@9
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@b
    invoke-virtual {v15}, Landroid/util/SparseIntArray;->clear()V

    #@e
    .line 678
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@11
    move-result-wide v15

    #@12
    move-object/from16 v0, p0

    #@14
    iput-wide v15, v0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@16
    .line 679
    const/16 v15, 0x8

    #@18
    move-object/from16 v0, p0

    #@1a
    iput v15, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@1c
    .line 680
    move-object/from16 v0, p0

    #@1e
    iget-boolean v13, v0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@20
    .line 681
    .local v13, wasAnimating:Z
    const/4 v15, 0x0

    #@21
    move-object/from16 v0, p0

    #@23
    iput-boolean v15, v0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@25
    .line 688
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@28
    .line 689
    invoke-static {}, Landroid/view/Surface;->setAnimationTransaction()V

    #@2b
    .line 691
    :try_start_2b
    invoke-direct/range {p0 .. p0}, Lcom/android/server/wm/WindowAnimator;->updateAppWindowsLocked()V

    #@2e
    .line 693
    move-object/from16 v0, p0

    #@30
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@32
    invoke-virtual {v15}, Landroid/util/SparseArray;->size()I

    #@35
    move-result v11

    #@36
    .line 694
    .local v11, numDisplays:I
    const/4 v9, 0x0

    #@37
    .local v9, i:I
    :goto_37
    if-ge v9, v11, :cond_c8

    #@39
    .line 695
    move-object/from16 v0, p0

    #@3b
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@3d
    invoke-virtual {v15, v9}, Landroid/util/SparseArray;->keyAt(I)I

    #@40
    move-result v7

    #@41
    .line 696
    .local v7, displayId:I
    move-object/from16 v0, p0

    #@43
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@45
    invoke-virtual {v15, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@48
    move-result-object v6

    #@49
    check-cast v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@4b
    .line 698
    .local v6, displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    iget-object v12, v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@4d
    .line 700
    .local v12, screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    if-eqz v12, :cond_65

    #@4f
    invoke-virtual {v12}, Lcom/android/server/wm/ScreenRotationAnimation;->isAnimating()Z

    #@52
    move-result v15

    #@53
    if-eqz v15, :cond_65

    #@55
    .line 701
    move-object/from16 v0, p0

    #@57
    iget-wide v15, v0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@59
    move-wide v0, v15

    #@5a
    invoke-virtual {v12, v0, v1}, Lcom/android/server/wm/ScreenRotationAnimation;->stepAnimationLocked(J)Z

    #@5d
    move-result v15

    #@5e
    if-eqz v15, :cond_81

    #@60
    .line 702
    const/4 v15, 0x1

    #@61
    move-object/from16 v0, p0

    #@63
    iput-boolean v15, v0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@65
    .line 712
    :cond_65
    :goto_65
    move-object/from16 v0, p0

    #@67
    invoke-direct {v0, v7}, Lcom/android/server/wm/WindowAnimator;->performAnimationsLocked(I)V

    #@6a
    .line 714
    iget-object v14, v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWinAnimators:Lcom/android/server/wm/WinAnimatorList;

    #@6c
    .line 715
    .local v14, winAnimatorList:Lcom/android/server/wm/WinAnimatorList;
    invoke-virtual {v14}, Lcom/android/server/wm/WinAnimatorList;->size()I

    #@6f
    move-result v3

    #@70
    .line 716
    .local v3, N:I
    const/4 v10, 0x0

    #@71
    .local v10, j:I
    :goto_71
    if-ge v10, v3, :cond_c4

    #@73
    .line 717
    invoke-virtual {v14, v10}, Lcom/android/server/wm/WinAnimatorList;->get(I)Ljava/lang/Object;

    #@76
    move-result-object v15

    #@77
    check-cast v15, Lcom/android/server/wm/WindowStateAnimator;

    #@79
    const/16 v16, 0x1

    #@7b
    invoke-virtual/range {v15 .. v16}, Lcom/android/server/wm/WindowStateAnimator;->prepareSurfaceLocked(Z)V

    #@7e
    .line 716
    add-int/lit8 v10, v10, 0x1

    #@80
    goto :goto_71

    #@81
    .line 704
    .end local v3           #N:I
    .end local v10           #j:I
    .end local v14           #winAnimatorList:Lcom/android/server/wm/WinAnimatorList;
    :cond_81
    move-object/from16 v0, p0

    #@83
    iget v15, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@85
    or-int/lit8 v15, v15, 0x1

    #@87
    move-object/from16 v0, p0

    #@89
    iput v15, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@8b
    .line 705
    invoke-virtual {v12}, Lcom/android/server/wm/ScreenRotationAnimation;->kill()V

    #@8e
    .line 706
    const/4 v15, 0x0

    #@8f
    iput-object v15, v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    :try_end_91
    .catchall {:try_start_2b .. :try_end_91} :catchall_14e
    .catch Ljava/lang/RuntimeException; {:try_start_2b .. :try_end_91} :catch_92

    #@91
    goto :goto_65

    #@92
    .line 747
    .end local v6           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .end local v7           #displayId:I
    .end local v9           #i:I
    .end local v11           #numDisplays:I
    .end local v12           #screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    :catch_92
    move-exception v8

    #@93
    .line 748
    .local v8, e:Ljava/lang/RuntimeException;
    :try_start_93
    const-string v15, "WindowAnimator"

    #@95
    const-string v16, "Unhandled exception in Window Manager"

    #@97
    move-object/from16 v0, v16

    #@99
    invoke-static {v15, v0, v8}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9c
    .catchall {:try_start_93 .. :try_end_9c} :catchall_14e

    #@9c
    .line 750
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@9f
    .line 755
    .end local v8           #e:Ljava/lang/RuntimeException;
    :goto_9f
    move-object/from16 v0, p0

    #@a1
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@a3
    invoke-virtual {v15}, Landroid/util/SparseIntArray;->size()I

    #@a6
    move-result v15

    #@a7
    add-int/lit8 v9, v15, -0x1

    #@a9
    .restart local v9       #i:I
    :goto_a9
    if-ltz v9, :cond_153

    #@ab
    .line 756
    move-object/from16 v0, p0

    #@ad
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@af
    invoke-virtual {v15, v9}, Landroid/util/SparseIntArray;->valueAt(I)I

    #@b2
    move-result v15

    #@b3
    and-int/lit8 v15, v15, 0x4

    #@b5
    if-eqz v15, :cond_c1

    #@b7
    .line 758
    move-object/from16 v0, p0

    #@b9
    iget v15, v0, Lcom/android/server/wm/WindowAnimator;->mPendingActions:I

    #@bb
    or-int/lit8 v15, v15, 0x1

    #@bd
    move-object/from16 v0, p0

    #@bf
    iput v15, v0, Lcom/android/server/wm/WindowAnimator;->mPendingActions:I

    #@c1
    .line 755
    :cond_c1
    add-int/lit8 v9, v9, -0x1

    #@c3
    goto :goto_a9

    #@c4
    .line 694
    .restart local v3       #N:I
    .restart local v6       #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .restart local v7       #displayId:I
    .restart local v10       #j:I
    .restart local v11       #numDisplays:I
    .restart local v12       #screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    .restart local v14       #winAnimatorList:Lcom/android/server/wm/WinAnimatorList;
    :cond_c4
    add-int/lit8 v9, v9, 0x1

    #@c6
    goto/16 :goto_37

    #@c8
    .line 721
    .end local v3           #N:I
    .end local v6           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .end local v7           #displayId:I
    .end local v10           #j:I
    .end local v12           #screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    .end local v14           #winAnimatorList:Lcom/android/server/wm/WinAnimatorList;
    :cond_c8
    :try_start_c8
    invoke-direct/range {p0 .. p0}, Lcom/android/server/wm/WindowAnimator;->testTokenMayBeDrawnLocked()V

    #@cb
    .line 723
    const/4 v9, 0x0

    #@cc
    :goto_cc
    if-ge v9, v11, :cond_138

    #@ce
    .line 724
    move-object/from16 v0, p0

    #@d0
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@d2
    invoke-virtual {v15, v9}, Landroid/util/SparseArray;->keyAt(I)I

    #@d5
    move-result v7

    #@d6
    .line 725
    .restart local v7       #displayId:I
    move-object/from16 v0, p0

    #@d8
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@da
    invoke-virtual {v15, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@dd
    move-result-object v6

    #@de
    check-cast v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@e0
    .line 727
    .restart local v6       #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    iget-object v12, v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@e2
    .line 729
    .restart local v12       #screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    if-eqz v12, :cond_e7

    #@e4
    .line 730
    invoke-virtual {v12}, Lcom/android/server/wm/ScreenRotationAnimation;->updateSurfacesInTransaction()V

    #@e7
    .line 733
    :cond_e7
    iget-object v5, v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@e9
    .line 734
    .local v5, dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    iget-object v4, v6, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimAnimator:Lcom/android/server/wm/DimAnimator;

    #@eb
    .line 735
    .local v4, dimAnimator:Lcom/android/server/wm/DimAnimator;
    if-eqz v4, :cond_102

    #@ed
    if-eqz v5, :cond_102

    #@ef
    .line 736
    move-object/from16 v0, p0

    #@f1
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mContext:Landroid/content/Context;

    #@f3
    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f6
    move-result-object v15

    #@f7
    move-object/from16 v0, p0

    #@f9
    iget-wide v0, v0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@fb
    move-wide/from16 v16, v0

    #@fd
    move-wide/from16 v0, v16

    #@ff
    invoke-virtual {v4, v15, v5, v0, v1}, Lcom/android/server/wm/DimAnimator;->updateParameters(Landroid/content/res/Resources;Lcom/android/server/wm/DimAnimator$Parameters;J)V

    #@102
    .line 738
    :cond_102
    if-eqz v4, :cond_133

    #@104
    iget-boolean v15, v4, Lcom/android/server/wm/DimAnimator;->mDimShown:Z

    #@106
    if-eqz v15, :cond_133

    #@108
    .line 739
    move-object/from16 v0, p0

    #@10a
    iget-boolean v0, v0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@10c
    move/from16 v16, v0

    #@10e
    move-object/from16 v0, p0

    #@110
    invoke-virtual {v0, v7}, Lcom/android/server/wm/WindowAnimator;->isDimmingLocked(I)Z

    #@113
    move-result v17

    #@114
    move-object/from16 v0, p0

    #@116
    iget-wide v0, v0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@118
    move-wide/from16 v18, v0

    #@11a
    move-object/from16 v0, p0

    #@11c
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@11e
    invoke-virtual {v15}, Lcom/android/server/wm/WindowManagerService;->okToDisplay()Z

    #@121
    move-result v15

    #@122
    if-nez v15, :cond_136

    #@124
    const/4 v15, 0x1

    #@125
    :goto_125
    move/from16 v0, v17

    #@127
    move-wide/from16 v1, v18

    #@129
    invoke-virtual {v4, v0, v1, v2, v15}, Lcom/android/server/wm/DimAnimator;->updateSurface(ZJZ)Z

    #@12c
    move-result v15

    #@12d
    or-int v15, v15, v16

    #@12f
    move-object/from16 v0, p0

    #@131
    iput-boolean v15, v0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@133
    .line 723
    :cond_133
    add-int/lit8 v9, v9, 0x1

    #@135
    goto :goto_cc

    #@136
    .line 739
    :cond_136
    const/4 v15, 0x0

    #@137
    goto :goto_125

    #@138
    .line 744
    .end local v4           #dimAnimator:Lcom/android/server/wm/DimAnimator;
    .end local v5           #dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    .end local v6           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .end local v7           #displayId:I
    .end local v12           #screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    :cond_138
    move-object/from16 v0, p0

    #@13a
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@13c
    iget-object v15, v15, Lcom/android/server/wm/WindowManagerService;->mWatermark:Lcom/android/server/wm/Watermark;

    #@13e
    if-eqz v15, :cond_149

    #@140
    .line 745
    move-object/from16 v0, p0

    #@142
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@144
    iget-object v15, v15, Lcom/android/server/wm/WindowManagerService;->mWatermark:Lcom/android/server/wm/Watermark;

    #@146
    invoke-virtual {v15}, Lcom/android/server/wm/Watermark;->drawIfNeeded()V
    :try_end_149
    .catchall {:try_start_c8 .. :try_end_149} :catchall_14e
    .catch Ljava/lang/RuntimeException; {:try_start_c8 .. :try_end_149} :catch_92

    #@149
    .line 750
    :cond_149
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@14c
    goto/16 :goto_9f

    #@14e
    .end local v9           #i:I
    .end local v11           #numDisplays:I
    :catchall_14e
    move-exception v15

    #@14f
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@152
    throw v15

    #@153
    .line 762
    .restart local v9       #i:I
    :cond_153
    move-object/from16 v0, p0

    #@155
    iget v15, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@157
    if-nez v15, :cond_163

    #@159
    move-object/from16 v0, p0

    #@15b
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@15d
    invoke-virtual {v15}, Landroid/util/SparseIntArray;->size()I

    #@160
    move-result v15

    #@161
    if-lez v15, :cond_166

    #@163
    .line 763
    :cond_163
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/WindowAnimator;->updateAnimToLayoutLocked()V

    #@166
    .line 766
    :cond_166
    move-object/from16 v0, p0

    #@168
    iget-boolean v15, v0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@16a
    if-eqz v15, :cond_182

    #@16c
    .line 767
    move-object/from16 v0, p0

    #@16e
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@170
    iget-object v0, v15, Lcom/android/server/wm/WindowManagerService;->mLayoutToAnim:Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;

    #@172
    move-object/from16 v16, v0

    #@174
    monitor-enter v16

    #@175
    .line 768
    :try_start_175
    move-object/from16 v0, p0

    #@177
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@179
    invoke-virtual {v15}, Lcom/android/server/wm/WindowManagerService;->scheduleAnimationLocked()V

    #@17c
    .line 769
    monitor-exit v16

    #@17d
    goto/16 :goto_6

    #@17f
    :catchall_17f
    move-exception v15

    #@180
    monitor-exit v16
    :try_end_181
    .catchall {:try_start_175 .. :try_end_181} :catchall_17f

    #@181
    throw v15

    #@182
    .line 770
    :cond_182
    if-eqz v13, :cond_6

    #@184
    .line 771
    move-object/from16 v0, p0

    #@186
    iget-object v15, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@188
    invoke-virtual {v15}, Lcom/android/server/wm/WindowManagerService;->requestTraversalLocked()V

    #@18b
    goto/16 :goto_6
.end method

.method static bulkUpdateParamsToString(I)Ljava/lang/String;
    .registers 3
    .parameter "bulkUpdateParams"

    #@0
    .prologue
    .line 805
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 806
    .local v0, builder:Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x1

    #@9
    if-eqz v1, :cond_10

    #@b
    .line 807
    const-string v1, " UPDATE_ROTATION"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 809
    :cond_10
    and-int/lit8 v1, p0, 0x2

    #@12
    if-eqz v1, :cond_19

    #@14
    .line 810
    const-string v1, " WALLPAPER_MAY_CHANGE"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 812
    :cond_19
    and-int/lit8 v1, p0, 0x4

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 813
    const-string v1, " FORCE_HIDING_CHANGED"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 815
    :cond_22
    and-int/lit8 v1, p0, 0x8

    #@24
    if-eqz v1, :cond_2b

    #@26
    .line 816
    const-string v1, " ORIENTATION_CHANGE_COMPLETE"

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 818
    :cond_2b
    and-int/lit8 v1, p0, 0x10

    #@2d
    if-eqz v1, :cond_34

    #@2f
    .line 819
    const-string v1, " TURN_ON_SCREEN"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 821
    :cond_34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    return-object v1
.end method

.method private copyLayoutToAnimParamsLocked()V
    .registers 18

    #@0
    .prologue
    .line 192
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4
    iget-object v8, v13, Lcom/android/server/wm/WindowManagerService;->mLayoutToAnim:Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;

    #@6
    .line 193
    .local v8, layoutToAnim:Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;
    monitor-enter v8

    #@7
    .line 194
    const/4 v13, 0x0

    #@8
    :try_start_8
    iput-boolean v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mAnimationScheduled:Z

    #@a
    .line 196
    iget-boolean v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mParamsModified:Z

    #@c
    if-nez v13, :cond_10

    #@e
    .line 197
    monitor-exit v8

    #@f
    .line 264
    :goto_f
    return-void

    #@10
    .line 199
    :cond_10
    const/4 v13, 0x0

    #@11
    iput-boolean v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mParamsModified:Z

    #@13
    .line 201
    iget-wide v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mChanges:J

    #@15
    const-wide/16 v15, 0x1

    #@17
    and-long/2addr v13, v15

    #@18
    const-wide/16 v15, 0x0

    #@1a
    cmp-long v13, v13, v15

    #@1c
    if-eqz v13, :cond_30

    #@1e
    .line 202
    iget-wide v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mChanges:J

    #@20
    const-wide/16 v15, -0x2

    #@22
    and-long/2addr v13, v15

    #@23
    iput-wide v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mChanges:J

    #@25
    .line 203
    new-instance v13, Ljava/util/ArrayList;

    #@27
    iget-object v14, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mWallpaperTokens:Ljava/util/ArrayList;

    #@29
    invoke-direct {v13, v14}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@2c
    move-object/from16 v0, p0

    #@2e
    iput-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTokens:Ljava/util/ArrayList;

    #@30
    .line 215
    :cond_30
    iget-object v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@32
    move-object/from16 v0, p0

    #@34
    iput-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@36
    .line 216
    move-object/from16 v0, p0

    #@38
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@3a
    if-nez v13, :cond_8e

    #@3c
    const/4 v13, 0x0

    #@3d
    :goto_3d
    move-object/from16 v0, p0

    #@3f
    iput-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mWpAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@41
    .line 219
    iget-object v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@43
    move-object/from16 v0, p0

    #@45
    iput-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@47
    .line 220
    iget-object v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mUpperWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@49
    move-object/from16 v0, p0

    #@4b
    iput-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mUpperWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@4d
    .line 223
    move-object/from16 v0, p0

    #@4f
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@51
    invoke-virtual {v13}, Landroid/util/SparseArray;->size()I

    #@54
    move-result v10

    #@55
    .line 224
    .local v10, numDisplays:I
    const/4 v7, 0x0

    #@56
    .local v7, i:I
    :goto_56
    if-ge v7, v10, :cond_c8

    #@58
    .line 225
    move-object/from16 v0, p0

    #@5a
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@5c
    invoke-virtual {v13, v7}, Landroid/util/SparseArray;->keyAt(I)I

    #@5f
    move-result v5

    #@60
    .line 226
    .local v5, displayId:I
    move-object/from16 v0, p0

    #@62
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@64
    invoke-virtual {v13, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@67
    move-result-object v4

    #@68
    check-cast v4, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@6a
    .line 228
    .local v4, displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    iget-object v13, v4, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWinAnimators:Lcom/android/server/wm/WinAnimatorList;

    #@6c
    invoke-virtual {v13}, Lcom/android/server/wm/WinAnimatorList;->clear()V

    #@6f
    .line 229
    iget-object v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mWinAnimatorLists:Landroid/util/SparseArray;

    #@71
    invoke-virtual {v13, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@74
    move-result-object v12

    #@75
    check-cast v12, Lcom/android/server/wm/WinAnimatorList;

    #@77
    .line 230
    .local v12, winAnimators:Lcom/android/server/wm/WinAnimatorList;
    if-eqz v12, :cond_7e

    #@79
    .line 231
    iget-object v13, v4, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWinAnimators:Lcom/android/server/wm/WinAnimatorList;

    #@7b
    invoke-virtual {v13, v12}, Lcom/android/server/wm/WinAnimatorList;->addAll(Ljava/util/Collection;)Z

    #@7e
    .line 234
    :cond_7e
    iget-object v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mDimParams:Landroid/util/SparseArray;

    #@80
    invoke-virtual {v13, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@83
    move-result-object v3

    #@84
    check-cast v3, Lcom/android/server/wm/DimAnimator$Parameters;

    #@86
    .line 235
    .local v3, dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    if-nez v3, :cond_a1

    #@88
    .line 236
    const/4 v13, 0x0

    #@89
    iput-object v13, v4, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@8b
    .line 224
    :cond_8b
    :goto_8b
    add-int/lit8 v7, v7, 0x1

    #@8d
    goto :goto_56

    #@8e
    .line 216
    .end local v3           #dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    .end local v4           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .end local v5           #displayId:I
    .end local v7           #i:I
    .end local v10           #numDisplays:I
    .end local v12           #winAnimators:Lcom/android/server/wm/WinAnimatorList;
    :cond_8e
    move-object/from16 v0, p0

    #@90
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@92
    iget-object v13, v13, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@94
    if-nez v13, :cond_98

    #@96
    const/4 v13, 0x0

    #@97
    goto :goto_3d

    #@98
    :cond_98
    move-object/from16 v0, p0

    #@9a
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@9c
    iget-object v13, v13, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@9e
    iget-object v13, v13, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@a0
    goto :goto_3d

    #@a1
    .line 238
    .restart local v3       #dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    .restart local v4       #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .restart local v5       #displayId:I
    .restart local v7       #i:I
    .restart local v10       #numDisplays:I
    .restart local v12       #winAnimators:Lcom/android/server/wm/WinAnimatorList;
    :cond_a1
    iget-object v9, v3, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@a3
    .line 241
    .local v9, newWinAnimator:Lcom/android/server/wm/WindowStateAnimator;
    iget-object v13, v4, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@a5
    if-nez v13, :cond_c3

    #@a7
    const/4 v6, 0x0

    #@a8
    .line 246
    .local v6, existingDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :goto_a8
    iget-boolean v13, v9, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    #@aa
    if-eqz v13, :cond_8b

    #@ac
    if-eqz v6, :cond_b8

    #@ae
    iget-boolean v13, v6, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    #@b0
    if-eqz v13, :cond_b8

    #@b2
    iget v13, v6, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@b4
    iget v14, v9, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@b6
    if-ge v13, v14, :cond_8b

    #@b8
    .line 249
    :cond_b8
    new-instance v13, Lcom/android/server/wm/DimAnimator$Parameters;

    #@ba
    invoke-direct {v13, v3}, Lcom/android/server/wm/DimAnimator$Parameters;-><init>(Lcom/android/server/wm/DimAnimator$Parameters;)V

    #@bd
    iput-object v13, v4, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@bf
    goto :goto_8b

    #@c0
    .line 263
    .end local v3           #dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    .end local v4           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .end local v5           #displayId:I
    .end local v6           #existingDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;
    .end local v7           #i:I
    .end local v9           #newWinAnimator:Lcom/android/server/wm/WindowStateAnimator;
    .end local v10           #numDisplays:I
    .end local v12           #winAnimators:Lcom/android/server/wm/WinAnimatorList;
    :catchall_c0
    move-exception v13

    #@c1
    monitor-exit v8
    :try_end_c2
    .catchall {:try_start_8 .. :try_end_c2} :catchall_c0

    #@c2
    throw v13

    #@c3
    .line 241
    .restart local v3       #dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    .restart local v4       #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .restart local v5       #displayId:I
    .restart local v7       #i:I
    .restart local v9       #newWinAnimator:Lcom/android/server/wm/WindowStateAnimator;
    .restart local v10       #numDisplays:I
    .restart local v12       #winAnimators:Lcom/android/server/wm/WinAnimatorList;
    :cond_c3
    :try_start_c3
    iget-object v13, v4, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@c5
    iget-object v6, v13, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@c7
    goto :goto_a8

    #@c8
    .line 254
    .end local v3           #dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    .end local v4           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .end local v5           #displayId:I
    .end local v9           #newWinAnimator:Lcom/android/server/wm/WindowStateAnimator;
    .end local v12           #winAnimators:Lcom/android/server/wm/WinAnimatorList;
    :cond_c8
    move-object/from16 v0, p0

    #@ca
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@cc
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    #@cf
    .line 255
    iget-object v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mAppWindowAnimParams:Ljava/util/ArrayList;

    #@d1
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@d4
    move-result v1

    #@d5
    .line 256
    .local v1, N:I
    const/4 v7, 0x0

    #@d6
    :goto_d6
    if-ge v7, v1, :cond_f8

    #@d8
    .line 257
    iget-object v13, v8, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mAppWindowAnimParams:Ljava/util/ArrayList;

    #@da
    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@dd
    move-result-object v11

    #@de
    check-cast v11, Lcom/android/server/wm/WindowManagerService$AppWindowAnimParams;

    #@e0
    .line 258
    .local v11, params:Lcom/android/server/wm/WindowManagerService$AppWindowAnimParams;
    iget-object v2, v11, Lcom/android/server/wm/WindowManagerService$AppWindowAnimParams;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@e2
    .line 259
    .local v2, appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    iget-object v13, v2, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@e4
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    #@e7
    .line 260
    iget-object v13, v2, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@e9
    iget-object v14, v11, Lcom/android/server/wm/WindowManagerService$AppWindowAnimParams;->mWinAnimators:Ljava/util/ArrayList;

    #@eb
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@ee
    .line 261
    move-object/from16 v0, p0

    #@f0
    iget-object v13, v0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@f2
    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f5
    .line 256
    add-int/lit8 v7, v7, 0x1

    #@f7
    goto :goto_d6

    #@f8
    .line 263
    .end local v2           #appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    .end local v11           #params:Lcom/android/server/wm/WindowManagerService$AppWindowAnimParams;
    :cond_f8
    monitor-exit v8
    :try_end_f9
    .catchall {:try_start_c3 .. :try_end_f9} :catchall_c0

    #@f9
    goto/16 :goto_f
.end method

.method private forceHidingToString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 118
    iget v0, p0, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@2
    packed-switch v0, :pswitch_data_28

    #@5
    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, "KEYGUARD STATE UNKNOWN "

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget v1, p0, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    :goto_1a
    return-object v0

    #@1b
    .line 119
    :pswitch_1b
    const-string v0, "KEYGUARD_NOT_SHOWN"

    #@1d
    goto :goto_1a

    #@1e
    .line 120
    :pswitch_1e
    const-string v0, "KEYGUARD_ANIMATING_IN"

    #@20
    goto :goto_1a

    #@21
    .line 121
    :pswitch_21
    const-string v0, "KEYGUARD_SHOWN"

    #@23
    goto :goto_1a

    #@24
    .line 122
    :pswitch_24
    const-string v0, "KEYGUARD_ANIMATING_OUT"

    #@26
    goto :goto_1a

    #@27
    .line 118
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
    .end packed-switch
.end method

.method private getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .registers 4
    .parameter "displayId"

    #@0
    .prologue
    .line 972
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@8
    .line 973
    .local v0, displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    if-nez v0, :cond_14

    #@a
    .line 974
    new-instance v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@c
    .end local v0           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    invoke-direct {v0, p0, p1}, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;-><init>(Lcom/android/server/wm/WindowAnimator;I)V

    #@f
    .line 975
    .restart local v0       #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 977
    :cond_14
    return-object v0
.end method

.method private performAnimationsLocked(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 666
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowAnimator;->updateWindowsLocked(I)V

    #@3
    .line 667
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowAnimator;->updateWallpaperLocked(I)V

    #@6
    .line 668
    return-void
.end method

.method private testTokenMayBeDrawnLocked()V
    .registers 9

    #@0
    .prologue
    .line 629
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 630
    .local v0, NT:I
    const/4 v3, 0x0

    #@7
    .local v3, i:I
    :goto_7
    if-ge v3, v0, :cond_4f

    #@9
    .line 631
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/server/wm/AppWindowAnimator;

    #@11
    .line 632
    .local v2, appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    iget-object v4, v2, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@13
    .line 633
    .local v4, wtoken:Lcom/android/server/wm/AppWindowToken;
    iget-boolean v1, v4, Lcom/android/server/wm/AppWindowToken;->allDrawn:Z

    #@15
    .line 634
    .local v1, allDrawn:Z
    iget-boolean v5, v2, Lcom/android/server/wm/AppWindowAnimator;->allDrawn:Z

    #@17
    if-eq v1, v5, :cond_31

    #@19
    .line 635
    iput-boolean v1, v2, Lcom/android/server/wm/AppWindowAnimator;->allDrawn:Z

    #@1b
    .line 636
    if-eqz v1, :cond_31

    #@1d
    .line 639
    iget-boolean v5, v2, Lcom/android/server/wm/AppWindowAnimator;->freezingScreen:Z

    #@1f
    if-eqz v5, :cond_34

    #@21
    .line 640
    invoke-virtual {v2}, Lcom/android/server/wm/AppWindowAnimator;->showAllWindowsLocked()Z

    #@24
    .line 641
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@26
    const/4 v6, 0x0

    #@27
    const/4 v7, 0x1

    #@28
    invoke-virtual {v5, v4, v6, v7}, Lcom/android/server/wm/WindowManagerService;->unsetAppFreezingScreenLocked(Lcom/android/server/wm/AppWindowToken;ZZ)V

    #@2b
    .line 647
    const/4 v5, 0x4

    #@2c
    const-string v6, "testTokenMayBeDrawnLocked: freezingScreen"

    #@2e
    invoke-virtual {p0, v2, v5, v6}, Lcom/android/server/wm/WindowAnimator;->setAppLayoutChanges(Lcom/android/server/wm/AppWindowAnimator;ILjava/lang/String;)V

    #@31
    .line 630
    :cond_31
    :goto_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_7

    #@34
    .line 651
    :cond_34
    const/16 v5, 0x8

    #@36
    const-string v6, "testTokenMayBeDrawnLocked"

    #@38
    invoke-virtual {p0, v2, v5, v6}, Lcom/android/server/wm/WindowAnimator;->setAppLayoutChanges(Lcom/android/server/wm/AppWindowAnimator;ILjava/lang/String;)V

    #@3b
    .line 656
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@3d
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mOpeningApps:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@42
    move-result v5

    #@43
    if-nez v5, :cond_31

    #@45
    .line 657
    iget-boolean v5, p0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@47
    invoke-virtual {v2}, Lcom/android/server/wm/AppWindowAnimator;->showAllWindowsLocked()Z

    #@4a
    move-result v6

    #@4b
    or-int/2addr v5, v6

    #@4c
    iput-boolean v5, p0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@4e
    goto :goto_31

    #@4f
    .line 663
    .end local v1           #allDrawn:Z
    .end local v2           #appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    .end local v4           #wtoken:Lcom/android/server/wm/AppWindowToken;
    :cond_4f
    return-void
.end method

.method private updateAppWindowsLocked()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x4

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    .line 312
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    .line 313
    .local v0, NAT:I
    const/4 v3, 0x0

    #@a
    .local v3, i:I
    :goto_a
    if-ge v3, v0, :cond_53

    #@c
    .line 314
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Lcom/android/server/wm/AppWindowAnimator;

    #@14
    .line 315
    .local v2, appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    iget-object v5, v2, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@16
    if-eqz v5, :cond_30

    #@18
    iget-object v5, v2, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@1a
    sget-object v8, Lcom/android/server/wm/AppWindowAnimator;->sDummyAnimation:Landroid/view/animation/Animation;

    #@1c
    if-eq v5, v8, :cond_30

    #@1e
    move v4, v6

    #@1f
    .line 317
    .local v4, wasAnimating:Z
    :goto_1f
    iget-wide v8, p0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@21
    iget v5, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDw:I

    #@23
    iget v10, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDh:I

    #@25
    invoke-virtual {v2, v8, v9, v5, v10}, Lcom/android/server/wm/AppWindowAnimator;->stepAnimationLocked(JII)Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_32

    #@2b
    .line 318
    iput-boolean v6, p0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@2d
    .line 313
    :cond_2d
    :goto_2d
    add-int/lit8 v3, v3, 0x1

    #@2f
    goto :goto_a

    #@30
    .end local v4           #wasAnimating:Z
    :cond_30
    move v4, v7

    #@31
    .line 315
    goto :goto_1f

    #@32
    .line 319
    .restart local v4       #wasAnimating:Z
    :cond_32
    if-eqz v4, :cond_2d

    #@34
    .line 321
    new-instance v5, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v8, "appToken "

    #@3b
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    iget-object v8, v2, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@41
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    const-string v8, " done"

    #@47
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {p0, v2, v11, v5}, Lcom/android/server/wm/WindowAnimator;->setAppLayoutChanges(Lcom/android/server/wm/AppWindowAnimator;ILjava/lang/String;)V

    #@52
    goto :goto_2d

    #@53
    .line 328
    .end local v2           #appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    .end local v4           #wasAnimating:Z
    :cond_53
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@55
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mExitingAppTokens:Ljava/util/ArrayList;

    #@57
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5a
    move-result v1

    #@5b
    .line 329
    .local v1, NEAT:I
    const/4 v3, 0x0

    #@5c
    :goto_5c
    if-ge v3, v1, :cond_a9

    #@5e
    .line 330
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@60
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mExitingAppTokens:Ljava/util/ArrayList;

    #@62
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@65
    move-result-object v5

    #@66
    check-cast v5, Lcom/android/server/wm/AppWindowToken;

    #@68
    iget-object v2, v5, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@6a
    .line 331
    .restart local v2       #appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    iget-object v5, v2, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@6c
    if-eqz v5, :cond_86

    #@6e
    iget-object v5, v2, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@70
    sget-object v8, Lcom/android/server/wm/AppWindowAnimator;->sDummyAnimation:Landroid/view/animation/Animation;

    #@72
    if-eq v5, v8, :cond_86

    #@74
    move v4, v6

    #@75
    .line 333
    .restart local v4       #wasAnimating:Z
    :goto_75
    iget-wide v8, p0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@77
    iget v5, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDw:I

    #@79
    iget v10, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDh:I

    #@7b
    invoke-virtual {v2, v8, v9, v5, v10}, Lcom/android/server/wm/AppWindowAnimator;->stepAnimationLocked(JII)Z

    #@7e
    move-result v5

    #@7f
    if-eqz v5, :cond_88

    #@81
    .line 334
    iput-boolean v6, p0, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@83
    .line 329
    :cond_83
    :goto_83
    add-int/lit8 v3, v3, 0x1

    #@85
    goto :goto_5c

    #@86
    .end local v4           #wasAnimating:Z
    :cond_86
    move v4, v7

    #@87
    .line 331
    goto :goto_75

    #@88
    .line 335
    .restart local v4       #wasAnimating:Z
    :cond_88
    if-eqz v4, :cond_83

    #@8a
    .line 337
    new-instance v5, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v8, "exiting appToken "

    #@91
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    iget-object v8, v2, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@97
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v5

    #@9b
    const-string v8, " done"

    #@9d
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v5

    #@a1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v5

    #@a5
    invoke-virtual {p0, v2, v11, v5}, Lcom/android/server/wm/WindowAnimator;->setAppLayoutChanges(Lcom/android/server/wm/AppWindowAnimator;ILjava/lang/String;)V

    #@a8
    goto :goto_83

    #@a9
    .line 343
    .end local v2           #appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    .end local v4           #wasAnimating:Z
    :cond_a9
    return-void
.end method

.method private updateWallpaperLocked(I)V
    .registers 24
    .parameter "displayId"

    #@0
    .prologue
    .line 527
    invoke-direct/range {p0 .. p1}, Lcom/android/server/wm/WindowAnimator;->getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@3
    move-result-object v10

    #@4
    .line 529
    .local v10, displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    iget-object v15, v10, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWinAnimators:Lcom/android/server/wm/WinAnimatorList;

    #@6
    .line 530
    .local v15, winAnimatorList:Lcom/android/server/wm/WinAnimatorList;
    const/16 v16, 0x0

    #@8
    .line 531
    .local v16, windowAnimationBackground:Lcom/android/server/wm/WindowStateAnimator;
    const/16 v17, 0x0

    #@a
    .line 532
    .local v17, windowAnimationBackgroundColor:I
    const/4 v9, 0x0

    #@b
    .line 533
    .local v9, detachedWallpaper:Lcom/android/server/wm/WindowState;
    iget-object v0, v10, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWindowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;

    #@d
    move-object/from16 v18, v0

    #@f
    .line 536
    .local v18, windowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;
    invoke-virtual {v15}, Lcom/android/server/wm/WinAnimatorList;->size()I

    #@12
    move-result v19

    #@13
    add-int/lit8 v12, v19, -0x1

    #@15
    .local v12, i:I
    :goto_15
    if-ltz v12, :cond_b2

    #@17
    .line 537
    invoke-virtual {v15, v12}, Lcom/android/server/wm/WinAnimatorList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v14

    #@1b
    check-cast v14, Lcom/android/server/wm/WindowStateAnimator;

    #@1d
    .line 538
    .local v14, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    iget-object v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@1f
    move-object/from16 v19, v0

    #@21
    if-nez v19, :cond_26

    #@23
    .line 536
    :cond_23
    :goto_23
    add-int/lit8 v12, v12, -0x1

    #@25
    goto :goto_15

    #@26
    .line 542
    :cond_26
    iget v11, v14, Lcom/android/server/wm/WindowStateAnimator;->mAttrFlags:I

    #@28
    .line 543
    .local v11, flags:I
    iget-object v13, v14, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@2a
    .line 548
    .local v13, win:Lcom/android/server/wm/WindowState;
    iget-boolean v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@2c
    move/from16 v19, v0

    #@2e
    if-eqz v19, :cond_6f

    #@30
    .line 549
    iget-object v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@32
    move-object/from16 v19, v0

    #@34
    if-eqz v19, :cond_67

    #@36
    .line 550
    const/high16 v19, 0x10

    #@38
    and-int v19, v19, v11

    #@3a
    if-eqz v19, :cond_47

    #@3c
    iget-object v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@3e
    move-object/from16 v19, v0

    #@40
    invoke-virtual/range {v19 .. v19}, Landroid/view/animation/Animation;->getDetachWallpaper()Z

    #@43
    move-result v19

    #@44
    if-eqz v19, :cond_47

    #@46
    .line 552
    move-object v9, v13

    #@47
    .line 554
    :cond_47
    iget-object v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@49
    move-object/from16 v19, v0

    #@4b
    invoke-virtual/range {v19 .. v19}, Landroid/view/animation/Animation;->getBackgroundColor()I

    #@4e
    move-result v8

    #@4f
    .line 555
    .local v8, backgroundColor:I
    if-eqz v8, :cond_67

    #@51
    .line 556
    if-eqz v16, :cond_63

    #@53
    iget v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@55
    move/from16 v19, v0

    #@57
    move-object/from16 v0, v16

    #@59
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@5b
    move/from16 v20, v0

    #@5d
    move/from16 v0, v19

    #@5f
    move/from16 v1, v20

    #@61
    if-ge v0, v1, :cond_67

    #@63
    .line 558
    :cond_63
    move-object/from16 v16, v14

    #@65
    .line 559
    move/from16 v17, v8

    #@67
    .line 563
    .end local v8           #backgroundColor:I
    :cond_67
    const/16 v19, 0x1

    #@69
    move/from16 v0, v19

    #@6b
    move-object/from16 v1, p0

    #@6d
    iput-boolean v0, v1, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@6f
    .line 569
    :cond_6f
    iget-object v7, v14, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@71
    .line 570
    .local v7, appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    if-eqz v7, :cond_23

    #@73
    iget-object v0, v7, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@75
    move-object/from16 v19, v0

    #@77
    if-eqz v19, :cond_23

    #@79
    iget-boolean v0, v7, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@7b
    move/from16 v19, v0

    #@7d
    if-eqz v19, :cond_23

    #@7f
    .line 572
    const/high16 v19, 0x10

    #@81
    and-int v19, v19, v11

    #@83
    if-eqz v19, :cond_90

    #@85
    iget-object v0, v7, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@87
    move-object/from16 v19, v0

    #@89
    invoke-virtual/range {v19 .. v19}, Landroid/view/animation/Animation;->getDetachWallpaper()Z

    #@8c
    move-result v19

    #@8d
    if-eqz v19, :cond_90

    #@8f
    .line 574
    move-object v9, v13

    #@90
    .line 577
    :cond_90
    iget-object v0, v7, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@92
    move-object/from16 v19, v0

    #@94
    invoke-virtual/range {v19 .. v19}, Landroid/view/animation/Animation;->getBackgroundColor()I

    #@97
    move-result v8

    #@98
    .line 578
    .restart local v8       #backgroundColor:I
    if-eqz v8, :cond_23

    #@9a
    .line 579
    if-eqz v16, :cond_ac

    #@9c
    iget v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@9e
    move/from16 v19, v0

    #@a0
    move-object/from16 v0, v16

    #@a2
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@a4
    move/from16 v20, v0

    #@a6
    move/from16 v0, v19

    #@a8
    move/from16 v1, v20

    #@aa
    if-ge v0, v1, :cond_23

    #@ac
    .line 581
    :cond_ac
    move-object/from16 v16, v14

    #@ae
    .line 582
    move/from16 v17, v8

    #@b0
    goto/16 :goto_23

    #@b2
    .line 588
    .end local v7           #appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    .end local v8           #backgroundColor:I
    .end local v11           #flags:I
    .end local v13           #win:Lcom/android/server/wm/WindowState;
    .end local v14           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_b2
    move-object/from16 v0, p0

    #@b4
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@b6
    move-object/from16 v19, v0

    #@b8
    move-object/from16 v0, v19

    #@ba
    if-eq v0, v9, :cond_ce

    #@bc
    .line 592
    move-object/from16 v0, p0

    #@be
    iput-object v9, v0, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@c0
    .line 593
    move-object/from16 v0, p0

    #@c2
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@c4
    move/from16 v19, v0

    #@c6
    or-int/lit8 v19, v19, 0x2

    #@c8
    move/from16 v0, v19

    #@ca
    move-object/from16 v1, p0

    #@cc
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@ce
    .line 596
    :cond_ce
    if-eqz v17, :cond_13c

    #@d0
    sget-boolean v19, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@d2
    if-eqz v19, :cond_13c

    #@d4
    move-object/from16 v0, p0

    #@d6
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@d8
    move-object/from16 v19, v0

    #@da
    invoke-virtual/range {v19 .. v19}, Lcom/android/server/wm/WindowManagerService;->getScreenIdOfFocusedWin()I

    #@dd
    move-result v19

    #@de
    if-nez v19, :cond_13c

    #@e0
    .line 600
    move-object/from16 v0, v16

    #@e2
    iget v6, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@e4
    .line 601
    .local v6, animLayer:I
    move-object/from16 v0, v16

    #@e6
    iget-object v13, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@e8
    .line 602
    .restart local v13       #win:Lcom/android/server/wm/WindowState;
    move-object/from16 v0, p0

    #@ea
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@ec
    move-object/from16 v19, v0

    #@ee
    move-object/from16 v0, v19

    #@f0
    if-eq v0, v13, :cond_106

    #@f2
    move-object/from16 v0, p0

    #@f4
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@f6
    move-object/from16 v19, v0

    #@f8
    move-object/from16 v0, v19

    #@fa
    if-eq v0, v13, :cond_106

    #@fc
    move-object/from16 v0, p0

    #@fe
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUpperWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@100
    move-object/from16 v19, v0

    #@102
    move-object/from16 v0, v19

    #@104
    if-ne v0, v13, :cond_11b

    #@106
    .line 604
    :cond_106
    invoke-virtual {v15}, Lcom/android/server/wm/WinAnimatorList;->size()I

    #@109
    move-result v5

    #@10a
    .line 605
    .local v5, N:I
    const/4 v12, 0x0

    #@10b
    :goto_10b
    if-ge v12, v5, :cond_11b

    #@10d
    .line 606
    invoke-virtual {v15, v12}, Lcom/android/server/wm/WinAnimatorList;->get(I)Ljava/lang/Object;

    #@110
    move-result-object v14

    #@111
    check-cast v14, Lcom/android/server/wm/WindowStateAnimator;

    #@113
    .line 607
    .restart local v14       #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    iget-boolean v0, v14, Lcom/android/server/wm/WindowStateAnimator;->mIsWallpaper:Z

    #@115
    move/from16 v19, v0

    #@117
    if-eqz v19, :cond_139

    #@119
    .line 608
    iget v6, v14, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@11b
    .line 614
    .end local v5           #N:I
    .end local v14           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_11b
    if-eqz v18, :cond_138

    #@11d
    .line 615
    move-object/from16 v0, p0

    #@11f
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mDw:I

    #@121
    move/from16 v19, v0

    #@123
    move-object/from16 v0, p0

    #@125
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mDh:I

    #@127
    move/from16 v20, v0

    #@129
    add-int/lit8 v21, v6, -0x1

    #@12b
    move-object/from16 v0, v18

    #@12d
    move/from16 v1, v19

    #@12f
    move/from16 v2, v20

    #@131
    move/from16 v3, v21

    #@133
    move/from16 v4, v17

    #@135
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/wm/DimSurface;->show(IIII)V

    #@138
    .line 624
    .end local v6           #animLayer:I
    .end local v13           #win:Lcom/android/server/wm/WindowState;
    :cond_138
    :goto_138
    return-void

    #@139
    .line 605
    .restart local v5       #N:I
    .restart local v6       #animLayer:I
    .restart local v13       #win:Lcom/android/server/wm/WindowState;
    .restart local v14       #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_139
    add-int/lit8 v12, v12, 0x1

    #@13b
    goto :goto_10b

    #@13c
    .line 620
    .end local v5           #N:I
    .end local v6           #animLayer:I
    .end local v13           #win:Lcom/android/server/wm/WindowState;
    .end local v14           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_13c
    if-eqz v18, :cond_138

    #@13e
    .line 621
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/wm/DimSurface;->hide()V

    #@141
    goto :goto_138
.end method

.method private updateWindowsLocked(I)V
    .registers 26
    .parameter "displayId"

    #@0
    .prologue
    .line 346
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mAnimTransactionSequence:I

    #@4
    move/from16 v20, v0

    #@6
    add-int/lit8 v20, v20, 0x1

    #@8
    move/from16 v0, v20

    #@a
    move-object/from16 v1, p0

    #@c
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mAnimTransactionSequence:I

    #@e
    .line 348
    invoke-direct/range {p0 .. p1}, Lcom/android/server/wm/WindowAnimator;->getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@11
    move-result-object v20

    #@12
    move-object/from16 v0, v20

    #@14
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWinAnimators:Lcom/android/server/wm/WinAnimatorList;

    #@16
    move-object/from16 v19, v0

    #@18
    .line 350
    .local v19, winAnimatorList:Lcom/android/server/wm/WinAnimatorList;
    const/4 v14, 0x0

    #@19
    .line 351
    .local v14, unForceHiding:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowStateAnimator;>;"
    const/4 v15, 0x0

    #@1a
    .line 352
    .local v15, wallpaperInUnForceHiding:Z
    const/16 v20, 0x0

    #@1c
    move/from16 v0, v20

    #@1e
    move-object/from16 v1, p0

    #@20
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@22
    .line 353
    const/4 v8, 0x0

    #@23
    .line 355
    .local v8, enableForceHidingWhenKeyguardIsShown:Z
    invoke-virtual/range {v19 .. v19}, Lcom/android/server/wm/WinAnimatorList;->size()I

    #@26
    move-result v20

    #@27
    add-int/lit8 v11, v20, -0x1

    #@29
    .local v11, i:I
    :goto_29
    if-ltz v11, :cond_34e

    #@2b
    .line 356
    move-object/from16 v0, v19

    #@2d
    invoke-virtual {v0, v11}, Lcom/android/server/wm/WinAnimatorList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v18

    #@31
    check-cast v18, Lcom/android/server/wm/WindowStateAnimator;

    #@33
    .line 357
    .local v18, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    move-object/from16 v0, v18

    #@35
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@37
    move-object/from16 v17, v0

    #@39
    .line 358
    .local v17, win:Lcom/android/server/wm/WindowState;
    move-object/from16 v0, v18

    #@3b
    iget v9, v0, Lcom/android/server/wm/WindowStateAnimator;->mAttrFlags:I

    #@3d
    .line 360
    .local v9, flags:I
    move-object/from16 v0, v18

    #@3f
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@41
    move-object/from16 v20, v0

    #@43
    if-eqz v20, :cond_115

    #@45
    .line 361
    move-object/from16 v0, v18

    #@47
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWasAnimating:Z

    #@49
    move/from16 v16, v0

    #@4b
    .line 362
    .local v16, wasAnimating:Z
    move-object/from16 v0, p0

    #@4d
    iget-wide v0, v0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@4f
    move-wide/from16 v20, v0

    #@51
    move-object/from16 v0, v18

    #@53
    move-wide/from16 v1, v20

    #@55
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowStateAnimator;->stepAnimationLocked(J)Z

    #@58
    move-result v12

    #@59
    .line 369
    .local v12, nowAnimating:Z
    if-eqz v16, :cond_a1

    #@5b
    move-object/from16 v0, v18

    #@5d
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@5f
    move/from16 v20, v0

    #@61
    if-nez v20, :cond_a1

    #@63
    move-object/from16 v0, p0

    #@65
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@67
    move-object/from16 v20, v0

    #@69
    move-object/from16 v0, v20

    #@6b
    move-object/from16 v1, v17

    #@6d
    if-ne v0, v1, :cond_a1

    #@6f
    .line 370
    move-object/from16 v0, p0

    #@71
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@73
    move/from16 v20, v0

    #@75
    or-int/lit8 v20, v20, 0x2

    #@77
    move/from16 v0, v20

    #@79
    move-object/from16 v1, p0

    #@7b
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@7d
    .line 371
    const/16 v20, 0x0

    #@7f
    const/16 v21, 0x4

    #@81
    move-object/from16 v0, p0

    #@83
    move/from16 v1, v20

    #@85
    move/from16 v2, v21

    #@87
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowAnimator;->setPendingLayoutChanges(II)V

    #@8a
    .line 374
    move-object/from16 v0, p0

    #@8c
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@8e
    move-object/from16 v20, v0

    #@90
    const-string v21, "updateWindowsAndWallpaperLocked 2"

    #@92
    move-object/from16 v0, p0

    #@94
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@96
    move-object/from16 v22, v0

    #@98
    const/16 v23, 0x0

    #@9a
    invoke-virtual/range {v22 .. v23}, Landroid/util/SparseIntArray;->get(I)I

    #@9d
    move-result v22

    #@9e
    invoke-virtual/range {v20 .. v22}, Lcom/android/server/wm/WindowManagerService;->debugLayoutRepeats(Ljava/lang/String;I)V

    #@a1
    .line 379
    :cond_a1
    move-object/from16 v0, p0

    #@a3
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@a5
    move-object/from16 v20, v0

    #@a7
    move-object/from16 v0, v17

    #@a9
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@ab
    move-object/from16 v21, v0

    #@ad
    move-object/from16 v0, v20

    #@af
    move-object/from16 v1, v17

    #@b1
    move-object/from16 v2, v21

    #@b3
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManagerPolicy;->doesForceHide(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    #@b6
    move-result v20

    #@b7
    if-eqz v20, :cond_230

    #@b9
    .line 380
    if-nez v16, :cond_fd

    #@bb
    if-eqz v12, :cond_fd

    #@bd
    .line 384
    move-object/from16 v0, p0

    #@bf
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@c1
    move/from16 v20, v0

    #@c3
    or-int/lit8 v20, v20, 0x4

    #@c5
    move/from16 v0, v20

    #@c7
    move-object/from16 v1, p0

    #@c9
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@cb
    .line 385
    const/16 v20, 0x4

    #@cd
    move-object/from16 v0, p0

    #@cf
    move/from16 v1, p1

    #@d1
    move/from16 v2, v20

    #@d3
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowAnimator;->setPendingLayoutChanges(II)V

    #@d6
    .line 388
    move-object/from16 v0, p0

    #@d8
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@da
    move-object/from16 v20, v0

    #@dc
    const-string v21, "updateWindowsAndWallpaperLocked 3"

    #@de
    move-object/from16 v0, p0

    #@e0
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@e2
    move-object/from16 v22, v0

    #@e4
    move-object/from16 v0, v22

    #@e6
    move/from16 v1, p1

    #@e8
    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    #@eb
    move-result v22

    #@ec
    invoke-virtual/range {v20 .. v22}, Lcom/android/server/wm/WindowManagerService;->debugLayoutRepeats(Ljava/lang/String;I)V

    #@ef
    .line 391
    move-object/from16 v0, p0

    #@f1
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f3
    move-object/from16 v20, v0

    #@f5
    const/16 v21, 0x1

    #@f7
    move/from16 v0, v21

    #@f9
    move-object/from16 v1, v20

    #@fb
    iput-boolean v0, v1, Lcom/android/server/wm/WindowManagerService;->mFocusMayChange:Z

    #@fd
    .line 393
    :cond_fd
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/wm/WindowState;->isReadyForDisplay()Z

    #@100
    move-result v20

    #@101
    if-eqz v20, :cond_115

    #@103
    .line 394
    if-eqz v12, :cond_1b3

    #@105
    .line 395
    move-object/from16 v0, v18

    #@107
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimationIsEntrance:Z

    #@109
    move/from16 v20, v0

    #@10b
    if-eqz v20, :cond_1a9

    #@10d
    .line 396
    const/16 v20, 0x1

    #@10f
    move/from16 v0, v20

    #@111
    move-object/from16 v1, p0

    #@113
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@115
    .line 487
    .end local v12           #nowAnimating:Z
    .end local v16           #wasAnimating:Z
    :cond_115
    :goto_115
    move-object/from16 v0, v17

    #@117
    iget-object v6, v0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@119
    .line 488
    .local v6, atoken:Lcom/android/server/wm/AppWindowToken;
    move-object/from16 v0, v18

    #@11b
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@11d
    move/from16 v20, v0

    #@11f
    const/16 v21, 0x3

    #@121
    move/from16 v0, v20

    #@123
    move/from16 v1, v21

    #@125
    if-ne v0, v1, :cond_15f

    #@127
    .line 489
    if-eqz v6, :cond_12f

    #@129
    iget-boolean v0, v6, Lcom/android/server/wm/AppWindowToken;->allDrawn:Z

    #@12b
    move/from16 v20, v0

    #@12d
    if-eqz v20, :cond_15f

    #@12f
    .line 490
    :cond_12f
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/wm/WindowStateAnimator;->performShowLocked()Z

    #@132
    move-result v20

    #@133
    if-eqz v20, :cond_15f

    #@135
    .line 491
    move-object/from16 v0, p0

    #@137
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@139
    move-object/from16 v20, v0

    #@13b
    const/16 v21, 0x8

    #@13d
    move-object/from16 v0, v20

    #@13f
    move/from16 v1, p1

    #@141
    move/from16 v2, v21

    #@143
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    #@146
    .line 494
    move-object/from16 v0, p0

    #@148
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@14a
    move-object/from16 v20, v0

    #@14c
    const-string v21, "updateWindowsAndWallpaperLocked 5"

    #@14e
    move-object/from16 v0, p0

    #@150
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@152
    move-object/from16 v22, v0

    #@154
    move-object/from16 v0, v22

    #@156
    move/from16 v1, p1

    #@158
    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    #@15b
    move-result v22

    #@15c
    invoke-virtual/range {v20 .. v22}, Lcom/android/server/wm/WindowManagerService;->debugLayoutRepeats(Ljava/lang/String;I)V

    #@15f
    .line 500
    :cond_15f
    move-object/from16 v0, v18

    #@161
    iget-object v4, v0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@163
    .line 501
    .local v4, appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    if-eqz v4, :cond_1a5

    #@165
    iget-object v0, v4, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@167
    move-object/from16 v20, v0

    #@169
    if-eqz v20, :cond_1a5

    #@16b
    .line 502
    iget v0, v4, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransactionSeq:I

    #@16d
    move/from16 v20, v0

    #@16f
    move-object/from16 v0, p0

    #@171
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mAnimTransactionSequence:I

    #@173
    move/from16 v21, v0

    #@175
    move/from16 v0, v20

    #@177
    move/from16 v1, v21

    #@179
    if-eq v0, v1, :cond_18b

    #@17b
    .line 503
    move-object/from16 v0, p0

    #@17d
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mAnimTransactionSequence:I

    #@17f
    move/from16 v20, v0

    #@181
    move/from16 v0, v20

    #@183
    iput v0, v4, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransactionSeq:I

    #@185
    .line 504
    const/16 v20, 0x0

    #@187
    move/from16 v0, v20

    #@189
    iput v0, v4, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@18b
    .line 506
    :cond_18b
    iget v0, v4, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@18d
    move/from16 v20, v0

    #@18f
    move-object/from16 v0, v18

    #@191
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@193
    move/from16 v21, v0

    #@195
    move/from16 v0, v20

    #@197
    move/from16 v1, v21

    #@199
    if-ge v0, v1, :cond_1a5

    #@19b
    .line 507
    move-object/from16 v0, v18

    #@19d
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@19f
    move/from16 v20, v0

    #@1a1
    move/from16 v0, v20

    #@1a3
    iput v0, v4, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@1a5
    .line 355
    .end local v4           #appAnimator:Lcom/android/server/wm/AppWindowAnimator;
    .end local v6           #atoken:Lcom/android/server/wm/AppWindowToken;
    :cond_1a5
    add-int/lit8 v11, v11, -0x1

    #@1a7
    goto/16 :goto_29

    #@1a9
    .line 398
    .restart local v12       #nowAnimating:Z
    .restart local v16       #wasAnimating:Z
    :cond_1a9
    const/16 v20, 0x3

    #@1ab
    move/from16 v0, v20

    #@1ad
    move-object/from16 v1, p0

    #@1af
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@1b1
    goto/16 :goto_115

    #@1b3
    .line 401
    :cond_1b3
    const/16 v20, 0x2

    #@1b5
    move/from16 v0, v20

    #@1b7
    move-object/from16 v1, p0

    #@1b9
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@1bb
    .line 411
    sget-boolean v20, Lcom/lge/config/ConfigBuildFlags;->CAPP_LOCKSCREEN:Z

    #@1bd
    if-eqz v20, :cond_22d

    #@1bf
    .line 412
    move-object/from16 v0, p0

    #@1c1
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1c3
    move-object/from16 v20, v0

    #@1c5
    invoke-virtual/range {v20 .. v20}, Lcom/android/server/wm/WindowManagerService;->getOrientationFromAppTokensLocked()I

    #@1c8
    move-result v5

    #@1c9
    .line 414
    .local v5, appOrientation:I
    move-object/from16 v0, p0

    #@1cb
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1cd
    move-object/from16 v20, v0

    #@1cf
    move-object/from16 v0, v20

    #@1d1
    iget v0, v0, Lcom/android/server/wm/WindowManagerService;->mRotation:I

    #@1d3
    move/from16 v20, v0

    #@1d5
    const/16 v21, 0x1

    #@1d7
    move/from16 v0, v20

    #@1d9
    move/from16 v1, v21

    #@1db
    if-eq v0, v1, :cond_1f1

    #@1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1e1
    move-object/from16 v20, v0

    #@1e3
    move-object/from16 v0, v20

    #@1e5
    iget v0, v0, Lcom/android/server/wm/WindowManagerService;->mRotation:I

    #@1e7
    move/from16 v20, v0

    #@1e9
    const/16 v21, 0x3

    #@1eb
    move/from16 v0, v20

    #@1ed
    move/from16 v1, v21

    #@1ef
    if-ne v0, v1, :cond_22b

    #@1f1
    :cond_1f1
    const/4 v13, 0x1

    #@1f2
    .line 415
    .local v13, rotated:Z
    :goto_1f2
    if-nez v13, :cond_202

    #@1f4
    if-eqz v5, :cond_228

    #@1f6
    const/16 v20, 0x8

    #@1f8
    move/from16 v0, v20

    #@1fa
    if-eq v5, v0, :cond_228

    #@1fc
    const/16 v20, 0x6

    #@1fe
    move/from16 v0, v20

    #@200
    if-eq v5, v0, :cond_228

    #@202
    :cond_202
    if-eqz v13, :cond_216

    #@204
    const/16 v20, 0x1

    #@206
    move/from16 v0, v20

    #@208
    if-eq v5, v0, :cond_228

    #@20a
    const/16 v20, 0x9

    #@20c
    move/from16 v0, v20

    #@20e
    if-eq v5, v0, :cond_228

    #@210
    const/16 v20, 0x7

    #@212
    move/from16 v0, v20

    #@214
    if-eq v5, v0, :cond_228

    #@216
    :cond_216
    move-object/from16 v0, v17

    #@218
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@21a
    move-object/from16 v20, v0

    #@21c
    move-object/from16 v0, v20

    #@21e
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@220
    move/from16 v20, v0

    #@222
    const/high16 v21, 0x2

    #@224
    and-int v20, v20, v21

    #@226
    if-nez v20, :cond_115

    #@228
    .line 420
    :cond_228
    const/4 v8, 0x1

    #@229
    goto/16 :goto_115

    #@22b
    .line 414
    .end local v13           #rotated:Z
    :cond_22b
    const/4 v13, 0x0

    #@22c
    goto :goto_1f2

    #@22d
    .line 423
    .end local v5           #appOrientation:I
    :cond_22d
    const/4 v8, 0x1

    #@22e
    goto/16 :goto_115

    #@230
    .line 436
    :cond_230
    move-object/from16 v0, p0

    #@232
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@234
    move-object/from16 v20, v0

    #@236
    move-object/from16 v0, v17

    #@238
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@23a
    move-object/from16 v21, v0

    #@23c
    move-object/from16 v0, v20

    #@23e
    move-object/from16 v1, v17

    #@240
    move-object/from16 v2, v21

    #@242
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManagerPolicy;->canBeForceHidden(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    #@245
    move-result v20

    #@246
    if-eqz v20, :cond_115

    #@248
    .line 438
    move-object/from16 v0, v17

    #@24a
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@24c
    move-object/from16 v20, v0

    #@24e
    if-eqz v20, :cond_26a

    #@250
    move-object/from16 v0, v17

    #@252
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@254
    move-object/from16 v20, v0

    #@256
    move-object/from16 v0, v20

    #@258
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@25a
    move-object/from16 v20, v0

    #@25c
    move-object/from16 v0, v20

    #@25e
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@260
    move/from16 v20, v0

    #@262
    const/16 v21, 0x7d4

    #@264
    move/from16 v0, v20

    #@266
    move/from16 v1, v21

    #@268
    if-eq v0, v1, :cond_1a5

    #@26a
    .line 443
    :cond_26a
    move-object/from16 v0, v18

    #@26c
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAttrFlags:I

    #@26e
    move/from16 v20, v0

    #@270
    const/high16 v21, 0x8

    #@272
    and-int v20, v20, v21

    #@274
    if-nez v20, :cond_2e9

    #@276
    const/4 v10, 0x1

    #@277
    .line 446
    .local v10, hideWhenLocked:Z
    :goto_277
    move-object/from16 v0, p0

    #@279
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@27b
    move/from16 v20, v0

    #@27d
    const/16 v21, 0x1

    #@27f
    move/from16 v0, v20

    #@281
    move/from16 v1, v21

    #@283
    if-ne v0, v1, :cond_28d

    #@285
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/wm/WindowStateAnimator;->isAnimating()Z

    #@288
    move-result v20

    #@289
    if-eqz v20, :cond_29f

    #@28b
    if-nez v10, :cond_29f

    #@28d
    :cond_28d
    move-object/from16 v0, p0

    #@28f
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mForceHiding:I

    #@291
    move/from16 v20, v0

    #@293
    const/16 v21, 0x2

    #@295
    move/from16 v0, v20

    #@297
    move/from16 v1, v21

    #@299
    if-ne v0, v1, :cond_2eb

    #@29b
    if-eqz v10, :cond_2eb

    #@29d
    if-eqz v8, :cond_2eb

    #@29f
    .line 449
    :cond_29f
    const/16 v20, 0x0

    #@2a1
    const/16 v21, 0x0

    #@2a3
    move-object/from16 v0, v17

    #@2a5
    move/from16 v1, v20

    #@2a7
    move/from16 v2, v21

    #@2a9
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowState;->hideLw(ZZ)Z

    #@2ac
    move-result v7

    #@2ad
    .line 475
    .local v7, changed:Z
    :cond_2ad
    :goto_2ad
    if-eqz v7, :cond_115

    #@2af
    const/high16 v20, 0x10

    #@2b1
    and-int v20, v20, v9

    #@2b3
    if-eqz v20, :cond_115

    #@2b5
    .line 476
    move-object/from16 v0, p0

    #@2b7
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@2b9
    move/from16 v20, v0

    #@2bb
    or-int/lit8 v20, v20, 0x2

    #@2bd
    move/from16 v0, v20

    #@2bf
    move-object/from16 v1, p0

    #@2c1
    iput v0, v1, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@2c3
    .line 477
    const/16 v20, 0x0

    #@2c5
    const/16 v21, 0x4

    #@2c7
    move-object/from16 v0, p0

    #@2c9
    move/from16 v1, v20

    #@2cb
    move/from16 v2, v21

    #@2cd
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowAnimator;->setPendingLayoutChanges(II)V

    #@2d0
    .line 480
    move-object/from16 v0, p0

    #@2d2
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2d4
    move-object/from16 v20, v0

    #@2d6
    const-string v21, "updateWindowsAndWallpaperLocked 4"

    #@2d8
    move-object/from16 v0, p0

    #@2da
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@2dc
    move-object/from16 v22, v0

    #@2de
    const/16 v23, 0x0

    #@2e0
    invoke-virtual/range {v22 .. v23}, Landroid/util/SparseIntArray;->get(I)I

    #@2e3
    move-result v22

    #@2e4
    invoke-virtual/range {v20 .. v22}, Lcom/android/server/wm/WindowManagerService;->debugLayoutRepeats(Ljava/lang/String;I)V

    #@2e7
    goto/16 :goto_115

    #@2e9
    .line 443
    .end local v7           #changed:Z
    .end local v10           #hideWhenLocked:Z
    :cond_2e9
    const/4 v10, 0x0

    #@2ea
    goto :goto_277

    #@2eb
    .line 453
    .restart local v10       #hideWhenLocked:Z
    :cond_2eb
    const/16 v20, 0x0

    #@2ed
    const/16 v21, 0x0

    #@2ef
    move-object/from16 v0, v17

    #@2f1
    move/from16 v1, v20

    #@2f3
    move/from16 v2, v21

    #@2f5
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowState;->showLw(ZZ)Z

    #@2f8
    move-result v7

    #@2f9
    .line 456
    .restart local v7       #changed:Z
    if-eqz v7, :cond_2ad

    #@2fb
    .line 457
    move-object/from16 v0, p0

    #@2fd
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@2ff
    move/from16 v20, v0

    #@301
    and-int/lit8 v20, v20, 0x4

    #@303
    if-eqz v20, :cond_31e

    #@305
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/wm/WindowState;->isVisibleNow()Z

    #@308
    move-result v20

    #@309
    if-eqz v20, :cond_31e

    #@30b
    .line 459
    if-nez v14, :cond_312

    #@30d
    .line 460
    new-instance v14, Ljava/util/ArrayList;

    #@30f
    .end local v14           #unForceHiding:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowStateAnimator;>;"
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    #@312
    .line 462
    .restart local v14       #unForceHiding:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowStateAnimator;>;"
    :cond_312
    move-object/from16 v0, v18

    #@314
    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@317
    .line 463
    const/high16 v20, 0x10

    #@319
    and-int v20, v20, v9

    #@31b
    if-eqz v20, :cond_31e

    #@31d
    .line 464
    const/4 v15, 0x1

    #@31e
    .line 467
    :cond_31e
    move-object/from16 v0, p0

    #@320
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    #@322
    move-object/from16 v20, v0

    #@324
    if-eqz v20, :cond_33e

    #@326
    move-object/from16 v0, p0

    #@328
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    #@32a
    move-object/from16 v20, v0

    #@32c
    move-object/from16 v0, v20

    #@32e
    iget v0, v0, Lcom/android/server/wm/WindowState;->mLayer:I

    #@330
    move/from16 v20, v0

    #@332
    move-object/from16 v0, v17

    #@334
    iget v0, v0, Lcom/android/server/wm/WindowState;->mLayer:I

    #@336
    move/from16 v21, v0

    #@338
    move/from16 v0, v20

    #@33a
    move/from16 v1, v21

    #@33c
    if-ge v0, v1, :cond_2ad

    #@33e
    .line 471
    :cond_33e
    move-object/from16 v0, p0

    #@340
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@342
    move-object/from16 v20, v0

    #@344
    const/16 v21, 0x1

    #@346
    move/from16 v0, v21

    #@348
    move-object/from16 v1, v20

    #@34a
    iput-boolean v0, v1, Lcom/android/server/wm/WindowManagerService;->mFocusMayChange:Z

    #@34c
    goto/16 :goto_2ad

    #@34e
    .line 514
    .end local v7           #changed:Z
    .end local v9           #flags:I
    .end local v10           #hideWhenLocked:Z
    .end local v12           #nowAnimating:Z
    .end local v16           #wasAnimating:Z
    .end local v17           #win:Lcom/android/server/wm/WindowState;
    .end local v18           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_34e
    if-eqz v14, :cond_37c

    #@350
    .line 515
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@353
    move-result v20

    #@354
    add-int/lit8 v11, v20, -0x1

    #@356
    :goto_356
    if-ltz v11, :cond_37c

    #@358
    .line 516
    move-object/from16 v0, p0

    #@35a
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@35c
    move-object/from16 v20, v0

    #@35e
    move-object/from16 v0, v20

    #@360
    invoke-interface {v0, v15}, Landroid/view/WindowManagerPolicy;->createForceHideEnterAnimation(Z)Landroid/view/animation/Animation;

    #@363
    move-result-object v3

    #@364
    .line 517
    .local v3, a:Landroid/view/animation/Animation;
    if-eqz v3, :cond_379

    #@366
    .line 518
    invoke-virtual {v14, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@369
    move-result-object v18

    #@36a
    check-cast v18, Lcom/android/server/wm/WindowStateAnimator;

    #@36c
    .line 519
    .restart local v18       #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    move-object/from16 v0, v18

    #@36e
    invoke-virtual {v0, v3}, Lcom/android/server/wm/WindowStateAnimator;->setAnimation(Landroid/view/animation/Animation;)V

    #@371
    .line 520
    const/16 v20, 0x1

    #@373
    move/from16 v0, v20

    #@375
    move-object/from16 v1, v18

    #@377
    iput-boolean v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mAnimationIsEntrance:Z

    #@379
    .line 515
    .end local v18           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_379
    add-int/lit8 v11, v11, -0x1

    #@37b
    goto :goto_356

    #@37c
    .line 524
    .end local v3           #a:Landroid/view/animation/Animation;
    :cond_37c
    return-void
.end method


# virtual methods
.method addDisplayLocked(I)V
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowAnimator;->getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@3
    .line 150
    if-nez p1, :cond_8

    #@5
    .line 151
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/server/wm/WindowAnimator;->mInitialized:Z

    #@8
    .line 153
    :cond_8
    return-void
.end method

.method clearPendingActions()V
    .registers 2

    #@0
    .prologue
    .line 945
    monitor-enter p0

    #@1
    .line 946
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput v0, p0, Lcom/android/server/wm/WindowAnimator;->mPendingActions:I

    #@4
    .line 947
    monitor-exit p0

    #@5
    .line 948
    return-void

    #@6
    .line 947
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method

.method public dumpLocked(Ljava/io/PrintWriter;Ljava/lang/String;Z)V
    .registers 15
    .parameter "pw"
    .parameter "prefix"
    .parameter "dumpAll"

    #@0
    .prologue
    .line 825
    new-instance v9, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v10, "  "

    #@7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v9

    #@b
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v9

    #@f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    .line 826
    .local v5, subPrefix:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v10, "  "

    #@1a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v9

    #@1e
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v9

    #@22
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v6

    #@26
    .line 828
    .local v6, subSubPrefix:Ljava/lang/String;
    const/4 v4, 0x0

    #@27
    .line 829
    .local v4, needSep:Z
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@2c
    move-result v9

    #@2d
    if-lez v9, :cond_6b

    #@2f
    .line 830
    const/4 v4, 0x1

    #@30
    .line 831
    const-string v9, "  App Animators:"

    #@32
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@35
    .line 832
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v9

    #@3b
    add-int/lit8 v2, v9, -0x1

    #@3d
    .local v2, i:I
    :goto_3d
    if-ltz v2, :cond_6b

    #@3f
    .line 833
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mAppAnimators:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, Lcom/android/server/wm/AppWindowAnimator;

    #@47
    .line 834
    .local v0, anim:Lcom/android/server/wm/AppWindowAnimator;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    const-string v9, "App Animator #"

    #@4c
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4f
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@52
    .line 835
    const/16 v9, 0x20

    #@54
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(C)V

    #@57
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@5a
    .line 836
    if-eqz p3, :cond_67

    #@5c
    .line 837
    const/16 v9, 0x3a

    #@5e
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(C)V

    #@61
    .line 838
    invoke-virtual {v0, p1, v5, p3}, Lcom/android/server/wm/AppWindowAnimator;->dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V

    #@64
    .line 832
    :goto_64
    add-int/lit8 v2, v2, -0x1

    #@66
    goto :goto_3d

    #@67
    .line 840
    :cond_67
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@6a
    goto :goto_64

    #@6b
    .line 844
    .end local v0           #anim:Lcom/android/server/wm/AppWindowAnimator;
    .end local v2           #i:I
    :cond_6b
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTokens:Ljava/util/ArrayList;

    #@6d
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@70
    move-result v9

    #@71
    if-lez v9, :cond_b7

    #@73
    .line 845
    if-eqz v4, :cond_78

    #@75
    .line 846
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@78
    .line 848
    :cond_78
    const/4 v4, 0x1

    #@79
    .line 849
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7c
    const-string v9, "Wallpaper tokens:"

    #@7e
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@81
    .line 850
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTokens:Ljava/util/ArrayList;

    #@83
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@86
    move-result v9

    #@87
    add-int/lit8 v2, v9, -0x1

    #@89
    .restart local v2       #i:I
    :goto_89
    if-ltz v2, :cond_b7

    #@8b
    .line 851
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTokens:Ljava/util/ArrayList;

    #@8d
    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@90
    move-result-object v7

    #@91
    check-cast v7, Lcom/android/server/wm/WindowToken;

    #@93
    .line 852
    .local v7, token:Lcom/android/server/wm/WindowToken;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@96
    const-string v9, "Wallpaper #"

    #@98
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9b
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@9e
    .line 853
    const/16 v9, 0x20

    #@a0
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(C)V

    #@a3
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@a6
    .line 854
    if-eqz p3, :cond_b3

    #@a8
    .line 855
    const/16 v9, 0x3a

    #@aa
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(C)V

    #@ad
    .line 856
    invoke-virtual {v7, p1, v5}, Lcom/android/server/wm/WindowToken;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@b0
    .line 850
    :goto_b0
    add-int/lit8 v2, v2, -0x1

    #@b2
    goto :goto_89

    #@b3
    .line 858
    :cond_b3
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@b6
    goto :goto_b0

    #@b7
    .line 863
    .end local v2           #i:I
    .end local v7           #token:Lcom/android/server/wm/WindowToken;
    :cond_b7
    if-eqz v4, :cond_bc

    #@b9
    .line 864
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@bc
    .line 866
    :cond_bc
    const/4 v2, 0x0

    #@bd
    .restart local v2       #i:I
    :goto_bd
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@bf
    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    #@c2
    move-result v9

    #@c3
    if-ge v2, v9, :cond_183

    #@c5
    .line 867
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c8
    const-string v9, "DisplayContentsAnimator #"

    #@ca
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cd
    .line 868
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@cf
    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->keyAt(I)I

    #@d2
    move-result v9

    #@d3
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@d6
    .line 869
    const-string v9, ":"

    #@d8
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@db
    .line 870
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@dd
    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@e0
    move-result-object v1

    #@e1
    check-cast v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@e3
    .line 871
    .local v1, displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    const/4 v3, 0x0

    #@e4
    .local v3, j:I
    :goto_e4
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWinAnimators:Lcom/android/server/wm/WinAnimatorList;

    #@e6
    invoke-virtual {v9}, Lcom/android/server/wm/WinAnimatorList;->size()I

    #@e9
    move-result v9

    #@ea
    if-ge v3, v9, :cond_10a

    #@ec
    .line 872
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWinAnimators:Lcom/android/server/wm/WinAnimatorList;

    #@ee
    invoke-virtual {v9, v3}, Lcom/android/server/wm/WinAnimatorList;->get(I)Ljava/lang/Object;

    #@f1
    move-result-object v8

    #@f2
    check-cast v8, Lcom/android/server/wm/WindowStateAnimator;

    #@f4
    .line 873
    .local v8, wanim:Lcom/android/server/wm/WindowStateAnimator;
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f7
    const-string v9, "Window #"

    #@f9
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fc
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V

    #@ff
    .line 874
    const-string v9, ": "

    #@101
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@104
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@107
    .line 871
    add-int/lit8 v3, v3, 0x1

    #@109
    goto :goto_e4

    #@10a
    .line 876
    .end local v8           #wanim:Lcom/android/server/wm/WindowStateAnimator;
    :cond_10a
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWindowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;

    #@10c
    if-eqz v9, :cond_123

    #@10e
    .line 877
    if-nez p3, :cond_116

    #@110
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWindowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;

    #@112
    iget-boolean v9, v9, Lcom/android/server/wm/DimSurface;->mDimShown:Z

    #@114
    if-eqz v9, :cond_123

    #@116
    .line 878
    :cond_116
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@119
    const-string v9, "mWindowAnimationBackgroundSurface:"

    #@11b
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@11e
    .line 879
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWindowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;

    #@120
    invoke-virtual {v9, v6, p1}, Lcom/android/server/wm/DimSurface;->printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@123
    .line 882
    :cond_123
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimAnimator:Lcom/android/server/wm/DimAnimator;

    #@125
    if-eqz v9, :cond_162

    #@127
    .line 883
    if-nez p3, :cond_12f

    #@129
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimAnimator:Lcom/android/server/wm/DimAnimator;

    #@12b
    iget-boolean v9, v9, Lcom/android/server/wm/DimAnimator;->mDimShown:Z

    #@12d
    if-eqz v9, :cond_13c

    #@12f
    .line 884
    :cond_12f
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@132
    const-string v9, "mDimAnimator:"

    #@134
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@137
    .line 885
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimAnimator:Lcom/android/server/wm/DimAnimator;

    #@139
    invoke-virtual {v9, v6, p1}, Lcom/android/server/wm/DimAnimator;->printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@13c
    .line 890
    :cond_13c
    :goto_13c
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@13e
    if-eqz v9, :cond_16d

    #@140
    .line 891
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@143
    const-string v9, "mDimParams:"

    #@145
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@148
    .line 892
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@14a
    invoke-virtual {v9, v6, p1}, Lcom/android/server/wm/DimAnimator$Parameters;->printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@14d
    .line 896
    :cond_14d
    :goto_14d
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@14f
    if-eqz v9, :cond_178

    #@151
    .line 897
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@154
    const-string v9, "mScreenRotationAnimation:"

    #@156
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@159
    .line 898
    iget-object v9, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@15b
    invoke-virtual {v9, v6, p1}, Lcom/android/server/wm/ScreenRotationAnimation;->printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@15e
    .line 866
    :cond_15e
    :goto_15e
    add-int/lit8 v2, v2, 0x1

    #@160
    goto/16 :goto_bd

    #@162
    .line 887
    :cond_162
    if-eqz p3, :cond_13c

    #@164
    .line 888
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@167
    const-string v9, "no DimAnimator "

    #@169
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16c
    goto :goto_13c

    #@16d
    .line 893
    :cond_16d
    if-eqz p3, :cond_14d

    #@16f
    .line 894
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@172
    const-string v9, "no DimParams "

    #@174
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@177
    goto :goto_14d

    #@178
    .line 899
    :cond_178
    if-eqz p3, :cond_15e

    #@17a
    .line 900
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17d
    const-string v9, "no ScreenRotationAnimation "

    #@17f
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@182
    goto :goto_15e

    #@183
    .line 904
    .end local v1           #displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    .end local v3           #j:I
    :cond_183
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@186
    .line 906
    if-eqz p3, :cond_1dd

    #@188
    .line 907
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18b
    const-string v9, "mAnimTransactionSequence="

    #@18d
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@190
    .line 908
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mAnimTransactionSequence:I

    #@192
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@195
    .line 909
    const-string v9, " mForceHiding="

    #@197
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19a
    invoke-direct {p0}, Lcom/android/server/wm/WindowAnimator;->forceHidingToString()Ljava/lang/String;

    #@19d
    move-result-object v9

    #@19e
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a1
    .line 910
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a4
    const-string v9, "mCurrentTime="

    #@1a6
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a9
    .line 911
    iget-wide v9, p0, Lcom/android/server/wm/WindowAnimator;->mCurrentTime:J

    #@1ab
    invoke-static {v9, v10}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@1ae
    move-result-object v9

    #@1af
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b2
    .line 912
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b5
    const-string v9, "mDw="

    #@1b7
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ba
    .line 913
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mDw:I

    #@1bc
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@1bf
    const-string v9, " mDh="

    #@1c1
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c4
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mDh:I

    #@1c6
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@1c9
    .line 914
    const-string v9, " mInnerDw="

    #@1cb
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ce
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDw:I

    #@1d0
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@1d3
    .line 915
    const-string v9, " mInnerDh="

    #@1d5
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d8
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDh:I

    #@1da
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(I)V

    #@1dd
    .line 917
    :cond_1dd
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@1df
    if-eqz v9, :cond_1fb

    #@1e1
    .line 918
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e4
    const-string v9, "mBulkUpdateParams=0x"

    #@1e6
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e9
    .line 919
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@1eb
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1ee
    move-result-object v9

    #@1ef
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f2
    .line 920
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@1f4
    invoke-static {v9}, Lcom/android/server/wm/WindowAnimator;->bulkUpdateParamsToString(I)Ljava/lang/String;

    #@1f7
    move-result-object v9

    #@1f8
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1fb
    .line 922
    :cond_1fb
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mPendingActions:I

    #@1fd
    if-eqz v9, :cond_210

    #@1ff
    .line 923
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@202
    const-string v9, "mPendingActions=0x"

    #@204
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@207
    .line 924
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mPendingActions:I

    #@209
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@20c
    move-result-object v9

    #@20d
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@210
    .line 926
    :cond_210
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@212
    if-eqz v9, :cond_221

    #@214
    .line 927
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@217
    const-string v9, "mWindowDetachedWallpaper="

    #@219
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21c
    .line 928
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@21e
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@221
    .line 930
    :cond_221
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@224
    const-string v9, "mWallpaperTarget="

    #@226
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@229
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@22b
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@22e
    .line 931
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@231
    const-string v9, "mWpAppAnimator="

    #@233
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@236
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mWpAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@238
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@23b
    .line 932
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@23d
    if-nez v9, :cond_243

    #@23f
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mUpperWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@241
    if-eqz v9, :cond_25d

    #@243
    .line 933
    :cond_243
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@246
    const-string v9, "mLowerWallpaperTarget="

    #@248
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24b
    .line 934
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@24d
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@250
    .line 935
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@253
    const-string v9, "mUpperWallpaperTarget="

    #@255
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@258
    .line 936
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mUpperWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@25a
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@25d
    .line 938
    :cond_25d
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@25f
    if-eqz v9, :cond_278

    #@261
    .line 939
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@264
    const-string v9, "mUniverseBackground="

    #@266
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@269
    iget-object v9, p0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@26b
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@26e
    .line 940
    const-string v9, " mAboveUniverseLayer="

    #@270
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@273
    iget v9, p0, Lcom/android/server/wm/WindowAnimator;->mAboveUniverseLayer:I

    #@275
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(I)V

    #@278
    .line 942
    :cond_278
    return-void
.end method

.method getScreenRotationAnimationLocked(I)Lcom/android/server/wm/ScreenRotationAnimation;
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 985
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowAnimator;->getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@6
    return-object v0
.end method

.method hideWallpapersLocked(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;Ljava/util/ArrayList;)V
    .registers 14
    .parameter "w"
    .parameter "wallpaperTarget"
    .parameter "lowerWallpaperTarget"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/WindowState;",
            "Lcom/android/server/wm/WindowState;",
            "Lcom/android/server/wm/WindowState;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/wm/WindowToken;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p4, wallpaperTokens:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowToken;>;"
    const/4 v8, 0x0

    #@1
    .line 286
    if-ne p2, p1, :cond_5

    #@3
    if-eqz p3, :cond_7

    #@5
    :cond_5
    if-nez p2, :cond_42

    #@7
    .line 287
    :cond_7
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    .line 288
    .local v2, numTokens:I
    add-int/lit8 v0, v2, -0x1

    #@d
    .local v0, i:I
    :goto_d
    if-ltz v0, :cond_42

    #@f
    .line 289
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v4

    #@13
    check-cast v4, Lcom/android/server/wm/WindowToken;

    #@15
    .line 290
    .local v4, token:Lcom/android/server/wm/WindowToken;
    iget-object v7, v4, Lcom/android/server/wm/WindowToken;->windows:Lcom/android/server/wm/WindowList;

    #@17
    invoke-virtual {v7}, Lcom/android/server/wm/WindowList;->size()I

    #@1a
    move-result v3

    #@1b
    .line 291
    .local v3, numWindows:I
    add-int/lit8 v1, v3, -0x1

    #@1d
    .local v1, j:I
    :goto_1d
    if-ltz v1, :cond_3c

    #@1f
    .line 292
    iget-object v7, v4, Lcom/android/server/wm/WindowToken;->windows:Lcom/android/server/wm/WindowList;

    #@21
    invoke-virtual {v7, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    check-cast v5, Lcom/android/server/wm/WindowState;

    #@27
    .line 293
    .local v5, wallpaper:Lcom/android/server/wm/WindowState;
    iget-object v6, v5, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@29
    .line 294
    .local v6, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    iget-boolean v7, v6, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@2b
    if-nez v7, :cond_39

    #@2d
    .line 295
    invoke-virtual {v6}, Lcom/android/server/wm/WindowStateAnimator;->hide()V

    #@30
    .line 296
    iget-object v7, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@32
    invoke-virtual {v7, v5, v8}, Lcom/android/server/wm/WindowManagerService;->dispatchWallpaperVisibility(Lcom/android/server/wm/WindowState;Z)V

    #@35
    .line 297
    const/4 v7, 0x4

    #@36
    invoke-virtual {p0, v8, v7}, Lcom/android/server/wm/WindowAnimator;->setPendingLayoutChanges(II)V

    #@39
    .line 291
    :cond_39
    add-int/lit8 v1, v1, -0x1

    #@3b
    goto :goto_1d

    #@3c
    .line 305
    .end local v5           #wallpaper:Lcom/android/server/wm/WindowState;
    .end local v6           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_3c
    const/4 v7, 0x1

    #@3d
    iput-boolean v7, v4, Lcom/android/server/wm/WindowToken;->hidden:Z

    #@3f
    .line 288
    add-int/lit8 v0, v0, -0x1

    #@41
    goto :goto_d

    #@42
    .line 308
    .end local v0           #i:I
    .end local v1           #j:I
    .end local v2           #numTokens:I
    .end local v3           #numWindows:I
    .end local v4           #token:Lcom/android/server/wm/WindowToken;
    :cond_42
    return-void
.end method

.method hideWallpapersLocked(Lcom/android/server/wm/WindowState;Z)V
    .registers 6
    .parameter "w"
    .parameter "fromAnimator"

    #@0
    .prologue
    .line 276
    if-eqz p2, :cond_c

    #@2
    .line 277
    iget-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@4
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@6
    iget-object v2, p0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTokens:Ljava/util/ArrayList;

    #@8
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/server/wm/WindowAnimator;->hideWallpapersLocked(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;Ljava/util/ArrayList;)V

    #@b
    .line 282
    :goto_b
    return-void

    #@c
    .line 279
    :cond_c
    iget-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@e
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@10
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@12
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@14
    iget-object v2, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@16
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mWallpaperTokens:Ljava/util/ArrayList;

    #@18
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/server/wm/WindowAnimator;->hideWallpapersLocked(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;Ljava/util/ArrayList;)V

    #@1b
    goto :goto_b
.end method

.method isDimmingLocked(I)Z
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 795
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowAnimator;->getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method isDimmingLocked(Lcom/android/server/wm/WindowStateAnimator;)Z
    .registers 4
    .parameter "winAnimator"

    #@0
    .prologue
    .line 799
    iget-object v1, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@2
    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getDisplayId()I

    #@5
    move-result v1

    #@6
    invoke-direct {p0, v1}, Lcom/android/server/wm/WindowAnimator;->getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@9
    move-result-object v1

    #@a
    iget-object v0, v1, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimParams:Lcom/android/server/wm/DimAnimator$Parameters;

    #@c
    .line 801
    .local v0, dimParams:Lcom/android/server/wm/DimAnimator$Parameters;
    if-eqz v0, :cond_14

    #@e
    iget-object v1, v0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@10
    if-ne v1, p1, :cond_14

    #@12
    const/4 v1, 0x1

    #@13
    :goto_13
    return v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method removeDisplayLocked(I)V
    .registers 5
    .parameter "displayId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 156
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@9
    .line 157
    .local v0, displayAnimator:Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;
    if-eqz v0, :cond_2c

    #@b
    .line 158
    iget-object v1, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWindowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;

    #@d
    if-eqz v1, :cond_16

    #@f
    .line 159
    iget-object v1, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWindowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;

    #@11
    invoke-virtual {v1}, Lcom/android/server/wm/DimSurface;->kill()V

    #@14
    .line 160
    iput-object v2, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mWindowAnimationBackgroundSurface:Lcom/android/server/wm/DimSurface;

    #@16
    .line 162
    :cond_16
    iget-object v1, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@18
    if-eqz v1, :cond_21

    #@1a
    .line 163
    iget-object v1, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@1c
    invoke-virtual {v1}, Lcom/android/server/wm/ScreenRotationAnimation;->kill()V

    #@1f
    .line 164
    iput-object v2, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@21
    .line 166
    :cond_21
    iget-object v1, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimAnimator:Lcom/android/server/wm/DimAnimator;

    #@23
    if-eqz v1, :cond_2c

    #@25
    .line 167
    iget-object v1, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimAnimator:Lcom/android/server/wm/DimAnimator;

    #@27
    invoke-virtual {v1}, Lcom/android/server/wm/DimAnimator;->kill()V

    #@2a
    .line 168
    iput-object v2, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mDimAnimator:Lcom/android/server/wm/DimAnimator;

    #@2c
    .line 172
    :cond_2c
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mDisplayContentsAnimators:Landroid/util/SparseArray;

    #@2e
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    #@31
    .line 173
    return-void
.end method

.method setAppLayoutChanges(Lcom/android/server/wm/AppWindowAnimator;ILjava/lang/String;)V
    .registers 10
    .parameter "appAnimator"
    .parameter "changes"
    .parameter "s"

    #@0
    .prologue
    .line 956
    new-instance v1, Landroid/util/SparseIntArray;

    #@2
    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    #@5
    .line 957
    .local v1, displays:Landroid/util/SparseIntArray;
    iget-object v4, p1, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v4

    #@b
    add-int/lit8 v2, v4, -0x1

    #@d
    .local v2, i:I
    :goto_d
    if-ltz v2, :cond_39

    #@f
    .line 958
    iget-object v4, p1, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Lcom/android/server/wm/WindowStateAnimator;

    #@17
    .line 959
    .local v3, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    iget-object v4, v3, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@19
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@1b
    invoke-virtual {v4}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    #@1e
    move-result v0

    #@1f
    .line 960
    .local v0, displayId:I
    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    #@22
    move-result v4

    #@23
    if-gez v4, :cond_36

    #@25
    .line 961
    invoke-virtual {p0, v0, p2}, Lcom/android/server/wm/WindowAnimator;->setPendingLayoutChanges(II)V

    #@28
    .line 963
    iget-object v4, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2a
    iget-object v5, p0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@2c
    invoke-virtual {v5, v0}, Landroid/util/SparseIntArray;->get(I)I

    #@2f
    move-result v5

    #@30
    invoke-virtual {v4, p3, v5}, Lcom/android/server/wm/WindowManagerService;->debugLayoutRepeats(Ljava/lang/String;I)V

    #@33
    .line 966
    invoke-virtual {v1, v0, p2}, Landroid/util/SparseIntArray;->put(II)V

    #@36
    .line 957
    :cond_36
    add-int/lit8 v2, v2, -0x1

    #@38
    goto :goto_d

    #@39
    .line 969
    .end local v0           #displayId:I
    .end local v3           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_39
    return-void
.end method

.method setCurrentFocus(Lcom/android/server/wm/WindowState;)V
    .registers 2
    .parameter "currentFocus"

    #@0
    .prologue
    .line 783
    iput-object p1, p0, Lcom/android/server/wm/WindowAnimator;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    #@2
    .line 784
    return-void
.end method

.method setDisplayDimensions(IIII)V
    .registers 5
    .parameter "curWidth"
    .parameter "curHeight"
    .parameter "appWidth"
    .parameter "appHeight"

    #@0
    .prologue
    .line 788
    iput p1, p0, Lcom/android/server/wm/WindowAnimator;->mDw:I

    #@2
    .line 789
    iput p2, p0, Lcom/android/server/wm/WindowAnimator;->mDh:I

    #@4
    .line 790
    iput p3, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDw:I

    #@6
    .line 791
    iput p4, p0, Lcom/android/server/wm/WindowAnimator;->mInnerDh:I

    #@8
    .line 792
    return-void
.end method

.method setPendingLayoutChanges(II)V
    .registers 5
    .parameter "displayId"
    .parameter "changes"

    #@0
    .prologue
    .line 951
    iget-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@2
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@4
    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    #@7
    move-result v1

    #@8
    or-int/2addr v1, p2

    #@9
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    #@c
    .line 952
    return-void
.end method

.method setScreenRotationAnimationLocked(ILcom/android/server/wm/ScreenRotationAnimation;)V
    .registers 4
    .parameter "displayId"
    .parameter "animation"

    #@0
    .prologue
    .line 981
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowAnimator;->getDisplayContentsAnimatorLocked(I)Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;

    #@3
    move-result-object v0

    #@4
    iput-object p2, v0, Lcom/android/server/wm/WindowAnimator$DisplayContentsAnimator;->mScreenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;

    #@6
    .line 982
    return-void
.end method

.method updateAnimToLayoutLocked()V
    .registers 5

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Lcom/android/server/wm/WindowAnimator;->mAnimToLayout:Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;

    #@2
    .line 178
    .local v0, animToLayout:Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;
    monitor-enter v0

    #@3
    .line 179
    :try_start_3
    iget v1, p0, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@5
    iput v1, v0, Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;->mBulkUpdateParams:I

    #@7
    .line 180
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@9
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clone()Landroid/util/SparseIntArray;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@f
    .line 181
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@11
    iput-object v1, v0, Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@13
    .line 183
    iget-boolean v1, v0, Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;->mUpdateQueued:Z

    #@15
    if-nez v1, :cond_2b

    #@17
    .line 184
    const/4 v1, 0x1

    #@18
    iput-boolean v1, v0, Lcom/android/server/wm/WindowAnimator$AnimatorToLayoutParams;->mUpdateQueued:Z

    #@1a
    .line 185
    iget-object v1, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1c
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@1e
    iget-object v2, p0, Lcom/android/server/wm/WindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@20
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@22
    const/16 v3, 0x19

    #@24
    invoke-virtual {v2, v3}, Lcom/android/server/wm/WindowManagerService$H;->obtainMessage(I)Landroid/os/Message;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService$H;->sendMessage(Landroid/os/Message;)Z

    #@2b
    .line 187
    :cond_2b
    monitor-exit v0

    #@2c
    .line 188
    return-void

    #@2d
    .line 187
    :catchall_2d
    move-exception v1

    #@2e
    monitor-exit v0
    :try_end_2f
    .catchall {:try_start_3 .. :try_end_2f} :catchall_2d

    #@2f
    throw v1
.end method
