.class Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;
.super Ljava/lang/Object;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhoneTypeWindowOrderHelper"
.end annotation


# instance fields
.field private lgWindowList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mLayer_diff:I

.field mTotal_Layer:I

.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;

.field private titleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 4820
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4776
    const/4 v1, 0x0

    #@6
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->mTotal_Layer:I

    #@8
    .line 4777
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v1, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->titleList:Ljava/util/ArrayList;

    #@f
    .line 4778
    new-instance v1, Ljava/util/HashMap;

    #@11
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@14
    iput-object v1, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->lgWindowList:Ljava/util/HashMap;

    #@16
    .line 4822
    :try_start_16
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->windOrderXMLparser()I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->mTotal_Layer:I
    :try_end_1c
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_16 .. :try_end_1c} :catch_24
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_1c} :catch_2d

    #@1c
    .line 4829
    :goto_1c
    const/16 v1, 0x2710

    #@1e
    iget v2, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->mTotal_Layer:I

    #@20
    div-int/2addr v1, v2

    #@21
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->mLayer_diff:I

    #@23
    .line 4830
    return-void

    #@24
    .line 4823
    :catch_24
    move-exception v0

    #@25
    .line 4824
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v1, "WindowManager"

    #@27
    const-string v2, "Exception error!!! XML pull parser is not available.."

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_1c

    #@2d
    .line 4825
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2d
    move-exception v0

    #@2e
    .line 4826
    .local v0, e:Ljava/io/IOException;
    const-string v1, "WindowManager"

    #@30
    const-string v2, "Exception error!!! XML file io error.."

    #@32
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_1c
.end method

.method private windOrderXMLparser()I
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    const/4 v9, 0x2

    #@2
    .line 4782
    const/4 v3, 0x0

    #@3
    .line 4783
    .local v3, windowNum:I
    const/4 v1, 0x0

    #@4
    .line 4784
    .local v1, total:I
    iget-object v5, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v5

    #@c
    const v6, 0x10f0004

    #@f
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@12
    move-result-object v4

    #@13
    .line 4785
    .local v4, xrp:Landroid/content/res/XmlResourceParser;
    if-nez v4, :cond_1e

    #@15
    .line 4786
    const-string v5, "lgwinorder"

    #@17
    const-string v6, "xml file may be invalid.. check xml.. id = com.android.internal.R.xml.lgwinorder"

    #@19
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 4787
    const/4 v5, 0x0

    #@1d
    .line 4817
    :goto_1d
    return v5

    #@1e
    .line 4790
    :cond_1e
    const-string v2, ""

    #@20
    .line 4791
    .local v2, windowName:Ljava/lang/String;
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getEventType()I

    #@23
    move-result v0

    #@24
    .line 4792
    .local v0, eventType:I
    :goto_24
    const/4 v5, 0x1

    #@25
    if-eq v0, v5, :cond_67

    #@27
    .line 4793
    if-nez v0, :cond_35

    #@29
    .line 4794
    const-string v5, "lgwinorder"

    #@2b
    const-string v6, "lgwinorder Start of lgwinorder xml document"

    #@2d
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 4815
    :cond_30
    :goto_30
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    #@33
    move-result v0

    #@34
    goto :goto_24

    #@35
    .line 4795
    :cond_35
    if-ne v0, v9, :cond_69

    #@37
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    const-string v6, "window"

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v5

    #@41
    if-eqz v5, :cond_69

    #@43
    .line 4796
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    #@46
    move-result v0

    #@47
    .line 4797
    if-ne v0, v10, :cond_60

    #@49
    .line 4798
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    const-string v6, " "

    #@4f
    const-string v7, ""

    #@51
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    const-string v6, "\n"

    #@57
    const-string v7, ""

    #@59
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    .line 4803
    add-int/lit8 v3, v3, 0x1

    #@5f
    goto :goto_30

    #@60
    .line 4800
    :cond_60
    const-string v5, "lgwinorder"

    #@62
    const-string v6, "error!!! lgwinorder.xml file must be wrongly written.."

    #@64
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    :cond_67
    :goto_67
    move v5, v3

    #@68
    .line 4817
    goto :goto_1d

    #@69
    .line 4804
    :cond_69
    if-ne v0, v9, :cond_30

    #@6b
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@6e
    move-result-object v5

    #@6f
    const-string v6, "title"

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v5

    #@75
    if-eqz v5, :cond_30

    #@77
    .line 4805
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    #@7a
    move-result v0

    #@7b
    .line 4806
    if-ne v0, v10, :cond_b0

    #@7d
    .line 4807
    iget-object v5, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->titleList:Ljava/util/ArrayList;

    #@7f
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    #@82
    move-result-object v6

    #@83
    const-string v7, " "

    #@85
    const-string v8, ""

    #@87
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@8a
    move-result-object v6

    #@8b
    const-string v7, "\n"

    #@8d
    const-string v8, ""

    #@8f
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@92
    move-result-object v6

    #@93
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@96
    .line 4808
    iget-object v5, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->lgWindowList:Ljava/util/HashMap;

    #@98
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    #@9b
    move-result-object v6

    #@9c
    const-string v7, " "

    #@9e
    const-string v8, ""

    #@a0
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@a3
    move-result-object v6

    #@a4
    const-string v7, "\n"

    #@a6
    const-string v8, ""

    #@a8
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@ab
    move-result-object v6

    #@ac
    invoke-virtual {v5, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@af
    goto :goto_30

    #@b0
    .line 4810
    :cond_b0
    const-string v5, "lgwinorder"

    #@b2
    const-string v6, "error!!! lgwinorder.xml file must be wrongly written.."

    #@b4
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 4811
    const/4 v3, 0x0

    #@b8
    .line 4812
    goto :goto_67
.end method


# virtual methods
.method public getSecondaryOffset(Landroid/view/WindowManager$LayoutParams;)I
    .registers 6
    .parameter "lp"

    #@0
    .prologue
    .line 4833
    const/4 v1, 0x0

    #@1
    .line 4835
    .local v1, numLayer:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->titleList:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    if-ge v0, v2, :cond_21

    #@a
    .line 4836
    invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->titleList:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Ljava/lang/CharSequence;

    #@1a
    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_25

    #@20
    .line 4837
    move v1, v0

    #@21
    .line 4842
    :cond_21
    iget v2, p0, Lcom/android/server/wm/WindowManagerService$PhoneTypeWindowOrderHelper;->mLayer_diff:I

    #@23
    mul-int/2addr v2, v1

    #@24
    return v2

    #@25
    .line 4835
    :cond_25
    add-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_2
.end method
