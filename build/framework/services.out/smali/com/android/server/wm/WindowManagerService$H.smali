.class final Lcom/android/server/wm/WindowManagerService$H;
.super Landroid/os/Handler;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "H"
.end annotation


# static fields
.field public static final ADD_STARTING:I = 0x5

.field public static final ANIMATOR_WHAT_OFFSET:I = 0x186a0

.field public static final APP_FREEZE_TIMEOUT:I = 0x11

.field public static final APP_TRANSITION_TIMEOUT:I = 0xd

.field public static final BOOT_TIMEOUT:I = 0x17

.field public static final CLEAR_PENDING_ACTIONS:I = 0x186a2

.field public static final CLIENT_FREEZE_TIMEOUT:I = 0x23

.field public static final DO_ANIMATION_CALLBACK:I = 0x1b

.field public static final DO_DISPLAY_ADDED:I = 0x20

.field public static final DO_DISPLAY_CHANGED:I = 0x22

.field public static final DO_DISPLAY_REMOVED:I = 0x21

.field public static final DO_TRAVERSAL:I = 0x4

.field public static final DRAG_END_TIMEOUT:I = 0x15

.field public static final DRAG_START_TIMEOUT:I = 0x14

.field public static final ENABLE_SCREEN:I = 0x10

.field public static final FINISHED_STARTING:I = 0x7

.field public static final FORCE_GC:I = 0xf

.field public static final MOVE_FOCUSED_SCREEN_TASK_TO_FRONT:I = 0x63

.field public static final NOTIFY_RECTANGLE_ON_SCREEN_REQUESTED:I = 0x1e

.field public static final NOTIFY_ROTATION_CHANGED:I = 0x1c

.field public static final NOTIFY_WINDOW_LAYERS_CHANGED:I = 0x1f

.field public static final NOTIFY_WINDOW_TRANSITION:I = 0x1d

.field public static final PERSIST_ANIMATION_SCALE:I = 0xe

.field public static final REMOVE_STARTING:I = 0x6

.field public static final REPORT_APPLICATION_TOKEN_DRAWN:I = 0x9

.field public static final REPORT_APPLICATION_TOKEN_WINDOWS:I = 0x8

.field public static final REPORT_FOCUS_CHANGE:I = 0x2

.field public static final REPORT_HARD_KEYBOARD_STATUS_CHANGE:I = 0x16

.field public static final REPORT_LOSING_FOCUS:I = 0x3

.field public static final REPORT_WINDOWS_CHANGE:I = 0x13

.field public static final SEND_NEW_CONFIGURATION:I = 0x12

.field public static final SET_TRANSPARENT_REGION:I = 0x186a1

.field public static final SHOW_STRICT_MODE_VIOLATION:I = 0x1a

.field public static final UPDATE_ANIM_PARAMETERS:I = 0x19

.field public static final WAITING_FOR_DRAWN_TIMEOUT:I = 0x18

.field public static final WINDOW_FREEZE_TIMEOUT:I = 0xb


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8829
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 8830
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 40
    .parameter "msg"

    #@0
    .prologue
    .line 8837
    move-object/from16 v0, p1

    #@2
    iget v2, v0, Landroid/os/Message;->what:I

    #@4
    sparse-switch v2, :sswitch_data_75e

    #@7
    .line 9364
    :cond_7
    :goto_7
    return-void

    #@8
    .line 8842
    :sswitch_8
    move-object/from16 v0, p0

    #@a
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@c
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@e
    monitor-enter v3

    #@f
    .line 8843
    :try_start_f
    move-object/from16 v0, p0

    #@11
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@13
    iget-object v0, v2, Lcom/android/server/wm/WindowManagerService;->mLastFocus:Lcom/android/server/wm/WindowState;

    #@15
    move-object/from16 v19, v0

    #@17
    .line 8844
    .local v19, lastFocus:Lcom/android/server/wm/WindowState;
    move-object/from16 v0, p0

    #@19
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@1b
    iget-object v0, v2, Lcom/android/server/wm/WindowManagerService;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    #@1d
    move-object/from16 v21, v0

    #@1f
    .line 8845
    .local v21, newFocus:Lcom/android/server/wm/WindowState;
    move-object/from16 v0, v19

    #@21
    move-object/from16 v1, v21

    #@23
    if-ne v0, v1, :cond_2a

    #@25
    .line 8847
    monitor-exit v3

    #@26
    goto :goto_7

    #@27
    .line 8858
    .end local v19           #lastFocus:Lcom/android/server/wm/WindowState;
    .end local v21           #newFocus:Lcom/android/server/wm/WindowState;
    :catchall_27
    move-exception v2

    #@28
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_f .. :try_end_29} :catchall_27

    #@29
    throw v2

    #@2a
    .line 8849
    .restart local v19       #lastFocus:Lcom/android/server/wm/WindowState;
    .restart local v21       #newFocus:Lcom/android/server/wm/WindowState;
    :cond_2a
    :try_start_2a
    move-object/from16 v0, p0

    #@2c
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2e
    move-object/from16 v0, v21

    #@30
    iput-object v0, v2, Lcom/android/server/wm/WindowManagerService;->mLastFocus:Lcom/android/server/wm/WindowState;

    #@32
    .line 8852
    if-eqz v21, :cond_49

    #@34
    if-eqz v19, :cond_49

    #@36
    invoke-virtual/range {v21 .. v21}, Lcom/android/server/wm/WindowState;->isDisplayedLw()Z

    #@39
    move-result v2

    #@3a
    if-nez v2, :cond_49

    #@3c
    .line 8855
    move-object/from16 v0, p0

    #@3e
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@40
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mLosingFocus:Ljava/util/ArrayList;

    #@42
    move-object/from16 v0, v19

    #@44
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@47
    .line 8856
    const/16 v19, 0x0

    #@49
    .line 8858
    :cond_49
    monitor-exit v3
    :try_end_4a
    .catchall {:try_start_2a .. :try_end_4a} :catchall_27

    #@4a
    .line 8860
    move-object/from16 v0, v19

    #@4c
    move-object/from16 v1, v21

    #@4e
    if-eq v0, v1, :cond_7

    #@50
    .line 8863
    if-eqz v21, :cond_67

    #@52
    .line 8866
    :try_start_52
    move-object/from16 v0, v21

    #@54
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@56
    const/4 v3, 0x1

    #@57
    move-object/from16 v0, p0

    #@59
    iget-object v4, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@5b
    iget-boolean v4, v4, Lcom/android/server/wm/WindowManagerService;->mInTouchMode:Z

    #@5d
    invoke-interface {v2, v3, v4}, Landroid/view/IWindow;->windowFocusChanged(ZZ)V
    :try_end_60
    .catch Landroid/os/RemoteException; {:try_start_52 .. :try_end_60} :catch_75b

    #@60
    .line 8870
    :goto_60
    move-object/from16 v0, p0

    #@62
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@64
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$500(Lcom/android/server/wm/WindowManagerService;)V

    #@67
    .line 8873
    :cond_67
    if-eqz v19, :cond_7

    #@69
    .line 8876
    :try_start_69
    move-object/from16 v0, v19

    #@6b
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@6d
    const/4 v3, 0x0

    #@6e
    move-object/from16 v0, p0

    #@70
    iget-object v4, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@72
    iget-boolean v4, v4, Lcom/android/server/wm/WindowManagerService;->mInTouchMode:Z

    #@74
    invoke-interface {v2, v3, v4}, Landroid/view/IWindow;->windowFocusChanged(ZZ)V
    :try_end_77
    .catch Landroid/os/RemoteException; {:try_start_69 .. :try_end_77} :catch_78

    #@77
    goto :goto_7

    #@78
    .line 8877
    :catch_78
    move-exception v2

    #@79
    goto :goto_7

    #@7a
    .line 8887
    .end local v19           #lastFocus:Lcom/android/server/wm/WindowState;
    .end local v21           #newFocus:Lcom/android/server/wm/WindowState;
    :sswitch_7a
    move-object/from16 v0, p0

    #@7c
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@7e
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@80
    monitor-enter v3

    #@81
    .line 8888
    :try_start_81
    move-object/from16 v0, p0

    #@83
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@85
    iget-object v0, v2, Lcom/android/server/wm/WindowManagerService;->mLosingFocus:Ljava/util/ArrayList;

    #@87
    move-object/from16 v20, v0

    #@89
    .line 8889
    .local v20, losers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowState;>;"
    move-object/from16 v0, p0

    #@8b
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@8d
    new-instance v4, Ljava/util/ArrayList;

    #@8f
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@92
    iput-object v4, v2, Lcom/android/server/wm/WindowManagerService;->mLosingFocus:Ljava/util/ArrayList;

    #@94
    .line 8890
    monitor-exit v3
    :try_end_95
    .catchall {:try_start_81 .. :try_end_95} :catchall_b8

    #@95
    .line 8892
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    #@98
    move-result v11

    #@99
    .line 8893
    .local v11, N:I
    const/16 v16, 0x0

    #@9b
    .local v16, i:I
    :goto_9b
    move/from16 v0, v16

    #@9d
    if-ge v0, v11, :cond_7

    #@9f
    .line 8896
    :try_start_9f
    move-object/from16 v0, v20

    #@a1
    move/from16 v1, v16

    #@a3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a6
    move-result-object v2

    #@a7
    check-cast v2, Lcom/android/server/wm/WindowState;

    #@a9
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@ab
    const/4 v3, 0x0

    #@ac
    move-object/from16 v0, p0

    #@ae
    iget-object v4, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@b0
    iget-boolean v4, v4, Lcom/android/server/wm/WindowManagerService;->mInTouchMode:Z

    #@b2
    invoke-interface {v2, v3, v4}, Landroid/view/IWindow;->windowFocusChanged(ZZ)V
    :try_end_b5
    .catch Landroid/os/RemoteException; {:try_start_9f .. :try_end_b5} :catch_758

    #@b5
    .line 8893
    :goto_b5
    add-int/lit8 v16, v16, 0x1

    #@b7
    goto :goto_9b

    #@b8
    .line 8890
    .end local v11           #N:I
    .end local v16           #i:I
    .end local v20           #losers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowState;>;"
    :catchall_b8
    move-exception v2

    #@b9
    :try_start_b9
    monitor-exit v3
    :try_end_ba
    .catchall {:try_start_b9 .. :try_end_ba} :catchall_b8

    #@ba
    throw v2

    #@bb
    .line 8904
    :sswitch_bb
    move-object/from16 v0, p0

    #@bd
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@bf
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@c1
    monitor-enter v3

    #@c2
    .line 8905
    :try_start_c2
    move-object/from16 v0, p0

    #@c4
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@c6
    const/4 v4, 0x0

    #@c7
    iput-boolean v4, v2, Lcom/android/server/wm/WindowManagerService;->mTraversalScheduled:Z

    #@c9
    .line 8906
    move-object/from16 v0, p0

    #@cb
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@cd
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$600(Lcom/android/server/wm/WindowManagerService;)V

    #@d0
    .line 8907
    monitor-exit v3

    #@d1
    goto/16 :goto_7

    #@d3
    :catchall_d3
    move-exception v2

    #@d4
    monitor-exit v3
    :try_end_d5
    .catchall {:try_start_c2 .. :try_end_d5} :catchall_d3

    #@d5
    throw v2

    #@d6
    .line 8911
    :sswitch_d6
    move-object/from16 v0, p1

    #@d8
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@da
    move-object/from16 v37, v0

    #@dc
    check-cast v37, Lcom/android/server/wm/AppWindowToken;

    #@de
    .line 8912
    .local v37, wtoken:Lcom/android/server/wm/AppWindowToken;
    move-object/from16 v0, v37

    #@e0
    iget-object v0, v0, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@e2
    move-object/from16 v28, v0

    #@e4
    .line 8914
    .local v28, sd:Lcom/android/server/wm/StartingData;
    if-eqz v28, :cond_7

    #@e6
    .line 8922
    const/16 v32, 0x0

    #@e8
    .line 8924
    .local v32, view:Landroid/view/View;
    :try_start_e8
    move-object/from16 v0, p0

    #@ea
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@ec
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@ee
    move-object/from16 v0, v37

    #@f0
    iget-object v3, v0, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@f2
    move-object/from16 v0, v28

    #@f4
    iget-object v4, v0, Lcom/android/server/wm/StartingData;->pkg:Ljava/lang/String;

    #@f6
    move-object/from16 v0, v28

    #@f8
    iget v5, v0, Lcom/android/server/wm/StartingData;->theme:I

    #@fa
    move-object/from16 v0, v28

    #@fc
    iget-object v6, v0, Lcom/android/server/wm/StartingData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@fe
    move-object/from16 v0, v28

    #@100
    iget-object v7, v0, Lcom/android/server/wm/StartingData;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@102
    move-object/from16 v0, v28

    #@104
    iget v8, v0, Lcom/android/server/wm/StartingData;->labelRes:I

    #@106
    move-object/from16 v0, v28

    #@108
    iget v9, v0, Lcom/android/server/wm/StartingData;->icon:I

    #@10a
    move-object/from16 v0, v28

    #@10c
    iget v10, v0, Lcom/android/server/wm/StartingData;->windowFlags:I

    #@10e
    invoke-interface/range {v2 .. v10}, Landroid/view/WindowManagerPolicy;->addStartingWindow(Landroid/os/IBinder;Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;III)Landroid/view/View;
    :try_end_111
    .catch Ljava/lang/Exception; {:try_start_e8 .. :try_end_111} :catch_157

    #@111
    move-result-object v32

    #@112
    .line 8931
    :goto_112
    if-eqz v32, :cond_7

    #@114
    .line 8932
    const/4 v12, 0x0

    #@115
    .line 8934
    .local v12, abort:Z
    move-object/from16 v0, p0

    #@117
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@119
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@11b
    monitor-enter v3

    #@11c
    .line 8935
    :try_start_11c
    move-object/from16 v0, v37

    #@11e
    iget-boolean v2, v0, Lcom/android/server/wm/AppWindowToken;->removed:Z

    #@120
    if-nez v2, :cond_128

    #@122
    move-object/from16 v0, v37

    #@124
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@126
    if-nez v2, :cond_160

    #@128
    .line 8938
    :cond_128
    move-object/from16 v0, v37

    #@12a
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@12c
    if-eqz v2, :cond_139

    #@12e
    .line 8943
    const/4 v2, 0x0

    #@12f
    move-object/from16 v0, v37

    #@131
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@133
    .line 8944
    const/4 v2, 0x0

    #@134
    move-object/from16 v0, v37

    #@136
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@138
    .line 8945
    const/4 v12, 0x1

    #@139
    .line 8955
    :cond_139
    :goto_139
    monitor-exit v3
    :try_end_13a
    .catchall {:try_start_11c .. :try_end_13a} :catchall_167

    #@13a
    .line 8957
    if-eqz v12, :cond_7

    #@13c
    .line 8959
    :try_start_13c
    move-object/from16 v0, p0

    #@13e
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@140
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@142
    move-object/from16 v0, v37

    #@144
    iget-object v3, v0, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@146
    move-object/from16 v0, v32

    #@148
    invoke-interface {v2, v3, v0}, Landroid/view/WindowManagerPolicy;->removeStartingWindow(Landroid/os/IBinder;Landroid/view/View;)V
    :try_end_14b
    .catch Ljava/lang/Exception; {:try_start_13c .. :try_end_14b} :catch_14d

    #@14b
    goto/16 :goto_7

    #@14d
    .line 8960
    :catch_14d
    move-exception v15

    #@14e
    .line 8961
    .local v15, e:Ljava/lang/Exception;
    const-string v2, "WindowManager"

    #@150
    const-string v3, "Exception when removing starting window"

    #@152
    invoke-static {v2, v3, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@155
    goto/16 :goto_7

    #@157
    .line 8927
    .end local v12           #abort:Z
    .end local v15           #e:Ljava/lang/Exception;
    :catch_157
    move-exception v15

    #@158
    .line 8928
    .restart local v15       #e:Ljava/lang/Exception;
    const-string v2, "WindowManager"

    #@15a
    const-string v3, "Exception when adding starting window"

    #@15c
    invoke-static {v2, v3, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15f
    goto :goto_112

    #@160
    .line 8948
    .end local v15           #e:Ljava/lang/Exception;
    .restart local v12       #abort:Z
    :cond_160
    :try_start_160
    move-object/from16 v0, v32

    #@162
    move-object/from16 v1, v37

    #@164
    iput-object v0, v1, Lcom/android/server/wm/AppWindowToken;->startingView:Landroid/view/View;

    #@166
    goto :goto_139

    #@167
    .line 8955
    :catchall_167
    move-exception v2

    #@168
    monitor-exit v3
    :try_end_169
    .catchall {:try_start_160 .. :try_end_169} :catchall_167

    #@169
    throw v2

    #@16a
    .line 8968
    .end local v12           #abort:Z
    .end local v28           #sd:Lcom/android/server/wm/StartingData;
    .end local v32           #view:Landroid/view/View;
    .end local v37           #wtoken:Lcom/android/server/wm/AppWindowToken;
    :sswitch_16a
    move-object/from16 v0, p1

    #@16c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16e
    move-object/from16 v37, v0

    #@170
    check-cast v37, Lcom/android/server/wm/AppWindowToken;

    #@172
    .line 8969
    .restart local v37       #wtoken:Lcom/android/server/wm/AppWindowToken;
    const/16 v30, 0x0

    #@174
    .line 8970
    .local v30, token:Landroid/os/IBinder;
    const/16 v32, 0x0

    #@176
    .line 8971
    .restart local v32       #view:Landroid/view/View;
    move-object/from16 v0, p0

    #@178
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@17a
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@17c
    monitor-enter v3

    #@17d
    .line 8976
    :try_start_17d
    move-object/from16 v0, v37

    #@17f
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@181
    if-eqz v2, :cond_1a3

    #@183
    .line 8977
    move-object/from16 v0, v37

    #@185
    iget-object v0, v0, Lcom/android/server/wm/AppWindowToken;->startingView:Landroid/view/View;

    #@187
    move-object/from16 v32, v0

    #@189
    .line 8978
    move-object/from16 v0, v37

    #@18b
    iget-object v0, v0, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@18d
    move-object/from16 v30, v0

    #@18f
    .line 8979
    const/4 v2, 0x0

    #@190
    move-object/from16 v0, v37

    #@192
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@194
    .line 8980
    const/4 v2, 0x0

    #@195
    move-object/from16 v0, v37

    #@197
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingView:Landroid/view/View;

    #@199
    .line 8981
    const/4 v2, 0x0

    #@19a
    move-object/from16 v0, v37

    #@19c
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@19e
    .line 8982
    const/4 v2, 0x0

    #@19f
    move-object/from16 v0, v37

    #@1a1
    iput-boolean v2, v0, Lcom/android/server/wm/AppWindowToken;->startingDisplayed:Z

    #@1a3
    .line 8984
    :cond_1a3
    monitor-exit v3
    :try_end_1a4
    .catchall {:try_start_17d .. :try_end_1a4} :catchall_1bf

    #@1a4
    .line 8985
    if-eqz v32, :cond_7

    #@1a6
    .line 8987
    :try_start_1a6
    move-object/from16 v0, p0

    #@1a8
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@1aa
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@1ac
    move-object/from16 v0, v30

    #@1ae
    move-object/from16 v1, v32

    #@1b0
    invoke-interface {v2, v0, v1}, Landroid/view/WindowManagerPolicy;->removeStartingWindow(Landroid/os/IBinder;Landroid/view/View;)V
    :try_end_1b3
    .catch Ljava/lang/Exception; {:try_start_1a6 .. :try_end_1b3} :catch_1b5

    #@1b3
    goto/16 :goto_7

    #@1b5
    .line 8988
    :catch_1b5
    move-exception v15

    #@1b6
    .line 8989
    .restart local v15       #e:Ljava/lang/Exception;
    const-string v2, "WindowManager"

    #@1b8
    const-string v3, "Exception when removing starting window"

    #@1ba
    invoke-static {v2, v3, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1bd
    goto/16 :goto_7

    #@1bf
    .line 8984
    .end local v15           #e:Ljava/lang/Exception;
    :catchall_1bf
    move-exception v2

    #@1c0
    :try_start_1c0
    monitor-exit v3
    :try_end_1c1
    .catchall {:try_start_1c0 .. :try_end_1c1} :catchall_1bf

    #@1c1
    throw v2

    #@1c2
    .line 8995
    .end local v30           #token:Landroid/os/IBinder;
    .end local v32           #view:Landroid/view/View;
    .end local v37           #wtoken:Lcom/android/server/wm/AppWindowToken;
    :sswitch_1c2
    const/16 v30, 0x0

    #@1c4
    .line 8996
    .restart local v30       #token:Landroid/os/IBinder;
    const/16 v32, 0x0

    #@1c6
    .line 8998
    .restart local v32       #view:Landroid/view/View;
    :goto_1c6
    move-object/from16 v0, p0

    #@1c8
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@1ca
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@1cc
    monitor-enter v3

    #@1cd
    .line 8999
    :try_start_1cd
    move-object/from16 v0, p0

    #@1cf
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@1d1
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mFinishedStarting:Ljava/util/ArrayList;

    #@1d3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1d6
    move-result v11

    #@1d7
    .line 9000
    .restart local v11       #N:I
    if-gtz v11, :cond_1df

    #@1d9
    .line 9001
    monitor-exit v3

    #@1da
    goto/16 :goto_7

    #@1dc
    .line 9020
    .end local v11           #N:I
    :catchall_1dc
    move-exception v2

    #@1dd
    monitor-exit v3
    :try_end_1de
    .catchall {:try_start_1cd .. :try_end_1de} :catchall_1dc

    #@1de
    throw v2

    #@1df
    .line 9003
    .restart local v11       #N:I
    :cond_1df
    :try_start_1df
    move-object/from16 v0, p0

    #@1e1
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@1e3
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mFinishedStarting:Ljava/util/ArrayList;

    #@1e5
    add-int/lit8 v4, v11, -0x1

    #@1e7
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1ea
    move-result-object v37

    #@1eb
    check-cast v37, Lcom/android/server/wm/AppWindowToken;

    #@1ed
    .line 9010
    .restart local v37       #wtoken:Lcom/android/server/wm/AppWindowToken;
    move-object/from16 v0, v37

    #@1ef
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@1f1
    if-nez v2, :cond_1f5

    #@1f3
    .line 9011
    monitor-exit v3

    #@1f4
    goto :goto_1c6

    #@1f5
    .line 9014
    :cond_1f5
    move-object/from16 v0, v37

    #@1f7
    iget-object v0, v0, Lcom/android/server/wm/AppWindowToken;->startingView:Landroid/view/View;

    #@1f9
    move-object/from16 v32, v0

    #@1fb
    .line 9015
    move-object/from16 v0, v37

    #@1fd
    iget-object v0, v0, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    #@1ff
    move-object/from16 v30, v0

    #@201
    .line 9016
    const/4 v2, 0x0

    #@202
    move-object/from16 v0, v37

    #@204
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@206
    .line 9017
    const/4 v2, 0x0

    #@207
    move-object/from16 v0, v37

    #@209
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingView:Landroid/view/View;

    #@20b
    .line 9018
    const/4 v2, 0x0

    #@20c
    move-object/from16 v0, v37

    #@20e
    iput-object v2, v0, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@210
    .line 9019
    const/4 v2, 0x0

    #@211
    move-object/from16 v0, v37

    #@213
    iput-boolean v2, v0, Lcom/android/server/wm/AppWindowToken;->startingDisplayed:Z

    #@215
    .line 9020
    monitor-exit v3
    :try_end_216
    .catchall {:try_start_1df .. :try_end_216} :catchall_1dc

    #@216
    .line 9023
    :try_start_216
    move-object/from16 v0, p0

    #@218
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@21a
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@21c
    move-object/from16 v0, v30

    #@21e
    move-object/from16 v1, v32

    #@220
    invoke-interface {v2, v0, v1}, Landroid/view/WindowManagerPolicy;->removeStartingWindow(Landroid/os/IBinder;Landroid/view/View;)V
    :try_end_223
    .catch Ljava/lang/Exception; {:try_start_216 .. :try_end_223} :catch_224

    #@223
    goto :goto_1c6

    #@224
    .line 9024
    :catch_224
    move-exception v15

    #@225
    .line 9025
    .restart local v15       #e:Ljava/lang/Exception;
    const-string v2, "WindowManager"

    #@227
    const-string v3, "Exception when removing starting window"

    #@229
    invoke-static {v2, v3, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22c
    goto :goto_1c6

    #@22d
    .line 9031
    .end local v11           #N:I
    .end local v15           #e:Ljava/lang/Exception;
    .end local v30           #token:Landroid/os/IBinder;
    .end local v32           #view:Landroid/view/View;
    .end local v37           #wtoken:Lcom/android/server/wm/AppWindowToken;
    :sswitch_22d
    move-object/from16 v0, p1

    #@22f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@231
    move-object/from16 v37, v0

    #@233
    check-cast v37, Lcom/android/server/wm/AppWindowToken;

    #@235
    .line 9036
    .restart local v37       #wtoken:Lcom/android/server/wm/AppWindowToken;
    :try_start_235
    move-object/from16 v0, v37

    #@237
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@239
    invoke-interface {v2}, Landroid/view/IApplicationToken;->windowsDrawn()V
    :try_end_23c
    .catch Landroid/os/RemoteException; {:try_start_235 .. :try_end_23c} :catch_23e

    #@23c
    goto/16 :goto_7

    #@23e
    .line 9037
    :catch_23e
    move-exception v2

    #@23f
    goto/16 :goto_7

    #@241
    .line 9042
    .end local v37           #wtoken:Lcom/android/server/wm/AppWindowToken;
    :sswitch_241
    move-object/from16 v0, p1

    #@243
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@245
    move-object/from16 v37, v0

    #@247
    check-cast v37, Lcom/android/server/wm/AppWindowToken;

    #@249
    .line 9044
    .restart local v37       #wtoken:Lcom/android/server/wm/AppWindowToken;
    move-object/from16 v0, p1

    #@24b
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@24d
    if-eqz v2, :cond_267

    #@24f
    const/16 v23, 0x1

    #@251
    .line 9045
    .local v23, nowVisible:Z
    :goto_251
    move-object/from16 v0, p1

    #@253
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@255
    if-eqz v2, :cond_26a

    #@257
    const/16 v22, 0x1

    #@259
    .line 9052
    .local v22, nowGone:Z
    :goto_259
    if-eqz v23, :cond_26d

    #@25b
    .line 9053
    :try_start_25b
    move-object/from16 v0, v37

    #@25d
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@25f
    invoke-interface {v2}, Landroid/view/IApplicationToken;->windowsVisible()V

    #@262
    goto/16 :goto_7

    #@264
    .line 9057
    :catch_264
    move-exception v2

    #@265
    goto/16 :goto_7

    #@267
    .line 9044
    .end local v22           #nowGone:Z
    .end local v23           #nowVisible:Z
    :cond_267
    const/16 v23, 0x0

    #@269
    goto :goto_251

    #@26a
    .line 9045
    .restart local v23       #nowVisible:Z
    :cond_26a
    const/16 v22, 0x0

    #@26c
    goto :goto_259

    #@26d
    .line 9055
    .restart local v22       #nowGone:Z
    :cond_26d
    move-object/from16 v0, v37

    #@26f
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@271
    invoke-interface {v2}, Landroid/view/IApplicationToken;->windowsGone()V
    :try_end_274
    .catch Landroid/os/RemoteException; {:try_start_25b .. :try_end_274} :catch_264

    #@274
    goto/16 :goto_7

    #@276
    .line 9063
    .end local v22           #nowGone:Z
    .end local v23           #nowVisible:Z
    .end local v37           #wtoken:Lcom/android/server/wm/AppWindowToken;
    :sswitch_276
    move-object/from16 v0, p0

    #@278
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@27a
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@27c
    monitor-enter v3

    #@27d
    .line 9064
    :try_start_27d
    const-string v2, "WindowManager"

    #@27f
    const-string v4, "Window freeze timeout expired."

    #@281
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@284
    .line 9065
    move-object/from16 v0, p0

    #@286
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@288
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getDefaultWindowListLocked()Lcom/android/server/wm/WindowList;

    #@28b
    move-result-object v36

    #@28c
    .line 9066
    .local v36, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual/range {v36 .. v36}, Lcom/android/server/wm/WindowList;->size()I

    #@28f
    move-result v16

    #@290
    .line 9067
    .restart local v16       #i:I
    :cond_290
    :goto_290
    if-lez v16, :cond_2c7

    #@292
    .line 9068
    add-int/lit8 v16, v16, -0x1

    #@294
    .line 9069
    move-object/from16 v0, v36

    #@296
    move/from16 v1, v16

    #@298
    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@29b
    move-result-object v33

    #@29c
    check-cast v33, Lcom/android/server/wm/WindowState;

    #@29e
    .line 9070
    .local v33, w:Lcom/android/server/wm/WindowState;
    move-object/from16 v0, v33

    #@2a0
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@2a2
    if-eqz v2, :cond_290

    #@2a4
    .line 9071
    const/4 v2, 0x0

    #@2a5
    move-object/from16 v0, v33

    #@2a7
    iput-boolean v2, v0, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@2a9
    .line 9072
    const-string v2, "WindowManager"

    #@2ab
    new-instance v4, Ljava/lang/StringBuilder;

    #@2ad
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b0
    const-string v5, "Force clearing orientation change: "

    #@2b2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b5
    move-result-object v4

    #@2b6
    move-object/from16 v0, v33

    #@2b8
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v4

    #@2bc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bf
    move-result-object v4

    #@2c0
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c3
    goto :goto_290

    #@2c4
    .line 9076
    .end local v16           #i:I
    .end local v33           #w:Lcom/android/server/wm/WindowState;
    .end local v36           #windows:Lcom/android/server/wm/WindowList;
    :catchall_2c4
    move-exception v2

    #@2c5
    monitor-exit v3
    :try_end_2c6
    .catchall {:try_start_27d .. :try_end_2c6} :catchall_2c4

    #@2c6
    throw v2

    #@2c7
    .line 9075
    .restart local v16       #i:I
    .restart local v36       #windows:Lcom/android/server/wm/WindowList;
    :cond_2c7
    :try_start_2c7
    move-object/from16 v0, p0

    #@2c9
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2cb
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$600(Lcom/android/server/wm/WindowManagerService;)V

    #@2ce
    .line 9076
    monitor-exit v3
    :try_end_2cf
    .catchall {:try_start_2c7 .. :try_end_2cf} :catchall_2c4

    #@2cf
    goto/16 :goto_7

    #@2d1
    .line 9081
    .end local v16           #i:I
    .end local v36           #windows:Lcom/android/server/wm/WindowList;
    :sswitch_2d1
    move-object/from16 v0, p0

    #@2d3
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2d5
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@2d7
    monitor-enter v3

    #@2d8
    .line 9082
    :try_start_2d8
    move-object/from16 v0, p0

    #@2da
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2dc
    iget v2, v2, Lcom/android/server/wm/WindowManagerService;->mNextAppTransition:I

    #@2de
    const/4 v4, -0x1

    #@2df
    if-eq v2, v4, :cond_30e

    #@2e1
    .line 9085
    move-object/from16 v0, p0

    #@2e3
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2e5
    const/4 v4, 0x1

    #@2e6
    iput-boolean v4, v2, Lcom/android/server/wm/WindowManagerService;->mAppTransitionReady:Z

    #@2e8
    .line 9086
    move-object/from16 v0, p0

    #@2ea
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2ec
    const/4 v4, 0x1

    #@2ed
    iput-boolean v4, v2, Lcom/android/server/wm/WindowManagerService;->mAppTransitionTimeout:Z

    #@2ef
    .line 9087
    move-object/from16 v0, p0

    #@2f1
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2f3
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAnimatingAppTokens:Ljava/util/ArrayList;

    #@2f5
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@2f8
    .line 9088
    move-object/from16 v0, p0

    #@2fa
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2fc
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAnimatingAppTokens:Ljava/util/ArrayList;

    #@2fe
    move-object/from16 v0, p0

    #@300
    iget-object v4, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@302
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mAppTokens:Ljava/util/ArrayList;

    #@304
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@307
    .line 9089
    move-object/from16 v0, p0

    #@309
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@30b
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$600(Lcom/android/server/wm/WindowManagerService;)V

    #@30e
    .line 9091
    :cond_30e
    monitor-exit v3

    #@30f
    goto/16 :goto_7

    #@311
    :catchall_311
    move-exception v2

    #@312
    monitor-exit v3
    :try_end_313
    .catchall {:try_start_2d8 .. :try_end_313} :catchall_311

    #@313
    throw v2

    #@314
    .line 9096
    :sswitch_314
    move-object/from16 v0, p0

    #@316
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@318
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@31a
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@31d
    move-result-object v2

    #@31e
    const-string v3, "window_animation_scale"

    #@320
    move-object/from16 v0, p0

    #@322
    iget-object v4, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@324
    iget v4, v4, Lcom/android/server/wm/WindowManagerService;->mWindowAnimationScale:F

    #@326
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    #@329
    .line 9098
    move-object/from16 v0, p0

    #@32b
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@32d
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@32f
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@332
    move-result-object v2

    #@333
    const-string v3, "transition_animation_scale"

    #@335
    move-object/from16 v0, p0

    #@337
    iget-object v4, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@339
    iget v4, v4, Lcom/android/server/wm/WindowManagerService;->mTransitionAnimationScale:F

    #@33b
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    #@33e
    .line 9100
    move-object/from16 v0, p0

    #@340
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@342
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@344
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@347
    move-result-object v2

    #@348
    const-string v3, "animator_duration_scale"

    #@34a
    move-object/from16 v0, p0

    #@34c
    iget-object v4, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@34e
    iget v4, v4, Lcom/android/server/wm/WindowManagerService;->mAnimatorDurationScale:F

    #@350
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    #@353
    goto/16 :goto_7

    #@355
    .line 9106
    :sswitch_355
    move-object/from16 v0, p0

    #@357
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@359
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@35b
    monitor-enter v3

    #@35c
    .line 9107
    :try_start_35c
    move-object/from16 v0, p0

    #@35e
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@360
    iget-object v4, v2, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@362
    monitor-enter v4
    :try_end_363
    .catchall {:try_start_35c .. :try_end_363} :catchall_392

    #@363
    .line 9110
    :try_start_363
    move-object/from16 v0, p0

    #@365
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@367
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@369
    iget-boolean v2, v2, Lcom/android/server/wm/WindowAnimator;->mAnimating:Z

    #@36b
    if-nez v2, :cond_377

    #@36d
    move-object/from16 v0, p0

    #@36f
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@371
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mLayoutToAnim:Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;

    #@373
    iget-boolean v2, v2, Lcom/android/server/wm/WindowManagerService$LayoutToAnimatorParams;->mAnimationScheduled:Z

    #@375
    if-eqz v2, :cond_395

    #@377
    .line 9113
    :cond_377
    move-object/from16 v0, p0

    #@379
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@37b
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@37d
    move-object/from16 v0, p0

    #@37f
    iget-object v5, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@381
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@383
    const/16 v6, 0xf

    #@385
    invoke-virtual {v5, v6}, Lcom/android/server/wm/WindowManagerService$H;->obtainMessage(I)Landroid/os/Message;

    #@388
    move-result-object v5

    #@389
    const-wide/16 v6, 0x7d0

    #@38b
    invoke-virtual {v2, v5, v6, v7}, Lcom/android/server/wm/WindowManagerService$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@38e
    .line 9115
    monitor-exit v4
    :try_end_38f
    .catchall {:try_start_363 .. :try_end_38f} :catchall_3ac

    #@38f
    :try_start_38f
    monitor-exit v3

    #@390
    goto/16 :goto_7

    #@392
    .line 9123
    :catchall_392
    move-exception v2

    #@393
    monitor-exit v3
    :try_end_394
    .catchall {:try_start_38f .. :try_end_394} :catchall_392

    #@394
    throw v2

    #@395
    .line 9119
    :cond_395
    :try_start_395
    move-object/from16 v0, p0

    #@397
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@399
    iget-boolean v2, v2, Lcom/android/server/wm/WindowManagerService;->mDisplayFrozen:Z

    #@39b
    if-eqz v2, :cond_3a1

    #@39d
    .line 9120
    monitor-exit v4
    :try_end_39e
    .catchall {:try_start_395 .. :try_end_39e} :catchall_3ac

    #@39e
    :try_start_39e
    monitor-exit v3
    :try_end_39f
    .catchall {:try_start_39e .. :try_end_39f} :catchall_392

    #@39f
    goto/16 :goto_7

    #@3a1
    .line 9122
    :cond_3a1
    :try_start_3a1
    monitor-exit v4
    :try_end_3a2
    .catchall {:try_start_3a1 .. :try_end_3a2} :catchall_3ac

    #@3a2
    .line 9123
    :try_start_3a2
    monitor-exit v3
    :try_end_3a3
    .catchall {:try_start_3a2 .. :try_end_3a3} :catchall_392

    #@3a3
    .line 9124
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@3a6
    move-result-object v2

    #@3a7
    invoke-virtual {v2}, Ljava/lang/Runtime;->gc()V

    #@3aa
    goto/16 :goto_7

    #@3ac
    .line 9122
    :catchall_3ac
    move-exception v2

    #@3ad
    :try_start_3ad
    monitor-exit v4
    :try_end_3ae
    .catchall {:try_start_3ad .. :try_end_3ae} :catchall_3ac

    #@3ae
    :try_start_3ae
    throw v2
    :try_end_3af
    .catchall {:try_start_3ae .. :try_end_3af} :catchall_392

    #@3af
    .line 9129
    :sswitch_3af
    move-object/from16 v0, p0

    #@3b1
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@3b3
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->performEnableScreen()V

    #@3b6
    goto/16 :goto_7

    #@3b8
    .line 9134
    :sswitch_3b8
    move-object/from16 v0, p0

    #@3ba
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@3bc
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@3be
    monitor-enter v3

    #@3bf
    .line 9135
    :try_start_3bf
    move-object/from16 v0, p0

    #@3c1
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@3c3
    iget-object v4, v2, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@3c5
    monitor-enter v4
    :try_end_3c6
    .catchall {:try_start_3bf .. :try_end_3c6} :catchall_41a

    #@3c6
    .line 9136
    :try_start_3c6
    const-string v2, "WindowManager"

    #@3c8
    const-string v5, "App freeze timeout expired."

    #@3ca
    invoke-static {v2, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3cd
    .line 9137
    move-object/from16 v0, p0

    #@3cf
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@3d1
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAppTokens:Ljava/util/ArrayList;

    #@3d3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@3d6
    move-result v16

    #@3d7
    .line 9138
    .restart local v16       #i:I
    :cond_3d7
    :goto_3d7
    if-lez v16, :cond_41d

    #@3d9
    .line 9139
    add-int/lit8 v16, v16, -0x1

    #@3db
    .line 9140
    move-object/from16 v0, p0

    #@3dd
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@3df
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAppTokens:Ljava/util/ArrayList;

    #@3e1
    move/from16 v0, v16

    #@3e3
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3e6
    move-result-object v29

    #@3e7
    check-cast v29, Lcom/android/server/wm/AppWindowToken;

    #@3e9
    .line 9141
    .local v29, tok:Lcom/android/server/wm/AppWindowToken;
    move-object/from16 v0, v29

    #@3eb
    iget-object v2, v0, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@3ed
    iget-boolean v2, v2, Lcom/android/server/wm/AppWindowAnimator;->freezingScreen:Z

    #@3ef
    if-eqz v2, :cond_3d7

    #@3f1
    .line 9142
    const-string v2, "WindowManager"

    #@3f3
    new-instance v5, Ljava/lang/StringBuilder;

    #@3f5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3f8
    const-string v6, "Force clearing freeze: "

    #@3fa
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fd
    move-result-object v5

    #@3fe
    move-object/from16 v0, v29

    #@400
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@403
    move-result-object v5

    #@404
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@407
    move-result-object v5

    #@408
    invoke-static {v2, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40b
    .line 9143
    move-object/from16 v0, p0

    #@40d
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@40f
    const/4 v5, 0x1

    #@410
    const/4 v6, 0x1

    #@411
    move-object/from16 v0, v29

    #@413
    invoke-virtual {v2, v0, v5, v6}, Lcom/android/server/wm/WindowManagerService;->unsetAppFreezingScreenLocked(Lcom/android/server/wm/AppWindowToken;ZZ)V

    #@416
    goto :goto_3d7

    #@417
    .line 9146
    .end local v16           #i:I
    .end local v29           #tok:Lcom/android/server/wm/AppWindowToken;
    :catchall_417
    move-exception v2

    #@418
    monitor-exit v4
    :try_end_419
    .catchall {:try_start_3c6 .. :try_end_419} :catchall_417

    #@419
    :try_start_419
    throw v2

    #@41a
    .line 9147
    :catchall_41a
    move-exception v2

    #@41b
    monitor-exit v3
    :try_end_41c
    .catchall {:try_start_419 .. :try_end_41c} :catchall_41a

    #@41c
    throw v2

    #@41d
    .line 9146
    .restart local v16       #i:I
    :cond_41d
    :try_start_41d
    monitor-exit v4
    :try_end_41e
    .catchall {:try_start_41d .. :try_end_41e} :catchall_417

    #@41e
    .line 9147
    :try_start_41e
    monitor-exit v3
    :try_end_41f
    .catchall {:try_start_41e .. :try_end_41f} :catchall_41a

    #@41f
    goto/16 :goto_7

    #@421
    .line 9152
    .end local v16           #i:I
    :sswitch_421
    move-object/from16 v0, p0

    #@423
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@425
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@427
    monitor-enter v3

    #@428
    .line 9153
    :try_start_428
    move-object/from16 v0, p0

    #@42a
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@42c
    iget-boolean v2, v2, Lcom/android/server/wm/WindowManagerService;->mClientFreezingScreen:Z

    #@42e
    if-eqz v2, :cond_43e

    #@430
    .line 9154
    move-object/from16 v0, p0

    #@432
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@434
    const/4 v4, 0x0

    #@435
    iput-boolean v4, v2, Lcom/android/server/wm/WindowManagerService;->mClientFreezingScreen:Z

    #@437
    .line 9155
    move-object/from16 v0, p0

    #@439
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@43b
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$700(Lcom/android/server/wm/WindowManagerService;)V

    #@43e
    .line 9157
    :cond_43e
    monitor-exit v3

    #@43f
    goto/16 :goto_7

    #@441
    :catchall_441
    move-exception v2

    #@442
    monitor-exit v3
    :try_end_443
    .catchall {:try_start_428 .. :try_end_443} :catchall_441

    #@443
    throw v2

    #@444
    .line 9162
    :sswitch_444
    const/16 v2, 0x12

    #@446
    move-object/from16 v0, p0

    #@448
    invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(I)V

    #@44b
    .line 9163
    move-object/from16 v0, p0

    #@44d
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@44f
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->sendNewConfiguration()V

    #@452
    goto/16 :goto_7

    #@454
    .line 9168
    :sswitch_454
    move-object/from16 v0, p0

    #@456
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@458
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$800(Lcom/android/server/wm/WindowManagerService;)Z

    #@45b
    move-result v2

    #@45c
    if-eqz v2, :cond_7

    #@45e
    .line 9169
    move-object/from16 v0, p0

    #@460
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@462
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@464
    monitor-enter v3

    #@465
    .line 9170
    :try_start_465
    move-object/from16 v0, p0

    #@467
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@469
    const/4 v4, 0x0

    #@46a
    invoke-static {v2, v4}, Lcom/android/server/wm/WindowManagerService;->access$802(Lcom/android/server/wm/WindowManagerService;Z)Z

    #@46d
    .line 9171
    monitor-exit v3
    :try_end_46e
    .catchall {:try_start_465 .. :try_end_46e} :catchall_477

    #@46e
    .line 9172
    move-object/from16 v0, p0

    #@470
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@472
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$900(Lcom/android/server/wm/WindowManagerService;)V

    #@475
    goto/16 :goto_7

    #@477
    .line 9171
    :catchall_477
    move-exception v2

    #@478
    :try_start_478
    monitor-exit v3
    :try_end_479
    .catchall {:try_start_478 .. :try_end_479} :catchall_477

    #@479
    throw v2

    #@47a
    .line 9178
    :sswitch_47a
    move-object/from16 v0, p1

    #@47c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@47e
    move-object/from16 v34, v0

    #@480
    check-cast v34, Landroid/os/IBinder;

    #@482
    .line 9182
    .local v34, win:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@484
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@486
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@488
    monitor-enter v3

    #@489
    .line 9184
    :try_start_489
    move-object/from16 v0, p0

    #@48b
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@48d
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@48f
    if-eqz v2, :cond_4b4

    #@491
    .line 9185
    move-object/from16 v0, p0

    #@493
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@495
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@497
    invoke-virtual {v2}, Lcom/android/server/wm/DragState;->unregister()V

    #@49a
    .line 9186
    move-object/from16 v0, p0

    #@49c
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@49e
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mInputMonitor:Lcom/android/server/wm/InputMonitor;

    #@4a0
    const/4 v4, 0x1

    #@4a1
    invoke-virtual {v2, v4}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@4a4
    .line 9187
    move-object/from16 v0, p0

    #@4a6
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4a8
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@4aa
    invoke-virtual {v2}, Lcom/android/server/wm/DragState;->reset()V

    #@4ad
    .line 9188
    move-object/from16 v0, p0

    #@4af
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4b1
    const/4 v4, 0x0

    #@4b2
    iput-object v4, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@4b4
    .line 9190
    :cond_4b4
    monitor-exit v3

    #@4b5
    goto/16 :goto_7

    #@4b7
    :catchall_4b7
    move-exception v2

    #@4b8
    monitor-exit v3
    :try_end_4b9
    .catchall {:try_start_489 .. :try_end_4b9} :catchall_4b7

    #@4b9
    throw v2

    #@4ba
    .line 9195
    .end local v34           #win:Landroid/os/IBinder;
    :sswitch_4ba
    move-object/from16 v0, p1

    #@4bc
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4be
    move-object/from16 v34, v0

    #@4c0
    check-cast v34, Landroid/os/IBinder;

    #@4c2
    .line 9199
    .restart local v34       #win:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@4c4
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4c6
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@4c8
    monitor-enter v3

    #@4c9
    .line 9201
    :try_start_4c9
    move-object/from16 v0, p0

    #@4cb
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4cd
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@4cf
    if-eqz v2, :cond_4e3

    #@4d1
    .line 9202
    move-object/from16 v0, p0

    #@4d3
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4d5
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@4d7
    const/4 v4, 0x0

    #@4d8
    iput-boolean v4, v2, Lcom/android/server/wm/DragState;->mDragResult:Z

    #@4da
    .line 9203
    move-object/from16 v0, p0

    #@4dc
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4de
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@4e0
    invoke-virtual {v2}, Lcom/android/server/wm/DragState;->endDragLw()V

    #@4e3
    .line 9205
    :cond_4e3
    monitor-exit v3

    #@4e4
    goto/16 :goto_7

    #@4e6
    :catchall_4e6
    move-exception v2

    #@4e7
    monitor-exit v3
    :try_end_4e8
    .catchall {:try_start_4c9 .. :try_end_4e8} :catchall_4e6

    #@4e8
    throw v2

    #@4e9
    .line 9210
    .end local v34           #win:Landroid/os/IBinder;
    :sswitch_4e9
    move-object/from16 v0, p0

    #@4eb
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4ed
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->notifyHardKeyboardStatusChange()V

    #@4f0
    goto/16 :goto_7

    #@4f2
    .line 9215
    :sswitch_4f2
    move-object/from16 v0, p0

    #@4f4
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4f6
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->performBootTimeout()V

    #@4f9
    goto/16 :goto_7

    #@4fb
    .line 9221
    :sswitch_4fb
    move-object/from16 v0, p0

    #@4fd
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@4ff
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@501
    monitor-enter v3

    #@502
    .line 9222
    :try_start_502
    move-object/from16 v0, p1

    #@504
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@506
    move-object/from16 v24, v0

    #@508
    check-cast v24, Landroid/util/Pair;

    #@50a
    .line 9223
    .local v24, pair:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/wm/WindowState;Landroid/os/IRemoteCallback;>;"
    const-string v2, "WindowManager"

    #@50c
    new-instance v4, Ljava/lang/StringBuilder;

    #@50e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@511
    const-string v5, "Timeout waiting for drawn: "

    #@513
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@516
    move-result-object v4

    #@517
    move-object/from16 v0, v24

    #@519
    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@51b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51e
    move-result-object v4

    #@51f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@522
    move-result-object v4

    #@523
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@526
    .line 9224
    move-object/from16 v0, p0

    #@528
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@52a
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mWaitingForDrawn:Ljava/util/ArrayList;

    #@52c
    move-object/from16 v0, v24

    #@52e
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@531
    move-result v2

    #@532
    if-nez v2, :cond_53a

    #@534
    .line 9225
    monitor-exit v3

    #@535
    goto/16 :goto_7

    #@537
    .line 9227
    .end local v24           #pair:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/wm/WindowState;Landroid/os/IRemoteCallback;>;"
    :catchall_537
    move-exception v2

    #@538
    monitor-exit v3
    :try_end_539
    .catchall {:try_start_502 .. :try_end_539} :catchall_537

    #@539
    throw v2

    #@53a
    .restart local v24       #pair:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/wm/WindowState;Landroid/os/IRemoteCallback;>;"
    :cond_53a
    :try_start_53a
    monitor-exit v3
    :try_end_53b
    .catchall {:try_start_53a .. :try_end_53b} :catchall_537

    #@53b
    .line 9229
    :try_start_53b
    move-object/from16 v0, v24

    #@53d
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@53f
    check-cast v2, Landroid/os/IRemoteCallback;

    #@541
    const/4 v3, 0x0

    #@542
    invoke-interface {v2, v3}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_545
    .catch Landroid/os/RemoteException; {:try_start_53b .. :try_end_545} :catch_547

    #@545
    goto/16 :goto_7

    #@547
    .line 9230
    :catch_547
    move-exception v2

    #@548
    goto/16 :goto_7

    #@54a
    .line 9237
    .end local v24           #pair:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/wm/WindowState;Landroid/os/IRemoteCallback;>;"
    :sswitch_54a
    move-object/from16 v0, p0

    #@54c
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@54e
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@550
    monitor-enter v3

    #@551
    .line 9238
    :try_start_551
    move-object/from16 v0, p0

    #@553
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@555
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$1000(Lcom/android/server/wm/WindowManagerService;)Z

    #@558
    move-result v2

    #@559
    if-eqz v2, :cond_56e

    #@55b
    .line 9239
    move-object/from16 v0, p0

    #@55d
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@55f
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@561
    const v4, 0x186a2

    #@564
    invoke-virtual {v2, v4}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z

    #@567
    .line 9240
    move-object/from16 v0, p0

    #@569
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@56b
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$600(Lcom/android/server/wm/WindowManagerService;)V

    #@56e
    .line 9242
    :cond_56e
    monitor-exit v3

    #@56f
    goto/16 :goto_7

    #@571
    :catchall_571
    move-exception v2

    #@572
    monitor-exit v3
    :try_end_573
    .catchall {:try_start_551 .. :try_end_573} :catchall_571

    #@573
    throw v2

    #@574
    .line 9247
    :sswitch_574
    move-object/from16 v0, p0

    #@576
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@578
    move-object/from16 v0, p1

    #@57a
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@57c
    move-object/from16 v0, p1

    #@57e
    iget v4, v0, Landroid/os/Message;->arg2:I

    #@580
    invoke-static {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->access$1100(Lcom/android/server/wm/WindowManagerService;II)V

    #@583
    goto/16 :goto_7

    #@585
    .line 9253
    :sswitch_585
    move-object/from16 v0, p1

    #@587
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@589
    move-object/from16 v25, v0

    #@58b
    check-cast v25, Landroid/util/Pair;

    #@58d
    .line 9255
    .local v25, pair:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/wm/WindowStateAnimator;Landroid/graphics/Region;>;"
    move-object/from16 v0, v25

    #@58f
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@591
    move-object/from16 v35, v0

    #@593
    check-cast v35, Lcom/android/server/wm/WindowStateAnimator;

    #@595
    .line 9256
    .local v35, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    move-object/from16 v0, v25

    #@597
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@599
    check-cast v2, Landroid/graphics/Region;

    #@59b
    move-object/from16 v0, v35

    #@59d
    invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowStateAnimator;->setTransparentRegionHint(Landroid/graphics/Region;)V

    #@5a0
    goto/16 :goto_7

    #@5a2
    .line 9261
    .end local v25           #pair:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/wm/WindowStateAnimator;Landroid/graphics/Region;>;"
    .end local v35           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :sswitch_5a2
    move-object/from16 v0, p0

    #@5a4
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@5a6
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@5a8
    invoke-virtual {v2}, Lcom/android/server/wm/WindowAnimator;->clearPendingActions()V

    #@5ab
    goto/16 :goto_7

    #@5ad
    .line 9267
    :sswitch_5ad
    :try_start_5ad
    move-object/from16 v0, p1

    #@5af
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5b1
    check-cast v2, Landroid/os/IRemoteCallback;

    #@5b3
    const/4 v3, 0x0

    #@5b4
    invoke-interface {v2, v3}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_5b7
    .catch Landroid/os/RemoteException; {:try_start_5ad .. :try_end_5b7} :catch_5b9

    #@5b7
    goto/16 :goto_7

    #@5b9
    .line 9268
    :catch_5b9
    move-exception v2

    #@5ba
    goto/16 :goto_7

    #@5bc
    .line 9274
    :sswitch_5bc
    move-object/from16 v0, p1

    #@5be
    iget v14, v0, Landroid/os/Message;->arg1:I

    #@5c0
    .line 9275
    .local v14, displayId:I
    move-object/from16 v0, p1

    #@5c2
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@5c4
    move/from16 v27, v0

    #@5c6
    .line 9276
    .local v27, rotation:I
    move-object/from16 v0, p0

    #@5c8
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@5ca
    move/from16 v0, v27

    #@5cc
    invoke-static {v2, v14, v0}, Lcom/android/server/wm/WindowManagerService;->access$1200(Lcom/android/server/wm/WindowManagerService;II)V

    #@5cf
    goto/16 :goto_7

    #@5d1
    .line 9281
    .end local v14           #displayId:I
    .end local v27           #rotation:I
    :sswitch_5d1
    move-object/from16 v0, p1

    #@5d3
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@5d5
    move/from16 v31, v0

    #@5d7
    .line 9282
    .local v31, transition:I
    move-object/from16 v0, p1

    #@5d9
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5db
    move-object/from16 v18, v0

    #@5dd
    check-cast v18, Landroid/view/WindowInfo;

    #@5df
    .line 9283
    .local v18, info:Landroid/view/WindowInfo;
    move-object/from16 v0, p0

    #@5e1
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@5e3
    move/from16 v0, v31

    #@5e5
    move-object/from16 v1, v18

    #@5e7
    invoke-static {v2, v0, v1}, Lcom/android/server/wm/WindowManagerService;->access$1300(Lcom/android/server/wm/WindowManagerService;ILandroid/view/WindowInfo;)V

    #@5ea
    goto/16 :goto_7

    #@5ec
    .line 9288
    .end local v18           #info:Landroid/view/WindowInfo;
    .end local v31           #transition:I
    :sswitch_5ec
    move-object/from16 v0, p1

    #@5ee
    iget v14, v0, Landroid/os/Message;->arg1:I

    #@5f0
    .line 9289
    .restart local v14       #displayId:I
    move-object/from16 v0, p1

    #@5f2
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@5f4
    const/4 v3, 0x1

    #@5f5
    if-ne v2, v3, :cond_60e

    #@5f7
    const/16 v17, 0x1

    #@5f9
    .line 9290
    .local v17, immediate:Z
    :goto_5f9
    move-object/from16 v0, p1

    #@5fb
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5fd
    move-object/from16 v26, v0

    #@5ff
    check-cast v26, Landroid/graphics/Rect;

    #@601
    .line 9291
    .local v26, rectangle:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@603
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@605
    move-object/from16 v0, v26

    #@607
    move/from16 v1, v17

    #@609
    invoke-static {v2, v14, v0, v1}, Lcom/android/server/wm/WindowManagerService;->access$1400(Lcom/android/server/wm/WindowManagerService;ILandroid/graphics/Rect;Z)V

    #@60c
    goto/16 :goto_7

    #@60e
    .line 9289
    .end local v17           #immediate:Z
    .end local v26           #rectangle:Landroid/graphics/Rect;
    :cond_60e
    const/16 v17, 0x0

    #@610
    goto :goto_5f9

    #@611
    .line 9296
    .end local v14           #displayId:I
    :sswitch_611
    move-object/from16 v0, p1

    #@613
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@615
    check-cast v13, Lcom/android/server/wm/DisplayContent;

    #@617
    .line 9297
    .local v13, displayContent:Lcom/android/server/wm/DisplayContent;
    move-object/from16 v0, p0

    #@619
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@61b
    invoke-static {v2, v13}, Lcom/android/server/wm/WindowManagerService;->access$1500(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/DisplayContent;)V

    #@61e
    goto/16 :goto_7

    #@620
    .line 9302
    .end local v13           #displayContent:Lcom/android/server/wm/DisplayContent;
    :sswitch_620
    const-string v2, "WindowManager"

    #@622
    new-instance v3, Ljava/lang/StringBuilder;

    #@624
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@627
    const-string v4, "[WMS.java] DO_DISPLAY_ADDED --> calling handleDisplayAddedLocked(), displayId = "

    #@629
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62c
    move-result-object v3

    #@62d
    move-object/from16 v0, p1

    #@62f
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@631
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@634
    move-result-object v3

    #@635
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@638
    move-result-object v3

    #@639
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@63c
    .line 9303
    move-object/from16 v0, p0

    #@63e
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@640
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@642
    monitor-enter v3

    #@643
    .line 9304
    :try_start_643
    move-object/from16 v0, p0

    #@645
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@647
    move-object/from16 v0, p1

    #@649
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@64b
    invoke-static {v2, v4}, Lcom/android/server/wm/WindowManagerService;->access$1600(Lcom/android/server/wm/WindowManagerService;I)V

    #@64e
    .line 9307
    move-object/from16 v0, p0

    #@650
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@652
    iget-boolean v2, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenConnected:Z

    #@654
    if-nez v2, :cond_66a

    #@656
    .line 9308
    move-object/from16 v0, p0

    #@658
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@65a
    move-object/from16 v0, p1

    #@65c
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@65e
    invoke-static {v2, v4}, Lcom/android/server/wm/WindowManagerService;->access$202(Lcom/android/server/wm/WindowManagerService;I)I

    #@661
    .line 9309
    move-object/from16 v0, p0

    #@663
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@665
    const/4 v4, 0x0

    #@666
    const/4 v5, 0x1

    #@667
    invoke-static {v2, v4, v5}, Lcom/android/server/wm/WindowManagerService;->access$300(Lcom/android/server/wm/WindowManagerService;ZI)I

    #@66a
    .line 9313
    :cond_66a
    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->access$1700()Ljava/lang/String;

    #@66d
    move-result-object v2

    #@66e
    const-string v4, "DCM"

    #@670
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@673
    move-result v2

    #@674
    if-eqz v2, :cond_682

    #@676
    .line 9314
    move-object/from16 v0, p0

    #@678
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@67a
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$1800(Lcom/android/server/wm/WindowManagerService;)Landroid/os/Handler;

    #@67d
    move-result-object v2

    #@67e
    const/4 v4, 0x0

    #@67f
    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@682
    .line 9317
    :cond_682
    move-object/from16 v0, p0

    #@684
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@686
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$400(Lcom/android/server/wm/WindowManagerService;)Landroid/os/Handler;

    #@689
    move-result-object v2

    #@68a
    const/4 v4, 0x0

    #@68b
    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@68e
    .line 9319
    monitor-exit v3

    #@68f
    goto/16 :goto_7

    #@691
    :catchall_691
    move-exception v2

    #@692
    monitor-exit v3
    :try_end_693
    .catchall {:try_start_643 .. :try_end_693} :catchall_691

    #@693
    throw v2

    #@694
    .line 9323
    :sswitch_694
    const-string v2, "WindowManager"

    #@696
    new-instance v3, Ljava/lang/StringBuilder;

    #@698
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@69b
    const-string v4, "[WMS.java] DO_DISPLAY_REMOVED --> calling handleDisplayRemovedLocked(), displayId = "

    #@69d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a0
    move-result-object v3

    #@6a1
    move-object/from16 v0, p1

    #@6a3
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@6a5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a8
    move-result-object v3

    #@6a9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6ac
    move-result-object v3

    #@6ad
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6b0
    .line 9324
    move-object/from16 v0, p0

    #@6b2
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6b4
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@6b6
    monitor-enter v3

    #@6b7
    .line 9327
    :try_start_6b7
    move-object/from16 v0, p0

    #@6b9
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6bb
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$200(Lcom/android/server/wm/WindowManagerService;)I

    #@6be
    move-result v2

    #@6bf
    move-object/from16 v0, p1

    #@6c1
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@6c3
    if-ne v2, v4, :cond_6d7

    #@6c5
    .line 9328
    move-object/from16 v0, p0

    #@6c7
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6c9
    const/4 v4, 0x0

    #@6ca
    invoke-static {v2, v4}, Lcom/android/server/wm/WindowManagerService;->access$202(Lcom/android/server/wm/WindowManagerService;I)I

    #@6cd
    .line 9329
    move-object/from16 v0, p0

    #@6cf
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6d1
    const/4 v4, 0x0

    #@6d2
    const/16 v5, 0x8

    #@6d4
    invoke-static {v2, v4, v5}, Lcom/android/server/wm/WindowManagerService;->access$300(Lcom/android/server/wm/WindowManagerService;ZI)I

    #@6d7
    .line 9333
    :cond_6d7
    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->access$1700()Ljava/lang/String;

    #@6da
    move-result-object v2

    #@6db
    const-string v4, "DCM"

    #@6dd
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e0
    move-result v2

    #@6e1
    if-eqz v2, :cond_6ef

    #@6e3
    .line 9334
    move-object/from16 v0, p0

    #@6e5
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6e7
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$1800(Lcom/android/server/wm/WindowManagerService;)Landroid/os/Handler;

    #@6ea
    move-result-object v2

    #@6eb
    const/4 v4, 0x0

    #@6ec
    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6ef
    .line 9337
    :cond_6ef
    move-object/from16 v0, p0

    #@6f1
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6f3
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$400(Lcom/android/server/wm/WindowManagerService;)Landroid/os/Handler;

    #@6f6
    move-result-object v2

    #@6f7
    const/4 v4, 0x0

    #@6f8
    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6fb
    .line 9340
    move-object/from16 v0, p0

    #@6fd
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@6ff
    move-object/from16 v0, p1

    #@701
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@703
    invoke-static {v2, v4}, Lcom/android/server/wm/WindowManagerService;->access$1900(Lcom/android/server/wm/WindowManagerService;I)V

    #@706
    .line 9341
    monitor-exit v3

    #@707
    goto/16 :goto_7

    #@709
    :catchall_709
    move-exception v2

    #@70a
    monitor-exit v3
    :try_end_70b
    .catchall {:try_start_6b7 .. :try_end_70b} :catchall_709

    #@70b
    throw v2

    #@70c
    .line 9345
    :sswitch_70c
    const-string v2, "WindowManager"

    #@70e
    new-instance v3, Ljava/lang/StringBuilder;

    #@710
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@713
    const-string v4, "[WMS.java] DO_DISPLAY_CHANGED --> calling handleDisplayChangedLocked(), displayId = "

    #@715
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@718
    move-result-object v3

    #@719
    move-object/from16 v0, p1

    #@71b
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@71d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@720
    move-result-object v3

    #@721
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@724
    move-result-object v3

    #@725
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@728
    .line 9346
    move-object/from16 v0, p0

    #@72a
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@72c
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@72e
    monitor-enter v3

    #@72f
    .line 9347
    :try_start_72f
    move-object/from16 v0, p0

    #@731
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@733
    move-object/from16 v0, p1

    #@735
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@737
    invoke-static {v2, v4}, Lcom/android/server/wm/WindowManagerService;->access$2000(Lcom/android/server/wm/WindowManagerService;I)V

    #@73a
    .line 9348
    monitor-exit v3

    #@73b
    goto/16 :goto_7

    #@73d
    :catchall_73d
    move-exception v2

    #@73e
    monitor-exit v3
    :try_end_73f
    .catchall {:try_start_72f .. :try_end_73f} :catchall_73d

    #@73f
    throw v2

    #@740
    .line 9353
    :sswitch_740
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@742
    if-eqz v2, :cond_7

    #@744
    .line 9354
    move-object/from16 v0, p0

    #@746
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@748
    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@74a
    monitor-enter v3

    #@74b
    .line 9355
    :try_start_74b
    move-object/from16 v0, p0

    #@74d
    iget-object v2, v0, Lcom/android/server/wm/WindowManagerService$H;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@74f
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$2100(Lcom/android/server/wm/WindowManagerService;)V

    #@752
    .line 9356
    monitor-exit v3

    #@753
    goto/16 :goto_7

    #@755
    :catchall_755
    move-exception v2

    #@756
    monitor-exit v3
    :try_end_757
    .catchall {:try_start_74b .. :try_end_757} :catchall_755

    #@757
    throw v2

    #@758
    .line 8897
    .restart local v11       #N:I
    .restart local v16       #i:I
    .restart local v20       #losers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowState;>;"
    :catch_758
    move-exception v2

    #@759
    goto/16 :goto_b5

    #@75b
    .line 8867
    .end local v11           #N:I
    .end local v16           #i:I
    .end local v20           #losers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/wm/WindowState;>;"
    .restart local v19       #lastFocus:Lcom/android/server/wm/WindowState;
    .restart local v21       #newFocus:Lcom/android/server/wm/WindowState;
    :catch_75b
    move-exception v2

    #@75c
    goto/16 :goto_60

    #@75e
    .line 8837
    :sswitch_data_75e
    .sparse-switch
        0x2 -> :sswitch_8
        0x3 -> :sswitch_7a
        0x4 -> :sswitch_bb
        0x5 -> :sswitch_d6
        0x6 -> :sswitch_16a
        0x7 -> :sswitch_1c2
        0x8 -> :sswitch_241
        0x9 -> :sswitch_22d
        0xb -> :sswitch_276
        0xd -> :sswitch_2d1
        0xe -> :sswitch_314
        0xf -> :sswitch_355
        0x10 -> :sswitch_3af
        0x11 -> :sswitch_3b8
        0x12 -> :sswitch_444
        0x13 -> :sswitch_454
        0x14 -> :sswitch_47a
        0x15 -> :sswitch_4ba
        0x16 -> :sswitch_4e9
        0x17 -> :sswitch_4f2
        0x18 -> :sswitch_4fb
        0x19 -> :sswitch_54a
        0x1a -> :sswitch_574
        0x1b -> :sswitch_5ad
        0x1c -> :sswitch_5bc
        0x1d -> :sswitch_5d1
        0x1e -> :sswitch_5ec
        0x1f -> :sswitch_611
        0x20 -> :sswitch_620
        0x21 -> :sswitch_694
        0x22 -> :sswitch_70c
        0x23 -> :sswitch_421
        0x63 -> :sswitch_740
        0x186a1 -> :sswitch_585
        0x186a2 -> :sswitch_5a2
    .end sparse-switch
.end method
