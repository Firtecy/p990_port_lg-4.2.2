.class Lcom/android/server/wm/DragState;
.super Ljava/lang/Object;
.source "DragState.java"


# instance fields
.field private endP:Landroid/graphics/PointF;

.field mClientChannel:Landroid/view/InputChannel;

.field mCurrentX:F

.field mCurrentY:F

.field mData:Landroid/content/ClipData;

.field mDataDescription:Landroid/content/ClipDescription;

.field mDisplay:Landroid/view/Display;

.field mDragApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

.field mDragInProgress:Z

.field mDragResult:Z

.field mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

.field mFlags:I

.field mInputEventReceiver:Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;

.field mLocalWin:Landroid/os/IBinder;

.field mNotifiedWindows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/wm/WindowState;",
            ">;"
        }
    .end annotation
.end field

.field mServerChannel:Landroid/view/InputChannel;

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field mSurface:Landroid/view/Surface;

.field mTargetWindow:Lcom/android/server/wm/WindowState;

.field mThumbOffsetX:F

.field mThumbOffsetY:F

.field private final mTmpRegion:Landroid/graphics/Region;

.field mToken:Landroid/os/IBinder;

.field private startP:Landroid/graphics/PointF;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;Landroid/view/Surface;ILandroid/os/IBinder;)V
    .registers 7
    .parameter "service"
    .parameter "token"
    .parameter "surface"
    .parameter "flags"
    .parameter "localWin"

    #@0
    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 70
    new-instance v0, Landroid/graphics/Region;

    #@5
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/wm/DragState;->mTmpRegion:Landroid/graphics/Region;

    #@a
    .line 73
    new-instance v0, Landroid/graphics/PointF;

    #@c
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/wm/DragState;->startP:Landroid/graphics/PointF;

    #@11
    .line 74
    new-instance v0, Landroid/graphics/PointF;

    #@13
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/wm/DragState;->endP:Landroid/graphics/PointF;

    #@18
    .line 79
    iput-object p1, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1a
    .line 80
    iput-object p2, p0, Lcom/android/server/wm/DragState;->mToken:Landroid/os/IBinder;

    #@1c
    .line 81
    iput-object p3, p0, Lcom/android/server/wm/DragState;->mSurface:Landroid/view/Surface;

    #@1e
    .line 82
    iput p4, p0, Lcom/android/server/wm/DragState;->mFlags:I

    #@20
    .line 83
    iput-object p5, p0, Lcom/android/server/wm/DragState;->mLocalWin:Landroid/os/IBinder;

    #@22
    .line 84
    new-instance v0, Ljava/util/ArrayList;

    #@24
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/server/wm/DragState;->mNotifiedWindows:Ljava/util/ArrayList;

    #@29
    .line 85
    return-void
.end method

.method private getTouchedWinAtPointLw(FF)Lcom/android/server/wm/WindowState;
    .registers 14
    .parameter "xf"
    .parameter "yf"

    #@0
    .prologue
    .line 471
    const/4 v5, 0x0

    #@1
    .line 472
    .local v5, touchedWin:Lcom/android/server/wm/WindowState;
    float-to-int v7, p1

    #@2
    .line 473
    .local v7, x:I
    float-to-int v8, p2

    #@3
    .line 475
    .local v8, y:I
    iget-object v9, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@5
    iget-object v10, p0, Lcom/android/server/wm/DragState;->mDisplay:Landroid/view/Display;

    #@7
    invoke-virtual {v9, v10}, Lcom/android/server/wm/WindowManagerService;->getWindowListLocked(Landroid/view/Display;)Lcom/android/server/wm/WindowList;

    #@a
    move-result-object v6

    #@b
    .line 476
    .local v6, windows:Lcom/android/server/wm/WindowList;
    if-nez v6, :cond_f

    #@d
    .line 477
    const/4 v9, 0x0

    #@e
    .line 504
    :goto_e
    return-object v9

    #@f
    .line 479
    :cond_f
    invoke-virtual {v6}, Lcom/android/server/wm/WindowList;->size()I

    #@12
    move-result v0

    #@13
    .line 480
    .local v0, N:I
    add-int/lit8 v3, v0, -0x1

    #@15
    .local v3, i:I
    :goto_15
    if-ltz v3, :cond_40

    #@17
    .line 481
    invoke-virtual {v6, v3}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Lcom/android/server/wm/WindowState;

    #@1d
    .line 482
    .local v1, child:Lcom/android/server/wm/WindowState;
    iget-object v9, v1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@1f
    iget v2, v9, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@21
    .line 483
    .local v2, flags:I
    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisibleLw()Z

    #@24
    move-result v9

    #@25
    if-nez v9, :cond_2a

    #@27
    .line 480
    :cond_27
    add-int/lit8 v3, v3, -0x1

    #@29
    goto :goto_15

    #@2a
    .line 487
    :cond_2a
    and-int/lit8 v9, v2, 0x10

    #@2c
    if-nez v9, :cond_27

    #@2e
    .line 492
    iget-object v9, p0, Lcom/android/server/wm/DragState;->mTmpRegion:Landroid/graphics/Region;

    #@30
    invoke-virtual {v1, v9}, Lcom/android/server/wm/WindowState;->getTouchableRegion(Landroid/graphics/Region;)V

    #@33
    .line 494
    and-int/lit8 v4, v2, 0x28

    #@35
    .line 497
    .local v4, touchFlags:I
    iget-object v9, p0, Lcom/android/server/wm/DragState;->mTmpRegion:Landroid/graphics/Region;

    #@37
    invoke-virtual {v9, v7, v8}, Landroid/graphics/Region;->contains(II)Z

    #@3a
    move-result v9

    #@3b
    if-nez v9, :cond_3f

    #@3d
    if-nez v4, :cond_27

    #@3f
    .line 499
    :cond_3f
    move-object v5, v1

    #@40
    .end local v1           #child:Lcom/android/server/wm/WindowState;
    .end local v2           #flags:I
    .end local v4           #touchFlags:I
    :cond_40
    move-object v9, v5

    #@41
    .line 504
    goto :goto_e
.end method

.method private static obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;
    .registers 15
    .parameter "win"
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "localState"
    .parameter "description"
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 510
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4
    int-to-float v0, v0

    #@5
    sub-float v1, p2, v0

    #@7
    .line 511
    .local v1, winX:F
    iget-object v0, p0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@9
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@b
    int-to-float v0, v0

    #@c
    sub-float v2, p3, v0

    #@e
    .line 512
    .local v2, winY:F
    iget-boolean v0, p0, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@10
    if-eqz v0, :cond_18

    #@12
    .line 513
    iget v0, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@14
    mul-float/2addr v1, v0

    #@15
    .line 514
    iget v0, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@17
    mul-float/2addr v2, v0

    #@18
    :cond_18
    move v0, p1

    #@19
    move-object v3, p4

    #@1a
    move-object v4, p5

    #@1b
    move-object v5, p6

    #@1c
    move v6, p7

    #@1d
    .line 516
    invoke-static/range {v0 .. v6}, Landroid/view/DragEvent;->obtain(IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@20
    move-result-object v0

    #@21
    return-object v0
.end method

.method private sendDragStartedLw(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V
    .registers 16
    .parameter "newWin"
    .parameter "touchX"
    .parameter "touchY"
    .parameter "desc"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 229
    iget v0, p0, Lcom/android/server/wm/DragState;->mFlags:I

    #@3
    and-int/lit8 v0, v0, 0x1

    #@5
    if-nez v0, :cond_12

    #@7
    .line 230
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@9
    invoke-interface {v0}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@c
    move-result-object v10

    #@d
    .line 231
    .local v10, winBinder:Landroid/os/IBinder;
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mLocalWin:Landroid/os/IBinder;

    #@f
    if-eq v10, v0, :cond_12

    #@11
    .line 255
    .end local v10           #winBinder:Landroid/os/IBinder;
    :cond_11
    :goto_11
    return-void

    #@12
    .line 239
    :cond_12
    iget-boolean v0, p0, Lcom/android/server/wm/DragState;->mDragInProgress:Z

    #@14
    if-eqz v0, :cond_11

    #@16
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->isPotentialDragTarget()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_11

    #@1c
    .line 240
    const/4 v1, 0x1

    #@1d
    const/4 v7, 0x0

    #@1e
    move-object v0, p1

    #@1f
    move v2, p2

    #@20
    move v3, p3

    #@21
    move-object v5, p4

    #@22
    move-object v6, v4

    #@23
    invoke-static/range {v0 .. v7}, Lcom/android/server/wm/DragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@26
    move-result-object v9

    #@27
    .line 243
    .local v9, event:Landroid/view/DragEvent;
    :try_start_27
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@29
    invoke-interface {v0, v9}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    #@2c
    .line 245
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mNotifiedWindows:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_31
    .catchall {:try_start_27 .. :try_end_31} :catchall_66
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_31} :catch_3f

    #@31
    .line 250
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@34
    move-result v0

    #@35
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@37
    iget v1, v1, Lcom/android/server/wm/Session;->mPid:I

    #@39
    if-eq v0, v1, :cond_11

    #@3b
    .line 251
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V

    #@3e
    goto :goto_11

    #@3f
    .line 246
    :catch_3f
    move-exception v8

    #@40
    .line 247
    .local v8, e:Landroid/os/RemoteException;
    :try_start_40
    const-string v0, "WindowManager"

    #@42
    new-instance v1, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v2, "Unable to drag-start window "

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_58
    .catchall {:try_start_40 .. :try_end_58} :catchall_66

    #@58
    .line 250
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@5b
    move-result v0

    #@5c
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@5e
    iget v1, v1, Lcom/android/server/wm/Session;->mPid:I

    #@60
    if-eq v0, v1, :cond_11

    #@62
    .line 251
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V

    #@65
    goto :goto_11

    #@66
    .line 250
    .end local v8           #e:Landroid/os/RemoteException;
    :catchall_66
    move-exception v0

    #@67
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@6a
    move-result v1

    #@6b
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@6d
    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    #@6f
    if-eq v1, v2, :cond_74

    #@71
    .line 251
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V

    #@74
    :cond_74
    throw v0
.end method


# virtual methods
.method broadcastDragEndedLw()V
    .registers 12

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 280
    const/4 v0, 0x4

    #@3
    iget-boolean v6, p0, Lcom/android/server/wm/DragState;->mDragResult:Z

    #@5
    move v2, v1

    #@6
    move-object v4, v3

    #@7
    move-object v5, v3

    #@8
    invoke-static/range {v0 .. v6}, Landroid/view/DragEvent;->obtain(IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@b
    move-result-object v8

    #@c
    .line 282
    .local v8, evt:Landroid/view/DragEvent;
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mNotifiedWindows:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v9

    #@12
    .local v9, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_3e

    #@18
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v10

    #@1c
    check-cast v10, Lcom/android/server/wm/WindowState;

    #@1e
    .line 284
    .local v10, ws:Lcom/android/server/wm/WindowState;
    :try_start_1e
    iget-object v0, v10, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@20
    invoke-interface {v0, v8}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_23} :catch_24

    #@23
    goto :goto_12

    #@24
    .line 285
    :catch_24
    move-exception v7

    #@25
    .line 286
    .local v7, e:Landroid/os/RemoteException;
    const-string v0, "WindowManager"

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "Unable to drag-end window "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    goto :goto_12

    #@3e
    .line 289
    .end local v7           #e:Landroid/os/RemoteException;
    .end local v10           #ws:Lcom/android/server/wm/WindowState;
    :cond_3e
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mNotifiedWindows:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@43
    .line 290
    const/4 v0, 0x0

    #@44
    iput-boolean v0, p0, Lcom/android/server/wm/DragState;->mDragInProgress:Z

    #@46
    .line 291
    invoke-virtual {v8}, Landroid/view/DragEvent;->recycle()V

    #@49
    .line 292
    return-void
.end method

.method broadcastDragStartedLw(FF)V
    .registers 8
    .parameter "touchX"
    .parameter "touchY"

    #@0
    .prologue
    .line 193
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@2
    if-eqz v3, :cond_9

    #@4
    .line 194
    iget-object v3, p0, Lcom/android/server/wm/DragState;->startP:Landroid/graphics/PointF;

    #@6
    invoke-virtual {v3, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    #@9
    .line 200
    :cond_9
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mData:Landroid/content/ClipData;

    #@b
    if-eqz v3, :cond_3c

    #@d
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mData:Landroid/content/ClipData;

    #@f
    invoke-virtual {v3}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    #@12
    move-result-object v3

    #@13
    :goto_13
    iput-object v3, p0, Lcom/android/server/wm/DragState;->mDataDescription:Landroid/content/ClipDescription;

    #@15
    .line 201
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mNotifiedWindows:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@1a
    .line 202
    const/4 v3, 0x1

    #@1b
    iput-boolean v3, p0, Lcom/android/server/wm/DragState;->mDragInProgress:Z

    #@1d
    .line 208
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1f
    iget-object v4, p0, Lcom/android/server/wm/DragState;->mDisplay:Landroid/view/Display;

    #@21
    invoke-virtual {v3, v4}, Lcom/android/server/wm/WindowManagerService;->getWindowListLocked(Landroid/view/Display;)Lcom/android/server/wm/WindowList;

    #@24
    move-result-object v2

    #@25
    .line 209
    .local v2, windows:Lcom/android/server/wm/WindowList;
    if-eqz v2, :cond_3e

    #@27
    .line 210
    invoke-virtual {v2}, Lcom/android/server/wm/WindowList;->size()I

    #@2a
    move-result v0

    #@2b
    .line 211
    .local v0, N:I
    const/4 v1, 0x0

    #@2c
    .local v1, i:I
    :goto_2c
    if-ge v1, v0, :cond_3e

    #@2e
    .line 212
    invoke-virtual {v2, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@31
    move-result-object v3

    #@32
    check-cast v3, Lcom/android/server/wm/WindowState;

    #@34
    iget-object v4, p0, Lcom/android/server/wm/DragState;->mDataDescription:Landroid/content/ClipDescription;

    #@36
    invoke-direct {p0, v3, p1, p2, v4}, Lcom/android/server/wm/DragState;->sendDragStartedLw(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V

    #@39
    .line 211
    add-int/lit8 v1, v1, 0x1

    #@3b
    goto :goto_2c

    #@3c
    .line 200
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #windows:Lcom/android/server/wm/WindowList;
    :cond_3c
    const/4 v3, 0x0

    #@3d
    goto :goto_13

    #@3e
    .line 215
    .restart local v2       #windows:Lcom/android/server/wm/WindowList;
    :cond_3e
    return-void
.end method

.method dropFailAnimation(Landroid/graphics/PointF;Landroid/graphics/PointF;FFLandroid/view/Surface;Lcom/android/server/wm/WindowManagerService;)V
    .registers 15
    .parameter "startP"
    .parameter "endP"
    .parameter "offsetX"
    .parameter "offsetY"
    .parameter "surface"
    .parameter "service"

    #@0
    .prologue
    .line 330
    new-instance v0, Lcom/android/server/wm/DragState$1;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p2

    #@4
    move-object v3, p1

    #@5
    move-object v4, p5

    #@6
    move v5, p3

    #@7
    move v6, p4

    #@8
    move-object v7, p6

    #@9
    invoke-direct/range {v0 .. v7}, Lcom/android/server/wm/DragState$1;-><init>(Lcom/android/server/wm/DragState;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/view/Surface;FFLcom/android/server/wm/WindowManagerService;)V

    #@c
    .line 359
    .local v0, t:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@f
    .line 360
    return-void
.end method

.method endDragLw()V
    .registers 3

    #@0
    .prologue
    .line 295
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@4
    invoke-virtual {v0}, Lcom/android/server/wm/DragState;->broadcastDragEndedLw()V

    #@7
    .line 298
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@b
    invoke-virtual {v0}, Lcom/android/server/wm/DragState;->unregister()V

    #@e
    .line 299
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@10
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputMonitor:Lcom/android/server/wm/InputMonitor;

    #@12
    const/4 v1, 0x1

    #@13
    invoke-virtual {v0, v1}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@16
    .line 302
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@18
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@1a
    invoke-virtual {v0}, Lcom/android/server/wm/DragState;->reset()V

    #@1d
    .line 303
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1f
    const/4 v1, 0x0

    #@20
    iput-object v1, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@22
    .line 304
    return-void
.end method

.method endDragLw(Lcom/android/server/wm/WindowState;)V
    .registers 10
    .parameter "win"

    #@0
    .prologue
    .line 307
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@4
    invoke-virtual {v0}, Lcom/android/server/wm/DragState;->broadcastDragEndedLw()V

    #@7
    .line 310
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@b
    invoke-virtual {v0}, Lcom/android/server/wm/DragState;->unregister()V

    #@e
    .line 311
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@10
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputMonitor:Lcom/android/server/wm/InputMonitor;

    #@12
    const/4 v1, 0x1

    #@13
    invoke-virtual {v0, v1}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@16
    .line 315
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@18
    if-eqz v0, :cond_3b

    #@1a
    if-eqz p1, :cond_3b

    #@1c
    iget-boolean v0, p0, Lcom/android/server/wm/DragState;->mDragResult:Z

    #@1e
    if-nez v0, :cond_3b

    #@20
    .line 316
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@22
    invoke-interface {v0}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@25
    move-result-object v7

    #@26
    .line 317
    .local v7, winBinder:Landroid/os/IBinder;
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mLocalWin:Landroid/os/IBinder;

    #@28
    if-eq v7, v0, :cond_3b

    #@2a
    .line 318
    iget-object v1, p0, Lcom/android/server/wm/DragState;->startP:Landroid/graphics/PointF;

    #@2c
    iget-object v2, p0, Lcom/android/server/wm/DragState;->endP:Landroid/graphics/PointF;

    #@2e
    iget v3, p0, Lcom/android/server/wm/DragState;->mThumbOffsetX:F

    #@30
    iget v4, p0, Lcom/android/server/wm/DragState;->mThumbOffsetY:F

    #@32
    iget-object v5, p0, Lcom/android/server/wm/DragState;->mSurface:Landroid/view/Surface;

    #@34
    iget-object v6, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@36
    move-object v0, p0

    #@37
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/wm/DragState;->dropFailAnimation(Landroid/graphics/PointF;Landroid/graphics/PointF;FFLandroid/view/Surface;Lcom/android/server/wm/WindowManagerService;)V

    #@3a
    .line 326
    .end local v7           #winBinder:Landroid/os/IBinder;
    :goto_3a
    return-void

    #@3b
    .line 324
    :cond_3b
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@3d
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@3f
    invoke-virtual {v0}, Lcom/android/server/wm/DragState;->reset()V

    #@42
    .line 325
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@44
    const/4 v1, 0x0

    #@45
    iput-object v1, v0, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@47
    goto :goto_3a
.end method

.method getDragLayerLw()I
    .registers 3

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    const/16 v1, 0x7e0

    #@6
    invoke-interface {v0, v1}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    #@9
    move-result v0

    #@a
    mul-int/lit16 v0, v0, 0x2710

    #@c
    add-int/lit16 v0, v0, 0x3e8

    #@e
    return v0
.end method

.method notifyDropLw(FF)Z
    .registers 16
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 430
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/DragState;->getTouchedWinAtPointLw(FF)Lcom/android/server/wm/WindowState;

    #@3
    move-result-object v0

    #@4
    .line 431
    .local v0, touchedWin:Lcom/android/server/wm/WindowState;
    if-nez v0, :cond_b

    #@6
    .line 434
    const/4 v1, 0x0

    #@7
    iput-boolean v1, p0, Lcom/android/server/wm/DragState;->mDragResult:Z

    #@9
    .line 435
    const/4 v1, 0x1

    #@a
    .line 466
    :cond_a
    :goto_a
    return v1

    #@b
    .line 441
    :cond_b
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@e
    move-result v11

    #@f
    .line 442
    .local v11, myPid:I
    iget-object v1, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@11
    invoke-interface {v1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v12

    #@15
    .line 443
    .local v12, token:Landroid/os/IBinder;
    const/4 v1, 0x3

    #@16
    const/4 v4, 0x0

    #@17
    const/4 v5, 0x0

    #@18
    iget-object v6, p0, Lcom/android/server/wm/DragState;->mData:Landroid/content/ClipData;

    #@1a
    const/4 v7, 0x0

    #@1b
    move v2, p1

    #@1c
    move v3, p2

    #@1d
    invoke-static/range {v0 .. v7}, Lcom/android/server/wm/DragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@20
    move-result-object v9

    #@21
    .line 446
    .local v9, evt:Landroid/view/DragEvent;
    :try_start_21
    iget-object v1, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@23
    invoke-interface {v1, v9}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    #@26
    .line 448
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@28
    if-eqz v1, :cond_2f

    #@2a
    .line 449
    iget-object v1, p0, Lcom/android/server/wm/DragState;->endP:Landroid/graphics/PointF;

    #@2c
    invoke-virtual {v1, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    #@2f
    .line 454
    :cond_2f
    iget-object v1, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@31
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@33
    const/16 v2, 0x15

    #@35
    invoke-virtual {v1, v2, v12}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(ILjava/lang/Object;)V

    #@38
    .line 455
    iget-object v1, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@3a
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@3c
    const/16 v2, 0x15

    #@3e
    invoke-virtual {v1, v2, v12}, Lcom/android/server/wm/WindowManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@41
    move-result-object v10

    #@42
    .line 456
    .local v10, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@44
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@46
    const-wide/16 v2, 0x1388

    #@48
    invoke-virtual {v1, v10, v2, v3}, Lcom/android/server/wm/WindowManagerService$H;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_4b
    .catchall {:try_start_21 .. :try_end_4b} :catchall_7c
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_4b} :catch_58

    #@4b
    .line 461
    iget-object v1, v0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@4d
    iget v1, v1, Lcom/android/server/wm/Session;->mPid:I

    #@4f
    if-eq v11, v1, :cond_54

    #@51
    .line 462
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V

    #@54
    .line 465
    :cond_54
    iput-object v12, p0, Lcom/android/server/wm/DragState;->mToken:Landroid/os/IBinder;

    #@56
    .line 466
    const/4 v1, 0x0

    #@57
    goto :goto_a

    #@58
    .line 457
    .end local v10           #msg:Landroid/os/Message;
    :catch_58
    move-exception v8

    #@59
    .line 458
    .local v8, e:Landroid/os/RemoteException;
    :try_start_59
    const-string v1, "WindowManager"

    #@5b
    new-instance v2, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v3, "can\'t send drop notification to win "

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_71
    .catchall {:try_start_59 .. :try_end_71} :catchall_7c

    #@71
    .line 459
    const/4 v1, 0x1

    #@72
    .line 461
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@74
    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    #@76
    if-eq v11, v2, :cond_a

    #@78
    .line 462
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V

    #@7b
    goto :goto_a

    #@7c
    .line 461
    .end local v8           #e:Landroid/os/RemoteException;
    :catchall_7c
    move-exception v1

    #@7d
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@7f
    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    #@81
    if-eq v11, v2, :cond_86

    #@83
    .line 462
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V

    #@86
    :cond_86
    throw v1
.end method

.method notifyMoveLw(FF)V
    .registers 16
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 364
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@3
    move-result v10

    #@4
    .line 369
    .local v10, myPid:I
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@7
    .line 371
    :try_start_7
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mSurface:Landroid/view/Surface;

    #@9
    iget v1, p0, Lcom/android/server/wm/DragState;->mThumbOffsetX:F

    #@b
    sub-float v1, p1, v1

    #@d
    iget v2, p0, Lcom/android/server/wm/DragState;->mThumbOffsetY:F

    #@f
    sub-float v2, p2, v2

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/view/Surface;->setPosition(FF)V
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_1e

    #@14
    .line 376
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@17
    .line 382
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/DragState;->getTouchedWinAtPointLw(FF)Lcom/android/server/wm/WindowState;

    #@1a
    move-result-object v12

    #@1b
    .line 383
    .local v12, touchedWin:Lcom/android/server/wm/WindowState;
    if-nez v12, :cond_23

    #@1d
    .line 424
    :goto_1d
    return-void

    #@1e
    .line 376
    .end local v12           #touchedWin:Lcom/android/server/wm/WindowState;
    :catchall_1e
    move-exception v0

    #@1f
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@22
    throw v0

    #@23
    .line 387
    .restart local v12       #touchedWin:Lcom/android/server/wm/WindowState;
    :cond_23
    iget v0, p0, Lcom/android/server/wm/DragState;->mFlags:I

    #@25
    and-int/lit8 v0, v0, 0x1

    #@27
    if-nez v0, :cond_34

    #@29
    .line 388
    iget-object v0, v12, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@2b
    invoke-interface {v0}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@2e
    move-result-object v11

    #@2f
    .line 389
    .local v11, touchedBinder:Landroid/os/IBinder;
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mLocalWin:Landroid/os/IBinder;

    #@31
    if-eq v11, v0, :cond_34

    #@33
    .line 392
    const/4 v12, 0x0

    #@34
    .line 397
    .end local v11           #touchedBinder:Landroid/os/IBinder;
    :cond_34
    :try_start_34
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    #@36
    if-eq v12, v0, :cond_5b

    #@38
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    #@3a
    if-eqz v0, :cond_5b

    #@3c
    .line 402
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    #@3e
    const/4 v1, 0x6

    #@3f
    const/4 v4, 0x0

    #@40
    const/4 v5, 0x0

    #@41
    const/4 v6, 0x0

    #@42
    const/4 v7, 0x0

    #@43
    move v2, p1

    #@44
    move v3, p2

    #@45
    invoke-static/range {v0 .. v7}, Lcom/android/server/wm/DragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@48
    move-result-object v9

    #@49
    .line 404
    .local v9, evt:Landroid/view/DragEvent;
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    #@4b
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@4d
    invoke-interface {v0, v9}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    #@50
    .line 405
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    #@52
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@54
    iget v0, v0, Lcom/android/server/wm/Session;->mPid:I

    #@56
    if-eq v10, v0, :cond_5b

    #@58
    .line 406
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V

    #@5b
    .line 409
    .end local v9           #evt:Landroid/view/DragEvent;
    :cond_5b
    if-eqz v12, :cond_77

    #@5d
    .line 413
    const/4 v1, 0x2

    #@5e
    const/4 v4, 0x0

    #@5f
    const/4 v5, 0x0

    #@60
    const/4 v6, 0x0

    #@61
    const/4 v7, 0x0

    #@62
    move-object v0, v12

    #@63
    move v2, p1

    #@64
    move v3, p2

    #@65
    invoke-static/range {v0 .. v7}, Lcom/android/server/wm/DragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@68
    move-result-object v9

    #@69
    .line 415
    .restart local v9       #evt:Landroid/view/DragEvent;
    iget-object v0, v12, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    #@6b
    invoke-interface {v0, v9}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    #@6e
    .line 416
    iget-object v0, v12, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@70
    iget v0, v0, Lcom/android/server/wm/Session;->mPid:I

    #@72
    if-eq v10, v0, :cond_77

    #@74
    .line 417
    invoke-virtual {v9}, Landroid/view/DragEvent;->recycle()V
    :try_end_77
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_77} :catch_7a

    #@77
    .line 423
    .end local v9           #evt:Landroid/view/DragEvent;
    :cond_77
    :goto_77
    iput-object v12, p0, Lcom/android/server/wm/DragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    #@79
    goto :goto_1d

    #@7a
    .line 420
    :catch_7a
    move-exception v8

    #@7b
    .line 421
    .local v8, e:Landroid/os/RemoteException;
    const-string v0, "WindowManager"

    #@7d
    const-string v1, "can\'t send drag notification to windows"

    #@7f
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_77
.end method

.method register(Landroid/view/Display;)V
    .registers 13
    .parameter "display"

    #@0
    .prologue
    const-wide v9, 0x12a05f200L

    #@5
    const/4 v8, 0x0

    #@6
    const/4 v7, 0x1

    #@7
    const/4 v6, 0x0

    #@8
    .line 104
    iput-object p1, p0, Lcom/android/server/wm/DragState;->mDisplay:Landroid/view/Display;

    #@a
    .line 106
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mClientChannel:Landroid/view/InputChannel;

    #@c
    if-eqz v2, :cond_16

    #@e
    .line 107
    const-string v2, "WindowManager"

    #@10
    const-string v3, "Duplicate register of drag input channel"

    #@12
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 157
    :goto_15
    return-void

    #@16
    .line 109
    :cond_16
    const-string v2, "drag"

    #@18
    invoke-static {v2}, Landroid/view/InputChannel;->openInputChannelPair(Ljava/lang/String;)[Landroid/view/InputChannel;

    #@1b
    move-result-object v0

    #@1c
    .line 110
    .local v0, channels:[Landroid/view/InputChannel;
    aget-object v2, v0, v6

    #@1e
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mServerChannel:Landroid/view/InputChannel;

    #@20
    .line 111
    aget-object v2, v0, v7

    #@22
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mClientChannel:Landroid/view/InputChannel;

    #@24
    .line 112
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@26
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@28
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mServerChannel:Landroid/view/InputChannel;

    #@2a
    invoke-virtual {v2, v3, v8}, Lcom/android/server/input/InputManagerService;->registerInputChannel(Landroid/view/InputChannel;Lcom/android/server/input/InputWindowHandle;)V

    #@2d
    .line 113
    new-instance v2, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;

    #@2f
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@31
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@34
    iget-object v4, p0, Lcom/android/server/wm/DragState;->mClientChannel:Landroid/view/InputChannel;

    #@36
    iget-object v5, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@38
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@3a
    invoke-virtual {v5}, Lcom/android/server/wm/WindowManagerService$H;->getLooper()Landroid/os/Looper;

    #@3d
    move-result-object v5

    #@3e
    invoke-direct {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@41
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mInputEventReceiver:Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;

    #@43
    .line 116
    new-instance v2, Lcom/android/server/input/InputApplicationHandle;

    #@45
    invoke-direct {v2, v8}, Lcom/android/server/input/InputApplicationHandle;-><init>(Ljava/lang/Object;)V

    #@48
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mDragApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@4a
    .line 117
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@4c
    const-string v3, "drag"

    #@4e
    iput-object v3, v2, Lcom/android/server/input/InputApplicationHandle;->name:Ljava/lang/String;

    #@50
    .line 118
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@52
    iput-wide v9, v2, Lcom/android/server/input/InputApplicationHandle;->dispatchingTimeoutNanos:J

    #@54
    .line 121
    new-instance v2, Lcom/android/server/input/InputWindowHandle;

    #@56
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mDragApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@58
    iget-object v4, p0, Lcom/android/server/wm/DragState;->mDisplay:Landroid/view/Display;

    #@5a
    invoke-virtual {v4}, Landroid/view/Display;->getDisplayId()I

    #@5d
    move-result v4

    #@5e
    invoke-direct {v2, v3, v8, v4}, Lcom/android/server/input/InputWindowHandle;-><init>(Lcom/android/server/input/InputApplicationHandle;Ljava/lang/Object;I)V

    #@61
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@63
    .line 123
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@65
    const-string v3, "drag"

    #@67
    iput-object v3, v2, Lcom/android/server/input/InputWindowHandle;->name:Ljava/lang/String;

    #@69
    .line 124
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@6b
    iget-object v3, p0, Lcom/android/server/wm/DragState;->mServerChannel:Landroid/view/InputChannel;

    #@6d
    iput-object v3, v2, Lcom/android/server/input/InputWindowHandle;->inputChannel:Landroid/view/InputChannel;

    #@6f
    .line 125
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@71
    invoke-virtual {p0}, Lcom/android/server/wm/DragState;->getDragLayerLw()I

    #@74
    move-result v3

    #@75
    iput v3, v2, Lcom/android/server/input/InputWindowHandle;->layer:I

    #@77
    .line 126
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@79
    iput v6, v2, Lcom/android/server/input/InputWindowHandle;->layoutParamsFlags:I

    #@7b
    .line 127
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@7d
    const/16 v3, 0x7e0

    #@7f
    iput v3, v2, Lcom/android/server/input/InputWindowHandle;->layoutParamsType:I

    #@81
    .line 128
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@83
    iput-wide v9, v2, Lcom/android/server/input/InputWindowHandle;->dispatchingTimeoutNanos:J

    #@85
    .line 130
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@87
    iput-boolean v7, v2, Lcom/android/server/input/InputWindowHandle;->visible:Z

    #@89
    .line 131
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@8b
    iput-boolean v6, v2, Lcom/android/server/input/InputWindowHandle;->canReceiveKeys:Z

    #@8d
    .line 132
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@8f
    iput-boolean v7, v2, Lcom/android/server/input/InputWindowHandle;->hasFocus:Z

    #@91
    .line 133
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@93
    iput-boolean v6, v2, Lcom/android/server/input/InputWindowHandle;->hasWallpaper:Z

    #@95
    .line 134
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@97
    iput-boolean v6, v2, Lcom/android/server/input/InputWindowHandle;->paused:Z

    #@99
    .line 135
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@9b
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@9e
    move-result v3

    #@9f
    iput v3, v2, Lcom/android/server/input/InputWindowHandle;->ownerPid:I

    #@a1
    .line 136
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@a3
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@a6
    move-result v3

    #@a7
    iput v3, v2, Lcom/android/server/input/InputWindowHandle;->ownerUid:I

    #@a9
    .line 137
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@ab
    iput v6, v2, Lcom/android/server/input/InputWindowHandle;->inputFeatures:I

    #@ad
    .line 138
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@af
    const/high16 v3, 0x3f80

    #@b1
    iput v3, v2, Lcom/android/server/input/InputWindowHandle;->scaleFactor:F

    #@b3
    .line 141
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@b5
    iget-object v2, v2, Lcom/android/server/input/InputWindowHandle;->touchableRegion:Landroid/graphics/Region;

    #@b7
    invoke-virtual {v2}, Landroid/graphics/Region;->setEmpty()V

    #@ba
    .line 144
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@bc
    iput v6, v2, Lcom/android/server/input/InputWindowHandle;->frameLeft:I

    #@be
    .line 145
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@c0
    iput v6, v2, Lcom/android/server/input/InputWindowHandle;->frameTop:I

    #@c2
    .line 146
    new-instance v1, Landroid/graphics/Point;

    #@c4
    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    #@c7
    .line 147
    .local v1, p:Landroid/graphics/Point;
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDisplay:Landroid/view/Display;

    #@c9
    invoke-virtual {v2, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@cc
    .line 148
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@ce
    iget v3, v1, Landroid/graphics/Point;->x:I

    #@d0
    iput v3, v2, Lcom/android/server/input/InputWindowHandle;->frameRight:I

    #@d2
    .line 149
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@d4
    iget v3, v1, Landroid/graphics/Point;->y:I

    #@d6
    iput v3, v2, Lcom/android/server/input/InputWindowHandle;->frameBottom:I

    #@d8
    .line 155
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@da
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->pauseRotationLocked()V

    #@dd
    goto/16 :goto_15
.end method

.method reset()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 88
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mSurface:Landroid/view/Surface;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 89
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mSurface:Landroid/view/Surface;

    #@7
    invoke-virtual {v0}, Landroid/view/Surface;->destroy()V

    #@a
    .line 91
    :cond_a
    iput-object v1, p0, Lcom/android/server/wm/DragState;->mSurface:Landroid/view/Surface;

    #@c
    .line 92
    const/4 v0, 0x0

    #@d
    iput v0, p0, Lcom/android/server/wm/DragState;->mFlags:I

    #@f
    .line 93
    iput-object v1, p0, Lcom/android/server/wm/DragState;->mLocalWin:Landroid/os/IBinder;

    #@11
    .line 94
    iput-object v1, p0, Lcom/android/server/wm/DragState;->mToken:Landroid/os/IBinder;

    #@13
    .line 95
    iput-object v1, p0, Lcom/android/server/wm/DragState;->mData:Landroid/content/ClipData;

    #@15
    .line 96
    const/4 v0, 0x0

    #@16
    iput v0, p0, Lcom/android/server/wm/DragState;->mThumbOffsetY:F

    #@18
    iput v0, p0, Lcom/android/server/wm/DragState;->mThumbOffsetX:F

    #@1a
    .line 97
    iput-object v1, p0, Lcom/android/server/wm/DragState;->mNotifiedWindows:Ljava/util/ArrayList;

    #@1c
    .line 98
    return-void
.end method

.method sendDragStartedIfNeededLw(Lcom/android/server/wm/WindowState;)V
    .registers 7
    .parameter "newWin"

    #@0
    .prologue
    .line 262
    iget-boolean v2, p0, Lcom/android/server/wm/DragState;->mDragInProgress:Z

    #@2
    if-eqz v2, :cond_18

    #@4
    .line 264
    iget-object v2, p0, Lcom/android/server/wm/DragState;->mNotifiedWindows:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_19

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/server/wm/WindowState;

    #@16
    .line 265
    .local v1, ws:Lcom/android/server/wm/WindowState;
    if-ne v1, p1, :cond_a

    #@18
    .line 274
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #ws:Lcom/android/server/wm/WindowState;
    :cond_18
    :goto_18
    return-void

    #@19
    .line 272
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_19
    iget v2, p0, Lcom/android/server/wm/DragState;->mCurrentX:F

    #@1b
    iget v3, p0, Lcom/android/server/wm/DragState;->mCurrentY:F

    #@1d
    iget-object v4, p0, Lcom/android/server/wm/DragState;->mDataDescription:Landroid/content/ClipDescription;

    #@1f
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/wm/DragState;->sendDragStartedLw(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V

    #@22
    goto :goto_18
.end method

.method unregister()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 161
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mClientChannel:Landroid/view/InputChannel;

    #@3
    if-nez v0, :cond_d

    #@5
    .line 162
    const-string v0, "WindowManager"

    #@7
    const-string v1, "Unregister of nonexistent drag input channel"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 181
    :goto_c
    return-void

    #@d
    .line 164
    :cond_d
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@11
    iget-object v1, p0, Lcom/android/server/wm/DragState;->mServerChannel:Landroid/view/InputChannel;

    #@13
    invoke-virtual {v0, v1}, Lcom/android/server/input/InputManagerService;->unregisterInputChannel(Landroid/view/InputChannel;)V

    #@16
    .line 165
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mInputEventReceiver:Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;

    #@18
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;->dispose()V

    #@1b
    .line 166
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mInputEventReceiver:Lcom/android/server/wm/WindowManagerService$DragInputEventReceiver;

    #@1d
    .line 167
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mClientChannel:Landroid/view/InputChannel;

    #@1f
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@22
    .line 168
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mServerChannel:Landroid/view/InputChannel;

    #@24
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@27
    .line 169
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mClientChannel:Landroid/view/InputChannel;

    #@29
    .line 170
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mServerChannel:Landroid/view/InputChannel;

    #@2b
    .line 172
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@2d
    .line 173
    iput-object v2, p0, Lcom/android/server/wm/DragState;->mDragApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@2f
    .line 179
    iget-object v0, p0, Lcom/android/server/wm/DragState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@31
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->resumeRotationLocked()V

    #@34
    goto :goto_c
.end method
