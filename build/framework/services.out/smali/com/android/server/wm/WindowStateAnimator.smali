.class Lcom/android/server/wm/WindowStateAnimator;
.super Ljava/lang/Object;
.source "WindowStateAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/WindowStateAnimator$SurfaceTrace;
    }
.end annotation


# static fields
.field static final COMMIT_DRAW_PENDING:I = 0x2

.field static final DEBUG_ANIM:Z = false

.field static final DEBUG_LAYERS:Z = false

.field static final DEBUG_ORIENTATION:Z = false

.field static final DEBUG_STARTING_WINDOW:Z = false

.field static final DEBUG_SURFACE_TRACE:Z = false

.field static final DEBUG_VISIBILITY:Z = false

.field static final DRAW_PENDING:I = 0x1

.field static final HAS_DRAWN:I = 0x4

.field static final NO_SURFACE:I = 0x0

.field static final READY_TO_SHOW:I = 0x3

.field static final SHOW_LIGHT_TRANSACTIONS:Z = false

.field static final SHOW_SURFACE_ALLOC:Z = false

.field static final SHOW_TRANSACTIONS:Z = false

.field static final TAG:Ljava/lang/String; = "WindowStateAnimator"

.field static final localLOGV:Z


# instance fields
.field mAlpha:F

.field mAnimDh:I

.field mAnimDw:I

.field mAnimLayer:I

.field mAnimating:Z

.field mAnimation:Landroid/view/animation/Animation;

.field mAnimationIsEntrance:Z

.field final mAnimator:Lcom/android/server/wm/WindowAnimator;

.field mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

.field final mAttachedWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

.field mAttrFlags:I

.field mAttrType:I

.field final mContext:Landroid/content/Context;

.field mDrawState:I

.field mDsDx:F

.field mDsDy:F

.field mDtDx:F

.field mDtDy:F

.field mEnterAnimationPending:Z

.field mHasLocalTransformation:Z

.field mHasTransformation:Z

.field mHaveMatrix:Z

.field final mIsWallpaper:Z

.field mLastAlpha:F

.field mLastDsDx:F

.field mLastDsDy:F

.field mLastDtDx:F

.field mLastDtDy:F

.field mLastHidden:Z

.field mLastLayer:I

.field final mLayerStack:I

.field mLocalAnimating:Z

.field mPendingDestroySurface:Landroid/view/Surface;

.field final mPolicy:Landroid/view/WindowManagerPolicy;

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field final mSession:Lcom/android/server/wm/Session;

.field mShownAlpha:F

.field mSurface:Landroid/view/Surface;

.field mSurfaceAlpha:F

.field mSurfaceDestroyDeferred:Z

.field mSurfaceH:F

.field mSurfaceLayer:I

.field mSurfaceResized:Z

.field mSurfaceShown:Z

.field mSurfaceW:F

.field mSurfaceX:F

.field mSurfaceY:F

.field final mTransformation:Landroid/view/animation/Transformation;

.field final mUniverseTransform:Landroid/view/animation/Transformation;

.field mWasAnimating:Z

.field final mWin:Lcom/android/server/wm/WindowState;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowState;)V
    .registers 8
    .parameter "win"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/high16 v5, 0x3f80

    #@3
    const/4 v4, 0x0

    #@4
    .line 162
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 76
    new-instance v2, Landroid/view/animation/Transformation;

    #@9
    invoke-direct {v2}, Landroid/view/animation/Transformation;-><init>()V

    #@c
    iput-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mUniverseTransform:Landroid/view/animation/Transformation;

    #@e
    .line 85
    new-instance v2, Landroid/view/animation/Transformation;

    #@10
    invoke-direct {v2}, Landroid/view/animation/Transformation;-><init>()V

    #@13
    iput-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@15
    .line 105
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@17
    .line 106
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAlpha:F

    #@19
    .line 107
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastAlpha:F

    #@1b
    .line 113
    iput v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@1d
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@1f
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@21
    iput v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@23
    .line 114
    iput v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDsDx:F

    #@25
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDtDx:F

    #@27
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDsDy:F

    #@29
    iput v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDtDy:F

    #@2b
    .line 163
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2d
    .line 165
    .local v1, service:Lcom/android/server/wm/WindowManagerService;
    iput-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2f
    .line 166
    iget-object v2, v1, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@31
    iput-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@33
    .line 167
    iget-object v2, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@35
    iput-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@37
    .line 168
    iget-object v2, v1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@39
    iput-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mContext:Landroid/content/Context;

    #@3b
    .line 169
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@3d
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@40
    move-result-object v0

    #@41
    .line 170
    .local v0, displayInfo:Landroid/view/DisplayInfo;
    iget v2, v0, Landroid/view/DisplayInfo;->appWidth:I

    #@43
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimDw:I

    #@45
    .line 171
    iget v2, v0, Landroid/view/DisplayInfo;->appHeight:I

    #@47
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimDh:I

    #@49
    .line 173
    iput-object p1, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@4b
    .line 174
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@4d
    if-nez v2, :cond_79

    #@4f
    move-object v2, v3

    #@50
    :goto_50
    iput-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAttachedWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@52
    .line 176
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@54
    if-nez v2, :cond_7e

    #@56
    :goto_56
    iput-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@58
    .line 177
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@5a
    iput-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mSession:Lcom/android/server/wm/Session;

    #@5c
    .line 178
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@5e
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@60
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAttrFlags:I

    #@62
    .line 179
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@64
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@66
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAttrType:I

    #@68
    .line 180
    iget-boolean v2, p1, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    #@6a
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mIsWallpaper:Z

    #@6c
    .line 181
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@6e
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v2}, Landroid/view/Display;->getLayerStack()I

    #@75
    move-result v2

    #@76
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mLayerStack:I

    #@78
    .line 182
    return-void

    #@79
    .line 174
    :cond_79
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAttachedWindow:Lcom/android/server/wm/WindowState;

    #@7b
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@7d
    goto :goto_50

    #@7e
    .line 176
    :cond_7e
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@80
    iget-object v3, v2, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@82
    goto :goto_56
.end method

.method static drawStateToString(I)Ljava/lang/String;
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 143
    packed-switch p0, :pswitch_data_18

    #@3
    .line 149
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 144
    :pswitch_8
    const-string v0, "NO_SURFACE"

    #@a
    goto :goto_7

    #@b
    .line 145
    :pswitch_b
    const-string v0, "DRAW_PENDING"

    #@d
    goto :goto_7

    #@e
    .line 146
    :pswitch_e
    const-string v0, "COMMIT_DRAW_PENDING"

    #@10
    goto :goto_7

    #@11
    .line 147
    :pswitch_11
    const-string v0, "READY_TO_SHOW"

    #@13
    goto :goto_7

    #@14
    .line 148
    :pswitch_14
    const-string v0, "HAS_DRAWN"

    #@16
    goto :goto_7

    #@17
    .line 143
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
    .end packed-switch
.end method

.method private stepAnimation(J)Z
    .registers 6
    .parameter "currentTime"

    #@0
    .prologue
    .line 236
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@2
    if-eqz v1, :cond_8

    #@4
    iget-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@6
    if-nez v1, :cond_a

    #@8
    .line 237
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 244
    :goto_9
    return v0

    #@a
    .line 239
    :cond_a
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@c
    invoke-virtual {v1}, Landroid/view/animation/Transformation;->clear()V

    #@f
    .line 240
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@11
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@13
    invoke-virtual {v1, p1, p2, v2}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    #@16
    move-result v0

    #@17
    .line 244
    .local v0, more:Z
    goto :goto_9
.end method


# virtual methods
.method applyAnimationLocked(IZ)Z
    .registers 10
    .parameter "transit"
    .parameter "isEntrance"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1528
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@3
    if-eqz v4, :cond_a

    #@5
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimationIsEntrance:Z

    #@7
    if-ne v4, p2, :cond_a

    #@9
    .line 1586
    :cond_9
    :goto_9
    return v3

    #@a
    .line 1538
    :cond_a
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@c
    invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->okToDisplay()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_56

    #@12
    .line 1539
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@14
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@16
    invoke-interface {v4, v5, p1}, Landroid/view/WindowManagerPolicy;->selectAnimationLw(Landroid/view/WindowManagerPolicy$WindowState;I)I

    #@19
    move-result v1

    #@1a
    .line 1540
    .local v1, anim:I
    const/4 v2, -0x1

    #@1b
    .line 1541
    .local v2, attr:I
    const/4 v0, 0x0

    #@1c
    .line 1542
    .local v0, a:Landroid/view/animation/Animation;
    if-eqz v1, :cond_36

    #@1e
    .line 1543
    const/4 v4, -0x1

    #@1f
    if-eq v1, v4, :cond_34

    #@21
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mContext:Landroid/content/Context;

    #@23
    invoke-static {v4, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@26
    move-result-object v0

    #@27
    .line 1570
    :cond_27
    :goto_27
    if-eqz v0, :cond_2e

    #@29
    .line 1579
    invoke-virtual {p0, v0}, Lcom/android/server/wm/WindowStateAnimator;->setAnimation(Landroid/view/animation/Animation;)V

    #@2c
    .line 1580
    iput-boolean p2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimationIsEntrance:Z

    #@2e
    .line 1586
    .end local v0           #a:Landroid/view/animation/Animation;
    .end local v1           #anim:I
    .end local v2           #attr:I
    :cond_2e
    :goto_2e
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@30
    if-nez v4, :cond_9

    #@32
    const/4 v3, 0x0

    #@33
    goto :goto_9

    #@34
    .line 1543
    .restart local v0       #a:Landroid/view/animation/Animation;
    .restart local v1       #anim:I
    .restart local v2       #attr:I
    :cond_34
    const/4 v0, 0x0

    #@35
    goto :goto_27

    #@36
    .line 1545
    :cond_36
    sparse-switch p1, :sswitch_data_5a

    #@39
    .line 1559
    :goto_39
    if-ltz v2, :cond_27

    #@3b
    .line 1560
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@3d
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@3f
    iget v5, v5, Lcom/android/server/wm/WindowState;->mOwnerUid:I

    #@41
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    #@44
    move-result v5

    #@45
    iget-object v6, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@47
    iget-object v6, v6, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@49
    invoke-virtual {v4, v5, v6, v2}, Lcom/android/server/wm/WindowManagerService;->loadAnimation(ILandroid/view/WindowManager$LayoutParams;I)Landroid/view/animation/Animation;

    #@4c
    move-result-object v0

    #@4d
    goto :goto_27

    #@4e
    .line 1547
    :sswitch_4e
    const/4 v2, 0x0

    #@4f
    .line 1548
    goto :goto_39

    #@50
    .line 1550
    :sswitch_50
    const/4 v2, 0x1

    #@51
    .line 1551
    goto :goto_39

    #@52
    .line 1553
    :sswitch_52
    const/4 v2, 0x2

    #@53
    .line 1554
    goto :goto_39

    #@54
    .line 1556
    :sswitch_54
    const/4 v2, 0x3

    #@55
    goto :goto_39

    #@56
    .line 1583
    .end local v0           #a:Landroid/view/animation/Animation;
    .end local v1           #anim:I
    .end local v2           #attr:I
    :cond_56
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->clearAnimation()V

    #@59
    goto :goto_2e

    #@5a
    .line 1545
    :sswitch_data_5a
    .sparse-switch
        0x1001 -> :sswitch_4e
        0x1003 -> :sswitch_52
        0x2002 -> :sswitch_50
        0x2004 -> :sswitch_54
    .end sparse-switch
.end method

.method applyDecorRect(Landroid/graphics/Rect;)V
    .registers 12
    .parameter "decorRect"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/high16 v9, 0x3f00

    #@3
    .line 1056
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5
    .line 1058
    .local v3, w:Lcom/android/server/wm/WindowState;
    iget v4, v3, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@7
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@9
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@b
    add-int v0, v4, v5

    #@d
    .line 1059
    .local v0, offX:I
    iget v4, v3, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@f
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@11
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@13
    add-int v1, v4, v5

    #@15
    .line 1061
    .local v1, offY:I
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@17
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@19
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@1c
    move-result v5

    #@1d
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@1f
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    #@22
    move-result v6

    #@23
    invoke-virtual {v4, v7, v7, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    #@26
    .line 1063
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@28
    iget v5, p1, Landroid/graphics/Rect;->left:I

    #@2a
    sub-int/2addr v5, v0

    #@2b
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@2d
    sub-int/2addr v6, v1

    #@2e
    iget v7, p1, Landroid/graphics/Rect;->right:I

    #@30
    sub-int/2addr v7, v0

    #@31
    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    #@33
    sub-int/2addr v8, v1

    #@34
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@37
    .line 1071
    iget-boolean v4, v3, Lcom/android/server/wm/WindowState;->mEnforceSizeCompat:Z

    #@39
    if-eqz v4, :cond_79

    #@3b
    iget v4, v3, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@3d
    const/high16 v5, 0x3f80

    #@3f
    cmpl-float v4, v4, v5

    #@41
    if-eqz v4, :cond_79

    #@43
    .line 1072
    iget v2, v3, Lcom/android/server/wm/WindowState;->mInvGlobalScale:F

    #@45
    .line 1073
    .local v2, scale:F
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@47
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@49
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@4b
    int-to-float v5, v5

    #@4c
    mul-float/2addr v5, v2

    #@4d
    sub-float/2addr v5, v9

    #@4e
    float-to-int v5, v5

    #@4f
    iput v5, v4, Landroid/graphics/Rect;->left:I

    #@51
    .line 1074
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@53
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@55
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@57
    int-to-float v5, v5

    #@58
    mul-float/2addr v5, v2

    #@59
    sub-float/2addr v5, v9

    #@5a
    float-to-int v5, v5

    #@5b
    iput v5, v4, Landroid/graphics/Rect;->top:I

    #@5d
    .line 1075
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@5f
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@61
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@63
    add-int/lit8 v5, v5, 0x1

    #@65
    int-to-float v5, v5

    #@66
    mul-float/2addr v5, v2

    #@67
    sub-float/2addr v5, v9

    #@68
    float-to-int v5, v5

    #@69
    iput v5, v4, Landroid/graphics/Rect;->right:I

    #@6b
    .line 1076
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@6d
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@6f
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@71
    add-int/lit8 v5, v5, 0x1

    #@73
    int-to-float v5, v5

    #@74
    mul-float/2addr v5, v2

    #@75
    sub-float/2addr v5, v9

    #@76
    float-to-int v5, v5

    #@77
    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    #@79
    .line 1078
    .end local v2           #scale:F
    :cond_79
    return-void
.end method

.method applyEnterAnimationLocked()V
    .registers 4

    #@0
    .prologue
    .line 1507
    iget-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mEnterAnimationPending:Z

    #@2
    if-eqz v1, :cond_15

    #@4
    .line 1508
    const/4 v1, 0x0

    #@5
    iput-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mEnterAnimationPending:Z

    #@7
    .line 1509
    const/16 v0, 0x1001

    #@9
    .line 1513
    .local v0, transit:I
    :goto_9
    const/4 v1, 0x1

    #@a
    invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/WindowStateAnimator;->applyAnimationLocked(IZ)Z

    #@d
    .line 1514
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@11
    invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->scheduleNotifyWindowTranstionIfNeededLocked(Lcom/android/server/wm/WindowState;I)V

    #@14
    .line 1515
    return-void

    #@15
    .line 1511
    .end local v0           #transit:I
    :cond_15
    const/16 v0, 0x1003

    #@17
    .restart local v0       #transit:I
    goto :goto_9
.end method

.method cancelExitAnimationForNextAnimationLocked()V
    .registers 2

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@2
    if-eqz v0, :cond_13

    #@4
    .line 228
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@6
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    #@9
    .line 229
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@c
    .line 230
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@f
    .line 231
    const/4 v0, 0x1

    #@10
    invoke-virtual {p0, v0}, Lcom/android/server/wm/WindowStateAnimator;->destroySurfaceLocked(Z)V

    #@13
    .line 233
    :cond_13
    return-void
.end method

.method public clearAnimation()V
    .registers 2

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 199
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@7
    .line 200
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@a
    .line 201
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@c
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    #@f
    .line 202
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@12
    .line 204
    :cond_12
    return-void
.end method

.method commitFinishDrawingLocked(J)Z
    .registers 9
    .parameter "currentTime"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 465
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@5
    const/4 v4, 0x2

    #@6
    if-eq v3, v4, :cond_9

    #@8
    .line 477
    :goto_8
    return v1

    #@9
    .line 471
    :cond_9
    iput v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@b
    .line 472
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@d
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@f
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@11
    if-ne v3, v5, :cond_14

    #@13
    move v1, v2

    #@14
    .line 473
    .local v1, starting:Z
    :cond_14
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@16
    iget-object v0, v3, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@18
    .line 474
    .local v0, atoken:Lcom/android/server/wm/AppWindowToken;
    if-eqz v0, :cond_20

    #@1a
    iget-boolean v3, v0, Lcom/android/server/wm/AppWindowToken;->allDrawn:Z

    #@1c
    if-nez v3, :cond_20

    #@1e
    if-eqz v1, :cond_23

    #@20
    .line 475
    :cond_20
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->performShowLocked()Z

    #@23
    :cond_23
    move v1, v2

    #@24
    .line 477
    goto :goto_8
.end method

.method computeShownFrameLocked()V
    .registers 28

    #@0
    .prologue
    .line 851
    move-object/from16 v0, p0

    #@2
    iget-boolean v13, v0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@4
    .line 852
    .local v13, selfTransformation:Z
    move-object/from16 v0, p0

    #@6
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAttachedWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@8
    move-object/from16 v22, v0

    #@a
    if-eqz v22, :cond_3b4

    #@c
    move-object/from16 v0, p0

    #@e
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAttachedWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@10
    move-object/from16 v22, v0

    #@12
    move-object/from16 v0, v22

    #@14
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@16
    move/from16 v22, v0

    #@18
    if-eqz v22, :cond_3b4

    #@1a
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAttachedWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@1e
    move-object/from16 v22, v0

    #@20
    move-object/from16 v0, v22

    #@22
    iget-object v7, v0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@24
    .line 855
    .local v7, attachedTransformation:Landroid/view/animation/Transformation;
    :goto_24
    move-object/from16 v0, p0

    #@26
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@28
    move-object/from16 v22, v0

    #@2a
    if-eqz v22, :cond_3b7

    #@2c
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@30
    move-object/from16 v22, v0

    #@32
    move-object/from16 v0, v22

    #@34
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowAnimator;->hasTransformation:Z

    #@36
    move/from16 v22, v0

    #@38
    if-eqz v22, :cond_3b7

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@3e
    move-object/from16 v22, v0

    #@40
    move-object/from16 v0, v22

    #@42
    iget-object v5, v0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@44
    .line 860
    .local v5, appTransformation:Landroid/view/animation/Transformation;
    :goto_44
    move-object/from16 v0, p0

    #@46
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mIsWallpaper:Z

    #@48
    move/from16 v22, v0

    #@4a
    if-eqz v22, :cond_c8

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@50
    move-object/from16 v22, v0

    #@52
    move-object/from16 v0, v22

    #@54
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@56
    move-object/from16 v22, v0

    #@58
    if-nez v22, :cond_c8

    #@5a
    move-object/from16 v0, p0

    #@5c
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@5e
    move-object/from16 v22, v0

    #@60
    move-object/from16 v0, v22

    #@62
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@64
    move-object/from16 v22, v0

    #@66
    if-eqz v22, :cond_c8

    #@68
    .line 862
    move-object/from16 v0, p0

    #@6a
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@6c
    move-object/from16 v22, v0

    #@6e
    move-object/from16 v0, v22

    #@70
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@72
    move-object/from16 v22, v0

    #@74
    move-object/from16 v0, v22

    #@76
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@78
    move-object/from16 v18, v0

    #@7a
    .line 863
    .local v18, wallpaperAnimator:Lcom/android/server/wm/WindowStateAnimator;
    move-object/from16 v0, v18

    #@7c
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@7e
    move/from16 v22, v0

    #@80
    if-eqz v22, :cond_9a

    #@82
    move-object/from16 v0, v18

    #@84
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@86
    move-object/from16 v22, v0

    #@88
    if-eqz v22, :cond_9a

    #@8a
    move-object/from16 v0, v18

    #@8c
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@8e
    move-object/from16 v22, v0

    #@90
    invoke-virtual/range {v22 .. v22}, Landroid/view/animation/Animation;->getDetachWallpaper()Z

    #@93
    move-result v22

    #@94
    if-nez v22, :cond_9a

    #@96
    .line 866
    move-object/from16 v0, v18

    #@98
    iget-object v7, v0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@9a
    .line 871
    :cond_9a
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@9e
    move-object/from16 v22, v0

    #@a0
    move-object/from16 v0, v22

    #@a2
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mWpAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@a4
    move-object/from16 v19, v0

    #@a6
    .line 872
    .local v19, wpAppAnimator:Lcom/android/server/wm/AppWindowAnimator;
    if-eqz v19, :cond_c8

    #@a8
    move-object/from16 v0, v19

    #@aa
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowAnimator;->hasTransformation:Z

    #@ac
    move/from16 v22, v0

    #@ae
    if-eqz v22, :cond_c8

    #@b0
    move-object/from16 v0, v19

    #@b2
    iget-object v0, v0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@b4
    move-object/from16 v22, v0

    #@b6
    if-eqz v22, :cond_c8

    #@b8
    move-object/from16 v0, v19

    #@ba
    iget-object v0, v0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@bc
    move-object/from16 v22, v0

    #@be
    invoke-virtual/range {v22 .. v22}, Landroid/view/animation/Animation;->getDetachWallpaper()Z

    #@c1
    move-result v22

    #@c2
    if-nez v22, :cond_c8

    #@c4
    .line 875
    move-object/from16 v0, v19

    #@c6
    iget-object v5, v0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@c8
    .line 882
    .end local v18           #wallpaperAnimator:Lcom/android/server/wm/WindowStateAnimator;
    .end local v19           #wpAppAnimator:Lcom/android/server/wm/AppWindowAnimator;
    :cond_c8
    move-object/from16 v0, p0

    #@ca
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@cc
    move-object/from16 v22, v0

    #@ce
    invoke-virtual/range {v22 .. v22}, Lcom/android/server/wm/WindowState;->getDisplayId()I

    #@d1
    move-result v8

    #@d2
    .line 883
    .local v8, displayId:I
    move-object/from16 v0, p0

    #@d4
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@d6
    move-object/from16 v22, v0

    #@d8
    move-object/from16 v0, v22

    #@da
    invoke-virtual {v0, v8}, Lcom/android/server/wm/WindowAnimator;->getScreenRotationAnimationLocked(I)Lcom/android/server/wm/ScreenRotationAnimation;

    #@dd
    move-result-object v12

    #@de
    .line 885
    .local v12, screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    if-eqz v12, :cond_3ba

    #@e0
    invoke-virtual {v12}, Lcom/android/server/wm/ScreenRotationAnimation;->isAnimating()Z

    #@e3
    move-result v22

    #@e4
    if-eqz v22, :cond_3ba

    #@e6
    const/4 v11, 0x1

    #@e7
    .line 887
    .local v11, screenAnimation:Z
    :goto_e7
    if-nez v13, :cond_ef

    #@e9
    if-nez v7, :cond_ef

    #@eb
    if-nez v5, :cond_ef

    #@ed
    if-eqz v11, :cond_3c7

    #@ef
    .line 890
    :cond_ef
    move-object/from16 v0, p0

    #@f1
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@f3
    move-object/from16 v22, v0

    #@f5
    move-object/from16 v0, v22

    #@f7
    iget-object v9, v0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@f9
    .line 891
    .local v9, frame:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@fb
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@fd
    move-object/from16 v22, v0

    #@ff
    move-object/from16 v0, v22

    #@101
    iget-object v15, v0, Lcom/android/server/wm/WindowManagerService;->mTmpFloats:[F

    #@103
    .line 892
    .local v15, tmpFloats:[F
    move-object/from16 v0, p0

    #@105
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@107
    move-object/from16 v22, v0

    #@109
    move-object/from16 v0, v22

    #@10b
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mTmpMatrix:Landroid/graphics/Matrix;

    #@10d
    move-object/from16 v16, v0

    #@10f
    .line 895
    .local v16, tmpMatrix:Landroid/graphics/Matrix;
    if-eqz v11, :cond_3c2

    #@111
    invoke-virtual {v12}, Lcom/android/server/wm/ScreenRotationAnimation;->isRotating()Z

    #@114
    move-result v22

    #@115
    if-eqz v22, :cond_3c2

    #@117
    .line 903
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    #@11a
    move-result v22

    #@11b
    move/from16 v0, v22

    #@11d
    int-to-float v0, v0

    #@11e
    move/from16 v17, v0

    #@120
    .line 904
    .local v17, w:F
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    #@123
    move-result v22

    #@124
    move/from16 v0, v22

    #@126
    int-to-float v10, v0

    #@127
    .line 905
    .local v10, h:F
    const/high16 v22, 0x3f80

    #@129
    cmpl-float v22, v17, v22

    #@12b
    if-ltz v22, :cond_3bd

    #@12d
    const/high16 v22, 0x3f80

    #@12f
    cmpl-float v22, v10, v22

    #@131
    if-ltz v22, :cond_3bd

    #@133
    .line 906
    const/high16 v22, 0x3f80

    #@135
    const/high16 v23, 0x4000

    #@137
    div-float v23, v23, v17

    #@139
    add-float v22, v22, v23

    #@13b
    const/high16 v23, 0x3f80

    #@13d
    const/high16 v24, 0x4000

    #@13f
    div-float v24, v24, v10

    #@141
    add-float v23, v23, v24

    #@143
    const/high16 v24, 0x4000

    #@145
    div-float v24, v17, v24

    #@147
    const/high16 v25, 0x4000

    #@149
    div-float v25, v10, v25

    #@14b
    move-object/from16 v0, v16

    #@14d
    move/from16 v1, v22

    #@14f
    move/from16 v2, v23

    #@151
    move/from16 v3, v24

    #@153
    move/from16 v4, v25

    #@155
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    #@158
    .line 913
    .end local v10           #h:F
    .end local v17           #w:F
    :goto_158
    move-object/from16 v0, p0

    #@15a
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@15c
    move-object/from16 v22, v0

    #@15e
    move-object/from16 v0, v22

    #@160
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@162
    move/from16 v22, v0

    #@164
    move-object/from16 v0, p0

    #@166
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@168
    move-object/from16 v23, v0

    #@16a
    move-object/from16 v0, v23

    #@16c
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@16e
    move/from16 v23, v0

    #@170
    move-object/from16 v0, v16

    #@172
    move/from16 v1, v22

    #@174
    move/from16 v2, v23

    #@176
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    #@179
    .line 914
    if-eqz v13, :cond_18c

    #@17b
    .line 915
    move-object/from16 v0, p0

    #@17d
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@17f
    move-object/from16 v22, v0

    #@181
    invoke-virtual/range {v22 .. v22}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@184
    move-result-object v22

    #@185
    move-object/from16 v0, v16

    #@187
    move-object/from16 v1, v22

    #@189
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@18c
    .line 917
    :cond_18c
    iget v0, v9, Landroid/graphics/Rect;->left:I

    #@18e
    move/from16 v22, v0

    #@190
    move-object/from16 v0, p0

    #@192
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@194
    move-object/from16 v23, v0

    #@196
    move-object/from16 v0, v23

    #@198
    iget v0, v0, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@19a
    move/from16 v23, v0

    #@19c
    add-int v22, v22, v23

    #@19e
    move/from16 v0, v22

    #@1a0
    int-to-float v0, v0

    #@1a1
    move/from16 v22, v0

    #@1a3
    iget v0, v9, Landroid/graphics/Rect;->top:I

    #@1a5
    move/from16 v23, v0

    #@1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@1ab
    move-object/from16 v24, v0

    #@1ad
    move-object/from16 v0, v24

    #@1af
    iget v0, v0, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@1b1
    move/from16 v24, v0

    #@1b3
    add-int v23, v23, v24

    #@1b5
    move/from16 v0, v23

    #@1b7
    int-to-float v0, v0

    #@1b8
    move/from16 v23, v0

    #@1ba
    move-object/from16 v0, v16

    #@1bc
    move/from16 v1, v22

    #@1be
    move/from16 v2, v23

    #@1c0
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@1c3
    .line 918
    if-eqz v7, :cond_1d0

    #@1c5
    .line 919
    invoke-virtual {v7}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@1c8
    move-result-object v22

    #@1c9
    move-object/from16 v0, v16

    #@1cb
    move-object/from16 v1, v22

    #@1cd
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@1d0
    .line 921
    :cond_1d0
    if-eqz v5, :cond_1dd

    #@1d2
    .line 922
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@1d5
    move-result-object v22

    #@1d6
    move-object/from16 v0, v16

    #@1d8
    move-object/from16 v1, v22

    #@1da
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@1dd
    .line 924
    :cond_1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@1e1
    move-object/from16 v22, v0

    #@1e3
    move-object/from16 v0, v22

    #@1e5
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@1e7
    move-object/from16 v22, v0

    #@1e9
    if-eqz v22, :cond_208

    #@1eb
    .line 925
    move-object/from16 v0, p0

    #@1ed
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@1ef
    move-object/from16 v22, v0

    #@1f1
    move-object/from16 v0, v22

    #@1f3
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@1f5
    move-object/from16 v22, v0

    #@1f7
    move-object/from16 v0, v22

    #@1f9
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mUniverseTransform:Landroid/view/animation/Transformation;

    #@1fb
    move-object/from16 v22, v0

    #@1fd
    invoke-virtual/range {v22 .. v22}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@200
    move-result-object v22

    #@201
    move-object/from16 v0, v16

    #@203
    move-object/from16 v1, v22

    #@205
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@208
    .line 927
    :cond_208
    if-eqz v11, :cond_219

    #@20a
    .line 928
    invoke-virtual {v12}, Lcom/android/server/wm/ScreenRotationAnimation;->getEnterTransformation()Landroid/view/animation/Transformation;

    #@20d
    move-result-object v22

    #@20e
    invoke-virtual/range {v22 .. v22}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@211
    move-result-object v22

    #@212
    move-object/from16 v0, v16

    #@214
    move-object/from16 v1, v22

    #@216
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@219
    .line 930
    :cond_219
    move-object/from16 v0, p0

    #@21b
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@21d
    move-object/from16 v22, v0

    #@21f
    invoke-virtual/range {v22 .. v22}, Lcom/android/server/wm/WindowState;->getWindowMagnificationSpecLocked()Lcom/android/server/wm/MagnificationSpec;

    #@222
    move-result-object v14

    #@223
    .line 931
    .local v14, spec:Lcom/android/server/wm/MagnificationSpec;
    if-eqz v14, :cond_24d

    #@225
    invoke-virtual {v14}, Lcom/android/server/wm/MagnificationSpec;->isNop()Z

    #@228
    move-result v22

    #@229
    if-nez v22, :cond_24d

    #@22b
    .line 932
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@22d
    move/from16 v22, v0

    #@22f
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@231
    move/from16 v23, v0

    #@233
    move-object/from16 v0, v16

    #@235
    move/from16 v1, v22

    #@237
    move/from16 v2, v23

    #@239
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    #@23c
    .line 933
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mOffsetX:F

    #@23e
    move/from16 v22, v0

    #@240
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mOffsetY:F

    #@242
    move/from16 v23, v0

    #@244
    move-object/from16 v0, v16

    #@246
    move/from16 v1, v22

    #@248
    move/from16 v2, v23

    #@24a
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@24d
    .line 942
    :cond_24d
    const/16 v22, 0x1

    #@24f
    move/from16 v0, v22

    #@251
    move-object/from16 v1, p0

    #@253
    iput-boolean v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mHaveMatrix:Z

    #@255
    .line 943
    move-object/from16 v0, v16

    #@257
    invoke-virtual {v0, v15}, Landroid/graphics/Matrix;->getValues([F)V

    #@25a
    .line 944
    const/16 v22, 0x0

    #@25c
    aget v22, v15, v22

    #@25e
    move/from16 v0, v22

    #@260
    move-object/from16 v1, p0

    #@262
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@264
    .line 945
    const/16 v22, 0x3

    #@266
    aget v22, v15, v22

    #@268
    move/from16 v0, v22

    #@26a
    move-object/from16 v1, p0

    #@26c
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@26e
    .line 946
    const/16 v22, 0x1

    #@270
    aget v22, v15, v22

    #@272
    move/from16 v0, v22

    #@274
    move-object/from16 v1, p0

    #@276
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@278
    .line 947
    const/16 v22, 0x4

    #@27a
    aget v22, v15, v22

    #@27c
    move/from16 v0, v22

    #@27e
    move-object/from16 v1, p0

    #@280
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@282
    .line 948
    const/16 v22, 0x2

    #@284
    aget v20, v15, v22

    #@286
    .line 949
    .local v20, x:F
    const/16 v22, 0x5

    #@288
    aget v21, v15, v22

    #@28a
    .line 950
    .local v21, y:F
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    #@28d
    move-result v17

    #@28e
    .line 951
    .local v17, w:I
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    #@291
    move-result v10

    #@292
    .line 952
    .local v10, h:I
    move-object/from16 v0, p0

    #@294
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@296
    move-object/from16 v22, v0

    #@298
    move-object/from16 v0, v22

    #@29a
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@29c
    move-object/from16 v22, v0

    #@29e
    move/from16 v0, v17

    #@2a0
    int-to-float v0, v0

    #@2a1
    move/from16 v23, v0

    #@2a3
    add-float v23, v23, v20

    #@2a5
    int-to-float v0, v10

    #@2a6
    move/from16 v24, v0

    #@2a8
    add-float v24, v24, v21

    #@2aa
    move-object/from16 v0, v22

    #@2ac
    move/from16 v1, v20

    #@2ae
    move/from16 v2, v21

    #@2b0
    move/from16 v3, v23

    #@2b2
    move/from16 v4, v24

    #@2b4
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    #@2b7
    .line 959
    move-object/from16 v0, p0

    #@2b9
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAlpha:F

    #@2bb
    move/from16 v22, v0

    #@2bd
    move/from16 v0, v22

    #@2bf
    move-object/from16 v1, p0

    #@2c1
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@2c3
    .line 960
    move-object/from16 v0, p0

    #@2c5
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2c7
    move-object/from16 v22, v0

    #@2c9
    move-object/from16 v0, v22

    #@2cb
    iget-boolean v0, v0, Lcom/android/server/wm/WindowManagerService;->mLimitedAlphaCompositing:Z

    #@2cd
    move/from16 v22, v0

    #@2cf
    if-eqz v22, :cond_327

    #@2d1
    move-object/from16 v0, p0

    #@2d3
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@2d5
    move-object/from16 v22, v0

    #@2d7
    move-object/from16 v0, v22

    #@2d9
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@2db
    move-object/from16 v22, v0

    #@2dd
    move-object/from16 v0, v22

    #@2df
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@2e1
    move/from16 v22, v0

    #@2e3
    invoke-static/range {v22 .. v22}, Landroid/graphics/PixelFormat;->formatHasAlpha(I)Z

    #@2e6
    move-result v22

    #@2e7
    if-eqz v22, :cond_327

    #@2e9
    move-object/from16 v0, p0

    #@2eb
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@2ed
    move-object/from16 v22, v0

    #@2ef
    move-object/from16 v0, p0

    #@2f1
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@2f3
    move/from16 v23, v0

    #@2f5
    move-object/from16 v0, p0

    #@2f7
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@2f9
    move/from16 v24, v0

    #@2fb
    move-object/from16 v0, p0

    #@2fd
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@2ff
    move/from16 v25, v0

    #@301
    move-object/from16 v0, p0

    #@303
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@305
    move/from16 v26, v0

    #@307
    invoke-virtual/range {v22 .. v26}, Lcom/android/server/wm/WindowState;->isIdentityMatrix(FFFF)Z

    #@30a
    move-result v22

    #@30b
    if-eqz v22, :cond_3b3

    #@30d
    iget v0, v9, Landroid/graphics/Rect;->left:I

    #@30f
    move/from16 v22, v0

    #@311
    move/from16 v0, v22

    #@313
    int-to-float v0, v0

    #@314
    move/from16 v22, v0

    #@316
    cmpl-float v22, v20, v22

    #@318
    if-nez v22, :cond_3b3

    #@31a
    iget v0, v9, Landroid/graphics/Rect;->top:I

    #@31c
    move/from16 v22, v0

    #@31e
    move/from16 v0, v22

    #@320
    int-to-float v0, v0

    #@321
    move/from16 v22, v0

    #@323
    cmpl-float v22, v21, v22

    #@325
    if-nez v22, :cond_3b3

    #@327
    .line 965
    :cond_327
    if-eqz v13, :cond_341

    #@329
    .line 966
    move-object/from16 v0, p0

    #@32b
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@32d
    move/from16 v22, v0

    #@32f
    move-object/from16 v0, p0

    #@331
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@333
    move-object/from16 v23, v0

    #@335
    invoke-virtual/range {v23 .. v23}, Landroid/view/animation/Transformation;->getAlpha()F

    #@338
    move-result v23

    #@339
    mul-float v22, v22, v23

    #@33b
    move/from16 v0, v22

    #@33d
    move-object/from16 v1, p0

    #@33f
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@341
    .line 968
    :cond_341
    if-eqz v7, :cond_355

    #@343
    .line 969
    move-object/from16 v0, p0

    #@345
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@347
    move/from16 v22, v0

    #@349
    invoke-virtual {v7}, Landroid/view/animation/Transformation;->getAlpha()F

    #@34c
    move-result v23

    #@34d
    mul-float v22, v22, v23

    #@34f
    move/from16 v0, v22

    #@351
    move-object/from16 v1, p0

    #@353
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@355
    .line 971
    :cond_355
    if-eqz v5, :cond_369

    #@357
    .line 972
    move-object/from16 v0, p0

    #@359
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@35b
    move/from16 v22, v0

    #@35d
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getAlpha()F

    #@360
    move-result v23

    #@361
    mul-float v22, v22, v23

    #@363
    move/from16 v0, v22

    #@365
    move-object/from16 v1, p0

    #@367
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@369
    .line 974
    :cond_369
    move-object/from16 v0, p0

    #@36b
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@36d
    move-object/from16 v22, v0

    #@36f
    move-object/from16 v0, v22

    #@371
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@373
    move-object/from16 v22, v0

    #@375
    if-eqz v22, :cond_39b

    #@377
    .line 975
    move-object/from16 v0, p0

    #@379
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@37b
    move/from16 v22, v0

    #@37d
    move-object/from16 v0, p0

    #@37f
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@381
    move-object/from16 v23, v0

    #@383
    move-object/from16 v0, v23

    #@385
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@387
    move-object/from16 v23, v0

    #@389
    move-object/from16 v0, v23

    #@38b
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mUniverseTransform:Landroid/view/animation/Transformation;

    #@38d
    move-object/from16 v23, v0

    #@38f
    invoke-virtual/range {v23 .. v23}, Landroid/view/animation/Transformation;->getAlpha()F

    #@392
    move-result v23

    #@393
    mul-float v22, v22, v23

    #@395
    move/from16 v0, v22

    #@397
    move-object/from16 v1, p0

    #@399
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@39b
    .line 977
    :cond_39b
    if-eqz v11, :cond_3b3

    #@39d
    .line 978
    move-object/from16 v0, p0

    #@39f
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@3a1
    move/from16 v22, v0

    #@3a3
    invoke-virtual {v12}, Lcom/android/server/wm/ScreenRotationAnimation;->getEnterTransformation()Landroid/view/animation/Transformation;

    #@3a6
    move-result-object v23

    #@3a7
    invoke-virtual/range {v23 .. v23}, Landroid/view/animation/Transformation;->getAlpha()F

    #@3aa
    move-result v23

    #@3ab
    mul-float v22, v22, v23

    #@3ad
    move/from16 v0, v22

    #@3af
    move-object/from16 v1, p0

    #@3b1
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@3b3
    .line 1053
    .end local v9           #frame:Landroid/graphics/Rect;
    .end local v10           #h:I
    .end local v14           #spec:Lcom/android/server/wm/MagnificationSpec;
    .end local v15           #tmpFloats:[F
    .end local v16           #tmpMatrix:Landroid/graphics/Matrix;
    .end local v17           #w:I
    .end local v20           #x:F
    .end local v21           #y:F
    :cond_3b3
    :goto_3b3
    return-void

    #@3b4
    .line 852
    .end local v5           #appTransformation:Landroid/view/animation/Transformation;
    .end local v7           #attachedTransformation:Landroid/view/animation/Transformation;
    .end local v8           #displayId:I
    .end local v11           #screenAnimation:Z
    .end local v12           #screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    :cond_3b4
    const/4 v7, 0x0

    #@3b5
    goto/16 :goto_24

    #@3b7
    .line 855
    .restart local v7       #attachedTransformation:Landroid/view/animation/Transformation;
    :cond_3b7
    const/4 v5, 0x0

    #@3b8
    goto/16 :goto_44

    #@3ba
    .line 885
    .restart local v5       #appTransformation:Landroid/view/animation/Transformation;
    .restart local v8       #displayId:I
    .restart local v12       #screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    :cond_3ba
    const/4 v11, 0x0

    #@3bb
    goto/16 :goto_e7

    #@3bd
    .line 908
    .restart local v9       #frame:Landroid/graphics/Rect;
    .local v10, h:F
    .restart local v11       #screenAnimation:Z
    .restart local v15       #tmpFloats:[F
    .restart local v16       #tmpMatrix:Landroid/graphics/Matrix;
    .local v17, w:F
    :cond_3bd
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Matrix;->reset()V

    #@3c0
    goto/16 :goto_158

    #@3c2
    .line 911
    .end local v10           #h:F
    .end local v17           #w:F
    :cond_3c2
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Matrix;->reset()V

    #@3c5
    goto/16 :goto_158

    #@3c7
    .line 994
    .end local v9           #frame:Landroid/graphics/Rect;
    .end local v15           #tmpFloats:[F
    .end local v16           #tmpMatrix:Landroid/graphics/Matrix;
    :cond_3c7
    move-object/from16 v0, p0

    #@3c9
    iget-boolean v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mIsWallpaper:Z

    #@3cb
    move/from16 v22, v0

    #@3cd
    if-eqz v22, :cond_3df

    #@3cf
    move-object/from16 v0, p0

    #@3d1
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@3d3
    move-object/from16 v22, v0

    #@3d5
    move-object/from16 v0, v22

    #@3d7
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mPendingActions:I

    #@3d9
    move/from16 v22, v0

    #@3db
    and-int/lit8 v22, v22, 0x1

    #@3dd
    if-nez v22, :cond_3b3

    #@3df
    .line 1003
    :cond_3df
    move-object/from16 v0, p0

    #@3e1
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@3e3
    move-object/from16 v22, v0

    #@3e5
    move-object/from16 v0, v22

    #@3e7
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@3e9
    move-object/from16 v22, v0

    #@3eb
    if-eqz v22, :cond_593

    #@3ed
    move-object/from16 v0, p0

    #@3ef
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@3f1
    move-object/from16 v22, v0

    #@3f3
    move-object/from16 v0, v22

    #@3f5
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@3f7
    move-object/from16 v22, v0

    #@3f9
    move-object/from16 v0, v22

    #@3fb
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@3fd
    move/from16 v22, v0

    #@3ff
    const/16 v23, 0x7e9

    #@401
    move/from16 v0, v22

    #@403
    move/from16 v1, v23

    #@405
    if-eq v0, v1, :cond_593

    #@407
    move-object/from16 v0, p0

    #@409
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@40b
    move-object/from16 v22, v0

    #@40d
    move-object/from16 v0, v22

    #@40f
    iget v0, v0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@411
    move/from16 v22, v0

    #@413
    move-object/from16 v0, p0

    #@415
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@417
    move-object/from16 v23, v0

    #@419
    move-object/from16 v0, v23

    #@41b
    iget v0, v0, Lcom/android/server/wm/WindowAnimator;->mAboveUniverseLayer:I

    #@41d
    move/from16 v23, v0

    #@41f
    move/from16 v0, v22

    #@421
    move/from16 v1, v23

    #@423
    if-ge v0, v1, :cond_593

    #@425
    const/4 v6, 0x1

    #@426
    .line 1006
    .local v6, applyUniverseTransformation:Z
    :goto_426
    move-object/from16 v0, p0

    #@428
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@42a
    move-object/from16 v22, v0

    #@42c
    invoke-virtual/range {v22 .. v22}, Lcom/android/server/wm/WindowState;->getWindowMagnificationSpecLocked()Lcom/android/server/wm/MagnificationSpec;

    #@42f
    move-result-object v14

    #@430
    .line 1007
    .restart local v14       #spec:Lcom/android/server/wm/MagnificationSpec;
    if-nez v6, :cond_434

    #@432
    if-eqz v14, :cond_596

    #@434
    .line 1008
    :cond_434
    move-object/from16 v0, p0

    #@436
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@438
    move-object/from16 v22, v0

    #@43a
    move-object/from16 v0, v22

    #@43c
    iget-object v9, v0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@43e
    .line 1009
    .restart local v9       #frame:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@440
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@442
    move-object/from16 v22, v0

    #@444
    move-object/from16 v0, v22

    #@446
    iget-object v15, v0, Lcom/android/server/wm/WindowManagerService;->mTmpFloats:[F

    #@448
    .line 1010
    .restart local v15       #tmpFloats:[F
    move-object/from16 v0, p0

    #@44a
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@44c
    move-object/from16 v22, v0

    #@44e
    move-object/from16 v0, v22

    #@450
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mTmpMatrix:Landroid/graphics/Matrix;

    #@452
    move-object/from16 v16, v0

    #@454
    .line 1012
    .restart local v16       #tmpMatrix:Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    #@456
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@458
    move-object/from16 v22, v0

    #@45a
    move-object/from16 v0, v22

    #@45c
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@45e
    move/from16 v22, v0

    #@460
    move-object/from16 v0, p0

    #@462
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@464
    move-object/from16 v23, v0

    #@466
    move-object/from16 v0, v23

    #@468
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@46a
    move/from16 v23, v0

    #@46c
    move-object/from16 v0, v16

    #@46e
    move/from16 v1, v22

    #@470
    move/from16 v2, v23

    #@472
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    #@475
    .line 1013
    iget v0, v9, Landroid/graphics/Rect;->left:I

    #@477
    move/from16 v22, v0

    #@479
    move-object/from16 v0, p0

    #@47b
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@47d
    move-object/from16 v23, v0

    #@47f
    move-object/from16 v0, v23

    #@481
    iget v0, v0, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@483
    move/from16 v23, v0

    #@485
    add-int v22, v22, v23

    #@487
    move/from16 v0, v22

    #@489
    int-to-float v0, v0

    #@48a
    move/from16 v22, v0

    #@48c
    iget v0, v9, Landroid/graphics/Rect;->top:I

    #@48e
    move/from16 v23, v0

    #@490
    move-object/from16 v0, p0

    #@492
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@494
    move-object/from16 v24, v0

    #@496
    move-object/from16 v0, v24

    #@498
    iget v0, v0, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@49a
    move/from16 v24, v0

    #@49c
    add-int v23, v23, v24

    #@49e
    move/from16 v0, v23

    #@4a0
    int-to-float v0, v0

    #@4a1
    move/from16 v23, v0

    #@4a3
    move-object/from16 v0, v16

    #@4a5
    move/from16 v1, v22

    #@4a7
    move/from16 v2, v23

    #@4a9
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@4ac
    .line 1015
    if-eqz v6, :cond_4cb

    #@4ae
    .line 1016
    move-object/from16 v0, p0

    #@4b0
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@4b2
    move-object/from16 v22, v0

    #@4b4
    move-object/from16 v0, v22

    #@4b6
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@4b8
    move-object/from16 v22, v0

    #@4ba
    move-object/from16 v0, v22

    #@4bc
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mUniverseTransform:Landroid/view/animation/Transformation;

    #@4be
    move-object/from16 v22, v0

    #@4c0
    invoke-virtual/range {v22 .. v22}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@4c3
    move-result-object v22

    #@4c4
    move-object/from16 v0, v16

    #@4c6
    move-object/from16 v1, v22

    #@4c8
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@4cb
    .line 1019
    :cond_4cb
    if-eqz v14, :cond_4f5

    #@4cd
    invoke-virtual {v14}, Lcom/android/server/wm/MagnificationSpec;->isNop()Z

    #@4d0
    move-result v22

    #@4d1
    if-nez v22, :cond_4f5

    #@4d3
    .line 1020
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@4d5
    move/from16 v22, v0

    #@4d7
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mScale:F

    #@4d9
    move/from16 v23, v0

    #@4db
    move-object/from16 v0, v16

    #@4dd
    move/from16 v1, v22

    #@4df
    move/from16 v2, v23

    #@4e1
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    #@4e4
    .line 1021
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mOffsetX:F

    #@4e6
    move/from16 v22, v0

    #@4e8
    iget v0, v14, Lcom/android/server/wm/MagnificationSpec;->mOffsetY:F

    #@4ea
    move/from16 v23, v0

    #@4ec
    move-object/from16 v0, v16

    #@4ee
    move/from16 v1, v22

    #@4f0
    move/from16 v2, v23

    #@4f2
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@4f5
    .line 1024
    :cond_4f5
    move-object/from16 v0, v16

    #@4f7
    invoke-virtual {v0, v15}, Landroid/graphics/Matrix;->getValues([F)V

    #@4fa
    .line 1026
    const/16 v22, 0x1

    #@4fc
    move/from16 v0, v22

    #@4fe
    move-object/from16 v1, p0

    #@500
    iput-boolean v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mHaveMatrix:Z

    #@502
    .line 1027
    const/16 v22, 0x0

    #@504
    aget v22, v15, v22

    #@506
    move/from16 v0, v22

    #@508
    move-object/from16 v1, p0

    #@50a
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@50c
    .line 1028
    const/16 v22, 0x3

    #@50e
    aget v22, v15, v22

    #@510
    move/from16 v0, v22

    #@512
    move-object/from16 v1, p0

    #@514
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@516
    .line 1029
    const/16 v22, 0x1

    #@518
    aget v22, v15, v22

    #@51a
    move/from16 v0, v22

    #@51c
    move-object/from16 v1, p0

    #@51e
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@520
    .line 1030
    const/16 v22, 0x4

    #@522
    aget v22, v15, v22

    #@524
    move/from16 v0, v22

    #@526
    move-object/from16 v1, p0

    #@528
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@52a
    .line 1031
    const/16 v22, 0x2

    #@52c
    aget v20, v15, v22

    #@52e
    .line 1032
    .restart local v20       #x:F
    const/16 v22, 0x5

    #@530
    aget v21, v15, v22

    #@532
    .line 1033
    .restart local v21       #y:F
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    #@535
    move-result v17

    #@536
    .line 1034
    .local v17, w:I
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    #@539
    move-result v10

    #@53a
    .line 1035
    .local v10, h:I
    move-object/from16 v0, p0

    #@53c
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@53e
    move-object/from16 v22, v0

    #@540
    move-object/from16 v0, v22

    #@542
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@544
    move-object/from16 v22, v0

    #@546
    move/from16 v0, v17

    #@548
    int-to-float v0, v0

    #@549
    move/from16 v23, v0

    #@54b
    add-float v23, v23, v20

    #@54d
    int-to-float v0, v10

    #@54e
    move/from16 v24, v0

    #@550
    add-float v24, v24, v21

    #@552
    move-object/from16 v0, v22

    #@554
    move/from16 v1, v20

    #@556
    move/from16 v2, v21

    #@558
    move/from16 v3, v23

    #@55a
    move/from16 v4, v24

    #@55c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    #@55f
    .line 1037
    move-object/from16 v0, p0

    #@561
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAlpha:F

    #@563
    move/from16 v22, v0

    #@565
    move/from16 v0, v22

    #@567
    move-object/from16 v1, p0

    #@569
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@56b
    .line 1038
    if-eqz v6, :cond_3b3

    #@56d
    .line 1039
    move-object/from16 v0, p0

    #@56f
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@571
    move/from16 v22, v0

    #@573
    move-object/from16 v0, p0

    #@575
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@577
    move-object/from16 v23, v0

    #@579
    move-object/from16 v0, v23

    #@57b
    iget-object v0, v0, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@57d
    move-object/from16 v23, v0

    #@57f
    move-object/from16 v0, v23

    #@581
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mUniverseTransform:Landroid/view/animation/Transformation;

    #@583
    move-object/from16 v23, v0

    #@585
    invoke-virtual/range {v23 .. v23}, Landroid/view/animation/Transformation;->getAlpha()F

    #@588
    move-result v23

    #@589
    mul-float v22, v22, v23

    #@58b
    move/from16 v0, v22

    #@58d
    move-object/from16 v1, p0

    #@58f
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@591
    goto/16 :goto_3b3

    #@593
    .line 1003
    .end local v6           #applyUniverseTransformation:Z
    .end local v9           #frame:Landroid/graphics/Rect;
    .end local v10           #h:I
    .end local v14           #spec:Lcom/android/server/wm/MagnificationSpec;
    .end local v15           #tmpFloats:[F
    .end local v16           #tmpMatrix:Landroid/graphics/Matrix;
    .end local v17           #w:I
    .end local v20           #x:F
    .end local v21           #y:F
    :cond_593
    const/4 v6, 0x0

    #@594
    goto/16 :goto_426

    #@596
    .line 1042
    .restart local v6       #applyUniverseTransformation:Z
    .restart local v14       #spec:Lcom/android/server/wm/MagnificationSpec;
    :cond_596
    move-object/from16 v0, p0

    #@598
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@59a
    move-object/from16 v22, v0

    #@59c
    move-object/from16 v0, v22

    #@59e
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@5a0
    move-object/from16 v22, v0

    #@5a2
    move-object/from16 v0, p0

    #@5a4
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5a6
    move-object/from16 v23, v0

    #@5a8
    move-object/from16 v0, v23

    #@5aa
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@5ac
    move-object/from16 v23, v0

    #@5ae
    invoke-virtual/range {v22 .. v23}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@5b1
    .line 1043
    move-object/from16 v0, p0

    #@5b3
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5b5
    move-object/from16 v22, v0

    #@5b7
    move-object/from16 v0, v22

    #@5b9
    iget v0, v0, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@5bb
    move/from16 v22, v0

    #@5bd
    if-nez v22, :cond_5cd

    #@5bf
    move-object/from16 v0, p0

    #@5c1
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5c3
    move-object/from16 v22, v0

    #@5c5
    move-object/from16 v0, v22

    #@5c7
    iget v0, v0, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@5c9
    move/from16 v22, v0

    #@5cb
    if-eqz v22, :cond_5fe

    #@5cd
    .line 1044
    :cond_5cd
    move-object/from16 v0, p0

    #@5cf
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5d1
    move-object/from16 v22, v0

    #@5d3
    move-object/from16 v0, v22

    #@5d5
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@5d7
    move-object/from16 v22, v0

    #@5d9
    move-object/from16 v0, p0

    #@5db
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5dd
    move-object/from16 v23, v0

    #@5df
    move-object/from16 v0, v23

    #@5e1
    iget v0, v0, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@5e3
    move/from16 v23, v0

    #@5e5
    move/from16 v0, v23

    #@5e7
    int-to-float v0, v0

    #@5e8
    move/from16 v23, v0

    #@5ea
    move-object/from16 v0, p0

    #@5ec
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5ee
    move-object/from16 v24, v0

    #@5f0
    move-object/from16 v0, v24

    #@5f2
    iget v0, v0, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@5f4
    move/from16 v24, v0

    #@5f6
    move/from16 v0, v24

    #@5f8
    int-to-float v0, v0

    #@5f9
    move/from16 v24, v0

    #@5fb
    invoke-virtual/range {v22 .. v24}, Landroid/graphics/RectF;->offset(FF)V

    #@5fe
    .line 1046
    :cond_5fe
    move-object/from16 v0, p0

    #@600
    iget v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAlpha:F

    #@602
    move/from16 v22, v0

    #@604
    move/from16 v0, v22

    #@606
    move-object/from16 v1, p0

    #@608
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@60a
    .line 1047
    const/16 v22, 0x0

    #@60c
    move/from16 v0, v22

    #@60e
    move-object/from16 v1, p0

    #@610
    iput-boolean v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mHaveMatrix:Z

    #@612
    .line 1048
    move-object/from16 v0, p0

    #@614
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@616
    move-object/from16 v22, v0

    #@618
    move-object/from16 v0, v22

    #@61a
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@61c
    move/from16 v22, v0

    #@61e
    move/from16 v0, v22

    #@620
    move-object/from16 v1, p0

    #@622
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@624
    .line 1049
    const/16 v22, 0x0

    #@626
    move/from16 v0, v22

    #@628
    move-object/from16 v1, p0

    #@62a
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@62c
    .line 1050
    const/16 v22, 0x0

    #@62e
    move/from16 v0, v22

    #@630
    move-object/from16 v1, p0

    #@632
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@634
    .line 1051
    move-object/from16 v0, p0

    #@636
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@638
    move-object/from16 v22, v0

    #@63a
    move-object/from16 v0, v22

    #@63c
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@63e
    move/from16 v22, v0

    #@640
    move/from16 v0, v22

    #@642
    move-object/from16 v1, p0

    #@644
    iput v0, v1, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@646
    goto/16 :goto_3b3
.end method

.method createSurfaceLocked()Landroid/view/Surface;
    .registers 16

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/high16 v14, -0x1000

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v12, 0x1

    #@5
    const/4 v13, 0x0

    #@6
    .line 627
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@8
    if-nez v0, :cond_100

    #@a
    .line 630
    iput v12, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@c
    .line 631
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@e
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@10
    if-eqz v0, :cond_18

    #@12
    .line 632
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@14
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@16
    iput-boolean v13, v0, Lcom/android/server/wm/AppWindowToken;->allDrawn:Z

    #@18
    .line 635
    :cond_18
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1a
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@1c
    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->makeWindowFreezingScreenIfNeededLocked(Lcom/android/server/wm/WindowState;)V

    #@1f
    .line 637
    const/4 v6, 0x4

    #@20
    .line 638
    .local v6, flags:I
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@22
    iget-object v7, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@24
    .line 640
    .local v7, attrs:Landroid/view/WindowManager$LayoutParams;
    iget v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@26
    and-int/lit16 v0, v0, 0x2000

    #@28
    if-eqz v0, :cond_2c

    #@2a
    .line 641
    or-int/lit16 v6, v6, 0x80

    #@2c
    .line 650
    :cond_2c
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@2e
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@30
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@33
    move-result v3

    #@34
    .line 651
    .local v3, w:I
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@36
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@38
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@3b
    move-result v4

    #@3c
    .line 652
    .local v4, h:I
    iget v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@3e
    and-int/lit16 v0, v0, 0x4000

    #@40
    if-eqz v0, :cond_4a

    #@42
    .line 655
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@44
    iget v3, v0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@46
    .line 656
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@48
    iget v4, v0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@4a
    .line 661
    :cond_4a
    if-gtz v3, :cond_4d

    #@4c
    const/4 v3, 0x1

    #@4d
    .line 662
    :cond_4d
    if-gtz v4, :cond_50

    #@4f
    const/4 v4, 0x1

    #@50
    .line 664
    :cond_50
    iput-boolean v13, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    #@52
    .line 665
    iput v13, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceLayer:I

    #@54
    .line 666
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceAlpha:F

    #@56
    .line 667
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceX:F

    #@58
    .line 668
    iput v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceY:F

    #@5a
    .line 669
    int-to-float v0, v3

    #@5b
    iput v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceW:F

    #@5d
    .line 670
    int-to-float v0, v4

    #@5e
    iput v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceH:F

    #@60
    .line 671
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@62
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mLastSystemDecorRect:Landroid/graphics/Rect;

    #@64
    invoke-virtual {v0, v13, v13, v13, v13}, Landroid/graphics/Rect;->set(IIII)V

    #@67
    .line 673
    :try_start_67
    iget v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@69
    const/high16 v1, 0x100

    #@6b
    and-int/2addr v0, v1

    #@6c
    if-eqz v0, :cond_103

    #@6e
    move v10, v12

    #@6f
    .line 675
    .local v10, isHwAccelerated:Z
    :goto_6f
    if-eqz v10, :cond_106

    #@71
    const/4 v5, -0x3

    #@72
    .line 676
    .local v5, format:I
    :goto_72
    iget v0, v7, Landroid/view/WindowManager$LayoutParams;->format:I

    #@74
    invoke-static {v0}, Landroid/graphics/PixelFormat;->formatHasAlpha(I)Z

    #@77
    move-result v0

    #@78
    if-nez v0, :cond_7c

    #@7a
    .line 677
    or-int/lit16 v6, v6, 0x400

    #@7c
    .line 680
    :cond_7c
    iget v9, v7, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@7e
    .line 681
    .local v9, extend2:I
    and-int v0, v9, v14

    #@80
    if-eqz v0, :cond_9f

    #@82
    .line 683
    const-string v0, "WindowManager"

    #@84
    new-instance v1, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v2, "create Surface Ext flags(Zoom) : "

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    and-int v2, v9, v14

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v1

    #@99
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 684
    and-int v0, v9, v14

    #@9e
    or-int/2addr v6, v0

    #@9f
    .line 693
    :cond_9f
    new-instance v0, Landroid/view/Surface;

    #@a1
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSession:Lcom/android/server/wm/Session;

    #@a3
    iget-object v1, v1, Lcom/android/server/wm/Session;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@a5
    invoke-virtual {v7}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@a8
    move-result-object v2

    #@a9
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@ac
    move-result-object v2

    #@ad
    invoke-direct/range {v0 .. v6}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@b0
    iput-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@b2
    .line 698
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@b4
    const/4 v1, 0x1

    #@b5
    iput-boolean v1, v0, Lcom/android/server/wm/WindowState;->mHasSurface:Z
    :try_end_b7
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_67 .. :try_end_b7} :catch_10a
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_b7} :catch_121

    #@b7
    .line 731
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@ba
    .line 734
    :try_start_ba
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@bc
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@be
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@c0
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@c2
    iget v1, v1, Lcom/android/server/wm/WindowState;->mXOffset:I

    #@c4
    add-int/2addr v0, v1

    #@c5
    int-to-float v0, v0

    #@c6
    iput v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceX:F

    #@c8
    .line 735
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@ca
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@cc
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@ce
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@d0
    iget v1, v1, Lcom/android/server/wm/WindowState;->mYOffset:I

    #@d2
    add-int/2addr v0, v1

    #@d3
    int-to-float v0, v0

    #@d4
    iput v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceY:F

    #@d6
    .line 736
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@d8
    iget v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceX:F

    #@da
    iget v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceY:F

    #@dc
    invoke-virtual {v0, v1, v2}, Landroid/view/Surface;->setPosition(FF)V

    #@df
    .line 737
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@e1
    iput v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceLayer:I

    #@e3
    .line 738
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@e5
    iget v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mLayerStack:I

    #@e7
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setLayerStack(I)V

    #@ea
    .line 739
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@ec
    iget v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@ee
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setLayer(I)V

    #@f1
    .line 740
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@f3
    const/4 v1, 0x0

    #@f4
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setAlpha(F)V

    #@f7
    .line 741
    const/4 v0, 0x0

    #@f8
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z
    :try_end_fa
    .catchall {:try_start_ba .. :try_end_fa} :catchall_153
    .catch Ljava/lang/RuntimeException; {:try_start_ba .. :try_end_fa} :catch_131

    #@fa
    .line 746
    :goto_fa
    const/4 v0, 0x1

    #@fb
    :try_start_fb
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z
    :try_end_fd
    .catchall {:try_start_fb .. :try_end_fd} :catchall_153

    #@fd
    .line 748
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@100
    .line 755
    .end local v3           #w:I
    .end local v4           #h:I
    .end local v5           #format:I
    .end local v6           #flags:I
    .end local v7           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v9           #extend2:I
    .end local v10           #isHwAccelerated:Z
    :cond_100
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@102
    :goto_102
    return-object v0

    #@103
    .restart local v3       #w:I
    .restart local v4       #h:I
    .restart local v6       #flags:I
    .restart local v7       #attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_103
    move v10, v13

    #@104
    .line 673
    goto/16 :goto_6f

    #@106
    .line 675
    .restart local v10       #isHwAccelerated:Z
    :cond_106
    :try_start_106
    iget v5, v7, Landroid/view/WindowManager$LayoutParams;->format:I
    :try_end_108
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_106 .. :try_end_108} :catch_10a
    .catch Ljava/lang/Exception; {:try_start_106 .. :try_end_108} :catch_121

    #@108
    goto/16 :goto_72

    #@10a
    .line 707
    .end local v10           #isHwAccelerated:Z
    :catch_10a
    move-exception v8

    #@10b
    .line 708
    .local v8, e:Landroid/view/Surface$OutOfResourcesException;
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@10d
    iput-boolean v13, v0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@10f
    .line 709
    const-string v0, "WindowStateAnimator"

    #@111
    const-string v1, "OutOfResourcesException creating surface"

    #@113
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@116
    .line 710
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@118
    const-string v1, "create"

    #@11a
    invoke-virtual {v0, p0, v1, v12}, Lcom/android/server/wm/WindowManagerService;->reclaimSomeSurfaceMemoryLocked(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z

    #@11d
    .line 711
    iput v13, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@11f
    move-object v0, v11

    #@120
    .line 712
    goto :goto_102

    #@121
    .line 713
    .end local v8           #e:Landroid/view/Surface$OutOfResourcesException;
    :catch_121
    move-exception v8

    #@122
    .line 714
    .local v8, e:Ljava/lang/Exception;
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@124
    iput-boolean v13, v0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@126
    .line 715
    const-string v0, "WindowStateAnimator"

    #@128
    const-string v1, "Exception creating surface"

    #@12a
    invoke-static {v0, v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12d
    .line 716
    iput v13, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@12f
    move-object v0, v11

    #@130
    .line 717
    goto :goto_102

    #@131
    .line 742
    .end local v8           #e:Ljava/lang/Exception;
    .restart local v5       #format:I
    .restart local v9       #extend2:I
    .restart local v10       #isHwAccelerated:Z
    :catch_131
    move-exception v8

    #@132
    .line 743
    .local v8, e:Ljava/lang/RuntimeException;
    :try_start_132
    const-string v0, "WindowStateAnimator"

    #@134
    new-instance v1, Ljava/lang/StringBuilder;

    #@136
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    const-string v2, "Error creating surface in "

    #@13b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v1

    #@13f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@142
    move-result-object v1

    #@143
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@146
    move-result-object v1

    #@147
    invoke-static {v0, v1, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14a
    .line 744
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@14c
    const-string v1, "create-init"

    #@14e
    const/4 v2, 0x1

    #@14f
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/server/wm/WindowManagerService;->reclaimSomeSurfaceMemoryLocked(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z
    :try_end_152
    .catchall {:try_start_132 .. :try_end_152} :catchall_153

    #@152
    goto :goto_fa

    #@153
    .line 748
    .end local v8           #e:Ljava/lang/RuntimeException;
    :catchall_153
    move-exception v0

    #@154
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@157
    throw v0
.end method

.method destroyDeferredSurfaceLocked(Z)V
    .registers 6
    .parameter "fromAnimator"

    #@0
    .prologue
    .line 829
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@2
    if-eqz v1, :cond_10

    #@4
    .line 838
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@6
    invoke-virtual {v1}, Landroid/view/Surface;->destroy()V

    #@9
    .line 839
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@b
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@d
    invoke-virtual {v1, v2, p1}, Lcom/android/server/wm/WindowAnimator;->hideWallpapersLocked(Lcom/android/server/wm/WindowState;Z)V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_10} :catch_17

    #@10
    .line 846
    :cond_10
    :goto_10
    const/4 v1, 0x0

    #@11
    iput-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceDestroyDeferred:Z

    #@13
    .line 847
    const/4 v1, 0x0

    #@14
    iput-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@16
    .line 848
    return-void

    #@17
    .line 841
    :catch_17
    move-exception v0

    #@18
    .line 842
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "WindowStateAnimator"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "Exception thrown when destroying Window "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, " surface "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    const-string v3, " session "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSession:Lcom/android/server/wm/Session;

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    const-string v3, ": "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_10
.end method

.method destroySurfaceLocked(Z)V
    .registers 9
    .parameter "fromAnimator"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 759
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@3
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@5
    if-eqz v3, :cond_17

    #@7
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@9
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@b
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@d
    iget-object v4, v4, Lcom/android/server/wm/AppWindowToken;->startingWindow:Lcom/android/server/wm/WindowState;

    #@f
    if-ne v3, v4, :cond_17

    #@11
    .line 760
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@13
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@15
    iput-boolean v6, v3, Lcom/android/server/wm/AppWindowToken;->startingDisplayed:Z

    #@17
    .line 763
    :cond_17
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@19
    if-eqz v3, :cond_67

    #@1b
    .line 765
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@1d
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@1f
    invoke-virtual {v3}, Lcom/android/server/wm/WindowList;->size()I

    #@22
    move-result v2

    #@23
    .line 766
    .local v2, i:I
    :goto_23
    if-lez v2, :cond_35

    #@25
    .line 767
    add-int/lit8 v2, v2, -0x1

    #@27
    .line 768
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@29
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@2b
    invoke-virtual {v3, v2}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Lcom/android/server/wm/WindowState;

    #@31
    .line 769
    .local v0, c:Lcom/android/server/wm/WindowState;
    const/4 v3, 0x1

    #@32
    iput-boolean v3, v0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@34
    goto :goto_23

    #@35
    .line 782
    .end local v0           #c:Lcom/android/server/wm/WindowState;
    :cond_35
    :try_start_35
    iget-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceDestroyDeferred:Z

    #@37
    if-eqz v3, :cond_68

    #@39
    .line 783
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@3b
    if-eqz v3, :cond_50

    #@3d
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@3f
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@41
    if-eq v3, v4, :cond_50

    #@43
    .line 784
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@45
    if-eqz v3, :cond_4c

    #@47
    .line 793
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@49
    invoke-virtual {v3}, Landroid/view/Surface;->destroy()V

    #@4c
    .line 795
    :cond_4c
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@4e
    iput-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@50
    .line 808
    :cond_50
    :goto_50
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@52
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@54
    invoke-virtual {v3, v4, p1}, Lcom/android/server/wm/WindowAnimator;->hideWallpapersLocked(Lcom/android/server/wm/WindowState;Z)V
    :try_end_57
    .catch Ljava/lang/RuntimeException; {:try_start_35 .. :try_end_57} :catch_6e

    #@57
    .line 815
    :goto_57
    iput-boolean v6, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    #@59
    .line 816
    const/4 v3, 0x0

    #@5a
    iput-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@5c
    .line 817
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5e
    iput-boolean v6, v3, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    #@60
    .line 818
    iput v6, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@62
    .line 821
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@64
    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDsdrStatus()I

    #@67
    .line 825
    .end local v2           #i:I
    :cond_67
    return-void

    #@68
    .line 806
    .restart local v2       #i:I
    :cond_68
    :try_start_68
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@6a
    invoke-virtual {v3}, Landroid/view/Surface;->destroy()V
    :try_end_6d
    .catch Ljava/lang/RuntimeException; {:try_start_68 .. :try_end_6d} :catch_6e

    #@6d
    goto :goto_50

    #@6e
    .line 809
    :catch_6e
    move-exception v1

    #@6f
    .line 810
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v3, "WindowStateAnimator"

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v5, "Exception thrown when destroying Window "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    const-string v5, " surface "

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@88
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v4

    #@8c
    const-string v5, " session "

    #@8e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v4

    #@92
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mSession:Lcom/android/server/wm/Session;

    #@94
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v4

    #@98
    const-string v5, ": "

    #@9a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto :goto_57
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V
    .registers 7
    .parameter "pw"
    .parameter "prefix"
    .parameter "dumpAll"

    #@0
    .prologue
    const/high16 v2, 0x3f80

    #@2
    .line 1590
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@4
    if-nez v0, :cond_12

    #@6
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@8
    if-nez v0, :cond_12

    #@a
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimationIsEntrance:Z

    #@c
    if-nez v0, :cond_12

    #@e
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@10
    if-eqz v0, :cond_3d

    #@12
    .line 1592
    :cond_12
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    const-string v0, "mAnimating="

    #@17
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@1c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@1f
    .line 1593
    const-string v0, " mLocalAnimating="

    #@21
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@26
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@29
    .line 1594
    const-string v0, " mAnimationIsEntrance="

    #@2b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimationIsEntrance:Z

    #@30
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@33
    .line 1595
    const-string v0, " mAnimation="

    #@35
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@3a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@3d
    .line 1597
    :cond_3d
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasTransformation:Z

    #@3f
    if-nez v0, :cond_45

    #@41
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@43
    if-eqz v0, :cond_69

    #@45
    .line 1598
    :cond_45
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@48
    const-string v0, "XForm: has="

    #@4a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4d
    .line 1599
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasTransformation:Z

    #@4f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@52
    .line 1600
    const-string v0, " hasLocal="

    #@54
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@57
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@59
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@5c
    .line 1601
    const-string v0, " "

    #@5e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@61
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@63
    invoke-virtual {v0, p1}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@66
    .line 1602
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@69
    .line 1604
    :cond_69
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@6b
    if-eqz v0, :cond_e0

    #@6d
    .line 1605
    if-eqz p3, :cond_97

    #@6f
    .line 1606
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@72
    const-string v0, "mSurface="

    #@74
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@77
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@79
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@7c
    .line 1607
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7f
    const-string v0, "mDrawState="

    #@81
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@84
    .line 1608
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@86
    invoke-static {v0}, Lcom/android/server/wm/WindowStateAnimator;->drawStateToString(I)Ljava/lang/String;

    #@89
    move-result-object v0

    #@8a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8d
    .line 1609
    const-string v0, " mLastHidden="

    #@8f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@92
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@94
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@97
    .line 1611
    :cond_97
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9a
    const-string v0, "Surface: shown="

    #@9c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9f
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    #@a1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@a4
    .line 1612
    const-string v0, " layer="

    #@a6
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a9
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceLayer:I

    #@ab
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@ae
    .line 1613
    const-string v0, " alpha="

    #@b0
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b3
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceAlpha:F

    #@b5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@b8
    .line 1614
    const-string v0, " rect=("

    #@ba
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bd
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceX:F

    #@bf
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@c2
    .line 1615
    const-string v0, ","

    #@c4
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c7
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceY:F

    #@c9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@cc
    .line 1616
    const-string v0, ") "

    #@ce
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d1
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceW:F

    #@d3
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@d6
    .line 1617
    const-string v0, " x "

    #@d8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@db
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceH:F

    #@dd
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(F)V

    #@e0
    .line 1619
    :cond_e0
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@e2
    if-eqz v0, :cond_f1

    #@e4
    .line 1620
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e7
    const-string v0, "mPendingDestroySurface="

    #@e9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ec
    .line 1621
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mPendingDestroySurface:Landroid/view/Surface;

    #@ee
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@f1
    .line 1623
    :cond_f1
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceResized:Z

    #@f3
    if-nez v0, :cond_f9

    #@f5
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceDestroyDeferred:Z

    #@f7
    if-eqz v0, :cond_110

    #@f9
    .line 1624
    :cond_f9
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fc
    const-string v0, "mSurfaceResized="

    #@fe
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@101
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceResized:Z

    #@103
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@106
    .line 1625
    const-string v0, " mSurfaceDestroyDeferred="

    #@108
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10b
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceDestroyDeferred:Z

    #@10d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@110
    .line 1627
    :cond_110
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@112
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@114
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@116
    const/16 v1, 0x7e9

    #@118
    if-ne v0, v1, :cond_12a

    #@11a
    .line 1628
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11d
    const-string v0, "mUniverseTransform="

    #@11f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@122
    .line 1629
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mUniverseTransform:Landroid/view/animation/Transformation;

    #@124
    invoke-virtual {v0, p1}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@127
    .line 1630
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@12a
    .line 1632
    :cond_12a
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@12c
    cmpl-float v0, v0, v2

    #@12e
    if-nez v0, :cond_13c

    #@130
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAlpha:F

    #@132
    cmpl-float v0, v0, v2

    #@134
    if-nez v0, :cond_13c

    #@136
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastAlpha:F

    #@138
    cmpl-float v0, v0, v2

    #@13a
    if-eqz v0, :cond_15d

    #@13c
    .line 1633
    :cond_13c
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13f
    const-string v0, "mShownAlpha="

    #@141
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@144
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@146
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@149
    .line 1634
    const-string v0, " mAlpha="

    #@14b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14e
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAlpha:F

    #@150
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@153
    .line 1635
    const-string v0, " mLastAlpha="

    #@155
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@158
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastAlpha:F

    #@15a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(F)V

    #@15d
    .line 1637
    :cond_15d
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mHaveMatrix:Z

    #@15f
    if-nez v0, :cond_169

    #@161
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@163
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@165
    cmpl-float v0, v0, v2

    #@167
    if-eqz v0, :cond_1a0

    #@169
    .line 1638
    :cond_169
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16c
    const-string v0, "mGlobalScale="

    #@16e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@171
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@173
    iget v0, v0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@175
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@178
    .line 1639
    const-string v0, " mDsDx="

    #@17a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17d
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@17f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@182
    .line 1640
    const-string v0, " mDtDx="

    #@184
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@187
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@189
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@18c
    .line 1641
    const-string v0, " mDsDy="

    #@18e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@191
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@193
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@196
    .line 1642
    const-string v0, " mDtDy="

    #@198
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19b
    iget v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@19d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(F)V

    #@1a0
    .line 1644
    :cond_1a0
    return-void
.end method

.method finishDrawingLocked()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 444
    iget v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@3
    if-ne v1, v0, :cond_9

    #@5
    .line 452
    const/4 v1, 0x2

    #@6
    iput v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@8
    .line 455
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method finishExit()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 390
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@4
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@6
    invoke-virtual {v2}, Lcom/android/server/wm/WindowList;->size()I

    #@9
    move-result v0

    #@a
    .line 391
    .local v0, N:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_1f

    #@d
    .line 392
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@f
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@11
    invoke-virtual {v2, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/server/wm/WindowState;

    #@17
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@19
    invoke-virtual {v2}, Lcom/android/server/wm/WindowStateAnimator;->finishExit()V

    #@1c
    .line 391
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_b

    #@1f
    .line 395
    :cond_1f
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@21
    iget-boolean v2, v2, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@23
    if-nez v2, :cond_26

    #@25
    .line 419
    :cond_25
    :goto_25
    return-void

    #@26
    .line 399
    :cond_26
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->isWindowAnimating()Z

    #@29
    move-result v2

    #@2a
    if-nez v2, :cond_25

    #@2c
    .line 406
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@2e
    if-eqz v2, :cond_40

    #@30
    .line 407
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@32
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDestroySurface:Ljava/util/ArrayList;

    #@34
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@36
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 408
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@3b
    iput-boolean v5, v2, Lcom/android/server/wm/WindowState;->mDestroying:Z

    #@3d
    .line 411
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->hide()V

    #@40
    .line 413
    :cond_40
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@42
    iput-boolean v4, v2, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@44
    .line 414
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@46
    iget-boolean v2, v2, Lcom/android/server/wm/WindowState;->mRemoveOnExit:Z

    #@48
    if-eqz v2, :cond_57

    #@4a
    .line 415
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4c
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPendingRemove:Ljava/util/ArrayList;

    #@4e
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@50
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    .line 416
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@55
    iput-boolean v4, v2, Lcom/android/server/wm/WindowState;->mRemoveOnExit:Z

    #@57
    .line 418
    :cond_57
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@59
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5b
    invoke-virtual {v2, v3, v5}, Lcom/android/server/wm/WindowAnimator;->hideWallpapersLocked(Lcom/android/server/wm/WindowState;Z)V

    #@5e
    goto :goto_25
.end method

.method hide()V
    .registers 5

    #@0
    .prologue
    .line 422
    iget-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@2
    if-nez v1, :cond_13

    #@4
    .line 424
    const/4 v1, 0x1

    #@5
    iput-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@7
    .line 427
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@9
    if-eqz v1, :cond_13

    #@b
    .line 428
    const/4 v1, 0x0

    #@c
    iput-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    #@e
    .line 430
    :try_start_e
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@10
    invoke-virtual {v1}, Landroid/view/Surface;->hide()V
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_13} :catch_14

    #@13
    .line 436
    :cond_13
    :goto_13
    return-void

    #@14
    .line 431
    :catch_14
    move-exception v0

    #@15
    .line 432
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "WindowStateAnimator"

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "Exception hiding surface in "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_13
.end method

.method isAnimating()Z
    .registers 2

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@2
    if-nez v0, :cond_20

    #@4
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAttachedWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@6
    if-eqz v0, :cond_e

    #@8
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAttachedWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@a
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@c
    if-nez v0, :cond_20

    #@e
    :cond_e
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@10
    if-eqz v0, :cond_22

    #@12
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@14
    iget-object v0, v0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@16
    if-nez v0, :cond_20

    #@18
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@1a
    iget-object v0, v0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@1c
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowToken;->inPendingTransaction:Z

    #@1e
    if-eqz v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method isDummyAnimation()Z
    .registers 3

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@6
    iget-object v0, v0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@8
    sget-object v1, Lcom/android/server/wm/AppWindowAnimator;->sDummyAnimation:Landroid/view/animation/Animation;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method isWindowAnimating()Z
    .registers 2

    #@0
    .prologue
    .line 223
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method performShowLocked()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 1377
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@5
    invoke-virtual {v4}, Lcom/android/server/wm/WindowState;->isHiddenFromUserLocked()Z

    #@8
    move-result v4

    #@9
    if-eqz v4, :cond_50

    #@b
    .line 1378
    const-string v3, "WindowStateAnimator"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "current user violation "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1a
    iget v5, v5, Lcom/android/server/wm/WindowManagerService;->mCurrentUserId:I

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    const-string v5, " trying to display "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    const-string v5, ", type "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@32
    iget-object v5, v5, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@34
    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->type:I

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    const-string v5, ", belonging to "

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@42
    iget v5, v5, Lcom/android/server/wm/WindowState;->mOwnerUid:I

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1472
    :cond_4f
    :goto_4f
    return v2

    #@50
    .line 1403
    :cond_50
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@52
    if-ne v4, v5, :cond_4f

    #@54
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@56
    invoke-virtual {v4}, Lcom/android/server/wm/WindowState;->isReadyForDisplayIgnoringKeyguard()Z

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_4f

    #@5c
    .line 1420
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@5e
    invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->enableScreenIfNeededLocked()V

    #@61
    .line 1422
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->applyEnterAnimationLocked()V

    #@64
    .line 1425
    const/high16 v4, -0x4080

    #@66
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastAlpha:F

    #@68
    .line 1428
    const/4 v4, 0x4

    #@69
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@6b
    .line 1429
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@6d
    invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->updateLayoutToAnimationLocked()V

    #@70
    .line 1431
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@72
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@74
    invoke-virtual {v4}, Lcom/android/server/wm/WindowList;->size()I

    #@77
    move-result v1

    #@78
    .line 1432
    .local v1, i:I
    :cond_78
    :goto_78
    if-lez v1, :cond_9c

    #@7a
    .line 1433
    add-int/lit8 v1, v1, -0x1

    #@7c
    .line 1434
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@7e
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mChildWindows:Lcom/android/server/wm/WindowList;

    #@80
    invoke-virtual {v4, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    #@83
    move-result-object v0

    #@84
    check-cast v0, Lcom/android/server/wm/WindowState;

    #@86
    .line 1435
    .local v0, c:Lcom/android/server/wm/WindowState;
    iget-boolean v4, v0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@88
    if-eqz v4, :cond_78

    #@8a
    .line 1436
    iput-boolean v2, v0, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@8c
    .line 1437
    iget-object v4, v0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@8e
    iget-object v4, v4, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@90
    if-eqz v4, :cond_78

    #@92
    .line 1438
    iget-object v4, v0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@94
    invoke-virtual {v4}, Lcom/android/server/wm/WindowStateAnimator;->performShowLocked()Z

    #@97
    .line 1444
    iget-object v4, v0, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@99
    iput-boolean v3, v4, Lcom/android/server/wm/DisplayContent;->layoutNeeded:Z

    #@9b
    goto :goto_78

    #@9c
    .line 1449
    .end local v0           #c:Lcom/android/server/wm/WindowState;
    :cond_9c
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@9e
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@a0
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@a2
    if-eq v2, v5, :cond_d5

    #@a4
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@a6
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@a8
    if-eqz v2, :cond_d5

    #@aa
    .line 1451
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@ac
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@ae
    iput-boolean v3, v2, Lcom/android/server/wm/AppWindowToken;->firstWindowDrawn:Z

    #@b0
    .line 1453
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@b2
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@b4
    iget-object v2, v2, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@b6
    if-eqz v2, :cond_ce

    #@b8
    .line 1462
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->clearAnimation()V

    #@bb
    .line 1463
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@bd
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mFinishedStarting:Ljava/util/ArrayList;

    #@bf
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@c1
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@c3
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c6
    .line 1464
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@c8
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@ca
    const/4 v4, 0x7

    #@cb
    invoke-virtual {v2, v4}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z

    #@ce
    .line 1466
    :cond_ce
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@d0
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@d2
    invoke-virtual {v2}, Lcom/android/server/wm/AppWindowToken;->updateReportedVisibilityLocked()V

    #@d5
    :cond_d5
    move v2, v3

    #@d6
    .line 1469
    goto/16 :goto_4f
.end method

.method public prepareSurfaceLocked(Z)V
    .registers 13
    .parameter "recoveringMemory"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1213
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@4
    .line 1214
    .local v2, w:Lcom/android/server/wm/WindowState;
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@6
    if-nez v3, :cond_f

    #@8
    .line 1215
    iget-boolean v3, v2, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@a
    if-eqz v3, :cond_e

    #@c
    .line 1219
    iput-boolean v9, v2, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@e
    .line 1326
    :cond_e
    :goto_e
    return-void

    #@f
    .line 1224
    :cond_f
    const/4 v0, 0x0

    #@10
    .line 1226
    .local v0, displayed:Z
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->computeShownFrameLocked()V

    #@13
    .line 1228
    invoke-virtual {p0, p1}, Lcom/android/server/wm/WindowStateAnimator;->setSurfaceBoundariesLocked(Z)V

    #@16
    .line 1230
    iget-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mIsWallpaper:Z

    #@18
    if-eqz v3, :cond_3c

    #@1a
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@1c
    iget-boolean v3, v3, Lcom/android/server/wm/WindowState;->mWallpaperVisible:Z

    #@1e
    if-nez v3, :cond_3c

    #@20
    .line 1232
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->hide()V

    #@23
    .line 1313
    :cond_23
    :goto_23
    if-eqz v0, :cond_e

    #@25
    .line 1314
    iget-boolean v3, v2, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@27
    if-eqz v3, :cond_37

    #@29
    .line 1315
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->isDrawnLw()Z

    #@2c
    move-result v3

    #@2d
    if-nez v3, :cond_13d

    #@2f
    .line 1316
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@31
    iget v4, v3, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@33
    and-int/lit8 v4, v4, -0x9

    #@35
    iput v4, v3, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@37
    .line 1324
    :cond_37
    :goto_37
    iget-object v3, v2, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    #@39
    iput-boolean v10, v3, Lcom/android/server/wm/WindowToken;->hasVisible:Z

    #@3b
    goto :goto_e

    #@3c
    .line 1233
    :cond_3c
    iget-boolean v3, v2, Lcom/android/server/wm/WindowState;->mAttachedHidden:Z

    #@3e
    if-nez v3, :cond_46

    #@40
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->isReadyForDisplay()Z

    #@43
    move-result v3

    #@44
    if-nez v3, :cond_55

    #@46
    .line 1234
    :cond_46
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->hide()V

    #@49
    .line 1235
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@4b
    invoke-virtual {v3, v2, v10}, Lcom/android/server/wm/WindowAnimator;->hideWallpapersLocked(Lcom/android/server/wm/WindowState;Z)V

    #@4e
    .line 1243
    iget-boolean v3, v2, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@50
    if-eqz v3, :cond_23

    #@52
    .line 1244
    iput-boolean v9, v2, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@54
    goto :goto_23

    #@55
    .line 1248
    :cond_55
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastLayer:I

    #@57
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@59
    if-ne v3, v4, :cond_97

    #@5b
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastAlpha:F

    #@5d
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@5f
    cmpl-float v3, v3, v4

    #@61
    if-nez v3, :cond_97

    #@63
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDsDx:F

    #@65
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@67
    cmpl-float v3, v3, v4

    #@69
    if-nez v3, :cond_97

    #@6b
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDtDx:F

    #@6d
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@6f
    cmpl-float v3, v3, v4

    #@71
    if-nez v3, :cond_97

    #@73
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDsDy:F

    #@75
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@77
    cmpl-float v3, v3, v4

    #@79
    if-nez v3, :cond_97

    #@7b
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDtDy:F

    #@7d
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@7f
    cmpl-float v3, v3, v4

    #@81
    if-nez v3, :cond_97

    #@83
    iget v3, v2, Lcom/android/server/wm/WindowState;->mLastHScale:F

    #@85
    iget v4, v2, Lcom/android/server/wm/WindowState;->mHScale:F

    #@87
    cmpl-float v3, v3, v4

    #@89
    if-nez v3, :cond_97

    #@8b
    iget v3, v2, Lcom/android/server/wm/WindowState;->mLastVScale:F

    #@8d
    iget v4, v2, Lcom/android/server/wm/WindowState;->mVScale:F

    #@8f
    cmpl-float v3, v3, v4

    #@91
    if-nez v3, :cond_97

    #@93
    iget-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@95
    if-eqz v3, :cond_13a

    #@97
    .line 1257
    :cond_97
    const/4 v0, 0x1

    #@98
    .line 1258
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@9a
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastAlpha:F

    #@9c
    .line 1259
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@9e
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastLayer:I

    #@a0
    .line 1260
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@a2
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDsDx:F

    #@a4
    .line 1261
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@a6
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDtDx:F

    #@a8
    .line 1262
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@aa
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDsDy:F

    #@ac
    .line 1263
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@ae
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastDtDy:F

    #@b0
    .line 1264
    iget v3, v2, Lcom/android/server/wm/WindowState;->mHScale:F

    #@b2
    iput v3, v2, Lcom/android/server/wm/WindowState;->mLastHScale:F

    #@b4
    .line 1265
    iget v3, v2, Lcom/android/server/wm/WindowState;->mVScale:F

    #@b6
    iput v3, v2, Lcom/android/server/wm/WindowState;->mLastVScale:F

    #@b8
    .line 1272
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@ba
    if-eqz v3, :cond_23

    #@bc
    .line 1274
    :try_start_bc
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@be
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceAlpha:F

    #@c0
    .line 1275
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@c2
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    #@c4
    invoke-virtual {v3, v4}, Landroid/view/Surface;->setAlpha(F)V

    #@c7
    .line 1276
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@c9
    iput v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceLayer:I

    #@cb
    .line 1277
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@cd
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@cf
    invoke-virtual {v3, v4}, Landroid/view/Surface;->setLayer(I)V

    #@d2
    .line 1278
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@d4
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDx:F

    #@d6
    iget v5, v2, Lcom/android/server/wm/WindowState;->mHScale:F

    #@d8
    mul-float/2addr v4, v5

    #@d9
    iget v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDx:F

    #@db
    iget v6, v2, Lcom/android/server/wm/WindowState;->mVScale:F

    #@dd
    mul-float/2addr v5, v6

    #@de
    iget v6, p0, Lcom/android/server/wm/WindowStateAnimator;->mDsDy:F

    #@e0
    iget v7, v2, Lcom/android/server/wm/WindowState;->mHScale:F

    #@e2
    mul-float/2addr v6, v7

    #@e3
    iget v7, p0, Lcom/android/server/wm/WindowStateAnimator;->mDtDy:F

    #@e5
    iget v8, v2, Lcom/android/server/wm/WindowState;->mVScale:F

    #@e7
    mul-float/2addr v7, v8

    #@e8
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@eb
    .line 1282
    iget-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@ed
    if-eqz v3, :cond_107

    #@ef
    iget v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@f1
    const/4 v4, 0x4

    #@f2
    if-ne v3, v4, :cond_107

    #@f4
    .line 1287
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->showSurfaceRobustlyLocked()Z

    #@f7
    move-result v3

    #@f8
    if-eqz v3, :cond_136

    #@fa
    .line 1288
    const/4 v3, 0x0

    #@fb
    iput-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@fd
    .line 1289
    iget-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mIsWallpaper:Z

    #@ff
    if-eqz v3, :cond_107

    #@101
    .line 1290
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@103
    const/4 v4, 0x1

    #@104
    invoke-virtual {v3, v2, v4}, Lcom/android/server/wm/WindowManagerService;->dispatchWallpaperVisibility(Lcom/android/server/wm/WindowState;Z)V

    #@107
    .line 1296
    :cond_107
    :goto_107
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@109
    if-eqz v3, :cond_23

    #@10b
    .line 1297
    iget-object v3, v2, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    #@10d
    const/4 v4, 0x1

    #@10e
    iput-boolean v4, v3, Lcom/android/server/wm/WindowToken;->hasVisible:Z
    :try_end_110
    .catch Ljava/lang/RuntimeException; {:try_start_bc .. :try_end_110} :catch_112

    #@110
    goto/16 :goto_23

    #@112
    .line 1299
    :catch_112
    move-exception v1

    #@113
    .line 1300
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v3, "WindowStateAnimator"

    #@115
    new-instance v4, Ljava/lang/StringBuilder;

    #@117
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v5, "Error updating surface in "

    #@11c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v4

    #@120
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v4

    #@124
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v4

    #@128
    invoke-static {v3, v4, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12b
    .line 1301
    if-nez p1, :cond_23

    #@12d
    .line 1302
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@12f
    const-string v4, "update"

    #@131
    invoke-virtual {v3, p0, v4, v10}, Lcom/android/server/wm/WindowManagerService;->reclaimSomeSurfaceMemoryLocked(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z

    #@134
    goto/16 :goto_23

    #@136
    .line 1293
    .end local v1           #e:Ljava/lang/RuntimeException;
    :cond_136
    const/4 v3, 0x0

    #@137
    :try_start_137
    iput-boolean v3, v2, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z
    :try_end_139
    .catch Ljava/lang/RuntimeException; {:try_start_137 .. :try_end_139} :catch_112

    #@139
    goto :goto_107

    #@13a
    .line 1310
    :cond_13a
    const/4 v0, 0x1

    #@13b
    goto/16 :goto_23

    #@13d
    .line 1320
    :cond_13d
    iput-boolean v9, v2, Lcom/android/server/wm/WindowState;->mOrientationChanging:Z

    #@13f
    goto/16 :goto_37
.end method

.method public setAnimation(Landroid/view/animation/Animation;)V
    .registers 5
    .parameter "anim"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 186
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@3
    .line 187
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@5
    .line 188
    iput-object p1, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@7
    .line 189
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@9
    const-wide/16 v1, 0x2710

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->restrictDuration(J)V

    #@e
    .line 190
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@10
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@12
    iget v1, v1, Lcom/android/server/wm/WindowManagerService;->mWindowAnimationScale:F

    #@14
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->scaleCurrentDuration(F)V

    #@17
    .line 192
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@19
    invoke-virtual {v0}, Landroid/view/animation/Transformation;->clear()V

    #@1c
    .line 193
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@1e
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mLastHidden:Z

    #@20
    if-eqz v0, :cond_2a

    #@22
    const/4 v0, 0x0

    #@23
    :goto_23
    invoke-virtual {v1, v0}, Landroid/view/animation/Transformation;->setAlpha(F)V

    #@26
    .line 194
    const/4 v0, 0x1

    #@27
    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@29
    .line 195
    return-void

    #@2a
    .line 193
    :cond_2a
    const/high16 v0, 0x3f80

    #@2c
    goto :goto_23
.end method

.method setSurfaceBoundariesLocked(Z)V
    .registers 16
    .parameter "recoveringMemory"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 1140
    iget-object v7, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@3
    .line 1142
    .local v7, w:Lcom/android/server/wm/WindowState;
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@5
    iget v9, v9, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@7
    and-int/lit16 v9, v9, 0x4000

    #@9
    if-eqz v9, :cond_81

    #@b
    .line 1145
    iget v8, v7, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@d
    .line 1146
    .local v8, width:I
    iget v3, v7, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@f
    .line 1152
    .local v3, height:I
    :goto_f
    if-ge v8, v10, :cond_12

    #@11
    .line 1153
    const/4 v8, 0x1

    #@12
    .line 1155
    :cond_12
    if-ge v3, v10, :cond_15

    #@14
    .line 1156
    const/4 v3, 0x1

    #@15
    .line 1158
    :cond_15
    iget v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceW:F

    #@17
    int-to-float v11, v8

    #@18
    cmpl-float v9, v9, v11

    #@1a
    if-nez v9, :cond_23

    #@1c
    iget v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceH:F

    #@1e
    int-to-float v11, v3

    #@1f
    cmpl-float v9, v9, v11

    #@21
    if-eqz v9, :cond_8e

    #@23
    :cond_23
    move v5, v10

    #@24
    .line 1159
    .local v5, surfaceResized:Z
    :goto_24
    if-eqz v5, :cond_2c

    #@26
    .line 1160
    int-to-float v9, v8

    #@27
    iput v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceW:F

    #@29
    .line 1161
    int-to-float v9, v3

    #@2a
    iput v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceH:F

    #@2c
    .line 1164
    :cond_2c
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@2e
    iget v4, v9, Landroid/graphics/RectF;->left:F

    #@30
    .line 1165
    .local v4, left:F
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mShownFrame:Landroid/graphics/RectF;

    #@32
    iget v6, v9, Landroid/graphics/RectF;->top:F

    #@34
    .line 1166
    .local v6, top:F
    iget v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceX:F

    #@36
    cmpl-float v9, v9, v4

    #@38
    if-nez v9, :cond_40

    #@3a
    iget v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceY:F

    #@3c
    cmpl-float v9, v9, v6

    #@3e
    if-eqz v9, :cond_49

    #@40
    .line 1170
    :cond_40
    :try_start_40
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceX:F

    #@42
    .line 1171
    iput v6, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceY:F

    #@44
    .line 1172
    iget-object v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@46
    invoke-virtual {v9, v4, v6}, Landroid/view/Surface;->setPosition(FF)V
    :try_end_49
    .catch Ljava/lang/RuntimeException; {:try_start_40 .. :try_end_49} :catch_90

    #@49
    .line 1183
    :cond_49
    :goto_49
    if-eqz v5, :cond_7d

    #@4b
    .line 1187
    const/4 v9, 0x1

    #@4c
    :try_start_4c
    iput-boolean v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceResized:Z

    #@4e
    .line 1188
    iget-object v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@50
    invoke-virtual {v9, v8, v3}, Landroid/view/Surface;->setSize(II)V

    #@53
    .line 1189
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@55
    invoke-virtual {v9}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    #@58
    move-result v0

    #@59
    .line 1190
    .local v0, displayId:I
    iget-object v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@5b
    const/4 v11, 0x4

    #@5c
    invoke-virtual {v9, v0, v11}, Lcom/android/server/wm/WindowAnimator;->setPendingLayoutChanges(II)V

    #@5f
    .line 1192
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@61
    iget v9, v9, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@63
    and-int/lit8 v9, v9, 0x2

    #@65
    if-eqz v9, :cond_7d

    #@67
    .line 1193
    iget-object v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@69
    iget-object v9, v9, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@6b
    invoke-virtual {v9}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@6e
    move-result-object v1

    #@6f
    .line 1194
    .local v1, displayInfo:Landroid/view/DisplayInfo;
    iget-object v11, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@71
    iget-boolean v9, v7, Lcom/android/server/wm/WindowState;->mExiting:Z

    #@73
    if-eqz v9, :cond_ce

    #@75
    const/4 v9, 0x0

    #@76
    :goto_76
    iget v12, v1, Landroid/view/DisplayInfo;->appWidth:I

    #@78
    iget v13, v1, Landroid/view/DisplayInfo;->appHeight:I

    #@7a
    invoke-virtual {v11, p0, v9, v12, v13}, Lcom/android/server/wm/WindowManagerService;->startDimmingLocked(Lcom/android/server/wm/WindowStateAnimator;FII)V
    :try_end_7d
    .catch Ljava/lang/RuntimeException; {:try_start_4c .. :try_end_7d} :catch_d3

    #@7d
    .line 1209
    .end local v0           #displayId:I
    .end local v1           #displayInfo:Landroid/view/DisplayInfo;
    :cond_7d
    :goto_7d
    invoke-virtual {p0, p1}, Lcom/android/server/wm/WindowStateAnimator;->updateSurfaceWindowCrop(Z)V

    #@80
    .line 1210
    return-void

    #@81
    .line 1148
    .end local v3           #height:I
    .end local v4           #left:F
    .end local v5           #surfaceResized:Z
    .end local v6           #top:F
    .end local v8           #width:I
    :cond_81
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@83
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    #@86
    move-result v8

    #@87
    .line 1149
    .restart local v8       #width:I
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@89
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    #@8c
    move-result v3

    #@8d
    .restart local v3       #height:I
    goto :goto_f

    #@8e
    .line 1158
    :cond_8e
    const/4 v5, 0x0

    #@8f
    goto :goto_24

    #@90
    .line 1173
    .restart local v4       #left:F
    .restart local v5       #surfaceResized:Z
    .restart local v6       #top:F
    :catch_90
    move-exception v2

    #@91
    .line 1174
    .local v2, e:Ljava/lang/RuntimeException;
    const-string v9, "WindowStateAnimator"

    #@93
    new-instance v11, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v12, "Error positioning surface of "

    #@9a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v11

    #@9e
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v11

    #@a2
    const-string v12, " pos=("

    #@a4
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v11

    #@a8
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v11

    #@ac
    const-string v12, ","

    #@ae
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v11

    #@b2
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v11

    #@b6
    const-string v12, ")"

    #@b8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v11

    #@bc
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v11

    #@c0
    invoke-static {v9, v11, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c3
    .line 1177
    if-nez p1, :cond_49

    #@c5
    .line 1178
    iget-object v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@c7
    const-string v11, "position"

    #@c9
    invoke-virtual {v9, p0, v11, v10}, Lcom/android/server/wm/WindowManagerService;->reclaimSomeSurfaceMemoryLocked(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z

    #@cc
    goto/16 :goto_49

    #@ce
    .line 1194
    .end local v2           #e:Ljava/lang/RuntimeException;
    .restart local v0       #displayId:I
    .restart local v1       #displayInfo:Landroid/view/DisplayInfo;
    :cond_ce
    :try_start_ce
    iget-object v9, v7, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@d0
    iget v9, v9, Landroid/view/WindowManager$LayoutParams;->dimAmount:F
    :try_end_d2
    .catch Ljava/lang/RuntimeException; {:try_start_ce .. :try_end_d2} :catch_d3

    #@d2
    goto :goto_76

    #@d3
    .line 1197
    .end local v0           #displayId:I
    .end local v1           #displayInfo:Landroid/view/DisplayInfo;
    :catch_d3
    move-exception v2

    #@d4
    .line 1201
    .restart local v2       #e:Ljava/lang/RuntimeException;
    const-string v9, "WindowStateAnimator"

    #@d6
    new-instance v11, Ljava/lang/StringBuilder;

    #@d8
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@db
    const-string v12, "Error resizing surface of "

    #@dd
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v11

    #@e1
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v11

    #@e5
    const-string v12, " size=("

    #@e7
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v11

    #@eb
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v11

    #@ef
    const-string v12, "x"

    #@f1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v11

    #@f5
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v11

    #@f9
    const-string v12, ")"

    #@fb
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v11

    #@ff
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v11

    #@103
    invoke-static {v9, v11, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@106
    .line 1203
    if-nez p1, :cond_7d

    #@108
    .line 1204
    iget-object v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@10a
    const-string v11, "size"

    #@10c
    invoke-virtual {v9, p0, v11, v10}, Lcom/android/server/wm/WindowManagerService;->reclaimSomeSurfaceMemoryLocked(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z

    #@10f
    goto/16 :goto_7d
.end method

.method setTransparentRegionHint(Landroid/graphics/Region;)V
    .registers 4
    .parameter "region"

    #@0
    .prologue
    .line 1329
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 1330
    const-string v0, "WindowStateAnimator"

    #@6
    const-string v1, "setTransparentRegionHint: null mSurface after mHasSurface true"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1345
    :goto_b
    return-void

    #@c
    .line 1335
    :cond_c
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@f
    .line 1339
    :try_start_f
    iget-object v0, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@11
    invoke-virtual {v0, p1}, Landroid/view/Surface;->setTransparentRegionHint(Landroid/graphics/Region;)V
    :try_end_14
    .catchall {:try_start_f .. :try_end_14} :catchall_18

    #@14
    .line 1341
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@17
    goto :goto_b

    #@18
    :catchall_18
    move-exception v0

    #@19
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@1c
    throw v0
.end method

.method setWallpaperOffset(II)V
    .registers 7
    .parameter "left"
    .parameter "top"

    #@0
    .prologue
    .line 1348
    int-to-float v1, p1

    #@1
    iput v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceX:F

    #@3
    .line 1349
    int-to-float v1, p2

    #@4
    iput v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceY:F

    #@6
    .line 1350
    iget-boolean v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@8
    if-eqz v1, :cond_b

    #@a
    .line 1373
    :goto_a
    return-void

    #@b
    .line 1359
    :cond_b
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@e
    .line 1363
    :try_start_e
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@10
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@12
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@14
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@16
    add-int/2addr v2, p1

    #@17
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@19
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@1b
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@1d
    add-int/2addr v3, p2

    #@1e
    invoke-virtual {v1, v2, v3}, Landroid/view/Surface;->setPosition(II)V

    #@21
    .line 1364
    const/4 v1, 0x0

    #@22
    invoke-virtual {p0, v1}, Lcom/android/server/wm/WindowStateAnimator;->updateSurfaceWindowCrop(Z)V
    :try_end_25
    .catchall {:try_start_e .. :try_end_25} :catchall_62
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_25} :catch_29

    #@25
    .line 1369
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@28
    goto :goto_a

    #@29
    .line 1365
    :catch_29
    move-exception v0

    #@2a
    .line 1366
    .local v0, e:Ljava/lang/RuntimeException;
    :try_start_2a
    const-string v1, "WindowStateAnimator"

    #@2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "Error positioning surface of "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, " pos=("

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    const-string v3, ","

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    const-string v3, ")"

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5e
    .catchall {:try_start_2a .. :try_end_5e} :catchall_62

    #@5e
    .line 1369
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@61
    goto :goto_a

    #@62
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catchall_62
    move-exception v1

    #@63
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@66
    throw v1
.end method

.method showSurfaceRobustlyLocked()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1485
    :try_start_2
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@4
    if-eqz v3, :cond_21

    #@6
    .line 1486
    const/4 v3, 0x1

    #@7
    iput-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    #@9
    .line 1487
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@b
    invoke-virtual {v3}, Landroid/view/Surface;->show()V

    #@e
    .line 1488
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@10
    iget-boolean v3, v3, Lcom/android/server/wm/WindowState;->mTurnOnScreen:Z

    #@12
    if-eqz v3, :cond_21

    #@14
    .line 1491
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@16
    const/4 v4, 0x0

    #@17
    iput-boolean v4, v3, Lcom/android/server/wm/WindowState;->mTurnOnScreen:Z

    #@19
    .line 1492
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@1b
    iget v4, v3, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I

    #@1d
    or-int/lit8 v4, v4, 0x10

    #@1f
    iput v4, v3, Lcom/android/server/wm/WindowAnimator;->mBulkUpdateParams:I
    :try_end_21
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_21} :catch_22

    #@21
    .line 1502
    :cond_21
    :goto_21
    return v1

    #@22
    .line 1496
    :catch_22
    move-exception v0

    #@23
    .line 1497
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v3, "WindowStateAnimator"

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "Failure showing surface "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    const-string v5, " in "

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@49
    .line 1500
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4b
    const-string v4, "show"

    #@4d
    invoke-virtual {v3, p0, v4, v1}, Lcom/android/server/wm/WindowManagerService;->reclaimSomeSurfaceMemoryLocked(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z

    #@50
    move v1, v2

    #@51
    .line 1502
    goto :goto_21
.end method

.method stepAnimationLocked(J)Z
    .registers 13
    .parameter "currentTime"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x1

    #@3
    .line 252
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@5
    iput-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWasAnimating:Z

    #@7
    .line 253
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9
    invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->okToDisplay()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_9c

    #@f
    .line 256
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@11
    invoke-virtual {v4}, Lcom/android/server/wm/WindowState;->isDrawnLw()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_64

    #@17
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@19
    if-eqz v4, :cond_64

    #@1b
    .line 257
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasTransformation:Z

    #@1d
    .line 258
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@1f
    .line 259
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@21
    if-nez v4, :cond_55

    #@23
    .line 266
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@25
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@27
    iget-object v5, v5, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@29
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@2c
    move-result v5

    #@2d
    iget-object v6, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@2f
    iget-object v6, v6, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@31
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    #@34
    move-result v6

    #@35
    iget v7, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimDw:I

    #@37
    iget v8, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimDh:I

    #@39
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/animation/Animation;->initialize(IIII)V

    #@3c
    .line 268
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@3e
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@40
    invoke-virtual {v4}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@43
    move-result-object v1

    #@44
    .line 269
    .local v1, displayInfo:Landroid/view/DisplayInfo;
    iget v4, v1, Landroid/view/DisplayInfo;->appWidth:I

    #@46
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimDw:I

    #@48
    .line 270
    iget v4, v1, Landroid/view/DisplayInfo;->appHeight:I

    #@4a
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimDh:I

    #@4c
    .line 271
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@4e
    invoke-virtual {v4, p1, p2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@51
    .line 272
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@53
    .line 273
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@55
    .line 275
    .end local v1           #displayInfo:Landroid/view/DisplayInfo;
    :cond_55
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@57
    if-eqz v4, :cond_64

    #@59
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@5b
    if-eqz v4, :cond_64

    #@5d
    .line 276
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/WindowStateAnimator;->stepAnimation(J)Z

    #@60
    move-result v4

    #@61
    if-eqz v4, :cond_64

    #@63
    .line 380
    :goto_63
    return v2

    #@64
    .line 285
    :cond_64
    iput-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@66
    .line 286
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@68
    if-eqz v4, :cond_6e

    #@6a
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimationIsEntrance:Z

    #@6c
    if-eqz v4, :cond_83

    #@6e
    :cond_6e
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@70
    if-eqz v4, :cond_83

    #@72
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    #@74
    iget-object v4, v4, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@76
    if-eqz v4, :cond_83

    #@78
    .line 295
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@7a
    .line 296
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasTransformation:Z

    #@7c
    .line 297
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@7e
    invoke-virtual {v2}, Landroid/view/animation/Transformation;->clear()V

    #@81
    move v2, v3

    #@82
    .line 298
    goto :goto_63

    #@83
    .line 299
    :cond_83
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasTransformation:Z

    #@85
    if-eqz v4, :cond_93

    #@87
    .line 302
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@89
    .line 312
    :cond_89
    :goto_89
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@8b
    if-nez v4, :cond_a3

    #@8d
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@8f
    if-nez v4, :cond_a3

    #@91
    move v2, v3

    #@92
    .line 313
    goto :goto_63

    #@93
    .line 303
    :cond_93
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->isAnimating()Z

    #@96
    move-result v4

    #@97
    if-eqz v4, :cond_89

    #@99
    .line 304
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@9b
    goto :goto_89

    #@9c
    .line 306
    :cond_9c
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@9e
    if-eqz v4, :cond_89

    #@a0
    .line 309
    iput-boolean v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@a2
    goto :goto_89

    #@a3
    .line 322
    :cond_a3
    iput-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@a5
    .line 323
    iput-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mLocalAnimating:Z

    #@a7
    .line 324
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@a9
    if-eqz v4, :cond_b2

    #@ab
    .line 325
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@ad
    invoke-virtual {v4}, Landroid/view/animation/Animation;->cancel()V

    #@b0
    .line 326
    iput-object v9, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@b2
    .line 328
    :cond_b2
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@b4
    iget-object v4, v4, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@b6
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@b8
    if-ne v4, v5, :cond_be

    #@ba
    .line 329
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@bc
    iput-object v9, v4, Lcom/android/server/wm/WindowAnimator;->mWindowDetachedWallpaper:Lcom/android/server/wm/WindowState;

    #@be
    .line 331
    :cond_be
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@c0
    iget v4, v4, Lcom/android/server/wm/WindowState;->mLayer:I

    #@c2
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@c4
    .line 332
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@c6
    iget-boolean v4, v4, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    #@c8
    if-eqz v4, :cond_173

    #@ca
    .line 333
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@cc
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@ce
    iget v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodAnimLayerAdjustment:I

    #@d0
    add-int/2addr v4, v5

    #@d1
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@d3
    .line 339
    :cond_d3
    :goto_d3
    iput-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasTransformation:Z

    #@d5
    .line 340
    iput-boolean v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mHasLocalTransformation:Z

    #@d7
    .line 341
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@d9
    iget-boolean v4, v4, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@db
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@dd
    iget-boolean v5, v5, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@df
    if-eq v4, v5, :cond_106

    #@e1
    .line 346
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@e3
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@e5
    iget-boolean v5, v5, Lcom/android/server/wm/WindowState;->mPolicyVisibilityAfterAnim:Z

    #@e7
    iput-boolean v5, v4, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@e9
    .line 347
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@eb
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@ed
    iput-boolean v2, v4, Lcom/android/server/wm/DisplayContent;->layoutNeeded:Z

    #@ef
    .line 348
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@f1
    iget-boolean v4, v4, Lcom/android/server/wm/WindowState;->mPolicyVisibility:Z

    #@f3
    if-nez v4, :cond_106

    #@f5
    .line 349
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f7
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    #@f9
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@fb
    if-ne v4, v5, :cond_101

    #@fd
    .line 350
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@ff
    iput-boolean v2, v4, Lcom/android/server/wm/WindowManagerService;->mFocusMayChange:Z

    #@101
    .line 355
    :cond_101
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@103
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->enableScreenIfNeededLocked()V

    #@106
    .line 358
    :cond_106
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mTransformation:Landroid/view/animation/Transformation;

    #@108
    invoke-virtual {v2}, Landroid/view/animation/Transformation;->clear()V

    #@10b
    .line 359
    iget v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    #@10d
    const/4 v4, 0x4

    #@10e
    if-ne v2, v4, :cond_142

    #@110
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@112
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@114
    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@116
    const/4 v4, 0x3

    #@117
    if-ne v2, v4, :cond_142

    #@119
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@11b
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@11d
    if-eqz v2, :cond_142

    #@11f
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@121
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@123
    iget-boolean v2, v2, Lcom/android/server/wm/AppWindowToken;->firstWindowDrawn:Z

    #@125
    if-eqz v2, :cond_142

    #@127
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@129
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@12b
    iget-object v2, v2, Lcom/android/server/wm/AppWindowToken;->startingData:Lcom/android/server/wm/StartingData;

    #@12d
    if-eqz v2, :cond_142

    #@12f
    .line 366
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@131
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mFinishedStarting:Ljava/util/ArrayList;

    #@133
    iget-object v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@135
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@137
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13a
    .line 367
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@13c
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    #@13e
    const/4 v4, 0x7

    #@13f
    invoke-virtual {v2, v4}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z

    #@142
    .line 370
    :cond_142
    invoke-virtual {p0}, Lcom/android/server/wm/WindowStateAnimator;->finishExit()V

    #@145
    .line 371
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@147
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@149
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    #@14c
    move-result v0

    #@14d
    .line 372
    .local v0, displayId:I
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@14f
    const/16 v4, 0x8

    #@151
    invoke-virtual {v2, v0, v4}, Lcom/android/server/wm/WindowAnimator;->setPendingLayoutChanges(II)V

    #@154
    .line 373
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@156
    const-string v4, "WindowStateAnimator"

    #@158
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@15a
    iget-object v5, v5, Lcom/android/server/wm/WindowAnimator;->mPendingLayoutChanges:Landroid/util/SparseIntArray;

    #@15c
    invoke-virtual {v5, v0}, Landroid/util/SparseIntArray;->get(I)I

    #@15f
    move-result v5

    #@160
    invoke-virtual {v2, v4, v5}, Lcom/android/server/wm/WindowManagerService;->debugLayoutRepeats(Ljava/lang/String;I)V

    #@163
    .line 376
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@165
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@167
    if-eqz v2, :cond_170

    #@169
    .line 377
    iget-object v2, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@16b
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@16d
    invoke-virtual {v2}, Lcom/android/server/wm/AppWindowToken;->updateReportedVisibilityLocked()V

    #@170
    :cond_170
    move v2, v3

    #@171
    .line 380
    goto/16 :goto_63

    #@173
    .line 334
    .end local v0           #displayId:I
    :cond_173
    iget-boolean v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mIsWallpaper:Z

    #@175
    if-eqz v4, :cond_d3

    #@177
    .line 335
    iget v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@179
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@17b
    iget v5, v5, Lcom/android/server/wm/WindowManagerService;->mWallpaperAnimLayerAdjustment:I

    #@17d
    add-int/2addr v4, v5

    #@17e
    iput v4, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@180
    goto/16 :goto_d3
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1648
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    const-string v1, "WindowStateAnimator{"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@7
    .line 1649
    .local v0, sb:Ljava/lang/StringBuffer;
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@a
    move-result v1

    #@b
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@12
    .line 1650
    const/16 v1, 0x20

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@17
    .line 1651
    iget-object v1, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@19
    iget-object v1, v1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@1b
    invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    #@22
    .line 1652
    const/16 v1, 0x7d

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@27
    .line 1653
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    return-object v1
.end method

.method updateSurfaceWindowCrop(Z)V
    .registers 13
    .parameter "recoveringMemory"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1081
    iget-object v3, p0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@4
    .line 1082
    .local v3, w:Lcom/android/server/wm/WindowState;
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    #@6
    invoke-virtual {v6}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@9
    move-result-object v0

    #@a
    .line 1084
    .local v0, displayInfo:Landroid/view/DisplayInfo;
    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getSystemUiVisibility()I

    #@d
    move-result v6

    #@e
    and-int/lit16 v6, v6, 0x600

    #@10
    if-eqz v6, :cond_3d

    #@12
    move v2, v4

    #@13
    .line 1088
    .local v2, mIsFullScreen:Z
    :goto_13
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@15
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@17
    and-int/lit16 v6, v6, 0x4000

    #@19
    if-eqz v6, :cond_3f

    #@1b
    .line 1091
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@1d
    iget v7, v3, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    #@1f
    iget v8, v3, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    #@21
    invoke-virtual {v6, v5, v5, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@24
    .line 1123
    :goto_24
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@26
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mLastSystemDecorRect:Landroid/graphics/Rect;

    #@28
    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v5

    #@2c
    if-nez v5, :cond_3c

    #@2e
    .line 1124
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mLastSystemDecorRect:Landroid/graphics/Rect;

    #@30
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@32
    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@35
    .line 1128
    :try_start_35
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mSurface:Landroid/view/Surface;

    #@37
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@39
    invoke-virtual {v5, v6}, Landroid/view/Surface;->setWindowCrop(Landroid/graphics/Rect;)V
    :try_end_3c
    .catch Ljava/lang/RuntimeException; {:try_start_35 .. :try_end_3c} :catch_dd

    #@3c
    .line 1137
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .end local v2           #mIsFullScreen:Z
    :cond_3d
    move v2, v5

    #@3e
    .line 1084
    goto :goto_13

    #@3f
    .line 1092
    .restart local v2       #mIsFullScreen:Z
    :cond_3f
    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->isDefaultDisplay()Z

    #@42
    move-result v6

    #@43
    if-nez v6, :cond_74

    #@45
    .line 1095
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@47
    iget-object v7, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@49
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    #@4c
    move-result v7

    #@4d
    iget-object v8, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@4f
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@52
    move-result v8

    #@53
    invoke-virtual {v6, v5, v5, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@56
    .line 1096
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@58
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@5a
    iget v6, v6, Landroid/graphics/Rect;->left:I

    #@5c
    neg-int v6, v6

    #@5d
    iget-object v7, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@5f
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@61
    neg-int v7, v7

    #@62
    iget v8, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@64
    iget-object v9, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@66
    iget v9, v9, Landroid/graphics/Rect;->left:I

    #@68
    sub-int/2addr v8, v9

    #@69
    iget v9, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@6b
    iget-object v10, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@6d
    iget v10, v10, Landroid/graphics/Rect;->top:I

    #@6f
    sub-int/2addr v9, v10

    #@70
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@73
    goto :goto_24

    #@74
    .line 1099
    :cond_74
    iget v6, v3, Lcom/android/server/wm/WindowState;->mLayer:I

    #@76
    iget-object v7, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@78
    iget v7, v7, Lcom/android/server/wm/WindowManagerService;->mSystemDecorLayer:I

    #@7a
    if-lt v6, v7, :cond_9c

    #@7c
    .line 1104
    iget-object v6, p0, Lcom/android/server/wm/WindowStateAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@7e
    iget-object v6, v6, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@80
    if-nez v6, :cond_94

    #@82
    .line 1105
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@84
    iget-object v7, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@86
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    #@89
    move-result v7

    #@8a
    iget-object v8, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@8c
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@8f
    move-result v8

    #@90
    invoke-virtual {v6, v5, v5, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@93
    goto :goto_24

    #@94
    .line 1108
    :cond_94
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@96
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mScreenRect:Landroid/graphics/Rect;

    #@98
    invoke-virtual {p0, v5}, Lcom/android/server/wm/WindowStateAnimator;->applyDecorRect(Landroid/graphics/Rect;)V

    #@9b
    goto :goto_24

    #@9c
    .line 1110
    :cond_9c
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@9e
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@a0
    const/16 v7, 0x7e9

    #@a2
    if-ne v6, v7, :cond_b7

    #@a4
    .line 1112
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@a6
    iget-object v7, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@a8
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    #@ab
    move-result v7

    #@ac
    iget-object v8, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@ae
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@b1
    move-result v8

    #@b2
    invoke-virtual {v6, v5, v5, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@b5
    goto/16 :goto_24

    #@b7
    .line 1115
    :cond_b7
    if-nez v2, :cond_c1

    #@b9
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@bb
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@bd
    const/16 v7, 0x7d4

    #@bf
    if-ne v6, v7, :cond_d4

    #@c1
    .line 1116
    :cond_c1
    iget-object v6, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@c3
    iget-object v7, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@c5
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    #@c8
    move-result v7

    #@c9
    iget-object v8, v3, Lcom/android/server/wm/WindowState;->mCompatFrame:Landroid/graphics/Rect;

    #@cb
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@ce
    move-result v8

    #@cf
    invoke-virtual {v6, v5, v5, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@d2
    goto/16 :goto_24

    #@d4
    .line 1119
    :cond_d4
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@d6
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mSystemDecorRect:Landroid/graphics/Rect;

    #@d8
    invoke-virtual {p0, v5}, Lcom/android/server/wm/WindowStateAnimator;->applyDecorRect(Landroid/graphics/Rect;)V

    #@db
    goto/16 :goto_24

    #@dd
    .line 1129
    :catch_dd
    move-exception v1

    #@de
    .line 1130
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v5, "WindowStateAnimator"

    #@e0
    new-instance v6, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v7, "Error setting crop surface of "

    #@e7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v6

    #@eb
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v6

    #@ef
    const-string v7, " crop="

    #@f1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v6

    #@f5
    iget-object v7, v3, Lcom/android/server/wm/WindowState;->mSystemDecorRect:Landroid/graphics/Rect;

    #@f7
    invoke-virtual {v7}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    #@fa
    move-result-object v7

    #@fb
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v6

    #@ff
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v6

    #@103
    invoke-static {v5, v6, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@106
    .line 1132
    if-nez p1, :cond_3c

    #@108
    .line 1133
    iget-object v5, p0, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@10a
    const-string v6, "crop"

    #@10c
    invoke-virtual {v5, p0, v6, v4}, Lcom/android/server/wm/WindowManagerService;->reclaimSomeSurfaceMemoryLocked(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z

    #@10f
    goto/16 :goto_3c
.end method
