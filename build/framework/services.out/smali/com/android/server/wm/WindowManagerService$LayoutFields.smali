.class Lcom/android/server/wm/WindowManagerService$LayoutFields;
.super Ljava/lang/Object;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LayoutFields"
.end annotation


# static fields
.field private static final DISPLAY_CONTENT_MIRROR:I = 0x1

.field private static final DISPLAY_CONTENT_UNIQUE:I = 0x2

.field private static final DISPLAY_CONTENT_UNKNOWN:I = 0x0

.field static final SET_FORCE_HIDING_CHANGED:I = 0x4

.field static final SET_ORIENTATION_CHANGE_COMPLETE:I = 0x8

.field static final SET_TURN_ON_SCREEN:I = 0x10

.field static final SET_UPDATE_ROTATION:I = 0x1

.field static final SET_WALLPAPER_MAY_CHANGE:I = 0x2


# instance fields
.field mAdjResult:I

.field private mButtonBrightness:F

.field private mCompatDisplayId:I

.field mDimming:Z

.field private mDisplayHasContent:I

.field private mHoldScreen:Lcom/android/server/wm/Session;

.field private mNeedCompatFiller:Z

.field private mObscured:Z

.field mOrientationChangeComplete:Z

.field private mScreenBrightness:F

.field private mSyswin:Z

.field private mUpdateRotation:Z

.field private mUserActivityTimeout:J

.field mWallpaperForceHidingChanged:Z

.field mWallpaperMayChange:Z

.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/high16 v1, -0x4080

    #@2
    const/4 v2, 0x0

    #@3
    .line 705
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@5
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 712
    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mWallpaperForceHidingChanged:Z

    #@a
    .line 713
    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mWallpaperMayChange:Z

    #@c
    .line 714
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mOrientationChangeComplete:Z

    #@f
    .line 715
    iput v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mAdjResult:I

    #@11
    .line 716
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mHoldScreen:Lcom/android/server/wm/Session;

    #@14
    .line 717
    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mObscured:Z

    #@16
    .line 718
    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mDimming:Z

    #@18
    .line 719
    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mSyswin:Z

    #@1a
    .line 720
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mScreenBrightness:F

    #@1c
    .line 721
    iput v1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mButtonBrightness:F

    #@1e
    .line 722
    const-wide/16 v0, -0x1

    #@20
    iput-wide v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mUserActivityTimeout:J

    #@22
    .line 723
    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mUpdateRotation:Z

    #@24
    .line 724
    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mNeedCompatFiller:Z

    #@26
    .line 725
    iput v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mCompatDisplayId:I

    #@28
    .line 730
    iput v2, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mDisplayHasContent:I

    #@2a
    return-void
.end method

.method static synthetic access$2200(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Lcom/android/server/wm/Session;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mHoldScreen:Lcom/android/server/wm/Session;

    #@2
    return-object v0
.end method

.method static synthetic access$2202(Lcom/android/server/wm/WindowManagerService$LayoutFields;Lcom/android/server/wm/Session;)Lcom/android/server/wm/Session;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mHoldScreen:Lcom/android/server/wm/Session;

    #@2
    return-object p1
.end method

.method static synthetic access$2300(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mSyswin:Z

    #@2
    return v0
.end method

.method static synthetic access$2302(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mSyswin:Z

    #@2
    return p1
.end method

.method static synthetic access$2400(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mScreenBrightness:F

    #@2
    return v0
.end method

.method static synthetic access$2402(Lcom/android/server/wm/WindowManagerService$LayoutFields;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mScreenBrightness:F

    #@2
    return p1
.end method

.method static synthetic access$2500(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mButtonBrightness:F

    #@2
    return v0
.end method

.method static synthetic access$2502(Lcom/android/server/wm/WindowManagerService$LayoutFields;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mButtonBrightness:F

    #@2
    return p1
.end method

.method static synthetic access$2600(Lcom/android/server/wm/WindowManagerService$LayoutFields;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget-wide v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mUserActivityTimeout:J

    #@2
    return-wide v0
.end method

.method static synthetic access$2602(Lcom/android/server/wm/WindowManagerService$LayoutFields;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput-wide p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mUserActivityTimeout:J

    #@2
    return-wide p1
.end method

.method static synthetic access$2700(Lcom/android/server/wm/WindowManagerService$LayoutFields;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mDisplayHasContent:I

    #@2
    return v0
.end method

.method static synthetic access$2702(Lcom/android/server/wm/WindowManagerService$LayoutFields;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mDisplayHasContent:I

    #@2
    return p1
.end method

.method static synthetic access$2800(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mObscured:Z

    #@2
    return v0
.end method

.method static synthetic access$2802(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mObscured:Z

    #@2
    return p1
.end method

.method static synthetic access$2900(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mNeedCompatFiller:Z

    #@2
    return v0
.end method

.method static synthetic access$2902(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mNeedCompatFiller:Z

    #@2
    return p1
.end method

.method static synthetic access$3000(Lcom/android/server/wm/WindowManagerService$LayoutFields;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mCompatDisplayId:I

    #@2
    return v0
.end method

.method static synthetic access$3002(Lcom/android/server/wm/WindowManagerService$LayoutFields;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mCompatDisplayId:I

    #@2
    return p1
.end method

.method static synthetic access$3100(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mUpdateRotation:Z

    #@2
    return v0
.end method

.method static synthetic access$3102(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 705
    iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerService$LayoutFields;->mUpdateRotation:Z

    #@2
    return p1
.end method
