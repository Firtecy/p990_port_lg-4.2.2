.class final Lcom/android/server/wm/InputMonitor;
.super Ljava/lang/Object;
.source "InputMonitor.java"

# interfaces
.implements Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;


# instance fields
.field private mInputDevicesReady:Z

.field private final mInputDevicesReadyMonitor:Ljava/lang/Object;

.field private mInputDispatchEnabled:Z

.field private mInputDispatchFrozen:Z

.field private mInputFocus:Lcom/android/server/wm/WindowState;

.field private mInputWindowHandleCount:I

.field private mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

.field private final mService:Lcom/android/server/wm/WindowManagerService;

.field private mUpdateInputWindowsNeeded:Z


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mUpdateInputWindowsNeeded:Z

    #@6
    .line 61
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReadyMonitor:Ljava/lang/Object;

    #@d
    .line 65
    iput-object p1, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f
    .line 66
    return-void
.end method

.method private addInputWindowHandleLw(Lcom/android/server/input/InputWindowHandle;)V
    .registers 5
    .parameter "windowHandle"

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 175
    const/16 v0, 0x10

    #@6
    new-array v0, v0, [Lcom/android/server/input/InputWindowHandle;

    #@8
    iput-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@a
    .line 177
    :cond_a
    iget v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandleCount:I

    #@c
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@e
    array-length v1, v1

    #@f
    if-lt v0, v1, :cond_1f

    #@11
    .line 178
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@13
    iget v1, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandleCount:I

    #@15
    mul-int/lit8 v1, v1, 0x2

    #@17
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, [Lcom/android/server/input/InputWindowHandle;

    #@1d
    iput-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@1f
    .line 181
    :cond_1f
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@21
    iget v1, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandleCount:I

    #@23
    add-int/lit8 v2, v1, 0x1

    #@25
    iput v2, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandleCount:I

    #@27
    aput-object p1, v0, v1

    #@29
    .line 182
    return-void
.end method

.method private addInputWindowHandleLw(Lcom/android/server/input/InputWindowHandle;Lcom/android/server/wm/WindowState;IIZZZ)V
    .registers 12
    .parameter "inputWindowHandle"
    .parameter "child"
    .parameter "flags"
    .parameter "type"
    .parameter "isVisible"
    .parameter "hasFocus"
    .parameter "hasWallpaper"

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    .line 188
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->toString()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    iput-object v1, p1, Lcom/android/server/input/InputWindowHandle;->name:Ljava/lang/String;

    #@8
    .line 189
    iput p3, p1, Lcom/android/server/input/InputWindowHandle;->layoutParamsFlags:I

    #@a
    .line 190
    iput p4, p1, Lcom/android/server/input/InputWindowHandle;->layoutParamsType:I

    #@c
    .line 191
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getInputDispatchingTimeoutNanos()J

    #@f
    move-result-wide v1

    #@10
    iput-wide v1, p1, Lcom/android/server/input/InputWindowHandle;->dispatchingTimeoutNanos:J

    #@12
    .line 192
    iput-boolean p5, p1, Lcom/android/server/input/InputWindowHandle;->visible:Z

    #@14
    .line 193
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->canReceiveKeys()Z

    #@17
    move-result v1

    #@18
    iput-boolean v1, p1, Lcom/android/server/input/InputWindowHandle;->canReceiveKeys:Z

    #@1a
    .line 194
    iput-boolean p6, p1, Lcom/android/server/input/InputWindowHandle;->hasFocus:Z

    #@1c
    .line 195
    iput-boolean p7, p1, Lcom/android/server/input/InputWindowHandle;->hasWallpaper:Z

    #@1e
    .line 196
    iget-object v1, p2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@20
    if-eqz v1, :cond_65

    #@22
    iget-object v1, p2, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@24
    iget-boolean v1, v1, Lcom/android/server/wm/WindowToken;->paused:Z

    #@26
    :goto_26
    iput-boolean v1, p1, Lcom/android/server/input/InputWindowHandle;->paused:Z

    #@28
    .line 197
    iget v1, p2, Lcom/android/server/wm/WindowState;->mLayer:I

    #@2a
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->layer:I

    #@2c
    .line 198
    iget-object v1, p2, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@2e
    iget v1, v1, Lcom/android/server/wm/Session;->mPid:I

    #@30
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->ownerPid:I

    #@32
    .line 199
    iget-object v1, p2, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@34
    iget v1, v1, Lcom/android/server/wm/Session;->mUid:I

    #@36
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->ownerUid:I

    #@38
    .line 200
    iget-object v1, p2, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@3a
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@3c
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->inputFeatures:I

    #@3e
    .line 202
    iget-object v0, p2, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    #@40
    .line 203
    .local v0, frame:Landroid/graphics/Rect;
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@42
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->frameLeft:I

    #@44
    .line 204
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@46
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->frameTop:I

    #@48
    .line 205
    iget v1, v0, Landroid/graphics/Rect;->right:I

    #@4a
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->frameRight:I

    #@4c
    .line 206
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@4e
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->frameBottom:I

    #@50
    .line 208
    iget v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@52
    cmpl-float v1, v1, v3

    #@54
    if-eqz v1, :cond_67

    #@56
    .line 212
    iget v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    #@58
    div-float v1, v3, v1

    #@5a
    iput v1, p1, Lcom/android/server/input/InputWindowHandle;->scaleFactor:F

    #@5c
    .line 217
    :goto_5c
    iget-object v1, p1, Lcom/android/server/input/InputWindowHandle;->touchableRegion:Landroid/graphics/Region;

    #@5e
    invoke-virtual {p2, v1}, Lcom/android/server/wm/WindowState;->getTouchableRegion(Landroid/graphics/Region;)V

    #@61
    .line 219
    invoke-direct {p0, p1}, Lcom/android/server/wm/InputMonitor;->addInputWindowHandleLw(Lcom/android/server/input/InputWindowHandle;)V

    #@64
    .line 220
    return-void

    #@65
    .line 196
    .end local v0           #frame:Landroid/graphics/Rect;
    :cond_65
    const/4 v1, 0x0

    #@66
    goto :goto_26

    #@67
    .line 214
    .restart local v0       #frame:Landroid/graphics/Rect;
    :cond_67
    iput v3, p1, Lcom/android/server/input/InputWindowHandle;->scaleFactor:F

    #@69
    goto :goto_5c
.end method

.method private clearInputWindowHandlesLw()V
    .registers 4

    #@0
    .prologue
    .line 223
    :goto_0
    iget v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandleCount:I

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 224
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@6
    iget v1, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandleCount:I

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    iput v1, p0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandleCount:I

    #@c
    const/4 v2, 0x0

    #@d
    aput-object v2, v0, v1

    #@f
    goto :goto_0

    #@10
    .line 226
    :cond_10
    return-void
.end method

.method private updateInputDispatchModeLw()V
    .registers 4

    #@0
    .prologue
    .line 488
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@4
    iget-boolean v1, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchEnabled:Z

    #@6
    iget-boolean v2, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchFrozen:Z

    #@8
    invoke-virtual {v0, v1, v2}, Lcom/android/server/input/InputManagerService;->setInputDispatchMode(ZZ)V

    #@b
    .line 489
    return-void
.end method


# virtual methods
.method public dispatchUnhandledKey(Lcom/android/server/input/InputWindowHandle;Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;
    .registers 6
    .parameter "focus"
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 383
    if-eqz p1, :cond_10

    #@2
    iget-object v1, p1, Lcom/android/server/input/InputWindowHandle;->windowState:Ljava/lang/Object;

    #@4
    check-cast v1, Lcom/android/server/wm/WindowState;

    #@6
    move-object v0, v1

    #@7
    .line 384
    .local v0, windowState:Lcom/android/server/wm/WindowState;
    :goto_7
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@b
    invoke-interface {v1, v0, p2, p3}, Landroid/view/WindowManagerPolicy;->dispatchUnhandledKey(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@e
    move-result-object v1

    #@f
    return-object v1

    #@10
    .line 383
    .end local v0           #windowState:Lcom/android/server/wm/WindowState;
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_7
.end method

.method public freezeInputDispatchingLw()V
    .registers 2

    #@0
    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchFrozen:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 460
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchFrozen:Z

    #@7
    .line 461
    invoke-direct {p0}, Lcom/android/server/wm/InputMonitor;->updateInputDispatchModeLw()V

    #@a
    .line 463
    :cond_a
    return-void
.end method

.method public getPointerLayer()I
    .registers 3

    #@0
    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    const/16 v1, 0x7e2

    #@6
    invoke-interface {v0, v1}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    #@9
    move-result v0

    #@a
    mul-int/lit16 v0, v0, 0x2710

    #@c
    add-int/lit16 v0, v0, 0x3e8

    #@e
    return v0
.end method

.method public interceptKeyBeforeDispatching(Lcom/android/server/input/InputWindowHandle;Landroid/view/KeyEvent;I)J
    .registers 7
    .parameter "focus"
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 375
    if-eqz p1, :cond_10

    #@2
    iget-object v1, p1, Lcom/android/server/input/InputWindowHandle;->windowState:Ljava/lang/Object;

    #@4
    check-cast v1, Lcom/android/server/wm/WindowState;

    #@6
    move-object v0, v1

    #@7
    .line 376
    .local v0, windowState:Lcom/android/server/wm/WindowState;
    :goto_7
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@9
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@b
    invoke-interface {v1, v0, p2, p3}, Landroid/view/WindowManagerPolicy;->interceptKeyBeforeDispatching(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)J

    #@e
    move-result-wide v1

    #@f
    return-wide v1

    #@10
    .line 375
    .end local v0           #windowState:Lcom/android/server/wm/WindowState;
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_7
.end method

.method public interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
    .registers 5
    .parameter "event"
    .parameter "policyFlags"
    .parameter "isScreenOn"

    #@0
    .prologue
    .line 361
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    invoke-interface {v0, p1, p2, p3}, Landroid/view/WindowManagerPolicy;->interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public interceptMotionBeforeQueueingWhenScreenOff(I)I
    .registers 3
    .parameter "policyFlags"

    #@0
    .prologue
    .line 368
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    invoke-interface {v0, p1}, Landroid/view/WindowManagerPolicy;->interceptMotionBeforeQueueingWhenScreenOff(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public notifyANR(Lcom/android/server/input/InputApplicationHandle;Lcom/android/server/input/InputWindowHandle;)J
    .registers 21
    .parameter "inputApplicationHandle"
    .parameter "inputWindowHandle"

    #@0
    .prologue
    .line 93
    const/4 v3, 0x0

    #@1
    .line 94
    .local v3, appWindowToken:Lcom/android/server/wm/AppWindowToken;
    const/4 v12, 0x0

    #@2
    .line 95
    .local v12, windowState:Lcom/android/server/wm/WindowState;
    const/4 v2, 0x0

    #@3
    .line 96
    .local v2, aboveSystem:Z
    move-object/from16 v0, p0

    #@5
    iget-object v14, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@7
    iget-object v15, v14, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@9
    monitor-enter v15

    #@a
    .line 97
    if-eqz p2, :cond_18

    #@c
    .line 98
    :try_start_c
    move-object/from16 v0, p2

    #@e
    iget-object v14, v0, Lcom/android/server/input/InputWindowHandle;->windowState:Ljava/lang/Object;

    #@10
    move-object v0, v14

    #@11
    check-cast v0, Lcom/android/server/wm/WindowState;

    #@13
    move-object v12, v0

    #@14
    .line 99
    if-eqz v12, :cond_18

    #@16
    .line 100
    iget-object v3, v12, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@18
    .line 103
    :cond_18
    if-nez v3, :cond_24

    #@1a
    if-eqz p1, :cond_24

    #@1c
    .line 104
    move-object/from16 v0, p1

    #@1e
    iget-object v14, v0, Lcom/android/server/input/InputApplicationHandle;->appWindowToken:Ljava/lang/Object;

    #@20
    move-object v0, v14

    #@21
    check-cast v0, Lcom/android/server/wm/AppWindowToken;

    #@23
    move-object v3, v0

    #@24
    .line 107
    :cond_24
    if-eqz v12, :cond_b9

    #@26
    .line 108
    const-string v14, "WindowManager"

    #@28
    new-instance v16, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v17, "Input event dispatching timed out sending to "

    #@2f
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v16

    #@33
    iget-object v0, v12, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@35
    move-object/from16 v17, v0

    #@37
    invoke-virtual/range {v17 .. v17}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@3a
    move-result-object v17

    #@3b
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v16

    #@3f
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v16

    #@43
    move-object/from16 v0, v16

    #@45
    invoke-static {v14, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 111
    const-string v14, "Keyguard"

    #@4a
    iget-object v0, v12, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@4c
    move-object/from16 v16, v0

    #@4e
    invoke-virtual/range {v16 .. v16}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@51
    move-result-object v16

    #@52
    move-object/from16 v0, v16

    #@54
    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v14

    #@58
    if-eqz v14, :cond_8b

    #@5a
    .line 114
    invoke-static {}, Lcom/android/internal/view/WindowManagerPolicyThread;->getThread()Ljava/lang/Thread;

    #@5d
    move-result-object v13

    #@5e
    .line 115
    .local v13, wmPolicyTread:Ljava/lang/Thread;
    if-eqz v13, :cond_82

    #@60
    .line 116
    invoke-virtual {v13}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@63
    move-result-object v8

    #@64
    .line 117
    .local v8, stackTraceForPolicy:[Ljava/lang/StackTraceElement;
    const-string v14, "WindowManager"

    #@66
    const-string v16, "StackTrace For WindowManagerPolicyThread"

    #@68
    move-object/from16 v0, v16

    #@6a
    invoke-static {v14, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 119
    move-object v4, v8

    #@6e
    .local v4, arr$:[Ljava/lang/StackTraceElement;
    array-length v6, v4

    #@6f
    .local v6, len$:I
    const/4 v5, 0x0

    #@70
    .local v5, i$:I
    :goto_70
    if-ge v5, v6, :cond_8b

    #@72
    aget-object v7, v4, v5

    #@74
    .line 120
    .local v7, s:Ljava/lang/StackTraceElement;
    const-string v14, "WindowManager"

    #@76
    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    #@79
    move-result-object v16

    #@7a
    move-object/from16 v0, v16

    #@7c
    invoke-static {v14, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 119
    add-int/lit8 v5, v5, 0x1

    #@81
    goto :goto_70

    #@82
    .line 123
    .end local v4           #arr$:[Ljava/lang/StackTraceElement;
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v7           #s:Ljava/lang/StackTraceElement;
    .end local v8           #stackTraceForPolicy:[Ljava/lang/StackTraceElement;
    :cond_82
    const-string v14, "WindowManager"

    #@84
    const-string v16, "WindowManagerPolicyThread is not invoked yet."

    #@86
    move-object/from16 v0, v16

    #@88
    invoke-static {v14, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 131
    .end local v13           #wmPolicyTread:Ljava/lang/Thread;
    :cond_8b
    move-object/from16 v0, p0

    #@8d
    iget-object v14, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@8f
    iget-object v14, v14, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@91
    const/16 v16, 0x7d3

    #@93
    move/from16 v0, v16

    #@95
    invoke-interface {v14, v0}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    #@98
    move-result v9

    #@99
    .line 133
    .local v9, systemAlertLayer:I
    iget v14, v12, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@9b
    if-le v14, v9, :cond_b7

    #@9d
    const/4 v2, 0x1

    #@9e
    .line 141
    .end local v9           #systemAlertLayer:I
    :goto_9e
    move-object/from16 v0, p0

    #@a0
    iget-object v14, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@a2
    invoke-virtual {v14, v3, v12}, Lcom/android/server/wm/WindowManagerService;->saveANRStateLocked(Lcom/android/server/wm/AppWindowToken;Lcom/android/server/wm/WindowState;)V

    #@a5
    .line 142
    monitor-exit v15
    :try_end_a6
    .catchall {:try_start_c .. :try_end_a6} :catchall_da

    #@a6
    .line 144
    if-eqz v3, :cond_e7

    #@a8
    iget-object v14, v3, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@aa
    if-eqz v14, :cond_e7

    #@ac
    .line 148
    :try_start_ac
    iget-object v14, v3, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    #@ae
    invoke-interface {v14}, Landroid/view/IApplicationToken;->keyDispatchingTimedOut()Z

    #@b1
    move-result v1

    #@b2
    .line 149
    .local v1, abort:Z
    if-nez v1, :cond_fb

    #@b4
    .line 152
    iget-wide v10, v3, Lcom/android/server/wm/AppWindowToken;->inputDispatchingTimeoutNanos:J
    :try_end_b6
    .catch Landroid/os/RemoteException; {:try_start_ac .. :try_end_b6} :catch_100

    #@b6
    .line 170
    .end local v1           #abort:Z
    :cond_b6
    :goto_b6
    return-wide v10

    #@b7
    .line 133
    .restart local v9       #systemAlertLayer:I
    :cond_b7
    const/4 v2, 0x0

    #@b8
    goto :goto_9e

    #@b9
    .line 134
    .end local v9           #systemAlertLayer:I
    :cond_b9
    if-eqz v3, :cond_dd

    #@bb
    .line 135
    :try_start_bb
    const-string v14, "WindowManager"

    #@bd
    new-instance v16, Ljava/lang/StringBuilder;

    #@bf
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@c2
    const-string v17, "Input event dispatching timed out sending to application "

    #@c4
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v16

    #@c8
    iget-object v0, v3, Lcom/android/server/wm/WindowToken;->stringName:Ljava/lang/String;

    #@ca
    move-object/from16 v17, v0

    #@cc
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v16

    #@d0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v16

    #@d4
    move-object/from16 v0, v16

    #@d6
    invoke-static {v14, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    goto :goto_9e

    #@da
    .line 142
    :catchall_da
    move-exception v14

    #@db
    monitor-exit v15
    :try_end_dc
    .catchall {:try_start_bb .. :try_end_dc} :catchall_da

    #@dc
    throw v14

    #@dd
    .line 138
    :cond_dd
    :try_start_dd
    const-string v14, "WindowManager"

    #@df
    const-string v16, "Input event dispatching timed out."

    #@e1
    move-object/from16 v0, v16

    #@e3
    invoke-static {v14, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e6
    .catchall {:try_start_dd .. :try_end_e6} :catchall_da

    #@e6
    goto :goto_9e

    #@e7
    .line 156
    :cond_e7
    if-eqz v12, :cond_fb

    #@e9
    .line 160
    :try_start_e9
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@ec
    move-result-object v14

    #@ed
    iget-object v15, v12, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@ef
    iget v15, v15, Lcom/android/server/wm/Session;->mPid:I

    #@f1
    invoke-interface {v14, v15, v2}, Landroid/app/IActivityManager;->inputDispatchingTimedOut(IZ)J
    :try_end_f4
    .catch Landroid/os/RemoteException; {:try_start_e9 .. :try_end_f4} :catch_fe

    #@f4
    move-result-wide v10

    #@f5
    .line 162
    .local v10, timeout:J
    const-wide/16 v14, 0x0

    #@f7
    cmp-long v14, v10, v14

    #@f9
    if-gez v14, :cond_b6

    #@fb
    .line 170
    .end local v10           #timeout:J
    :cond_fb
    :goto_fb
    const-wide/16 v10, 0x0

    #@fd
    goto :goto_b6

    #@fe
    .line 167
    :catch_fe
    move-exception v14

    #@ff
    goto :goto_fb

    #@100
    .line 154
    :catch_100
    move-exception v14

    #@101
    goto :goto_fb
.end method

.method public notifyConfigurationChanged()V
    .registers 3

    #@0
    .prologue
    .line 324
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->sendNewConfiguration()V

    #@5
    .line 326
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReadyMonitor:Ljava/lang/Object;

    #@7
    monitor-enter v1

    #@8
    .line 327
    :try_start_8
    iget-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReady:Z

    #@a
    if-nez v0, :cond_14

    #@c
    .line 328
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReady:Z

    #@f
    .line 329
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReadyMonitor:Ljava/lang/Object;

    #@11
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@14
    .line 331
    :cond_14
    monitor-exit v1

    #@15
    .line 332
    return-void

    #@16
    .line 331
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public notifyInputChannelBroken(Lcom/android/server/input/InputWindowHandle;)V
    .registers 7
    .parameter "inputWindowHandle"

    #@0
    .prologue
    .line 73
    if-nez p1, :cond_3

    #@2
    .line 84
    :goto_2
    return-void

    #@3
    .line 77
    :cond_3
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@5
    iget-object v2, v1, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    #@7
    monitor-enter v2

    #@8
    .line 78
    :try_start_8
    iget-object v0, p1, Lcom/android/server/input/InputWindowHandle;->windowState:Ljava/lang/Object;

    #@a
    check-cast v0, Lcom/android/server/wm/WindowState;

    #@c
    .line 79
    .local v0, windowState:Lcom/android/server/wm/WindowState;
    if-eqz v0, :cond_2d

    #@e
    .line 80
    const-string v1, "WindowManager"

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "WINDOW DIED "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 81
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@28
    iget-object v3, v0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    #@2a
    invoke-virtual {v1, v3, v0}, Lcom/android/server/wm/WindowManagerService;->removeWindowLocked(Lcom/android/server/wm/Session;Lcom/android/server/wm/WindowState;)V

    #@2d
    .line 83
    :cond_2d
    monitor-exit v2

    #@2e
    goto :goto_2

    #@2f
    .end local v0           #windowState:Lcom/android/server/wm/WindowState;
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_2f

    #@31
    throw v1
.end method

.method public notifyJackSwitchChanged(JII)V
    .registers 6
    .parameter "whenNanos"
    .parameter "switchValues"
    .parameter "switchMask"

    #@0
    .prologue
    .line 354
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/WindowManagerPolicy;->notifyJackSwitchChanged(JII)V

    #@7
    .line 355
    return-void
.end method

.method public notifyLidSwitchChanged(JZ)V
    .registers 5
    .parameter "whenNanos"
    .parameter "lidOpen"

    #@0
    .prologue
    .line 349
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@4
    invoke-interface {v0, p1, p2, p3}, Landroid/view/WindowManagerPolicy;->notifyLidSwitchChanged(JZ)V

    #@7
    .line 350
    return-void
.end method

.method public pauseDispatchingLw(Lcom/android/server/wm/WindowToken;)V
    .registers 4
    .parameter "window"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 433
    iget-boolean v0, p1, Lcom/android/server/wm/WindowToken;->paused:Z

    #@3
    if-nez v0, :cond_a

    #@5
    .line 438
    iput-boolean v1, p1, Lcom/android/server/wm/WindowToken;->paused:Z

    #@7
    .line 439
    invoke-virtual {p0, v1}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@a
    .line 441
    :cond_a
    return-void
.end method

.method public resumeDispatchingLw(Lcom/android/server/wm/WindowToken;)V
    .registers 3
    .parameter "window"

    #@0
    .prologue
    .line 444
    iget-boolean v0, p1, Lcom/android/server/wm/WindowToken;->paused:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 449
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p1, Lcom/android/server/wm/WindowToken;->paused:Z

    #@7
    .line 450
    const/4 v0, 0x1

    #@8
    invoke-virtual {p0, v0}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@b
    .line 452
    :cond_b
    return-void
.end method

.method public setEventDispatchingLw(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 477
    iget-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchEnabled:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 482
    iput-boolean p1, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchEnabled:Z

    #@6
    .line 483
    invoke-direct {p0}, Lcom/android/server/wm/InputMonitor;->updateInputDispatchModeLw()V

    #@9
    .line 485
    :cond_9
    return-void
.end method

.method public setFocusedAppLw(Lcom/android/server/wm/AppWindowToken;)V
    .registers 5
    .parameter "newApp"

    #@0
    .prologue
    .line 421
    if-nez p1, :cond_b

    #@2
    .line 422
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v1, v2}, Lcom/android/server/input/InputManagerService;->setFocusedApplication(Lcom/android/server/input/InputApplicationHandle;)V

    #@a
    .line 430
    :goto_a
    return-void

    #@b
    .line 424
    :cond_b
    iget-object v0, p1, Lcom/android/server/wm/AppWindowToken;->mInputApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@d
    .line 425
    .local v0, handle:Lcom/android/server/input/InputApplicationHandle;
    invoke-virtual {p1}, Lcom/android/server/wm/AppWindowToken;->toString()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    iput-object v1, v0, Lcom/android/server/input/InputApplicationHandle;->name:Ljava/lang/String;

    #@13
    .line 426
    iget-wide v1, p1, Lcom/android/server/wm/AppWindowToken;->inputDispatchingTimeoutNanos:J

    #@15
    iput-wide v1, v0, Lcom/android/server/input/InputApplicationHandle;->dispatchingTimeoutNanos:J

    #@17
    .line 428
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@19
    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@1b
    invoke-virtual {v1, v0}, Lcom/android/server/input/InputManagerService;->setFocusedApplication(Lcom/android/server/input/InputApplicationHandle;)V

    #@1e
    goto :goto_a
.end method

.method public setInputFocusLw(Lcom/android/server/wm/WindowState;Z)V
    .registers 5
    .parameter "newWindow"
    .parameter "updateInputWindows"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 402
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputFocus:Lcom/android/server/wm/WindowState;

    #@3
    if-eq p1, v0, :cond_1b

    #@5
    .line 403
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->canReceiveKeys()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_11

    #@d
    .line 407
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    #@f
    iput-boolean v1, v0, Lcom/android/server/wm/WindowToken;->paused:Z

    #@11
    .line 410
    :cond_11
    iput-object p1, p0, Lcom/android/server/wm/InputMonitor;->mInputFocus:Lcom/android/server/wm/WindowState;

    #@13
    .line 411
    invoke-virtual {p0}, Lcom/android/server/wm/InputMonitor;->setUpdateInputWindowsNeededLw()V

    #@16
    .line 413
    if-eqz p2, :cond_1b

    #@18
    .line 414
    invoke-virtual {p0, v1}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsLw(Z)V

    #@1b
    .line 417
    :cond_1b
    return-void
.end method

.method public setUpdateInputWindowsNeededLw()V
    .registers 2

    #@0
    .prologue
    .line 229
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mUpdateInputWindowsNeeded:Z

    #@3
    .line 230
    return-void
.end method

.method public thawInputDispatchingLw()V
    .registers 2

    #@0
    .prologue
    .line 466
    iget-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchFrozen:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 471
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDispatchFrozen:Z

    #@7
    .line 472
    invoke-direct {p0}, Lcom/android/server/wm/InputMonitor;->updateInputDispatchModeLw()V

    #@a
    .line 474
    :cond_a
    return-void
.end method

.method public updateInputWindowsLw(Z)V
    .registers 29
    .parameter "force"

    #@0
    .prologue
    .line 234
    if-nez p1, :cond_9

    #@2
    move-object/from16 v0, p0

    #@4
    iget-boolean v2, v0, Lcom/android/server/wm/InputMonitor;->mUpdateInputWindowsNeeded:Z

    #@6
    if-nez v2, :cond_9

    #@8
    .line 320
    :goto_8
    return-void

    #@9
    .line 237
    :cond_9
    const/4 v2, 0x0

    #@a
    move-object/from16 v0, p0

    #@c
    iput-boolean v2, v0, Lcom/android/server/wm/InputMonitor;->mUpdateInputWindowsNeeded:Z

    #@e
    .line 246
    move-object/from16 v0, p0

    #@10
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@12
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@14
    iget-object v0, v2, Lcom/android/server/wm/WindowAnimator;->mUniverseBackground:Lcom/android/server/wm/WindowStateAnimator;

    #@16
    move-object/from16 v26, v0

    #@18
    .line 247
    .local v26, universeBackground:Lcom/android/server/wm/WindowStateAnimator;
    move-object/from16 v0, p0

    #@1a
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1c
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@1e
    iget v14, v2, Lcom/android/server/wm/WindowAnimator;->mAboveUniverseLayer:I

    #@20
    .line 248
    .local v14, aboveUniverseLayer:I
    const/4 v15, 0x0

    #@21
    .line 251
    .local v15, addedUniverse:Z
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@25
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@27
    if-eqz v2, :cond_68

    #@29
    const/16 v20, 0x1

    #@2b
    .line 252
    .local v20, inDrag:Z
    :goto_2b
    if-eqz v20, :cond_40

    #@2d
    .line 256
    move-object/from16 v0, p0

    #@2f
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@31
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@33
    iget-object v0, v2, Lcom/android/server/wm/DragState;->mDragWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@35
    move-object/from16 v17, v0

    #@37
    .line 257
    .local v17, dragWindowHandle:Lcom/android/server/input/InputWindowHandle;
    if-eqz v17, :cond_6b

    #@39
    .line 258
    move-object/from16 v0, p0

    #@3b
    move-object/from16 v1, v17

    #@3d
    invoke-direct {v0, v1}, Lcom/android/server/wm/InputMonitor;->addInputWindowHandleLw(Lcom/android/server/input/InputWindowHandle;)V

    #@40
    .line 265
    .end local v17           #dragWindowHandle:Lcom/android/server/input/InputWindowHandle;
    :cond_40
    :goto_40
    move-object/from16 v0, p0

    #@42
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@44
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mFakeWindows:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@49
    move-result v13

    #@4a
    .line 266
    .local v13, NFW:I
    const/16 v19, 0x0

    #@4c
    .local v19, i:I
    :goto_4c
    move/from16 v0, v19

    #@4e
    if-ge v0, v13, :cond_73

    #@50
    .line 267
    move-object/from16 v0, p0

    #@52
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@54
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mFakeWindows:Ljava/util/ArrayList;

    #@56
    move/from16 v0, v19

    #@58
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5b
    move-result-object v2

    #@5c
    check-cast v2, Lcom/android/server/wm/FakeWindowImpl;

    #@5e
    iget-object v2, v2, Lcom/android/server/wm/FakeWindowImpl;->mWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@60
    move-object/from16 v0, p0

    #@62
    invoke-direct {v0, v2}, Lcom/android/server/wm/InputMonitor;->addInputWindowHandleLw(Lcom/android/server/input/InputWindowHandle;)V

    #@65
    .line 266
    add-int/lit8 v19, v19, 0x1

    #@67
    goto :goto_4c

    #@68
    .line 251
    .end local v13           #NFW:I
    .end local v19           #i:I
    .end local v20           #inDrag:Z
    :cond_68
    const/16 v20, 0x0

    #@6a
    goto :goto_2b

    #@6b
    .line 260
    .restart local v17       #dragWindowHandle:Lcom/android/server/input/InputWindowHandle;
    .restart local v20       #inDrag:Z
    :cond_6b
    const-string v2, "WindowManager"

    #@6d
    const-string v3, "Drag is in progress but there is no drag window handle."

    #@6f
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_40

    #@73
    .line 271
    .end local v17           #dragWindowHandle:Lcom/android/server/input/InputWindowHandle;
    .restart local v13       #NFW:I
    .restart local v19       #i:I
    :cond_73
    new-instance v23, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;

    #@75
    move-object/from16 v0, p0

    #@77
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@79
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@7c
    const/4 v3, 0x1

    #@7d
    move-object/from16 v0, v23

    #@7f
    invoke-direct {v0, v2, v3}, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;-><init>(Lcom/android/server/wm/WindowManagerService;Z)V

    #@82
    .line 273
    .local v23, iterator:Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;
    :cond_82
    :goto_82
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->hasNext()Z

    #@85
    move-result v2

    #@86
    if-eqz v2, :cond_13a

    #@88
    .line 274
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/wm/WindowManagerService$AllWindowsIterator;->next()Lcom/android/server/wm/WindowState;

    #@8b
    move-result-object v16

    #@8c
    .line 275
    .local v16, child:Lcom/android/server/wm/WindowState;
    move-object/from16 v0, v16

    #@8e
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@90
    move-object/from16 v21, v0

    #@92
    .line 276
    .local v21, inputChannel:Landroid/view/InputChannel;
    move-object/from16 v0, v16

    #@94
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@96
    move-object/from16 v22, v0

    #@98
    .line 277
    .local v22, inputWindowHandle:Lcom/android/server/input/InputWindowHandle;
    if-eqz v21, :cond_82

    #@9a
    if-eqz v22, :cond_82

    #@9c
    move-object/from16 v0, v16

    #@9e
    iget-boolean v2, v0, Lcom/android/server/wm/WindowState;->mRemoved:Z

    #@a0
    if-nez v2, :cond_82

    #@a2
    .line 282
    move-object/from16 v0, v16

    #@a4
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@a6
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@a8
    move/from16 v18, v0

    #@aa
    .line 283
    .local v18, flags:I
    move-object/from16 v0, v16

    #@ac
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@ae
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@b0
    move/from16 v25, v0

    #@b2
    .line 285
    .local v25, type:I
    move-object/from16 v0, p0

    #@b4
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mInputFocus:Lcom/android/server/wm/WindowState;

    #@b6
    move-object/from16 v0, v16

    #@b8
    if-ne v0, v2, :cond_131

    #@ba
    const/4 v11, 0x1

    #@bb
    .line 286
    .local v11, hasFocus:Z
    :goto_bb
    invoke-virtual/range {v16 .. v16}, Lcom/android/server/wm/WindowState;->isVisibleLw()Z

    #@be
    move-result v10

    #@bf
    .line 287
    .local v10, isVisible:Z
    move-object/from16 v0, p0

    #@c1
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@c3
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@c5
    move-object/from16 v0, v16

    #@c7
    if-ne v0, v2, :cond_133

    #@c9
    const/16 v2, 0x7d4

    #@cb
    move/from16 v0, v25

    #@cd
    if-eq v0, v2, :cond_133

    #@cf
    const/4 v12, 0x1

    #@d0
    .line 289
    .local v12, hasWallpaper:Z
    :goto_d0
    invoke-virtual/range {v16 .. v16}, Lcom/android/server/wm/WindowState;->getDisplayId()I

    #@d3
    move-result v2

    #@d4
    if-nez v2, :cond_135

    #@d6
    const/16 v24, 0x1

    #@d8
    .line 293
    .local v24, onDefaultDisplay:Z
    :goto_d8
    if-eqz v20, :cond_e9

    #@da
    if-eqz v10, :cond_e9

    #@dc
    if-eqz v24, :cond_e9

    #@de
    .line 294
    move-object/from16 v0, p0

    #@e0
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@e2
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragState:Lcom/android/server/wm/DragState;

    #@e4
    move-object/from16 v0, v16

    #@e6
    invoke-virtual {v2, v0}, Lcom/android/server/wm/DragState;->sendDragStartedIfNeededLw(Lcom/android/server/wm/WindowState;)V

    #@e9
    .line 297
    :cond_e9
    if-eqz v26, :cond_11a

    #@eb
    if-nez v15, :cond_11a

    #@ed
    move-object/from16 v0, v16

    #@ef
    iget v2, v0, Lcom/android/server/wm/WindowState;->mBaseLayer:I

    #@f1
    if-ge v2, v14, :cond_11a

    #@f3
    if-eqz v24, :cond_11a

    #@f5
    .line 299
    move-object/from16 v0, v26

    #@f7
    iget-object v4, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@f9
    .line 300
    .local v4, u:Lcom/android/server/wm/WindowState;
    iget-object v2, v4, Lcom/android/server/wm/WindowState;->mInputChannel:Landroid/view/InputChannel;

    #@fb
    if-eqz v2, :cond_119

    #@fd
    iget-object v2, v4, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@ff
    if-eqz v2, :cond_119

    #@101
    .line 301
    iget-object v3, v4, Lcom/android/server/wm/WindowState;->mInputWindowHandle:Lcom/android/server/input/InputWindowHandle;

    #@103
    iget-object v2, v4, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@105
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@107
    iget-object v2, v4, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@109
    iget v6, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@10b
    const/4 v7, 0x1

    #@10c
    move-object/from16 v0, p0

    #@10e
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mInputFocus:Lcom/android/server/wm/WindowState;

    #@110
    if-ne v4, v2, :cond_138

    #@112
    const/4 v8, 0x1

    #@113
    :goto_113
    const/4 v9, 0x0

    #@114
    move-object/from16 v2, p0

    #@116
    invoke-direct/range {v2 .. v9}, Lcom/android/server/wm/InputMonitor;->addInputWindowHandleLw(Lcom/android/server/input/InputWindowHandle;Lcom/android/server/wm/WindowState;IIZZZ)V

    #@119
    .line 304
    :cond_119
    const/4 v15, 0x1

    #@11a
    .line 307
    .end local v4           #u:Lcom/android/server/wm/WindowState;
    :cond_11a
    move-object/from16 v0, v16

    #@11c
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@11e
    move-object/from16 v0, v26

    #@120
    if-eq v2, v0, :cond_82

    #@122
    move-object/from16 v5, p0

    #@124
    move-object/from16 v6, v22

    #@126
    move-object/from16 v7, v16

    #@128
    move/from16 v8, v18

    #@12a
    move/from16 v9, v25

    #@12c
    .line 308
    invoke-direct/range {v5 .. v12}, Lcom/android/server/wm/InputMonitor;->addInputWindowHandleLw(Lcom/android/server/input/InputWindowHandle;Lcom/android/server/wm/WindowState;IIZZZ)V

    #@12f
    goto/16 :goto_82

    #@131
    .line 285
    .end local v10           #isVisible:Z
    .end local v11           #hasFocus:Z
    .end local v12           #hasWallpaper:Z
    .end local v24           #onDefaultDisplay:Z
    :cond_131
    const/4 v11, 0x0

    #@132
    goto :goto_bb

    #@133
    .line 287
    .restart local v10       #isVisible:Z
    .restart local v11       #hasFocus:Z
    :cond_133
    const/4 v12, 0x0

    #@134
    goto :goto_d0

    #@135
    .line 289
    .restart local v12       #hasWallpaper:Z
    :cond_135
    const/16 v24, 0x0

    #@137
    goto :goto_d8

    #@138
    .line 301
    .restart local v4       #u:Lcom/android/server/wm/WindowState;
    .restart local v24       #onDefaultDisplay:Z
    :cond_138
    const/4 v8, 0x0

    #@139
    goto :goto_113

    #@13a
    .line 314
    .end local v4           #u:Lcom/android/server/wm/WindowState;
    .end local v10           #isVisible:Z
    .end local v11           #hasFocus:Z
    .end local v12           #hasWallpaper:Z
    .end local v16           #child:Lcom/android/server/wm/WindowState;
    .end local v18           #flags:I
    .end local v21           #inputChannel:Landroid/view/InputChannel;
    .end local v22           #inputWindowHandle:Lcom/android/server/input/InputWindowHandle;
    .end local v24           #onDefaultDisplay:Z
    .end local v25           #type:I
    :cond_13a
    move-object/from16 v0, p0

    #@13c
    iget-object v2, v0, Lcom/android/server/wm/InputMonitor;->mService:Lcom/android/server/wm/WindowManagerService;

    #@13e
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@140
    move-object/from16 v0, p0

    #@142
    iget-object v3, v0, Lcom/android/server/wm/InputMonitor;->mInputWindowHandles:[Lcom/android/server/input/InputWindowHandle;

    #@144
    invoke-virtual {v2, v3}, Lcom/android/server/input/InputManagerService;->setInputWindows([Lcom/android/server/input/InputWindowHandle;)V

    #@147
    .line 317
    invoke-direct/range {p0 .. p0}, Lcom/android/server/wm/InputMonitor;->clearInputWindowHandlesLw()V

    #@14a
    goto/16 :goto_8
.end method

.method public waitForInputDevicesReady(J)Z
    .registers 5
    .parameter "timeoutMillis"

    #@0
    .prologue
    .line 336
    iget-object v1, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReadyMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 337
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReady:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_10

    #@5
    if-nez v0, :cond_c

    #@7
    .line 339
    :try_start_7
    iget-object v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReadyMonitor:Ljava/lang/Object;

    #@9
    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_10
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_c} :catch_13

    #@c
    .line 343
    :cond_c
    :goto_c
    :try_start_c
    iget-boolean v0, p0, Lcom/android/server/wm/InputMonitor;->mInputDevicesReady:Z

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 344
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_10

    #@12
    throw v0

    #@13
    .line 340
    :catch_13
    move-exception v0

    #@14
    goto :goto_c
.end method
