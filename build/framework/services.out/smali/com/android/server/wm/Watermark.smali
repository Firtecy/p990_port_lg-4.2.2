.class Lcom/android/server/wm/Watermark;
.super Ljava/lang/Object;
.source "Watermark.java"


# instance fields
.field final mDeltaX:I

.field final mDeltaY:I

.field final mDisplay:Landroid/view/Display;

.field mDrawNeeded:Z

.field mLastDH:I

.field mLastDW:I

.field mSurface:Landroid/view/Surface;

.field final mText:Ljava/lang/String;

.field final mTextAscent:I

.field final mTextDescent:I

.field final mTextHeight:I

.field final mTextPaint:Landroid/graphics/Paint;

.field final mTextWidth:I

.field final mTokens:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/view/Display;Landroid/util/DisplayMetrics;Landroid/view/SurfaceSession;[Ljava/lang/String;)V
    .registers 26
    .parameter "display"
    .parameter "dm"
    .parameter "session"
    .parameter "tokens"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    move-object/from16 v0, p1

    #@5
    move-object/from16 v1, p0

    #@7
    iput-object v0, v1, Lcom/android/server/wm/Watermark;->mDisplay:Landroid/view/Display;

    #@9
    .line 63
    move-object/from16 v0, p4

    #@b
    move-object/from16 v1, p0

    #@d
    iput-object v0, v1, Lcom/android/server/wm/Watermark;->mTokens:[Ljava/lang/String;

    #@f
    .line 65
    new-instance v9, Ljava/lang/StringBuilder;

    #@11
    const/16 v2, 0x20

    #@13
    invoke-direct {v9, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@16
    .line 66
    .local v9, builder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    #@18
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTokens:[Ljava/lang/String;

    #@1a
    const/4 v3, 0x0

    #@1b
    aget-object v2, v2, v3

    #@1d
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@20
    move-result v16

    #@21
    .line 67
    .local v16, len:I
    and-int/lit8 v16, v16, -0x2

    #@23
    .line 68
    const/4 v15, 0x0

    #@24
    .local v15, i:I
    :goto_24
    move/from16 v0, v16

    #@26
    if-ge v15, v0, :cond_84

    #@28
    .line 69
    move-object/from16 v0, p0

    #@2a
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTokens:[Ljava/lang/String;

    #@2c
    const/4 v3, 0x0

    #@2d
    aget-object v2, v2, v3

    #@2f
    invoke-virtual {v2, v15}, Ljava/lang/String;->charAt(I)C

    #@32
    move-result v10

    #@33
    .line 70
    .local v10, c1:I
    move-object/from16 v0, p0

    #@35
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTokens:[Ljava/lang/String;

    #@37
    const/4 v3, 0x0

    #@38
    aget-object v2, v2, v3

    #@3a
    add-int/lit8 v3, v15, 0x1

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    #@3f
    move-result v11

    #@40
    .line 71
    .local v11, c2:I
    const/16 v2, 0x61

    #@42
    if-lt v10, v2, :cond_64

    #@44
    const/16 v2, 0x66

    #@46
    if-gt v10, v2, :cond_64

    #@48
    add-int/lit8 v2, v10, -0x61

    #@4a
    add-int/lit8 v10, v2, 0xa

    #@4c
    .line 74
    :goto_4c
    const/16 v2, 0x61

    #@4e
    if-lt v11, v2, :cond_74

    #@50
    const/16 v2, 0x66

    #@52
    if-gt v11, v2, :cond_74

    #@54
    add-int/lit8 v2, v11, -0x61

    #@56
    add-int/lit8 v11, v2, 0xa

    #@58
    .line 77
    :goto_58
    mul-int/lit8 v2, v10, 0x10

    #@5a
    add-int/2addr v2, v11

    #@5b
    rsub-int v2, v2, 0xff

    #@5d
    int-to-char v2, v2

    #@5e
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@61
    .line 68
    add-int/lit8 v15, v15, 0x2

    #@63
    goto :goto_24

    #@64
    .line 72
    :cond_64
    const/16 v2, 0x41

    #@66
    if-lt v10, v2, :cond_71

    #@68
    const/16 v2, 0x46

    #@6a
    if-gt v10, v2, :cond_71

    #@6c
    add-int/lit8 v2, v10, -0x41

    #@6e
    add-int/lit8 v10, v2, 0xa

    #@70
    goto :goto_4c

    #@71
    .line 73
    :cond_71
    add-int/lit8 v10, v10, -0x30

    #@73
    goto :goto_4c

    #@74
    .line 75
    :cond_74
    const/16 v2, 0x41

    #@76
    if-lt v11, v2, :cond_81

    #@78
    const/16 v2, 0x46

    #@7a
    if-gt v11, v2, :cond_81

    #@7c
    add-int/lit8 v2, v11, -0x41

    #@7e
    add-int/lit8 v11, v2, 0xa

    #@80
    goto :goto_58

    #@81
    .line 76
    :cond_81
    add-int/lit8 v11, v11, -0x30

    #@83
    goto :goto_58

    #@84
    .line 79
    .end local v10           #c1:I
    .end local v11           #c2:I
    :cond_84
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    move-object/from16 v0, p0

    #@8a
    iput-object v2, v0, Lcom/android/server/wm/Watermark;->mText:Ljava/lang/String;

    #@8c
    .line 84
    const/4 v2, 0x1

    #@8d
    const/4 v3, 0x1

    #@8e
    const/16 v4, 0x14

    #@90
    move-object/from16 v0, p4

    #@92
    move-object/from16 v1, p2

    #@94
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@97
    move-result v14

    #@98
    .line 87
    .local v14, fontSize:I
    new-instance v2, Landroid/graphics/Paint;

    #@9a
    const/4 v3, 0x1

    #@9b
    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    #@9e
    move-object/from16 v0, p0

    #@a0
    iput-object v2, v0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@a2
    .line 88
    move-object/from16 v0, p0

    #@a4
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@a6
    int-to-float v3, v14

    #@a7
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    #@aa
    .line 89
    move-object/from16 v0, p0

    #@ac
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@ae
    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@b0
    const/4 v4, 0x1

    #@b1
    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    #@b4
    move-result-object v3

    #@b5
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@b8
    .line 91
    move-object/from16 v0, p0

    #@ba
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@bc
    invoke-virtual {v2}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    #@bf
    move-result-object v13

    #@c0
    .line 92
    .local v13, fm:Landroid/graphics/Paint$FontMetricsInt;
    move-object/from16 v0, p0

    #@c2
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@c4
    move-object/from16 v0, p0

    #@c6
    iget-object v3, v0, Lcom/android/server/wm/Watermark;->mText:Ljava/lang/String;

    #@c8
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    #@cb
    move-result v2

    #@cc
    float-to-int v2, v2

    #@cd
    move-object/from16 v0, p0

    #@cf
    iput v2, v0, Lcom/android/server/wm/Watermark;->mTextWidth:I

    #@d1
    .line 93
    iget v2, v13, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@d3
    move-object/from16 v0, p0

    #@d5
    iput v2, v0, Lcom/android/server/wm/Watermark;->mTextAscent:I

    #@d7
    .line 94
    iget v2, v13, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@d9
    move-object/from16 v0, p0

    #@db
    iput v2, v0, Lcom/android/server/wm/Watermark;->mTextDescent:I

    #@dd
    .line 95
    iget v2, v13, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@df
    iget v3, v13, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@e1
    sub-int/2addr v2, v3

    #@e2
    move-object/from16 v0, p0

    #@e4
    iput v2, v0, Lcom/android/server/wm/Watermark;->mTextHeight:I

    #@e6
    .line 97
    const/4 v2, 0x2

    #@e7
    const/4 v3, 0x0

    #@e8
    move-object/from16 v0, p0

    #@ea
    iget v4, v0, Lcom/android/server/wm/Watermark;->mTextWidth:I

    #@ec
    mul-int/lit8 v4, v4, 0x2

    #@ee
    move-object/from16 v0, p4

    #@f0
    move-object/from16 v1, p2

    #@f2
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@f5
    move-result v2

    #@f6
    move-object/from16 v0, p0

    #@f8
    iput v2, v0, Lcom/android/server/wm/Watermark;->mDeltaX:I

    #@fa
    .line 99
    const/4 v2, 0x3

    #@fb
    const/4 v3, 0x0

    #@fc
    move-object/from16 v0, p0

    #@fe
    iget v4, v0, Lcom/android/server/wm/Watermark;->mTextHeight:I

    #@100
    mul-int/lit8 v4, v4, 0x3

    #@102
    move-object/from16 v0, p4

    #@104
    move-object/from16 v1, p2

    #@106
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@109
    move-result v2

    #@10a
    move-object/from16 v0, p0

    #@10c
    iput v2, v0, Lcom/android/server/wm/Watermark;->mDeltaY:I

    #@10e
    .line 101
    const/4 v2, 0x4

    #@10f
    const/4 v3, 0x0

    #@110
    const/high16 v4, -0x5000

    #@112
    move-object/from16 v0, p4

    #@114
    move-object/from16 v1, p2

    #@116
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@119
    move-result v17

    #@11a
    .line 103
    .local v17, shadowColor:I
    const/4 v2, 0x5

    #@11b
    const/4 v3, 0x0

    #@11c
    const v4, 0x60ffffff

    #@11f
    move-object/from16 v0, p4

    #@121
    move-object/from16 v1, p2

    #@123
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@126
    move-result v12

    #@127
    .line 105
    .local v12, color:I
    const/4 v2, 0x6

    #@128
    const/4 v3, 0x0

    #@129
    const/4 v4, 0x7

    #@12a
    move-object/from16 v0, p4

    #@12c
    move-object/from16 v1, p2

    #@12e
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@131
    move-result v20

    #@132
    .line 107
    .local v20, shadowRadius:I
    const/16 v2, 0x8

    #@134
    const/4 v3, 0x0

    #@135
    const/4 v4, 0x0

    #@136
    move-object/from16 v0, p4

    #@138
    move-object/from16 v1, p2

    #@13a
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@13d
    move-result v18

    #@13e
    .line 109
    .local v18, shadowDx:I
    const/16 v2, 0x9

    #@140
    const/4 v3, 0x0

    #@141
    const/4 v4, 0x0

    #@142
    move-object/from16 v0, p4

    #@144
    move-object/from16 v1, p2

    #@146
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/server/wm/WindowManagerService;->getPropertyInt([Ljava/lang/String;IIILandroid/util/DisplayMetrics;)I

    #@149
    move-result v19

    #@14a
    .line 112
    .local v19, shadowDy:I
    move-object/from16 v0, p0

    #@14c
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@14e
    invoke-virtual {v2, v12}, Landroid/graphics/Paint;->setColor(I)V

    #@151
    .line 113
    move-object/from16 v0, p0

    #@153
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@155
    move/from16 v0, v20

    #@157
    int-to-float v3, v0

    #@158
    move/from16 v0, v18

    #@15a
    int-to-float v4, v0

    #@15b
    move/from16 v0, v19

    #@15d
    int-to-float v5, v0

    #@15e
    move/from16 v0, v17

    #@160
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    #@163
    .line 116
    :try_start_163
    new-instance v2, Landroid/view/Surface;

    #@165
    const-string v4, "WatermarkSurface"

    #@167
    const/4 v5, 0x1

    #@168
    const/4 v6, 0x1

    #@169
    const/4 v7, -0x3

    #@16a
    const/4 v8, 0x4

    #@16b
    move-object/from16 v3, p3

    #@16d
    invoke-direct/range {v2 .. v8}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@170
    move-object/from16 v0, p0

    #@172
    iput-object v2, v0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@174
    .line 118
    move-object/from16 v0, p0

    #@176
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@178
    move-object/from16 v0, p0

    #@17a
    iget-object v3, v0, Lcom/android/server/wm/Watermark;->mDisplay:Landroid/view/Display;

    #@17c
    invoke-virtual {v3}, Landroid/view/Display;->getLayerStack()I

    #@17f
    move-result v3

    #@180
    invoke-virtual {v2, v3}, Landroid/view/Surface;->setLayerStack(I)V

    #@183
    .line 119
    move-object/from16 v0, p0

    #@185
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@187
    const v3, 0xf4240

    #@18a
    invoke-virtual {v2, v3}, Landroid/view/Surface;->setLayer(I)V

    #@18d
    .line 120
    move-object/from16 v0, p0

    #@18f
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@191
    const/4 v3, 0x0

    #@192
    const/4 v4, 0x0

    #@193
    invoke-virtual {v2, v3, v4}, Landroid/view/Surface;->setPosition(II)V

    #@196
    .line 121
    move-object/from16 v0, p0

    #@198
    iget-object v2, v0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@19a
    invoke-virtual {v2}, Landroid/view/Surface;->show()V
    :try_end_19d
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_163 .. :try_end_19d} :catch_19e

    #@19d
    .line 124
    :goto_19d
    return-void

    #@19e
    .line 122
    :catch_19e
    move-exception v2

    #@19f
    goto :goto_19d
.end method


# virtual methods
.method drawIfNeeded()V
    .registers 16

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 136
    iget-boolean v11, p0, Lcom/android/server/wm/Watermark;->mDrawNeeded:Z

    #@3
    if-eqz v11, :cond_5b

    #@5
    .line 137
    iget v6, p0, Lcom/android/server/wm/Watermark;->mLastDW:I

    #@7
    .line 138
    .local v6, dw:I
    iget v3, p0, Lcom/android/server/wm/Watermark;->mLastDH:I

    #@9
    .line 140
    .local v3, dh:I
    iput-boolean v12, p0, Lcom/android/server/wm/Watermark;->mDrawNeeded:Z

    #@b
    .line 141
    new-instance v4, Landroid/graphics/Rect;

    #@d
    invoke-direct {v4, v12, v12, v6, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    #@10
    .line 142
    .local v4, dirty:Landroid/graphics/Rect;
    const/4 v0, 0x0

    #@11
    .line 144
    .local v0, c:Landroid/graphics/Canvas;
    :try_start_11
    iget-object v11, p0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@13
    invoke-virtual {v11, v4}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    :try_end_16
    .catch Ljava/lang/IllegalArgumentException; {:try_start_11 .. :try_end_16} :catch_5e
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_11 .. :try_end_16} :catch_5c

    #@16
    move-result-object v0

    #@17
    .line 148
    :goto_17
    if-eqz v0, :cond_5b

    #@19
    .line 149
    sget-object v11, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@1b
    invoke-virtual {v0, v12, v11}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@1e
    .line 151
    iget v1, p0, Lcom/android/server/wm/Watermark;->mDeltaX:I

    #@20
    .line 152
    .local v1, deltaX:I
    iget v2, p0, Lcom/android/server/wm/Watermark;->mDeltaY:I

    #@22
    .line 156
    .local v2, deltaY:I
    iget v11, p0, Lcom/android/server/wm/Watermark;->mTextWidth:I

    #@24
    add-int/2addr v11, v6

    #@25
    div-int v5, v11, v1

    #@27
    .line 157
    .local v5, div:I
    iget v11, p0, Lcom/android/server/wm/Watermark;->mTextWidth:I

    #@29
    add-int/2addr v11, v6

    #@2a
    mul-int v12, v5, v1

    #@2c
    sub-int v8, v11, v12

    #@2e
    .line 158
    .local v8, rem:I
    div-int/lit8 v7, v1, 0x4

    #@30
    .line 159
    .local v7, qdelta:I
    if-lt v8, v7, :cond_36

    #@32
    sub-int v11, v1, v7

    #@34
    if-le v8, v11, :cond_39

    #@36
    .line 160
    :cond_36
    div-int/lit8 v11, v1, 0x3

    #@38
    add-int/2addr v1, v11

    #@39
    .line 163
    :cond_39
    iget v11, p0, Lcom/android/server/wm/Watermark;->mTextHeight:I

    #@3b
    neg-int v10, v11

    #@3c
    .line 164
    .local v10, y:I
    iget v11, p0, Lcom/android/server/wm/Watermark;->mTextWidth:I

    #@3e
    neg-int v9, v11

    #@3f
    .line 165
    .local v9, x:I
    :cond_3f
    :goto_3f
    iget v11, p0, Lcom/android/server/wm/Watermark;->mTextHeight:I

    #@41
    add-int/2addr v11, v3

    #@42
    if-ge v10, v11, :cond_56

    #@44
    .line 166
    iget-object v11, p0, Lcom/android/server/wm/Watermark;->mText:Ljava/lang/String;

    #@46
    int-to-float v12, v9

    #@47
    int-to-float v13, v10

    #@48
    iget-object v14, p0, Lcom/android/server/wm/Watermark;->mTextPaint:Landroid/graphics/Paint;

    #@4a
    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@4d
    .line 167
    add-int/2addr v9, v1

    #@4e
    .line 168
    if-lt v9, v6, :cond_3f

    #@50
    .line 169
    iget v11, p0, Lcom/android/server/wm/Watermark;->mTextWidth:I

    #@52
    add-int/2addr v11, v6

    #@53
    sub-int/2addr v9, v11

    #@54
    .line 170
    add-int/2addr v10, v2

    #@55
    goto :goto_3f

    #@56
    .line 173
    :cond_56
    iget-object v11, p0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@58
    invoke-virtual {v11, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    #@5b
    .line 176
    .end local v0           #c:Landroid/graphics/Canvas;
    .end local v1           #deltaX:I
    .end local v2           #deltaY:I
    .end local v3           #dh:I
    .end local v4           #dirty:Landroid/graphics/Rect;
    .end local v5           #div:I
    .end local v6           #dw:I
    .end local v7           #qdelta:I
    .end local v8           #rem:I
    .end local v9           #x:I
    .end local v10           #y:I
    :cond_5b
    return-void

    #@5c
    .line 146
    .restart local v0       #c:Landroid/graphics/Canvas;
    .restart local v3       #dh:I
    .restart local v4       #dirty:Landroid/graphics/Rect;
    .restart local v6       #dw:I
    :catch_5c
    move-exception v11

    #@5d
    goto :goto_17

    #@5e
    .line 145
    :catch_5e
    move-exception v11

    #@5f
    goto :goto_17
.end method

.method positionSurface(II)V
    .registers 4
    .parameter "dw"
    .parameter "dh"

    #@0
    .prologue
    .line 127
    iget v0, p0, Lcom/android/server/wm/Watermark;->mLastDW:I

    #@2
    if-ne v0, p1, :cond_8

    #@4
    iget v0, p0, Lcom/android/server/wm/Watermark;->mLastDH:I

    #@6
    if-eq v0, p2, :cond_14

    #@8
    .line 128
    :cond_8
    iput p1, p0, Lcom/android/server/wm/Watermark;->mLastDW:I

    #@a
    .line 129
    iput p2, p0, Lcom/android/server/wm/Watermark;->mLastDH:I

    #@c
    .line 130
    iget-object v0, p0, Lcom/android/server/wm/Watermark;->mSurface:Landroid/view/Surface;

    #@e
    invoke-virtual {v0, p1, p2}, Landroid/view/Surface;->setSize(II)V

    #@11
    .line 131
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Lcom/android/server/wm/Watermark;->mDrawNeeded:Z

    #@14
    .line 133
    :cond_14
    return-void
.end method
