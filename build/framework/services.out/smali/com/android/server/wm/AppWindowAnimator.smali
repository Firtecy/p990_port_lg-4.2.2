.class public Lcom/android/server/wm/AppWindowAnimator;
.super Ljava/lang/Object;
.source "AppWindowAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/AppWindowAnimator$DummyAnimation;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "AppWindowAnimator"

.field static final sDummyAnimation:Landroid/view/animation/Animation;


# instance fields
.field allDrawn:Z

.field animInitialized:Z

.field animLayerAdjustment:I

.field animating:Z

.field animation:Landroid/view/animation/Animation;

.field freezingScreen:Z

.field hasTransformation:Z

.field mAllAppWinAnimators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/wm/WindowStateAnimator;",
            ">;"
        }
    .end annotation
.end field

.field final mAnimator:Lcom/android/server/wm/WindowAnimator;

.field final mAppToken:Lcom/android/server/wm/AppWindowToken;

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field thumbnail:Landroid/view/Surface;

.field thumbnailAnimation:Landroid/view/animation/Animation;

.field thumbnailLayer:I

.field thumbnailTransactionSeq:I

.field final thumbnailTransformation:Landroid/view/animation/Transformation;

.field thumbnailX:I

.field thumbnailY:I

.field final transformation:Landroid/view/animation/Transformation;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    new-instance v0, Lcom/android/server/wm/AppWindowAnimator$DummyAnimation;

    #@2
    invoke-direct {v0}, Lcom/android/server/wm/AppWindowAnimator$DummyAnimation;-><init>()V

    #@5
    sput-object v0, Lcom/android/server/wm/AppWindowAnimator;->sDummyAnimation:Landroid/view/animation/Animation;

    #@7
    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/AppWindowToken;)V
    .registers 3
    .parameter "atoken"

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 27
    new-instance v0, Landroid/view/animation/Transformation;

    #@5
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@a
    .line 48
    new-instance v0, Landroid/view/animation/Transformation;

    #@c
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@11
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@18
    .line 56
    iput-object p1, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@1a
    .line 57
    iget-object v0, p1, Lcom/android/server/wm/WindowToken;->service:Lcom/android/server/wm/WindowManagerService;

    #@1c
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@1e
    .line 58
    iget-object v0, p1, Lcom/android/server/wm/AppWindowToken;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@20
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@22
    .line 59
    return-void
.end method

.method private stepAnimation(J)Z
    .registers 6
    .parameter "currentTime"

    #@0
    .prologue
    .line 170
    iget-object v1, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@2
    if-nez v1, :cond_6

    #@4
    .line 171
    const/4 v0, 0x0

    #@5
    .line 184
    :goto_5
    return v0

    #@6
    .line 173
    :cond_6
    iget-object v1, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@8
    invoke-virtual {v1}, Landroid/view/animation/Transformation;->clear()V

    #@b
    .line 174
    iget-object v1, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@d
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@f
    invoke-virtual {v1, p1, p2, v2}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    #@12
    move-result v0

    #@13
    .line 177
    .local v0, more:Z
    if-nez v0, :cond_1b

    #@15
    .line 178
    const/4 v1, 0x0

    #@16
    iput-object v1, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@18
    .line 179
    invoke-virtual {p0}, Lcom/android/server/wm/AppWindowAnimator;->clearThumbnail()V

    #@1b
    .line 183
    :cond_1b
    iput-boolean v0, p0, Lcom/android/server/wm/AppWindowAnimator;->hasTransformation:Z

    #@1d
    goto :goto_5
.end method

.method private stepThumbnailAnimation(J)V
    .registers 11
    .parameter "currentTime"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 135
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@4
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->clear()V

    #@7
    .line 136
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailAnimation:Landroid/view/animation/Animation;

    #@9
    iget-object v6, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@b
    invoke-virtual {v5, p1, p2, v6}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    #@e
    .line 137
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@10
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@13
    move-result-object v5

    #@14
    iget v6, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailX:I

    #@16
    int-to-float v6, v6

    #@17
    iget v7, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailY:I

    #@19
    int-to-float v7, v7

    #@1a
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    #@1d
    .line 139
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@1f
    invoke-virtual {v5, v4}, Lcom/android/server/wm/WindowAnimator;->getScreenRotationAnimationLocked(I)Lcom/android/server/wm/ScreenRotationAnimation;

    #@22
    move-result-object v1

    #@23
    .line 141
    .local v1, screenRotationAnimation:Lcom/android/server/wm/ScreenRotationAnimation;
    if-eqz v1, :cond_75

    #@25
    invoke-virtual {v1}, Lcom/android/server/wm/ScreenRotationAnimation;->isAnimating()Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_75

    #@2b
    move v0, v3

    #@2c
    .line 143
    .local v0, screenAnimation:Z
    :goto_2c
    if-eqz v0, :cond_37

    #@2e
    .line 144
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@30
    invoke-virtual {v1}, Lcom/android/server/wm/ScreenRotationAnimation;->getEnterTransformation()Landroid/view/animation/Transformation;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v5, v6}, Landroid/view/animation/Transformation;->postCompose(Landroid/view/animation/Transformation;)V

    #@37
    .line 147
    :cond_37
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@39
    iget-object v2, v5, Lcom/android/server/wm/WindowManagerService;->mTmpFloats:[F

    #@3b
    .line 148
    .local v2, tmpFloats:[F
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@3d
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->getValues([F)V

    #@44
    .line 152
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@46
    const/4 v6, 0x2

    #@47
    aget v6, v2, v6

    #@49
    const/4 v7, 0x5

    #@4a
    aget v7, v2, v7

    #@4c
    invoke-virtual {v5, v6, v7}, Landroid/view/Surface;->setPosition(FF)V

    #@4f
    .line 160
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@51
    iget-object v6, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@53
    invoke-virtual {v6}, Landroid/view/animation/Transformation;->getAlpha()F

    #@56
    move-result v6

    #@57
    invoke-virtual {v5, v6}, Landroid/view/Surface;->setAlpha(F)V

    #@5a
    .line 163
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@5c
    iget v6, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@5e
    add-int/lit8 v6, v6, 0x5

    #@60
    add-int/lit8 v6, v6, -0x4

    #@62
    invoke-virtual {v5, v6}, Landroid/view/Surface;->setLayer(I)V

    #@65
    .line 165
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@67
    aget v4, v2, v4

    #@69
    const/4 v6, 0x3

    #@6a
    aget v6, v2, v6

    #@6c
    aget v3, v2, v3

    #@6e
    const/4 v7, 0x4

    #@6f
    aget v7, v2, v7

    #@71
    invoke-virtual {v5, v4, v6, v3, v7}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@74
    .line 167
    return-void

    #@75
    .end local v0           #screenAnimation:Z
    .end local v2           #tmpFloats:[F
    :cond_75
    move v0, v4

    #@76
    .line 141
    goto :goto_2c
.end method


# virtual methods
.method public clearAnimation()V
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 98
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@7
    .line 99
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@a
    .line 100
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/server/wm/AppWindowAnimator;->animInitialized:Z

    #@d
    .line 102
    :cond_d
    invoke-virtual {p0}, Lcom/android/server/wm/AppWindowAnimator;->clearThumbnail()V

    #@10
    .line 103
    return-void
.end method

.method public clearThumbnail()V
    .registers 2

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 107
    iget-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@6
    invoke-virtual {v0}, Landroid/view/Surface;->destroy()V

    #@9
    .line 108
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@c
    .line 110
    :cond_c
    return-void
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V
    .registers 7
    .parameter "pw"
    .parameter "prefix"
    .parameter "dumpAll"

    #@0
    .prologue
    .line 283
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v2, "mAppToken="

    #@5
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@a
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@d
    .line 284
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    const-string v2, "mAnimator="

    #@12
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@17
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1a
    .line 285
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d
    const-string v2, "freezingScreen="

    #@1f
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    iget-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->freezingScreen:Z

    #@24
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@27
    .line 286
    const-string v2, " allDrawn="

    #@29
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c
    iget-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->allDrawn:Z

    #@2e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@31
    .line 287
    const-string v2, " animLayerAdjustment="

    #@33
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36
    iget v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@38
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(I)V

    #@3b
    .line 288
    iget-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@3d
    if-nez v2, :cond_43

    #@3f
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@41
    if-eqz v2, :cond_67

    #@43
    .line 289
    :cond_43
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@46
    const-string v2, "animating="

    #@48
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4b
    iget-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@4d
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@50
    .line 290
    const-string v2, " animInitialized="

    #@52
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@55
    iget-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animInitialized:Z

    #@57
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Z)V

    #@5a
    .line 291
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d
    const-string v2, "animation="

    #@5f
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@64
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@67
    .line 293
    :cond_67
    iget-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->hasTransformation:Z

    #@69
    if-eqz v2, :cond_7b

    #@6b
    .line 294
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6e
    const-string v2, "XForm: "

    #@70
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@73
    .line 295
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@75
    invoke-virtual {v2, p1}, Landroid/view/animation/Transformation;->printShortString(Ljava/io/PrintWriter;)V

    #@78
    .line 296
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@7b
    .line 298
    :cond_7b
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@7d
    if-eqz v2, :cond_c8

    #@7f
    .line 299
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@82
    const-string v2, "thumbnail="

    #@84
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@87
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@89
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@8c
    .line 300
    const-string v2, " x="

    #@8e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@91
    iget v2, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailX:I

    #@93
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@96
    .line 301
    const-string v2, " y="

    #@98
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9b
    iget v2, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailY:I

    #@9d
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@a0
    .line 302
    const-string v2, " layer="

    #@a2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a5
    iget v2, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@a7
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(I)V

    #@aa
    .line 303
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ad
    const-string v2, "thumbnailAnimation="

    #@af
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b2
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailAnimation:Landroid/view/animation/Animation;

    #@b4
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@b7
    .line 304
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ba
    const-string v2, "thumbnailTransformation="

    #@bc
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bf
    .line 305
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailTransformation:Landroid/view/animation/Transformation;

    #@c1
    invoke-virtual {v2}, Landroid/view/animation/Transformation;->toShortString()Ljava/lang/String;

    #@c4
    move-result-object v2

    #@c5
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c8
    .line 307
    :cond_c8
    const/4 v0, 0x0

    #@c9
    .local v0, i:I
    :goto_c9
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@cb
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@ce
    move-result v2

    #@cf
    if-ge v0, v2, :cond_ef

    #@d1
    .line 308
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@d3
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d6
    move-result-object v1

    #@d7
    check-cast v1, Lcom/android/server/wm/WindowStateAnimator;

    #@d9
    .line 309
    .local v1, wanim:Lcom/android/server/wm/WindowStateAnimator;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@dc
    const-string v2, "App Win Anim #"

    #@de
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@e4
    .line 310
    const-string v2, ": "

    #@e6
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e9
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@ec
    .line 307
    add-int/lit8 v0, v0, 0x1

    #@ee
    goto :goto_c9

    #@ef
    .line 312
    .end local v1           #wanim:Lcom/android/server/wm/WindowStateAnimator;
    :cond_ef
    return-void
.end method

.method public setAnimation(Landroid/view/animation/Animation;Z)V
    .registers 8
    .parameter "anim"
    .parameter "initialized"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 64
    iput-object p1, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@3
    .line 65
    const/4 v2, 0x0

    #@4
    iput-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@6
    .line 66
    iput-boolean p2, p0, Lcom/android/server/wm/AppWindowAnimator;->animInitialized:Z

    #@8
    .line 67
    const-wide/16 v2, 0x2710

    #@a
    invoke-virtual {p1, v2, v3}, Landroid/view/animation/Animation;->restrictDuration(J)V

    #@d
    .line 68
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@f
    iget v2, v2, Lcom/android/server/wm/WindowManagerService;->mTransitionAnimationScale:F

    #@11
    invoke-virtual {p1, v2}, Landroid/view/animation/Animation;->scaleCurrentDuration(F)V

    #@14
    .line 69
    invoke-virtual {p1}, Landroid/view/animation/Animation;->getZAdjustment()I

    #@17
    move-result v1

    #@18
    .line 70
    .local v1, zorder:I
    const/4 v0, 0x0

    #@19
    .line 71
    .local v0, adj:I
    if-ne v1, v4, :cond_3b

    #@1b
    .line 72
    const/16 v0, 0x3e8

    #@1d
    .line 77
    :cond_1d
    :goto_1d
    iget v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@1f
    if-eq v2, v0, :cond_26

    #@21
    .line 78
    iput v0, p0, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@23
    .line 79
    invoke-virtual {p0}, Lcom/android/server/wm/AppWindowAnimator;->updateLayers()V

    #@26
    .line 82
    :cond_26
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@28
    invoke-virtual {v2}, Landroid/view/animation/Transformation;->clear()V

    #@2b
    .line 83
    iget-object v3, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@2d
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2f
    iget-boolean v2, v2, Lcom/android/server/wm/AppWindowToken;->reportedVisible:Z

    #@31
    if-eqz v2, :cond_41

    #@33
    const/high16 v2, 0x3f80

    #@35
    :goto_35
    invoke-virtual {v3, v2}, Landroid/view/animation/Transformation;->setAlpha(F)V

    #@38
    .line 84
    iput-boolean v4, p0, Lcom/android/server/wm/AppWindowAnimator;->hasTransformation:Z

    #@3a
    .line 85
    return-void

    #@3b
    .line 73
    :cond_3b
    const/4 v2, -0x1

    #@3c
    if-ne v1, v2, :cond_1d

    #@3e
    .line 74
    const/16 v0, -0x3e8

    #@40
    goto :goto_1d

    #@41
    .line 83
    :cond_41
    const/4 v2, 0x0

    #@42
    goto :goto_35
.end method

.method public setDummyAnimation()V
    .registers 3

    #@0
    .prologue
    .line 89
    sget-object v0, Lcom/android/server/wm/AppWindowAnimator;->sDummyAnimation:Landroid/view/animation/Animation;

    #@2
    iput-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@4
    .line 90
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/server/wm/AppWindowAnimator;->animInitialized:Z

    #@7
    .line 91
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/android/server/wm/AppWindowAnimator;->hasTransformation:Z

    #@a
    .line 92
    iget-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@c
    invoke-virtual {v0}, Landroid/view/animation/Transformation;->clear()V

    #@f
    .line 93
    iget-object v1, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@11
    iget-object v0, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@13
    iget-boolean v0, v0, Lcom/android/server/wm/AppWindowToken;->reportedVisible:Z

    #@15
    if-eqz v0, :cond_1d

    #@17
    const/high16 v0, 0x3f80

    #@19
    :goto_19
    invoke-virtual {v1, v0}, Landroid/view/animation/Transformation;->setAlpha(F)V

    #@1c
    .line 94
    return-void

    #@1d
    .line 93
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_19
.end method

.method showAllWindowsLocked()Z
    .registers 6

    #@0
    .prologue
    .line 270
    const/4 v2, 0x0

    #@1
    .line 271
    .local v2, isAnimating:Z
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 272
    .local v0, NW:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1d

    #@a
    .line 273
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Lcom/android/server/wm/WindowStateAnimator;

    #@12
    .line 276
    .local v3, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    invoke-virtual {v3}, Lcom/android/server/wm/WindowStateAnimator;->performShowLocked()Z

    #@15
    .line 277
    invoke-virtual {v3}, Lcom/android/server/wm/WindowStateAnimator;->isAnimating()Z

    #@18
    move-result v4

    #@19
    or-int/2addr v2, v4

    #@1a
    .line 272
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_8

    #@1d
    .line 279
    .end local v3           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_1d
    return v2
.end method

.method stepAnimationLocked(JII)Z
    .registers 12
    .parameter "currentTime"
    .parameter "dw"
    .parameter "dh"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 189
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4
    invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->okToDisplay()Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_56

    #@a
    .line 192
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@c
    sget-object v5, Lcom/android/server/wm/AppWindowAnimator;->sDummyAnimation:Landroid/view/animation/Animation;

    #@e
    if-ne v4, v5, :cond_12

    #@10
    move v2, v3

    #@11
    .line 266
    :cond_11
    :goto_11
    return v2

    #@12
    .line 200
    :cond_12
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@14
    iget-boolean v4, v4, Lcom/android/server/wm/AppWindowToken;->allDrawn:Z

    #@16
    if-nez v4, :cond_22

    #@18
    iget-boolean v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@1a
    if-nez v4, :cond_22

    #@1c
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@1e
    iget-boolean v4, v4, Lcom/android/server/wm/AppWindowToken;->startingDisplayed:Z

    #@20
    if-eqz v4, :cond_5f

    #@22
    :cond_22
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@24
    if-eqz v4, :cond_5f

    #@26
    .line 202
    iget-boolean v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@28
    if-nez v4, :cond_48

    #@2a
    .line 208
    iget-boolean v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animInitialized:Z

    #@2c
    if-nez v4, :cond_33

    #@2e
    .line 209
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@30
    invoke-virtual {v4, p3, p4, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    #@33
    .line 211
    :cond_33
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@35
    invoke-virtual {v4, p1, p2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@38
    .line 212
    iput-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@3a
    .line 213
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@3c
    if-eqz v4, :cond_48

    #@3e
    .line 214
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@40
    invoke-virtual {v4}, Landroid/view/Surface;->show()V

    #@43
    .line 215
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailAnimation:Landroid/view/animation/Animation;

    #@45
    invoke-virtual {v4, p1, p2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@48
    .line 218
    :cond_48
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/AppWindowAnimator;->stepAnimation(J)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_5f

    #@4e
    .line 221
    iget-object v3, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnail:Landroid/view/Surface;

    #@50
    if-eqz v3, :cond_11

    #@52
    .line 222
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/AppWindowAnimator;->stepThumbnailAnimation(J)V

    #@55
    goto :goto_11

    #@56
    .line 227
    :cond_56
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@58
    if-eqz v4, :cond_5f

    #@5a
    .line 230
    iput-boolean v2, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@5c
    .line 231
    const/4 v4, 0x0

    #@5d
    iput-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@5f
    .line 234
    :cond_5f
    iput-boolean v3, p0, Lcom/android/server/wm/AppWindowAnimator;->hasTransformation:Z

    #@61
    .line 236
    iget-boolean v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@63
    if-nez v4, :cond_6b

    #@65
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    #@67
    if-nez v4, :cond_6b

    #@69
    move v2, v3

    #@6a
    .line 237
    goto :goto_11

    #@6b
    .line 240
    :cond_6b
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@6d
    const/16 v5, 0x8

    #@6f
    const-string v6, "AppWindowToken"

    #@71
    invoke-virtual {v4, p0, v5, v6}, Lcom/android/server/wm/WindowAnimator;->setAppLayoutChanges(Lcom/android/server/wm/AppWindowAnimator;ILjava/lang/String;)V

    #@74
    .line 243
    invoke-virtual {p0}, Lcom/android/server/wm/AppWindowAnimator;->clearAnimation()V

    #@77
    .line 244
    iput-boolean v3, p0, Lcom/android/server/wm/AppWindowAnimator;->animating:Z

    #@79
    .line 245
    iget v4, p0, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@7b
    if-eqz v4, :cond_82

    #@7d
    .line 246
    iput v3, p0, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@7f
    .line 247
    invoke-virtual {p0}, Lcom/android/server/wm/AppWindowAnimator;->updateLayers()V

    #@82
    .line 249
    :cond_82
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@84
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mInputMethodTarget:Lcom/android/server/wm/WindowState;

    #@86
    if-eqz v4, :cond_97

    #@88
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@8a
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mInputMethodTarget:Lcom/android/server/wm/WindowState;

    #@8c
    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@8e
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@90
    if-ne v4, v5, :cond_97

    #@92
    .line 251
    iget-object v4, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@94
    invoke-virtual {v4, v2}, Lcom/android/server/wm/WindowManagerService;->moveInputMethodWindowsIfNeededLocked(Z)Z

    #@97
    .line 258
    :cond_97
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->transformation:Landroid/view/animation/Transformation;

    #@99
    invoke-virtual {v2}, Landroid/view/animation/Transformation;->clear()V

    #@9c
    .line 260
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@9e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a1
    move-result v0

    #@a2
    .line 261
    .local v0, N:I
    const/4 v1, 0x0

    #@a3
    .local v1, i:I
    :goto_a3
    if-ge v1, v0, :cond_b3

    #@a5
    .line 262
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAllAppWinAnimators:Ljava/util/ArrayList;

    #@a7
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@aa
    move-result-object v2

    #@ab
    check-cast v2, Lcom/android/server/wm/WindowStateAnimator;

    #@ad
    invoke-virtual {v2}, Lcom/android/server/wm/WindowStateAnimator;->finishExit()V

    #@b0
    .line 261
    add-int/lit8 v1, v1, 0x1

    #@b2
    goto :goto_a3

    #@b3
    .line 264
    :cond_b3
    iget-object v2, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@b5
    invoke-virtual {v2}, Lcom/android/server/wm/AppWindowToken;->updateReportedVisibilityLocked()V

    #@b8
    move v2, v3

    #@b9
    .line 266
    goto/16 :goto_11
.end method

.method updateLayers()V
    .registers 8

    #@0
    .prologue
    .line 113
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@2
    iget-object v5, v5, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v0

    #@8
    .line 114
    .local v0, N:I
    iget v1, p0, Lcom/android/server/wm/AppWindowAnimator;->animLayerAdjustment:I

    #@a
    .line 115
    .local v1, adj:I
    const/4 v5, -0x1

    #@b
    iput v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@d
    .line 116
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v0, :cond_50

    #@10
    .line 117
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    #@12
    iget-object v5, v5, Lcom/android/server/wm/AppWindowToken;->allAppWindows:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Lcom/android/server/wm/WindowState;

    #@1a
    .line 118
    .local v3, w:Lcom/android/server/wm/WindowState;
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@1c
    .line 119
    .local v4, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    iget v5, v3, Lcom/android/server/wm/WindowState;->mLayer:I

    #@1e
    add-int/2addr v5, v1

    #@1f
    iput v5, v4, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@21
    .line 120
    iget v5, v4, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@23
    iget v6, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@25
    if-le v5, v6, :cond_2b

    #@27
    .line 121
    iget v5, v4, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@29
    iput v5, p0, Lcom/android/server/wm/AppWindowAnimator;->thumbnailLayer:I

    #@2b
    .line 125
    :cond_2b
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@2d
    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodTarget:Lcom/android/server/wm/WindowState;

    #@2f
    if-ne v3, v5, :cond_3c

    #@31
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@33
    iget-boolean v5, v5, Lcom/android/server/wm/WindowManagerService;->mInputMethodTargetWaitingAnim:Z

    #@35
    if-nez v5, :cond_3c

    #@37
    .line 126
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@39
    invoke-virtual {v5, v1}, Lcom/android/server/wm/WindowManagerService;->setInputMethodAnimLayerAdjustment(I)V

    #@3c
    .line 128
    :cond_3c
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@3e
    iget-object v5, v5, Lcom/android/server/wm/WindowAnimator;->mWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@40
    if-ne v3, v5, :cond_4d

    #@42
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mAnimator:Lcom/android/server/wm/WindowAnimator;

    #@44
    iget-object v5, v5, Lcom/android/server/wm/WindowAnimator;->mLowerWallpaperTarget:Lcom/android/server/wm/WindowState;

    #@46
    if-nez v5, :cond_4d

    #@48
    .line 129
    iget-object v5, p0, Lcom/android/server/wm/AppWindowAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    #@4a
    invoke-virtual {v5, v1}, Lcom/android/server/wm/WindowManagerService;->setWallpaperAnimLayerAdjustmentLocked(I)V

    #@4d
    .line 116
    :cond_4d
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_e

    #@50
    .line 132
    .end local v3           #w:Lcom/android/server/wm/WindowState;
    .end local v4           #winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    :cond_50
    return-void
.end method
