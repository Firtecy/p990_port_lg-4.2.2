.class Lcom/android/server/wm/WindowManagerService$6;
.super Landroid/os/Handler;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3647
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$6;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 3649
    const/4 v2, 0x0

    #@3
    .line 3650
    .local v2, dsdpDisplayId:I
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$6;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@5
    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@7
    const-string v8, "display"

    #@9
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/hardware/display/DisplayManager;

    #@f
    .line 3651
    .local v1, displayManager:Landroid/hardware/display/DisplayManager;
    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->getDisplays()[Landroid/view/Display;

    #@12
    move-result-object v5

    #@13
    .line 3652
    .local v5, presentationDisplays:[Landroid/view/Display;
    array-length v7, v5

    #@14
    sput v7, Lcom/android/server/wm/WindowManagerService;->m_DsdpDisplayNum:I

    #@16
    .line 3654
    iget v7, p1, Landroid/os/Message;->what:I

    #@18
    if-ne v7, v9, :cond_2e

    #@1a
    .line 3655
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$6;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@1c
    invoke-static {v7}, Lcom/android/server/wm/WindowManagerService;->access$200(Lcom/android/server/wm/WindowManagerService;)I

    #@1f
    move-result v2

    #@20
    .line 3662
    :cond_20
    :goto_20
    sget v7, Lcom/android/server/wm/WindowManagerService;->m_DsdpDisplayNum:I

    #@22
    if-ne v7, v9, :cond_3d

    #@24
    .line 3664
    sput v10, Lcom/android/server/wm/WindowManagerService;->mDsdpMode:I

    #@26
    .line 3665
    const-string v7, "sys.lge.dsdp.mode"

    #@28
    const-string v8, "stop"

    #@2a
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 3700
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 3656
    :cond_2e
    iget v7, p1, Landroid/os/Message;->what:I

    #@30
    if-nez v7, :cond_20

    #@32
    .line 3657
    sget v7, Lcom/android/server/wm/WindowManagerService;->m_DsdpDisplayNum:I

    #@34
    if-le v7, v9, :cond_20

    #@36
    .line 3658
    aget-object v7, v5, v9

    #@38
    invoke-virtual {v7}, Landroid/view/Display;->getDisplayId()I

    #@3b
    move-result v2

    #@3c
    goto :goto_20

    #@3d
    .line 3667
    :cond_3d
    const/4 v3, 0x1

    #@3e
    .line 3668
    .local v3, isMirroring:Z
    const/4 v4, 0x0

    #@3f
    .line 3670
    .local v4, mirroringMode:I
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerService$6;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@41
    invoke-virtual {v7, v2}, Lcom/android/server/wm/WindowManagerService;->getDisplayContentLocked(I)Lcom/android/server/wm/DisplayContent;

    #@44
    move-result-object v0

    #@45
    .line 3671
    .local v0, displayContent:Lcom/android/server/wm/DisplayContent;
    if-nez v0, :cond_52

    #@47
    .line 3673
    const-string v7, "sys.lge.dsdp.mode"

    #@49
    const-string v8, "stop"

    #@4b
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    .line 3674
    const/4 v3, 0x1

    #@4f
    .line 3675
    sput v10, Lcom/android/server/wm/WindowManagerService;->mDsdpMode:I

    #@51
    goto :goto_2d

    #@52
    .line 3678
    :cond_52
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getWindowList()Lcom/android/server/wm/WindowList;

    #@55
    move-result-object v6

    #@56
    .line 3679
    .local v6, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual {v6}, Lcom/android/server/wm/WindowList;->size()I

    #@59
    move-result v7

    #@5a
    if-eqz v7, :cond_5d

    #@5c
    .line 3680
    const/4 v3, 0x0

    #@5d
    .line 3685
    :cond_5d
    if-ne v3, v9, :cond_72

    #@5f
    .line 3686
    const/4 v4, 0x1

    #@60
    .line 3691
    :goto_60
    sget v7, Lcom/android/server/wm/WindowManagerService;->mDsdpMode:I

    #@62
    if-eq v7, v4, :cond_2d

    #@64
    .line 3693
    sput v4, Lcom/android/server/wm/WindowManagerService;->mDsdpMode:I

    #@66
    .line 3694
    sget v7, Lcom/android/server/wm/WindowManagerService;->mDsdpMode:I

    #@68
    if-ne v7, v9, :cond_74

    #@6a
    .line 3695
    const-string v7, "sys.lge.dsdp.mode"

    #@6c
    const-string v8, "stop"

    #@6e
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    goto :goto_2d

    #@72
    .line 3688
    :cond_72
    const/4 v4, 0x2

    #@73
    goto :goto_60

    #@74
    .line 3697
    :cond_74
    const-string v7, "sys.lge.dsdp.mode"

    #@76
    const-string v8, "start"

    #@78
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    goto :goto_2d
.end method
