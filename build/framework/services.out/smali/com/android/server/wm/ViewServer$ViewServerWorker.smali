.class Lcom/android/server/wm/ViewServer$ViewServerWorker;
.super Ljava/lang/Object;
.source "ViewServer.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lcom/android/server/wm/WindowManagerService$WindowChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/ViewServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewServerWorker"
.end annotation


# instance fields
.field private mClient:Ljava/net/Socket;

.field private mNeedFocusedWindowUpdate:Z

.field private mNeedWindowListUpdate:Z

.field final synthetic this$0:Lcom/android/server/wm/ViewServer;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/ViewServer;Ljava/net/Socket;)V
    .registers 4
    .parameter
    .parameter "client"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 211
    iput-object p1, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 212
    iput-object p2, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@8
    .line 213
    iput-boolean v0, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedWindowListUpdate:Z

    #@a
    .line 214
    iput-boolean v0, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedFocusedWindowUpdate:Z

    #@c
    .line 215
    return-void
.end method

.method private windowManagerAutolistLoop()Z
    .registers 7

    #@0
    .prologue
    .line 292
    iget-object v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@2
    invoke-static {v4}, Lcom/android/server/wm/ViewServer;->access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v4, p0}, Lcom/android/server/wm/WindowManagerService;->addWindowChangeListener(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V

    #@9
    .line 293
    const/4 v2, 0x0

    #@a
    .line 295
    .local v2, out:Ljava/io/BufferedWriter;
    :try_start_a
    new-instance v3, Ljava/io/BufferedWriter;

    #@c
    new-instance v4, Ljava/io/OutputStreamWriter;

    #@e
    iget-object v5, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@10
    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    #@13
    move-result-object v5

    #@14
    invoke-direct {v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@17
    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1a
    .catchall {:try_start_a .. :try_end_1a} :catchall_91
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_1a} :catch_93

    #@1a
    .line 296
    .end local v2           #out:Ljava/io/BufferedWriter;
    .local v3, out:Ljava/io/BufferedWriter;
    :cond_1a
    :goto_1a
    :try_start_1a
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    #@1d
    move-result v4

    #@1e
    if-nez v4, :cond_7b

    #@20
    .line 297
    const/4 v1, 0x0

    #@21
    .line 298
    .local v1, needWindowListUpdate:Z
    const/4 v0, 0x0

    #@22
    .line 299
    .local v0, needFocusedWindowUpdate:Z
    monitor-enter p0
    :try_end_23
    .catchall {:try_start_1a .. :try_end_23} :catchall_6a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_23} :catch_32

    #@23
    .line 300
    :goto_23
    :try_start_23
    iget-boolean v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedWindowListUpdate:Z

    #@25
    if-nez v4, :cond_44

    #@27
    iget-boolean v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedFocusedWindowUpdate:Z

    #@29
    if-nez v4, :cond_44

    #@2b
    .line 301
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    #@2e
    goto :goto_23

    #@2f
    .line 311
    :catchall_2f
    move-exception v4

    #@30
    monitor-exit p0
    :try_end_31
    .catchall {:try_start_23 .. :try_end_31} :catchall_2f

    #@31
    :try_start_31
    throw v4
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_6a
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_32} :catch_32

    #@32
    .line 321
    .end local v0           #needFocusedWindowUpdate:Z
    .end local v1           #needWindowListUpdate:Z
    :catch_32
    move-exception v4

    #@33
    move-object v2, v3

    #@34
    .line 324
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :goto_34
    if-eqz v2, :cond_39

    #@36
    .line 326
    :try_start_36
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_39} :catch_8d

    #@39
    .line 331
    :cond_39
    :goto_39
    iget-object v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@3b
    invoke-static {v4}, Lcom/android/server/wm/ViewServer;->access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, p0}, Lcom/android/server/wm/WindowManagerService;->removeWindowChangeListener(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V

    #@42
    .line 333
    :goto_42
    const/4 v4, 0x1

    #@43
    return v4

    #@44
    .line 303
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v0       #needFocusedWindowUpdate:Z
    .restart local v1       #needWindowListUpdate:Z
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :cond_44
    :try_start_44
    iget-boolean v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedWindowListUpdate:Z

    #@46
    if-eqz v4, :cond_4c

    #@48
    .line 304
    const/4 v4, 0x0

    #@49
    iput-boolean v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedWindowListUpdate:Z

    #@4b
    .line 305
    const/4 v1, 0x1

    #@4c
    .line 307
    :cond_4c
    iget-boolean v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedFocusedWindowUpdate:Z

    #@4e
    if-eqz v4, :cond_54

    #@50
    .line 308
    const/4 v4, 0x0

    #@51
    iput-boolean v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedFocusedWindowUpdate:Z

    #@53
    .line 309
    const/4 v0, 0x1

    #@54
    .line 311
    :cond_54
    monitor-exit p0
    :try_end_55
    .catchall {:try_start_44 .. :try_end_55} :catchall_2f

    #@55
    .line 312
    if-eqz v1, :cond_5f

    #@57
    .line 313
    :try_start_57
    const-string v4, "LIST UPDATE\n"

    #@59
    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@5c
    .line 314
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V

    #@5f
    .line 316
    :cond_5f
    if-eqz v0, :cond_1a

    #@61
    .line 317
    const-string v4, "FOCUS UPDATE\n"

    #@63
    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@66
    .line 318
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_69
    .catchall {:try_start_57 .. :try_end_69} :catchall_6a
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_69} :catch_32

    #@69
    goto :goto_1a

    #@6a
    .line 324
    .end local v0           #needFocusedWindowUpdate:Z
    .end local v1           #needWindowListUpdate:Z
    :catchall_6a
    move-exception v4

    #@6b
    move-object v2, v3

    #@6c
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :goto_6c
    if-eqz v2, :cond_71

    #@6e
    .line 326
    :try_start_6e
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_8f

    #@71
    .line 331
    :cond_71
    :goto_71
    iget-object v5, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@73
    invoke-static {v5}, Lcom/android/server/wm/ViewServer;->access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v5, p0}, Lcom/android/server/wm/WindowManagerService;->removeWindowChangeListener(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V

    #@7a
    throw v4

    #@7b
    .line 324
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :cond_7b
    if-eqz v3, :cond_80

    #@7d
    .line 326
    :try_start_7d
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_80
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_80} :catch_8b

    #@80
    .line 331
    :cond_80
    :goto_80
    iget-object v4, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@82
    invoke-static {v4}, Lcom/android/server/wm/ViewServer;->access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4, p0}, Lcom/android/server/wm/WindowManagerService;->removeWindowChangeListener(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V

    #@89
    move-object v2, v3

    #@8a
    .line 332
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    goto :goto_42

    #@8b
    .line 327
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :catch_8b
    move-exception v4

    #@8c
    goto :goto_80

    #@8d
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :catch_8d
    move-exception v4

    #@8e
    goto :goto_39

    #@8f
    :catch_8f
    move-exception v5

    #@90
    goto :goto_71

    #@91
    .line 324
    :catchall_91
    move-exception v4

    #@92
    goto :goto_6c

    #@93
    .line 321
    :catch_93
    move-exception v4

    #@94
    goto :goto_34
.end method


# virtual methods
.method public focusChanged()V
    .registers 2

    #@0
    .prologue
    .line 285
    monitor-enter p0

    #@1
    .line 286
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedFocusedWindowUpdate:Z

    #@4
    .line 287
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@7
    .line 288
    monitor-exit p0

    #@8
    .line 289
    return-void

    #@9
    .line 288
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public run()V
    .registers 12

    #@0
    .prologue
    .line 219
    const/4 v2, 0x0

    #@1
    .line 221
    .local v2, in:Ljava/io/BufferedReader;
    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    #@3
    new-instance v8, Ljava/io/InputStreamReader;

    #@5
    iget-object v9, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@7
    invoke-virtual {v9}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    #@a
    move-result-object v9

    #@b
    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@e
    const/16 v9, 0x400

    #@10
    invoke-direct {v3, v8, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_ed
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_13} :catch_ca

    #@13
    .line 223
    .end local v2           #in:Ljava/io/BufferedReader;
    .local v3, in:Ljava/io/BufferedReader;
    :try_start_13
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@16
    move-result-object v6

    #@17
    .line 228
    .local v6, request:Ljava/lang/String;
    const/16 v8, 0x20

    #@19
    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(I)I

    #@1c
    move-result v4

    #@1d
    .line 229
    .local v4, index:I
    const/4 v8, -0x1

    #@1e
    if-ne v4, v8, :cond_5d

    #@20
    .line 230
    move-object v0, v6

    #@21
    .line 231
    .local v0, command:Ljava/lang/String;
    const-string v5, ""

    #@23
    .line 238
    .local v5, parameters:Ljava/lang/String;
    :goto_23
    const-string v8, "PROTOCOL"

    #@25
    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@28
    move-result v8

    #@29
    if-eqz v8, :cond_69

    #@2b
    .line 239
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@2d
    const-string v9, "4"

    #@2f
    invoke-static {v8, v9}, Lcom/android/server/wm/ViewServer;->access$000(Ljava/net/Socket;Ljava/lang/String;)Z

    #@32
    move-result v7

    #@33
    .line 253
    .local v7, result:Z
    :goto_33
    if-nez v7, :cond_4d

    #@35
    .line 254
    const-string v8, "ViewServer"

    #@37
    new-instance v9, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v10, "An error occurred with the command: "

    #@3e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v9

    #@42
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v9

    #@46
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v9

    #@4a
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4d
    .catchall {:try_start_13 .. :try_end_4d} :catchall_107
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_4d} :catch_10a

    #@4d
    .line 259
    :cond_4d
    if-eqz v3, :cond_52

    #@4f
    .line 261
    :try_start_4f
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_52
    .catch Ljava/io/IOException; {:try_start_4f .. :try_end_52} :catch_bf

    #@52
    .line 267
    :cond_52
    :goto_52
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@54
    if-eqz v8, :cond_10d

    #@56
    .line 269
    :try_start_56
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@58
    invoke-virtual {v8}, Ljava/net/Socket;->close()V
    :try_end_5b
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_5b} :catch_c4

    #@5b
    move-object v2, v3

    #@5c
    .line 275
    .end local v0           #command:Ljava/lang/String;
    .end local v3           #in:Ljava/io/BufferedReader;
    .end local v4           #index:I
    .end local v5           #parameters:Ljava/lang/String;
    .end local v6           #request:Ljava/lang/String;
    .end local v7           #result:Z
    .restart local v2       #in:Ljava/io/BufferedReader;
    :cond_5c
    :goto_5c
    return-void

    #@5d
    .line 233
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/BufferedReader;
    .restart local v4       #index:I
    .restart local v6       #request:Ljava/lang/String;
    :cond_5d
    const/4 v8, 0x0

    #@5e
    :try_start_5e
    invoke-virtual {v6, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    .line 234
    .restart local v0       #command:Ljava/lang/String;
    add-int/lit8 v8, v4, 0x1

    #@64
    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    .restart local v5       #parameters:Ljava/lang/String;
    goto :goto_23

    #@69
    .line 240
    :cond_69
    const-string v8, "SERVER"

    #@6b
    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@6e
    move-result v8

    #@6f
    if-eqz v8, :cond_7a

    #@71
    .line 241
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@73
    const-string v9, "4"

    #@75
    invoke-static {v8, v9}, Lcom/android/server/wm/ViewServer;->access$000(Ljava/net/Socket;Ljava/lang/String;)Z

    #@78
    move-result v7

    #@79
    .restart local v7       #result:Z
    goto :goto_33

    #@7a
    .line 242
    .end local v7           #result:Z
    :cond_7a
    const-string v8, "LIST"

    #@7c
    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7f
    move-result v8

    #@80
    if-eqz v8, :cond_8f

    #@82
    .line 243
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@84
    invoke-static {v8}, Lcom/android/server/wm/ViewServer;->access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;

    #@87
    move-result-object v8

    #@88
    iget-object v9, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@8a
    invoke-virtual {v8, v9}, Lcom/android/server/wm/WindowManagerService;->viewServerListWindows(Ljava/net/Socket;)Z

    #@8d
    move-result v7

    #@8e
    .restart local v7       #result:Z
    goto :goto_33

    #@8f
    .line 244
    .end local v7           #result:Z
    :cond_8f
    const-string v8, "GET_FOCUS"

    #@91
    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@94
    move-result v8

    #@95
    if-eqz v8, :cond_a4

    #@97
    .line 245
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@99
    invoke-static {v8}, Lcom/android/server/wm/ViewServer;->access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;

    #@9c
    move-result-object v8

    #@9d
    iget-object v9, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@9f
    invoke-virtual {v8, v9}, Lcom/android/server/wm/WindowManagerService;->viewServerGetFocusedWindow(Ljava/net/Socket;)Z

    #@a2
    move-result v7

    #@a3
    .restart local v7       #result:Z
    goto :goto_33

    #@a4
    .line 246
    .end local v7           #result:Z
    :cond_a4
    const-string v8, "AUTOLIST"

    #@a6
    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a9
    move-result v8

    #@aa
    if-eqz v8, :cond_b1

    #@ac
    .line 247
    invoke-direct {p0}, Lcom/android/server/wm/ViewServer$ViewServerWorker;->windowManagerAutolistLoop()Z

    #@af
    move-result v7

    #@b0
    .restart local v7       #result:Z
    goto :goto_33

    #@b1
    .line 249
    .end local v7           #result:Z
    :cond_b1
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->this$0:Lcom/android/server/wm/ViewServer;

    #@b3
    invoke-static {v8}, Lcom/android/server/wm/ViewServer;->access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;

    #@b6
    move-result-object v8

    #@b7
    iget-object v9, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@b9
    invoke-virtual {v8, v9, v0, v5}, Lcom/android/server/wm/WindowManagerService;->viewServerWindowCommand(Ljava/net/Socket;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_bc
    .catchall {:try_start_5e .. :try_end_bc} :catchall_107
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_bc} :catch_10a

    #@bc
    move-result v7

    #@bd
    .restart local v7       #result:Z
    goto/16 :goto_33

    #@bf
    .line 263
    :catch_bf
    move-exception v1

    #@c0
    .line 264
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@c3
    goto :goto_52

    #@c4
    .line 270
    .end local v1           #e:Ljava/io/IOException;
    :catch_c4
    move-exception v1

    #@c5
    .line 271
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@c8
    move-object v2, v3

    #@c9
    .line 272
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    goto :goto_5c

    #@ca
    .line 256
    .end local v0           #command:Ljava/lang/String;
    .end local v1           #e:Ljava/io/IOException;
    .end local v4           #index:I
    .end local v5           #parameters:Ljava/lang/String;
    .end local v6           #request:Ljava/lang/String;
    .end local v7           #result:Z
    :catch_ca
    move-exception v1

    #@cb
    .line 257
    .restart local v1       #e:Ljava/io/IOException;
    :goto_cb
    :try_start_cb
    const-string v8, "ViewServer"

    #@cd
    const-string v9, "Connection error: "

    #@cf
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d2
    .catchall {:try_start_cb .. :try_end_d2} :catchall_ed

    #@d2
    .line 259
    if-eqz v2, :cond_d7

    #@d4
    .line 261
    :try_start_d4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_d7
    .catch Ljava/io/IOException; {:try_start_d4 .. :try_end_d7} :catch_e8

    #@d7
    .line 267
    :cond_d7
    :goto_d7
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@d9
    if-eqz v8, :cond_5c

    #@db
    .line 269
    :try_start_db
    iget-object v8, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@dd
    invoke-virtual {v8}, Ljava/net/Socket;->close()V
    :try_end_e0
    .catch Ljava/io/IOException; {:try_start_db .. :try_end_e0} :catch_e2

    #@e0
    goto/16 :goto_5c

    #@e2
    .line 270
    :catch_e2
    move-exception v1

    #@e3
    .line 271
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@e6
    goto/16 :goto_5c

    #@e8
    .line 263
    :catch_e8
    move-exception v1

    #@e9
    .line 264
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@ec
    goto :goto_d7

    #@ed
    .line 259
    .end local v1           #e:Ljava/io/IOException;
    :catchall_ed
    move-exception v8

    #@ee
    :goto_ee
    if-eqz v2, :cond_f3

    #@f0
    .line 261
    :try_start_f0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_f3
    .catch Ljava/io/IOException; {:try_start_f0 .. :try_end_f3} :catch_fd

    #@f3
    .line 267
    :cond_f3
    :goto_f3
    iget-object v9, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@f5
    if-eqz v9, :cond_fc

    #@f7
    .line 269
    :try_start_f7
    iget-object v9, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mClient:Ljava/net/Socket;

    #@f9
    invoke-virtual {v9}, Ljava/net/Socket;->close()V
    :try_end_fc
    .catch Ljava/io/IOException; {:try_start_f7 .. :try_end_fc} :catch_102

    #@fc
    .line 272
    :cond_fc
    :goto_fc
    throw v8

    #@fd
    .line 263
    :catch_fd
    move-exception v1

    #@fe
    .line 264
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@101
    goto :goto_f3

    #@102
    .line 270
    .end local v1           #e:Ljava/io/IOException;
    :catch_102
    move-exception v1

    #@103
    .line 271
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@106
    goto :goto_fc

    #@107
    .line 259
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/BufferedReader;
    :catchall_107
    move-exception v8

    #@108
    move-object v2, v3

    #@109
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    goto :goto_ee

    #@10a
    .line 256
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/BufferedReader;
    :catch_10a
    move-exception v1

    #@10b
    move-object v2, v3

    #@10c
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    goto :goto_cb

    #@10d
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v0       #command:Ljava/lang/String;
    .restart local v3       #in:Ljava/io/BufferedReader;
    .restart local v4       #index:I
    .restart local v5       #parameters:Ljava/lang/String;
    .restart local v6       #request:Ljava/lang/String;
    .restart local v7       #result:Z
    :cond_10d
    move-object v2, v3

    #@10e
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    goto/16 :goto_5c
.end method

.method public windowsChanged()V
    .registers 2

    #@0
    .prologue
    .line 278
    monitor-enter p0

    #@1
    .line 279
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/server/wm/ViewServer$ViewServerWorker;->mNeedWindowListUpdate:Z

    #@4
    .line 280
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@7
    .line 281
    monitor-exit p0

    #@8
    .line 282
    return-void

    #@9
    .line 281
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method
