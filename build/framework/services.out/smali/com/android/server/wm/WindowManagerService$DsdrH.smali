.class final Lcom/android/server/wm/WindowManagerService$DsdrH;
.super Landroid/os/Handler;
.source "WindowManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "DsdrH"
.end annotation


# static fields
.field public static final REPORT_DUAL_SCREEN_STATUS_CHANGE:I = 0x1

.field public static final STOP_EXTERNAL_APP:I = 0x3


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8697
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 8702
    iget v2, p1, Landroid/os/Message;->what:I

    #@5
    packed-switch v2, :pswitch_data_126

    #@8
    .line 8777
    :cond_8
    :goto_8
    :pswitch_8
    return-void

    #@9
    .line 8705
    :pswitch_9
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@b
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDsdrUtil:Lcom/android/server/wm/WindowManagerService$DsdrUtil;

    #@d
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService$DsdrUtil;->destroyExternalApp()V

    #@10
    goto :goto_8

    #@11
    .line 8710
    :pswitch_11
    const-string v2, "WindowManager"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "[DSDR][WMS.java]handleMessage() : REPORT_DUAL_SCREEN_STATUS_CHANGE, mDualScreenStatus == "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 8711
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@2d
    packed-switch v2, :pswitch_data_130

    #@30
    .line 8754
    :goto_30
    :pswitch_30
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@32
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@34
    if-eqz v2, :cond_f9

    #@36
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@38
    iget-boolean v2, v2, Lcom/android/server/wm/WindowManagerService;->mSystemBooted:Z

    #@3a
    if-eqz v2, :cond_f9

    #@3c
    .line 8756
    :try_start_3c
    new-instance v1, Landroid/content/Intent;

    #@3e
    const-string v2, "com.lge.intent.action.LG_DUAL_SCREEN_STATUS_CHANGED"

    #@40
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@43
    .line 8757
    .local v1, notifyIntent:Landroid/content/Intent;
    const-string v2, "Enable"

    #@45
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@47
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4a
    .line 8758
    const-string v2, "Status"

    #@4c
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@4e
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@51
    .line 8759
    const-string v2, "WindowManager"

    #@53
    new-instance v3, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v4, "[DSDR][WMS.java]handleMessage() : sending broadcast - mDualScreenActivated == "

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    const-string v4, ", mDualScreenStatus == "

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v3

    #@74
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 8760
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@79
    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@7b
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@7e
    .line 8763
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@80
    const/4 v3, 0x4

    #@81
    if-eq v2, v3, :cond_87

    #@83
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@85
    if-ne v2, v7, :cond_8

    #@87
    .line 8764
    :cond_87
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@89
    invoke-static {v2}, Lcom/android/server/wm/WindowManagerService;->access$400(Lcom/android/server/wm/WindowManagerService;)Landroid/os/Handler;

    #@8c
    move-result-object v2

    #@8d
    const/4 v3, 0x1

    #@8e
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_91} :catch_93

    #@91
    goto/16 :goto_8

    #@93
    .line 8767
    .end local v1           #notifyIntent:Landroid/content/Intent;
    :catch_93
    move-exception v0

    #@94
    .line 8768
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@97
    goto/16 :goto_8

    #@99
    .line 8713
    .end local v0           #e:Ljava/lang/Exception;
    :pswitch_99
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@9b
    iput-boolean v6, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenConnected:Z

    #@9d
    goto :goto_30

    #@9e
    .line 8720
    :pswitch_9e
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@a0
    iput-boolean v6, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenActivated:Z

    #@a2
    goto :goto_30

    #@a3
    .line 8724
    :pswitch_a3
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@a5
    iput-boolean v6, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenVisible:Z

    #@a7
    goto :goto_30

    #@a8
    .line 8728
    :pswitch_a8
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@aa
    iput-boolean v5, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenVisible:Z

    #@ac
    .line 8729
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@ae
    const/4 v3, 0x6

    #@af
    invoke-static {v2, v5, v3}, Lcom/android/server/wm/WindowManagerService;->access$300(Lcom/android/server/wm/WindowManagerService;ZI)I

    #@b2
    .line 8730
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@b4
    const/4 v3, 0x7

    #@b5
    invoke-static {v2, v5, v3}, Lcom/android/server/wm/WindowManagerService;->access$300(Lcom/android/server/wm/WindowManagerService;ZI)I

    #@b8
    goto/16 :goto_30

    #@ba
    .line 8737
    :pswitch_ba
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@bc
    iput-boolean v5, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenActivated:Z

    #@be
    .line 8738
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@c0
    iput-boolean v5, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenVisible:Z

    #@c2
    goto/16 :goto_30

    #@c4
    .line 8742
    :pswitch_c4
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@c6
    iput-boolean v5, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenConnected:Z

    #@c8
    .line 8743
    const-string v2, "WindowManager"

    #@ca
    new-instance v3, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v4, "[DSDR][WMS.java]handleMessage() : DUAL_SCREEN_STATUS_DISCONNECTED - mDualScreenVisible = "

    #@d1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v3

    #@d5
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@d7
    iget-boolean v4, v4, Lcom/android/server/wm/WindowManagerService;->mDualScreenVisible:Z

    #@d9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v3

    #@dd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v3

    #@e1
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e4
    .line 8744
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@e6
    iget-boolean v2, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenVisible:Z

    #@e8
    if-eqz v2, :cond_ef

    #@ea
    .line 8745
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@ec
    invoke-static {v2, v6, v7}, Lcom/android/server/wm/WindowManagerService;->access$300(Lcom/android/server/wm/WindowManagerService;ZI)I

    #@ef
    .line 8747
    :cond_ef
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@f1
    iput-boolean v5, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenActivated:Z

    #@f3
    .line 8748
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@f5
    iput-boolean v5, v2, Lcom/android/server/wm/WindowManagerService;->mDualScreenVisible:Z

    #@f7
    goto/16 :goto_30

    #@f9
    .line 8771
    :cond_f9
    const-string v2, "WindowManager"

    #@fb
    new-instance v3, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v4, "[DSDR][WMS.java]handleMessage() : context = "

    #@102
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v3

    #@106
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@108
    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    #@10a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v3

    #@10e
    const-string v4, ", mSystemBooted =  "

    #@110
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v3

    #@114
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerService$DsdrH;->this$0:Lcom/android/server/wm/WindowManagerService;

    #@116
    iget-boolean v4, v4, Lcom/android/server/wm/WindowManagerService;->mSystemBooted:Z

    #@118
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v3

    #@11c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v3

    #@120
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@123
    goto/16 :goto_8

    #@125
    .line 8702
    nop

    #@126
    :pswitch_data_126
    .packed-switch 0x1
        :pswitch_11
        :pswitch_8
        :pswitch_9
    .end packed-switch

    #@130
    .line 8711
    :pswitch_data_130
    .packed-switch 0x1
        :pswitch_99
        :pswitch_30
        :pswitch_9e
        :pswitch_a3
        :pswitch_a8
        :pswitch_30
        :pswitch_ba
        :pswitch_c4
    .end packed-switch
.end method
