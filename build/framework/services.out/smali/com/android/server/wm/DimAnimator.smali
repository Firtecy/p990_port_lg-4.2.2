.class Lcom/android/server/wm/DimAnimator;
.super Ljava/lang/Object;
.source "DimAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/DimAnimator$Parameters;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "DimAnimator"


# instance fields
.field mDimCurrentAlpha:F

.field mDimDeltaPerMs:F

.field mDimShown:Z

.field mDimSurface:Landroid/view/Surface;

.field mDimTargetAlpha:F

.field mLastDimAnimTime:J

.field mLastDimHeight:I

.field mLastDimWidth:I


# direct methods
.method constructor <init>(Landroid/view/SurfaceSession;I)V
    .registers 11
    .parameter "session"
    .parameter "layerStack"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/server/wm/DimAnimator;->mDimShown:Z

    #@6
    .line 52
    :try_start_6
    new-instance v0, Landroid/view/Surface;

    #@8
    const-string v2, "DimAnimator"

    #@a
    const/16 v3, 0x10

    #@c
    const/16 v4, 0x10

    #@e
    const/4 v5, -0x1

    #@f
    const v6, 0x20004

    #@12
    move-object v1, p1

    #@13
    invoke-direct/range {v0 .. v6}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@16
    iput-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@18
    .line 59
    iget-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@1a
    invoke-virtual {v0, p2}, Landroid/view/Surface;->setLayerStack(I)V

    #@1d
    .line 60
    iget-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@1f
    const/4 v1, 0x0

    #@20
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setAlpha(F)V

    #@23
    .line 61
    iget-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@25
    invoke-virtual {v0}, Landroid/view/Surface;->show()V
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_28} :catch_29

    #@28
    .line 65
    :goto_28
    return-void

    #@29
    .line 62
    :catch_29
    move-exception v7

    #@2a
    .line 63
    .local v7, e:Ljava/lang/Exception;
    const-string v0, "WindowManager"

    #@2c
    const-string v1, "Exception creating Dim surface"

    #@2e
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31
    goto :goto_28
.end method


# virtual methods
.method public kill()V
    .registers 2

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 202
    iget-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@6
    invoke-virtual {v0}, Landroid/view/Surface;->destroy()V

    #@9
    .line 203
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@c
    .line 205
    :cond_c
    return-void
.end method

.method public printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 5
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 208
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    .line 209
    const-string v0, "mDimSurface="

    #@5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v0, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@d
    .line 210
    const-string v0, " "

    #@f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    iget v0, p0, Lcom/android/server/wm/DimAnimator;->mLastDimWidth:I

    #@14
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@17
    const-string v0, " x "

    #@19
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c
    .line 211
    iget v0, p0, Lcom/android/server/wm/DimAnimator;->mLastDimHeight:I

    #@1e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@21
    .line 212
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    .line 213
    const-string v0, "mDimShown="

    #@26
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    iget-boolean v0, p0, Lcom/android/server/wm/DimAnimator;->mDimShown:Z

    #@2b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@2e
    .line 214
    const-string v0, " current="

    #@30
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33
    iget v0, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(F)V

    #@38
    .line 215
    const-string v0, " target="

    #@3a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d
    iget v0, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@3f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(F)V

    #@42
    .line 216
    const-string v0, " delta="

    #@44
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@47
    iget v0, p0, Lcom/android/server/wm/DimAnimator;->mDimDeltaPerMs:F

    #@49
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(F)V

    #@4c
    .line 217
    const-string v0, " lastAnimTime="

    #@4e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@51
    iget-wide v0, p0, Lcom/android/server/wm/DimAnimator;->mLastDimAnimTime:J

    #@53
    invoke-virtual {p2, v0, v1}, Ljava/io/PrintWriter;->println(J)V

    #@56
    .line 218
    return-void
.end method

.method updateParameters(Landroid/content/res/Resources;Lcom/android/server/wm/DimAnimator$Parameters;J)V
    .registers 17
    .parameter "res"
    .parameter "params"
    .parameter "currentTime"

    #@0
    .prologue
    .line 72
    iget-object v8, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@2
    if-nez v8, :cond_c

    #@4
    .line 73
    const-string v8, "DimAnimator"

    #@6
    const-string v9, "updateParameters: no Surface"

    #@8
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 134
    :cond_b
    :goto_b
    return-void

    #@c
    .line 79
    :cond_c
    iget v8, p2, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWidth:I

    #@e
    int-to-double v8, v8

    #@f
    const-wide/high16 v10, 0x3ff8

    #@11
    mul-double/2addr v8, v10

    #@12
    double-to-int v3, v8

    #@13
    .line 80
    .local v3, dw:I
    iget v8, p2, Lcom/android/server/wm/DimAnimator$Parameters;->mDimHeight:I

    #@15
    int-to-double v8, v8

    #@16
    const-wide/high16 v10, 0x3ff8

    #@18
    mul-double/2addr v8, v10

    #@19
    double-to-int v0, v8

    #@1a
    .line 81
    .local v0, dh:I
    iget-object v7, p2, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@1c
    .line 82
    .local v7, winAnimator:Lcom/android/server/wm/WindowStateAnimator;
    iget v5, p2, Lcom/android/server/wm/DimAnimator$Parameters;->mDimTarget:F

    #@1e
    .line 83
    .local v5, target:F
    iget-boolean v8, p0, Lcom/android/server/wm/DimAnimator;->mDimShown:Z

    #@20
    if-nez v8, :cond_9a

    #@22
    .line 86
    const/4 v8, 0x1

    #@23
    iput-boolean v8, p0, Lcom/android/server/wm/DimAnimator;->mDimShown:Z

    #@25
    .line 88
    :try_start_25
    iput v3, p0, Lcom/android/server/wm/DimAnimator;->mLastDimWidth:I

    #@27
    .line 89
    iput v0, p0, Lcom/android/server/wm/DimAnimator;->mLastDimHeight:I

    #@29
    .line 91
    iget-object v8, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@2b
    mul-int/lit8 v9, v3, -0x1

    #@2d
    div-int/lit8 v9, v9, 0x6

    #@2f
    mul-int/lit8 v10, v0, -0x1

    #@31
    div-int/lit8 v10, v10, 0x6

    #@33
    invoke-virtual {v8, v9, v10}, Landroid/view/Surface;->setPosition(II)V

    #@36
    .line 92
    iget-object v8, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@38
    invoke-virtual {v8, v3, v0}, Landroid/view/Surface;->setSize(II)V

    #@3b
    .line 93
    iget-object v8, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@3d
    invoke-virtual {v8}, Landroid/view/Surface;->show()V
    :try_end_40
    .catch Ljava/lang/RuntimeException; {:try_start_25 .. :try_end_40} :catch_91

    #@40
    .line 105
    :cond_40
    :goto_40
    iget-object v8, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@42
    iget v9, v7, Lcom/android/server/wm/WindowStateAnimator;->mAnimLayer:I

    #@44
    add-int/lit8 v9, v9, -0x1

    #@46
    invoke-virtual {v8, v9}, Landroid/view/Surface;->setLayer(I)V

    #@49
    .line 109
    iget v8, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@4b
    cmpl-float v8, v8, v5

    #@4d
    if-eqz v8, :cond_b

    #@4f
    .line 112
    iput-wide p3, p0, Lcom/android/server/wm/DimAnimator;->mLastDimAnimTime:J

    #@51
    .line 113
    iget-boolean v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mAnimating:Z

    #@53
    if-eqz v8, :cond_b9

    #@55
    iget-object v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@57
    if-eqz v8, :cond_b9

    #@59
    iget-object v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mAnimation:Landroid/view/animation/Animation;

    #@5b
    invoke-virtual {v8}, Landroid/view/animation/Animation;->computeDurationHint()J

    #@5e
    move-result-wide v1

    #@5f
    .line 116
    .local v1, duration:J
    :goto_5f
    iget v8, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@61
    cmpl-float v8, v5, v8

    #@63
    if-lez v8, :cond_7c

    #@65
    .line 117
    new-instance v6, Landroid/util/TypedValue;

    #@67
    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    #@6a
    .line 118
    .local v6, tv:Landroid/util/TypedValue;
    const/high16 v8, 0x112

    #@6c
    const/4 v9, 0x1

    #@6d
    invoke-virtual {p1, v8, v6, v9}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@70
    .line 120
    iget v8, v6, Landroid/util/TypedValue;->type:I

    #@72
    const/4 v9, 0x6

    #@73
    if-ne v8, v9, :cond_bc

    #@75
    .line 121
    long-to-float v8, v1

    #@76
    long-to-float v9, v1

    #@77
    invoke-virtual {v6, v8, v9}, Landroid/util/TypedValue;->getFraction(FF)F

    #@7a
    move-result v8

    #@7b
    float-to-long v1, v8

    #@7c
    .line 127
    .end local v6           #tv:Landroid/util/TypedValue;
    :cond_7c
    :goto_7c
    const-wide/16 v8, 0x1

    #@7e
    cmp-long v8, v1, v8

    #@80
    if-gez v8, :cond_84

    #@82
    .line 129
    const-wide/16 v1, 0x1

    #@84
    .line 131
    :cond_84
    iput v5, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@86
    .line 132
    iget v8, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@88
    iget v9, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@8a
    sub-float/2addr v8, v9

    #@8b
    long-to-float v9, v1

    #@8c
    div-float/2addr v8, v9

    #@8d
    iput v8, p0, Lcom/android/server/wm/DimAnimator;->mDimDeltaPerMs:F

    #@8f
    goto/16 :goto_b

    #@91
    .line 94
    .end local v1           #duration:J
    :catch_91
    move-exception v4

    #@92
    .line 95
    .local v4, e:Ljava/lang/RuntimeException;
    const-string v8, "WindowManager"

    #@94
    const-string v9, "Failure showing dim surface"

    #@96
    invoke-static {v8, v9, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@99
    goto :goto_40

    #@9a
    .line 97
    .end local v4           #e:Ljava/lang/RuntimeException;
    :cond_9a
    iget v8, p0, Lcom/android/server/wm/DimAnimator;->mLastDimWidth:I

    #@9c
    if-ne v8, v3, :cond_a2

    #@9e
    iget v8, p0, Lcom/android/server/wm/DimAnimator;->mLastDimHeight:I

    #@a0
    if-eq v8, v0, :cond_40

    #@a2
    .line 98
    :cond_a2
    iput v3, p0, Lcom/android/server/wm/DimAnimator;->mLastDimWidth:I

    #@a4
    .line 99
    iput v0, p0, Lcom/android/server/wm/DimAnimator;->mLastDimHeight:I

    #@a6
    .line 100
    iget-object v8, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@a8
    invoke-virtual {v8, v3, v0}, Landroid/view/Surface;->setSize(II)V

    #@ab
    .line 102
    iget-object v8, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@ad
    mul-int/lit8 v9, v3, -0x1

    #@af
    div-int/lit8 v9, v9, 0x6

    #@b1
    mul-int/lit8 v10, v0, -0x1

    #@b3
    div-int/lit8 v10, v10, 0x6

    #@b5
    invoke-virtual {v8, v9, v10}, Landroid/view/Surface;->setPosition(II)V

    #@b8
    goto :goto_40

    #@b9
    .line 113
    :cond_b9
    const-wide/16 v1, 0xc8

    #@bb
    goto :goto_5f

    #@bc
    .line 122
    .restart local v1       #duration:J
    .restart local v6       #tv:Landroid/util/TypedValue;
    :cond_bc
    iget v8, v6, Landroid/util/TypedValue;->type:I

    #@be
    const/16 v9, 0x10

    #@c0
    if-lt v8, v9, :cond_7c

    #@c2
    iget v8, v6, Landroid/util/TypedValue;->type:I

    #@c4
    const/16 v9, 0x1f

    #@c6
    if-gt v8, v9, :cond_7c

    #@c8
    .line 124
    iget v8, v6, Landroid/util/TypedValue;->data:I

    #@ca
    int-to-long v1, v8

    #@cb
    goto :goto_7c
.end method

.method updateSurface(ZJZ)Z
    .registers 15
    .parameter "dimming"
    .parameter "currentTime"
    .parameter "displayFrozen"

    #@0
    .prologue
    const-wide/16 v8, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v7, 0x0

    #@4
    .line 141
    iget-object v3, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@6
    if-nez v3, :cond_11

    #@8
    .line 142
    const-string v3, "DimAnimator"

    #@a
    const-string v4, "updateSurface: no Surface"

    #@c
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    move v0, v2

    #@10
    .line 197
    :cond_10
    :goto_10
    return v0

    #@11
    .line 146
    :cond_11
    if-nez p1, :cond_25

    #@13
    .line 147
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@15
    cmpl-float v3, v3, v7

    #@17
    if-eqz v3, :cond_25

    #@19
    .line 148
    iput-wide p2, p0, Lcom/android/server/wm/DimAnimator;->mLastDimAnimTime:J

    #@1b
    .line 149
    iput v7, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@1d
    .line 150
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@1f
    neg-float v3, v3

    #@20
    const/high16 v4, 0x4348

    #@22
    div-float/2addr v3, v4

    #@23
    iput v3, p0, Lcom/android/server/wm/DimAnimator;->mDimDeltaPerMs:F

    #@25
    .line 154
    :cond_25
    iget-wide v3, p0, Lcom/android/server/wm/DimAnimator;->mLastDimAnimTime:J

    #@27
    cmp-long v3, v3, v8

    #@29
    if-eqz v3, :cond_4a

    #@2b
    const/4 v0, 0x1

    #@2c
    .line 155
    .local v0, animating:Z
    :goto_2c
    if-eqz v0, :cond_10

    #@2e
    .line 156
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@30
    iget v4, p0, Lcom/android/server/wm/DimAnimator;->mDimDeltaPerMs:F

    #@32
    iget-wide v5, p0, Lcom/android/server/wm/DimAnimator;->mLastDimAnimTime:J

    #@34
    sub-long v5, p2, v5

    #@36
    long-to-float v5, v5

    #@37
    mul-float/2addr v4, v5

    #@38
    add-float/2addr v3, v4

    #@39
    iput v3, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@3b
    .line 158
    if-eqz p4, :cond_4c

    #@3d
    .line 160
    const/4 v0, 0x0

    #@3e
    .line 174
    :cond_3e
    :goto_3e
    if-eqz v0, :cond_6e

    #@40
    .line 177
    iput-wide p2, p0, Lcom/android/server/wm/DimAnimator;->mLastDimAnimTime:J

    #@42
    .line 178
    iget-object v2, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@44
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@46
    invoke-virtual {v2, v3}, Landroid/view/Surface;->setAlpha(F)V

    #@49
    goto :goto_10

    #@4a
    .end local v0           #animating:Z
    :cond_4a
    move v0, v2

    #@4b
    .line 154
    goto :goto_2c

    #@4c
    .line 161
    .restart local v0       #animating:Z
    :cond_4c
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimDeltaPerMs:F

    #@4e
    cmpl-float v3, v3, v7

    #@50
    if-lez v3, :cond_5c

    #@52
    .line 162
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@54
    iget v4, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@56
    cmpl-float v3, v3, v4

    #@58
    if-lez v3, :cond_3e

    #@5a
    .line 163
    const/4 v0, 0x0

    #@5b
    goto :goto_3e

    #@5c
    .line 165
    :cond_5c
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimDeltaPerMs:F

    #@5e
    cmpg-float v3, v3, v7

    #@60
    if-gez v3, :cond_6c

    #@62
    .line 166
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@64
    iget v4, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@66
    cmpg-float v3, v3, v4

    #@68
    if-gez v3, :cond_3e

    #@6a
    .line 167
    const/4 v0, 0x0

    #@6b
    goto :goto_3e

    #@6c
    .line 170
    :cond_6c
    const/4 v0, 0x0

    #@6d
    goto :goto_3e

    #@6e
    .line 180
    :cond_6e
    iget v3, p0, Lcom/android/server/wm/DimAnimator;->mDimTargetAlpha:F

    #@70
    iput v3, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@72
    .line 181
    iput-wide v8, p0, Lcom/android/server/wm/DimAnimator;->mLastDimAnimTime:J

    #@74
    .line 184
    iget-object v3, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@76
    iget v4, p0, Lcom/android/server/wm/DimAnimator;->mDimCurrentAlpha:F

    #@78
    invoke-virtual {v3, v4}, Landroid/view/Surface;->setAlpha(F)V

    #@7b
    .line 185
    if-nez p1, :cond_10

    #@7d
    .line 189
    :try_start_7d
    iget-object v3, p0, Lcom/android/server/wm/DimAnimator;->mDimSurface:Landroid/view/Surface;

    #@7f
    invoke-virtual {v3}, Landroid/view/Surface;->hide()V
    :try_end_82
    .catch Ljava/lang/RuntimeException; {:try_start_7d .. :try_end_82} :catch_85

    #@82
    .line 193
    :goto_82
    iput-boolean v2, p0, Lcom/android/server/wm/DimAnimator;->mDimShown:Z

    #@84
    goto :goto_10

    #@85
    .line 190
    :catch_85
    move-exception v1

    #@86
    .line 191
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v3, "WindowManager"

    #@88
    const-string v4, "Illegal argument exception hiding dim surface"

    #@8a
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    goto :goto_82
.end method
