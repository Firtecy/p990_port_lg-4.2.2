.class Lcom/android/server/wm/DisplayContent;
.super Ljava/lang/Object;
.source "DisplayContent.java"


# instance fields
.field final isDefaultDisplay:Z

.field layoutNeeded:Z

.field mBaseDisplayDensity:I

.field mBaseDisplayHeight:I

.field mBaseDisplayWidth:I

.field private final mDisplay:Landroid/view/Display;

.field mDisplayContentChangeListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/view/IDisplayContentChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayId:I

.field private final mDisplayInfo:Landroid/view/DisplayInfo;

.field final mDisplaySizeLock:Ljava/lang/Object;

.field mInitialDisplayDensity:I

.field mInitialDisplayHeight:I

.field mInitialDisplayWidth:I

.field mMagnificationSpec:Lcom/android/server/wm/MagnificationSpec;

.field private mWindows:Lcom/android/server/wm/WindowList;

.field pendingLayoutChanges:I


# direct methods
.method constructor <init>(Landroid/view/Display;)V
    .registers 4
    .parameter "display"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 80
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 44
    new-instance v1, Lcom/android/server/wm/WindowList;

    #@6
    invoke-direct {v1}, Lcom/android/server/wm/WindowList;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/wm/DisplayContent;->mWindows:Lcom/android/server/wm/WindowList;

    #@b
    .line 62
    new-instance v1, Ljava/lang/Object;

    #@d
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@10
    iput-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplaySizeLock:Ljava/lang/Object;

    #@12
    .line 63
    iput v0, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayWidth:I

    #@14
    .line 64
    iput v0, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayHeight:I

    #@16
    .line 65
    iput v0, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayDensity:I

    #@18
    .line 66
    iput v0, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayWidth:I

    #@1a
    .line 67
    iput v0, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayHeight:I

    #@1c
    .line 68
    iput v0, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayDensity:I

    #@1e
    .line 69
    new-instance v1, Landroid/view/DisplayInfo;

    #@20
    invoke-direct {v1}, Landroid/view/DisplayInfo;-><init>()V

    #@23
    iput-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@25
    .line 81
    iput-object p1, p0, Lcom/android/server/wm/DisplayContent;->mDisplay:Landroid/view/Display;

    #@27
    .line 82
    invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I

    #@2a
    move-result v1

    #@2b
    iput v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayId:I

    #@2d
    .line 83
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@2f
    invoke-virtual {p1, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    #@32
    .line 84
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayId:I

    #@34
    if-nez v1, :cond_37

    #@36
    const/4 v0, 0x1

    #@37
    :cond_37
    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContent;->isDefaultDisplay:Z

    #@39
    .line 85
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 108
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v1, "Display: mDisplayId="

    #@5
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayId:I

    #@a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(I)V

    #@d
    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "  "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 110
    .local v0, subPrefix:Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23
    const-string v1, "init="

    #@25
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayWidth:I

    #@2a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@2d
    const-string v1, "x"

    #@2f
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32
    .line 111
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayHeight:I

    #@34
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@37
    const-string v1, " "

    #@39
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3c
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayDensity:I

    #@3e
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@41
    .line 112
    const-string v1, "dpi"

    #@43
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@46
    .line 113
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayWidth:I

    #@48
    iget v2, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayWidth:I

    #@4a
    if-ne v1, v2, :cond_58

    #@4c
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayHeight:I

    #@4e
    iget v2, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayHeight:I

    #@50
    if-ne v1, v2, :cond_58

    #@52
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayDensity:I

    #@54
    iget v2, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayDensity:I

    #@56
    if-eq v1, v2, :cond_7b

    #@58
    .line 116
    :cond_58
    const-string v1, " base="

    #@5a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d
    .line 117
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayWidth:I

    #@5f
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@62
    const-string v1, "x"

    #@64
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@67
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayHeight:I

    #@69
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@6c
    .line 118
    const-string v1, " "

    #@6e
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@71
    iget v1, p0, Lcom/android/server/wm/DisplayContent;->mBaseDisplayDensity:I

    #@73
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@76
    const-string v1, "dpi"

    #@78
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7b
    .line 120
    :cond_7b
    const-string v1, " cur="

    #@7d
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@80
    .line 121
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@82
    iget v1, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    #@84
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@87
    .line 122
    const-string v1, "x"

    #@89
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8c
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@8e
    iget v1, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    #@90
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@93
    .line 123
    const-string v1, " app="

    #@95
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@98
    .line 124
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@9a
    iget v1, v1, Landroid/view/DisplayInfo;->appWidth:I

    #@9c
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@9f
    .line 125
    const-string v1, "x"

    #@a1
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a4
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@a6
    iget v1, v1, Landroid/view/DisplayInfo;->appHeight:I

    #@a8
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@ab
    .line 126
    const-string v1, " rng="

    #@ad
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b0
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@b2
    iget v1, v1, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@b4
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@b7
    .line 127
    const-string v1, "x"

    #@b9
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bc
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@be
    iget v1, v1, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@c0
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@c3
    .line 128
    const-string v1, "-"

    #@c5
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c8
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@ca
    iget v1, v1, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@cc
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@cf
    .line 129
    const-string v1, "x"

    #@d1
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d4
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@d6
    iget v1, v1, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@d8
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(I)V

    #@db
    .line 130
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@de
    const-string v1, "layoutNeeded="

    #@e0
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e3
    iget-boolean v1, p0, Lcom/android/server/wm/DisplayContent;->layoutNeeded:Z

    #@e5
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@e8
    .line 131
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mMagnificationSpec:Lcom/android/server/wm/MagnificationSpec;

    #@ea
    if-eqz v1, :cond_f6

    #@ec
    .line 132
    const-string v1, " mMagnificationSpec="

    #@ee
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f1
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mMagnificationSpec:Lcom/android/server/wm/MagnificationSpec;

    #@f3
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@f6
    .line 134
    :cond_f6
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@f9
    .line 135
    return-void
.end method

.method getDisplay()Landroid/view/Display;
    .registers 2

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/server/wm/DisplayContent;->mDisplay:Landroid/view/Display;

    #@2
    return-object v0
.end method

.method getDisplayId()I
    .registers 2

    #@0
    .prologue
    .line 88
    iget v0, p0, Lcom/android/server/wm/DisplayContent;->mDisplayId:I

    #@2
    return v0
.end method

.method getDisplayInfo()Landroid/view/DisplayInfo;
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@2
    return-object v0
.end method

.method getWindowList()Lcom/android/server/wm/WindowList;
    .registers 2

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/server/wm/DisplayContent;->mWindows:Lcom/android/server/wm/WindowList;

    #@2
    return-object v0
.end method

.method public updateDisplayInfo()V
    .registers 3

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/server/wm/DisplayContent;->mDisplay:Landroid/view/Display;

    #@2
    iget-object v1, p0, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@4
    invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    #@7
    .line 105
    return-void
.end method
