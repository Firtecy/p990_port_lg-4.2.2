.class Lcom/android/server/wm/ViewServer;
.super Ljava/lang/Object;
.source "ViewServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/ViewServer$ViewServerWorker;
    }
.end annotation


# static fields
.field private static final COMMAND_PROTOCOL_VERSION:Ljava/lang/String; = "PROTOCOL"

.field private static final COMMAND_SERVER_VERSION:Ljava/lang/String; = "SERVER"

.field private static final COMMAND_WINDOW_MANAGER_AUTOLIST:Ljava/lang/String; = "AUTOLIST"

.field private static final COMMAND_WINDOW_MANAGER_GET_FOCUS:Ljava/lang/String; = "GET_FOCUS"

.field private static final COMMAND_WINDOW_MANAGER_LIST:Ljava/lang/String; = "LIST"

.field private static final LOG_TAG:Ljava/lang/String; = "ViewServer"

.field private static final VALUE_PROTOCOL_VERSION:Ljava/lang/String; = "4"

.field private static final VALUE_SERVER_VERSION:Ljava/lang/String; = "4"

.field public static final VIEW_SERVER_DEFAULT_PORT:I = 0x134b

.field private static final VIEW_SERVER_MAX_CONNECTIONS:I = 0xa


# instance fields
.field private final mPort:I

.field private mServer:Ljava/net/ServerSocket;

.field private mThread:Ljava/lang/Thread;

.field private mThreadPool:Ljava/util/concurrent/ExecutorService;

.field private final mWindowManager:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerService;I)V
    .registers 3
    .parameter "windowManager"
    .parameter "port"

    #@0
    .prologue
    .line 84
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 85
    iput-object p1, p0, Lcom/android/server/wm/ViewServer;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@5
    .line 86
    iput p2, p0, Lcom/android/server/wm/ViewServer;->mPort:I

    #@7
    .line 87
    return-void
.end method

.method static synthetic access$000(Ljava/net/Socket;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/android/server/wm/ViewServer;->writeValue(Ljava/net/Socket;Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/server/wm/ViewServer;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2
    return-object v0
.end method

.method private static writeValue(Ljava/net/Socket;Ljava/lang/String;)Z
    .registers 9
    .parameter "client"
    .parameter "value"

    #@0
    .prologue
    .line 184
    const/4 v2, 0x0

    #@1
    .line 186
    .local v2, out:Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {p0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    #@4
    move-result-object v0

    #@5
    .line 187
    .local v0, clientStream:Ljava/io/OutputStream;
    new-instance v3, Ljava/io/BufferedWriter;

    #@7
    new-instance v5, Ljava/io/OutputStreamWriter;

    #@9
    invoke-direct {v5, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@c
    const/16 v6, 0x2000

    #@e
    invoke-direct {v3, v5, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_33
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_11} :catch_28

    #@11
    .line 188
    .end local v2           #out:Ljava/io/BufferedWriter;
    .local v3, out:Ljava/io/BufferedWriter;
    :try_start_11
    invoke-virtual {v3, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@14
    .line 189
    const-string v5, "\n"

    #@16
    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@19
    .line 190
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1c
    .catchall {:try_start_11 .. :try_end_1c} :catchall_3c
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_1c} :catch_3f

    #@1c
    .line 191
    const/4 v4, 0x1

    #@1d
    .line 195
    .local v4, result:Z
    if-eqz v3, :cond_42

    #@1f
    .line 197
    :try_start_1f
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_22} :catch_24

    #@22
    move-object v2, v3

    #@23
    .line 203
    .end local v0           #clientStream:Ljava/io/OutputStream;
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :cond_23
    :goto_23
    return v4

    #@24
    .line 198
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v0       #clientStream:Ljava/io/OutputStream;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :catch_24
    move-exception v1

    #@25
    .line 199
    .local v1, e:Ljava/io/IOException;
    const/4 v4, 0x0

    #@26
    move-object v2, v3

    #@27
    .line 200
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    goto :goto_23

    #@28
    .line 192
    .end local v0           #clientStream:Ljava/io/OutputStream;
    .end local v1           #e:Ljava/io/IOException;
    .end local v4           #result:Z
    :catch_28
    move-exception v1

    #@29
    .line 193
    .local v1, e:Ljava/lang/Exception;
    :goto_29
    const/4 v4, 0x0

    #@2a
    .line 195
    .restart local v4       #result:Z
    if-eqz v2, :cond_23

    #@2c
    .line 197
    :try_start_2c
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_2f} :catch_30

    #@2f
    goto :goto_23

    #@30
    .line 198
    :catch_30
    move-exception v1

    #@31
    .line 199
    .local v1, e:Ljava/io/IOException;
    const/4 v4, 0x0

    #@32
    .line 200
    goto :goto_23

    #@33
    .line 195
    .end local v1           #e:Ljava/io/IOException;
    .end local v4           #result:Z
    :catchall_33
    move-exception v5

    #@34
    :goto_34
    if-eqz v2, :cond_39

    #@36
    .line 197
    :try_start_36
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_39} :catch_3a

    #@39
    .line 200
    :cond_39
    :goto_39
    throw v5

    #@3a
    .line 198
    :catch_3a
    move-exception v1

    #@3b
    .line 199
    .restart local v1       #e:Ljava/io/IOException;
    goto :goto_39

    #@3c
    .line 195
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v0       #clientStream:Ljava/io/OutputStream;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :catchall_3c
    move-exception v5

    #@3d
    move-object v2, v3

    #@3e
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    goto :goto_34

    #@3f
    .line 192
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :catch_3f
    move-exception v1

    #@40
    move-object v2, v3

    #@41
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    goto :goto_29

    #@42
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    .restart local v4       #result:Z
    :cond_42
    move-object v2, v3

    #@43
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    goto :goto_23
.end method


# virtual methods
.method isRunning()Z
    .registers 2

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@6
    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 163
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v2

    #@4
    iget-object v3, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@6
    if-ne v2, v3, :cond_2f

    #@8
    .line 166
    :try_start_8
    iget-object v2, p0, Lcom/android/server/wm/ViewServer;->mServer:Ljava/net/ServerSocket;

    #@a
    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    #@d
    move-result-object v0

    #@e
    .line 167
    .local v0, client:Ljava/net/Socket;
    iget-object v2, p0, Lcom/android/server/wm/ViewServer;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    #@10
    if-eqz v2, :cond_26

    #@12
    .line 168
    iget-object v2, p0, Lcom/android/server/wm/ViewServer;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    #@14
    new-instance v3, Lcom/android/server/wm/ViewServer$ViewServerWorker;

    #@16
    invoke-direct {v3, p0, v0}, Lcom/android/server/wm/ViewServer$ViewServerWorker;-><init>(Lcom/android/server/wm/ViewServer;Ljava/net/Socket;)V

    #@19
    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_1c} :catch_1d

    #@1c
    goto :goto_0

    #@1d
    .line 176
    .end local v0           #client:Ljava/net/Socket;
    :catch_1d
    move-exception v1

    #@1e
    .line 177
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "ViewServer"

    #@20
    const-string v3, "Connection error: "

    #@22
    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    goto :goto_0

    #@26
    .line 171
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v0       #client:Ljava/net/Socket;
    :cond_26
    :try_start_26
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_29} :catch_2a
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_29} :catch_1d

    #@29
    goto :goto_0

    #@2a
    .line 172
    :catch_2a
    move-exception v1

    #@2b
    .line 173
    .local v1, e:Ljava/io/IOException;
    :try_start_2b
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2e} :catch_1d

    #@2e
    goto :goto_0

    #@2f
    .line 180
    .end local v0           #client:Ljava/net/Socket;
    .end local v1           #e:Ljava/io/IOException;
    :cond_2f
    return-void
.end method

.method start()Z
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v3, 0xa

    #@2
    .line 100
    iget-object v0, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 101
    const/4 v0, 0x0

    #@7
    .line 109
    :goto_7
    return v0

    #@8
    .line 104
    :cond_8
    new-instance v0, Ljava/net/ServerSocket;

    #@a
    iget v1, p0, Lcom/android/server/wm/ViewServer;->mPort:I

    #@c
    invoke-static {}, Ljava/net/InetAddress;->getLocalHost()Ljava/net/InetAddress;

    #@f
    move-result-object v2

    #@10
    invoke-direct {v0, v1, v3, v2}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    #@13
    iput-object v0, p0, Lcom/android/server/wm/ViewServer;->mServer:Ljava/net/ServerSocket;

    #@15
    .line 105
    new-instance v0, Ljava/lang/Thread;

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "Remote View Server [port="

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget v2, p0, Lcom/android/server/wm/ViewServer;->mPort:I

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, "]"

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@35
    iput-object v0, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@37
    .line 106
    invoke-static {v3}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    #@3a
    move-result-object v0

    #@3b
    iput-object v0, p0, Lcom/android/server/wm/ViewServer;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    #@3d
    .line 107
    iget-object v0, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@3f
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@42
    .line 109
    const/4 v0, 0x1

    #@43
    goto :goto_7
.end method

.method stop()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 123
    iget-object v1, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@3
    if-eqz v1, :cond_32

    #@5
    .line 125
    iget-object v1, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@7
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    #@a
    .line 126
    iget-object v1, p0, Lcom/android/server/wm/ViewServer;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    #@c
    if-eqz v1, :cond_13

    #@e
    .line 128
    :try_start_e
    iget-object v1, p0, Lcom/android/server/wm/ViewServer;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    #@10
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;
    :try_end_13
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_13} :catch_21

    #@13
    .line 133
    :cond_13
    :goto_13
    iput-object v3, p0, Lcom/android/server/wm/ViewServer;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    #@15
    .line 134
    iput-object v3, p0, Lcom/android/server/wm/ViewServer;->mThread:Ljava/lang/Thread;

    #@17
    .line 136
    :try_start_17
    iget-object v1, p0, Lcom/android/server/wm/ViewServer;->mServer:Ljava/net/ServerSocket;

    #@19
    invoke-virtual {v1}, Ljava/net/ServerSocket;->close()V

    #@1c
    .line 137
    const/4 v1, 0x0

    #@1d
    iput-object v1, p0, Lcom/android/server/wm/ViewServer;->mServer:Ljava/net/ServerSocket;
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_1f} :catch_2a

    #@1f
    .line 138
    const/4 v1, 0x1

    #@20
    .line 143
    :goto_20
    return v1

    #@21
    .line 129
    :catch_21
    move-exception v0

    #@22
    .line 130
    .local v0, e:Ljava/lang/SecurityException;
    const-string v1, "ViewServer"

    #@24
    const-string v2, "Could not stop all view server threads"

    #@26
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_13

    #@2a
    .line 139
    .end local v0           #e:Ljava/lang/SecurityException;
    :catch_2a
    move-exception v0

    #@2b
    .line 140
    .local v0, e:Ljava/io/IOException;
    const-string v1, "ViewServer"

    #@2d
    const-string v2, "Could not close the view server"

    #@2f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 143
    .end local v0           #e:Ljava/io/IOException;
    :cond_32
    const/4 v1, 0x0

    #@33
    goto :goto_20
.end method
