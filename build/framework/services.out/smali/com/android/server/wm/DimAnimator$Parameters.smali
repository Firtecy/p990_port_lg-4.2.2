.class Lcom/android/server/wm/DimAnimator$Parameters;
.super Ljava/lang/Object;
.source "DimAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/DimAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Parameters"
.end annotation


# instance fields
.field final mDimHeight:I

.field final mDimTarget:F

.field final mDimWidth:I

.field final mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;


# direct methods
.method constructor <init>(Lcom/android/server/wm/DimAnimator$Parameters;)V
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 234
    iget-object v0, p1, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@5
    iput-object v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@7
    .line 235
    iget v0, p1, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWidth:I

    #@9
    iput v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWidth:I

    #@b
    .line 236
    iget v0, p1, Lcom/android/server/wm/DimAnimator$Parameters;->mDimHeight:I

    #@d
    iput v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimHeight:I

    #@f
    .line 237
    iget v0, p1, Lcom/android/server/wm/DimAnimator$Parameters;->mDimTarget:F

    #@11
    iput v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimTarget:F

    #@13
    .line 238
    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/WindowStateAnimator;IIF)V
    .registers 5
    .parameter "dimWinAnimator"
    .parameter "dimWidth"
    .parameter "dimHeight"
    .parameter "dimTarget"

    #@0
    .prologue
    .line 226
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 227
    iput-object p1, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@5
    .line 228
    iput p2, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWidth:I

    #@7
    .line 229
    iput p3, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimHeight:I

    #@9
    .line 230
    iput p4, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimTarget:F

    #@b
    .line 231
    return-void
.end method


# virtual methods
.method public printTo(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 241
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    .line 242
    const-string v0, "mDimWinAnimator="

    #@5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    #@a
    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    #@c
    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    #@e
    invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@15
    .line 243
    const-string v0, " "

    #@17
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a
    iget v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimWidth:I

    #@1c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1f
    const-string v0, " x "

    #@21
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    .line 244
    iget v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimHeight:I

    #@26
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@29
    .line 245
    const-string v0, " mDimTarget="

    #@2b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    iget v0, p0, Lcom/android/server/wm/DimAnimator$Parameters;->mDimTarget:F

    #@30
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(F)V

    #@33
    .line 246
    return-void
.end method
