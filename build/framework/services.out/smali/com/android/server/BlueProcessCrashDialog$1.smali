.class Lcom/android/server/BlueProcessCrashDialog$1;
.super Landroid/os/Handler;
.source "BlueProcessCrashDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BlueProcessCrashDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BlueProcessCrashDialog;


# direct methods
.method constructor <init>(Lcom/android/server/BlueProcessCrashDialog;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 116
    iput-object p1, p0, Lcom/android/server/BlueProcessCrashDialog$1;->this$0:Lcom/android/server/BlueProcessCrashDialog;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 118
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v2, :sswitch_data_2e

    #@5
    .line 138
    :goto_5
    return-void

    #@6
    .line 122
    :sswitch_6
    iget-object v2, p0, Lcom/android/server/BlueProcessCrashDialog$1;->this$0:Lcom/android/server/BlueProcessCrashDialog;

    #@8
    invoke-virtual {v2}, Lcom/android/server/BlueProcessCrashDialog;->dismiss()V

    #@b
    goto :goto_5

    #@c
    .line 128
    :sswitch_c
    :try_start_c
    new-instance v1, Ljava/lang/ProcessBuilder;

    #@e
    const/4 v2, 0x0

    #@f
    new-array v2, v2, [Ljava/lang/String;

    #@11
    invoke-direct {v1, v2}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    #@14
    .line 129
    .local v1, exec:Ljava/lang/ProcessBuilder;
    const/4 v2, 0x2

    #@15
    new-array v2, v2, [Ljava/lang/String;

    #@17
    const/4 v3, 0x0

    #@18
    const-string v4, "/system/bin/blue_error_report"

    #@1a
    aput-object v4, v2, v3

    #@1c
    const/4 v3, 0x1

    #@1d
    const-string v4, "-p"

    #@1f
    aput-object v4, v2, v3

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/ProcessBuilder;->command([Ljava/lang/String;)Ljava/lang/ProcessBuilder;

    #@24
    .line 130
    invoke-virtual {v1}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_27} :catch_28

    #@27
    goto :goto_5

    #@28
    .line 132
    .end local v1           #exec:Ljava/lang/ProcessBuilder;
    :catch_28
    move-exception v0

    #@29
    .line 133
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@2c
    goto :goto_5

    #@2d
    .line 118
    nop

    #@2e
    :sswitch_data_2e
    .sparse-switch
        0x0 -> :sswitch_6
        0x5 -> :sswitch_c
    .end sparse-switch
.end method
