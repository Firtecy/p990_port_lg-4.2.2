.class public abstract Lcom/android/server/IntentResolver;
.super Ljava/lang/Object;
.source "IntentResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/IntentResolver$ValidationFailure;,
        Lcom/android/server/IntentResolver$IteratorWrapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Landroid/content/IntentFilter;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "IntentResolver"

.field private static final VALIDATE:Z

.field private static final localLOGV:Z

.field private static final mResolvePrioritySorter:Ljava/util/Comparator;


# instance fields
.field private final mActionToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;"
        }
    .end annotation
.end field

.field private final mBaseTypeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;"
        }
    .end annotation
.end field

.field private final mFilters:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TF;>;"
        }
    .end annotation
.end field

.field private final mOldResolver:Lcom/android/server/IntentResolverOld;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/server/IntentResolverOld",
            "<TF;TR;>;"
        }
    .end annotation
.end field

.field private final mSchemeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;"
        }
    .end annotation
.end field

.field private final mTypeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;"
        }
    .end annotation
.end field

.field private final mTypedActionToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;"
        }
    .end annotation
.end field

.field private final mWildTypeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 608
    new-instance v0, Lcom/android/server/IntentResolver$1;

    #@2
    invoke-direct {v0}, Lcom/android/server/IntentResolver$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/server/IntentResolver;->mResolvePrioritySorter:Ljava/util/Comparator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 45
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 712
    new-instance v0, Lcom/android/server/IntentResolver$2;

    #@5
    invoke-direct {v0, p0}, Lcom/android/server/IntentResolver$2;-><init>(Lcom/android/server/IntentResolver;)V

    #@8
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mOldResolver:Lcom/android/server/IntentResolverOld;

    #@a
    .line 733
    new-instance v0, Ljava/util/HashSet;

    #@c
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mFilters:Ljava/util/HashSet;

    #@11
    .line 739
    new-instance v0, Ljava/util/HashMap;

    #@13
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mTypeToFilter:Ljava/util/HashMap;

    #@18
    .line 746
    new-instance v0, Ljava/util/HashMap;

    #@1a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1d
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@1f
    .line 755
    new-instance v0, Ljava/util/HashMap;

    #@21
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@24
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@26
    .line 760
    new-instance v0, Ljava/util/HashMap;

    #@28
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2b
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mSchemeToFilter:Ljava/util/HashMap;

    #@2d
    .line 766
    new-instance v0, Ljava/util/HashMap;

    #@2f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@32
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mActionToFilter:Ljava/util/HashMap;

    #@34
    .line 771
    new-instance v0, Ljava/util/HashMap;

    #@36
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@39
    iput-object v0, p0, Lcom/android/server/IntentResolver;->mTypedActionToFilter:Ljava/util/HashMap;

    #@3b
    return-void
.end method

.method private final addFilter(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/IntentFilter;)V
    .registers 10
    .parameter
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;",
            "Ljava/lang/String;",
            "TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;[TF;>;"
    .local p3, filter:Landroid/content/IntentFilter;,"TF;"
    const/4 v5, 0x0

    #@1
    .line 383
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4
    move-result-object v1

    #@5
    check-cast v1, [Landroid/content/IntentFilter;

    #@7
    .line 384
    .local v1, array:[Landroid/content/IntentFilter;,"[TF;"
    if-nez v1, :cond_14

    #@9
    .line 385
    const/4 v4, 0x2

    #@a
    invoke-virtual {p0, v4}, Lcom/android/server/IntentResolver;->newArray(I)[Landroid/content/IntentFilter;

    #@d
    move-result-object v1

    #@e
    .line 386
    invoke-virtual {p1, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 387
    aput-object p3, v1, v5

    #@13
    .line 403
    :goto_13
    return-void

    #@14
    .line 389
    :cond_14
    array-length v0, v1

    #@15
    .line 390
    .local v0, N:I
    move v2, v0

    #@16
    .line 391
    .local v2, i:I
    :goto_16
    if-lez v2, :cond_21

    #@18
    add-int/lit8 v4, v2, -0x1

    #@1a
    aget-object v4, v1, v4

    #@1c
    if-nez v4, :cond_21

    #@1e
    .line 392
    add-int/lit8 v2, v2, -0x1

    #@20
    goto :goto_16

    #@21
    .line 394
    :cond_21
    if-ge v2, v0, :cond_26

    #@23
    .line 395
    aput-object p3, v1, v2

    #@25
    goto :goto_13

    #@26
    .line 397
    :cond_26
    mul-int/lit8 v4, v0, 0x3

    #@28
    div-int/lit8 v4, v4, 0x2

    #@2a
    invoke-virtual {p0, v4}, Lcom/android/server/IntentResolver;->newArray(I)[Landroid/content/IntentFilter;

    #@2d
    move-result-object v3

    #@2e
    .line 398
    .local v3, newa:[Landroid/content/IntentFilter;,"[TF;"
    invoke-static {v1, v5, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@31
    .line 399
    aput-object p3, v3, v0

    #@33
    .line 400
    invoke-virtual {p1, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    goto :goto_13
.end method

.method private buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;[Landroid/content/IntentFilter;Ljava/util/List;I)V
    .registers 27
    .parameter "intent"
    .parameter
    .parameter "debug"
    .parameter "defaultOnly"
    .parameter "resolvedType"
    .parameter "scheme"
    .parameter
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Landroid/util/FastImmutableArraySet",
            "<",
            "Ljava/lang/String;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[TF;",
            "Ljava/util/List",
            "<TR;>;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 537
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p2, categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .local p7, src:[Landroid/content/IntentFilter;,"[TF;"
    .local p8, dest:Ljava/util/List;,"Ljava/util/List<TR;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 538
    .local v3, action:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@7
    move-result-object v6

    #@8
    .line 539
    .local v6, data:Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    #@b
    move-result-object v15

    #@c
    .line 541
    .local v15, packageName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->isExcludingStopped()Z

    #@f
    move-result v10

    #@10
    .line 543
    .local v10, excludingStopped:Z
    if-eqz p7, :cond_4f

    #@12
    move-object/from16 v0, p7

    #@14
    array-length v9, v0

    #@15
    .line 544
    .local v9, N:I
    :goto_15
    const/4 v11, 0x0

    #@16
    .line 547
    .local v11, hasNonDefaults:Z
    const/4 v12, 0x0

    #@17
    .local v12, i:I
    :goto_17
    if-ge v12, v9, :cond_10d

    #@19
    aget-object v2, p7, v12

    #@1b
    .local v2, filter:Landroid/content/IntentFilter;,"TF;"
    if-eqz v2, :cond_10d

    #@1d
    .line 549
    if-eqz p3, :cond_37

    #@1f
    const-string v4, "IntentResolver"

    #@21
    new-instance v5, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v7, "Matching against filter "

    #@28
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 551
    :cond_37
    if-eqz v10, :cond_51

    #@39
    move-object/from16 v0, p0

    #@3b
    move/from16 v1, p9

    #@3d
    invoke-virtual {v0, v2, v1}, Lcom/android/server/IntentResolver;->isFilterStopped(Landroid/content/IntentFilter;I)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_51

    #@43
    .line 552
    if-eqz p3, :cond_4c

    #@45
    .line 553
    const-string v4, "IntentResolver"

    #@47
    const-string v5, "  Filter\'s target is stopped; skipping"

    #@49
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 547
    :cond_4c
    :goto_4c
    add-int/lit8 v12, v12, 0x1

    #@4e
    goto :goto_17

    #@4f
    .line 543
    .end local v2           #filter:Landroid/content/IntentFilter;,"TF;"
    .end local v9           #N:I
    .end local v11           #hasNonDefaults:Z
    .end local v12           #i:I
    :cond_4f
    const/4 v9, 0x0

    #@50
    goto :goto_15

    #@51
    .line 559
    .restart local v2       #filter:Landroid/content/IntentFilter;,"TF;"
    .restart local v9       #N:I
    .restart local v11       #hasNonDefaults:Z
    .restart local v12       #i:I
    :cond_51
    if-eqz v15, :cond_80

    #@53
    move-object/from16 v0, p0

    #@55
    invoke-virtual {v0, v2}, Lcom/android/server/IntentResolver;->packageForFilter(Landroid/content/IntentFilter;)Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v4

    #@5d
    if-nez v4, :cond_80

    #@5f
    .line 560
    if-eqz p3, :cond_4c

    #@61
    .line 561
    const-string v4, "IntentResolver"

    #@63
    new-instance v5, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v7, "  Filter is not from package "

    #@6a
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v5

    #@72
    const-string v7, "; skipping"

    #@74
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v5

    #@7c
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_4c

    #@80
    .line 567
    :cond_80
    move-object/from16 v0, p0

    #@82
    move-object/from16 v1, p8

    #@84
    invoke-virtual {v0, v2, v1}, Lcom/android/server/IntentResolver;->allowFilterResult(Landroid/content/IntentFilter;Ljava/util/List;)Z

    #@87
    move-result v4

    #@88
    if-nez v4, :cond_94

    #@8a
    .line 568
    if-eqz p3, :cond_4c

    #@8c
    .line 569
    const-string v4, "IntentResolver"

    #@8e
    const-string v5, "  Filter\'s target already added"

    #@90
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    goto :goto_4c

    #@94
    .line 574
    :cond_94
    const-string v8, "IntentResolver"

    #@96
    move-object/from16 v4, p5

    #@98
    move-object/from16 v5, p6

    #@9a
    move-object/from16 v7, p2

    #@9c
    invoke-virtual/range {v2 .. v8}, Landroid/content/IntentFilter;->match(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/lang/String;)I

    #@9f
    move-result v13

    #@a0
    .line 575
    .local v13, match:I
    if-ltz v13, :cond_de

    #@a2
    .line 576
    if-eqz p3, :cond_c0

    #@a4
    const-string v4, "IntentResolver"

    #@a6
    new-instance v5, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v7, "  Filter matched!  match=0x"

    #@ad
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v5

    #@b1
    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@b4
    move-result-object v7

    #@b5
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v5

    #@b9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v5

    #@bd
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    .line 578
    :cond_c0
    if-eqz p4, :cond_ca

    #@c2
    const-string v4, "android.intent.category.DEFAULT"

    #@c4
    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    #@c7
    move-result v4

    #@c8
    if-eqz v4, :cond_db

    #@ca
    .line 579
    :cond_ca
    move-object/from16 v0, p0

    #@cc
    move/from16 v1, p9

    #@ce
    invoke-virtual {v0, v2, v13, v1}, Lcom/android/server/IntentResolver;->newResult(Landroid/content/IntentFilter;II)Ljava/lang/Object;

    #@d1
    move-result-object v14

    #@d2
    .line 580
    .local v14, oneResult:Ljava/lang/Object;,"TR;"
    if-eqz v14, :cond_4c

    #@d4
    .line 581
    move-object/from16 v0, p8

    #@d6
    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d9
    goto/16 :goto_4c

    #@db
    .line 584
    .end local v14           #oneResult:Ljava/lang/Object;,"TR;"
    :cond_db
    const/4 v11, 0x1

    #@dc
    goto/16 :goto_4c

    #@de
    .line 587
    :cond_de
    if-eqz p3, :cond_4c

    #@e0
    .line 589
    packed-switch v13, :pswitch_data_11e

    #@e3
    .line 594
    const-string v16, "unknown reason"

    #@e5
    .line 596
    .local v16, reason:Ljava/lang/String;
    :goto_e5
    const-string v4, "IntentResolver"

    #@e7
    new-instance v5, Ljava/lang/StringBuilder;

    #@e9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ec
    const-string v7, "  Filter did not match: "

    #@ee
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v5

    #@f2
    move-object/from16 v0, v16

    #@f4
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v5

    #@f8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v5

    #@fc
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    goto/16 :goto_4c

    #@101
    .line 590
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_101
    const-string v16, "action"

    #@103
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_e5

    #@104
    .line 591
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_104
    const-string v16, "category"

    #@106
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_e5

    #@107
    .line 592
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_107
    const-string v16, "data"

    #@109
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_e5

    #@10a
    .line 593
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_10a
    const-string v16, "type"

    #@10c
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_e5

    #@10d
    .line 601
    .end local v2           #filter:Landroid/content/IntentFilter;,"TF;"
    .end local v13           #match:I
    .end local v16           #reason:Ljava/lang/String;
    :cond_10d
    invoke-interface/range {p8 .. p8}, Ljava/util/List;->size()I

    #@110
    move-result v4

    #@111
    if-nez v4, :cond_11c

    #@113
    if-eqz v11, :cond_11c

    #@115
    .line 602
    const-string v4, "IntentResolver"

    #@117
    const-string v5, "resolveIntent failed: found match, but none with Intent.CATEGORY_DEFAULT"

    #@119
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 604
    :cond_11c
    return-void

    #@11d
    .line 589
    nop

    #@11e
    :pswitch_data_11e
    .packed-switch -0x4
        :pswitch_104
        :pswitch_101
        :pswitch_107
        :pswitch_10a
    .end packed-switch
.end method

.method private compareMaps(Landroid/content/IntentFilter;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .registers 27
    .parameter "src"
    .parameter "name"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/IntentFilter;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 630
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p3, cur:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;[TF;>;"
    .local p4, old:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    invoke-virtual/range {p3 .. p3}, Ljava/util/HashMap;->size()I

    #@3
    move-result v19

    #@4
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->size()I

    #@7
    move-result v20

    #@8
    move/from16 v0, v19

    #@a
    move/from16 v1, v20

    #@c
    if-eq v0, v1, :cond_11e

    #@e
    .line 631
    new-instance v14, Ljava/lang/StringBuilder;

    #@10
    const/16 v19, 0x80

    #@12
    move/from16 v0, v19

    #@14
    invoke-direct {v14, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@17
    .line 632
    .local v14, missing:Ljava/lang/StringBuilder;
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@1a
    move-result-object v19

    #@1b
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v12

    #@1f
    .local v12, i$:Ljava/util/Iterator;
    :cond_1f
    :goto_1f
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v19

    #@23
    if-eqz v19, :cond_54

    #@25
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v5

    #@29
    check-cast v5, Ljava/util/Map$Entry;

    #@2b
    .line 633
    .local v5, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2e
    move-result-object v19

    #@2f
    move-object/from16 v0, p3

    #@31
    move-object/from16 v1, v19

    #@33
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, [Landroid/content/IntentFilter;

    #@39
    .line 634
    .local v3, curArray:[Landroid/content/IntentFilter;,"[TF;"
    if-nez v3, :cond_1f

    #@3b
    .line 635
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    #@3e
    move-result v19

    #@3f
    if-lez v19, :cond_48

    #@41
    .line 636
    const/16 v19, 0x20

    #@43
    move/from16 v0, v19

    #@45
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@48
    .line 638
    :cond_48
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@4b
    move-result-object v19

    #@4c
    check-cast v19, Ljava/lang/String;

    #@4e
    move-object/from16 v0, v19

    #@50
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    goto :goto_1f

    #@54
    .line 641
    .end local v3           #curArray:[Landroid/content/IntentFilter;,"[TF;"
    .end local v5           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    :cond_54
    new-instance v7, Ljava/lang/StringBuilder;

    #@56
    const/16 v19, 0x80

    #@58
    move/from16 v0, v19

    #@5a
    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@5d
    .line 642
    .local v7, extra:Ljava/lang/StringBuilder;
    invoke-virtual/range {p3 .. p3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@60
    move-result-object v19

    #@61
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@64
    move-result-object v12

    #@65
    :cond_65
    :goto_65
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@68
    move-result v19

    #@69
    if-eqz v19, :cond_98

    #@6b
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6e
    move-result-object v6

    #@6f
    check-cast v6, Ljava/util/Map$Entry;

    #@71
    .line 643
    .local v6, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;[TF;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@74
    move-result-object v19

    #@75
    move-object/from16 v0, p4

    #@77
    move-object/from16 v1, v19

    #@79
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7c
    move-result-object v19

    #@7d
    if-nez v19, :cond_65

    #@7f
    .line 644
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    #@82
    move-result v19

    #@83
    if-lez v19, :cond_8c

    #@85
    .line 645
    const/16 v19, 0x20

    #@87
    move/from16 v0, v19

    #@89
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@8c
    .line 647
    :cond_8c
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@8f
    move-result-object v19

    #@90
    check-cast v19, Ljava/lang/String;

    #@92
    move-object/from16 v0, v19

    #@94
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    goto :goto_65

    #@98
    .line 650
    .end local v6           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;[TF;>;"
    :cond_98
    new-instance v18, Ljava/lang/StringBuilder;

    #@9a
    const/16 v19, 0x400

    #@9c
    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9f
    .line 651
    .local v18, srcStr:Ljava/lang/StringBuilder;
    new-instance v17, Landroid/util/StringBuilderPrinter;

    #@a1
    invoke-direct/range {v17 .. v18}, Landroid/util/StringBuilderPrinter;-><init>(Ljava/lang/StringBuilder;)V

    #@a4
    .line 652
    .local v17, printer:Landroid/util/StringBuilderPrinter;
    const-string v19, ""

    #@a6
    move-object/from16 v0, p1

    #@a8
    move-object/from16 v1, v17

    #@aa
    move-object/from16 v2, v19

    #@ac
    invoke-virtual {v0, v1, v2}, Landroid/content/IntentFilter;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@af
    .line 653
    new-instance v10, Lcom/android/server/IntentResolver$ValidationFailure;

    #@b1
    invoke-direct {v10}, Lcom/android/server/IntentResolver$ValidationFailure;-><init>()V

    #@b4
    .line 654
    .local v10, here:Lcom/android/server/IntentResolver$ValidationFailure;
    invoke-virtual {v10}, Lcom/android/server/IntentResolver$ValidationFailure;->fillInStackTrace()Ljava/lang/Throwable;

    #@b7
    .line 655
    const-string v19, "IntentResolver"

    #@b9
    new-instance v20, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v21, "New map "

    #@c0
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v20

    #@c4
    move-object/from16 v0, v20

    #@c6
    move-object/from16 v1, p2

    #@c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v20

    #@cc
    const-string v21, " size is "

    #@ce
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v20

    #@d2
    invoke-virtual/range {p3 .. p3}, Ljava/util/HashMap;->size()I

    #@d5
    move-result v21

    #@d6
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v20

    #@da
    const-string v21, "; old implementation is "

    #@dc
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v20

    #@e0
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->size()I

    #@e3
    move-result v21

    #@e4
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v20

    #@e8
    const-string v21, "; missing: "

    #@ea
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v20

    #@ee
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v21

    #@f2
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v20

    #@f6
    const-string v21, "; extra: "

    #@f8
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v20

    #@fc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ff
    move-result-object v21

    #@100
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v20

    #@104
    const-string v21, "; src: "

    #@106
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v20

    #@10a
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v21

    #@10e
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v20

    #@112
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v20

    #@116
    move-object/from16 v0, v19

    #@118
    move-object/from16 v1, v20

    #@11a
    invoke-static {v0, v1, v10}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11d
    .line 710
    .end local v7           #extra:Ljava/lang/StringBuilder;
    .end local v10           #here:Lcom/android/server/IntentResolver$ValidationFailure;
    .end local v14           #missing:Ljava/lang/StringBuilder;
    .end local v17           #printer:Landroid/util/StringBuilderPrinter;
    .end local v18           #srcStr:Ljava/lang/StringBuilder;
    :cond_11d
    :goto_11d
    return-void

    #@11e
    .line 662
    .end local v12           #i$:Ljava/util/Iterator;
    :cond_11e
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@121
    move-result-object v19

    #@122
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@125
    move-result-object v12

    #@126
    .restart local v12       #i$:Ljava/util/Iterator;
    :cond_126
    :goto_126
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@129
    move-result v19

    #@12a
    if-eqz v19, :cond_11d

    #@12c
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12f
    move-result-object v5

    #@130
    check-cast v5, Ljava/util/Map$Entry;

    #@132
    .line 663
    .restart local v5       #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@135
    move-result-object v19

    #@136
    move-object/from16 v0, p3

    #@138
    move-object/from16 v1, v19

    #@13a
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13d
    move-result-object v3

    #@13e
    check-cast v3, [Landroid/content/IntentFilter;

    #@140
    .line 664
    .restart local v3       #curArray:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v3, :cond_1a5

    #@142
    array-length v4, v3

    #@143
    .line 665
    .local v4, curLen:I
    :goto_143
    if-nez v4, :cond_1a7

    #@145
    .line 666
    new-instance v10, Lcom/android/server/IntentResolver$ValidationFailure;

    #@147
    invoke-direct {v10}, Lcom/android/server/IntentResolver$ValidationFailure;-><init>()V

    #@14a
    .line 667
    .restart local v10       #here:Lcom/android/server/IntentResolver$ValidationFailure;
    invoke-virtual {v10}, Lcom/android/server/IntentResolver$ValidationFailure;->fillInStackTrace()Ljava/lang/Throwable;

    #@14d
    .line 668
    const-string v20, "IntentResolver"

    #@14f
    new-instance v19, Ljava/lang/StringBuilder;

    #@151
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@154
    const-string v21, "New map "

    #@156
    move-object/from16 v0, v19

    #@158
    move-object/from16 v1, v21

    #@15a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v19

    #@15e
    move-object/from16 v0, v19

    #@160
    move-object/from16 v1, p2

    #@162
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v19

    #@166
    const-string v21, " doesn\'t contain expected key "

    #@168
    move-object/from16 v0, v19

    #@16a
    move-object/from16 v1, v21

    #@16c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v21

    #@170
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@173
    move-result-object v19

    #@174
    check-cast v19, Ljava/lang/String;

    #@176
    move-object/from16 v0, v21

    #@178
    move-object/from16 v1, v19

    #@17a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v19

    #@17e
    const-string v21, " (array="

    #@180
    move-object/from16 v0, v19

    #@182
    move-object/from16 v1, v21

    #@184
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v19

    #@188
    move-object/from16 v0, v19

    #@18a
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v19

    #@18e
    const-string v21, ")"

    #@190
    move-object/from16 v0, v19

    #@192
    move-object/from16 v1, v21

    #@194
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v19

    #@198
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19b
    move-result-object v19

    #@19c
    move-object/from16 v0, v20

    #@19e
    move-object/from16 v1, v19

    #@1a0
    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@1a3
    goto/16 :goto_11d

    #@1a5
    .line 664
    .end local v4           #curLen:I
    .end local v10           #here:Lcom/android/server/IntentResolver$ValidationFailure;
    :cond_1a5
    const/4 v4, 0x0

    #@1a6
    goto :goto_143

    #@1a7
    .line 672
    .restart local v4       #curLen:I
    :cond_1a7
    :goto_1a7
    if-lez v4, :cond_1b2

    #@1a9
    add-int/lit8 v19, v4, -0x1

    #@1ab
    aget-object v19, v3, v19

    #@1ad
    if-nez v19, :cond_1b2

    #@1af
    .line 673
    add-int/lit8 v4, v4, -0x1

    #@1b1
    goto :goto_1a7

    #@1b2
    .line 675
    :cond_1b2
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1b5
    move-result-object v15

    #@1b6
    check-cast v15, Ljava/util/ArrayList;

    #@1b8
    .line 676
    .local v15, oldArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@1bb
    move-result v16

    #@1bc
    .line 677
    .local v16, oldLen:I
    move/from16 v0, v16

    #@1be
    if-eq v4, v0, :cond_228

    #@1c0
    .line 678
    new-instance v10, Lcom/android/server/IntentResolver$ValidationFailure;

    #@1c2
    invoke-direct {v10}, Lcom/android/server/IntentResolver$ValidationFailure;-><init>()V

    #@1c5
    .line 679
    .restart local v10       #here:Lcom/android/server/IntentResolver$ValidationFailure;
    invoke-virtual {v10}, Lcom/android/server/IntentResolver$ValidationFailure;->fillInStackTrace()Ljava/lang/Throwable;

    #@1c8
    .line 680
    const-string v20, "IntentResolver"

    #@1ca
    new-instance v19, Ljava/lang/StringBuilder;

    #@1cc
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1cf
    const-string v21, "New map "

    #@1d1
    move-object/from16 v0, v19

    #@1d3
    move-object/from16 v1, v21

    #@1d5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v19

    #@1d9
    move-object/from16 v0, v19

    #@1db
    move-object/from16 v1, p2

    #@1dd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e0
    move-result-object v19

    #@1e1
    const-string v21, " entry "

    #@1e3
    move-object/from16 v0, v19

    #@1e5
    move-object/from16 v1, v21

    #@1e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ea
    move-result-object v21

    #@1eb
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1ee
    move-result-object v19

    #@1ef
    check-cast v19, Ljava/lang/String;

    #@1f1
    move-object/from16 v0, v21

    #@1f3
    move-object/from16 v1, v19

    #@1f5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v19

    #@1f9
    const-string v21, " size is "

    #@1fb
    move-object/from16 v0, v19

    #@1fd
    move-object/from16 v1, v21

    #@1ff
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v19

    #@203
    move-object/from16 v0, v19

    #@205
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@208
    move-result-object v19

    #@209
    const-string v21, "; old implementation is "

    #@20b
    move-object/from16 v0, v19

    #@20d
    move-object/from16 v1, v21

    #@20f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v19

    #@213
    move-object/from16 v0, v19

    #@215
    move/from16 v1, v16

    #@217
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v19

    #@21b
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21e
    move-result-object v19

    #@21f
    move-object/from16 v0, v20

    #@221
    move-object/from16 v1, v19

    #@223
    invoke-static {v0, v1, v10}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@226
    goto/16 :goto_11d

    #@228
    .line 684
    .end local v10           #here:Lcom/android/server/IntentResolver$ValidationFailure;
    :cond_228
    const/4 v11, 0x0

    #@229
    .local v11, i:I
    :goto_229
    move/from16 v0, v16

    #@22b
    if-ge v11, v0, :cond_29a

    #@22d
    .line 685
    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@230
    move-result-object v8

    #@231
    check-cast v8, Landroid/content/IntentFilter;

    #@233
    .line 686
    .local v8, f:Landroid/content/IntentFilter;,"TF;"
    const/4 v9, 0x0

    #@234
    .line 687
    .local v9, found:Z
    const/4 v13, 0x0

    #@235
    .local v13, j:I
    :goto_235
    if-ge v13, v4, :cond_23e

    #@237
    .line 688
    aget-object v19, v3, v13

    #@239
    move-object/from16 v0, v19

    #@23b
    if-ne v0, v8, :cond_297

    #@23d
    .line 689
    const/4 v9, 0x1

    #@23e
    .line 693
    :cond_23e
    if-nez v9, :cond_294

    #@240
    .line 694
    new-instance v10, Lcom/android/server/IntentResolver$ValidationFailure;

    #@242
    invoke-direct {v10}, Lcom/android/server/IntentResolver$ValidationFailure;-><init>()V

    #@245
    .line 695
    .restart local v10       #here:Lcom/android/server/IntentResolver$ValidationFailure;
    invoke-virtual {v10}, Lcom/android/server/IntentResolver$ValidationFailure;->fillInStackTrace()Ljava/lang/Throwable;

    #@248
    .line 696
    const-string v20, "IntentResolver"

    #@24a
    new-instance v19, Ljava/lang/StringBuilder;

    #@24c
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@24f
    const-string v21, "New map "

    #@251
    move-object/from16 v0, v19

    #@253
    move-object/from16 v1, v21

    #@255
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@258
    move-result-object v19

    #@259
    move-object/from16 v0, v19

    #@25b
    move-object/from16 v1, p2

    #@25d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@260
    move-result-object v19

    #@261
    const-string v21, " entry + "

    #@263
    move-object/from16 v0, v19

    #@265
    move-object/from16 v1, v21

    #@267
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v21

    #@26b
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@26e
    move-result-object v19

    #@26f
    check-cast v19, Ljava/lang/String;

    #@271
    move-object/from16 v0, v21

    #@273
    move-object/from16 v1, v19

    #@275
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@278
    move-result-object v19

    #@279
    const-string v21, " doesn\'t contain expected filter "

    #@27b
    move-object/from16 v0, v19

    #@27d
    move-object/from16 v1, v21

    #@27f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@282
    move-result-object v19

    #@283
    move-object/from16 v0, v19

    #@285
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v19

    #@289
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28c
    move-result-object v19

    #@28d
    move-object/from16 v0, v20

    #@28f
    move-object/from16 v1, v19

    #@291
    invoke-static {v0, v1, v10}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@294
    .line 684
    .end local v10           #here:Lcom/android/server/IntentResolver$ValidationFailure;
    :cond_294
    add-int/lit8 v11, v11, 0x1

    #@296
    goto :goto_229

    #@297
    .line 687
    :cond_297
    add-int/lit8 v13, v13, 0x1

    #@299
    goto :goto_235

    #@29a
    .line 700
    .end local v8           #f:Landroid/content/IntentFilter;,"TF;"
    .end local v9           #found:Z
    .end local v13           #j:I
    :cond_29a
    const/4 v11, 0x0

    #@29b
    :goto_29b
    if-ge v11, v4, :cond_126

    #@29d
    .line 701
    aget-object v19, v3, v11

    #@29f
    if-nez v19, :cond_307

    #@2a1
    .line 702
    new-instance v10, Lcom/android/server/IntentResolver$ValidationFailure;

    #@2a3
    invoke-direct {v10}, Lcom/android/server/IntentResolver$ValidationFailure;-><init>()V

    #@2a6
    .line 703
    .restart local v10       #here:Lcom/android/server/IntentResolver$ValidationFailure;
    invoke-virtual {v10}, Lcom/android/server/IntentResolver$ValidationFailure;->fillInStackTrace()Ljava/lang/Throwable;

    #@2a9
    .line 704
    const-string v20, "IntentResolver"

    #@2ab
    new-instance v19, Ljava/lang/StringBuilder;

    #@2ad
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@2b0
    const-string v21, "New map "

    #@2b2
    move-object/from16 v0, v19

    #@2b4
    move-object/from16 v1, v21

    #@2b6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b9
    move-result-object v19

    #@2ba
    move-object/from16 v0, v19

    #@2bc
    move-object/from16 v1, p2

    #@2be
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v19

    #@2c2
    const-string v21, " entry + "

    #@2c4
    move-object/from16 v0, v19

    #@2c6
    move-object/from16 v1, v21

    #@2c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cb
    move-result-object v21

    #@2cc
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2cf
    move-result-object v19

    #@2d0
    check-cast v19, Ljava/lang/String;

    #@2d2
    move-object/from16 v0, v21

    #@2d4
    move-object/from16 v1, v19

    #@2d6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d9
    move-result-object v19

    #@2da
    const-string v21, " has unexpected null at "

    #@2dc
    move-object/from16 v0, v19

    #@2de
    move-object/from16 v1, v21

    #@2e0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e3
    move-result-object v19

    #@2e4
    move-object/from16 v0, v19

    #@2e6
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e9
    move-result-object v19

    #@2ea
    const-string v21, "; array: "

    #@2ec
    move-object/from16 v0, v19

    #@2ee
    move-object/from16 v1, v21

    #@2f0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f3
    move-result-object v19

    #@2f4
    move-object/from16 v0, v19

    #@2f6
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v19

    #@2fa
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fd
    move-result-object v19

    #@2fe
    move-object/from16 v0, v20

    #@300
    move-object/from16 v1, v19

    #@302
    invoke-static {v0, v1, v10}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@305
    goto/16 :goto_126

    #@307
    .line 700
    .end local v10           #here:Lcom/android/server/IntentResolver$ValidationFailure;
    :cond_307
    add-int/lit8 v11, v11, 0x1

    #@309
    goto :goto_29b
.end method

.method private static getFastIntentCategories(Landroid/content/Intent;)Landroid/util/FastImmutableArraySet;
    .registers 4
    .parameter "intent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Landroid/util/FastImmutableArraySet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 527
    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@3
    move-result-object v0

    #@4
    .line 528
    .local v0, categories:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_8

    #@6
    .line 529
    const/4 v1, 0x0

    #@7
    .line 531
    :goto_7
    return-object v1

    #@8
    :cond_8
    new-instance v1, Landroid/util/FastImmutableArraySet;

    #@a
    invoke-interface {v0}, Ljava/util/Set;->size()I

    #@d
    move-result v2

    #@e
    new-array v2, v2, [Ljava/lang/String;

    #@10
    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    invoke-direct {v1, v2}, Landroid/util/FastImmutableArraySet;-><init>([Ljava/lang/Object;)V

    #@17
    goto :goto_7
.end method

.method private final register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .line 468
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    .local p2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .local p3, dest:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;[TF;>;"
    if-nez p2, :cond_4

    #@2
    .line 469
    const/4 v1, 0x0

    #@3
    .line 479
    :cond_3
    return v1

    #@4
    .line 472
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 473
    .local v1, num:I
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_3

    #@b
    .line 474
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Ljava/lang/String;

    #@11
    .line 475
    .local v0, name:Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    #@13
    .line 477
    invoke-direct {p0, p3, v0, p1}, Lcom/android/server/IntentResolver;->addFilter(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/IntentFilter;)V

    #@16
    goto :goto_5
.end method

.method private final register_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I
    .registers 11
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    const/4 v5, 0x0

    #@1
    .line 406
    invoke-virtual {p1}, Landroid/content/IntentFilter;->typesIterator()Ljava/util/Iterator;

    #@4
    move-result-object v1

    #@5
    .line 407
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-nez v1, :cond_9

    #@7
    move v3, v5

    #@8
    .line 433
    :cond_8
    return v3

    #@9
    .line 411
    :cond_9
    const/4 v3, 0x0

    #@a
    .line 412
    .local v3, num:I
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_8

    #@10
    .line 413
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Ljava/lang/String;

    #@16
    .line 414
    .local v2, name:Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    #@18
    .line 416
    move-object v0, v2

    #@19
    .line 417
    .local v0, baseName:Ljava/lang/String;
    const/16 v6, 0x2f

    #@1b
    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    #@1e
    move-result v4

    #@1f
    .line 418
    .local v4, slashpos:I
    if-lez v4, :cond_36

    #@21
    .line 419
    invoke-virtual {v2, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 424
    :goto_29
    iget-object v6, p0, Lcom/android/server/IntentResolver;->mTypeToFilter:Ljava/util/HashMap;

    #@2b
    invoke-direct {p0, v6, v2, p1}, Lcom/android/server/IntentResolver;->addFilter(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/IntentFilter;)V

    #@2e
    .line 426
    if-lez v4, :cond_4a

    #@30
    .line 427
    iget-object v6, p0, Lcom/android/server/IntentResolver;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@32
    invoke-direct {p0, v6, v0, p1}, Lcom/android/server/IntentResolver;->addFilter(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/IntentFilter;)V

    #@35
    goto :goto_a

    #@36
    .line 421
    :cond_36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, "/*"

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    goto :goto_29

    #@4a
    .line 429
    :cond_4a
    iget-object v6, p0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@4c
    invoke-direct {p0, v6, v0, p1}, Lcom/android/server/IntentResolver;->addFilter(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/IntentFilter;)V

    #@4f
    goto :goto_a
.end method

.method private final remove_all_objects(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Object;)V
    .registers 11
    .parameter
    .parameter "name"
    .parameter "object"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;[TF;>;"
    const/4 v6, 0x0

    #@1
    .line 500
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4
    move-result-object v1

    #@5
    check-cast v1, [Landroid/content/IntentFilter;

    #@7
    .line 501
    .local v1, array:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v1, :cond_32

    #@9
    .line 502
    array-length v5, v1

    #@a
    add-int/lit8 v0, v5, -0x1

    #@c
    .line 503
    .local v0, LAST:I
    :goto_c
    if-ltz v0, :cond_15

    #@e
    aget-object v5, v1, v0

    #@10
    if-nez v5, :cond_15

    #@12
    .line 504
    add-int/lit8 v0, v0, -0x1

    #@14
    goto :goto_c

    #@15
    .line 506
    :cond_15
    move v2, v0

    #@16
    .local v2, idx:I
    :goto_16
    if-ltz v2, :cond_2d

    #@18
    .line 507
    aget-object v5, v1, v2

    #@1a
    if-ne v5, p3, :cond_2a

    #@1c
    .line 508
    sub-int v4, v0, v2

    #@1e
    .line 509
    .local v4, remain:I
    if-lez v4, :cond_25

    #@20
    .line 510
    add-int/lit8 v5, v2, 0x1

    #@22
    invoke-static {v1, v5, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@25
    .line 512
    :cond_25
    const/4 v5, 0x0

    #@26
    aput-object v5, v1, v0

    #@28
    .line 513
    add-int/lit8 v0, v0, -0x1

    #@2a
    .line 506
    .end local v4           #remain:I
    :cond_2a
    add-int/lit8 v2, v2, -0x1

    #@2c
    goto :goto_16

    #@2d
    .line 516
    :cond_2d
    if-gez v0, :cond_33

    #@2f
    .line 517
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    .line 524
    .end local v0           #LAST:I
    .end local v2           #idx:I
    :cond_32
    :goto_32
    return-void

    #@33
    .line 518
    .restart local v0       #LAST:I
    .restart local v2       #idx:I
    :cond_33
    array-length v5, v1

    #@34
    div-int/lit8 v5, v5, 0x2

    #@36
    if-ge v0, v5, :cond_32

    #@38
    .line 519
    add-int/lit8 v5, v0, 0x2

    #@3a
    invoke-virtual {p0, v5}, Lcom/android/server/IntentResolver;->newArray(I)[Landroid/content/IntentFilter;

    #@3d
    move-result-object v3

    #@3e
    .line 520
    .local v3, newa:[Landroid/content/IntentFilter;,"[TF;"
    add-int/lit8 v5, v0, 0x1

    #@40
    invoke-static {v1, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@43
    .line 521
    invoke-virtual {p1, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    goto :goto_32
.end method

.method private final unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[TF;>;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .line 484
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    .local p2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .local p3, dest:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;[TF;>;"
    if-nez p2, :cond_4

    #@2
    .line 485
    const/4 v1, 0x0

    #@3
    .line 495
    :cond_3
    return v1

    #@4
    .line 488
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 489
    .local v1, num:I
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_3

    #@b
    .line 490
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Ljava/lang/String;

    #@11
    .line 491
    .local v0, name:Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    #@13
    .line 493
    invoke-direct {p0, p3, v0, p1}, Lcom/android/server/IntentResolver;->remove_all_objects(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Object;)V

    #@16
    goto :goto_5
.end method

.method private final unregister_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I
    .registers 11
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    const/4 v5, 0x0

    #@1
    .line 437
    invoke-virtual {p1}, Landroid/content/IntentFilter;->typesIterator()Ljava/util/Iterator;

    #@4
    move-result-object v1

    #@5
    .line 438
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-nez v1, :cond_9

    #@7
    move v3, v5

    #@8
    .line 463
    :cond_8
    return v3

    #@9
    .line 442
    :cond_9
    const/4 v3, 0x0

    #@a
    .line 443
    .local v3, num:I
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_8

    #@10
    .line 444
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Ljava/lang/String;

    #@16
    .line 445
    .local v2, name:Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    #@18
    .line 447
    move-object v0, v2

    #@19
    .line 448
    .local v0, baseName:Ljava/lang/String;
    const/16 v6, 0x2f

    #@1b
    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    #@1e
    move-result v4

    #@1f
    .line 449
    .local v4, slashpos:I
    if-lez v4, :cond_36

    #@21
    .line 450
    invoke-virtual {v2, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 455
    :goto_29
    iget-object v6, p0, Lcom/android/server/IntentResolver;->mTypeToFilter:Ljava/util/HashMap;

    #@2b
    invoke-direct {p0, v6, v2, p1}, Lcom/android/server/IntentResolver;->remove_all_objects(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Object;)V

    #@2e
    .line 457
    if-lez v4, :cond_4a

    #@30
    .line 458
    iget-object v6, p0, Lcom/android/server/IntentResolver;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@32
    invoke-direct {p0, v6, v0, p1}, Lcom/android/server/IntentResolver;->remove_all_objects(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Object;)V

    #@35
    goto :goto_a

    #@36
    .line 452
    :cond_36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, "/*"

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    goto :goto_29

    #@4a
    .line 460
    :cond_4a
    iget-object v6, p0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@4c
    invoke-direct {p0, v6, v0, p1}, Lcom/android/server/IntentResolver;->remove_all_objects(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Object;)V

    #@4f
    goto :goto_a
.end method

.method private verifyDataStructures(Landroid/content/IntentFilter;)V
    .registers 5
    .parameter "src"

    #@0
    .prologue
    .line 620
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    const-string v0, "mTypeToFilter"

    #@2
    iget-object v1, p0, Lcom/android/server/IntentResolver;->mTypeToFilter:Ljava/util/HashMap;

    #@4
    iget-object v2, p0, Lcom/android/server/IntentResolver;->mOldResolver:Lcom/android/server/IntentResolverOld;

    #@6
    iget-object v2, v2, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@8
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/IntentResolver;->compareMaps(Landroid/content/IntentFilter;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    #@b
    .line 621
    const-string v0, "mBaseTypeToFilter"

    #@d
    iget-object v1, p0, Lcom/android/server/IntentResolver;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@f
    iget-object v2, p0, Lcom/android/server/IntentResolver;->mOldResolver:Lcom/android/server/IntentResolverOld;

    #@11
    iget-object v2, v2, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@13
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/IntentResolver;->compareMaps(Landroid/content/IntentFilter;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    #@16
    .line 622
    const-string v0, "mWildTypeToFilter"

    #@18
    iget-object v1, p0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@1a
    iget-object v2, p0, Lcom/android/server/IntentResolver;->mOldResolver:Lcom/android/server/IntentResolverOld;

    #@1c
    iget-object v2, v2, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@1e
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/IntentResolver;->compareMaps(Landroid/content/IntentFilter;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    #@21
    .line 623
    const-string v0, "mSchemeToFilter"

    #@23
    iget-object v1, p0, Lcom/android/server/IntentResolver;->mSchemeToFilter:Ljava/util/HashMap;

    #@25
    iget-object v2, p0, Lcom/android/server/IntentResolver;->mOldResolver:Lcom/android/server/IntentResolverOld;

    #@27
    iget-object v2, v2, Lcom/android/server/IntentResolverOld;->mSchemeToFilter:Ljava/util/HashMap;

    #@29
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/IntentResolver;->compareMaps(Landroid/content/IntentFilter;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    #@2c
    .line 624
    const-string v0, "mActionToFilter"

    #@2e
    iget-object v1, p0, Lcom/android/server/IntentResolver;->mActionToFilter:Ljava/util/HashMap;

    #@30
    iget-object v2, p0, Lcom/android/server/IntentResolver;->mOldResolver:Lcom/android/server/IntentResolverOld;

    #@32
    iget-object v2, v2, Lcom/android/server/IntentResolverOld;->mActionToFilter:Ljava/util/HashMap;

    #@34
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/IntentResolver;->compareMaps(Landroid/content/IntentFilter;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    #@37
    .line 625
    const-string v0, "mTypedActionToFilter"

    #@39
    iget-object v1, p0, Lcom/android/server/IntentResolver;->mTypedActionToFilter:Ljava/util/HashMap;

    #@3b
    iget-object v2, p0, Lcom/android/server/IntentResolver;->mOldResolver:Lcom/android/server/IntentResolverOld;

    #@3d
    iget-object v2, v2, Lcom/android/server/IntentResolverOld;->mTypedActionToFilter:Ljava/util/HashMap;

    #@3f
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/IntentResolver;->compareMaps(Landroid/content/IntentFilter;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    #@42
    .line 626
    return-void
.end method


# virtual methods
.method public addFilter(Landroid/content/IntentFilter;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 58
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, f:Landroid/content/IntentFilter;,"TF;"
    iget-object v2, p0, Lcom/android/server/IntentResolver;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5
    .line 59
    invoke-virtual {p1}, Landroid/content/IntentFilter;->schemesIterator()Ljava/util/Iterator;

    #@8
    move-result-object v2

    #@9
    iget-object v3, p0, Lcom/android/server/IntentResolver;->mSchemeToFilter:Ljava/util/HashMap;

    #@b
    const-string v4, "      Scheme: "

    #@d
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolver;->register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    .line 61
    .local v0, numS:I
    const-string v2, "      Type: "

    #@13
    invoke-direct {p0, p1, v2}, Lcom/android/server/IntentResolver;->register_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I

    #@16
    move-result v1

    #@17
    .line 62
    .local v1, numT:I
    if-nez v0, :cond_26

    #@19
    if-nez v1, :cond_26

    #@1b
    .line 63
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@1e
    move-result-object v2

    #@1f
    iget-object v3, p0, Lcom/android/server/IntentResolver;->mActionToFilter:Ljava/util/HashMap;

    #@21
    const-string v4, "      Action: "

    #@23
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolver;->register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@26
    .line 66
    :cond_26
    if-eqz v1, :cond_33

    #@28
    .line 67
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@2b
    move-result-object v2

    #@2c
    iget-object v3, p0, Lcom/android/server/IntentResolver;->mTypedActionToFilter:Ljava/util/HashMap;

    #@2e
    const-string v4, "      TypedAction: "

    #@30
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolver;->register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@33
    .line 75
    :cond_33
    return-void
.end method

.method protected allowFilterResult(Landroid/content/IntentFilter;Ljava/util/List;)Z
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/util/List",
            "<TR;>;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 347
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    .local p2, dest:Ljava/util/List;,"Ljava/util/List<TR;>;"
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 15
    .parameter "out"
    .parameter "title"
    .parameter "prefix"
    .parameter "packageName"
    .parameter "printFilter"

    #@0
    .prologue
    .line 146
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "  "

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    .line 147
    .local v4, innerPrefix:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v1, "\n"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v8

    #@26
    .line 148
    .local v8, sepPrefix:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, "\n"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    .line 149
    .local v2, curPrefix:Ljava/lang/String;
    const-string v3, "Full MIME Types:"

    #@3f
    iget-object v5, p0, Lcom/android/server/IntentResolver;->mTypeToFilter:Ljava/util/HashMap;

    #@41
    move-object v0, p0

    #@42
    move-object v1, p1

    #@43
    move-object v6, p4

    #@44
    move v7, p5

    #@45
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolver;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@48
    move-result v0

    #@49
    if-eqz v0, :cond_4c

    #@4b
    .line 151
    move-object v2, v8

    #@4c
    .line 153
    :cond_4c
    const-string v3, "Base MIME Types:"

    #@4e
    iget-object v5, p0, Lcom/android/server/IntentResolver;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@50
    move-object v0, p0

    #@51
    move-object v1, p1

    #@52
    move-object v6, p4

    #@53
    move v7, p5

    #@54
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolver;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_5b

    #@5a
    .line 155
    move-object v2, v8

    #@5b
    .line 157
    :cond_5b
    const-string v3, "Wild MIME Types:"

    #@5d
    iget-object v5, p0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@5f
    move-object v0, p0

    #@60
    move-object v1, p1

    #@61
    move-object v6, p4

    #@62
    move v7, p5

    #@63
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolver;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_6a

    #@69
    .line 159
    move-object v2, v8

    #@6a
    .line 161
    :cond_6a
    const-string v3, "Schemes:"

    #@6c
    iget-object v5, p0, Lcom/android/server/IntentResolver;->mSchemeToFilter:Ljava/util/HashMap;

    #@6e
    move-object v0, p0

    #@6f
    move-object v1, p1

    #@70
    move-object v6, p4

    #@71
    move v7, p5

    #@72
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolver;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@75
    move-result v0

    #@76
    if-eqz v0, :cond_79

    #@78
    .line 163
    move-object v2, v8

    #@79
    .line 165
    :cond_79
    const-string v3, "Non-Data Actions:"

    #@7b
    iget-object v5, p0, Lcom/android/server/IntentResolver;->mActionToFilter:Ljava/util/HashMap;

    #@7d
    move-object v0, p0

    #@7e
    move-object v1, p1

    #@7f
    move-object v6, p4

    #@80
    move v7, p5

    #@81
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolver;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@84
    move-result v0

    #@85
    if-eqz v0, :cond_88

    #@87
    .line 167
    move-object v2, v8

    #@88
    .line 169
    :cond_88
    const-string v3, "MIME Typed Actions:"

    #@8a
    iget-object v5, p0, Lcom/android/server/IntentResolver;->mTypedActionToFilter:Ljava/util/HashMap;

    #@8c
    move-object v0, p0

    #@8d
    move-object v1, p1

    #@8e
    move-object v6, p4

    #@8f
    move v7, p5

    #@90
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolver;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@93
    move-result v0

    #@94
    if-eqz v0, :cond_97

    #@96
    .line 171
    move-object v2, v8

    #@97
    .line 173
    :cond_97
    if-ne v2, v8, :cond_9b

    #@99
    const/4 v0, 0x1

    #@9a
    :goto_9a
    return v0

    #@9b
    :cond_9b
    const/4 v0, 0x0

    #@9c
    goto :goto_9a
.end method

.method protected dumpFilter(Ljava/io/PrintWriter;Ljava/lang/String;Landroid/content/IntentFilter;)V
    .registers 4
    .parameter "out"
    .parameter "prefix"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Ljava/lang/String;",
            "TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 379
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p3, filter:Landroid/content/IntentFilter;,"TF;"
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    invoke-virtual {p1, p3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@6
    .line 380
    return-void
.end method

.method dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z
    .registers 22
    .parameter "out"
    .parameter "titlePrefix"
    .parameter "title"
    .parameter "prefix"
    .parameter
    .parameter "packageName"
    .parameter "printFilter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[TF;>;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 110
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p5, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;[TF;>;"
    new-instance v12, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    move-object/from16 v0, p4

    #@7
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v12

    #@b
    const-string v13, "  "

    #@d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v12

    #@11
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    .line 111
    .local v4, eprefix:Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    move-object/from16 v0, p4

    #@1c
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v12

    #@20
    const-string v13, "    "

    #@22
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v12

    #@26
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    .line 112
    .local v6, fprefix:Ljava/lang/String;
    const/4 v10, 0x0

    #@2b
    .line 113
    .local v10, printedSomething:Z
    const/4 v11, 0x0

    #@2c
    .line 114
    .local v11, printer:Landroid/util/Printer;
    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@2f
    move-result-object v12

    #@30
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@33
    move-result-object v8

    #@34
    .local v8, i$:Ljava/util/Iterator;
    :cond_34
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@37
    move-result v12

    #@38
    if-eqz v12, :cond_a4

    #@3a
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3d
    move-result-object v3

    #@3e
    check-cast v3, Ljava/util/Map$Entry;

    #@40
    .line 115
    .local v3, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;[TF;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@43
    move-result-object v2

    #@44
    check-cast v2, [Landroid/content/IntentFilter;

    #@46
    .line 116
    .local v2, a:[Landroid/content/IntentFilter;,"[TF;"
    array-length v1, v2

    #@47
    .line 117
    .local v1, N:I
    const/4 v9, 0x0

    #@48
    .line 119
    .local v9, printedHeader:Z
    const/4 v7, 0x0

    #@49
    .local v7, i:I
    :goto_49
    if-ge v7, v1, :cond_34

    #@4b
    aget-object v5, v2, v7

    #@4d
    .local v5, filter:Landroid/content/IntentFilter;,"TF;"
    if-eqz v5, :cond_34

    #@4f
    .line 120
    if-eqz p6, :cond_60

    #@51
    invoke-virtual {p0, v5}, Lcom/android/server/IntentResolver;->packageForFilter(Landroid/content/IntentFilter;)Ljava/lang/String;

    #@54
    move-result-object v12

    #@55
    move-object/from16 v0, p6

    #@57
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v12

    #@5b
    if-nez v12, :cond_60

    #@5d
    .line 119
    :cond_5d
    :goto_5d
    add-int/lit8 v7, v7, 0x1

    #@5f
    goto :goto_49

    #@60
    .line 123
    :cond_60
    if-eqz p3, :cond_6c

    #@62
    .line 124
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@65
    move-object/from16 v0, p3

    #@67
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6a
    .line 125
    const/16 p3, 0x0

    #@6c
    .line 127
    :cond_6c
    if-nez v9, :cond_80

    #@6e
    .line 128
    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@71
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@74
    move-result-object v12

    #@75
    check-cast v12, Ljava/lang/String;

    #@77
    invoke-virtual {p1, v12}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7a
    const-string v12, ":"

    #@7c
    invoke-virtual {p1, v12}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7f
    .line 129
    const/4 v9, 0x1

    #@80
    .line 131
    :cond_80
    const/4 v10, 0x1

    #@81
    .line 132
    invoke-virtual {p0, p1, v6, v5}, Lcom/android/server/IntentResolver;->dumpFilter(Ljava/io/PrintWriter;Ljava/lang/String;Landroid/content/IntentFilter;)V

    #@84
    .line 133
    if-eqz p7, :cond_5d

    #@86
    .line 134
    if-nez v11, :cond_8d

    #@88
    .line 135
    new-instance v11, Landroid/util/PrintWriterPrinter;

    #@8a
    .end local v11           #printer:Landroid/util/Printer;
    invoke-direct {v11, p1}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@8d
    .line 137
    .restart local v11       #printer:Landroid/util/Printer;
    :cond_8d
    new-instance v12, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v12

    #@96
    const-string v13, "  "

    #@98
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v12

    #@9c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v12

    #@a0
    invoke-virtual {v5, v11, v12}, Landroid/content/IntentFilter;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@a3
    goto :goto_5d

    #@a4
    .line 141
    .end local v1           #N:I
    .end local v2           #a:[Landroid/content/IntentFilter;,"[TF;"
    .end local v3           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;[TF;>;"
    .end local v5           #filter:Landroid/content/IntentFilter;,"TF;"
    .end local v7           #i:I
    .end local v9           #printedHeader:Z
    :cond_a4
    return v10
.end method

.method public filterIterator()Ljava/util/Iterator;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TF;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 205
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    new-instance v0, Lcom/android/server/IntentResolver$IteratorWrapper;

    #@2
    iget-object v1, p0, Lcom/android/server/IntentResolver;->mFilters:Ljava/util/HashSet;

    #@4
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, p0, v1}, Lcom/android/server/IntentResolver$IteratorWrapper;-><init>(Lcom/android/server/IntentResolver;Ljava/util/Iterator;)V

    #@b
    return-object v0
.end method

.method public filterSet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TF;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 212
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    iget-object v0, p0, Lcom/android/server/IntentResolver;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected isFilterStopped(Landroid/content/IntentFilter;I)Z
    .registers 4
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;I)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 356
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected abstract newArray(I)[Landroid/content/IntentFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[TF;"
        }
    .end annotation
.end method

.method protected newResult(Landroid/content/IntentFilter;II)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter "match"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;II)TR;"
        }
    .end annotation

    #@0
    .prologue
    .line 370
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    return-object p1
.end method

.method protected abstract packageForFilter(Landroid/content/IntentFilter;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public queryIntent(Landroid/content/Intent;Ljava/lang/String;ZI)Ljava/util/List;
    .registers 46
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "defaultOnly"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "ZI)",
            "Ljava/util/List",
            "<TR;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 235
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    #@3
    move-result-object v8

    #@4
    .line 237
    .local v8, scheme:Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    #@6
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 239
    .local v10, finalList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TR;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getFlags()I

    #@c
    move-result v2

    #@d
    and-int/lit8 v2, v2, 0x8

    #@f
    if-eqz v2, :cond_1db

    #@11
    const/4 v5, 0x1

    #@12
    .line 242
    .local v5, debug:Z
    :goto_12
    if-eqz v5, :cond_44

    #@14
    const-string v2, "IntentResolver"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v6, "Resolving type "

    #@1d
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    move-object/from16 v0, p2

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v6, " scheme "

    #@29
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v6, " of intent "

    #@33
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    move-object/from16 v0, p1

    #@39
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 246
    :cond_44
    const/4 v9, 0x0

    #@45
    .line 247
    .local v9, firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    const/16 v18, 0x0

    #@47
    .line 248
    .local v18, secondTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    const/16 v26, 0x0

    #@49
    .line 249
    .local v26, thirdTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    const/16 v34, 0x0

    #@4b
    .line 253
    .local v34, schemeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz p2, :cond_f4

    #@4d
    .line 254
    const/16 v2, 0x2f

    #@4f
    move-object/from16 v0, p2

    #@51
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    #@54
    move-result v40

    #@55
    .line 255
    .local v40, slashpos:I
    if-lez v40, :cond_f4

    #@57
    .line 256
    const/4 v2, 0x0

    #@58
    move-object/from16 v0, p2

    #@5a
    move/from16 v1, v40

    #@5c
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5f
    move-result-object v37

    #@60
    .line 257
    .local v37, baseType:Ljava/lang/String;
    const-string v2, "*"

    #@62
    move-object/from16 v0, v37

    #@64
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v2

    #@68
    if-nez v2, :cond_22e

    #@6a
    .line 258
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@6d
    move-result v2

    #@6e
    add-int/lit8 v3, v40, 0x2

    #@70
    if-ne v2, v3, :cond_7e

    #@72
    add-int/lit8 v2, v40, 0x1

    #@74
    move-object/from16 v0, p2

    #@76
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    #@79
    move-result v2

    #@7a
    const/16 v3, 0x2a

    #@7c
    if-eq v2, v3, :cond_1de

    #@7e
    .line 262
    :cond_7e
    move-object/from16 v0, p0

    #@80
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mTypeToFilter:Ljava/util/HashMap;

    #@82
    move-object/from16 v0, p2

    #@84
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    move-result-object v9

    #@88
    .end local v9           #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v9, [Landroid/content/IntentFilter;

    #@8a
    .line 263
    .restart local v9       #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_a4

    #@8c
    const-string v2, "IntentResolver"

    #@8e
    new-instance v3, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v6, "First type cut: "

    #@95
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v3

    #@99
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v3

    #@9d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v3

    #@a1
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 264
    :cond_a4
    move-object/from16 v0, p0

    #@a6
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@a8
    move-object/from16 v0, v37

    #@aa
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ad
    move-result-object v18

    #@ae
    .end local v18           #secondTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v18, [Landroid/content/IntentFilter;

    #@b0
    .line 265
    .restart local v18       #secondTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_cc

    #@b2
    const-string v2, "IntentResolver"

    #@b4
    new-instance v3, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v6, "Second type cut: "

    #@bb
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v3

    #@bf
    move-object/from16 v0, v18

    #@c1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v3

    #@c5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v3

    #@c9
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    .line 275
    :cond_cc
    :goto_cc
    move-object/from16 v0, p0

    #@ce
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@d0
    const-string v3, "*"

    #@d2
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d5
    move-result-object v26

    #@d6
    .end local v26           #thirdTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v26, [Landroid/content/IntentFilter;

    #@d8
    .line 276
    .restart local v26       #thirdTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_f4

    #@da
    const-string v2, "IntentResolver"

    #@dc
    new-instance v3, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v6, "Third type cut: "

    #@e3
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v3

    #@e7
    move-object/from16 v0, v26

    #@e9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v3

    #@ed
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v3

    #@f1
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 290
    .end local v37           #baseType:Ljava/lang/String;
    .end local v40           #slashpos:I
    :cond_f4
    :goto_f4
    if-eqz v8, :cond_11c

    #@f6
    .line 291
    move-object/from16 v0, p0

    #@f8
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mSchemeToFilter:Ljava/util/HashMap;

    #@fa
    invoke-virtual {v2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@fd
    move-result-object v34

    #@fe
    .end local v34           #schemeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v34, [Landroid/content/IntentFilter;

    #@100
    .line 292
    .restart local v34       #schemeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_11c

    #@102
    const-string v2, "IntentResolver"

    #@104
    new-instance v3, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v6, "Scheme list: "

    #@10b
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v3

    #@10f
    move-object/from16 v0, v34

    #@111
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v3

    #@115
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v3

    #@119
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 298
    :cond_11c
    if-nez p2, :cond_14e

    #@11e
    if-nez v8, :cond_14e

    #@120
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@123
    move-result-object v2

    #@124
    if-eqz v2, :cond_14e

    #@126
    .line 299
    move-object/from16 v0, p0

    #@128
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mActionToFilter:Ljava/util/HashMap;

    #@12a
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@12d
    move-result-object v3

    #@12e
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@131
    move-result-object v9

    #@132
    .end local v9           #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v9, [Landroid/content/IntentFilter;

    #@134
    .line 300
    .restart local v9       #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_14e

    #@136
    const-string v2, "IntentResolver"

    #@138
    new-instance v3, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v6, "Action list: "

    #@13f
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v3

    #@143
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v3

    #@147
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v3

    #@14b
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@14e
    .line 303
    :cond_14e
    invoke-static/range {p1 .. p1}, Lcom/android/server/IntentResolver;->getFastIntentCategories(Landroid/content/Intent;)Landroid/util/FastImmutableArraySet;

    #@151
    move-result-object v4

    #@152
    .line 304
    .local v4, categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    if-eqz v9, :cond_161

    #@154
    move-object/from16 v2, p0

    #@156
    move-object/from16 v3, p1

    #@158
    move/from16 v6, p3

    #@15a
    move-object/from16 v7, p2

    #@15c
    move/from16 v11, p4

    #@15e
    .line 305
    invoke-direct/range {v2 .. v11}, Lcom/android/server/IntentResolver;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;[Landroid/content/IntentFilter;Ljava/util/List;I)V

    #@161
    .line 308
    :cond_161
    if-eqz v18, :cond_176

    #@163
    move-object/from16 v11, p0

    #@165
    move-object/from16 v12, p1

    #@167
    move-object v13, v4

    #@168
    move v14, v5

    #@169
    move/from16 v15, p3

    #@16b
    move-object/from16 v16, p2

    #@16d
    move-object/from16 v17, v8

    #@16f
    move-object/from16 v19, v10

    #@171
    move/from16 v20, p4

    #@173
    .line 309
    invoke-direct/range {v11 .. v20}, Lcom/android/server/IntentResolver;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;[Landroid/content/IntentFilter;Ljava/util/List;I)V

    #@176
    .line 312
    :cond_176
    if-eqz v26, :cond_18d

    #@178
    move-object/from16 v19, p0

    #@17a
    move-object/from16 v20, p1

    #@17c
    move-object/from16 v21, v4

    #@17e
    move/from16 v22, v5

    #@180
    move/from16 v23, p3

    #@182
    move-object/from16 v24, p2

    #@184
    move-object/from16 v25, v8

    #@186
    move-object/from16 v27, v10

    #@188
    move/from16 v28, p4

    #@18a
    .line 313
    invoke-direct/range {v19 .. v28}, Lcom/android/server/IntentResolver;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;[Landroid/content/IntentFilter;Ljava/util/List;I)V

    #@18d
    .line 316
    :cond_18d
    if-eqz v34, :cond_1a4

    #@18f
    move-object/from16 v27, p0

    #@191
    move-object/from16 v28, p1

    #@193
    move-object/from16 v29, v4

    #@195
    move/from16 v30, v5

    #@197
    move/from16 v31, p3

    #@199
    move-object/from16 v32, p2

    #@19b
    move-object/from16 v33, v8

    #@19d
    move-object/from16 v35, v10

    #@19f
    move/from16 v36, p4

    #@1a1
    .line 317
    invoke-direct/range {v27 .. v36}, Lcom/android/server/IntentResolver;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;[Landroid/content/IntentFilter;Ljava/util/List;I)V

    #@1a4
    .line 320
    :cond_1a4
    move-object/from16 v0, p0

    #@1a6
    invoke-virtual {v0, v10}, Lcom/android/server/IntentResolver;->sortResults(Ljava/util/List;)V

    #@1a9
    .line 332
    if-eqz v5, :cond_25e

    #@1ab
    .line 333
    const-string v2, "IntentResolver"

    #@1ad
    const-string v3, "Final result list:"

    #@1af
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b2
    .line 334
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1b5
    move-result-object v38

    #@1b6
    .local v38, i$:Ljava/util/Iterator;
    :goto_1b6
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    #@1b9
    move-result v2

    #@1ba
    if-eqz v2, :cond_25e

    #@1bc
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1bf
    move-result-object v39

    #@1c0
    .line 335
    .local v39, r:Ljava/lang/Object;,"TR;"
    const-string v2, "IntentResolver"

    #@1c2
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    const-string v6, "  "

    #@1c9
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v3

    #@1cd
    move-object/from16 v0, v39

    #@1cf
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v3

    #@1d3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d6
    move-result-object v3

    #@1d7
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1da
    goto :goto_1b6

    #@1db
    .line 239
    .end local v4           #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .end local v5           #debug:Z
    .end local v9           #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    .end local v18           #secondTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    .end local v26           #thirdTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    .end local v34           #schemeCut:[Landroid/content/IntentFilter;,"[TF;"
    .end local v38           #i$:Ljava/util/Iterator;
    .end local v39           #r:Ljava/lang/Object;,"TR;"
    :cond_1db
    const/4 v5, 0x0

    #@1dc
    goto/16 :goto_12

    #@1de
    .line 268
    .restart local v5       #debug:Z
    .restart local v9       #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    .restart local v18       #secondTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    .restart local v26       #thirdTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    .restart local v34       #schemeCut:[Landroid/content/IntentFilter;,"[TF;"
    .restart local v37       #baseType:Ljava/lang/String;
    .restart local v40       #slashpos:I
    :cond_1de
    move-object/from16 v0, p0

    #@1e0
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@1e2
    move-object/from16 v0, v37

    #@1e4
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e7
    move-result-object v9

    #@1e8
    .end local v9           #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v9, [Landroid/content/IntentFilter;

    #@1ea
    .line 269
    .restart local v9       #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_204

    #@1ec
    const-string v2, "IntentResolver"

    #@1ee
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    const-string v6, "First type cut: "

    #@1f5
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v3

    #@1f9
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v3

    #@1fd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@200
    move-result-object v3

    #@201
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@204
    .line 270
    :cond_204
    move-object/from16 v0, p0

    #@206
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mWildTypeToFilter:Ljava/util/HashMap;

    #@208
    move-object/from16 v0, v37

    #@20a
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@20d
    move-result-object v18

    #@20e
    .end local v18           #secondTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v18, [Landroid/content/IntentFilter;

    #@210
    .line 271
    .restart local v18       #secondTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_cc

    #@212
    const-string v2, "IntentResolver"

    #@214
    new-instance v3, Ljava/lang/StringBuilder;

    #@216
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@219
    const-string v6, "Second type cut: "

    #@21b
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v3

    #@21f
    move-object/from16 v0, v18

    #@221
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v3

    #@225
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@228
    move-result-object v3

    #@229
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22c
    goto/16 :goto_cc

    #@22e
    .line 277
    :cond_22e
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@231
    move-result-object v2

    #@232
    if-eqz v2, :cond_f4

    #@234
    .line 281
    move-object/from16 v0, p0

    #@236
    iget-object v2, v0, Lcom/android/server/IntentResolver;->mTypedActionToFilter:Ljava/util/HashMap;

    #@238
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@23b
    move-result-object v3

    #@23c
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@23f
    move-result-object v9

    #@240
    .end local v9           #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    check-cast v9, [Landroid/content/IntentFilter;

    #@242
    .line 282
    .restart local v9       #firstTypeCut:[Landroid/content/IntentFilter;,"[TF;"
    if-eqz v5, :cond_f4

    #@244
    const-string v2, "IntentResolver"

    #@246
    new-instance v3, Ljava/lang/StringBuilder;

    #@248
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24b
    const-string v6, "Typed Action list: "

    #@24d
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v3

    #@251
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@254
    move-result-object v3

    #@255
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@258
    move-result-object v3

    #@259
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25c
    goto/16 :goto_f4

    #@25e
    .line 338
    .end local v37           #baseType:Ljava/lang/String;
    .end local v40           #slashpos:I
    .restart local v4       #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    :cond_25e
    return-object v10
.end method

.method public queryIntentFromList(Landroid/content/Intent;Ljava/lang/String;ZLjava/util/ArrayList;I)Ljava/util/List;
    .registers 19
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "defaultOnly"
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList",
            "<[TF;>;I)",
            "Ljava/util/List",
            "<TR;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 217
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p4, listCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[TF;>;"
    new-instance v9, Ljava/util/ArrayList;

    #@2
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 219
    .local v9, resultList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TR;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    #@8
    move-result v1

    #@9
    and-int/lit8 v1, v1, 0x8

    #@b
    if-eqz v1, :cond_32

    #@d
    const/4 v4, 0x1

    #@e
    .line 222
    .local v4, debug:Z
    :goto_e
    invoke-static {p1}, Lcom/android/server/IntentResolver;->getFastIntentCategories(Landroid/content/Intent;)Landroid/util/FastImmutableArraySet;

    #@11
    move-result-object v3

    #@12
    .line 223
    .local v3, categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    #@15
    move-result-object v7

    #@16
    .line 224
    .local v7, scheme:Ljava/lang/String;
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v11

    #@1a
    .line 225
    .local v11, N:I
    const/4 v12, 0x0

    #@1b
    .local v12, i:I
    :goto_1b
    if-ge v12, v11, :cond_34

    #@1d
    .line 226
    move-object/from16 v0, p4

    #@1f
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v8

    #@23
    check-cast v8, [Landroid/content/IntentFilter;

    #@25
    move-object v1, p0

    #@26
    move-object v2, p1

    #@27
    move/from16 v5, p3

    #@29
    move-object v6, p2

    #@2a
    move/from16 v10, p5

    #@2c
    invoke-direct/range {v1 .. v10}, Lcom/android/server/IntentResolver;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;[Landroid/content/IntentFilter;Ljava/util/List;I)V

    #@2f
    .line 225
    add-int/lit8 v12, v12, 0x1

    #@31
    goto :goto_1b

    #@32
    .line 219
    .end local v3           #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .end local v4           #debug:Z
    .end local v7           #scheme:Ljava/lang/String;
    .end local v11           #N:I
    .end local v12           #i:I
    :cond_32
    const/4 v4, 0x0

    #@33
    goto :goto_e

    #@34
    .line 229
    .restart local v3       #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .restart local v4       #debug:Z
    .restart local v7       #scheme:Ljava/lang/String;
    .restart local v11       #N:I
    .restart local v12       #i:I
    :cond_34
    invoke-virtual {p0, v9}, Lcom/android/server/IntentResolver;->sortResults(Ljava/util/List;)V

    #@37
    .line 230
    return-object v9
.end method

.method public removeFilter(Landroid/content/IntentFilter;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 78
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, f:Landroid/content/IntentFilter;,"TF;"
    invoke-virtual {p0, p1}, Lcom/android/server/IntentResolver;->removeFilterInternal(Landroid/content/IntentFilter;)V

    #@3
    .line 79
    iget-object v0, p0, Lcom/android/server/IntentResolver;->mFilters:Ljava/util/HashSet;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@8
    .line 85
    return-void
.end method

.method removeFilterInternal(Landroid/content/IntentFilter;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 94
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, f:Landroid/content/IntentFilter;,"TF;"
    invoke-virtual {p1}, Landroid/content/IntentFilter;->schemesIterator()Ljava/util/Iterator;

    #@3
    move-result-object v2

    #@4
    iget-object v3, p0, Lcom/android/server/IntentResolver;->mSchemeToFilter:Ljava/util/HashMap;

    #@6
    const-string v4, "      Scheme: "

    #@8
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolver;->unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    .line 96
    .local v0, numS:I
    const-string v2, "      Type: "

    #@e
    invoke-direct {p0, p1, v2}, Lcom/android/server/IntentResolver;->unregister_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I

    #@11
    move-result v1

    #@12
    .line 97
    .local v1, numT:I
    if-nez v0, :cond_21

    #@14
    if-nez v1, :cond_21

    #@16
    .line 98
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@19
    move-result-object v2

    #@1a
    iget-object v3, p0, Lcom/android/server/IntentResolver;->mActionToFilter:Ljava/util/HashMap;

    #@1c
    const-string v4, "      Action: "

    #@1e
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolver;->unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@21
    .line 101
    :cond_21
    if-eqz v1, :cond_2e

    #@23
    .line 102
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@26
    move-result-object v2

    #@27
    iget-object v3, p0, Lcom/android/server/IntentResolver;->mTypedActionToFilter:Ljava/util/HashMap;

    #@29
    const-string v4, "      TypedAction: "

    #@2b
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolver;->unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@2e
    .line 105
    :cond_2e
    return-void
.end method

.method protected sortResults(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TR;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 375
    .local p0, this:Lcom/android/server/IntentResolver;,"Lcom/android/server/IntentResolver<TF;TR;>;"
    .local p1, results:Ljava/util/List;,"Ljava/util/List<TR;>;"
    sget-object v0, Lcom/android/server/IntentResolver;->mResolvePrioritySorter:Ljava/util/Comparator;

    #@2
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@5
    .line 376
    return-void
.end method
