.class public Lcom/android/server/SystemBackupAgent;
.super Landroid/app/backup/BackupAgentHelper;
.source "SystemBackupAgent.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SystemBackupAgent"

.field private static final WALLPAPER_IMAGE:Ljava/lang/String; = null

.field private static final WALLPAPER_IMAGE_DIR:Ljava/lang/String; = null

.field private static final WALLPAPER_IMAGE_FILENAME:Ljava/lang/String; = "wallpaper"

.field private static final WALLPAPER_IMAGE_KEY:Ljava/lang/String; = "/data/data/com.android.settings/files/wallpaper"

.field private static final WALLPAPER_INFO:Ljava/lang/String; = null

.field private static final WALLPAPER_INFO_DIR:Ljava/lang/String; = null

.field private static final WALLPAPER_INFO_FILENAME:Ljava/lang/String; = "wallpaper_info.xml"

.field private static final WALLPAPER_INFO_KEY:Ljava/lang/String; = "/data/system/wallpaper_info.xml"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 50
    invoke-static {v1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE_DIR:Ljava/lang/String;

    #@b
    .line 52
    sget-object v0, Landroid/app/backup/WallpaperBackupHelper;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@d
    sput-object v0, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@f
    .line 55
    invoke-static {v1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    sput-object v0, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO_DIR:Ljava/lang/String;

    #@19
    .line 57
    sget-object v0, Landroid/app/backup/WallpaperBackupHelper;->WALLPAPER_INFO:Ljava/lang/String;

    #@1b
    sput-object v0, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@1d
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/backup/BackupAgentHelper;-><init>()V

    #@3
    return-void
.end method

.method private fullWallpaperBackup(Landroid/app/backup/FullBackupDataOutput;)V
    .registers 8
    .parameter "output"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 92
    invoke-virtual {p0}, Lcom/android/server/SystemBackupAgent;->getPackageName()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    const-string v1, "r"

    #@7
    sget-object v3, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO_DIR:Ljava/lang/String;

    #@9
    sget-object v4, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@b
    invoke-virtual {p1}, Landroid/app/backup/FullBackupDataOutput;->getData()Landroid/app/backup/BackupDataOutput;

    #@e
    move-result-object v5

    #@f
    invoke-static/range {v0 .. v5}, Landroid/app/backup/FullBackup;->backupToTar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/BackupDataOutput;)I

    #@12
    .line 94
    invoke-virtual {p0}, Lcom/android/server/SystemBackupAgent;->getPackageName()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    const-string v1, "r"

    #@18
    sget-object v3, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE_DIR:Ljava/lang/String;

    #@1a
    sget-object v4, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@1c
    invoke-virtual {p1}, Landroid/app/backup/FullBackupDataOutput;->getData()Landroid/app/backup/BackupDataOutput;

    #@1f
    move-result-object v5

    #@20
    invoke-static/range {v0 .. v5}, Landroid/app/backup/FullBackup;->backupToTar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/BackupDataOutput;)I

    #@23
    .line 96
    return-void
.end method


# virtual methods
.method public onBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .registers 11
    .parameter "oldState"
    .parameter "data"
    .parameter "newState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 66
    const-string v3, "wallpaper"

    #@5
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Lcom/android/server/WallpaperManagerService;

    #@b
    .line 68
    .local v2, wallpaper:Lcom/android/server/WallpaperManagerService;
    new-array v0, v6, [Ljava/lang/String;

    #@d
    sget-object v3, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@f
    aput-object v3, v0, v4

    #@11
    sget-object v3, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@13
    aput-object v3, v0, v5

    #@15
    .line 69
    .local v0, files:[Ljava/lang/String;
    new-array v1, v6, [Ljava/lang/String;

    #@17
    const-string v3, "/data/data/com.android.settings/files/wallpaper"

    #@19
    aput-object v3, v1, v4

    #@1b
    const-string v3, "/data/system/wallpaper_info.xml"

    #@1d
    aput-object v3, v1, v5

    #@1f
    .line 70
    .local v1, keys:[Ljava/lang/String;
    if-eqz v2, :cond_3d

    #@21
    invoke-virtual {v2}, Lcom/android/server/WallpaperManagerService;->getName()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    if-eqz v3, :cond_3d

    #@27
    invoke-virtual {v2}, Lcom/android/server/WallpaperManagerService;->getName()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@2e
    move-result v3

    #@2f
    if-lez v3, :cond_3d

    #@31
    .line 74
    new-array v0, v5, [Ljava/lang/String;

    #@33
    .end local v0           #files:[Ljava/lang/String;
    sget-object v3, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@35
    aput-object v3, v0, v4

    #@37
    .line 75
    .restart local v0       #files:[Ljava/lang/String;
    new-array v1, v5, [Ljava/lang/String;

    #@39
    .end local v1           #keys:[Ljava/lang/String;
    const-string v3, "/data/system/wallpaper_info.xml"

    #@3b
    aput-object v3, v1, v4

    #@3d
    .line 77
    .restart local v1       #keys:[Ljava/lang/String;
    :cond_3d
    const-string v3, "wallpaper"

    #@3f
    new-instance v4, Landroid/app/backup/WallpaperBackupHelper;

    #@41
    invoke-direct {v4, p0, v0, v1}, Landroid/app/backup/WallpaperBackupHelper;-><init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)V

    #@44
    invoke-virtual {p0, v3, v4}, Lcom/android/server/SystemBackupAgent;->addHelper(Ljava/lang/String;Landroid/app/backup/BackupHelper;)V

    #@47
    .line 78
    invoke-super {p0, p1, p2, p3}, Landroid/app/backup/BackupAgentHelper;->onBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V

    #@4a
    .line 79
    return-void
.end method

.method public onFullBackup(Landroid/app/backup/FullBackupDataOutput;)V
    .registers 2
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/android/server/SystemBackupAgent;->fullWallpaperBackup(Landroid/app/backup/FullBackupDataOutput;)V

    #@3
    .line 85
    return-void
.end method

.method public onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V
    .registers 13
    .parameter "data"
    .parameter "appVersionCode"
    .parameter "newState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    .line 102
    const-string v2, "wallpaper"

    #@5
    new-instance v3, Landroid/app/backup/WallpaperBackupHelper;

    #@7
    new-array v4, v6, [Ljava/lang/String;

    #@9
    sget-object v5, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@b
    aput-object v5, v4, v7

    #@d
    sget-object v5, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@f
    aput-object v5, v4, v8

    #@11
    new-array v5, v6, [Ljava/lang/String;

    #@13
    const-string v6, "/data/data/com.android.settings/files/wallpaper"

    #@15
    aput-object v6, v5, v7

    #@17
    const-string v6, "/data/system/wallpaper_info.xml"

    #@19
    aput-object v6, v5, v8

    #@1b
    invoke-direct {v3, p0, v4, v5}, Landroid/app/backup/WallpaperBackupHelper;-><init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)V

    #@1e
    invoke-virtual {p0, v2, v3}, Lcom/android/server/SystemBackupAgent;->addHelper(Ljava/lang/String;Landroid/app/backup/BackupHelper;)V

    #@21
    .line 105
    const-string v2, "system_files"

    #@23
    new-instance v3, Landroid/app/backup/WallpaperBackupHelper;

    #@25
    new-array v4, v8, [Ljava/lang/String;

    #@27
    sget-object v5, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@29
    aput-object v5, v4, v7

    #@2b
    new-array v5, v8, [Ljava/lang/String;

    #@2d
    const-string v6, "/data/data/com.android.settings/files/wallpaper"

    #@2f
    aput-object v6, v5, v7

    #@31
    invoke-direct {v3, p0, v4, v5}, Landroid/app/backup/WallpaperBackupHelper;-><init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)V

    #@34
    invoke-virtual {p0, v2, v3}, Lcom/android/server/SystemBackupAgent;->addHelper(Ljava/lang/String;Landroid/app/backup/BackupHelper;)V

    #@37
    .line 110
    :try_start_37
    invoke-super {p0, p1, p2, p3}, Landroid/app/backup/BackupAgentHelper;->onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V

    #@3a
    .line 112
    const-string v2, "wallpaper"

    #@3c
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3f
    move-result-object v1

    #@40
    check-cast v1, Lcom/android/server/WallpaperManagerService;

    #@42
    .line 114
    .local v1, wallpaper:Lcom/android/server/WallpaperManagerService;
    invoke-virtual {v1}, Lcom/android/server/WallpaperManagerService;->settingsRestored()V
    :try_end_45
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_45} :catch_46

    #@45
    .line 122
    .end local v1           #wallpaper:Lcom/android/server/WallpaperManagerService;
    :goto_45
    return-void

    #@46
    .line 115
    :catch_46
    move-exception v0

    #@47
    .line 118
    .local v0, ex:Ljava/io/IOException;
    const-string v2, "SystemBackupAgent"

    #@49
    const-string v3, "restore failed"

    #@4b
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4e
    .line 119
    new-instance v2, Ljava/io/File;

    #@50
    sget-object v3, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@52
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@55
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@58
    .line 120
    new-instance v2, Ljava/io/File;

    #@5a
    sget-object v3, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@5c
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5f
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@62
    goto :goto_45
.end method

.method public onRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJ)V
    .registers 24
    .parameter "data"
    .parameter "size"
    .parameter "type"
    .parameter "domain"
    .parameter "path"
    .parameter "mode"
    .parameter "mtime"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 128
    const-string v1, "SystemBackupAgent"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Restoring file domain="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    move-object/from16 v0, p5

    #@f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " path="

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    move-object/from16 v0, p6

    #@1b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 131
    const/4 v11, 0x0

    #@27
    .line 133
    .local v11, restoredWallpaper:Z
    const/4 v9, 0x0

    #@28
    .line 135
    .local v9, outFile:Ljava/io/File;
    const-string v1, "r"

    #@2a
    move-object/from16 v0, p5

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_44

    #@32
    .line 136
    const-string v1, "wallpaper_info.xml"

    #@34
    move-object/from16 v0, p6

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_8b

    #@3c
    .line 137
    new-instance v9, Ljava/io/File;

    #@3e
    .end local v9           #outFile:Ljava/io/File;
    sget-object v1, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@40
    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@43
    .line 138
    .restart local v9       #outFile:Ljava/io/File;
    const/4 v11, 0x1

    #@44
    .line 146
    :cond_44
    :goto_44
    if-nez v9, :cond_72

    #@46
    .line 147
    :try_start_46
    const-string v1, "SystemBackupAgent"

    #@48
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "Skipping unrecognized system file: [ "

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    move-object/from16 v0, p5

    #@55
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    const-string v3, " : "

    #@5b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    move-object/from16 v0, p6

    #@61
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    const-string v3, " ]"

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    :cond_72
    move-object v1, p1

    #@73
    move-wide v2, p2

    #@74
    move/from16 v4, p4

    #@76
    move-wide/from16 v5, p7

    #@78
    move-wide/from16 v7, p9

    #@7a
    .line 149
    invoke-static/range {v1 .. v9}, Landroid/app/backup/FullBackup;->restoreFile(Landroid/os/ParcelFileDescriptor;JIJJLjava/io/File;)V

    #@7d
    .line 151
    if-eqz v11, :cond_8a

    #@7f
    .line 152
    const-string v1, "wallpaper"

    #@81
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@84
    move-result-object v12

    #@85
    check-cast v12, Lcom/android/server/WallpaperManagerService;

    #@87
    .line 155
    .local v12, wallpaper:Lcom/android/server/WallpaperManagerService;
    invoke-virtual {v12}, Lcom/android/server/WallpaperManagerService;->settingsRestored()V
    :try_end_8a
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_8a} :catch_9e

    #@8a
    .line 164
    .end local v12           #wallpaper:Lcom/android/server/WallpaperManagerService;
    :cond_8a
    :goto_8a
    return-void

    #@8b
    .line 139
    :cond_8b
    const-string v1, "wallpaper"

    #@8d
    move-object/from16 v0, p6

    #@8f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v1

    #@93
    if-eqz v1, :cond_44

    #@95
    .line 140
    new-instance v9, Ljava/io/File;

    #@97
    .end local v9           #outFile:Ljava/io/File;
    sget-object v1, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@99
    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9c
    .line 141
    .restart local v9       #outFile:Ljava/io/File;
    const/4 v11, 0x1

    #@9d
    goto :goto_44

    #@9e
    .line 157
    :catch_9e
    move-exception v10

    #@9f
    .line 158
    .local v10, e:Ljava/io/IOException;
    if-eqz v11, :cond_8a

    #@a1
    .line 160
    new-instance v1, Ljava/io/File;

    #@a3
    sget-object v2, Lcom/android/server/SystemBackupAgent;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@a5
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a8
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@ab
    .line 161
    new-instance v1, Ljava/io/File;

    #@ad
    sget-object v2, Lcom/android/server/SystemBackupAgent;->WALLPAPER_INFO:Ljava/lang/String;

    #@af
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b2
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@b5
    goto :goto_8a
.end method
