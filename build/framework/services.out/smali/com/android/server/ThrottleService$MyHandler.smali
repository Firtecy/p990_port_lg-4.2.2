.class Lcom/android/server/ThrottleService$MyHandler;
.super Landroid/os/Handler;
.source "ThrottleService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ThrottleService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ThrottleService;


# direct methods
.method public constructor <init>(Lcom/android/server/ThrottleService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "l"

    #@0
    .prologue
    .line 390
    iput-object p1, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2
    .line 391
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 392
    return-void
.end method

.method private calculatePeriodEnd(J)Ljava/util/Calendar;
    .registers 12
    .parameter "now"

    #@0
    .prologue
    const/16 v8, 0xb

    #@2
    const/4 v7, 0x5

    #@3
    const/4 v6, 0x2

    #@4
    const/4 v5, 0x1

    #@5
    const/4 v4, 0x0

    #@6
    .line 713
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    #@9
    move-result-object v1

    #@a
    .line 714
    .local v1, end:Ljava/util/Calendar;
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@d
    .line 715
    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    #@10
    move-result v0

    #@11
    .line 716
    .local v0, day:I
    iget-object v3, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@13
    invoke-static {v3}, Lcom/android/server/ThrottleService;->access$700(Lcom/android/server/ThrottleService;)I

    #@16
    move-result v3

    #@17
    invoke-virtual {v1, v7, v3}, Ljava/util/Calendar;->set(II)V

    #@1a
    .line 717
    invoke-virtual {v1, v8, v4}, Ljava/util/Calendar;->set(II)V

    #@1d
    .line 718
    const/16 v3, 0xc

    #@1f
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->set(II)V

    #@22
    .line 719
    const/16 v3, 0xd

    #@24
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->set(II)V

    #@27
    .line 720
    const/16 v3, 0xe

    #@29
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->set(II)V

    #@2c
    .line 721
    iget-object v3, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2e
    invoke-static {v3}, Lcom/android/server/ThrottleService;->access$700(Lcom/android/server/ThrottleService;)I

    #@31
    move-result v3

    #@32
    if-lt v0, v3, :cond_49

    #@34
    .line 722
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    #@37
    move-result v2

    #@38
    .line 723
    .local v2, month:I
    if-ne v2, v8, :cond_44

    #@3a
    .line 724
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    #@3d
    move-result v3

    #@3e
    add-int/lit8 v3, v3, 0x1

    #@40
    invoke-virtual {v1, v5, v3}, Ljava/util/Calendar;->set(II)V

    #@43
    .line 725
    const/4 v2, -0x1

    #@44
    .line 727
    :cond_44
    add-int/lit8 v3, v2, 0x1

    #@46
    invoke-virtual {v1, v6, v3}, Ljava/util/Calendar;->set(II)V

    #@49
    .line 731
    .end local v2           #month:I
    :cond_49
    const-string v3, "persist.throttle.testing"

    #@4b
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    const-string v4, "true"

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v3

    #@55
    if-eqz v3, :cond_65

    #@57
    .line 732
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    #@5a
    move-result-object v1

    #@5b
    .line 733
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@5e
    .line 734
    const/16 v3, 0xd

    #@60
    const/16 v4, 0x258

    #@62
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->add(II)V

    #@65
    .line 736
    :cond_65
    return-object v1
.end method

.method private calculatePeriodStart(Ljava/util/Calendar;)Ljava/util/Calendar;
    .registers 7
    .parameter "end"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x2

    #@2
    .line 739
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Ljava/util/Calendar;

    #@8
    .line 740
    .local v1, start:Ljava/util/Calendar;
    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    #@b
    move-result v0

    #@c
    .line 741
    .local v0, month:I
    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_1d

    #@12
    .line 742
    const/16 v0, 0xc

    #@14
    .line 743
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    #@17
    move-result v2

    #@18
    add-int/lit8 v2, v2, -0x1

    #@1a
    invoke-virtual {v1, v4, v2}, Ljava/util/Calendar;->set(II)V

    #@1d
    .line 745
    :cond_1d
    add-int/lit8 v2, v0, -0x1

    #@1f
    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    #@22
    .line 748
    const-string v2, "persist.throttle.testing"

    #@24
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    const-string v3, "true"

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v2

    #@2e
    if-eqz v2, :cond_3d

    #@30
    .line 749
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    #@33
    move-result-object v1

    #@34
    .end local v1           #start:Ljava/util/Calendar;
    check-cast v1, Ljava/util/Calendar;

    #@36
    .line 750
    .restart local v1       #start:Ljava/util/Calendar;
    const/16 v2, 0xd

    #@38
    const/16 v3, -0x258

    #@3a
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->add(II)V

    #@3d
    .line 752
    :cond_3d
    return-object v1
.end method

.method private checkThrottleAndPostNotification(J)V
    .registers 28
    .parameter "currentTotal"

    #@0
    .prologue
    .line 590
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@4
    move-object/from16 v21, v0

    #@6
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@9
    move-result-object v21

    #@a
    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@d
    move-result-wide v15

    #@e
    .line 591
    .local v15, threshold:J
    const-wide/16 v21, 0x0

    #@10
    cmp-long v21, v15, v21

    #@12
    if-nez v21, :cond_18

    #@14
    .line 592
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ThrottleService$MyHandler;->clearThrottleAndNotification()V

    #@17
    .line 665
    :cond_17
    :goto_17
    return-void

    #@18
    .line 598
    :cond_18
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1c
    move-object/from16 v21, v0

    #@1e
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;

    #@21
    move-result-object v21

    #@22
    invoke-interface/range {v21 .. v21}, Landroid/util/TrustedTime;->hasCache()Z

    #@25
    move-result v21

    #@26
    if-nez v21, :cond_30

    #@28
    .line 599
    const-string v21, "ThrottleService"

    #@2a
    const-string v22, "missing trusted time, skipping throttle check"

    #@2c
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_17

    #@30
    .line 604
    :cond_30
    cmp-long v21, p1, v15

    #@32
    if-lez v21, :cond_126

    #@34
    .line 605
    move-object/from16 v0, p0

    #@36
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@38
    move-object/from16 v21, v0

    #@3a
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@3d
    move-result-object v21

    #@3e
    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@41
    move-result v21

    #@42
    const/16 v22, 0x1

    #@44
    move/from16 v0, v21

    #@46
    move/from16 v1, v22

    #@48
    if-eq v0, v1, :cond_17

    #@4a
    .line 606
    move-object/from16 v0, p0

    #@4c
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@4e
    move-object/from16 v21, v0

    #@50
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@53
    move-result-object v21

    #@54
    const/16 v22, 0x1

    #@56
    invoke-virtual/range {v21 .. v22}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@59
    .line 607
    const-string v21, "ThrottleService"

    #@5b
    new-instance v22, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v23, "Threshold "

    #@62
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v22

    #@66
    move-object/from16 v0, v22

    #@68
    move-wide v1, v15

    #@69
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v22

    #@6d
    const-string v23, " exceeded!"

    #@6f
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v22

    #@73
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v22

    #@77
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 609
    :try_start_7a
    move-object/from16 v0, p0

    #@7c
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@7e
    move-object/from16 v21, v0

    #@80
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$1200(Lcom/android/server/ThrottleService;)Landroid/os/INetworkManagementService;

    #@83
    move-result-object v21

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@88
    move-object/from16 v22, v0

    #@8a
    invoke-static/range {v22 .. v22}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@8d
    move-result-object v22

    #@8e
    move-object/from16 v0, p0

    #@90
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@92
    move-object/from16 v23, v0

    #@94
    invoke-static/range {v23 .. v23}, Lcom/android/server/ThrottleService;->access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@97
    move-result-object v23

    #@98
    invoke-virtual/range {v23 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@9b
    move-result v23

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@a0
    move-object/from16 v24, v0

    #@a2
    invoke-static/range {v24 .. v24}, Lcom/android/server/ThrottleService;->access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@a5
    move-result-object v24

    #@a6
    invoke-virtual/range {v24 .. v24}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@a9
    move-result v24

    #@aa
    invoke-interface/range {v21 .. v24}, Landroid/os/INetworkManagementService;->setInterfaceThrottle(Ljava/lang/String;II)V
    :try_end_ad
    .catch Ljava/lang/Exception; {:try_start_7a .. :try_end_ad} :catch_10a

    #@ad
    .line 615
    :goto_ad
    move-object/from16 v0, p0

    #@af
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@b1
    move-object/from16 v21, v0

    #@b3
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$1800(Lcom/android/server/ThrottleService;)Landroid/app/NotificationManager;

    #@b6
    move-result-object v21

    #@b7
    const v22, 0x1080570

    #@ba
    invoke-virtual/range {v21 .. v22}, Landroid/app/NotificationManager;->cancel(I)V

    #@bd
    .line 617
    const v21, 0x10404c4

    #@c0
    const v22, 0x10404c5

    #@c3
    const v23, 0x1080570

    #@c6
    const/16 v24, 0x2

    #@c8
    move-object/from16 v0, p0

    #@ca
    move/from16 v1, v21

    #@cc
    move/from16 v2, v22

    #@ce
    move/from16 v3, v23

    #@d0
    move/from16 v4, v24

    #@d2
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/ThrottleService$MyHandler;->postNotification(IIII)V

    #@d5
    .line 622
    new-instance v5, Landroid/content/Intent;

    #@d7
    const-string v21, "android.net.thrott.THROTTLE_ACTION"

    #@d9
    move-object/from16 v0, v21

    #@db
    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@de
    .line 623
    .local v5, broadcast:Landroid/content/Intent;
    const-string v21, "level"

    #@e0
    move-object/from16 v0, p0

    #@e2
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@e4
    move-object/from16 v22, v0

    #@e6
    invoke-static/range {v22 .. v22}, Lcom/android/server/ThrottleService;->access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@e9
    move-result-object v22

    #@ea
    invoke-virtual/range {v22 .. v22}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@ed
    move-result v22

    #@ee
    move-object/from16 v0, v21

    #@f0
    move/from16 v1, v22

    #@f2
    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@f5
    .line 625
    move-object/from16 v0, p0

    #@f7
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@f9
    move-object/from16 v21, v0

    #@fb
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@fe
    move-result-object v21

    #@ff
    sget-object v22, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@101
    move-object/from16 v0, v21

    #@103
    move-object/from16 v1, v22

    #@105
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@108
    goto/16 :goto_17

    #@10a
    .line 611
    .end local v5           #broadcast:Landroid/content/Intent;
    :catch_10a
    move-exception v6

    #@10b
    .line 612
    .local v6, e:Ljava/lang/Exception;
    const-string v21, "ThrottleService"

    #@10d
    new-instance v22, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v23, "error setting Throttle: "

    #@114
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v22

    #@118
    move-object/from16 v0, v22

    #@11a
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v22

    #@11e
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v22

    #@122
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    goto :goto_ad

    #@126
    .line 629
    .end local v6           #e:Ljava/lang/Exception;
    :cond_126
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ThrottleService$MyHandler;->clearThrottleAndNotification()V

    #@129
    .line 630
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@12d
    move-object/from16 v21, v0

    #@12f
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$900(Lcom/android/server/ThrottleService;)I

    #@132
    move-result v21

    #@133
    and-int/lit8 v21, v21, 0x2

    #@135
    if-eqz v21, :cond_17

    #@137
    .line 642
    move-object/from16 v0, p0

    #@139
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@13b
    move-object/from16 v21, v0

    #@13d
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@140
    move-result-object v21

    #@141
    invoke-virtual/range {v21 .. v21}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodStart()J

    #@144
    move-result-wide v13

    #@145
    .line 643
    .local v13, start:J
    move-object/from16 v0, p0

    #@147
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@149
    move-object/from16 v21, v0

    #@14b
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@14e
    move-result-object v21

    #@14f
    invoke-virtual/range {v21 .. v21}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodEnd()J

    #@152
    move-result-wide v7

    #@153
    .line 644
    .local v7, end:J
    sub-long v11, v7, v13

    #@155
    .line 645
    .local v11, periodLength:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@158
    move-result-wide v9

    #@159
    .line 646
    .local v9, now:J
    sub-long v17, v9, v13

    #@15b
    .line 647
    .local v17, timeUsed:J
    const-wide/16 v21, 0x2

    #@15d
    mul-long v21, v21, v15

    #@15f
    mul-long v21, v21, v17

    #@161
    add-long v23, v17, v11

    #@163
    div-long v19, v21, v23

    #@165
    .line 648
    .local v19, warningThreshold:J
    cmp-long v21, p1, v19

    #@167
    if-lez v21, :cond_1b2

    #@169
    const-wide/16 v21, 0x4

    #@16b
    div-long v21, v15, v21

    #@16d
    cmp-long v21, p1, v21

    #@16f
    if-lez v21, :cond_1b2

    #@171
    .line 649
    move-object/from16 v0, p0

    #@173
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@175
    move-object/from16 v21, v0

    #@177
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$1900(Lcom/android/server/ThrottleService;)Z

    #@17a
    move-result v21

    #@17b
    if-nez v21, :cond_17

    #@17d
    .line 650
    move-object/from16 v0, p0

    #@17f
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@181
    move-object/from16 v21, v0

    #@183
    const/16 v22, 0x1

    #@185
    invoke-static/range {v21 .. v22}, Lcom/android/server/ThrottleService;->access$1902(Lcom/android/server/ThrottleService;Z)Z

    #@188
    .line 651
    move-object/from16 v0, p0

    #@18a
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@18c
    move-object/from16 v21, v0

    #@18e
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$1800(Lcom/android/server/ThrottleService;)Landroid/app/NotificationManager;

    #@191
    move-result-object v21

    #@192
    const v22, 0x1080570

    #@195
    invoke-virtual/range {v21 .. v22}, Landroid/app/NotificationManager;->cancel(I)V

    #@198
    .line 652
    const v21, 0x10404c2

    #@19b
    const v22, 0x10404c3

    #@19e
    const v23, 0x1080570

    #@1a1
    const/16 v24, 0x0

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    move/from16 v1, v21

    #@1a7
    move/from16 v2, v22

    #@1a9
    move/from16 v3, v23

    #@1ab
    move/from16 v4, v24

    #@1ad
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/ThrottleService$MyHandler;->postNotification(IIII)V

    #@1b0
    goto/16 :goto_17

    #@1b2
    .line 658
    :cond_1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1b6
    move-object/from16 v21, v0

    #@1b8
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$1900(Lcom/android/server/ThrottleService;)Z

    #@1bb
    move-result v21

    #@1bc
    const/16 v22, 0x1

    #@1be
    move/from16 v0, v21

    #@1c0
    move/from16 v1, v22

    #@1c2
    if-ne v0, v1, :cond_17

    #@1c4
    .line 659
    move-object/from16 v0, p0

    #@1c6
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1c8
    move-object/from16 v21, v0

    #@1ca
    invoke-static/range {v21 .. v21}, Lcom/android/server/ThrottleService;->access$1800(Lcom/android/server/ThrottleService;)Landroid/app/NotificationManager;

    #@1cd
    move-result-object v21

    #@1ce
    const v22, 0x1080570

    #@1d1
    invoke-virtual/range {v21 .. v22}, Landroid/app/NotificationManager;->cancel(I)V

    #@1d4
    .line 660
    move-object/from16 v0, p0

    #@1d6
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1d8
    move-object/from16 v21, v0

    #@1da
    const/16 v22, 0x0

    #@1dc
    invoke-static/range {v21 .. v22}, Lcom/android/server/ThrottleService;->access$1902(Lcom/android/server/ThrottleService;Z)Z

    #@1df
    goto/16 :goto_17
.end method

.method private clearThrottleAndNotification()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, -0x1

    #@2
    .line 696
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@4
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_53

    #@e
    .line 697
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@10
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, v7}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@17
    .line 699
    :try_start_17
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@19
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$1200(Lcom/android/server/ThrottleService;)Landroid/os/INetworkManagementService;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1f
    invoke-static {v3}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    const/4 v4, -0x1

    #@24
    const/4 v5, -0x1

    #@25
    invoke-interface {v2, v3, v4, v5}, Landroid/os/INetworkManagementService;->setInterfaceThrottle(Ljava/lang/String;II)V
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_28} :catch_54

    #@28
    .line 703
    :goto_28
    new-instance v0, Landroid/content/Intent;

    #@2a
    const-string v2, "android.net.thrott.THROTTLE_ACTION"

    #@2c
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2f
    .line 704
    .local v0, broadcast:Landroid/content/Intent;
    const-string v2, "level"

    #@31
    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@34
    .line 705
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@36
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@39
    move-result-object v2

    #@3a
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@3c
    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3f
    .line 706
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@41
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$1800(Lcom/android/server/ThrottleService;)Landroid/app/NotificationManager;

    #@44
    move-result-object v2

    #@45
    const/4 v3, 0x0

    #@46
    const v4, 0x1080570

    #@49
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@4b
    invoke-virtual {v2, v3, v4, v5}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@4e
    .line 708
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@50
    invoke-static {v2, v7}, Lcom/android/server/ThrottleService;->access$1902(Lcom/android/server/ThrottleService;Z)Z

    #@53
    .line 710
    .end local v0           #broadcast:Landroid/content/Intent;
    :cond_53
    return-void

    #@54
    .line 700
    :catch_54
    move-exception v1

    #@55
    .line 701
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "ThrottleService"

    #@57
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v4, "error clearing Throttle: "

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_28
.end method

.method private onIfaceUp()V
    .registers 6

    #@0
    .prologue
    .line 577
    iget-object v1, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2
    invoke-static {v1}, Lcom/android/server/ThrottleService;->access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@9
    move-result v1

    #@a
    const/4 v2, 0x1

    #@b
    if-ne v1, v2, :cond_41

    #@d
    .line 579
    :try_start_d
    iget-object v1, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@f
    invoke-static {v1}, Lcom/android/server/ThrottleService;->access$1200(Lcom/android/server/ThrottleService;)Landroid/os/INetworkManagementService;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@15
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    const/4 v3, -0x1

    #@1a
    const/4 v4, -0x1

    #@1b
    invoke-interface {v1, v2, v3, v4}, Landroid/os/INetworkManagementService;->setInterfaceThrottle(Ljava/lang/String;II)V

    #@1e
    .line 580
    iget-object v1, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@20
    invoke-static {v1}, Lcom/android/server/ThrottleService;->access$1200(Lcom/android/server/ThrottleService;)Landroid/os/INetworkManagementService;

    #@23
    move-result-object v1

    #@24
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@26
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    iget-object v3, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2c
    invoke-static {v3}, Lcom/android/server/ThrottleService;->access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@33
    move-result v3

    #@34
    iget-object v4, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@36
    invoke-static {v4}, Lcom/android/server/ThrottleService;->access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@3d
    move-result v4

    #@3e
    invoke-interface {v1, v2, v3, v4}, Landroid/os/INetworkManagementService;->setInterfaceThrottle(Ljava/lang/String;II)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_41} :catch_42

    #@41
    .line 586
    :cond_41
    :goto_41
    return-void

    #@42
    .line 582
    :catch_42
    move-exception v0

    #@43
    .line 583
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "ThrottleService"

    #@45
    new-instance v2, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v3, "error setting Throttle: "

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_41
.end method

.method private onPolicyChanged()V
    .registers 18

    #@0
    .prologue
    .line 435
    const-string v13, "persist.throttle.testing"

    #@2
    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v13

    #@6
    const-string v14, "true"

    #@8
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v9

    #@c
    .line 437
    .local v9, testing:Z
    move-object/from16 v0, p0

    #@e
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@10
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@13
    move-result-object v13

    #@14
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17
    move-result-object v13

    #@18
    const v14, 0x10e002b

    #@1b
    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getInteger(I)I

    #@1e
    move-result v8

    #@1f
    .line 439
    .local v8, pollingPeriod:I
    move-object/from16 v0, p0

    #@21
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@23
    move-object/from16 v0, p0

    #@25
    iget-object v14, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@27
    invoke-static {v14}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@2a
    move-result-object v14

    #@2b
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2e
    move-result-object v14

    #@2f
    const-string v15, "throttle_polling_sec"

    #@31
    invoke-static {v14, v15, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@34
    move-result v14

    #@35
    invoke-static {v13, v14}, Lcom/android/server/ThrottleService;->access$402(Lcom/android/server/ThrottleService;I)I

    #@38
    .line 443
    move-object/from16 v0, p0

    #@3a
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@3c
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@3f
    move-result-object v13

    #@40
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@43
    move-result-object v13

    #@44
    const v14, 0x10e002c

    #@47
    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getInteger(I)I

    #@4a
    move-result v13

    #@4b
    int-to-long v3, v13

    #@4c
    .line 445
    .local v3, defaultThreshold:J
    move-object/from16 v0, p0

    #@4e
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@50
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@53
    move-result-object v13

    #@54
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@57
    move-result-object v13

    #@58
    const v14, 0x10e002d

    #@5b
    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getInteger(I)I

    #@5e
    move-result v5

    #@5f
    .line 447
    .local v5, defaultValue:I
    move-object/from16 v0, p0

    #@61
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@63
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@66
    move-result-object v13

    #@67
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6a
    move-result-object v13

    #@6b
    const-string v14, "throttle_threshold_bytes"

    #@6d
    invoke-static {v13, v14, v3, v4}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@70
    move-result-wide v10

    #@71
    .line 449
    .local v10, threshold:J
    move-object/from16 v0, p0

    #@73
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@75
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@78
    move-result-object v13

    #@79
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7c
    move-result-object v13

    #@7d
    const-string v14, "throttle_value_kbitsps"

    #@7f
    invoke-static {v13, v14, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@82
    move-result v12

    #@83
    .line 452
    .local v12, value:I
    move-object/from16 v0, p0

    #@85
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@87
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@8a
    move-result-object v13

    #@8b
    invoke-virtual {v13, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    #@8e
    .line 453
    move-object/from16 v0, p0

    #@90
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@92
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@95
    move-result-object v13

    #@96
    invoke-virtual {v13, v12}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@99
    .line 454
    if-eqz v9, :cond_b2

    #@9b
    .line 455
    move-object/from16 v0, p0

    #@9d
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@9f
    const/16 v14, 0x3c

    #@a1
    invoke-static {v13, v14}, Lcom/android/server/ThrottleService;->access$402(Lcom/android/server/ThrottleService;I)I

    #@a4
    .line 456
    move-object/from16 v0, p0

    #@a6
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@a8
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@ab
    move-result-object v13

    #@ac
    const-wide/32 v14, 0x100000

    #@af
    invoke-virtual {v13, v14, v15}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    #@b2
    .line 459
    :cond_b2
    move-object/from16 v0, p0

    #@b4
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@b6
    move-object/from16 v0, p0

    #@b8
    iget-object v14, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@ba
    invoke-static {v14}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@bd
    move-result-object v14

    #@be
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c1
    move-result-object v14

    #@c2
    const-string v15, "throttle_reset_day"

    #@c4
    const/16 v16, -0x1

    #@c6
    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c9
    move-result v14

    #@ca
    invoke-static {v13, v14}, Lcom/android/server/ThrottleService;->access$702(Lcom/android/server/ThrottleService;I)I

    #@cd
    .line 461
    move-object/from16 v0, p0

    #@cf
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@d1
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$700(Lcom/android/server/ThrottleService;)I

    #@d4
    move-result v13

    #@d5
    const/4 v14, -0x1

    #@d6
    if-eq v13, v14, :cond_ef

    #@d8
    move-object/from16 v0, p0

    #@da
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@dc
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$700(Lcom/android/server/ThrottleService;)I

    #@df
    move-result v13

    #@e0
    const/4 v14, 0x1

    #@e1
    if-lt v13, v14, :cond_ef

    #@e3
    move-object/from16 v0, p0

    #@e5
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@e7
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$700(Lcom/android/server/ThrottleService;)I

    #@ea
    move-result v13

    #@eb
    const/16 v14, 0x1c

    #@ed
    if-le v13, v14, :cond_11c

    #@ef
    .line 463
    :cond_ef
    new-instance v6, Ljava/util/Random;

    #@f1
    invoke-direct {v6}, Ljava/util/Random;-><init>()V

    #@f4
    .line 464
    .local v6, g:Ljava/util/Random;
    move-object/from16 v0, p0

    #@f6
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@f8
    const/16 v14, 0x1c

    #@fa
    invoke-virtual {v6, v14}, Ljava/util/Random;->nextInt(I)I

    #@fd
    move-result v14

    #@fe
    add-int/lit8 v14, v14, 0x1

    #@100
    invoke-static {v13, v14}, Lcom/android/server/ThrottleService;->access$702(Lcom/android/server/ThrottleService;I)I

    #@103
    .line 465
    move-object/from16 v0, p0

    #@105
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@107
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@10a
    move-result-object v13

    #@10b
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10e
    move-result-object v13

    #@10f
    const-string v14, "throttle_reset_day"

    #@111
    move-object/from16 v0, p0

    #@113
    iget-object v15, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@115
    invoke-static {v15}, Lcom/android/server/ThrottleService;->access$700(Lcom/android/server/ThrottleService;)I

    #@118
    move-result v15

    #@119
    invoke-static {v13, v14, v15}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@11c
    .line 468
    .end local v6           #g:Ljava/util/Random;
    :cond_11c
    move-object/from16 v0, p0

    #@11e
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@120
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@123
    move-result-object v13

    #@124
    if-nez v13, :cond_133

    #@126
    .line 469
    move-object/from16 v0, p0

    #@128
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@12a
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@12d
    move-result-object v13

    #@12e
    const-wide/16 v14, 0x0

    #@130
    invoke-virtual {v13, v14, v15}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    #@133
    .line 472
    :cond_133
    move-object/from16 v0, p0

    #@135
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@137
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@13a
    move-result-object v13

    #@13b
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@13e
    move-result-object v13

    #@13f
    const v14, 0x10e002e

    #@142
    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getInteger(I)I

    #@145
    move-result v2

    #@146
    .line 474
    .local v2, defaultNotificationType:I
    move-object/from16 v0, p0

    #@148
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@14a
    move-object/from16 v0, p0

    #@14c
    iget-object v14, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@14e
    invoke-static {v14}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@151
    move-result-object v14

    #@152
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@155
    move-result-object v14

    #@156
    const-string v15, "throttle_notification_type"

    #@158
    invoke-static {v14, v15, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15b
    move-result v14

    #@15c
    invoke-static {v13, v14}, Lcom/android/server/ThrottleService;->access$902(Lcom/android/server/ThrottleService;I)I

    #@15f
    .line 477
    move-object/from16 v0, p0

    #@161
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@163
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@166
    move-result-object v13

    #@167
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16a
    move-result-object v13

    #@16b
    const-string v14, "throttle_max_ntp_cache_age_sec"

    #@16d
    const v15, 0x15180

    #@170
    invoke-static {v13, v14, v15}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@173
    move-result v7

    #@174
    .line 480
    .local v7, maxNtpCacheAgeSec:I
    move-object/from16 v0, p0

    #@176
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@178
    mul-int/lit16 v14, v7, 0x3e8

    #@17a
    int-to-long v14, v14

    #@17b
    invoke-static {v13, v14, v15}, Lcom/android/server/ThrottleService;->access$1002(Lcom/android/server/ThrottleService;J)J

    #@17e
    .line 482
    move-object/from16 v0, p0

    #@180
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@182
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@185
    move-result-object v13

    #@186
    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@189
    move-result-wide v13

    #@18a
    const-wide/16 v15, 0x0

    #@18c
    cmp-long v13, v13, v15

    #@18e
    if-eqz v13, :cond_21c

    #@190
    .line 483
    const-string v13, "ThrottleService"

    #@192
    new-instance v14, Ljava/lang/StringBuilder;

    #@194
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@197
    const-string v15, "onPolicyChanged testing="

    #@199
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v14

    #@19d
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v14

    #@1a1
    const-string v15, ", period="

    #@1a3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v14

    #@1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v15, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1ab
    invoke-static {v15}, Lcom/android/server/ThrottleService;->access$400(Lcom/android/server/ThrottleService;)I

    #@1ae
    move-result v15

    #@1af
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v14

    #@1b3
    const-string v15, ", threshold="

    #@1b5
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v14

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iget-object v15, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1bd
    invoke-static {v15}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@1c0
    move-result-object v15

    #@1c1
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@1c4
    move-result-wide v15

    #@1c5
    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v14

    #@1c9
    const-string v15, ", value="

    #@1cb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v14

    #@1cf
    move-object/from16 v0, p0

    #@1d1
    iget-object v15, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1d3
    invoke-static {v15}, Lcom/android/server/ThrottleService;->access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@1d6
    move-result-object v15

    #@1d7
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@1da
    move-result v15

    #@1db
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v14

    #@1df
    const-string v15, ", resetDay="

    #@1e1
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e4
    move-result-object v14

    #@1e5
    move-object/from16 v0, p0

    #@1e7
    iget-object v15, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1e9
    invoke-static {v15}, Lcom/android/server/ThrottleService;->access$700(Lcom/android/server/ThrottleService;)I

    #@1ec
    move-result v15

    #@1ed
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v14

    #@1f1
    const-string v15, ", noteType="

    #@1f3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v14

    #@1f7
    move-object/from16 v0, p0

    #@1f9
    iget-object v15, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1fb
    invoke-static {v15}, Lcom/android/server/ThrottleService;->access$900(Lcom/android/server/ThrottleService;)I

    #@1fe
    move-result v15

    #@1ff
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@202
    move-result-object v14

    #@203
    const-string v15, ", mMaxNtpCacheAge="

    #@205
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v14

    #@209
    move-object/from16 v0, p0

    #@20b
    iget-object v15, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@20d
    invoke-static {v15}, Lcom/android/server/ThrottleService;->access$1000(Lcom/android/server/ThrottleService;)J

    #@210
    move-result-wide v15

    #@211
    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@214
    move-result-object v14

    #@215
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v14

    #@219
    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21c
    .line 491
    :cond_21c
    move-object/from16 v0, p0

    #@21e
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@220
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@223
    move-result-object v13

    #@224
    const/4 v14, -0x1

    #@225
    invoke-virtual {v13, v14}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@228
    .line 493
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ThrottleService$MyHandler;->onResetAlarm()V

    #@22b
    .line 495
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ThrottleService$MyHandler;->onPollAlarm()V

    #@22e
    .line 497
    new-instance v1, Landroid/content/Intent;

    #@230
    const-string v13, "android.net.thrott.POLICY_CHANGED_ACTION"

    #@232
    invoke-direct {v1, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@235
    .line 498
    .local v1, broadcast:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@237
    iget-object v13, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@239
    invoke-static {v13}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@23c
    move-result-object v13

    #@23d
    sget-object v14, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@23f
    invoke-virtual {v13, v1, v14}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@242
    .line 499
    return-void
.end method

.method private onPollAlarm()V
    .registers 30

    #@0
    .prologue
    .line 502
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v15

    #@4
    .line 503
    .local v15, now:J
    move-object/from16 v0, p0

    #@6
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@8
    move-object/from16 v25, v0

    #@a
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$400(Lcom/android/server/ThrottleService;)I

    #@d
    move-result v25

    #@e
    move/from16 v0, v25

    #@10
    mul-int/lit16 v0, v0, 0x3e8

    #@12
    move/from16 v25, v0

    #@14
    move/from16 v0, v25

    #@16
    int-to-long v0, v0

    #@17
    move-wide/from16 v25, v0

    #@19
    add-long v13, v15, v25

    #@1b
    .line 506
    .local v13, next:J
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1f
    move-object/from16 v25, v0

    #@21
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;

    #@24
    move-result-object v25

    #@25
    invoke-interface/range {v25 .. v25}, Landroid/util/TrustedTime;->getCacheAge()J

    #@28
    move-result-wide v25

    #@29
    move-object/from16 v0, p0

    #@2b
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2d
    move-object/from16 v27, v0

    #@2f
    invoke-static/range {v27 .. v27}, Lcom/android/server/ThrottleService;->access$1000(Lcom/android/server/ThrottleService;)J

    #@32
    move-result-wide v27

    #@33
    cmp-long v25, v25, v27

    #@35
    if-lez v25, :cond_50

    #@37
    .line 507
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@3b
    move-object/from16 v25, v0

    #@3d
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;

    #@40
    move-result-object v25

    #@41
    invoke-interface/range {v25 .. v25}, Landroid/util/TrustedTime;->forceRefresh()Z

    #@44
    move-result v25

    #@45
    if-eqz v25, :cond_50

    #@47
    .line 509
    move-object/from16 v0, p0

    #@49
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@4b
    move-object/from16 v25, v0

    #@4d
    invoke-virtual/range {v25 .. v25}, Lcom/android/server/ThrottleService;->dispatchReset()V

    #@50
    .line 513
    :cond_50
    const-wide/16 v8, 0x0

    #@52
    .line 514
    .local v8, incRead:J
    const-wide/16 v10, 0x0

    #@54
    .line 516
    .local v10, incWrite:J
    :try_start_54
    move-object/from16 v0, p0

    #@56
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@58
    move-object/from16 v25, v0

    #@5a
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$1200(Lcom/android/server/ThrottleService;)Landroid/os/INetworkManagementService;

    #@5d
    move-result-object v25

    #@5e
    invoke-interface/range {v25 .. v25}, Landroid/os/INetworkManagementService;->getNetworkStatsSummaryDev()Landroid/net/NetworkStats;

    #@61
    move-result-object v22

    #@62
    .line 517
    .local v22, stats:Landroid/net/NetworkStats;
    move-object/from16 v0, p0

    #@64
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@66
    move-object/from16 v25, v0

    #@68
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@6b
    move-result-object v25

    #@6c
    const/16 v26, -0x1

    #@6e
    const/16 v27, 0x0

    #@70
    const/16 v28, 0x0

    #@72
    move-object/from16 v0, v22

    #@74
    move-object/from16 v1, v25

    #@76
    move/from16 v2, v26

    #@78
    move/from16 v3, v27

    #@7a
    move/from16 v4, v28

    #@7c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/net/NetworkStats;->findIndex(Ljava/lang/String;III)I

    #@7f
    move-result v12

    #@80
    .line 520
    .local v12, index:I
    const/16 v25, -0x1

    #@82
    move/from16 v0, v25

    #@84
    if-eq v12, v0, :cond_246

    #@86
    .line 521
    const/16 v25, 0x0

    #@88
    move-object/from16 v0, v22

    #@8a
    move-object/from16 v1, v25

    #@8c
    invoke-virtual {v0, v12, v1}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@8f
    move-result-object v7

    #@90
    .line 522
    .local v7, entry:Landroid/net/NetworkStats$Entry;
    iget-wide v0, v7, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@92
    move-wide/from16 v25, v0

    #@94
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@98
    move-object/from16 v27, v0

    #@9a
    invoke-static/range {v27 .. v27}, Lcom/android/server/ThrottleService;->access$1300(Lcom/android/server/ThrottleService;)J

    #@9d
    move-result-wide v27

    #@9e
    sub-long v8, v25, v27

    #@a0
    .line 523
    iget-wide v0, v7, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@a2
    move-wide/from16 v25, v0

    #@a4
    move-object/from16 v0, p0

    #@a6
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@a8
    move-object/from16 v27, v0

    #@aa
    invoke-static/range {v27 .. v27}, Lcom/android/server/ThrottleService;->access$1400(Lcom/android/server/ThrottleService;)J

    #@ad
    move-result-wide v27

    #@ae
    sub-long v10, v25, v27

    #@b0
    .line 531
    .end local v7           #entry:Landroid/net/NetworkStats$Entry;
    :goto_b0
    const-wide/16 v25, 0x0

    #@b2
    cmp-long v25, v8, v25

    #@b4
    if-ltz v25, :cond_bc

    #@b6
    const-wide/16 v25, 0x0

    #@b8
    cmp-long v25, v10, v25

    #@ba
    if-gez v25, :cond_ea

    #@bc
    .line 532
    :cond_bc
    move-object/from16 v0, p0

    #@be
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@c0
    move-object/from16 v25, v0

    #@c2
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$1300(Lcom/android/server/ThrottleService;)J

    #@c5
    move-result-wide v25

    #@c6
    add-long v8, v8, v25

    #@c8
    .line 533
    move-object/from16 v0, p0

    #@ca
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@cc
    move-object/from16 v25, v0

    #@ce
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$1400(Lcom/android/server/ThrottleService;)J

    #@d1
    move-result-wide v25

    #@d2
    add-long v10, v10, v25

    #@d4
    .line 534
    move-object/from16 v0, p0

    #@d6
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@d8
    move-object/from16 v25, v0

    #@da
    const-wide/16 v26, 0x0

    #@dc
    invoke-static/range {v25 .. v27}, Lcom/android/server/ThrottleService;->access$1302(Lcom/android/server/ThrottleService;J)J

    #@df
    .line 535
    move-object/from16 v0, p0

    #@e1
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@e3
    move-object/from16 v25, v0

    #@e5
    const-wide/16 v26, 0x0

    #@e7
    invoke-static/range {v25 .. v27}, Lcom/android/server/ThrottleService;->access$1402(Lcom/android/server/ThrottleService;J)J
    :try_end_ea
    .catch Ljava/lang/IllegalStateException; {:try_start_54 .. :try_end_ea} :catch_26a
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_ea} :catch_287

    #@ea
    .line 544
    .end local v12           #index:I
    .end local v22           #stats:Landroid/net/NetworkStats;
    :cond_ea
    :goto_ea
    const-string v25, "true"

    #@ec
    const-string v26, "gsm.operator.isroaming"

    #@ee
    invoke-static/range {v26 .. v26}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f1
    move-result-object v26

    #@f2
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f5
    move-result v21

    #@f6
    .line 546
    .local v21, roaming:Z
    if-nez v21, :cond_107

    #@f8
    .line 547
    move-object/from16 v0, p0

    #@fa
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@fc
    move-object/from16 v25, v0

    #@fe
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@101
    move-result-object v25

    #@102
    move-object/from16 v0, v25

    #@104
    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/android/server/ThrottleService$DataRecorder;->addData(JJ)V

    #@107
    .line 550
    :cond_107
    move-object/from16 v0, p0

    #@109
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@10b
    move-object/from16 v25, v0

    #@10d
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@110
    move-result-object v25

    #@111
    const/16 v26, 0x0

    #@113
    invoke-virtual/range {v25 .. v26}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodRx(I)J

    #@116
    move-result-wide v17

    #@117
    .line 551
    .local v17, periodRx:J
    move-object/from16 v0, p0

    #@119
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@11b
    move-object/from16 v25, v0

    #@11d
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@120
    move-result-object v25

    #@121
    const/16 v26, 0x0

    #@123
    invoke-virtual/range {v25 .. v26}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodTx(I)J

    #@126
    move-result-wide v19

    #@127
    .line 552
    .local v19, periodTx:J
    add-long v23, v17, v19

    #@129
    .line 553
    .local v23, total:J
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@12d
    move-object/from16 v25, v0

    #@12f
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@132
    move-result-object v25

    #@133
    invoke-virtual/range {v25 .. v25}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@136
    move-result-wide v25

    #@137
    const-wide/16 v27, 0x0

    #@139
    cmp-long v25, v25, v27

    #@13b
    if-eqz v25, :cond_17f

    #@13d
    .line 554
    const-string v25, "ThrottleService"

    #@13f
    new-instance v26, Ljava/lang/StringBuilder;

    #@141
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@144
    const-string v27, "onPollAlarm - roaming ="

    #@146
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v26

    #@14a
    move-object/from16 v0, v26

    #@14c
    move/from16 v1, v21

    #@14e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@151
    move-result-object v26

    #@152
    const-string v27, ", read ="

    #@154
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v26

    #@158
    move-object/from16 v0, v26

    #@15a
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v26

    #@15e
    const-string v27, ", written ="

    #@160
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v26

    #@164
    move-object/from16 v0, v26

    #@166
    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@169
    move-result-object v26

    #@16a
    const-string v27, ", new total ="

    #@16c
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v26

    #@170
    move-object/from16 v0, v26

    #@172
    move-wide/from16 v1, v23

    #@174
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@177
    move-result-object v26

    #@178
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17b
    move-result-object v26

    #@17c
    invoke-static/range {v25 .. v26}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17f
    .line 557
    :cond_17f
    move-object/from16 v0, p0

    #@181
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@183
    move-object/from16 v25, v0

    #@185
    move-object/from16 v0, v25

    #@187
    invoke-static {v0, v8, v9}, Lcom/android/server/ThrottleService;->access$1314(Lcom/android/server/ThrottleService;J)J

    #@18a
    .line 558
    move-object/from16 v0, p0

    #@18c
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@18e
    move-object/from16 v25, v0

    #@190
    move-object/from16 v0, v25

    #@192
    invoke-static {v0, v10, v11}, Lcom/android/server/ThrottleService;->access$1414(Lcom/android/server/ThrottleService;J)J

    #@195
    .line 560
    move-object/from16 v0, p0

    #@197
    move-wide/from16 v1, v23

    #@199
    invoke-direct {v0, v1, v2}, Lcom/android/server/ThrottleService$MyHandler;->checkThrottleAndPostNotification(J)V

    #@19c
    .line 562
    new-instance v5, Landroid/content/Intent;

    #@19e
    const-string v25, "android.net.thrott.POLL_ACTION"

    #@1a0
    move-object/from16 v0, v25

    #@1a2
    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1a5
    .line 563
    .local v5, broadcast:Landroid/content/Intent;
    const-string v25, "cycleRead"

    #@1a7
    move-object/from16 v0, v25

    #@1a9
    move-wide/from16 v1, v17

    #@1ab
    invoke-virtual {v5, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@1ae
    .line 564
    const-string v25, "cycleWrite"

    #@1b0
    move-object/from16 v0, v25

    #@1b2
    move-wide/from16 v1, v19

    #@1b4
    invoke-virtual {v5, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@1b7
    .line 565
    const-string v25, "cycleStart"

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1bd
    move-object/from16 v26, v0

    #@1bf
    move-object/from16 v0, p0

    #@1c1
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1c3
    move-object/from16 v27, v0

    #@1c5
    invoke-static/range {v27 .. v27}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@1c8
    move-result-object v27

    #@1c9
    invoke-virtual/range {v26 .. v27}, Lcom/android/server/ThrottleService;->getPeriodStartTime(Ljava/lang/String;)J

    #@1cc
    move-result-wide v26

    #@1cd
    move-object/from16 v0, v25

    #@1cf
    move-wide/from16 v1, v26

    #@1d1
    invoke-virtual {v5, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@1d4
    .line 566
    const-string v25, "cycleEnd"

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1da
    move-object/from16 v26, v0

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1e0
    move-object/from16 v27, v0

    #@1e2
    invoke-static/range {v27 .. v27}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@1e5
    move-result-object v27

    #@1e6
    invoke-virtual/range {v26 .. v27}, Lcom/android/server/ThrottleService;->getResetTime(Ljava/lang/String;)J

    #@1e9
    move-result-wide v26

    #@1ea
    move-object/from16 v0, v25

    #@1ec
    move-wide/from16 v1, v26

    #@1ee
    invoke-virtual {v5, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@1f1
    .line 567
    move-object/from16 v0, p0

    #@1f3
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1f5
    move-object/from16 v25, v0

    #@1f7
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@1fa
    move-result-object v25

    #@1fb
    sget-object v26, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1fd
    move-object/from16 v0, v25

    #@1ff
    move-object/from16 v1, v26

    #@201
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@204
    .line 568
    move-object/from16 v0, p0

    #@206
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@208
    move-object/from16 v25, v0

    #@20a
    move-object/from16 v0, v25

    #@20c
    invoke-static {v0, v5}, Lcom/android/server/ThrottleService;->access$1502(Lcom/android/server/ThrottleService;Landroid/content/Intent;)Landroid/content/Intent;

    #@20f
    .line 570
    move-object/from16 v0, p0

    #@211
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@213
    move-object/from16 v25, v0

    #@215
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$1700(Lcom/android/server/ThrottleService;)Landroid/app/AlarmManager;

    #@218
    move-result-object v25

    #@219
    move-object/from16 v0, p0

    #@21b
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@21d
    move-object/from16 v26, v0

    #@21f
    invoke-static/range {v26 .. v26}, Lcom/android/server/ThrottleService;->access$1600(Lcom/android/server/ThrottleService;)Landroid/app/PendingIntent;

    #@222
    move-result-object v26

    #@223
    invoke-virtual/range {v25 .. v26}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@226
    .line 571
    move-object/from16 v0, p0

    #@228
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@22a
    move-object/from16 v25, v0

    #@22c
    invoke-static/range {v25 .. v25}, Lcom/android/server/ThrottleService;->access$1700(Lcom/android/server/ThrottleService;)Landroid/app/AlarmManager;

    #@22f
    move-result-object v25

    #@230
    const/16 v26, 0x3

    #@232
    move-object/from16 v0, p0

    #@234
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@236
    move-object/from16 v27, v0

    #@238
    invoke-static/range {v27 .. v27}, Lcom/android/server/ThrottleService;->access$1600(Lcom/android/server/ThrottleService;)Landroid/app/PendingIntent;

    #@23b
    move-result-object v27

    #@23c
    move-object/from16 v0, v25

    #@23e
    move/from16 v1, v26

    #@240
    move-object/from16 v2, v27

    #@242
    invoke-virtual {v0, v1, v13, v14, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@245
    .line 572
    return-void

    #@246
    .line 526
    .end local v5           #broadcast:Landroid/content/Intent;
    .end local v17           #periodRx:J
    .end local v19           #periodTx:J
    .end local v21           #roaming:Z
    .end local v23           #total:J
    .restart local v12       #index:I
    .restart local v22       #stats:Landroid/net/NetworkStats;
    :cond_246
    :try_start_246
    const-string v25, "ThrottleService"

    #@248
    new-instance v26, Ljava/lang/StringBuilder;

    #@24a
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@24d
    const-string v27, "unable to find stats for iface "

    #@24f
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v26

    #@253
    move-object/from16 v0, p0

    #@255
    iget-object v0, v0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@257
    move-object/from16 v27, v0

    #@259
    invoke-static/range {v27 .. v27}, Lcom/android/server/ThrottleService;->access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;

    #@25c
    move-result-object v27

    #@25d
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@260
    move-result-object v26

    #@261
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@264
    move-result-object v26

    #@265
    invoke-static/range {v25 .. v26}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_268
    .catch Ljava/lang/IllegalStateException; {:try_start_246 .. :try_end_268} :catch_26a
    .catch Landroid/os/RemoteException; {:try_start_246 .. :try_end_268} :catch_287

    #@268
    goto/16 :goto_b0

    #@26a
    .line 537
    .end local v12           #index:I
    .end local v22           #stats:Landroid/net/NetworkStats;
    :catch_26a
    move-exception v6

    #@26b
    .line 538
    .local v6, e:Ljava/lang/IllegalStateException;
    const-string v25, "ThrottleService"

    #@26d
    new-instance v26, Ljava/lang/StringBuilder;

    #@26f
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@272
    const-string v27, "problem during onPollAlarm: "

    #@274
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@277
    move-result-object v26

    #@278
    move-object/from16 v0, v26

    #@27a
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v26

    #@27e
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@281
    move-result-object v26

    #@282
    invoke-static/range {v25 .. v26}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@285
    goto/16 :goto_ea

    #@287
    .line 539
    .end local v6           #e:Ljava/lang/IllegalStateException;
    :catch_287
    move-exception v6

    #@288
    .line 540
    .local v6, e:Landroid/os/RemoteException;
    const-string v25, "ThrottleService"

    #@28a
    new-instance v26, Ljava/lang/StringBuilder;

    #@28c
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@28f
    const-string v27, "problem during onPollAlarm: "

    #@291
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v26

    #@295
    move-object/from16 v0, v26

    #@297
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29a
    move-result-object v26

    #@29b
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29e
    move-result-object v26

    #@29f
    invoke-static/range {v25 .. v26}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a2
    goto/16 :goto_ea
.end method

.method private onRebootRecovery()V
    .registers 5

    #@0
    .prologue
    .line 419
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    move-result-object v0

    #@6
    const/4 v1, -0x1

    #@7
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@a
    .line 421
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@c
    new-instance v1, Lcom/android/server/ThrottleService$DataRecorder;

    #@e
    iget-object v2, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@10
    invoke-static {v2}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@16
    invoke-direct {v1, v2, v3}, Lcom/android/server/ThrottleService$DataRecorder;-><init>(Landroid/content/Context;Lcom/android/server/ThrottleService;)V

    #@19
    invoke-static {v0, v1}, Lcom/android/server/ThrottleService;->access$102(Lcom/android/server/ThrottleService;Lcom/android/server/ThrottleService$DataRecorder;)Lcom/android/server/ThrottleService$DataRecorder;

    #@1c
    .line 424
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@1e
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$300(Lcom/android/server/ThrottleService;)Landroid/os/Handler;

    #@21
    move-result-object v0

    #@22
    const/4 v1, 0x1

    #@23
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@2a
    .line 429
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2c
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$300(Lcom/android/server/ThrottleService;)Landroid/os/Handler;

    #@2f
    move-result-object v0

    #@30
    iget-object v1, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@32
    invoke-static {v1}, Lcom/android/server/ThrottleService;->access$300(Lcom/android/server/ThrottleService;)Landroid/os/Handler;

    #@35
    move-result-object v1

    #@36
    const/4 v2, 0x2

    #@37
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3a
    move-result-object v1

    #@3b
    const-wide/32 v2, 0x15f90

    #@3e
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@41
    .line 431
    return-void
.end method

.method private onResetAlarm()V
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 756
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@3
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;

    #@6
    move-result-object v6

    #@7
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@a
    move-result-wide v6

    #@b
    const-wide/16 v8, 0x0

    #@d
    cmp-long v6, v6, v8

    #@f
    if-eqz v6, :cond_4d

    #@11
    .line 757
    const-string v6, "ThrottleService"

    #@13
    new-instance v7, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v8, "onResetAlarm - last period had "

    #@1a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    iget-object v8, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@20
    invoke-static {v8}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@23
    move-result-object v8

    #@24
    invoke-virtual {v8, v10}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodRx(I)J

    #@27
    move-result-wide v8

    #@28
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v7

    #@2c
    const-string v8, " bytes read and "

    #@2e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v7

    #@32
    iget-object v8, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@34
    invoke-static {v8}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@37
    move-result-object v8

    #@38
    invoke-virtual {v8, v10}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodTx(I)J

    #@3b
    move-result-wide v8

    #@3c
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v7

    #@40
    const-string v8, " written"

    #@42
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v7

    #@4a
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 762
    :cond_4d
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@4f
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;

    #@52
    move-result-object v6

    #@53
    invoke-interface {v6}, Landroid/util/TrustedTime;->getCacheAge()J

    #@56
    move-result-wide v6

    #@57
    iget-object v8, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@59
    invoke-static {v8}, Lcom/android/server/ThrottleService;->access$1000(Lcom/android/server/ThrottleService;)J

    #@5c
    move-result-wide v8

    #@5d
    cmp-long v6, v6, v8

    #@5f
    if-lez v6, :cond_6a

    #@61
    .line 763
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@63
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;

    #@66
    move-result-object v6

    #@67
    invoke-interface {v6}, Landroid/util/TrustedTime;->forceRefresh()Z

    #@6a
    .line 768
    :cond_6a
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@6c
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;

    #@6f
    move-result-object v6

    #@70
    invoke-interface {v6}, Landroid/util/TrustedTime;->hasCache()Z

    #@73
    move-result v6

    #@74
    if-eqz v6, :cond_c1

    #@76
    .line 769
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@78
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;

    #@7b
    move-result-object v6

    #@7c
    invoke-interface {v6}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@7f
    move-result-wide v1

    #@80
    .line 770
    .local v1, now:J
    invoke-direct {p0, v1, v2}, Lcom/android/server/ThrottleService$MyHandler;->calculatePeriodEnd(J)Ljava/util/Calendar;

    #@83
    move-result-object v0

    #@84
    .line 771
    .local v0, end:Ljava/util/Calendar;
    invoke-direct {p0, v0}, Lcom/android/server/ThrottleService$MyHandler;->calculatePeriodStart(Ljava/util/Calendar;)Ljava/util/Calendar;

    #@87
    move-result-object v5

    #@88
    .line 773
    .local v5, start:Ljava/util/Calendar;
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@8a
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;

    #@8d
    move-result-object v6

    #@8e
    invoke-virtual {v6, v5, v0}, Lcom/android/server/ThrottleService$DataRecorder;->setNextPeriod(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    #@91
    move-result v6

    #@92
    if-eqz v6, :cond_97

    #@94
    .line 774
    invoke-direct {p0}, Lcom/android/server/ThrottleService$MyHandler;->onPollAlarm()V

    #@97
    .line 777
    :cond_97
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@99
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$1700(Lcom/android/server/ThrottleService;)Landroid/app/AlarmManager;

    #@9c
    move-result-object v6

    #@9d
    iget-object v7, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@9f
    invoke-static {v7}, Lcom/android/server/ThrottleService;->access$2100(Lcom/android/server/ThrottleService;)Landroid/app/PendingIntent;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v6, v7}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@a6
    .line 778
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@a9
    move-result-wide v6

    #@aa
    sub-long v3, v6, v1

    #@ac
    .line 780
    .local v3, offset:J
    iget-object v6, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@ae
    invoke-static {v6}, Lcom/android/server/ThrottleService;->access$1700(Lcom/android/server/ThrottleService;)Landroid/app/AlarmManager;

    #@b1
    move-result-object v6

    #@b2
    const/4 v7, 0x3

    #@b3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b6
    move-result-wide v8

    #@b7
    add-long/2addr v8, v3

    #@b8
    iget-object v10, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@ba
    invoke-static {v10}, Lcom/android/server/ThrottleService;->access$2100(Lcom/android/server/ThrottleService;)Landroid/app/PendingIntent;

    #@bd
    move-result-object v10

    #@be
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@c1
    .line 786
    .end local v0           #end:Ljava/util/Calendar;
    .end local v1           #now:J
    .end local v3           #offset:J
    .end local v5           #start:Ljava/util/Calendar;
    :cond_c1
    return-void
.end method

.method private postNotification(IIII)V
    .registers 15
    .parameter "titleInt"
    .parameter "messageInt"
    .parameter "icon"
    .parameter "flags"

    #@0
    .prologue
    .line 668
    new-instance v2, Landroid/content/Intent;

    #@2
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@5
    .line 670
    .local v2, intent:Landroid/content/Intent;
    const-string v0, "com.android.phone"

    #@7
    const-string v1, "com.android.phone.DataUsage"

    #@9
    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 671
    const/high16 v0, 0x4000

    #@e
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@11
    .line 673
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@13
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@16
    move-result-object v0

    #@17
    const/4 v1, 0x0

    #@18
    const/4 v3, 0x0

    #@19
    const/4 v4, 0x0

    #@1a
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@1c
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@1f
    move-result-object v7

    #@20
    .line 676
    .local v7, pi:Landroid/app/PendingIntent;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@23
    move-result-object v8

    #@24
    .line 677
    .local v8, r:Landroid/content/res/Resources;
    invoke-virtual {v8, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@27
    move-result-object v9

    #@28
    .line 678
    .local v9, title:Ljava/lang/CharSequence;
    invoke-virtual {v8, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@2b
    move-result-object v6

    #@2c
    .line 679
    .local v6, message:Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@2e
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@31
    move-result-object v0

    #@32
    if-nez v0, :cond_5c

    #@34
    .line 680
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@36
    new-instance v1, Landroid/app/Notification;

    #@38
    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    #@3b
    invoke-static {v0, v1}, Lcom/android/server/ThrottleService;->access$2002(Lcom/android/server/ThrottleService;Landroid/app/Notification;)Landroid/app/Notification;

    #@3e
    .line 681
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@40
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@43
    move-result-object v0

    #@44
    const-wide/16 v3, 0x0

    #@46
    iput-wide v3, v0, Landroid/app/Notification;->when:J

    #@48
    .line 683
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@4a
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@4d
    move-result-object v0

    #@4e
    iput p3, v0, Landroid/app/Notification;->icon:I

    #@50
    .line 684
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@52
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@55
    move-result-object v0

    #@56
    iget v1, v0, Landroid/app/Notification;->defaults:I

    #@58
    and-int/lit8 v1, v1, -0x2

    #@5a
    iput v1, v0, Landroid/app/Notification;->defaults:I

    #@5c
    .line 686
    :cond_5c
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@5e
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@61
    move-result-object v0

    #@62
    iput p4, v0, Landroid/app/Notification;->flags:I

    #@64
    .line 687
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@66
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@69
    move-result-object v0

    #@6a
    iput-object v9, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@6c
    .line 688
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@6e
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@71
    move-result-object v0

    #@72
    iget-object v1, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@74
    invoke-static {v1}, Lcom/android/server/ThrottleService;->access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v0, v1, v9, v6, v7}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@7b
    .line 690
    iget-object v0, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@7d
    invoke-static {v0}, Lcom/android/server/ThrottleService;->access$1800(Lcom/android/server/ThrottleService;)Landroid/app/NotificationManager;

    #@80
    move-result-object v0

    #@81
    const/4 v1, 0x0

    #@82
    iget-object v3, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@84
    invoke-static {v3}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@87
    move-result-object v3

    #@88
    iget v3, v3, Landroid/app/Notification;->icon:I

    #@8a
    iget-object v4, p0, Lcom/android/server/ThrottleService$MyHandler;->this$0:Lcom/android/server/ThrottleService;

    #@8c
    invoke-static {v4}, Lcom/android/server/ThrottleService;->access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;

    #@8f
    move-result-object v4

    #@90
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@92
    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@95
    .line 692
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 396
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_1a

    #@5
    .line 412
    :goto_5
    return-void

    #@6
    .line 398
    :pswitch_6
    invoke-direct {p0}, Lcom/android/server/ThrottleService$MyHandler;->onRebootRecovery()V

    #@9
    goto :goto_5

    #@a
    .line 401
    :pswitch_a
    invoke-direct {p0}, Lcom/android/server/ThrottleService$MyHandler;->onPolicyChanged()V

    #@d
    goto :goto_5

    #@e
    .line 404
    :pswitch_e
    invoke-direct {p0}, Lcom/android/server/ThrottleService$MyHandler;->onPollAlarm()V

    #@11
    goto :goto_5

    #@12
    .line 407
    :pswitch_12
    invoke-direct {p0}, Lcom/android/server/ThrottleService$MyHandler;->onResetAlarm()V

    #@15
    goto :goto_5

    #@16
    .line 410
    :pswitch_16
    invoke-direct {p0}, Lcom/android/server/ThrottleService$MyHandler;->onIfaceUp()V

    #@19
    goto :goto_5

    #@1a
    .line 396
    :pswitch_data_1a
    .packed-switch 0x0
        :pswitch_6
        :pswitch_a
        :pswitch_e
        :pswitch_12
        :pswitch_16
    .end packed-switch
.end method
