.class public Lcom/android/server/NotificationManagerService;
.super Landroid/app/INotificationManager$Stub;
.source "NotificationManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/NotificationManagerService$WorkerHandler;,
        Lcom/android/server/NotificationManagerService$SettingsObserver;,
        Lcom/android/server/NotificationManagerService$ToastRecord;,
        Lcom/android/server/NotificationManagerService$NotificationRecord;
    }
.end annotation


# static fields
.field private static ACTION_ADD_RECORD:I = 0x0

.field private static ACTION_CLEAR_RECORD:I = 0x0

.field private static final ACTION_EXCESS_SPC_FAIL_EVENT:Ljava/lang/String; = "com.lge.intent.action.EXCESS_SPC_FAIL_EVENT"

.field private static ACTION_NOTHING:I = 0x0

.field private static final ACTION_NOTIFICATION_CLEAR:Ljava/lang/String; = "lge.intent.action.REMOVE_NOTI"

.field private static ACTION_REMOVE_RECORD:I = 0x0

.field private static final ATTR_NAME:Ljava/lang/String; = "name"

.field private static final ATTR_VERSION:Ljava/lang/String; = "version"

.field private static final DBG:Z = false

.field private static final DB_VERSION:I = 0x1

.field private static final DEFAULT_STREAM_TYPE:I = 0x5

.field private static final DEFAULT_VIBRATE_PATTERN:[J = null

.field private static final ENABLE_BLOCKED_NOTIFICATIONS:Z = true

.field private static final ENABLE_BLOCKED_TOASTS:Z = true

.field private static final JUNK_SCORE:I = -0x3e8

.field private static final LONG_DELAY:I = 0xdac

.field private static final MAX_PACKAGE_NOTIFICATIONS:I = 0x32

.field private static final MESSAGE_EXCESS_SPC_FAIL_POWER_OFF:I = 0x7

.field private static final MESSAGE_TIMEOUT:I = 0x2

.field private static final NOTIFICATION_PRIORITY_MULTIPLIER:I = 0xa

.field private static final SCORE_DISPLAY_THRESHOLD:I = -0x14

.field private static final SCORE_INTERRUPTION_THRESHOLD:I = -0xa

.field private static final SCORE_ONGOING_HIGHER:Z = false

.field private static final SHORT_DELAY:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "NotificationService"

.field private static final TAG_BLOCKED_PKGS:Ljava/lang/String; = "blocked-packages"

.field private static final TAG_BODY:Ljava/lang/String; = "notification-policy"

.field private static final TAG_PACKAGE:Ljava/lang/String; = "package"

.field private static final VIBRATE_PATTERN_MAXLEN:I = 0x11

.field private static dialog:Landroid/app/AlertDialog;

.field private static mOSPAsInterfaceMethod:Ljava/lang/reflect/Method;

.field private static mOSPIsConnectedMethod:Ljava/lang/reflect/Method;

.field private static mOSPNotifyMethod:Ljava/lang/reflect/Method;


# instance fields
.field final mAm:Landroid/app/IActivityManager;

.field private mAttentionLight:Lcom/android/server/LightsService$Light;

.field private mAudioService:Landroid/media/IAudioService;

.field private mBlockedPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBootCompleted:Z

.field final mContext:Landroid/content/Context;

.field private mDefaultNotificationColor:I

.field private mDefaultNotificationLedOff:I

.field private mDefaultNotificationLedOn:I

.field private mDefaultVibrationPattern:[J

.field private mDisabledNotifications:I

.field private mFallbackVibrationPattern:[J

.field final mForegroundToken:Landroid/os/IBinder;

.field private mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

.field private mIOSPService:Ljava/lang/Object;

.field private mInCall:Z

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

.field private mLedOnEvenWhenLcdOn:Z

.field private mLights:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/NotificationManagerService$NotificationRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

.field private mNotificationExceptionalPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationLight:Lcom/android/server/LightsService$Light;

.field private final mNotificationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/NotificationManagerService$NotificationRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationPulseEnabled:Z

.field private mOSPConnection:Landroid/content/ServiceConnection;

.field private mOSPSkipList:[Ljava/lang/CharSequence;

.field private mOperator:Ljava/lang/String;

.field private mPolicyFile:Landroid/util/AtomicFile;

.field private mScreenOn:Z

.field private mSoundNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

.field private mStatusBar:Lcom/android/server/StatusBarManagerService;

.field private mSystemReady:Z

.field private mToastQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/NotificationManagerService$ToastRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mVibrateNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

.field mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;


# direct methods
.method static constructor <clinit>()V
    .registers 12

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    const/4 v8, 0x0

    #@4
    .line 129
    new-array v7, v11, [J

    #@6
    fill-array-data v7, :array_84

    #@9
    sput-object v7, Lcom/android/server/NotificationManagerService;->DEFAULT_VIBRATE_PATTERN:[J

    #@b
    .line 138
    sput-object v8, Lcom/android/server/NotificationManagerService;->dialog:Landroid/app/AlertDialog;

    #@d
    .line 213
    sput v9, Lcom/android/server/NotificationManagerService;->ACTION_NOTHING:I

    #@f
    .line 214
    sput v10, Lcom/android/server/NotificationManagerService;->ACTION_CLEAR_RECORD:I

    #@11
    .line 215
    sput v11, Lcom/android/server/NotificationManagerService;->ACTION_REMOVE_RECORD:I

    #@13
    .line 216
    const/4 v7, 0x3

    #@14
    sput v7, Lcom/android/server/NotificationManagerService;->ACTION_ADD_RECORD:I

    #@16
    .line 1938
    sput-object v8, Lcom/android/server/NotificationManagerService;->mOSPIsConnectedMethod:Ljava/lang/reflect/Method;

    #@18
    .line 1939
    sput-object v8, Lcom/android/server/NotificationManagerService;->mOSPNotifyMethod:Ljava/lang/reflect/Method;

    #@1a
    .line 1940
    sput-object v8, Lcom/android/server/NotificationManagerService;->mOSPAsInterfaceMethod:Ljava/lang/reflect/Method;

    #@1c
    .line 1947
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@1e
    if-eqz v7, :cond_7c

    #@20
    .line 1949
    :try_start_20
    const-string v7, "com.lge.osp.IOSPService"

    #@22
    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@25
    move-result-object v5

    #@26
    .line 1950
    .local v5, ospInterface:Ljava/lang/Class;
    const/4 v6, 0x0

    #@27
    .line 1951
    .local v6, stubClass:Ljava/lang/Class;
    invoke-virtual {v5}, Ljava/lang/Class;->getDeclaredClasses()[Ljava/lang/Class;

    #@2a
    move-result-object v0

    #@2b
    .local v0, arr$:[Ljava/lang/Class;
    array-length v4, v0

    #@2c
    .local v4, len$:I
    const/4 v3, 0x0

    #@2d
    .local v3, i$:I
    :goto_2d
    if-ge v3, v4, :cond_3e

    #@2f
    aget-object v2, v0, v3

    #@31
    .line 1952
    .local v2, cls:Ljava/lang/Class;
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    const-string v8, "Stub"

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v7

    #@3b
    if-eqz v7, :cond_7d

    #@3d
    .line 1953
    move-object v6, v2

    #@3e
    .line 1958
    .end local v2           #cls:Ljava/lang/Class;
    :cond_3e
    if-eqz v6, :cond_54

    #@40
    .line 1959
    const-string v7, "android.os.IBinder"

    #@42
    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@45
    move-result-object v1

    #@46
    .line 1960
    .local v1, binderClass:Ljava/lang/Class;
    const-string v7, "asInterface"

    #@48
    const/4 v8, 0x1

    #@49
    new-array v8, v8, [Ljava/lang/Class;

    #@4b
    const/4 v9, 0x0

    #@4c
    aput-object v1, v8, v9

    #@4e
    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@51
    move-result-object v7

    #@52
    sput-object v7, Lcom/android/server/NotificationManagerService;->mOSPAsInterfaceMethod:Ljava/lang/reflect/Method;

    #@54
    .line 1965
    .end local v1           #binderClass:Ljava/lang/Class;
    :cond_54
    sget-object v7, Lcom/android/server/NotificationManagerService;->mOSPIsConnectedMethod:Ljava/lang/reflect/Method;

    #@56
    if-nez v7, :cond_63

    #@58
    .line 1966
    const-string v8, "isConnected"

    #@5a
    const/4 v7, 0x0

    #@5b
    check-cast v7, [Ljava/lang/Class;

    #@5d
    invoke-virtual {v5, v8, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@60
    move-result-object v7

    #@61
    sput-object v7, Lcom/android/server/NotificationManagerService;->mOSPIsConnectedMethod:Ljava/lang/reflect/Method;

    #@63
    .line 1970
    :cond_63
    sget-object v7, Lcom/android/server/NotificationManagerService;->mOSPNotifyMethod:Ljava/lang/reflect/Method;

    #@65
    if-nez v7, :cond_7c

    #@67
    .line 1971
    const-string v7, "notify"

    #@69
    const/4 v8, 0x2

    #@6a
    new-array v8, v8, [Ljava/lang/Class;

    #@6c
    const/4 v9, 0x0

    #@6d
    const-class v10, Ljava/lang/CharSequence;

    #@6f
    aput-object v10, v8, v9

    #@71
    const/4 v9, 0x1

    #@72
    const-class v10, Ljava/lang/CharSequence;

    #@74
    aput-object v10, v8, v9

    #@76
    invoke-virtual {v5, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@79
    move-result-object v7

    #@7a
    sput-object v7, Lcom/android/server/NotificationManagerService;->mOSPNotifyMethod:Ljava/lang/reflect/Method;
    :try_end_7c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_20 .. :try_end_7c} :catch_82
    .catch Ljava/lang/NoSuchMethodException; {:try_start_20 .. :try_end_7c} :catch_80

    #@7c
    .line 1978
    .end local v0           #arr$:[Ljava/lang/Class;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_7c
    :goto_7c
    return-void

    #@7d
    .line 1951
    .restart local v0       #arr$:[Ljava/lang/Class;
    .restart local v2       #cls:Ljava/lang/Class;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_7d
    add-int/lit8 v3, v3, 0x1

    #@7f
    goto :goto_2d

    #@80
    .line 1975
    .end local v0           #arr$:[Ljava/lang/Class;
    .end local v2           #cls:Ljava/lang/Class;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :catch_80
    move-exception v7

    #@81
    goto :goto_7c

    #@82
    .line 1974
    :catch_82
    move-exception v7

    #@83
    goto :goto_7c

    #@84
    .line 129
    :array_84
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x20t 0x3t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/StatusBarManagerService;Lcom/android/server/LightsService;)V
    .registers 16
    .parameter "context"
    .parameter "statusBar"
    .parameter "lights"

    #@0
    .prologue
    .line 755
    invoke-direct {p0}, Landroid/app/INotificationManager$Stub;-><init>()V

    #@3
    .line 140
    const/4 v9, 0x0

    #@4
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mOperator:Ljava/lang/String;

    #@6
    .line 157
    new-instance v9, Landroid/os/Binder;

    #@8
    invoke-direct {v9}, Landroid/os/Binder;-><init>()V

    #@b
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mForegroundToken:Landroid/os/IBinder;

    #@d
    .line 184
    const/4 v9, 0x1

    #@e
    iput-boolean v9, p0, Lcom/android/server/NotificationManagerService;->mScreenOn:Z

    #@10
    .line 185
    const/4 v9, 0x0

    #@11
    iput-boolean v9, p0, Lcom/android/server/NotificationManagerService;->mInCall:Z

    #@13
    .line 189
    const/4 v9, 0x0

    #@14
    iput-boolean v9, p0, Lcom/android/server/NotificationManagerService;->mBootCompleted:Z

    #@16
    .line 192
    new-instance v9, Ljava/util/ArrayList;

    #@18
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@1d
    .line 197
    new-instance v9, Ljava/util/ArrayList;

    #@1f
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@22
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@24
    .line 202
    new-instance v9, Ljava/util/HashSet;

    #@26
    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    #@29
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@2b
    .line 220
    const/4 v9, 0x0

    #@2c
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@2e
    .line 221
    const/4 v9, 0x0

    #@2f
    iput-boolean v9, p0, Lcom/android/server/NotificationManagerService;->mLedOnEvenWhenLcdOn:Z

    #@31
    .line 478
    new-instance v9, Lcom/android/server/NotificationManagerService$1;

    #@33
    invoke-direct {v9, p0}, Lcom/android/server/NotificationManagerService$1;-><init>(Lcom/android/server/NotificationManagerService;)V

    #@36
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@38
    .line 590
    new-instance v9, Lcom/android/server/NotificationManagerService$2;

    #@3a
    invoke-direct {v9, p0}, Lcom/android/server/NotificationManagerService$2;-><init>(Lcom/android/server/NotificationManagerService;)V

    #@3d
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@3f
    .line 1937
    const/4 v9, 0x0

    #@40
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mIOSPService:Ljava/lang/Object;

    #@42
    .line 1942
    const/4 v9, 0x3

    #@43
    new-array v9, v9, [Ljava/lang/CharSequence;

    #@45
    const/4 v10, 0x0

    #@46
    const-string v11, "com.android.systemui"

    #@48
    aput-object v11, v9, v10

    #@4a
    const/4 v10, 0x1

    #@4b
    const-string v11, "android"

    #@4d
    aput-object v11, v9, v10

    #@4f
    const/4 v10, 0x2

    #@50
    const-string v11, "com.lge.osp"

    #@52
    aput-object v11, v9, v10

    #@54
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mOSPSkipList:[Ljava/lang/CharSequence;

    #@56
    .line 1980
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@58
    if-eqz v9, :cond_1b0

    #@5a
    new-instance v9, Lcom/android/server/NotificationManagerService$3;

    #@5c
    invoke-direct {v9, p0}, Lcom/android/server/NotificationManagerService$3;-><init>(Lcom/android/server/NotificationManagerService;)V

    #@5f
    :goto_5f
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mOSPConnection:Landroid/content/ServiceConnection;

    #@61
    .line 756
    iput-object p1, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@63
    .line 760
    :try_start_63
    sget-object v9, Lcom/lge/loader/RuntimeLibraryLoader;->VOLUME_MANAGER:Ljava/lang/String;

    #@65
    invoke-static {v9}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;

    #@68
    move-result-object v9

    #@69
    const/4 v10, 0x0

    #@6a
    invoke-virtual {v9, v10}, Lcom/lge/loader/InstanceCreator;->newInstance(Ljava/lang/Object;)Ljava/lang/Object;

    #@6d
    move-result-object v9

    #@6e
    check-cast v9, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@70
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;
    :try_end_72
    .catch Ljava/lang/NullPointerException; {:try_start_63 .. :try_end_72} :catch_1b3
    .catch Ljava/lang/Exception; {:try_start_63 .. :try_end_72} :catch_1b9

    #@72
    .line 770
    :goto_72
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@75
    move-result-object v9

    #@76
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mAm:Landroid/app/IActivityManager;

    #@78
    .line 771
    new-instance v9, Ljava/util/ArrayList;

    #@7a
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@7d
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@7f
    .line 772
    new-instance v9, Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@81
    const/4 v10, 0x0

    #@82
    invoke-direct {v9, p0, v10}, Lcom/android/server/NotificationManagerService$WorkerHandler;-><init>(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$1;)V

    #@85
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@87
    .line 774
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->loadBlockDb()V

    #@8a
    .line 776
    iput-object p2, p0, Lcom/android/server/NotificationManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@8c
    .line 777
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@8e
    invoke-virtual {p2, v9}, Lcom/android/server/StatusBarManagerService;->setNotificationCallbacks(Lcom/android/server/StatusBarManagerService$NotificationCallbacks;)V

    #@91
    .line 779
    const/4 v9, 0x4

    #@92
    invoke-virtual {p3, v9}, Lcom/android/server/LightsService;->getLight(I)Lcom/android/server/LightsService$Light;

    #@95
    move-result-object v9

    #@96
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@98
    .line 780
    const/4 v9, 0x5

    #@99
    invoke-virtual {p3, v9}, Lcom/android/server/LightsService;->getLight(I)Lcom/android/server/LightsService$Light;

    #@9c
    move-result-object v9

    #@9d
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mAttentionLight:Lcom/android/server/LightsService$Light;

    #@9f
    .line 782
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@a1
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a4
    move-result-object v5

    #@a5
    .line 783
    .local v5, resources:Landroid/content/res/Resources;
    const v9, 0x106006b

    #@a8
    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    #@ab
    move-result v9

    #@ac
    iput v9, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationColor:I

    #@ae
    .line 785
    const v9, 0x10e001d

    #@b1
    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getInteger(I)I

    #@b4
    move-result v9

    #@b5
    iput v9, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOn:I

    #@b7
    .line 787
    const v9, 0x10e001e

    #@ba
    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getInteger(I)I

    #@bd
    move-result v9

    #@be
    iput v9, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOff:I

    #@c0
    .line 790
    const v9, 0x107003a

    #@c3
    const/16 v10, 0x11

    #@c5
    sget-object v11, Lcom/android/server/NotificationManagerService;->DEFAULT_VIBRATE_PATTERN:[J

    #@c7
    invoke-static {v5, v9, v10, v11}, Lcom/android/server/NotificationManagerService;->getLongArray(Landroid/content/res/Resources;II[J)[J

    #@ca
    move-result-object v9

    #@cb
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mDefaultVibrationPattern:[J

    #@cd
    .line 795
    const v9, 0x107003b

    #@d0
    const/16 v10, 0x11

    #@d2
    sget-object v11, Lcom/android/server/NotificationManagerService;->DEFAULT_VIBRATE_PATTERN:[J

    #@d4
    invoke-static {v5, v9, v10, v11}, Lcom/android/server/NotificationManagerService;->getLongArray(Landroid/content/res/Resources;II[J)[J

    #@d7
    move-result-object v9

    #@d8
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mFallbackVibrationPattern:[J

    #@da
    .line 803
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@dc
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@df
    move-result-object v9

    #@e0
    const v10, 0x2080001

    #@e3
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@e6
    move-result-object v9

    #@e7
    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@ea
    move-result-object v9

    #@eb
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@ed
    .line 807
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@ef
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f2
    move-result-object v9

    #@f3
    const v10, 0x2060024

    #@f6
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@f9
    move-result v9

    #@fa
    iput-boolean v9, p0, Lcom/android/server/NotificationManagerService;->mLedOnEvenWhenLcdOn:Z

    #@fc
    .line 816
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@fe
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@101
    move-result-object v9

    #@102
    const-string v10, "device_provisioned"

    #@104
    const/4 v11, 0x0

    #@105
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@108
    move-result v9

    #@109
    if-nez v9, :cond_10f

    #@10b
    .line 818
    const/high16 v9, 0x4

    #@10d
    iput v9, p0, Lcom/android/server/NotificationManagerService;->mDisabledNotifications:I

    #@10f
    .line 822
    :cond_10f
    new-instance v2, Landroid/content/IntentFilter;

    #@111
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@114
    .line 823
    .local v2, filter:Landroid/content/IntentFilter;
    const-string v9, "android.intent.action.SCREEN_ON"

    #@116
    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@119
    .line 824
    const-string v9, "android.intent.action.SCREEN_OFF"

    #@11b
    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@11e
    .line 825
    const-string v9, "android.intent.action.PHONE_STATE"

    #@120
    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@123
    .line 826
    const-string v9, "android.intent.action.USER_PRESENT"

    #@125
    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@128
    .line 827
    const-string v9, "android.intent.action.USER_STOPPED"

    #@12a
    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@12d
    .line 829
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@12f
    if-eqz v9, :cond_136

    #@131
    .line 830
    const-string v9, "com.lge.osp.ALERT_CONNECTION"

    #@133
    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@136
    .line 833
    :cond_136
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@138
    iget-object v10, p0, Lcom/android/server/NotificationManagerService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@13a
    invoke-virtual {v9, v10, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@13d
    .line 834
    new-instance v4, Landroid/content/IntentFilter;

    #@13f
    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    #@142
    .line 835
    .local v4, pkgFilter:Landroid/content/IntentFilter;
    const-string v9, "android.intent.action.PACKAGE_REMOVED"

    #@144
    invoke-virtual {v4, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@147
    .line 836
    const-string v9, "android.intent.action.PACKAGE_CHANGED"

    #@149
    invoke-virtual {v4, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14c
    .line 837
    const-string v9, "android.intent.action.PACKAGE_RESTARTED"

    #@14e
    invoke-virtual {v4, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@151
    .line 838
    const-string v9, "android.intent.action.QUERY_PACKAGE_RESTART"

    #@153
    invoke-virtual {v4, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@156
    .line 839
    const-string v9, "package"

    #@158
    invoke-virtual {v4, v9}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@15b
    .line 840
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@15d
    iget-object v10, p0, Lcom/android/server/NotificationManagerService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@15f
    invoke-virtual {v9, v10, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@162
    .line 841
    new-instance v6, Landroid/content/IntentFilter;

    #@164
    const-string v9, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@166
    invoke-direct {v6, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@169
    .line 842
    .local v6, sdFilter:Landroid/content/IntentFilter;
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@16b
    iget-object v10, p0, Lcom/android/server/NotificationManagerService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@16d
    invoke-virtual {v9, v10, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@170
    .line 844
    new-instance v8, Landroid/content/IntentFilter;

    #@172
    const-string v9, "com.lge.media.STOP_NOTIFICATION"

    #@174
    invoke-direct {v8, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@177
    .line 845
    .local v8, stopNoti:Landroid/content/IntentFilter;
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@179
    iget-object v10, p0, Lcom/android/server/NotificationManagerService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@17b
    invoke-virtual {v9, v10, v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@17e
    .line 849
    new-instance v0, Landroid/content/IntentFilter;

    #@180
    const-string v9, "android.intent.action.BOOT_COMPLETED"

    #@182
    invoke-direct {v0, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@185
    .line 850
    .local v0, boot_complete:Landroid/content/IntentFilter;
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@187
    iget-object v10, p0, Lcom/android/server/NotificationManagerService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@189
    invoke-virtual {v9, v10, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@18c
    .line 853
    new-instance v3, Lcom/android/server/NotificationManagerService$SettingsObserver;

    #@18e
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@190
    invoke-direct {v3, p0, v9}, Lcom/android/server/NotificationManagerService$SettingsObserver;-><init>(Lcom/android/server/NotificationManagerService;Landroid/os/Handler;)V

    #@193
    .line 854
    .local v3, observer:Lcom/android/server/NotificationManagerService$SettingsObserver;
    invoke-virtual {v3}, Lcom/android/server/NotificationManagerService$SettingsObserver;->observe()V

    #@196
    .line 856
    const-string v9, "ro.build.target_operator"

    #@198
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@19b
    move-result-object v9

    #@19c
    iput-object v9, p0, Lcom/android/server/NotificationManagerService;->mOperator:Ljava/lang/String;

    #@19e
    .line 857
    new-instance v7, Landroid/content/IntentFilter;

    #@1a0
    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    #@1a3
    .line 858
    .local v7, spcIntentFilter:Landroid/content/IntentFilter;
    const-string v9, "com.lge.intent.action.EXCESS_SPC_FAIL_EVENT"

    #@1a5
    invoke-virtual {v7, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a8
    .line 859
    iget-object v9, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@1aa
    iget-object v10, p0, Lcom/android/server/NotificationManagerService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1ac
    invoke-virtual {v9, v10, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1af
    .line 861
    return-void

    #@1b0
    .line 1980
    .end local v0           #boot_complete:Landroid/content/IntentFilter;
    .end local v2           #filter:Landroid/content/IntentFilter;
    .end local v3           #observer:Lcom/android/server/NotificationManagerService$SettingsObserver;
    .end local v4           #pkgFilter:Landroid/content/IntentFilter;
    .end local v5           #resources:Landroid/content/res/Resources;
    .end local v6           #sdFilter:Landroid/content/IntentFilter;
    .end local v7           #spcIntentFilter:Landroid/content/IntentFilter;
    .end local v8           #stopNoti:Landroid/content/IntentFilter;
    :cond_1b0
    const/4 v9, 0x0

    #@1b1
    goto/16 :goto_5f

    #@1b3
    .line 764
    :catch_1b3
    move-exception v1

    #@1b4
    .line 765
    .local v1, e:Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@1b7
    goto/16 :goto_72

    #@1b9
    .line 766
    .end local v1           #e:Ljava/lang/NullPointerException;
    :catch_1b9
    move-exception v1

    #@1ba
    .line 767
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@1bd
    goto/16 :goto_72
.end method

.method static synthetic access$000(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 115
    invoke-static {p0, p1, p2}, Lcom/android/server/NotificationManagerService;->idDebugString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/server/NotificationManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 115
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1202(Lcom/android/server/NotificationManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/android/server/NotificationManagerService;->mScreenOn:Z

    #@2
    return p1
.end method

.method static synthetic access$1302(Lcom/android/server/NotificationManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/android/server/NotificationManagerService;->mInCall:Z

    #@2
    return p1
.end method

.method static synthetic access$1400(Lcom/android/server/NotificationManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 115
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->updateNotificationPulse()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/NotificationManagerService;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/NotificationManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/android/server/NotificationManagerService;->mLedOnEvenWhenLcdOn:Z

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/android/server/NotificationManagerService;)Lcom/android/server/LightsService$Light;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/server/NotificationManagerService;)Landroid/content/ServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mOSPConnection:Landroid/content/ServiceConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/server/NotificationManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mIOSPService:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1902(Lcom/android/server/NotificationManagerService;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/server/NotificationManagerService;->mIOSPService:Ljava/lang/Object;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/server/NotificationManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget v0, p0, Lcom/android/server/NotificationManagerService;->mDisabledNotifications:I

    #@2
    return v0
.end method

.method static synthetic access$2000(Lcom/android/server/NotificationManagerService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mOperator:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/android/server/NotificationManagerService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput p1, p0, Lcom/android/server/NotificationManagerService;->mDisabledNotifications:I

    #@2
    return p1
.end method

.method static synthetic access$2100(Lcom/android/server/NotificationManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 115
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->spcErrorNotification()V

    #@3
    return-void
.end method

.method static synthetic access$2202(Lcom/android/server/NotificationManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/android/server/NotificationManagerService;->mBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$2300(Lcom/android/server/NotificationManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/android/server/NotificationManagerService;->mNotificationPulseEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$2302(Lcom/android/server/NotificationManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/android/server/NotificationManagerService;->mNotificationPulseEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$2500(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$ToastRecord;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/android/server/NotificationManagerService;->handleTimeout(Lcom/android/server/NotificationManagerService$ToastRecord;)V

    #@3
    return-void
.end method

.method static synthetic access$2600()Landroid/app/AlertDialog;
    .registers 1

    #@0
    .prologue
    .line 115
    sget-object v0, Lcom/android/server/NotificationManagerService;->dialog:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$2700()Ljava/lang/reflect/Method;
    .registers 1

    #@0
    .prologue
    .line 115
    sget-object v0, Lcom/android/server/NotificationManagerService;->mOSPAsInterfaceMethod:Ljava/lang/reflect/Method;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/NotificationManagerService;)Landroid/media/IAudioService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mAudioService:Landroid/media/IAudioService;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/NotificationManagerService;Ljava/lang/String;Ljava/lang/String;IIIZI)V
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"

    #@0
    .prologue
    .line 115
    invoke-direct/range {p0 .. p7}, Lcom/android/server/NotificationManagerService;->cancelNotification(Ljava/lang/String;Ljava/lang/String;IIIZI)V

    #@3
    return-void
.end method

.method static synthetic access$502(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/server/NotificationManagerService;->mSoundNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@2
    return-object p1
.end method

.method static synthetic access$602(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/server/NotificationManagerService;->mVibrateNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$800()I
    .registers 1

    #@0
    .prologue
    .line 115
    sget v0, Lcom/android/server/NotificationManagerService;->ACTION_CLEAR_RECORD:I

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lcom/android/server/NotificationManagerService;->updateLightListLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;I)V

    #@3
    return-void
.end method

.method private areNotificationsEnabledForPackageInt(Ljava/lang/String;)Z
    .registers 4
    .parameter "pkg"

    #@0
    .prologue
    .line 314
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    .line 318
    .local v0, enabled:Z
    :goto_9
    return v0

    #@a
    .line 314
    .end local v0           #enabled:Z
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private cancelNotification(Ljava/lang/String;Ljava/lang/String;IIIZI)V
    .registers 14
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"
    .parameter "mustHaveFlags"
    .parameter "mustNotHaveFlags"
    .parameter "sendDelete"
    .parameter "userId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1537
    const/16 v2, 0xabf

    #@3
    const/4 v3, 0x6

    #@4
    new-array v3, v3, [Ljava/lang/Object;

    #@6
    aput-object p1, v3, v4

    #@8
    const/4 v4, 0x1

    #@9
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v5

    #@d
    aput-object v5, v3, v4

    #@f
    const/4 v4, 0x2

    #@10
    aput-object p2, v3, v4

    #@12
    const/4 v4, 0x3

    #@13
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v5

    #@17
    aput-object v5, v3, v4

    #@19
    const/4 v4, 0x4

    #@1a
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v5

    #@1e
    aput-object v5, v3, v4

    #@20
    const/4 v4, 0x5

    #@21
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v5

    #@25
    aput-object v5, v3, v4

    #@27
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2a
    .line 1540
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@2c
    monitor-enter v3

    #@2d
    .line 1541
    :try_start_2d
    invoke-direct {p0, p1, p2, p3, p7}, Lcom/android/server/NotificationManagerService;->indexOfNotificationLocked(Ljava/lang/String;Ljava/lang/String;II)I

    #@30
    move-result v0

    #@31
    .line 1542
    .local v0, index:I
    if-ltz v0, :cond_66

    #@33
    .line 1543
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v1

    #@39
    check-cast v1, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@3b
    .line 1545
    .local v1, r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    iget-object v2, v1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@3d
    iget v2, v2, Landroid/app/Notification;->flags:I

    #@3f
    and-int/2addr v2, p4

    #@40
    if-eq v2, p4, :cond_44

    #@42
    .line 1546
    monitor-exit v3

    #@43
    .line 1560
    .end local v1           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :goto_43
    return-void

    #@44
    .line 1548
    .restart local v1       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_44
    iget-object v2, v1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@46
    iget v2, v2, Landroid/app/Notification;->flags:I

    #@48
    and-int/2addr v2, p5

    #@49
    if-eqz v2, :cond_50

    #@4b
    .line 1549
    monitor-exit v3

    #@4c
    goto :goto_43

    #@4d
    .line 1559
    .end local v0           #index:I
    .end local v1           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :catchall_4d
    move-exception v2

    #@4e
    monitor-exit v3
    :try_end_4f
    .catchall {:try_start_2d .. :try_end_4f} :catchall_4d

    #@4f
    throw v2

    #@50
    .line 1552
    .restart local v0       #index:I
    .restart local v1       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_50
    :try_start_50
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@55
    .line 1553
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/NotificationManagerService;->sendBroadcastCancelNotification(Ljava/lang/String;Ljava/lang/String;I)V

    #@58
    .line 1554
    iget-object v2, v1, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@5a
    iget-object v4, v1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@5c
    const/4 v5, 0x0

    #@5d
    invoke-direct {p0, v2, v4, v5}, Lcom/android/server/NotificationManagerService;->sendNotificationToTMode(Ljava/lang/String;Landroid/app/Notification;Z)V

    #@60
    .line 1556
    invoke-direct {p0, v1, p6}, Lcom/android/server/NotificationManagerService;->cancelNotificationLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;Z)V

    #@63
    .line 1557
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V

    #@66
    .line 1559
    .end local v1           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_66
    monitor-exit v3
    :try_end_67
    .catchall {:try_start_50 .. :try_end_67} :catchall_4d

    #@67
    goto :goto_43
.end method

.method private cancelNotificationLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;Z)V
    .registers 11
    .parameter "r"
    .parameter "sendDelete"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1465
    if-eqz p2, :cond_10

    #@3
    .line 1466
    iget-object v4, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@5
    iget-object v4, v4, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@7
    if-eqz v4, :cond_10

    #@9
    .line 1468
    :try_start_9
    iget-object v4, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@b
    iget-object v4, v4, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@d
    invoke-virtual {v4}, Landroid/app/PendingIntent;->send()V
    :try_end_10
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_9 .. :try_end_10} :catch_61

    #@10
    .line 1478
    :cond_10
    :goto_10
    iget-object v4, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@12
    iget v4, v4, Landroid/app/Notification;->icon:I

    #@14
    if-eqz v4, :cond_26

    #@16
    .line 1479
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@19
    move-result-wide v1

    #@1a
    .line 1481
    .local v1, identity:J
    :try_start_1a
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@1c
    iget-object v5, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@1e
    invoke-virtual {v4, v5}, Lcom/android/server/StatusBarManagerService;->removeNotification(Landroid/os/IBinder;)V
    :try_end_21
    .catchall {:try_start_1a .. :try_end_21} :catchall_7d

    #@21
    .line 1484
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@24
    .line 1486
    iput-object v7, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@26
    .line 1490
    .end local v1           #identity:J
    :cond_26
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mSoundNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@28
    if-ne v4, p1, :cond_3e

    #@2a
    .line 1491
    iput-object v7, p0, Lcom/android/server/NotificationManagerService;->mSoundNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@2c
    .line 1492
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@2f
    move-result-wide v1

    #@30
    .line 1494
    .restart local v1       #identity:J
    :try_start_30
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mAudioService:Landroid/media/IAudioService;

    #@32
    invoke-interface {v4}, Landroid/media/IAudioService;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@35
    move-result-object v3

    #@36
    .line 1495
    .local v3, player:Landroid/media/IRingtonePlayer;
    if-eqz v3, :cond_3b

    #@38
    .line 1496
    invoke-interface {v3}, Landroid/media/IRingtonePlayer;->stopAsync()V
    :try_end_3b
    .catchall {:try_start_30 .. :try_end_3b} :catchall_82
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_3b} :catch_8c

    #@3b
    .line 1500
    .end local v3           #player:Landroid/media/IRingtonePlayer;
    :cond_3b
    :goto_3b
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3e
    .line 1505
    .end local v1           #identity:J
    :cond_3e
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mVibrateNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@40
    if-ne v4, p1, :cond_50

    #@42
    .line 1506
    iput-object v7, p0, Lcom/android/server/NotificationManagerService;->mVibrateNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@44
    .line 1507
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@47
    move-result-wide v1

    #@48
    .line 1511
    .restart local v1       #identity:J
    :try_start_48
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@4a
    invoke-interface {v4}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->cancel()V
    :try_end_4d
    .catchall {:try_start_48 .. :try_end_4d} :catchall_87

    #@4d
    .line 1515
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@50
    .line 1520
    .end local v1           #identity:J
    :cond_50
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@55
    .line 1523
    sget v4, Lcom/android/server/NotificationManagerService;->ACTION_REMOVE_RECORD:I

    #@57
    invoke-direct {p0, p1, v4}, Lcom/android/server/NotificationManagerService;->updateLightListLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;I)V

    #@5a
    .line 1526
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@5c
    if-ne v4, p1, :cond_60

    #@5e
    .line 1527
    iput-object v7, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@60
    .line 1529
    :cond_60
    return-void

    #@61
    .line 1469
    :catch_61
    move-exception v0

    #@62
    .line 1472
    .local v0, ex:Landroid/app/PendingIntent$CanceledException;
    const-string v4, "NotificationService"

    #@64
    new-instance v5, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v6, "canceled PendingIntent for "

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    iget-object v6, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7c
    goto :goto_10

    #@7d
    .line 1484
    .end local v0           #ex:Landroid/app/PendingIntent$CanceledException;
    .restart local v1       #identity:J
    :catchall_7d
    move-exception v4

    #@7e
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@81
    throw v4

    #@82
    .line 1500
    :catchall_82
    move-exception v4

    #@83
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@86
    throw v4

    #@87
    .line 1515
    :catchall_87
    move-exception v4

    #@88
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@8b
    throw v4

    #@8c
    .line 1498
    :catch_8c
    move-exception v4

    #@8d
    goto :goto_3b
.end method

.method private cancelToastLocked(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 987
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@8
    .line 989
    .local v1, record:Lcom/android/server/NotificationManagerService$ToastRecord;
    :try_start_8
    iget-object v2, v1, Lcom/android/server/NotificationManagerService$ToastRecord;->callback:Landroid/app/ITransientNotification;

    #@a
    invoke-interface {v2}, Landroid/app/ITransientNotification;->hide()V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_d} :catch_23

    #@d
    .line 996
    :goto_d
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@12
    .line 997
    iget v2, v1, Lcom/android/server/NotificationManagerService$ToastRecord;->pid:I

    #@14
    invoke-direct {p0, v2}, Lcom/android/server/NotificationManagerService;->keepProcessAliveLocked(I)V

    #@17
    .line 998
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v2

    #@1d
    if-lez v2, :cond_22

    #@1f
    .line 1002
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->showNextToastLocked()V

    #@22
    .line 1004
    :cond_22
    return-void

    #@23
    .line 990
    :catch_23
    move-exception v0

    #@24
    .line 991
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "NotificationService"

    #@26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "Object died trying to hide notification "

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    iget-object v4, v1, Lcom/android/server/NotificationManagerService$ToastRecord;->callback:Landroid/app/ITransientNotification;

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, " in package "

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    iget-object v4, v1, Lcom/android/server/NotificationManagerService$ToastRecord;->pkg:Ljava/lang/String;

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_d
.end method

.method private static final clamp(III)I
    .registers 3
    .parameter "x"
    .parameter "low"
    .parameter "high"

    #@0
    .prologue
    .line 1093
    if-ge p0, p1, :cond_3

    #@2
    .end local p1
    :goto_2
    return p1

    #@3
    .restart local p1
    :cond_3
    if-le p0, p2, :cond_7

    #@5
    move p1, p2

    #@6
    goto :goto_2

    #@7
    :cond_7
    move p1, p0

    #@8
    goto :goto_2
.end method

.method static getLongArray(Landroid/content/res/Resources;II[J)[J
    .registers 10
    .parameter "r"
    .parameter "resid"
    .parameter "maxlen"
    .parameter "def"

    #@0
    .prologue
    .line 740
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@3
    move-result-object v0

    #@4
    .line 741
    .local v0, ar:[I
    if-nez v0, :cond_7

    #@6
    .line 749
    .end local p3
    :goto_6
    return-object p3

    #@7
    .line 744
    .restart local p3
    :cond_7
    array-length v4, v0

    #@8
    if-le v4, p2, :cond_18

    #@a
    move v2, p2

    #@b
    .line 745
    .local v2, len:I
    :goto_b
    new-array v3, v2, [J

    #@d
    .line 746
    .local v3, out:[J
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v2, :cond_1a

    #@10
    .line 747
    aget v4, v0, v1

    #@12
    int-to-long v4, v4

    #@13
    aput-wide v4, v3, v1

    #@15
    .line 746
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_e

    #@18
    .line 744
    .end local v1           #i:I
    .end local v2           #len:I
    .end local v3           #out:[J
    :cond_18
    array-length v2, v0

    #@19
    goto :goto_b

    #@1a
    .restart local v1       #i:I
    .restart local v2       #len:I
    .restart local v3       #out:[J
    :cond_1a
    move-object p3, v3

    #@1b
    .line 749
    goto :goto_6
.end method

.method private getVibratorVolumePattern(I)[I
    .registers 7
    .parameter "length"

    #@0
    .prologue
    .line 1435
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@2
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@4
    const/4 v4, 0x1

    #@5
    invoke-interface {v3, v4}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->getVibrateVolume(I)I

    #@8
    move-result v1

    #@9
    .line 1436
    .local v1, volume:I
    new-array v2, p1, [I

    #@b
    .line 1437
    .local v2, volumeArray:[I
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    if-ge v0, p1, :cond_13

    #@e
    .line 1438
    aput v1, v2, v0

    #@10
    .line 1437
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_c

    #@13
    .line 1440
    :cond_13
    return-object v2
.end method

.method private handleTimeout(Lcom/android/server/NotificationManagerService$ToastRecord;)V
    .registers 6
    .parameter "record"

    #@0
    .prologue
    .line 1017
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 1018
    :try_start_3
    iget-object v1, p1, Lcom/android/server/NotificationManagerService$ToastRecord;->pkg:Ljava/lang/String;

    #@5
    iget-object v3, p1, Lcom/android/server/NotificationManagerService$ToastRecord;->callback:Landroid/app/ITransientNotification;

    #@7
    invoke-direct {p0, v1, v3}, Lcom/android/server/NotificationManagerService;->indexOfToastLocked(Ljava/lang/String;Landroid/app/ITransientNotification;)I

    #@a
    move-result v0

    #@b
    .line 1019
    .local v0, index:I
    if-ltz v0, :cond_10

    #@d
    .line 1020
    invoke-direct {p0, v0}, Lcom/android/server/NotificationManagerService;->cancelToastLocked(I)V

    #@10
    .line 1022
    :cond_10
    monitor-exit v2

    #@11
    .line 1023
    return-void

    #@12
    .line 1022
    .end local v0           #index:I
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method private static idDebugString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .registers 7
    .parameter "baseContext"
    .parameter "packageName"
    .parameter "id"

    #@0
    .prologue
    .line 350
    const/4 v0, 0x0

    #@1
    .line 352
    .local v0, c:Landroid/content/Context;
    if-eqz p1, :cond_14

    #@3
    .line 354
    const/4 v3, 0x0

    #@4
    :try_start_4
    invoke-virtual {p0, p1, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_7} :catch_11

    #@7
    move-result-object v0

    #@8
    .line 366
    :goto_8
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v2

    #@c
    .line 368
    .local v2, r:Landroid/content/res/Resources;
    :try_start_c
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_f
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_c .. :try_end_f} :catch_16

    #@f
    move-result-object v3

    #@10
    .line 370
    :goto_10
    return-object v3

    #@11
    .line 355
    .end local v2           #r:Landroid/content/res/Resources;
    :catch_11
    move-exception v1

    #@12
    .line 356
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    move-object v0, p0

    #@13
    .line 357
    goto :goto_8

    #@14
    .line 359
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_14
    move-object v0, p0

    #@15
    goto :goto_8

    #@16
    .line 369
    .restart local v2       #r:Landroid/content/res/Resources;
    :catch_16
    move-exception v1

    #@17
    .line 370
    .local v1, e:Landroid/content/res/Resources$NotFoundException;
    const-string v3, "<name unknown>"

    #@19
    goto :goto_10
.end method

.method private indexOfNotificationLocked(Ljava/lang/String;Ljava/lang/String;II)I
    .registers 10
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"
    .parameter "userId"

    #@0
    .prologue
    .line 1816
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@2
    .line 1817
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/NotificationManagerService$NotificationRecord;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1818
    .local v1, len:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_34

    #@9
    .line 1819
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@f
    .line 1820
    .local v3, r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    invoke-direct {p0, v3, p4}, Lcom/android/server/NotificationManagerService;->notificationMatchesUserId(Lcom/android/server/NotificationManagerService$NotificationRecord;I)Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_19

    #@15
    iget v4, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->id:I

    #@17
    if-eq v4, p3, :cond_1c

    #@19
    .line 1818
    :cond_19
    :goto_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_7

    #@1c
    .line 1823
    :cond_1c
    if-nez p2, :cond_2b

    #@1e
    .line 1824
    iget-object v4, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->tag:Ljava/lang/String;

    #@20
    if-nez v4, :cond_19

    #@22
    .line 1832
    :cond_22
    iget-object v4, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@24
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_19

    #@2a
    .line 1836
    .end local v0           #i:I
    .end local v3           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :goto_2a
    return v0

    #@2b
    .line 1828
    .restart local v0       #i:I
    .restart local v3       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_2b
    iget-object v4, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->tag:Ljava/lang/String;

    #@2d
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v4

    #@31
    if-nez v4, :cond_22

    #@33
    goto :goto_19

    #@34
    .line 1836
    .end local v3           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_34
    const/4 v0, -0x1

    #@35
    goto :goto_2a
.end method

.method private indexOfToastLocked(Ljava/lang/String;Landroid/app/ITransientNotification;)I
    .registers 9
    .parameter "pkg"
    .parameter "callback"

    #@0
    .prologue
    .line 1028
    invoke-interface {p2}, Landroid/app/ITransientNotification;->asBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    .line 1029
    .local v0, cbak:Landroid/os/IBinder;
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@6
    .line 1030
    .local v3, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/NotificationManagerService$ToastRecord;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    .line 1031
    .local v2, len:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v2, :cond_27

    #@d
    .line 1032
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v4

    #@11
    check-cast v4, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@13
    .line 1033
    .local v4, r:Lcom/android/server/NotificationManagerService$ToastRecord;
    iget-object v5, v4, Lcom/android/server/NotificationManagerService$ToastRecord;->pkg:Ljava/lang/String;

    #@15
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_24

    #@1b
    iget-object v5, v4, Lcom/android/server/NotificationManagerService$ToastRecord;->callback:Landroid/app/ITransientNotification;

    #@1d
    invoke-interface {v5}, Landroid/app/ITransientNotification;->asBinder()Landroid/os/IBinder;

    #@20
    move-result-object v5

    #@21
    if-ne v5, v0, :cond_24

    #@23
    .line 1037
    .end local v1           #i:I
    .end local v4           #r:Lcom/android/server/NotificationManagerService$ToastRecord;
    :goto_23
    return v1

    #@24
    .line 1031
    .restart local v1       #i:I
    .restart local v4       #r:Lcom/android/server/NotificationManagerService$ToastRecord;
    :cond_24
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_b

    #@27
    .line 1037
    .end local v4           #r:Lcom/android/server/NotificationManagerService$ToastRecord;
    :cond_27
    const/4 v1, -0x1

    #@28
    goto :goto_23
.end method

.method private isExceptionalPackage(Ljava/lang/String;)Z
    .registers 4
    .parameter "pkg"

    #@0
    .prologue
    .line 1805
    const/4 v0, 0x0

    #@1
    .line 1806
    .local v0, val:Z
    if-eqz p1, :cond_d

    #@3
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@5
    if-eqz v1, :cond_d

    #@7
    .line 1807
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@9
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    .line 1809
    :cond_d
    return v0
.end method

.method private keepProcessAliveLocked(I)V
    .registers 10
    .parameter "pid"

    #@0
    .prologue
    .line 1043
    const/4 v4, 0x0

    #@1
    .line 1044
    .local v4, toastCount:I
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@3
    .line 1045
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/NotificationManagerService$ToastRecord;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 1046
    .local v0, N:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_19

    #@a
    .line 1047
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v3

    #@e
    check-cast v3, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@10
    .line 1048
    .local v3, r:Lcom/android/server/NotificationManagerService$ToastRecord;
    iget v5, v3, Lcom/android/server/NotificationManagerService$ToastRecord;->pid:I

    #@12
    if-ne v5, p1, :cond_16

    #@14
    .line 1049
    add-int/lit8 v4, v4, 0x1

    #@16
    .line 1046
    :cond_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_8

    #@19
    .line 1053
    .end local v3           #r:Lcom/android/server/NotificationManagerService$ToastRecord;
    :cond_19
    :try_start_19
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mAm:Landroid/app/IActivityManager;

    #@1b
    iget-object v7, p0, Lcom/android/server/NotificationManagerService;->mForegroundToken:Landroid/os/IBinder;

    #@1d
    if-lez v4, :cond_24

    #@1f
    const/4 v5, 0x1

    #@20
    :goto_20
    invoke-interface {v6, v7, p1, v5}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_23} :catch_26

    #@23
    .line 1057
    :goto_23
    return-void

    #@24
    .line 1053
    :cond_24
    const/4 v5, 0x0

    #@25
    goto :goto_20

    #@26
    .line 1054
    :catch_26
    move-exception v5

    #@27
    goto :goto_23
.end method

.method private loadBlockDb()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    .line 225
    iget-object v8, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@3
    monitor-enter v8

    #@4
    .line 226
    :try_start_4
    iget-object v7, p0, Lcom/android/server/NotificationManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@6
    if-nez v7, :cond_7e

    #@8
    .line 227
    new-instance v0, Ljava/io/File;

    #@a
    const-string v7, "/data/system"

    #@c
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@f
    .line 228
    .local v0, dir:Ljava/io/File;
    new-instance v7, Landroid/util/AtomicFile;

    #@11
    new-instance v9, Ljava/io/File;

    #@13
    const-string v10, "notification_policy.xml"

    #@15
    invoke-direct {v9, v0, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@18
    invoke-direct {v7, v9}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@1b
    iput-object v7, p0, Lcom/android/server/NotificationManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@1d
    .line 230
    iget-object v7, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@1f
    invoke-virtual {v7}, Ljava/util/HashSet;->clear()V
    :try_end_22
    .catchall {:try_start_4 .. :try_end_22} :catchall_98

    #@22
    .line 232
    const/4 v2, 0x0

    #@23
    .line 234
    .local v2, infile:Ljava/io/FileInputStream;
    :try_start_23
    iget-object v7, p0, Lcom/android/server/NotificationManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@25
    invoke-virtual {v7}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@28
    move-result-object v2

    #@29
    .line 235
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@2c
    move-result-object v3

    #@2d
    .line 236
    .local v3, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v7, 0x0

    #@2e
    invoke-interface {v3, v2, v7}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@31
    .line 240
    const/4 v6, 0x1

    #@32
    .line 241
    .local v6, version:I
    :cond_32
    :goto_32
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@35
    move-result v5

    #@36
    .local v5, type:I
    if-eq v5, v11, :cond_b8

    #@38
    .line 242
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    .line 243
    .local v4, tag:Ljava/lang/String;
    const/4 v7, 0x2

    #@3d
    if-ne v5, v7, :cond_32

    #@3f
    .line 244
    const-string v7, "notification-policy"

    #@41
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_53

    #@47
    .line 245
    const/4 v7, 0x0

    #@48
    const-string v9, "version"

    #@4a
    invoke-interface {v3, v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v7

    #@4e
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@51
    move-result v6

    #@52
    goto :goto_32

    #@53
    .line 246
    :cond_53
    const-string v7, "blocked-packages"

    #@55
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v7

    #@59
    if-eqz v7, :cond_32

    #@5b
    .line 247
    :cond_5b
    :goto_5b
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@5e
    move-result v5

    #@5f
    if-eq v5, v11, :cond_32

    #@61
    .line 248
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    .line 249
    const-string v7, "package"

    #@67
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v7

    #@6b
    if-eqz v7, :cond_80

    #@6d
    .line 250
    iget-object v7, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@6f
    const/4 v9, 0x0

    #@70
    const-string v10, "name"

    #@72
    invoke-interface {v3, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@75
    move-result-object v9

    #@76
    invoke-virtual {v7, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_79
    .catchall {:try_start_23 .. :try_end_79} :catchall_b3
    .catch Ljava/io/FileNotFoundException; {:try_start_23 .. :try_end_79} :catch_7a
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_79} :catch_8c
    .catch Ljava/lang/NumberFormatException; {:try_start_23 .. :try_end_79} :catch_9b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_23 .. :try_end_79} :catch_a7

    #@79
    goto :goto_5b

    #@7a
    .line 258
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v4           #tag:Ljava/lang/String;
    .end local v5           #type:I
    .end local v6           #version:I
    :catch_7a
    move-exception v7

    #@7b
    .line 267
    :try_start_7b
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@7e
    .line 270
    .end local v0           #dir:Ljava/io/File;
    .end local v2           #infile:Ljava/io/FileInputStream;
    :cond_7e
    :goto_7e
    monitor-exit v8
    :try_end_7f
    .catchall {:try_start_7b .. :try_end_7f} :catchall_98

    #@7f
    .line 271
    return-void

    #@80
    .line 251
    .restart local v0       #dir:Ljava/io/File;
    .restart local v2       #infile:Ljava/io/FileInputStream;
    .restart local v3       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v4       #tag:Ljava/lang/String;
    .restart local v5       #type:I
    .restart local v6       #version:I
    :cond_80
    :try_start_80
    const-string v7, "blocked-packages"

    #@82
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_85
    .catchall {:try_start_80 .. :try_end_85} :catchall_b3
    .catch Ljava/io/FileNotFoundException; {:try_start_80 .. :try_end_85} :catch_7a
    .catch Ljava/io/IOException; {:try_start_80 .. :try_end_85} :catch_8c
    .catch Ljava/lang/NumberFormatException; {:try_start_80 .. :try_end_85} :catch_9b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_80 .. :try_end_85} :catch_a7

    #@85
    move-result v7

    #@86
    if-eqz v7, :cond_5b

    #@88
    const/4 v7, 0x3

    #@89
    if-ne v5, v7, :cond_5b

    #@8b
    goto :goto_32

    #@8c
    .line 260
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v4           #tag:Ljava/lang/String;
    .end local v5           #type:I
    .end local v6           #version:I
    :catch_8c
    move-exception v1

    #@8d
    .line 261
    .local v1, e:Ljava/io/IOException;
    :try_start_8d
    const-string v7, "NotificationService"

    #@8f
    const-string v9, "Unable to read blocked notifications database"

    #@91
    invoke-static {v7, v9, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_94
    .catchall {:try_start_8d .. :try_end_94} :catchall_b3

    #@94
    .line 267
    :try_start_94
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@97
    goto :goto_7e

    #@98
    .line 270
    .end local v0           #dir:Ljava/io/File;
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #infile:Ljava/io/FileInputStream;
    :catchall_98
    move-exception v7

    #@99
    monitor-exit v8
    :try_end_9a
    .catchall {:try_start_94 .. :try_end_9a} :catchall_98

    #@9a
    throw v7

    #@9b
    .line 262
    .restart local v0       #dir:Ljava/io/File;
    .restart local v2       #infile:Ljava/io/FileInputStream;
    :catch_9b
    move-exception v1

    #@9c
    .line 263
    .local v1, e:Ljava/lang/NumberFormatException;
    :try_start_9c
    const-string v7, "NotificationService"

    #@9e
    const-string v9, "Unable to parse blocked notifications database"

    #@a0
    invoke-static {v7, v9, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a3
    .catchall {:try_start_9c .. :try_end_a3} :catchall_b3

    #@a3
    .line 267
    :try_start_a3
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V
    :try_end_a6
    .catchall {:try_start_a3 .. :try_end_a6} :catchall_98

    #@a6
    goto :goto_7e

    #@a7
    .line 264
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :catch_a7
    move-exception v1

    #@a8
    .line 265
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_a8
    const-string v7, "NotificationService"

    #@aa
    const-string v9, "Unable to parse blocked notifications database"

    #@ac
    invoke-static {v7, v9, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_af
    .catchall {:try_start_a8 .. :try_end_af} :catchall_b3

    #@af
    .line 267
    :try_start_af
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@b2
    goto :goto_7e

    #@b3
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_b3
    move-exception v7

    #@b4
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@b7
    throw v7

    #@b8
    .restart local v3       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v5       #type:I
    .restart local v6       #version:I
    :cond_b8
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V
    :try_end_bb
    .catchall {:try_start_af .. :try_end_bb} :catchall_98

    #@bb
    goto :goto_7e
.end method

.method private notificationMatchesUserId(Lcom/android/server/NotificationManagerService$NotificationRecord;I)Z
    .registers 5
    .parameter "r"
    .parameter "userId"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1567
    if-eq p2, v1, :cond_b

    #@3
    iget v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->userId:I

    #@5
    if-eq v0, v1, :cond_b

    #@7
    iget v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->userId:I

    #@9
    if-ne v0, p2, :cond_d

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method private scheduleTimeoutLocked(Lcom/android/server/NotificationManagerService$ToastRecord;Z)V
    .registers 8
    .parameter "r"
    .parameter "immediate"

    #@0
    .prologue
    .line 1008
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v2

    #@7
    .line 1009
    .local v2, m:Landroid/os/Message;
    if-eqz p2, :cond_16

    #@9
    const-wide/16 v0, 0x0

    #@b
    .line 1010
    .local v0, delay:J
    :goto_b
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@d
    invoke-virtual {v3, p1}, Lcom/android/server/NotificationManagerService$WorkerHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@10
    .line 1011
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@12
    invoke-virtual {v3, v2, v0, v1}, Lcom/android/server/NotificationManagerService$WorkerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@15
    .line 1012
    return-void

    #@16
    .line 1009
    .end local v0           #delay:J
    :cond_16
    iget v3, p1, Lcom/android/server/NotificationManagerService$ToastRecord;->duration:I

    #@18
    const/4 v4, 0x1

    #@19
    if-ne v3, v4, :cond_1f

    #@1b
    const/16 v3, 0xdac

    #@1d
    :goto_1d
    int-to-long v0, v3

    #@1e
    goto :goto_b

    #@1f
    :cond_1f
    const/16 v3, 0x7d0

    #@21
    goto :goto_1d
.end method

.method private sendAccessibilityEvent(Landroid/app/Notification;Ljava/lang/CharSequence;)V
    .registers 7
    .parameter "notification"
    .parameter "packageName"

    #@0
    .prologue
    .line 1445
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v3}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    .line 1446
    .local v1, manager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_d

    #@c
    .line 1461
    :goto_c
    return-void

    #@d
    .line 1450
    :cond_d
    const/16 v3, 0x40

    #@f
    invoke-static {v3}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@12
    move-result-object v0

    #@13
    .line 1452
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, p2}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    #@16
    .line 1453
    const-class v3, Landroid/app/Notification;

    #@18
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@1f
    .line 1454
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityEvent;->setParcelableData(Landroid/os/Parcelable;)V

    #@22
    .line 1455
    iget-object v2, p1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@24
    .line 1456
    .local v2, tickerText:Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@27
    move-result v3

    #@28
    if-nez v3, :cond_31

    #@2a
    .line 1457
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@2d
    move-result-object v3

    #@2e
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@31
    .line 1460
    :cond_31
    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@34
    goto :goto_c
.end method

.method private sendBroadcastCancelNotification(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 6
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"

    #@0
    .prologue
    .line 2030
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "lge.intent.action.REMOVE_NOTI"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2031
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "pkg"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 2032
    const-string v1, "tag"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11
    .line 2033
    const-string v1, "id"

    #@13
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@16
    .line 2034
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@18
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1b
    .line 2035
    return-void
.end method

.method private sendNotificationToTMode(Ljava/lang/String;Landroid/app/Notification;Z)V
    .registers 7
    .parameter "pkg"
    .parameter "notification"
    .parameter "add"

    #@0
    .prologue
    .line 2060
    const-string v1, "ro.build.target_operator"

    #@2
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    const-string v2, "SKT"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_2e

    #@e
    .line 2061
    new-instance v0, Landroid/content/Intent;

    #@10
    const-string v1, "com.lge.android.intent.action.TMODE_NOTI"

    #@12
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15
    .line 2062
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.skt.tmode"

    #@17
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@1a
    .line 2063
    const-string v1, "pkg"

    #@1c
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 2064
    const-string v1, "add"

    #@21
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@24
    .line 2065
    const-string v1, "noti"

    #@26
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@29
    .line 2066
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2e
    .line 2070
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2e
    return-void
.end method

.method private sendOSPEvent(Landroid/app/Notification;Ljava/lang/CharSequence;)V
    .registers 13
    .parameter "notification"
    .parameter "packageName"

    #@0
    .prologue
    .line 2000
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mIOSPService:Ljava/lang/Object;

    #@2
    if-eqz v5, :cond_20

    #@4
    sget-object v5, Lcom/android/server/NotificationManagerService;->mOSPIsConnectedMethod:Ljava/lang/reflect/Method;

    #@6
    if-eqz v5, :cond_20

    #@8
    sget-object v5, Lcom/android/server/NotificationManagerService;->mOSPNotifyMethod:Ljava/lang/reflect/Method;

    #@a
    if-eqz v5, :cond_20

    #@c
    if-eqz p2, :cond_20

    #@e
    iget-object v5, p1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@10
    if-eqz v5, :cond_20

    #@12
    .line 2002
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mOSPSkipList:[Ljava/lang/CharSequence;

    #@14
    .local v0, arr$:[Ljava/lang/CharSequence;
    array-length v2, v0

    #@15
    .local v2, len$:I
    const/4 v1, 0x0

    #@16
    .local v1, i$:I
    :goto_16
    if-ge v1, v2, :cond_24

    #@18
    aget-object v4, v0, v1

    #@1a
    .line 2003
    .local v4, skipPkg:Ljava/lang/CharSequence;
    invoke-virtual {p2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_21

    #@20
    .line 2025
    .end local v0           #arr$:[Ljava/lang/CharSequence;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v4           #skipPkg:Ljava/lang/CharSequence;
    :cond_20
    :goto_20
    return-void

    #@21
    .line 2002
    .restart local v0       #arr$:[Ljava/lang/CharSequence;
    .restart local v1       #i$:I
    .restart local v2       #len$:I
    .restart local v4       #skipPkg:Ljava/lang/CharSequence;
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_16

    #@24
    .line 2010
    .end local v4           #skipPkg:Ljava/lang/CharSequence;
    :cond_24
    :try_start_24
    sget-object v6, Lcom/android/server/NotificationManagerService;->mOSPIsConnectedMethod:Ljava/lang/reflect/Method;

    #@26
    iget-object v7, p0, Lcom/android/server/NotificationManagerService;->mIOSPService:Ljava/lang/Object;

    #@28
    const/4 v5, 0x0

    #@29
    check-cast v5, [Ljava/lang/Object;

    #@2b
    invoke-virtual {v6, v7, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Ljava/lang/Boolean;

    #@31
    .line 2013
    .local v3, result:Ljava/lang/Boolean;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    #@34
    move-result v5

    #@35
    if-eqz v5, :cond_4c

    #@37
    .line 2015
    sget-object v5, Lcom/android/server/NotificationManagerService;->mOSPNotifyMethod:Ljava/lang/reflect/Method;

    #@39
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mIOSPService:Ljava/lang/Object;

    #@3b
    const/4 v7, 0x2

    #@3c
    new-array v7, v7, [Ljava/lang/Object;

    #@3e
    const/4 v8, 0x0

    #@3f
    aput-object p2, v7, v8

    #@41
    const/4 v8, 0x1

    #@42
    iget-object v9, p1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@44
    aput-object v9, v7, v8

    #@46
    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    goto :goto_20

    #@4a
    .line 2020
    .end local v3           #result:Ljava/lang/Boolean;
    :catch_4a
    move-exception v5

    #@4b
    goto :goto_20

    #@4c
    .line 2017
    .restart local v3       #result:Ljava/lang/Boolean;
    :cond_4c
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mOSPConnection:Landroid/content/ServiceConnection;

    #@4e
    if-eqz v5, :cond_20

    #@50
    .line 2018
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@52
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mOSPConnection:Landroid/content/ServiceConnection;

    #@54
    invoke-virtual {v5, v6}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_57
    .catch Ljava/lang/IllegalAccessException; {:try_start_24 .. :try_end_57} :catch_4a
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_24 .. :try_end_57} :catch_58
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_57} :catch_5a

    #@57
    goto :goto_20

    #@58
    .line 2021
    .end local v3           #result:Ljava/lang/Boolean;
    :catch_58
    move-exception v5

    #@59
    goto :goto_20

    #@5a
    .line 2022
    :catch_5a
    move-exception v5

    #@5b
    goto :goto_20
.end method

.method private showNextToastLocked()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 961
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    check-cast v2, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@9
    .line 962
    .local v2, record:Lcom/android/server/NotificationManagerService$ToastRecord;
    :goto_9
    if-eqz v2, :cond_14

    #@b
    .line 965
    :try_start_b
    iget-object v3, v2, Lcom/android/server/NotificationManagerService$ToastRecord;->callback:Landroid/app/ITransientNotification;

    #@d
    invoke-interface {v3}, Landroid/app/ITransientNotification;->show()V

    #@10
    .line 966
    const/4 v3, 0x0

    #@11
    invoke-direct {p0, v2, v3}, Lcom/android/server/NotificationManagerService;->scheduleTimeoutLocked(Lcom/android/server/NotificationManagerService$ToastRecord;Z)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_14} :catch_15

    #@14
    .line 984
    :cond_14
    return-void

    #@15
    .line 968
    :catch_15
    move-exception v0

    #@16
    .line 969
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "NotificationService"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Object died trying to show notification "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    iget-object v5, v2, Lcom/android/server/NotificationManagerService$ToastRecord;->callback:Landroid/app/ITransientNotification;

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, " in package "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    iget-object v5, v2, Lcom/android/server/NotificationManagerService$ToastRecord;->pkg:Ljava/lang/String;

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 972
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@3e
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@41
    move-result v1

    #@42
    .line 973
    .local v1, index:I
    if-ltz v1, :cond_49

    #@44
    .line 974
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@49
    .line 976
    :cond_49
    iget v3, v2, Lcom/android/server/NotificationManagerService$ToastRecord;->pid:I

    #@4b
    invoke-direct {p0, v3}, Lcom/android/server/NotificationManagerService;->keepProcessAliveLocked(I)V

    #@4e
    .line 977
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@53
    move-result v3

    #@54
    if-lez v3, :cond_5f

    #@56
    .line 978
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@58
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5b
    move-result-object v2

    #@5c
    .end local v2           #record:Lcom/android/server/NotificationManagerService$ToastRecord;
    check-cast v2, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@5e
    .restart local v2       #record:Lcom/android/server/NotificationManagerService$ToastRecord;
    goto :goto_9

    #@5f
    .line 980
    :cond_5f
    const/4 v2, 0x0

    #@60
    goto :goto_9
.end method

.method private spcErrorNotification()V
    .registers 5

    #@0
    .prologue
    .line 697
    const-string v0, "NotificationService"

    #@2
    const-string v1, "spcErrorNotification"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 698
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@9
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@b
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@e
    const v1, 0x2090307

    #@11
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@14
    move-result-object v0

    #@15
    const v1, 0x2090308

    #@18
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@1b
    move-result-object v0

    #@1c
    const/4 v1, 0x0

    #@1d
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@24
    move-result-object v0

    #@25
    sput-object v0, Lcom/android/server/NotificationManagerService;->dialog:Landroid/app/AlertDialog;

    #@27
    .line 705
    sget-object v0, Lcom/android/server/NotificationManagerService;->dialog:Landroid/app/AlertDialog;

    #@29
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@2c
    move-result-object v0

    #@2d
    const/16 v1, 0x7da

    #@2f
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@32
    .line 706
    sget-object v0, Lcom/android/server/NotificationManagerService;->dialog:Landroid/app/AlertDialog;

    #@34
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@37
    move-result-object v0

    #@38
    const/16 v1, 0x80

    #@3a
    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    #@3d
    .line 707
    sget-object v0, Lcom/android/server/NotificationManagerService;->dialog:Landroid/app/AlertDialog;

    #@3f
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@42
    .line 708
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@44
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mHandler:Lcom/android/server/NotificationManagerService$WorkerHandler;

    #@46
    const/4 v2, 0x7

    #@47
    invoke-virtual {v1, v2}, Lcom/android/server/NotificationManagerService$WorkerHandler;->obtainMessage(I)Landroid/os/Message;

    #@4a
    move-result-object v1

    #@4b
    const-wide/32 v2, 0xea60

    #@4e
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/NotificationManagerService$WorkerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@51
    .line 709
    return-void
.end method

.method private updateLightListLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;I)V
    .registers 11
    .parameter "r"
    .parameter "action"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1855
    iget-boolean v0, p0, Lcom/android/server/NotificationManagerService;->mSystemReady:Z

    #@3
    if-nez v0, :cond_6

    #@5
    .line 1878
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1858
    :cond_6
    const-string v0, "NotificationService"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "updateLightListLocked :r="

    #@f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v3, ", action="

    #@19
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1859
    if-eqz p1, :cond_44

    #@2a
    const-string v0, "NotificationService"

    #@2c
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "notification="

    #@33
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-object v3, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@39
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1861
    :cond_44
    sget v0, Lcom/android/server/NotificationManagerService;->ACTION_CLEAR_RECORD:I

    #@46
    if-ne p2, v0, :cond_54

    #@48
    .line 1862
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@4a
    const/4 v7, 0x0

    #@4b
    move v1, p2

    #@4c
    move v3, v2

    #@4d
    move v4, v2

    #@4e
    move v5, v2

    #@4f
    move v6, v2

    #@50
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/LightsService$Light;->updateLightList(IIZIIILjava/lang/String;)V

    #@53
    goto :goto_5

    #@54
    .line 1863
    :cond_54
    if-eqz p1, :cond_5

    #@56
    iget-object v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@58
    if-eqz v0, :cond_5

    #@5a
    iget-object v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@5c
    iget v0, v0, Landroid/app/Notification;->flags:I

    #@5e
    and-int/lit8 v0, v0, 0x1

    #@60
    if-eqz v0, :cond_5

    #@62
    .line 1865
    iget-object v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@64
    iget v4, v0, Landroid/app/Notification;->ledARGB:I

    #@66
    .line 1866
    .local v4, ledARGB:I
    iget-object v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@68
    iget v5, v0, Landroid/app/Notification;->ledOnMS:I

    #@6a
    .line 1867
    .local v5, ledOnMS:I
    iget-object v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@6c
    iget v6, v0, Landroid/app/Notification;->ledOffMS:I

    #@6e
    .line 1868
    .local v6, ledOffMS:I
    iget-object v0, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@70
    iget v0, v0, Landroid/app/Notification;->defaults:I

    #@72
    and-int/lit8 v0, v0, 0x4

    #@74
    if-eqz v0, :cond_7c

    #@76
    .line 1869
    iget v4, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationColor:I

    #@78
    .line 1870
    iget v5, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOn:I

    #@7a
    .line 1871
    iget v6, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOff:I

    #@7c
    .line 1873
    :cond_7c
    if-eqz v4, :cond_5

    #@7e
    if-nez v6, :cond_82

    #@80
    if-eqz v5, :cond_5

    #@82
    .line 1874
    :cond_82
    iget-object v0, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@84
    iget v2, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->id:I

    #@86
    iget-boolean v3, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->mExceptional:Z

    #@88
    iget-object v7, p1, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@8a
    move v1, p2

    #@8b
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/LightsService$Light;->updateLightList(IIZIIILjava/lang/String;)V

    #@8e
    goto/16 :goto_5
.end method

.method private updateLightsLocked()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 1738
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@3
    if-nez v4, :cond_19

    #@5
    .line 1740
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v3

    #@b
    .line 1741
    .local v3, n:I
    if-lez v3, :cond_19

    #@d
    .line 1742
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@f
    add-int/lit8 v5, v3, -0x1

    #@11
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v4

    #@15
    check-cast v4, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@17
    iput-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@19
    .line 1747
    .end local v3           #n:I
    :cond_19
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@1b
    if-eqz v4, :cond_bc

    #@1d
    iget-boolean v4, p0, Lcom/android/server/NotificationManagerService;->mLedOnEvenWhenLcdOn:Z

    #@1f
    if-ne v4, v7, :cond_bc

    #@21
    .line 1748
    const-string v4, "NotificationService"

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v6, "updateLightsLocked() start. mLights.size : "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@33
    move-result v6

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v5

    #@3c
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 1750
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@41
    if-eqz v4, :cond_47

    #@43
    iget-boolean v4, p0, Lcom/android/server/NotificationManagerService;->mInCall:Z

    #@45
    if-eqz v4, :cond_54

    #@47
    .line 1751
    :cond_47
    const-string v4, "NotificationService"

    #@49
    const-string v5, "mNotificationLight.turnOff() : 1"

    #@4b
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1752
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@50
    invoke-virtual {v4}, Lcom/android/server/LightsService$Light;->turnOff()V

    #@53
    .line 1794
    :cond_53
    :goto_53
    return-void

    #@54
    .line 1754
    :cond_54
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@56
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@58
    iget v0, v4, Landroid/app/Notification;->ledARGB:I

    #@5a
    .line 1755
    .local v0, ledARGB:I
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@5c
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@5e
    iget v2, v4, Landroid/app/Notification;->ledOnMS:I

    #@60
    .line 1756
    .local v2, ledOnMS:I
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@62
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@64
    iget v1, v4, Landroid/app/Notification;->ledOffMS:I

    #@66
    .line 1757
    .local v1, ledOffMS:I
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@68
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@6a
    iget v4, v4, Landroid/app/Notification;->defaults:I

    #@6c
    and-int/lit8 v4, v4, 0x4

    #@6e
    if-eqz v4, :cond_98

    #@70
    .line 1758
    iget v0, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationColor:I

    #@72
    .line 1759
    iget v2, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOn:I

    #@74
    .line 1760
    iget v1, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOff:I

    #@76
    .line 1761
    const-string v4, "NotificationService"

    #@78
    new-instance v5, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v6, "mNotificationLight1 : ledOnMS = "

    #@7f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    const-string v6, " ,ledOffMS = "

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 1763
    :cond_98
    const-string v4, "NotificationService"

    #@9a
    new-instance v5, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v6, "updateLightsLocked(1) : mNotificationPulseEnabled = "

    #@a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    iget-boolean v6, p0, Lcom/android/server/NotificationManagerService;->mNotificationPulseEnabled:Z

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v5

    #@af
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    .line 1764
    iget-boolean v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationPulseEnabled:Z

    #@b4
    if-eqz v4, :cond_53

    #@b6
    .line 1766
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@b8
    invoke-virtual {v4, v0, v7, v2, v1}, Lcom/android/server/LightsService$Light;->setFlashing(IIII)V

    #@bb
    goto :goto_53

    #@bc
    .line 1773
    .end local v0           #ledARGB:I
    .end local v1           #ledOffMS:I
    .end local v2           #ledOnMS:I
    :cond_bc
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@be
    if-eqz v4, :cond_c8

    #@c0
    iget-boolean v4, p0, Lcom/android/server/NotificationManagerService;->mInCall:Z

    #@c2
    if-nez v4, :cond_c8

    #@c4
    iget-boolean v4, p0, Lcom/android/server/NotificationManagerService;->mScreenOn:Z

    #@c6
    if-eqz v4, :cond_d6

    #@c8
    .line 1774
    :cond_c8
    const-string v4, "NotificationService"

    #@ca
    const-string v5, "mNotificationLight.turnOff() : 2"

    #@cc
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 1775
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@d1
    invoke-virtual {v4}, Lcom/android/server/LightsService$Light;->turnOff()V

    #@d4
    goto/16 :goto_53

    #@d6
    .line 1777
    :cond_d6
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@d8
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@da
    iget v0, v4, Landroid/app/Notification;->ledARGB:I

    #@dc
    .line 1778
    .restart local v0       #ledARGB:I
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@de
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@e0
    iget v2, v4, Landroid/app/Notification;->ledOnMS:I

    #@e2
    .line 1779
    .restart local v2       #ledOnMS:I
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@e4
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@e6
    iget v1, v4, Landroid/app/Notification;->ledOffMS:I

    #@e8
    .line 1780
    .restart local v1       #ledOffMS:I
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@ea
    iget-object v4, v4, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@ec
    iget v4, v4, Landroid/app/Notification;->defaults:I

    #@ee
    and-int/lit8 v4, v4, 0x4

    #@f0
    if-eqz v4, :cond_11a

    #@f2
    .line 1781
    iget v0, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationColor:I

    #@f4
    .line 1782
    iget v2, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOn:I

    #@f6
    .line 1783
    iget v1, p0, Lcom/android/server/NotificationManagerService;->mDefaultNotificationLedOff:I

    #@f8
    .line 1784
    const-string v4, "NotificationService"

    #@fa
    new-instance v5, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v6, "mNotificationLight2 : ledOnMS = "

    #@101
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v5

    #@105
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@108
    move-result-object v5

    #@109
    const-string v6, " ,ledOffMS = "

    #@10b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v5

    #@10f
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@112
    move-result-object v5

    #@113
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v5

    #@117
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    .line 1786
    :cond_11a
    const-string v4, "NotificationService"

    #@11c
    new-instance v5, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v6, "updateLightsLocked(2) : mNotificationPulseEnabled = "

    #@123
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v5

    #@127
    iget-boolean v6, p0, Lcom/android/server/NotificationManagerService;->mNotificationPulseEnabled:Z

    #@129
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v5

    #@12d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v5

    #@131
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@134
    .line 1787
    iget-boolean v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationPulseEnabled:Z

    #@136
    if-eqz v4, :cond_53

    #@138
    .line 1789
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationLight:Lcom/android/server/LightsService$Light;

    #@13a
    invoke-virtual {v4, v0, v7, v2, v1}, Lcom/android/server/LightsService$Light;->setFlashing(IIII)V

    #@13d
    goto/16 :goto_53
.end method

.method private updateNotificationPulse()V
    .registers 3

    #@0
    .prologue
    .line 1840
    iget-object v1, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 1841
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V

    #@6
    .line 1842
    monitor-exit v1

    #@7
    .line 1843
    return-void

    #@8
    .line 1842
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private writeBlockDb()V
    .registers 10

    #@0
    .prologue
    .line 274
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@2
    monitor-enter v6

    #@3
    .line 275
    const/4 v3, 0x0

    #@4
    .line 277
    .local v3, outfile:Ljava/io/FileOutputStream;
    :try_start_4
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@6
    invoke-virtual {v5}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@9
    move-result-object v3

    #@a
    .line 279
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    #@c
    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@f
    .line 280
    .local v2, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v5, "utf-8"

    #@11
    invoke-interface {v2, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@14
    .line 282
    const/4 v5, 0x0

    #@15
    const/4 v7, 0x1

    #@16
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@19
    move-result-object v7

    #@1a
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@1d
    .line 284
    const/4 v5, 0x0

    #@1e
    const-string v7, "notification-policy"

    #@20
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@23
    .line 285
    const/4 v5, 0x0

    #@24
    const-string v7, "version"

    #@26
    const/4 v8, 0x1

    #@27
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@2a
    move-result-object v8

    #@2b
    invoke-interface {v2, v5, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2e
    .line 286
    const/4 v5, 0x0

    #@2f
    const-string v7, "blocked-packages"

    #@31
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@34
    .line 288
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@36
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@39
    move-result-object v1

    #@3a
    .local v1, i$:Ljava/util/Iterator;
    :goto_3a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_63

    #@40
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@43
    move-result-object v4

    #@44
    check-cast v4, Ljava/lang/String;

    #@46
    .line 289
    .local v4, pkg:Ljava/lang/String;
    const/4 v5, 0x0

    #@47
    const-string v7, "package"

    #@49
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4c
    .line 290
    const/4 v5, 0x0

    #@4d
    const-string v7, "name"

    #@4f
    invoke-interface {v2, v5, v7, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@52
    .line 291
    const/4 v5, 0x0

    #@53
    const-string v7, "package"

    #@55
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_58
    .catchall {:try_start_4 .. :try_end_58} :catchall_78
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_58} :catch_59

    #@58
    goto :goto_3a

    #@59
    .line 299
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v4           #pkg:Ljava/lang/String;
    :catch_59
    move-exception v0

    #@5a
    .line 300
    .local v0, e:Ljava/io/IOException;
    if-eqz v3, :cond_61

    #@5c
    .line 301
    :try_start_5c
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@5e
    invoke-virtual {v5, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@61
    .line 304
    .end local v0           #e:Ljava/io/IOException;
    :cond_61
    :goto_61
    monitor-exit v6
    :try_end_62
    .catchall {:try_start_5c .. :try_end_62} :catchall_78

    #@62
    .line 305
    return-void

    #@63
    .line 293
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #out:Lorg/xmlpull/v1/XmlSerializer;
    :cond_63
    const/4 v5, 0x0

    #@64
    :try_start_64
    const-string v7, "blocked-packages"

    #@66
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@69
    .line 294
    const/4 v5, 0x0

    #@6a
    const-string v7, "notification-policy"

    #@6c
    invoke-interface {v2, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6f
    .line 296
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@72
    .line 298
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@74
    invoke-virtual {v5, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_77
    .catchall {:try_start_64 .. :try_end_77} :catchall_78
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_77} :catch_59

    #@77
    goto :goto_61

    #@78
    .line 304
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #out:Lorg/xmlpull/v1/XmlSerializer;
    :catchall_78
    move-exception v5

    #@79
    :try_start_79
    monitor-exit v6
    :try_end_7a
    .catchall {:try_start_79 .. :try_end_7a} :catchall_78

    #@7a
    throw v5
.end method


# virtual methods
.method public areNotificationsEnabledForPackage(Ljava/lang/String;)Z
    .registers 3
    .parameter "pkg"

    #@0
    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/android/server/NotificationManagerService;->checkCallerIsSystem()V

    #@3
    .line 309
    invoke-direct {p0, p1}, Lcom/android/server/NotificationManagerService;->areNotificationsEnabledForPackageInt(Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method cancelAll(I)V
    .registers 9
    .parameter "userId"

    #@0
    .prologue
    .line 1712
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 1713
    :try_start_3
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    .line 1714
    .local v0, N:I
    add-int/lit8 v1, v0, -0x1

    #@b
    .local v1, i:I
    :goto_b
    if-ltz v1, :cond_44

    #@d
    .line 1715
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@15
    .line 1717
    .local v2, r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    invoke-direct {p0, v2, p1}, Lcom/android/server/NotificationManagerService;->notificationMatchesUserId(Lcom/android/server/NotificationManagerService$NotificationRecord;I)Z

    #@18
    move-result v3

    #@19
    if-nez v3, :cond_1e

    #@1b
    .line 1714
    :cond_1b
    :goto_1b
    add-int/lit8 v1, v1, -0x1

    #@1d
    goto :goto_b

    #@1e
    .line 1721
    :cond_1e
    iget-object v3, v2, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@20
    iget v3, v3, Landroid/app/Notification;->flags:I

    #@22
    and-int/lit8 v3, v3, 0x22

    #@24
    if-nez v3, :cond_1b

    #@26
    .line 1723
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2b
    .line 1724
    iget-object v3, v2, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@2d
    iget-object v5, v2, Lcom/android/server/NotificationManagerService$NotificationRecord;->tag:Ljava/lang/String;

    #@2f
    iget v6, v2, Lcom/android/server/NotificationManagerService$NotificationRecord;->id:I

    #@31
    invoke-direct {p0, v3, v5, v6}, Lcom/android/server/NotificationManagerService;->sendBroadcastCancelNotification(Ljava/lang/String;Ljava/lang/String;I)V

    #@34
    .line 1725
    iget-object v3, v2, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@36
    iget-object v5, v2, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@38
    const/4 v6, 0x0

    #@39
    invoke-direct {p0, v3, v5, v6}, Lcom/android/server/NotificationManagerService;->sendNotificationToTMode(Ljava/lang/String;Landroid/app/Notification;Z)V

    #@3c
    .line 1726
    const/4 v3, 0x1

    #@3d
    invoke-direct {p0, v2, v3}, Lcom/android/server/NotificationManagerService;->cancelNotificationLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;Z)V

    #@40
    goto :goto_1b

    #@41
    .line 1731
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :catchall_41
    move-exception v3

    #@42
    monitor-exit v4
    :try_end_43
    .catchall {:try_start_3 .. :try_end_43} :catchall_41

    #@43
    throw v3

    #@44
    .line 1730
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_44
    :try_start_44
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V

    #@47
    .line 1731
    monitor-exit v4
    :try_end_48
    .catchall {:try_start_44 .. :try_end_48} :catchall_41

    #@48
    .line 1732
    return-void
.end method

.method public cancelAllNotifications(Ljava/lang/String;I)V
    .registers 14
    .parameter "pkg"
    .parameter "userId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1631
    invoke-virtual {p0, p1}, Lcom/android/server/NotificationManagerService;->checkCallerIsSystemOrSameApp(Ljava/lang/String;)V

    #@5
    .line 1633
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@8
    move-result v0

    #@9
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@c
    move-result v1

    #@d
    const-string v5, "cancelAllNotifications"

    #@f
    move v2, p2

    #@10
    move-object v6, p1

    #@11
    invoke-static/range {v0 .. v6}, Landroid/app/ActivityManager;->handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I

    #@14
    move-result p2

    #@15
    .line 1638
    const/16 v8, 0x40

    #@17
    move-object v5, p0

    #@18
    move-object v6, p1

    #@19
    move v7, v4

    #@1a
    move v9, v3

    #@1b
    move v10, p2

    #@1c
    invoke-virtual/range {v5 .. v10}, Lcom/android/server/NotificationManagerService;->cancelAllNotificationsInt(Ljava/lang/String;IIZI)Z

    #@1f
    .line 1639
    return-void
.end method

.method cancelAllNotificationsInt(Ljava/lang/String;IIZI)Z
    .registers 15
    .parameter "pkg"
    .parameter "mustHaveFlags"
    .parameter "mustNotHaveFlags"
    .parameter "doit"
    .parameter "userId"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 1582
    const/16 v5, 0xac0

    #@4
    const/4 v6, 0x4

    #@5
    new-array v6, v6, [Ljava/lang/Object;

    #@7
    aput-object p1, v6, v7

    #@9
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v7

    #@d
    aput-object v7, v6, v4

    #@f
    const/4 v7, 0x2

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v8

    #@14
    aput-object v8, v6, v7

    #@16
    const/4 v7, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v8

    #@1b
    aput-object v8, v6, v7

    #@1d
    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 1585
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@22
    monitor-enter v5

    #@23
    .line 1586
    :try_start_23
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@28
    move-result v0

    #@29
    .line 1587
    .local v0, N:I
    const/4 v1, 0x0

    #@2a
    .line 1588
    .local v1, canceledSomething:Z
    add-int/lit8 v2, v0, -0x1

    #@2c
    .local v2, i:I
    :goto_2c
    if-ltz v2, :cond_70

    #@2e
    .line 1589
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v3

    #@34
    check-cast v3, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@36
    .line 1590
    .local v3, r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    invoke-direct {p0, v3, p5}, Lcom/android/server/NotificationManagerService;->notificationMatchesUserId(Lcom/android/server/NotificationManagerService$NotificationRecord;I)Z

    #@39
    move-result v6

    #@3a
    if-nez v6, :cond_3f

    #@3c
    .line 1588
    :cond_3c
    :goto_3c
    add-int/lit8 v2, v2, -0x1

    #@3e
    goto :goto_2c

    #@3f
    .line 1594
    :cond_3f
    iget v6, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->userId:I

    #@41
    const/4 v7, -0x1

    #@42
    if-ne v6, v7, :cond_46

    #@44
    if-eqz p1, :cond_3c

    #@46
    .line 1597
    :cond_46
    iget-object v6, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@48
    iget v6, v6, Landroid/app/Notification;->flags:I

    #@4a
    and-int/2addr v6, p2

    #@4b
    if-ne v6, p2, :cond_3c

    #@4d
    .line 1600
    iget-object v6, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@4f
    iget v6, v6, Landroid/app/Notification;->flags:I

    #@51
    and-int/2addr v6, p3

    #@52
    if-nez v6, :cond_3c

    #@54
    .line 1603
    if-eqz p1, :cond_5e

    #@56
    iget-object v6, v3, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@58
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v6

    #@5c
    if-eqz v6, :cond_3c

    #@5e
    .line 1606
    :cond_5e
    const/4 v1, 0x1

    #@5f
    .line 1607
    if-nez p4, :cond_63

    #@61
    .line 1608
    monitor-exit v5

    #@62
    .line 1616
    .end local v3           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :goto_62
    return v4

    #@63
    .line 1610
    .restart local v3       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_63
    iget-object v6, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@68
    .line 1611
    const/4 v6, 0x0

    #@69
    invoke-direct {p0, v3, v6}, Lcom/android/server/NotificationManagerService;->cancelNotificationLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;Z)V

    #@6c
    goto :goto_3c

    #@6d
    .line 1617
    .end local v0           #N:I
    .end local v1           #canceledSomething:Z
    .end local v2           #i:I
    .end local v3           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :catchall_6d
    move-exception v4

    #@6e
    monitor-exit v5
    :try_end_6f
    .catchall {:try_start_23 .. :try_end_6f} :catchall_6d

    #@6f
    throw v4

    #@70
    .line 1613
    .restart local v0       #N:I
    .restart local v1       #canceledSomething:Z
    .restart local v2       #i:I
    :cond_70
    if-eqz v1, :cond_75

    #@72
    .line 1614
    :try_start_72
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V

    #@75
    .line 1616
    :cond_75
    monitor-exit v5
    :try_end_76
    .catchall {:try_start_72 .. :try_end_76} :catchall_6d

    #@76
    move v4, v1

    #@77
    goto :goto_62
.end method

.method public cancelNotificationWithTag(Ljava/lang/String;Ljava/lang/String;II)V
    .registers 13
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"
    .parameter "userId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1621
    invoke-virtual {p0, p1}, Lcom/android/server/NotificationManagerService;->checkCallerIsSystemOrSameApp(Ljava/lang/String;)V

    #@4
    .line 1622
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v0

    #@8
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@b
    move-result v1

    #@c
    const/4 v3, 0x1

    #@d
    const-string v5, "cancelNotificationWithTag"

    #@f
    move v2, p4

    #@10
    move-object v6, p1

    #@11
    invoke-static/range {v0 .. v6}, Landroid/app/ActivityManager;->handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I

    #@14
    move-result p4

    #@15
    .line 1625
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@18
    move-result v0

    #@19
    const/16 v1, 0x3e8

    #@1b
    if-ne v0, v1, :cond_28

    #@1d
    move v5, v4

    #@1e
    :goto_1e
    move-object v0, p0

    #@1f
    move-object v1, p1

    #@20
    move-object v2, p2

    #@21
    move v3, p3

    #@22
    move v6, v4

    #@23
    move v7, p4

    #@24
    invoke-direct/range {v0 .. v7}, Lcom/android/server/NotificationManagerService;->cancelNotification(Ljava/lang/String;Ljava/lang/String;IIIZI)V

    #@27
    .line 1628
    return-void

    #@28
    .line 1625
    :cond_28
    const/16 v5, 0x40

    #@2a
    goto :goto_1e
.end method

.method public cancelToast(Ljava/lang/String;Landroid/app/ITransientNotification;)V
    .registers 10
    .parameter "pkg"
    .parameter "callback"

    #@0
    .prologue
    .line 938
    const-string v3, "NotificationService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "cancelToast pkg="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, " callback="

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 940
    if-eqz p1, :cond_26

    #@24
    if-nez p2, :cond_49

    #@26
    .line 941
    :cond_26
    const-string v3, "NotificationService"

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "Not cancelling notification. pkg="

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, " callback="

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 958
    :goto_48
    return-void

    #@49
    .line 945
    :cond_49
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@4b
    monitor-enter v4

    #@4c
    .line 946
    :try_start_4c
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_5e

    #@4f
    move-result-wide v0

    #@50
    .line 948
    .local v0, callingId:J
    :try_start_50
    invoke-direct {p0, p1, p2}, Lcom/android/server/NotificationManagerService;->indexOfToastLocked(Ljava/lang/String;Landroid/app/ITransientNotification;)I

    #@53
    move-result v2

    #@54
    .line 949
    .local v2, index:I
    if-ltz v2, :cond_61

    #@56
    .line 950
    invoke-direct {p0, v2}, Lcom/android/server/NotificationManagerService;->cancelToastLocked(I)V
    :try_end_59
    .catchall {:try_start_50 .. :try_end_59} :catchall_84

    #@59
    .line 955
    :goto_59
    :try_start_59
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5c
    .line 957
    monitor-exit v4

    #@5d
    goto :goto_48

    #@5e
    .end local v0           #callingId:J
    .end local v2           #index:I
    :catchall_5e
    move-exception v3

    #@5f
    monitor-exit v4
    :try_end_60
    .catchall {:try_start_59 .. :try_end_60} :catchall_5e

    #@60
    throw v3

    #@61
    .line 952
    .restart local v0       #callingId:J
    .restart local v2       #index:I
    :cond_61
    :try_start_61
    const-string v3, "NotificationService"

    #@63
    new-instance v5, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v6, "Toast already cancelled. pkg="

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v5

    #@72
    const-string v6, " callback="

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v5

    #@80
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_83
    .catchall {:try_start_61 .. :try_end_83} :catchall_84

    #@83
    goto :goto_59

    #@84
    .line 955
    .end local v2           #index:I
    :catchall_84
    move-exception v3

    #@85
    :try_start_85
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@88
    throw v3
    :try_end_89
    .catchall {:try_start_85 .. :try_end_89} :catchall_5e
.end method

.method checkCallerIsSystem()V
    .registers 5

    #@0
    .prologue
    .line 1687
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 1688
    .local v0, uid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v1

    #@8
    const/16 v2, 0x3e8

    #@a
    if-eq v1, v2, :cond_e

    #@c
    if-nez v0, :cond_f

    #@e
    .line 1689
    :cond_e
    return-void

    #@f
    .line 1691
    :cond_f
    new-instance v1, Ljava/lang/SecurityException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Disallowed call for uid "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1
.end method

.method checkCallerIsSystemOrSameApp(Ljava/lang/String;)V
    .registers 8
    .parameter "pkg"

    #@0
    .prologue
    .line 1695
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v2

    #@4
    .line 1696
    .local v2, uid:I
    invoke-static {v2}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v3

    #@8
    const/16 v4, 0x3e8

    #@a
    if-eq v3, v4, :cond_e

    #@c
    if-nez v2, :cond_f

    #@e
    .line 1709
    :cond_e
    return-void

    #@f
    .line 1700
    :cond_f
    :try_start_f
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@12
    move-result-object v3

    #@13
    const/4 v4, 0x0

    #@14
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@17
    move-result v5

    #@18
    invoke-interface {v3, p1, v4, v5}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    #@1b
    move-result-object v0

    #@1c
    .line 1702
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1e
    invoke-static {v3, v2}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_e

    #@24
    .line 1703
    new-instance v3, Ljava/lang/SecurityException;

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "Calling uid "

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, " gave package"

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, " which is owned by uid "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@52
    throw v3
    :try_end_53
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_53} :catch_53

    #@53
    .line 1706
    .end local v0           #ai:Landroid/content/pm/ApplicationInfo;
    :catch_53
    move-exception v1

    #@54
    .line 1707
    .local v1, re:Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/SecurityException;

    #@56
    new-instance v4, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v5, "Unknown package "

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, "\n"

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v4

    #@73
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@76
    throw v3
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 10
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 1885
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DUMP"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_33

    #@a
    .line 1887
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Permission Denial: can\'t dump NotificationManager from from pid="

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v3

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ", uid="

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 1933
    :goto_32
    return-void

    #@33
    .line 1893
    :cond_33
    const-string v2, "Current Notification Manager state:"

    #@35
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 1897
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@3a
    monitor-enter v3

    #@3b
    .line 1898
    :try_start_3b
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@3d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@40
    move-result v0

    #@41
    .line 1899
    .local v0, N:I
    if-lez v0, :cond_60

    #@43
    .line 1900
    const-string v2, "  Toast Queue:"

    #@45
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@48
    .line 1901
    const/4 v1, 0x0

    #@49
    .local v1, i:I
    :goto_49
    if-ge v1, v0, :cond_5b

    #@4b
    .line 1902
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@50
    move-result-object v2

    #@51
    check-cast v2, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@53
    const-string v4, "    "

    #@55
    invoke-virtual {v2, p2, v4}, Lcom/android/server/NotificationManagerService$ToastRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@58
    .line 1901
    add-int/lit8 v1, v1, 0x1

    #@5a
    goto :goto_49

    #@5b
    .line 1904
    :cond_5b
    const-string v2, "  "

    #@5d
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@60
    .line 1907
    .end local v1           #i:I
    :cond_60
    monitor-exit v3
    :try_end_61
    .catchall {:try_start_3b .. :try_end_61} :catchall_86

    #@61
    .line 1909
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@63
    monitor-enter v3

    #@64
    .line 1910
    :try_start_64
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@66
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@69
    move-result v0

    #@6a
    .line 1911
    if-lez v0, :cond_8e

    #@6c
    .line 1912
    const-string v2, "  Notification List:"

    #@6e
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@71
    .line 1913
    const/4 v1, 0x0

    #@72
    .restart local v1       #i:I
    :goto_72
    if-ge v1, v0, :cond_89

    #@74
    .line 1914
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@76
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@79
    move-result-object v2

    #@7a
    check-cast v2, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@7c
    const-string v4, "    "

    #@7e
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@80
    invoke-virtual {v2, p2, v4, v5}, Lcom/android/server/NotificationManagerService$NotificationRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;Landroid/content/Context;)V
    :try_end_83
    .catchall {:try_start_64 .. :try_end_83} :catchall_11c

    #@83
    .line 1913
    add-int/lit8 v1, v1, 0x1

    #@85
    goto :goto_72

    #@86
    .line 1907
    .end local v0           #N:I
    .end local v1           #i:I
    :catchall_86
    move-exception v2

    #@87
    :try_start_87
    monitor-exit v3
    :try_end_88
    .catchall {:try_start_87 .. :try_end_88} :catchall_86

    #@88
    throw v2

    #@89
    .line 1916
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_89
    :try_start_89
    const-string v2, "  "

    #@8b
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8e
    .line 1919
    .end local v1           #i:I
    :cond_8e
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@90
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@93
    move-result v0

    #@94
    .line 1920
    if-lez v0, :cond_b5

    #@96
    .line 1921
    const-string v2, "  Lights List:"

    #@98
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9b
    .line 1922
    const/4 v1, 0x0

    #@9c
    .restart local v1       #i:I
    :goto_9c
    if-ge v1, v0, :cond_b0

    #@9e
    .line 1923
    iget-object v2, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@a0
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a3
    move-result-object v2

    #@a4
    check-cast v2, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@a6
    const-string v4, "    "

    #@a8
    iget-object v5, p0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@aa
    invoke-virtual {v2, p2, v4, v5}, Lcom/android/server/NotificationManagerService$NotificationRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;Landroid/content/Context;)V

    #@ad
    .line 1922
    add-int/lit8 v1, v1, 0x1

    #@af
    goto :goto_9c

    #@b0
    .line 1925
    :cond_b0
    const-string v2, "  "

    #@b2
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b5
    .line 1928
    .end local v1           #i:I
    :cond_b5
    new-instance v2, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v4, "  mSoundNotification="

    #@bc
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mSoundNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@c2
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v2

    #@ca
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@cd
    .line 1929
    new-instance v2, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v4, "  mVibrateNotification="

    #@d4
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v2

    #@d8
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mVibrateNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@da
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v2

    #@de
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v2

    #@e2
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e5
    .line 1930
    new-instance v2, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v4, "  mDisabledNotifications=0x"

    #@ec
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v2

    #@f0
    iget v4, p0, Lcom/android/server/NotificationManagerService;->mDisabledNotifications:I

    #@f2
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@f5
    move-result-object v4

    #@f6
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v2

    #@fa
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v2

    #@fe
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@101
    .line 1931
    new-instance v2, Ljava/lang/StringBuilder;

    #@103
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@106
    const-string v4, "  mSystemReady="

    #@108
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v2

    #@10c
    iget-boolean v4, p0, Lcom/android/server/NotificationManagerService;->mSystemReady:Z

    #@10e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@111
    move-result-object v2

    #@112
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v2

    #@116
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@119
    .line 1932
    monitor-exit v3

    #@11a
    goto/16 :goto_32

    #@11c
    :catchall_11c
    move-exception v2

    #@11d
    monitor-exit v3
    :try_end_11e
    .catchall {:try_start_89 .. :try_end_11e} :catchall_11c

    #@11e
    throw v2
.end method

.method public enqueueNotificationInternal(Ljava/lang/String;IILjava/lang/String;ILandroid/app/Notification;[II)V
    .registers 50
    .parameter "pkg"
    .parameter "callingUid"
    .parameter "callingPid"
    .parameter "tag"
    .parameter "id"
    .parameter "notification"
    .parameter "idOut"
    .parameter "userId"

    #@0
    .prologue
    .line 1102
    const-string v7, "NotificationService"

    #@2
    new-instance v8, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v9, "enqueueNotificationInternal: pkg="

    #@9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v8

    #@d
    move-object/from16 v0, p1

    #@f
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    const-string v9, " id="

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    move/from16 v0, p5

    #@1b
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v8

    #@1f
    const-string v9, " notification="

    #@21
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    move-object/from16 v0, p6

    #@27
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v8

    #@2f
    invoke-static {v7, v8}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1104
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/NotificationManagerService;->checkCallerIsSystemOrSameApp(Ljava/lang/String;)V

    #@35
    .line 1105
    const-string v7, "android"

    #@37
    move-object/from16 v0, p1

    #@39
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v29

    #@3d
    .line 1107
    .local v29, isSystemNotification:Z
    const/4 v8, 0x1

    #@3e
    const/4 v9, 0x0

    #@3f
    const-string v10, "enqueueNotification"

    #@41
    move/from16 v5, p3

    #@43
    move/from16 v6, p2

    #@45
    move/from16 v7, p8

    #@47
    move-object/from16 v11, p1

    #@49
    invoke-static/range {v5 .. v11}, Landroid/app/ActivityManager;->handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I

    #@4c
    move-result p8

    #@4d
    .line 1109
    new-instance v14, Landroid/os/UserHandle;

    #@4f
    move/from16 v0, p8

    #@51
    invoke-direct {v14, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@54
    .line 1113
    .local v14, user:Landroid/os/UserHandle;
    if-nez v29, :cond_bb

    #@56
    .line 1114
    move-object/from16 v0, p0

    #@58
    iget-object v8, v0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@5a
    monitor-enter v8

    #@5b
    .line 1115
    const/16 v20, 0x0

    #@5d
    .line 1116
    .local v20, count:I
    :try_start_5d
    move-object/from16 v0, p0

    #@5f
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@61
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@64
    move-result v15

    #@65
    .line 1117
    .local v15, N:I
    const/16 v25, 0x0

    #@67
    .local v25, i:I
    :goto_67
    move/from16 v0, v25

    #@69
    if-ge v0, v15, :cond_ba

    #@6b
    .line 1118
    move-object/from16 v0, p0

    #@6d
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@6f
    move/from16 v0, v25

    #@71
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@74
    move-result-object v5

    #@75
    check-cast v5, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@77
    .line 1119
    .local v5, r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    iget-object v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@79
    move-object/from16 v0, p1

    #@7b
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v7

    #@7f
    if-eqz v7, :cond_b7

    #@81
    iget v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->userId:I

    #@83
    move/from16 v0, p8

    #@85
    if-ne v7, v0, :cond_b7

    #@87
    .line 1120
    add-int/lit8 v20, v20, 0x1

    #@89
    .line 1121
    const/16 v7, 0x32

    #@8b
    move/from16 v0, v20

    #@8d
    if-lt v0, v7, :cond_b7

    #@8f
    .line 1122
    const-string v7, "NotificationService"

    #@91
    new-instance v9, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v10, "Package has already posted "

    #@98
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v9

    #@9c
    move/from16 v0, v20

    #@9e
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v9

    #@a2
    const-string v10, " notifications.  Not showing more.  package="

    #@a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    move-object/from16 v0, p1

    #@aa
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v9

    #@b2
    invoke-static {v7, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 1124
    monitor-exit v8

    #@b6
    .line 1430
    .end local v5           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .end local v15           #N:I
    .end local v20           #count:I
    .end local v25           #i:I
    :cond_b6
    :goto_b6
    return-void

    #@b7
    .line 1117
    .restart local v5       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .restart local v15       #N:I
    .restart local v20       #count:I
    .restart local v25       #i:I
    :cond_b7
    add-int/lit8 v25, v25, 0x1

    #@b9
    goto :goto_67

    #@ba
    .line 1128
    .end local v5           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_ba
    monitor-exit v8
    :try_end_bb
    .catchall {:try_start_5d .. :try_end_bb} :catchall_128

    #@bb
    .line 1133
    .end local v15           #N:I
    .end local v20           #count:I
    .end local v25           #i:I
    :cond_bb
    const-string v7, "com.android.providers.downloads"

    #@bd
    move-object/from16 v0, p1

    #@bf
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c2
    move-result v7

    #@c3
    if-eqz v7, :cond_ce

    #@c5
    const-string v7, "DownloadManager"

    #@c7
    const/4 v8, 0x2

    #@c8
    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@cb
    move-result v7

    #@cc
    if-eqz v7, :cond_f1

    #@ce
    .line 1135
    :cond_ce
    const/16 v7, 0xabe

    #@d0
    const/4 v8, 0x5

    #@d1
    new-array v8, v8, [Ljava/lang/Object;

    #@d3
    const/4 v9, 0x0

    #@d4
    aput-object p1, v8, v9

    #@d6
    const/4 v9, 0x1

    #@d7
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@da
    move-result-object v10

    #@db
    aput-object v10, v8, v9

    #@dd
    const/4 v9, 0x2

    #@de
    aput-object p4, v8, v9

    #@e0
    const/4 v9, 0x3

    #@e1
    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e4
    move-result-object v10

    #@e5
    aput-object v10, v8, v9

    #@e7
    const/4 v9, 0x4

    #@e8
    invoke-virtual/range {p6 .. p6}, Landroid/app/Notification;->toString()Ljava/lang/String;

    #@eb
    move-result-object v10

    #@ec
    aput-object v10, v8, v9

    #@ee
    invoke-static {v7, v8}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@f1
    .line 1139
    :cond_f1
    if-eqz p1, :cond_f5

    #@f3
    if-nez p6, :cond_12b

    #@f5
    .line 1140
    :cond_f5
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@f7
    new-instance v8, Ljava/lang/StringBuilder;

    #@f9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@fc
    const-string v9, "null not allowed: pkg="

    #@fe
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v8

    #@102
    move-object/from16 v0, p1

    #@104
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v8

    #@108
    const-string v9, " id="

    #@10a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v8

    #@10e
    move/from16 v0, p5

    #@110
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@113
    move-result-object v8

    #@114
    const-string v9, " notification="

    #@116
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v8

    #@11a
    move-object/from16 v0, p6

    #@11c
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v8

    #@120
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v8

    #@124
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@127
    throw v7

    #@128
    .line 1128
    .restart local v20       #count:I
    :catchall_128
    move-exception v7

    #@129
    :try_start_129
    monitor-exit v8
    :try_end_12a
    .catchall {:try_start_129 .. :try_end_12a} :catchall_128

    #@12a
    throw v7

    #@12b
    .line 1143
    .end local v20           #count:I
    :cond_12b
    move-object/from16 v0, p6

    #@12d
    iget v7, v0, Landroid/app/Notification;->icon:I

    #@12f
    if-eqz v7, :cond_16a

    #@131
    .line 1144
    move-object/from16 v0, p6

    #@133
    iget-object v7, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@135
    if-nez v7, :cond_16a

    #@137
    .line 1145
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@139
    new-instance v8, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v9, "contentView required: pkg="

    #@140
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v8

    #@144
    move-object/from16 v0, p1

    #@146
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v8

    #@14a
    const-string v9, " id="

    #@14c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v8

    #@150
    move/from16 v0, p5

    #@152
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@155
    move-result-object v8

    #@156
    const-string v9, " notification="

    #@158
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v8

    #@15c
    move-object/from16 v0, p6

    #@15e
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v8

    #@162
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@165
    move-result-object v8

    #@166
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@169
    throw v7

    #@16a
    .line 1153
    :cond_16a
    move-object/from16 v0, p6

    #@16c
    iget v7, v0, Landroid/app/Notification;->priority:I

    #@16e
    const/4 v8, -0x2

    #@16f
    const/4 v9, 0x2

    #@170
    invoke-static {v7, v8, v9}, Lcom/android/server/NotificationManagerService;->clamp(III)I

    #@173
    move-result v7

    #@174
    move-object/from16 v0, p6

    #@176
    iput v7, v0, Landroid/app/Notification;->priority:I

    #@178
    .line 1155
    move-object/from16 v0, p6

    #@17a
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@17c
    and-int/lit16 v7, v7, 0x80

    #@17e
    if-eqz v7, :cond_18c

    #@180
    .line 1156
    move-object/from16 v0, p6

    #@182
    iget v7, v0, Landroid/app/Notification;->priority:I

    #@184
    const/4 v8, 0x2

    #@185
    if-ge v7, v8, :cond_18c

    #@187
    const/4 v7, 0x2

    #@188
    move-object/from16 v0, p6

    #@18a
    iput v7, v0, Landroid/app/Notification;->priority:I

    #@18c
    .line 1162
    :cond_18c
    move-object/from16 v0, p6

    #@18e
    iget v7, v0, Landroid/app/Notification;->priority:I

    #@190
    mul-int/lit8 v12, v7, 0xa

    #@192
    .line 1169
    .local v12, score:I
    if-nez v29, :cond_1bc

    #@194
    invoke-direct/range {p0 .. p1}, Lcom/android/server/NotificationManagerService;->areNotificationsEnabledForPackageInt(Ljava/lang/String;)Z

    #@197
    move-result v7

    #@198
    if-nez v7, :cond_1bc

    #@19a
    .line 1170
    const/16 v12, -0x3e8

    #@19c
    .line 1171
    const-string v7, "NotificationService"

    #@19e
    new-instance v8, Ljava/lang/StringBuilder;

    #@1a0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1a3
    const-string v9, "Suppressing notification from package "

    #@1a5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v8

    #@1a9
    move-object/from16 v0, p1

    #@1ab
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v8

    #@1af
    const-string v9, " by user request."

    #@1b1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v8

    #@1b5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b8
    move-result-object v8

    #@1b9
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1bc
    .line 1178
    :cond_1bc
    const/16 v7, -0x14

    #@1be
    if-lt v12, v7, :cond_b6

    #@1c0
    .line 1184
    const/16 v7, -0xa

    #@1c2
    if-lt v12, v7, :cond_3cb

    #@1c4
    const/16 v18, 0x1

    #@1c6
    .line 1186
    .local v18, canInterrupt:Z
    :goto_1c6
    move-object/from16 v0, p0

    #@1c8
    iget-object v0, v0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@1ca
    move-object/from16 v40, v0

    #@1cc
    monitor-enter v40

    #@1cd
    .line 1187
    :try_start_1cd
    new-instance v5, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@1cf
    move-object/from16 v6, p1

    #@1d1
    move-object/from16 v7, p4

    #@1d3
    move/from16 v8, p5

    #@1d5
    move/from16 v9, p2

    #@1d7
    move/from16 v10, p3

    #@1d9
    move/from16 v11, p8

    #@1db
    move-object/from16 v13, p6

    #@1dd
    invoke-direct/range {v5 .. v13}, Lcom/android/server/NotificationManagerService$NotificationRecord;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIILandroid/app/Notification;)V

    #@1e0
    .line 1191
    .restart local v5       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    const/16 v31, 0x0

    #@1e2
    .line 1194
    .local v31, old:Lcom/android/server/NotificationManagerService$NotificationRecord;
    invoke-direct/range {p0 .. p1}, Lcom/android/server/NotificationManagerService;->isExceptionalPackage(Ljava/lang/String;)Z

    #@1e5
    move-result v7

    #@1e6
    if-eqz v7, :cond_1eb

    #@1e8
    .line 1195
    const/4 v7, 0x1

    #@1e9
    iput-boolean v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->mExceptional:Z

    #@1eb
    .line 1199
    :cond_1eb
    move-object/from16 v0, p0

    #@1ed
    move-object/from16 v1, p1

    #@1ef
    move-object/from16 v2, p4

    #@1f1
    move/from16 v3, p5

    #@1f3
    move/from16 v4, p8

    #@1f5
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/NotificationManagerService;->indexOfNotificationLocked(Ljava/lang/String;Ljava/lang/String;II)I

    #@1f8
    move-result v28

    #@1f9
    .line 1200
    .local v28, index:I
    if-gez v28, :cond_3cf

    #@1fb
    .line 1201
    move-object/from16 v0, p0

    #@1fd
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@1ff
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@202
    .line 1214
    :cond_202
    :goto_202
    move-object/from16 v0, p6

    #@204
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@206
    and-int/lit8 v7, v7, 0x40

    #@208
    if-eqz v7, :cond_214

    #@20a
    .line 1215
    move-object/from16 v0, p6

    #@20c
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@20e
    or-int/lit8 v7, v7, 0x22

    #@210
    move-object/from16 v0, p6

    #@212
    iput v7, v0, Landroid/app/Notification;->flags:I

    #@214
    .line 1220
    :cond_214
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_217
    .catchall {:try_start_1cd .. :try_end_217} :catchall_3f9

    #@217
    move-result-wide v35

    #@218
    .line 1222
    .local v35, token:J
    :try_start_218
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I
    :try_end_21b
    .catchall {:try_start_218 .. :try_end_21b} :catchall_3fc

    #@21b
    move-result v21

    #@21c
    .line 1224
    .local v21, currentUser:I
    :try_start_21c
    invoke-static/range {v35 .. v36}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@21f
    .line 1227
    move-object/from16 v0, p6

    #@221
    iget v7, v0, Landroid/app/Notification;->icon:I

    #@223
    if-eqz v7, :cond_42f

    #@225
    .line 1228
    new-instance v6, Lcom/android/internal/statusbar/StatusBarNotification;

    #@227
    iget v10, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->uid:I

    #@229
    iget v11, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->initialPid:I

    #@22b
    move-object/from16 v7, p1

    #@22d
    move/from16 v8, p5

    #@22f
    move-object/from16 v9, p4

    #@231
    move-object/from16 v13, p6

    #@233
    invoke-direct/range {v6 .. v14}, Lcom/android/internal/statusbar/StatusBarNotification;-><init>(Ljava/lang/String;ILjava/lang/String;IIILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@236
    .line 1230
    .local v6, n:Lcom/android/internal/statusbar/StatusBarNotification;
    if-eqz v31, :cond_406

    #@238
    move-object/from16 v0, v31

    #@23a
    iget-object v7, v0, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@23c
    if-eqz v7, :cond_406

    #@23e
    .line 1231
    move-object/from16 v0, v31

    #@240
    iget-object v7, v0, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@242
    iput-object v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@244
    .line 1232
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_247
    .catchall {:try_start_21c .. :try_end_247} :catchall_3f9

    #@247
    move-result-wide v26

    #@248
    .line 1234
    .local v26, identity:J
    :try_start_248
    move-object/from16 v0, p0

    #@24a
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@24c
    iget-object v8, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@24e
    invoke-virtual {v7, v8, v6}, Lcom/android/server/StatusBarManagerService;->updateNotification(Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;)V
    :try_end_251
    .catchall {:try_start_248 .. :try_end_251} :catchall_401

    #@251
    .line 1237
    :try_start_251
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@254
    .line 1253
    :goto_254
    move/from16 v0, v21

    #@256
    move/from16 v1, p8

    #@258
    if-ne v0, v1, :cond_263

    #@25a
    .line 1254
    move-object/from16 v0, p0

    #@25c
    move-object/from16 v1, p6

    #@25e
    move-object/from16 v2, p1

    #@260
    invoke-direct {v0, v1, v2}, Lcom/android/server/NotificationManagerService;->sendAccessibilityEvent(Landroid/app/Notification;Ljava/lang/CharSequence;)V

    #@263
    .line 1257
    :cond_263
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@265
    if-eqz v7, :cond_270

    #@267
    .line 1258
    move-object/from16 v0, p0

    #@269
    move-object/from16 v1, p6

    #@26b
    move-object/from16 v2, p1

    #@26d
    invoke-direct {v0, v1, v2}, Lcom/android/server/NotificationManagerService;->sendOSPEvent(Landroid/app/Notification;Ljava/lang/CharSequence;)V

    #@270
    .line 1262
    :cond_270
    move-object/from16 v0, p0

    #@272
    iget-boolean v7, v0, Lcom/android/server/NotificationManagerService;->mBootCompleted:Z

    #@274
    if-eqz v7, :cond_280

    #@276
    .line 1263
    iget-object v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@278
    iget-object v8, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@27a
    const/4 v9, 0x1

    #@27b
    move-object/from16 v0, p0

    #@27d
    invoke-direct {v0, v7, v8, v9}, Lcom/android/server/NotificationManagerService;->sendNotificationToTMode(Ljava/lang/String;Landroid/app/Notification;Z)V

    #@280
    .line 1280
    .end local v6           #n:Lcom/android/internal/statusbar/StatusBarNotification;
    .end local v26           #identity:J
    :cond_280
    :goto_280
    move-object/from16 v0, p0

    #@282
    iget v7, v0, Lcom/android/server/NotificationManagerService;->mDisabledNotifications:I

    #@284
    const/high16 v8, 0x4

    #@286
    and-int/2addr v7, v8

    #@287
    if-nez v7, :cond_38b

    #@289
    if-eqz v31, :cond_293

    #@28b
    move-object/from16 v0, p6

    #@28d
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@28f
    and-int/lit8 v7, v7, 0x8

    #@291
    if-nez v7, :cond_38b

    #@293
    :cond_293
    iget v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->userId:I

    #@295
    const/4 v8, -0x1

    #@296
    if-eq v7, v8, :cond_2a4

    #@298
    iget v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->userId:I

    #@29a
    move/from16 v0, p8

    #@29c
    if-ne v7, v0, :cond_38b

    #@29e
    iget v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->userId:I

    #@2a0
    move/from16 v0, v21

    #@2a2
    if-ne v7, v0, :cond_38b

    #@2a4
    :cond_2a4
    if-eqz v18, :cond_38b

    #@2a6
    move-object/from16 v0, p0

    #@2a8
    iget-boolean v7, v0, Lcom/android/server/NotificationManagerService;->mSystemReady:Z

    #@2aa
    if-eqz v7, :cond_38b

    #@2ac
    .line 1288
    move-object/from16 v0, p0

    #@2ae
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@2b0
    const-string v8, "audio"

    #@2b2
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2b5
    move-result-object v16

    #@2b6
    check-cast v16, Landroid/media/AudioManager;

    #@2b8
    .line 1292
    .local v16, audioManager:Landroid/media/AudioManager;
    move-object/from16 v0, p6

    #@2ba
    iget v7, v0, Landroid/app/Notification;->defaults:I

    #@2bc
    and-int/lit8 v7, v7, 0x1

    #@2be
    if-eqz v7, :cond_46a

    #@2c0
    const/16 v37, 0x1

    #@2c2
    .line 1295
    .local v37, useDefaultSound:Z
    :goto_2c2
    const/16 v34, 0x0

    #@2c4
    .line 1296
    .local v34, soundUri:Landroid/net/Uri;
    const/16 v24, 0x0

    #@2c6
    .line 1298
    .local v24, hasValidSound:Z
    if-eqz v37, :cond_472

    #@2c8
    .line 1299
    sget-object v34, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    #@2ca
    .line 1302
    move-object/from16 v0, p0

    #@2cc
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@2ce
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2d1
    move-result-object v33

    #@2d2
    .line 1303
    .local v33, resolver:Landroid/content/ContentResolver;
    const-string v7, "notification_sound"

    #@2d4
    move-object/from16 v0, v33

    #@2d6
    invoke-static {v0, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@2d9
    move-result-object v7

    #@2da
    if-eqz v7, :cond_46e

    #@2dc
    const/16 v24, 0x1

    #@2de
    .line 1310
    .end local v33           #resolver:Landroid/content/ContentResolver;
    :cond_2de
    :goto_2de
    if-eqz v24, :cond_329

    #@2e0
    .line 1311
    move-object/from16 v0, p6

    #@2e2
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@2e4
    and-int/lit8 v7, v7, 0x4

    #@2e6
    if-eqz v7, :cond_497

    #@2e8
    const/16 v30, 0x1

    #@2ea
    .line 1313
    .local v30, looping:Z
    :goto_2ea
    move-object/from16 v0, p6

    #@2ec
    iget v7, v0, Landroid/app/Notification;->audioStreamType:I

    #@2ee
    if-ltz v7, :cond_49b

    #@2f0
    .line 1314
    move-object/from16 v0, p6

    #@2f2
    iget v0, v0, Landroid/app/Notification;->audioStreamType:I

    #@2f4
    move/from16 v17, v0

    #@2f6
    .line 1318
    .local v17, audioStreamType:I
    :goto_2f6
    move-object/from16 v0, p0

    #@2f8
    iput-object v5, v0, Lcom/android/server/NotificationManagerService;->mSoundNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@2fa
    .line 1321
    invoke-virtual/range {v16 .. v17}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@2fd
    move-result v7

    #@2fe
    if-eqz v7, :cond_329

    #@300
    invoke-virtual/range {v16 .. v16}, Landroid/media/AudioManager;->isSpeechRecognitionActive()Z

    #@303
    move-result v7

    #@304
    if-nez v7, :cond_329

    #@306
    .line 1323
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_309
    .catchall {:try_start_251 .. :try_end_309} :catchall_3f9

    #@309
    move-result-wide v26

    #@30a
    .line 1325
    .restart local v26       #identity:J
    :try_start_30a
    move-object/from16 v0, p0

    #@30c
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mAudioService:Landroid/media/IAudioService;

    #@30e
    invoke-interface {v7}, Landroid/media/IAudioService;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@311
    move-result-object v32

    #@312
    .line 1326
    .local v32, player:Landroid/media/IRingtonePlayer;
    if-eqz v32, :cond_326

    #@314
    .line 1329
    const/4 v7, -0x1

    #@315
    move/from16 v0, p8

    #@317
    if-ne v0, v7, :cond_49f

    #@319
    .line 1332
    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@31b
    move-object/from16 v0, v32

    #@31d
    move-object/from16 v1, v34

    #@31f
    move/from16 v2, v30

    #@321
    move/from16 v3, v17

    #@323
    invoke-interface {v0, v1, v7, v2, v3}, Landroid/media/IRingtonePlayer;->playAsync(Landroid/net/Uri;Landroid/os/UserHandle;ZI)V
    :try_end_326
    .catchall {:try_start_30a .. :try_end_326} :catchall_4b2
    .catch Landroid/os/RemoteException; {:try_start_30a .. :try_end_326} :catch_4ac

    #@326
    .line 1340
    :cond_326
    :goto_326
    :try_start_326
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@329
    .line 1347
    .end local v17           #audioStreamType:I
    .end local v26           #identity:J
    .end local v30           #looping:Z
    .end local v32           #player:Landroid/media/IRingtonePlayer;
    :cond_329
    :goto_329
    move-object/from16 v0, p6

    #@32b
    iget-object v7, v0, Landroid/app/Notification;->vibrate:[J

    #@32d
    if-eqz v7, :cond_4b7

    #@32f
    const/16 v23, 0x1

    #@331
    .line 1351
    .local v23, hasCustomVibrate:Z
    :goto_331
    if-nez v23, :cond_4bb

    #@333
    if-eqz v24, :cond_4bb

    #@335
    invoke-virtual/range {v16 .. v16}, Landroid/media/AudioManager;->getRingerMode()I

    #@338
    move-result v7

    #@339
    const/4 v8, 0x1

    #@33a
    if-ne v7, v8, :cond_4bb

    #@33c
    const/16 v19, 0x1

    #@33e
    .line 1357
    .local v19, convertSoundToVibration:Z
    :goto_33e
    move-object/from16 v0, p6

    #@340
    iget v7, v0, Landroid/app/Notification;->defaults:I

    #@342
    and-int/lit8 v7, v7, 0x2

    #@344
    if-eqz v7, :cond_4bf

    #@346
    const/16 v38, 0x1

    #@348
    .line 1360
    .local v38, useDefaultVibrate:Z
    :goto_348
    if-nez v38, :cond_34e

    #@34a
    if-nez v19, :cond_34e

    #@34c
    if-eqz v23, :cond_38b

    #@34e
    :cond_34e
    invoke-virtual/range {v16 .. v16}, Landroid/media/AudioManager;->isRecording()Z

    #@351
    move-result v7

    #@352
    if-nez v7, :cond_38b

    #@354
    invoke-virtual/range {v16 .. v16}, Landroid/media/AudioManager;->getRingerMode()I

    #@357
    move-result v7

    #@358
    if-eqz v7, :cond_38b

    #@35a
    .line 1363
    move-object/from16 v0, p0

    #@35c
    iput-object v5, v0, Lcom/android/server/NotificationManagerService;->mVibrateNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@35e
    .line 1367
    if-nez v38, :cond_362

    #@360
    if-eqz v19, :cond_4df

    #@362
    .line 1370
    :cond_362
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_365
    .catchall {:try_start_326 .. :try_end_365} :catchall_3f9

    #@365
    move-result-wide v26

    #@366
    .line 1372
    .restart local v26       #identity:J
    :try_start_366
    sget-object v7, Lcom/android/server/NotificationManagerService;->DEFAULT_VIBRATE_PATTERN:[J

    #@368
    array-length v7, v7

    #@369
    move-object/from16 v0, p0

    #@36b
    invoke-direct {v0, v7}, Lcom/android/server/NotificationManagerService;->getVibratorVolumePattern(I)[I

    #@36e
    move-result-object v39

    #@36f
    .line 1374
    .local v39, vibratePattern:[I
    move-object/from16 v0, p0

    #@371
    iget-object v9, v0, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@373
    if-eqz v19, :cond_4c3

    #@375
    move-object/from16 v0, p0

    #@377
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mFallbackVibrationPattern:[J

    #@379
    move-object v8, v7

    #@37a
    :goto_37a
    move-object/from16 v0, p6

    #@37c
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@37e
    and-int/lit8 v7, v7, 0x4

    #@380
    if-eqz v7, :cond_4ca

    #@382
    const/4 v7, 0x0

    #@383
    :goto_383
    move-object/from16 v0, v39

    #@385
    invoke-interface {v9, v8, v7, v0}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->vibrate([JI[I)V
    :try_end_388
    .catchall {:try_start_366 .. :try_end_388} :catchall_4da
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_366 .. :try_end_388} :catch_4cd

    #@388
    .line 1380
    :try_start_388
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@38b
    .line 1401
    .end local v16           #audioManager:Landroid/media/AudioManager;
    .end local v19           #convertSoundToVibration:Z
    .end local v23           #hasCustomVibrate:Z
    .end local v24           #hasValidSound:Z
    .end local v26           #identity:J
    .end local v34           #soundUri:Landroid/net/Uri;
    .end local v37           #useDefaultSound:Z
    .end local v38           #useDefaultVibrate:Z
    .end local v39           #vibratePattern:[I
    :cond_38b
    :goto_38b
    move-object/from16 v0, p0

    #@38d
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@38f
    move-object/from16 v0, v31

    #@391
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@394
    .line 1404
    sget v7, Lcom/android/server/NotificationManagerService;->ACTION_REMOVE_RECORD:I

    #@396
    move-object/from16 v0, p0

    #@398
    move-object/from16 v1, v31

    #@39a
    invoke-direct {v0, v1, v7}, Lcom/android/server/NotificationManagerService;->updateLightListLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;I)V

    #@39d
    .line 1407
    move-object/from16 v0, p0

    #@39f
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@3a1
    move-object/from16 v0, v31

    #@3a3
    if-ne v7, v0, :cond_3aa

    #@3a5
    .line 1408
    const/4 v7, 0x0

    #@3a6
    move-object/from16 v0, p0

    #@3a8
    iput-object v7, v0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@3aa
    .line 1412
    :cond_3aa
    move-object/from16 v0, p6

    #@3ac
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@3ae
    and-int/lit8 v7, v7, 0x1

    #@3b0
    if-eqz v7, :cond_516

    #@3b2
    if-eqz v18, :cond_516

    #@3b4
    .line 1414
    move-object/from16 v0, p0

    #@3b6
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@3b8
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3bb
    .line 1417
    sget v7, Lcom/android/server/NotificationManagerService;->ACTION_ADD_RECORD:I

    #@3bd
    move-object/from16 v0, p0

    #@3bf
    invoke-direct {v0, v5, v7}, Lcom/android/server/NotificationManagerService;->updateLightListLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;I)V

    #@3c2
    .line 1420
    invoke-direct/range {p0 .. p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V

    #@3c5
    .line 1427
    :cond_3c5
    :goto_3c5
    monitor-exit v40
    :try_end_3c6
    .catchall {:try_start_388 .. :try_end_3c6} :catchall_3f9

    #@3c6
    .line 1429
    const/4 v7, 0x0

    #@3c7
    aput p5, p7, v7

    #@3c9
    goto/16 :goto_b6

    #@3cb
    .line 1184
    .end local v5           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .end local v18           #canInterrupt:Z
    .end local v21           #currentUser:I
    .end local v28           #index:I
    .end local v31           #old:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .end local v35           #token:J
    :cond_3cb
    const/16 v18, 0x0

    #@3cd
    goto/16 :goto_1c6

    #@3cf
    .line 1203
    .restart local v5       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .restart local v18       #canInterrupt:Z
    .restart local v28       #index:I
    .restart local v31       #old:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_3cf
    :try_start_3cf
    move-object/from16 v0, p0

    #@3d1
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@3d3
    move/from16 v0, v28

    #@3d5
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@3d8
    move-result-object v31

    #@3d9
    .end local v31           #old:Lcom/android/server/NotificationManagerService$NotificationRecord;
    check-cast v31, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@3db
    .line 1204
    .restart local v31       #old:Lcom/android/server/NotificationManagerService$NotificationRecord;
    move-object/from16 v0, p0

    #@3dd
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@3df
    move/from16 v0, v28

    #@3e1
    invoke-virtual {v7, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@3e4
    .line 1206
    if-eqz v31, :cond_202

    #@3e6
    .line 1207
    move-object/from16 v0, p6

    #@3e8
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@3ea
    move-object/from16 v0, v31

    #@3ec
    iget-object v8, v0, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@3ee
    iget v8, v8, Landroid/app/Notification;->flags:I

    #@3f0
    and-int/lit8 v8, v8, 0x40

    #@3f2
    or-int/2addr v7, v8

    #@3f3
    move-object/from16 v0, p6

    #@3f5
    iput v7, v0, Landroid/app/Notification;->flags:I

    #@3f7
    goto/16 :goto_202

    #@3f9
    .line 1427
    .end local v5           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .end local v28           #index:I
    .end local v31           #old:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :catchall_3f9
    move-exception v7

    #@3fa
    monitor-exit v40
    :try_end_3fb
    .catchall {:try_start_3cf .. :try_end_3fb} :catchall_3f9

    #@3fb
    throw v7

    #@3fc
    .line 1224
    .restart local v5       #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .restart local v28       #index:I
    .restart local v31       #old:Lcom/android/server/NotificationManagerService$NotificationRecord;
    .restart local v35       #token:J
    :catchall_3fc
    move-exception v7

    #@3fd
    :try_start_3fd
    invoke-static/range {v35 .. v36}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@400
    throw v7

    #@401
    .line 1237
    .restart local v6       #n:Lcom/android/internal/statusbar/StatusBarNotification;
    .restart local v21       #currentUser:I
    .restart local v26       #identity:J
    :catchall_401
    move-exception v7

    #@402
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@405
    throw v7

    #@406
    .line 1240
    .end local v26           #identity:J
    :cond_406
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_409
    .catchall {:try_start_3fd .. :try_end_409} :catchall_3f9

    #@409
    move-result-wide v26

    #@40a
    .line 1242
    .restart local v26       #identity:J
    :try_start_40a
    move-object/from16 v0, p0

    #@40c
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@40e
    invoke-virtual {v7, v6}, Lcom/android/server/StatusBarManagerService;->addNotification(Lcom/android/internal/statusbar/StatusBarNotification;)Landroid/os/IBinder;

    #@411
    move-result-object v7

    #@412
    iput-object v7, v5, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@414
    .line 1243
    iget-object v7, v6, Lcom/android/internal/statusbar/StatusBarNotification;->notification:Landroid/app/Notification;

    #@416
    iget v7, v7, Landroid/app/Notification;->flags:I

    #@418
    and-int/lit8 v7, v7, 0x1

    #@41a
    if-eqz v7, :cond_425

    #@41c
    if-eqz v18, :cond_425

    #@41e
    .line 1245
    move-object/from16 v0, p0

    #@420
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mAttentionLight:Lcom/android/server/LightsService$Light;

    #@422
    invoke-virtual {v7}, Lcom/android/server/LightsService$Light;->pulse()V
    :try_end_425
    .catchall {:try_start_40a .. :try_end_425} :catchall_42a

    #@425
    .line 1249
    :cond_425
    :try_start_425
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@428
    goto/16 :goto_254

    #@42a
    :catchall_42a
    move-exception v7

    #@42b
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@42e
    throw v7

    #@42f
    .line 1267
    .end local v6           #n:Lcom/android/internal/statusbar/StatusBarNotification;
    .end local v26           #identity:J
    :cond_42f
    const-string v7, "NotificationService"

    #@431
    new-instance v8, Ljava/lang/StringBuilder;

    #@433
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@436
    const-string v9, "Ignoring notification with icon==0: "

    #@438
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43b
    move-result-object v8

    #@43c
    move-object/from16 v0, p6

    #@43e
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@441
    move-result-object v8

    #@442
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@445
    move-result-object v8

    #@446
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@449
    .line 1268
    if-eqz v31, :cond_280

    #@44b
    move-object/from16 v0, v31

    #@44d
    iget-object v7, v0, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@44f
    if-eqz v7, :cond_280

    #@451
    .line 1269
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_454
    .catchall {:try_start_425 .. :try_end_454} :catchall_3f9

    #@454
    move-result-wide v26

    #@455
    .line 1271
    .restart local v26       #identity:J
    :try_start_455
    move-object/from16 v0, p0

    #@457
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@459
    move-object/from16 v0, v31

    #@45b
    iget-object v8, v0, Lcom/android/server/NotificationManagerService$NotificationRecord;->statusBarKey:Landroid/os/IBinder;

    #@45d
    invoke-virtual {v7, v8}, Lcom/android/server/StatusBarManagerService;->removeNotification(Landroid/os/IBinder;)V
    :try_end_460
    .catchall {:try_start_455 .. :try_end_460} :catchall_465

    #@460
    .line 1274
    :try_start_460
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@463
    goto/16 :goto_280

    #@465
    :catchall_465
    move-exception v7

    #@466
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@469
    throw v7

    #@46a
    .line 1292
    .end local v26           #identity:J
    .restart local v16       #audioManager:Landroid/media/AudioManager;
    :cond_46a
    const/16 v37, 0x0

    #@46c
    goto/16 :goto_2c2

    #@46e
    .line 1303
    .restart local v24       #hasValidSound:Z
    .restart local v33       #resolver:Landroid/content/ContentResolver;
    .restart local v34       #soundUri:Landroid/net/Uri;
    .restart local v37       #useDefaultSound:Z
    :cond_46e
    const/16 v24, 0x0

    #@470
    goto/16 :goto_2de

    #@472
    .line 1305
    .end local v33           #resolver:Landroid/content/ContentResolver;
    :cond_472
    move-object/from16 v0, p6

    #@474
    iget-object v7, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@476
    if-eqz v7, :cond_2de

    #@478
    move-object/from16 v0, p6

    #@47a
    iget-object v7, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@47c
    const-string v8, ""

    #@47e
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@481
    move-result-object v8

    #@482
    invoke-virtual {v7, v8}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@485
    move-result v7

    #@486
    if-nez v7, :cond_2de

    #@488
    .line 1306
    move-object/from16 v0, p6

    #@48a
    iget-object v0, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@48c
    move-object/from16 v34, v0
    :try_end_48e
    .catchall {:try_start_460 .. :try_end_48e} :catchall_3f9

    #@48e
    .line 1307
    if-eqz v34, :cond_494

    #@490
    const/16 v24, 0x1

    #@492
    :goto_492
    goto/16 :goto_2de

    #@494
    :cond_494
    const/16 v24, 0x0

    #@496
    goto :goto_492

    #@497
    .line 1311
    :cond_497
    const/16 v30, 0x0

    #@499
    goto/16 :goto_2ea

    #@49b
    .line 1316
    .restart local v30       #looping:Z
    :cond_49b
    const/16 v17, 0x5

    #@49d
    .restart local v17       #audioStreamType:I
    goto/16 :goto_2f6

    #@49f
    .line 1334
    .restart local v26       #identity:J
    .restart local v32       #player:Landroid/media/IRingtonePlayer;
    :cond_49f
    :try_start_49f
    move-object/from16 v0, v32

    #@4a1
    move-object/from16 v1, v34

    #@4a3
    move/from16 v2, v30

    #@4a5
    move/from16 v3, v17

    #@4a7
    invoke-interface {v0, v1, v14, v2, v3}, Landroid/media/IRingtonePlayer;->playAsync(Landroid/net/Uri;Landroid/os/UserHandle;ZI)V
    :try_end_4aa
    .catchall {:try_start_49f .. :try_end_4aa} :catchall_4b2
    .catch Landroid/os/RemoteException; {:try_start_49f .. :try_end_4aa} :catch_4ac

    #@4aa
    goto/16 :goto_326

    #@4ac
    .line 1338
    .end local v32           #player:Landroid/media/IRingtonePlayer;
    :catch_4ac
    move-exception v7

    #@4ad
    .line 1340
    :try_start_4ad
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b0
    goto/16 :goto_329

    #@4b2
    :catchall_4b2
    move-exception v7

    #@4b3
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b6
    throw v7
    :try_end_4b7
    .catchall {:try_start_4ad .. :try_end_4b7} :catchall_3f9

    #@4b7
    .line 1347
    .end local v17           #audioStreamType:I
    .end local v26           #identity:J
    .end local v30           #looping:Z
    :cond_4b7
    const/16 v23, 0x0

    #@4b9
    goto/16 :goto_331

    #@4bb
    .line 1351
    .restart local v23       #hasCustomVibrate:Z
    :cond_4bb
    const/16 v19, 0x0

    #@4bd
    goto/16 :goto_33e

    #@4bf
    .line 1357
    .restart local v19       #convertSoundToVibration:Z
    :cond_4bf
    const/16 v38, 0x0

    #@4c1
    goto/16 :goto_348

    #@4c3
    .line 1374
    .restart local v26       #identity:J
    .restart local v38       #useDefaultVibrate:Z
    .restart local v39       #vibratePattern:[I
    :cond_4c3
    :try_start_4c3
    move-object/from16 v0, p0

    #@4c5
    iget-object v7, v0, Lcom/android/server/NotificationManagerService;->mDefaultVibrationPattern:[J
    :try_end_4c7
    .catchall {:try_start_4c3 .. :try_end_4c7} :catchall_4da
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4c3 .. :try_end_4c7} :catch_4cd

    #@4c7
    move-object v8, v7

    #@4c8
    goto/16 :goto_37a

    #@4ca
    :cond_4ca
    const/4 v7, -0x1

    #@4cb
    goto/16 :goto_383

    #@4cd
    .line 1377
    .end local v39           #vibratePattern:[I
    :catch_4cd
    move-exception v22

    #@4ce
    .line 1378
    .local v22, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    :try_start_4ce
    const-string v7, "NotificationService"

    #@4d0
    const-string v8, "Unexpected ArrayIndexOutOfBoundsException was thrown"

    #@4d2
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4d5
    .catchall {:try_start_4ce .. :try_end_4d5} :catchall_4da

    #@4d5
    .line 1380
    :try_start_4d5
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4d8
    goto/16 :goto_38b

    #@4da
    .end local v22           #e:Ljava/lang/ArrayIndexOutOfBoundsException;
    :catchall_4da
    move-exception v7

    #@4db
    invoke-static/range {v26 .. v27}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4de
    throw v7

    #@4df
    .line 1382
    .end local v26           #identity:J
    :cond_4df
    move-object/from16 v0, p6

    #@4e1
    iget-object v7, v0, Landroid/app/Notification;->vibrate:[J

    #@4e3
    array-length v7, v7

    #@4e4
    const/4 v8, 0x1

    #@4e5
    if-le v7, v8, :cond_38b

    #@4e7
    .line 1384
    move-object/from16 v0, p6

    #@4e9
    iget-object v7, v0, Landroid/app/Notification;->vibrate:[J

    #@4eb
    array-length v7, v7

    #@4ec
    move-object/from16 v0, p0

    #@4ee
    invoke-direct {v0, v7}, Lcom/android/server/NotificationManagerService;->getVibratorVolumePattern(I)[I
    :try_end_4f1
    .catchall {:try_start_4d5 .. :try_end_4f1} :catchall_3f9

    #@4f1
    move-result-object v39

    #@4f2
    .line 1387
    .restart local v39       #vibratePattern:[I
    :try_start_4f2
    move-object/from16 v0, p0

    #@4f4
    iget-object v8, v0, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@4f6
    move-object/from16 v0, p6

    #@4f8
    iget-object v9, v0, Landroid/app/Notification;->vibrate:[J

    #@4fa
    move-object/from16 v0, p6

    #@4fc
    iget v7, v0, Landroid/app/Notification;->flags:I

    #@4fe
    and-int/lit8 v7, v7, 0x4

    #@500
    if-eqz v7, :cond_514

    #@502
    const/4 v7, 0x0

    #@503
    :goto_503
    move-object/from16 v0, v39

    #@505
    invoke-interface {v8, v9, v7, v0}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->vibrate([JI[I)V
    :try_end_508
    .catchall {:try_start_4f2 .. :try_end_508} :catchall_3f9
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4f2 .. :try_end_508} :catch_50a

    #@508
    goto/16 :goto_38b

    #@50a
    .line 1389
    :catch_50a
    move-exception v22

    #@50b
    .line 1390
    .restart local v22       #e:Ljava/lang/ArrayIndexOutOfBoundsException;
    :try_start_50b
    const-string v7, "NotificationService"

    #@50d
    const-string v8, "Unexpected ArrayIndexOutOfBoundsException was thrown"

    #@50f
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@512
    goto/16 :goto_38b

    #@514
    .line 1387
    .end local v22           #e:Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_514
    const/4 v7, -0x1

    #@515
    goto :goto_503

    #@516
    .line 1422
    .end local v16           #audioManager:Landroid/media/AudioManager;
    .end local v19           #convertSoundToVibration:Z
    .end local v23           #hasCustomVibrate:Z
    .end local v24           #hasValidSound:Z
    .end local v34           #soundUri:Landroid/net/Uri;
    .end local v37           #useDefaultSound:Z
    .end local v38           #useDefaultVibrate:Z
    .end local v39           #vibratePattern:[I
    :cond_516
    if-eqz v31, :cond_3c5

    #@518
    move-object/from16 v0, v31

    #@51a
    iget-object v7, v0, Lcom/android/server/NotificationManagerService$NotificationRecord;->notification:Landroid/app/Notification;

    #@51c
    iget v7, v7, Landroid/app/Notification;->flags:I

    #@51e
    and-int/lit8 v7, v7, 0x1

    #@520
    if-eqz v7, :cond_3c5

    #@522
    .line 1424
    invoke-direct/range {p0 .. p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V
    :try_end_525
    .catchall {:try_start_50b .. :try_end_525} :catchall_3f9

    #@525
    goto/16 :goto_3c5
.end method

.method public enqueueNotificationWithTag(Ljava/lang/String;Ljava/lang/String;ILandroid/app/Notification;[II)V
    .registers 16
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"
    .parameter "notification"
    .parameter "idOut"
    .parameter "userId"

    #@0
    .prologue
    .line 1088
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v2

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v3

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move-object v4, p2

    #@b
    move v5, p3

    #@c
    move-object v6, p4

    #@d
    move-object v7, p5

    #@e
    move v8, p6

    #@f
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/NotificationManagerService;->enqueueNotificationInternal(Ljava/lang/String;IILjava/lang/String;ILandroid/app/Notification;[II)V

    #@12
    .line 1090
    return-void
.end method

.method public enqueueToast(Ljava/lang/String;Landroid/app/ITransientNotification;I)V
    .registers 21
    .parameter "pkg"
    .parameter "callback"
    .parameter "duration"

    #@0
    .prologue
    .line 877
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_2b

    #@4
    .line 878
    :cond_4
    const-string v13, "NotificationService"

    #@6
    new-instance v14, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v15, "Not doing toast. pkg="

    #@d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v14

    #@11
    move-object/from16 v0, p1

    #@13
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v14

    #@17
    const-string v15, " callback="

    #@19
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v14

    #@1d
    move-object/from16 v0, p2

    #@1f
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v14

    #@23
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v14

    #@27
    invoke-static {v13, v14}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 935
    :goto_2a
    return-void

    #@2b
    .line 882
    :cond_2b
    const-string v13, "android"

    #@2d
    move-object/from16 v0, p1

    #@2f
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v10

    #@33
    .line 884
    .local v10, isSystemToast:Z
    if-nez v10, :cond_5c

    #@35
    invoke-direct/range {p0 .. p1}, Lcom/android/server/NotificationManagerService;->areNotificationsEnabledForPackageInt(Ljava/lang/String;)Z

    #@38
    move-result v13

    #@39
    if-nez v13, :cond_5c

    #@3b
    .line 885
    const-string v13, "NotificationService"

    #@3d
    new-instance v14, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v15, "Suppressing toast from package "

    #@44
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v14

    #@48
    move-object/from16 v0, p1

    #@4a
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v14

    #@4e
    const-string v15, " by user request."

    #@50
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v14

    #@54
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v14

    #@58
    invoke-static {v13, v14}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_2a

    #@5c
    .line 889
    :cond_5c
    move-object/from16 v0, p0

    #@5e
    iget-object v14, v0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@60
    monitor-enter v14

    #@61
    .line 890
    :try_start_61
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@64
    move-result v6

    #@65
    .line 891
    .local v6, callingPid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_68
    .catchall {:try_start_61 .. :try_end_68} :catchall_88

    #@68
    move-result-wide v4

    #@69
    .line 894
    .local v4, callingId:J
    :try_start_69
    invoke-direct/range {p0 .. p2}, Lcom/android/server/NotificationManagerService;->indexOfToastLocked(Ljava/lang/String;Landroid/app/ITransientNotification;)I

    #@6c
    move-result v9

    #@6d
    .line 897
    .local v9, index:I
    if-ltz v9, :cond_8b

    #@6f
    .line 898
    move-object/from16 v0, p0

    #@71
    iget-object v13, v0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@73
    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@76
    move-result-object v12

    #@77
    check-cast v12, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@79
    .line 899
    .local v12, record:Lcom/android/server/NotificationManagerService$ToastRecord;
    move/from16 v0, p3

    #@7b
    invoke-virtual {v12, v0}, Lcom/android/server/NotificationManagerService$ToastRecord;->update(I)V

    #@7e
    .line 928
    :goto_7e
    if-nez v9, :cond_83

    #@80
    .line 929
    invoke-direct/range {p0 .. p0}, Lcom/android/server/NotificationManagerService;->showNextToastLocked()V
    :try_end_83
    .catchall {:try_start_69 .. :try_end_83} :catchall_103

    #@83
    .line 932
    :cond_83
    :try_start_83
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@86
    .line 934
    monitor-exit v14

    #@87
    goto :goto_2a

    #@88
    .end local v4           #callingId:J
    .end local v6           #callingPid:I
    .end local v9           #index:I
    .end local v12           #record:Lcom/android/server/NotificationManagerService$ToastRecord;
    :catchall_88
    move-exception v13

    #@89
    monitor-exit v14
    :try_end_8a
    .catchall {:try_start_83 .. :try_end_8a} :catchall_88

    #@8a
    throw v13

    #@8b
    .line 903
    .restart local v4       #callingId:J
    .restart local v6       #callingPid:I
    .restart local v9       #index:I
    :cond_8b
    if-nez v10, :cond_e0

    #@8d
    .line 904
    const/4 v7, 0x0

    #@8e
    .line 905
    .local v7, count:I
    :try_start_8e
    move-object/from16 v0, p0

    #@90
    iget-object v13, v0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@92
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@95
    move-result v3

    #@96
    .line 906
    .local v3, N:I
    const/4 v8, 0x0

    #@97
    .local v8, i:I
    :goto_97
    if-ge v8, v3, :cond_e0

    #@99
    .line 907
    move-object/from16 v0, p0

    #@9b
    iget-object v13, v0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@9d
    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a0
    move-result-object v11

    #@a1
    check-cast v11, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@a3
    .line 908
    .local v11, r:Lcom/android/server/NotificationManagerService$ToastRecord;
    iget-object v13, v11, Lcom/android/server/NotificationManagerService$ToastRecord;->pkg:Ljava/lang/String;

    #@a5
    move-object/from16 v0, p1

    #@a7
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v13

    #@ab
    if-eqz v13, :cond_dd

    #@ad
    .line 909
    add-int/lit8 v7, v7, 0x1

    #@af
    .line 910
    const/16 v13, 0x32

    #@b1
    if-lt v7, v13, :cond_dd

    #@b3
    .line 911
    const-string v13, "NotificationService"

    #@b5
    new-instance v15, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v16, "Package has already posted "

    #@bc
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v15

    #@c0
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v15

    #@c4
    const-string v16, " toasts. Not showing more. Package="

    #@c6
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v15

    #@ca
    move-object/from16 v0, p1

    #@cc
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v15

    #@d0
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v15

    #@d4
    invoke-static {v13, v15}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d7
    .catchall {:try_start_8e .. :try_end_d7} :catchall_103

    #@d7
    .line 932
    :try_start_d7
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@da
    .line 913
    monitor-exit v14
    :try_end_db
    .catchall {:try_start_d7 .. :try_end_db} :catchall_88

    #@db
    goto/16 :goto_2a

    #@dd
    .line 906
    :cond_dd
    add-int/lit8 v8, v8, 0x1

    #@df
    goto :goto_97

    #@e0
    .line 919
    .end local v3           #N:I
    .end local v7           #count:I
    .end local v8           #i:I
    .end local v11           #r:Lcom/android/server/NotificationManagerService$ToastRecord;
    :cond_e0
    :try_start_e0
    new-instance v12, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@e2
    move-object/from16 v0, p1

    #@e4
    move-object/from16 v1, p2

    #@e6
    move/from16 v2, p3

    #@e8
    invoke-direct {v12, v6, v0, v1, v2}, Lcom/android/server/NotificationManagerService$ToastRecord;-><init>(ILjava/lang/String;Landroid/app/ITransientNotification;I)V

    #@eb
    .line 920
    .restart local v12       #record:Lcom/android/server/NotificationManagerService$ToastRecord;
    move-object/from16 v0, p0

    #@ed
    iget-object v13, v0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@ef
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f2
    .line 921
    move-object/from16 v0, p0

    #@f4
    iget-object v13, v0, Lcom/android/server/NotificationManagerService;->mToastQueue:Ljava/util/ArrayList;

    #@f6
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@f9
    move-result v13

    #@fa
    add-int/lit8 v9, v13, -0x1

    #@fc
    .line 922
    move-object/from16 v0, p0

    #@fe
    invoke-direct {v0, v6}, Lcom/android/server/NotificationManagerService;->keepProcessAliveLocked(I)V
    :try_end_101
    .catchall {:try_start_e0 .. :try_end_101} :catchall_103

    #@101
    goto/16 :goto_7e

    #@103
    .line 932
    .end local v9           #index:I
    .end local v12           #record:Lcom/android/server/NotificationManagerService$ToastRecord;
    :catchall_103
    move-exception v13

    #@104
    :try_start_104
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@107
    throw v13
    :try_end_108
    .catchall {:try_start_104 .. :try_end_108} :catchall_88
.end method

.method public setNotificationsEnabledForPackage(Ljava/lang/String;Z)V
    .registers 8
    .parameter "pkg"
    .parameter "enabled"

    #@0
    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/android/server/NotificationManagerService;->checkCallerIsSystem()V

    #@3
    .line 326
    if-eqz p2, :cond_e

    #@5
    .line 327
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@7
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@a
    .line 345
    :goto_a
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->writeBlockDb()V

    #@d
    .line 346
    return-void

    #@e
    .line 329
    :cond_e
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mBlockedPackages:Ljava/util/HashSet;

    #@10
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@13
    .line 333
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@15
    monitor-enter v4

    #@16
    .line 334
    :try_start_16
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v0

    #@1c
    .line 335
    .local v0, N:I
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    if-ge v1, v0, :cond_36

    #@1f
    .line 336
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@27
    .line 337
    .local v2, r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    iget-object v3, v2, Lcom/android/server/NotificationManagerService$NotificationRecord;->pkg:Ljava/lang/String;

    #@29
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_33

    #@2f
    .line 338
    const/4 v3, 0x0

    #@30
    invoke-direct {p0, v2, v3}, Lcom/android/server/NotificationManagerService;->cancelNotificationLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;Z)V

    #@33
    .line 335
    :cond_33
    add-int/lit8 v1, v1, 0x1

    #@35
    goto :goto_1d

    #@36
    .line 341
    .end local v2           #r:Lcom/android/server/NotificationManagerService$NotificationRecord;
    :cond_36
    monitor-exit v4

    #@37
    goto :goto_a

    #@38
    .end local v0           #N:I
    .end local v1           #i:I
    :catchall_38
    move-exception v3

    #@39
    monitor-exit v4
    :try_end_3a
    .catchall {:try_start_16 .. :try_end_3a} :catchall_38

    #@3a
    throw v3
.end method

.method public stopAlertSound()V
    .registers 7

    #@0
    .prologue
    .line 1651
    iget-object v4, p0, Lcom/android/server/NotificationManagerService;->mNotificationList:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 1653
    const/4 v3, 0x0

    #@4
    :try_start_4
    iput-object v3, p0, Lcom/android/server/NotificationManagerService;->mSoundNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@6
    .line 1654
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_3f

    #@9
    move-result-wide v0

    #@a
    .line 1656
    .local v0, identity:J
    :try_start_a
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mAudioService:Landroid/media/IAudioService;

    #@c
    invoke-interface {v3}, Landroid/media/IAudioService;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@f
    move-result-object v2

    #@10
    .line 1657
    .local v2, player:Landroid/media/IRingtonePlayer;
    if-eqz v2, :cond_15

    #@12
    .line 1658
    invoke-interface {v2}, Landroid/media/IRingtonePlayer;->stopAsync()V
    :try_end_15
    .catchall {:try_start_a .. :try_end_15} :catchall_3a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_15} :catch_47

    #@15
    .line 1662
    :cond_15
    :try_start_15
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@18
    .line 1666
    .end local v2           #player:Landroid/media/IRingtonePlayer;
    :goto_18
    const/4 v3, 0x0

    #@19
    iput-object v3, p0, Lcom/android/server/NotificationManagerService;->mVibrateNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@1b
    .line 1667
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_3f

    #@1e
    move-result-wide v0

    #@1f
    .line 1669
    :try_start_1f
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@21
    invoke-interface {v3}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->cancel()V
    :try_end_24
    .catchall {:try_start_1f .. :try_end_24} :catchall_42

    #@24
    .line 1671
    :try_start_24
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@27
    .line 1675
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mLights:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@2c
    .line 1677
    const/4 v3, 0x0

    #@2d
    sget v5, Lcom/android/server/NotificationManagerService;->ACTION_CLEAR_RECORD:I

    #@2f
    invoke-direct {p0, v3, v5}, Lcom/android/server/NotificationManagerService;->updateLightListLocked(Lcom/android/server/NotificationManagerService$NotificationRecord;I)V

    #@32
    .line 1679
    const/4 v3, 0x0

    #@33
    iput-object v3, p0, Lcom/android/server/NotificationManagerService;->mLedNotification:Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@35
    .line 1680
    invoke-direct {p0}, Lcom/android/server/NotificationManagerService;->updateLightsLocked()V

    #@38
    .line 1681
    monitor-exit v4

    #@39
    .line 1682
    return-void

    #@3a
    .line 1662
    :catchall_3a
    move-exception v3

    #@3b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3e
    throw v3

    #@3f
    .line 1681
    .end local v0           #identity:J
    :catchall_3f
    move-exception v3

    #@40
    monitor-exit v4
    :try_end_41
    .catchall {:try_start_24 .. :try_end_41} :catchall_3f

    #@41
    throw v3

    #@42
    .line 1671
    .restart local v0       #identity:J
    :catchall_42
    move-exception v3

    #@43
    :try_start_43
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@46
    throw v3

    #@47
    .line 1660
    :catch_47
    move-exception v3

    #@48
    .line 1662
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_4b
    .catchall {:try_start_43 .. :try_end_4b} :catchall_3f

    #@4b
    goto :goto_18
.end method

.method public stopSound()V
    .registers 5

    #@0
    .prologue
    .line 2044
    invoke-virtual {p0}, Lcom/android/server/NotificationManagerService;->checkCallerIsSystem()V

    #@3
    .line 2046
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@6
    move-result-wide v0

    #@7
    .line 2048
    .local v0, identity:J
    :try_start_7
    iget-object v3, p0, Lcom/android/server/NotificationManagerService;->mAudioService:Landroid/media/IAudioService;

    #@9
    invoke-interface {v3}, Landroid/media/IAudioService;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@c
    move-result-object v2

    #@d
    .line 2049
    .local v2, player:Landroid/media/IRingtonePlayer;
    if-eqz v2, :cond_12

    #@f
    .line 2050
    invoke-interface {v2}, Landroid/media/IRingtonePlayer;->stopAsync()V
    :try_end_12
    .catchall {:try_start_7 .. :try_end_12} :catchall_16
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_12} :catch_1b

    #@12
    .line 2054
    .end local v2           #player:Landroid/media/IRingtonePlayer;
    :cond_12
    :goto_12
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@15
    .line 2056
    return-void

    #@16
    .line 2054
    :catchall_16
    move-exception v3

    #@17
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1a
    throw v3

    #@1b
    .line 2052
    :catch_1b
    move-exception v3

    #@1c
    goto :goto_12
.end method

.method systemReady()V
    .registers 2

    #@0
    .prologue
    .line 864
    const-string v0, "audio"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/server/NotificationManagerService;->mAudioService:Landroid/media/IAudioService;

    #@c
    .line 868
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Lcom/android/server/NotificationManagerService;->mSystemReady:Z

    #@f
    .line 869
    return-void
.end method
