.class Lcom/android/server/NativeDaemonConnector$ResponseQueue;
.super Ljava/lang/Object;
.source "NativeDaemonConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NativeDaemonConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResponseQueue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    }
.end annotation


# instance fields
.field private mMaxCount:I

.field private final mPendingCmds:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(I)V
    .registers 3
    .parameter "maxCount"

    #@0
    .prologue
    .line 542
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 543
    new-instance v0, Ljava/util/LinkedList;

    #@5
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@a
    .line 544
    iput p1, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mMaxCount:I

    #@c
    .line 545
    return-void
.end method


# virtual methods
.method public add(ILcom/android/server/NativeDaemonEvent;)V
    .registers 11
    .parameter "cmdNum"
    .parameter "response"

    #@0
    .prologue
    .line 548
    const/4 v0, 0x0

    #@1
    .line 549
    .local v0, found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    iget-object v5, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@3
    monitor-enter v5

    #@4
    .line 550
    :try_start_4
    iget-object v4, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@6
    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v2

    #@a
    .local v2, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_b2

    #@10
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;

    #@16
    .line 551
    .local v3, pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    iget v4, v3, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->cmdNum:I
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_ae

    #@18
    if-ne v4, p1, :cond_a

    #@1a
    .line 552
    move-object v0, v3

    #@1b
    move-object v1, v0

    #@1c
    .line 556
    .end local v0           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .end local v3           #pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .local v1, found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :goto_1c
    if-nez v1, :cond_b0

    #@1e
    .line 558
    :goto_1e
    :try_start_1e
    iget-object v4, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@20
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    #@23
    move-result v4

    #@24
    iget v6, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mMaxCount:I

    #@26
    if-lt v4, v6, :cond_8b

    #@28
    .line 559
    const-string v4, "NativeDaemonConnector.ResponseQueue"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "more buffered than allowed: "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    iget-object v7, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@37
    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    #@3a
    move-result v7

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, " >= "

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    iget v7, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mMaxCount:I

    #@47
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    invoke-static {v4, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 563
    iget-object v4, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@54
    invoke-virtual {v4}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    #@57
    move-result-object v3

    #@58
    check-cast v3, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;

    #@5a
    .line 564
    .restart local v3       #pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    const-string v4, "NativeDaemonConnector.ResponseQueue"

    #@5c
    new-instance v6, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v7, "Removing request: "

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    iget-object v7, v3, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->request:Ljava/lang/String;

    #@69
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    const-string v7, " ("

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    iget v7, v3, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->cmdNum:I

    #@75
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    const-string v7, ")"

    #@7b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v6

    #@83
    invoke-static {v4, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_86
    .catchall {:try_start_1e .. :try_end_86} :catchall_87

    #@86
    goto :goto_1e

    #@87
    .line 575
    .end local v3           #pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :catchall_87
    move-exception v4

    #@88
    move-object v0, v1

    #@89
    .end local v1           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .end local v2           #i$:Ljava/util/Iterator;
    .restart local v0       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :goto_89
    :try_start_89
    monitor-exit v5
    :try_end_8a
    .catchall {:try_start_89 .. :try_end_8a} :catchall_ae

    #@8a
    throw v4

    #@8b
    .line 568
    .end local v0           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v1       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_8b
    :try_start_8b
    new-instance v0, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;

    #@8d
    const/4 v4, 0x0

    #@8e
    invoke-direct {v0, p1, v4}, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;-><init>(ILjava/lang/String;)V
    :try_end_91
    .catchall {:try_start_8b .. :try_end_91} :catchall_87

    #@91
    .line 569
    .end local v1           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v0       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :try_start_91
    iget-object v4, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@93
    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@96
    .line 571
    :goto_96
    iget v4, v0, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->availableResponseCount:I

    #@98
    add-int/lit8 v4, v4, 0x1

    #@9a
    iput v4, v0, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->availableResponseCount:I

    #@9c
    .line 574
    iget v4, v0, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->availableResponseCount:I

    #@9e
    if-nez v4, :cond_a5

    #@a0
    iget-object v4, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@a2
    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    #@a5
    .line 575
    :cond_a5
    monitor-exit v5
    :try_end_a6
    .catchall {:try_start_91 .. :try_end_a6} :catchall_ae

    #@a6
    .line 577
    :try_start_a6
    iget-object v4, v0, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->responses:Ljava/util/concurrent/BlockingQueue;

    #@a8
    invoke-interface {v4, p2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_ab
    .catch Ljava/lang/InterruptedException; {:try_start_a6 .. :try_end_ab} :catch_ac

    #@ab
    .line 579
    :goto_ab
    return-void

    #@ac
    .line 578
    :catch_ac
    move-exception v4

    #@ad
    goto :goto_ab

    #@ae
    .line 575
    .end local v2           #i$:Ljava/util/Iterator;
    :catchall_ae
    move-exception v4

    #@af
    goto :goto_89

    #@b0
    .end local v0           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v1       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_b0
    move-object v0, v1

    #@b1
    .end local v1           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v0       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    goto :goto_96

    #@b2
    :cond_b2
    move-object v1, v0

    #@b3
    .end local v0           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v1       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    goto/16 :goto_1c
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 612
    const-string v2, "Pending requests:"

    #@2
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 613
    iget-object v3, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@7
    monitor-enter v3

    #@8
    .line 614
    :try_start_8
    iget-object v2, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@a
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v0

    #@e
    .local v0, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_42

    #@14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;

    #@1a
    .line 615
    .local v1, pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "  Cmd "

    #@21
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget v4, v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->cmdNum:I

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v4, " - "

    #@2d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-object v4, v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->request:Ljava/lang/String;

    #@33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e
    goto :goto_e

    #@3f
    .line 617
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :catchall_3f
    move-exception v2

    #@40
    monitor-exit v3
    :try_end_41
    .catchall {:try_start_8 .. :try_end_41} :catchall_3f

    #@41
    throw v2

    #@42
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_42
    :try_start_42
    monitor-exit v3
    :try_end_43
    .catchall {:try_start_42 .. :try_end_43} :catchall_3f

    #@43
    .line 618
    return-void
.end method

.method public remove(IILjava/lang/String;)Lcom/android/server/NativeDaemonEvent;
    .registers 14
    .parameter "cmdNum"
    .parameter "timeoutMs"
    .parameter "origCmd"

    #@0
    .prologue
    .line 584
    const/4 v1, 0x0

    #@1
    .line 585
    .local v1, found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    iget-object v7, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@3
    monitor-enter v7

    #@4
    .line 586
    :try_start_4
    iget-object v6, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@6
    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v3

    #@a
    .local v3, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_5a

    #@10
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;

    #@16
    .line 587
    .local v4, pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    iget v6, v4, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->cmdNum:I
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_50

    #@18
    if-ne v6, p1, :cond_a

    #@1a
    .line 588
    move-object v1, v4

    #@1b
    move-object v2, v1

    #@1c
    .line 592
    .end local v1           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .end local v4           #pendingCmd:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .local v2, found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :goto_1c
    if-nez v2, :cond_58

    #@1e
    .line 593
    :try_start_1e
    new-instance v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;

    #@20
    invoke-direct {v1, p1, p3}, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;-><init>(ILjava/lang/String;)V
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_55

    #@23
    .line 594
    .end local v2           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v1       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :try_start_23
    iget-object v6, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@25
    invoke-virtual {v6, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@28
    .line 596
    :goto_28
    iget v6, v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->availableResponseCount:I

    #@2a
    add-int/lit8 v6, v6, -0x1

    #@2c
    iput v6, v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->availableResponseCount:I

    #@2e
    .line 599
    iget v6, v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->availableResponseCount:I

    #@30
    if-nez v6, :cond_37

    #@32
    iget-object v6, p0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->mPendingCmds:Ljava/util/LinkedList;

    #@34
    invoke-virtual {v6, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    #@37
    .line 600
    :cond_37
    monitor-exit v7
    :try_end_38
    .catchall {:try_start_23 .. :try_end_38} :catchall_50

    #@38
    .line 601
    const/4 v5, 0x0

    #@39
    .line 603
    .local v5, result:Lcom/android/server/NativeDaemonEvent;
    :try_start_39
    iget-object v6, v1, Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;->responses:Ljava/util/concurrent/BlockingQueue;

    #@3b
    int-to-long v7, p2

    #@3c
    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@3e
    invoke-interface {v6, v7, v8, v9}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    #@41
    move-result-object v6

    #@42
    move-object v0, v6

    #@43
    check-cast v0, Lcom/android/server/NativeDaemonEvent;

    #@45
    move-object v5, v0
    :try_end_46
    .catch Ljava/lang/InterruptedException; {:try_start_39 .. :try_end_46} :catch_53

    #@46
    .line 605
    :goto_46
    if-nez v5, :cond_4f

    #@48
    .line 606
    const-string v6, "NativeDaemonConnector.ResponseQueue"

    #@4a
    const-string v7, "Timeout waiting for response"

    #@4c
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 608
    :cond_4f
    return-object v5

    #@50
    .line 600
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v5           #result:Lcom/android/server/NativeDaemonEvent;
    :catchall_50
    move-exception v6

    #@51
    :goto_51
    :try_start_51
    monitor-exit v7
    :try_end_52
    .catchall {:try_start_51 .. :try_end_52} :catchall_50

    #@52
    throw v6

    #@53
    .line 604
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v5       #result:Lcom/android/server/NativeDaemonEvent;
    :catch_53
    move-exception v6

    #@54
    goto :goto_46

    #@55
    .line 600
    .end local v1           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .end local v5           #result:Lcom/android/server/NativeDaemonEvent;
    .restart local v2       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :catchall_55
    move-exception v6

    #@56
    move-object v1, v2

    #@57
    .end local v2           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v1       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    goto :goto_51

    #@58
    .end local v1           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v2       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    :cond_58
    move-object v1, v2

    #@59
    .end local v2           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v1       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    goto :goto_28

    #@5a
    :cond_5a
    move-object v2, v1

    #@5b
    .end local v1           #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    .restart local v2       #found:Lcom/android/server/NativeDaemonConnector$ResponseQueue$PendingCmd;
    goto :goto_1c
.end method
