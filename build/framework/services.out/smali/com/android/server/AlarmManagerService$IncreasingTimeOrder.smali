.class public Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;
.super Ljava/lang/Object;
.source "AlarmManagerService.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AlarmManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IncreasingTimeOrder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/server/AlarmManagerService$Alarm;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 778
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Lcom/android/server/AlarmManagerService$Alarm;Lcom/android/server/AlarmManagerService$Alarm;)I
    .registers 11
    .parameter "a1"
    .parameter "a2"

    #@0
    .prologue
    const-wide/16 v6, 0x0

    #@2
    .line 780
    iget-wide v0, p1, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@4
    .line 781
    .local v0, when1:J
    iget-wide v2, p2, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@6
    .line 782
    .local v2, when2:J
    sub-long v4, v0, v2

    #@8
    cmp-long v4, v4, v6

    #@a
    if-lez v4, :cond_e

    #@c
    .line 783
    const/4 v4, 0x1

    #@d
    .line 788
    :goto_d
    return v4

    #@e
    .line 785
    :cond_e
    sub-long v4, v0, v2

    #@10
    cmp-long v4, v4, v6

    #@12
    if-gez v4, :cond_16

    #@14
    .line 786
    const/4 v4, -0x1

    #@15
    goto :goto_d

    #@16
    .line 788
    :cond_16
    const/4 v4, 0x0

    #@17
    goto :goto_d
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 778
    check-cast p1, Lcom/android/server/AlarmManagerService$Alarm;

    #@2
    .end local p1
    check-cast p2, Lcom/android/server/AlarmManagerService$Alarm;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;->compare(Lcom/android/server/AlarmManagerService$Alarm;Lcom/android/server/AlarmManagerService$Alarm;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
