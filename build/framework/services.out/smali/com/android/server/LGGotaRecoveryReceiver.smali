.class public Lcom/android/server/LGGotaRecoveryReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LGGotaRecoveryReceiver.java"


# static fields
.field private static ACTION_START_GOTA:Ljava/lang/String; = null

.field private static final MISC_PATH:Ljava/lang/String; = "/dev/block/platform/msm_sdcc.1/by-name/misc"

.field private static final TAG:Ljava/lang/String; = "LGGotaRecoveryReceiver"

.field private static value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    const-string v0, "com.lge.intent.action.GOTA_START"

    #@2
    sput-object v0, Lcom/android/server/LGGotaRecoveryReceiver;->ACTION_START_GOTA:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 43
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 44
    .local v0, action:Ljava/lang/String;
    const-string v8, "PACKAGE_FILENAME"

    #@6
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v8

    #@a
    sput-object v8, Lcom/android/server/LGGotaRecoveryReceiver;->value:Ljava/lang/String;

    #@c
    .line 46
    sget-object v8, Lcom/android/server/LGGotaRecoveryReceiver;->value:Ljava/lang/String;

    #@e
    if-nez v8, :cond_11

    #@10
    .line 78
    :goto_10
    return-void

    #@11
    .line 51
    :cond_11
    :try_start_11
    sget-object v8, Lcom/android/server/LGGotaRecoveryReceiver;->ACTION_START_GOTA:Ljava/lang/String;

    #@13
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v8

    #@17
    if-eqz v8, :cond_4d

    #@19
    .line 52
    const-string v1, "boot-recovery"

    #@1b
    .line 53
    .local v1, command:Ljava/lang/String;
    const-string v6, "recovery"

    #@1d
    .line 54
    .local v6, recovery1:Ljava/lang/String;
    const-string v7, "--update_package="

    #@1f
    .line 55
    .local v7, recovery2:Ljava/lang/String;
    new-instance v5, Ljava/io/RandomAccessFile;

    #@21
    const-string v8, "/dev/block/platform/msm_sdcc.1/by-name/misc"

    #@23
    const-string v9, "rw"

    #@25
    invoke-direct {v5, v8, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_28} :catch_61

    #@28
    .line 57
    .local v5, misc:Ljava/io/RandomAccessFile;
    :try_start_28
    invoke-virtual {v5, v1}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    #@2b
    .line 58
    const-wide/16 v8, 0x40

    #@2d
    invoke-virtual {v5, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    #@30
    .line 59
    invoke-virtual {v5, v6}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    #@33
    .line 60
    const-string v8, "\n"

    #@35
    invoke-virtual {v5, v8}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    #@38
    .line 61
    invoke-virtual {v5, v7}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    #@3b
    .line 62
    sget-object v8, Lcom/android/server/LGGotaRecoveryReceiver;->value:Ljava/lang/String;

    #@3d
    invoke-virtual {v5, v8}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    #@40
    .line 63
    const-string v8, "\u0000"

    #@42
    invoke-virtual {v5, v8}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    #@45
    .line 64
    const-string v8, "\n"

    #@47
    invoke-virtual {v5, v8}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V
    :try_end_4a
    .catchall {:try_start_28 .. :try_end_4a} :catchall_76
    .catch Ljava/io/FileNotFoundException; {:try_start_28 .. :try_end_4a} :catch_55
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_4a} :catch_6a

    #@4a
    .line 70
    :try_start_4a
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_61

    #@4d
    .line 77
    .end local v1           #command:Ljava/lang/String;
    .end local v5           #misc:Ljava/io/RandomAccessFile;
    .end local v6           #recovery1:Ljava/lang/String;
    .end local v7           #recovery2:Ljava/lang/String;
    :cond_4d
    :goto_4d
    const-string v8, "LGGotaRecoveryReceiver"

    #@4f
    const-string v9, "LGGotaRecoveryReceiver ACTION_GOTA_START"

    #@51
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_10

    #@55
    .line 65
    .restart local v1       #command:Ljava/lang/String;
    .restart local v5       #misc:Ljava/io/RandomAccessFile;
    .restart local v6       #recovery1:Ljava/lang/String;
    .restart local v7       #recovery2:Ljava/lang/String;
    :catch_55
    move-exception v3

    #@56
    .line 66
    .local v3, fne:Ljava/io/FileNotFoundException;
    :try_start_56
    const-string v8, "LGGotaRecoveryReceiver"

    #@58
    const-string v9, "LGGotaRecoveryReceiver-"

    #@5a
    invoke-static {v8, v9, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5d
    .catchall {:try_start_56 .. :try_end_5d} :catchall_76

    #@5d
    .line 70
    :try_start_5d
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V
    :try_end_60
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_60} :catch_61

    #@60
    goto :goto_4d

    #@61
    .line 73
    .end local v1           #command:Ljava/lang/String;
    .end local v3           #fne:Ljava/io/FileNotFoundException;
    .end local v5           #misc:Ljava/io/RandomAccessFile;
    .end local v6           #recovery1:Ljava/lang/String;
    .end local v7           #recovery2:Ljava/lang/String;
    :catch_61
    move-exception v4

    #@62
    .line 74
    .local v4, ie:Ljava/io/IOException;
    const-string v8, "LGGotaRecoveryReceiver"

    #@64
    const-string v9, "LGGotaRecoveryReceiver-"

    #@66
    invoke-static {v8, v9, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@69
    goto :goto_4d

    #@6a
    .line 67
    .end local v4           #ie:Ljava/io/IOException;
    .restart local v1       #command:Ljava/lang/String;
    .restart local v5       #misc:Ljava/io/RandomAccessFile;
    .restart local v6       #recovery1:Ljava/lang/String;
    .restart local v7       #recovery2:Ljava/lang/String;
    :catch_6a
    move-exception v2

    #@6b
    .line 68
    .local v2, e:Ljava/lang/Exception;
    :try_start_6b
    const-string v8, "LGGotaRecoveryReceiver"

    #@6d
    const-string v9, "LGGotaRecoveryReceiver-"

    #@6f
    invoke-static {v8, v9, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_72
    .catchall {:try_start_6b .. :try_end_72} :catchall_76

    #@72
    .line 70
    :try_start_72
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V

    #@75
    goto :goto_4d

    #@76
    .end local v2           #e:Ljava/lang/Exception;
    :catchall_76
    move-exception v8

    #@77
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V

    #@7a
    throw v8
    :try_end_7b
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_7b} :catch_61
.end method
