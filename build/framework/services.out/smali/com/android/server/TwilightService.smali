.class public final Lcom/android/server/TwilightService;
.super Ljava/lang/Object;
.source "TwilightService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/TwilightService$LocationHandler;,
        Lcom/android/server/TwilightService$TwilightListenerRecord;,
        Lcom/android/server/TwilightService$TwilightListener;,
        Lcom/android/server/TwilightService$TwilightState;
    }
.end annotation


# static fields
.field private static final ACTION_UPDATE_TWILIGHT_STATE:Ljava/lang/String; = "com.android.server.action.UPDATE_TWILIGHT_STATE"

.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "TwilightService"


# instance fields
.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private final mContext:Landroid/content/Context;

.field private final mEmptyLocationListener:Landroid/location/LocationListener;

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/TwilightService$TwilightListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocationHandler:Lcom/android/server/TwilightService$LocationHandler;

.field private final mLocationListener:Landroid/location/LocationListener;

.field private final mLocationManager:Landroid/location/LocationManager;

.field private final mLock:Ljava/lang/Object;

.field private mSystemReady:Z

.field private mTwilightState:Lcom/android/server/TwilightService$TwilightState;

.field private final mUpdateLocationReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/TwilightService;->mLock:Ljava/lang/Object;

    #@a
    .line 65
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/TwilightService;->mListeners:Ljava/util/ArrayList;

    #@11
    .line 527
    new-instance v0, Lcom/android/server/TwilightService$1;

    #@13
    invoke-direct {v0, p0}, Lcom/android/server/TwilightService$1;-><init>(Lcom/android/server/TwilightService;)V

    #@16
    iput-object v0, p0, Lcom/android/server/TwilightService;->mUpdateLocationReceiver:Landroid/content/BroadcastReceiver;

    #@18
    .line 544
    new-instance v0, Lcom/android/server/TwilightService$2;

    #@1a
    invoke-direct {v0, p0}, Lcom/android/server/TwilightService$2;-><init>(Lcom/android/server/TwilightService;)V

    #@1d
    iput-object v0, p0, Lcom/android/server/TwilightService;->mEmptyLocationListener:Landroid/location/LocationListener;

    #@1f
    .line 558
    new-instance v0, Lcom/android/server/TwilightService$3;

    #@21
    invoke-direct {v0, p0}, Lcom/android/server/TwilightService$3;-><init>(Lcom/android/server/TwilightService;)V

    #@24
    iput-object v0, p0, Lcom/android/server/TwilightService;->mLocationListener:Landroid/location/LocationListener;

    #@26
    .line 73
    iput-object p1, p0, Lcom/android/server/TwilightService;->mContext:Landroid/content/Context;

    #@28
    .line 75
    iget-object v0, p0, Lcom/android/server/TwilightService;->mContext:Landroid/content/Context;

    #@2a
    const-string v1, "alarm"

    #@2c
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Landroid/app/AlarmManager;

    #@32
    iput-object v0, p0, Lcom/android/server/TwilightService;->mAlarmManager:Landroid/app/AlarmManager;

    #@34
    .line 76
    iget-object v0, p0, Lcom/android/server/TwilightService;->mContext:Landroid/content/Context;

    #@36
    const-string v1, "location"

    #@38
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    check-cast v0, Landroid/location/LocationManager;

    #@3e
    iput-object v0, p0, Lcom/android/server/TwilightService;->mLocationManager:Landroid/location/LocationManager;

    #@40
    .line 77
    new-instance v0, Lcom/android/server/TwilightService$LocationHandler;

    #@42
    const/4 v1, 0x0

    #@43
    invoke-direct {v0, p0, v1}, Lcom/android/server/TwilightService$LocationHandler;-><init>(Lcom/android/server/TwilightService;Lcom/android/server/TwilightService$1;)V

    #@46
    iput-object v0, p0, Lcom/android/server/TwilightService;->mLocationHandler:Lcom/android/server/TwilightService$LocationHandler;

    #@48
    .line 78
    return-void
.end method

.method static synthetic access$100(Landroid/location/Location;Landroid/location/Location;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/android/server/TwilightService;->hasMoved(Landroid/location/Location;Landroid/location/Location;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/TwilightService;)Landroid/location/LocationListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/TwilightService;->mEmptyLocationListener:Landroid/location/LocationListener;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/TwilightService;->mLocationManager:Landroid/location/LocationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/TwilightService;)Landroid/location/LocationListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/TwilightService;->mLocationListener:Landroid/location/LocationListener;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/TwilightService;Lcom/android/server/TwilightService$TwilightState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/android/server/TwilightService;->setTwilightState(Lcom/android/server/TwilightService$TwilightState;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/TwilightService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/TwilightService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/TwilightService;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/TwilightService;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/TwilightService;)Lcom/android/server/TwilightService$LocationHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/TwilightService;->mLocationHandler:Lcom/android/server/TwilightService$LocationHandler;

    #@2
    return-object v0
.end method

.method private static hasMoved(Landroid/location/Location;Landroid/location/Location;)Z
    .registers 10
    .parameter "from"
    .parameter "to"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 141
    if-nez p1, :cond_5

    #@4
    .line 162
    :cond_4
    :goto_4
    return v3

    #@5
    .line 145
    :cond_5
    if-nez p0, :cond_9

    #@7
    move v3, v2

    #@8
    .line 146
    goto :goto_4

    #@9
    .line 150
    :cond_9
    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@c
    move-result-wide v4

    #@d
    invoke-virtual {p0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@10
    move-result-wide v6

    #@11
    cmp-long v4, v4, v6

    #@13
    if-ltz v4, :cond_4

    #@15
    .line 155
    invoke-virtual {p0, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    #@18
    move-result v0

    #@19
    .line 158
    .local v0, distance:F
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    #@1c
    move-result v4

    #@1d
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@20
    move-result v5

    #@21
    add-float v1, v4, v5

    #@23
    .line 162
    .local v1, totalAccuracy:F
    cmpl-float v4, v0, v1

    #@25
    if-ltz v4, :cond_29

    #@27
    :goto_27
    move v3, v2

    #@28
    goto :goto_4

    #@29
    :cond_29
    move v2, v3

    #@2a
    goto :goto_27
.end method

.method private setTwilightState(Lcom/android/server/TwilightService$TwilightState;)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 124
    iget-object v3, p0, Lcom/android/server/TwilightService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 125
    :try_start_3
    iget-object v2, p0, Lcom/android/server/TwilightService;->mTwilightState:Lcom/android/server/TwilightService$TwilightState;

    #@5
    invoke-static {v2, p1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_24

    #@b
    .line 130
    iput-object p1, p0, Lcom/android/server/TwilightService;->mTwilightState:Lcom/android/server/TwilightService$TwilightState;

    #@d
    .line 131
    iget-object v2, p0, Lcom/android/server/TwilightService;->mListeners:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v0

    #@13
    .line 132
    .local v0, count:I
    const/4 v1, 0x0

    #@14
    .local v1, i:I
    :goto_14
    if-ge v1, v0, :cond_24

    #@16
    .line 133
    iget-object v2, p0, Lcom/android/server/TwilightService;->mListeners:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Lcom/android/server/TwilightService$TwilightListenerRecord;

    #@1e
    invoke-virtual {v2}, Lcom/android/server/TwilightService$TwilightListenerRecord;->post()V

    #@21
    .line 132
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_14

    #@24
    .line 136
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_24
    monitor-exit v3

    #@25
    .line 137
    return-void

    #@26
    .line 136
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method


# virtual methods
.method public getCurrentState()Lcom/android/server/TwilightService$TwilightState;
    .registers 3

    #@0
    .prologue
    .line 102
    iget-object v1, p0, Lcom/android/server/TwilightService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 103
    :try_start_3
    iget-object v0, p0, Lcom/android/server/TwilightService;->mTwilightState:Lcom/android/server/TwilightService$TwilightState;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 104
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public registerListener(Lcom/android/server/TwilightService$TwilightListener;Landroid/os/Handler;)V
    .registers 6
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 114
    iget-object v1, p0, Lcom/android/server/TwilightService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 115
    :try_start_3
    iget-object v0, p0, Lcom/android/server/TwilightService;->mListeners:Ljava/util/ArrayList;

    #@5
    new-instance v2, Lcom/android/server/TwilightService$TwilightListenerRecord;

    #@7
    invoke-direct {v2, p1, p2}, Lcom/android/server/TwilightService$TwilightListenerRecord;-><init>(Lcom/android/server/TwilightService$TwilightListener;Landroid/os/Handler;)V

    #@a
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 117
    iget-boolean v0, p0, Lcom/android/server/TwilightService;->mSystemReady:Z

    #@f
    if-eqz v0, :cond_1f

    #@11
    iget-object v0, p0, Lcom/android/server/TwilightService;->mListeners:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v0

    #@17
    const/4 v2, 0x1

    #@18
    if-ne v0, v2, :cond_1f

    #@1a
    .line 118
    iget-object v0, p0, Lcom/android/server/TwilightService;->mLocationHandler:Lcom/android/server/TwilightService$LocationHandler;

    #@1c
    invoke-virtual {v0}, Lcom/android/server/TwilightService$LocationHandler;->enableLocationUpdates()V

    #@1f
    .line 120
    :cond_1f
    monitor-exit v1

    #@20
    .line 121
    return-void

    #@21
    .line 120
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v0
.end method

.method systemReady()V
    .registers 5

    #@0
    .prologue
    .line 81
    iget-object v2, p0, Lcom/android/server/TwilightService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 82
    const/4 v1, 0x1

    #@4
    :try_start_4
    iput-boolean v1, p0, Lcom/android/server/TwilightService;->mSystemReady:Z

    #@6
    .line 84
    new-instance v0, Landroid/content/IntentFilter;

    #@8
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    #@a
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@d
    .line 85
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_SET"

    #@f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@12
    .line 86
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 87
    const-string v1, "com.android.server.action.UPDATE_TWILIGHT_STATE"

    #@19
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 88
    iget-object v1, p0, Lcom/android/server/TwilightService;->mContext:Landroid/content/Context;

    #@1e
    iget-object v3, p0, Lcom/android/server/TwilightService;->mUpdateLocationReceiver:Landroid/content/BroadcastReceiver;

    #@20
    invoke-virtual {v1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@23
    .line 90
    iget-object v1, p0, Lcom/android/server/TwilightService;->mListeners:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@28
    move-result v1

    #@29
    if-nez v1, :cond_30

    #@2b
    .line 91
    iget-object v1, p0, Lcom/android/server/TwilightService;->mLocationHandler:Lcom/android/server/TwilightService$LocationHandler;

    #@2d
    invoke-virtual {v1}, Lcom/android/server/TwilightService$LocationHandler;->enableLocationUpdates()V

    #@30
    .line 93
    :cond_30
    monitor-exit v2

    #@31
    .line 94
    return-void

    #@32
    .line 93
    .end local v0           #filter:Landroid/content/IntentFilter;
    :catchall_32
    move-exception v1

    #@33
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_4 .. :try_end_34} :catchall_32

    #@34
    throw v1
.end method
