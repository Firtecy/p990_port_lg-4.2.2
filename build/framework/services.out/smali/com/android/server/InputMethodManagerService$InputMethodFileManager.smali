.class Lcom/android/server/InputMethodManagerService$InputMethodFileManager;
.super Ljava/lang/Object;
.source "InputMethodManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/InputMethodManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InputMethodFileManager"
.end annotation


# static fields
.field private static final ADDITIONAL_SUBTYPES_FILE_NAME:Ljava/lang/String; = "subtypes.xml"

.field private static final ATTR_ICON:Ljava/lang/String; = "icon"

.field private static final ATTR_ID:Ljava/lang/String; = "id"

.field private static final ATTR_IME_SUBTYPE_EXTRA_VALUE:Ljava/lang/String; = "imeSubtypeExtraValue"

.field private static final ATTR_IME_SUBTYPE_LOCALE:Ljava/lang/String; = "imeSubtypeLocale"

.field private static final ATTR_IME_SUBTYPE_MODE:Ljava/lang/String; = "imeSubtypeMode"

.field private static final ATTR_IS_AUXILIARY:Ljava/lang/String; = "isAuxiliary"

.field private static final ATTR_LABEL:Ljava/lang/String; = "label"

.field private static final INPUT_METHOD_PATH:Ljava/lang/String; = "inputmethod"

.field private static final NODE_IMI:Ljava/lang/String; = "imi"

.field private static final NODE_SUBTYPE:Ljava/lang/String; = "subtype"

.field private static final NODE_SUBTYPES:Ljava/lang/String; = "subtypes"

.field private static final SYSTEM_PATH:Ljava/lang/String; = "system"


# instance fields
.field private final mAdditionalInputMethodSubtypeFile:Landroid/util/AtomicFile;

.field private final mAdditionalSubtypesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mMethodMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/HashMap;I)V
    .registers 9
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3957
    .local p1, methodMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3955
    new-instance v3, Ljava/util/HashMap;

    #@5
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v3, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@a
    .line 3958
    if-nez p1, :cond_14

    #@c
    .line 3959
    new-instance v3, Ljava/lang/NullPointerException;

    #@e
    const-string v4, "methodMap is null"

    #@10
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@13
    throw v3

    #@14
    .line 3961
    :cond_14
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mMethodMap:Ljava/util/HashMap;

    #@16
    .line 3962
    if-nez p2, :cond_68

    #@18
    new-instance v2, Ljava/io/File;

    #@1a
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, "system"

    #@20
    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@23
    .line 3965
    .local v2, systemDir:Ljava/io/File;
    :goto_23
    new-instance v0, Ljava/io/File;

    #@25
    const-string v3, "inputmethod"

    #@27
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2a
    .line 3966
    .local v0, inputMethodDir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@2d
    move-result v3

    #@2e
    if-nez v3, :cond_4c

    #@30
    .line 3967
    const-string v3, "InputMethodManagerService"

    #@32
    new-instance v4, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v5, "Couldn\'t create dir.: "

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 3969
    :cond_4c
    new-instance v1, Ljava/io/File;

    #@4e
    const-string v3, "subtypes.xml"

    #@50
    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@53
    .line 3970
    .local v1, subtypeFile:Ljava/io/File;
    new-instance v3, Landroid/util/AtomicFile;

    #@55
    invoke-direct {v3, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@58
    iput-object v3, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalInputMethodSubtypeFile:Landroid/util/AtomicFile;

    #@5a
    .line 3971
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@5d
    move-result v3

    #@5e
    if-nez v3, :cond_6d

    #@60
    .line 3973
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@62
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalInputMethodSubtypeFile:Landroid/util/AtomicFile;

    #@64
    invoke-static {v3, v4, p1}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->writeAdditionalInputMethodSubtypes(Ljava/util/HashMap;Landroid/util/AtomicFile;Ljava/util/HashMap;)V

    #@67
    .line 3979
    :goto_67
    return-void

    #@68
    .line 3962
    .end local v0           #inputMethodDir:Ljava/io/File;
    .end local v1           #subtypeFile:Ljava/io/File;
    .end local v2           #systemDir:Ljava/io/File;
    :cond_68
    invoke-static {p2}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@6b
    move-result-object v2

    #@6c
    goto :goto_23

    #@6d
    .line 3976
    .restart local v0       #inputMethodDir:Ljava/io/File;
    .restart local v1       #subtypeFile:Ljava/io/File;
    .restart local v2       #systemDir:Ljava/io/File;
    :cond_6d
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@6f
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalInputMethodSubtypeFile:Landroid/util/AtomicFile;

    #@71
    invoke-static {v3, v4}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->readAdditionalInputMethodSubtypes(Ljava/util/HashMap;Landroid/util/AtomicFile;)V

    #@74
    goto :goto_67
.end method

.method static synthetic access$500(Lcom/android/server/InputMethodManagerService$InputMethodFileManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3939
    invoke-direct {p0, p1}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->deleteAllInputMethodSubtypes(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private deleteAllInputMethodSubtypes(Ljava/lang/String;)V
    .registers 6
    .parameter "imiId"

    #@0
    .prologue
    .line 3982
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 3983
    :try_start_3
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 3984
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@a
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalInputMethodSubtypeFile:Landroid/util/AtomicFile;

    #@c
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mMethodMap:Ljava/util/HashMap;

    #@e
    invoke-static {v0, v2, v3}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->writeAdditionalInputMethodSubtypes(Ljava/util/HashMap;Landroid/util/AtomicFile;Ljava/util/HashMap;)V

    #@11
    .line 3986
    monitor-exit v1

    #@12
    .line 3987
    return-void

    #@13
    .line 3986
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method private static readAdditionalInputMethodSubtypes(Ljava/util/HashMap;Landroid/util/AtomicFile;)V
    .registers 25
    .parameter
    .parameter "subtypesFile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;",
            "Landroid/util/AtomicFile;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 4064
    .local p0, allSubtypes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    if-eqz p0, :cond_4

    #@2
    if-nez p1, :cond_5

    #@4
    .line 4136
    :cond_4
    :goto_4
    return-void

    #@5
    .line 4065
    :cond_5
    invoke-virtual/range {p0 .. p0}, Ljava/util/HashMap;->clear()V

    #@8
    .line 4066
    const/4 v15, 0x0

    #@9
    .line 4068
    .local v15, fis:Ljava/io/FileInputStream;
    :try_start_9
    invoke-virtual/range {p1 .. p1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@c
    move-result-object v15

    #@d
    .line 4069
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@10
    move-result-object v17

    #@11
    .line 4070
    .local v17, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/16 v20, 0x0

    #@13
    move-object/from16 v0, v17

    #@15
    move-object/from16 v1, v20

    #@17
    invoke-interface {v0, v15, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@1a
    .line 4071
    invoke-interface/range {v17 .. v17}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@1d
    move-result v19

    #@1e
    .line 4074
    .local v19, type:I
    :cond_1e
    invoke-interface/range {v17 .. v17}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@21
    move-result v19

    #@22
    const/16 v20, 0x2

    #@24
    move/from16 v0, v19

    #@26
    move/from16 v1, v20

    #@28
    if-eq v0, v1, :cond_32

    #@2a
    const/16 v20, 0x1

    #@2c
    move/from16 v0, v19

    #@2e
    move/from16 v1, v20

    #@30
    if-ne v0, v1, :cond_1e

    #@32
    .line 4075
    :cond_32
    invoke-interface/range {v17 .. v17}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@35
    move-result-object v14

    #@36
    .line 4076
    .local v14, firstNodeName:Ljava/lang/String;
    const-string v20, "subtypes"

    #@38
    move-object/from16 v0, v20

    #@3a
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v20

    #@3e
    if-nez v20, :cond_72

    #@40
    .line 4077
    new-instance v20, Lorg/xmlpull/v1/XmlPullParserException;

    #@42
    const-string v21, "Xml doesn\'t start with subtypes"

    #@44
    invoke-direct/range {v20 .. v21}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@47
    throw v20
    :try_end_48
    .catchall {:try_start_9 .. :try_end_48} :catchall_159
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_48} :catch_48
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_48} :catch_c9
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_48} :catch_100

    #@48
    .line 4118
    .end local v14           #firstNodeName:Ljava/lang/String;
    .end local v17           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v19           #type:I
    :catch_48
    move-exception v12

    #@49
    .line 4119
    .local v12, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_49
    const-string v20, "InputMethodManagerService"

    #@4b
    new-instance v21, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v22, "Error reading subtypes: "

    #@52
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v21

    #@56
    move-object/from16 v0, v21

    #@58
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v21

    #@5c
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v21

    #@60
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_63
    .catchall {:try_start_49 .. :try_end_63} :catchall_159

    #@63
    .line 4128
    if-eqz v15, :cond_4

    #@65
    .line 4130
    :try_start_65
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_68
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_68} :catch_69

    #@68
    goto :goto_4

    #@69
    .line 4131
    :catch_69
    move-exception v13

    #@6a
    .line 4132
    .local v13, e1:Ljava/io/IOException;
    const-string v20, "InputMethodManagerService"

    #@6c
    const-string v21, "Failed to close."

    #@6e
    .end local v12           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_6e
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    goto :goto_4

    #@72
    .line 4079
    .end local v13           #e1:Ljava/io/IOException;
    .restart local v14       #firstNodeName:Ljava/lang/String;
    .restart local v17       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v19       #type:I
    :cond_72
    :try_start_72
    invoke-interface/range {v17 .. v17}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@75
    move-result v11

    #@76
    .line 4080
    .local v11, depth:I
    const/4 v10, 0x0

    #@77
    .line 4081
    .local v10, currentImiId:Ljava/lang/String;
    const/16 v18, 0x0

    #@79
    .line 4083
    .local v18, tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_79
    :goto_79
    invoke-interface/range {v17 .. v17}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7c
    move-result v19

    #@7d
    const/16 v20, 0x3

    #@7f
    move/from16 v0, v19

    #@81
    move/from16 v1, v20

    #@83
    if-ne v0, v1, :cond_8d

    #@85
    invoke-interface/range {v17 .. v17}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@88
    move-result v20

    #@89
    move/from16 v0, v20

    #@8b
    if-le v0, v11, :cond_1e4

    #@8d
    :cond_8d
    const/16 v20, 0x1

    #@8f
    move/from16 v0, v19

    #@91
    move/from16 v1, v20

    #@93
    if-eq v0, v1, :cond_1e4

    #@95
    .line 4084
    const/16 v20, 0x2

    #@97
    move/from16 v0, v19

    #@99
    move/from16 v1, v20

    #@9b
    if-ne v0, v1, :cond_79

    #@9d
    .line 4086
    invoke-interface/range {v17 .. v17}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@a0
    move-result-object v16

    #@a1
    .line 4087
    .local v16, nodeName:Ljava/lang/String;
    const-string v20, "imi"

    #@a3
    move-object/from16 v0, v20

    #@a5
    move-object/from16 v1, v16

    #@a7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v20

    #@ab
    if-eqz v20, :cond_129

    #@ad
    .line 4088
    const/16 v20, 0x0

    #@af
    const-string v21, "id"

    #@b1
    move-object/from16 v0, v17

    #@b3
    move-object/from16 v1, v20

    #@b5
    move-object/from16 v2, v21

    #@b7
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ba
    move-result-object v10

    #@bb
    .line 4089
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@be
    move-result v20

    #@bf
    if-eqz v20, :cond_f2

    #@c1
    .line 4090
    const-string v20, "InputMethodManagerService"

    #@c3
    const-string v21, "Invalid imi id found in subtypes.xml"

    #@c5
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c8
    .catchall {:try_start_72 .. :try_end_c8} :catchall_159
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_72 .. :try_end_c8} :catch_48
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_c8} :catch_c9
    .catch Ljava/lang/NumberFormatException; {:try_start_72 .. :try_end_c8} :catch_100

    #@c8
    goto :goto_79

    #@c9
    .line 4121
    .end local v10           #currentImiId:Ljava/lang/String;
    .end local v11           #depth:I
    .end local v14           #firstNodeName:Ljava/lang/String;
    .end local v16           #nodeName:Ljava/lang/String;
    .end local v17           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v18           #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v19           #type:I
    :catch_c9
    move-exception v12

    #@ca
    .line 4122
    .local v12, e:Ljava/io/IOException;
    :try_start_ca
    const-string v20, "InputMethodManagerService"

    #@cc
    new-instance v21, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v22, "Error reading subtypes: "

    #@d3
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v21

    #@d7
    move-object/from16 v0, v21

    #@d9
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v21

    #@dd
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v21

    #@e1
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e4
    .catchall {:try_start_ca .. :try_end_e4} :catchall_159

    #@e4
    .line 4128
    if-eqz v15, :cond_4

    #@e6
    .line 4130
    :try_start_e6
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_e9
    .catch Ljava/io/IOException; {:try_start_e6 .. :try_end_e9} :catch_eb

    #@e9
    goto/16 :goto_4

    #@eb
    .line 4131
    :catch_eb
    move-exception v13

    #@ec
    .line 4132
    .restart local v13       #e1:Ljava/io/IOException;
    const-string v20, "InputMethodManagerService"

    #@ee
    const-string v21, "Failed to close."

    #@f0
    goto/16 :goto_6e

    #@f2
    .line 4093
    .end local v12           #e:Ljava/io/IOException;
    .end local v13           #e1:Ljava/io/IOException;
    .restart local v10       #currentImiId:Ljava/lang/String;
    .restart local v11       #depth:I
    .restart local v14       #firstNodeName:Ljava/lang/String;
    .restart local v16       #nodeName:Ljava/lang/String;
    .restart local v17       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v18       #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .restart local v19       #type:I
    :cond_f2
    :try_start_f2
    new-instance v18, Ljava/util/ArrayList;

    #@f4
    .end local v18           #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    #@f7
    .line 4094
    .restart local v18       #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    move-object/from16 v0, p0

    #@f9
    move-object/from16 v1, v18

    #@fb
    invoke-virtual {v0, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_fe
    .catchall {:try_start_f2 .. :try_end_fe} :catchall_159
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_f2 .. :try_end_fe} :catch_48
    .catch Ljava/io/IOException; {:try_start_f2 .. :try_end_fe} :catch_c9
    .catch Ljava/lang/NumberFormatException; {:try_start_f2 .. :try_end_fe} :catch_100

    #@fe
    goto/16 :goto_79

    #@100
    .line 4124
    .end local v10           #currentImiId:Ljava/lang/String;
    .end local v11           #depth:I
    .end local v14           #firstNodeName:Ljava/lang/String;
    .end local v16           #nodeName:Ljava/lang/String;
    .end local v17           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v18           #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v19           #type:I
    :catch_100
    move-exception v12

    #@101
    .line 4125
    .local v12, e:Ljava/lang/NumberFormatException;
    :try_start_101
    const-string v20, "InputMethodManagerService"

    #@103
    new-instance v21, Ljava/lang/StringBuilder;

    #@105
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v22, "Error reading subtypes: "

    #@10a
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v21

    #@10e
    move-object/from16 v0, v21

    #@110
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v21

    #@114
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v21

    #@118
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11b
    .catchall {:try_start_101 .. :try_end_11b} :catchall_159

    #@11b
    .line 4128
    if-eqz v15, :cond_4

    #@11d
    .line 4130
    :try_start_11d
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_120
    .catch Ljava/io/IOException; {:try_start_11d .. :try_end_120} :catch_122

    #@120
    goto/16 :goto_4

    #@122
    .line 4131
    :catch_122
    move-exception v13

    #@123
    .line 4132
    .restart local v13       #e1:Ljava/io/IOException;
    const-string v20, "InputMethodManagerService"

    #@125
    const-string v21, "Failed to close."

    #@127
    goto/16 :goto_6e

    #@129
    .line 4095
    .end local v12           #e:Ljava/lang/NumberFormatException;
    .end local v13           #e1:Ljava/io/IOException;
    .restart local v10       #currentImiId:Ljava/lang/String;
    .restart local v11       #depth:I
    .restart local v14       #firstNodeName:Ljava/lang/String;
    .restart local v16       #nodeName:Ljava/lang/String;
    .restart local v17       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v18       #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .restart local v19       #type:I
    :cond_129
    :try_start_129
    const-string v20, "subtype"

    #@12b
    move-object/from16 v0, v20

    #@12d
    move-object/from16 v1, v16

    #@12f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@132
    move-result v20

    #@133
    if-eqz v20, :cond_79

    #@135
    .line 4096
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@138
    move-result v20

    #@139
    if-nez v20, :cond_13d

    #@13b
    if-nez v18, :cond_160

    #@13d
    .line 4097
    :cond_13d
    const-string v20, "InputMethodManagerService"

    #@13f
    new-instance v21, Ljava/lang/StringBuilder;

    #@141
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@144
    const-string v22, "IME uninstalled or not valid.: "

    #@146
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v21

    #@14a
    move-object/from16 v0, v21

    #@14c
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v21

    #@150
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v21

    #@154
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_157
    .catchall {:try_start_129 .. :try_end_157} :catchall_159
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_129 .. :try_end_157} :catch_48
    .catch Ljava/io/IOException; {:try_start_129 .. :try_end_157} :catch_c9
    .catch Ljava/lang/NumberFormatException; {:try_start_129 .. :try_end_157} :catch_100

    #@157
    goto/16 :goto_79

    #@159
    .line 4128
    .end local v10           #currentImiId:Ljava/lang/String;
    .end local v11           #depth:I
    .end local v14           #firstNodeName:Ljava/lang/String;
    .end local v16           #nodeName:Ljava/lang/String;
    .end local v17           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v18           #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v19           #type:I
    :catchall_159
    move-exception v20

    #@15a
    if-eqz v15, :cond_15f

    #@15c
    .line 4130
    :try_start_15c
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_15f
    .catch Ljava/io/IOException; {:try_start_15c .. :try_end_15f} :catch_1da

    #@15f
    .line 4128
    :cond_15f
    :goto_15f
    throw v20

    #@160
    .line 4100
    .restart local v10       #currentImiId:Ljava/lang/String;
    .restart local v11       #depth:I
    .restart local v14       #firstNodeName:Ljava/lang/String;
    .restart local v16       #nodeName:Ljava/lang/String;
    .restart local v17       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v18       #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .restart local v19       #type:I
    :cond_160
    const/16 v20, 0x0

    #@162
    :try_start_162
    const-string v21, "icon"

    #@164
    move-object/from16 v0, v17

    #@166
    move-object/from16 v1, v20

    #@168
    move-object/from16 v2, v21

    #@16a
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@16d
    move-result-object v20

    #@16e
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@171
    move-result-object v20

    #@172
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    #@175
    move-result v5

    #@176
    .line 4102
    .local v5, icon:I
    const/16 v20, 0x0

    #@178
    const-string v21, "label"

    #@17a
    move-object/from16 v0, v17

    #@17c
    move-object/from16 v1, v20

    #@17e
    move-object/from16 v2, v21

    #@180
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@183
    move-result-object v20

    #@184
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@187
    move-result-object v20

    #@188
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    #@18b
    move-result v4

    #@18c
    .line 4104
    .local v4, label:I
    const/16 v20, 0x0

    #@18e
    const-string v21, "imeSubtypeLocale"

    #@190
    move-object/from16 v0, v17

    #@192
    move-object/from16 v1, v20

    #@194
    move-object/from16 v2, v21

    #@196
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@199
    move-result-object v6

    #@19a
    .line 4106
    .local v6, imeSubtypeLocale:Ljava/lang/String;
    const/16 v20, 0x0

    #@19c
    const-string v21, "imeSubtypeMode"

    #@19e
    move-object/from16 v0, v17

    #@1a0
    move-object/from16 v1, v20

    #@1a2
    move-object/from16 v2, v21

    #@1a4
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a7
    move-result-object v7

    #@1a8
    .line 4108
    .local v7, imeSubtypeMode:Ljava/lang/String;
    const/16 v20, 0x0

    #@1aa
    const-string v21, "imeSubtypeExtraValue"

    #@1ac
    move-object/from16 v0, v17

    #@1ae
    move-object/from16 v1, v20

    #@1b0
    move-object/from16 v2, v21

    #@1b2
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b5
    move-result-object v8

    #@1b6
    .line 4110
    .local v8, imeSubtypeExtraValue:Ljava/lang/String;
    const-string v20, "1"

    #@1b8
    const/16 v21, 0x0

    #@1ba
    const-string v22, "isAuxiliary"

    #@1bc
    move-object/from16 v0, v17

    #@1be
    move-object/from16 v1, v21

    #@1c0
    move-object/from16 v2, v22

    #@1c2
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c5
    move-result-object v21

    #@1c6
    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@1c9
    move-result-object v21

    #@1ca
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1cd
    move-result v9

    #@1ce
    .line 4112
    .local v9, isAuxiliary:Z
    new-instance v3, Landroid/view/inputmethod/InputMethodSubtype;

    #@1d0
    invoke-direct/range {v3 .. v9}, Landroid/view/inputmethod/InputMethodSubtype;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    #@1d3
    .line 4115
    .local v3, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    move-object/from16 v0, v18

    #@1d5
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1d8
    .catchall {:try_start_162 .. :try_end_1d8} :catchall_159
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_162 .. :try_end_1d8} :catch_48
    .catch Ljava/io/IOException; {:try_start_162 .. :try_end_1d8} :catch_c9
    .catch Ljava/lang/NumberFormatException; {:try_start_162 .. :try_end_1d8} :catch_100

    #@1d8
    goto/16 :goto_79

    #@1da
    .line 4131
    .end local v3           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v4           #label:I
    .end local v5           #icon:I
    .end local v6           #imeSubtypeLocale:Ljava/lang/String;
    .end local v7           #imeSubtypeMode:Ljava/lang/String;
    .end local v8           #imeSubtypeExtraValue:Ljava/lang/String;
    .end local v9           #isAuxiliary:Z
    .end local v10           #currentImiId:Ljava/lang/String;
    .end local v11           #depth:I
    .end local v14           #firstNodeName:Ljava/lang/String;
    .end local v16           #nodeName:Ljava/lang/String;
    .end local v17           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v18           #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v19           #type:I
    :catch_1da
    move-exception v13

    #@1db
    .line 4132
    .restart local v13       #e1:Ljava/io/IOException;
    const-string v21, "InputMethodManagerService"

    #@1dd
    const-string v22, "Failed to close."

    #@1df
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e2
    goto/16 :goto_15f

    #@1e4
    .line 4128
    .end local v13           #e1:Ljava/io/IOException;
    .restart local v10       #currentImiId:Ljava/lang/String;
    .restart local v11       #depth:I
    .restart local v14       #firstNodeName:Ljava/lang/String;
    .restart local v17       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v18       #tempSubtypesArray:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .restart local v19       #type:I
    :cond_1e4
    if-eqz v15, :cond_4

    #@1e6
    .line 4130
    :try_start_1e6
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_1e9
    .catch Ljava/io/IOException; {:try_start_1e6 .. :try_end_1e9} :catch_1eb

    #@1e9
    goto/16 :goto_4

    #@1eb
    .line 4131
    :catch_1eb
    move-exception v13

    #@1ec
    .line 4132
    .restart local v13       #e1:Ljava/io/IOException;
    const-string v20, "InputMethodManagerService"

    #@1ee
    const-string v21, "Failed to close."

    #@1f0
    goto/16 :goto_6e
.end method

.method private static writeAdditionalInputMethodSubtypes(Ljava/util/HashMap;Landroid/util/AtomicFile;Ljava/util/HashMap;)V
    .registers 16
    .parameter
    .parameter "subtypesFile"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;",
            "Landroid/util/AtomicFile;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 4019
    .local p0, allSubtypes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    .local p2, methodMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz p2, :cond_70

    #@2
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    #@5
    move-result v10

    #@6
    if-lez v10, :cond_70

    #@8
    const/4 v6, 0x1

    #@9
    .line 4020
    .local v6, isSetMethodMap:Z
    :goto_9
    const/4 v2, 0x0

    #@a
    .line 4022
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_a
    invoke-virtual {p1}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@d
    move-result-object v2

    #@e
    .line 4023
    new-instance v7, Lcom/android/internal/util/FastXmlSerializer;

    #@10
    invoke-direct {v7}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@13
    .line 4024
    .local v7, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v10, "utf-8"

    #@15
    invoke-interface {v7, v2, v10}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@18
    .line 4025
    const/4 v10, 0x0

    #@19
    const/4 v11, 0x1

    #@1a
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1d
    move-result-object v11

    #@1e
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@21
    .line 4026
    const-string v10, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@23
    const/4 v11, 0x1

    #@24
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@27
    .line 4027
    const/4 v10, 0x0

    #@28
    const-string v11, "subtypes"

    #@2a
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2d
    .line 4028
    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@30
    move-result-object v10

    #@31
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@34
    move-result-object v4

    #@35
    .local v4, i$:Ljava/util/Iterator;
    :goto_35
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@38
    move-result v10

    #@39
    if-eqz v10, :cond_f5

    #@3b
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3e
    move-result-object v5

    #@3f
    check-cast v5, Ljava/lang/String;

    #@41
    .line 4029
    .local v5, imiId:Ljava/lang/String;
    if-eqz v6, :cond_72

    #@43
    invoke-virtual {p2, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@46
    move-result v10

    #@47
    if-nez v10, :cond_72

    #@49
    .line 4030
    const-string v10, "InputMethodManagerService"

    #@4b
    new-instance v11, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v12, "IME uninstalled or not valid.: "

    #@52
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v11

    #@56
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v11

    #@5a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v11

    #@5e
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_61} :catch_62

    #@61
    goto :goto_35

    #@62
    .line 4054
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v5           #imiId:Ljava/lang/String;
    .end local v7           #out:Lorg/xmlpull/v1/XmlSerializer;
    :catch_62
    move-exception v1

    #@63
    .line 4055
    .local v1, e:Ljava/io/IOException;
    const-string v10, "InputMethodManagerService"

    #@65
    const-string v11, "Error writing subtypes"

    #@67
    invoke-static {v10, v11, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6a
    .line 4056
    if-eqz v2, :cond_6f

    #@6c
    .line 4057
    invoke-virtual {p1, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@6f
    .line 4060
    .end local v1           #e:Ljava/io/IOException;
    :cond_6f
    :goto_6f
    return-void

    #@70
    .line 4019
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .end local v6           #isSetMethodMap:Z
    :cond_70
    const/4 v6, 0x0

    #@71
    goto :goto_9

    #@72
    .line 4033
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    .restart local v4       #i$:Ljava/util/Iterator;
    .restart local v5       #imiId:Ljava/lang/String;
    .restart local v6       #isSetMethodMap:Z
    .restart local v7       #out:Lorg/xmlpull/v1/XmlSerializer;
    :cond_72
    const/4 v10, 0x0

    #@73
    :try_start_73
    const-string v11, "imi"

    #@75
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@78
    .line 4034
    const/4 v10, 0x0

    #@79
    const-string v11, "id"

    #@7b
    invoke-interface {v7, v10, v11, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7e
    .line 4035
    invoke-virtual {p0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@81
    move-result-object v9

    #@82
    check-cast v9, Ljava/util/List;

    #@84
    .line 4036
    .local v9, subtypesList:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    #@87
    move-result v0

    #@88
    .line 4037
    .local v0, N:I
    const/4 v3, 0x0

    #@89
    .local v3, i:I
    :goto_89
    if-ge v3, v0, :cond_ed

    #@8b
    .line 4038
    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@8e
    move-result-object v8

    #@8f
    check-cast v8, Landroid/view/inputmethod/InputMethodSubtype;

    #@91
    .line 4039
    .local v8, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    const/4 v10, 0x0

    #@92
    const-string v11, "subtype"

    #@94
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@97
    .line 4040
    const/4 v10, 0x0

    #@98
    const-string v11, "icon"

    #@9a
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getIconResId()I

    #@9d
    move-result v12

    #@9e
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@a1
    move-result-object v12

    #@a2
    invoke-interface {v7, v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a5
    .line 4041
    const/4 v10, 0x0

    #@a6
    const-string v11, "label"

    #@a8
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getNameResId()I

    #@ab
    move-result v12

    #@ac
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@af
    move-result-object v12

    #@b0
    invoke-interface {v7, v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b3
    .line 4042
    const/4 v10, 0x0

    #@b4
    const-string v11, "imeSubtypeLocale"

    #@b6
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@b9
    move-result-object v12

    #@ba
    invoke-interface {v7, v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@bd
    .line 4043
    const/4 v10, 0x0

    #@be
    const-string v11, "imeSubtypeMode"

    #@c0
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@c3
    move-result-object v12

    #@c4
    invoke-interface {v7, v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c7
    .line 4044
    const/4 v10, 0x0

    #@c8
    const-string v11, "imeSubtypeExtraValue"

    #@ca
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValue()Ljava/lang/String;

    #@cd
    move-result-object v12

    #@ce
    invoke-interface {v7, v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d1
    .line 4045
    const/4 v11, 0x0

    #@d2
    const-string v12, "isAuxiliary"

    #@d4
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@d7
    move-result v10

    #@d8
    if-eqz v10, :cond_eb

    #@da
    const/4 v10, 0x1

    #@db
    :goto_db
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@de
    move-result-object v10

    #@df
    invoke-interface {v7, v11, v12, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e2
    .line 4047
    const/4 v10, 0x0

    #@e3
    const-string v11, "subtype"

    #@e5
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e8
    .line 4037
    add-int/lit8 v3, v3, 0x1

    #@ea
    goto :goto_89

    #@eb
    .line 4045
    :cond_eb
    const/4 v10, 0x0

    #@ec
    goto :goto_db

    #@ed
    .line 4049
    .end local v8           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_ed
    const/4 v10, 0x0

    #@ee
    const-string v11, "imi"

    #@f0
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f3
    goto/16 :goto_35

    #@f5
    .line 4051
    .end local v0           #N:I
    .end local v3           #i:I
    .end local v5           #imiId:Ljava/lang/String;
    .end local v9           #subtypesList:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_f5
    const/4 v10, 0x0

    #@f6
    const-string v11, "subtypes"

    #@f8
    invoke-interface {v7, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@fb
    .line 4052
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@fe
    .line 4053
    invoke-virtual {p1, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_101
    .catch Ljava/io/IOException; {:try_start_73 .. :try_end_101} :catch_62

    #@101
    goto/16 :goto_6f
.end method


# virtual methods
.method public addInputMethodSubtypes(Landroid/view/inputmethod/InputMethodInfo;[Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 11
    .parameter "imi"
    .parameter "additionalSubtypes"

    #@0
    .prologue
    .line 3991
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v5

    #@3
    .line 3992
    :try_start_3
    new-instance v3, Ljava/util/ArrayList;

    #@5
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@8
    .line 3993
    .local v3, subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    array-length v0, p2

    #@9
    .line 3994
    .local v0, N:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_48

    #@c
    .line 3995
    aget-object v2, p2, v1

    #@e
    .line 3996
    .local v2, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@11
    move-result v4

    #@12
    if-nez v4, :cond_1a

    #@14
    .line 3997
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@17
    .line 3994
    :goto_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_a

    #@1a
    .line 3999
    :cond_1a
    const-string v4, "InputMethodManagerService"

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "Duplicated subtype definition found: "

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    const-string v7, ", "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_17

    #@45
    .line 4006
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v3           #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :catchall_45
    move-exception v4

    #@46
    monitor-exit v5
    :try_end_47
    .catchall {:try_start_3 .. :try_end_47} :catchall_45

    #@47
    throw v4

    #@48
    .line 4003
    .restart local v0       #N:I
    .restart local v1       #i:I
    .restart local v3       #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_48
    :try_start_48
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@4a
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v4, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    .line 4004
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@53
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalInputMethodSubtypeFile:Landroid/util/AtomicFile;

    #@55
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mMethodMap:Ljava/util/HashMap;

    #@57
    invoke-static {v4, v6, v7}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->writeAdditionalInputMethodSubtypes(Ljava/util/HashMap;Landroid/util/AtomicFile;Ljava/util/HashMap;)V

    #@5a
    .line 4006
    monitor-exit v5
    :try_end_5b
    .catchall {:try_start_48 .. :try_end_5b} :catchall_45

    #@5b
    .line 4007
    return-void
.end method

.method public getAllAdditionalInputMethodSubtypes()Ljava/util/HashMap;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 4010
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 4011
    :try_start_3
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->mAdditionalSubtypesMap:Ljava/util/HashMap;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 4012
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method
