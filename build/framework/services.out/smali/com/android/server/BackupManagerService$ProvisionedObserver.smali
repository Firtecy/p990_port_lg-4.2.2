.class Lcom/android/server/BackupManagerService$ProvisionedObserver;
.super Landroid/database/ContentObserver;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProvisionedObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/BackupManagerService;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "handler"

    #@0
    .prologue
    .line 262
    iput-object p1, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    .line 263
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    .line 264
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 8
    .parameter "selfChange"

    #@0
    .prologue
    .line 267
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    iget-boolean v1, v2, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@4
    .line 268
    .local v1, wasProvisioned:Z
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@6
    invoke-virtual {v2}, Lcom/android/server/BackupManagerService;->deviceIsProvisioned()Z

    #@9
    move-result v0

    #@a
    .line 270
    .local v0, isProvisioned:Z
    iget-object v3, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@c
    if-nez v1, :cond_10

    #@e
    if-eqz v0, :cond_30

    #@10
    :cond_10
    const/4 v2, 0x1

    #@11
    :goto_11
    iput-boolean v2, v3, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@13
    .line 276
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@15
    iget-object v3, v2, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@17
    monitor-enter v3

    #@18
    .line 277
    :try_start_18
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@1a
    iget-boolean v2, v2, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@1c
    if-eqz v2, :cond_2e

    #@1e
    if-nez v1, :cond_2e

    #@20
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@22
    iget-boolean v2, v2, Lcom/android/server/BackupManagerService;->mEnabled:Z

    #@24
    if-eqz v2, :cond_2e

    #@26
    .line 280
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ProvisionedObserver;->this$0:Lcom/android/server/BackupManagerService;

    #@28
    const-wide/32 v4, 0x2932e00

    #@2b
    invoke-static {v2, v4, v5}, Lcom/android/server/BackupManagerService;->access$000(Lcom/android/server/BackupManagerService;J)V

    #@2e
    .line 282
    :cond_2e
    monitor-exit v3

    #@2f
    .line 283
    return-void

    #@30
    .line 270
    :cond_30
    const/4 v2, 0x0

    #@31
    goto :goto_11

    #@32
    .line 282
    :catchall_32
    move-exception v2

    #@33
    monitor-exit v3
    :try_end_34
    .catchall {:try_start_18 .. :try_end_34} :catchall_32

    #@34
    throw v2
.end method
