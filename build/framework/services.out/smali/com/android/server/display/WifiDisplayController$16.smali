.class Lcom/android/server/display/WifiDisplayController$16;
.super Ljava/lang/Object;
.source "WifiDisplayController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/display/WifiDisplayController;->advertiseDisplay(Landroid/hardware/display/WifiDisplay;Landroid/view/Surface;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/WifiDisplayController;

.field final synthetic val$display:Landroid/hardware/display/WifiDisplay;

.field final synthetic val$flags:I

.field final synthetic val$height:I

.field final synthetic val$oldDisplay:Landroid/hardware/display/WifiDisplay;

.field final synthetic val$oldSurface:Landroid/view/Surface;

.field final synthetic val$surface:Landroid/view/Surface;

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/android/server/display/WifiDisplayController;Landroid/view/Surface;Landroid/view/Surface;Landroid/hardware/display/WifiDisplay;Landroid/hardware/display/WifiDisplay;III)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 826
    iput-object p1, p0, Lcom/android/server/display/WifiDisplayController$16;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@2
    iput-object p2, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldSurface:Landroid/view/Surface;

    #@4
    iput-object p3, p0, Lcom/android/server/display/WifiDisplayController$16;->val$surface:Landroid/view/Surface;

    #@6
    iput-object p4, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldDisplay:Landroid/hardware/display/WifiDisplay;

    #@8
    iput-object p5, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@a
    iput p6, p0, Lcom/android/server/display/WifiDisplayController$16;->val$width:I

    #@c
    iput p7, p0, Lcom/android/server/display/WifiDisplayController$16;->val$height:I

    #@e
    iput p8, p0, Lcom/android/server/display/WifiDisplayController$16;->val$flags:I

    #@10
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@13
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 829
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldSurface:Landroid/view/Surface;

    #@2
    if-eqz v0, :cond_4a

    #@4
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$surface:Landroid/view/Surface;

    #@6
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldSurface:Landroid/view/Surface;

    #@8
    if-eq v0, v1, :cond_4a

    #@a
    .line 830
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@c
    invoke-static {v0}, Lcom/android/server/display/WifiDisplayController;->access$400(Lcom/android/server/display/WifiDisplayController;)Lcom/android/server/display/WifiDisplayController$Listener;

    #@f
    move-result-object v0

    #@10
    invoke-interface {v0}, Lcom/android/server/display/WifiDisplayController$Listener;->onDisplayDisconnected()V

    #@13
    .line 835
    :cond_13
    :goto_13
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@15
    if-eqz v0, :cond_49

    #@17
    .line 836
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@19
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldDisplay:Landroid/hardware/display/WifiDisplay;

    #@1b
    invoke-virtual {v0, v1}, Landroid/hardware/display/WifiDisplay;->hasSameAddress(Landroid/hardware/display/WifiDisplay;)Z

    #@1e
    move-result v0

    #@1f
    if-nez v0, :cond_62

    #@21
    .line 837
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@23
    invoke-static {v0}, Lcom/android/server/display/WifiDisplayController;->access$400(Lcom/android/server/display/WifiDisplayController;)Lcom/android/server/display/WifiDisplayController$Listener;

    #@26
    move-result-object v0

    #@27
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@29
    invoke-interface {v0, v1}, Lcom/android/server/display/WifiDisplayController$Listener;->onDisplayConnecting(Landroid/hardware/display/WifiDisplay;)V

    #@2c
    .line 843
    :cond_2c
    :goto_2c
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$surface:Landroid/view/Surface;

    #@2e
    if-eqz v0, :cond_49

    #@30
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$surface:Landroid/view/Surface;

    #@32
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldSurface:Landroid/view/Surface;

    #@34
    if-eq v0, v1, :cond_49

    #@36
    .line 844
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@38
    invoke-static {v0}, Lcom/android/server/display/WifiDisplayController;->access$400(Lcom/android/server/display/WifiDisplayController;)Lcom/android/server/display/WifiDisplayController$Listener;

    #@3b
    move-result-object v0

    #@3c
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@3e
    iget-object v2, p0, Lcom/android/server/display/WifiDisplayController$16;->val$surface:Landroid/view/Surface;

    #@40
    iget v3, p0, Lcom/android/server/display/WifiDisplayController$16;->val$width:I

    #@42
    iget v4, p0, Lcom/android/server/display/WifiDisplayController$16;->val$height:I

    #@44
    iget v5, p0, Lcom/android/server/display/WifiDisplayController$16;->val$flags:I

    #@46
    invoke-interface/range {v0 .. v5}, Lcom/android/server/display/WifiDisplayController$Listener;->onDisplayConnected(Landroid/hardware/display/WifiDisplay;Landroid/view/Surface;III)V

    #@49
    .line 847
    :cond_49
    return-void

    #@4a
    .line 831
    :cond_4a
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldDisplay:Landroid/hardware/display/WifiDisplay;

    #@4c
    if-eqz v0, :cond_13

    #@4e
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldDisplay:Landroid/hardware/display/WifiDisplay;

    #@50
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@52
    invoke-virtual {v0, v1}, Landroid/hardware/display/WifiDisplay;->hasSameAddress(Landroid/hardware/display/WifiDisplay;)Z

    #@55
    move-result v0

    #@56
    if-nez v0, :cond_13

    #@58
    .line 832
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@5a
    invoke-static {v0}, Lcom/android/server/display/WifiDisplayController;->access$400(Lcom/android/server/display/WifiDisplayController;)Lcom/android/server/display/WifiDisplayController$Listener;

    #@5d
    move-result-object v0

    #@5e
    invoke-interface {v0}, Lcom/android/server/display/WifiDisplayController$Listener;->onDisplayConnectionFailed()V

    #@61
    goto :goto_13

    #@62
    .line 838
    :cond_62
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@64
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$oldDisplay:Landroid/hardware/display/WifiDisplay;

    #@66
    invoke-virtual {v0, v1}, Landroid/hardware/display/WifiDisplay;->equals(Landroid/hardware/display/WifiDisplay;)Z

    #@69
    move-result v0

    #@6a
    if-nez v0, :cond_2c

    #@6c
    .line 841
    iget-object v0, p0, Lcom/android/server/display/WifiDisplayController$16;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@6e
    invoke-static {v0}, Lcom/android/server/display/WifiDisplayController;->access$400(Lcom/android/server/display/WifiDisplayController;)Lcom/android/server/display/WifiDisplayController$Listener;

    #@71
    move-result-object v0

    #@72
    iget-object v1, p0, Lcom/android/server/display/WifiDisplayController$16;->val$display:Landroid/hardware/display/WifiDisplay;

    #@74
    invoke-interface {v0, v1}, Lcom/android/server/display/WifiDisplayController$Listener;->onDisplayChanged(Landroid/hardware/display/WifiDisplay;)V

    #@77
    goto :goto_2c
.end method
