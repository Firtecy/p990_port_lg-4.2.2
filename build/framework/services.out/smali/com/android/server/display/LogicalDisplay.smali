.class final Lcom/android/server/display/LogicalDisplay;
.super Ljava/lang/Object;
.source "LogicalDisplay.java"


# static fields
.field private static final BLANK_LAYER_STACK:I = -0x1


# instance fields
.field private final mBaseDisplayInfo:Landroid/view/DisplayInfo;

.field private final mDisplayId:I

.field private mHasContent:Z

.field private mInfo:Landroid/view/DisplayInfo;

.field private final mLayerStack:I

.field private mOverrideDisplayInfo:Landroid/view/DisplayInfo;

.field private mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

.field private mPrimaryDisplayDeviceInfo:Lcom/android/server/display/DisplayDeviceInfo;

.field private final mTempDisplayRect:Landroid/graphics/Rect;

.field private final mTempLayerStackRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(IILcom/android/server/display/DisplayDevice;)V
    .registers 5
    .parameter "displayId"
    .parameter "layerStack"
    .parameter "primaryDisplayDevice"

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    new-instance v0, Landroid/view/DisplayInfo;

    #@5
    invoke-direct {v0}, Landroid/view/DisplayInfo;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@a
    .line 76
    new-instance v0, Landroid/graphics/Rect;

    #@c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mTempLayerStackRect:Landroid/graphics/Rect;

    #@11
    .line 77
    new-instance v0, Landroid/graphics/Rect;

    #@13
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mTempDisplayRect:Landroid/graphics/Rect;

    #@18
    .line 80
    iput p1, p0, Lcom/android/server/display/LogicalDisplay;->mDisplayId:I

    #@1a
    .line 81
    iput p2, p0, Lcom/android/server/display/LogicalDisplay;->mLayerStack:I

    #@1c
    .line 82
    iput-object p3, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@1e
    .line 83
    return-void
.end method


# virtual methods
.method public configureDisplayInTransactionLocked(Lcom/android/server/display/DisplayDevice;Z)V
    .registers 19
    .parameter "device"
    .parameter "isBlanked"

    #@0
    .prologue
    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    #@3
    move-result-object v2

    #@4
    .line 234
    .local v2, displayInfo:Landroid/view/DisplayInfo;
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@7
    move-result-object v1

    #@8
    .line 237
    .local v1, displayDeviceInfo:Lcom/android/server/display/DisplayDeviceInfo;
    if-eqz p2, :cond_73

    #@a
    const/4 v11, -0x1

    #@b
    :goto_b
    move-object/from16 v0, p1

    #@d
    invoke-virtual {v0, v11}, Lcom/android/server/display/DisplayDevice;->setLayerStackInTransactionLocked(I)V

    #@10
    .line 242
    move-object/from16 v0, p0

    #@12
    iget-object v11, v0, Lcom/android/server/display/LogicalDisplay;->mTempLayerStackRect:Landroid/graphics/Rect;

    #@14
    const/4 v12, 0x0

    #@15
    const/4 v13, 0x0

    #@16
    iget v14, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    #@18
    iget v15, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    #@1a
    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    #@1d
    .line 247
    const/4 v7, 0x0

    #@1e
    .line 248
    .local v7, orientation:I
    move-object/from16 v0, p0

    #@20
    iget-object v11, v0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@22
    move-object/from16 v0, p1

    #@24
    if-ne v0, v11, :cond_2e

    #@26
    iget v11, v1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@28
    and-int/lit8 v11, v11, 0x2

    #@2a
    if-eqz v11, :cond_2e

    #@2c
    .line 250
    iget v7, v2, Landroid/view/DisplayInfo;->rotation:I

    #@2e
    .line 254
    :cond_2e
    iget v11, v1, Lcom/android/server/display/DisplayDeviceInfo;->rotation:I

    #@30
    add-int/2addr v11, v7

    #@31
    rem-int/lit8 v7, v11, 0x4

    #@33
    .line 261
    const/4 v11, 0x1

    #@34
    if-eq v7, v11, :cond_39

    #@36
    const/4 v11, 0x3

    #@37
    if-ne v7, v11, :cond_78

    #@39
    :cond_39
    const/4 v10, 0x1

    #@3a
    .line 263
    .local v10, rotated:Z
    :goto_3a
    if-eqz v10, :cond_7a

    #@3c
    iget v9, v1, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@3e
    .line 264
    .local v9, physWidth:I
    :goto_3e
    if-eqz v10, :cond_7d

    #@40
    iget v8, v1, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@42
    .line 274
    .local v8, physHeight:I
    :goto_42
    iget v11, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    #@44
    mul-int/2addr v11, v9

    #@45
    iget v12, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    #@47
    mul-int/2addr v12, v8

    #@48
    if-ge v11, v12, :cond_80

    #@4a
    .line 277
    move v6, v9

    #@4b
    .line 278
    .local v6, displayRectWidth:I
    iget v11, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    #@4d
    mul-int/2addr v11, v9

    #@4e
    iget v12, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    #@50
    div-int v3, v11, v12

    #@52
    .line 284
    .local v3, displayRectHeight:I
    :goto_52
    sub-int v11, v8, v3

    #@54
    div-int/lit8 v5, v11, 0x2

    #@56
    .line 285
    .local v5, displayRectTop:I
    sub-int v11, v9, v6

    #@58
    div-int/lit8 v4, v11, 0x2

    #@5a
    .line 286
    .local v4, displayRectLeft:I
    move-object/from16 v0, p0

    #@5c
    iget-object v11, v0, Lcom/android/server/display/LogicalDisplay;->mTempDisplayRect:Landroid/graphics/Rect;

    #@5e
    add-int v12, v4, v6

    #@60
    add-int v13, v5, v3

    #@62
    invoke-virtual {v11, v4, v5, v12, v13}, Landroid/graphics/Rect;->set(IIII)V

    #@65
    .line 289
    move-object/from16 v0, p0

    #@67
    iget-object v11, v0, Lcom/android/server/display/LogicalDisplay;->mTempLayerStackRect:Landroid/graphics/Rect;

    #@69
    move-object/from16 v0, p0

    #@6b
    iget-object v12, v0, Lcom/android/server/display/LogicalDisplay;->mTempDisplayRect:Landroid/graphics/Rect;

    #@6d
    move-object/from16 v0, p1

    #@6f
    invoke-virtual {v0, v7, v11, v12}, Lcom/android/server/display/DisplayDevice;->setProjectionInTransactionLocked(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@72
    .line 290
    return-void

    #@73
    .line 237
    .end local v3           #displayRectHeight:I
    .end local v4           #displayRectLeft:I
    .end local v5           #displayRectTop:I
    .end local v6           #displayRectWidth:I
    .end local v7           #orientation:I
    .end local v8           #physHeight:I
    .end local v9           #physWidth:I
    .end local v10           #rotated:Z
    :cond_73
    move-object/from16 v0, p0

    #@75
    iget v11, v0, Lcom/android/server/display/LogicalDisplay;->mLayerStack:I

    #@77
    goto :goto_b

    #@78
    .line 261
    .restart local v7       #orientation:I
    :cond_78
    const/4 v10, 0x0

    #@79
    goto :goto_3a

    #@7a
    .line 263
    .restart local v10       #rotated:Z
    :cond_7a
    iget v9, v1, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@7c
    goto :goto_3e

    #@7d
    .line 264
    .restart local v9       #physWidth:I
    :cond_7d
    iget v8, v1, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@7f
    goto :goto_42

    #@80
    .line 281
    .restart local v8       #physHeight:I
    :cond_80
    iget v11, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    #@82
    mul-int/2addr v11, v8

    #@83
    iget v12, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    #@85
    div-int v6, v11, v12

    #@87
    .line 282
    .restart local v6       #displayRectWidth:I
    move v3, v8

    #@88
    .restart local v3       #displayRectHeight:I
    goto :goto_52
.end method

.method public dumpLocked(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "mDisplayId="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/server/display/LogicalDisplay;->mDisplayId:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@18
    .line 317
    new-instance v0, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v1, "mLayerStack="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/server/display/LogicalDisplay;->mLayerStack:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@30
    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v1, "mHasContent="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-boolean v1, p0, Lcom/android/server/display/LogicalDisplay;->mHasContent:Z

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@48
    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v1, "mPrimaryDisplayDevice="

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@55
    if-eqz v0, :cond_99

    #@57
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@59
    invoke-virtual {v0}, Lcom/android/server/display/DisplayDevice;->getNameLocked()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    :goto_5d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, "mBaseDisplayInfo="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, "mOverrideDisplayInfo="

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 323
    return-void

    #@99
    .line 319
    :cond_99
    const-string v0, "null"

    #@9b
    goto :goto_5d
.end method

.method public getDisplayIdLocked()I
    .registers 2

    #@0
    .prologue
    .line 91
    iget v0, p0, Lcom/android/server/display/LogicalDisplay;->mDisplayId:I

    #@2
    return v0
.end method

.method public getDisplayInfoLocked()Landroid/view/DisplayInfo;
    .registers 3

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@2
    if-nez v0, :cond_26

    #@4
    .line 112
    new-instance v0, Landroid/view/DisplayInfo;

    #@6
    invoke-direct {v0}, Landroid/view/DisplayInfo;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@b
    .line 113
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@d
    if-eqz v0, :cond_29

    #@f
    .line 114
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@11
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@13
    invoke-virtual {v0, v1}, Landroid/view/DisplayInfo;->copyFrom(Landroid/view/DisplayInfo;)V

    #@16
    .line 115
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@18
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@1a
    iget v1, v1, Landroid/view/DisplayInfo;->layerStack:I

    #@1c
    iput v1, v0, Landroid/view/DisplayInfo;->layerStack:I

    #@1e
    .line 116
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@20
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@22
    iget-object v1, v1, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@24
    iput-object v1, v0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@26
    .line 121
    :cond_26
    :goto_26
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@28
    return-object v0

    #@29
    .line 118
    :cond_29
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@2b
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@2d
    invoke-virtual {v0, v1}, Landroid/view/DisplayInfo;->copyFrom(Landroid/view/DisplayInfo;)V

    #@30
    goto :goto_26
.end method

.method public getPrimaryDisplayDeviceLocked()Lcom/android/server/display/DisplayDevice;
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@2
    return-object v0
.end method

.method public hasContentLocked()Z
    .registers 2

    #@0
    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/android/server/display/LogicalDisplay;->mHasContent:Z

    #@2
    return v0
.end method

.method public isValidLocked()Z
    .registers 2

    #@0
    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public setDisplayInfoOverrideFromWindowManagerLocked(Landroid/view/DisplayInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 132
    if-eqz p1, :cond_21

    #@3
    .line 133
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@5
    if-nez v0, :cond_11

    #@7
    .line 134
    new-instance v0, Landroid/view/DisplayInfo;

    #@9
    invoke-direct {v0, p1}, Landroid/view/DisplayInfo;-><init>(Landroid/view/DisplayInfo;)V

    #@c
    iput-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@e
    .line 135
    iput-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@10
    .line 144
    :cond_10
    :goto_10
    return-void

    #@11
    .line 136
    :cond_11
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@13
    invoke-virtual {v0, p1}, Landroid/view/DisplayInfo;->equals(Landroid/view/DisplayInfo;)Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_10

    #@19
    .line 137
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@1b
    invoke-virtual {v0, p1}, Landroid/view/DisplayInfo;->copyFrom(Landroid/view/DisplayInfo;)V

    #@1e
    .line 138
    iput-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@20
    goto :goto_10

    #@21
    .line 140
    :cond_21
    iget-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@23
    if-eqz v0, :cond_10

    #@25
    .line 141
    iput-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mOverrideDisplayInfo:Landroid/view/DisplayInfo;

    #@27
    .line 142
    iput-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@29
    goto :goto_10
.end method

.method public setHasContentLocked(Z)V
    .registers 2
    .parameter "hasContent"

    #@0
    .prologue
    .line 312
    iput-boolean p1, p0, Lcom/android/server/display/LogicalDisplay;->mHasContent:Z

    #@2
    .line 313
    return-void
.end method

.method public updateLocked(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/display/DisplayDevice;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, devices:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/display/DisplayDevice;>;"
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 167
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@4
    if-nez v1, :cond_7

    #@6
    .line 212
    :cond_6
    :goto_6
    return-void

    #@7
    .line 172
    :cond_7
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@9
    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_12

    #@f
    .line 173
    iput-object v4, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@11
    goto :goto_6

    #@12
    .line 182
    :cond_12
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDevice:Lcom/android/server/display/DisplayDevice;

    #@14
    invoke-virtual {v1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@17
    move-result-object v0

    #@18
    .line 183
    .local v0, deviceInfo:Lcom/android/server/display/DisplayDeviceInfo;
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDeviceInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@1a
    invoke-static {v1, v0}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_6

    #@20
    .line 184
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@22
    iget v2, p0, Lcom/android/server/display/LogicalDisplay;->mLayerStack:I

    #@24
    iput v2, v1, Landroid/view/DisplayInfo;->layerStack:I

    #@26
    .line 185
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@28
    iput v3, v1, Landroid/view/DisplayInfo;->flags:I

    #@2a
    .line 186
    iget v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@2c
    and-int/lit8 v1, v1, 0x8

    #@2e
    if-eqz v1, :cond_38

    #@30
    .line 187
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@32
    iget v2, v1, Landroid/view/DisplayInfo;->flags:I

    #@34
    or-int/lit8 v2, v2, 0x1

    #@36
    iput v2, v1, Landroid/view/DisplayInfo;->flags:I

    #@38
    .line 189
    :cond_38
    iget v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@3a
    and-int/lit8 v1, v1, 0x4

    #@3c
    if-eqz v1, :cond_46

    #@3e
    .line 190
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@40
    iget v2, v1, Landroid/view/DisplayInfo;->flags:I

    #@42
    or-int/lit8 v2, v2, 0x2

    #@44
    iput v2, v1, Landroid/view/DisplayInfo;->flags:I

    #@46
    .line 192
    :cond_46
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@48
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    #@4a
    iput v2, v1, Landroid/view/DisplayInfo;->type:I

    #@4c
    .line 193
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@4e
    iget-object v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->address:Ljava/lang/String;

    #@50
    iput-object v2, v1, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@52
    .line 194
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@54
    iget-object v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@56
    iput-object v2, v1, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@58
    .line 195
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@5a
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@5c
    iput v2, v1, Landroid/view/DisplayInfo;->appWidth:I

    #@5e
    .line 196
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@60
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@62
    iput v2, v1, Landroid/view/DisplayInfo;->appHeight:I

    #@64
    .line 197
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@66
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@68
    iput v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    #@6a
    .line 198
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@6c
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@6e
    iput v2, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    #@70
    .line 199
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@72
    iput v3, v1, Landroid/view/DisplayInfo;->rotation:I

    #@74
    .line 200
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@76
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->refreshRate:F

    #@78
    iput v2, v1, Landroid/view/DisplayInfo;->refreshRate:F

    #@7a
    .line 201
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@7c
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@7e
    iput v2, v1, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@80
    .line 202
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@82
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@84
    iput v2, v1, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@86
    .line 203
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@88
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@8a
    iput v2, v1, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@8c
    .line 204
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@8e
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@90
    iput v2, v1, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@92
    .line 205
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@94
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@96
    iput v2, v1, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@98
    .line 206
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@9a
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@9c
    iput v2, v1, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@9e
    .line 207
    iget-object v1, p0, Lcom/android/server/display/LogicalDisplay;->mBaseDisplayInfo:Landroid/view/DisplayInfo;

    #@a0
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@a2
    iput v2, v1, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@a4
    .line 209
    iput-object v0, p0, Lcom/android/server/display/LogicalDisplay;->mPrimaryDisplayDeviceInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@a6
    .line 210
    iput-object v4, p0, Lcom/android/server/display/LogicalDisplay;->mInfo:Landroid/view/DisplayInfo;

    #@a8
    goto/16 :goto_6
.end method
