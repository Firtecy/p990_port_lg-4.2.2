.class final Lcom/android/server/display/LocalDisplayAdapter;
.super Lcom/android/server/display/DisplayAdapter;
.source "LocalDisplayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/LocalDisplayAdapter$HotplugDisplayEventReceiver;,
        Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    }
.end annotation


# static fields
.field private static final BUILT_IN_DISPLAY_IDS_TO_SCAN:[I = null

.field private static final TAG:Ljava/lang/String; = "LocalDisplayAdapter"


# instance fields
.field private final MSG_REGISTER_BROADCAST_RECEIVER:I

.field private firstboot:Z

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mDevices:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mHotplugReceiver:Lcom/android/server/display/LocalDisplayAdapter$HotplugDisplayEventReceiver;

.field private final mTempPhys:Landroid/view/Surface$PhysicalDisplayInfo;

.field private wfdSecureConnect:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 48
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [I

    #@3
    fill-array-data v0, :array_a

    #@6
    sput-object v0, Lcom/android/server/display/LocalDisplayAdapter;->BUILT_IN_DISPLAY_IDS_TO_SCAN:[I

    #@8
    return-void

    #@9
    nop

    #@a
    :array_a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;)V
    .registers 12
    .parameter "syncRoot"
    .parameter "context"
    .parameter "handler"
    .parameter "listener"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 82
    const-string v5, "LocalDisplayAdapter"

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/DisplayAdapter;-><init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;Ljava/lang/String;)V

    #@b
    .line 53
    new-instance v0, Landroid/util/SparseArray;

    #@d
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->mDevices:Landroid/util/SparseArray;

    #@12
    .line 57
    new-instance v0, Landroid/view/Surface$PhysicalDisplayInfo;

    #@14
    invoke-direct {v0}, Landroid/view/Surface$PhysicalDisplayInfo;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->mTempPhys:Landroid/view/Surface$PhysicalDisplayInfo;

    #@19
    .line 60
    iput-boolean v6, p0, Lcom/android/server/display/LocalDisplayAdapter;->wfdSecureConnect:Z

    #@1b
    .line 61
    const/4 v0, 0x0

    #@1c
    iput-boolean v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->firstboot:Z

    #@1e
    .line 62
    iput v6, p0, Lcom/android/server/display/LocalDisplayAdapter;->MSG_REGISTER_BROADCAST_RECEIVER:I

    #@20
    .line 63
    new-instance v0, Lcom/android/server/display/LocalDisplayAdapter$1;

    #@22
    invoke-virtual {p0}, Lcom/android/server/display/LocalDisplayAdapter;->getHandler()Landroid/os/Handler;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@29
    move-result-object v1

    #@2a
    invoke-direct {v0, p0, v1}, Lcom/android/server/display/LocalDisplayAdapter$1;-><init>(Lcom/android/server/display/LocalDisplayAdapter;Landroid/os/Looper;)V

    #@2d
    iput-object v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->mHandler:Landroid/os/Handler;

    #@2f
    .line 139
    new-instance v0, Lcom/android/server/display/LocalDisplayAdapter$2;

    #@31
    invoke-direct {v0, p0}, Lcom/android/server/display/LocalDisplayAdapter$2;-><init>(Lcom/android/server/display/LocalDisplayAdapter;)V

    #@34
    iput-object v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@36
    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/display/LocalDisplayAdapter;)Landroid/content/BroadcastReceiver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/display/LocalDisplayAdapter;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->wfdSecureConnect:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/server/display/LocalDisplayAdapter;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/android/server/display/LocalDisplayAdapter;->wfdSecureConnect:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/server/display/LocalDisplayAdapter;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->mDevices:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/display/LocalDisplayAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Lcom/android/server/display/LocalDisplayAdapter;->scanDisplaysLocked()V

    #@3
    return-void
.end method

.method private scanDisplaysLocked()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 95
    sget-object v0, Lcom/android/server/display/LocalDisplayAdapter;->BUILT_IN_DISPLAY_IDS_TO_SCAN:[I

    #@4
    .local v0, arr$:[I
    array-length v5, v0

    #@5
    .local v5, len$:I
    const/4 v4, 0x0

    #@6
    .local v4, i$:I
    :goto_6
    if-ge v4, v5, :cond_7c

    #@8
    aget v1, v0, v4

    #@a
    .line 96
    .local v1, builtInDisplayId:I
    invoke-static {v1}, Landroid/view/Surface;->getBuiltInDisplay(I)Landroid/os/IBinder;

    #@d
    move-result-object v3

    #@e
    .line 97
    .local v3, displayToken:Landroid/os/IBinder;
    if-eqz v3, :cond_64

    #@10
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mTempPhys:Landroid/view/Surface$PhysicalDisplayInfo;

    #@12
    invoke-static {v3, v7}, Landroid/view/Surface;->getDisplayInfo(Landroid/os/IBinder;Landroid/view/Surface$PhysicalDisplayInfo;)Z

    #@15
    move-result v7

    #@16
    if-eqz v7, :cond_64

    #@18
    .line 98
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mDevices:Landroid/util/SparseArray;

    #@1a
    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    #@20
    .line 99
    .local v2, device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    if-nez v2, :cond_57

    #@22
    .line 103
    iget-boolean v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->firstboot:Z

    #@24
    if-eqz v7, :cond_2f

    #@26
    if-ne v1, v8, :cond_2f

    #@28
    .line 104
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mHandler:Landroid/os/Handler;

    #@2a
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@2d
    .line 105
    iput-boolean v9, p0, Lcom/android/server/display/LocalDisplayAdapter;->firstboot:Z

    #@2f
    .line 107
    :cond_2f
    iget-boolean v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->wfdSecureConnect:Z

    #@31
    if-nez v7, :cond_4f

    #@33
    if-ne v1, v8, :cond_4f

    #@35
    .line 108
    new-instance v6, Landroid/view/Surface$PhysicalDisplayInfo;

    #@37
    invoke-direct {v6}, Landroid/view/Surface$PhysicalDisplayInfo;-><init>()V

    #@3a
    .line 109
    .local v6, wfdPhys:Landroid/view/Surface$PhysicalDisplayInfo;
    invoke-static {v3, v6}, Landroid/view/Surface;->getDisplayInfo(Landroid/os/IBinder;Landroid/view/Surface$PhysicalDisplayInfo;)Z

    #@3d
    .line 110
    iput-boolean v9, v6, Landroid/view/Surface$PhysicalDisplayInfo;->secure:Z

    #@3f
    .line 111
    new-instance v2, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    #@41
    .end local v2           #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    invoke-direct {v2, p0, v3, v1, v6}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;-><init>(Lcom/android/server/display/LocalDisplayAdapter;Landroid/os/IBinder;ILandroid/view/Surface$PhysicalDisplayInfo;)V

    #@44
    .line 116
    .end local v6           #wfdPhys:Landroid/view/Surface$PhysicalDisplayInfo;
    .restart local v2       #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    :goto_44
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mDevices:Landroid/util/SparseArray;

    #@46
    invoke-virtual {v7, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@49
    .line 117
    invoke-virtual {p0, v2, v8}, Lcom/android/server/display/LocalDisplayAdapter;->sendDisplayDeviceEventLocked(Lcom/android/server/display/DisplayDevice;I)V

    #@4c
    .line 95
    :cond_4c
    :goto_4c
    add-int/lit8 v4, v4, 0x1

    #@4e
    goto :goto_6

    #@4f
    .line 113
    :cond_4f
    new-instance v2, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    #@51
    .end local v2           #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mTempPhys:Landroid/view/Surface$PhysicalDisplayInfo;

    #@53
    invoke-direct {v2, p0, v3, v1, v7}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;-><init>(Lcom/android/server/display/LocalDisplayAdapter;Landroid/os/IBinder;ILandroid/view/Surface$PhysicalDisplayInfo;)V

    #@56
    .restart local v2       #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    goto :goto_44

    #@57
    .line 118
    :cond_57
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mTempPhys:Landroid/view/Surface$PhysicalDisplayInfo;

    #@59
    invoke-virtual {v2, v7}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->updatePhysicalDisplayInfoLocked(Landroid/view/Surface$PhysicalDisplayInfo;)Z

    #@5c
    move-result v7

    #@5d
    if-eqz v7, :cond_4c

    #@5f
    .line 120
    const/4 v7, 0x2

    #@60
    invoke-virtual {p0, v2, v7}, Lcom/android/server/display/LocalDisplayAdapter;->sendDisplayDeviceEventLocked(Lcom/android/server/display/DisplayDevice;I)V

    #@63
    goto :goto_4c

    #@64
    .line 123
    .end local v2           #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    :cond_64
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mDevices:Landroid/util/SparseArray;

    #@66
    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@69
    move-result-object v2

    #@6a
    check-cast v2, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    #@6c
    .line 124
    .restart local v2       #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    if-eqz v2, :cond_4c

    #@6e
    .line 127
    if-ne v1, v8, :cond_72

    #@70
    .line 128
    iput-boolean v8, p0, Lcom/android/server/display/LocalDisplayAdapter;->wfdSecureConnect:Z

    #@72
    .line 131
    :cond_72
    iget-object v7, p0, Lcom/android/server/display/LocalDisplayAdapter;->mDevices:Landroid/util/SparseArray;

    #@74
    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->remove(I)V

    #@77
    .line 132
    const/4 v7, 0x3

    #@78
    invoke-virtual {p0, v2, v7}, Lcom/android/server/display/LocalDisplayAdapter;->sendDisplayDeviceEventLocked(Lcom/android/server/display/DisplayDevice;I)V

    #@7b
    goto :goto_4c

    #@7c
    .line 136
    .end local v1           #builtInDisplayId:I
    .end local v2           #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    .end local v3           #displayToken:Landroid/os/IBinder;
    :cond_7c
    return-void
.end method


# virtual methods
.method public registerLocked()V
    .registers 3

    #@0
    .prologue
    .line 87
    invoke-super {p0}, Lcom/android/server/display/DisplayAdapter;->registerLocked()V

    #@3
    .line 89
    new-instance v0, Lcom/android/server/display/LocalDisplayAdapter$HotplugDisplayEventReceiver;

    #@5
    invoke-virtual {p0}, Lcom/android/server/display/LocalDisplayAdapter;->getHandler()Landroid/os/Handler;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@c
    move-result-object v1

    #@d
    invoke-direct {v0, p0, v1}, Lcom/android/server/display/LocalDisplayAdapter$HotplugDisplayEventReceiver;-><init>(Lcom/android/server/display/LocalDisplayAdapter;Landroid/os/Looper;)V

    #@10
    iput-object v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->mHotplugReceiver:Lcom/android/server/display/LocalDisplayAdapter$HotplugDisplayEventReceiver;

    #@12
    .line 90
    invoke-direct {p0}, Lcom/android/server/display/LocalDisplayAdapter;->scanDisplaysLocked()V

    #@15
    .line 91
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Lcom/android/server/display/LocalDisplayAdapter;->firstboot:Z

    #@18
    .line 92
    return-void
.end method
