.class abstract Lcom/android/server/display/DisplayDevice;
.super Ljava/lang/Object;
.source "DisplayDevice.java"


# instance fields
.field private mCurrentDisplayRect:Landroid/graphics/Rect;

.field private mCurrentLayerStack:I

.field private mCurrentLayerStackRect:Landroid/graphics/Rect;

.field private mCurrentOrientation:I

.field private mCurrentSurface:Landroid/view/Surface;

.field private final mDisplayAdapter:Lcom/android/server/display/DisplayAdapter;

.field private final mDisplayToken:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayAdapter;Landroid/os/IBinder;)V
    .registers 4
    .parameter "displayAdapter"
    .parameter "displayToken"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 38
    iput v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStack:I

    #@6
    .line 39
    iput v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentOrientation:I

    #@8
    .line 48
    iput-object p1, p0, Lcom/android/server/display/DisplayDevice;->mDisplayAdapter:Lcom/android/server/display/DisplayAdapter;

    #@a
    .line 49
    iput-object p2, p0, Lcom/android/server/display/DisplayDevice;->mDisplayToken:Landroid/os/IBinder;

    #@c
    .line 50
    return-void
.end method


# virtual methods
.method public applyPendingDisplayDeviceInfoChangesLocked()V
    .registers 1

    #@0
    .prologue
    .line 99
    return-void
.end method

.method public blankLocked()V
    .registers 1

    #@0
    .prologue
    .line 111
    return-void
.end method

.method public dumpLocked(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "mAdapter="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/server/display/DisplayDevice;->mDisplayAdapter:Lcom/android/server/display/DisplayAdapter;

    #@d
    invoke-virtual {v1}, Lcom/android/server/display/DisplayAdapter;->getName()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1c
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v1, "mDisplayToken="

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    iget-object v1, p0, Lcom/android/server/display/DisplayDevice;->mDisplayToken:Landroid/os/IBinder;

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@34
    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v1, "mCurrentLayerStack="

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    iget v1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStack:I

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4c
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v1, "mCurrentOrientation="

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    iget v1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentOrientation:I

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@64
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v1, "mCurrentLayerStackRect="

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v0

    #@6f
    iget-object v1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v0

    #@75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7c
    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v1, "mCurrentDisplayRect="

    #@83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v0

    #@87
    iget-object v1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@94
    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v1, "mCurrentSurface="

    #@9b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v0

    #@9f
    iget-object v1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentSurface:Landroid/view/Surface;

    #@a1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v0

    #@a5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v0

    #@a9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ac
    .line 211
    return-void
.end method

.method public final getAdapterLocked()Lcom/android/server/display/DisplayAdapter;
    .registers 2

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mDisplayAdapter:Lcom/android/server/display/DisplayAdapter;

    #@2
    return-object v0
.end method

.method public abstract getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;
.end method

.method public final getDisplayTokenLocked()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mDisplayToken:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public final getNameLocked()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public performTraversalInTransactionLocked()V
    .registers 1

    #@0
    .prologue
    .line 105
    return-void
.end method

.method public final populateViewportLocked(Lcom/android/server/display/DisplayViewport;)V
    .registers 6
    .parameter "viewport"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 178
    iget v2, p0, Lcom/android/server/display/DisplayDevice;->mCurrentOrientation:I

    #@3
    iput v2, p1, Lcom/android/server/display/DisplayViewport;->orientation:I

    #@5
    .line 180
    iget-object v2, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@7
    if-eqz v2, :cond_35

    #@9
    .line 181
    iget-object v2, p1, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@b
    iget-object v3, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@d
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@10
    .line 186
    :goto_10
    iget-object v2, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@12
    if-eqz v2, :cond_3b

    #@14
    .line 187
    iget-object v2, p1, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@16
    iget-object v3, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@18
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@1b
    .line 192
    :goto_1b
    iget v2, p0, Lcom/android/server/display/DisplayDevice;->mCurrentOrientation:I

    #@1d
    if-eq v2, v1, :cond_24

    #@1f
    iget v2, p0, Lcom/android/server/display/DisplayDevice;->mCurrentOrientation:I

    #@21
    const/4 v3, 0x3

    #@22
    if-ne v2, v3, :cond_41

    #@24
    .line 194
    .local v1, isRotated:Z
    :cond_24
    :goto_24
    invoke-virtual {p0}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@27
    move-result-object v0

    #@28
    .line 195
    .local v0, info:Lcom/android/server/display/DisplayDeviceInfo;
    if-eqz v1, :cond_43

    #@2a
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@2c
    :goto_2c
    iput v2, p1, Lcom/android/server/display/DisplayViewport;->deviceWidth:I

    #@2e
    .line 196
    if-eqz v1, :cond_46

    #@30
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@32
    :goto_32
    iput v2, p1, Lcom/android/server/display/DisplayViewport;->deviceHeight:I

    #@34
    .line 197
    return-void

    #@35
    .line 183
    .end local v0           #info:Lcom/android/server/display/DisplayDeviceInfo;
    .end local v1           #isRotated:Z
    :cond_35
    iget-object v2, p1, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@37
    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    #@3a
    goto :goto_10

    #@3b
    .line 189
    :cond_3b
    iget-object v2, p1, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@3d
    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    #@40
    goto :goto_1b

    #@41
    .line 192
    :cond_41
    const/4 v1, 0x0

    #@42
    goto :goto_24

    #@43
    .line 195
    .restart local v0       #info:Lcom/android/server/display/DisplayDeviceInfo;
    .restart local v1       #isRotated:Z
    :cond_43
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@45
    goto :goto_2c

    #@46
    .line 196
    :cond_46
    iget v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@48
    goto :goto_32
.end method

.method public final setLayerStackInTransactionLocked(I)V
    .registers 3
    .parameter "layerStack"

    #@0
    .prologue
    .line 123
    iget v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStack:I

    #@2
    if-eq v0, p1, :cond_b

    #@4
    .line 124
    iput p1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStack:I

    #@6
    .line 125
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mDisplayToken:Landroid/os/IBinder;

    #@8
    invoke-static {v0, p1}, Landroid/view/Surface;->setDisplayLayerStack(Landroid/os/IBinder;I)V

    #@b
    .line 127
    :cond_b
    return-void
.end method

.method public final setProjectionInTransactionLocked(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 5
    .parameter "orientation"
    .parameter "layerStackRect"
    .parameter "displayRect"

    #@0
    .prologue
    .line 141
    iget v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentOrientation:I

    #@2
    if-ne v0, p1, :cond_1c

    #@4
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@6
    if-eqz v0, :cond_1c

    #@8
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@a
    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1c

    #@10
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@12
    if-eqz v0, :cond_1c

    #@14
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@16
    invoke-virtual {v0, p3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_43

    #@1c
    .line 146
    :cond_1c
    iput p1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentOrientation:I

    #@1e
    .line 148
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@20
    if-nez v0, :cond_29

    #@22
    .line 149
    new-instance v0, Landroid/graphics/Rect;

    #@24
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@29
    .line 151
    :cond_29
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentLayerStackRect:Landroid/graphics/Rect;

    #@2b
    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@2e
    .line 153
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@30
    if-nez v0, :cond_39

    #@32
    .line 154
    new-instance v0, Landroid/graphics/Rect;

    #@34
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@37
    iput-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@39
    .line 156
    :cond_39
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentDisplayRect:Landroid/graphics/Rect;

    #@3b
    invoke-virtual {v0, p3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@3e
    .line 158
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mDisplayToken:Landroid/os/IBinder;

    #@40
    invoke-static {v0, p1, p2, p3}, Landroid/view/Surface;->setDisplayProjection(Landroid/os/IBinder;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@43
    .line 161
    :cond_43
    return-void
.end method

.method public final setSurfaceInTransactionLocked(Landroid/view/Surface;)V
    .registers 3
    .parameter "surface"

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mCurrentSurface:Landroid/view/Surface;

    #@2
    if-eq v0, p1, :cond_b

    #@4
    .line 168
    iput-object p1, p0, Lcom/android/server/display/DisplayDevice;->mCurrentSurface:Landroid/view/Surface;

    #@6
    .line 169
    iget-object v0, p0, Lcom/android/server/display/DisplayDevice;->mDisplayToken:Landroid/os/IBinder;

    #@8
    invoke-static {v0, p1}, Landroid/view/Surface;->setDisplaySurface(Landroid/os/IBinder;Landroid/view/Surface;)V

    #@b
    .line 171
    :cond_b
    return-void
.end method

.method public unblankLocked()V
    .registers 1

    #@0
    .prologue
    .line 117
    return-void
.end method
