.class public final Lcom/android/server/display/DisplayManagerService;
.super Landroid/hardware/display/IDisplayManager$Stub;
.source "DisplayManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/DisplayManagerService$1;,
        Lcom/android/server/display/DisplayManagerService$CallbackRecord;,
        Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;,
        Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;,
        Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;,
        Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;,
        Lcom/android/server/display/DisplayManagerService$SyncRoot;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DISPLAY_BLANK_STATE_BLANKED:I = 0x1

.field private static final DISPLAY_BLANK_STATE_UNBLANKED:I = 0x2

.field private static final DISPLAY_BLANK_STATE_UNKNOWN:I = 0x0

.field private static final FORCE_WIFI_DISPLAY_ENABLE:Ljava/lang/String; = "persist.debug.wfd.enable"

.field private static final MSG_DELIVER_DISPLAY_EVENT:I = 0x3

.field private static final MSG_REGISTER_ADDITIONAL_DISPLAY_ADAPTERS:I = 0x2

.field private static final MSG_REGISTER_DEFAULT_DISPLAY_ADAPTER:I = 0x1

.field private static final MSG_REQUEST_TRAVERSAL:I = 0x4

.field private static final MSG_UPDATE_VIEWPORT:I = 0x5

.field private static final SYSTEM_HEADLESS:Ljava/lang/String; = "ro.config.headless"

.field private static final TAG:Ljava/lang/String; = "DisplayManagerService"

.field private static final WAIT_FOR_DEFAULT_DISPLAY_TIMEOUT:J = 0x2710L


# instance fields
.field private mAllDisplayBlankStateFromPowerManager:I

.field public final mCallbacks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/display/DisplayManagerService$CallbackRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDefaultViewport:Lcom/android/server/display/DisplayViewport;

.field private final mDisplayAdapterListener:Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;

.field private final mDisplayAdapters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/display/DisplayAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/display/DisplayDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayTransactionListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/android/server/display/DisplayTransactionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

.field private final mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

.field private final mHeadless:Z

.field private mInputManagerFuncs:Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;

.field private final mLogicalDisplays:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/display/LogicalDisplay;",
            ">;"
        }
    .end annotation
.end field

.field private mNextNonDefaultDisplayId:I

.field public mOnlyCore:Z

.field private mPendingTraversal:Z

.field private final mPersistentDataStore:Lcom/android/server/display/PersistentDataStore;

.field private final mRemovedDisplayDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/display/DisplayDevice;",
            ">;"
        }
    .end annotation
.end field

.field public mSafeMode:Z

.field private final mSingleDisplayDemoMode:Z

.field private final mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

.field private final mTempCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/display/DisplayManagerService$CallbackRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempDefaultViewport:Lcom/android/server/display/DisplayViewport;

.field private final mTempDisplayInfo:Landroid/view/DisplayInfo;

.field private final mTempExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

.field private final mUiHandler:Landroid/os/Handler;

.field private mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

.field private mWindowManagerFuncs:Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;)V
    .registers 7
    .parameter "context"
    .parameter "mainHandler"
    .parameter "uiHandler"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 190
    invoke-direct {p0}, Landroid/hardware/display/IDisplayManager$Stub;-><init>()V

    #@4
    .line 121
    new-instance v0, Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@6
    invoke-direct {v0}, Lcom/android/server/display/DisplayManagerService$SyncRoot;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@b
    .line 139
    new-instance v0, Landroid/util/SparseArray;

    #@d
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mCallbacks:Landroid/util/SparseArray;

    #@12
    .line 143
    new-instance v0, Ljava/util/ArrayList;

    #@14
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapters:Ljava/util/ArrayList;

    #@19
    .line 146
    new-instance v0, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@20
    .line 149
    new-instance v0, Ljava/util/ArrayList;

    #@22
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mRemovedDisplayDevices:Ljava/util/ArrayList;

    #@27
    .line 152
    new-instance v0, Landroid/util/SparseArray;

    #@29
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@2c
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@2e
    .line 154
    iput v2, p0, Lcom/android/server/display/DisplayManagerService;->mNextNonDefaultDisplayId:I

    #@30
    .line 157
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    #@32
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    #@35
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayTransactionListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@37
    .line 172
    new-instance v0, Lcom/android/server/display/DisplayViewport;

    #@39
    invoke-direct {v0}, Lcom/android/server/display/DisplayViewport;-><init>()V

    #@3c
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@3e
    .line 173
    new-instance v0, Lcom/android/server/display/DisplayViewport;

    #@40
    invoke-direct {v0}, Lcom/android/server/display/DisplayViewport;-><init>()V

    #@43
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@45
    .line 176
    new-instance v0, Lcom/android/server/display/PersistentDataStore;

    #@47
    invoke-direct {v0}, Lcom/android/server/display/PersistentDataStore;-><init>()V

    #@4a
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mPersistentDataStore:Lcom/android/server/display/PersistentDataStore;

    #@4c
    .line 180
    new-instance v0, Ljava/util/ArrayList;

    #@4e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@51
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mTempCallbacks:Ljava/util/ArrayList;

    #@53
    .line 183
    new-instance v0, Landroid/view/DisplayInfo;

    #@55
    invoke-direct {v0}, Landroid/view/DisplayInfo;-><init>()V

    #@58
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mTempDisplayInfo:Landroid/view/DisplayInfo;

    #@5a
    .line 187
    new-instance v0, Lcom/android/server/display/DisplayViewport;

    #@5c
    invoke-direct {v0}, Lcom/android/server/display/DisplayViewport;-><init>()V

    #@5f
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mTempDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@61
    .line 188
    new-instance v0, Lcom/android/server/display/DisplayViewport;

    #@63
    invoke-direct {v0}, Lcom/android/server/display/DisplayViewport;-><init>()V

    #@66
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mTempExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@68
    .line 191
    iput-object p1, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@6a
    .line 192
    const-string v0, "ro.config.headless"

    #@6c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    const-string v1, "1"

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v0

    #@76
    iput-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mHeadless:Z

    #@78
    .line 194
    new-instance v0, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@7a
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@7d
    move-result-object v1

    #@7e
    invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;-><init>(Lcom/android/server/display/DisplayManagerService;Landroid/os/Looper;)V

    #@81
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@83
    .line 195
    iput-object p3, p0, Lcom/android/server/display/DisplayManagerService;->mUiHandler:Landroid/os/Handler;

    #@85
    .line 196
    new-instance v0, Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;

    #@87
    const/4 v1, 0x0

    #@88
    invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;-><init>(Lcom/android/server/display/DisplayManagerService;Lcom/android/server/display/DisplayManagerService$1;)V

    #@8b
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapterListener:Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;

    #@8d
    .line 197
    const-string v0, "persist.demo.singledisplay"

    #@8f
    const/4 v1, 0x0

    #@90
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@93
    move-result v0

    #@94
    iput-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mSingleDisplayDemoMode:Z

    #@96
    .line 199
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@98
    invoke-virtual {v0, v2}, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;->sendEmptyMessage(I)Z

    #@9b
    .line 200
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/display/DisplayManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 89
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->registerDefaultDisplayAdapter()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/display/DisplayManagerService;)Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mInputManagerFuncs:Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/server/display/DisplayManagerService;Lcom/android/server/display/DisplayDevice;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerService;->handleDisplayDeviceAdded(Lcom/android/server/display/DisplayDevice;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/display/DisplayManagerService;Lcom/android/server/display/DisplayDevice;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerService;->handleDisplayDeviceChanged(Lcom/android/server/display/DisplayDevice;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/display/DisplayManagerService;Lcom/android/server/display/DisplayDevice;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerService;->handleDisplayDeviceRemoved(Lcom/android/server/display/DisplayDevice;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/display/DisplayManagerService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/display/DisplayManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerService;->onCallbackDied(I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/display/DisplayManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 89
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->registerAdditionalDisplayAdapters()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/display/DisplayManagerService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayManagerService;->deliverDisplayEvent(II)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/display/DisplayManagerService;)Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mWindowManagerFuncs:Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/display/DisplayManagerService;)Lcom/android/server/display/DisplayManagerService$SyncRoot;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/display/DisplayManagerService;)Lcom/android/server/display/DisplayViewport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/display/DisplayManagerService;)Lcom/android/server/display/DisplayViewport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mTempDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/display/DisplayManagerService;)Lcom/android/server/display/DisplayViewport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/display/DisplayManagerService;)Lcom/android/server/display/DisplayViewport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mTempExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@2
    return-object v0
.end method

.method private addLogicalDisplayLocked(Lcom/android/server/display/DisplayDevice;)V
    .registers 11
    .parameter "device"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 674
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@5
    move-result-object v0

    #@6
    .line 675
    .local v0, deviceInfo:Lcom/android/server/display/DisplayDeviceInfo;
    iget v7, v0, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@8
    and-int/lit8 v7, v7, 0x1

    #@a
    if-eqz v7, :cond_4f

    #@c
    move v3, v5

    #@d
    .line 677
    .local v3, isDefault:Z
    :goto_d
    if-eqz v3, :cond_30

    #@f
    iget-object v7, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v7, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v6

    #@15
    if-eqz v6, :cond_30

    #@17
    .line 678
    const-string v6, "DisplayManagerService"

    #@19
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v8, "Ignoring attempt to add a second default display: "

    #@20
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v7

    #@2c
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 679
    const/4 v3, 0x0

    #@30
    .line 682
    :cond_30
    if-nez v3, :cond_51

    #@32
    iget-boolean v6, p0, Lcom/android/server/display/DisplayManagerService;->mSingleDisplayDemoMode:Z

    #@34
    if-eqz v6, :cond_51

    #@36
    .line 683
    const-string v5, "DisplayManagerService"

    #@38
    new-instance v6, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v7, "Not creating a logical display for a secondary display  because single display demo mode is enabled: "

    #@3f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 708
    :goto_4e
    return-void

    #@4f
    .end local v3           #isDefault:Z
    :cond_4f
    move v3, v6

    #@50
    .line 675
    goto :goto_d

    #@51
    .line 688
    .restart local v3       #isDefault:Z
    :cond_51
    invoke-direct {p0, v3}, Lcom/android/server/display/DisplayManagerService;->assignDisplayIdLocked(Z)I

    #@54
    move-result v2

    #@55
    .line 689
    .local v2, displayId:I
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerService;->assignLayerStackLocked(I)I

    #@58
    move-result v4

    #@59
    .line 691
    .local v4, layerStack:I
    new-instance v1, Lcom/android/server/display/LogicalDisplay;

    #@5b
    invoke-direct {v1, v2, v4, p1}, Lcom/android/server/display/LogicalDisplay;-><init>(IILcom/android/server/display/DisplayDevice;)V

    #@5e
    .line 692
    .local v1, display:Lcom/android/server/display/LogicalDisplay;
    iget-object v6, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@60
    invoke-virtual {v1, v6}, Lcom/android/server/display/LogicalDisplay;->updateLocked(Ljava/util/List;)V

    #@63
    .line 693
    invoke-virtual {v1}, Lcom/android/server/display/LogicalDisplay;->isValidLocked()Z

    #@66
    move-result v6

    #@67
    if-nez v6, :cond_82

    #@69
    .line 695
    const-string v5, "DisplayManagerService"

    #@6b
    new-instance v6, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v7, "Ignoring display device because the logical display created from it was not considered valid: "

    #@72
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v6

    #@7e
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_4e

    #@82
    .line 700
    :cond_82
    iget-object v6, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@84
    invoke-virtual {v6, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@87
    .line 703
    if-eqz v3, :cond_8e

    #@89
    .line 704
    iget-object v6, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@8b
    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    #@8e
    .line 707
    :cond_8e
    invoke-direct {p0, v2, v5}, Lcom/android/server/display/DisplayManagerService;->sendDisplayEventLocked(II)V

    #@91
    goto :goto_4e
.end method

.method private assignDisplayIdLocked(Z)I
    .registers 4
    .parameter "isDefault"

    #@0
    .prologue
    .line 711
    if-eqz p1, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    iget v0, p0, Lcom/android/server/display/DisplayManagerService;->mNextNonDefaultDisplayId:I

    #@6
    add-int/lit8 v1, v0, 0x1

    #@8
    iput v1, p0, Lcom/android/server/display/DisplayManagerService;->mNextNonDefaultDisplayId:I

    #@a
    goto :goto_3
.end method

.method private assignLayerStackLocked(I)I
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 717
    return p1
.end method

.method private canCallerConfigureWifiDisplay()Z
    .registers 3

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONFIGURE_WIFI_DISPLAY"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private clearViewportsLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 795
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@3
    iput-boolean v1, v0, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@5
    .line 796
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@7
    iput-boolean v1, v0, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@9
    .line 797
    return-void
.end method

.method private configureDisplayInTransactionLocked(Lcom/android/server/display/DisplayDevice;)V
    .registers 8
    .parameter "device"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 801
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerService;->findLogicalDisplayForDeviceLocked(Lcom/android/server/display/DisplayDevice;)Lcom/android/server/display/LogicalDisplay;

    #@5
    move-result-object v0

    #@6
    .line 802
    .local v0, display:Lcom/android/server/display/LogicalDisplay;
    if-eqz v0, :cond_f

    #@8
    invoke-virtual {v0}, Lcom/android/server/display/LogicalDisplay;->hasContentLocked()Z

    #@b
    move-result v4

    #@c
    if-nez v4, :cond_f

    #@e
    .line 803
    const/4 v0, 0x0

    #@f
    .line 805
    :cond_f
    if-nez v0, :cond_19

    #@11
    .line 806
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    .end local v0           #display:Lcom/android/server/display/LogicalDisplay;
    check-cast v0, Lcom/android/server/display/LogicalDisplay;

    #@19
    .line 810
    .restart local v0       #display:Lcom/android/server/display/LogicalDisplay;
    :cond_19
    if-nez v0, :cond_38

    #@1b
    .line 812
    const-string v3, "DisplayManagerService"

    #@1d
    new-instance v4, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v5, "Missing logical display to use for physical display device: "

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 831
    :cond_37
    :goto_37
    return-void

    #@38
    .line 816
    :cond_38
    iget v4, p0, Lcom/android/server/display/DisplayManagerService;->mAllDisplayBlankStateFromPowerManager:I

    #@3a
    if-ne v4, v2, :cond_65

    #@3c
    .line 818
    .local v2, isBlanked:Z
    :goto_3c
    invoke-virtual {v0, p1, v2}, Lcom/android/server/display/LogicalDisplay;->configureDisplayInTransactionLocked(Lcom/android/server/display/DisplayDevice;Z)V

    #@3f
    .line 822
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@42
    move-result-object v1

    #@43
    .line 823
    .local v1, info:Lcom/android/server/display/DisplayDeviceInfo;
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@45
    iget-boolean v3, v3, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@47
    if-nez v3, :cond_54

    #@49
    iget v3, v1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@4b
    and-int/lit8 v3, v3, 0x1

    #@4d
    if-eqz v3, :cond_54

    #@4f
    .line 825
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@51
    invoke-static {v3, v0, p1}, Lcom/android/server/display/DisplayManagerService;->setViewportLocked(Lcom/android/server/display/DisplayViewport;Lcom/android/server/display/LogicalDisplay;Lcom/android/server/display/DisplayDevice;)V

    #@54
    .line 827
    :cond_54
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@56
    iget-boolean v3, v3, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@58
    if-nez v3, :cond_37

    #@5a
    iget v3, v1, Lcom/android/server/display/DisplayDeviceInfo;->touch:I

    #@5c
    const/4 v4, 0x2

    #@5d
    if-ne v3, v4, :cond_37

    #@5f
    .line 829
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@61
    invoke-static {v3, v0, p1}, Lcom/android/server/display/DisplayManagerService;->setViewportLocked(Lcom/android/server/display/DisplayViewport;Lcom/android/server/display/LogicalDisplay;Lcom/android/server/display/DisplayDevice;)V

    #@64
    goto :goto_37

    #@65
    .end local v1           #info:Lcom/android/server/display/DisplayDeviceInfo;
    .end local v2           #isBlanked:Z
    :cond_65
    move v2, v3

    #@66
    .line 816
    goto :goto_3c
.end method

.method private deliverDisplayEvent(II)V
    .registers 8
    .parameter "displayId"
    .parameter "event"

    #@0
    .prologue
    .line 877
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v3

    #@3
    .line 878
    :try_start_3
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mCallbacks:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@8
    move-result v0

    #@9
    .line 879
    .local v0, count:I
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mTempCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@e
    .line 880
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_1f

    #@11
    .line 881
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mTempCallbacks:Ljava/util/ArrayList;

    #@13
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mCallbacks:Landroid/util/SparseArray;

    #@15
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 880
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_f

    #@1f
    .line 883
    :cond_1f
    monitor-exit v3
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_31

    #@20
    .line 886
    const/4 v1, 0x0

    #@21
    :goto_21
    if-ge v1, v0, :cond_34

    #@23
    .line 887
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mTempCallbacks:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v2

    #@29
    check-cast v2, Lcom/android/server/display/DisplayManagerService$CallbackRecord;

    #@2b
    invoke-virtual {v2, p1, p2}, Lcom/android/server/display/DisplayManagerService$CallbackRecord;->notifyDisplayEventAsync(II)V

    #@2e
    .line 886
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_21

    #@31
    .line 883
    .end local v0           #count:I
    .end local v1           #i:I
    :catchall_31
    move-exception v2

    #@32
    :try_start_32
    monitor-exit v3
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_31

    #@33
    throw v2

    #@34
    .line 889
    .restart local v0       #count:I
    .restart local v1       #i:I
    :cond_34
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mTempCallbacks:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@39
    .line 890
    return-void
.end method

.method private findLogicalDisplayForDeviceLocked(Lcom/android/server/display/DisplayDevice;)Lcom/android/server/display/LogicalDisplay;
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 841
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@5
    move-result v0

    #@6
    .line 842
    .local v0, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v0, :cond_1b

    #@9
    .line 843
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@b
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/display/LogicalDisplay;

    #@11
    .line 844
    .local v1, display:Lcom/android/server/display/LogicalDisplay;
    invoke-virtual {v1}, Lcom/android/server/display/LogicalDisplay;->getPrimaryDisplayDeviceLocked()Lcom/android/server/display/DisplayDevice;

    #@14
    move-result-object v3

    #@15
    if-ne v3, p1, :cond_18

    #@17
    .line 848
    .end local v1           #display:Lcom/android/server/display/LogicalDisplay;
    :goto_17
    return-object v1

    #@18
    .line 842
    .restart local v1       #display:Lcom/android/server/display/LogicalDisplay;
    :cond_18
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_7

    #@1b
    .line 848
    .end local v1           #display:Lcom/android/server/display/LogicalDisplay;
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_17
.end method

.method private handleDisplayDeviceAdded(Lcom/android/server/display/DisplayDevice;)V
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 612
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 613
    :try_start_3
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_29

    #@b
    .line 614
    const-string v0, "DisplayManagerService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Attempted to add already added display device: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 616
    monitor-exit v1

    #@28
    .line 636
    :goto_28
    return-void

    #@29
    .line 619
    :cond_29
    const-string v0, "DisplayManagerService"

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "Display device added: "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 621
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@47
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4a
    .line 622
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerService;->addLogicalDisplayLocked(Lcom/android/server/display/DisplayDevice;)V

    #@4d
    .line 623
    const/4 v0, 0x0

    #@4e
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@51
    .line 627
    iget v0, p0, Lcom/android/server/display/DisplayManagerService;->mAllDisplayBlankStateFromPowerManager:I

    #@53
    packed-switch v0, :pswitch_data_64

    #@56
    .line 635
    :goto_56
    monitor-exit v1

    #@57
    goto :goto_28

    #@58
    :catchall_58
    move-exception v0

    #@59
    monitor-exit v1
    :try_end_5a
    .catchall {:try_start_3 .. :try_end_5a} :catchall_58

    #@5a
    throw v0

    #@5b
    .line 629
    :pswitch_5b
    :try_start_5b
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->blankLocked()V

    #@5e
    goto :goto_56

    #@5f
    .line 632
    :pswitch_5f
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->unblankLocked()V
    :try_end_62
    .catchall {:try_start_5b .. :try_end_62} :catchall_58

    #@62
    goto :goto_56

    #@63
    .line 627
    nop

    #@64
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_5b
        :pswitch_5f
    .end packed-switch
.end method

.method private handleDisplayDeviceChanged(Lcom/android/server/display/DisplayDevice;)V
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 639
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 640
    :try_start_3
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_29

    #@b
    .line 641
    const-string v0, "DisplayManagerService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Attempted to change non-existent display device: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 643
    monitor-exit v1

    #@28
    .line 653
    :goto_28
    return-void

    #@29
    .line 646
    :cond_29
    const-string v0, "DisplayManagerService"

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "Display device changed: "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 648
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->applyPendingDisplayDeviceInfoChangesLocked()V

    #@48
    .line 649
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->updateLogicalDisplaysLocked()Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_52

    #@4e
    .line 650
    const/4 v0, 0x0

    #@4f
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@52
    .line 652
    :cond_52
    monitor-exit v1

    #@53
    goto :goto_28

    #@54
    :catchall_54
    move-exception v0

    #@55
    monitor-exit v1
    :try_end_56
    .catchall {:try_start_3 .. :try_end_56} :catchall_54

    #@56
    throw v0
.end method

.method private handleDisplayDeviceRemoved(Lcom/android/server/display/DisplayDevice;)V
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 656
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 657
    :try_start_3
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_29

    #@b
    .line 658
    const-string v0, "DisplayManagerService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Attempted to remove non-existent display device: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 660
    monitor-exit v1

    #@28
    .line 669
    :goto_28
    return-void

    #@29
    .line 663
    :cond_29
    const-string v0, "DisplayManagerService"

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "Display device removed: "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 665
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mRemovedDisplayDevices:Ljava/util/ArrayList;

    #@47
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4a
    .line 666
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->updateLogicalDisplaysLocked()Z

    #@4d
    .line 667
    const/4 v0, 0x0

    #@4e
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@51
    .line 668
    monitor-exit v1

    #@52
    goto :goto_28

    #@53
    :catchall_53
    move-exception v0

    #@54
    monitor-exit v1
    :try_end_55
    .catchall {:try_start_3 .. :try_end_55} :catchall_53

    #@55
    throw v0
.end method

.method private onCallbackDied(I)V
    .registers 4
    .parameter "pid"

    #@0
    .prologue
    .line 441
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 442
    :try_start_3
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mCallbacks:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@8
    .line 443
    monitor-exit v1

    #@9
    .line 444
    return-void

    #@a
    .line 443
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method private performTraversalInTransactionLocked()V
    .registers 7

    #@0
    .prologue
    .line 745
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mRemovedDisplayDevices:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v3

    #@6
    .line 746
    .local v3, removedCount:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v3, :cond_17

    #@9
    .line 747
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mRemovedDisplayDevices:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/display/DisplayDevice;

    #@11
    .line 748
    .local v1, device:Lcom/android/server/display/DisplayDevice;
    invoke-virtual {v1}, Lcom/android/server/display/DisplayDevice;->performTraversalInTransactionLocked()V

    #@14
    .line 746
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_7

    #@17
    .line 750
    .end local v1           #device:Lcom/android/server/display/DisplayDevice;
    :cond_17
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mRemovedDisplayDevices:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@1c
    .line 754
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->clearViewportsLocked()V

    #@1f
    .line 757
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@24
    move-result v0

    #@25
    .line 758
    .local v0, count:I
    const/4 v2, 0x0

    #@26
    :goto_26
    if-ge v2, v0, :cond_39

    #@28
    .line 759
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Lcom/android/server/display/DisplayDevice;

    #@30
    .line 760
    .restart local v1       #device:Lcom/android/server/display/DisplayDevice;
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerService;->configureDisplayInTransactionLocked(Lcom/android/server/display/DisplayDevice;)V

    #@33
    .line 761
    invoke-virtual {v1}, Lcom/android/server/display/DisplayDevice;->performTraversalInTransactionLocked()V

    #@36
    .line 758
    add-int/lit8 v2, v2, 0x1

    #@38
    goto :goto_26

    #@39
    .line 765
    .end local v1           #device:Lcom/android/server/display/DisplayDevice;
    :cond_39
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mInputManagerFuncs:Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;

    #@3b
    if-eqz v4, :cond_43

    #@3d
    .line 766
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@3f
    const/4 v5, 0x5

    #@40
    invoke-virtual {v4, v5}, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;->sendEmptyMessage(I)Z

    #@43
    .line 768
    :cond_43
    return-void
.end method

.method private registerAdditionalDisplayAdapters()V
    .registers 3

    #@0
    .prologue
    .line 572
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 573
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->shouldRegisterNonEssentialDisplayAdaptersLocked()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 574
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->registerOverlayDisplayAdapterLocked()V

    #@c
    .line 575
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->registerWifiDisplayAdapterLocked()V

    #@f
    .line 577
    :cond_f
    monitor-exit v1

    #@10
    .line 578
    return-void

    #@11
    .line 577
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method private registerDefaultDisplayAdapter()V
    .registers 7

    #@0
    .prologue
    .line 560
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 561
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mHeadless:Z

    #@5
    if-eqz v0, :cond_19

    #@7
    .line 562
    new-instance v0, Lcom/android/server/display/HeadlessDisplayAdapter;

    #@9
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@b
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@d
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@f
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapterListener:Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;

    #@11
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/android/server/display/HeadlessDisplayAdapter;-><init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;)V

    #@14
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->registerDisplayAdapterLocked(Lcom/android/server/display/DisplayAdapter;)V

    #@17
    .line 568
    :goto_17
    monitor-exit v1

    #@18
    .line 569
    return-void

    #@19
    .line 565
    :cond_19
    new-instance v0, Lcom/android/server/display/LocalDisplayAdapter;

    #@1b
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@1d
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@1f
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@21
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapterListener:Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;

    #@23
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/android/server/display/LocalDisplayAdapter;-><init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;)V

    #@26
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->registerDisplayAdapterLocked(Lcom/android/server/display/DisplayAdapter;)V

    #@29
    goto :goto_17

    #@2a
    .line 568
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_3 .. :try_end_2c} :catchall_2a

    #@2c
    throw v0
.end method

.method private registerDisplayAdapterLocked(Lcom/android/server/display/DisplayAdapter;)V
    .registers 3
    .parameter "adapter"

    #@0
    .prologue
    .line 607
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapters:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 608
    invoke-virtual {p1}, Lcom/android/server/display/DisplayAdapter;->registerLocked()V

    #@8
    .line 609
    return-void
.end method

.method private registerOverlayDisplayAdapterLocked()V
    .registers 7

    #@0
    .prologue
    .line 581
    new-instance v0, Lcom/android/server/display/OverlayDisplayAdapter;

    #@2
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@4
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@6
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@8
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapterListener:Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;

    #@a
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mUiHandler:Landroid/os/Handler;

    #@c
    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/OverlayDisplayAdapter;-><init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;Landroid/os/Handler;)V

    #@f
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->registerDisplayAdapterLocked(Lcom/android/server/display/DisplayAdapter;)V

    #@12
    .line 583
    return-void
.end method

.method private registerWifiDisplayAdapterLocked()V
    .registers 7

    #@0
    .prologue
    .line 586
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x1110047

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_19

    #@f
    const-string v0, "persist.debug.wfd.enable"

    #@11
    const/4 v1, -0x1

    #@12
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@15
    move-result v0

    #@16
    const/4 v1, 0x1

    #@17
    if-ne v0, v1, :cond_2f

    #@19
    .line 589
    :cond_19
    new-instance v0, Lcom/android/server/display/WifiDisplayAdapter;

    #@1b
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@1d
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@1f
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@21
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapterListener:Lcom/android/server/display/DisplayManagerService$DisplayAdapterListener;

    #@23
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mPersistentDataStore:Lcom/android/server/display/PersistentDataStore;

    #@25
    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/WifiDisplayAdapter;-><init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;Lcom/android/server/display/PersistentDataStore;)V

    #@28
    iput-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@2a
    .line 592
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@2c
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->registerDisplayAdapterLocked(Lcom/android/server/display/DisplayAdapter;)V

    #@2f
    .line 594
    :cond_2f
    return-void
.end method

.method private scheduleTraversalLocked(Z)V
    .registers 4
    .parameter "inTraversal"

    #@0
    .prologue
    .line 859
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mPendingTraversal:Z

    #@2
    if-nez v0, :cond_13

    #@4
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mWindowManagerFuncs:Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 860
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mPendingTraversal:Z

    #@b
    .line 861
    if-nez p1, :cond_13

    #@d
    .line 862
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@f
    const/4 v1, 0x4

    #@10
    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;->sendEmptyMessage(I)Z

    #@13
    .line 865
    :cond_13
    return-void
.end method

.method private sendDisplayEventLocked(II)V
    .registers 6
    .parameter "displayId"
    .parameter "event"

    #@0
    .prologue
    .line 852
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@2
    const/4 v2, 0x3

    #@3
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;->obtainMessage(III)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 853
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@9
    invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 854
    return-void
.end method

.method private static setViewportLocked(Lcom/android/server/display/DisplayViewport;Lcom/android/server/display/LogicalDisplay;Lcom/android/server/display/DisplayDevice;)V
    .registers 4
    .parameter "viewport"
    .parameter "display"
    .parameter "device"

    #@0
    .prologue
    .line 835
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@3
    .line 836
    invoke-virtual {p1}, Lcom/android/server/display/LogicalDisplay;->getDisplayIdLocked()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Lcom/android/server/display/DisplayViewport;->displayId:I

    #@9
    .line 837
    invoke-virtual {p2, p0}, Lcom/android/server/display/DisplayDevice;->populateViewportLocked(Lcom/android/server/display/DisplayViewport;)V

    #@c
    .line 838
    return-void
.end method

.method private shouldRegisterNonEssentialDisplayAdaptersLocked()Z
    .registers 2

    #@0
    .prologue
    .line 603
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mSafeMode:Z

    #@2
    if-nez v0, :cond_a

    #@4
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mOnlyCore:Z

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private updateLogicalDisplaysLocked()Z
    .registers 8

    #@0
    .prologue
    .line 724
    const/4 v0, 0x0

    #@1
    .line 725
    .local v0, changed:Z
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@6
    move-result v3

    #@7
    .local v3, i:I
    move v4, v3

    #@8
    .end local v3           #i:I
    .local v4, i:I
    :goto_8
    add-int/lit8 v3, v4, -0x1

    #@a
    .end local v4           #i:I
    .restart local v3       #i:I
    if-lez v4, :cond_4c

    #@c
    .line 726
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->keyAt(I)I

    #@11
    move-result v2

    #@12
    .line 727
    .local v2, displayId:I
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@14
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/android/server/display/LogicalDisplay;

    #@1a
    .line 729
    .local v1, display:Lcom/android/server/display/LogicalDisplay;
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mTempDisplayInfo:Landroid/view/DisplayInfo;

    #@1c
    invoke-virtual {v1}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v5, v6}, Landroid/view/DisplayInfo;->copyFrom(Landroid/view/DisplayInfo;)V

    #@23
    .line 730
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v1, v5}, Lcom/android/server/display/LogicalDisplay;->updateLocked(Ljava/util/List;)V

    #@28
    .line 731
    invoke-virtual {v1}, Lcom/android/server/display/LogicalDisplay;->isValidLocked()Z

    #@2b
    move-result v5

    #@2c
    if-nez v5, :cond_3a

    #@2e
    .line 732
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@30
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->removeAt(I)V

    #@33
    .line 733
    const/4 v5, 0x3

    #@34
    invoke-direct {p0, v2, v5}, Lcom/android/server/display/DisplayManagerService;->sendDisplayEventLocked(II)V

    #@37
    .line 734
    const/4 v0, 0x1

    #@38
    :cond_38
    :goto_38
    move v4, v3

    #@39
    .line 739
    .end local v3           #i:I
    .restart local v4       #i:I
    goto :goto_8

    #@3a
    .line 735
    .end local v4           #i:I
    .restart local v3       #i:I
    :cond_3a
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mTempDisplayInfo:Landroid/view/DisplayInfo;

    #@3c
    invoke-virtual {v1}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v5, v6}, Landroid/view/DisplayInfo;->equals(Landroid/view/DisplayInfo;)Z

    #@43
    move-result v5

    #@44
    if-nez v5, :cond_38

    #@46
    .line 736
    const/4 v5, 0x2

    #@47
    invoke-direct {p0, v2, v5}, Lcom/android/server/display/DisplayManagerService;->sendDisplayEventLocked(II)V

    #@4a
    .line 737
    const/4 v0, 0x1

    #@4b
    goto :goto_38

    #@4c
    .line 740
    .end local v1           #display:Lcom/android/server/display/LogicalDisplay;
    .end local v2           #displayId:I
    :cond_4c
    return v0
.end method


# virtual methods
.method public blankAllDisplaysFromPowerManager()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 347
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@3
    monitor-enter v4

    #@4
    .line 348
    :try_start_4
    iget v3, p0, Lcom/android/server/display/DisplayManagerService;->mAllDisplayBlankStateFromPowerManager:I

    #@6
    if-eq v3, v5, :cond_26

    #@8
    .line 349
    const/4 v3, 0x1

    #@9
    iput v3, p0, Lcom/android/server/display/DisplayManagerService;->mAllDisplayBlankStateFromPowerManager:I

    #@b
    .line 351
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v0

    #@11
    .line 352
    .local v0, count:I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v0, :cond_22

    #@14
    .line 353
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Lcom/android/server/display/DisplayDevice;

    #@1c
    .line 354
    .local v1, device:Lcom/android/server/display/DisplayDevice;
    invoke-virtual {v1}, Lcom/android/server/display/DisplayDevice;->blankLocked()V

    #@1f
    .line 352
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_12

    #@22
    .line 357
    .end local v1           #device:Lcom/android/server/display/DisplayDevice;
    :cond_22
    const/4 v3, 0x0

    #@23
    invoke-direct {p0, v3}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@26
    .line 359
    .end local v0           #count:I
    .end local v2           #i:I
    :cond_26
    monitor-exit v4

    #@27
    .line 360
    return-void

    #@28
    .line 359
    :catchall_28
    move-exception v3

    #@29
    monitor-exit v4
    :try_end_2a
    .catchall {:try_start_4 .. :try_end_2a} :catchall_28

    #@2a
    throw v3
.end method

.method public connectWifiDisplay(Ljava/lang/String;)V
    .registers 7
    .parameter "address"

    #@0
    .prologue
    .line 462
    if-nez p1, :cond_a

    #@2
    .line 463
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v4, "address must not be null"

    #@6
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v3

    #@a
    .line 466
    :cond_a
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->canCallerConfigureWifiDisplay()Z

    #@d
    move-result v2

    #@e
    .line 467
    .local v2, trusted:Z
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 469
    .local v0, token:J
    :try_start_12
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@14
    monitor-enter v4
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_26

    #@15
    .line 470
    :try_start_15
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@17
    if-eqz v3, :cond_1e

    #@19
    .line 471
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@1b
    invoke-virtual {v3, p1, v2}, Lcom/android/server/display/WifiDisplayAdapter;->requestConnectLocked(Ljava/lang/String;Z)V

    #@1e
    .line 473
    :cond_1e
    monitor-exit v4
    :try_end_1f
    .catchall {:try_start_15 .. :try_end_1f} :catchall_23

    #@1f
    .line 475
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    .line 477
    return-void

    #@23
    .line 473
    :catchall_23
    move-exception v3

    #@24
    :try_start_24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    #@25
    :try_start_25
    throw v3
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_26

    #@26
    .line 475
    :catchall_26
    move-exception v3

    #@27
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2a
    throw v3
.end method

.method public disconnectWifiDisplay()V
    .registers 5

    #@0
    .prologue
    .line 481
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 483
    .local v0, token:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@6
    monitor-enter v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_18

    #@7
    .line 484
    :try_start_7
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@9
    if-eqz v2, :cond_10

    #@b
    .line 485
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@d
    invoke-virtual {v2}, Lcom/android/server/display/WifiDisplayAdapter;->requestDisconnectLocked()V

    #@10
    .line 487
    :cond_10
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_15

    #@11
    .line 489
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    .line 491
    return-void

    #@15
    .line 487
    :catchall_15
    move-exception v2

    #@16
    :try_start_16
    monitor-exit v3
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    :try_start_17
    throw v2
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    #@18
    .line 489
    :catchall_18
    move-exception v2

    #@19
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    throw v2
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 15
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 894
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@2
    if-eqz v8, :cond_e

    #@4
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string v9, "android.permission.DUMP"

    #@8
    invoke-virtual {v8, v9}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@b
    move-result v8

    #@c
    if-eqz v8, :cond_37

    #@e
    .line 897
    :cond_e
    new-instance v8, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v9, "Permission Denial: can\'t dump DisplayManager from from pid="

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1c
    move-result v9

    #@1d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    const-string v9, ", uid="

    #@23
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v8

    #@27
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2a
    move-result v9

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@36
    .line 943
    :goto_36
    return-void

    #@37
    .line 902
    :cond_37
    const-string v8, "DISPLAY MANAGER (dumpsys display)"

    #@39
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3c
    .line 904
    iget-object v9, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@3e
    monitor-enter v9

    #@3f
    .line 905
    :try_start_3f
    new-instance v8, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v10, "  mHeadless="

    #@46
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v8

    #@4a
    iget-boolean v10, p0, Lcom/android/server/display/DisplayManagerService;->mHeadless:Z

    #@4c
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v8

    #@50
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v8

    #@54
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@57
    .line 906
    new-instance v8, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v10, "  mOnlyCode="

    #@5e
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v8

    #@62
    iget-boolean v10, p0, Lcom/android/server/display/DisplayManagerService;->mOnlyCore:Z

    #@64
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@67
    move-result-object v8

    #@68
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6f
    .line 907
    new-instance v8, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v10, "  mSafeMode="

    #@76
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    iget-boolean v10, p0, Lcom/android/server/display/DisplayManagerService;->mSafeMode:Z

    #@7c
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v8

    #@80
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v8

    #@84
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@87
    .line 908
    new-instance v8, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v10, "  mPendingTraversal="

    #@8e
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    iget-boolean v10, p0, Lcom/android/server/display/DisplayManagerService;->mPendingTraversal:Z

    #@94
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@97
    move-result-object v8

    #@98
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v8

    #@9c
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9f
    .line 909
    new-instance v8, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v10, "  mAllDisplayBlankStateFromPowerManager="

    #@a6
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v8

    #@aa
    iget v10, p0, Lcom/android/server/display/DisplayManagerService;->mAllDisplayBlankStateFromPowerManager:I

    #@ac
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v8

    #@b4
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b7
    .line 911
    new-instance v8, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v10, "  mNextNonDefaultDisplayId="

    #@be
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v8

    #@c2
    iget v10, p0, Lcom/android/server/display/DisplayManagerService;->mNextNonDefaultDisplayId:I

    #@c4
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v8

    #@c8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v8

    #@cc
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@cf
    .line 912
    new-instance v8, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v10, "  mDefaultViewport="

    #@d6
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v8

    #@da
    iget-object v10, p0, Lcom/android/server/display/DisplayManagerService;->mDefaultViewport:Lcom/android/server/display/DisplayViewport;

    #@dc
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v8

    #@e0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v8

    #@e4
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e7
    .line 913
    new-instance v8, Ljava/lang/StringBuilder;

    #@e9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ec
    const-string v10, "  mExternalTouchViewport="

    #@ee
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v8

    #@f2
    iget-object v10, p0, Lcom/android/server/display/DisplayManagerService;->mExternalTouchViewport:Lcom/android/server/display/DisplayViewport;

    #@f4
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v8

    #@f8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v8

    #@fc
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ff
    .line 914
    new-instance v8, Ljava/lang/StringBuilder;

    #@101
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v10, "  mSingleDisplayDemoMode="

    #@106
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v8

    #@10a
    iget-boolean v10, p0, Lcom/android/server/display/DisplayManagerService;->mSingleDisplayDemoMode:Z

    #@10c
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v8

    #@110
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v8

    #@114
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@117
    .line 916
    new-instance v6, Lcom/android/internal/util/IndentingPrintWriter;

    #@119
    const-string v8, "    "

    #@11b
    invoke-direct {v6, p2, v8}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@11e
    .line 917
    .local v6, ipw:Lcom/android/internal/util/IndentingPrintWriter;
    invoke-virtual {v6}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@121
    .line 919
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@124
    .line 920
    new-instance v8, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v10, "Display Adapters: size="

    #@12b
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v8

    #@12f
    iget-object v10, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapters:Ljava/util/ArrayList;

    #@131
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@134
    move-result v10

    #@135
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@138
    move-result-object v8

    #@139
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v8

    #@13d
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@140
    .line 921
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayAdapters:Ljava/util/ArrayList;

    #@142
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@145
    move-result-object v5

    #@146
    .local v5, i$:Ljava/util/Iterator;
    :goto_146
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@149
    move-result v8

    #@14a
    if-eqz v8, :cond_173

    #@14c
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14f
    move-result-object v0

    #@150
    check-cast v0, Lcom/android/server/display/DisplayAdapter;

    #@152
    .line 922
    .local v0, adapter:Lcom/android/server/display/DisplayAdapter;
    new-instance v8, Ljava/lang/StringBuilder;

    #@154
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v10, "  "

    #@159
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v8

    #@15d
    invoke-virtual {v0}, Lcom/android/server/display/DisplayAdapter;->getName()Ljava/lang/String;

    #@160
    move-result-object v10

    #@161
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v8

    #@165
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v8

    #@169
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16c
    .line 923
    invoke-virtual {v0, v6}, Lcom/android/server/display/DisplayAdapter;->dumpLocked(Ljava/io/PrintWriter;)V

    #@16f
    goto :goto_146

    #@170
    .line 942
    .end local v0           #adapter:Lcom/android/server/display/DisplayAdapter;
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v6           #ipw:Lcom/android/internal/util/IndentingPrintWriter;
    :catchall_170
    move-exception v8

    #@171
    monitor-exit v9
    :try_end_172
    .catchall {:try_start_3f .. :try_end_172} :catchall_170

    #@172
    throw v8

    #@173
    .line 926
    .restart local v5       #i$:Ljava/util/Iterator;
    .restart local v6       #ipw:Lcom/android/internal/util/IndentingPrintWriter;
    :cond_173
    :try_start_173
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@176
    .line 927
    new-instance v8, Ljava/lang/StringBuilder;

    #@178
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@17b
    const-string v10, "Display Devices: size="

    #@17d
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v8

    #@181
    iget-object v10, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@183
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@186
    move-result v10

    #@187
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v8

    #@18b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18e
    move-result-object v8

    #@18f
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@192
    .line 928
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@194
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@197
    move-result-object v5

    #@198
    :goto_198
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@19b
    move-result v8

    #@19c
    if-eqz v8, :cond_1c2

    #@19e
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a1
    move-result-object v1

    #@1a2
    check-cast v1, Lcom/android/server/display/DisplayDevice;

    #@1a4
    .line 929
    .local v1, device:Lcom/android/server/display/DisplayDevice;
    new-instance v8, Ljava/lang/StringBuilder;

    #@1a6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1a9
    const-string v10, "  "

    #@1ab
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v8

    #@1af
    invoke-virtual {v1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    #@1b2
    move-result-object v10

    #@1b3
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v8

    #@1b7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ba
    move-result-object v8

    #@1bb
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1be
    .line 930
    invoke-virtual {v1, v6}, Lcom/android/server/display/DisplayDevice;->dumpLocked(Ljava/io/PrintWriter;)V

    #@1c1
    goto :goto_198

    #@1c2
    .line 933
    .end local v1           #device:Lcom/android/server/display/DisplayDevice;
    :cond_1c2
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@1c4
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    #@1c7
    move-result v7

    #@1c8
    .line 934
    .local v7, logicalDisplayCount:I
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@1cb
    .line 935
    new-instance v8, Ljava/lang/StringBuilder;

    #@1cd
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1d0
    const-string v10, "Logical Displays: size="

    #@1d2
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v8

    #@1d6
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v8

    #@1da
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dd
    move-result-object v8

    #@1de
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e1
    .line 936
    const/4 v4, 0x0

    #@1e2
    .local v4, i:I
    :goto_1e2
    if-ge v4, v7, :cond_214

    #@1e4
    .line 937
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@1e6
    invoke-virtual {v8, v4}, Landroid/util/SparseArray;->keyAt(I)I

    #@1e9
    move-result v3

    #@1ea
    .line 938
    .local v3, displayId:I
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@1ec
    invoke-virtual {v8, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@1ef
    move-result-object v2

    #@1f0
    check-cast v2, Lcom/android/server/display/LogicalDisplay;

    #@1f2
    .line 939
    .local v2, display:Lcom/android/server/display/LogicalDisplay;
    new-instance v8, Ljava/lang/StringBuilder;

    #@1f4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f7
    const-string v10, "  Display "

    #@1f9
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v8

    #@1fd
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@200
    move-result-object v8

    #@201
    const-string v10, ":"

    #@203
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v8

    #@207
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20a
    move-result-object v8

    #@20b
    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20e
    .line 940
    invoke-virtual {v2, v6}, Lcom/android/server/display/LogicalDisplay;->dumpLocked(Ljava/io/PrintWriter;)V

    #@211
    .line 936
    add-int/lit8 v4, v4, 0x1

    #@213
    goto :goto_1e2

    #@214
    .line 942
    .end local v2           #display:Lcom/android/server/display/LogicalDisplay;
    .end local v3           #displayId:I
    :cond_214
    monitor-exit v9
    :try_end_215
    .catchall {:try_start_173 .. :try_end_215} :catchall_170

    #@215
    goto/16 :goto_36
.end method

.method public forgetWifiDisplay(Ljava/lang/String;)V
    .registers 6
    .parameter "address"

    #@0
    .prologue
    .line 517
    if-nez p1, :cond_a

    #@2
    .line 518
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v3, "address must not be null"

    #@6
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v2

    #@a
    .line 520
    :cond_a
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->canCallerConfigureWifiDisplay()Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_18

    #@10
    .line 521
    new-instance v2, Ljava/lang/SecurityException;

    #@12
    const-string v3, "Requires CONFIGURE_WIFI_DISPLAY permission to forget a wifi display."

    #@14
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 525
    :cond_18
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1b
    move-result-wide v0

    #@1c
    .line 527
    .local v0, token:J
    :try_start_1c
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@1e
    monitor-enter v3
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_30

    #@1f
    .line 528
    :try_start_1f
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@21
    if-eqz v2, :cond_28

    #@23
    .line 529
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@25
    invoke-virtual {v2, p1}, Lcom/android/server/display/WifiDisplayAdapter;->requestForgetLocked(Ljava/lang/String;)V

    #@28
    .line 531
    :cond_28
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_1f .. :try_end_29} :catchall_2d

    #@29
    .line 533
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2c
    .line 535
    return-void

    #@2d
    .line 531
    :catchall_2d
    move-exception v2

    #@2e
    :try_start_2e
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    :try_start_2f
    throw v2
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_30

    #@30
    .line 533
    :catchall_30
    move-exception v2

    #@31
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@34
    throw v2
.end method

.method public getDisplayIds()[I
    .registers 6

    #@0
    .prologue
    .line 404
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v4

    #@3
    .line 405
    :try_start_3
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@8
    move-result v0

    #@9
    .line 406
    .local v0, count:I
    new-array v1, v0, [I

    #@b
    .line 407
    .local v1, displayIds:[I
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v0, :cond_19

    #@e
    .line 408
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@10
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    #@13
    move-result v3

    #@14
    aput v3, v1, v2

    #@16
    .line 407
    add-int/lit8 v2, v2, 0x1

    #@18
    goto :goto_c

    #@19
    .line 410
    :cond_19
    monitor-exit v4

    #@1a
    return-object v1

    #@1b
    .line 411
    .end local v0           #count:I
    .end local v1           #displayIds:[I
    .end local v2           #i:I
    :catchall_1b
    move-exception v3

    #@1c
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v3
.end method

.method public getDisplayInfo(I)Landroid/view/DisplayInfo;
    .registers 5
    .parameter "displayId"

    #@0
    .prologue
    .line 390
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v2

    #@3
    .line 391
    :try_start_3
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/server/display/LogicalDisplay;

    #@b
    .line 392
    .local v0, display:Lcom/android/server/display/LogicalDisplay;
    if-eqz v0, :cond_13

    #@d
    .line 393
    invoke-virtual {v0}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    #@10
    move-result-object v1

    #@11
    monitor-exit v2

    #@12
    .line 395
    :goto_12
    return-object v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    monitor-exit v2

    #@15
    goto :goto_12

    #@16
    .line 396
    .end local v0           #display:Lcom/android/server/display/LogicalDisplay;
    :catchall_16
    move-exception v1

    #@17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v1
.end method

.method public getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;
    .registers 5

    #@0
    .prologue
    .line 539
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 541
    .local v0, token:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@6
    monitor-enter v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_23

    #@7
    .line 542
    :try_start_7
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@9
    if-eqz v2, :cond_16

    #@b
    .line 543
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@d
    invoke-virtual {v2}, Lcom/android/server/display/WifiDisplayAdapter;->getWifiDisplayStatusLocked()Landroid/hardware/display/WifiDisplayStatus;

    #@10
    move-result-object v2

    #@11
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_7 .. :try_end_12} :catchall_20

    #@12
    .line 549
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@15
    :goto_15
    return-object v2

    #@16
    .line 545
    :cond_16
    :try_start_16
    new-instance v2, Landroid/hardware/display/WifiDisplayStatus;

    #@18
    invoke-direct {v2}, Landroid/hardware/display/WifiDisplayStatus;-><init>()V

    #@1b
    monitor-exit v3
    :try_end_1c
    .catchall {:try_start_16 .. :try_end_1c} :catchall_20

    #@1c
    .line 549
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    goto :goto_15

    #@20
    .line 547
    :catchall_20
    move-exception v2

    #@21
    :try_start_21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    :try_start_22
    throw v2
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_23

    #@23
    .line 549
    :catchall_23
    move-exception v2

    #@24
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@27
    throw v2
.end method

.method public isHeadless()Z
    .registers 2

    #@0
    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerService;->mHeadless:Z

    #@2
    return v0
.end method

.method public performTraversalInTransactionFromWindowManager()V
    .registers 5

    #@0
    .prologue
    .line 328
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v3

    #@3
    .line 329
    :try_start_3
    iget-boolean v2, p0, Lcom/android/server/display/DisplayManagerService;->mPendingTraversal:Z

    #@5
    if-nez v2, :cond_9

    #@7
    .line 330
    monitor-exit v3

    #@8
    .line 341
    :cond_8
    return-void

    #@9
    .line 332
    :cond_9
    const/4 v2, 0x0

    #@a
    iput-boolean v2, p0, Lcom/android/server/display/DisplayManagerService;->mPendingTraversal:Z

    #@c
    .line 334
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->performTraversalInTransactionLocked()V

    #@f
    .line 335
    monitor-exit v3
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_26

    #@10
    .line 338
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayTransactionListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@12
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v0

    #@16
    .local v0, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_8

    #@1c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Lcom/android/server/display/DisplayTransactionListener;

    #@22
    .line 339
    .local v1, listener:Lcom/android/server/display/DisplayTransactionListener;
    invoke-interface {v1}, Lcom/android/server/display/DisplayTransactionListener;->onDisplayTransaction()V

    #@25
    goto :goto_16

    #@26
    .line 335
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Lcom/android/server/display/DisplayTransactionListener;
    :catchall_26
    move-exception v2

    #@27
    :try_start_27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method

.method public registerCallback(Landroid/hardware/display/IDisplayManagerCallback;)V
    .registers 9
    .parameter "callback"

    #@0
    .prologue
    .line 416
    if-nez p1, :cond_a

    #@2
    .line 417
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v5, "listener must not be null"

    #@6
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v4

    #@a
    .line 420
    :cond_a
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@c
    monitor-enter v5

    #@d
    .line 421
    :try_start_d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v1

    #@11
    .line 422
    .local v1, callingPid:I
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mCallbacks:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v4

    #@17
    if-eqz v4, :cond_24

    #@19
    .line 423
    new-instance v4, Ljava/lang/SecurityException;

    #@1b
    const-string v6, "The calling process has already registered an IDisplayManagerCallback."

    #@1d
    invoke-direct {v4, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@20
    throw v4

    #@21
    .line 437
    .end local v1           #callingPid:I
    :catchall_21
    move-exception v4

    #@22
    monitor-exit v5
    :try_end_23
    .catchall {:try_start_d .. :try_end_23} :catchall_21

    #@23
    throw v4

    #@24
    .line 427
    .restart local v1       #callingPid:I
    :cond_24
    :try_start_24
    new-instance v3, Lcom/android/server/display/DisplayManagerService$CallbackRecord;

    #@26
    invoke-direct {v3, p0, v1, p1}, Lcom/android/server/display/DisplayManagerService$CallbackRecord;-><init>(Lcom/android/server/display/DisplayManagerService;ILandroid/hardware/display/IDisplayManagerCallback;)V
    :try_end_29
    .catchall {:try_start_24 .. :try_end_29} :catchall_21

    #@29
    .line 429
    .local v3, record:Lcom/android/server/display/DisplayManagerService$CallbackRecord;
    :try_start_29
    invoke-interface {p1}, Landroid/hardware/display/IDisplayManagerCallback;->asBinder()Landroid/os/IBinder;

    #@2c
    move-result-object v0

    #@2d
    .line 430
    .local v0, binder:Landroid/os/IBinder;
    const/4 v4, 0x0

    #@2e
    invoke-interface {v0, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_31
    .catchall {:try_start_29 .. :try_end_31} :catchall_21
    .catch Landroid/os/RemoteException; {:try_start_29 .. :try_end_31} :catch_38

    #@31
    .line 436
    :try_start_31
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mCallbacks:Landroid/util/SparseArray;

    #@33
    invoke-virtual {v4, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@36
    .line 437
    monitor-exit v5

    #@37
    .line 438
    return-void

    #@38
    .line 431
    .end local v0           #binder:Landroid/os/IBinder;
    :catch_38
    move-exception v2

    #@39
    .line 433
    .local v2, ex:Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@3b
    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@3e
    throw v4
    :try_end_3f
    .catchall {:try_start_31 .. :try_end_3f} :catchall_21
.end method

.method public registerDisplayTransactionListener(Lcom/android/server/display/DisplayTransactionListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 275
    if-nez p1, :cond_a

    #@2
    .line 276
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "listener must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 280
    :cond_a
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayTransactionListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@c
    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    #@f
    .line 281
    return-void
.end method

.method public renameWifiDisplay(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "address"
    .parameter "alias"

    #@0
    .prologue
    .line 495
    if-nez p1, :cond_a

    #@2
    .line 496
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v3, "address must not be null"

    #@6
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v2

    #@a
    .line 498
    :cond_a
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerService;->canCallerConfigureWifiDisplay()Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_18

    #@10
    .line 499
    new-instance v2, Ljava/lang/SecurityException;

    #@12
    const-string v3, "Requires CONFIGURE_WIFI_DISPLAY permission to rename a wifi display."

    #@14
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 503
    :cond_18
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1b
    move-result-wide v0

    #@1c
    .line 505
    .local v0, token:J
    :try_start_1c
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@1e
    monitor-enter v3
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_30

    #@1f
    .line 506
    :try_start_1f
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@21
    if-eqz v2, :cond_28

    #@23
    .line 507
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@25
    invoke-virtual {v2, p1, p2}, Lcom/android/server/display/WifiDisplayAdapter;->requestRenameLocked(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 509
    :cond_28
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_1f .. :try_end_29} :catchall_2d

    #@29
    .line 511
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2c
    .line 513
    return-void

    #@2d
    .line 509
    :catchall_2d
    move-exception v2

    #@2e
    :try_start_2e
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    :try_start_2f
    throw v2
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_30

    #@30
    .line 511
    :catchall_30
    move-exception v2

    #@31
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@34
    throw v2
.end method

.method public scanWifiDisplays()V
    .registers 5

    #@0
    .prologue
    .line 448
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 450
    .local v0, token:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@6
    monitor-enter v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_18

    #@7
    .line 451
    :try_start_7
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@9
    if-eqz v2, :cond_10

    #@b
    .line 452
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mWifiDisplayAdapter:Lcom/android/server/display/WifiDisplayAdapter;

    #@d
    invoke-virtual {v2}, Lcom/android/server/display/WifiDisplayAdapter;->requestScanLocked()V

    #@10
    .line 454
    :cond_10
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_15

    #@11
    .line 456
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    .line 458
    return-void

    #@15
    .line 454
    :catchall_15
    move-exception v2

    #@16
    :try_start_16
    monitor-exit v3
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    :try_start_17
    throw v2
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    #@18
    .line 456
    :catchall_18
    move-exception v2

    #@19
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    throw v2
.end method

.method public setDisplayHasContent(IZZ)V
    .registers 7
    .parameter "displayId"
    .parameter "hasContent"
    .parameter "inTraversal"

    #@0
    .prologue
    .line 785
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v2

    #@3
    .line 786
    :try_start_3
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/server/display/LogicalDisplay;

    #@b
    .line 787
    .local v0, display:Lcom/android/server/display/LogicalDisplay;
    if-eqz v0, :cond_19

    #@d
    invoke-virtual {v0}, Lcom/android/server/display/LogicalDisplay;->hasContentLocked()Z

    #@10
    move-result v1

    #@11
    if-eq v1, p2, :cond_19

    #@13
    .line 788
    invoke-virtual {v0, p2}, Lcom/android/server/display/LogicalDisplay;->setHasContentLocked(Z)V

    #@16
    .line 789
    invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@19
    .line 791
    :cond_19
    monitor-exit v2

    #@1a
    .line 792
    return-void

    #@1b
    .line 791
    .end local v0           #display:Lcom/android/server/display/LogicalDisplay;
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v1
.end method

.method public setDisplayInfoOverrideFromWindowManager(ILandroid/view/DisplayInfo;)V
    .registers 7
    .parameter "displayId"
    .parameter "info"

    #@0
    .prologue
    .line 310
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v2

    #@3
    .line 311
    :try_start_3
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/server/display/LogicalDisplay;

    #@b
    .line 312
    .local v0, display:Lcom/android/server/display/LogicalDisplay;
    if-eqz v0, :cond_2d

    #@d
    .line 313
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mTempDisplayInfo:Landroid/view/DisplayInfo;

    #@f
    invoke-virtual {v0}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v1, v3}, Landroid/view/DisplayInfo;->copyFrom(Landroid/view/DisplayInfo;)V

    #@16
    .line 314
    invoke-virtual {v0, p2}, Lcom/android/server/display/LogicalDisplay;->setDisplayInfoOverrideFromWindowManagerLocked(Landroid/view/DisplayInfo;)V

    #@19
    .line 315
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mTempDisplayInfo:Landroid/view/DisplayInfo;

    #@1b
    invoke-virtual {v0}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v1, v3}, Landroid/view/DisplayInfo;->equals(Landroid/view/DisplayInfo;)Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_2d

    #@25
    .line 316
    const/4 v1, 0x2

    #@26
    invoke-direct {p0, p1, v1}, Lcom/android/server/display/DisplayManagerService;->sendDisplayEventLocked(II)V

    #@29
    .line 317
    const/4 v1, 0x0

    #@2a
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@2d
    .line 320
    :cond_2d
    monitor-exit v2

    #@2e
    .line 321
    return-void

    #@2f
    .line 320
    .end local v0           #display:Lcom/android/server/display/LogicalDisplay;
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    #@31
    throw v1
.end method

.method public setInputManager(Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;)V
    .registers 4
    .parameter "inputManagerFuncs"

    #@0
    .prologue
    .line 241
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 242
    :try_start_3
    iput-object p1, p0, Lcom/android/server/display/DisplayManagerService;->mInputManagerFuncs:Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;

    #@5
    .line 243
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@9
    .line 244
    monitor-exit v1

    #@a
    .line 245
    return-void

    #@b
    .line 244
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public setWindowManager(Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;)V
    .registers 4
    .parameter "windowManagerFuncs"

    #@0
    .prologue
    .line 230
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 231
    :try_start_3
    iput-object p1, p0, Lcom/android/server/display/DisplayManagerService;->mWindowManagerFuncs:Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;

    #@5
    .line 232
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@9
    .line 233
    monitor-exit v1

    #@a
    .line 234
    return-void

    #@b
    .line 233
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public systemReady(ZZ)V
    .registers 5
    .parameter "safeMode"
    .parameter "onlyCore"

    #@0
    .prologue
    .line 251
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@2
    monitor-enter v1

    #@3
    .line 252
    :try_start_3
    iput-boolean p1, p0, Lcom/android/server/display/DisplayManagerService;->mSafeMode:Z

    #@5
    .line 253
    iput-boolean p2, p0, Lcom/android/server/display/DisplayManagerService;->mOnlyCore:Z

    #@7
    .line 254
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_f

    #@8
    .line 256
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mHandler:Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;

    #@a
    const/4 v1, 0x2

    #@b
    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayManagerService$DisplayManagerHandler;->sendEmptyMessage(I)Z

    #@e
    .line 257
    return-void

    #@f
    .line 254
    :catchall_f
    move-exception v0

    #@10
    :try_start_10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public unblankAllDisplaysFromPowerManager()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 366
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@3
    monitor-enter v4

    #@4
    .line 367
    :try_start_4
    iget v3, p0, Lcom/android/server/display/DisplayManagerService;->mAllDisplayBlankStateFromPowerManager:I

    #@6
    if-eq v3, v5, :cond_26

    #@8
    .line 368
    const/4 v3, 0x2

    #@9
    iput v3, p0, Lcom/android/server/display/DisplayManagerService;->mAllDisplayBlankStateFromPowerManager:I

    #@b
    .line 370
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v0

    #@11
    .line 371
    .local v0, count:I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v0, :cond_22

    #@14
    .line 372
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayDevices:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Lcom/android/server/display/DisplayDevice;

    #@1c
    .line 373
    .local v1, device:Lcom/android/server/display/DisplayDevice;
    invoke-virtual {v1}, Lcom/android/server/display/DisplayDevice;->unblankLocked()V

    #@1f
    .line 371
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_12

    #@22
    .line 376
    .end local v1           #device:Lcom/android/server/display/DisplayDevice;
    :cond_22
    const/4 v3, 0x0

    #@23
    invoke-direct {p0, v3}, Lcom/android/server/display/DisplayManagerService;->scheduleTraversalLocked(Z)V

    #@26
    .line 378
    .end local v0           #count:I
    .end local v2           #i:I
    :cond_26
    monitor-exit v4

    #@27
    .line 379
    return-void

    #@28
    .line 378
    :catchall_28
    move-exception v3

    #@29
    monitor-exit v4
    :try_end_2a
    .catchall {:try_start_4 .. :try_end_2a} :catchall_28

    #@2a
    throw v3
.end method

.method public unregisterDisplayTransactionListener(Lcom/android/server/display/DisplayTransactionListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 290
    if-nez p1, :cond_a

    #@2
    .line 291
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "listener must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 295
    :cond_a
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerService;->mDisplayTransactionListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@c
    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@f
    .line 296
    return-void
.end method

.method public waitForDefaultDisplay()Z
    .registers 11

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 206
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@3
    monitor-enter v5

    #@4
    .line 207
    :try_start_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v6

    #@8
    const-wide/16 v8, 0x2710

    #@a
    add-long v2, v6, v8

    #@c
    .line 208
    .local v2, timeout:J
    :goto_c
    iget-object v6, p0, Lcom/android/server/display/DisplayManagerService;->mLogicalDisplays:Landroid/util/SparseArray;

    #@e
    const/4 v7, 0x0

    #@f
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v6

    #@13
    if-nez v6, :cond_2b

    #@15
    .line 209
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@18
    move-result-wide v6

    #@19
    sub-long v0, v2, v6

    #@1b
    .line 210
    .local v0, delay:J
    const-wide/16 v6, 0x0

    #@1d
    cmp-long v6, v0, v6

    #@1f
    if-gtz v6, :cond_23

    #@21
    .line 211
    monitor-exit v5
    :try_end_22
    .catchall {:try_start_4 .. :try_end_22} :catchall_2e

    #@22
    .line 222
    .end local v0           #delay:J
    :goto_22
    return v4

    #@23
    .line 217
    .restart local v0       #delay:J
    :cond_23
    :try_start_23
    iget-object v6, p0, Lcom/android/server/display/DisplayManagerService;->mSyncRoot:Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@25
    invoke-virtual {v6, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_28
    .catchall {:try_start_23 .. :try_end_28} :catchall_2e
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_28} :catch_29

    #@28
    goto :goto_c

    #@29
    .line 218
    :catch_29
    move-exception v6

    #@2a
    goto :goto_c

    #@2b
    .line 221
    .end local v0           #delay:J
    :cond_2b
    :try_start_2b
    monitor-exit v5

    #@2c
    .line 222
    const/4 v4, 0x1

    #@2d
    goto :goto_22

    #@2e
    .line 221
    .end local v2           #timeout:J
    :catchall_2e
    move-exception v4

    #@2f
    monitor-exit v5
    :try_end_30
    .catchall {:try_start_2b .. :try_end_30} :catchall_2e

    #@30
    throw v4
.end method
