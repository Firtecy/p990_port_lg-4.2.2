.class Lcom/android/server/display/ExtendedRemoteDisplayHelper;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplayHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ExtendedRemoteDisplayHelper"

.field private static sExtRemoteDisplayClass:Ljava/lang/Class;

.field private static sExtRemoteDisplayDispose:Ljava/lang/reflect/Method;

.field private static sExtRemoteDisplayListen:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 61
    :try_start_0
    const-string v2, "com.qualcomm.wfd.ExtendedRemoteDisplay"

    #@2
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@5
    move-result-object v2

    #@6
    sput-object v2, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayClass:Ljava/lang/Class;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_8} :catch_42

    #@8
    .line 66
    .local v1, t:Ljava/lang/Throwable;
    :goto_8
    sget-object v2, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayClass:Ljava/lang/Class;

    #@a
    if-eqz v2, :cond_41

    #@c
    .line 68
    const-string v2, "ExtendedRemoteDisplayHelper"

    #@e
    const-string v3, "ExtendedRemoteDisplay Is available. Find Methods"

    #@10
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 70
    const/4 v2, 0x4

    #@14
    :try_start_14
    new-array v0, v2, [Ljava/lang/Class;

    #@16
    const/4 v2, 0x0

    #@17
    const-class v3, Ljava/lang/String;

    #@19
    aput-object v3, v0, v2

    #@1b
    const/4 v2, 0x1

    #@1c
    const-class v3, Landroid/media/RemoteDisplay$Listener;

    #@1e
    aput-object v3, v0, v2

    #@20
    const/4 v2, 0x2

    #@21
    const-class v3, Landroid/os/Handler;

    #@23
    aput-object v3, v0, v2

    #@25
    const/4 v2, 0x3

    #@26
    const-class v3, Landroid/content/Context;

    #@28
    aput-object v3, v0, v2

    #@2a
    .line 75
    .local v0, args:[Ljava/lang/Class;
    sget-object v2, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayClass:Ljava/lang/Class;

    #@2c
    .end local v1           #t:Ljava/lang/Throwable;
    const-string v3, "listen"

    #@2e
    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@31
    move-result-object v2

    #@32
    sput-object v2, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayListen:Ljava/lang/reflect/Method;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_34} :catch_4b

    #@34
    .line 81
    .end local v0           #args:[Ljava/lang/Class;
    :goto_34
    const/4 v2, 0x0

    #@35
    :try_start_35
    new-array v0, v2, [Ljava/lang/Class;

    #@37
    .line 82
    .restart local v0       #args:[Ljava/lang/Class;
    sget-object v2, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayClass:Ljava/lang/Class;

    #@39
    const-string v3, "dispose"

    #@3b
    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@3e
    move-result-object v2

    #@3f
    sput-object v2, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayDispose:Ljava/lang/reflect/Method;
    :try_end_41
    .catch Ljava/lang/Throwable; {:try_start_35 .. :try_end_41} :catch_54

    #@41
    .line 87
    .end local v0           #args:[Ljava/lang/Class;
    .restart local v1       #t:Ljava/lang/Throwable;
    :cond_41
    :goto_41
    return-void

    #@42
    .line 62
    .end local v1           #t:Ljava/lang/Throwable;
    :catch_42
    move-exception v1

    #@43
    .line 63
    .restart local v1       #t:Ljava/lang/Throwable;
    const-string v2, "ExtendedRemoteDisplayHelper"

    #@45
    const-string v3, "ExtendedRemoteDisplay Not available."

    #@47
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_8

    #@4b
    .line 76
    :catch_4b
    move-exception v1

    #@4c
    .line 77
    const-string v2, "ExtendedRemoteDisplayHelper"

    #@4e
    const-string v3, "ExtendedRemoteDisplay.listen Not available."

    #@50
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_34

    #@54
    .line 83
    .end local v1           #t:Ljava/lang/Throwable;
    :catch_54
    move-exception v1

    #@55
    .line 84
    .restart local v1       #t:Ljava/lang/Throwable;
    const-string v2, "ExtendedRemoteDisplayHelper"

    #@57
    const-string v3, "ExtendedRemoteDisplay.dispose Not available."

    #@59
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_41
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static dispose(Ljava/lang/Object;)V
    .registers 5
    .parameter "extRemoteDisplay"

    #@0
    .prologue
    .line 130
    const-string v2, "ExtendedRemoteDisplayHelper"

    #@2
    const-string v3, "ExtendedRemoteDisplay.dispose"

    #@4
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 132
    :try_start_7
    sget-object v2, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayDispose:Ljava/lang/reflect/Method;

    #@9
    const/4 v3, 0x0

    #@a
    new-array v3, v3, [Ljava/lang/Object;

    #@c
    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_7 .. :try_end_f} :catch_10
    .catch Ljava/lang/IllegalAccessException; {:try_start_7 .. :try_end_f} :catch_30

    #@f
    .line 147
    :goto_f
    return-void

    #@10
    .line 133
    :catch_10
    move-exception v1

    #@11
    .line 134
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "ExtendedRemoteDisplayHelper"

    #@13
    const-string v3, "ExtendedRemoteDisplay.dispose - InvocationTargetException"

    #@15
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 135
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@1b
    move-result-object v0

    #@1c
    .line 136
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v2, v0, Ljava/lang/RuntimeException;

    #@1e
    if-eqz v2, :cond_23

    #@20
    .line 137
    check-cast v0, Ljava/lang/RuntimeException;

    #@22
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@23
    .line 138
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_23
    instance-of v2, v0, Ljava/lang/Error;

    #@25
    if-eqz v2, :cond_2a

    #@27
    .line 139
    check-cast v0, Ljava/lang/Error;

    #@29
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@2a
    .line 141
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_2a
    new-instance v2, Ljava/lang/RuntimeException;

    #@2c
    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@2f
    throw v2

    #@30
    .line 143
    .end local v0           #cause:Ljava/lang/Throwable;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_30
    move-exception v1

    #@31
    .line 144
    .local v1, e:Ljava/lang/IllegalAccessException;
    const-string v2, "ExtendedRemoteDisplayHelper"

    #@33
    const-string v3, "ExtendedRemoteDisplay.dispose-IllegalAccessException"

    #@35
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 145
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@3b
    goto :goto_f
.end method

.method public static isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 154
    sget-object v0, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayClass:Ljava/lang/Class;

    #@2
    if-eqz v0, :cond_15

    #@4
    sget-object v0, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayDispose:Ljava/lang/reflect/Method;

    #@6
    if-eqz v0, :cond_15

    #@8
    sget-object v0, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayListen:Ljava/lang/reflect/Method;

    #@a
    if-eqz v0, :cond_15

    #@c
    .line 157
    const-string v0, "ExtendedRemoteDisplayHelper"

    #@e
    const-string v1, "ExtendedRemoteDisplay isAvailable() : Available."

    #@10
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 158
    const/4 v0, 0x1

    #@14
    .line 161
    :goto_14
    return v0

    #@15
    .line 160
    :cond_15
    const-string v0, "ExtendedRemoteDisplayHelper"

    #@17
    const-string v1, "ExtendedRemoteDisplay isAvailable() : Not Available."

    #@19
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 161
    const/4 v0, 0x0

    #@1d
    goto :goto_14
.end method

.method public static listen(Ljava/lang/String;Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;Landroid/content/Context;)Ljava/lang/Object;
    .registers 11
    .parameter "iface"
    .parameter "listener"
    .parameter "handler"
    .parameter "context"

    #@0
    .prologue
    .line 101
    const/4 v2, 0x0

    #@1
    .line 102
    .local v2, extRemoteDisplay:Ljava/lang/Object;
    const-string v3, "ExtendedRemoteDisplayHelper"

    #@3
    const-string v4, "ExtendedRemoteDisplay.listen"

    #@5
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 104
    sget-object v3, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayListen:Ljava/lang/reflect/Method;

    #@a
    if-eqz v3, :cond_26

    #@c
    sget-object v3, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayDispose:Ljava/lang/reflect/Method;

    #@e
    if-eqz v3, :cond_26

    #@10
    .line 106
    :try_start_10
    sget-object v3, Lcom/android/server/display/ExtendedRemoteDisplayHelper;->sExtRemoteDisplayListen:Ljava/lang/reflect/Method;

    #@12
    const/4 v4, 0x0

    #@13
    const/4 v5, 0x4

    #@14
    new-array v5, v5, [Ljava/lang/Object;

    #@16
    const/4 v6, 0x0

    #@17
    aput-object p0, v5, v6

    #@19
    const/4 v6, 0x1

    #@1a
    aput-object p1, v5, v6

    #@1c
    const/4 v6, 0x2

    #@1d
    aput-object p2, v5, v6

    #@1f
    const/4 v6, 0x3

    #@20
    aput-object p3, v5, v6

    #@22
    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_25
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_10 .. :try_end_25} :catch_27
    .catch Ljava/lang/IllegalAccessException; {:try_start_10 .. :try_end_25} :catch_47

    #@25
    move-result-object v2

    #@26
    .line 123
    .end local v2           #extRemoteDisplay:Ljava/lang/Object;
    :cond_26
    :goto_26
    return-object v2

    #@27
    .line 108
    .restart local v2       #extRemoteDisplay:Ljava/lang/Object;
    :catch_27
    move-exception v1

    #@28
    .line 109
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "ExtendedRemoteDisplayHelper"

    #@2a
    const-string v4, "ExtendedRemoteDisplay.listen - InvocationTargetException"

    #@2c
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 110
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@32
    move-result-object v0

    #@33
    .line 111
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v3, v0, Ljava/lang/RuntimeException;

    #@35
    if-eqz v3, :cond_3a

    #@37
    .line 112
    check-cast v0, Ljava/lang/RuntimeException;

    #@39
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@3a
    .line 113
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_3a
    instance-of v3, v0, Ljava/lang/Error;

    #@3c
    if-eqz v3, :cond_41

    #@3e
    .line 114
    check-cast v0, Ljava/lang/Error;

    #@40
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@41
    .line 116
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_41
    new-instance v3, Ljava/lang/RuntimeException;

    #@43
    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@46
    throw v3

    #@47
    .line 118
    .end local v0           #cause:Ljava/lang/Throwable;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_47
    move-exception v1

    #@48
    .line 119
    .local v1, e:Ljava/lang/IllegalAccessException;
    const-string v3, "ExtendedRemoteDisplayHelper"

    #@4a
    const-string v4, "ExtendedRemoteDisplay.listen -IllegalAccessException"

    #@4c
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 120
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@52
    goto :goto_26
.end method
