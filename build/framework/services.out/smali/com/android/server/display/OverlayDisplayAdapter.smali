.class final Lcom/android/server/display/OverlayDisplayAdapter;
.super Lcom/android/server/display/DisplayAdapter;
.source "OverlayDisplayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;,
        Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayDevice;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field private static final MAX_HEIGHT:I = 0x1000

.field private static final MAX_WIDTH:I = 0x1000

.field private static final MIN_HEIGHT:I = 0x64

.field private static final MIN_WIDTH:I = 0x64

.field private static final SETTING_PATTERN:Ljava/util/regex/Pattern; = null

.field static final TAG:Ljava/lang/String; = "OverlayDisplayAdapter"


# instance fields
.field private mCurrentOverlaySetting:Ljava/lang/String;

.field private final mOverlays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 60
    const-string v0, "(\\d+)x(\\d+)/(\\d+)"

    #@2
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/display/OverlayDisplayAdapter;->SETTING_PATTERN:Ljava/util/regex/Pattern;

    #@8
    return-void
.end method

.method public constructor <init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;Landroid/os/Handler;)V
    .registers 12
    .parameter "syncRoot"
    .parameter "context"
    .parameter "handler"
    .parameter "listener"
    .parameter "uiHandler"

    #@0
    .prologue
    .line 71
    const-string v5, "OverlayDisplayAdapter"

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/DisplayAdapter;-><init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;Ljava/lang/String;)V

    #@a
    .line 64
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayAdapter;->mOverlays:Ljava/util/ArrayList;

    #@11
    .line 66
    const-string v0, ""

    #@13
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayAdapter;->mCurrentOverlaySetting:Ljava/lang/String;

    #@15
    .line 72
    iput-object p5, p0, Lcom/android/server/display/OverlayDisplayAdapter;->mUiHandler:Landroid/os/Handler;

    #@17
    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/display/OverlayDisplayAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayAdapter;->updateOverlayDisplayDevices()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/display/OverlayDisplayAdapter;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayAdapter;->mUiHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private static chooseOverlayGravity(I)I
    .registers 2
    .parameter "overlayNumber"

    #@0
    .prologue
    .line 173
    packed-switch p0, :pswitch_data_10

    #@3
    .line 182
    const/16 v0, 0x53

    #@5
    :goto_5
    return v0

    #@6
    .line 175
    :pswitch_6
    const/16 v0, 0x33

    #@8
    goto :goto_5

    #@9
    .line 177
    :pswitch_9
    const/16 v0, 0x55

    #@b
    goto :goto_5

    #@c
    .line 179
    :pswitch_c
    const/16 v0, 0x35

    #@e
    goto :goto_5

    #@f
    .line 173
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
    .end packed-switch
.end method

.method private updateOverlayDisplayDevices()V
    .registers 3

    #@0
    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/android/server/display/OverlayDisplayAdapter;->getSyncRoot()Lcom/android/server/display/DisplayManagerService$SyncRoot;

    #@3
    move-result-object v1

    #@4
    monitor-enter v1

    #@5
    .line 109
    :try_start_5
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayAdapter;->updateOverlayDisplayDevicesLocked()V

    #@8
    .line 110
    monitor-exit v1

    #@9
    .line 111
    return-void

    #@a
    .line 110
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method private updateOverlayDisplayDevicesLocked()V
    .registers 22

    #@0
    .prologue
    .line 114
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/OverlayDisplayAdapter;->getContext()Landroid/content/Context;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "overlay_display_devices"

    #@a
    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v17

    #@e
    .line 116
    .local v17, value:Ljava/lang/String;
    if-nez v17, :cond_12

    #@10
    .line 117
    const-string v17, ""

    #@12
    .line 120
    :cond_12
    move-object/from16 v0, p0

    #@14
    iget-object v2, v0, Lcom/android/server/display/OverlayDisplayAdapter;->mCurrentOverlaySetting:Ljava/lang/String;

    #@16
    move-object/from16 v0, v17

    #@18
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_1f

    #@1e
    .line 170
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 123
    :cond_1f
    move-object/from16 v0, v17

    #@21
    move-object/from16 v1, p0

    #@23
    iput-object v0, v1, Lcom/android/server/display/OverlayDisplayAdapter;->mCurrentOverlaySetting:Ljava/lang/String;

    #@25
    .line 125
    move-object/from16 v0, p0

    #@27
    iget-object v2, v0, Lcom/android/server/display/OverlayDisplayAdapter;->mOverlays:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_55

    #@2f
    .line 126
    const-string v2, "OverlayDisplayAdapter"

    #@31
    const-string v3, "Dismissing all overlay display devices."

    #@33
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 127
    move-object/from16 v0, p0

    #@38
    iget-object v2, v0, Lcom/android/server/display/OverlayDisplayAdapter;->mOverlays:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v11

    #@3e
    .local v11, i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_4e

    #@44
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v15

    #@48
    check-cast v15, Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;

    #@4a
    .line 128
    .local v15, overlay:Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;
    invoke-virtual {v15}, Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;->dismissLocked()V

    #@4d
    goto :goto_3e

    #@4e
    .line 130
    .end local v15           #overlay:Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;
    :cond_4e
    move-object/from16 v0, p0

    #@50
    iget-object v2, v0, Lcom/android/server/display/OverlayDisplayAdapter;->mOverlays:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@55
    .line 133
    .end local v11           #i$:Ljava/util/Iterator;
    :cond_55
    const/4 v10, 0x0

    #@56
    .line 134
    .local v10, count:I
    const-string v2, ";"

    #@58
    move-object/from16 v0, v17

    #@5a
    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5d
    move-result-object v9

    #@5e
    .local v9, arr$:[Ljava/lang/String;
    array-length v12, v9

    #@5f
    .local v12, len$:I
    const/4 v11, 0x0

    #@60
    .local v11, i$:I
    :goto_60
    if-ge v11, v12, :cond_1e

    #@62
    aget-object v16, v9, v11

    #@64
    .line 135
    .local v16, part:Ljava/lang/String;
    sget-object v2, Lcom/android/server/display/OverlayDisplayAdapter;->SETTING_PATTERN:Ljava/util/regex/Pattern;

    #@66
    move-object/from16 v0, v16

    #@68
    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@6b
    move-result-object v13

    #@6c
    .line 136
    .local v13, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->matches()Z

    #@6f
    move-result v2

    #@70
    if-eqz v2, :cond_153

    #@72
    .line 137
    const/4 v2, 0x4

    #@73
    if-lt v10, v2, :cond_92

    #@75
    .line 138
    const-string v2, "OverlayDisplayAdapter"

    #@77
    new-instance v3, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v18, "Too many overlay display devices specified: "

    #@7e
    move-object/from16 v0, v18

    #@80
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v3

    #@84
    move-object/from16 v0, v17

    #@86
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v3

    #@8e
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    goto :goto_1e

    #@92
    .line 142
    :cond_92
    const/4 v2, 0x1

    #@93
    :try_start_93
    invoke-virtual {v13, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@96
    move-result-object v2

    #@97
    const/16 v3, 0xa

    #@99
    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@9c
    move-result v5

    #@9d
    .line 143
    .local v5, width:I
    const/4 v2, 0x2

    #@9e
    invoke-virtual {v13, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@a1
    move-result-object v2

    #@a2
    const/16 v3, 0xa

    #@a4
    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@a7
    move-result v6

    #@a8
    .line 144
    .local v6, height:I
    const/4 v2, 0x3

    #@a9
    invoke-virtual {v13, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@ac
    move-result-object v2

    #@ad
    const/16 v3, 0xa

    #@af
    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@b2
    move-result v7

    #@b3
    .line 145
    .local v7, densityDpi:I
    const/16 v2, 0x64

    #@b5
    if-lt v5, v2, :cond_159

    #@b7
    const/16 v2, 0x1000

    #@b9
    if-gt v5, v2, :cond_159

    #@bb
    const/16 v2, 0x64

    #@bd
    if-lt v6, v2, :cond_159

    #@bf
    const/16 v2, 0x1000

    #@c1
    if-gt v6, v2, :cond_159

    #@c3
    const/16 v2, 0x78

    #@c5
    if-lt v7, v2, :cond_159

    #@c7
    const/16 v2, 0x1e0

    #@c9
    if-gt v7, v2, :cond_159

    #@cb
    .line 149
    add-int/lit8 v10, v10, 0x1

    #@cd
    move v14, v10

    #@ce
    .line 150
    .local v14, number:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/OverlayDisplayAdapter;->getContext()Landroid/content/Context;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d5
    move-result-object v2

    #@d6
    const v3, 0x1040545

    #@d9
    const/16 v18, 0x1

    #@db
    move/from16 v0, v18

    #@dd
    new-array v0, v0, [Ljava/lang/Object;

    #@df
    move-object/from16 v18, v0

    #@e1
    const/16 v19, 0x0

    #@e3
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e6
    move-result-object v20

    #@e7
    aput-object v20, v18, v19

    #@e9
    move-object/from16 v0, v18

    #@eb
    invoke-virtual {v2, v3, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@ee
    move-result-object v4

    #@ef
    .line 153
    .local v4, name:Ljava/lang/String;
    invoke-static {v14}, Lcom/android/server/display/OverlayDisplayAdapter;->chooseOverlayGravity(I)I

    #@f2
    move-result v8

    #@f3
    .line 155
    .local v8, gravity:I
    const-string v2, "OverlayDisplayAdapter"

    #@f5
    new-instance v3, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v18, "Showing overlay display device #"

    #@fc
    move-object/from16 v0, v18

    #@fe
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v3

    #@102
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v3

    #@106
    const-string v18, ": name="

    #@108
    move-object/from16 v0, v18

    #@10a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v3

    #@10e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v3

    #@112
    const-string v18, ", width="

    #@114
    move-object/from16 v0, v18

    #@116
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v3

    #@11a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v3

    #@11e
    const-string v18, ", height="

    #@120
    move-object/from16 v0, v18

    #@122
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v3

    #@126
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@129
    move-result-object v3

    #@12a
    const-string v18, ", densityDpi="

    #@12c
    move-object/from16 v0, v18

    #@12e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v3

    #@132
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v3

    #@136
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v3

    #@13a
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    .line 159
    move-object/from16 v0, p0

    #@13f
    iget-object v0, v0, Lcom/android/server/display/OverlayDisplayAdapter;->mOverlays:Ljava/util/ArrayList;

    #@141
    move-object/from16 v18, v0

    #@143
    new-instance v2, Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;

    #@145
    move-object/from16 v3, p0

    #@147
    invoke-direct/range {v2 .. v8}, Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;-><init>(Lcom/android/server/display/OverlayDisplayAdapter;Ljava/lang/String;IIII)V

    #@14a
    move-object/from16 v0, v18

    #@14c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_14f
    .catch Ljava/lang/NumberFormatException; {:try_start_93 .. :try_end_14f} :catch_176

    #@14f
    .line 134
    .end local v4           #name:Ljava/lang/String;
    .end local v5           #width:I
    .end local v6           #height:I
    .end local v7           #densityDpi:I
    .end local v8           #gravity:I
    .end local v14           #number:I
    :cond_14f
    :goto_14f
    add-int/lit8 v11, v11, 0x1

    #@151
    goto/16 :goto_60

    #@153
    .line 165
    :cond_153
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->isEmpty()Z

    #@156
    move-result v2

    #@157
    if-nez v2, :cond_14f

    #@159
    .line 168
    :cond_159
    :goto_159
    const-string v2, "OverlayDisplayAdapter"

    #@15b
    new-instance v3, Ljava/lang/StringBuilder;

    #@15d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@160
    const-string v18, "Malformed overlay display devices setting: "

    #@162
    move-object/from16 v0, v18

    #@164
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v3

    #@168
    move-object/from16 v0, v17

    #@16a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v3

    #@16e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@171
    move-result-object v3

    #@172
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@175
    goto :goto_14f

    #@176
    .line 163
    :catch_176
    move-exception v2

    #@177
    goto :goto_159
.end method


# virtual methods
.method public dumpLocked(Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "pw"

    #@0
    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/android/server/display/DisplayAdapter;->dumpLocked(Ljava/io/PrintWriter;)V

    #@3
    .line 79
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "mCurrentOverlaySetting="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayAdapter;->mCurrentOverlaySetting:Ljava/lang/String;

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b
    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "mOverlays: size="

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayAdapter;->mOverlays:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@2b
    move-result v3

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@37
    .line 81
    iget-object v2, p0, Lcom/android/server/display/OverlayDisplayAdapter;->mOverlays:Ljava/util/ArrayList;

    #@39
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3c
    move-result-object v0

    #@3d
    .local v0, i$:Ljava/util/Iterator;
    :goto_3d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@40
    move-result v2

    #@41
    if-eqz v2, :cond_4d

    #@43
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@46
    move-result-object v1

    #@47
    check-cast v1, Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;

    #@49
    .line 82
    .local v1, overlay:Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;
    invoke-virtual {v1, p1}, Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;->dumpLocked(Ljava/io/PrintWriter;)V

    #@4c
    goto :goto_3d

    #@4d
    .line 84
    .end local v1           #overlay:Lcom/android/server/display/OverlayDisplayAdapter$OverlayDisplayHandle;
    :cond_4d
    return-void
.end method

.method public registerLocked()V
    .registers 3

    #@0
    .prologue
    .line 88
    invoke-super {p0}, Lcom/android/server/display/DisplayAdapter;->registerLocked()V

    #@3
    .line 90
    invoke-virtual {p0}, Lcom/android/server/display/OverlayDisplayAdapter;->getHandler()Landroid/os/Handler;

    #@6
    move-result-object v0

    #@7
    new-instance v1, Lcom/android/server/display/OverlayDisplayAdapter$1;

    #@9
    invoke-direct {v1, p0}, Lcom/android/server/display/OverlayDisplayAdapter$1;-><init>(Lcom/android/server/display/OverlayDisplayAdapter;)V

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@f
    .line 105
    return-void
.end method
