.class final Lcom/android/server/display/PersistentDataStore;
.super Ljava/lang/Object;
.source "PersistentDataStore.java"


# static fields
.field static final TAG:Ljava/lang/String; = "DisplayManager"


# instance fields
.field private final mAtomicFile:Landroid/util/AtomicFile;

.field private mDirty:Z

.field private mLoaded:Z

.field private mRememberedWifiDisplays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/display/WifiDisplay;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@a
    .line 74
    new-instance v0, Landroid/util/AtomicFile;

    #@c
    new-instance v1, Ljava/io/File;

    #@e
    const-string v2, "/data/system/display-manager-state.xml"

    #@10
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@13
    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@16
    iput-object v0, p0, Lcom/android/server/display/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@18
    .line 75
    return-void
.end method

.method private clearState()V
    .registers 2

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 182
    return-void
.end method

.method private findRememberedWifiDisplay(Ljava/lang/String;)I
    .registers 5
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 160
    iget-object v2, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 161
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_1f

    #@9
    .line 162
    iget-object v2, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/hardware/display/WifiDisplay;

    #@11
    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_1c

    #@1b
    .line 166
    .end local v1           #i:I
    :goto_1b
    return v1

    #@1c
    .line 161
    .restart local v1       #i:I
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_7

    #@1f
    .line 166
    :cond_1f
    const/4 v1, -0x1

    #@20
    goto :goto_1b
.end method

.method private load()V
    .registers 6

    #@0
    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->clearState()V

    #@3
    .line 189
    :try_start_3
    iget-object v3, p0, Lcom/android/server/display/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@5
    invoke-virtual {v3}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_8} :catch_1d

    #@8
    move-result-object v1

    #@9
    .line 196
    .local v1, is:Ljava/io/InputStream;
    :try_start_9
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@c
    move-result-object v2

    #@d
    .line 197
    .local v2, parser:Lorg/xmlpull/v1/XmlPullParser;
    new-instance v3, Ljava/io/BufferedInputStream;

    #@f
    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@12
    const/4 v4, 0x0

    #@13
    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@16
    .line 198
    invoke-direct {p0, v2}, Lcom/android/server/display/PersistentDataStore;->loadFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_19
    .catchall {:try_start_9 .. :try_end_19} :catchall_3d
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_19} :catch_1f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_19} :catch_2e

    #@19
    .line 206
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@1c
    .line 208
    .end local v1           #is:Ljava/io/InputStream;
    .end local v2           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :goto_1c
    return-void

    #@1d
    .line 190
    :catch_1d
    move-exception v0

    #@1e
    .line 191
    .local v0, ex:Ljava/io/FileNotFoundException;
    goto :goto_1c

    #@1f
    .line 199
    .end local v0           #ex:Ljava/io/FileNotFoundException;
    .restart local v1       #is:Ljava/io/InputStream;
    :catch_1f
    move-exception v0

    #@20
    .line 200
    .local v0, ex:Ljava/io/IOException;
    :try_start_20
    const-string v3, "DisplayManager"

    #@22
    const-string v4, "Failed to load display manager persistent store data."

    #@24
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    .line 201
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->clearState()V
    :try_end_2a
    .catchall {:try_start_20 .. :try_end_2a} :catchall_3d

    #@2a
    .line 206
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@2d
    goto :goto_1c

    #@2e
    .line 202
    .end local v0           #ex:Ljava/io/IOException;
    :catch_2e
    move-exception v0

    #@2f
    .line 203
    .local v0, ex:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_2f
    const-string v3, "DisplayManager"

    #@31
    const-string v4, "Failed to load display manager persistent store data."

    #@33
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    .line 204
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->clearState()V
    :try_end_39
    .catchall {:try_start_2f .. :try_end_39} :catchall_3d

    #@39
    .line 206
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@3c
    goto :goto_1c

    #@3d
    .end local v0           #ex:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_3d
    move-exception v3

    #@3e
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@41
    throw v3
.end method

.method private loadFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 5
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 235
    const-string v1, "display-manager-state"

    #@2
    invoke-static {p1, v1}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@5
    .line 236
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@8
    move-result v0

    #@9
    .line 237
    .local v0, outerDepth:I
    :cond_9
    :goto_9
    invoke-static {p1, v0}, Lcom/android/internal/util/XmlUtils;->nextElementWithin(Lorg/xmlpull/v1/XmlPullParser;I)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_1f

    #@f
    .line 238
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    const-string v2, "remembered-wifi-displays"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_9

    #@1b
    .line 239
    invoke-direct {p0, p1}, Lcom/android/server/display/PersistentDataStore;->loadRememberedWifiDisplaysFromXml(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1e
    goto :goto_9

    #@1f
    .line 242
    :cond_1f
    return-void
.end method

.method private loadIfNeeded()V
    .registers 2

    #@0
    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/android/server/display/PersistentDataStore;->mLoaded:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 171
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->load()V

    #@7
    .line 172
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/android/server/display/PersistentDataStore;->mLoaded:Z

    #@a
    .line 174
    :cond_a
    return-void
.end method

.method private loadRememberedWifiDisplaysFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 9
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 246
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@4
    move-result v3

    #@5
    .line 247
    .local v3, outerDepth:I
    :cond_5
    :goto_5
    invoke-static {p1, v3}, Lcom/android/internal/util/XmlUtils;->nextElementWithin(Lorg/xmlpull/v1/XmlPullParser;I)Z

    #@8
    move-result v4

    #@9
    if-eqz v4, :cond_4e

    #@b
    .line 248
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    const-string v5, "wifi-display"

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_5

    #@17
    .line 249
    const-string v4, "deviceAddress"

    #@19
    invoke-interface {p1, v6, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 250
    .local v0, deviceAddress:Ljava/lang/String;
    const-string v4, "deviceName"

    #@1f
    invoke-interface {p1, v6, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 251
    .local v2, deviceName:Ljava/lang/String;
    const-string v4, "deviceAlias"

    #@25
    invoke-interface {p1, v6, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    .line 252
    .local v1, deviceAlias:Ljava/lang/String;
    if-eqz v0, :cond_2d

    #@2b
    if-nez v2, :cond_35

    #@2d
    .line 253
    :cond_2d
    new-instance v4, Lorg/xmlpull/v1/XmlPullParserException;

    #@2f
    const-string v5, "Missing deviceAddress or deviceName attribute on wifi-display."

    #@31
    invoke-direct {v4, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@34
    throw v4

    #@35
    .line 256
    :cond_35
    invoke-direct {p0, v0}, Lcom/android/server/display/PersistentDataStore;->findRememberedWifiDisplay(Ljava/lang/String;)I

    #@38
    move-result v4

    #@39
    if-ltz v4, :cond_43

    #@3b
    .line 257
    new-instance v4, Lorg/xmlpull/v1/XmlPullParserException;

    #@3d
    const-string v5, "Found duplicate wifi display device address."

    #@3f
    invoke-direct {v4, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@42
    throw v4

    #@43
    .line 261
    :cond_43
    iget-object v4, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@45
    new-instance v5, Landroid/hardware/display/WifiDisplay;

    #@47
    invoke-direct {v5, v0, v2, v1}, Landroid/hardware/display/WifiDisplay;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4d
    goto :goto_5

    #@4e
    .line 265
    .end local v0           #deviceAddress:Ljava/lang/String;
    .end local v1           #deviceAlias:Ljava/lang/String;
    .end local v2           #deviceName:Ljava/lang/String;
    :cond_4e
    return-void
.end method

.method private save()V
    .registers 7

    #@0
    .prologue
    .line 213
    :try_start_0
    iget-object v4, p0, Lcom/android/server/display/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@2
    invoke-virtual {v4}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_2b

    #@5
    move-result-object v1

    #@6
    .line 214
    .local v1, os:Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    #@7
    .line 216
    .local v3, success:Z
    :try_start_7
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    #@9
    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@c
    .line 217
    .local v2, serializer:Lorg/xmlpull/v1/XmlSerializer;
    new-instance v4, Ljava/io/BufferedOutputStream;

    #@e
    invoke-direct {v4, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@11
    const-string v5, "utf-8"

    #@13
    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@16
    .line 218
    invoke-direct {p0, v2}, Lcom/android/server/display/PersistentDataStore;->saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    #@19
    .line 219
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->flush()V
    :try_end_1c
    .catchall {:try_start_7 .. :try_end_1c} :catchall_34

    #@1c
    .line 220
    const/4 v3, 0x1

    #@1d
    .line 222
    if-eqz v3, :cond_25

    #@1f
    .line 223
    :try_start_1f
    iget-object v4, p0, Lcom/android/server/display/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@21
    invoke-virtual {v4, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    #@24
    .line 231
    .end local v1           #os:Ljava/io/FileOutputStream;
    .end local v2           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .end local v3           #success:Z
    :goto_24
    return-void

    #@25
    .line 225
    .restart local v1       #os:Ljava/io/FileOutputStream;
    .restart local v2       #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v3       #success:Z
    :cond_25
    iget-object v4, p0, Lcom/android/server/display/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@27
    invoke-virtual {v4, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2a} :catch_2b

    #@2a
    goto :goto_24

    #@2b
    .line 228
    .end local v1           #os:Ljava/io/FileOutputStream;
    .end local v2           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .end local v3           #success:Z
    :catch_2b
    move-exception v0

    #@2c
    .line 229
    .local v0, ex:Ljava/io/IOException;
    const-string v4, "DisplayManager"

    #@2e
    const-string v5, "Failed to save display manager persistent store data."

    #@30
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_24

    #@34
    .line 222
    .end local v0           #ex:Ljava/io/IOException;
    .restart local v1       #os:Ljava/io/FileOutputStream;
    .restart local v3       #success:Z
    :catchall_34
    move-exception v4

    #@35
    if-eqz v3, :cond_3d

    #@37
    .line 223
    :try_start_37
    iget-object v5, p0, Lcom/android/server/display/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@39
    invoke-virtual {v5, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    #@3c
    .line 225
    :goto_3c
    throw v4

    #@3d
    :cond_3d
    iget-object v5, p0, Lcom/android/server/display/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@3f
    invoke-virtual {v5, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_42} :catch_2b

    #@42
    goto :goto_3c
.end method

.method private saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 7
    .parameter "serializer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 268
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@5
    move-result-object v2

    #@6
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@9
    .line 269
    const-string v2, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@b
    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@e
    .line 270
    const-string v2, "display-manager-state"

    #@10
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@13
    .line 271
    const-string v2, "remembered-wifi-displays"

    #@15
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@18
    .line 272
    iget-object v2, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1d
    move-result-object v1

    #@1e
    .local v1, i$:Ljava/util/Iterator;
    :goto_1e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_56

    #@24
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Landroid/hardware/display/WifiDisplay;

    #@2a
    .line 273
    .local v0, display:Landroid/hardware/display/WifiDisplay;
    const-string v2, "wifi-display"

    #@2c
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2f
    .line 274
    const-string v2, "deviceAddress"

    #@31
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@38
    .line 275
    const-string v2, "deviceName"

    #@3a
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@41
    .line 276
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAlias()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    if-eqz v2, :cond_50

    #@47
    .line 277
    const-string v2, "deviceAlias"

    #@49
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAlias()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@50
    .line 279
    :cond_50
    const-string v2, "wifi-display"

    #@52
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@55
    goto :goto_1e

    #@56
    .line 281
    .end local v0           #display:Landroid/hardware/display/WifiDisplay;
    :cond_56
    const-string v2, "remembered-wifi-displays"

    #@58
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5b
    .line 282
    const-string v2, "display-manager-state"

    #@5d
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@60
    .line 283
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@63
    .line 284
    return-void
.end method

.method private setDirty()V
    .registers 2

    #@0
    .prologue
    .line 177
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/display/PersistentDataStore;->mDirty:Z

    #@3
    .line 178
    return-void
.end method


# virtual methods
.method public applyWifiDisplayAlias(Landroid/hardware/display/WifiDisplay;)Landroid/hardware/display/WifiDisplay;
    .registers 7
    .parameter "display"

    #@0
    .prologue
    .line 99
    if-eqz p1, :cond_34

    #@2
    .line 100
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->loadIfNeeded()V

    #@5
    .line 102
    const/4 v0, 0x0

    #@6
    .line 103
    .local v0, alias:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-direct {p0, v2}, Lcom/android/server/display/PersistentDataStore;->findRememberedWifiDisplay(Ljava/lang/String;)I

    #@d
    move-result v1

    #@e
    .line 104
    .local v1, index:I
    if-ltz v1, :cond_1c

    #@10
    .line 105
    iget-object v2, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/hardware/display/WifiDisplay;

    #@18
    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplay;->getDeviceAlias()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 107
    :cond_1c
    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceAlias()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v2, v0}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@23
    move-result v2

    #@24
    if-nez v2, :cond_34

    #@26
    .line 108
    new-instance v2, Landroid/hardware/display/WifiDisplay;

    #@28
    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-direct {v2, v3, v4, v0}, Landroid/hardware/display/WifiDisplay;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@33
    move-object p1, v2

    #@34
    .line 111
    .end local v0           #alias:Ljava/lang/String;
    .end local v1           #index:I
    .end local p1
    :cond_34
    return-object p1
.end method

.method public applyWifiDisplayAliases([Landroid/hardware/display/WifiDisplay;)[Landroid/hardware/display/WifiDisplay;
    .registers 8
    .parameter "displays"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 115
    move-object v3, p1

    #@2
    .line 116
    .local v3, results:[Landroid/hardware/display/WifiDisplay;
    if-eqz v3, :cond_1e

    #@4
    .line 117
    array-length v0, p1

    #@5
    .line 118
    .local v0, count:I
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    if-ge v1, v0, :cond_1e

    #@8
    .line 119
    aget-object v4, p1, v1

    #@a
    invoke-virtual {p0, v4}, Lcom/android/server/display/PersistentDataStore;->applyWifiDisplayAlias(Landroid/hardware/display/WifiDisplay;)Landroid/hardware/display/WifiDisplay;

    #@d
    move-result-object v2

    #@e
    .line 120
    .local v2, result:Landroid/hardware/display/WifiDisplay;
    aget-object v4, p1, v1

    #@10
    if-eq v2, v4, :cond_1b

    #@12
    .line 121
    if-ne v3, p1, :cond_19

    #@14
    .line 122
    new-array v3, v0, [Landroid/hardware/display/WifiDisplay;

    #@16
    .line 123
    invoke-static {p1, v5, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 125
    :cond_19
    aput-object v2, v3, v1

    #@1b
    .line 118
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_6

    #@1e
    .line 129
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #result:Landroid/hardware/display/WifiDisplay;
    :cond_1e
    return-object v3
.end method

.method public forgetWifiDisplay(Ljava/lang/String;)Z
    .registers 4
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/android/server/display/PersistentDataStore;->findRememberedWifiDisplay(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    .line 151
    .local v0, index:I
    if-ltz v0, :cond_10

    #@6
    .line 152
    iget-object v1, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@b
    .line 153
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->setDirty()V

    #@e
    .line 154
    const/4 v1, 0x1

    #@f
    .line 156
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method public getRememberedWifiDisplay(Ljava/lang/String;)Landroid/hardware/display/WifiDisplay;
    .registers 4
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->loadIfNeeded()V

    #@3
    .line 86
    invoke-direct {p0, p1}, Lcom/android/server/display/PersistentDataStore;->findRememberedWifiDisplay(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    .line 87
    .local v0, index:I
    if-ltz v0, :cond_12

    #@9
    .line 88
    iget-object v1, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/hardware/display/WifiDisplay;

    #@11
    .line 90
    :goto_11
    return-object v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method

.method public getRememberedWifiDisplays()[Landroid/hardware/display/WifiDisplay;
    .registers 3

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->loadIfNeeded()V

    #@3
    .line 95
    iget-object v0, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@5
    iget-object v1, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v1

    #@b
    new-array v1, v1, [Landroid/hardware/display/WifiDisplay;

    #@d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, [Landroid/hardware/display/WifiDisplay;

    #@13
    return-object v0
.end method

.method public rememberWifiDisplay(Landroid/hardware/display/WifiDisplay;)Z
    .registers 5
    .parameter "display"

    #@0
    .prologue
    .line 133
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->loadIfNeeded()V

    #@3
    .line 135
    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    invoke-direct {p0, v2}, Lcom/android/server/display/PersistentDataStore;->findRememberedWifiDisplay(Ljava/lang/String;)I

    #@a
    move-result v0

    #@b
    .line 136
    .local v0, index:I
    if-ltz v0, :cond_27

    #@d
    .line 137
    iget-object v2, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/hardware/display/WifiDisplay;

    #@15
    .line 138
    .local v1, other:Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v1, p1}, Landroid/hardware/display/WifiDisplay;->equals(Landroid/hardware/display/WifiDisplay;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_1d

    #@1b
    .line 139
    const/4 v2, 0x0

    #@1c
    .line 146
    .end local v1           #other:Landroid/hardware/display/WifiDisplay;
    :goto_1c
    return v2

    #@1d
    .line 141
    .restart local v1       #other:Landroid/hardware/display/WifiDisplay;
    :cond_1d
    iget-object v2, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v2, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 145
    .end local v1           #other:Landroid/hardware/display/WifiDisplay;
    :goto_22
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->setDirty()V

    #@25
    .line 146
    const/4 v2, 0x1

    #@26
    goto :goto_1c

    #@27
    .line 143
    :cond_27
    iget-object v2, p0, Lcom/android/server/display/PersistentDataStore;->mRememberedWifiDisplays:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    goto :goto_22
.end method

.method public saveIfNeeded()V
    .registers 2

    #@0
    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/android/server/display/PersistentDataStore;->mDirty:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 79
    invoke-direct {p0}, Lcom/android/server/display/PersistentDataStore;->save()V

    #@7
    .line 80
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/server/display/PersistentDataStore;->mDirty:Z

    #@a
    .line 82
    :cond_a
    return-void
.end method
