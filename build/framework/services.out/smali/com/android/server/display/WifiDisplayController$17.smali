.class Lcom/android/server/display/WifiDisplayController$17;
.super Landroid/content/BroadcastReceiver;
.source "WifiDisplayController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/WifiDisplayController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/WifiDisplayController;


# direct methods
.method constructor <init>(Lcom/android/server/display/WifiDisplayController;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 917
    iput-object p1, p0, Lcom/android/server/display/WifiDisplayController$17;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 920
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 921
    .local v0, action:Ljava/lang/String;
    const-string v3, "android.net.wifi.p2p.STATE_CHANGED"

    #@7
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_1e

    #@d
    .line 924
    const-string v3, "wifi_p2p_state"

    #@f
    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@12
    move-result v3

    #@13
    const/4 v4, 0x2

    #@14
    if-ne v3, v4, :cond_1c

    #@16
    .line 932
    .local v1, enabled:Z
    :goto_16
    iget-object v3, p0, Lcom/android/server/display/WifiDisplayController$17;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@18
    invoke-static {v3, v1}, Lcom/android/server/display/WifiDisplayController;->access$3100(Lcom/android/server/display/WifiDisplayController;Z)V

    #@1b
    .line 949
    .end local v1           #enabled:Z
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 924
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_16

    #@1e
    .line 933
    :cond_1e
    const-string v3, "android.net.wifi.p2p.PEERS_CHANGED"

    #@20
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_2c

    #@26
    .line 938
    iget-object v3, p0, Lcom/android/server/display/WifiDisplayController$17;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@28
    invoke-static {v3}, Lcom/android/server/display/WifiDisplayController;->access$3200(Lcom/android/server/display/WifiDisplayController;)V

    #@2b
    goto :goto_1b

    #@2c
    .line 939
    :cond_2c
    const-string v3, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    #@2e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_1b

    #@34
    .line 940
    const-string v3, "networkInfo"

    #@36
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@39
    move-result-object v2

    #@3a
    check-cast v2, Landroid/net/NetworkInfo;

    #@3c
    .line 947
    .local v2, networkInfo:Landroid/net/NetworkInfo;
    iget-object v3, p0, Lcom/android/server/display/WifiDisplayController$17;->this$0:Lcom/android/server/display/WifiDisplayController;

    #@3e
    invoke-static {v3, v2}, Lcom/android/server/display/WifiDisplayController;->access$3300(Lcom/android/server/display/WifiDisplayController;Landroid/net/NetworkInfo;)V

    #@41
    goto :goto_1b
.end method
