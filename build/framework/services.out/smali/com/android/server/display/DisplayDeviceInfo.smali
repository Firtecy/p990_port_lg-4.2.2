.class final Lcom/android/server/display/DisplayDeviceInfo;
.super Ljava/lang/Object;
.source "DisplayDeviceInfo.java"


# static fields
.field public static final FLAG_DEFAULT_DISPLAY:I = 0x1

.field public static final FLAG_ROTATES_WITH_CONTENT:I = 0x2

.field public static final FLAG_SECURE:I = 0x4

.field public static final FLAG_SUPPORTS_PROTECTED_BUFFERS:I = 0x8

.field public static final TOUCH_EXTERNAL:I = 0x2

.field public static final TOUCH_INTERNAL:I = 0x1

.field public static final TOUCH_NONE:I


# instance fields
.field public address:Ljava/lang/String;

.field public densityDpi:I

.field public flags:I

.field public height:I

.field public name:Ljava/lang/String;

.field public refreshRate:F

.field public rotation:I

.field public touch:I

.field public type:I

.field public width:I

.field public xDpi:F

.field public yDpi:F


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 140
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->rotation:I

    #@6
    return-void
.end method

.method private static flagsToString(I)Ljava/lang/String;
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 230
    .local v0, msg:Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x1

    #@7
    if-eqz v1, :cond_e

    #@9
    .line 231
    const-string v1, ", FLAG_DEFAULT_DISPLAY"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 233
    :cond_e
    and-int/lit8 v1, p0, 0x2

    #@10
    if-eqz v1, :cond_17

    #@12
    .line 234
    const-string v1, ", FLAG_ROTATES_WITH_CONTENT"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 236
    :cond_17
    and-int/lit8 v1, p0, 0x4

    #@19
    if-eqz v1, :cond_20

    #@1b
    .line 237
    const-string v1, ", FLAG_SECURE"

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 239
    :cond_20
    and-int/lit8 v1, p0, 0x8

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 240
    const-string v1, ", FLAG_SUPPORTS_PROTECTED_BUFFERS"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 242
    :cond_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    return-object v1
.end method

.method private static touchToString(I)Ljava/lang/String;
    .registers 2
    .parameter "touch"

    #@0
    .prologue
    .line 216
    packed-switch p0, :pswitch_data_12

    #@3
    .line 224
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 218
    :pswitch_8
    const-string v0, "NONE"

    #@a
    goto :goto_7

    #@b
    .line 220
    :pswitch_b
    const-string v0, "INTERNAL"

    #@d
    goto :goto_7

    #@e
    .line 222
    :pswitch_e
    const-string v0, "EXTERNAL"

    #@10
    goto :goto_7

    #@11
    .line 216
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_e
    .end packed-switch
.end method


# virtual methods
.method public copyFrom(Lcom/android/server/display/DisplayDeviceInfo;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 188
    iget-object v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@2
    iput-object v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@4
    .line 189
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@6
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@8
    .line 190
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@a
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@c
    .line 191
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->refreshRate:F

    #@e
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->refreshRate:F

    #@10
    .line 192
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@12
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@14
    .line 193
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@16
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@18
    .line 194
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@1a
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@1c
    .line 195
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@1e
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@20
    .line 196
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->touch:I

    #@22
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->touch:I

    #@24
    .line 197
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->rotation:I

    #@26
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->rotation:I

    #@28
    .line 198
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    #@2a
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    #@2c
    .line 199
    iget-object v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->address:Ljava/lang/String;

    #@2e
    iput-object v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->address:Ljava/lang/String;

    #@30
    .line 200
    return-void
.end method

.method public equals(Lcom/android/server/display/DisplayDeviceInfo;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 167
    if-eqz p1, :cond_5a

    #@2
    iget-object v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@4
    iget-object v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@6
    invoke-static {v0, v1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_5a

    #@c
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@e
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@10
    if-ne v0, v1, :cond_5a

    #@12
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@14
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@16
    if-ne v0, v1, :cond_5a

    #@18
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->refreshRate:F

    #@1a
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->refreshRate:F

    #@1c
    cmpl-float v0, v0, v1

    #@1e
    if-nez v0, :cond_5a

    #@20
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@22
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@24
    if-ne v0, v1, :cond_5a

    #@26
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@28
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@2a
    cmpl-float v0, v0, v1

    #@2c
    if-nez v0, :cond_5a

    #@2e
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@30
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@32
    cmpl-float v0, v0, v1

    #@34
    if-nez v0, :cond_5a

    #@36
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@38
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@3a
    if-ne v0, v1, :cond_5a

    #@3c
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->touch:I

    #@3e
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->touch:I

    #@40
    if-ne v0, v1, :cond_5a

    #@42
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->rotation:I

    #@44
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->rotation:I

    #@46
    if-ne v0, v1, :cond_5a

    #@48
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    #@4a
    iget v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    #@4c
    if-ne v0, v1, :cond_5a

    #@4e
    iget-object v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->address:Ljava/lang/String;

    #@50
    iget-object v1, p1, Lcom/android/server/display/DisplayDeviceInfo;->address:Ljava/lang/String;

    #@52
    invoke-static {v0, v1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_5a

    #@58
    const/4 v0, 0x1

    #@59
    :goto_59
    return v0

    #@5a
    :cond_5a
    const/4 v0, 0x0

    #@5b
    goto :goto_59
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 163
    instance-of v0, p1, Lcom/android/server/display/DisplayDeviceInfo;

    #@2
    if-eqz v0, :cond_e

    #@4
    check-cast p1, Lcom/android/server/display/DisplayDeviceInfo;

    #@6
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/server/display/DisplayDeviceInfo;->equals(Lcom/android/server/display/DisplayDeviceInfo;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 184
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setAssumedDensityForExternalDisplay(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 154
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    #@3
    move-result v0

    #@4
    mul-int/lit16 v0, v0, 0x140

    #@6
    div-int/lit16 v0, v0, 0x438

    #@8
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@a
    .line 157
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@c
    int-to-float v0, v0

    #@d
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@f
    .line 158
    iget v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@11
    int-to-float v0, v0

    #@12
    iput v0, p0, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@14
    .line 159
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DisplayDeviceInfo{\""

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "\": "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " x "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->refreshRate:F

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " fps, "

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, "density "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    const-string v1, ", "

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, " x "

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    const-string v1, " dpi"

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    const-string v1, ", touch "

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->touch:I

    #@6d
    invoke-static {v1}, Lcom/android/server/display/DisplayDeviceInfo;->touchToString(I)Ljava/lang/String;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v0

    #@75
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@77
    invoke-static {v1}, Lcom/android/server/display/DisplayDeviceInfo;->flagsToString(I)Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    const-string v1, ", rotation "

    #@81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->rotation:I

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    const-string v1, ", type "

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    iget v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    #@93
    invoke-static {v1}, Landroid/view/Display;->typeToString(I)Ljava/lang/String;

    #@96
    move-result-object v1

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v0

    #@9b
    const-string v1, ", address "

    #@9d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v0

    #@a1
    iget-object v1, p0, Lcom/android/server/display/DisplayDeviceInfo;->address:Ljava/lang/String;

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    const-string v1, "}"

    #@a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v0

    #@ad
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v0

    #@b1
    return-object v0
.end method
