.class final Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;
.super Lcom/android/server/display/DisplayDevice;
.source "HeadlessDisplayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/HeadlessDisplayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HeadlessDisplayDevice"
.end annotation


# instance fields
.field private mInfo:Lcom/android/server/display/DisplayDeviceInfo;

.field final synthetic this$0:Lcom/android/server/display/HeadlessDisplayAdapter;


# direct methods
.method public constructor <init>(Lcom/android/server/display/HeadlessDisplayAdapter;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->this$0:Lcom/android/server/display/HeadlessDisplayAdapter;

    #@2
    .line 49
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Lcom/android/server/display/DisplayDevice;-><init>(Lcom/android/server/display/DisplayAdapter;Landroid/os/IBinder;)V

    #@6
    .line 50
    return-void
.end method


# virtual methods
.method public getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;
    .registers 5

    #@0
    .prologue
    const/high16 v3, 0x4320

    #@2
    .line 54
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@4
    if-nez v0, :cond_52

    #@6
    .line 55
    new-instance v0, Lcom/android/server/display/DisplayDeviceInfo;

    #@8
    invoke-direct {v0}, Lcom/android/server/display/DisplayDeviceInfo;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@d
    .line 56
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@f
    iget-object v1, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->this$0:Lcom/android/server/display/HeadlessDisplayAdapter;

    #@11
    invoke-virtual {v1}, Lcom/android/server/display/HeadlessDisplayAdapter;->getContext()Landroid/content/Context;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@18
    move-result-object v1

    #@19
    const v2, 0x1040543

    #@1c
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    iput-object v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    #@22
    .line 58
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@24
    const/16 v1, 0x280

    #@26
    iput v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    #@28
    .line 59
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@2a
    const/16 v1, 0x1e0

    #@2c
    iput v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    #@2e
    .line 60
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@30
    const/high16 v1, 0x4270

    #@32
    iput v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->refreshRate:F

    #@34
    .line 61
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@36
    const/16 v1, 0xa0

    #@38
    iput v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    #@3a
    .line 62
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@3c
    iput v3, v0, Lcom/android/server/display/DisplayDeviceInfo;->xDpi:F

    #@3e
    .line 63
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@40
    iput v3, v0, Lcom/android/server/display/DisplayDeviceInfo;->yDpi:F

    #@42
    .line 64
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@44
    const/16 v1, 0xd

    #@46
    iput v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    #@48
    .line 67
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@4a
    const/4 v1, 0x1

    #@4b
    iput v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    #@4d
    .line 68
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@4f
    const/4 v1, 0x0

    #@50
    iput v1, v0, Lcom/android/server/display/DisplayDeviceInfo;->touch:I

    #@52
    .line 70
    :cond_52
    iget-object v0, p0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;->mInfo:Lcom/android/server/display/DisplayDeviceInfo;

    #@54
    return-object v0
.end method
