.class final Lcom/android/server/display/OverlayDisplayWindow;
.super Ljava/lang/Object;
.source "OverlayDisplayWindow.java"

# interfaces
.implements Lcom/android/internal/util/DumpUtils$Dump;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/OverlayDisplayWindow$Listener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "OverlayDisplayWindow"


# instance fields
.field private final DISABLE_MOVE_AND_RESIZE:Z

.field private final INITIAL_SCALE:F

.field private final MAX_SCALE:F

.field private final MIN_SCALE:F

.field private final WINDOW_ALPHA:F

.field private final mContext:Landroid/content/Context;

.field private final mDefaultDisplay:Landroid/view/Display;

.field private final mDefaultDisplayInfo:Landroid/view/DisplayInfo;

.field private final mDensityDpi:I

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private final mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mGravity:I

.field private final mHeight:I

.field private final mListener:Lcom/android/server/display/OverlayDisplayWindow$Listener;

.field private mLiveScale:F

.field private mLiveTranslationX:F

.field private mLiveTranslationY:F

.field private final mName:Ljava/lang/String;

.field private final mOnGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private final mOnScaleGestureListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field private final mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private final mSurfaceTextureListener:Landroid/view/TextureView$SurfaceTextureListener;

.field private mTextureView:Landroid/view/TextureView;

.field private final mTitle:Ljava/lang/String;

.field private mTitleTextView:Landroid/widget/TextView;

.field private final mWidth:I

.field private mWindowContent:Landroid/view/View;

.field private final mWindowManager:Landroid/view/WindowManager;

.field private mWindowParams:Landroid/view/WindowManager$LayoutParams;

.field private mWindowScale:F

.field private mWindowVisible:Z

.field private mWindowX:I

.field private mWindowY:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIIILcom/android/server/display/OverlayDisplayWindow$Listener;)V
    .registers 13
    .parameter "context"
    .parameter "name"
    .parameter "width"
    .parameter "height"
    .parameter "densityDpi"
    .parameter "gravity"
    .parameter "listener"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/high16 v1, 0x3f80

    #@3
    .line 95
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 51
    const/high16 v0, 0x3f00

    #@8
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->INITIAL_SCALE:F

    #@a
    .line 52
    const v0, 0x3e99999a

    #@d
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->MIN_SCALE:F

    #@f
    .line 53
    iput v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->MAX_SCALE:F

    #@11
    .line 54
    const v0, 0x3f4ccccd

    #@14
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->WINDOW_ALPHA:F

    #@16
    .line 59
    iput-boolean v4, p0, Lcom/android/server/display/OverlayDisplayWindow;->DISABLE_MOVE_AND_RESIZE:Z

    #@18
    .line 75
    new-instance v0, Landroid/view/DisplayInfo;

    #@1a
    invoke-direct {v0}, Landroid/view/DisplayInfo;-><init>()V

    #@1d
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@1f
    .line 92
    iput v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveScale:F

    #@21
    .line 264
    new-instance v0, Lcom/android/server/display/OverlayDisplayWindow$1;

    #@23
    invoke-direct {v0, p0}, Lcom/android/server/display/OverlayDisplayWindow$1;-><init>(Lcom/android/server/display/OverlayDisplayWindow;)V

    #@26
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@28
    .line 289
    new-instance v0, Lcom/android/server/display/OverlayDisplayWindow$2;

    #@2a
    invoke-direct {v0, p0}, Lcom/android/server/display/OverlayDisplayWindow$2;-><init>(Lcom/android/server/display/OverlayDisplayWindow;)V

    #@2d
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mSurfaceTextureListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@2f
    .line 313
    new-instance v0, Lcom/android/server/display/OverlayDisplayWindow$3;

    #@31
    invoke-direct {v0, p0}, Lcom/android/server/display/OverlayDisplayWindow$3;-><init>(Lcom/android/server/display/OverlayDisplayWindow;)V

    #@34
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    #@36
    .line 337
    new-instance v0, Lcom/android/server/display/OverlayDisplayWindow$4;

    #@38
    invoke-direct {v0, p0}, Lcom/android/server/display/OverlayDisplayWindow$4;-><init>(Lcom/android/server/display/OverlayDisplayWindow;)V

    #@3b
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mOnGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    #@3d
    .line 349
    new-instance v0, Lcom/android/server/display/OverlayDisplayWindow$5;

    #@3f
    invoke-direct {v0, p0}, Lcom/android/server/display/OverlayDisplayWindow$5;-><init>(Lcom/android/server/display/OverlayDisplayWindow;)V

    #@42
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mOnScaleGestureListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    #@44
    .line 96
    iput-object p1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mContext:Landroid/content/Context;

    #@46
    .line 97
    iput-object p2, p0, Lcom/android/server/display/OverlayDisplayWindow;->mName:Ljava/lang/String;

    #@48
    .line 98
    iput p3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWidth:I

    #@4a
    .line 99
    iput p4, p0, Lcom/android/server/display/OverlayDisplayWindow;->mHeight:I

    #@4c
    .line 100
    iput p5, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDensityDpi:I

    #@4e
    .line 101
    iput p6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mGravity:I

    #@50
    .line 102
    iput-object p7, p0, Lcom/android/server/display/OverlayDisplayWindow;->mListener:Lcom/android/server/display/OverlayDisplayWindow$Listener;

    #@52
    .line 103
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@55
    move-result-object v0

    #@56
    const v1, 0x1040546

    #@59
    const/4 v2, 0x4

    #@5a
    new-array v2, v2, [Ljava/lang/Object;

    #@5c
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mName:Ljava/lang/String;

    #@5e
    aput-object v3, v2, v4

    #@60
    const/4 v3, 0x1

    #@61
    iget v4, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWidth:I

    #@63
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v4

    #@67
    aput-object v4, v2, v3

    #@69
    const/4 v3, 0x2

    #@6a
    iget v4, p0, Lcom/android/server/display/OverlayDisplayWindow;->mHeight:I

    #@6c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6f
    move-result-object v4

    #@70
    aput-object v4, v2, v3

    #@72
    const/4 v3, 0x3

    #@73
    iget v4, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDensityDpi:I

    #@75
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@78
    move-result-object v4

    #@79
    aput-object v4, v2, v3

    #@7b
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@7e
    move-result-object v0

    #@7f
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTitle:Ljava/lang/String;

    #@81
    .line 107
    const-string v0, "display"

    #@83
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@86
    move-result-object v0

    #@87
    check-cast v0, Landroid/hardware/display/DisplayManager;

    #@89
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    #@8b
    .line 109
    const-string v0, "window"

    #@8d
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@90
    move-result-object v0

    #@91
    check-cast v0, Landroid/view/WindowManager;

    #@93
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowManager:Landroid/view/WindowManager;

    #@95
    .line 112
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowManager:Landroid/view/WindowManager;

    #@97
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@9a
    move-result-object v0

    #@9b
    iput-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplay:Landroid/view/Display;

    #@9d
    .line 113
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->updateDefaultDisplayInfo()Z

    #@a0
    .line 115
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->createWindow()V

    #@a3
    .line 116
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/display/OverlayDisplayWindow;)Landroid/view/Display;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplay:Landroid/view/Display;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/display/OverlayDisplayWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->updateDefaultDisplayInfo()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/display/OverlayDisplayWindow;)Landroid/view/DisplayInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/display/OverlayDisplayWindow;)Lcom/android/server/display/OverlayDisplayWindow$Listener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mListener:Lcom/android/server/display/OverlayDisplayWindow$Listener;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/display/OverlayDisplayWindow;)Landroid/view/GestureDetector;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mGestureDetector:Landroid/view/GestureDetector;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/display/OverlayDisplayWindow;)Landroid/view/ScaleGestureDetector;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/display/OverlayDisplayWindow;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->saveWindowParams()V

    #@3
    return-void
.end method

.method static synthetic access$724(Lcom/android/server/display/OverlayDisplayWindow;F)F
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iget v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationX:F

    #@2
    sub-float/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationX:F

    #@5
    return v0
.end method

.method static synthetic access$824(Lcom/android/server/display/OverlayDisplayWindow;F)F
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iget v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationY:F

    #@2
    sub-float/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationY:F

    #@5
    return v0
.end method

.method static synthetic access$932(Lcom/android/server/display/OverlayDisplayWindow;F)F
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iget v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveScale:F

    #@2
    mul-float/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveScale:F

    #@5
    return v0
.end method

.method private clearLiveState()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 259
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationX:F

    #@3
    .line 260
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationY:F

    #@5
    .line 261
    const/high16 v0, 0x3f80

    #@7
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveScale:F

    #@9
    .line 262
    return-void
.end method

.method private createWindow()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 174
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@7
    move-result-object v0

    #@8
    .line 176
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x109009b

    #@b
    const/4 v3, 0x0

    #@c
    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@f
    move-result-object v1

    #@10
    iput-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowContent:Landroid/view/View;

    #@12
    .line 178
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowContent:Landroid/view/View;

    #@14
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    #@16
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@19
    .line 180
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowContent:Landroid/view/View;

    #@1b
    const v3, 0x102034b

    #@1e
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/view/TextureView;

    #@24
    iput-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@26
    .line 182
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@28
    invoke-virtual {v1, v4}, Landroid/view/TextureView;->setPivotX(F)V

    #@2b
    .line 183
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@2d
    invoke-virtual {v1, v4}, Landroid/view/TextureView;->setPivotY(F)V

    #@30
    .line 184
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@32
    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@35
    move-result-object v1

    #@36
    iget v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWidth:I

    #@38
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3a
    .line 185
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@3c
    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3f
    move-result-object v1

    #@40
    iget v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mHeight:I

    #@42
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@44
    .line 186
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@46
    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setOpaque(Z)V

    #@49
    .line 187
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@4b
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mSurfaceTextureListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@4d
    invoke-virtual {v1, v3}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    #@50
    .line 189
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowContent:Landroid/view/View;

    #@52
    const v3, 0x102034c

    #@55
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@58
    move-result-object v1

    #@59
    check-cast v1, Landroid/widget/TextView;

    #@5b
    iput-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTitleTextView:Landroid/widget/TextView;

    #@5d
    .line 191
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTitleTextView:Landroid/widget/TextView;

    #@5f
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTitle:Ljava/lang/String;

    #@61
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@64
    .line 193
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    #@66
    const/16 v3, 0x7ea

    #@68
    invoke-direct {v1, v3}, Landroid/view/WindowManager$LayoutParams;-><init>(I)V

    #@6b
    iput-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@6d
    .line 195
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@6f
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@71
    const v4, 0x1000328

    #@74
    or-int/2addr v3, v4

    #@75
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@77
    .line 203
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@79
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@7b
    or-int/lit8 v3, v3, 0x2

    #@7d
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@7f
    .line 205
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@81
    const v3, 0x3f4ccccd

    #@84
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@86
    .line 206
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@88
    const/16 v3, 0x33

    #@8a
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@8c
    .line 207
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@8e
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTitle:Ljava/lang/String;

    #@90
    invoke-virtual {v1, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@93
    .line 209
    new-instance v1, Landroid/view/GestureDetector;

    #@95
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mContext:Landroid/content/Context;

    #@97
    iget-object v4, p0, Lcom/android/server/display/OverlayDisplayWindow;->mOnGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    #@99
    invoke-direct {v1, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    #@9c
    iput-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mGestureDetector:Landroid/view/GestureDetector;

    #@9e
    .line 210
    new-instance v1, Landroid/view/ScaleGestureDetector;

    #@a0
    iget-object v3, p0, Lcom/android/server/display/OverlayDisplayWindow;->mContext:Landroid/content/Context;

    #@a2
    iget-object v4, p0, Lcom/android/server/display/OverlayDisplayWindow;->mOnScaleGestureListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    #@a4
    invoke-direct {v1, v3, v4}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    #@a7
    iput-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    #@a9
    .line 214
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mGravity:I

    #@ab
    and-int/lit8 v1, v1, 0x3

    #@ad
    const/4 v3, 0x3

    #@ae
    if-ne v1, v3, :cond_c2

    #@b0
    move v1, v2

    #@b1
    :goto_b1
    iput v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowX:I

    #@b3
    .line 216
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mGravity:I

    #@b5
    and-int/lit8 v1, v1, 0x30

    #@b7
    const/16 v3, 0x30

    #@b9
    if-ne v1, v3, :cond_c7

    #@bb
    :goto_bb
    iput v2, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowY:I

    #@bd
    .line 218
    const/high16 v1, 0x3f00

    #@bf
    iput v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowScale:F

    #@c1
    .line 219
    return-void

    #@c2
    .line 214
    :cond_c2
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@c4
    iget v1, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    #@c6
    goto :goto_b1

    #@c7
    .line 216
    :cond_c7
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@c9
    iget v2, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    #@cb
    goto :goto_bb
.end method

.method private saveWindowParams()V
    .registers 2

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@2
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@4
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowX:I

    #@6
    .line 253
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@8
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@a
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowY:I

    #@c
    .line 254
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@e
    invoke-virtual {v0}, Landroid/view/TextureView;->getScaleX()F

    #@11
    move-result v0

    #@12
    iput v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowScale:F

    #@14
    .line 255
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->clearLiveState()V

    #@17
    .line 256
    return-void
.end method

.method private updateDefaultDisplayInfo()Z
    .registers 3

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplay:Landroid/view/Display;

    #@2
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@4
    invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_13

    #@a
    .line 166
    const-string v0, "OverlayDisplayWindow"

    #@c
    const-string v1, "Cannot show overlay display because there is no default display upon which to show it."

    #@e
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 168
    const/4 v0, 0x0

    #@12
    .line 170
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x1

    #@14
    goto :goto_12
.end method

.method private updateWindowParams()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/high16 v8, 0x3f80

    #@3
    .line 222
    iget v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowScale:F

    #@5
    iget v7, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveScale:F

    #@7
    mul-float v2, v6, v7

    #@9
    .line 223
    .local v2, scale:F
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@b
    iget v6, v6, Landroid/view/DisplayInfo;->logicalWidth:I

    #@d
    int-to-float v6, v6

    #@e
    iget v7, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWidth:I

    #@10
    int-to-float v7, v7

    #@11
    div-float/2addr v6, v7

    #@12
    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    #@15
    move-result v2

    #@16
    .line 224
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@18
    iget v6, v6, Landroid/view/DisplayInfo;->logicalHeight:I

    #@1a
    int-to-float v6, v6

    #@1b
    iget v7, p0, Lcom/android/server/display/OverlayDisplayWindow;->mHeight:I

    #@1d
    int-to-float v7, v7

    #@1e
    div-float/2addr v6, v7

    #@1f
    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    #@22
    move-result v2

    #@23
    .line 225
    const v6, 0x3e99999a

    #@26
    invoke-static {v8, v2}, Ljava/lang/Math;->min(FF)F

    #@29
    move-result v7

    #@2a
    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    #@2d
    move-result v2

    #@2e
    .line 227
    iget v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowScale:F

    #@30
    div-float v6, v2, v6

    #@32
    sub-float/2addr v6, v8

    #@33
    const/high16 v7, 0x3f00

    #@35
    mul-float v1, v6, v7

    #@37
    .line 228
    .local v1, offsetScale:F
    iget v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWidth:I

    #@39
    int-to-float v6, v6

    #@3a
    mul-float/2addr v6, v2

    #@3b
    float-to-int v3, v6

    #@3c
    .line 229
    .local v3, width:I
    iget v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mHeight:I

    #@3e
    int-to-float v6, v6

    #@3f
    mul-float/2addr v6, v2

    #@40
    float-to-int v0, v6

    #@41
    .line 230
    .local v0, height:I
    iget v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowX:I

    #@43
    int-to-float v6, v6

    #@44
    iget v7, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationX:F

    #@46
    add-float/2addr v6, v7

    #@47
    int-to-float v7, v3

    #@48
    mul-float/2addr v7, v1

    #@49
    sub-float/2addr v6, v7

    #@4a
    float-to-int v4, v6

    #@4b
    .line 231
    .local v4, x:I
    iget v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowY:I

    #@4d
    int-to-float v6, v6

    #@4e
    iget v7, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationY:F

    #@50
    add-float/2addr v6, v7

    #@51
    int-to-float v7, v0

    #@52
    mul-float/2addr v7, v1

    #@53
    sub-float/2addr v6, v7

    #@54
    float-to-int v5, v6

    #@55
    .line 232
    .local v5, y:I
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@57
    iget v6, v6, Landroid/view/DisplayInfo;->logicalWidth:I

    #@59
    sub-int/2addr v6, v3

    #@5a
    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    #@5d
    move-result v6

    #@5e
    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    #@61
    move-result v4

    #@62
    .line 233
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@64
    iget v6, v6, Landroid/view/DisplayInfo;->logicalHeight:I

    #@66
    sub-int/2addr v6, v0

    #@67
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@6a
    move-result v6

    #@6b
    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    #@6e
    move-result v5

    #@6f
    .line 242
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@71
    invoke-virtual {v6, v2}, Landroid/view/TextureView;->setScaleX(F)V

    #@74
    .line 243
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@76
    invoke-virtual {v6, v2}, Landroid/view/TextureView;->setScaleY(F)V

    #@79
    .line 245
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@7b
    iput v4, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    #@7d
    .line 246
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@7f
    iput v5, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    #@81
    .line 247
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@83
    iput v3, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@85
    .line 248
    iget-object v6, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@87
    iput v0, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@89
    .line 249
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 3

    #@0
    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowVisible:Z

    #@2
    if-eqz v0, :cond_15

    #@4
    .line 135
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    #@6
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@8
    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    #@b
    .line 136
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowManager:Landroid/view/WindowManager;

    #@d
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowContent:Landroid/view/View;

    #@f
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@12
    .line 137
    const/4 v0, 0x0

    #@13
    iput-boolean v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowVisible:Z

    #@15
    .line 139
    :cond_15
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "mWindowVisible="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowVisible:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@18
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v1, "mWindowX="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowX:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@30
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v1, "mWindowY="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowY:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@48
    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v1, "mWindowScale="

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowScale:F

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@60
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v1, "mWindowParams="

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@78
    .line 155
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@7a
    if-eqz v0, :cond_b4

    #@7c
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v1, "mTextureView.getScaleX()="

    #@83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v0

    #@87
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@89
    invoke-virtual {v1}, Landroid/view/TextureView;->getScaleX()F

    #@8c
    move-result v1

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v1, "mTextureView.getScaleY()="

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v0

    #@a3
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mTextureView:Landroid/view/TextureView;

    #@a5
    invoke-virtual {v1}, Landroid/view/TextureView;->getScaleY()F

    #@a8
    move-result v1

    #@a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v0

    #@ad
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v0

    #@b1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b4
    .line 159
    :cond_b4
    new-instance v0, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v1, "mLiveTranslationX="

    #@bb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v0

    #@bf
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationX:F

    #@c1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v0

    #@c5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v0

    #@c9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@cc
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v1, "mLiveTranslationY="

    #@d3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v0

    #@d7
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveTranslationY:F

    #@d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v0

    #@dd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v0

    #@e1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e4
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v1, "mLiveScale="

    #@eb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v0

    #@ef
    iget v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mLiveScale:F

    #@f1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v0

    #@f5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v0

    #@f9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fc
    .line 162
    return-void
.end method

.method public relayout()V
    .registers 4

    #@0
    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowVisible:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 143
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->updateWindowParams()V

    #@7
    .line 144
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowManager:Landroid/view/WindowManager;

    #@9
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowContent:Landroid/view/View;

    #@b
    iget-object v2, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@d
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@10
    .line 146
    :cond_10
    return-void
.end method

.method public show()V
    .registers 4

    #@0
    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowVisible:Z

    #@2
    if-nez v0, :cond_19

    #@4
    .line 120
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    #@6
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    #@c
    .line 121
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->updateDefaultDisplayInfo()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1a

    #@12
    .line 122
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    #@14
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@16
    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    #@19
    .line 131
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 126
    :cond_1a
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->clearLiveState()V

    #@1d
    .line 127
    invoke-direct {p0}, Lcom/android/server/display/OverlayDisplayWindow;->updateWindowParams()V

    #@20
    .line 128
    iget-object v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowManager:Landroid/view/WindowManager;

    #@22
    iget-object v1, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowContent:Landroid/view/View;

    #@24
    iget-object v2, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@26
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@29
    .line 129
    const/4 v0, 0x1

    #@2a
    iput-boolean v0, p0, Lcom/android/server/display/OverlayDisplayWindow;->mWindowVisible:Z

    #@2c
    goto :goto_19
.end method
