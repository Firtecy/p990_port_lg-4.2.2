.class final Lcom/android/server/display/HeadlessDisplayAdapter;
.super Lcom/android/server/display/DisplayAdapter;
.source "HeadlessDisplayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlessDisplayAdapter"


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;)V
    .registers 11
    .parameter "syncRoot"
    .parameter "context"
    .parameter "handler"
    .parameter "listener"

    #@0
    .prologue
    .line 36
    const-string v5, "HeadlessDisplayAdapter"

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/DisplayAdapter;-><init>(Lcom/android/server/display/DisplayManagerService$SyncRoot;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/display/DisplayAdapter$Listener;Ljava/lang/String;)V

    #@a
    .line 37
    return-void
.end method


# virtual methods
.method public registerLocked()V
    .registers 3

    #@0
    .prologue
    .line 41
    invoke-super {p0}, Lcom/android/server/display/DisplayAdapter;->registerLocked()V

    #@3
    .line 42
    new-instance v0, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;

    #@5
    invoke-direct {v0, p0}, Lcom/android/server/display/HeadlessDisplayAdapter$HeadlessDisplayDevice;-><init>(Lcom/android/server/display/HeadlessDisplayAdapter;)V

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {p0, v0, v1}, Lcom/android/server/display/HeadlessDisplayAdapter;->sendDisplayDeviceEventLocked(Lcom/android/server/display/DisplayDevice;I)V

    #@c
    .line 43
    return-void
.end method
