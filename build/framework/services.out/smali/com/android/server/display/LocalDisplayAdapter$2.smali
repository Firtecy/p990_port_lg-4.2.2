.class Lcom/android/server/display/LocalDisplayAdapter$2;
.super Landroid/content/BroadcastReceiver;
.source "LocalDisplayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/LocalDisplayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/LocalDisplayAdapter;


# direct methods
.method constructor <init>(Lcom/android/server/display/LocalDisplayAdapter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 139
    iput-object p1, p0, Lcom/android/server/display/LocalDisplayAdapter$2;->this$0:Lcom/android/server/display/LocalDisplayAdapter;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 142
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    const-string v4, "com.lge.systemservice.core.wfdmanager.WFD_STATE_CHANGED"

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_57

    #@e
    const/4 v3, 0x5

    #@f
    const-string v4, "wfd_state"

    #@11
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@14
    move-result v4

    #@15
    if-ne v3, v4, :cond_57

    #@17
    .line 144
    iget-object v3, p0, Lcom/android/server/display/LocalDisplayAdapter$2;->this$0:Lcom/android/server/display/LocalDisplayAdapter;

    #@19
    const-string v4, "hdcp_enabled"

    #@1b
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@1e
    move-result v4

    #@1f
    invoke-static {v3, v4}, Lcom/android/server/display/LocalDisplayAdapter;->access$102(Lcom/android/server/display/LocalDisplayAdapter;Z)Z

    #@22
    .line 145
    iget-object v3, p0, Lcom/android/server/display/LocalDisplayAdapter$2;->this$0:Lcom/android/server/display/LocalDisplayAdapter;

    #@24
    invoke-static {v3}, Lcom/android/server/display/LocalDisplayAdapter;->access$100(Lcom/android/server/display/LocalDisplayAdapter;)Z

    #@27
    move-result v3

    #@28
    if-nez v3, :cond_57

    #@2a
    .line 146
    invoke-static {v6}, Landroid/view/Surface;->getBuiltInDisplay(I)Landroid/os/IBinder;

    #@2d
    move-result-object v1

    #@2e
    .line 147
    .local v1, displayToken:Landroid/os/IBinder;
    new-instance v2, Landroid/view/Surface$PhysicalDisplayInfo;

    #@30
    invoke-direct {v2}, Landroid/view/Surface$PhysicalDisplayInfo;-><init>()V

    #@33
    .line 148
    .local v2, wfdPhys:Landroid/view/Surface$PhysicalDisplayInfo;
    if-eqz v1, :cond_57

    #@35
    invoke-static {v1, v2}, Landroid/view/Surface;->getDisplayInfo(Landroid/os/IBinder;Landroid/view/Surface$PhysicalDisplayInfo;)Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_57

    #@3b
    .line 149
    iget-object v3, p0, Lcom/android/server/display/LocalDisplayAdapter$2;->this$0:Lcom/android/server/display/LocalDisplayAdapter;

    #@3d
    invoke-static {v3}, Lcom/android/server/display/LocalDisplayAdapter;->access$200(Lcom/android/server/display/LocalDisplayAdapter;)Landroid/util/SparseArray;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    #@47
    .line 150
    .local v0, device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    if-eqz v0, :cond_57

    #@49
    .line 151
    iput-boolean v5, v2, Landroid/view/Surface$PhysicalDisplayInfo;->secure:Z

    #@4b
    .line 152
    invoke-virtual {v0, v2}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->updatePhysicalDisplayInfoLocked(Landroid/view/Surface$PhysicalDisplayInfo;)Z

    #@4e
    move-result v3

    #@4f
    if-eqz v3, :cond_57

    #@51
    .line 153
    iget-object v3, p0, Lcom/android/server/display/LocalDisplayAdapter$2;->this$0:Lcom/android/server/display/LocalDisplayAdapter;

    #@53
    const/4 v4, 0x2

    #@54
    invoke-virtual {v3, v0, v4}, Lcom/android/server/display/LocalDisplayAdapter;->sendDisplayDeviceEventLocked(Lcom/android/server/display/DisplayDevice;I)V

    #@57
    .line 159
    .end local v0           #device:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;
    .end local v1           #displayToken:Landroid/os/IBinder;
    .end local v2           #wfdPhys:Landroid/view/Surface$PhysicalDisplayInfo;
    :cond_57
    return-void
.end method
