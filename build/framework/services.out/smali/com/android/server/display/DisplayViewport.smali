.class public final Lcom/android/server/display/DisplayViewport;
.super Ljava/lang/Object;
.source "DisplayViewport.java"


# instance fields
.field public deviceHeight:I

.field public deviceWidth:I

.field public displayId:I

.field public final logicalFrame:Landroid/graphics/Rect;

.field public orientation:I

.field public final physicalFrame:Landroid/graphics/Rect;

.field public valid:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@a
    .line 45
    new-instance v0, Landroid/graphics/Rect;

    #@c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@11
    return-void
.end method


# virtual methods
.method public copyFrom(Lcom/android/server/display/DisplayViewport;)V
    .registers 4
    .parameter "viewport"

    #@0
    .prologue
    .line 54
    iget-boolean v0, p1, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@2
    iput-boolean v0, p0, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@4
    .line 55
    iget v0, p1, Lcom/android/server/display/DisplayViewport;->displayId:I

    #@6
    iput v0, p0, Lcom/android/server/display/DisplayViewport;->displayId:I

    #@8
    .line 56
    iget v0, p1, Lcom/android/server/display/DisplayViewport;->orientation:I

    #@a
    iput v0, p0, Lcom/android/server/display/DisplayViewport;->orientation:I

    #@c
    .line 57
    iget-object v0, p0, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@e
    iget-object v1, p1, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@10
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@13
    .line 58
    iget-object v0, p0, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@15
    iget-object v1, p1, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@17
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@1a
    .line 59
    iget v0, p1, Lcom/android/server/display/DisplayViewport;->deviceWidth:I

    #@1c
    iput v0, p0, Lcom/android/server/display/DisplayViewport;->deviceWidth:I

    #@1e
    .line 60
    iget v0, p1, Lcom/android/server/display/DisplayViewport;->deviceHeight:I

    #@20
    iput v0, p0, Lcom/android/server/display/DisplayViewport;->deviceHeight:I

    #@22
    .line 61
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DisplayViewport{valid="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", displayId="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/android/server/display/DisplayViewport;->displayId:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", orientation="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/server/display/DisplayViewport;->orientation:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", logicalFrame="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p0, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ", physicalFrame="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ", deviceWidth="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Lcom/android/server/display/DisplayViewport;->deviceWidth:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, ", deviceHeight="

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget v1, p0, Lcom/android/server/display/DisplayViewport;->deviceHeight:I

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const-string v1, "}"

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    return-object v0
.end method
