.class Lcom/android/server/MSimTelephonyRegistry;
.super Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;
.source "MSimTelephonyRegistry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MSimTelephonyRegistry$Record;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final DBG_LOC:Z = false

.field private static final MSG_USER_SWITCHED:I = 0x1

.field static final PHONE_STATE_PERMISSION_MASK:I = 0xec

.field private static final TAG:Ljava/lang/String; = "MSimTelephonyRegistry"


# instance fields
.field private final mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallForwarding:[Z

.field private mCallIncomingNumber:[Ljava/lang/String;

.field private mCallState:[I

.field private mCellInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCellLocation:[Landroid/os/Bundle;

.field private mConnectedApns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mDataActivity:I

.field private mDataConnectionApn:Ljava/lang/String;

.field private mDataConnectionLinkCapabilities:Landroid/net/LinkCapabilities;

.field private mDataConnectionLinkProperties:Landroid/net/LinkProperties;

.field private mDataConnectionNetworkType:I

.field private mDataConnectionPossible:Z

.field private mDataConnectionReason:Ljava/lang/String;

.field private mDataConnectionState:I

.field private mDefaultSubscription:I

.field private final mHandler:Landroid/os/Handler;

.field private mMessageWaiting:[Z

.field private mOtaspMode:I

.field private final mRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/MSimTelephonyRegistry$Record;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoveList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceState:[Landroid/telephony/ServiceState;

.field private mSignalStrength:[Landroid/telephony/SignalStrength;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 183
    invoke-direct {p0}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;-><init>()V

    #@5
    .line 97
    new-instance v3, Ljava/util/ArrayList;

    #@7
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@c
    .line 98
    new-instance v3, Ljava/util/ArrayList;

    #@e
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@13
    .line 114
    iput v5, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataActivity:I

    #@15
    .line 116
    const/4 v3, -0x1

    #@16
    iput v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionState:I

    #@18
    .line 118
    iput-boolean v5, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionPossible:Z

    #@1a
    .line 120
    const-string v3, ""

    #@1c
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionReason:Ljava/lang/String;

    #@1e
    .line 122
    const-string v3, ""

    #@20
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionApn:Ljava/lang/String;

    #@22
    .line 134
    const/4 v3, 0x1

    #@23
    iput v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mOtaspMode:I

    #@25
    .line 136
    iput-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellInfo:Ljava/util/ArrayList;

    #@27
    .line 138
    iput v5, p0, Lcom/android/server/MSimTelephonyRegistry;->mDefaultSubscription:I

    #@29
    .line 149
    new-instance v3, Lcom/android/server/MSimTelephonyRegistry$1;

    #@2b
    invoke-direct {v3, p0}, Lcom/android/server/MSimTelephonyRegistry$1;-><init>(Lcom/android/server/MSimTelephonyRegistry;)V

    #@2e
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mHandler:Landroid/os/Handler;

    #@30
    .line 165
    new-instance v3, Lcom/android/server/MSimTelephonyRegistry$2;

    #@32
    invoke-direct {v3, p0}, Lcom/android/server/MSimTelephonyRegistry$2;-><init>(Lcom/android/server/MSimTelephonyRegistry;)V

    #@35
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@37
    .line 184
    invoke-static {}, Landroid/telephony/CellLocation;->getEmpty()Landroid/telephony/CellLocation;

    #@3a
    move-result-object v1

    #@3b
    .line 186
    .local v1, location:Landroid/telephony/CellLocation;
    iput-object p1, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@3d
    .line 187
    invoke-static {}, Lcom/android/server/am/BatteryStatsService;->getService()Lcom/android/internal/app/IBatteryStats;

    #@40
    move-result-object v3

    #@41
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@43
    .line 188
    new-instance v3, Ljava/util/ArrayList;

    #@45
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@48
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@4a
    .line 191
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@51
    move-result v3

    #@52
    iput v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mDefaultSubscription:I

    #@54
    .line 193
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@5b
    move-result v2

    #@5c
    .line 194
    .local v2, numPhones:I
    new-array v3, v2, [I

    #@5e
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallState:[I

    #@60
    .line 195
    new-array v3, v2, [Ljava/lang/String;

    #@62
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallIncomingNumber:[Ljava/lang/String;

    #@64
    .line 196
    new-array v3, v2, [Landroid/telephony/ServiceState;

    #@66
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mServiceState:[Landroid/telephony/ServiceState;

    #@68
    .line 197
    new-array v3, v2, [Landroid/telephony/SignalStrength;

    #@6a
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mSignalStrength:[Landroid/telephony/SignalStrength;

    #@6c
    .line 198
    new-array v3, v2, [Z

    #@6e
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mMessageWaiting:[Z

    #@70
    .line 199
    new-array v3, v2, [Z

    #@72
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallForwarding:[Z

    #@74
    .line 200
    new-array v3, v2, [Landroid/os/Bundle;

    #@76
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellLocation:[Landroid/os/Bundle;

    #@78
    .line 201
    new-instance v3, Ljava/util/ArrayList;

    #@7a
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@7d
    iput-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellInfo:Ljava/util/ArrayList;

    #@7f
    .line 202
    const/4 v0, 0x0

    #@80
    .local v0, i:I
    :goto_80
    if-ge v0, v2, :cond_b7

    #@82
    .line 203
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallState:[I

    #@84
    aput v5, v3, v0

    #@86
    .line 204
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallIncomingNumber:[Ljava/lang/String;

    #@88
    const-string v4, ""

    #@8a
    aput-object v4, v3, v0

    #@8c
    .line 205
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mServiceState:[Landroid/telephony/ServiceState;

    #@8e
    new-instance v4, Landroid/telephony/ServiceState;

    #@90
    invoke-direct {v4}, Landroid/telephony/ServiceState;-><init>()V

    #@93
    aput-object v4, v3, v0

    #@95
    .line 206
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mSignalStrength:[Landroid/telephony/SignalStrength;

    #@97
    new-instance v4, Landroid/telephony/SignalStrength;

    #@99
    invoke-direct {v4}, Landroid/telephony/SignalStrength;-><init>()V

    #@9c
    aput-object v4, v3, v0

    #@9e
    .line 207
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mMessageWaiting:[Z

    #@a0
    aput-boolean v5, v3, v0

    #@a2
    .line 208
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallForwarding:[Z

    #@a4
    aput-boolean v5, v3, v0

    #@a6
    .line 209
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellLocation:[Landroid/os/Bundle;

    #@a8
    new-instance v4, Landroid/os/Bundle;

    #@aa
    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    #@ad
    aput-object v4, v3, v0

    #@af
    .line 210
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellInfo:Ljava/util/ArrayList;

    #@b1
    invoke-virtual {v3, v0, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@b4
    .line 202
    add-int/lit8 v0, v0, 0x1

    #@b6
    goto :goto_80

    #@b7
    .line 215
    :cond_b7
    if-eqz v1, :cond_ce

    #@b9
    .line 216
    const/4 v0, 0x0

    #@ba
    :goto_ba
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@bd
    move-result-object v3

    #@be
    invoke-virtual {v3}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@c1
    move-result v3

    #@c2
    if-ge v0, v3, :cond_ce

    #@c4
    .line 217
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellLocation:[Landroid/os/Bundle;

    #@c6
    aget-object v3, v3, v0

    #@c8
    invoke-virtual {v1, v3}, Landroid/telephony/CellLocation;->fillInNotifierBundle(Landroid/os/Bundle;)V

    #@cb
    .line 216
    add-int/lit8 v0, v0, 0x1

    #@cd
    goto :goto_ba

    #@ce
    .line 220
    :cond_ce
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/MSimTelephonyRegistry;)[Landroid/os/Bundle;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellLocation:[Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/MSimTelephonyRegistry;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private broadcastCallStateChanged(ILjava/lang/String;I)V
    .registers 10
    .parameter "state"
    .parameter "incomingNumber"
    .parameter "subscription"

    #@0
    .prologue
    .line 759
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 761
    .local v0, ident:J
    if-nez p1, :cond_3c

    #@6
    .line 762
    :try_start_6
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@8
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->notePhoneOff()V
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_44
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_42

    #@b
    .line 769
    :goto_b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@e
    .line 772
    new-instance v2, Landroid/content/Intent;

    #@10
    const-string v3, "android.intent.action.PHONE_STATE"

    #@12
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15
    .line 773
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "state"

    #@17
    invoke-static {p1}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->convertCallState(I)Lcom/android/internal/telephony/PhoneConstants$State;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneConstants$State;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@22
    .line 775
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_2d

    #@28
    .line 776
    const-string v3, "incoming_number"

    #@2a
    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2d
    .line 778
    :cond_2d
    const-string v3, "subscription"

    #@2f
    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@32
    .line 779
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@34
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@36
    const-string v5, "android.permission.READ_PHONE_STATE"

    #@38
    invoke-virtual {v3, v2, v4, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    #@3b
    .line 781
    return-void

    #@3c
    .line 764
    .end local v2           #intent:Landroid/content/Intent;
    :cond_3c
    :try_start_3c
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@3e
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->notePhoneOn()V
    :try_end_41
    .catchall {:try_start_3c .. :try_end_41} :catchall_44
    .catch Landroid/os/RemoteException; {:try_start_3c .. :try_end_41} :catch_42

    #@41
    goto :goto_b

    #@42
    .line 766
    :catch_42
    move-exception v3

    #@43
    goto :goto_b

    #@44
    .line 769
    :catchall_44
    move-exception v3

    #@45
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@48
    throw v3
.end method

.method private broadcastDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "reason"
    .parameter "apnType"

    #@0
    .prologue
    .line 828
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.DATA_CONNECTION_FAILED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 829
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "reason"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 830
    const-string v1, "apnType"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11
    .line 831
    const-string v1, "subscription"

    #@13
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->getPreferredDataSubscription()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1e
    .line 833
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@20
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@22
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@25
    .line 834
    return-void
.end method

.method private broadcastDataConnectionStateChanged(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;ZI)V
    .registers 15
    .parameter "state"
    .parameter "isDataConnectivityPossible"
    .parameter "reason"
    .parameter "apn"
    .parameter "apnType"
    .parameter "linkProperties"
    .parameter "linkCapabilities"
    .parameter "roaming"
    .parameter "smCause"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 798
    new-instance v1, Landroid/content/Intent;

    #@3
    const-string v2, "android.intent.action.ANY_DATA_STATE"

    #@5
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8
    .line 799
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "state"

    #@a
    invoke-static {p1}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->convertDataState(I)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneConstants$DataState;->toString()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 801
    if-nez p2, :cond_1c

    #@17
    .line 802
    const-string v2, "networkUnvailable"

    #@19
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1c
    .line 804
    :cond_1c
    if-eqz p3, :cond_23

    #@1e
    .line 805
    const-string v2, "reason"

    #@20
    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@23
    .line 807
    :cond_23
    if-eqz p6, :cond_35

    #@25
    .line 808
    const-string v2, "linkProperties"

    #@27
    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@2a
    .line 809
    invoke-virtual {p6}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    .line 810
    .local v0, iface:Ljava/lang/String;
    if-eqz v0, :cond_35

    #@30
    .line 811
    const-string v2, "iface"

    #@32
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@35
    .line 814
    .end local v0           #iface:Ljava/lang/String;
    :cond_35
    if-eqz p7, :cond_3c

    #@37
    .line 815
    const-string v2, "linkCapabilities"

    #@39
    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@3c
    .line 817
    :cond_3c
    if-eqz p8, :cond_43

    #@3e
    const-string v2, "networkRoaming"

    #@40
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@43
    .line 819
    :cond_43
    const-string v2, "apn"

    #@45
    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@48
    .line 820
    const-string v2, "apnType"

    #@4a
    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4d
    .line 822
    const-string v2, "smCause"

    #@4f
    invoke-virtual {v1, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@52
    .line 824
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@54
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@56
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@59
    .line 825
    return-void
.end method

.method private broadcastServiceStateChanged(Landroid/telephony/ServiceState;I)V
    .registers 9
    .parameter "state"
    .parameter "subscription"

    #@0
    .prologue
    .line 721
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 723
    .local v1, ident:J
    :try_start_4
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@6
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@9
    move-result v5

    #@a
    invoke-interface {v4, v5}, Lcom/android/internal/app/IBatteryStats;->notePhoneState(I)V
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_2f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_34

    #@d
    .line 727
    :goto_d
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@10
    .line 730
    new-instance v3, Landroid/content/Intent;

    #@12
    const-string v4, "android.intent.action.SERVICE_STATE"

    #@14
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17
    .line 731
    .local v3, intent:Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    #@19
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1c
    .line 732
    .local v0, data:Landroid/os/Bundle;
    invoke-virtual {p1, v0}, Landroid/telephony/ServiceState;->fillInNotifierBundle(Landroid/os/Bundle;)V

    #@1f
    .line 733
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@22
    .line 735
    const-string v4, "subscription"

    #@24
    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@27
    .line 736
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@29
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@2b
    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@2e
    .line 737
    return-void

    #@2f
    .line 727
    .end local v0           #data:Landroid/os/Bundle;
    .end local v3           #intent:Landroid/content/Intent;
    :catchall_2f
    move-exception v4

    #@30
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@33
    throw v4

    #@34
    .line 724
    :catch_34
    move-exception v4

    #@35
    goto :goto_d
.end method

.method private broadcastSignalStrengthChanged(Landroid/telephony/SignalStrength;I)V
    .registers 9
    .parameter "signalStrength"
    .parameter "subscription"

    #@0
    .prologue
    .line 740
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 742
    .local v1, ident:J
    :try_start_4
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@6
    invoke-interface {v4, p1}, Lcom/android/internal/app/IBatteryStats;->notePhoneSignalStrength(Landroid/telephony/SignalStrength;)V
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_30
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_35

    #@9
    .line 746
    :goto_9
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@c
    .line 749
    new-instance v3, Landroid/content/Intent;

    #@e
    const-string v4, "android.intent.action.SIG_STR"

    #@10
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13
    .line 750
    .local v3, intent:Landroid/content/Intent;
    const/high16 v4, 0x2000

    #@15
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@18
    .line 751
    new-instance v0, Landroid/os/Bundle;

    #@1a
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1d
    .line 752
    .local v0, data:Landroid/os/Bundle;
    invoke-virtual {p1, v0}, Landroid/telephony/SignalStrength;->fillInNotifierBundle(Landroid/os/Bundle;)V

    #@20
    .line 753
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@23
    .line 754
    const-string v4, "subscription"

    #@25
    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@28
    .line 755
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@2a
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@2c
    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@2f
    .line 756
    return-void

    #@30
    .line 746
    .end local v0           #data:Landroid/os/Bundle;
    .end local v3           #intent:Landroid/content/Intent;
    :catchall_30
    move-exception v4

    #@31
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@34
    throw v4

    #@35
    .line 743
    :catch_35
    move-exception v4

    #@36
    goto :goto_9
.end method

.method private checkListenerPermission(I)V
    .registers 5
    .parameter "events"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 848
    and-int/lit8 v0, p1, 0x10

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 849
    iget-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@7
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 854
    :cond_c
    and-int/lit16 v0, p1, 0x400

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 855
    iget-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@12
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 860
    :cond_17
    and-int/lit16 v0, p1, 0xec

    #@19
    if-eqz v0, :cond_22

    #@1b
    .line 861
    iget-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@1d
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@1f
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 864
    :cond_22
    return-void
.end method

.method private checkNotifyPermission(Ljava/lang/String;)Z
    .registers 5
    .parameter "method"

    #@0
    .prologue
    .line 837
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.MODIFY_PHONE_STATE"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_c

    #@a
    .line 839
    const/4 v1, 0x1

    #@b
    .line 844
    :goto_b
    return v1

    #@c
    .line 841
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Modify Phone State Permission Denial: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " from pid="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@24
    move-result v2

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, ", uid="

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@32
    move-result v2

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    .line 844
    .local v0, msg:Ljava/lang/String;
    const/4 v1, 0x0

    #@3c
    goto :goto_b
.end method

.method private handleRemoveListLocked()V
    .registers 4

    #@0
    .prologue
    .line 867
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    if-lez v2, :cond_23

    #@8
    .line 868
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v1

    #@e
    .local v1, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_1e

    #@14
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/os/IBinder;

    #@1a
    .line 869
    .local v0, b:Landroid/os/IBinder;
    invoke-direct {p0, v0}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@1d
    goto :goto_e

    #@1e
    .line 871
    .end local v0           #b:Landroid/os/IBinder;
    :cond_1e
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@23
    .line 873
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_23
    return-void
.end method

.method private remove(Landroid/os/IBinder;)V
    .registers 6
    .parameter "binder"

    #@0
    .prologue
    .line 366
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 367
    :try_start_3
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    .line 368
    .local v1, recordCount:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_22

    #@c
    .line 369
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@14
    iget-object v2, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@16
    if-ne v2, p1, :cond_1f

    #@18
    .line 370
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1d
    .line 371
    monitor-exit v3

    #@1e
    .line 375
    :goto_1e
    return-void

    #@1f
    .line 368
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_a

    #@22
    .line 374
    :cond_22
    monitor-exit v3

    #@23
    goto :goto_1e

    #@24
    .end local v0           #i:I
    .end local v1           #recordCount:I
    :catchall_24
    move-exception v2

    #@25
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v2
.end method

.method private validateEventsAndUserLocked(Lcom/android/server/MSimTelephonyRegistry$Record;I)Z
    .registers 8
    .parameter "r"
    .parameter "events"

    #@0
    .prologue
    .line 877
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 878
    .local v0, callingIdentity:J
    const/4 v3, 0x0

    #@5
    .line 880
    .local v3, valid:Z
    :try_start_5
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@8
    move-result v2

    #@9
    .line 881
    .local v2, foregroundUser:I
    iget v4, p1, Lcom/android/server/MSimTelephonyRegistry$Record;->callerUid:I

    #@b
    if-ne v4, v2, :cond_17

    #@d
    iget v4, p1, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_19

    #@f
    and-int/2addr v4, p2

    #@10
    if-eqz v4, :cond_17

    #@12
    const/4 v3, 0x1

    #@13
    .line 888
    :goto_13
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@16
    .line 890
    return v3

    #@17
    .line 881
    :cond_17
    const/4 v3, 0x0

    #@18
    goto :goto_13

    #@19
    .line 888
    .end local v2           #foregroundUser:I
    :catchall_19
    move-exception v4

    #@1a
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1d
    throw v4
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 11
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 682
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.DUMP"

    #@4
    invoke-virtual {v4, v5}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_33

    #@a
    .line 684
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, "Permission Denial: can\'t dump telephony.registry from from pid="

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v5

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    const-string v5, ", uid="

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v5

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 714
    :goto_32
    return-void

    #@33
    .line 688
    :cond_33
    iget-object v5, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@35
    monitor-enter v5

    #@36
    .line 689
    :try_start_36
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v3

    #@3c
    .line 690
    .local v3, recordCount:I
    const-string v4, "last known state:"

    #@3e
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@41
    .line 691
    const/4 v0, 0x0

    #@42
    .local v0, i:I
    :goto_42
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@49
    move-result v4

    #@4a
    if-ge v0, v4, :cond_172

    #@4c
    .line 692
    new-instance v4, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "  mCallState["

    #@53
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    const-string v6, "]="

    #@5d
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallState:[I

    #@63
    aget v6, v6, v0

    #@65
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@70
    .line 693
    new-instance v4, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v6, "  mCallIncomingNumber["

    #@77
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v4

    #@7b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    const-string v6, "]="

    #@81
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v4

    #@85
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallIncomingNumber:[Ljava/lang/String;

    #@87
    aget-object v6, v6, v0

    #@89
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@94
    .line 694
    new-instance v4, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v6, "  mServiceState["

    #@9b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v4

    #@a3
    const-string v6, "]="

    #@a5
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v4

    #@a9
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mServiceState:[Landroid/telephony/ServiceState;

    #@ab
    aget-object v6, v6, v0

    #@ad
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v4

    #@b1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b8
    .line 695
    new-instance v4, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v6, "  mSignalStrength["

    #@bf
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v4

    #@c3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v4

    #@c7
    const-string v6, "]="

    #@c9
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v4

    #@cd
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mSignalStrength:[Landroid/telephony/SignalStrength;

    #@cf
    aget-object v6, v6, v0

    #@d1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v4

    #@d5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v4

    #@d9
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dc
    .line 696
    new-instance v4, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v6, "  mMessageWaiting["

    #@e3
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v4

    #@e7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v4

    #@eb
    const-string v6, "]="

    #@ed
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v4

    #@f1
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mMessageWaiting:[Z

    #@f3
    aget-boolean v6, v6, v0

    #@f5
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v4

    #@f9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v4

    #@fd
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@100
    .line 697
    new-instance v4, Ljava/lang/StringBuilder;

    #@102
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@105
    const-string v6, "  mCallForwarding["

    #@107
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v4

    #@10b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v4

    #@10f
    const-string v6, "]="

    #@111
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v4

    #@115
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallForwarding:[Z

    #@117
    aget-boolean v6, v6, v0

    #@119
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v4

    #@11d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v4

    #@121
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@124
    .line 698
    new-instance v4, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v6, "  mCellLocation["

    #@12b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v4

    #@12f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@132
    move-result-object v4

    #@133
    const-string v6, "]="

    #@135
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v4

    #@139
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellLocation:[Landroid/os/Bundle;

    #@13b
    aget-object v6, v6, v0

    #@13d
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v4

    #@141
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v4

    #@145
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@148
    .line 699
    new-instance v4, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v6, "  mCellInfo["

    #@14f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v4

    #@153
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@156
    move-result-object v4

    #@157
    const-string v6, "]="

    #@159
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v4

    #@15d
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellInfo:Ljava/util/ArrayList;

    #@15f
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@162
    move-result-object v6

    #@163
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v4

    #@167
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16a
    move-result-object v4

    #@16b
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16e
    .line 691
    add-int/lit8 v0, v0, 0x1

    #@170
    goto/16 :goto_42

    #@172
    .line 701
    :cond_172
    new-instance v4, Ljava/lang/StringBuilder;

    #@174
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v6, "  mDataActivity="

    #@179
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v4

    #@17d
    iget v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataActivity:I

    #@17f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@182
    move-result-object v4

    #@183
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@186
    move-result-object v4

    #@187
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@18a
    .line 702
    new-instance v4, Ljava/lang/StringBuilder;

    #@18c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18f
    const-string v6, "  mDataConnectionState="

    #@191
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v4

    #@195
    iget v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionState:I

    #@197
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v4

    #@19b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19e
    move-result-object v4

    #@19f
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a2
    .line 703
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a7
    const-string v6, "  mDataConnectionPossible="

    #@1a9
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v4

    #@1ad
    iget-boolean v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionPossible:Z

    #@1af
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v4

    #@1b3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b6
    move-result-object v4

    #@1b7
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1ba
    .line 704
    new-instance v4, Ljava/lang/StringBuilder;

    #@1bc
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1bf
    const-string v6, "  mDataConnectionReason="

    #@1c1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v4

    #@1c5
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionReason:Ljava/lang/String;

    #@1c7
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ca
    move-result-object v4

    #@1cb
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ce
    move-result-object v4

    #@1cf
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d2
    .line 705
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d7
    const-string v6, "  mDataConnectionApn="

    #@1d9
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v4

    #@1dd
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionApn:Ljava/lang/String;

    #@1df
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v4

    #@1e3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e6
    move-result-object v4

    #@1e7
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1ea
    .line 706
    new-instance v4, Ljava/lang/StringBuilder;

    #@1ec
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1ef
    const-string v6, "  mDataConnectionLinkProperties="

    #@1f1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v4

    #@1f5
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionLinkProperties:Landroid/net/LinkProperties;

    #@1f7
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v4

    #@1fb
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fe
    move-result-object v4

    #@1ff
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@202
    .line 707
    new-instance v4, Ljava/lang/StringBuilder;

    #@204
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@207
    const-string v6, "  mDataConnectionLinkCapabilities="

    #@209
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v4

    #@20d
    iget-object v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionLinkCapabilities:Landroid/net/LinkCapabilities;

    #@20f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v4

    #@213
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@216
    move-result-object v4

    #@217
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@21a
    .line 708
    new-instance v4, Ljava/lang/StringBuilder;

    #@21c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21f
    const-string v6, "  mDataConnectionNetworkType="

    #@221
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v4

    #@225
    iget v6, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionNetworkType:I

    #@227
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v4

    #@22b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22e
    move-result-object v4

    #@22f
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@232
    .line 709
    new-instance v4, Ljava/lang/StringBuilder;

    #@234
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@237
    const-string v6, "registrations: count="

    #@239
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23c
    move-result-object v4

    #@23d
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@240
    move-result-object v4

    #@241
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@244
    move-result-object v4

    #@245
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@248
    .line 710
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@24a
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@24d
    move-result-object v1

    #@24e
    .local v1, i$:Ljava/util/Iterator;
    :goto_24e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@251
    move-result v4

    #@252
    if-eqz v4, :cond_286

    #@254
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@257
    move-result-object v2

    #@258
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@25a
    .line 711
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    new-instance v4, Ljava/lang/StringBuilder;

    #@25c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25f
    const-string v6, "  "

    #@261
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@264
    move-result-object v4

    #@265
    iget-object v6, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->pkgForDebug:Ljava/lang/String;

    #@267
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v4

    #@26b
    const-string v6, " 0x"

    #@26d
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@270
    move-result-object v4

    #@271
    iget v6, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@273
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@276
    move-result-object v6

    #@277
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v4

    #@27b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27e
    move-result-object v4

    #@27f
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@282
    goto :goto_24e

    #@283
    .line 713
    .end local v0           #i:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .end local v3           #recordCount:I
    :catchall_283
    move-exception v4

    #@284
    monitor-exit v5
    :try_end_285
    .catchall {:try_start_36 .. :try_end_285} :catchall_283

    #@285
    throw v4

    #@286
    .restart local v0       #i:I
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #recordCount:I
    :cond_286
    :try_start_286
    monitor-exit v5
    :try_end_287
    .catchall {:try_start_286 .. :try_end_287} :catchall_283

    #@287
    goto/16 :goto_32
.end method

.method public listen(Ljava/lang/String;Lcom/android/internal/telephony/IPhoneStateListener;IZI)V
    .registers 21
    .parameter "pkgForDebug"
    .parameter "callback"
    .parameter "events"
    .parameter "notifyNow"
    .parameter "subscription"

    #@0
    .prologue
    .line 233
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v3

    #@4
    .line 234
    .local v3, callerUid:I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@7
    move-result v7

    #@8
    .line 240
    .local v7, myUid:I
    if-eqz p3, :cond_16d

    #@a
    .line 242
    move/from16 v0, p3

    #@c
    invoke-direct {p0, v0}, Lcom/android/server/MSimTelephonyRegistry;->checkListenerPermission(I)V

    #@f
    .line 244
    iget-object v12, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@11
    monitor-enter v12

    #@12
    .line 246
    const/4 v8, 0x0

    #@13
    .line 248
    .local v8, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :try_start_13
    invoke-interface/range {p2 .. p2}, Lcom/android/internal/telephony/IPhoneStateListener;->asBinder()Landroid/os/IBinder;

    #@16
    move-result-object v2

    #@17
    .line 249
    .local v2, b:Landroid/os/IBinder;
    iget-object v11, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I
    :try_end_1c
    .catchall {:try_start_13 .. :try_end_1c} :catchall_113

    #@1c
    move-result v1

    #@1d
    .line 250
    .local v1, N:I
    const/4 v6, 0x0

    #@1e
    .local v6, i:I
    move-object v9, v8

    #@1f
    .end local v8           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .local v9, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :goto_1f
    if-ge v6, v1, :cond_f6

    #@21
    .line 251
    :try_start_21
    iget-object v11, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v8

    #@27
    check-cast v8, Lcom/android/server/MSimTelephonyRegistry$Record;
    :try_end_29
    .catchall {:try_start_21 .. :try_end_29} :catchall_176

    #@29
    .line 252
    .end local v9           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .restart local v8       #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :try_start_29
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@2b
    if-ne v2, v11, :cond_f1

    #@2d
    .line 265
    :goto_2d
    iget v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@2f
    xor-int v11, v11, p3

    #@31
    and-int v10, p3, v11

    #@33
    .line 266
    .local v10, send:I
    move/from16 v0, p3

    #@35
    iput v0, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I
    :try_end_37
    .catchall {:try_start_29 .. :try_end_37} :catchall_113

    #@37
    .line 267
    if-eqz p4, :cond_ef

    #@39
    .line 268
    and-int/lit8 v11, p3, 0x1

    #@3b
    if-eqz v11, :cond_4b

    #@3d
    .line 270
    :try_start_3d
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@3f
    new-instance v13, Landroid/telephony/ServiceState;

    #@41
    iget-object v14, p0, Lcom/android/server/MSimTelephonyRegistry;->mServiceState:[Landroid/telephony/ServiceState;

    #@43
    aget-object v14, v14, p5

    #@45
    invoke-direct {v13, v14}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@48
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V
    :try_end_4b
    .catchall {:try_start_3d .. :try_end_4b} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_4b} :catch_116

    #@4b
    .line 276
    :cond_4b
    :goto_4b
    and-int/lit8 v11, p3, 0x2

    #@4d
    if-eqz v11, :cond_61

    #@4f
    .line 278
    :try_start_4f
    iget-object v11, p0, Lcom/android/server/MSimTelephonyRegistry;->mSignalStrength:[Landroid/telephony/SignalStrength;

    #@51
    aget-object v11, v11, p5

    #@53
    invoke-virtual {v11}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@56
    move-result v5

    #@57
    .line 280
    .local v5, gsmSignalStrength:I
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@59
    const/16 v13, 0x63

    #@5b
    if-ne v5, v13, :cond_5e

    #@5d
    const/4 v5, -0x1

    #@5e
    .end local v5           #gsmSignalStrength:I
    :cond_5e
    invoke-interface {v11, v5}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthChanged(I)V
    :try_end_61
    .catchall {:try_start_4f .. :try_end_61} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_4f .. :try_end_61} :catch_11e

    #@61
    .line 286
    :cond_61
    :goto_61
    and-int/lit8 v11, p3, 0x4

    #@63
    if-eqz v11, :cond_6e

    #@65
    .line 288
    :try_start_65
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@67
    iget-object v13, p0, Lcom/android/server/MSimTelephonyRegistry;->mMessageWaiting:[Z

    #@69
    aget-boolean v13, v13, p5

    #@6b
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onMessageWaitingIndicatorChanged(Z)V
    :try_end_6e
    .catchall {:try_start_65 .. :try_end_6e} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_65 .. :try_end_6e} :catch_126

    #@6e
    .line 294
    :cond_6e
    :goto_6e
    and-int/lit8 v11, p3, 0x8

    #@70
    if-eqz v11, :cond_7b

    #@72
    .line 296
    :try_start_72
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@74
    iget-object v13, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallForwarding:[Z

    #@76
    aget-boolean v13, v13, p5

    #@78
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallForwardingIndicatorChanged(Z)V
    :try_end_7b
    .catchall {:try_start_72 .. :try_end_7b} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_72 .. :try_end_7b} :catch_12e

    #@7b
    .line 302
    :cond_7b
    :goto_7b
    const/16 v11, 0x10

    #@7d
    :try_start_7d
    invoke-direct {p0, v8, v11}, Lcom/android/server/MSimTelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/MSimTelephonyRegistry$Record;I)Z
    :try_end_80
    .catchall {:try_start_7d .. :try_end_80} :catchall_113

    #@80
    move-result v11

    #@81
    if-eqz v11, :cond_91

    #@83
    .line 306
    :try_start_83
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@85
    new-instance v13, Landroid/os/Bundle;

    #@87
    iget-object v14, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellLocation:[Landroid/os/Bundle;

    #@89
    aget-object v14, v14, p5

    #@8b
    invoke-direct {v13, v14}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@8e
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellLocationChanged(Landroid/os/Bundle;)V
    :try_end_91
    .catchall {:try_start_83 .. :try_end_91} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_83 .. :try_end_91} :catch_136

    #@91
    .line 312
    :cond_91
    :goto_91
    and-int/lit8 v11, p3, 0x20

    #@93
    if-eqz v11, :cond_a2

    #@95
    .line 314
    :try_start_95
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@97
    iget-object v13, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallState:[I

    #@99
    aget v13, v13, p5

    #@9b
    iget-object v14, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallIncomingNumber:[Ljava/lang/String;

    #@9d
    aget-object v14, v14, p5

    #@9f
    invoke-interface {v11, v13, v14}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V
    :try_end_a2
    .catchall {:try_start_95 .. :try_end_a2} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_95 .. :try_end_a2} :catch_13e

    #@a2
    .line 320
    :cond_a2
    :goto_a2
    and-int/lit8 v11, p3, 0x40

    #@a4
    if-eqz v11, :cond_af

    #@a6
    .line 322
    :try_start_a6
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@a8
    iget v13, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionState:I

    #@aa
    iget v14, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionNetworkType:I

    #@ac
    invoke-interface {v11, v13, v14}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataConnectionStateChanged(II)V
    :try_end_af
    .catchall {:try_start_a6 .. :try_end_af} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_a6 .. :try_end_af} :catch_146

    #@af
    .line 328
    :cond_af
    :goto_af
    move/from16 v0, p3

    #@b1
    and-int/lit16 v11, v0, 0x80

    #@b3
    if-eqz v11, :cond_bc

    #@b5
    .line 330
    :try_start_b5
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@b7
    iget v13, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataActivity:I

    #@b9
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataActivity(I)V
    :try_end_bc
    .catchall {:try_start_b5 .. :try_end_bc} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_b5 .. :try_end_bc} :catch_14e

    #@bc
    .line 335
    :cond_bc
    :goto_bc
    move/from16 v0, p3

    #@be
    and-int/lit16 v11, v0, 0x100

    #@c0
    if-eqz v11, :cond_cb

    #@c2
    .line 337
    :try_start_c2
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@c4
    iget-object v13, p0, Lcom/android/server/MSimTelephonyRegistry;->mSignalStrength:[Landroid/telephony/SignalStrength;

    #@c6
    aget-object v13, v13, p5

    #@c8
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    :try_end_cb
    .catchall {:try_start_c2 .. :try_end_cb} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_c2 .. :try_end_cb} :catch_156

    #@cb
    .line 342
    :cond_cb
    :goto_cb
    move/from16 v0, p3

    #@cd
    and-int/lit16 v11, v0, 0x200

    #@cf
    if-eqz v11, :cond_d8

    #@d1
    .line 344
    :try_start_d1
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@d3
    iget v13, p0, Lcom/android/server/MSimTelephonyRegistry;->mOtaspMode:I

    #@d5
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onOtaspChanged(I)V
    :try_end_d8
    .catchall {:try_start_d1 .. :try_end_d8} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_d1 .. :try_end_d8} :catch_15e

    #@d8
    .line 349
    :cond_d8
    :goto_d8
    const/16 v11, 0x400

    #@da
    :try_start_da
    invoke-direct {p0, v8, v11}, Lcom/android/server/MSimTelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/MSimTelephonyRegistry$Record;I)Z
    :try_end_dd
    .catchall {:try_start_da .. :try_end_dd} :catchall_113

    #@dd
    move-result v11

    #@de
    if-eqz v11, :cond_ef

    #@e0
    .line 353
    :try_start_e0
    iget-object v13, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@e2
    iget-object v11, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellInfo:Ljava/util/ArrayList;

    #@e4
    move/from16 v0, p5

    #@e6
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e9
    move-result-object v11

    #@ea
    check-cast v11, Ljava/util/List;

    #@ec
    invoke-interface {v13, v11}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellInfoChanged(Ljava/util/List;)V
    :try_end_ef
    .catchall {:try_start_e0 .. :try_end_ef} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_e0 .. :try_end_ef} :catch_166

    #@ef
    .line 359
    :cond_ef
    :goto_ef
    :try_start_ef
    monitor-exit v12
    :try_end_f0
    .catchall {:try_start_ef .. :try_end_f0} :catchall_113

    #@f0
    .line 363
    .end local v1           #N:I
    .end local v2           #b:Landroid/os/IBinder;
    .end local v6           #i:I
    .end local v8           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .end local v10           #send:I
    :goto_f0
    return-void

    #@f1
    .line 250
    .restart local v1       #N:I
    .restart local v2       #b:Landroid/os/IBinder;
    .restart local v6       #i:I
    .restart local v8       #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :cond_f1
    add-int/lit8 v6, v6, 0x1

    #@f3
    move-object v9, v8

    #@f4
    .end local v8           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .restart local v9       #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    goto/16 :goto_1f

    #@f6
    .line 256
    :cond_f6
    :try_start_f6
    new-instance v8, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@f8
    const/4 v11, 0x0

    #@f9
    invoke-direct {v8, v11}, Lcom/android/server/MSimTelephonyRegistry$Record;-><init>(Lcom/android/server/MSimTelephonyRegistry$1;)V
    :try_end_fc
    .catchall {:try_start_f6 .. :try_end_fc} :catchall_176

    #@fc
    .line 257
    .end local v9           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .restart local v8       #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :try_start_fc
    iput-object v2, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@fe
    .line 258
    move-object/from16 v0, p2

    #@100
    iput-object v0, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@102
    .line 259
    move-object/from16 v0, p1

    #@104
    iput-object v0, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->pkgForDebug:Ljava/lang/String;

    #@106
    .line 260
    iput v3, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->callerUid:I

    #@108
    .line 261
    move/from16 v0, p5

    #@10a
    iput v0, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I

    #@10c
    .line 262
    iget-object v11, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10e
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@111
    goto/16 :goto_2d

    #@113
    .line 359
    .end local v1           #N:I
    .end local v2           #b:Landroid/os/IBinder;
    .end local v6           #i:I
    :catchall_113
    move-exception v11

    #@114
    :goto_114
    monitor-exit v12
    :try_end_115
    .catchall {:try_start_fc .. :try_end_115} :catchall_113

    #@115
    throw v11

    #@116
    .line 272
    .restart local v1       #N:I
    .restart local v2       #b:Landroid/os/IBinder;
    .restart local v6       #i:I
    .restart local v10       #send:I
    :catch_116
    move-exception v4

    #@117
    .line 273
    .local v4, ex:Landroid/os/RemoteException;
    :try_start_117
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@119
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@11c
    goto/16 :goto_4b

    #@11e
    .line 282
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_11e
    move-exception v4

    #@11f
    .line 283
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@121
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@124
    goto/16 :goto_61

    #@126
    .line 290
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_126
    move-exception v4

    #@127
    .line 291
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@129
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@12c
    goto/16 :goto_6e

    #@12e
    .line 298
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_12e
    move-exception v4

    #@12f
    .line 299
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@131
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@134
    goto/16 :goto_7b

    #@136
    .line 308
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_136
    move-exception v4

    #@137
    .line 309
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@139
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@13c
    goto/16 :goto_91

    #@13e
    .line 316
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_13e
    move-exception v4

    #@13f
    .line 317
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@141
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@144
    goto/16 :goto_a2

    #@146
    .line 324
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_146
    move-exception v4

    #@147
    .line 325
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@149
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@14c
    goto/16 :goto_af

    #@14e
    .line 331
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_14e
    move-exception v4

    #@14f
    .line 332
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@151
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@154
    goto/16 :goto_bc

    #@156
    .line 338
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_156
    move-exception v4

    #@157
    .line 339
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@159
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@15c
    goto/16 :goto_cb

    #@15e
    .line 345
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_15e
    move-exception v4

    #@15f
    .line 346
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@161
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@164
    goto/16 :goto_d8

    #@166
    .line 354
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_166
    move-exception v4

    #@167
    .line 355
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@169
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V
    :try_end_16c
    .catchall {:try_start_117 .. :try_end_16c} :catchall_113

    #@16c
    goto :goto_ef

    #@16d
    .line 361
    .end local v1           #N:I
    .end local v2           #b:Landroid/os/IBinder;
    .end local v4           #ex:Landroid/os/RemoteException;
    .end local v6           #i:I
    .end local v8           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .end local v10           #send:I
    :cond_16d
    invoke-interface/range {p2 .. p2}, Lcom/android/internal/telephony/IPhoneStateListener;->asBinder()Landroid/os/IBinder;

    #@170
    move-result-object v11

    #@171
    invoke-direct {p0, v11}, Lcom/android/server/MSimTelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@174
    goto/16 :goto_f0

    #@176
    .line 359
    .restart local v1       #N:I
    .restart local v2       #b:Landroid/os/IBinder;
    .restart local v6       #i:I
    .restart local v9       #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_176
    move-exception v11

    #@177
    move-object v8, v9

    #@178
    .end local v9           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    .restart local v8       #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    goto :goto_114
.end method

.method public notifyCallForwardingChanged(ZI)V
    .registers 9
    .parameter "cfi"
    .parameter "subscription"

    #@0
    .prologue
    .line 497
    const-string v3, "notifyCallForwardingChanged()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 514
    :goto_8
    return-void

    #@9
    .line 500
    :cond_9
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 501
    :try_start_c
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallForwarding:[Z

    #@e
    aput-boolean p1, v3, p2

    #@10
    .line 502
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_3e

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@22
    .line 503
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@24
    and-int/lit8 v3, v3, 0x8

    #@26
    if-eqz v3, :cond_16

    #@28
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_2a
    .catchall {:try_start_c .. :try_end_2a} :catchall_3b

    #@2a
    if-ne v3, p2, :cond_16

    #@2c
    .line 506
    :try_start_2c
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@2e
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallForwardingIndicatorChanged(Z)V
    :try_end_31
    .catchall {:try_start_2c .. :try_end_31} :catchall_3b
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_31} :catch_32

    #@31
    goto :goto_16

    #@32
    .line 507
    :catch_32
    move-exception v0

    #@33
    .line 508
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_33
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@35
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@37
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a
    goto :goto_16

    #@3b
    .line 513
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_3b
    move-exception v3

    #@3c
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_33 .. :try_end_3d} :catchall_3b

    #@3d
    throw v3

    #@3e
    .line 512
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_3e
    :try_start_3e
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@41
    .line 513
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_3b

    #@42
    goto :goto_8
.end method

.method public notifyCallState(ILjava/lang/String;I)V
    .registers 10
    .parameter "state"
    .parameter "incomingNumber"
    .parameter "subscription"

    #@0
    .prologue
    .line 378
    const-string v3, "notifyCallState()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 397
    :goto_8
    return-void

    #@9
    .line 381
    :cond_9
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 382
    :try_start_c
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallState:[I

    #@e
    aput p1, v3, p3

    #@10
    .line 383
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCallIncomingNumber:[Ljava/lang/String;

    #@12
    aput-object p2, v3, p3

    #@14
    .line 384
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v1

    #@1a
    .local v1, i$:Ljava/util/Iterator;
    :cond_1a
    :goto_1a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_42

    #@20
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@26
    .line 385
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@28
    and-int/lit8 v3, v3, 0x20

    #@2a
    if-eqz v3, :cond_1a

    #@2c
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_2e
    .catchall {:try_start_c .. :try_end_2e} :catchall_3f

    #@2e
    if-ne v3, p3, :cond_1a

    #@30
    .line 388
    :try_start_30
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@32
    invoke-interface {v3, p1, p2}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V
    :try_end_35
    .catchall {:try_start_30 .. :try_end_35} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_35} :catch_36

    #@35
    goto :goto_1a

    #@36
    .line 389
    :catch_36
    move-exception v0

    #@37
    .line 390
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_37
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@39
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@3b
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3e
    goto :goto_1a

    #@3f
    .line 395
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_3f
    move-exception v3

    #@40
    monitor-exit v4
    :try_end_41
    .catchall {:try_start_37 .. :try_end_41} :catchall_3f

    #@41
    throw v3

    #@42
    .line 394
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_42
    :try_start_42
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@45
    .line 395
    monitor-exit v4
    :try_end_46
    .catchall {:try_start_42 .. :try_end_46} :catchall_3f

    #@46
    .line 396
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/MSimTelephonyRegistry;->broadcastCallStateChanged(ILjava/lang/String;I)V

    #@49
    goto :goto_8
.end method

.method public notifyCellInfo(Ljava/util/List;I)V
    .registers 9
    .parameter
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 453
    .local p1, cellInfo:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    const-string v3, "notifyCellInfo()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 474
    :goto_8
    return-void

    #@9
    .line 457
    :cond_9
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 458
    :try_start_c
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellInfo:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v3, p2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 459
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v1

    #@17
    .local v1, i$:Ljava/util/Iterator;
    :cond_17
    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_41

    #@1d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v2

    #@21
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@23
    .line 460
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    const/16 v3, 0x400

    #@25
    invoke-direct {p0, v2, v3}, Lcom/android/server/MSimTelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/MSimTelephonyRegistry$Record;I)Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_17

    #@2b
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_2d
    .catchall {:try_start_c .. :try_end_2d} :catchall_3e

    #@2d
    if-ne v3, p2, :cond_17

    #@2f
    .line 466
    :try_start_2f
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@31
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellInfoChanged(Ljava/util/List;)V
    :try_end_34
    .catchall {:try_start_2f .. :try_end_34} :catchall_3e
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_34} :catch_35

    #@34
    goto :goto_17

    #@35
    .line 467
    :catch_35
    move-exception v0

    #@36
    .line 468
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_36
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@38
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@3a
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d
    goto :goto_17

    #@3e
    .line 473
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_3e
    move-exception v3

    #@3f
    monitor-exit v4
    :try_end_40
    .catchall {:try_start_36 .. :try_end_40} :catchall_3e

    #@40
    throw v3

    #@41
    .line 472
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_41
    :try_start_41
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@44
    .line 473
    monitor-exit v4
    :try_end_45
    .catchall {:try_start_41 .. :try_end_45} :catchall_3e

    #@45
    goto :goto_8
.end method

.method public notifyCellLocation(Landroid/os/Bundle;I)V
    .registers 9
    .parameter "cellLocation"
    .parameter "subscription"

    #@0
    .prologue
    .line 637
    const-string v3, "notifyCellLocation()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 659
    :goto_8
    return-void

    #@9
    .line 640
    :cond_9
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 641
    :try_start_c
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mCellLocation:[Landroid/os/Bundle;

    #@e
    aput-object p1, v3, p2

    #@10
    .line 642
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_45

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@22
    .line 643
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    const/16 v3, 0x10

    #@24
    invoke-direct {p0, v2, v3}, Lcom/android/server/MSimTelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/MSimTelephonyRegistry$Record;I)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_16

    #@2a
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_2c
    .catchall {:try_start_c .. :try_end_2c} :catchall_42

    #@2c
    if-ne v3, p2, :cond_16

    #@2e
    .line 650
    :try_start_2e
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@30
    new-instance v5, Landroid/os/Bundle;

    #@32
    invoke-direct {v5, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@35
    invoke-interface {v3, v5}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellLocationChanged(Landroid/os/Bundle;)V
    :try_end_38
    .catchall {:try_start_2e .. :try_end_38} :catchall_42
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_38} :catch_39

    #@38
    goto :goto_16

    #@39
    .line 651
    :catch_39
    move-exception v0

    #@3a
    .line 652
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_3a
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@3c
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@3e
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@41
    goto :goto_16

    #@42
    .line 658
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_42
    move-exception v3

    #@43
    monitor-exit v4
    :try_end_44
    .catchall {:try_start_3a .. :try_end_44} :catchall_42

    #@44
    throw v3

    #@45
    .line 657
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_45
    :try_start_45
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@48
    .line 658
    monitor-exit v4
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_42

    #@49
    goto :goto_8
.end method

.method public notifyDataActivity(I)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    .line 517
    const-string v3, "notifyDataActivity()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 533
    :goto_8
    return-void

    #@9
    .line 520
    :cond_9
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 521
    :try_start_c
    iput p1, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataActivity:I

    #@e
    .line 522
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_38

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@20
    .line 523
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_35

    #@22
    and-int/lit16 v3, v3, 0x80

    #@24
    if-eqz v3, :cond_14

    #@26
    .line 525
    :try_start_26
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataActivity(I)V
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_35
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_14

    #@2c
    .line 526
    :catch_2c
    move-exception v0

    #@2d
    .line 527
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2d
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2f
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@31
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_14

    #@35
    .line 532
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 531
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_38
    :try_start_38
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@3b
    .line 532
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_35

    #@3c
    goto :goto_8
.end method

.method public notifyDataConnection(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;IZI)V
    .registers 26
    .parameter "state"
    .parameter "isDataConnectivityPossible"
    .parameter "reason"
    .parameter "apn"
    .parameter "apnType"
    .parameter "linkProperties"
    .parameter "linkCapabilities"
    .parameter "networkType"
    .parameter "roaming"
    .parameter "smCause"

    #@0
    .prologue
    .line 545
    const-string v1, "notifyDataConnection()"

    #@2
    invoke-direct {p0, v1}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 613
    :goto_8
    return-void

    #@9
    .line 557
    :cond_9
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v2

    #@c
    .line 558
    const/4 v13, 0x0

    #@d
    .line 559
    .local v13, modified:Z
    const/4 v1, 0x2

    #@e
    move/from16 v0, p1

    #@10
    if-ne v0, v1, :cond_79

    #@12
    .line 560
    :try_start_12
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@14
    move-object/from16 v0, p5

    #@16
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_2e

    #@1c
    .line 561
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@1e
    move-object/from16 v0, p5

    #@20
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 562
    iget v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionState:I

    #@25
    move/from16 v0, p1

    #@27
    if-eq v1, v0, :cond_2e

    #@29
    .line 563
    move/from16 v0, p1

    #@2b
    iput v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionState:I

    #@2d
    .line 564
    const/4 v13, 0x1

    #@2e
    .line 578
    :cond_2e
    :goto_2e
    move/from16 v0, p2

    #@30
    iput-boolean v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionPossible:Z

    #@32
    .line 579
    move-object/from16 v0, p3

    #@34
    iput-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionReason:Ljava/lang/String;

    #@36
    .line 580
    move-object/from16 v0, p6

    #@38
    iput-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionLinkProperties:Landroid/net/LinkProperties;

    #@3a
    .line 581
    move-object/from16 v0, p7

    #@3c
    iput-object v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionLinkCapabilities:Landroid/net/LinkCapabilities;

    #@3e
    .line 582
    iget v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionNetworkType:I

    #@40
    move/from16 v0, p8

    #@42
    if-eq v1, v0, :cond_49

    #@44
    .line 583
    move/from16 v0, p8

    #@46
    iput v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionNetworkType:I

    #@48
    .line 585
    const/4 v13, 0x1

    #@49
    .line 587
    :cond_49
    if-eqz v13, :cond_94

    #@4b
    .line 592
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@50
    move-result-object v12

    #@51
    .local v12, i$:Ljava/util/Iterator;
    :cond_51
    :goto_51
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@54
    move-result v1

    #@55
    if-eqz v1, :cond_91

    #@57
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5a
    move-result-object v14

    #@5b
    check-cast v14, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@5d
    .line 593
    .local v14, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v1, v14, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I
    :try_end_5f
    .catchall {:try_start_12 .. :try_end_5f} :catchall_76

    #@5f
    and-int/lit8 v1, v1, 0x40

    #@61
    if-eqz v1, :cond_51

    #@63
    .line 595
    :try_start_63
    iget-object v1, v14, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@65
    iget v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionState:I

    #@67
    iget v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionNetworkType:I

    #@69
    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataConnectionStateChanged(II)V
    :try_end_6c
    .catchall {:try_start_63 .. :try_end_6c} :catchall_76
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_6c} :catch_6d

    #@6c
    goto :goto_51

    #@6d
    .line 597
    :catch_6d
    move-exception v11

    #@6e
    .line 598
    .local v11, ex:Landroid/os/RemoteException;
    :try_start_6e
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@70
    iget-object v3, v14, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@72
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    goto :goto_51

    #@76
    .line 604
    .end local v11           #ex:Landroid/os/RemoteException;
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v14           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_76
    move-exception v1

    #@77
    monitor-exit v2
    :try_end_78
    .catchall {:try_start_6e .. :try_end_78} :catchall_76

    #@78
    throw v1

    #@79
    .line 568
    :cond_79
    :try_start_79
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@7b
    move-object/from16 v0, p5

    #@7d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@80
    move-result v1

    #@81
    if-eqz v1, :cond_2e

    #@83
    .line 569
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@85
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@88
    move-result v1

    #@89
    if-eqz v1, :cond_2e

    #@8b
    .line 570
    move/from16 v0, p1

    #@8d
    iput v0, p0, Lcom/android/server/MSimTelephonyRegistry;->mDataConnectionState:I

    #@8f
    .line 571
    const/4 v13, 0x1

    #@90
    goto :goto_2e

    #@91
    .line 602
    .restart local v12       #i$:Ljava/util/Iterator;
    :cond_91
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@94
    .line 604
    .end local v12           #i$:Ljava/util/Iterator;
    :cond_94
    monitor-exit v2
    :try_end_95
    .catchall {:try_start_79 .. :try_end_95} :catchall_76

    #@95
    move-object v1, p0

    #@96
    move/from16 v2, p1

    #@98
    move/from16 v3, p2

    #@9a
    move-object/from16 v4, p3

    #@9c
    move-object/from16 v5, p4

    #@9e
    move-object/from16 v6, p5

    #@a0
    move-object/from16 v7, p6

    #@a2
    move-object/from16 v8, p7

    #@a4
    move/from16 v9, p9

    #@a6
    move/from16 v10, p10

    #@a8
    .line 606
    invoke-direct/range {v1 .. v10}, Lcom/android/server/MSimTelephonyRegistry;->broadcastDataConnectionStateChanged(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;ZI)V

    #@ab
    goto/16 :goto_8
.end method

.method public notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "reason"
    .parameter "apnType"

    #@0
    .prologue
    .line 616
    const-string v0, "notifyDataConnectionFailed()"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 634
    :goto_8
    return-void

    #@9
    .line 633
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/android/server/MSimTelephonyRegistry;->broadcastDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    goto :goto_8
.end method

.method public notifyMessageWaitingChanged(ZI)V
    .registers 9
    .parameter "mwi"
    .parameter "subscription"

    #@0
    .prologue
    .line 477
    const-string v3, "notifyMessageWaitingChanged()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 494
    :goto_8
    return-void

    #@9
    .line 480
    :cond_9
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 481
    :try_start_c
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mMessageWaiting:[Z

    #@e
    aput-boolean p1, v3, p2

    #@10
    .line 482
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_3e

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@22
    .line 483
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@24
    and-int/lit8 v3, v3, 0x4

    #@26
    if-eqz v3, :cond_16

    #@28
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_2a
    .catchall {:try_start_c .. :try_end_2a} :catchall_3b

    #@2a
    if-ne v3, p2, :cond_16

    #@2c
    .line 486
    :try_start_2c
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@2e
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onMessageWaitingIndicatorChanged(Z)V
    :try_end_31
    .catchall {:try_start_2c .. :try_end_31} :catchall_3b
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_31} :catch_32

    #@31
    goto :goto_16

    #@32
    .line 487
    :catch_32
    move-exception v0

    #@33
    .line 488
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_33
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@35
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@37
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a
    goto :goto_16

    #@3b
    .line 493
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_3b
    move-exception v3

    #@3c
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_33 .. :try_end_3d} :catchall_3b

    #@3d
    throw v3

    #@3e
    .line 492
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_3e
    :try_start_3e
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@41
    .line 493
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_3b

    #@42
    goto :goto_8
.end method

.method public notifyOtaspChanged(I)V
    .registers 8
    .parameter "otaspMode"

    #@0
    .prologue
    .line 662
    const-string v3, "notifyOtaspChanged()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 678
    :goto_8
    return-void

    #@9
    .line 665
    :cond_9
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 666
    :try_start_c
    iput p1, p0, Lcom/android/server/MSimTelephonyRegistry;->mOtaspMode:I

    #@e
    .line 667
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_38

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@20
    .line 668
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_35

    #@22
    and-int/lit16 v3, v3, 0x200

    #@24
    if-eqz v3, :cond_14

    #@26
    .line 670
    :try_start_26
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onOtaspChanged(I)V
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_35
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_14

    #@2c
    .line 671
    :catch_2c
    move-exception v0

    #@2d
    .line 672
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2d
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2f
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@31
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_14

    #@35
    .line 677
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 676
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_38
    :try_start_38
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@3b
    .line 677
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_35

    #@3c
    goto :goto_8
.end method

.method public notifyServiceState(Landroid/telephony/ServiceState;I)V
    .registers 9
    .parameter "state"
    .parameter "subscription"

    #@0
    .prologue
    .line 400
    const-string v3, "notifyServiceState()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 419
    :goto_8
    return-void

    #@9
    .line 403
    :cond_9
    const-string v3, "MSimTelephonyRegistry"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "notifyServiceState: "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 404
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@23
    monitor-enter v4

    #@24
    .line 405
    :try_start_24
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mServiceState:[Landroid/telephony/ServiceState;

    #@26
    aput-object p1, v3, p2

    #@28
    .line 406
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@2d
    move-result-object v1

    #@2e
    .local v1, i$:Ljava/util/Iterator;
    :cond_2e
    :goto_2e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_5b

    #@34
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@37
    move-result-object v2

    #@38
    check-cast v2, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@3a
    .line 407
    .local v2, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@3c
    and-int/lit8 v3, v3, 0x1

    #@3e
    if-eqz v3, :cond_2e

    #@40
    iget v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_42
    .catchall {:try_start_24 .. :try_end_42} :catchall_58

    #@42
    if-ne v3, p2, :cond_2e

    #@44
    .line 410
    :try_start_44
    iget-object v3, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@46
    new-instance v5, Landroid/telephony/ServiceState;

    #@48
    invoke-direct {v5, p1}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@4b
    invoke-interface {v3, v5}, Lcom/android/internal/telephony/IPhoneStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V
    :try_end_4e
    .catchall {:try_start_44 .. :try_end_4e} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_2e

    #@4f
    .line 411
    :catch_4f
    move-exception v0

    #@50
    .line 412
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_50
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@52
    iget-object v5, v2, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@54
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    goto :goto_2e

    #@58
    .line 417
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_58
    move-exception v3

    #@59
    monitor-exit v4
    :try_end_5a
    .catchall {:try_start_50 .. :try_end_5a} :catchall_58

    #@5a
    throw v3

    #@5b
    .line 416
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_5b
    :try_start_5b
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@5e
    .line 417
    monitor-exit v4
    :try_end_5f
    .catchall {:try_start_5b .. :try_end_5f} :catchall_58

    #@5f
    .line 418
    invoke-direct {p0, p1, p2}, Lcom/android/server/MSimTelephonyRegistry;->broadcastServiceStateChanged(Landroid/telephony/ServiceState;I)V

    #@62
    goto :goto_8
.end method

.method public notifySignalStrength(Landroid/telephony/SignalStrength;I)V
    .registers 10
    .parameter "signalStrength"
    .parameter "subscription"

    #@0
    .prologue
    .line 422
    const-string v4, "notifySignalStrength()"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/MSimTelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v4

    #@6
    if-nez v4, :cond_9

    #@8
    .line 450
    :goto_8
    return-void

    #@9
    .line 425
    :cond_9
    iget-object v5, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v5

    #@c
    .line 426
    :try_start_c
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mSignalStrength:[Landroid/telephony/SignalStrength;

    #@e
    aput-object p1, v4, p2

    #@10
    .line 427
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v2

    #@16
    .local v2, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_64

    #@1c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Lcom/android/server/MSimTelephonyRegistry$Record;

    #@22
    .line 428
    .local v3, r:Lcom/android/server/MSimTelephonyRegistry$Record;
    iget v4, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@24
    and-int/lit16 v4, v4, 0x100

    #@26
    if-eqz v4, :cond_36

    #@28
    iget v4, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_2a
    .catchall {:try_start_c .. :try_end_2a} :catchall_58

    #@2a
    if-ne v4, p2, :cond_36

    #@2c
    .line 431
    :try_start_2c
    iget-object v4, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@2e
    new-instance v6, Landroid/telephony/SignalStrength;

    #@30
    invoke-direct {v6, p1}, Landroid/telephony/SignalStrength;-><init>(Landroid/telephony/SignalStrength;)V

    #@33
    invoke-interface {v4, v6}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    :try_end_36
    .catchall {:try_start_2c .. :try_end_36} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_36} :catch_5b

    #@36
    .line 436
    :cond_36
    :goto_36
    :try_start_36
    iget v4, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->events:I

    #@38
    and-int/lit8 v4, v4, 0x2

    #@3a
    if-eqz v4, :cond_16

    #@3c
    iget v4, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->subscription:I
    :try_end_3e
    .catchall {:try_start_36 .. :try_end_3e} :catchall_58

    #@3e
    if-ne v4, p2, :cond_16

    #@40
    .line 439
    :try_start_40
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@43
    move-result v1

    #@44
    .line 440
    .local v1, gsmSignalStrength:I
    iget-object v4, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@46
    const/16 v6, 0x63

    #@48
    if-ne v1, v6, :cond_4b

    #@4a
    const/4 v1, -0x1

    #@4b
    .end local v1           #gsmSignalStrength:I
    :cond_4b
    invoke-interface {v4, v1}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthChanged(I)V
    :try_end_4e
    .catchall {:try_start_40 .. :try_end_4e} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_40 .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_16

    #@4f
    .line 442
    :catch_4f
    move-exception v0

    #@50
    .line 443
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_50
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@52
    iget-object v6, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@54
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    goto :goto_16

    #@58
    .line 448
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catchall_58
    move-exception v4

    #@59
    monitor-exit v5
    :try_end_5a
    .catchall {:try_start_50 .. :try_end_5a} :catchall_58

    #@5a
    throw v4

    #@5b
    .line 432
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :catch_5b
    move-exception v0

    #@5c
    .line 433
    .restart local v0       #ex:Landroid/os/RemoteException;
    :try_start_5c
    iget-object v4, p0, Lcom/android/server/MSimTelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@5e
    iget-object v6, v3, Lcom/android/server/MSimTelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@60
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@63
    goto :goto_36

    #@64
    .line 447
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v3           #r:Lcom/android/server/MSimTelephonyRegistry$Record;
    :cond_64
    invoke-direct {p0}, Lcom/android/server/MSimTelephonyRegistry;->handleRemoveListLocked()V

    #@67
    .line 448
    monitor-exit v5
    :try_end_68
    .catchall {:try_start_5c .. :try_end_68} :catchall_58

    #@68
    .line 449
    invoke-direct {p0, p1, p2}, Lcom/android/server/MSimTelephonyRegistry;->broadcastSignalStrengthChanged(Landroid/telephony/SignalStrength;I)V

    #@6b
    goto :goto_8
.end method

.method public systemReady()V
    .registers 4

    #@0
    .prologue
    .line 224
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 225
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_SWITCHED"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 226
    const-string v1, "android.intent.action.USER_REMOVED"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 227
    iget-object v1, p0, Lcom/android/server/MSimTelephonyRegistry;->mContext:Landroid/content/Context;

    #@11
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@13
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@16
    .line 228
    return-void
.end method
