.class Lcom/android/server/BackupManagerService$PerformFullRestoreTask;
.super Ljava/lang/Object;
.source "BackupManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerformFullRestoreTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;,
        Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;,
        Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreFileRunnable;
    }
.end annotation


# instance fields
.field mAgent:Landroid/app/IBackupAgent;

.field mAgentPackage:Ljava/lang/String;

.field mBytes:J

.field final mClearedPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mCurrentPassword:Ljava/lang/String;

.field mDecryptPassword:Ljava/lang/String;

.field final mDeleteObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;

.field mInputFile:Landroid/os/ParcelFileDescriptor;

.field final mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

.field mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final mManifestSignatures:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Landroid/content/pm/Signature;",
            ">;"
        }
    .end annotation
.end field

.field mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

.field final mPackageInstallers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mPackagePolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/BackupManagerService$RestorePolicy;",
            ">;"
        }
    .end annotation
.end field

.field mPipes:[Landroid/os/ParcelFileDescriptor;

.field mTargetApp:Landroid/content/pm/ApplicationInfo;

.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .registers 9
    .parameter
    .parameter "fd"
    .parameter "curPassword"
    .parameter "decryptPassword"
    .parameter "observer"
    .parameter "latch"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2931
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 2912
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@8
    .line 2917
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@f
    .line 2921
    new-instance v0, Ljava/util/HashMap;

    #@11
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackageInstallers:Ljava/util/HashMap;

    #@16
    .line 2924
    new-instance v0, Ljava/util/HashMap;

    #@18
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mManifestSignatures:Ljava/util/HashMap;

    #@1d
    .line 2928
    new-instance v0, Ljava/util/HashSet;

    #@1f
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@22
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mClearedPackages:Ljava/util/HashSet;

    #@24
    .line 3531
    new-instance v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@26
    invoke-direct {v0, p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;-><init>(Lcom/android/server/BackupManagerService$PerformFullRestoreTask;)V

    #@29
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@2b
    .line 3532
    new-instance v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;

    #@2d
    invoke-direct {v0, p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;-><init>(Lcom/android/server/BackupManagerService$PerformFullRestoreTask;)V

    #@30
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDeleteObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;

    #@32
    .line 2932
    iput-object p2, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInputFile:Landroid/os/ParcelFileDescriptor;

    #@34
    .line 2933
    iput-object p3, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mCurrentPassword:Ljava/lang/String;

    #@36
    .line 2934
    iput-object p4, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDecryptPassword:Ljava/lang/String;

    #@38
    .line 2935
    iput-object p5, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@3a
    .line 2936
    iput-object p6, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3c
    .line 2937
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@3e
    .line 2938
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgentPackage:Ljava/lang/String;

    #@40
    .line 2939
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@42
    .line 2943
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mClearedPackages:Ljava/util/HashSet;

    #@44
    const-string v1, "android"

    #@46
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@49
    .line 2944
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mClearedPackages:Ljava/util/HashSet;

    #@4b
    const-string v1, "com.android.providers.settings"

    #@4d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@50
    .line 2946
    return-void
.end method

.method private HEXLOG([B)V
    .registers 13
    .parameter "block"

    #@0
    .prologue
    const/16 v5, 0x10

    #@2
    const/4 v10, 0x1

    #@3
    const/4 v9, 0x0

    #@4
    .line 3929
    const/4 v3, 0x0

    #@5
    .line 3930
    .local v3, offset:I
    array-length v4, p1

    #@6
    .line 3931
    .local v4, todo:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    const/16 v6, 0x40

    #@a
    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 3932
    .local v0, buf:Ljava/lang/StringBuilder;
    :goto_d
    if-lez v4, :cond_4f

    #@f
    .line 3933
    const-string v6, "%04x   "

    #@11
    new-array v7, v10, [Ljava/lang/Object;

    #@13
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v8

    #@17
    aput-object v8, v7, v9

    #@19
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 3934
    if-le v4, v5, :cond_3e

    #@22
    move v2, v5

    #@23
    .line 3935
    .local v2, numThisLine:I
    :goto_23
    const/4 v1, 0x0

    #@24
    .local v1, i:I
    :goto_24
    if-ge v1, v2, :cond_40

    #@26
    .line 3936
    const-string v6, "%02x "

    #@28
    new-array v7, v10, [Ljava/lang/Object;

    #@2a
    add-int v8, v3, v1

    #@2c
    aget-byte v8, p1, v8

    #@2e
    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@31
    move-result-object v8

    #@32
    aput-object v8, v7, v9

    #@34
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    .line 3935
    add-int/lit8 v1, v1, 0x1

    #@3d
    goto :goto_24

    #@3e
    .end local v1           #i:I
    .end local v2           #numThisLine:I
    :cond_3e
    move v2, v4

    #@3f
    .line 3934
    goto :goto_23

    #@40
    .line 3938
    .restart local v1       #i:I
    .restart local v2       #numThisLine:I
    :cond_40
    const-string v6, "hexdump"

    #@42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 3939
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    #@4c
    .line 3940
    sub-int/2addr v4, v2

    #@4d
    .line 3941
    add-int/2addr v3, v2

    #@4e
    .line 3942
    goto :goto_d

    #@4f
    .line 3943
    .end local v1           #i:I
    .end local v2           #numThisLine:I
    :cond_4f
    return-void
.end method


# virtual methods
.method decodeAesHeaderAndInitialize(Ljava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;
    .registers 33
    .parameter "encryptionName"
    .parameter "rawInStream"

    #@0
    .prologue
    .line 3087
    const/16 v19, 0x0

    #@2
    .line 3089
    .local v19, result:Ljava/io/InputStream;
    :try_start_2
    const-string v26, "AES-256"

    #@4
    move-object/from16 v0, p1

    #@6
    move-object/from16 v1, v26

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v26

    #@c
    if-eqz v26, :cond_129

    #@e
    .line 3091
    move-object/from16 v0, p0

    #@10
    move-object/from16 v1, p2

    #@12
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@15
    move-result-object v25

    #@16
    .line 3092
    .local v25, userSaltHex:Ljava/lang/String;
    move-object/from16 v0, p0

    #@18
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1a
    move-object/from16 v26, v0

    #@1c
    move-object/from16 v0, v26

    #@1e
    move-object/from16 v1, v25

    #@20
    invoke-static {v0, v1}, Lcom/android/server/BackupManagerService;->access$1600(Lcom/android/server/BackupManagerService;Ljava/lang/String;)[B

    #@23
    move-result-object v24

    #@24
    .line 3094
    .local v24, userSalt:[B
    move-object/from16 v0, p0

    #@26
    move-object/from16 v1, p2

    #@28
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    .line 3095
    .local v8, ckSaltHex:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@30
    move-object/from16 v26, v0

    #@32
    move-object/from16 v0, v26

    #@34
    invoke-static {v0, v8}, Lcom/android/server/BackupManagerService;->access$1600(Lcom/android/server/BackupManagerService;Ljava/lang/String;)[B

    #@37
    move-result-object v7

    #@38
    .line 3097
    .local v7, ckSalt:[B
    move-object/from16 v0, p0

    #@3a
    move-object/from16 v1, p2

    #@3c
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@3f
    move-result-object v26

    #@40
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@43
    move-result v21

    #@44
    .line 3098
    .local v21, rounds:I
    move-object/from16 v0, p0

    #@46
    move-object/from16 v1, p2

    #@48
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@4b
    move-result-object v22

    #@4c
    .line 3100
    .local v22, userIvHex:Ljava/lang/String;
    move-object/from16 v0, p0

    #@4e
    move-object/from16 v1, p2

    #@50
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@53
    move-result-object v12

    #@54
    .line 3103
    .local v12, masterKeyBlobHex:Ljava/lang/String;
    const-string v26, "AES/CBC/PKCS5Padding"

    #@56
    invoke-static/range {v26 .. v26}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    #@59
    move-result-object v5

    #@5a
    .line 3104
    .local v5, c:Ljavax/crypto/Cipher;
    move-object/from16 v0, p0

    #@5c
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@5e
    move-object/from16 v26, v0

    #@60
    move-object/from16 v0, p0

    #@62
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDecryptPassword:Ljava/lang/String;

    #@64
    move-object/from16 v27, v0

    #@66
    move-object/from16 v0, v26

    #@68
    move-object/from16 v1, v27

    #@6a
    move-object/from16 v2, v24

    #@6c
    move/from16 v3, v21

    #@6e
    invoke-static {v0, v1, v2, v3}, Lcom/android/server/BackupManagerService;->access$1200(Lcom/android/server/BackupManagerService;Ljava/lang/String;[BI)Ljavax/crypto/SecretKey;

    #@71
    move-result-object v23

    #@72
    .line 3106
    .local v23, userKey:Ljavax/crypto/SecretKey;
    move-object/from16 v0, p0

    #@74
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@76
    move-object/from16 v26, v0

    #@78
    move-object/from16 v0, v26

    #@7a
    move-object/from16 v1, v22

    #@7c
    invoke-static {v0, v1}, Lcom/android/server/BackupManagerService;->access$1600(Lcom/android/server/BackupManagerService;Ljava/lang/String;)[B

    #@7f
    move-result-object v4

    #@80
    .line 3107
    .local v4, IV:[B
    new-instance v10, Ljavax/crypto/spec/IvParameterSpec;

    #@82
    invoke-direct {v10, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    #@85
    .line 3108
    .local v10, ivSpec:Ljavax/crypto/spec/IvParameterSpec;
    const/16 v26, 0x2

    #@87
    new-instance v27, Ljavax/crypto/spec/SecretKeySpec;

    #@89
    invoke-interface/range {v23 .. v23}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@8c
    move-result-object v28

    #@8d
    const-string v29, "AES"

    #@8f
    invoke-direct/range {v27 .. v29}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    #@92
    move/from16 v0, v26

    #@94
    move-object/from16 v1, v27

    #@96
    invoke-virtual {v5, v0, v1, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    #@99
    .line 3111
    move-object/from16 v0, p0

    #@9b
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@9d
    move-object/from16 v26, v0

    #@9f
    move-object/from16 v0, v26

    #@a1
    invoke-static {v0, v12}, Lcom/android/server/BackupManagerService;->access$1600(Lcom/android/server/BackupManagerService;Ljava/lang/String;)[B

    #@a4
    move-result-object v16

    #@a5
    .line 3112
    .local v16, mkCipher:[B
    move-object/from16 v0, v16

    #@a7
    invoke-virtual {v5, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    #@aa
    move-result-object v14

    #@ab
    .line 3115
    .local v14, mkBlob:[B
    const/16 v17, 0x0

    #@ad
    .line 3116
    .local v17, offset:I
    add-int/lit8 v18, v17, 0x1

    #@af
    .end local v17           #offset:I
    .local v18, offset:I
    aget-byte v11, v14, v17

    #@b1
    .line 3117
    .local v11, len:I
    add-int/lit8 v26, v11, 0x1

    #@b3
    move/from16 v0, v18

    #@b5
    move/from16 v1, v26

    #@b7
    invoke-static {v14, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@ba
    move-result-object v4

    #@bb
    .line 3118
    add-int/lit8 v17, v11, 0x1

    #@bd
    .line 3120
    .end local v18           #offset:I
    .restart local v17       #offset:I
    add-int/lit8 v18, v17, 0x1

    #@bf
    .end local v17           #offset:I
    .restart local v18       #offset:I
    aget-byte v11, v14, v17

    #@c1
    .line 3121
    add-int v26, v18, v11

    #@c3
    move/from16 v0, v18

    #@c5
    move/from16 v1, v26

    #@c7
    invoke-static {v14, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@ca
    move-result-object v13

    #@cb
    .line 3123
    .local v13, mk:[B
    add-int v17, v18, v11

    #@cd
    .line 3125
    .end local v18           #offset:I
    .restart local v17       #offset:I
    add-int/lit8 v18, v17, 0x1

    #@cf
    .end local v17           #offset:I
    .restart local v18       #offset:I
    aget-byte v11, v14, v17

    #@d1
    .line 3126
    add-int v26, v18, v11

    #@d3
    move/from16 v0, v18

    #@d5
    move/from16 v1, v26

    #@d7
    invoke-static {v14, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@da
    move-result-object v15

    #@db
    .line 3130
    .local v15, mkChecksum:[B
    move-object/from16 v0, p0

    #@dd
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@df
    move-object/from16 v26, v0

    #@e1
    move-object/from16 v0, v26

    #@e3
    move/from16 v1, v21

    #@e5
    invoke-static {v0, v13, v7, v1}, Lcom/android/server/BackupManagerService;->access$1500(Lcom/android/server/BackupManagerService;[B[BI)[B

    #@e8
    move-result-object v6

    #@e9
    .line 3131
    .local v6, calculatedCk:[B
    invoke-static {v6, v15}, Ljava/util/Arrays;->equals([B[B)Z

    #@ec
    move-result v26

    #@ed
    if-eqz v26, :cond_114

    #@ef
    .line 3132
    new-instance v10, Ljavax/crypto/spec/IvParameterSpec;

    #@f1
    .end local v10           #ivSpec:Ljavax/crypto/spec/IvParameterSpec;
    invoke-direct {v10, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    #@f4
    .line 3133
    .restart local v10       #ivSpec:Ljavax/crypto/spec/IvParameterSpec;
    const/16 v26, 0x2

    #@f6
    new-instance v27, Ljavax/crypto/spec/SecretKeySpec;

    #@f8
    const-string v28, "AES"

    #@fa
    move-object/from16 v0, v27

    #@fc
    move-object/from16 v1, v28

    #@fe
    invoke-direct {v0, v13, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    #@101
    move/from16 v0, v26

    #@103
    move-object/from16 v1, v27

    #@105
    invoke-virtual {v5, v0, v1, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    #@108
    .line 3137
    new-instance v20, Ljavax/crypto/CipherInputStream;

    #@10a
    move-object/from16 v0, v20

    #@10c
    move-object/from16 v1, p2

    #@10e
    invoke-direct {v0, v1, v5}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    #@111
    .end local v19           #result:Ljava/io/InputStream;
    .local v20, result:Ljava/io/InputStream;
    move-object/from16 v19, v20

    #@113
    .line 3162
    .end local v4           #IV:[B
    .end local v5           #c:Ljavax/crypto/Cipher;
    .end local v6           #calculatedCk:[B
    .end local v7           #ckSalt:[B
    .end local v8           #ckSaltHex:Ljava/lang/String;
    .end local v10           #ivSpec:Ljavax/crypto/spec/IvParameterSpec;
    .end local v11           #len:I
    .end local v12           #masterKeyBlobHex:Ljava/lang/String;
    .end local v13           #mk:[B
    .end local v14           #mkBlob:[B
    .end local v15           #mkChecksum:[B
    .end local v16           #mkCipher:[B
    .end local v18           #offset:I
    .end local v20           #result:Ljava/io/InputStream;
    .end local v21           #rounds:I
    .end local v22           #userIvHex:Ljava/lang/String;
    .end local v23           #userKey:Ljavax/crypto/SecretKey;
    .end local v24           #userSalt:[B
    .end local v25           #userSaltHex:Ljava/lang/String;
    .restart local v19       #result:Ljava/io/InputStream;
    :goto_113
    return-object v19

    #@114
    .line 3138
    .restart local v4       #IV:[B
    .restart local v5       #c:Ljavax/crypto/Cipher;
    .restart local v6       #calculatedCk:[B
    .restart local v7       #ckSalt:[B
    .restart local v8       #ckSaltHex:Ljava/lang/String;
    .restart local v10       #ivSpec:Ljavax/crypto/spec/IvParameterSpec;
    .restart local v11       #len:I
    .restart local v12       #masterKeyBlobHex:Ljava/lang/String;
    .restart local v13       #mk:[B
    .restart local v14       #mkBlob:[B
    .restart local v15       #mkChecksum:[B
    .restart local v16       #mkCipher:[B
    .restart local v18       #offset:I
    .restart local v21       #rounds:I
    .restart local v22       #userIvHex:Ljava/lang/String;
    .restart local v23       #userKey:Ljavax/crypto/SecretKey;
    .restart local v24       #userSalt:[B
    .restart local v25       #userSaltHex:Ljava/lang/String;
    :cond_114
    const-string v26, "BackupManagerService"

    #@116
    const-string v27, "Incorrect password"

    #@118
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11b
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_2 .. :try_end_11b} :catch_11c
    .catch Ljavax/crypto/BadPaddingException; {:try_start_2 .. :try_end_11b} :catch_146
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_2 .. :try_end_11b} :catch_14f
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_11b} :catch_158
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_11b} :catch_161
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_11b} :catch_16a
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_11b} :catch_173
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_11b} :catch_17c

    #@11b
    goto :goto_113

    #@11c
    .line 3140
    .end local v4           #IV:[B
    .end local v5           #c:Ljavax/crypto/Cipher;
    .end local v6           #calculatedCk:[B
    .end local v7           #ckSalt:[B
    .end local v8           #ckSaltHex:Ljava/lang/String;
    .end local v10           #ivSpec:Ljavax/crypto/spec/IvParameterSpec;
    .end local v11           #len:I
    .end local v12           #masterKeyBlobHex:Ljava/lang/String;
    .end local v13           #mk:[B
    .end local v14           #mkBlob:[B
    .end local v15           #mkChecksum:[B
    .end local v16           #mkCipher:[B
    .end local v18           #offset:I
    .end local v21           #rounds:I
    .end local v22           #userIvHex:Ljava/lang/String;
    .end local v23           #userKey:Ljavax/crypto/SecretKey;
    .end local v24           #userSalt:[B
    .end local v25           #userSaltHex:Ljava/lang/String;
    :catch_11c
    move-exception v9

    #@11d
    .line 3141
    .local v9, e:Ljava/security/InvalidAlgorithmParameterException;
    const-string v26, "BackupManagerService"

    #@11f
    const-string v27, "Needed parameter spec unavailable!"

    #@121
    move-object/from16 v0, v26

    #@123
    move-object/from16 v1, v27

    #@125
    invoke-static {v0, v1, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@128
    goto :goto_113

    #@129
    .line 3139
    .end local v9           #e:Ljava/security/InvalidAlgorithmParameterException;
    :cond_129
    :try_start_129
    const-string v26, "BackupManagerService"

    #@12b
    new-instance v27, Ljava/lang/StringBuilder;

    #@12d
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@130
    const-string v28, "Unsupported encryption method: "

    #@132
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v27

    #@136
    move-object/from16 v0, v27

    #@138
    move-object/from16 v1, p1

    #@13a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v27

    #@13e
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v27

    #@142
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_145
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_129 .. :try_end_145} :catch_11c
    .catch Ljavax/crypto/BadPaddingException; {:try_start_129 .. :try_end_145} :catch_146
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_129 .. :try_end_145} :catch_14f
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_129 .. :try_end_145} :catch_158
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_129 .. :try_end_145} :catch_161
    .catch Ljava/security/InvalidKeyException; {:try_start_129 .. :try_end_145} :catch_16a
    .catch Ljava/lang/NumberFormatException; {:try_start_129 .. :try_end_145} :catch_173
    .catch Ljava/io/IOException; {:try_start_129 .. :try_end_145} :catch_17c

    #@145
    goto :goto_113

    #@146
    .line 3142
    :catch_146
    move-exception v9

    #@147
    .line 3147
    .local v9, e:Ljavax/crypto/BadPaddingException;
    const-string v26, "BackupManagerService"

    #@149
    const-string v27, "Incorrect password"

    #@14b
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14e
    goto :goto_113

    #@14f
    .line 3148
    .end local v9           #e:Ljavax/crypto/BadPaddingException;
    :catch_14f
    move-exception v9

    #@150
    .line 3149
    .local v9, e:Ljavax/crypto/IllegalBlockSizeException;
    const-string v26, "BackupManagerService"

    #@152
    const-string v27, "Invalid block size in master key"

    #@154
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@157
    goto :goto_113

    #@158
    .line 3150
    .end local v9           #e:Ljavax/crypto/IllegalBlockSizeException;
    :catch_158
    move-exception v9

    #@159
    .line 3151
    .local v9, e:Ljava/security/NoSuchAlgorithmException;
    const-string v26, "BackupManagerService"

    #@15b
    const-string v27, "Needed decryption algorithm unavailable!"

    #@15d
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    goto :goto_113

    #@161
    .line 3152
    .end local v9           #e:Ljava/security/NoSuchAlgorithmException;
    :catch_161
    move-exception v9

    #@162
    .line 3153
    .local v9, e:Ljavax/crypto/NoSuchPaddingException;
    const-string v26, "BackupManagerService"

    #@164
    const-string v27, "Needed padding mechanism unavailable!"

    #@166
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@169
    goto :goto_113

    #@16a
    .line 3154
    .end local v9           #e:Ljavax/crypto/NoSuchPaddingException;
    :catch_16a
    move-exception v9

    #@16b
    .line 3155
    .local v9, e:Ljava/security/InvalidKeyException;
    const-string v26, "BackupManagerService"

    #@16d
    const-string v27, "Illegal password; aborting"

    #@16f
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    goto :goto_113

    #@173
    .line 3156
    .end local v9           #e:Ljava/security/InvalidKeyException;
    :catch_173
    move-exception v9

    #@174
    .line 3157
    .local v9, e:Ljava/lang/NumberFormatException;
    const-string v26, "BackupManagerService"

    #@176
    const-string v27, "Can\'t parse restore data header"

    #@178
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    goto :goto_113

    #@17c
    .line 3158
    .end local v9           #e:Ljava/lang/NumberFormatException;
    :catch_17c
    move-exception v9

    #@17d
    .line 3159
    .local v9, e:Ljava/io/IOException;
    const-string v26, "BackupManagerService"

    #@17f
    const-string v27, "Can\'t read input header"

    #@181
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@184
    goto :goto_113
.end method

.method dumpFileMetadata(Lcom/android/server/BackupManagerService$FileMetadata;)V
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 3817
    return-void
.end method

.method extractLine([BI[Ljava/lang/String;)I
    .registers 10
    .parameter "buffer"
    .parameter "offset"
    .parameter "outStr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3772
    array-length v1, p1

    #@1
    .line 3773
    .local v1, end:I
    if-lt p2, v1, :cond_b

    #@3
    new-instance v3, Ljava/io/IOException;

    #@5
    const-string v4, "Incomplete data"

    #@7
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 3776
    :cond_b
    move v2, p2

    #@c
    .local v2, pos:I
    :goto_c
    if-ge v2, v1, :cond_14

    #@e
    .line 3777
    aget-byte v0, p1, v2

    #@10
    .line 3780
    .local v0, c:B
    const/16 v3, 0xa

    #@12
    if-ne v0, v3, :cond_21

    #@14
    .line 3784
    .end local v0           #c:B
    :cond_14
    const/4 v3, 0x0

    #@15
    new-instance v4, Ljava/lang/String;

    #@17
    sub-int v5, v2, p2

    #@19
    invoke-direct {v4, p1, p2, v5}, Ljava/lang/String;-><init>([BII)V

    #@1c
    aput-object v4, p3, v3

    #@1e
    .line 3785
    add-int/lit8 v2, v2, 0x1

    #@20
    .line 3786
    return v2

    #@21
    .line 3776
    .restart local v0       #c:B
    :cond_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_c
.end method

.method extractRadix([BIII)J
    .registers 14
    .parameter "data"
    .parameter "offset"
    .parameter "maxChars"
    .parameter "radix"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 4031
    const-wide/16 v3, 0x0

    #@2
    .line 4032
    .local v3, value:J
    add-int v1, p2, p3

    #@4
    .line 4033
    .local v1, end:I
    move v2, p2

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v1, :cond_f

    #@7
    .line 4034
    aget-byte v0, p1, v2

    #@9
    .line 4036
    .local v0, b:B
    if-eqz v0, :cond_f

    #@b
    const/16 v5, 0x20

    #@d
    if-ne v0, v5, :cond_10

    #@f
    .line 4042
    .end local v0           #b:B
    :cond_f
    return-wide v3

    #@10
    .line 4037
    .restart local v0       #b:B
    :cond_10
    const/16 v5, 0x30

    #@12
    if-lt v0, v5, :cond_1a

    #@14
    add-int/lit8 v5, p4, 0x30

    #@16
    add-int/lit8 v5, v5, -0x1

    #@18
    if-le v0, v5, :cond_3e

    #@1a
    .line 4038
    :cond_1a
    new-instance v5, Ljava/io/IOException;

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "Invalid number in header: \'"

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    int-to-char v7, v0

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    const-string v7, "\' for radix "

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v5

    #@3e
    .line 4040
    :cond_3e
    int-to-long v5, p4

    #@3f
    mul-long/2addr v5, v3

    #@40
    add-int/lit8 v7, v0, -0x30

    #@42
    int-to-long v7, v7

    #@43
    add-long v3, v5, v7

    #@45
    .line 4033
    add-int/lit8 v2, v2, 0x1

    #@47
    goto :goto_5
.end method

.method extractString([BII)Ljava/lang/String;
    .registers 9
    .parameter "data"
    .parameter "offset"
    .parameter "maxChars"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 4046
    add-int v0, p2, p3

    #@2
    .line 4047
    .local v0, end:I
    move v1, p2

    #@3
    .line 4049
    .local v1, eos:I
    :goto_3
    if-ge v1, v0, :cond_c

    #@5
    aget-byte v2, p1, v1

    #@7
    if-eqz v2, :cond_c

    #@9
    add-int/lit8 v1, v1, 0x1

    #@b
    goto :goto_3

    #@c
    .line 4050
    :cond_c
    new-instance v2, Ljava/lang/String;

    #@e
    sub-int v3, v1, p2

    #@10
    const-string v4, "US-ASCII"

    #@12
    invoke-direct {v2, p1, p2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@15
    return-object v2
.end method

.method installApk(Lcom/android/server/BackupManagerService$FileMetadata;Ljava/lang/String;Ljava/io/InputStream;)Z
    .registers 26
    .parameter "info"
    .parameter "installerPackage"
    .parameter "instream"

    #@0
    .prologue
    .line 3535
    const/4 v9, 0x1

    #@1
    .line 3541
    .local v9, okay:Z
    new-instance v4, Ljava/io/File;

    #@3
    move-object/from16 v0, p0

    #@5
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@7
    move-object/from16 v18, v0

    #@9
    move-object/from16 v0, v18

    #@b
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mDataDir:Ljava/io/File;

    #@d
    move-object/from16 v18, v0

    #@f
    move-object/from16 v0, p1

    #@11
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@13
    move-object/from16 v19, v0

    #@15
    move-object/from16 v0, v18

    #@17
    move-object/from16 v1, v19

    #@19
    invoke-direct {v4, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1c
    .line 3543
    .local v4, apkFile:Ljava/io/File;
    :try_start_1c
    new-instance v5, Ljava/io/FileOutputStream;

    #@1e
    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@21
    .line 3544
    .local v5, apkStream:Ljava/io/FileOutputStream;
    const v18, 0x8000

    #@24
    move/from16 v0, v18

    #@26
    new-array v6, v0, [B

    #@28
    .line 3545
    .local v6, buffer:[B
    move-object/from16 v0, p1

    #@2a
    iget-wide v13, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@2c
    .line 3546
    .local v13, size:J
    :goto_2c
    const-wide/16 v18, 0x0

    #@2e
    cmp-long v18, v13, v18

    #@30
    if-lez v18, :cond_75

    #@32
    .line 3547
    array-length v0, v6

    #@33
    move/from16 v18, v0

    #@35
    move/from16 v0, v18

    #@37
    int-to-long v0, v0

    #@38
    move-wide/from16 v18, v0

    #@3a
    cmp-long v18, v18, v13

    #@3c
    if-gez v18, :cond_73

    #@3e
    array-length v0, v6

    #@3f
    move/from16 v18, v0

    #@41
    move/from16 v0, v18

    #@43
    int-to-long v15, v0

    #@44
    .line 3548
    .local v15, toRead:J
    :goto_44
    const/16 v18, 0x0

    #@46
    long-to-int v0, v15

    #@47
    move/from16 v19, v0

    #@49
    move-object/from16 v0, p3

    #@4b
    move/from16 v1, v18

    #@4d
    move/from16 v2, v19

    #@4f
    invoke-virtual {v0, v6, v1, v2}, Ljava/io/InputStream;->read([BII)I

    #@52
    move-result v7

    #@53
    .line 3549
    .local v7, didRead:I
    if-ltz v7, :cond_66

    #@55
    move-object/from16 v0, p0

    #@57
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@59
    move-wide/from16 v18, v0

    #@5b
    int-to-long v0, v7

    #@5c
    move-wide/from16 v20, v0

    #@5e
    add-long v18, v18, v20

    #@60
    move-wide/from16 v0, v18

    #@62
    move-object/from16 v2, p0

    #@64
    iput-wide v0, v2, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@66
    .line 3550
    :cond_66
    const/16 v18, 0x0

    #@68
    move/from16 v0, v18

    #@6a
    invoke-virtual {v5, v6, v0, v7}, Ljava/io/FileOutputStream;->write([BII)V

    #@6d
    .line 3551
    int-to-long v0, v7

    #@6e
    move-wide/from16 v18, v0

    #@70
    sub-long v13, v13, v18

    #@72
    .line 3552
    goto :goto_2c

    #@73
    .end local v7           #didRead:I
    .end local v15           #toRead:J
    :cond_73
    move-wide v15, v13

    #@74
    .line 3547
    goto :goto_44

    #@75
    .line 3553
    :cond_75
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    #@78
    .line 3556
    const/16 v18, 0x1

    #@7a
    const/16 v19, 0x0

    #@7c
    move/from16 v0, v18

    #@7e
    move/from16 v1, v19

    #@80
    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    #@83
    .line 3559
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@86
    move-result-object v10

    #@87
    .line 3560
    .local v10, packageUri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@89
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@8b
    move-object/from16 v18, v0

    #@8d
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;->reset()V

    #@90
    .line 3561
    move-object/from16 v0, p0

    #@92
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@94
    move-object/from16 v18, v0

    #@96
    invoke-static/range {v18 .. v18}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@99
    move-result-object v18

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@9e
    move-object/from16 v19, v0

    #@a0
    const/16 v20, 0x22

    #@a2
    move-object/from16 v0, v18

    #@a4
    move-object/from16 v1, v19

    #@a6
    move/from16 v2, v20

    #@a8
    move-object/from16 v3, p2

    #@aa
    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    #@ad
    .line 3564
    move-object/from16 v0, p0

    #@af
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@b1
    move-object/from16 v18, v0

    #@b3
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;->waitForCompletion()V

    #@b6
    .line 3566
    move-object/from16 v0, p0

    #@b8
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@ba
    move-object/from16 v18, v0

    #@bc
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;->getResult()I

    #@bf
    move-result v18

    #@c0
    const/16 v19, 0x1

    #@c2
    move/from16 v0, v18

    #@c4
    move/from16 v1, v19

    #@c6
    if-eq v0, v1, :cond_e5

    #@c8
    .line 3570
    move-object/from16 v0, p0

    #@ca
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@cc
    move-object/from16 v18, v0

    #@ce
    move-object/from16 v0, p1

    #@d0
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@d2
    move-object/from16 v19, v0

    #@d4
    invoke-virtual/range {v18 .. v19}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d7
    move-result-object v18

    #@d8
    sget-object v19, Lcom/android/server/BackupManagerService$RestorePolicy;->ACCEPT:Lcom/android/server/BackupManagerService$RestorePolicy;
    :try_end_da
    .catchall {:try_start_1c .. :try_end_da} :catchall_27c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_da} :catch_16d

    #@da
    move-object/from16 v0, v18

    #@dc
    move-object/from16 v1, v19

    #@de
    if-eq v0, v1, :cond_e1

    #@e0
    .line 3571
    const/4 v9, 0x0

    #@e1
    .line 3630
    .end local v5           #apkStream:Ljava/io/FileOutputStream;
    .end local v6           #buffer:[B
    .end local v10           #packageUri:Landroid/net/Uri;
    .end local v13           #size:J
    :cond_e1
    :goto_e1
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@e4
    .line 3633
    return v9

    #@e5
    .line 3575
    .restart local v5       #apkStream:Ljava/io/FileOutputStream;
    .restart local v6       #buffer:[B
    .restart local v10       #packageUri:Landroid/net/Uri;
    .restart local v13       #size:J
    :cond_e5
    const/16 v17, 0x0

    #@e7
    .line 3576
    .local v17, uninstall:Z
    :try_start_e7
    move-object/from16 v0, p0

    #@e9
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@eb
    move-object/from16 v18, v0

    #@ed
    move-object/from16 v0, v18

    #@ef
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;->mPackageName:Ljava/lang/String;

    #@f1
    move-object/from16 v18, v0

    #@f3
    move-object/from16 v0, p1

    #@f5
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@f7
    move-object/from16 v19, v0

    #@f9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fc
    move-result v18

    #@fd
    if-nez v18, :cond_178

    #@ff
    .line 3577
    const-string v18, "BackupManagerService"

    #@101
    new-instance v19, Ljava/lang/StringBuilder;

    #@103
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@106
    const-string v20, "Restore stream claimed to include apk for "

    #@108
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v19

    #@10c
    move-object/from16 v0, p1

    #@10e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@110
    move-object/from16 v20, v0

    #@112
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v19

    #@116
    const-string v20, " but apk was really "

    #@118
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v19

    #@11c
    move-object/from16 v0, p0

    #@11e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@120
    move-object/from16 v20, v0

    #@122
    move-object/from16 v0, v20

    #@124
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;->mPackageName:Ljava/lang/String;

    #@126
    move-object/from16 v20, v0

    #@128
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v19

    #@12c
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12f
    move-result-object v19

    #@130
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 3581
    const/4 v9, 0x0

    #@134
    .line 3582
    const/16 v17, 0x1

    #@136
    .line 3619
    :cond_136
    :goto_136
    if-eqz v17, :cond_e1

    #@138
    .line 3620
    move-object/from16 v0, p0

    #@13a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDeleteObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;

    #@13c
    move-object/from16 v18, v0

    #@13e
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;->reset()V

    #@141
    .line 3621
    move-object/from16 v0, p0

    #@143
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@145
    move-object/from16 v18, v0

    #@147
    invoke-static/range {v18 .. v18}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@14a
    move-result-object v18

    #@14b
    move-object/from16 v0, p0

    #@14d
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInstallObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;

    #@14f
    move-object/from16 v19, v0

    #@151
    move-object/from16 v0, v19

    #@153
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreInstallObserver;->mPackageName:Ljava/lang/String;

    #@155
    move-object/from16 v19, v0

    #@157
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDeleteObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;

    #@15b
    move-object/from16 v20, v0

    #@15d
    const/16 v21, 0x0

    #@15f
    invoke-virtual/range {v18 .. v21}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    #@162
    .line 3623
    move-object/from16 v0, p0

    #@164
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDeleteObserver:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;

    #@166
    move-object/from16 v18, v0

    #@168
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreDeleteObserver;->waitForCompletion()V
    :try_end_16b
    .catchall {:try_start_e7 .. :try_end_16b} :catchall_27c
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_16b} :catch_16d

    #@16b
    goto/16 :goto_e1

    #@16d
    .line 3626
    .end local v5           #apkStream:Ljava/io/FileOutputStream;
    .end local v6           #buffer:[B
    .end local v10           #packageUri:Landroid/net/Uri;
    .end local v13           #size:J
    .end local v17           #uninstall:Z
    :catch_16d
    move-exception v8

    #@16e
    .line 3627
    .local v8, e:Ljava/io/IOException;
    :try_start_16e
    const-string v18, "BackupManagerService"

    #@170
    const-string v19, "Unable to transcribe restored apk for install"

    #@172
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_175
    .catchall {:try_start_16e .. :try_end_175} :catchall_27c

    #@175
    .line 3628
    const/4 v9, 0x0

    #@176
    goto/16 :goto_e1

    #@178
    .line 3585
    .end local v8           #e:Ljava/io/IOException;
    .restart local v5       #apkStream:Ljava/io/FileOutputStream;
    .restart local v6       #buffer:[B
    .restart local v10       #packageUri:Landroid/net/Uri;
    .restart local v13       #size:J
    .restart local v17       #uninstall:Z
    :cond_178
    :try_start_178
    move-object/from16 v0, p0

    #@17a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@17c
    move-object/from16 v18, v0

    #@17e
    invoke-static/range {v18 .. v18}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@181
    move-result-object v18

    #@182
    move-object/from16 v0, p1

    #@184
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@186
    move-object/from16 v19, v0

    #@188
    const/16 v20, 0x40

    #@18a
    invoke-virtual/range {v18 .. v20}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@18d
    move-result-object v11

    #@18e
    .line 3587
    .local v11, pkg:Landroid/content/pm/PackageInfo;
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@190
    move-object/from16 v18, v0

    #@192
    move-object/from16 v0, v18

    #@194
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@196
    move/from16 v18, v0

    #@198
    const v19, 0x8000

    #@19b
    and-int v18, v18, v19

    #@19d
    if-nez v18, :cond_1c6

    #@19f
    .line 3588
    const-string v18, "BackupManagerService"

    #@1a1
    new-instance v19, Ljava/lang/StringBuilder;

    #@1a3
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1a6
    const-string v20, "Restore stream contains apk of package "

    #@1a8
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v19

    #@1ac
    move-object/from16 v0, p1

    #@1ae
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@1b0
    move-object/from16 v20, v0

    #@1b2
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v19

    #@1b6
    const-string v20, " but it disallows backup/restore"

    #@1b8
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v19

    #@1bc
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bf
    move-result-object v19

    #@1c0
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c3
    .line 3590
    const/4 v9, 0x0

    #@1c4
    goto/16 :goto_136

    #@1c6
    .line 3593
    :cond_1c6
    move-object/from16 v0, p0

    #@1c8
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mManifestSignatures:Ljava/util/HashMap;

    #@1ca
    move-object/from16 v18, v0

    #@1cc
    move-object/from16 v0, p1

    #@1ce
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@1d0
    move-object/from16 v19, v0

    #@1d2
    invoke-virtual/range {v18 .. v19}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d5
    move-result-object v12

    #@1d6
    check-cast v12, [Landroid/content/pm/Signature;

    #@1d8
    .line 3594
    .local v12, sigs:[Landroid/content/pm/Signature;
    move-object/from16 v0, p0

    #@1da
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1dc
    move-object/from16 v18, v0

    #@1de
    move-object/from16 v0, v18

    #@1e0
    invoke-static {v0, v12, v11}, Lcom/android/server/BackupManagerService;->access$1700(Lcom/android/server/BackupManagerService;[Landroid/content/pm/Signature;Landroid/content/pm/PackageInfo;)Z

    #@1e3
    move-result v18

    #@1e4
    if-eqz v18, :cond_22b

    #@1e6
    .line 3597
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1e8
    move-object/from16 v18, v0

    #@1ea
    move-object/from16 v0, v18

    #@1ec
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1ee
    move/from16 v18, v0

    #@1f0
    const/16 v19, 0x2710

    #@1f2
    move/from16 v0, v18

    #@1f4
    move/from16 v1, v19

    #@1f6
    if-ge v0, v1, :cond_136

    #@1f8
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1fa
    move-object/from16 v18, v0

    #@1fc
    move-object/from16 v0, v18

    #@1fe
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@200
    move-object/from16 v18, v0

    #@202
    if-nez v18, :cond_136

    #@204
    .line 3599
    const-string v18, "BackupManagerService"

    #@206
    new-instance v19, Ljava/lang/StringBuilder;

    #@208
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@20b
    const-string v20, "Installed app "

    #@20d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v19

    #@211
    move-object/from16 v0, p1

    #@213
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@215
    move-object/from16 v20, v0

    #@217
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v19

    #@21b
    const-string v20, " has restricted uid and no agent"

    #@21d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v19

    #@221
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@224
    move-result-object v19

    #@225
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@228
    .line 3601
    const/4 v9, 0x0

    #@229
    goto/16 :goto_136

    #@22b
    .line 3604
    :cond_22b
    const-string v18, "BackupManagerService"

    #@22d
    new-instance v19, Ljava/lang/StringBuilder;

    #@22f
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@232
    const-string v20, "Installed app "

    #@234
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@237
    move-result-object v19

    #@238
    move-object/from16 v0, p1

    #@23a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@23c
    move-object/from16 v20, v0

    #@23e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@241
    move-result-object v19

    #@242
    const-string v20, " signatures do not match restore manifest"

    #@244
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@247
    move-result-object v19

    #@248
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24b
    move-result-object v19

    #@24c
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_24f
    .catchall {:try_start_178 .. :try_end_24f} :catchall_27c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_178 .. :try_end_24f} :catch_254
    .catch Ljava/io/IOException; {:try_start_178 .. :try_end_24f} :catch_16d

    #@24f
    .line 3606
    const/4 v9, 0x0

    #@250
    .line 3607
    const/16 v17, 0x1

    #@252
    goto/16 :goto_136

    #@254
    .line 3610
    .end local v11           #pkg:Landroid/content/pm/PackageInfo;
    .end local v12           #sigs:[Landroid/content/pm/Signature;
    :catch_254
    move-exception v8

    #@255
    .line 3611
    .local v8, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_255
    const-string v18, "BackupManagerService"

    #@257
    new-instance v19, Ljava/lang/StringBuilder;

    #@259
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@25c
    const-string v20, "Install of package "

    #@25e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@261
    move-result-object v19

    #@262
    move-object/from16 v0, p1

    #@264
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@266
    move-object/from16 v20, v0

    #@268
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26b
    move-result-object v19

    #@26c
    const-string v20, " succeeded but now not found"

    #@26e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@271
    move-result-object v19

    #@272
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@275
    move-result-object v19

    #@276
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_279
    .catchall {:try_start_255 .. :try_end_279} :catchall_27c
    .catch Ljava/io/IOException; {:try_start_255 .. :try_end_279} :catch_16d

    #@279
    .line 3613
    const/4 v9, 0x0

    #@27a
    goto/16 :goto_136

    #@27c
    .line 3630
    .end local v5           #apkStream:Ljava/io/FileOutputStream;
    .end local v6           #buffer:[B
    .end local v8           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v10           #packageUri:Landroid/net/Uri;
    .end local v13           #size:J
    .end local v17           #uninstall:Z
    :catchall_27c
    move-exception v18

    #@27d
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@280
    throw v18
.end method

.method readAppManifest(Lcom/android/server/BackupManagerService$FileMetadata;Ljava/io/InputStream;)Lcom/android/server/BackupManagerService$RestorePolicy;
    .registers 25
    .parameter "info"
    .parameter "instream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3653
    move-object/from16 v0, p1

    #@2
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@4
    move-wide/from16 v18, v0

    #@6
    const-wide/32 v20, 0x10000

    #@9
    cmp-long v18, v18, v20

    #@b
    if-lez v18, :cond_2c

    #@d
    .line 3654
    new-instance v18, Ljava/io/IOException;

    #@f
    new-instance v19, Ljava/lang/StringBuilder;

    #@11
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v20, "Restore manifest too big; corrupt? size="

    #@16
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v19

    #@1a
    move-object/from16 v0, p1

    #@1c
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@1e
    move-wide/from16 v20, v0

    #@20
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@23
    move-result-object v19

    #@24
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v19

    #@28
    invoke-direct/range {v18 .. v19}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v18

    #@2c
    .line 3657
    :cond_2c
    move-object/from16 v0, p1

    #@2e
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@30
    move-wide/from16 v18, v0

    #@32
    move-wide/from16 v0, v18

    #@34
    long-to-int v0, v0

    #@35
    move/from16 v18, v0

    #@37
    move/from16 v0, v18

    #@39
    new-array v4, v0, [B

    #@3b
    .line 3658
    .local v4, buffer:[B
    const/16 v18, 0x0

    #@3d
    move-object/from16 v0, p1

    #@3f
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@41
    move-wide/from16 v19, v0

    #@43
    move-wide/from16 v0, v19

    #@45
    long-to-int v0, v0

    #@46
    move/from16 v19, v0

    #@48
    move-object/from16 v0, p0

    #@4a
    move-object/from16 v1, p2

    #@4c
    move/from16 v2, v18

    #@4e
    move/from16 v3, v19

    #@50
    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readExactly(Ljava/io/InputStream;[BII)I

    #@53
    move-result v18

    #@54
    move/from16 v0, v18

    #@56
    int-to-long v0, v0

    #@57
    move-wide/from16 v18, v0

    #@59
    move-object/from16 v0, p1

    #@5b
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@5d
    move-wide/from16 v20, v0

    #@5f
    cmp-long v18, v18, v20

    #@61
    if-nez v18, :cond_12f

    #@63
    .line 3659
    move-object/from16 v0, p0

    #@65
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@67
    move-wide/from16 v18, v0

    #@69
    move-object/from16 v0, p1

    #@6b
    iget-wide v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@6d
    move-wide/from16 v20, v0

    #@6f
    add-long v18, v18, v20

    #@71
    move-wide/from16 v0, v18

    #@73
    move-object/from16 v2, p0

    #@75
    iput-wide v0, v2, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@77
    .line 3662
    sget-object v14, Lcom/android/server/BackupManagerService$RestorePolicy;->IGNORE:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@79
    .line 3663
    .local v14, policy:Lcom/android/server/BackupManagerService$RestorePolicy;
    const/16 v18, 0x1

    #@7b
    move/from16 v0, v18

    #@7d
    new-array v0, v0, [Ljava/lang/String;

    #@7f
    move-object/from16 v16, v0

    #@81
    .line 3664
    .local v16, str:[Ljava/lang/String;
    const/4 v11, 0x0

    #@82
    .line 3667
    .local v11, offset:I
    :try_start_82
    move-object/from16 v0, p0

    #@84
    move-object/from16 v1, v16

    #@86
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@89
    move-result v11

    #@8a
    .line 3668
    const/16 v18, 0x0

    #@8c
    aget-object v18, v16, v18

    #@8e
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@91
    move-result v17

    #@92
    .line 3669
    .local v17, version:I
    const/16 v18, 0x1

    #@94
    move/from16 v0, v17

    #@96
    move/from16 v1, v18

    #@98
    if-ne v0, v1, :cond_2d4

    #@9a
    .line 3670
    move-object/from16 v0, p0

    #@9c
    move-object/from16 v1, v16

    #@9e
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@a1
    move-result v11

    #@a2
    .line 3671
    const/16 v18, 0x0

    #@a4
    aget-object v9, v16, v18

    #@a6
    .line 3673
    .local v9, manifestPackage:Ljava/lang/String;
    move-object/from16 v0, p1

    #@a8
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@aa
    move-object/from16 v18, v0

    #@ac
    move-object/from16 v0, v18

    #@ae
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v18

    #@b2
    if-eqz v18, :cond_2a8

    #@b4
    .line 3674
    move-object/from16 v0, p0

    #@b6
    move-object/from16 v1, v16

    #@b8
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@bb
    move-result v11

    #@bc
    .line 3675
    const/16 v18, 0x0

    #@be
    aget-object v18, v16, v18

    #@c0
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c3
    move-result v17

    #@c4
    .line 3676
    move-object/from16 v0, p0

    #@c6
    move-object/from16 v1, v16

    #@c8
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@cb
    move-result v11

    #@cc
    .line 3677
    const/16 v18, 0x0

    #@ce
    aget-object v18, v16, v18

    #@d0
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@d3
    move-result v13

    #@d4
    .line 3678
    .local v13, platformVersion:I
    move-object/from16 v0, p0

    #@d6
    move-object/from16 v1, v16

    #@d8
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@db
    move-result v11

    #@dc
    .line 3679
    const/16 v18, 0x0

    #@de
    aget-object v18, v16, v18

    #@e0
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@e3
    move-result v18

    #@e4
    if-lez v18, :cond_137

    #@e6
    const/16 v18, 0x0

    #@e8
    aget-object v18, v16, v18

    #@ea
    :goto_ea
    move-object/from16 v0, v18

    #@ec
    move-object/from16 v1, p1

    #@ee
    iput-object v0, v1, Lcom/android/server/BackupManagerService$FileMetadata;->installerPackageName:Ljava/lang/String;

    #@f0
    .line 3680
    move-object/from16 v0, p0

    #@f2
    move-object/from16 v1, v16

    #@f4
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@f7
    move-result v11

    #@f8
    .line 3681
    const/16 v18, 0x0

    #@fa
    aget-object v18, v16, v18

    #@fc
    const-string v19, "1"

    #@fe
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@101
    move-result v7

    #@102
    .line 3682
    .local v7, hasApk:Z
    move-object/from16 v0, p0

    #@104
    move-object/from16 v1, v16

    #@106
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@109
    move-result v11

    #@10a
    .line 3683
    const/16 v18, 0x0

    #@10c
    aget-object v18, v16, v18

    #@10e
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@111
    move-result v10

    #@112
    .line 3684
    .local v10, numSigs:I
    if-lez v10, :cond_27c

    #@114
    .line 3685
    new-array v15, v10, [Landroid/content/pm/Signature;

    #@116
    .line 3686
    .local v15, sigs:[Landroid/content/pm/Signature;
    const/4 v8, 0x0

    #@117
    .local v8, i:I
    :goto_117
    if-ge v8, v10, :cond_13a

    #@119
    .line 3687
    move-object/from16 v0, p0

    #@11b
    move-object/from16 v1, v16

    #@11d
    invoke-virtual {v0, v4, v11, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractLine([BI[Ljava/lang/String;)I

    #@120
    move-result v11

    #@121
    .line 3688
    new-instance v18, Landroid/content/pm/Signature;

    #@123
    const/16 v19, 0x0

    #@125
    aget-object v19, v16, v19

    #@127
    invoke-direct/range {v18 .. v19}, Landroid/content/pm/Signature;-><init>(Ljava/lang/String;)V

    #@12a
    aput-object v18, v15, v8
    :try_end_12c
    .catch Ljava/lang/NumberFormatException; {:try_start_82 .. :try_end_12c} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_82 .. :try_end_12c} :catch_29c

    #@12c
    .line 3686
    add-int/lit8 v8, v8, 0x1

    #@12e
    goto :goto_117

    #@12f
    .line 3660
    .end local v7           #hasApk:Z
    .end local v8           #i:I
    .end local v9           #manifestPackage:Ljava/lang/String;
    .end local v10           #numSigs:I
    .end local v11           #offset:I
    .end local v13           #platformVersion:I
    .end local v14           #policy:Lcom/android/server/BackupManagerService$RestorePolicy;
    .end local v15           #sigs:[Landroid/content/pm/Signature;
    .end local v16           #str:[Ljava/lang/String;
    .end local v17           #version:I
    :cond_12f
    new-instance v18, Ljava/io/IOException;

    #@131
    const-string v19, "Unexpected EOF in manifest"

    #@133
    invoke-direct/range {v18 .. v19}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@136
    throw v18

    #@137
    .line 3679
    .restart local v9       #manifestPackage:Ljava/lang/String;
    .restart local v11       #offset:I
    .restart local v13       #platformVersion:I
    .restart local v14       #policy:Lcom/android/server/BackupManagerService$RestorePolicy;
    .restart local v16       #str:[Ljava/lang/String;
    .restart local v17       #version:I
    :cond_137
    const/16 v18, 0x0

    #@139
    goto :goto_ea

    #@13a
    .line 3690
    .restart local v7       #hasApk:Z
    .restart local v8       #i:I
    .restart local v10       #numSigs:I
    .restart local v15       #sigs:[Landroid/content/pm/Signature;
    :cond_13a
    :try_start_13a
    move-object/from16 v0, p0

    #@13c
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mManifestSignatures:Ljava/util/HashMap;

    #@13e
    move-object/from16 v18, v0

    #@140
    move-object/from16 v0, p1

    #@142
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@144
    move-object/from16 v19, v0

    #@146
    move-object/from16 v0, v18

    #@148
    move-object/from16 v1, v19

    #@14a
    invoke-virtual {v0, v1, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_14d
    .catch Ljava/lang/NumberFormatException; {:try_start_13a .. :try_end_14d} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_13a .. :try_end_14d} :catch_29c

    #@14d
    .line 3694
    :try_start_14d
    move-object/from16 v0, p0

    #@14f
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@151
    move-object/from16 v18, v0

    #@153
    invoke-static/range {v18 .. v18}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@156
    move-result-object v18

    #@157
    move-object/from16 v0, p1

    #@159
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@15b
    move-object/from16 v19, v0

    #@15d
    const/16 v20, 0x40

    #@15f
    invoke-virtual/range {v18 .. v20}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@162
    move-result-object v12

    #@163
    .line 3697
    .local v12, pkgInfo:Landroid/content/pm/PackageInfo;
    iget-object v0, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@165
    move-object/from16 v18, v0

    #@167
    move-object/from16 v0, v18

    #@169
    iget v6, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@16b
    .line 3698
    .local v6, flags:I
    const v18, 0x8000

    #@16e
    and-int v18, v18, v6

    #@170
    if-eqz v18, :cond_1b1

    #@172
    .line 3701
    iget-object v0, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@174
    move-object/from16 v18, v0

    #@176
    move-object/from16 v0, v18

    #@178
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@17a
    move/from16 v18, v0

    #@17c
    const/16 v19, 0x2710

    #@17e
    move/from16 v0, v18

    #@180
    move/from16 v1, v19

    #@182
    if-ge v0, v1, :cond_190

    #@184
    iget-object v0, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@186
    move-object/from16 v18, v0

    #@188
    move-object/from16 v0, v18

    #@18a
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@18c
    move-object/from16 v18, v0

    #@18e
    if-eqz v18, :cond_235

    #@190
    .line 3710
    :cond_190
    move-object/from16 v0, p0

    #@192
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@194
    move-object/from16 v18, v0

    #@196
    move-object/from16 v0, v18

    #@198
    invoke-static {v0, v15, v12}, Lcom/android/server/BackupManagerService;->access$1700(Lcom/android/server/BackupManagerService;[Landroid/content/pm/Signature;Landroid/content/pm/PackageInfo;)Z

    #@19b
    move-result v18

    #@19c
    if-eqz v18, :cond_211

    #@19e
    .line 3711
    iget v0, v12, Landroid/content/pm/PackageInfo;->versionCode:I

    #@1a0
    move/from16 v18, v0

    #@1a2
    move/from16 v0, v18

    #@1a4
    move/from16 v1, v17

    #@1a6
    if-lt v0, v1, :cond_1de

    #@1a8
    .line 3712
    const-string v18, "BackupManagerService"

    #@1aa
    const-string v19, "Sig + version match; taking data"

    #@1ac
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    .line 3713
    sget-object v14, Lcom/android/server/BackupManagerService$RestorePolicy;->ACCEPT:Lcom/android/server/BackupManagerService$RestorePolicy;
    :try_end_1b1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14d .. :try_end_1b1} :catch_230
    .catch Ljava/lang/NumberFormatException; {:try_start_14d .. :try_end_1b1} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_14d .. :try_end_1b1} :catch_29c

    #@1b1
    .line 3744
    .end local v6           #flags:I
    .end local v12           #pkgInfo:Landroid/content/pm/PackageInfo;
    :cond_1b1
    :goto_1b1
    :try_start_1b1
    sget-object v18, Lcom/android/server/BackupManagerService$RestorePolicy;->ACCEPT_IF_APK:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@1b3
    move-object/from16 v0, v18

    #@1b5
    if-ne v14, v0, :cond_1dd

    #@1b7
    if-nez v7, :cond_1dd

    #@1b9
    .line 3745
    const-string v18, "BackupManagerService"

    #@1bb
    new-instance v19, Ljava/lang/StringBuilder;

    #@1bd
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1c0
    const-string v20, "Cannot restore package "

    #@1c2
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v19

    #@1c6
    move-object/from16 v0, p1

    #@1c8
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@1ca
    move-object/from16 v20, v0

    #@1cc
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v19

    #@1d0
    const-string v20, " without the matching .apk"

    #@1d2
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v19

    #@1d6
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d9
    move-result-object v19

    #@1da
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1dd
    .catch Ljava/lang/NumberFormatException; {:try_start_1b1 .. :try_end_1dd} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1b1 .. :try_end_1dd} :catch_29c

    #@1dd
    .line 3766
    .end local v7           #hasApk:Z
    .end local v8           #i:I
    .end local v9           #manifestPackage:Ljava/lang/String;
    .end local v10           #numSigs:I
    .end local v13           #platformVersion:I
    .end local v15           #sigs:[Landroid/content/pm/Signature;
    .end local v17           #version:I
    :cond_1dd
    :goto_1dd
    return-object v14

    #@1de
    .line 3718
    .restart local v6       #flags:I
    .restart local v7       #hasApk:Z
    .restart local v8       #i:I
    .restart local v9       #manifestPackage:Ljava/lang/String;
    .restart local v10       #numSigs:I
    .restart local v12       #pkgInfo:Landroid/content/pm/PackageInfo;
    .restart local v13       #platformVersion:I
    .restart local v15       #sigs:[Landroid/content/pm/Signature;
    .restart local v17       #version:I
    :cond_1de
    :try_start_1de
    const-string v18, "BackupManagerService"

    #@1e0
    new-instance v19, Ljava/lang/StringBuilder;

    #@1e2
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1e5
    const-string v20, "Data version "

    #@1e7
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ea
    move-result-object v19

    #@1eb
    move-object/from16 v0, v19

    #@1ed
    move/from16 v1, v17

    #@1ef
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v19

    #@1f3
    const-string v20, " is newer than installed version "

    #@1f5
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v19

    #@1f9
    iget v0, v12, Landroid/content/pm/PackageInfo;->versionCode:I

    #@1fb
    move/from16 v20, v0

    #@1fd
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@200
    move-result-object v19

    #@201
    const-string v20, " - requiring apk"

    #@203
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v19

    #@207
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20a
    move-result-object v19

    #@20b
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20e
    .line 3721
    sget-object v14, Lcom/android/server/BackupManagerService$RestorePolicy;->ACCEPT_IF_APK:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@210
    goto :goto_1b1

    #@211
    .line 3724
    :cond_211
    const-string v18, "BackupManagerService"

    #@213
    new-instance v19, Ljava/lang/StringBuilder;

    #@215
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@218
    const-string v20, "Restore manifest signatures do not match installed application for "

    #@21a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v19

    #@21e
    move-object/from16 v0, p1

    #@220
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@222
    move-object/from16 v20, v0

    #@224
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v19

    #@228
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22b
    move-result-object v19

    #@22c
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_22f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1de .. :try_end_22f} :catch_230
    .catch Ljava/lang/NumberFormatException; {:try_start_1de .. :try_end_22f} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1de .. :try_end_22f} :catch_29c

    #@22f
    goto :goto_1b1

    #@230
    .line 3735
    .end local v6           #flags:I
    .end local v12           #pkgInfo:Landroid/content/pm/PackageInfo;
    :catch_230
    move-exception v5

    #@231
    .line 3741
    .local v5, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_231
    sget-object v14, Lcom/android/server/BackupManagerService$RestorePolicy;->ACCEPT_IF_APK:Lcom/android/server/BackupManagerService$RestorePolicy;
    :try_end_233
    .catch Ljava/lang/NumberFormatException; {:try_start_231 .. :try_end_233} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_231 .. :try_end_233} :catch_29c

    #@233
    goto/16 :goto_1b1

    #@235
    .line 3728
    .end local v5           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6       #flags:I
    .restart local v12       #pkgInfo:Landroid/content/pm/PackageInfo;
    :cond_235
    :try_start_235
    const-string v18, "BackupManagerService"

    #@237
    new-instance v19, Ljava/lang/StringBuilder;

    #@239
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@23c
    const-string v20, "Package "

    #@23e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@241
    move-result-object v19

    #@242
    move-object/from16 v0, p1

    #@244
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@246
    move-object/from16 v20, v0

    #@248
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24b
    move-result-object v19

    #@24c
    const-string v20, " is system level with no agent"

    #@24e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@251
    move-result-object v19

    #@252
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@255
    move-result-object v19

    #@256
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_259
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_235 .. :try_end_259} :catch_230
    .catch Ljava/lang/NumberFormatException; {:try_start_235 .. :try_end_259} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_235 .. :try_end_259} :catch_29c

    #@259
    goto/16 :goto_1b1

    #@25b
    .line 3760
    .end local v6           #flags:I
    .end local v7           #hasApk:Z
    .end local v8           #i:I
    .end local v9           #manifestPackage:Ljava/lang/String;
    .end local v10           #numSigs:I
    .end local v12           #pkgInfo:Landroid/content/pm/PackageInfo;
    .end local v13           #platformVersion:I
    .end local v15           #sigs:[Landroid/content/pm/Signature;
    .end local v17           #version:I
    :catch_25b
    move-exception v5

    #@25c
    .line 3761
    .local v5, e:Ljava/lang/NumberFormatException;
    const-string v18, "BackupManagerService"

    #@25e
    new-instance v19, Ljava/lang/StringBuilder;

    #@260
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@263
    const-string v20, "Corrupt restore manifest for package "

    #@265
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@268
    move-result-object v19

    #@269
    move-object/from16 v0, p1

    #@26b
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@26d
    move-object/from16 v20, v0

    #@26f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@272
    move-result-object v19

    #@273
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@276
    move-result-object v19

    #@277
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27a
    goto/16 :goto_1dd

    #@27c
    .line 3749
    .end local v5           #e:Ljava/lang/NumberFormatException;
    .restart local v7       #hasApk:Z
    .restart local v9       #manifestPackage:Ljava/lang/String;
    .restart local v10       #numSigs:I
    .restart local v13       #platformVersion:I
    .restart local v17       #version:I
    :cond_27c
    :try_start_27c
    const-string v18, "BackupManagerService"

    #@27e
    new-instance v19, Ljava/lang/StringBuilder;

    #@280
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@283
    const-string v20, "Missing signature on backed-up package "

    #@285
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v19

    #@289
    move-object/from16 v0, p1

    #@28b
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@28d
    move-object/from16 v20, v0

    #@28f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@292
    move-result-object v19

    #@293
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@296
    move-result-object v19

    #@297
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_29a
    .catch Ljava/lang/NumberFormatException; {:try_start_27c .. :try_end_29a} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_27c .. :try_end_29a} :catch_29c

    #@29a
    goto/16 :goto_1dd

    #@29c
    .line 3762
    .end local v7           #hasApk:Z
    .end local v9           #manifestPackage:Ljava/lang/String;
    .end local v10           #numSigs:I
    .end local v13           #platformVersion:I
    .end local v17           #version:I
    :catch_29c
    move-exception v5

    #@29d
    .line 3763
    .local v5, e:Ljava/lang/IllegalArgumentException;
    const-string v18, "BackupManagerService"

    #@29f
    invoke-virtual {v5}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@2a2
    move-result-object v19

    #@2a3
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a6
    goto/16 :goto_1dd

    #@2a8
    .line 3753
    .end local v5           #e:Ljava/lang/IllegalArgumentException;
    .restart local v9       #manifestPackage:Ljava/lang/String;
    .restart local v17       #version:I
    :cond_2a8
    :try_start_2a8
    const-string v18, "BackupManagerService"

    #@2aa
    new-instance v19, Ljava/lang/StringBuilder;

    #@2ac
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@2af
    const-string v20, "Expected package "

    #@2b1
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b4
    move-result-object v19

    #@2b5
    move-object/from16 v0, p1

    #@2b7
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@2b9
    move-object/from16 v20, v0

    #@2bb
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2be
    move-result-object v19

    #@2bf
    const-string v20, " but restore manifest claims "

    #@2c1
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c4
    move-result-object v19

    #@2c5
    move-object/from16 v0, v19

    #@2c7
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v19

    #@2cb
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ce
    move-result-object v19

    #@2cf
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2d2
    goto/16 :goto_1dd

    #@2d4
    .line 3757
    .end local v9           #manifestPackage:Ljava/lang/String;
    :cond_2d4
    const-string v18, "BackupManagerService"

    #@2d6
    new-instance v19, Ljava/lang/StringBuilder;

    #@2d8
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@2db
    const-string v20, "Unknown restore manifest version "

    #@2dd
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e0
    move-result-object v19

    #@2e1
    move-object/from16 v0, v19

    #@2e3
    move/from16 v1, v17

    #@2e5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v19

    #@2e9
    const-string v20, " for package "

    #@2eb
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ee
    move-result-object v19

    #@2ef
    move-object/from16 v0, p1

    #@2f1
    iget-object v0, v0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@2f3
    move-object/from16 v20, v0

    #@2f5
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f8
    move-result-object v19

    #@2f9
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fc
    move-result-object v19

    #@2fd
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_300
    .catch Ljava/lang/NumberFormatException; {:try_start_2a8 .. :try_end_300} :catch_25b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2a8 .. :try_end_300} :catch_29c

    #@300
    goto/16 :goto_1dd
.end method

.method readExactly(Ljava/io/InputStream;[BII)I
    .registers 9
    .parameter "in"
    .parameter "buffer"
    .parameter "offset"
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3950
    if-gtz p4, :cond_a

    #@2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v3, "size must be > 0"

    #@6
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v2

    #@a
    .line 3952
    :cond_a
    const/4 v1, 0x0

    #@b
    .line 3953
    .local v1, soFar:I
    :goto_b
    if-ge v1, p4, :cond_17

    #@d
    .line 3954
    add-int v2, p3, v1

    #@f
    sub-int v3, p4, v1

    #@11
    invoke-virtual {p1, p2, v2, v3}, Ljava/io/InputStream;->read([BII)I

    #@14
    move-result v0

    #@15
    .line 3955
    .local v0, nRead:I
    if-gtz v0, :cond_18

    #@17
    .line 3961
    .end local v0           #nRead:I
    :cond_17
    return v1

    #@18
    .line 3959
    .restart local v0       #nRead:I
    :cond_18
    add-int/2addr v1, v0

    #@19
    .line 3960
    goto :goto_b
.end method

.method readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;
    .registers 5
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3078
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v2, 0x50

    #@4
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 3079
    .local v0, buffer:Ljava/lang/StringBuilder;
    :goto_7
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    #@a
    move-result v1

    #@b
    .local v1, c:I
    if-ltz v1, :cond_11

    #@d
    .line 3080
    const/16 v2, 0xa

    #@f
    if-ne v1, v2, :cond_16

    #@11
    .line 3083
    :cond_11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    return-object v2

    #@16
    .line 3081
    :cond_16
    int-to-char v2, v1

    #@17
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a
    goto :goto_7
.end method

.method readPaxExtendedHeader(Ljava/io/InputStream;Lcom/android/server/BackupManagerService$FileMetadata;)Z
    .registers 18
    .parameter "instream"
    .parameter "info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3976
    move-object/from16 v0, p2

    #@2
    iget-wide v11, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@4
    const-wide/32 v13, 0x8000

    #@7
    cmp-long v11, v11, v13

    #@9
    if-lez v11, :cond_4a

    #@b
    .line 3977
    const-string v11, "BackupManagerService"

    #@d
    new-instance v12, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v13, "Suspiciously large pax header size "

    #@14
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v12

    #@18
    move-object/from16 v0, p2

    #@1a
    iget-wide v13, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@1c
    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v12

    #@20
    const-string v13, " - aborting"

    #@22
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v12

    #@26
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v12

    #@2a
    invoke-static {v11, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 3979
    new-instance v11, Ljava/io/IOException;

    #@2f
    new-instance v12, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v13, "Sanity failure: pax header size "

    #@36
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v12

    #@3a
    move-object/from16 v0, p2

    #@3c
    iget-wide v13, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@3e
    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@41
    move-result-object v12

    #@42
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v12

    #@46
    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@49
    throw v11

    #@4a
    .line 3983
    :cond_4a
    move-object/from16 v0, p2

    #@4c
    iget-wide v11, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@4e
    const-wide/16 v13, 0x1ff

    #@50
    add-long/2addr v11, v13

    #@51
    const/16 v13, 0x9

    #@53
    shr-long/2addr v11, v13

    #@54
    long-to-int v7, v11

    #@55
    .line 3984
    .local v7, numBlocks:I
    mul-int/lit16 v11, v7, 0x200

    #@57
    new-array v2, v11, [B

    #@59
    .line 3985
    .local v2, data:[B
    const/4 v11, 0x0

    #@5a
    array-length v12, v2

    #@5b
    move-object/from16 v0, p1

    #@5d
    invoke-virtual {p0, v0, v2, v11, v12}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readExactly(Ljava/io/InputStream;[BII)I

    #@60
    move-result v11

    #@61
    array-length v12, v2

    #@62
    if-ge v11, v12, :cond_6c

    #@64
    .line 3986
    new-instance v11, Ljava/io/IOException;

    #@66
    const-string v12, "Unable to read full pax header"

    #@68
    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v11

    #@6c
    .line 3988
    :cond_6c
    iget-wide v11, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@6e
    array-length v13, v2

    #@6f
    int-to-long v13, v13

    #@70
    add-long/2addr v11, v13

    #@71
    iput-wide v11, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@73
    .line 3990
    move-object/from16 v0, p2

    #@75
    iget-wide v11, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@77
    long-to-int v1, v11

    #@78
    .line 3991
    .local v1, contentSize:I
    const/4 v8, 0x0

    #@79
    .line 3994
    .local v8, offset:I
    :cond_79
    add-int/lit8 v3, v8, 0x1

    #@7b
    .line 3995
    .local v3, eol:I
    :goto_7b
    if-ge v3, v1, :cond_86

    #@7d
    aget-byte v11, v2, v3

    #@7f
    const/16 v12, 0x20

    #@81
    if-eq v11, v12, :cond_86

    #@83
    add-int/lit8 v3, v3, 0x1

    #@85
    goto :goto_7b

    #@86
    .line 3996
    :cond_86
    if-lt v3, v1, :cond_90

    #@88
    .line 3998
    new-instance v11, Ljava/io/IOException;

    #@8a
    const-string v12, "Invalid pax data"

    #@8c
    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@8f
    throw v11

    #@90
    .line 4001
    :cond_90
    sub-int v11, v3, v8

    #@92
    const/16 v12, 0xa

    #@94
    invoke-virtual {p0, v2, v8, v11, v12}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractRadix([BIII)J

    #@97
    move-result-wide v11

    #@98
    long-to-int v6, v11

    #@99
    .line 4002
    .local v6, linelen:I
    add-int/lit8 v4, v3, 0x1

    #@9b
    .line 4003
    .local v4, key:I
    add-int v11, v8, v6

    #@9d
    add-int/lit8 v3, v11, -0x1

    #@9f
    .line 4005
    add-int/lit8 v10, v4, 0x1

    #@a1
    .local v10, value:I
    :goto_a1
    aget-byte v11, v2, v10

    #@a3
    const/16 v12, 0x3d

    #@a5
    if-eq v11, v12, :cond_ac

    #@a7
    if-gt v10, v3, :cond_ac

    #@a9
    add-int/lit8 v10, v10, 0x1

    #@ab
    goto :goto_a1

    #@ac
    .line 4006
    :cond_ac
    if-le v10, v3, :cond_b6

    #@ae
    .line 4007
    new-instance v11, Ljava/io/IOException;

    #@b0
    const-string v12, "Invalid pax declaration"

    #@b2
    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@b5
    throw v11

    #@b6
    .line 4011
    :cond_b6
    new-instance v5, Ljava/lang/String;

    #@b8
    sub-int v11, v10, v4

    #@ba
    const-string v12, "UTF-8"

    #@bc
    invoke-direct {v5, v2, v4, v11, v12}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@bf
    .line 4013
    .local v5, keyStr:Ljava/lang/String;
    new-instance v9, Ljava/lang/String;

    #@c1
    add-int/lit8 v11, v10, 0x1

    #@c3
    sub-int v12, v3, v10

    #@c5
    add-int/lit8 v12, v12, -0x1

    #@c7
    const-string v13, "UTF-8"

    #@c9
    invoke-direct {v9, v2, v11, v12, v13}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@cc
    .line 4015
    .local v9, valStr:Ljava/lang/String;
    const-string v11, "path"

    #@ce
    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d1
    move-result v11

    #@d2
    if-eqz v11, :cond_dd

    #@d4
    .line 4016
    move-object/from16 v0, p2

    #@d6
    iput-object v9, v0, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@d8
    .line 4023
    :cond_d8
    :goto_d8
    add-int/2addr v8, v6

    #@d9
    .line 4024
    if-lt v8, v1, :cond_79

    #@db
    .line 4026
    const/4 v11, 0x1

    #@dc
    return v11

    #@dd
    .line 4017
    :cond_dd
    const-string v11, "size"

    #@df
    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e2
    move-result v11

    #@e3
    if-eqz v11, :cond_d8

    #@e5
    .line 4018
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@e8
    move-result-wide v11

    #@e9
    move-object/from16 v0, p2

    #@eb
    iput-wide v11, v0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@ed
    goto :goto_d8
.end method

.method readTarHeader(Ljava/io/InputStream;[B)Z
    .registers 8
    .parameter "instream"
    .parameter "block"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v2, 0x200

    #@2
    const/4 v1, 0x0

    #@3
    .line 3965
    invoke-virtual {p0, p1, p2, v1, v2}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readExactly(Ljava/io/InputStream;[BII)I

    #@6
    move-result v0

    #@7
    .line 3966
    .local v0, got:I
    if-nez v0, :cond_a

    #@9
    .line 3969
    :goto_9
    return v1

    #@a
    .line 3967
    :cond_a
    if-ge v0, v2, :cond_14

    #@c
    new-instance v1, Ljava/io/IOException;

    #@e
    const-string v2, "Unable to read full block header"

    #@10
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 3968
    :cond_14
    iget-wide v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@16
    const-wide/16 v3, 0x200

    #@18
    add-long/2addr v1, v3

    #@19
    iput-wide v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@1b
    .line 3969
    const/4 v1, 0x1

    #@1c
    goto :goto_9
.end method

.method readTarHeaders(Ljava/io/InputStream;)Lcom/android/server/BackupManagerService$FileMetadata;
    .registers 15
    .parameter "instream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3820
    const/16 v8, 0x200

    #@2
    new-array v0, v8, [B

    #@4
    .line 3821
    .local v0, block:[B
    const/4 v3, 0x0

    #@5
    .line 3823
    .local v3, info:Lcom/android/server/BackupManagerService$FileMetadata;
    invoke-virtual {p0, p1, v0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readTarHeader(Ljava/io/InputStream;[B)Z

    #@8
    move-result v2

    #@9
    .line 3824
    .local v2, gotHeader:Z
    if-eqz v2, :cond_102

    #@b
    .line 3827
    :try_start_b
    new-instance v4, Lcom/android/server/BackupManagerService$FileMetadata;

    #@d
    invoke-direct {v4}, Lcom/android/server/BackupManagerService$FileMetadata;-><init>()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_10} :catch_21c

    #@10
    .line 3828
    .end local v3           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .local v4, info:Lcom/android/server/BackupManagerService$FileMetadata;
    const/16 v8, 0x7c

    #@12
    const/16 v9, 0xc

    #@14
    const/16 v10, 0x8

    #@16
    :try_start_16
    invoke-virtual {p0, v0, v8, v9, v10}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractRadix([BIII)J

    #@19
    move-result-wide v8

    #@1a
    iput-wide v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@1c
    .line 3829
    const/16 v8, 0x88

    #@1e
    const/16 v9, 0xc

    #@20
    const/16 v10, 0x8

    #@22
    invoke-virtual {p0, v0, v8, v9, v10}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractRadix([BIII)J

    #@25
    move-result-wide v8

    #@26
    iput-wide v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->mtime:J

    #@28
    .line 3830
    const/16 v8, 0x64

    #@2a
    const/16 v9, 0x8

    #@2c
    const/16 v10, 0x8

    #@2e
    invoke-virtual {p0, v0, v8, v9, v10}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractRadix([BIII)J

    #@31
    move-result-wide v8

    #@32
    iput-wide v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->mode:J

    #@34
    .line 3832
    const/16 v8, 0x159

    #@36
    const/16 v9, 0x9b

    #@38
    invoke-virtual {p0, v0, v8, v9}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractString([BII)Ljava/lang/String;

    #@3b
    move-result-object v8

    #@3c
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@3e
    .line 3833
    const/4 v8, 0x0

    #@3f
    const/16 v9, 0x64

    #@41
    invoke-virtual {p0, v0, v8, v9}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->extractString([BII)Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    .line 3834
    .local v5, path:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@48
    move-result v8

    #@49
    if-lez v8, :cond_7f

    #@4b
    .line 3835
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@4d
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@50
    move-result v8

    #@51
    if-lez v8, :cond_6a

    #@53
    new-instance v8, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    iget-object v9, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@5a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v8

    #@5e
    const/16 v9, 0x2f

    #@60
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v8

    #@68
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@6a
    .line 3836
    :cond_6a
    new-instance v8, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    iget-object v9, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@71
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v8

    #@75
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v8

    #@7d
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@7f
    .line 3840
    :cond_7f
    const/16 v8, 0x9c

    #@81
    aget-byte v7, v0, v8

    #@83
    .line 3841
    .local v7, typeChar:I
    const/16 v8, 0x78

    #@85
    if-ne v7, v8, :cond_a2

    #@87
    .line 3843
    invoke-virtual {p0, p1, v4}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readPaxExtendedHeader(Ljava/io/InputStream;Lcom/android/server/BackupManagerService$FileMetadata;)Z

    #@8a
    move-result v2

    #@8b
    .line 3844
    if-eqz v2, :cond_91

    #@8d
    .line 3847
    invoke-virtual {p0, p1, v0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readTarHeader(Ljava/io/InputStream;[B)Z

    #@90
    move-result v2

    #@91
    .line 3849
    :cond_91
    if-nez v2, :cond_9e

    #@93
    new-instance v8, Ljava/io/IOException;

    #@95
    const-string v9, "Bad or missing pax header"

    #@97
    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@9a
    throw v8
    :try_end_9b
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_9b} :catch_9b

    #@9b
    .line 3917
    .end local v5           #path:Ljava/lang/String;
    .end local v7           #typeChar:I
    :catch_9b
    move-exception v1

    #@9c
    move-object v3, v4

    #@9d
    .line 3922
    .end local v4           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .local v1, e:Ljava/io/IOException;
    .restart local v3       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    :goto_9d
    throw v1

    #@9e
    .line 3851
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .restart local v4       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .restart local v5       #path:Ljava/lang/String;
    .restart local v7       #typeChar:I
    :cond_9e
    const/16 v8, 0x9c

    #@a0
    :try_start_a0
    aget-byte v7, v0, v8

    #@a2
    .line 3854
    :cond_a2
    sparse-switch v7, :sswitch_data_220

    #@a5
    .line 3870
    const-string v8, "BackupManagerService"

    #@a7
    new-instance v9, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v10, "Unknown tar entity type: "

    #@ae
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v9

    #@b2
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v9

    #@b6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v9

    #@ba
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    .line 3871
    new-instance v8, Ljava/io/IOException;

    #@bf
    new-instance v9, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v10, "Unknown entity type "

    #@c6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v9

    #@ca
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v9

    #@ce
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v9

    #@d2
    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@d5
    throw v8

    #@d6
    .line 3855
    :sswitch_d6
    const/4 v8, 0x1

    #@d7
    iput v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->type:I

    #@d9
    .line 3878
    :cond_d9
    :goto_d9
    const-string v8, "shared/"

    #@db
    const/4 v9, 0x0

    #@dc
    iget-object v10, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@de
    const/4 v11, 0x0

    #@df
    const-string v12, "shared/"

    #@e1
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@e4
    move-result v12

    #@e5
    invoke-virtual {v8, v9, v10, v11, v12}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@e8
    move-result v8

    #@e9
    if-eqz v8, :cond_11f

    #@eb
    .line 3881
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@ed
    const-string v9, "shared/"

    #@ef
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@f2
    move-result v9

    #@f3
    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@f6
    move-result-object v8

    #@f7
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@f9
    .line 3882
    const-string v8, "com.android.sharedstoragebackup"

    #@fb
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@fd
    .line 3883
    const-string v8, "shared"

    #@ff
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@101
    :cond_101
    :goto_101
    move-object v3, v4

    #@102
    .end local v4           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .end local v5           #path:Ljava/lang/String;
    .end local v7           #typeChar:I
    .restart local v3       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    :cond_102
    move-object v8, v3

    #@103
    .line 3925
    :goto_103
    return-object v8

    #@104
    .line 3857
    .end local v3           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .restart local v4       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .restart local v5       #path:Ljava/lang/String;
    .restart local v7       #typeChar:I
    :sswitch_104
    const/4 v8, 0x2

    #@105
    iput v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->type:I

    #@107
    .line 3858
    iget-wide v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@109
    const-wide/16 v10, 0x0

    #@10b
    cmp-long v8, v8, v10

    #@10d
    if-eqz v8, :cond_d9

    #@10f
    .line 3859
    const-string v8, "BackupManagerService"

    #@111
    const-string v9, "Directory entry with nonzero size in header"

    #@113
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@116
    .line 3860
    const-wide/16 v8, 0x0

    #@118
    iput-wide v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@11a
    goto :goto_d9

    #@11b
    .line 3867
    :sswitch_11b
    const/4 v3, 0x0

    #@11c
    move-object v8, v3

    #@11d
    move-object v3, v4

    #@11e
    .end local v4           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .restart local v3       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    goto :goto_103

    #@11f
    .line 3885
    .end local v3           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .restart local v4       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    :cond_11f
    const-string v8, "apps/"

    #@121
    const/4 v9, 0x0

    #@122
    iget-object v10, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@124
    const/4 v11, 0x0

    #@125
    const-string v12, "apps/"

    #@127
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@12a
    move-result v12

    #@12b
    invoke-virtual {v8, v9, v10, v11, v12}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@12e
    move-result v8

    #@12f
    if-eqz v8, :cond_101

    #@131
    .line 3890
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@133
    const-string v9, "apps/"

    #@135
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@138
    move-result v9

    #@139
    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@13c
    move-result-object v8

    #@13d
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@13f
    .line 3893
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@141
    const/16 v9, 0x2f

    #@143
    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(I)I

    #@146
    move-result v6

    #@147
    .line 3894
    .local v6, slash:I
    if-gez v6, :cond_164

    #@149
    new-instance v8, Ljava/io/IOException;

    #@14b
    new-instance v9, Ljava/lang/StringBuilder;

    #@14d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@150
    const-string v10, "Illegal semantic path in "

    #@152
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v9

    #@156
    iget-object v10, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@158
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v9

    #@15c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15f
    move-result-object v9

    #@160
    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@163
    throw v8

    #@164
    .line 3895
    :cond_164
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@166
    const/4 v9, 0x0

    #@167
    invoke-virtual {v8, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@16a
    move-result-object v8

    #@16b
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@16d
    .line 3896
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@16f
    add-int/lit8 v9, v6, 0x1

    #@171
    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@174
    move-result-object v8

    #@175
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@177
    .line 3899
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@179
    const-string v9, "_manifest"

    #@17b
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17e
    move-result v8

    #@17f
    if-nez v8, :cond_101

    #@181
    .line 3900
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@183
    const/16 v9, 0x2f

    #@185
    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(I)I

    #@188
    move-result v6

    #@189
    .line 3901
    if-gez v6, :cond_1a6

    #@18b
    new-instance v8, Ljava/io/IOException;

    #@18d
    new-instance v9, Ljava/lang/StringBuilder;

    #@18f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@192
    const-string v10, "Illegal semantic path in non-manifest "

    #@194
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v9

    #@198
    iget-object v10, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@19a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v9

    #@19e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a1
    move-result-object v9

    #@1a2
    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1a5
    throw v8

    #@1a6
    .line 3902
    :cond_1a6
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@1a8
    const/4 v9, 0x0

    #@1a9
    invoke-virtual {v8, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1ac
    move-result-object v8

    #@1ad
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1af
    .line 3904
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1b1
    const-string v9, "a"

    #@1b3
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b6
    move-result v8

    #@1b7
    if-nez v8, :cond_210

    #@1b9
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1bb
    const-string v9, "f"

    #@1bd
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c0
    move-result v8

    #@1c1
    if-nez v8, :cond_210

    #@1c3
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1c5
    const-string v9, "db"

    #@1c7
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ca
    move-result v8

    #@1cb
    if-nez v8, :cond_210

    #@1cd
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1cf
    const-string v9, "r"

    #@1d1
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d4
    move-result v8

    #@1d5
    if-nez v8, :cond_210

    #@1d7
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1d9
    const-string v9, "sp"

    #@1db
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1de
    move-result v8

    #@1df
    if-nez v8, :cond_210

    #@1e1
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1e3
    const-string v9, "obb"

    #@1e5
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e8
    move-result v8

    #@1e9
    if-nez v8, :cond_210

    #@1eb
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@1ed
    const-string v9, "c"

    #@1ef
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f2
    move-result v8

    #@1f3
    if-nez v8, :cond_210

    #@1f5
    .line 3911
    new-instance v8, Ljava/io/IOException;

    #@1f7
    new-instance v9, Ljava/lang/StringBuilder;

    #@1f9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1fc
    const-string v10, "Unrecognized domain "

    #@1fe
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@201
    move-result-object v9

    #@202
    iget-object v10, v4, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@204
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@207
    move-result-object v9

    #@208
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20b
    move-result-object v9

    #@20c
    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@20f
    throw v8

    #@210
    .line 3914
    :cond_210
    iget-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@212
    add-int/lit8 v9, v6, 0x1

    #@214
    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@217
    move-result-object v8

    #@218
    iput-object v8, v4, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;
    :try_end_21a
    .catch Ljava/io/IOException; {:try_start_a0 .. :try_end_21a} :catch_9b

    #@21a
    goto/16 :goto_101

    #@21c
    .line 3917
    .end local v4           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .end local v5           #path:Ljava/lang/String;
    .end local v6           #slash:I
    .end local v7           #typeChar:I
    .restart local v3       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    :catch_21c
    move-exception v1

    #@21d
    goto/16 :goto_9d

    #@21f
    .line 3854
    nop

    #@220
    :sswitch_data_220
    .sparse-switch
        0x0 -> :sswitch_11b
        0x30 -> :sswitch_d6
        0x35 -> :sswitch_104
    .end sparse-switch
.end method

.method restoreOneFile(Ljava/io/InputStream;[B)Z
    .registers 40
    .parameter "instream"
    .parameter "buffer"

    #@0
    .prologue
    .line 3168
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readTarHeaders(Ljava/io/InputStream;)Lcom/android/server/BackupManagerService$FileMetadata;

    #@3
    move-result-object v6

    #@4
    .line 3169
    .local v6, info:Lcom/android/server/BackupManagerService$FileMetadata;
    if-eqz v6, :cond_7f

    #@6
    .line 3174
    iget-object v0, v6, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@8
    move-object/from16 v32, v0

    #@a
    .line 3175
    .local v32, pkg:Ljava/lang/String;
    move-object/from16 v0, p0

    #@c
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgentPackage:Ljava/lang/String;

    #@e
    move-object/from16 v0, v32

    #@10
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_49

    #@16
    .line 3178
    move-object/from16 v0, p0

    #@18
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@1a
    move-object/from16 v0, v32

    #@1c
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@1f
    move-result v4

    #@20
    if-nez v4, :cond_2d

    #@22
    .line 3179
    move-object/from16 v0, p0

    #@24
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@26
    sget-object v5, Lcom/android/server/BackupManagerService$RestorePolicy;->IGNORE:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@28
    move-object/from16 v0, v32

    #@2a
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 3184
    :cond_2d
    move-object/from16 v0, p0

    #@2f
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@31
    if-eqz v4, :cond_49

    #@33
    .line 3186
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@36
    .line 3187
    move-object/from16 v0, p0

    #@38
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@3a
    move-object/from16 v0, p0

    #@3c
    invoke-virtual {v0, v4}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownAgent(Landroid/content/pm/ApplicationInfo;)V

    #@3f
    .line 3188
    const/4 v4, 0x0

    #@40
    move-object/from16 v0, p0

    #@42
    iput-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@44
    .line 3189
    const/4 v4, 0x0

    #@45
    move-object/from16 v0, p0

    #@47
    iput-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgentPackage:Ljava/lang/String;

    #@49
    .line 3193
    :cond_49
    iget-object v4, v6, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@4b
    const-string v5, "_manifest"

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v4

    #@51
    if-eqz v4, :cond_83

    #@53
    .line 3194
    move-object/from16 v0, p0

    #@55
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@57
    move-object/from16 v0, p0

    #@59
    move-object/from16 v1, p1

    #@5b
    invoke-virtual {v0, v6, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readAppManifest(Lcom/android/server/BackupManagerService$FileMetadata;Ljava/io/InputStream;)Lcom/android/server/BackupManagerService$RestorePolicy;

    #@5e
    move-result-object v5

    #@5f
    move-object/from16 v0, v32

    #@61
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@64
    .line 3195
    move-object/from16 v0, p0

    #@66
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackageInstallers:Ljava/util/HashMap;

    #@68
    iget-object v5, v6, Lcom/android/server/BackupManagerService$FileMetadata;->installerPackageName:Ljava/lang/String;

    #@6a
    move-object/from16 v0, v32

    #@6c
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6f
    .line 3199
    iget-wide v4, v6, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@71
    move-object/from16 v0, p0

    #@73
    move-object/from16 v1, p1

    #@75
    invoke-virtual {v0, v4, v5, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->skipTarPadding(JLjava/io/InputStream;)V

    #@78
    .line 3200
    move-object/from16 v0, p0

    #@7a
    move-object/from16 v1, v32

    #@7c
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->sendOnRestorePackage(Ljava/lang/String;)V

    #@7f
    .line 3420
    .end local v32           #pkg:Ljava/lang/String;
    :cond_7f
    :goto_7f
    if-eqz v6, :cond_33b

    #@81
    const/4 v4, 0x1

    #@82
    :goto_82
    return v4

    #@83
    .line 3204
    .restart local v32       #pkg:Ljava/lang/String;
    :cond_83
    const/16 v29, 0x1

    #@85
    .line 3205
    .local v29, okay:Z
    move-object/from16 v0, p0

    #@87
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@89
    move-object/from16 v0, v32

    #@8b
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8e
    move-result-object v33

    #@8f
    check-cast v33, Lcom/android/server/BackupManagerService$RestorePolicy;

    #@91
    .line 3206
    .local v33, policy:Lcom/android/server/BackupManagerService$RestorePolicy;
    sget-object v4, Lcom/android/server/BackupManagerService$4;->$SwitchMap$com$android$server$BackupManagerService$RestorePolicy:[I

    #@93
    invoke-virtual/range {v33 .. v33}, Lcom/android/server/BackupManagerService$RestorePolicy;->ordinal()I

    #@96
    move-result v5

    #@97
    aget v4, v4, v5

    #@99
    packed-switch v4, :pswitch_data_344

    #@9c
    .line 3251
    const-string v4, "BackupManagerService"

    #@9e
    const-string v5, "Invalid policy from manifest"

    #@a0
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 3252
    const/16 v29, 0x0

    #@a5
    .line 3253
    move-object/from16 v0, p0

    #@a7
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@a9
    sget-object v5, Lcom/android/server/BackupManagerService$RestorePolicy;->IGNORE:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@ab
    move-object/from16 v0, v32

    #@ad
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b0
    .line 3262
    :cond_b0
    :goto_b0
    if-eqz v29, :cond_121

    #@b2
    move-object/from16 v0, p0

    #@b4
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_b6} :catch_337

    #@b6
    if-nez v4, :cond_121

    #@b8
    .line 3266
    :try_start_b8
    move-object/from16 v0, p0

    #@ba
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@bc
    invoke-static {v4}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@bf
    move-result-object v4

    #@c0
    const/4 v5, 0x0

    #@c1
    move-object/from16 v0, v32

    #@c3
    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@c6
    move-result-object v4

    #@c7
    move-object/from16 v0, p0

    #@c9
    iput-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@cb
    .line 3270
    move-object/from16 v0, p0

    #@cd
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mClearedPackages:Ljava/util/HashSet;

    #@cf
    move-object/from16 v0, v32

    #@d1
    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@d4
    move-result v4

    #@d5
    if-nez v4, :cond_f1

    #@d7
    .line 3274
    move-object/from16 v0, p0

    #@d9
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@db
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@dd
    if-nez v4, :cond_e8

    #@df
    .line 3276
    move-object/from16 v0, p0

    #@e1
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e3
    move-object/from16 v0, v32

    #@e5
    invoke-virtual {v4, v0}, Lcom/android/server/BackupManagerService;->clearApplicationDataSynchronous(Ljava/lang/String;)V

    #@e8
    .line 3281
    :cond_e8
    move-object/from16 v0, p0

    #@ea
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mClearedPackages:Ljava/util/HashSet;

    #@ec
    move-object/from16 v0, v32

    #@ee
    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@f1
    .line 3287
    :cond_f1
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->setUpPipes()V

    #@f4
    .line 3288
    move-object/from16 v0, p0

    #@f6
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@f8
    move-object/from16 v0, p0

    #@fa
    iget-object v5, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@fc
    const/4 v7, 0x3

    #@fd
    invoke-virtual {v4, v5, v7}, Lcom/android/server/BackupManagerService;->bindToAgentSynchronous(Landroid/content/pm/ApplicationInfo;I)Landroid/app/IBackupAgent;

    #@100
    move-result-object v4

    #@101
    move-object/from16 v0, p0

    #@103
    iput-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@105
    .line 3290
    move-object/from16 v0, v32

    #@107
    move-object/from16 v1, p0

    #@109
    iput-object v0, v1, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgentPackage:Ljava/lang/String;
    :try_end_10b
    .catch Ljava/io/IOException; {:try_start_b8 .. :try_end_10b} :catch_341
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_b8 .. :try_end_10b} :catch_33e

    #@10b
    .line 3297
    :goto_10b
    :try_start_10b
    move-object/from16 v0, p0

    #@10d
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@10f
    if-nez v4, :cond_121

    #@111
    .line 3299
    const/16 v29, 0x0

    #@113
    .line 3300
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@116
    .line 3301
    move-object/from16 v0, p0

    #@118
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@11a
    sget-object v5, Lcom/android/server/BackupManagerService$RestorePolicy;->IGNORE:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@11c
    move-object/from16 v0, v32

    #@11e
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@121
    .line 3307
    :cond_121
    if-eqz v29, :cond_159

    #@123
    move-object/from16 v0, p0

    #@125
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgentPackage:Ljava/lang/String;

    #@127
    move-object/from16 v0, v32

    #@129
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12c
    move-result v4

    #@12d
    if-nez v4, :cond_159

    #@12f
    .line 3308
    const-string v4, "BackupManagerService"

    #@131
    new-instance v5, Ljava/lang/StringBuilder;

    #@133
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v7, "Restoring data for "

    #@138
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v5

    #@13c
    move-object/from16 v0, v32

    #@13e
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v5

    #@142
    const-string v7, " but agent is for "

    #@144
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v5

    #@148
    move-object/from16 v0, p0

    #@14a
    iget-object v7, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgentPackage:Ljava/lang/String;

    #@14c
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v5

    #@150
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v5

    #@154
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@157
    .line 3310
    const/16 v29, 0x0

    #@159
    .line 3317
    :cond_159
    if-eqz v29, :cond_220

    #@15b
    .line 3318
    const/16 v22, 0x1

    #@15d
    .line 3319
    .local v22, agentSuccess:Z
    iget-wide v0, v6, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@15f
    move-wide/from16 v34, v0

    #@161
    .line 3320
    .local v34, toCopy:J
    move-object/from16 v0, p0

    #@163
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@165
    invoke-virtual {v4}, Lcom/android/server/BackupManagerService;->generateToken()I
    :try_end_168
    .catch Ljava/io/IOException; {:try_start_10b .. :try_end_168} :catch_337

    #@168
    move-result v8

    #@169
    .line 3324
    .local v8, token:I
    :try_start_169
    move-object/from16 v0, p0

    #@16b
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@16d
    const-wide/32 v9, 0x493e0

    #@170
    const/4 v5, 0x0

    #@171
    invoke-virtual {v4, v8, v9, v10, v5}, Lcom/android/server/BackupManagerService;->prepareOperationTimeout(IJLcom/android/server/BackupManagerService$BackupRestoreTask;)V

    #@174
    .line 3329
    move-object/from16 v0, p0

    #@176
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@178
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@17a
    const-string v5, "system"

    #@17c
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17f
    move-result v4

    #@180
    if-eqz v4, :cond_2c2

    #@182
    .line 3330
    const-string v4, "BackupManagerService"

    #@184
    const-string v5, "system process agent - spinning a thread"

    #@186
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@189
    .line 3331
    new-instance v3, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreFileRunnable;

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget-object v5, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@18f
    move-object/from16 v0, p0

    #@191
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@193
    const/4 v7, 0x0

    #@194
    aget-object v7, v4, v7

    #@196
    move-object/from16 v4, p0

    #@198
    invoke-direct/range {v3 .. v8}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreFileRunnable;-><init>(Lcom/android/server/BackupManagerService$PerformFullRestoreTask;Landroid/app/IBackupAgent;Lcom/android/server/BackupManagerService$FileMetadata;Landroid/os/ParcelFileDescriptor;I)V

    #@19b
    .line 3333
    .local v3, runner:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreFileRunnable;
    new-instance v4, Ljava/lang/Thread;

    #@19d
    invoke-direct {v4, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@1a0
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V
    :try_end_1a3
    .catch Ljava/io/IOException; {:try_start_169 .. :try_end_1a3} :catch_2ec
    .catch Landroid/os/RemoteException; {:try_start_169 .. :try_end_1a3} :catch_2fa

    #@1a3
    .line 3353
    .end local v3           #runner:Lcom/android/server/BackupManagerService$PerformFullRestoreTask$RestoreFileRunnable;
    :goto_1a3
    if-eqz v29, :cond_1f8

    #@1a5
    .line 3354
    const/16 v31, 0x1

    #@1a7
    .line 3355
    .local v31, pipeOkay:Z
    :try_start_1a7
    new-instance v30, Ljava/io/FileOutputStream;

    #@1a9
    move-object/from16 v0, p0

    #@1ab
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@1ad
    const/4 v5, 0x1

    #@1ae
    aget-object v4, v4, v5

    #@1b0
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@1b3
    move-result-object v4

    #@1b4
    move-object/from16 v0, v30

    #@1b6
    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@1b9
    .line 3357
    .local v30, pipe:Ljava/io/FileOutputStream;
    :cond_1b9
    :goto_1b9
    const-wide/16 v4, 0x0

    #@1bb
    cmp-long v4, v34, v4

    #@1bd
    if-lez v4, :cond_1e7

    #@1bf
    .line 3358
    move-object/from16 v0, p2

    #@1c1
    array-length v4, v0

    #@1c2
    int-to-long v4, v4

    #@1c3
    cmp-long v4, v34, v4

    #@1c5
    if-lez v4, :cond_308

    #@1c7
    move-object/from16 v0, p2

    #@1c9
    array-length v0, v0

    #@1ca
    move/from16 v36, v0

    #@1cc
    .line 3360
    .local v36, toRead:I
    :goto_1cc
    const/4 v4, 0x0

    #@1cd
    move-object/from16 v0, p1

    #@1cf
    move-object/from16 v1, p2

    #@1d1
    move/from16 v2, v36

    #@1d3
    invoke-virtual {v0, v1, v4, v2}, Ljava/io/InputStream;->read([BII)I

    #@1d6
    move-result v27

    #@1d7
    .line 3361
    .local v27, nRead:I
    if-ltz v27, :cond_1e5

    #@1d9
    move-object/from16 v0, p0

    #@1db
    iget-wide v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@1dd
    move/from16 v0, v27

    #@1df
    int-to-long v9, v0

    #@1e0
    add-long/2addr v4, v9

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    iput-wide v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@1e5
    .line 3362
    :cond_1e5
    if-gtz v27, :cond_30f

    #@1e7
    .line 3379
    .end local v27           #nRead:I
    .end local v36           #toRead:I
    :cond_1e7
    iget-wide v4, v6, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@1e9
    move-object/from16 v0, p0

    #@1eb
    move-object/from16 v1, p1

    #@1ed
    invoke-virtual {v0, v4, v5, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->skipTarPadding(JLjava/io/InputStream;)V

    #@1f0
    .line 3383
    move-object/from16 v0, p0

    #@1f2
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1f4
    invoke-virtual {v4, v8}, Lcom/android/server/BackupManagerService;->waitUntilOperationComplete(I)Z

    #@1f7
    move-result v22

    #@1f8
    .line 3388
    .end local v30           #pipe:Ljava/io/FileOutputStream;
    .end local v31           #pipeOkay:Z
    :cond_1f8
    if-nez v22, :cond_220

    #@1fa
    .line 3389
    move-object/from16 v0, p0

    #@1fc
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1fe
    iget-object v4, v4, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@200
    const/4 v5, 0x7

    #@201
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@204
    .line 3390
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@207
    .line 3391
    move-object/from16 v0, p0

    #@209
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@20b
    move-object/from16 v0, p0

    #@20d
    invoke-virtual {v0, v4}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownAgent(Landroid/content/pm/ApplicationInfo;)V

    #@210
    .line 3392
    const/4 v4, 0x0

    #@211
    move-object/from16 v0, p0

    #@213
    iput-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@215
    .line 3393
    move-object/from16 v0, p0

    #@217
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@219
    sget-object v5, Lcom/android/server/BackupManagerService$RestorePolicy;->IGNORE:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@21b
    move-object/from16 v0, v32

    #@21d
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@220
    .line 3400
    .end local v8           #token:I
    .end local v22           #agentSuccess:Z
    .end local v34           #toCopy:J
    :cond_220
    if-nez v29, :cond_7f

    #@222
    .line 3402
    iget-wide v4, v6, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@224
    const-wide/16 v9, 0x1ff

    #@226
    add-long/2addr v4, v9

    #@227
    const-wide/16 v9, -0x200

    #@229
    and-long v23, v4, v9

    #@22b
    .line 3403
    .local v23, bytesToConsume:J
    :goto_22b
    const-wide/16 v4, 0x0

    #@22d
    cmp-long v4, v23, v4

    #@22f
    if-lez v4, :cond_7f

    #@231
    .line 3404
    move-object/from16 v0, p2

    #@233
    array-length v4, v0

    #@234
    int-to-long v4, v4

    #@235
    cmp-long v4, v23, v4

    #@237
    if-lez v4, :cond_330

    #@239
    move-object/from16 v0, p2

    #@23b
    array-length v0, v0

    #@23c
    move/from16 v36, v0

    #@23e
    .line 3406
    .restart local v36       #toRead:I
    :goto_23e
    const/4 v4, 0x0

    #@23f
    move-object/from16 v0, p1

    #@241
    move-object/from16 v1, p2

    #@243
    move/from16 v2, v36

    #@245
    invoke-virtual {v0, v1, v4, v2}, Ljava/io/InputStream;->read([BII)I

    #@248
    move-result v4

    #@249
    int-to-long v0, v4

    #@24a
    move-wide/from16 v27, v0

    #@24c
    .line 3407
    .local v27, nRead:J
    const-wide/16 v4, 0x0

    #@24e
    cmp-long v4, v27, v4

    #@250
    if-ltz v4, :cond_25c

    #@252
    move-object/from16 v0, p0

    #@254
    iget-wide v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@256
    add-long v4, v4, v27

    #@258
    move-object/from16 v0, p0

    #@25a
    iput-wide v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@25c
    .line 3408
    :cond_25c
    const-wide/16 v4, 0x0

    #@25e
    cmp-long v4, v27, v4

    #@260
    if-lez v4, :cond_7f

    #@262
    .line 3409
    sub-long v23, v23, v27

    #@264
    .line 3410
    goto :goto_22b

    #@265
    .line 3208
    .end local v23           #bytesToConsume:J
    .end local v27           #nRead:J
    .end local v36           #toRead:I
    :pswitch_265
    const/16 v29, 0x0

    #@267
    .line 3209
    goto/16 :goto_b0

    #@269
    .line 3214
    :pswitch_269
    iget-object v4, v6, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@26b
    const-string v5, "a"

    #@26d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@270
    move-result v4

    #@271
    if-eqz v4, :cond_2a5

    #@273
    .line 3217
    move-object/from16 v0, p0

    #@275
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackageInstallers:Ljava/util/HashMap;

    #@277
    move-object/from16 v0, v32

    #@279
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27c
    move-result-object v26

    #@27d
    check-cast v26, Ljava/lang/String;

    #@27f
    .line 3218
    .local v26, installerName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@281
    move-object/from16 v1, v26

    #@283
    move-object/from16 v2, p1

    #@285
    invoke-virtual {v0, v6, v1, v2}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->installApk(Lcom/android/server/BackupManagerService$FileMetadata;Ljava/lang/String;Ljava/io/InputStream;)Z

    #@288
    move-result v29

    #@289
    .line 3220
    move-object/from16 v0, p0

    #@28b
    iget-object v5, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@28d
    if-eqz v29, :cond_2a2

    #@28f
    sget-object v4, Lcom/android/server/BackupManagerService$RestorePolicy;->ACCEPT:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@291
    :goto_291
    move-object/from16 v0, v32

    #@293
    invoke-virtual {v5, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@296
    .line 3226
    iget-wide v4, v6, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@298
    move-object/from16 v0, p0

    #@29a
    move-object/from16 v1, p1

    #@29c
    invoke-virtual {v0, v4, v5, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->skipTarPadding(JLjava/io/InputStream;)V

    #@29f
    .line 3227
    const/4 v4, 0x1

    #@2a0
    goto/16 :goto_82

    #@2a2
    .line 3220
    :cond_2a2
    sget-object v4, Lcom/android/server/BackupManagerService$RestorePolicy;->IGNORE:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@2a4
    goto :goto_291

    #@2a5
    .line 3231
    .end local v26           #installerName:Ljava/lang/String;
    :cond_2a5
    move-object/from16 v0, p0

    #@2a7
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@2a9
    sget-object v5, Lcom/android/server/BackupManagerService$RestorePolicy;->IGNORE:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@2ab
    move-object/from16 v0, v32

    #@2ad
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b0
    .line 3232
    const/16 v29, 0x0

    #@2b2
    .line 3234
    goto/16 :goto_b0

    #@2b4
    .line 3237
    :pswitch_2b4
    iget-object v4, v6, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@2b6
    const-string v5, "a"

    #@2b8
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2bb
    .catch Ljava/io/IOException; {:try_start_1a7 .. :try_end_2bb} :catch_337

    #@2bb
    move-result v4

    #@2bc
    if-eqz v4, :cond_b0

    #@2be
    .line 3243
    const/16 v29, 0x0

    #@2c0
    goto/16 :goto_b0

    #@2c2
    .line 3335
    .restart local v8       #token:I
    .restart local v22       #agentSuccess:Z
    .restart local v34       #toCopy:J
    :cond_2c2
    :try_start_2c2
    move-object/from16 v0, p0

    #@2c4
    iget-object v9, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@2c6
    move-object/from16 v0, p0

    #@2c8
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@2ca
    const/4 v5, 0x0

    #@2cb
    aget-object v10, v4, v5

    #@2cd
    iget-wide v11, v6, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@2cf
    iget v13, v6, Lcom/android/server/BackupManagerService$FileMetadata;->type:I

    #@2d1
    iget-object v14, v6, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@2d3
    iget-object v15, v6, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@2d5
    iget-wide v0, v6, Lcom/android/server/BackupManagerService$FileMetadata;->mode:J

    #@2d7
    move-wide/from16 v16, v0

    #@2d9
    iget-wide v0, v6, Lcom/android/server/BackupManagerService$FileMetadata;->mtime:J

    #@2db
    move-wide/from16 v18, v0

    #@2dd
    move-object/from16 v0, p0

    #@2df
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2e1
    iget-object v0, v4, Lcom/android/server/BackupManagerService;->mBackupManagerBinder:Landroid/app/backup/IBackupManager;

    #@2e3
    move-object/from16 v21, v0

    #@2e5
    move/from16 v20, v8

    #@2e7
    invoke-interface/range {v9 .. v21}, Landroid/app/IBackupAgent;->doRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJILandroid/app/backup/IBackupManager;)V
    :try_end_2ea
    .catch Ljava/io/IOException; {:try_start_2c2 .. :try_end_2ea} :catch_2ec
    .catch Landroid/os/RemoteException; {:try_start_2c2 .. :try_end_2ea} :catch_2fa

    #@2ea
    goto/16 :goto_1a3

    #@2ec
    .line 3339
    :catch_2ec
    move-exception v25

    #@2ed
    .line 3341
    .local v25, e:Ljava/io/IOException;
    :try_start_2ed
    const-string v4, "BackupManagerService"

    #@2ef
    const-string v5, "Couldn\'t establish restore"

    #@2f1
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f4
    .line 3342
    const/16 v22, 0x0

    #@2f6
    .line 3343
    const/16 v29, 0x0

    #@2f8
    .line 3350
    goto/16 :goto_1a3

    #@2fa
    .line 3344
    .end local v25           #e:Ljava/io/IOException;
    :catch_2fa
    move-exception v25

    #@2fb
    .line 3347
    .local v25, e:Landroid/os/RemoteException;
    const-string v4, "BackupManagerService"

    #@2fd
    const-string v5, "Agent crashed during full restore"

    #@2ff
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_302
    .catch Ljava/io/IOException; {:try_start_2ed .. :try_end_302} :catch_337

    #@302
    .line 3348
    const/16 v22, 0x0

    #@304
    .line 3349
    const/16 v29, 0x0

    #@306
    goto/16 :goto_1a3

    #@308
    .line 3358
    .end local v25           #e:Landroid/os/RemoteException;
    .restart local v30       #pipe:Ljava/io/FileOutputStream;
    .restart local v31       #pipeOkay:Z
    :cond_308
    move-wide/from16 v0, v34

    #@30a
    long-to-int v0, v0

    #@30b
    move/from16 v36, v0

    #@30d
    goto/16 :goto_1cc

    #@30f
    .line 3363
    .local v27, nRead:I
    .restart local v36       #toRead:I
    :cond_30f
    move/from16 v0, v27

    #@311
    int-to-long v4, v0

    #@312
    sub-long v34, v34, v4

    #@314
    .line 3367
    if-eqz v31, :cond_1b9

    #@316
    .line 3369
    const/4 v4, 0x0

    #@317
    :try_start_317
    move-object/from16 v0, v30

    #@319
    move-object/from16 v1, p2

    #@31b
    move/from16 v2, v27

    #@31d
    invoke-virtual {v0, v1, v4, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_320
    .catch Ljava/io/IOException; {:try_start_317 .. :try_end_320} :catch_322

    #@320
    goto/16 :goto_1b9

    #@322
    .line 3370
    :catch_322
    move-exception v25

    #@323
    .line 3371
    .local v25, e:Ljava/io/IOException;
    :try_start_323
    const-string v4, "BackupManagerService"

    #@325
    const-string v5, "Failed to write to restore pipe"

    #@327
    move-object/from16 v0, v25

    #@329
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_32c
    .catch Ljava/io/IOException; {:try_start_323 .. :try_end_32c} :catch_337

    #@32c
    .line 3372
    const/16 v31, 0x0

    #@32e
    goto/16 :goto_1b9

    #@330
    .line 3404
    .end local v8           #token:I
    .end local v22           #agentSuccess:Z
    .end local v25           #e:Ljava/io/IOException;
    .end local v27           #nRead:I
    .end local v30           #pipe:Ljava/io/FileOutputStream;
    .end local v31           #pipeOkay:Z
    .end local v34           #toCopy:J
    .end local v36           #toRead:I
    .restart local v23       #bytesToConsume:J
    :cond_330
    move-wide/from16 v0, v23

    #@332
    long-to-int v0, v0

    #@333
    move/from16 v36, v0

    #@335
    goto/16 :goto_23e

    #@337
    .line 3414
    .end local v6           #info:Lcom/android/server/BackupManagerService$FileMetadata;
    .end local v23           #bytesToConsume:J
    .end local v29           #okay:Z
    .end local v32           #pkg:Ljava/lang/String;
    .end local v33           #policy:Lcom/android/server/BackupManagerService$RestorePolicy;
    :catch_337
    move-exception v25

    #@338
    .line 3417
    .restart local v25       #e:Ljava/io/IOException;
    const/4 v6, 0x0

    #@339
    .restart local v6       #info:Lcom/android/server/BackupManagerService$FileMetadata;
    goto/16 :goto_7f

    #@33b
    .line 3420
    .end local v25           #e:Ljava/io/IOException;
    :cond_33b
    const/4 v4, 0x0

    #@33c
    goto/16 :goto_82

    #@33e
    .line 3293
    .restart local v29       #okay:Z
    .restart local v32       #pkg:Ljava/lang/String;
    .restart local v33       #policy:Lcom/android/server/BackupManagerService$RestorePolicy;
    :catch_33e
    move-exception v4

    #@33f
    goto/16 :goto_10b

    #@341
    .line 3291
    :catch_341
    move-exception v4

    #@342
    goto/16 :goto_10b

    #@344
    .line 3206
    :pswitch_data_344
    .packed-switch 0x1
        :pswitch_265
        :pswitch_269
        :pswitch_2b4
    .end packed-switch
.end method

.method public run()V
    .registers 23

    #@0
    .prologue
    .line 2982
    const-string v18, "BackupManagerService"

    #@2
    const-string v19, "--- Performing full-dataset restore ---"

    #@4
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2983
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->sendStartRestore()V

    #@a
    .line 2986
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@d
    move-result-object v18

    #@e
    const-string v19, "mounted"

    #@10
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v18

    #@14
    if-eqz v18, :cond_23

    #@16
    .line 2987
    move-object/from16 v0, p0

    #@18
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPackagePolicies:Ljava/util/HashMap;

    #@1a
    move-object/from16 v18, v0

    #@1c
    const-string v19, "com.android.sharedstoragebackup"

    #@1e
    sget-object v20, Lcom/android/server/BackupManagerService$RestorePolicy;->ACCEPT:Lcom/android/server/BackupManagerService$RestorePolicy;

    #@20
    invoke-virtual/range {v18 .. v20}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    .line 2990
    :cond_23
    const/4 v14, 0x0

    #@24
    .line 2991
    .local v14, rawInStream:Ljava/io/FileInputStream;
    const/4 v12, 0x0

    #@25
    .line 2993
    .local v12, rawDataIn:Ljava/io/DataInputStream;
    :try_start_25
    move-object/from16 v0, p0

    #@27
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@29
    move-object/from16 v18, v0

    #@2b
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/BackupManagerService;->hasBackupPassword()Z

    #@2e
    move-result v18

    #@2f
    if-eqz v18, :cond_bf

    #@31
    .line 2994
    move-object/from16 v0, p0

    #@33
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@35
    move-object/from16 v18, v0

    #@37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mCurrentPassword:Ljava/lang/String;

    #@3b
    move-object/from16 v19, v0

    #@3d
    const/16 v20, 0x2710

    #@3f
    invoke-virtual/range {v18 .. v20}, Lcom/android/server/BackupManagerService;->passwordMatchesSaved(Ljava/lang/String;I)Z
    :try_end_42
    .catchall {:try_start_25 .. :try_end_42} :catchall_400
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_42} :catch_407

    #@42
    move-result v18

    #@43
    if-nez v18, :cond_bf

    #@45
    .line 3052
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@48
    .line 3053
    move-object/from16 v0, p0

    #@4a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@4c
    move-object/from16 v18, v0

    #@4e
    move-object/from16 v0, p0

    #@50
    move-object/from16 v1, v18

    #@52
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownAgent(Landroid/content/pm/ApplicationInfo;)V

    #@55
    .line 3056
    if-eqz v12, :cond_5a

    #@57
    :try_start_57
    #Replaced unresolvable odex instruction with a throw
    throw v12
    #invoke-virtual-quick {v12}, vtable@0xc

    #@5a
    .line 3057
    :cond_5a
    if-eqz v14, :cond_5f

    #@5c
    #Replaced unresolvable odex instruction with a throw
    throw v14
    #invoke-virtual-quick {v14}, vtable@0xc

    #@5f
    .line 3058
    :cond_5f
    move-object/from16 v0, p0

    #@61
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInputFile:Landroid/os/ParcelFileDescriptor;

    #@63
    move-object/from16 v18, v0

    #@65
    invoke-virtual/range {v18 .. v18}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_68
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_68} :catch_3c4

    #@68
    .line 3063
    :goto_68
    move-object/from16 v0, p0

    #@6a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@6c
    move-object/from16 v18, v0

    #@6e
    move-object/from16 v0, v18

    #@70
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@72
    move-object/from16 v19, v0

    #@74
    monitor-enter v19

    #@75
    .line 3064
    :try_start_75
    move-object/from16 v0, p0

    #@77
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@79
    move-object/from16 v18, v0

    #@7b
    move-object/from16 v0, v18

    #@7d
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@7f
    move-object/from16 v18, v0

    #@81
    invoke-virtual/range {v18 .. v18}, Landroid/util/SparseArray;->clear()V

    #@84
    .line 3065
    monitor-exit v19
    :try_end_85
    .catchall {:try_start_75 .. :try_end_85} :catchall_3d2

    #@85
    .line 3066
    move-object/from16 v0, p0

    #@87
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@89
    move-object/from16 v19, v0

    #@8b
    monitor-enter v19

    #@8c
    .line 3067
    :try_start_8c
    move-object/from16 v0, p0

    #@8e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@90
    move-object/from16 v18, v0

    #@92
    const/16 v20, 0x1

    #@94
    move-object/from16 v0, v18

    #@96
    move/from16 v1, v20

    #@98
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@9b
    .line 3068
    move-object/from16 v0, p0

    #@9d
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@9f
    move-object/from16 v18, v0

    #@a1
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    #@a4
    .line 3069
    monitor-exit v19
    :try_end_a5
    .catchall {:try_start_8c .. :try_end_a5} :catchall_3d5

    #@a5
    .line 3070
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->sendEndRestore()V

    #@a8
    .line 3071
    const-string v18, "BackupManagerService"

    #@aa
    const-string v19, "Full restore pass complete."

    #@ac
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    .line 3072
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@b3
    move-object/from16 v18, v0

    #@b5
    move-object/from16 v0, v18

    #@b7
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@b9
    move-object/from16 v18, v0

    #@bb
    :goto_bb
    invoke-virtual/range {v18 .. v18}, Landroid/os/PowerManager$WakeLock;->release()V

    #@be
    .line 3074
    :goto_be
    return-void

    #@bf
    .line 3000
    :cond_bf
    const-wide/16 v18, 0x0

    #@c1
    :try_start_c1
    move-wide/from16 v0, v18

    #@c3
    move-object/from16 v2, p0

    #@c5
    iput-wide v0, v2, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@c7
    .line 3001
    const v18, 0x8000

    #@ca
    move/from16 v0, v18

    #@cc
    new-array v3, v0, [B

    #@ce
    .line 3002
    .local v3, buffer:[B
    new-instance v15, Ljava/io/FileInputStream;

    #@d0
    move-object/from16 v0, p0

    #@d2
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInputFile:Landroid/os/ParcelFileDescriptor;

    #@d4
    move-object/from16 v18, v0

    #@d6
    invoke-virtual/range {v18 .. v18}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@d9
    move-result-object v18

    #@da
    move-object/from16 v0, v18

    #@dc
    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_df
    .catchall {:try_start_c1 .. :try_end_df} :catchall_400
    .catch Ljava/io/IOException; {:try_start_c1 .. :try_end_df} :catch_407

    #@df
    .line 3003
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .local v15, rawInStream:Ljava/io/FileInputStream;
    :try_start_df
    new-instance v13, Ljava/io/DataInputStream;

    #@e1
    invoke-direct {v13, v15}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_e4
    .catchall {:try_start_df .. :try_end_e4} :catchall_403
    .catch Ljava/io/IOException; {:try_start_df .. :try_end_e4} :catch_40a

    #@e4
    .line 3006
    .end local v12           #rawDataIn:Ljava/io/DataInputStream;
    .local v13, rawDataIn:Ljava/io/DataInputStream;
    const/4 v4, 0x0

    #@e5
    .line 3007
    .local v4, compressed:Z
    move-object v11, v15

    #@e6
    .line 3010
    .local v11, preCompressStream:Ljava/io/InputStream;
    const/4 v10, 0x0

    #@e7
    .line 3011
    .local v10, okay:Z
    :try_start_e7
    const-string v18, "ANDROID BACKUP\n"

    #@e9
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@ec
    move-result v7

    #@ed
    .line 3012
    .local v7, headerLen:I
    new-array v0, v7, [B

    #@ef
    move-object/from16 v17, v0

    #@f1
    .line 3013
    .local v17, streamHeader:[B
    move-object/from16 v0, v17

    #@f3
    invoke-virtual {v13, v0}, Ljava/io/DataInputStream;->readFully([B)V

    #@f6
    .line 3014
    const-string v18, "ANDROID BACKUP\n"

    #@f8
    const-string v19, "UTF-8"

    #@fa
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@fd
    move-result-object v9

    #@fe
    .line 3015
    .local v9, magicBytes:[B
    move-object/from16 v0, v17

    #@100
    invoke-static {v9, v0}, Ljava/util/Arrays;->equals([B[B)Z

    #@103
    move-result v18

    #@104
    if-eqz v18, :cond_304

    #@106
    .line 3017
    move-object/from16 v0, p0

    #@108
    invoke-virtual {v0, v15}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@10b
    move-result-object v16

    #@10c
    .line 3018
    .local v16, s:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@10f
    move-result v18

    #@110
    const/16 v19, 0x1

    #@112
    move/from16 v0, v18

    #@114
    move/from16 v1, v19

    #@116
    if-ne v0, v1, :cond_26d

    #@118
    .line 3020
    move-object/from16 v0, p0

    #@11a
    invoke-virtual {v0, v15}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@11d
    move-result-object v16

    #@11e
    .line 3021
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@121
    move-result v18

    #@122
    if-eqz v18, :cond_1be

    #@124
    const/4 v4, 0x1

    #@125
    .line 3022
    :goto_125
    move-object/from16 v0, p0

    #@127
    invoke-virtual {v0, v15}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;

    #@12a
    move-result-object v16

    #@12b
    .line 3023
    const-string v18, "none"

    #@12d
    move-object/from16 v0, v16

    #@12f
    move-object/from16 v1, v18

    #@131
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@134
    move-result v18

    #@135
    if-eqz v18, :cond_1c1

    #@137
    .line 3025
    const/4 v10, 0x1

    #@138
    .line 3035
    .end local v16           #s:Ljava/lang/String;
    :cond_138
    :goto_138
    if-nez v10, :cond_30d

    #@13a
    .line 3036
    const-string v18, "BackupManagerService"

    #@13c
    const-string v19, "Invalid restore data; aborting."

    #@13e
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_141
    .catchall {:try_start_e7 .. :try_end_141} :catchall_28b
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_141} :catch_1eb

    #@141
    .line 3052
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@144
    .line 3053
    move-object/from16 v0, p0

    #@146
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@148
    move-object/from16 v18, v0

    #@14a
    move-object/from16 v0, p0

    #@14c
    move-object/from16 v1, v18

    #@14e
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownAgent(Landroid/content/pm/ApplicationInfo;)V

    #@151
    .line 3056
    if-eqz v13, :cond_156

    #@153
    :try_start_153
    invoke-virtual {v13}, Ljava/io/DataInputStream;->close()V

    #@156
    .line 3057
    :cond_156
    if-eqz v15, :cond_15b

    #@158
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    #@15b
    .line 3058
    :cond_15b
    move-object/from16 v0, p0

    #@15d
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInputFile:Landroid/os/ParcelFileDescriptor;

    #@15f
    move-object/from16 v18, v0

    #@161
    invoke-virtual/range {v18 .. v18}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_164
    .catch Ljava/io/IOException; {:try_start_153 .. :try_end_164} :catch_3d8

    #@164
    .line 3063
    :goto_164
    move-object/from16 v0, p0

    #@166
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@168
    move-object/from16 v18, v0

    #@16a
    move-object/from16 v0, v18

    #@16c
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@16e
    move-object/from16 v19, v0

    #@170
    monitor-enter v19

    #@171
    .line 3064
    :try_start_171
    move-object/from16 v0, p0

    #@173
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@175
    move-object/from16 v18, v0

    #@177
    move-object/from16 v0, v18

    #@179
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@17b
    move-object/from16 v18, v0

    #@17d
    invoke-virtual/range {v18 .. v18}, Landroid/util/SparseArray;->clear()V

    #@180
    .line 3065
    monitor-exit v19
    :try_end_181
    .catchall {:try_start_171 .. :try_end_181} :catchall_3e6

    #@181
    .line 3066
    move-object/from16 v0, p0

    #@183
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@185
    move-object/from16 v19, v0

    #@187
    monitor-enter v19

    #@188
    .line 3067
    :try_start_188
    move-object/from16 v0, p0

    #@18a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@18c
    move-object/from16 v18, v0

    #@18e
    const/16 v20, 0x1

    #@190
    move-object/from16 v0, v18

    #@192
    move/from16 v1, v20

    #@194
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@197
    .line 3068
    move-object/from16 v0, p0

    #@199
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@19b
    move-object/from16 v18, v0

    #@19d
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    #@1a0
    .line 3069
    monitor-exit v19
    :try_end_1a1
    .catchall {:try_start_188 .. :try_end_1a1} :catchall_3e9

    #@1a1
    .line 3070
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->sendEndRestore()V

    #@1a4
    .line 3071
    const-string v18, "BackupManagerService"

    #@1a6
    const-string v19, "Full restore pass complete."

    #@1a8
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ab
    .line 3072
    move-object/from16 v0, p0

    #@1ad
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1af
    move-object/from16 v18, v0

    #@1b1
    move-object/from16 v0, v18

    #@1b3
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@1b5
    move-object/from16 v18, v0

    #@1b7
    invoke-virtual/range {v18 .. v18}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1ba
    move-object v12, v13

    #@1bb
    .end local v13           #rawDataIn:Ljava/io/DataInputStream;
    .restart local v12       #rawDataIn:Ljava/io/DataInputStream;
    move-object v14, v15

    #@1bc
    .line 3037
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    goto/16 :goto_be

    #@1be
    .line 3021
    .end local v12           #rawDataIn:Ljava/io/DataInputStream;
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .restart local v13       #rawDataIn:Ljava/io/DataInputStream;
    .restart local v15       #rawInStream:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/lang/String;
    :cond_1be
    const/4 v4, 0x0

    #@1bf
    goto/16 :goto_125

    #@1c1
    .line 3026
    :cond_1c1
    :try_start_1c1
    move-object/from16 v0, p0

    #@1c3
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDecryptPassword:Ljava/lang/String;

    #@1c5
    move-object/from16 v18, v0

    #@1c7
    if-eqz v18, :cond_1e2

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mDecryptPassword:Ljava/lang/String;

    #@1cd
    move-object/from16 v18, v0

    #@1cf
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@1d2
    move-result v18

    #@1d3
    if-lez v18, :cond_1e2

    #@1d5
    .line 3027
    move-object/from16 v0, p0

    #@1d7
    move-object/from16 v1, v16

    #@1d9
    invoke-virtual {v0, v1, v15}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->decodeAesHeaderAndInitialize(Ljava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;

    #@1dc
    move-result-object v11

    #@1dd
    .line 3028
    if-eqz v11, :cond_138

    #@1df
    .line 3029
    const/4 v10, 0x1

    #@1e0
    goto/16 :goto_138

    #@1e2
    .line 3031
    :cond_1e2
    const-string v18, "BackupManagerService"

    #@1e4
    const-string v19, "Archive is encrypted but no password given"

    #@1e6
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e9
    .catchall {:try_start_1c1 .. :try_end_1e9} :catchall_28b
    .catch Ljava/io/IOException; {:try_start_1c1 .. :try_end_1e9} :catch_1eb

    #@1e9
    goto/16 :goto_138

    #@1eb
    .line 3049
    .end local v7           #headerLen:I
    .end local v9           #magicBytes:[B
    .end local v16           #s:Ljava/lang/String;
    .end local v17           #streamHeader:[B
    :catch_1eb
    move-exception v6

    #@1ec
    move-object v12, v13

    #@1ed
    .end local v13           #rawDataIn:Ljava/io/DataInputStream;
    .restart local v12       #rawDataIn:Ljava/io/DataInputStream;
    move-object v14, v15

    #@1ee
    .line 3050
    .end local v3           #buffer:[B
    .end local v4           #compressed:Z
    .end local v10           #okay:Z
    .end local v11           #preCompressStream:Ljava/io/InputStream;
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .local v6, e:Ljava/io/IOException;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    :goto_1ee
    :try_start_1ee
    const-string v18, "BackupManagerService"

    #@1f0
    const-string v19, "Unable to read restore input"

    #@1f2
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f5
    .catchall {:try_start_1ee .. :try_end_1f5} :catchall_400

    #@1f5
    .line 3052
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@1f8
    .line 3053
    move-object/from16 v0, p0

    #@1fa
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@1fc
    move-object/from16 v18, v0

    #@1fe
    move-object/from16 v0, p0

    #@200
    move-object/from16 v1, v18

    #@202
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownAgent(Landroid/content/pm/ApplicationInfo;)V

    #@205
    .line 3056
    if-eqz v12, :cond_20a

    #@207
    :try_start_207
    invoke-virtual {v12}, Ljava/io/DataInputStream;->close()V

    #@20a
    .line 3057
    :cond_20a
    if-eqz v14, :cond_20f

    #@20c
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    #@20f
    .line 3058
    :cond_20f
    move-object/from16 v0, p0

    #@211
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInputFile:Landroid/os/ParcelFileDescriptor;

    #@213
    move-object/from16 v18, v0

    #@215
    invoke-virtual/range {v18 .. v18}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_218
    .catch Ljava/io/IOException; {:try_start_207 .. :try_end_218} :catch_3b0

    #@218
    .line 3063
    :goto_218
    move-object/from16 v0, p0

    #@21a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@21c
    move-object/from16 v18, v0

    #@21e
    move-object/from16 v0, v18

    #@220
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@222
    move-object/from16 v19, v0

    #@224
    monitor-enter v19

    #@225
    .line 3064
    :try_start_225
    move-object/from16 v0, p0

    #@227
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@229
    move-object/from16 v18, v0

    #@22b
    move-object/from16 v0, v18

    #@22d
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@22f
    move-object/from16 v18, v0

    #@231
    invoke-virtual/range {v18 .. v18}, Landroid/util/SparseArray;->clear()V

    #@234
    .line 3065
    monitor-exit v19
    :try_end_235
    .catchall {:try_start_225 .. :try_end_235} :catchall_3be

    #@235
    .line 3066
    move-object/from16 v0, p0

    #@237
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@239
    move-object/from16 v19, v0

    #@23b
    monitor-enter v19

    #@23c
    .line 3067
    :try_start_23c
    move-object/from16 v0, p0

    #@23e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@240
    move-object/from16 v18, v0

    #@242
    const/16 v20, 0x1

    #@244
    move-object/from16 v0, v18

    #@246
    move/from16 v1, v20

    #@248
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@24b
    .line 3068
    move-object/from16 v0, p0

    #@24d
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@24f
    move-object/from16 v18, v0

    #@251
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    #@254
    .line 3069
    monitor-exit v19
    :try_end_255
    .catchall {:try_start_23c .. :try_end_255} :catchall_3c1

    #@255
    .line 3070
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->sendEndRestore()V

    #@258
    .line 3071
    const-string v18, "BackupManagerService"

    #@25a
    const-string v19, "Full restore pass complete."

    #@25c
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25f
    .line 3072
    move-object/from16 v0, p0

    #@261
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@263
    move-object/from16 v18, v0

    #@265
    move-object/from16 v0, v18

    #@267
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@269
    move-object/from16 v18, v0

    #@26b
    goto/16 :goto_bb

    #@26d
    .line 3032
    .end local v6           #e:Ljava/io/IOException;
    .end local v12           #rawDataIn:Ljava/io/DataInputStream;
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .restart local v3       #buffer:[B
    .restart local v4       #compressed:Z
    .restart local v7       #headerLen:I
    .restart local v9       #magicBytes:[B
    .restart local v10       #okay:Z
    .restart local v11       #preCompressStream:Ljava/io/InputStream;
    .restart local v13       #rawDataIn:Ljava/io/DataInputStream;
    .restart local v15       #rawInStream:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/lang/String;
    .restart local v17       #streamHeader:[B
    :cond_26d
    :try_start_26d
    const-string v18, "BackupManagerService"

    #@26f
    new-instance v19, Ljava/lang/StringBuilder;

    #@271
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@274
    const-string v20, "Wrong header version: "

    #@276
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@279
    move-result-object v19

    #@27a
    move-object/from16 v0, v19

    #@27c
    move-object/from16 v1, v16

    #@27e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@281
    move-result-object v19

    #@282
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@285
    move-result-object v19

    #@286
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_289
    .catchall {:try_start_26d .. :try_end_289} :catchall_28b
    .catch Ljava/io/IOException; {:try_start_26d .. :try_end_289} :catch_1eb

    #@289
    goto/16 :goto_138

    #@28b
    .line 3052
    .end local v7           #headerLen:I
    .end local v9           #magicBytes:[B
    .end local v16           #s:Ljava/lang/String;
    .end local v17           #streamHeader:[B
    :catchall_28b
    move-exception v18

    #@28c
    move-object v12, v13

    #@28d
    .end local v13           #rawDataIn:Ljava/io/DataInputStream;
    .restart local v12       #rawDataIn:Ljava/io/DataInputStream;
    move-object v14, v15

    #@28e
    .end local v3           #buffer:[B
    .end local v4           #compressed:Z
    .end local v10           #okay:Z
    .end local v11           #preCompressStream:Ljava/io/InputStream;
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    :goto_28e
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@291
    .line 3053
    move-object/from16 v0, p0

    #@293
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@295
    move-object/from16 v19, v0

    #@297
    move-object/from16 v0, p0

    #@299
    move-object/from16 v1, v19

    #@29b
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownAgent(Landroid/content/pm/ApplicationInfo;)V

    #@29e
    .line 3056
    if-eqz v12, :cond_2a3

    #@2a0
    :try_start_2a0
    invoke-virtual {v12}, Ljava/io/DataInputStream;->close()V

    #@2a3
    .line 3057
    :cond_2a3
    if-eqz v14, :cond_2a8

    #@2a5
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    #@2a8
    .line 3058
    :cond_2a8
    move-object/from16 v0, p0

    #@2aa
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInputFile:Landroid/os/ParcelFileDescriptor;

    #@2ac
    move-object/from16 v19, v0

    #@2ae
    invoke-virtual/range {v19 .. v19}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2b1
    .catch Ljava/io/IOException; {:try_start_2a0 .. :try_end_2b1} :catch_39c

    #@2b1
    .line 3063
    :goto_2b1
    move-object/from16 v0, p0

    #@2b3
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2b5
    move-object/from16 v19, v0

    #@2b7
    move-object/from16 v0, v19

    #@2b9
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@2bb
    move-object/from16 v19, v0

    #@2bd
    monitor-enter v19

    #@2be
    .line 3064
    :try_start_2be
    move-object/from16 v0, p0

    #@2c0
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2c2
    move-object/from16 v20, v0

    #@2c4
    move-object/from16 v0, v20

    #@2c6
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@2c8
    move-object/from16 v20, v0

    #@2ca
    invoke-virtual/range {v20 .. v20}, Landroid/util/SparseArray;->clear()V

    #@2cd
    .line 3065
    monitor-exit v19
    :try_end_2ce
    .catchall {:try_start_2be .. :try_end_2ce} :catchall_3aa

    #@2ce
    .line 3066
    move-object/from16 v0, p0

    #@2d0
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2d2
    move-object/from16 v19, v0

    #@2d4
    monitor-enter v19

    #@2d5
    .line 3067
    :try_start_2d5
    move-object/from16 v0, p0

    #@2d7
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2d9
    move-object/from16 v20, v0

    #@2db
    const/16 v21, 0x1

    #@2dd
    invoke-virtual/range {v20 .. v21}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@2e0
    .line 3068
    move-object/from16 v0, p0

    #@2e2
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2e4
    move-object/from16 v20, v0

    #@2e6
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->notifyAll()V

    #@2e9
    .line 3069
    monitor-exit v19
    :try_end_2ea
    .catchall {:try_start_2d5 .. :try_end_2ea} :catchall_3ad

    #@2ea
    .line 3070
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->sendEndRestore()V

    #@2ed
    .line 3071
    const-string v19, "BackupManagerService"

    #@2ef
    const-string v20, "Full restore pass complete."

    #@2f1
    invoke-static/range {v19 .. v20}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f4
    .line 3072
    move-object/from16 v0, p0

    #@2f6
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2f8
    move-object/from16 v19, v0

    #@2fa
    move-object/from16 v0, v19

    #@2fc
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@2fe
    move-object/from16 v19, v0

    #@300
    invoke-virtual/range {v19 .. v19}, Landroid/os/PowerManager$WakeLock;->release()V

    #@303
    .line 3052
    throw v18

    #@304
    .line 3033
    .end local v12           #rawDataIn:Ljava/io/DataInputStream;
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .restart local v3       #buffer:[B
    .restart local v4       #compressed:Z
    .restart local v7       #headerLen:I
    .restart local v9       #magicBytes:[B
    .restart local v10       #okay:Z
    .restart local v11       #preCompressStream:Ljava/io/InputStream;
    .restart local v13       #rawDataIn:Ljava/io/DataInputStream;
    .restart local v15       #rawInStream:Ljava/io/FileInputStream;
    .restart local v17       #streamHeader:[B
    :cond_304
    :try_start_304
    const-string v18, "BackupManagerService"

    #@306
    const-string v19, "Didn\'t read the right header magic"

    #@308
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30b
    goto/16 :goto_138

    #@30d
    .line 3041
    :cond_30d
    if-eqz v4, :cond_399

    #@30f
    new-instance v8, Ljava/util/zip/InflaterInputStream;

    #@311
    invoke-direct {v8, v11}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    #@314
    .line 3045
    .local v8, in:Ljava/io/InputStream;
    :cond_314
    :goto_314
    move-object/from16 v0, p0

    #@316
    invoke-virtual {v0, v8, v3}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->restoreOneFile(Ljava/io/InputStream;[B)Z
    :try_end_319
    .catchall {:try_start_304 .. :try_end_319} :catchall_28b
    .catch Ljava/io/IOException; {:try_start_304 .. :try_end_319} :catch_1eb

    #@319
    move-result v5

    #@31a
    .line 3046
    .local v5, didRestore:Z
    if-nez v5, :cond_314

    #@31c
    .line 3052
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownPipes()V

    #@31f
    .line 3053
    move-object/from16 v0, p0

    #@321
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mTargetApp:Landroid/content/pm/ApplicationInfo;

    #@323
    move-object/from16 v18, v0

    #@325
    move-object/from16 v0, p0

    #@327
    move-object/from16 v1, v18

    #@329
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->tearDownAgent(Landroid/content/pm/ApplicationInfo;)V

    #@32c
    .line 3056
    if-eqz v13, :cond_331

    #@32e
    :try_start_32e
    invoke-virtual {v13}, Ljava/io/DataInputStream;->close()V

    #@331
    .line 3057
    :cond_331
    if-eqz v15, :cond_336

    #@333
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    #@336
    .line 3058
    :cond_336
    move-object/from16 v0, p0

    #@338
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mInputFile:Landroid/os/ParcelFileDescriptor;

    #@33a
    move-object/from16 v18, v0

    #@33c
    invoke-virtual/range {v18 .. v18}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_33f
    .catch Ljava/io/IOException; {:try_start_32e .. :try_end_33f} :catch_3ec

    #@33f
    .line 3063
    :goto_33f
    move-object/from16 v0, p0

    #@341
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@343
    move-object/from16 v18, v0

    #@345
    move-object/from16 v0, v18

    #@347
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@349
    move-object/from16 v19, v0

    #@34b
    monitor-enter v19

    #@34c
    .line 3064
    :try_start_34c
    move-object/from16 v0, p0

    #@34e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@350
    move-object/from16 v18, v0

    #@352
    move-object/from16 v0, v18

    #@354
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@356
    move-object/from16 v18, v0

    #@358
    invoke-virtual/range {v18 .. v18}, Landroid/util/SparseArray;->clear()V

    #@35b
    .line 3065
    monitor-exit v19
    :try_end_35c
    .catchall {:try_start_34c .. :try_end_35c} :catchall_3fa

    #@35c
    .line 3066
    move-object/from16 v0, p0

    #@35e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@360
    move-object/from16 v19, v0

    #@362
    monitor-enter v19

    #@363
    .line 3067
    :try_start_363
    move-object/from16 v0, p0

    #@365
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@367
    move-object/from16 v18, v0

    #@369
    const/16 v20, 0x1

    #@36b
    move-object/from16 v0, v18

    #@36d
    move/from16 v1, v20

    #@36f
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@372
    .line 3068
    move-object/from16 v0, p0

    #@374
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@376
    move-object/from16 v18, v0

    #@378
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    #@37b
    .line 3069
    monitor-exit v19
    :try_end_37c
    .catchall {:try_start_363 .. :try_end_37c} :catchall_3fd

    #@37c
    .line 3070
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->sendEndRestore()V

    #@37f
    .line 3071
    const-string v18, "BackupManagerService"

    #@381
    const-string v19, "Full restore pass complete."

    #@383
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@386
    .line 3072
    move-object/from16 v0, p0

    #@388
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@38a
    move-object/from16 v18, v0

    #@38c
    move-object/from16 v0, v18

    #@38e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@390
    move-object/from16 v18, v0

    #@392
    invoke-virtual/range {v18 .. v18}, Landroid/os/PowerManager$WakeLock;->release()V

    #@395
    move-object v12, v13

    #@396
    .end local v13           #rawDataIn:Ljava/io/DataInputStream;
    .restart local v12       #rawDataIn:Ljava/io/DataInputStream;
    move-object v14, v15

    #@397
    .line 3073
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    goto/16 :goto_be

    #@399
    .end local v5           #didRestore:Z
    .end local v8           #in:Ljava/io/InputStream;
    .end local v12           #rawDataIn:Ljava/io/DataInputStream;
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .restart local v13       #rawDataIn:Ljava/io/DataInputStream;
    .restart local v15       #rawInStream:Ljava/io/FileInputStream;
    :cond_399
    move-object v8, v11

    #@39a
    .line 3041
    goto/16 :goto_314

    #@39c
    .line 3059
    .end local v3           #buffer:[B
    .end local v4           #compressed:Z
    .end local v7           #headerLen:I
    .end local v9           #magicBytes:[B
    .end local v10           #okay:Z
    .end local v11           #preCompressStream:Ljava/io/InputStream;
    .end local v13           #rawDataIn:Ljava/io/DataInputStream;
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .end local v17           #streamHeader:[B
    .restart local v12       #rawDataIn:Ljava/io/DataInputStream;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    :catch_39c
    move-exception v6

    #@39d
    .line 3060
    .restart local v6       #e:Ljava/io/IOException;
    const-string v19, "BackupManagerService"

    #@39f
    const-string v20, "Close of restore data pipe threw"

    #@3a1
    move-object/from16 v0, v19

    #@3a3
    move-object/from16 v1, v20

    #@3a5
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3a8
    goto/16 :goto_2b1

    #@3aa
    .line 3065
    .end local v6           #e:Ljava/io/IOException;
    :catchall_3aa
    move-exception v18

    #@3ab
    :try_start_3ab
    monitor-exit v19
    :try_end_3ac
    .catchall {:try_start_3ab .. :try_end_3ac} :catchall_3aa

    #@3ac
    throw v18

    #@3ad
    .line 3069
    :catchall_3ad
    move-exception v18

    #@3ae
    :try_start_3ae
    monitor-exit v19
    :try_end_3af
    .catchall {:try_start_3ae .. :try_end_3af} :catchall_3ad

    #@3af
    throw v18

    #@3b0
    .line 3059
    .restart local v6       #e:Ljava/io/IOException;
    :catch_3b0
    move-exception v6

    #@3b1
    .line 3060
    const-string v18, "BackupManagerService"

    #@3b3
    const-string v19, "Close of restore data pipe threw"

    #@3b5
    move-object/from16 v0, v18

    #@3b7
    move-object/from16 v1, v19

    #@3b9
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3bc
    goto/16 :goto_218

    #@3be
    .line 3065
    :catchall_3be
    move-exception v18

    #@3bf
    :try_start_3bf
    monitor-exit v19
    :try_end_3c0
    .catchall {:try_start_3bf .. :try_end_3c0} :catchall_3be

    #@3c0
    throw v18

    #@3c1
    .line 3069
    :catchall_3c1
    move-exception v18

    #@3c2
    :try_start_3c2
    monitor-exit v19
    :try_end_3c3
    .catchall {:try_start_3c2 .. :try_end_3c3} :catchall_3c1

    #@3c3
    throw v18

    #@3c4
    .line 3059
    .end local v6           #e:Ljava/io/IOException;
    :catch_3c4
    move-exception v6

    #@3c5
    .line 3060
    .restart local v6       #e:Ljava/io/IOException;
    const-string v18, "BackupManagerService"

    #@3c7
    const-string v19, "Close of restore data pipe threw"

    #@3c9
    move-object/from16 v0, v18

    #@3cb
    move-object/from16 v1, v19

    #@3cd
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d0
    goto/16 :goto_68

    #@3d2
    .line 3065
    .end local v6           #e:Ljava/io/IOException;
    :catchall_3d2
    move-exception v18

    #@3d3
    :try_start_3d3
    monitor-exit v19
    :try_end_3d4
    .catchall {:try_start_3d3 .. :try_end_3d4} :catchall_3d2

    #@3d4
    throw v18

    #@3d5
    .line 3069
    :catchall_3d5
    move-exception v18

    #@3d6
    :try_start_3d6
    monitor-exit v19
    :try_end_3d7
    .catchall {:try_start_3d6 .. :try_end_3d7} :catchall_3d5

    #@3d7
    throw v18

    #@3d8
    .line 3059
    .end local v12           #rawDataIn:Ljava/io/DataInputStream;
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .restart local v3       #buffer:[B
    .restart local v4       #compressed:Z
    .restart local v7       #headerLen:I
    .restart local v9       #magicBytes:[B
    .restart local v10       #okay:Z
    .restart local v11       #preCompressStream:Ljava/io/InputStream;
    .restart local v13       #rawDataIn:Ljava/io/DataInputStream;
    .restart local v15       #rawInStream:Ljava/io/FileInputStream;
    .restart local v17       #streamHeader:[B
    :catch_3d8
    move-exception v6

    #@3d9
    .line 3060
    .restart local v6       #e:Ljava/io/IOException;
    const-string v18, "BackupManagerService"

    #@3db
    const-string v19, "Close of restore data pipe threw"

    #@3dd
    move-object/from16 v0, v18

    #@3df
    move-object/from16 v1, v19

    #@3e1
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3e4
    goto/16 :goto_164

    #@3e6
    .line 3065
    .end local v6           #e:Ljava/io/IOException;
    :catchall_3e6
    move-exception v18

    #@3e7
    :try_start_3e7
    monitor-exit v19
    :try_end_3e8
    .catchall {:try_start_3e7 .. :try_end_3e8} :catchall_3e6

    #@3e8
    throw v18

    #@3e9
    .line 3069
    :catchall_3e9
    move-exception v18

    #@3ea
    :try_start_3ea
    monitor-exit v19
    :try_end_3eb
    .catchall {:try_start_3ea .. :try_end_3eb} :catchall_3e9

    #@3eb
    throw v18

    #@3ec
    .line 3059
    .restart local v5       #didRestore:Z
    .restart local v8       #in:Ljava/io/InputStream;
    :catch_3ec
    move-exception v6

    #@3ed
    .line 3060
    .restart local v6       #e:Ljava/io/IOException;
    const-string v18, "BackupManagerService"

    #@3ef
    const-string v19, "Close of restore data pipe threw"

    #@3f1
    move-object/from16 v0, v18

    #@3f3
    move-object/from16 v1, v19

    #@3f5
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f8
    goto/16 :goto_33f

    #@3fa
    .line 3065
    .end local v6           #e:Ljava/io/IOException;
    :catchall_3fa
    move-exception v18

    #@3fb
    :try_start_3fb
    monitor-exit v19
    :try_end_3fc
    .catchall {:try_start_3fb .. :try_end_3fc} :catchall_3fa

    #@3fc
    throw v18

    #@3fd
    .line 3069
    :catchall_3fd
    move-exception v18

    #@3fe
    :try_start_3fe
    monitor-exit v19
    :try_end_3ff
    .catchall {:try_start_3fe .. :try_end_3ff} :catchall_3fd

    #@3ff
    throw v18

    #@400
    .line 3052
    .end local v3           #buffer:[B
    .end local v4           #compressed:Z
    .end local v5           #didRestore:Z
    .end local v7           #headerLen:I
    .end local v8           #in:Ljava/io/InputStream;
    .end local v9           #magicBytes:[B
    .end local v10           #okay:Z
    .end local v11           #preCompressStream:Ljava/io/InputStream;
    .end local v13           #rawDataIn:Ljava/io/DataInputStream;
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .end local v17           #streamHeader:[B
    .restart local v12       #rawDataIn:Ljava/io/DataInputStream;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    :catchall_400
    move-exception v18

    #@401
    goto/16 :goto_28e

    #@403
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .restart local v3       #buffer:[B
    .restart local v15       #rawInStream:Ljava/io/FileInputStream;
    :catchall_403
    move-exception v18

    #@404
    move-object v14, v15

    #@405
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    goto/16 :goto_28e

    #@407
    .line 3049
    .end local v3           #buffer:[B
    :catch_407
    move-exception v6

    #@408
    goto/16 :goto_1ee

    #@40a
    .end local v14           #rawInStream:Ljava/io/FileInputStream;
    .restart local v3       #buffer:[B
    .restart local v15       #rawInStream:Ljava/io/FileInputStream;
    :catch_40a
    move-exception v6

    #@40b
    move-object v14, v15

    #@40c
    .end local v15           #rawInStream:Ljava/io/FileInputStream;
    .restart local v14       #rawInStream:Ljava/io/FileInputStream;
    goto/16 :goto_1ee
.end method

.method sendEndRestore()V
    .registers 4

    #@0
    .prologue
    .line 4077
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 4079
    :try_start_4
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@6
    invoke-interface {v1}, Landroid/app/backup/IFullBackupRestoreObserver;->onEndRestore()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 4085
    :cond_9
    :goto_9
    return-void

    #@a
    .line 4080
    :catch_a
    move-exception v0

    #@b
    .line 4081
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@d
    const-string v2, "full restore observer went away: endRestore"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 4082
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@15
    goto :goto_9
.end method

.method sendOnRestorePackage(Ljava/lang/String;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 4065
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 4068
    :try_start_4
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@6
    invoke-interface {v1, p1}, Landroid/app/backup/IFullBackupRestoreObserver;->onRestorePackage(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 4074
    :cond_9
    :goto_9
    return-void

    #@a
    .line 4069
    :catch_a
    move-exception v0

    #@b
    .line 4070
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@d
    const-string v2, "full restore observer went away: restorePackage"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 4071
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@15
    goto :goto_9
.end method

.method sendStartRestore()V
    .registers 4

    #@0
    .prologue
    .line 4054
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 4056
    :try_start_4
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@6
    invoke-interface {v1}, Landroid/app/backup/IFullBackupRestoreObserver;->onStartRestore()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 4062
    :cond_9
    :goto_9
    return-void

    #@a
    .line 4057
    :catch_a
    move-exception v0

    #@b
    .line 4058
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@d
    const-string v2, "full restore observer went away: startRestore"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 4059
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@15
    goto :goto_9
.end method

.method setUpPipes()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3424
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@6
    .line 3425
    return-void
.end method

.method skipTarPadding(JLjava/io/InputStream;)V
    .registers 12
    .parameter "size"
    .parameter "instream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v6, 0x200

    #@2
    .line 3639
    add-long v4, p1, v6

    #@4
    rem-long v2, v4, v6

    #@6
    .line 3640
    .local v2, partial:J
    const-wide/16 v4, 0x0

    #@8
    cmp-long v4, v2, v4

    #@a
    if-lez v4, :cond_1e

    #@c
    .line 3641
    long-to-int v4, v2

    #@d
    rsub-int v1, v4, 0x200

    #@f
    .line 3642
    .local v1, needed:I
    new-array v0, v1, [B

    #@11
    .line 3643
    .local v0, buffer:[B
    const/4 v4, 0x0

    #@12
    invoke-virtual {p0, p3, v0, v4, v1}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->readExactly(Ljava/io/InputStream;[BII)I

    #@15
    move-result v4

    #@16
    if-ne v4, v1, :cond_1f

    #@18
    .line 3644
    iget-wide v4, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@1a
    int-to-long v6, v1

    #@1b
    add-long/2addr v4, v6

    #@1c
    iput-wide v4, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mBytes:J

    #@1e
    .line 3647
    .end local v0           #buffer:[B
    .end local v1           #needed:I
    :cond_1e
    return-void

    #@1f
    .line 3645
    .restart local v0       #buffer:[B
    .restart local v1       #needed:I
    :cond_1f
    new-instance v4, Ljava/io/IOException;

    #@21
    const-string v5, "Unexpected EOF in padding"

    #@23
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@26
    throw v4
.end method

.method tearDownAgent(Landroid/content/pm/ApplicationInfo;)V
    .registers 6
    .parameter "app"

    #@0
    .prologue
    .line 3442
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@2
    if-eqz v1, :cond_2d

    #@4
    .line 3445
    :try_start_4
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@6
    invoke-static {v1}, Lcom/android/server/BackupManagerService;->access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->unbindBackupAgent(Landroid/content/pm/ApplicationInfo;)V

    #@d
    .line 3450
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@f
    const/16 v2, 0x3e8

    #@11
    if-eq v1, v2, :cond_2a

    #@13
    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@15
    const-string v2, "com.android.backupconfirm"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_2a

    #@1d
    .line 3453
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1f
    invoke-static {v1}, Lcom/android/server/BackupManagerService;->access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;

    #@22
    move-result-object v1

    #@23
    iget-object v2, p1, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@25
    iget v3, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@27
    invoke-interface {v1, v2, v3}, Landroid/app/IActivityManager;->killApplicationProcess(Ljava/lang/String;I)V
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_2a} :catch_2e

    #@2a
    .line 3460
    :cond_2a
    :goto_2a
    const/4 v1, 0x0

    #@2b
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mAgent:Landroid/app/IBackupAgent;

    #@2d
    .line 3462
    :cond_2d
    return-void

    #@2e
    .line 3457
    :catch_2e
    move-exception v0

    #@2f
    .line 3458
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@31
    const-string v2, "Lost app trying to shut down"

    #@33
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_2a
.end method

.method tearDownPipes()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3428
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@3
    if-eqz v1, :cond_23

    #@5
    .line 3430
    :try_start_5
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@7
    const/4 v2, 0x0

    #@8
    aget-object v1, v1, v2

    #@a
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V

    #@d
    .line 3431
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@f
    const/4 v2, 0x0

    #@10
    const/4 v3, 0x0

    #@11
    aput-object v3, v1, v2

    #@13
    .line 3432
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@15
    const/4 v2, 0x1

    #@16
    aget-object v1, v1, v2

    #@18
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V

    #@1b
    .line 3433
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@1d
    const/4 v2, 0x1

    #@1e
    const/4 v3, 0x0

    #@1f
    aput-object v3, v1, v2
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_21} :catch_24

    #@21
    .line 3437
    :goto_21
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;->mPipes:[Landroid/os/ParcelFileDescriptor;

    #@23
    .line 3439
    :cond_23
    return-void

    #@24
    .line 3434
    :catch_24
    move-exception v0

    #@25
    .line 3435
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BackupManagerService"

    #@27
    const-string v2, "Couldn\'t close agent pipes"

    #@29
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2c
    goto :goto_21
.end method
