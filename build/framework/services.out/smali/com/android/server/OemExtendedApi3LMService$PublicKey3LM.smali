.class Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;
.super Ljava/lang/Object;
.source "OemExtendedApi3LMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/OemExtendedApi3LMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PublicKey3LM"
.end annotation


# instance fields
.field private final m3LMPublicKey:[B

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 88
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 89
    iput-object p1, p0, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->mContext:Landroid/content/Context;

    #@5
    .line 90
    iget-object v3, p0, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v2

    #@b
    .line 91
    .local v2, resources:Landroid/content/res/Resources;
    const-string v0, ""

    #@d
    .line 92
    .local v0, cert:Ljava/lang/String;
    const-string v3, "1"

    #@f
    const-string v4, "ro.3lm.production"

    #@11
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_34

    #@1b
    .line 93
    const v3, 0x1040018

    #@1e
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    .line 97
    :goto_22
    const/4 v3, 0x0

    #@23
    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    #@26
    move-result-object v3

    #@27
    invoke-direct {p0, v3}, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->extractPublicKey([B)Ljava/security/PublicKey;

    #@2a
    move-result-object v1

    #@2b
    .line 98
    .local v1, key:Ljava/security/PublicKey;
    if-eqz v1, :cond_3c

    #@2d
    .line 99
    invoke-interface {v1}, Ljava/security/PublicKey;->getEncoded()[B

    #@30
    move-result-object v3

    #@31
    iput-object v3, p0, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->m3LMPublicKey:[B

    #@33
    .line 103
    :goto_33
    return-void

    #@34
    .line 95
    .end local v1           #key:Ljava/security/PublicKey;
    :cond_34
    const v3, 0x1040019

    #@37
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    goto :goto_22

    #@3c
    .line 101
    .restart local v1       #key:Ljava/security/PublicKey;
    :cond_3c
    const/4 v3, 0x0

    #@3d
    iput-object v3, p0, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->m3LMPublicKey:[B

    #@3f
    goto :goto_33
.end method

.method private extractPublicKey([B)Ljava/security/PublicKey;
    .registers 8
    .parameter "blob"

    #@0
    .prologue
    .line 109
    :try_start_0
    const-string v3, "X509"

    #@2
    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@5
    move-result-object v0

    #@6
    .line 110
    .local v0, certFactory:Ljava/security/cert/CertificateFactory;
    new-instance v3, Ljava/io/ByteArrayInputStream;

    #@8
    invoke-direct {v3, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@b
    invoke-virtual {v0, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@e
    move-result-object v2

    #@f
    .line 112
    .local v2, x509Cert:Ljava/security/cert/Certificate;
    invoke-virtual {v2}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;
    :try_end_12
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_12} :catch_14

    #@12
    move-result-object v3

    #@13
    .line 116
    .end local v0           #certFactory:Ljava/security/cert/CertificateFactory;
    .end local v2           #x509Cert:Ljava/security/cert/Certificate;
    :goto_13
    return-object v3

    #@14
    .line 113
    :catch_14
    move-exception v1

    #@15
    .line 114
    .local v1, e:Ljava/security/cert/CertificateException;
    const-string v3, "OemExtendedApi3LM"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "Certificate parsing exception: "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 116
    const/4 v3, 0x0

    #@2e
    goto :goto_13
.end method


# virtual methods
.method public comparePublicKey([B)Z
    .registers 5
    .parameter "blob"

    #@0
    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->extractPublicKey([B)Ljava/security/PublicKey;

    #@3
    move-result-object v0

    #@4
    .line 121
    .local v0, blobKey:Ljava/security/PublicKey;
    if-eqz v0, :cond_11

    #@6
    .line 122
    invoke-interface {v0}, Ljava/security/PublicKey;->getEncoded()[B

    #@9
    move-result-object v1

    #@a
    iget-object v2, p0, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->m3LMPublicKey:[B

    #@c
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    #@f
    move-result v1

    #@10
    .line 124
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method
