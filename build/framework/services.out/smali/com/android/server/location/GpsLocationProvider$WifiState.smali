.class Lcom/android/server/location/GpsLocationProvider$WifiState;
.super Ljava/lang/Object;
.source "GpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WifiState"
.end annotation


# static fields
.field private static final WIFI_STATE_CLOSED:I = 0x0

.field private static final WIFI_STATE_CLOSING:I = 0x3

.field private static final WIFI_STATE_OPEN:I = 0x2

.field private static final WIFI_STATE_OPENING:I = 0x1


# instance fields
.field private currentNetId:I

.field private currentSSID:Ljava/lang/String;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private originalNetId:I

.field private originalNetworkPreference:I

.field private originalSSID:Ljava/lang/String;

.field private reportFailOnClosed:Z

.field private state:I

.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method public constructor <init>(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v0, -0x1

    #@3
    .line 2449
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@5
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 2438
    iput v2, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@a
    .line 2439
    iput-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentSSID:Ljava/lang/String;

    #@c
    .line 2440
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@e
    .line 2441
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@10
    .line 2442
    iput-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalSSID:Ljava/lang/String;

    #@12
    .line 2443
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetworkPreference:I

    #@14
    .line 2445
    iput-boolean v2, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->reportFailOnClosed:Z

    #@16
    .line 2447
    iput-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@18
    .line 2450
    invoke-static {p1}, Lcom/android/server/location/GpsLocationProvider;->access$600(Lcom/android/server/location/GpsLocationProvider;)Landroid/content/Context;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, "wifi"

    #@1e
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@24
    iput-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@26
    .line 2451
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/location/GpsLocationProvider$WifiState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@2
    return v0
.end method

.method static synthetic access$1002(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2432
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/server/location/GpsLocationProvider$WifiState;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentSSID:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/server/location/GpsLocationProvider$WifiState;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2432
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentSSID:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1200(Lcom/android/server/location/GpsLocationProvider$WifiState;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalSSID:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/server/location/GpsLocationProvider$WifiState;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2432
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalSSID:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/location/GpsLocationProvider$WifiState;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->handleFailure()V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/server/location/GpsLocationProvider$WifiState;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->handleSuccess()V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/android/server/location/GpsLocationProvider$WifiState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@2
    return v0
.end method

.method static synthetic access$3402(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2432
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@2
    return p1
.end method

.method static synthetic access$3500(Lcom/android/server/location/GpsLocationProvider$WifiState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetworkPreference:I

    #@2
    return v0
.end method

.method static synthetic access$3502(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2432
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetworkPreference:I

    #@2
    return p1
.end method

.method static synthetic access$3600(Lcom/android/server/location/GpsLocationProvider$WifiState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2432
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@2
    return v0
.end method

.method static synthetic access$3602(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2432
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@2
    return p1
.end method

.method static synthetic access$4000(Lcom/android/server/location/GpsLocationProvider$WifiState;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2432
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider$WifiState;->restoreOriginalWifiSettings(Z)V

    #@3
    return-void
.end method

.method private handleFailure()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2576
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@3
    if-ne v0, v2, :cond_16

    #@5
    .line 2577
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_12

    #@b
    const-string v0, "GpsLocationProvider"

    #@d
    const-string v1, "handleFailure for WIFI_STATE_OPENING"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 2578
    :cond_12
    invoke-direct {p0, v2}, Lcom/android/server/location/GpsLocationProvider$WifiState;->restoreOriginalWifiSettings(Z)V

    #@15
    .line 2586
    :cond_15
    :goto_15
    return-void

    #@16
    .line 2579
    :cond_16
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@18
    const/4 v1, 0x3

    #@19
    if-ne v0, v1, :cond_32

    #@1b
    .line 2580
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_28

    #@21
    const-string v0, "GpsLocationProvider"

    #@23
    const-string v1, "handleFailure for WIFI_STATE_CLOSING"

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 2581
    :cond_28
    const/4 v0, 0x0

    #@29
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@2b
    .line 2582
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2d
    const/4 v1, 0x4

    #@2e
    invoke-static {v0, v1}, Lcom/android/server/location/GpsLocationProvider;->access$5500(Lcom/android/server/location/GpsLocationProvider;I)V

    #@31
    goto :goto_15

    #@32
    .line 2584
    :cond_32
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_15

    #@38
    const-string v0, "GpsLocationProvider"

    #@3a
    const-string v1, "handleFailure invalid case"

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_15
.end method

.method private handleSuccess()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, -0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x4

    #@5
    .line 2545
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_2a

    #@a
    .line 2546
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_17

    #@10
    const-string v0, "GpsLocationProvider"

    #@12
    const-string v1, "handleSuccess for WIFI_STATE_OPENING"

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2547
    :cond_17
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@19
    const-string v1, "dummy-apn"

    #@1b
    invoke-static {v0, v2, v1, v3}, Lcom/android/server/location/GpsLocationProvider;->access$5700(Lcom/android/server/location/GpsLocationProvider;ILjava/lang/String;I)V

    #@1e
    .line 2549
    iput v5, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@20
    .line 2550
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@22
    invoke-static {v0, v2}, Lcom/android/server/location/GpsLocationProvider;->access$5800(Lcom/android/server/location/GpsLocationProvider;I)Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@25
    move-result-object v0

    #@26
    invoke-static {v0, v5}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I

    #@29
    .line 2569
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 2551
    :cond_2a
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@2c
    const/4 v1, 0x3

    #@2d
    if-ne v0, v1, :cond_63

    #@2f
    .line 2552
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@32
    move-result v0

    #@33
    if-eqz v0, :cond_3c

    #@35
    const-string v0, "GpsLocationProvider"

    #@37
    const-string v1, "handleSuccess for WIFI_STATE_CLOSING"

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 2553
    :cond_3c
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->reportFailOnClosed:Z

    #@3e
    if-eqz v0, :cond_5d

    #@40
    .line 2554
    iput-boolean v3, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->reportFailOnClosed:Z

    #@42
    .line 2555
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@44
    invoke-static {v0, v2}, Lcom/android/server/location/GpsLocationProvider;->access$5500(Lcom/android/server/location/GpsLocationProvider;I)V

    #@47
    .line 2559
    :goto_47
    iput v3, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@49
    .line 2560
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@4b
    invoke-static {v0, v2}, Lcom/android/server/location/GpsLocationProvider;->access$5800(Lcom/android/server/location/GpsLocationProvider;I)Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@4e
    move-result-object v0

    #@4f
    invoke-static {v0, v3}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I

    #@52
    .line 2561
    iput v4, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@54
    .line 2562
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentSSID:Ljava/lang/String;

    #@56
    .line 2563
    iput v4, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@58
    .line 2564
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalSSID:Ljava/lang/String;

    #@5a
    .line 2565
    iput v4, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetworkPreference:I

    #@5c
    goto :goto_29

    #@5d
    .line 2557
    :cond_5d
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@5f
    invoke-static {v0, v2}, Lcom/android/server/location/GpsLocationProvider;->access$5600(Lcom/android/server/location/GpsLocationProvider;I)V

    #@62
    goto :goto_47

    #@63
    .line 2567
    :cond_63
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_29

    #@69
    const-string v0, "GpsLocationProvider"

    #@6b
    const-string v1, "handleSuccess invalid case"

    #@6d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    goto :goto_29
.end method

.method private restoreOriginalNetworkPreference()V
    .registers 3

    #@0
    .prologue
    .line 2454
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$5400(Lcom/android/server/location/GpsLocationProvider;)Landroid/net/ConnectivityManager;

    #@5
    move-result-object v0

    #@6
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetworkPreference:I

    #@8
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->setNetworkPreference(I)V

    #@b
    .line 2455
    const/4 v0, -0x1

    #@c
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetworkPreference:I

    #@e
    .line 2456
    return-void
.end method

.method private restoreOriginalWifiSettings(Z)V
    .registers 7
    .parameter "ReportFailOnClosed"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    .line 2464
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_e

    #@7
    const-string v1, "GpsLocationProvider"

    #@9
    const-string v2, "restoreOriginalWifiSettings"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 2466
    :cond_e
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_2e

    #@14
    const-string v1, "GpsLocationProvider"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "originalNetId = "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    iget v3, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 2467
    :cond_2e
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@31
    move-result v1

    #@32
    if-eqz v1, :cond_4e

    #@34
    const-string v1, "GpsLocationProvider"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "currentNetId = "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    iget v3, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 2468
    :cond_4e
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@51
    move-result v1

    #@52
    if-eqz v1, :cond_6e

    #@54
    const-string v1, "GpsLocationProvider"

    #@56
    new-instance v2, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v3, "reportFailOnClosed = "

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    iget-boolean v3, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->reportFailOnClosed:Z

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 2471
    :cond_6e
    iput-boolean p1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->reportFailOnClosed:Z

    #@70
    .line 2473
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@72
    invoke-static {v1}, Lcom/android/server/location/GpsLocationProvider;->access$5400(Lcom/android/server/location/GpsLocationProvider;)Landroid/net/ConnectivityManager;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getNetworkPreference()I

    #@79
    move-result v1

    #@7a
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetworkPreference:I

    #@7c
    if-ne v1, v2, :cond_f8

    #@7e
    .line 2474
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@81
    move-result v1

    #@82
    if-eqz v1, :cond_8b

    #@84
    const-string v1, "GpsLocationProvider"

    #@86
    const-string v2, "current network preference same as original. do nothing."

    #@88
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 2485
    :cond_8b
    :goto_8b
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@8d
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@8f
    if-eq v1, v2, :cond_96

    #@91
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@93
    const/4 v2, -0x1

    #@94
    if-ne v1, v2, :cond_12e

    #@96
    .line 2487
    :cond_96
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@99
    move-result v1

    #@9a
    if-eqz v1, :cond_a3

    #@9c
    const-string v1, "GpsLocationProvider"

    #@9e
    const-string v2, "currentNetId == originalNetId or original was not connected. don\'t touch netId."

    #@a0
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 2489
    :cond_a3
    const/4 v1, 0x0

    #@a4
    iput v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@a6
    .line 2490
    iget-boolean v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->reportFailOnClosed:Z

    #@a8
    if-eqz v1, :cond_128

    #@aa
    .line 2494
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@ac
    if-ltz v1, :cond_e5

    #@ae
    .line 2495
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@b1
    move-result v1

    #@b2
    if-eqz v1, :cond_ce

    #@b4
    const-string v1, "GpsLocationProvider"

    #@b6
    new-instance v2, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v3, "removing currentNetId = "

    #@bd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v2

    #@c1
    iget v3, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@c3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v2

    #@c7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v2

    #@cb
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 2496
    :cond_ce
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@d0
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@d2
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    #@d5
    move-result v0

    #@d6
    .line 2497
    .local v0, b:Z
    if-eqz v0, :cond_109

    #@d8
    .line 2498
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@db
    move-result v1

    #@dc
    if-eqz v1, :cond_e5

    #@de
    const-string v1, "GpsLocationProvider"

    #@e0
    const-string v2, "successfully removed current AP"

    #@e2
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 2503
    .end local v0           #b:Z
    :cond_e5
    :goto_e5
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@e7
    invoke-static {v1, v4}, Lcom/android/server/location/GpsLocationProvider;->access$5500(Lcom/android/server/location/GpsLocationProvider;I)V

    #@ea
    .line 2536
    :cond_ea
    :goto_ea
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@ed
    move-result v1

    #@ee
    if-eqz v1, :cond_f7

    #@f0
    const-string v1, "GpsLocationProvider"

    #@f2
    const-string v2, "restoreOriginalWifiSettings end"

    #@f4
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f7
    .line 2537
    :cond_f7
    return-void

    #@f8
    .line 2476
    :cond_f8
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@fb
    move-result v1

    #@fc
    if-eqz v1, :cond_105

    #@fe
    const-string v1, "GpsLocationProvider"

    #@100
    const-string v2, "restoring original Network Preference..."

    #@102
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@105
    .line 2477
    :cond_105
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->restoreOriginalNetworkPreference()V

    #@108
    goto :goto_8b

    #@109
    .line 2500
    .restart local v0       #b:Z
    :cond_109
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@10c
    move-result v1

    #@10d
    if-eqz v1, :cond_e5

    #@10f
    const-string v1, "GpsLocationProvider"

    #@111
    new-instance v2, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v3, "ERROR: removeNetwork returned "

    #@118
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v2

    #@11c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v2

    #@120
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v2

    #@124
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@127
    goto :goto_e5

    #@128
    .line 2505
    .end local v0           #b:Z
    :cond_128
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@12a
    invoke-static {v1, v4}, Lcom/android/server/location/GpsLocationProvider;->access$5600(Lcom/android/server/location/GpsLocationProvider;I)V

    #@12d
    goto :goto_ea

    #@12e
    .line 2508
    :cond_12e
    const/4 v1, 0x3

    #@12f
    iput v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->state:I

    #@131
    .line 2509
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@133
    if-ltz v1, :cond_14c

    #@135
    .line 2520
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@137
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->currentNetId:I

    #@139
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    #@13c
    move-result v0

    #@13d
    .line 2521
    .restart local v0       #b:Z
    if-eqz v0, :cond_184

    #@13f
    .line 2522
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@142
    move-result v1

    #@143
    if-eqz v1, :cond_14c

    #@145
    const-string v1, "GpsLocationProvider"

    #@147
    const-string v2, "successfully removed current AP"

    #@149
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14c
    .line 2528
    .end local v0           #b:Z
    :cond_14c
    :goto_14c
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@14f
    move-result v1

    #@150
    if-eqz v1, :cond_159

    #@152
    const-string v1, "GpsLocationProvider"

    #@154
    const-string v2, "restoring original network..."

    #@156
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@159
    .line 2529
    :cond_159
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@15b
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$WifiState;->originalNetId:I

    #@15d
    const/4 v3, 0x1

    #@15e
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    #@161
    move-result v0

    #@162
    .line 2530
    .restart local v0       #b:Z
    if-eqz v0, :cond_1a3

    #@164
    .line 2531
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@167
    move-result v1

    #@168
    if-eqz v1, :cond_ea

    #@16a
    const-string v1, "GpsLocationProvider"

    #@16c
    new-instance v2, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v3, "enableNetwork returned "

    #@173
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v2

    #@177
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v2

    #@17b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v2

    #@17f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@182
    goto/16 :goto_ea

    #@184
    .line 2524
    :cond_184
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@187
    move-result v1

    #@188
    if-eqz v1, :cond_14c

    #@18a
    const-string v1, "GpsLocationProvider"

    #@18c
    new-instance v2, Ljava/lang/StringBuilder;

    #@18e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@191
    const-string v3, "ERROR: removeNetwork returned "

    #@193
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    move-result-object v2

    #@197
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v2

    #@19b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19e
    move-result-object v2

    #@19f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a2
    goto :goto_14c

    #@1a3
    .line 2533
    :cond_1a3
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@1a6
    move-result v1

    #@1a7
    if-eqz v1, :cond_ea

    #@1a9
    const-string v1, "GpsLocationProvider"

    #@1ab
    new-instance v2, Ljava/lang/StringBuilder;

    #@1ad
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b0
    const-string v3, "ERROR: enableNetwork returned "

    #@1b2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v2

    #@1b6
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v2

    #@1ba
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bd
    move-result-object v2

    #@1be
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c1
    goto/16 :goto_ea
.end method
