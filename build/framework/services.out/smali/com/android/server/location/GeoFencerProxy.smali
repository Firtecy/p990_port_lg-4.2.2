.class public Lcom/android/server/location/GeoFencerProxy;
.super Lcom/android/server/location/GeoFencerBase;
.source "GeoFencerProxy.java"


# static fields
.field private static final LOGV_ENABLED:Z = true

.field private static final TAG:Ljava/lang/String; = "GeoFencerProxy"

.field private static mGeoFencerProxy:Lcom/android/server/location/GeoFencerProxy;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mGeoFencer:Landroid/location/IGeoFencer;

.field private final mIntent:Landroid/content/Intent;

.field private final mListener:Landroid/location/IGeoFenceListener$Stub;

.field private final mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "serviceName"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/server/location/GeoFencerBase;-><init>()V

    #@3
    .line 50
    new-instance v0, Lcom/android/server/location/GeoFencerProxy$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/server/location/GeoFencerProxy$1;-><init>(Lcom/android/server/location/GeoFencerProxy;)V

    #@8
    iput-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mServiceConnection:Landroid/content/ServiceConnection;

    #@a
    .line 66
    new-instance v0, Lcom/android/server/location/GeoFencerProxy$2;

    #@c
    invoke-direct {v0, p0}, Lcom/android/server/location/GeoFencerProxy$2;-><init>(Lcom/android/server/location/GeoFencerProxy;)V

    #@f
    iput-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mListener:Landroid/location/IGeoFenceListener$Stub;

    #@11
    .line 83
    iput-object p1, p0, Lcom/android/server/location/GeoFencerProxy;->mContext:Landroid/content/Context;

    #@13
    .line 84
    new-instance v0, Landroid/content/Intent;

    #@15
    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18
    iput-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mIntent:Landroid/content/Intent;

    #@1a
    .line 85
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mContext:Landroid/content/Context;

    #@1c
    iget-object v1, p0, Lcom/android/server/location/GeoFencerProxy;->mIntent:Landroid/content/Intent;

    #@1e
    iget-object v2, p0, Lcom/android/server/location/GeoFencerProxy;->mServiceConnection:Landroid/content/ServiceConnection;

    #@20
    const/16 v3, 0x15

    #@22
    const/4 v4, -0x2

    #@23
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@26
    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/location/GeoFencerProxy;)Landroid/location/IGeoFencer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/android/server/location/GeoFencerProxy;Landroid/location/IGeoFencer;)Landroid/location/IGeoFencer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/server/location/GeoFencerProxy;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/android/server/location/GeoFencerProxy;->logv(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private ensureGeoFencer()Z
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 103
    iget-object v2, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@3
    if-nez v2, :cond_33

    #@5
    .line 105
    :try_start_5
    iget-object v3, p0, Lcom/android/server/location/GeoFencerProxy;->mServiceConnection:Landroid/content/ServiceConnection;

    #@7
    monitor-enter v3
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_2a

    #@8
    .line 106
    :try_start_8
    const-string v2, "waiting..."

    #@a
    invoke-direct {p0, v2}, Lcom/android/server/location/GeoFencerProxy;->logv(Ljava/lang/String;)V

    #@d
    .line 107
    iget-object v2, p0, Lcom/android/server/location/GeoFencerProxy;->mServiceConnection:Landroid/content/ServiceConnection;

    #@f
    const-wide/32 v4, 0xea60

    #@12
    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V

    #@15
    .line 108
    const-string v2, "woke up!!!"

    #@17
    invoke-direct {p0, v2}, Lcom/android/server/location/GeoFencerProxy;->logv(Ljava/lang/String;)V

    #@1a
    .line 109
    monitor-exit v3
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_27

    #@1b
    .line 115
    iget-object v2, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@1d
    if-nez v2, :cond_33

    #@1f
    .line 116
    const-string v2, "GeoFencerProxy"

    #@21
    const-string v3, "Timed out. No GeoFencer connection"

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 121
    :goto_26
    return v1

    #@27
    .line 109
    :catchall_27
    move-exception v2

    #@28
    :try_start_28
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_27

    #@29
    :try_start_29
    throw v2
    :try_end_2a
    .catch Ljava/lang/InterruptedException; {:try_start_29 .. :try_end_2a} :catch_2a

    #@2a
    .line 110
    :catch_2a
    move-exception v0

    #@2b
    .line 111
    .local v0, ie:Ljava/lang/InterruptedException;
    const-string v2, "GeoFencerProxy"

    #@2d
    const-string v3, "Interrupted while waiting for GeoFencer"

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_26

    #@33
    .line 121
    .end local v0           #ie:Ljava/lang/InterruptedException;
    :cond_33
    const/4 v1, 0x1

    #@34
    goto :goto_26
.end method

.method public static getGeoFencerProxy(Landroid/content/Context;Ljava/lang/String;)Lcom/android/server/location/GeoFencerProxy;
    .registers 3
    .parameter "context"
    .parameter "serviceName"

    #@0
    .prologue
    .line 76
    sget-object v0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencerProxy:Lcom/android/server/location/GeoFencerProxy;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 77
    new-instance v0, Lcom/android/server/location/GeoFencerProxy;

    #@6
    invoke-direct {v0, p0, p1}, Lcom/android/server/location/GeoFencerProxy;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@9
    sput-object v0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencerProxy:Lcom/android/server/location/GeoFencerProxy;

    #@b
    .line 79
    :cond_b
    sget-object v0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencerProxy:Lcom/android/server/location/GeoFencerProxy;

    #@d
    return-object v0
.end method

.method private logv(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 146
    const-string v0, "GeoFencerProxy"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 147
    return-void
.end method


# virtual methods
.method public removeCaller(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/android/server/location/GeoFencerBase;->removeCaller(I)V

    #@3
    .line 92
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 94
    :try_start_7
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@9
    invoke-interface {v0, p1}, Landroid/location/IGeoFencer;->clearGeoFenceUser(I)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_15

    #@c
    .line 100
    :goto_c
    return-void

    #@d
    .line 99
    :cond_d
    const-string v0, "GeoFencerProxy"

    #@f
    const-string v1, "removeCaller - mGeoFencer is null"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    goto :goto_c

    #@15
    .line 95
    :catch_15
    move-exception v0

    #@16
    goto :goto_c
.end method

.method protected start(Landroid/location/GeoFenceParams;)Z
    .registers 4
    .parameter "geofence"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Lcom/android/server/location/GeoFencerProxy;->ensureGeoFencer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_10

    #@6
    .line 127
    :try_start_6
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@8
    iget-object v1, p0, Lcom/android/server/location/GeoFencerProxy;->mListener:Landroid/location/IGeoFenceListener$Stub;

    #@a
    invoke-interface {v0, v1, p1}, Landroid/location/IGeoFencer;->setGeoFence(Landroid/os/IBinder;Landroid/location/GeoFenceParams;)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_f

    #@d
    move-result v0

    #@e
    .line 131
    :goto_e
    return v0

    #@f
    .line 128
    :catch_f
    move-exception v0

    #@10
    .line 131
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_e
.end method

.method protected stop(Landroid/app/PendingIntent;)Z
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 135
    invoke-direct {p0}, Lcom/android/server/location/GeoFencerProxy;->ensureGeoFencer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_10

    #@6
    .line 137
    :try_start_6
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy;->mGeoFencer:Landroid/location/IGeoFencer;

    #@8
    iget-object v1, p0, Lcom/android/server/location/GeoFencerProxy;->mListener:Landroid/location/IGeoFenceListener$Stub;

    #@a
    invoke-interface {v0, v1, p1}, Landroid/location/IGeoFencer;->clearGeoFence(Landroid/os/IBinder;Landroid/app/PendingIntent;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_f

    #@d
    .line 138
    const/4 v0, 0x1

    #@e
    .line 142
    :goto_e
    return v0

    #@f
    .line 139
    :catch_f
    move-exception v0

    #@10
    .line 142
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_e
.end method
