.class public Lcom/android/server/location/GpsXtraDownloader;
.super Ljava/lang/Object;
.source "GpsXtraDownloader.java"


# static fields
.field static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "GpsXtraDownloader"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNextServerIndex:I

.field private mXtraServers:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/Properties;)V
    .registers 11
    .parameter "context"
    .parameter "properties"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    iput-object p1, p0, Lcom/android/server/location/GpsXtraDownloader;->mContext:Landroid/content/Context;

    #@5
    .line 57
    const/4 v0, 0x0

    #@6
    .line 58
    .local v0, count:I
    const-string v6, "XTRA_SERVER_1"

    #@8
    invoke-virtual {p2, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    .line 59
    .local v3, server1:Ljava/lang/String;
    const-string v6, "XTRA_SERVER_2"

    #@e
    invoke-virtual {p2, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    .line 60
    .local v4, server2:Ljava/lang/String;
    const-string v6, "XTRA_SERVER_3"

    #@14
    invoke-virtual {p2, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v5

    #@18
    .line 61
    .local v5, server3:Ljava/lang/String;
    if-eqz v3, :cond_1c

    #@1a
    add-int/lit8 v0, v0, 0x1

    #@1c
    .line 62
    :cond_1c
    if-eqz v4, :cond_20

    #@1e
    add-int/lit8 v0, v0, 0x1

    #@20
    .line 63
    :cond_20
    if-eqz v5, :cond_24

    #@22
    add-int/lit8 v0, v0, 0x1

    #@24
    .line 65
    :cond_24
    if-nez v0, :cond_2e

    #@26
    .line 66
    const-string v6, "GpsXtraDownloader"

    #@28
    const-string v7, "No XTRA servers were specified in the GPS configuration"

    #@2a
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 79
    :goto_2d
    return-void

    #@2e
    .line 69
    :cond_2e
    new-array v6, v0, [Ljava/lang/String;

    #@30
    iput-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mXtraServers:[Ljava/lang/String;

    #@32
    .line 70
    const/4 v0, 0x0

    #@33
    .line 71
    if-eqz v3, :cond_5a

    #@35
    iget-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mXtraServers:[Ljava/lang/String;

    #@37
    add-int/lit8 v1, v0, 0x1

    #@39
    .end local v0           #count:I
    .local v1, count:I
    aput-object v3, v6, v0

    #@3b
    .line 72
    :goto_3b
    if-eqz v4, :cond_44

    #@3d
    iget-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mXtraServers:[Ljava/lang/String;

    #@3f
    add-int/lit8 v0, v1, 0x1

    #@41
    .end local v1           #count:I
    .restart local v0       #count:I
    aput-object v4, v6, v1

    #@43
    move v1, v0

    #@44
    .line 73
    .end local v0           #count:I
    .restart local v1       #count:I
    :cond_44
    if-eqz v5, :cond_58

    #@46
    iget-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mXtraServers:[Ljava/lang/String;

    #@48
    add-int/lit8 v0, v1, 0x1

    #@4a
    .end local v1           #count:I
    .restart local v0       #count:I
    aput-object v5, v6, v1

    #@4c
    .line 76
    :goto_4c
    new-instance v2, Ljava/util/Random;

    #@4e
    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    #@51
    .line 77
    .local v2, random:Ljava/util/Random;
    invoke-virtual {v2, v0}, Ljava/util/Random;->nextInt(I)I

    #@54
    move-result v6

    #@55
    iput v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@57
    goto :goto_2d

    #@58
    .end local v0           #count:I
    .end local v2           #random:Ljava/util/Random;
    .restart local v1       #count:I
    :cond_58
    move v0, v1

    #@59
    .end local v1           #count:I
    .restart local v0       #count:I
    goto :goto_4c

    #@5a
    :cond_5a
    move v1, v0

    #@5b
    .end local v0           #count:I
    .restart local v1       #count:I
    goto :goto_3b
.end method

.method protected static doDownload(Ljava/lang/String;ZLjava/lang/String;I)[B
    .registers 19
    .parameter "url"
    .parameter "isProxySet"
    .parameter "proxyHost"
    .parameter "proxyPort"

    #@0
    .prologue
    .line 112
    const/4 v3, 0x0

    #@1
    .line 117
    .local v3, client:Landroid/net/http/AndroidHttpClient;
    :try_start_1
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->getXTRAUserAgent()Ljava/lang/String;

    #@4
    move-result-object v11

    #@5
    invoke-static {v11}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    #@8
    move-result-object v3

    #@9
    .line 119
    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    #@b
    invoke-direct {v8, p0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    #@e
    .line 121
    .local v8, req:Lorg/apache/http/client/methods/HttpUriRequest;
    if-eqz p1, :cond_20

    #@10
    .line 122
    new-instance v7, Lorg/apache/http/HttpHost;

    #@12
    move-object/from16 v0, p2

    #@14
    move/from16 v1, p3

    #@16
    invoke-direct {v7, v0, v1}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    #@19
    .line 123
    .local v7, proxy:Lorg/apache/http/HttpHost;
    invoke-interface {v8}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    #@1c
    move-result-object v11

    #@1d
    invoke-static {v11, v7}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    #@20
    .line 126
    .end local v7           #proxy:Lorg/apache/http/HttpHost;
    :cond_20
    const-string v11, "Accept"

    #@22
    const-string v12, "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"

    #@24
    invoke-interface {v8, v11, v12}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 130
    const-string v11, "x-wap-profile"

    #@29
    const-string v12, "http://www.openmobilealliance.org/tech/profiles/UAPROF/ccppschema-20021212#"

    #@2b
    invoke-interface {v8, v11, v12}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 134
    invoke-virtual {v3, v8}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    #@31
    move-result-object v9

    #@32
    .line 135
    .local v9, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    #@35
    move-result-object v10

    #@36
    .line 136
    .local v10, status:Lorg/apache/http/StatusLine;
    invoke-interface {v10}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_39
    .catchall {:try_start_1 .. :try_end_39} :catchall_88
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_39} :catch_80

    #@39
    move-result v11

    #@3a
    const/16 v12, 0xc8

    #@3c
    if-eq v11, v12, :cond_45

    #@3e
    .line 138
    const/4 v2, 0x0

    #@3f
    .line 168
    if-eqz v3, :cond_44

    #@41
    .line 169
    :goto_41
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    #@44
    .line 172
    .end local v8           #req:Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v9           #response:Lorg/apache/http/HttpResponse;
    .end local v10           #status:Lorg/apache/http/StatusLine;
    :cond_44
    :goto_44
    return-object v2

    #@45
    .line 141
    .restart local v8       #req:Lorg/apache/http/client/methods/HttpUriRequest;
    .restart local v9       #response:Lorg/apache/http/HttpResponse;
    .restart local v10       #status:Lorg/apache/http/StatusLine;
    :cond_45
    :try_start_45
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_48
    .catchall {:try_start_45 .. :try_end_48} :catchall_88
    .catch Ljava/lang/Exception; {:try_start_45 .. :try_end_48} :catch_80

    #@48
    move-result-object v6

    #@49
    .line 142
    .local v6, entity:Lorg/apache/http/HttpEntity;
    const/4 v2, 0x0

    #@4a
    .line 143
    .local v2, body:[B
    if-eqz v6, :cond_71

    #@4c
    .line 145
    :try_start_4c
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContentLength()J

    #@4f
    move-result-wide v11

    #@50
    const-wide/16 v13, 0x0

    #@52
    cmp-long v11, v11, v13

    #@54
    if-lez v11, :cond_6c

    #@56
    .line 146
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContentLength()J

    #@59
    move-result-wide v11

    #@5a
    long-to-int v11, v11

    #@5b
    new-array v2, v11, [B

    #@5d
    .line 147
    new-instance v4, Ljava/io/DataInputStream;

    #@5f
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    #@62
    move-result-object v11

    #@63
    invoke-direct {v4, v11}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_66
    .catchall {:try_start_4c .. :try_end_66} :catchall_79

    #@66
    .line 149
    .local v4, dis:Ljava/io/DataInputStream;
    :try_start_66
    invoke-virtual {v4, v2}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_69
    .catchall {:try_start_66 .. :try_end_69} :catchall_74

    #@69
    .line 152
    :try_start_69
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_6c
    .catchall {:try_start_69 .. :try_end_6c} :catchall_79
    .catch Ljava/io/IOException; {:try_start_69 .. :try_end_6c} :catch_98

    #@6c
    .line 159
    .end local v4           #dis:Ljava/io/DataInputStream;
    :cond_6c
    :goto_6c
    if-eqz v6, :cond_71

    #@6e
    .line 160
    :try_start_6e
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_71
    .catchall {:try_start_6e .. :try_end_71} :catchall_88
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_71} :catch_80

    #@71
    .line 168
    :cond_71
    if-eqz v3, :cond_44

    #@73
    goto :goto_41

    #@74
    .line 151
    .restart local v4       #dis:Ljava/io/DataInputStream;
    :catchall_74
    move-exception v11

    #@75
    .line 152
    :try_start_75
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_78
    .catchall {:try_start_75 .. :try_end_78} :catchall_79
    .catch Ljava/io/IOException; {:try_start_75 .. :try_end_78} :catch_8f

    #@78
    .line 151
    :goto_78
    :try_start_78
    throw v11
    :try_end_79
    .catchall {:try_start_78 .. :try_end_79} :catchall_79

    #@79
    .line 159
    .end local v4           #dis:Ljava/io/DataInputStream;
    :catchall_79
    move-exception v11

    #@7a
    if-eqz v6, :cond_7f

    #@7c
    .line 160
    :try_start_7c
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->consumeContent()V

    #@7f
    .line 159
    :cond_7f
    throw v11
    :try_end_80
    .catchall {:try_start_7c .. :try_end_80} :catchall_88
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_80} :catch_80

    #@80
    .line 165
    .end local v2           #body:[B
    .end local v6           #entity:Lorg/apache/http/HttpEntity;
    .end local v8           #req:Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v9           #response:Lorg/apache/http/HttpResponse;
    .end local v10           #status:Lorg/apache/http/StatusLine;
    :catch_80
    move-exception v11

    #@81
    .line 168
    if-eqz v3, :cond_86

    #@83
    .line 169
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    #@86
    .line 172
    :cond_86
    const/4 v2, 0x0

    #@87
    goto :goto_44

    #@88
    .line 168
    :catchall_88
    move-exception v11

    #@89
    if-eqz v3, :cond_8e

    #@8b
    .line 169
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    #@8e
    .line 168
    :cond_8e
    throw v11

    #@8f
    .line 153
    .restart local v2       #body:[B
    .restart local v4       #dis:Ljava/io/DataInputStream;
    .restart local v6       #entity:Lorg/apache/http/HttpEntity;
    .restart local v8       #req:Lorg/apache/http/client/methods/HttpUriRequest;
    .restart local v9       #response:Lorg/apache/http/HttpResponse;
    .restart local v10       #status:Lorg/apache/http/StatusLine;
    :catch_8f
    move-exception v5

    #@90
    .line 154
    .local v5, e:Ljava/io/IOException;
    :try_start_90
    const-string v12, "GpsXtraDownloader"

    #@92
    const-string v13, "Unexpected IOException."

    #@94
    invoke-static {v12, v13, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@97
    goto :goto_78

    #@98
    .line 153
    .end local v5           #e:Ljava/io/IOException;
    :catch_98
    move-exception v5

    #@99
    .line 154
    .restart local v5       #e:Ljava/io/IOException;
    const-string v11, "GpsXtraDownloader"

    #@9b
    const-string v12, "Unexpected IOException."

    #@9d
    invoke-static {v11, v12, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a0
    .catchall {:try_start_90 .. :try_end_a0} :catchall_79

    #@a0
    goto :goto_6c
.end method


# virtual methods
.method downloadXtraData()[B
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 82
    iget-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v6}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 83
    .local v0, proxyHost:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mContext:Landroid/content/Context;

    #@9
    invoke-static {v6}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    #@c
    move-result v1

    #@d
    .line 84
    .local v1, proxyPort:I
    if-eqz v0, :cond_1c

    #@f
    const/4 v6, -0x1

    #@10
    if-eq v1, v6, :cond_1c

    #@12
    const/4 v4, 0x1

    #@13
    .line 85
    .local v4, useProxy:Z
    :goto_13
    const/4 v2, 0x0

    #@14
    .line 86
    .local v2, result:[B
    iget v3, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@16
    .line 88
    .local v3, startIndex:I
    iget-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mXtraServers:[Ljava/lang/String;

    #@18
    if-nez v6, :cond_1e

    #@1a
    .line 89
    const/4 v5, 0x0

    #@1b
    .line 105
    :goto_1b
    return-object v5

    #@1c
    .end local v2           #result:[B
    .end local v3           #startIndex:I
    .end local v4           #useProxy:Z
    :cond_1c
    move v4, v5

    #@1d
    .line 84
    goto :goto_13

    #@1e
    .line 93
    .restart local v2       #result:[B
    .restart local v3       #startIndex:I
    .restart local v4       #useProxy:Z
    :cond_1e
    if-nez v2, :cond_3d

    #@20
    .line 94
    iget-object v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mXtraServers:[Ljava/lang/String;

    #@22
    iget v7, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@24
    aget-object v6, v6, v7

    #@26
    invoke-static {v6, v4, v0, v1}, Lcom/android/server/location/GpsXtraDownloader;->doDownload(Ljava/lang/String;ZLjava/lang/String;I)[B

    #@29
    move-result-object v2

    #@2a
    .line 97
    iget v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@2c
    add-int/lit8 v6, v6, 0x1

    #@2e
    iput v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@30
    .line 98
    iget v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@32
    iget-object v7, p0, Lcom/android/server/location/GpsXtraDownloader;->mXtraServers:[Ljava/lang/String;

    #@34
    array-length v7, v7

    #@35
    if-ne v6, v7, :cond_39

    #@37
    .line 99
    iput v5, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@39
    .line 102
    :cond_39
    iget v6, p0, Lcom/android/server/location/GpsXtraDownloader;->mNextServerIndex:I

    #@3b
    if-ne v6, v3, :cond_1e

    #@3d
    :cond_3d
    move-object v5, v2

    #@3e
    .line 105
    goto :goto_1b
.end method
