.class Lcom/android/server/location/LocationProviderProxy$1;
.super Ljava/lang/Object;
.source "LocationProviderProxy.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/LocationProviderProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/LocationProviderProxy;


# direct methods
.method constructor <init>(Lcom/android/server/location/LocationProviderProxy;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 93
    iput-object p1, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    .line 96
    const-string v6, "LocationProviderProxy"

    #@2
    const-string v7, "applying state to connected service"

    #@4
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 99
    const/4 v2, 0x0

    #@8
    .line 103
    .local v2, properties:Lcom/android/internal/location/ProviderProperties;
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@a
    invoke-static {v6}, Lcom/android/server/location/LocationProviderProxy;->access$000(Lcom/android/server/location/LocationProviderProxy;)Ljava/lang/Object;

    #@d
    move-result-object v7

    #@e
    monitor-enter v7

    #@f
    .line 104
    :try_start_f
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@11
    invoke-static {v6}, Lcom/android/server/location/LocationProviderProxy;->access$100(Lcom/android/server/location/LocationProviderProxy;)Z

    #@14
    move-result v1

    #@15
    .line 105
    .local v1, enabled:Z
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@17
    invoke-static {v6}, Lcom/android/server/location/LocationProviderProxy;->access$200(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/internal/location/ProviderRequest;

    #@1a
    move-result-object v3

    #@1b
    .line 106
    .local v3, request:Lcom/android/internal/location/ProviderRequest;
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@1d
    invoke-static {v6}, Lcom/android/server/location/LocationProviderProxy;->access$300(Lcom/android/server/location/LocationProviderProxy;)Landroid/os/WorkSource;

    #@20
    move-result-object v5

    #@21
    .line 107
    .local v5, source:Landroid/os/WorkSource;
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@23
    invoke-static {v6}, Lcom/android/server/location/LocationProviderProxy;->access$400(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/internal/location/ILocationProvider;

    #@26
    move-result-object v4

    #@27
    .line 108
    .local v4, service:Lcom/android/internal/location/ILocationProvider;
    monitor-exit v7

    #@28
    .line 110
    if-nez v4, :cond_2e

    #@2a
    .line 137
    :goto_2a
    return-void

    #@2b
    .line 108
    .end local v1           #enabled:Z
    .end local v3           #request:Lcom/android/internal/location/ProviderRequest;
    .end local v4           #service:Lcom/android/internal/location/ILocationProvider;
    .end local v5           #source:Landroid/os/WorkSource;
    :catchall_2b
    move-exception v6

    #@2c
    monitor-exit v7
    :try_end_2d
    .catchall {:try_start_f .. :try_end_2d} :catchall_2b

    #@2d
    throw v6

    #@2e
    .line 114
    .restart local v1       #enabled:Z
    .restart local v3       #request:Lcom/android/internal/location/ProviderRequest;
    .restart local v4       #service:Lcom/android/internal/location/ILocationProvider;
    .restart local v5       #source:Landroid/os/WorkSource;
    :cond_2e
    :try_start_2e
    invoke-interface {v4}, Lcom/android/internal/location/ILocationProvider;->getProperties()Lcom/android/internal/location/ProviderProperties;

    #@31
    move-result-object v2

    #@32
    .line 115
    if-nez v2, :cond_56

    #@34
    .line 116
    const-string v6, "LocationProviderProxy"

    #@36
    new-instance v7, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    iget-object v8, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@3d
    invoke-static {v8}, Lcom/android/server/location/LocationProviderProxy;->access$500(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/server/ServiceWatcher;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    const-string v8, " has invalid locatino provider properties"

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v7

    #@53
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 121
    :cond_56
    if-eqz v1, :cond_60

    #@58
    .line 122
    invoke-interface {v4}, Lcom/android/internal/location/ILocationProvider;->enable()V

    #@5b
    .line 123
    if-eqz v3, :cond_60

    #@5d
    .line 124
    invoke-interface {v4, v3, v5}, Lcom/android/internal/location/ILocationProvider;->setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    :try_end_60
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_60} :catch_71
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_60} :catch_78

    #@60
    .line 134
    :cond_60
    :goto_60
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@62
    invoke-static {v6}, Lcom/android/server/location/LocationProviderProxy;->access$000(Lcom/android/server/location/LocationProviderProxy;)Ljava/lang/Object;

    #@65
    move-result-object v7

    #@66
    monitor-enter v7

    #@67
    .line 135
    :try_start_67
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@69
    invoke-static {v6, v2}, Lcom/android/server/location/LocationProviderProxy;->access$602(Lcom/android/server/location/LocationProviderProxy;Lcom/android/internal/location/ProviderProperties;)Lcom/android/internal/location/ProviderProperties;

    #@6c
    .line 136
    monitor-exit v7

    #@6d
    goto :goto_2a

    #@6e
    :catchall_6e
    move-exception v6

    #@6f
    monitor-exit v7
    :try_end_70
    .catchall {:try_start_67 .. :try_end_70} :catchall_6e

    #@70
    throw v6

    #@71
    .line 127
    :catch_71
    move-exception v0

    #@72
    .line 128
    .local v0, e:Landroid/os/RemoteException;
    const-string v6, "LocationProviderProxy"

    #@74
    invoke-static {v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@77
    goto :goto_60

    #@78
    .line 129
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_78
    move-exception v0

    #@79
    .line 131
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "LocationProviderProxy"

    #@7b
    new-instance v7, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v8, "Exception from "

    #@82
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v7

    #@86
    iget-object v8, p0, Lcom/android/server/location/LocationProviderProxy$1;->this$0:Lcom/android/server/location/LocationProviderProxy;

    #@88
    invoke-static {v8}, Lcom/android/server/location/LocationProviderProxy;->access$500(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/server/ServiceWatcher;

    #@8b
    move-result-object v8

    #@8c
    invoke-virtual {v8}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@8f
    move-result-object v8

    #@90
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v7

    #@94
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v7

    #@98
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9b
    goto :goto_60
.end method
