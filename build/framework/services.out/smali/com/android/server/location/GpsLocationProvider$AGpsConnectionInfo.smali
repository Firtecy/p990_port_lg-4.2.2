.class Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;
.super Ljava/lang/Object;
.source "GpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AGpsConnectionInfo"
.end annotation


# static fields
.field private static final BEARER_INVALID:I = -0x1

.field private static final BEARER_IPV4:I = 0x0

.field private static final BEARER_IPV4V6:I = 0x2

.field private static final BEARER_IPV6:I = 0x1

.field private static final CONNECTION_TYPE_ANY:I = 0x0

.field private static final CONNECTION_TYPE_C2K:I = 0x2

.field private static final CONNECTION_TYPE_SUPL:I = 0x1

.field private static final CONNECTION_TYPE_WIFI:I = 0x4

.field private static final CONNECTION_TYPE_WWAN_ANY:I = 0x3

.field private static final STATE_CLOSED:I = 0x0

.field private static final STATE_KEEP_OPEN:I = 0x3

.field private static final STATE_OPEN:I = 0x2

.field private static final STATE_OPENING:I = 0x1


# instance fields
.field private mAPN:Ljava/lang/String;

.field private final mAgpsType:I

.field private mBearerType:I

.field private final mCMConnType:I

.field private mIPvVerType:I

.field private mIpAddr:Ljava/net/InetAddress;

.field private final mPHConnFeatureStr:Ljava/lang/String;

.field private mState:I

.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method private constructor <init>(Lcom/android/server/location/GpsLocationProvider;II)V
    .registers 6
    .parameter
    .parameter "connMgrConnType"
    .parameter "agpsType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2300
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 2301
    iput p2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mCMConnType:I

    #@8
    .line 2302
    iput p3, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAgpsType:I

    #@a
    .line 2303
    const/4 v0, 0x3

    #@b
    if-ne v0, p2, :cond_1c

    #@d
    .line 2304
    const-string v0, "enableSUPL"

    #@f
    iput-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mPHConnFeatureStr:Ljava/lang/String;

    #@11
    .line 2308
    :goto_11
    iput-object v1, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@13
    .line 2309
    const/4 v0, 0x0

    #@14
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@16
    .line 2310
    iput-object v1, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mIpAddr:Ljava/net/InetAddress;

    #@18
    .line 2311
    const/4 v0, -0x1

    #@19
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mBearerType:I

    #@1b
    .line 2312
    return-void

    #@1c
    .line 2306
    :cond_1c
    const-string v0, "enableMMS"

    #@1e
    iput-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mPHConnFeatureStr:Ljava/lang/String;

    #@20
    goto :goto_11
.end method

.method synthetic constructor <init>(Lcom/android/server/location/GpsLocationProvider;IILcom/android/server/location/GpsLocationProvider$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;-><init>(Lcom/android/server/location/GpsLocationProvider;II)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2270
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@2
    return v0
.end method

.method static synthetic access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2270
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@2
    return p1
.end method

.method static synthetic access$1700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/net/InetAddress;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getIpAddr()Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getCMConnType()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getAgpsType()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2000(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getApn(Landroid/net/NetworkInfo;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getBearerType(Landroid/net/NetworkInfo;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2302(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2270
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$2400(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2270
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAgpsType:I

    #@2
    return v0
.end method

.method static synthetic access$3700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;[B)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->connect(Landroid/net/NetworkInfo;[B)V

    #@3
    return-void
.end method

.method static synthetic access$3800(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2270
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getApn(Landroid/net/NetworkInfo;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$3900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2270
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mPHConnFeatureStr:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private connect(Landroid/net/NetworkInfo;[B)V
    .registers 10
    .parameter "info"
    .parameter "ipAddr"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 2397
    const/4 v0, -0x1

    #@4
    .line 2398
    .local v0, result:I
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mCMConnType:I

    #@6
    if-nez v2, :cond_30

    #@8
    if-eqz p1, :cond_30

    #@a
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_30

    #@10
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isRoaming()Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_30

    #@16
    .line 2400
    iput v6, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@18
    .line 2401
    const/4 v0, 0x0

    #@19
    .line 2416
    :cond_19
    :goto_19
    if-nez v0, :cond_56

    #@1b
    .line 2417
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_28

    #@21
    const-string v2, "GpsLocationProvider"

    #@23
    const-string v3, "Phone.APN_ALREADY_ACTIVE"

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 2418
    :cond_28
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@2a
    if-eq v2, v6, :cond_2f

    #@2c
    .line 2419
    const/4 v2, 0x2

    #@2d
    iput v2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@2f
    .line 2428
    :cond_2f
    :goto_2f
    return-void

    #@30
    .line 2405
    :cond_30
    iput v4, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@32
    .line 2406
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@34
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$5400(Lcom/android/server/location/GpsLocationProvider;)Landroid/net/ConnectivityManager;

    #@37
    move-result-object v2

    #@38
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mPHConnFeatureStr:Ljava/lang/String;

    #@3a
    invoke-virtual {v2, v5, v3}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    #@3d
    move-result v0

    #@3e
    .line 2408
    if-eqz p2, :cond_19

    #@40
    .line 2410
    :try_start_40
    invoke-static {p2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@43
    move-result-object v2

    #@44
    iput-object v2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mIpAddr:Ljava/net/InetAddress;
    :try_end_46
    .catch Ljava/net/UnknownHostException; {:try_start_40 .. :try_end_46} :catch_47

    #@46
    goto :goto_19

    #@47
    .line 2411
    :catch_47
    move-exception v1

    #@48
    .line 2412
    .local v1, uhe:Ljava/net/UnknownHostException;
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@4b
    move-result v2

    #@4c
    if-eqz v2, :cond_19

    #@4e
    const-string v2, "GpsLocationProvider"

    #@50
    const-string v3, "bad ipaddress"

    #@52
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_19

    #@56
    .line 2421
    .end local v1           #uhe:Ljava/net/UnknownHostException;
    :cond_56
    if-ne v0, v4, :cond_66

    #@58
    .line 2422
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@5b
    move-result v2

    #@5c
    if-eqz v2, :cond_2f

    #@5e
    const-string v2, "GpsLocationProvider"

    #@60
    const-string v3, "Phone.APN_REQUEST_STARTED"

    #@62
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    goto :goto_2f

    #@66
    .line 2425
    :cond_66
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@69
    move-result v2

    #@6a
    if-eqz v2, :cond_84

    #@6c
    const-string v2, "GpsLocationProvider"

    #@6e
    new-instance v3, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v4, "startUsingNetworkFeature failed with "

    #@75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v3

    #@81
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 2426
    :cond_84
    iput v5, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@86
    goto :goto_2f
.end method

.method private getAgpsType()I
    .registers 2

    #@0
    .prologue
    .line 2314
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAgpsType:I

    #@2
    return v0
.end method

.method private getApn(Landroid/net/NetworkInfo;)Ljava/lang/String;
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 2323
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$5300(Lcom/android/server/location/GpsLocationProvider;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-direct {p0, p1, v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getApn(Landroid/net/NetworkInfo;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private getApn(Landroid/net/NetworkInfo;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "info"
    .parameter "defaultApn"

    #@0
    .prologue
    .line 2327
    if-eqz p1, :cond_8

    #@2
    .line 2328
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@8
    .line 2330
    :cond_8
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@a
    if-nez v0, :cond_e

    #@c
    .line 2333
    iput-object p2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@e
    .line 2336
    :cond_e
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@10
    return-object v0
.end method

.method private getBearerType(Landroid/net/NetworkInfo;)I
    .registers 15
    .parameter "info"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 2339
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@4
    if-nez v0, :cond_c

    #@6
    .line 2340
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->getApn(Landroid/net/NetworkInfo;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@c
    .line 2342
    :cond_c
    const/4 v8, 0x0

    #@d
    .line 2343
    .local v8, ipProtocol:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@f
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$600(Lcom/android/server/location/GpsLocationProvider;)Landroid/content/Context;

    #@12
    move-result-object v0

    #@13
    const-string v1, "phone"

    #@15
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@18
    move-result-object v10

    #@19
    check-cast v10, Landroid/telephony/TelephonyManager;

    #@1b
    .line 2347
    .local v10, phone:Landroid/telephony/TelephonyManager;
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@1d
    const/4 v1, 0x3

    #@1e
    if-ne v0, v1, :cond_3c

    #@20
    .line 2348
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@23
    move-result v9

    #@24
    .line 2349
    .local v9, networkType:I
    const/4 v0, 0x4

    #@25
    if-eq v0, v9, :cond_34

    #@27
    const/4 v0, 0x5

    #@28
    if-eq v0, v9, :cond_34

    #@2a
    const/4 v0, 0x6

    #@2b
    if-eq v0, v9, :cond_34

    #@2d
    const/4 v0, 0x7

    #@2e
    if-eq v0, v9, :cond_34

    #@30
    const/16 v0, 0xc

    #@32
    if-ne v0, v9, :cond_3c

    #@34
    .line 2354
    :cond_34
    const-string v0, "persist.telephony.cdma.protocol"

    #@36
    const-string v1, "IP"

    #@38
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v8

    #@3c
    .line 2358
    .end local v9           #networkType:I
    :cond_3c
    if-nez v8, :cond_a1

    #@3e
    .line 2359
    const-string v4, "current = 1"

    #@40
    .line 2360
    .local v4, selection:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    const-string v1, " and apn = \'"

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string v1, "\'"

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    .line 2361
    new-instance v0, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    const-string v1, " and carrier_enabled = 1"

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    .line 2366
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@74
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$600(Lcom/android/server/location/GpsLocationProvider;)Landroid/content/Context;

    #@77
    move-result-object v0

    #@78
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@7a
    invoke-static {v1}, Lcom/android/server/location/GpsLocationProvider;->access$600(Lcom/android/server/location/GpsLocationProvider;)Landroid/content/Context;

    #@7d
    move-result-object v1

    #@7e
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@81
    move-result-object v1

    #@82
    sget-object v2, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@84
    new-array v3, v12, [Ljava/lang/String;

    #@86
    const-string v5, "protocol"

    #@88
    aput-object v5, v3, v11

    #@8a
    const/4 v5, 0x0

    #@8b
    const-string v6, "name ASC"

    #@8d
    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@90
    move-result-object v7

    #@91
    .line 2370
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_a1

    #@93
    .line 2372
    :try_start_93
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@96
    move-result v0

    #@97
    if-eqz v0, :cond_9e

    #@99
    .line 2373
    const/4 v0, 0x0

    #@9a
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_9d
    .catchall {:try_start_93 .. :try_end_9d} :catchall_e6

    #@9d
    move-result-object v8

    #@9e
    .line 2376
    :cond_9e
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@a1
    .line 2380
    .end local v4           #selection:Ljava/lang/String;
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_a1
    const-string v0, "GpsLocationProvider"

    #@a3
    new-instance v1, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v2, "ipProtocol: "

    #@aa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v1

    #@b2
    const-string v2, " apn: "

    #@b4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v1

    #@b8
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mAPN:Ljava/lang/String;

    #@ba
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v1

    #@be
    const-string v2, " networkType: "

    #@c0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v1

    #@c4
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkTypeName()Ljava/lang/String;

    #@c7
    move-result-object v2

    #@c8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v1

    #@cc
    const-string v2, " state: "

    #@ce
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v1

    #@d2
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mState:I

    #@d4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v1

    #@d8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v1

    #@dc
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 2383
    if-nez v8, :cond_eb

    #@e1
    .line 2384
    iput v11, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mBearerType:I

    #@e3
    .line 2393
    :goto_e3
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mBearerType:I

    #@e5
    return v0

    #@e6
    .line 2376
    .restart local v4       #selection:Ljava/lang/String;
    .restart local v7       #cursor:Landroid/database/Cursor;
    :catchall_e6
    move-exception v0

    #@e7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@ea
    throw v0

    #@eb
    .line 2385
    .end local v4           #selection:Ljava/lang/String;
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_eb
    const-string v0, "IPV6"

    #@ed
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f0
    move-result v0

    #@f1
    if-eqz v0, :cond_f6

    #@f3
    .line 2386
    iput v12, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mBearerType:I

    #@f5
    goto :goto_e3

    #@f6
    .line 2387
    :cond_f6
    const-string v0, "IPV4V6"

    #@f8
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fb
    move-result v0

    #@fc
    if-eqz v0, :cond_102

    #@fe
    .line 2388
    const/4 v0, 0x2

    #@ff
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mBearerType:I

    #@101
    goto :goto_e3

    #@102
    .line 2390
    :cond_102
    iput v11, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mBearerType:I

    #@104
    goto :goto_e3
.end method

.method private getCMConnType()I
    .registers 2

    #@0
    .prologue
    .line 2317
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mCMConnType:I

    #@2
    return v0
.end method

.method private getIpAddr()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 2320
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->mIpAddr:Ljava/net/InetAddress;

    #@2
    return-object v0
.end method
