.class public Lcom/android/server/location/GpsLocationProvider;
.super Ljava/lang/Object;
.source "GpsLocationProvider.java"

# interfaces
.implements Lcom/android/server/location/LocationProviderInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;,
        Lcom/android/server/location/GpsLocationProvider$WifiState;,
        Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;,
        Lcom/android/server/location/GpsLocationProvider$NetworkLocationListener;,
        Lcom/android/server/location/GpsLocationProvider$ProviderHandler;,
        Lcom/android/server/location/GpsLocationProvider$Listener;,
        Lcom/android/server/location/GpsLocationProvider$GpsRequest;
    }
.end annotation


# static fields
.field private static final ADD_LISTENER:I = 0x8

.field private static final AGPS_REF_LOCATION_TYPE_GSM_CELLID:I = 0x1

.field private static final AGPS_REF_LOCATION_TYPE_UMTS_CELLID:I = 0x2

.field private static final AGPS_REG_LOCATION_TYPE_MAC:I = 0x3

.field private static final AGPS_RIL_REQUEST_REFLOC_CELLID:I = 0x1

.field private static final AGPS_RIL_REQUEST_REFLOC_MAC:I = 0x2

.field private static final AGPS_RIL_REQUEST_SETID_IMSI:I = 0x1

.field private static final AGPS_RIL_REQUEST_SETID_MSISDN:I = 0x2

.field private static final AGPS_SETID_TYPE_IMSI:I = 0x1

.field private static final AGPS_SETID_TYPE_MSISDN:I = 0x2

.field private static final AGPS_SETID_TYPE_NONE:I = 0x0

.field private static final ALARM_TIMEOUT:Ljava/lang/String; = "com.android.internal.location.ALARM_TIMEOUT"

.field private static final ALARM_WAKEUP:Ljava/lang/String; = "com.android.internal.location.ALARM_WAKEUP"

.field private static final ALMANAC_MASK:I = 0x1

.field private static final CHECK_LOCATION:I = 0x1

.field private static DEBUG:Z = false

.field private static final DOWNLOAD_XTRA_DATA:I = 0x6

.field private static final DOWNLOAD_XTRA_DATA_FINISHED:I = 0xb

.field private static final ENABLE:I = 0x2

.field private static final EPHEMERIS_MASK:I = 0x0

.field private static final GPS_AGPS_DATA_CONNECTED:I = 0x3

.field private static final GPS_AGPS_DATA_CONN_DONE:I = 0x4

.field private static final GPS_AGPS_DATA_CONN_FAILED:I = 0x5

.field private static final GPS_CAPABILITY_MSA:I = 0x4

.field private static final GPS_CAPABILITY_MSB:I = 0x2

.field private static final GPS_CAPABILITY_ON_DEMAND_TIME:I = 0x10

.field private static final GPS_CAPABILITY_SCHEDULING:I = 0x1

.field private static final GPS_CAPABILITY_SINGLE_SHOT:I = 0x8

.field private static final GPS_DELETE_ALL:I = -0x1

.field private static final GPS_DELETE_ALMANAC:I = 0x2

.field private static final GPS_DELETE_ALMANAC_CORR:I = 0x1000

.field private static final GPS_DELETE_ALMANAC_CORR_GLO:I = 0x40000

.field private static final GPS_DELETE_ALMANAC_GLO:I = 0x8000

.field private static final GPS_DELETE_CELLDB_INFO:I = 0x800

.field private static final GPS_DELETE_EPHEMERIS:I = 0x1

.field private static final GPS_DELETE_EPHEMERIS_GLO:I = 0x4000

.field private static final GPS_DELETE_FREQ_BIAS_EST:I = 0x2000

.field private static final GPS_DELETE_HEALTH:I = 0x40

.field private static final GPS_DELETE_IONO:I = 0x10

.field private static final GPS_DELETE_POSITION:I = 0x4

.field private static final GPS_DELETE_RTI:I = 0x400

.field private static final GPS_DELETE_SADATA:I = 0x200

.field private static final GPS_DELETE_SVDIR:I = 0x80

.field private static final GPS_DELETE_SVDIR_GLO:I = 0x10000

.field private static final GPS_DELETE_SVSTEER:I = 0x100

.field private static final GPS_DELETE_SVSTEER_GLO:I = 0x20000

.field private static final GPS_DELETE_TIME:I = 0x8

.field private static final GPS_DELETE_TIME_GLO:I = 0x100000

.field private static final GPS_DELETE_TIME_GPS:I = 0x80000

.field private static final GPS_DELETE_UTC:I = 0x20

.field public static final GPS_ENGINE_STATUS:Ljava/lang/String; = "com.lge.location.GPS_ENGINE_STATUS"

.field public static final GPS_ENGINE_STATUS_CHANGE:Ljava/lang/String; = "com.lge.location.GPS_ENGINE_STATUS_CHANGE"

.field private static final GPS_POLLING_THRESHOLD_INTERVAL:I = 0x2710

.field private static final GPS_POSITION_MODE_MS_ASSISTED:I = 0x2

.field private static final GPS_POSITION_MODE_MS_BASED:I = 0x1

.field private static final GPS_POSITION_MODE_STANDALONE:I = 0x0

.field private static final GPS_POSITION_RECURRENCE_PERIODIC:I = 0x0

.field private static final GPS_POSITION_RECURRENCE_SINGLE:I = 0x1

.field private static final GPS_RELEASE_AGPS_DATA_CONN:I = 0x2

.field private static final GPS_REQUEST_AGPS_DATA_CONN:I = 0x1

.field private static final GPS_STATUS_ENGINE_OFF:I = 0x4

.field private static final GPS_STATUS_ENGINE_ON:I = 0x3

.field private static final GPS_STATUS_NONE:I = 0x0

.field private static final GPS_STATUS_SESSION_BEGIN:I = 0x1

.field private static final GPS_STATUS_SESSION_END:I = 0x2

.field public static final GPS_XTRA_DATA_DOWNLOAD:Ljava/lang/String; = "com.lge.location.GPS_XTRA_DATA_DOWNLOAD"

.field private static final INJECT_NTP_TIME:I = 0x5

.field private static final INJECT_NTP_TIME_FINISHED:I = 0xa

.field private static final LOCATION_HAS_ACCURACY:I = 0x10

.field private static final LOCATION_HAS_ALTITUDE:I = 0x2

.field private static final LOCATION_HAS_BEARING:I = 0x8

.field private static final LOCATION_HAS_LAT_LONG:I = 0x1

.field private static final LOCATION_HAS_SPEED:I = 0x4

.field private static final LOCATION_INVALID:I = 0x0

.field private static final MAX_SVS:I = 0x20

.field private static final NO_FIX_TIMEOUT:I = 0xea60

.field private static final NTP_INTERVAL:J = 0x5265c00L

.field private static final PROPERTIES:Lcom/android/internal/location/ProviderProperties; = null

.field private static final PROPERTIES_FILE:Ljava/lang/String; = "/etc/gps.conf"

.field private static final RECENT_FIX_TIMEOUT:J = 0x2710L

.field private static final REMOVE_LISTENER:I = 0x9

.field private static final REPORT_AGPS_STATUS:I = 0xc

.field private static final RETRY_INTERVAL:J = 0x493e0L

.field private static final SET_REQUEST:I = 0x3

.field private static final STATE_DOWNLOADING:I = 0x1

.field private static final STATE_IDLE:I = 0x2

.field private static final STATE_PENDING_NETWORK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GpsLocationProvider"

.field private static final UPDATE_LOCATION:I = 0x7

.field private static final UPDATE_NETWORK_STATE:I = 0x4

.field private static final USED_FOR_FIX_MASK:I = 0x2

.field private static VERBOSE:Z = false

.field private static final WAKELOCK_KEY:Ljava/lang/String; = "GpsLocationProvider"

.field private static mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

.field private static mVendorName:Ljava/lang/String;

.field protected static mXtraDownloadDate:J


# instance fields
.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private final mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private final mBroadcastReciever:Landroid/content/BroadcastReceiver;

.field private mC2KServerHost:Ljava/lang/String;

.field private mC2KServerPort:I

.field private mClientUids:[I

.field private final mConnMgr:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private mDownloadXtraDataPending:I

.field private mEnabled:Z

.field private mEngineCapabilities:I

.field private mEngineOn:Z

.field private mFixInterval:I

.field private mFixRequestTime:J

.field private final mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

.field private mHandler:Landroid/os/Handler;

.field private final mILocationManager:Landroid/location/ILocationManager;

.field private mInjectNtpTimePending:I

.field private mLastFixTime:J

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/location/GpsLocationProvider$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mLocation:Landroid/location/Location;

.field private mLocationExtras:Landroid/os/Bundle;

.field private mLocationFlags:I

.field private mLock:Ljava/lang/Object;

.field private final mNIHandler:Lcom/android/internal/location/GpsNetInitiatedHandler;

.field private mNavigating:Z

.field private final mNetInitiatedListener:Landroid/location/INetInitiatedListener;

.field protected mNetworkAvailable:Z

.field private mNmeaBuffer:[B

.field private mNtpServer:Ljava/lang/String;

.field private final mNtpTime:Landroid/util/NtpTrustedTime;

.field private mPeriodicTimeInjection:Z

.field protected mPositionMode:I

.field private mProperties:Ljava/util/Properties;

.field private mSnrs:[F

.field private final mSpirentWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mSpirentWakeLock2:Landroid/os/PowerManager$WakeLock;

.field private mStarted:Z

.field private mStatus:I

.field private mStatusUpdateTime:J

.field private mSuplServerHost:Ljava/lang/String;

.field private mSuplServerPort:I

.field private mSupportsXtra:Z

.field private mSvAzimuths:[F

.field private mSvCount:I

.field private mSvElevations:[F

.field private mSvMasks:[I

.field private mSvs:[I

.field private mTimeToFirstFix:I

.field private final mTimeoutIntent:Landroid/app/PendingIntent;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mWakeupIntent:Landroid/app/PendingIntent;

.field private final mWifiScanReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    .line 111
    sput-boolean v1, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@5
    .line 112
    sput-boolean v1, Lcom/android/server/location/GpsLocationProvider;->VERBOSE:Z

    #@7
    .line 117
    new-instance v0, Lcom/android/internal/location/ProviderProperties;

    #@9
    move v2, v1

    #@a
    move v4, v3

    #@b
    move v5, v1

    #@c
    move v6, v1

    #@d
    move v7, v1

    #@e
    move v9, v1

    #@f
    invoke-direct/range {v0 .. v9}, Lcom/android/internal/location/ProviderProperties;-><init>(ZZZZZZZII)V

    #@12
    sput-object v0, Lcom/android/server/location/GpsLocationProvider;->PROPERTIES:Lcom/android/internal/location/ProviderProperties;

    #@14
    .line 316
    const-string v0, ""

    #@16
    sput-object v0, Lcom/android/server/location/GpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@18
    .line 317
    const-wide/16 v0, 0x0

    #@1a
    sput-wide v0, Lcom/android/server/location/GpsLocationProvider;->mXtraDownloadDate:J

    #@1c
    .line 2196
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->class_init_native()V

    #@1f
    .line 2247
    new-array v0, v8, [Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@21
    sput-object v0, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/location/ILocationManager;)V
    .registers 15
    .parameter "context"
    .parameter "ilocationManager"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/16 v8, 0x20

    #@3
    const/4 v10, 0x1

    #@4
    const/4 v9, 0x0

    #@5
    .line 477
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 228
    new-instance v6, Ljava/lang/Object;

    #@a
    invoke-direct/range {v6 .. v6}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mLock:Ljava/lang/Object;

    #@f
    .line 230
    iput v9, p0, Lcom/android/server/location/GpsLocationProvider;->mLocationFlags:I

    #@11
    .line 233
    iput v10, p0, Lcom/android/server/location/GpsLocationProvider;->mStatus:I

    #@13
    .line 236
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@16
    move-result-wide v6

    #@17
    iput-wide v6, p0, Lcom/android/server/location/GpsLocationProvider;->mStatusUpdateTime:J

    #@19
    .line 273
    iput v9, p0, Lcom/android/server/location/GpsLocationProvider;->mInjectNtpTimePending:I

    #@1b
    .line 274
    iput v9, p0, Lcom/android/server/location/GpsLocationProvider;->mDownloadXtraDataPending:I

    #@1d
    .line 286
    const/16 v6, 0x3e8

    #@1f
    iput v6, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@21
    .line 298
    const-wide/16 v6, 0x0

    #@23
    iput-wide v6, p0, Lcom/android/server/location/GpsLocationProvider;->mFixRequestTime:J

    #@25
    .line 300
    iput v9, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeToFirstFix:I

    #@27
    .line 327
    new-instance v6, Landroid/location/Location;

    #@29
    const-string v7, "gps"

    #@2b
    invoke-direct {v6, v7}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@2e
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@30
    .line 328
    new-instance v6, Landroid/os/Bundle;

    #@32
    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    #@35
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mLocationExtras:Landroid/os/Bundle;

    #@37
    .line 329
    new-instance v6, Ljava/util/ArrayList;

    #@39
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@3c
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@3e
    .line 363
    new-array v6, v9, [I

    #@40
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mClientUids:[I

    #@42
    .line 364
    iput-object v11, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@44
    .line 366
    new-instance v6, Lcom/android/server/location/GpsLocationProvider$1;

    #@46
    invoke-direct {v6, p0}, Lcom/android/server/location/GpsLocationProvider$1;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@49
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

    #@4b
    .line 419
    new-instance v6, Lcom/android/server/location/GpsLocationProvider$2;

    #@4d
    invoke-direct {v6, p0}, Lcom/android/server/location/GpsLocationProvider$2;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@50
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mBroadcastReciever:Landroid/content/BroadcastReceiver;

    #@52
    .line 590
    new-instance v6, Lcom/android/server/location/GpsLocationProvider$4;

    #@54
    invoke-direct {v6, p0}, Lcom/android/server/location/GpsLocationProvider$4;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@57
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiScanReceiver:Landroid/content/BroadcastReceiver;

    #@59
    .line 1871
    new-instance v6, Lcom/android/server/location/GpsLocationProvider$7;

    #@5b
    invoke-direct {v6, p0}, Lcom/android/server/location/GpsLocationProvider$7;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@5e
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNetInitiatedListener:Landroid/location/INetInitiatedListener;

    #@60
    .line 2187
    new-array v6, v8, [I

    #@62
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSvs:[I

    #@64
    .line 2188
    new-array v6, v8, [F

    #@66
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSnrs:[F

    #@68
    .line 2189
    new-array v6, v8, [F

    #@6a
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSvElevations:[F

    #@6c
    .line 2190
    new-array v6, v8, [F

    #@6e
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSvAzimuths:[F

    #@70
    .line 2191
    const/4 v6, 0x3

    #@71
    new-array v6, v6, [I

    #@73
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@75
    .line 2194
    const/16 v6, 0x78

    #@77
    new-array v6, v6, [B

    #@79
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNmeaBuffer:[B

    #@7b
    .line 478
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@7d
    .line 479
    invoke-static {p1}, Landroid/util/NtpTrustedTime;->getInstance(Landroid/content/Context;)Landroid/util/NtpTrustedTime;

    #@80
    move-result-object v6

    #@81
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNtpTime:Landroid/util/NtpTrustedTime;

    #@83
    .line 480
    iput-object p2, p0, Lcom/android/server/location/GpsLocationProvider;->mILocationManager:Landroid/location/ILocationManager;

    #@85
    .line 481
    new-instance v6, Lcom/android/internal/location/GpsNetInitiatedHandler;

    #@87
    invoke-direct {v6, p1}, Lcom/android/internal/location/GpsNetInitiatedHandler;-><init>(Landroid/content/Context;)V

    #@8a
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNIHandler:Lcom/android/internal/location/GpsNetInitiatedHandler;

    #@8c
    .line 483
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@8e
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocationExtras:Landroid/os/Bundle;

    #@90
    invoke-virtual {v6, v7}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    #@93
    .line 486
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@95
    const-string v7, "power"

    #@97
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9a
    move-result-object v3

    #@9b
    check-cast v3, Landroid/os/PowerManager;

    #@9d
    .line 487
    .local v3, powerManager:Landroid/os/PowerManager;
    const-string v6, "GpsLocationProvider"

    #@9f
    invoke-virtual {v3, v10, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@a2
    move-result-object v6

    #@a3
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@a5
    .line 488
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@a7
    invoke-virtual {v6, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@aa
    .line 490
    const-string v6, "GPS-SPIRENT"

    #@ac
    invoke-virtual {v3, v10, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@af
    move-result-object v6

    #@b0
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock:Landroid/os/PowerManager$WakeLock;

    #@b2
    .line 491
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock:Landroid/os/PowerManager$WakeLock;

    #@b4
    invoke-virtual {v6, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@b7
    .line 492
    const-string v6, "GPS-SPIRENT2"

    #@b9
    invoke-virtual {v3, v10, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@bc
    move-result-object v6

    #@bd
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock2:Landroid/os/PowerManager$WakeLock;

    #@bf
    .line 493
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock2:Landroid/os/PowerManager$WakeLock;

    #@c1
    invoke-virtual {v6, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@c4
    .line 495
    new-instance v6, Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@c6
    invoke-direct {v6, p0}, Lcom/android/server/location/GpsLocationProvider$WifiState;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@c9
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@cb
    .line 497
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@cd
    const-string v7, "alarm"

    #@cf
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d2
    move-result-object v6

    #@d3
    check-cast v6, Landroid/app/AlarmManager;

    #@d5
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@d7
    .line 498
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@d9
    new-instance v7, Landroid/content/Intent;

    #@db
    const-string v8, "com.android.internal.location.ALARM_WAKEUP"

    #@dd
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e0
    invoke-static {v6, v9, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@e3
    move-result-object v6

    #@e4
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeupIntent:Landroid/app/PendingIntent;

    #@e6
    .line 499
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@e8
    new-instance v7, Landroid/content/Intent;

    #@ea
    const-string v8, "com.android.internal.location.ALARM_TIMEOUT"

    #@ec
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ef
    invoke-static {v6, v9, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@f2
    move-result-object v6

    #@f3
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeoutIntent:Landroid/app/PendingIntent;

    #@f5
    .line 501
    const-string v6, "connectivity"

    #@f7
    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@fa
    move-result-object v6

    #@fb
    check-cast v6, Landroid/net/ConnectivityManager;

    #@fd
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@ff
    .line 504
    const-string v6, "batteryinfo"

    #@101
    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@104
    move-result-object v6

    #@105
    invoke-static {v6}, Lcom/android/internal/app/IBatteryStats$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IBatteryStats;

    #@108
    move-result-object v6

    #@109
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@10b
    .line 506
    new-instance v6, Ljava/util/Properties;

    #@10d
    invoke-direct {v6}, Ljava/util/Properties;-><init>()V

    #@110
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@112
    .line 508
    :try_start_112
    new-instance v1, Ljava/io/File;

    #@114
    const-string v6, "/etc/gps.conf"

    #@116
    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@119
    .line 509
    .local v1, file:Ljava/io/File;
    new-instance v5, Ljava/io/FileInputStream;

    #@11b
    invoke-direct {v5, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@11e
    .line 510
    .local v5, stream:Ljava/io/FileInputStream;
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@120
    invoke-virtual {v6, v5}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    #@123
    .line 511
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    #@126
    .line 514
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@128
    const-string v7, "NTP_SERVER"

    #@12a
    const/4 v8, 0x0

    #@12b
    invoke-virtual {v6, v7, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12e
    move-result-object v6

    #@12f
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNtpServer:Ljava/lang/String;

    #@131
    .line 516
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@133
    const-string v7, "VENDOR"

    #@135
    const-string v8, ""

    #@137
    invoke-virtual {v6, v7, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13a
    move-result-object v6

    #@13b
    sput-object v6, Lcom/android/server/location/GpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@13d
    .line 519
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@13f
    const-string v7, "SUPL_HOST"

    #@141
    invoke-virtual {v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@144
    move-result-object v6

    #@145
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSuplServerHost:Ljava/lang/String;

    #@147
    .line 520
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@149
    const-string v7, "SUPL_PORT"

    #@14b
    invoke-virtual {v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@14e
    move-result-object v2

    #@14f
    .line 521
    .local v2, portString:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSuplServerHost:Ljava/lang/String;
    :try_end_151
    .catch Ljava/io/IOException; {:try_start_112 .. :try_end_151} :catch_1e6

    #@151
    if-eqz v6, :cond_15b

    #@153
    if-eqz v2, :cond_15b

    #@155
    .line 523
    :try_start_155
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@158
    move-result v6

    #@159
    iput v6, p0, Lcom/android/server/location/GpsLocationProvider;->mSuplServerPort:I
    :try_end_15b
    .catch Ljava/lang/NumberFormatException; {:try_start_155 .. :try_end_15b} :catch_1cb
    .catch Ljava/io/IOException; {:try_start_155 .. :try_end_15b} :catch_1e6

    #@15b
    .line 529
    :cond_15b
    :goto_15b
    :try_start_15b
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@15d
    const-string v7, "C2K_HOST"

    #@15f
    invoke-virtual {v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@162
    move-result-object v6

    #@163
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mC2KServerHost:Ljava/lang/String;

    #@165
    .line 530
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@167
    const-string v7, "C2K_PORT"

    #@169
    invoke-virtual {v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@16c
    move-result-object v2

    #@16d
    .line 531
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mC2KServerHost:Ljava/lang/String;
    :try_end_16f
    .catch Ljava/io/IOException; {:try_start_15b .. :try_end_16f} :catch_1e6

    #@16f
    if-eqz v6, :cond_179

    #@171
    if-eqz v2, :cond_179

    #@173
    .line 533
    :try_start_173
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@176
    move-result v6

    #@177
    iput v6, p0, Lcom/android/server/location/GpsLocationProvider;->mC2KServerPort:I
    :try_end_179
    .catch Ljava/lang/NumberFormatException; {:try_start_173 .. :try_end_179} :catch_1ef
    .catch Ljava/io/IOException; {:try_start_173 .. :try_end_179} :catch_1e6

    #@179
    .line 544
    .end local v1           #file:Ljava/io/File;
    .end local v2           #portString:Ljava/lang/String;
    .end local v5           #stream:Ljava/io/FileInputStream;
    :cond_179
    :goto_179
    const-string v6, "LGU"

    #@17b
    sget-object v7, Lcom/android/server/location/GpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@17d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@180
    move-result v6

    #@181
    if-eqz v6, :cond_186

    #@183
    .line 545
    invoke-virtual {p0}, Lcom/android/server/location/GpsLocationProvider;->readXtraDownloadDate()V

    #@186
    .line 550
    :cond_186
    new-instance v6, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;

    #@188
    invoke-direct {v6, p0}, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@18b
    iput-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@18d
    .line 551
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->listenForBroadcasts()V

    #@190
    .line 554
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@192
    new-instance v7, Lcom/android/server/location/GpsLocationProvider$3;

    #@194
    invoke-direct {v7, p0}, Lcom/android/server/location/GpsLocationProvider$3;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@197
    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@19a
    .line 564
    const-string v6, "ro.build.target_operator"

    #@19c
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@19f
    move-result-object v6

    #@1a0
    const-string v7, "TMO"

    #@1a2
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a5
    move-result v6

    #@1a6
    if-eqz v6, :cond_1ca

    #@1a8
    const-string v6, "ro.build.target_country"

    #@1aa
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1ad
    move-result-object v6

    #@1ae
    const-string v7, "US"

    #@1b0
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b3
    move-result v6

    #@1b4
    if-eqz v6, :cond_1ca

    #@1b6
    .line 567
    const-string v6, "persist.service.privacy.enable"

    #@1b8
    const-string v7, "NA"

    #@1ba
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1bd
    move-result-object v4

    #@1be
    .line 568
    .local v4, privacy_check:Ljava/lang/String;
    const-string v6, "1"

    #@1c0
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c3
    move-result v6

    #@1c4
    if-eqz v6, :cond_20a

    #@1c6
    .line 571
    sput-boolean v10, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@1c8
    .line 572
    sput-boolean v10, Lcom/android/server/location/GpsLocationProvider;->VERBOSE:Z

    #@1ca
    .line 582
    .end local v4           #privacy_check:Ljava/lang/String;
    :cond_1ca
    :goto_1ca
    return-void

    #@1cb
    .line 524
    .restart local v1       #file:Ljava/io/File;
    .restart local v2       #portString:Ljava/lang/String;
    .restart local v5       #stream:Ljava/io/FileInputStream;
    :catch_1cb
    move-exception v0

    #@1cc
    .line 525
    .local v0, e:Ljava/lang/NumberFormatException;
    :try_start_1cc
    const-string v6, "GpsLocationProvider"

    #@1ce
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d3
    const-string v8, "unable to parse SUPL_PORT: "

    #@1d5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v7

    #@1d9
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v7

    #@1dd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e0
    move-result-object v7

    #@1e1
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e4
    .catch Ljava/io/IOException; {:try_start_1cc .. :try_end_1e4} :catch_1e6

    #@1e4
    goto/16 :goto_15b

    #@1e6
    .line 538
    .end local v0           #e:Ljava/lang/NumberFormatException;
    .end local v1           #file:Ljava/io/File;
    .end local v2           #portString:Ljava/lang/String;
    .end local v5           #stream:Ljava/io/FileInputStream;
    :catch_1e6
    move-exception v0

    #@1e7
    .line 539
    .local v0, e:Ljava/io/IOException;
    const-string v6, "GpsLocationProvider"

    #@1e9
    const-string v7, "Could not open GPS configuration file /etc/gps.conf"

    #@1eb
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1ee
    goto :goto_179

    #@1ef
    .line 534
    .end local v0           #e:Ljava/io/IOException;
    .restart local v1       #file:Ljava/io/File;
    .restart local v2       #portString:Ljava/lang/String;
    .restart local v5       #stream:Ljava/io/FileInputStream;
    :catch_1ef
    move-exception v0

    #@1f0
    .line 535
    .local v0, e:Ljava/lang/NumberFormatException;
    :try_start_1f0
    const-string v6, "GpsLocationProvider"

    #@1f2
    new-instance v7, Ljava/lang/StringBuilder;

    #@1f4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f7
    const-string v8, "unable to parse C2K_PORT: "

    #@1f9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v7

    #@1fd
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v7

    #@201
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@204
    move-result-object v7

    #@205
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_208
    .catch Ljava/io/IOException; {:try_start_1f0 .. :try_end_208} :catch_1e6

    #@208
    goto/16 :goto_179

    #@20a
    .line 577
    .end local v0           #e:Ljava/lang/NumberFormatException;
    .end local v1           #file:Ljava/io/File;
    .end local v2           #portString:Ljava/lang/String;
    .end local v5           #stream:Ljava/io/FileInputStream;
    .restart local v4       #privacy_check:Ljava/lang/String;
    :cond_20a
    sput-boolean v9, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@20c
    .line 578
    sput-boolean v9, Lcom/android/server/location/GpsLocationProvider;->VERBOSE:Z

    #@20e
    goto :goto_1ca
.end method

.method static synthetic access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 106
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$1400(Lcom/android/server/location/GpsLocationProvider;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 106
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->startNavigating()V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/server/location/GpsLocationProvider;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNtpServer:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/server/location/GpsLocationProvider;JJI)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 106
    invoke-direct/range {p0 .. p5}, Lcom/android/server/location/GpsLocationProvider;->native_inject_time(JJI)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/android/server/location/GpsLocationProvider;IILjava/lang/Object;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/server/location/GpsLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mPeriodicTimeInjection:Z

    #@2
    return v0
.end method

.method static synthetic access$2900(Lcom/android/server/location/GpsLocationProvider;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 106
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->hibernate()V

    #@3
    return-void
.end method

.method static synthetic access$3000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/Properties;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mProperties:Ljava/util/Properties;

    #@2
    return-object v0
.end method

.method static synthetic access$3100(Lcom/android/server/location/GpsLocationProvider;[BI)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->native_inject_xtra_data([BI)V

    #@3
    return-void
.end method

.method static synthetic access$3200()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 106
    sget-object v0, Lcom/android/server/location/GpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/server/location/GpsLocationProvider;[B)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->broadcastXtraDownload([B)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/location/GpsLocationProvider;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->checkSmsSuplInit(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$4100(Lcom/android/server/location/GpsLocationProvider;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->native_send_ni_response(II)V

    #@3
    return-void
.end method

.method static synthetic access$4200(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 106
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->handleDisable()V

    #@3
    return-void
.end method

.method static synthetic access$4300(Lcom/android/server/location/GpsLocationProvider;Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->handleSetRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V

    #@3
    return-void
.end method

.method static synthetic access$4400(Lcom/android/server/location/GpsLocationProvider;ILandroid/net/NetworkInfo;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->handleUpdateNetworkState(ILandroid/net/NetworkInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$4500(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 106
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->handleInjectNtpTime()V

    #@3
    return-void
.end method

.method static synthetic access$4600(Lcom/android/server/location/GpsLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mSupportsXtra:Z

    #@2
    return v0
.end method

.method static synthetic access$4700(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 106
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->handleDownloadXtraData()V

    #@3
    return-void
.end method

.method static synthetic access$4802(Lcom/android/server/location/GpsLocationProvider;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider;->mInjectNtpTimePending:I

    #@2
    return p1
.end method

.method static synthetic access$4902(Lcom/android/server/location/GpsLocationProvider;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider;->mDownloadXtraDataPending:I

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/server/location/GpsLocationProvider;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->checkWapSuplInit(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$5000(Lcom/android/server/location/GpsLocationProvider;Landroid/location/Location;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->handleUpdateLocation(Landroid/location/Location;)V

    #@3
    return-void
.end method

.method static synthetic access$5100(Lcom/android/server/location/GpsLocationProvider;Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->handleReportAgpsStatus(Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;)V

    #@3
    return-void
.end method

.method static synthetic access$5300(Lcom/android/server/location/GpsLocationProvider;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->getDefaultApn()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$5400(Lcom/android/server/location/GpsLocationProvider;)Landroid/net/ConnectivityManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@2
    return-object v0
.end method

.method static synthetic access$5500(Lcom/android/server/location/GpsLocationProvider;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed(I)V

    #@3
    return-void
.end method

.method static synthetic access$5600(Lcom/android/server/location/GpsLocationProvider;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_closed(I)V

    #@3
    return-void
.end method

.method static synthetic access$5700(Lcom/android/server/location/GpsLocationProvider;ILjava/lang/String;I)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_open(ILjava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$5800(Lcom/android/server/location/GpsLocationProvider;I)Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->getAGpsConnectionInfo(I)Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/location/GpsLocationProvider;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/location/GpsLocationProvider;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/location/GpsLocationProvider;)Lcom/android/server/location/GpsLocationProvider$WifiState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@2
    return-object v0
.end method

.method private associateToNetwork(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "ssid"
    .parameter "password"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1462
    const-string v3, "GpsLocationProvider"

    #@3
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "associateToNetwork begin ssid ="

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    const-string v5, ", password="

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1464
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@25
    invoke-static {v3, p1}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1102(Lcom/android/server/location/GpsLocationProvider$WifiState;Ljava/lang/String;)Ljava/lang/String;

    #@28
    .line 1467
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@2a
    invoke-static {v3}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@31
    move-result-object v1

    #@32
    .line 1468
    .local v1, originalNetworkInfo:Landroid/net/wifi/WifiInfo;
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@34
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@37
    move-result v4

    #@38
    invoke-static {v3, v4}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3402(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@3b
    .line 1469
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@3d
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1202(Lcom/android/server/location/GpsLocationProvider$WifiState;Ljava/lang/String;)Ljava/lang/String;

    #@44
    .line 1470
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@46
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@48
    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getNetworkPreference()I

    #@4b
    move-result v4

    #@4c
    invoke-static {v3, v4}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3502(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@4f
    .line 1472
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@51
    if-eqz v3, :cond_81

    #@53
    const-string v3, "GpsLocationProvider"

    #@55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "saved original wifi info. originalNetId  = "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@62
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3400(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@65
    move-result v5

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    const-string v5, ", originalNetworkPreference = "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@72
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3500(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@75
    move-result v5

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 1475
    :cond_81
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@83
    invoke-virtual {v3, v6}, Landroid/net/ConnectivityManager;->setNetworkPreference(I)V

    #@86
    .line 1476
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@88
    if-eqz v3, :cond_91

    #@8a
    const-string v3, "GpsLocationProvider"

    #@8c
    const-string v4, "network prefence changed to wifi"

    #@8e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 1477
    :cond_91
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@93
    if-eqz v3, :cond_b7

    #@95
    const-string v3, "GpsLocationProvider"

    #@97
    new-instance v4, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v5, "get WPA wifi config for ssid="

    #@9e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v4

    #@a2
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    const-string v5, ", password="

    #@a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v4

    #@b0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v4

    #@b4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 1479
    :cond_b7
    if-nez p2, :cond_122

    #@b9
    .line 1480
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->getWifiConfigurationForOpen(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    #@bc
    move-result-object v2

    #@bd
    .line 1484
    .local v2, wc:Landroid/net/wifi/WifiConfiguration;
    :goto_bd
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@bf
    if-eqz v3, :cond_d9

    #@c1
    const-string v3, "GpsLocationProvider"

    #@c3
    new-instance v4, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v5, "wifi configuration is : "

    #@ca
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v4

    #@ce
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v4

    #@d2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v4

    #@d6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 1485
    :cond_d9
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@db
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@dd
    invoke-static {v4}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@e0
    move-result-object v4

    #@e1
    invoke-virtual {v4, v2}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    #@e4
    move-result v4

    #@e5
    invoke-static {v3, v4}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3602(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@e8
    .line 1487
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@ea
    invoke-static {v3}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3600(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@ed
    move-result v3

    #@ee
    if-gez v3, :cond_127

    #@f0
    .line 1488
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@f2
    if-eqz v3, :cond_11c

    #@f4
    const-string v3, "GpsLocationProvider"

    #@f6
    new-instance v4, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    const-string v5, "ERROR: "

    #@fd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v4

    #@101
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v4

    #@105
    const-string v5, " add Network returned "

    #@107
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v4

    #@10b
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@10d
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3600(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@110
    move-result v5

    #@111
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@114
    move-result-object v4

    #@115
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v4

    #@119
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 1489
    :cond_11c
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@11e
    invoke-static {v3}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1500(Lcom/android/server/location/GpsLocationProvider$WifiState;)V

    #@121
    .line 1503
    :cond_121
    :goto_121
    return-void

    #@122
    .line 1482
    .end local v2           #wc:Landroid/net/wifi/WifiConfiguration;
    :cond_122
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->getWifiConfigurationForWPA(Ljava/lang/String;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    #@125
    move-result-object v2

    #@126
    .restart local v2       #wc:Landroid/net/wifi/WifiConfiguration;
    goto :goto_bd

    #@127
    .line 1491
    :cond_127
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@129
    if-eqz v3, :cond_14d

    #@12b
    const-string v3, "GpsLocationProvider"

    #@12d
    new-instance v4, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v4

    #@136
    const-string v5, " add Network returned "

    #@138
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v4

    #@13c
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@13e
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3600(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@141
    move-result v5

    #@142
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@145
    move-result-object v4

    #@146
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v4

    #@14a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14d
    .line 1494
    :cond_14d
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@14f
    invoke-static {v3}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@152
    move-result-object v3

    #@153
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@155
    invoke-static {v4}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3600(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@158
    move-result v4

    #@159
    invoke-virtual {v3, v4, v6}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    #@15c
    move-result v0

    #@15d
    .line 1496
    .local v0, b:Z
    if-eqz v0, :cond_17c

    #@15f
    .line 1497
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@161
    if-eqz v3, :cond_121

    #@163
    const-string v3, "GpsLocationProvider"

    #@165
    new-instance v4, Ljava/lang/StringBuilder;

    #@167
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16a
    const-string v5, "enableNetwork returned "

    #@16c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v4

    #@170
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@173
    move-result-object v4

    #@174
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@177
    move-result-object v4

    #@178
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    goto :goto_121

    #@17c
    .line 1499
    :cond_17c
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@17e
    if-eqz v3, :cond_198

    #@180
    const-string v3, "GpsLocationProvider"

    #@182
    new-instance v4, Ljava/lang/StringBuilder;

    #@184
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@187
    const-string v5, "ERROR: enableNetwork returned "

    #@189
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v4

    #@18d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@190
    move-result-object v4

    #@191
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@194
    move-result-object v4

    #@195
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@198
    .line 1500
    :cond_198
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@19a
    invoke-static {v3}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1500(Lcom/android/server/location/GpsLocationProvider$WifiState;)V

    #@19d
    goto :goto_121
.end method

.method private broadcastXtraDownload([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 2613
    if-eqz p1, :cond_3b

    #@2
    .line 2614
    const-string v1, "end"

    #@4
    .line 2616
    .local v1, str:Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@7
    move-result-wide v2

    #@8
    sput-wide v2, Lcom/android/server/location/GpsLocationProvider;->mXtraDownloadDate:J

    #@a
    .line 2618
    invoke-virtual {p0}, Lcom/android/server/location/GpsLocationProvider;->saveXtraDownloadDate()V

    #@d
    .line 2623
    :goto_d
    new-instance v0, Landroid/content/Intent;

    #@f
    const-string v2, "com.lge.location.GPS_XTRA_DATA_DOWNLOAD"

    #@11
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@14
    .line 2624
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "xtra_download"

    #@16
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@19
    .line 2625
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1e
    .line 2626
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@20
    if-eqz v2, :cond_3a

    #@22
    const-string v2, "GpsLocationProvider"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "xtra_download : "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 2627
    :cond_3a
    return-void

    #@3b
    .line 2621
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #str:Ljava/lang/String;
    :cond_3b
    const-string v1, "fail"

    #@3d
    .restart local v1       #str:Ljava/lang/String;
    goto :goto_d
.end method

.method private checkSmsSuplInit(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 461
    invoke-static {p1}, Landroid/provider/Telephony$Sms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    #@3
    move-result-object v1

    #@4
    .line 462
    .local v1, messages:[Landroid/telephony/SmsMessage;
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    array-length v3, v1

    #@6
    if-ge v0, v3, :cond_15

    #@8
    .line 463
    aget-object v3, v1, v0

    #@a
    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getUserData()[B

    #@d
    move-result-object v2

    #@e
    .line 464
    .local v2, supl_init:[B
    array-length v3, v2

    #@f
    invoke-direct {p0, v2, v3}, Lcom/android/server/location/GpsLocationProvider;->native_agps_ni_message([BI)V

    #@12
    .line 462
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_5

    #@15
    .line 466
    .end local v2           #supl_init:[B
    :cond_15
    return-void
.end method

.method private checkWapSuplInit(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 469
    const-string v1, "data"

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, [B

    #@8
    move-object v0, v1

    #@9
    check-cast v0, [B

    #@b
    .line 470
    .local v0, supl_init:[B
    array-length v1, v0

    #@c
    invoke-direct {p0, v0, v1}, Lcom/android/server/location/GpsLocationProvider;->native_agps_ni_message([BI)V

    #@f
    .line 471
    return-void
.end method

.method private static native class_init_native()V
.end method

.method private convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "string"

    #@0
    .prologue
    const/16 v2, 0x22

    #@2
    .line 1525
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_b

    #@8
    .line 1526
    const-string p1, ""

    #@a
    .line 1534
    .end local p1
    :cond_a
    :goto_a
    return-object p1

    #@b
    .line 1529
    .restart local p1
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v1

    #@f
    add-int/lit8 v0, v1, -0x1

    #@11
    .line 1530
    .local v0, lastPos:I
    if-ltz v0, :cond_a

    #@13
    const/4 v1, 0x0

    #@14
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v1

    #@18
    if-ne v1, v2, :cond_20

    #@1a
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v1

    #@1e
    if-eq v1, v2, :cond_a

    #@20
    .line 1534
    :cond_20
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "\""

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, "\""

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object p1

    #@39
    goto :goto_a
.end method

.method private deleteAidingData(Landroid/os/Bundle;)Z
    .registers 4
    .parameter "extras"

    #@0
    .prologue
    .line 1143
    if-nez p1, :cond_a

    #@2
    .line 1144
    const/4 v0, -0x1

    #@3
    .line 1171
    .local v0, flags:I
    :cond_3
    :goto_3
    if-eqz v0, :cond_f0

    #@5
    .line 1172
    invoke-direct {p0, v0}, Lcom/android/server/location/GpsLocationProvider;->native_delete_aiding_data(I)V

    #@8
    .line 1173
    const/4 v1, 0x1

    #@9
    .line 1176
    :goto_9
    return v1

    #@a
    .line 1146
    .end local v0           #flags:I
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 1147
    .restart local v0       #flags:I
    const-string v1, "ephemeris"

    #@d
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_15

    #@13
    or-int/lit8 v0, v0, 0x1

    #@15
    .line 1148
    :cond_15
    const-string v1, "almanac"

    #@17
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_1f

    #@1d
    or-int/lit8 v0, v0, 0x2

    #@1f
    .line 1149
    :cond_1f
    const-string v1, "position"

    #@21
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_29

    #@27
    or-int/lit8 v0, v0, 0x4

    #@29
    .line 1150
    :cond_29
    const-string v1, "time"

    #@2b
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_33

    #@31
    or-int/lit8 v0, v0, 0x8

    #@33
    .line 1151
    :cond_33
    const-string v1, "iono"

    #@35
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_3d

    #@3b
    or-int/lit8 v0, v0, 0x10

    #@3d
    .line 1152
    :cond_3d
    const-string v1, "utc"

    #@3f
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@42
    move-result v1

    #@43
    if-eqz v1, :cond_47

    #@45
    or-int/lit8 v0, v0, 0x20

    #@47
    .line 1153
    :cond_47
    const-string v1, "health"

    #@49
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@4c
    move-result v1

    #@4d
    if-eqz v1, :cond_51

    #@4f
    or-int/lit8 v0, v0, 0x40

    #@51
    .line 1154
    :cond_51
    const-string v1, "svdir"

    #@53
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_5b

    #@59
    or-int/lit16 v0, v0, 0x80

    #@5b
    .line 1155
    :cond_5b
    const-string v1, "svsteer"

    #@5d
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@60
    move-result v1

    #@61
    if-eqz v1, :cond_65

    #@63
    or-int/lit16 v0, v0, 0x100

    #@65
    .line 1156
    :cond_65
    const-string v1, "sadata"

    #@67
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@6a
    move-result v1

    #@6b
    if-eqz v1, :cond_6f

    #@6d
    or-int/lit16 v0, v0, 0x200

    #@6f
    .line 1157
    :cond_6f
    const-string v1, "rti"

    #@71
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@74
    move-result v1

    #@75
    if-eqz v1, :cond_79

    #@77
    or-int/lit16 v0, v0, 0x400

    #@79
    .line 1158
    :cond_79
    const-string v1, "celldb-info"

    #@7b
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@7e
    move-result v1

    #@7f
    if-eqz v1, :cond_83

    #@81
    or-int/lit16 v0, v0, 0x800

    #@83
    .line 1159
    :cond_83
    const-string v1, "almanac-corr"

    #@85
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@88
    move-result v1

    #@89
    if-eqz v1, :cond_8d

    #@8b
    or-int/lit16 v0, v0, 0x1000

    #@8d
    .line 1160
    :cond_8d
    const-string v1, "freq-bias-est"

    #@8f
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@92
    move-result v1

    #@93
    if-eqz v1, :cond_97

    #@95
    or-int/lit16 v0, v0, 0x2000

    #@97
    .line 1161
    :cond_97
    const-string v1, "ephemeris-GLO"

    #@99
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@9c
    move-result v1

    #@9d
    if-eqz v1, :cond_a1

    #@9f
    or-int/lit16 v0, v0, 0x4000

    #@a1
    .line 1162
    :cond_a1
    const-string v1, "almanac-GLO"

    #@a3
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@a6
    move-result v1

    #@a7
    if-eqz v1, :cond_ad

    #@a9
    const v1, 0x8000

    #@ac
    or-int/2addr v0, v1

    #@ad
    .line 1163
    :cond_ad
    const-string v1, "svdir-GLO"

    #@af
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@b2
    move-result v1

    #@b3
    if-eqz v1, :cond_b8

    #@b5
    const/high16 v1, 0x1

    #@b7
    or-int/2addr v0, v1

    #@b8
    .line 1164
    :cond_b8
    const-string v1, "svsteer-GLO"

    #@ba
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@bd
    move-result v1

    #@be
    if-eqz v1, :cond_c3

    #@c0
    const/high16 v1, 0x2

    #@c2
    or-int/2addr v0, v1

    #@c3
    .line 1165
    :cond_c3
    const-string v1, "almanac-corr-GLO"

    #@c5
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@c8
    move-result v1

    #@c9
    if-eqz v1, :cond_ce

    #@cb
    const/high16 v1, 0x4

    #@cd
    or-int/2addr v0, v1

    #@ce
    .line 1166
    :cond_ce
    const-string v1, "time-gps"

    #@d0
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@d3
    move-result v1

    #@d4
    if-eqz v1, :cond_d9

    #@d6
    const/high16 v1, 0x8

    #@d8
    or-int/2addr v0, v1

    #@d9
    .line 1167
    :cond_d9
    const-string v1, "time-GLO"

    #@db
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@de
    move-result v1

    #@df
    if-eqz v1, :cond_e4

    #@e1
    const/high16 v1, 0x10

    #@e3
    or-int/2addr v0, v1

    #@e4
    .line 1168
    :cond_e4
    const-string v1, "all"

    #@e6
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@e9
    move-result v1

    #@ea
    if-eqz v1, :cond_3

    #@ec
    or-int/lit8 v0, v0, -0x1

    #@ee
    goto/16 :goto_3

    #@f0
    .line 1176
    :cond_f0
    const/4 v1, 0x0

    #@f1
    goto/16 :goto_9
.end method

.method private getAGpsConnectionInfo(I)Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;
    .registers 9
    .parameter "connType"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 2249
    sget-boolean v1, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@6
    if-eqz v1, :cond_20

    #@8
    const-string v1, "GpsLocationProvider"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getAGpsConnectionInfo connType - "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 2250
    :cond_20
    packed-switch p1, :pswitch_data_62

    #@23
    .line 2266
    :goto_23
    return-object v0

    #@24
    .line 2254
    :pswitch_24
    sget-object v1, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@26
    aget-object v1, v1, v4

    #@28
    if-nez v1, :cond_33

    #@2a
    .line 2255
    sget-object v1, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@2c
    new-instance v2, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@2e
    invoke-direct {v2, p0, v4, p1, v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;-><init>(Lcom/android/server/location/GpsLocationProvider;IILcom/android/server/location/GpsLocationProvider$1;)V

    #@31
    aput-object v2, v1, v4

    #@33
    .line 2256
    :cond_33
    sget-object v0, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@35
    aget-object v0, v0, v4

    #@37
    goto :goto_23

    #@38
    .line 2258
    :pswitch_38
    sget-object v1, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@3a
    aget-object v1, v1, v5

    #@3c
    if-nez v1, :cond_48

    #@3e
    .line 2259
    sget-object v1, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@40
    new-instance v2, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@42
    const/4 v3, 0x3

    #@43
    invoke-direct {v2, p0, v3, p1, v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;-><init>(Lcom/android/server/location/GpsLocationProvider;IILcom/android/server/location/GpsLocationProvider$1;)V

    #@46
    aput-object v2, v1, v5

    #@48
    .line 2260
    :cond_48
    sget-object v0, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@4a
    aget-object v0, v0, v5

    #@4c
    goto :goto_23

    #@4d
    .line 2262
    :pswitch_4d
    sget-object v1, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@4f
    aget-object v1, v1, v6

    #@51
    if-nez v1, :cond_5c

    #@53
    .line 2263
    sget-object v1, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@55
    new-instance v2, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@57
    invoke-direct {v2, p0, v5, p1, v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;-><init>(Lcom/android/server/location/GpsLocationProvider;IILcom/android/server/location/GpsLocationProvider$1;)V

    #@5a
    aput-object v2, v1, v6

    #@5c
    .line 2264
    :cond_5c
    sget-object v0, Lcom/android/server/location/GpsLocationProvider;->mAGpsConnections:[Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@5e
    aget-object v0, v0, v6

    #@60
    goto :goto_23

    #@61
    .line 2250
    nop

    #@62
    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_38
        :pswitch_24
        :pswitch_24
        :pswitch_4d
    .end packed-switch
.end method

.method private getCurrentNetId()I
    .registers 3

    #@0
    .prologue
    .line 1511
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@2
    invoke-static {v1}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@9
    move-result-object v0

    #@a
    .line 1512
    .local v0, info:Landroid/net/wifi/WifiInfo;
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@d
    move-result v1

    #@e
    return v1
.end method

.method private getDefaultApn()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 2153
    const-string v0, "content://telephony/carriers/preferapn"

    #@4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v2

    #@8
    .line 2154
    .local v2, uri:Landroid/net/Uri;
    const/4 v7, 0x0

    #@9
    .line 2159
    .local v7, apn:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@b
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v1

    #@11
    const/4 v3, 0x1

    #@12
    new-array v3, v3, [Ljava/lang/String;

    #@14
    const-string v5, "apn"

    #@16
    aput-object v5, v3, v6

    #@18
    const-string v6, "name ASC"

    #@1a
    move-object v5, v4

    #@1b
    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1e
    move-result-object v8

    #@1f
    .line 2163
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_2f

    #@21
    .line 2165
    :try_start_21
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_2c

    #@27
    .line 2166
    const/4 v0, 0x0

    #@28
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2b
    .catchall {:try_start_21 .. :try_end_2b} :catchall_34

    #@2b
    move-result-object v7

    #@2c
    .line 2169
    :cond_2c
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@2f
    .line 2173
    :cond_2f
    if-nez v7, :cond_33

    #@31
    .line 2174
    const-string v7, "dummy-apn"

    #@33
    .line 2177
    :cond_33
    return-object v7

    #@34
    .line 2169
    :catchall_34
    move-exception v0

    #@35
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@38
    throw v0
.end method

.method private getWifiConfigurationForNoAuth(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;
    .registers 5
    .parameter "SSID"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1538
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    #@3
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@6
    .line 1539
    .local v0, conf:Landroid/net/wifi/WifiConfiguration;
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@8
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@b
    .line 1540
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@d
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@10
    .line 1541
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@12
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@15
    .line 1542
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@17
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@1a
    .line 1543
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@1c
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@1f
    .line 1544
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@21
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    #@24
    .line 1545
    iput-boolean v2, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@26
    .line 1546
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@2c
    .line 1548
    const/4 v1, 0x2

    #@2d
    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@2f
    .line 1550
    return-object v0
.end method

.method private getWifiConfigurationForOpen(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;
    .registers 7
    .parameter "SSID"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x1

    #@3
    .line 1555
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    #@5
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@8
    .line 1556
    .local v0, conf:Landroid/net/wifi/WifiConfiguration;
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@a
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@d
    .line 1557
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@f
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@12
    .line 1558
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@14
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@17
    .line 1559
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@19
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@1c
    .line 1560
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@1e
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@21
    .line 1561
    iput-boolean v3, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@23
    .line 1562
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@29
    .line 1563
    iput v2, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@2b
    .line 1565
    iput v2, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@2d
    .line 1566
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@2f
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@32
    .line 1567
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@34
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    #@37
    .line 1568
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@39
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@3c
    .line 1569
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@3e
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@41
    .line 1570
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@43
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    #@46
    .line 1571
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@48
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@4b
    .line 1572
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@4d
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    #@50
    .line 1573
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@52
    const/4 v2, 0x3

    #@53
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    #@56
    .line 1574
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@58
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@5b
    .line 1576
    return-object v0
.end method

.method private getWifiConfigurationForWEP(Ljava/lang/String;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;
    .registers 8
    .parameter "SSID"
    .parameter "password"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1615
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    #@4
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@7
    .line 1616
    .local v0, conf:Landroid/net/wifi/WifiConfiguration;
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@9
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@c
    .line 1617
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@e
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@11
    .line 1618
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@13
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@16
    .line 1619
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@18
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@1b
    .line 1620
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@1d
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@20
    .line 1621
    iput-boolean v3, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@22
    .line 1622
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@28
    .line 1623
    const/16 v1, 0x28

    #@2a
    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@2c
    .line 1625
    iput v4, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@2e
    .line 1626
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@30
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@33
    .line 1627
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@35
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@38
    .line 1628
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@3a
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@3d
    .line 1629
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@3f
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@42
    .line 1630
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@44
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@47
    .line 1631
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@49
    const/4 v2, 0x2

    #@4a
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    #@4d
    .line 1632
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@4f
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@52
    .line 1633
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@54
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@57
    .line 1634
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@59
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@5c
    .line 1636
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@5e
    invoke-direct {p0, p2}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    aput-object v2, v1, v3

    #@64
    .line 1637
    iput v3, v0, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@66
    .line 1639
    return-object v0
.end method

.method private getWifiConfigurationForWPA(Ljava/lang/String;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;
    .registers 9
    .parameter "SSID"
    .parameter "password"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x1

    #@3
    .line 1580
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    #@5
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@8
    .line 1581
    .local v0, conf:Landroid/net/wifi/WifiConfiguration;
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@a
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@d
    .line 1582
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@f
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@12
    .line 1583
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@14
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@17
    .line 1584
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@19
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@1c
    .line 1585
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@1e
    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    #@21
    .line 1586
    iput-boolean v4, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@23
    .line 1587
    invoke-direct {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@29
    .line 1588
    iput v3, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@2b
    .line 1590
    const-string v1, "[0-9A-Fa-f]{64}"

    #@2d
    invoke-virtual {p2, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@30
    move-result v1

    #@31
    if-eqz v1, :cond_6d

    #@33
    .line 1591
    const-string v1, "GpsLocationProvider"

    #@35
    const-string v2, "A 64 bit hex password entered."

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1592
    iput-object p2, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@3c
    .line 1598
    :goto_3c
    iput v3, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@3e
    .line 1601
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@40
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@43
    .line 1602
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@45
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@48
    .line 1603
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@4a
    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    #@4d
    .line 1604
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@4f
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@52
    .line 1605
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@54
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@57
    .line 1606
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@59
    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    #@5c
    .line 1607
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@5e
    const/4 v2, 0x3

    #@5f
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    #@62
    .line 1608
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@64
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    #@67
    .line 1609
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@69
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    #@6c
    .line 1611
    return-object v0

    #@6d
    .line 1594
    :cond_6d
    const-string v1, "GpsLocationProvider"

    #@6f
    const-string v2, "A normal password entered: I am quoting it."

    #@71
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 1595
    invoke-direct {p0, p2}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v1

    #@78
    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@7a
    goto :goto_3c
.end method

.method private handleDisable()V
    .registers 3

    #@0
    .prologue
    .line 959
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "GpsLocationProvider"

    #@6
    const-string v1, "handleDisable"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 967
    :cond_b
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->stopNavigating()V

    #@e
    .line 968
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@10
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeupIntent:Landroid/app/PendingIntent;

    #@12
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@15
    .line 969
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@17
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeoutIntent:Landroid/app/PendingIntent;

    #@19
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@1c
    .line 972
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_cleanup()V

    #@1f
    .line 973
    return-void
.end method

.method private handleDownloadXtraData()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 834
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mDownloadXtraDataPending:I

    #@3
    if-ne v0, v1, :cond_6

    #@5
    .line 880
    :cond_5
    :goto_5
    return-void

    #@6
    .line 838
    :cond_6
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNetworkAvailable:Z

    #@8
    if-nez v0, :cond_1c

    #@a
    .line 840
    const/4 v0, 0x0

    #@b
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider;->mDownloadXtraDataPending:I

    #@d
    .line 843
    const-string v0, "LGU"

    #@f
    sget-object v1, Lcom/android/server/location/GpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_5

    #@17
    const/4 v0, 0x0

    #@18
    invoke-direct {p0, v0}, Lcom/android/server/location/GpsLocationProvider;->broadcastXtraDownload([B)V

    #@1b
    goto :goto_5

    #@1c
    .line 847
    :cond_1c
    iput v1, p0, Lcom/android/server/location/GpsLocationProvider;->mDownloadXtraDataPending:I

    #@1e
    .line 850
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@20
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@23
    .line 851
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    #@25
    new-instance v1, Lcom/android/server/location/GpsLocationProvider$6;

    #@27
    invoke-direct {v1, p0}, Lcom/android/server/location/GpsLocationProvider$6;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@2a
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    #@2d
    goto :goto_5
.end method

.method private handleInjectNtpTime()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 765
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mInjectNtpTimePending:I

    #@3
    if-ne v0, v1, :cond_6

    #@5
    .line 830
    :goto_5
    return-void

    #@6
    .line 769
    :cond_6
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNetworkAvailable:Z

    #@8
    if-nez v0, :cond_e

    #@a
    .line 771
    const/4 v0, 0x0

    #@b
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider;->mInjectNtpTimePending:I

    #@d
    goto :goto_5

    #@e
    .line 774
    :cond_e
    iput v1, p0, Lcom/android/server/location/GpsLocationProvider;->mInjectNtpTimePending:I

    #@10
    .line 777
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@12
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@15
    .line 778
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    #@17
    new-instance v1, Lcom/android/server/location/GpsLocationProvider$5;

    #@19
    invoke-direct {v1, p0}, Lcom/android/server/location/GpsLocationProvider$5;-><init>(Lcom/android/server/location/GpsLocationProvider;)V

    #@1c
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    #@1f
    goto :goto_5
.end method

.method private handleReportAgpsStatus(Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;)V
    .registers 16
    .parameter "rasm"

    #@0
    .prologue
    const/4 v13, 0x3

    #@1
    const/4 v12, 0x2

    #@2
    const/4 v11, 0x0

    #@3
    const/4 v10, 0x1

    #@4
    .line 1687
    iget v6, p1, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;->type:I

    #@6
    .line 1688
    .local v6, type:I
    iget v5, p1, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;->status:I

    #@8
    .line 1689
    .local v5, status:I
    iget-object v2, p1, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;->ipAddr:[B

    #@a
    .line 1690
    .local v2, ipAddr:[B
    iget-object v4, p1, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;->ssid:Ljava/lang/String;

    #@c
    .line 1691
    .local v4, ssid:Ljava/lang/String;
    iget-object v3, p1, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;->password:Ljava/lang/String;

    #@e
    .line 1693
    .local v3, password:Ljava/lang/String;
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@10
    if-eqz v7, :cond_52

    #@12
    const-string v7, "GpsLocationProvider"

    #@14
    new-instance v8, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v9, "handleReportAgpsStatus with type = "

    #@1b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    const-string v9, "status = "

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    const-string v9, "ipAddr = "

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    const-string v9, "ssid = "

    #@39
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    const-string v9, "password = "

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1700
    :cond_52
    invoke-direct {p0, v4}, Lcom/android/server/location/GpsLocationProvider;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    .line 1702
    invoke-direct {p0, v6}, Lcom/android/server/location/GpsLocationProvider;->getAGpsConnectionInfo(I)Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@59
    move-result-object v0

    #@5a
    .line 1703
    .local v0, agpsConnInfo:Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;
    if-nez v0, :cond_79

    #@5c
    .line 1704
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@5e
    if-eqz v7, :cond_78

    #@60
    const-string v7, "GpsLocationProvider"

    #@62
    new-instance v8, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v9, "reportAGpsStatus agpsConnInfo is null for type "

    #@69
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v8

    #@71
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v8

    #@75
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 1820
    :cond_78
    :goto_78
    return-void

    #@79
    .line 1709
    :cond_79
    packed-switch v5, :pswitch_data_272

    #@7c
    goto :goto_78

    #@7d
    .line 1711
    :pswitch_7d
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@7f
    if-eqz v7, :cond_88

    #@81
    const-string v7, "GpsLocationProvider"

    #@83
    const-string v8, "GPS_REQUEST_AGPS_DATA_CONN"

    #@85
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 1713
    :cond_88
    packed-switch v6, :pswitch_data_280

    #@8b
    .line 1778
    :pswitch_8b
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@8d
    if-eqz v7, :cond_78

    #@8f
    const-string v7, "GpsLocationProvider"

    #@91
    const-string v8, "type == unknown"

    #@93
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    goto :goto_78

    #@97
    .line 1716
    :pswitch_97
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@99
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1800(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@9c
    move-result v8

    #@9d
    invoke-virtual {v7, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@a0
    move-result-object v1

    #@a1
    .line 1717
    .local v1, info:Landroid/net/NetworkInfo;
    invoke-static {v0, v1, v2}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$3700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;[B)V

    #@a4
    .line 1718
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1600(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@a7
    move-result v7

    #@a8
    if-eq v7, v12, :cond_b0

    #@aa
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1600(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@ad
    move-result v7

    #@ae
    if-ne v7, v13, :cond_fe

    #@b0
    .line 1720
    :cond_b0
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/net/InetAddress;

    #@b3
    move-result-object v7

    #@b4
    if-eqz v7, :cond_ed

    #@b6
    .line 1721
    const-string v7, "GpsLocationProvider"

    #@b8
    new-instance v8, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v9, "agpsConnInfo.mIpAddr "

    #@bf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v8

    #@c3
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/net/InetAddress;

    #@c6
    move-result-object v9

    #@c7
    invoke-virtual {v9}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    #@ca
    move-result-object v9

    #@cb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v8

    #@cf
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v8

    #@d3
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 1722
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@d8
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1800(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@db
    move-result v8

    #@dc
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/net/InetAddress;

    #@df
    move-result-object v9

    #@e0
    invoke-virtual {v7, v8, v9}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@e3
    move-result v7

    #@e4
    if-nez v7, :cond_ed

    #@e6
    .line 1723
    const-string v7, "GpsLocationProvider"

    #@e8
    const-string v8, "call requestRouteToHostAddress failed"

    #@ea
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 1726
    :cond_ed
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@f0
    move-result v7

    #@f1
    invoke-static {v0, v1}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$3800(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;)Ljava/lang/String;

    #@f4
    move-result-object v8

    #@f5
    invoke-static {v0, v1}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$2100(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;)I

    #@f8
    move-result v9

    #@f9
    invoke-direct {p0, v7, v8, v9}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_open(ILjava/lang/String;I)V

    #@fc
    goto/16 :goto_78

    #@fe
    .line 1729
    :cond_fe
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1600(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@101
    move-result v7

    #@102
    if-eq v7, v10, :cond_78

    #@104
    .line 1732
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@107
    move-result v7

    #@108
    invoke-direct {p0, v7}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed(I)V

    #@10b
    goto/16 :goto_78

    #@10d
    .line 1738
    .end local v1           #info:Landroid/net/NetworkInfo;
    :pswitch_10d
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@10f
    if-eqz v7, :cond_118

    #@111
    const-string v7, "GpsLocationProvider"

    #@113
    const-string v8, "type == AGpsConnectionInfo.CONNECTION_TYPE_WIFI"

    #@115
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@118
    .line 1739
    :cond_118
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@11a
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1000(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@11d
    move-result v7

    #@11e
    if-eqz v7, :cond_132

    #@120
    .line 1740
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@122
    if-eqz v7, :cond_12b

    #@124
    const-string v7, "GpsLocationProvider"

    #@126
    const-string v8, "Error: request Wifi but WifiState is not WIFI_STATE_CLOSED"

    #@128
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12b
    .line 1741
    :cond_12b
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@12e
    move-result v7

    #@12f
    invoke-direct {p0, v7}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed(I)V

    #@132
    .line 1743
    :cond_132
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@134
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@137
    move-result-object v7

    #@138
    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    #@13b
    move-result v7

    #@13c
    if-eqz v7, :cond_1e9

    #@13e
    .line 1744
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@140
    if-eqz v7, :cond_149

    #@142
    const-string v7, "GpsLocationProvider"

    #@144
    const-string v8, "wifi enabled"

    #@146
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@149
    .line 1745
    :cond_149
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@14b
    invoke-virtual {v7, v10}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@14e
    move-result-object v1

    #@14f
    .line 1746
    .restart local v1       #info:Landroid/net/NetworkInfo;
    invoke-virtual {p0, v1, v4}, Lcom/android/server/location/GpsLocationProvider;->isWifiConnectedToSSID(Landroid/net/NetworkInfo;Ljava/lang/String;)Z

    #@152
    move-result v7

    #@153
    if-eqz v7, :cond_1d1

    #@155
    .line 1747
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@157
    if-eqz v7, :cond_160

    #@159
    const-string v7, "GpsLocationProvider"

    #@15b
    const-string v8, "already connected to this SSID. not associating to it..."

    #@15d
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    .line 1748
    :cond_160
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@162
    iget-object v8, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@164
    invoke-virtual {v8}, Landroid/net/ConnectivityManager;->getNetworkPreference()I

    #@167
    move-result v8

    #@168
    invoke-static {v7, v8}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3502(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@16b
    .line 1749
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@16d
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3500(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@170
    move-result v7

    #@171
    if-ne v7, v10, :cond_1c0

    #@173
    .line 1750
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@175
    if-eqz v7, :cond_17e

    #@177
    const-string v7, "GpsLocationProvider"

    #@179
    const-string v8, "network Preference already TYPE_mWifiState.mWifiManager. do nothing"

    #@17b
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17e
    .line 1755
    :cond_17e
    :goto_17e
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@180
    if-eqz v7, :cond_189

    #@182
    const-string v7, "GpsLocationProvider"

    #@184
    const-string v8, "wifi connected, and ssid matches expected!"

    #@186
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@189
    .line 1756
    :cond_189
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@18b
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->getCurrentNetId()I

    #@18e
    move-result v8

    #@18f
    invoke-static {v7, v8}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3402(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@192
    .line 1757
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@194
    iget-object v8, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@196
    invoke-static {v8}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3400(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@199
    move-result v8

    #@19a
    invoke-static {v7, v8}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3602(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@19d
    .line 1758
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@19f
    iget-object v8, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@1a1
    invoke-static {v8}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@1a4
    move-result-object v8

    #@1a5
    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@1a8
    move-result-object v8

    #@1a9
    invoke-virtual {v8}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@1ac
    move-result-object v8

    #@1ad
    invoke-static {v7, v8}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1202(Lcom/android/server/location/GpsLocationProvider$WifiState;Ljava/lang/String;)Ljava/lang/String;

    #@1b0
    .line 1760
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@1b2
    invoke-static {v7, v12}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1002(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@1b5
    .line 1761
    invoke-static {v0, v12}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I

    #@1b8
    .line 1763
    const/4 v7, 0x4

    #@1b9
    const-string v8, "dummy-apn"

    #@1bb
    invoke-direct {p0, v7, v8, v11}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_open(ILjava/lang/String;I)V

    #@1be
    goto/16 :goto_78

    #@1c0
    .line 1752
    :cond_1c0
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@1c2
    if-eqz v7, :cond_1cb

    #@1c4
    const-string v7, "GpsLocationProvider"

    #@1c6
    const-string v8, "network Preference not already TYPE_mWifiState.mWifiManager. change it."

    #@1c8
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cb
    .line 1753
    :cond_1cb
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@1cd
    invoke-virtual {v7, v10}, Landroid/net/ConnectivityManager;->setNetworkPreference(I)V

    #@1d0
    goto :goto_17e

    #@1d1
    .line 1765
    :cond_1d1
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@1d3
    if-eqz v7, :cond_1dc

    #@1d5
    const-string v7, "GpsLocationProvider"

    #@1d7
    const-string v8, "not already connected to this SSID. associating to it..."

    #@1d9
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1dc
    .line 1766
    :cond_1dc
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@1de
    invoke-static {v7, v10}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1002(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@1e1
    .line 1767
    invoke-static {v0, v10}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I

    #@1e4
    .line 1768
    invoke-direct {p0, v4, v3}, Lcom/android/server/location/GpsLocationProvider;->associateToNetwork(Ljava/lang/String;Ljava/lang/String;)V

    #@1e7
    goto/16 :goto_78

    #@1e9
    .line 1771
    .end local v1           #info:Landroid/net/NetworkInfo;
    :cond_1e9
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@1eb
    if-eqz v7, :cond_1f4

    #@1ed
    const-string v7, "GpsLocationProvider"

    #@1ef
    const-string v8, "ERROR: wifi not enabled.. (we assume it is enabled)"

    #@1f1
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f4
    .line 1772
    :cond_1f4
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@1f7
    move-result v7

    #@1f8
    invoke-direct {p0, v7}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed(I)V

    #@1fb
    goto/16 :goto_78

    #@1fd
    .line 1783
    :pswitch_1fd
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@1ff
    if-eqz v7, :cond_208

    #@201
    const-string v7, "GpsLocationProvider"

    #@203
    const-string v8, "GPS_RELEASE_AGPS_DATA_CONN"

    #@205
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@208
    .line 1785
    :cond_208
    packed-switch v6, :pswitch_data_28c

    #@20b
    .line 1805
    :pswitch_20b
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@20d
    if-eqz v7, :cond_78

    #@20f
    const-string v7, "GpsLocationProvider"

    #@211
    const-string v8, "GPS_RELEASE_AGPS_DATA_CONN but current network state is unknown!"

    #@213
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@216
    goto/16 :goto_78

    #@218
    .line 1789
    :pswitch_218
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1600(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@21b
    move-result v7

    #@21c
    if-eqz v7, :cond_78

    #@21e
    .line 1790
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1600(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@221
    move-result v7

    #@222
    if-eq v7, v13, :cond_22d

    #@224
    .line 1791
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@226
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$3900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/lang/String;

    #@229
    move-result-object v8

    #@22a
    invoke-virtual {v7, v11, v8}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    #@22d
    .line 1794
    :cond_22d
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@230
    move-result v7

    #@231
    invoke-direct {p0, v7}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_closed(I)V

    #@234
    .line 1795
    invoke-static {v0, v11}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I

    #@237
    goto/16 :goto_78

    #@239
    .line 1800
    :pswitch_239
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@23b
    if-eqz v7, :cond_244

    #@23d
    const-string v7, "GpsLocationProvider"

    #@23f
    const-string v8, "case AGpsConnectionInfo.CONNECTION_TYPE_WIFI"

    #@241
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@244
    .line 1801
    :cond_244
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@246
    invoke-static {v7, v11}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$4000(Lcom/android/server/location/GpsLocationProvider$WifiState;Z)V

    #@249
    goto/16 :goto_78

    #@24b
    .line 1811
    :pswitch_24b
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@24d
    if-eqz v7, :cond_78

    #@24f
    const-string v7, "GpsLocationProvider"

    #@251
    const-string v8, "GPS_AGPS_DATA_CONNECTED"

    #@253
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@256
    goto/16 :goto_78

    #@258
    .line 1814
    :pswitch_258
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@25a
    if-eqz v7, :cond_78

    #@25c
    const-string v7, "GpsLocationProvider"

    #@25e
    const-string v8, "GPS_AGPS_DATA_CONN_DONE"

    #@260
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@263
    goto/16 :goto_78

    #@265
    .line 1817
    :pswitch_265
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@267
    if-eqz v7, :cond_78

    #@269
    const-string v7, "GpsLocationProvider"

    #@26b
    const-string v8, "GPS_AGPS_DATA_CONN_FAILED"

    #@26d
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@270
    goto/16 :goto_78

    #@272
    .line 1709
    :pswitch_data_272
    .packed-switch 0x1
        :pswitch_7d
        :pswitch_1fd
        :pswitch_24b
        :pswitch_258
        :pswitch_265
    .end packed-switch

    #@280
    .line 1713
    :pswitch_data_280
    .packed-switch 0x1
        :pswitch_97
        :pswitch_8b
        :pswitch_97
        :pswitch_10d
    .end packed-switch

    #@28c
    .line 1785
    :pswitch_data_28c
    .packed-switch 0x1
        :pswitch_218
        :pswitch_20b
        :pswitch_218
        :pswitch_239
    .end packed-switch
.end method

.method private handleSetRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 11
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1015
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@3
    if-eqz v0, :cond_1d

    #@5
    const-string v0, "GpsLocationProvider"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "setRequest "

    #@e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1019
    :cond_1d
    iget-boolean v0, p1, Lcom/android/internal/location/ProviderRequest;->reportLocation:Z

    #@1f
    if-eqz v0, :cond_8f

    #@21
    .line 1021
    invoke-virtual {p2}, Landroid/os/WorkSource;->size()I

    #@24
    move-result v0

    #@25
    new-array v7, v0, [I

    #@27
    .line 1022
    .local v7, uids:[I
    const/4 v6, 0x0

    #@28
    .local v6, i:I
    :goto_28
    invoke-virtual {p2}, Landroid/os/WorkSource;->size()I

    #@2b
    move-result v0

    #@2c
    if-ge v6, v0, :cond_37

    #@2e
    .line 1023
    invoke-virtual {p2, v6}, Landroid/os/WorkSource;->get(I)I

    #@31
    move-result v0

    #@32
    aput v0, v7, v6

    #@34
    .line 1022
    add-int/lit8 v6, v6, 0x1

    #@36
    goto :goto_28

    #@37
    .line 1025
    :cond_37
    invoke-direct {p0, v7}, Lcom/android/server/location/GpsLocationProvider;->updateClientUids([I)V

    #@3a
    .line 1027
    iget-wide v0, p1, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@3c
    long-to-int v0, v0

    #@3d
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@3f
    .line 1030
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@41
    int-to-long v0, v0

    #@42
    iget-wide v3, p1, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@44
    cmp-long v0, v0, v3

    #@46
    if-eqz v0, :cond_67

    #@48
    .line 1031
    const-string v0, "GpsLocationProvider"

    #@4a
    new-instance v1, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v3, "interval overflow: "

    #@51
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    iget-wide v3, p1, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@57
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 1032
    const v0, 0x7fffffff

    #@65
    iput v0, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@67
    .line 1036
    :cond_67
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@69
    if-eqz v0, :cond_87

    #@6b
    const/4 v0, 0x1

    #@6c
    invoke-direct {p0, v0}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@6f
    move-result v0

    #@70
    if-eqz v0, :cond_87

    #@72
    .line 1038
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider;->mPositionMode:I

    #@74
    iget v3, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@76
    move-object v0, p0

    #@77
    move v4, v2

    #@78
    move v5, v2

    #@79
    invoke-direct/range {v0 .. v5}, Lcom/android/server/location/GpsLocationProvider;->native_set_position_mode(IIIII)Z

    #@7c
    move-result v0

    #@7d
    if-nez v0, :cond_86

    #@7f
    .line 1040
    const-string v0, "GpsLocationProvider"

    #@81
    const-string v1, "set_position_mode failed in setMinTime()"

    #@83
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 1053
    .end local v6           #i:I
    .end local v7           #uids:[I
    :cond_86
    :goto_86
    return-void

    #@87
    .line 1042
    .restart local v6       #i:I
    .restart local v7       #uids:[I
    :cond_87
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@89
    if-nez v0, :cond_86

    #@8b
    .line 1044
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->startNavigating()V

    #@8e
    goto :goto_86

    #@8f
    .line 1047
    .end local v6           #i:I
    .end local v7           #uids:[I
    :cond_8f
    new-array v0, v2, [I

    #@91
    invoke-direct {p0, v0}, Lcom/android/server/location/GpsLocationProvider;->updateClientUids([I)V

    #@94
    .line 1049
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->stopNavigating()V

    #@97
    .line 1050
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@99
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeupIntent:Landroid/app/PendingIntent;

    #@9b
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@9e
    .line 1051
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@a0
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeoutIntent:Landroid/app/PendingIntent;

    #@a2
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@a5
    goto :goto_86
.end method

.method private handleUpdateLocation(Landroid/location/Location;)V
    .registers 8
    .parameter "location"

    #@0
    .prologue
    .line 883
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_16

    #@6
    .line 884
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    #@9
    move-result-wide v1

    #@a
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    #@d
    move-result-wide v3

    #@e
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@11
    move-result v5

    #@12
    move-object v0, p0

    #@13
    invoke-direct/range {v0 .. v5}, Lcom/android/server/location/GpsLocationProvider;->native_inject_location(DDF)V

    #@16
    .line 887
    :cond_16
    return-void
.end method

.method private handleUpdateNetworkState(ILandroid/net/NetworkInfo;)V
    .registers 16
    .parameter "state"
    .parameter "info"

    #@0
    .prologue
    .line 666
    const/4 v0, 0x2

    #@1
    if-ne p1, v0, :cond_f6

    #@3
    const/4 v0, 0x1

    #@4
    :goto_4
    iput-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNetworkAvailable:Z

    #@6
    .line 668
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@8
    if-eqz v0, :cond_32

    #@a
    .line 669
    const-string v1, "GpsLocationProvider"

    #@c
    new-instance v0, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "updateNetworkState "

    #@13
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNetworkAvailable:Z

    #@19
    if-eqz v0, :cond_f9

    #@1b
    const-string v0, "available"

    #@1d
    :goto_1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v2, " info: "

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 673
    :cond_32
    if-eqz p2, :cond_dd

    #@34
    .line 674
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@36
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@39
    move-result-object v0

    #@3a
    const-string v1, "mobile_data"

    #@3c
    const/4 v2, 0x1

    #@3d
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@40
    move-result v0

    #@41
    const/4 v1, 0x1

    #@42
    if-ne v0, v1, :cond_fd

    #@44
    const/4 v9, 0x1

    #@45
    .line 676
    .local v9, dataEnabled:Z
    :goto_45
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@48
    move-result v0

    #@49
    if-eqz v0, :cond_100

    #@4b
    if-eqz v9, :cond_100

    #@4d
    const/4 v4, 0x1

    #@4e
    .line 677
    .local v4, networkAvailable:Z
    :goto_4e
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->getDefaultApn()Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    .line 679
    .local v6, defaultApn:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->isConnected()Z

    #@55
    move-result v1

    #@56
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getType()I

    #@59
    move-result v2

    #@5a
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->isRoaming()Z

    #@5d
    move-result v3

    #@5e
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@61
    move-result-object v5

    #@62
    move-object v0, p0

    #@63
    invoke-direct/range {v0 .. v6}, Lcom/android/server/location/GpsLocationProvider;->native_update_network_state(ZIZZLjava/lang/String;Ljava/lang/String;)V

    #@66
    .line 684
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getType()I

    #@69
    move-result v0

    #@6a
    packed-switch v0, :pswitch_data_1e6

    #@6d
    .line 694
    :pswitch_6d
    const/4 v8, 0x3

    #@6e
    .line 698
    .local v8, connType:I
    :goto_6e
    invoke-direct {p0, v8}, Lcom/android/server/location/GpsLocationProvider;->getAGpsConnectionInfo(I)Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;

    #@71
    move-result-object v7

    #@72
    .line 700
    .local v7, agpsConnInfo:Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;
    if-eqz v7, :cond_109

    #@74
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1600(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@77
    move-result v0

    #@78
    const/4 v1, 0x1

    #@79
    if-ne v0, v1, :cond_109

    #@7b
    const/4 v0, 0x4

    #@7c
    if-eq v8, v0, :cond_109

    #@7e
    .line 703
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNetworkAvailable:Z

    #@80
    if-eqz v0, :cond_dd

    #@82
    .line 704
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/net/InetAddress;

    #@85
    move-result-object v0

    #@86
    if-eqz v0, :cond_bf

    #@88
    .line 705
    const-string v0, "GpsLocationProvider"

    #@8a
    new-instance v1, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v2, "agpsConnInfo.mIpAddr "

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/net/InetAddress;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v2}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    #@9c
    move-result-object v2

    #@9d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v1

    #@a1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v1

    #@a5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 706
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@aa
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1800(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@ad
    move-result v1

    #@ae
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1700(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)Ljava/net/InetAddress;

    #@b1
    move-result-object v2

    #@b2
    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@b5
    move-result v0

    #@b6
    if-nez v0, :cond_bf

    #@b8
    .line 707
    const-string v0, "GpsLocationProvider"

    #@ba
    const-string v1, "call requestRouteToHostAddress failed"

    #@bc
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 710
    :cond_bf
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@c1
    if-eqz v0, :cond_ca

    #@c3
    const-string v0, "GpsLocationProvider"

    #@c5
    const-string v1, "call native_agps_data_conn_open"

    #@c7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 711
    :cond_ca
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1900(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@cd
    move-result v0

    #@ce
    invoke-static {v7, p2, v6}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$2000(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;Ljava/lang/String;)Ljava/lang/String;

    #@d1
    move-result-object v1

    #@d2
    invoke-static {v7, p2}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$2100(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Landroid/net/NetworkInfo;)I

    #@d5
    move-result v2

    #@d6
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_open(ILjava/lang/String;I)V

    #@d9
    .line 714
    const/4 v0, 0x2

    #@da
    invoke-static {v7, v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I

    #@dd
    .line 754
    .end local v4           #networkAvailable:Z
    .end local v6           #defaultApn:Ljava/lang/String;
    .end local v7           #agpsConnInfo:Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;
    .end local v8           #connType:I
    .end local v9           #dataEnabled:Z
    :cond_dd
    :goto_dd
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNetworkAvailable:Z

    #@df
    if-eqz v0, :cond_f5

    #@e1
    .line 755
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mInjectNtpTimePending:I

    #@e3
    if-nez v0, :cond_eb

    #@e5
    .line 756
    const/4 v0, 0x5

    #@e6
    const/4 v1, 0x0

    #@e7
    const/4 v2, 0x0

    #@e8
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@eb
    .line 758
    :cond_eb
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mDownloadXtraDataPending:I

    #@ed
    if-nez v0, :cond_f5

    #@ef
    .line 759
    const/4 v0, 0x6

    #@f0
    const/4 v1, 0x0

    #@f1
    const/4 v2, 0x0

    #@f2
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@f5
    .line 762
    :cond_f5
    :goto_f5
    return-void

    #@f6
    .line 666
    :cond_f6
    const/4 v0, 0x0

    #@f7
    goto/16 :goto_4

    #@f9
    .line 669
    :cond_f9
    const-string v0, "unavailable"

    #@fb
    goto/16 :goto_1d

    #@fd
    .line 674
    :cond_fd
    const/4 v9, 0x0

    #@fe
    goto/16 :goto_45

    #@100
    .line 676
    .restart local v9       #dataEnabled:Z
    :cond_100
    const/4 v4, 0x0

    #@101
    goto/16 :goto_4e

    #@103
    .line 686
    .restart local v4       #networkAvailable:Z
    .restart local v6       #defaultApn:Ljava/lang/String;
    :pswitch_103
    const/4 v8, 0x1

    #@104
    .line 687
    .restart local v8       #connType:I
    goto/16 :goto_6e

    #@106
    .line 690
    .end local v8           #connType:I
    :pswitch_106
    const/4 v8, 0x4

    #@107
    .line 691
    .restart local v8       #connType:I
    goto/16 :goto_6e

    #@109
    .line 716
    .restart local v7       #agpsConnInfo:Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;
    :cond_109
    if-eqz v7, :cond_1ca

    #@10b
    const/4 v0, 0x4

    #@10c
    if-ne v8, v0, :cond_1ca

    #@10e
    .line 718
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@110
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1000(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@113
    move-result v0

    #@114
    const/4 v1, 0x1

    #@115
    if-eq v0, v1, :cond_120

    #@117
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@119
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1000(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@11c
    move-result v0

    #@11d
    const/4 v1, 0x3

    #@11e
    if-ne v0, v1, :cond_1bd

    #@120
    .line 720
    :cond_120
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@123
    move-result-object v11

    #@124
    .line 721
    .local v11, networkState:Landroid/net/NetworkInfo$State;
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@127
    move-result-object v10

    #@128
    .line 722
    .local v10, detailedState:Landroid/net/NetworkInfo$DetailedState;
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@12a
    if-eqz v0, :cond_133

    #@12c
    const-string v0, "GpsLocationProvider"

    #@12e
    const-string v1, "handleUpdateNetworkState for TYPE_WIFI"

    #@130
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 723
    :cond_133
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@135
    if-eqz v0, :cond_159

    #@137
    const-string v0, "GpsLocationProvider"

    #@139
    new-instance v1, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v2, "handleUpdateNetworkState detailedstate = "

    #@140
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v1

    #@144
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v1

    #@148
    const-string v2, ", and state = "

    #@14a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v1

    #@14e
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v1

    #@152
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v1

    #@156
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@159
    .line 724
    :cond_159
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@15c
    move-result v0

    #@15d
    if-nez v0, :cond_170

    #@15f
    .line 725
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@161
    if-eqz v0, :cond_16a

    #@163
    const-string v0, "GpsLocationProvider"

    #@165
    const-string v1, "ERROR: handleUpdateNetworkState connect to wifi failed!!"

    #@167
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16a
    .line 726
    :cond_16a
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@16c
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1500(Lcom/android/server/location/GpsLocationProvider$WifiState;)V

    #@16f
    goto :goto_f5

    #@170
    .line 729
    :cond_170
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@172
    if-eq v10, v0, :cond_181

    #@174
    .line 732
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@176
    if-eqz v0, :cond_f5

    #@178
    const-string v0, "GpsLocationProvider"

    #@17a
    const-string v1, "handleUpdateNetworkState neither connected nor disconnected... return until it is ready"

    #@17c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17f
    goto/16 :goto_f5

    #@181
    .line 735
    :cond_181
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@183
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1000(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@186
    move-result v0

    #@187
    const/4 v1, 0x1

    #@188
    if-ne v0, v1, :cond_1a8

    #@18a
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@18c
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1100(Lcom/android/server/location/GpsLocationProvider$WifiState;)Ljava/lang/String;

    #@18f
    move-result-object v12

    #@190
    .line 736
    .local v12, ssid:Ljava/lang/String;
    :goto_190
    invoke-virtual {p0, p2, v12}, Lcom/android/server/location/GpsLocationProvider;->isWifiConnectedToSSID(Landroid/net/NetworkInfo;Ljava/lang/String;)Z

    #@193
    move-result v0

    #@194
    if-eqz v0, :cond_1af

    #@196
    .line 737
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@198
    if-eqz v0, :cond_1a1

    #@19a
    const-string v0, "GpsLocationProvider"

    #@19c
    const-string v1, "handleUpdateNetworkState succeeded! wifi connected, and ssid matches expected!"

    #@19e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a1
    .line 738
    :cond_1a1
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@1a3
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$2200(Lcom/android/server/location/GpsLocationProvider$WifiState;)V

    #@1a6
    goto/16 :goto_dd

    #@1a8
    .line 735
    .end local v12           #ssid:Ljava/lang/String;
    :cond_1a8
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@1aa
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1200(Lcom/android/server/location/GpsLocationProvider$WifiState;)Ljava/lang/String;

    #@1ad
    move-result-object v12

    #@1ae
    goto :goto_190

    #@1af
    .line 740
    .restart local v12       #ssid:Ljava/lang/String;
    :cond_1af
    const-string v0, "GpsLocationProvider"

    #@1b1
    const-string v1, "isWifiConnectedToSSID returned false!"

    #@1b3
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b6
    .line 741
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@1b8
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1500(Lcom/android/server/location/GpsLocationProvider$WifiState;)V

    #@1bb
    goto/16 :goto_dd

    #@1bd
    .line 744
    .end local v10           #detailedState:Landroid/net/NetworkInfo$DetailedState;
    .end local v11           #networkState:Landroid/net/NetworkInfo$State;
    .end local v12           #ssid:Ljava/lang/String;
    :cond_1bd
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@1bf
    if-eqz v0, :cond_dd

    #@1c1
    const-string v0, "GpsLocationProvider"

    #@1c3
    const-string v1, "ignore wifi update if we are not in OPENING or CLOSING"

    #@1c5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c8
    goto/16 :goto_dd

    #@1ca
    .line 747
    :cond_1ca
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@1cc
    if-eqz v0, :cond_1d5

    #@1ce
    const-string v0, "GpsLocationProvider"

    #@1d0
    const-string v1, "call native_agps_data_conn_failed"

    #@1d2
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d5
    .line 748
    :cond_1d5
    const/4 v0, 0x0

    #@1d6
    invoke-static {v7, v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$2302(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;Ljava/lang/String;)Ljava/lang/String;

    #@1d9
    .line 749
    const/4 v0, 0x0

    #@1da
    invoke-static {v7, v0}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$1602(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;I)I

    #@1dd
    .line 750
    invoke-static {v7}, Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;->access$2400(Lcom/android/server/location/GpsLocationProvider$AGpsConnectionInfo;)I

    #@1e0
    move-result v0

    #@1e1
    invoke-direct {p0, v0}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed(I)V

    #@1e4
    goto/16 :goto_dd

    #@1e6
    .line 684
    :pswitch_data_1e6
    .packed-switch 0x1
        :pswitch_106
        :pswitch_6d
        :pswitch_103
    .end packed-switch
.end method

.method private hasCapability(I)Z
    .registers 3
    .parameter "capability"

    #@0
    .prologue
    .line 1249
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineCapabilities:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-eqz v0, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method private hibernate()V
    .registers 8

    #@0
    .prologue
    .line 1241
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->stopNavigating()V

    #@3
    .line 1242
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@5
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeoutIntent:Landroid/app/PendingIntent;

    #@7
    invoke-virtual {v2, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@a
    .line 1243
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@c
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeupIntent:Landroid/app/PendingIntent;

    #@e
    invoke-virtual {v2, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@11
    .line 1244
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@14
    move-result-wide v0

    #@15
    .line 1245
    .local v0, now:J
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@17
    const/4 v3, 0x2

    #@18
    iget v4, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@1a
    int-to-long v4, v4

    #@1b
    add-long/2addr v4, v0

    #@1c
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeupIntent:Landroid/app/PendingIntent;

    #@1e
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@21
    .line 1246
    return-void
.end method

.method public static isSupported()Z
    .registers 1

    #@0
    .prologue
    .line 474
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->native_is_supported()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private listenForBroadcasts()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 620
    new-instance v1, Landroid/content/IntentFilter;

    #@3
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@6
    .line 621
    .local v1, intentFilter:Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.DATA_SMS_RECEIVED"

    #@8
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b
    .line 622
    const-string v3, "sms"

    #@d
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@10
    .line 623
    const-string v3, "localhost"

    #@12
    const-string v4, "7275"

    #@14
    invoke-virtual {v1, v3, v4}, Landroid/content/IntentFilter;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 624
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@19
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mBroadcastReciever:Landroid/content/BroadcastReceiver;

    #@1b
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@1d
    invoke-virtual {v3, v4, v1, v6, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@20
    .line 626
    new-instance v1, Landroid/content/IntentFilter;

    #@22
    .end local v1           #intentFilter:Landroid/content/IntentFilter;
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@25
    .line 627
    .restart local v1       #intentFilter:Landroid/content/IntentFilter;
    const-string v3, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    #@27
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2a
    .line 629
    :try_start_2a
    const-string v3, "application/vnd.omaloc-supl-init"

    #@2c
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_2f
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_2a .. :try_end_2f} :catch_67

    #@2f
    .line 633
    :goto_2f
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@31
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mBroadcastReciever:Landroid/content/BroadcastReceiver;

    #@33
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@35
    invoke-virtual {v3, v4, v1, v6, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@38
    .line 635
    new-instance v1, Landroid/content/IntentFilter;

    #@3a
    .end local v1           #intentFilter:Landroid/content/IntentFilter;
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@3d
    .line 636
    .restart local v1       #intentFilter:Landroid/content/IntentFilter;
    const-string v3, "com.android.internal.location.ALARM_WAKEUP"

    #@3f
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@42
    .line 637
    const-string v3, "com.android.internal.location.ALARM_TIMEOUT"

    #@44
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@47
    .line 640
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@49
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4c
    .line 641
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@4e
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mBroadcastReciever:Landroid/content/BroadcastReceiver;

    #@50
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@52
    invoke-virtual {v3, v4, v1, v6, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@55
    .line 643
    new-instance v2, Landroid/content/IntentFilter;

    #@57
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@5a
    .line 644
    .local v2, intentFilter1:Landroid/content/IntentFilter;
    const-string v3, "android.net.wifi.SCAN_RESULTS"

    #@5c
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@5f
    .line 645
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@61
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiScanReceiver:Landroid/content/BroadcastReceiver;

    #@63
    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@66
    .line 646
    return-void

    #@67
    .line 630
    .end local v2           #intentFilter1:Landroid/content/IntentFilter;
    :catch_67
    move-exception v0

    #@68
    .line 631
    .local v0, e:Landroid/content/IntentFilter$MalformedMimeTypeException;
    const-string v3, "GpsLocationProvider"

    #@6a
    const-string v4, "Malformed SUPL init mime type"

    #@6c
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    goto :goto_2f
.end method

.method private native_agps_data_conn_closed(I)V
	.registers 2
	
	invoke-virtual {p0}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_closed()V
.end method

.method private native native_agps_data_conn_closed()V
.end method

.method private native_agps_data_conn_failed(I)V
	.registers 2
	
	invoke-virtual {p0}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed()V
.end method

.method private native native_agps_data_conn_failed()V
.end method

.method private native native_agps_data_conn_open(Ljava/lang/String;)V
.end method

.method private native_agps_data_conn_open(ILjava/lang/String;I)V
	.registers 4
	
	invoke-virtual {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_open(Ljava/lang/String;)V
.end method

.method private native native_agps_ni_message([BI)V
.end method

.method private native native_agps_set_id(ILjava/lang/String;)V
.end method

.method private native native_agps_set_ref_location_cellid(IIIII)V
.end method

.method private native native_cleanup()V
.end method

.method private native native_delete_aiding_data(I)V
.end method

.method private native native_get_internal_state()Ljava/lang/String;
.end method

.method private native native_inject_location(DDF)V
.end method

.method private native native_inject_time(JJI)V
.end method

.method private native native_inject_xtra_data([BI)V
.end method

.method private static native native_is_supported()Z
.end method

.method private native native_read_nmea([BI)I
.end method

.method private native native_read_sv_status([I[F[F[F[I)I
.end method

.method private native native_send_ni_response(II)V
.end method

.method private native native_set_position_mode(IIIII)Z
.end method

.method private native native_start()Z
.end method

.method private native native_stop()Z
.end method

.method private native native_supports_xtra()Z
.end method

.method private native native_update_network_state(ZIZZLjava/lang/String;Ljava/lang/String;)V
.end method

.method private reportAGpsStatus(III)V
    .registers 10
    .parameter "type"
    .parameter "status"
    .parameter "ipaddr"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1215
    packed-switch p2, :pswitch_data_e6

    .line 1271
    :cond_5
    :goto_5
    return-void

    .line 1217
    :pswitch_6
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_11

    const-string v2, "GpsLocationProvider"

    const-string v3, "GPS_REQUEST_AGPS_DATA_CONN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1220
    :cond_11
    iput v5, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionState:I

    .line 1221
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    const-string v3, "enableSUPL"

    invoke-virtual {v2, v4, v3}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v0

    .line 1223
    .local v0, result:I
    iput p3, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionIpAddr:I

    .line 1224
    if-nez v0, :cond_80

    .line 1225
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_2a

    const-string v2, "GpsLocationProvider"

    const-string v3, "PhoneConstants.APN_ALREADY_ACTIVE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    :cond_2a
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsApn:Ljava/lang/String;

    if-eqz v2, :cond_73

    .line 1227
    const-string v2, "GpsLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mAGpsDataConnectionIpAddr "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionIpAddr:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1228
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionIpAddr:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_6a

    .line 1230
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_58

    const-string v2, "GpsLocationProvider"

    const-string v3, "call requestRouteToHost"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1231
    :cond_58
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v3, 0x3

    iget v4, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionIpAddr:I

    invoke-virtual {v2, v3, v4}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v1

    .line 1234
    .local v1, route_result:Z
    if-nez v1, :cond_6a

    const-string v2, "GpsLocationProvider"

    const-string v3, "call requestRouteToHost failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1236
    .end local v1           #route_result:Z
    :cond_6a
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsApn:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_open(Ljava/lang/String;)V

    .line 1237
    const/4 v2, 0x2

    iput v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionState:I

    goto :goto_5

    .line 1239
    :cond_73
    const-string v2, "GpsLocationProvider"

    const-string v3, "mAGpsApn not set when receiving PhoneConstants.APN_ALREADY_ACTIVE"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    iput v4, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionState:I

    .line 1241
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed()V

    goto :goto_5

    .line 1243
    :cond_80
    if-ne v0, v5, :cond_8f

    .line 1244
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_5

    const-string v2, "GpsLocationProvider"

    const-string v3, "PhoneConstants.APN_REQUEST_STARTED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 1247
    :cond_8f
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_9a

    const-string v2, "GpsLocationProvider"

    const-string v3, "startUsingNetworkFeature failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    :cond_9a
    iput v4, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionState:I

    .line 1249
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_failed()V

    goto/16 :goto_5

    .line 1253
    .end local v0           #result:I
    :pswitch_a1
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_ac

    const-string v2, "GpsLocationProvider"

    const-string v3, "GPS_RELEASE_AGPS_DATA_CONN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1254
    :cond_ac
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionState:I

    if-eqz v2, :cond_5

    .line 1255
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    const-string v3, "enableSUPL"

    invoke-virtual {v2, v4, v3}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    .line 1257
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_agps_data_conn_closed()V

    .line 1258
    iput v4, p0, Lcom/android/server/location/GpsLocationProvider;->mAGpsDataConnectionState:I

    goto/16 :goto_5

    .line 1262
    :pswitch_be
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_5

    const-string v2, "GpsLocationProvider"

    const-string v3, "GPS_AGPS_DATA_CONNECTED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 1265
    :pswitch_cb
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_5

    const-string v2, "GpsLocationProvider"

    const-string v3, "GPS_AGPS_DATA_CONN_DONE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 1268
    :pswitch_d8
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    if-eqz v2, :cond_5

    const-string v2, "GpsLocationProvider"

    const-string v3, "GPS_AGPS_DATA_CONN_FAILED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 1215
    nop

    :pswitch_data_e6
    .packed-switch 0x1
        :pswitch_6
        :pswitch_a1
        :pswitch_be
        :pswitch_cb
        :pswitch_d8
    .end packed-switch
.end method

.method private reportAGpsStatus(II[BLjava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "type"
    .parameter "status"
    .parameter "ipAddr"
    .parameter "ssid"
    .parameter "password"

    #@0
    .prologue
    .line 1668
    sget-boolean v1, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@2
    if-eqz v1, :cond_44

    #@4
    const-string v1, "GpsLocationProvider"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "reportAGpsStatus with type = "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "status = "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "ipAddr = "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, "ssid = "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, "password = "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1673
    :cond_44
    new-instance v0, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;

    #@46
    move-object v1, p0

    #@47
    move v2, p1

    #@48
    move v3, p2

    #@49
    move-object v4, p3

    #@4a
    move-object v5, p4

    #@4b
    move-object v6, p5

    #@4c
    invoke-direct/range {v0 .. v6}, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;-><init>(Lcom/android/server/location/GpsLocationProvider;II[BLjava/lang/String;Ljava/lang/String;)V

    #@4f
    .line 1679
    .local v0, rasm:Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;
    new-instance v7, Landroid/os/Message;

    #@51
    invoke-direct {v7}, Landroid/os/Message;-><init>()V

    #@54
    .line 1680
    .local v7, msg:Landroid/os/Message;
    const/16 v1, 0xc

    #@56
    iput v1, v7, Landroid/os/Message;->what:I

    #@58
    .line 1681
    iput-object v0, v7, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5a
    .line 1683
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@5c
    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@5f
    .line 1684
    return-void
.end method

.method private reportLocation(IDDDFFFJ)V
    .registers 24
    .parameter "flags"
    .parameter "latitude"
    .parameter "longitude"
    .parameter "altitude"
    .parameter "speed"
    .parameter "bearing"
    .parameter "accuracy"
    .parameter "timestamp"

    #@0
    .prologue
    .line 1257
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->VERBOSE:Z

    #@2
    if-eqz v7, :cond_33

    #@4
    const-string v7, "GpsLocationProvider"

    #@6
    new-instance v8, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v9, "reportLocation lat: "

    #@d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@14
    move-result-object v8

    #@15
    const-string v9, " long: "

    #@17
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    move-wide v0, p4

    #@1c
    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v8

    #@20
    const-string v9, " timestamp: "

    #@22
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v8

    #@26
    move-wide/from16 v0, p11

    #@28
    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v8

    #@30
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 1260
    :cond_33
    iget-object v8, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@35
    monitor-enter v8

    #@36
    .line 1261
    :try_start_36
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider;->mLocationFlags:I

    #@38
    .line 1262
    and-int/lit8 v7, p1, 0x1

    #@3a
    const/4 v9, 0x1

    #@3b
    if-ne v7, v9, :cond_58

    #@3d
    .line 1263
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@3f
    invoke-virtual {v7, p2, p3}, Landroid/location/Location;->setLatitude(D)V

    #@42
    .line 1264
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@44
    move-wide v0, p4

    #@45
    invoke-virtual {v7, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    #@48
    .line 1265
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@4a
    move-wide/from16 v0, p11

    #@4c
    invoke-virtual {v7, v0, v1}, Landroid/location/Location;->setTime(J)V

    #@4f
    .line 1268
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@51
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@54
    move-result-wide v9

    #@55
    invoke-virtual {v7, v9, v10}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    #@58
    .line 1270
    :cond_58
    and-int/lit8 v7, p1, 0x2

    #@5a
    const/4 v9, 0x2

    #@5b
    if-ne v7, v9, :cond_ed

    #@5d
    .line 1271
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@5f
    move-wide/from16 v0, p6

    #@61
    invoke-virtual {v7, v0, v1}, Landroid/location/Location;->setAltitude(D)V

    #@64
    .line 1275
    :goto_64
    and-int/lit8 v7, p1, 0x4

    #@66
    const/4 v9, 0x4

    #@67
    if-ne v7, v9, :cond_f7

    #@69
    .line 1276
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@6b
    move/from16 v0, p8

    #@6d
    invoke-virtual {v7, v0}, Landroid/location/Location;->setSpeed(F)V

    #@70
    .line 1280
    :goto_70
    and-int/lit8 v7, p1, 0x8

    #@72
    const/16 v9, 0x8

    #@74
    if-ne v7, v9, :cond_fe

    #@76
    .line 1281
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@78
    move/from16 v0, p9

    #@7a
    invoke-virtual {v7, v0}, Landroid/location/Location;->setBearing(F)V

    #@7d
    .line 1285
    :goto_7d
    and-int/lit8 v7, p1, 0x10

    #@7f
    const/16 v9, 0x10

    #@81
    if-ne v7, v9, :cond_105

    #@83
    .line 1286
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@85
    move/from16 v0, p10

    #@87
    invoke-virtual {v7, v0}, Landroid/location/Location;->setAccuracy(F)V

    #@8a
    .line 1290
    :goto_8a
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@8c
    iget-object v9, p0, Lcom/android/server/location/GpsLocationProvider;->mLocationExtras:Landroid/os/Bundle;

    #@8e
    invoke-virtual {v7, v9}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V
    :try_end_91
    .catchall {:try_start_36 .. :try_end_91} :catchall_f4

    #@91
    .line 1293
    :try_start_91
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mILocationManager:Landroid/location/ILocationManager;

    #@93
    iget-object v9, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@95
    const/4 v10, 0x0

    #@96
    invoke-interface {v7, v9, v10}, Landroid/location/ILocationManager;->reportLocation(Landroid/location/Location;Z)V
    :try_end_99
    .catchall {:try_start_91 .. :try_end_99} :catchall_f4
    .catch Landroid/os/RemoteException; {:try_start_91 .. :try_end_99} :catch_10b

    #@99
    .line 1297
    :goto_99
    :try_start_99
    monitor-exit v8
    :try_end_9a
    .catchall {:try_start_99 .. :try_end_9a} :catchall_f4

    #@9a
    .line 1299
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9d
    move-result-wide v7

    #@9e
    iput-wide v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLastFixTime:J

    #@a0
    .line 1301
    iget v7, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeToFirstFix:I

    #@a2
    if-nez v7, :cond_125

    #@a4
    and-int/lit8 v7, p1, 0x1

    #@a6
    const/4 v8, 0x1

    #@a7
    if-ne v7, v8, :cond_125

    #@a9
    .line 1302
    iget-wide v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLastFixTime:J

    #@ab
    iget-wide v9, p0, Lcom/android/server/location/GpsLocationProvider;->mFixRequestTime:J

    #@ad
    sub-long/2addr v7, v9

    #@ae
    long-to-int v7, v7

    #@af
    iput v7, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeToFirstFix:I

    #@b1
    .line 1303
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@b3
    if-eqz v7, :cond_cf

    #@b5
    const-string v7, "GpsLocationProvider"

    #@b7
    new-instance v8, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v9, "TTFF: "

    #@be
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v8

    #@c2
    iget v9, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeToFirstFix:I

    #@c4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v8

    #@c8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v8

    #@cc
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 1306
    :cond_cf
    iget-object v8, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@d1
    monitor-enter v8

    #@d2
    .line 1307
    :try_start_d2
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@d4
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@d7
    move-result v6

    #@d8
    .line 1308
    .local v6, size:I
    const/4 v3, 0x0

    #@d9
    .local v3, i:I
    :goto_d9
    if-ge v3, v6, :cond_124

    #@db
    .line 1309
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@dd
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e0
    move-result-object v5

    #@e1
    check-cast v5, Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_end_e3
    .catchall {:try_start_d2 .. :try_end_e3} :catchall_181

    #@e3
    .line 1311
    .local v5, listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_start_e3
    iget-object v7, v5, Lcom/android/server/location/GpsLocationProvider$Listener;->mListener:Landroid/location/IGpsStatusListener;

    #@e5
    iget v9, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeToFirstFix:I

    #@e7
    invoke-interface {v7, v9}, Landroid/location/IGpsStatusListener;->onFirstFix(I)V
    :try_end_ea
    .catchall {:try_start_e3 .. :try_end_ea} :catchall_181
    .catch Landroid/os/RemoteException; {:try_start_e3 .. :try_end_ea} :catch_114

    #@ea
    .line 1308
    :goto_ea
    add-int/lit8 v3, v3, 0x1

    #@ec
    goto :goto_d9

    #@ed
    .line 1273
    .end local v3           #i:I
    .end local v5           #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    .end local v6           #size:I
    :cond_ed
    :try_start_ed
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@ef
    invoke-virtual {v7}, Landroid/location/Location;->removeAltitude()V

    #@f2
    goto/16 :goto_64

    #@f4
    .line 1297
    :catchall_f4
    move-exception v7

    #@f5
    monitor-exit v8
    :try_end_f6
    .catchall {:try_start_ed .. :try_end_f6} :catchall_f4

    #@f6
    throw v7

    #@f7
    .line 1278
    :cond_f7
    :try_start_f7
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@f9
    invoke-virtual {v7}, Landroid/location/Location;->removeSpeed()V

    #@fc
    goto/16 :goto_70

    #@fe
    .line 1283
    :cond_fe
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@100
    invoke-virtual {v7}, Landroid/location/Location;->removeBearing()V

    #@103
    goto/16 :goto_7d

    #@105
    .line 1288
    :cond_105
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mLocation:Landroid/location/Location;

    #@107
    invoke-virtual {v7}, Landroid/location/Location;->removeAccuracy()V

    #@10a
    goto :goto_8a

    #@10b
    .line 1294
    :catch_10b
    move-exception v2

    #@10c
    .line 1295
    .local v2, e:Landroid/os/RemoteException;
    const-string v7, "GpsLocationProvider"

    #@10e
    const-string v9, "RemoteException calling reportLocation"

    #@110
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_113
    .catchall {:try_start_f7 .. :try_end_113} :catchall_f4

    #@113
    goto :goto_99

    #@114
    .line 1312
    .end local v2           #e:Landroid/os/RemoteException;
    .restart local v3       #i:I
    .restart local v5       #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    .restart local v6       #size:I
    :catch_114
    move-exception v2

    #@115
    .line 1313
    .restart local v2       #e:Landroid/os/RemoteException;
    :try_start_115
    const-string v7, "GpsLocationProvider"

    #@117
    const-string v9, "RemoteException in stopNavigating"

    #@119
    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 1314
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@11e
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@121
    .line 1316
    add-int/lit8 v6, v6, -0x1

    #@123
    goto :goto_ea

    #@124
    .line 1319
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v5           #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    :cond_124
    monitor-exit v8
    :try_end_125
    .catchall {:try_start_115 .. :try_end_125} :catchall_181

    #@125
    .line 1324
    .end local v3           #i:I
    .end local v6           #size:I
    :cond_125
    iget-boolean v7, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@127
    if-eqz v7, :cond_161

    #@129
    iget-boolean v7, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@12b
    if-eqz v7, :cond_161

    #@12d
    iget v7, p0, Lcom/android/server/location/GpsLocationProvider;->mStatus:I

    #@12f
    const/4 v8, 0x2

    #@130
    if-eq v7, v8, :cond_161

    #@132
    .line 1327
    const/4 v7, 0x1

    #@133
    invoke-direct {p0, v7}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@136
    move-result v7

    #@137
    if-nez v7, :cond_147

    #@139
    iget v7, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@13b
    const v8, 0xea60

    #@13e
    if-ge v7, v8, :cond_147

    #@140
    .line 1328
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@142
    iget-object v8, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeoutIntent:Landroid/app/PendingIntent;

    #@144
    invoke-virtual {v7, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@147
    .line 1332
    :cond_147
    new-instance v4, Landroid/content/Intent;

    #@149
    const-string v7, "android.location.GPS_FIX_CHANGE"

    #@14b
    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@14e
    .line 1333
    .local v4, intent:Landroid/content/Intent;
    const-string v7, "enabled"

    #@150
    const/4 v8, 0x1

    #@151
    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@154
    .line 1334
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@156
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@158
    invoke-virtual {v7, v4, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@15b
    .line 1335
    const/4 v7, 0x2

    #@15c
    iget v8, p0, Lcom/android/server/location/GpsLocationProvider;->mSvCount:I

    #@15e
    invoke-direct {p0, v7, v8}, Lcom/android/server/location/GpsLocationProvider;->updateStatus(II)V

    #@161
    .line 1338
    .end local v4           #intent:Landroid/content/Intent;
    :cond_161
    const/4 v7, 0x1

    #@162
    invoke-direct {p0, v7}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@165
    move-result v7

    #@166
    if-nez v7, :cond_180

    #@168
    iget-boolean v7, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@16a
    if-eqz v7, :cond_180

    #@16c
    iget v7, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@16e
    const/16 v8, 0x2710

    #@170
    if-le v7, v8, :cond_180

    #@172
    .line 1340
    sget-boolean v7, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@174
    if-eqz v7, :cond_17d

    #@176
    const-string v7, "GpsLocationProvider"

    #@178
    const-string v8, "got fix, hibernating"

    #@17a
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17d
    .line 1341
    :cond_17d
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->hibernate()V

    #@180
    .line 1343
    :cond_180
    return-void

    #@181
    .line 1319
    :catchall_181
    move-exception v7

    #@182
    :try_start_182
    monitor-exit v8
    :try_end_183
    .catchall {:try_start_182 .. :try_end_183} :catchall_181

    #@183
    throw v7
.end method

.method private reportNmea(J)V
    .registers 12
    .parameter "timestamp"

    #@0
    .prologue
    .line 1826
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v7

    #@3
    .line 1827
    :try_start_3
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v5

    #@9
    .line 1828
    .local v5, size:I
    if-lez v5, :cond_3f

    #@b
    .line 1830
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNmeaBuffer:[B

    #@d
    iget-object v8, p0, Lcom/android/server/location/GpsLocationProvider;->mNmeaBuffer:[B

    #@f
    array-length v8, v8

    #@10
    invoke-direct {p0, v6, v8}, Lcom/android/server/location/GpsLocationProvider;->native_read_nmea([BI)I

    #@13
    move-result v2

    #@14
    .line 1831
    .local v2, length:I
    new-instance v4, Ljava/lang/String;

    #@16
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNmeaBuffer:[B

    #@18
    const/4 v8, 0x0

    #@19
    invoke-direct {v4, v6, v8, v2}, Ljava/lang/String;-><init>([BII)V

    #@1c
    .line 1833
    .local v4, nmea:Ljava/lang/String;
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    if-ge v1, v5, :cond_3f

    #@1f
    .line 1834
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v3

    #@25
    check-cast v3, Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_41

    #@27
    .line 1836
    .local v3, listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_start_27
    iget-object v6, v3, Lcom/android/server/location/GpsLocationProvider$Listener;->mListener:Landroid/location/IGpsStatusListener;

    #@29
    invoke-interface {v6, p1, p2, v4}, Landroid/location/IGpsStatusListener;->onNmeaReceived(JLjava/lang/String;)V
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_41
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2c} :catch_2f

    #@2c
    .line 1833
    :goto_2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_1d

    #@2f
    .line 1837
    :catch_2f
    move-exception v0

    #@30
    .line 1838
    .local v0, e:Landroid/os/RemoteException;
    :try_start_30
    const-string v6, "GpsLocationProvider"

    #@32
    const-string v8, "RemoteException in reportNmea"

    #@34
    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1839
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@39
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@3c
    .line 1841
    add-int/lit8 v5, v5, -0x1

    #@3e
    goto :goto_2c

    #@3f
    .line 1845
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    .end local v4           #nmea:Ljava/lang/String;
    :cond_3f
    monitor-exit v7

    #@40
    .line 1846
    return-void

    #@41
    .line 1845
    .end local v5           #size:I
    :catchall_41
    move-exception v6

    #@42
    monitor-exit v7
    :try_end_43
    .catchall {:try_start_30 .. :try_end_43} :catchall_41

    #@43
    throw v6
.end method

.method private reportStatus(I)V
    .registers 12
    .parameter "status"

    #@0
    .prologue
    .line 1349
    sget-boolean v6, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@2
    if-eqz v6, :cond_1c

    #@4
    const-string v6, "GpsLocationProvider"

    #@6
    new-instance v7, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v8, "reportStatus status: "

    #@d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v7

    #@11
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v7

    #@19
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1351
    :cond_1c
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@1e
    monitor-enter v7

    #@1f
    .line 1352
    :try_start_1f
    iget-boolean v5, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@21
    .line 1354
    .local v5, wasNavigating:Z
    packed-switch p1, :pswitch_data_d0

    #@24
    .line 1381
    :cond_24
    :goto_24
    iget-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@26
    if-eq v5, v6, :cond_ce

    #@28
    .line 1382
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v4

    #@2e
    .line 1383
    .local v4, size:I
    const/4 v1, 0x0

    #@2f
    .local v1, i:I
    :goto_2f
    if-ge v1, v4, :cond_b9

    #@31
    .line 1384
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_end_39
    .catchall {:try_start_1f .. :try_end_39} :catchall_4c

    #@39
    .line 1386
    .local v3, listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_start_39
    iget-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@3b
    if-eqz v6, :cond_a3

    #@3d
    .line 1387
    iget-object v6, v3, Lcom/android/server/location/GpsLocationProvider$Listener;->mListener:Landroid/location/IGpsStatusListener;

    #@3f
    invoke-interface {v6}, Landroid/location/IGpsStatusListener;->onGpsStarted()V
    :try_end_42
    .catchall {:try_start_39 .. :try_end_42} :catchall_4c
    .catch Landroid/os/RemoteException; {:try_start_39 .. :try_end_42} :catch_a9

    #@42
    .line 1383
    :goto_42
    add-int/lit8 v1, v1, 0x1

    #@44
    goto :goto_2f

    #@45
    .line 1356
    .end local v1           #i:I
    .end local v3           #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    .end local v4           #size:I
    :pswitch_45
    const/4 v6, 0x1

    #@46
    :try_start_46
    iput-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@48
    .line 1357
    const/4 v6, 0x1

    #@49
    iput-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineOn:Z

    #@4b
    goto :goto_24

    #@4c
    .line 1404
    .end local v5           #wasNavigating:Z
    :catchall_4c
    move-exception v6

    #@4d
    monitor-exit v7
    :try_end_4e
    .catchall {:try_start_46 .. :try_end_4e} :catchall_4c

    #@4e
    throw v6

    #@4f
    .line 1360
    .restart local v5       #wasNavigating:Z
    :pswitch_4f
    const/4 v6, 0x0

    #@50
    :try_start_50
    iput-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@52
    goto :goto_24

    #@53
    .line 1363
    :pswitch_53
    const/4 v6, 0x1

    #@54
    iput-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineOn:Z

    #@56
    goto :goto_24

    #@57
    .line 1366
    :pswitch_57
    const/4 v6, 0x0

    #@58
    iput-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineOn:Z

    #@5a
    .line 1367
    const/4 v6, 0x0

    #@5b
    iput-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@5d
    .line 1371
    const-string v6, "LGU"

    #@5f
    sget-object v8, Lcom/android/server/location/GpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@61
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v6

    #@65
    if-nez v6, :cond_71

    #@67
    const-string v6, "DCM"

    #@69
    sget-object v8, Lcom/android/server/location/GpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@6b
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v6

    #@6f
    if-eqz v6, :cond_24

    #@71
    .line 1372
    :cond_71
    new-instance v2, Landroid/content/Intent;

    #@73
    const-string v6, "com.lge.location.GPS_ENGINE_STATUS_CHANGE"

    #@75
    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@78
    .line 1373
    .local v2, intent:Landroid/content/Intent;
    const-string v6, "com.lge.location.GPS_ENGINE_STATUS"

    #@7a
    iget-boolean v8, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineOn:Z

    #@7c
    invoke-virtual {v2, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@7f
    .line 1374
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@81
    invoke-virtual {v6, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@84
    .line 1375
    sget-boolean v6, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@86
    if-eqz v6, :cond_24

    #@88
    const-string v6, "GpsLocationProvider"

    #@8a
    new-instance v8, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v9, "reportStatus, [LGE] GPS_ENGINE_STATUS_CHANGE "

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    iget-boolean v9, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineOn:Z

    #@97
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v8

    #@9b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v8

    #@9f
    invoke-static {v6, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a2
    .catchall {:try_start_50 .. :try_end_a2} :catchall_4c

    #@a2
    goto :goto_24

    #@a3
    .line 1389
    .end local v2           #intent:Landroid/content/Intent;
    .restart local v1       #i:I
    .restart local v3       #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    .restart local v4       #size:I
    :cond_a3
    :try_start_a3
    iget-object v6, v3, Lcom/android/server/location/GpsLocationProvider$Listener;->mListener:Landroid/location/IGpsStatusListener;

    #@a5
    invoke-interface {v6}, Landroid/location/IGpsStatusListener;->onGpsStopped()V
    :try_end_a8
    .catchall {:try_start_a3 .. :try_end_a8} :catchall_4c
    .catch Landroid/os/RemoteException; {:try_start_a3 .. :try_end_a8} :catch_a9

    #@a8
    goto :goto_42

    #@a9
    .line 1391
    :catch_a9
    move-exception v0

    #@aa
    .line 1392
    .local v0, e:Landroid/os/RemoteException;
    :try_start_aa
    const-string v6, "GpsLocationProvider"

    #@ac
    const-string v8, "RemoteException in reportStatus"

    #@ae
    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    .line 1393
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@b3
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@b6
    .line 1395
    add-int/lit8 v4, v4, -0x1

    #@b8
    goto :goto_42

    #@b9
    .line 1400
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v3           #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    :cond_b9
    new-instance v2, Landroid/content/Intent;

    #@bb
    const-string v6, "android.location.GPS_ENABLED_CHANGE"

    #@bd
    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c0
    .line 1401
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v6, "enabled"

    #@c2
    iget-boolean v8, p0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@c4
    invoke-virtual {v2, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@c7
    .line 1402
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@c9
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@cb
    invoke-virtual {v6, v2, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@ce
    .line 1404
    .end local v1           #i:I
    .end local v2           #intent:Landroid/content/Intent;
    .end local v4           #size:I
    :cond_ce
    monitor-exit v7
    :try_end_cf
    .catchall {:try_start_aa .. :try_end_cf} :catchall_4c

    #@cf
    .line 1405
    return-void

    #@d0
    .line 1354
    :pswitch_data_d0
    .packed-switch 0x1
        :pswitch_45
        :pswitch_4f
        :pswitch_53
        :pswitch_57
    .end packed-switch
.end method

.method private reportSvStatus()V
    .registers 18

    #@0
    .prologue
    .line 1412
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Lcom/android/server/location/GpsLocationProvider;->mSvs:[I

    #@4
    move-object/from16 v0, p0

    #@6
    iget-object v3, v0, Lcom/android/server/location/GpsLocationProvider;->mSnrs:[F

    #@8
    move-object/from16 v0, p0

    #@a
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSvElevations:[F

    #@c
    move-object/from16 v0, p0

    #@e
    iget-object v5, v0, Lcom/android/server/location/GpsLocationProvider;->mSvAzimuths:[F

    #@10
    move-object/from16 v0, p0

    #@12
    iget-object v6, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@14
    move-object/from16 v1, p0

    #@16
    invoke-direct/range {v1 .. v6}, Lcom/android/server/location/GpsLocationProvider;->native_read_sv_status([I[F[F[F[I)I

    #@19
    move-result v2

    #@1a
    .line 1414
    .local v2, svCount:I
    move-object/from16 v0, p0

    #@1c
    iget-object v15, v0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@1e
    monitor-enter v15

    #@1f
    .line 1415
    :try_start_1f
    move-object/from16 v0, p0

    #@21
    iget-object v1, v0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v14

    #@27
    .line 1416
    .local v14, size:I
    const/4 v11, 0x0

    #@28
    .local v11, i:I
    :goto_28
    if-ge v11, v14, :cond_74

    #@2a
    .line 1417
    move-object/from16 v0, p0

    #@2c
    iget-object v1, v0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@31
    move-result-object v13

    #@32
    check-cast v13, Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_end_34
    .catchall {:try_start_1f .. :try_end_34} :catchall_161

    #@34
    .line 1419
    .local v13, listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    :try_start_34
    iget-object v1, v13, Lcom/android/server/location/GpsLocationProvider$Listener;->mListener:Landroid/location/IGpsStatusListener;

    #@36
    move-object/from16 v0, p0

    #@38
    iget-object v3, v0, Lcom/android/server/location/GpsLocationProvider;->mSvs:[I

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSnrs:[F

    #@3e
    move-object/from16 v0, p0

    #@40
    iget-object v5, v0, Lcom/android/server/location/GpsLocationProvider;->mSvElevations:[F

    #@42
    move-object/from16 v0, p0

    #@44
    iget-object v6, v0, Lcom/android/server/location/GpsLocationProvider;->mSvAzimuths:[F

    #@46
    move-object/from16 v0, p0

    #@48
    iget-object v7, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@4a
    const/4 v8, 0x0

    #@4b
    aget v7, v7, v8

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget-object v8, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@51
    const/4 v9, 0x1

    #@52
    aget v8, v8, v9

    #@54
    move-object/from16 v0, p0

    #@56
    iget-object v9, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@58
    const/16 v16, 0x2

    #@5a
    aget v9, v9, v16

    #@5c
    invoke-interface/range {v1 .. v9}, Landroid/location/IGpsStatusListener;->onSvStatusChanged(I[I[F[F[FIII)V
    :try_end_5f
    .catchall {:try_start_34 .. :try_end_5f} :catchall_161
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_5f} :catch_62

    #@5f
    .line 1416
    :goto_5f
    add-int/lit8 v11, v11, 0x1

    #@61
    goto :goto_28

    #@62
    .line 1422
    :catch_62
    move-exception v10

    #@63
    .line 1423
    .local v10, e:Landroid/os/RemoteException;
    :try_start_63
    const-string v1, "GpsLocationProvider"

    #@65
    const-string v3, "RemoteException in reportSvInfo"

    #@67
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 1424
    move-object/from16 v0, p0

    #@6c
    iget-object v1, v0, Lcom/android/server/location/GpsLocationProvider;->mListeners:Ljava/util/ArrayList;

    #@6e
    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@71
    .line 1426
    add-int/lit8 v14, v14, -0x1

    #@73
    goto :goto_5f

    #@74
    .line 1429
    .end local v10           #e:Landroid/os/RemoteException;
    .end local v13           #listener:Lcom/android/server/location/GpsLocationProvider$Listener;
    :cond_74
    monitor-exit v15
    :try_end_75
    .catchall {:try_start_63 .. :try_end_75} :catchall_161

    #@75
    .line 1431
    sget-boolean v1, Lcom/android/server/location/GpsLocationProvider;->VERBOSE:Z

    #@77
    if-eqz v1, :cond_16d

    #@79
    .line 1432
    const-string v1, "GpsLocationProvider"

    #@7b
    new-instance v3, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v4, "SV count: "

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    const-string v4, " ephemerisMask: "

    #@8c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    move-object/from16 v0, p0

    #@92
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@94
    const/4 v5, 0x0

    #@95
    aget v4, v4, v5

    #@97
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    const-string v4, " almanacMask: "

    #@a1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    move-object/from16 v0, p0

    #@a7
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@a9
    const/4 v5, 0x1

    #@aa
    aget v4, v4, v5

    #@ac
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@af
    move-result-object v4

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v3

    #@b4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v3

    #@b8
    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 1435
    const/4 v11, 0x0

    #@bc
    :goto_bc
    if-ge v11, v2, :cond_16d

    #@be
    .line 1436
    const-string v3, "GpsLocationProvider"

    #@c0
    new-instance v1, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v4, "sv: "

    #@c7
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v1

    #@cb
    move-object/from16 v0, p0

    #@cd
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSvs:[I

    #@cf
    aget v4, v4, v11

    #@d1
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v1

    #@d5
    const-string v4, " snr: "

    #@d7
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v1

    #@db
    move-object/from16 v0, p0

    #@dd
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSnrs:[F

    #@df
    aget v4, v4, v11

    #@e1
    const/high16 v5, 0x4120

    #@e3
    div-float/2addr v4, v5

    #@e4
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v1

    #@e8
    const-string v4, " elev: "

    #@ea
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    move-object/from16 v0, p0

    #@f0
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSvElevations:[F

    #@f2
    aget v4, v4, v11

    #@f4
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v1

    #@f8
    const-string v4, " azimuth: "

    #@fa
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v1

    #@fe
    move-object/from16 v0, p0

    #@100
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider;->mSvAzimuths:[F

    #@102
    aget v4, v4, v11

    #@104
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@107
    move-result-object v4

    #@108
    move-object/from16 v0, p0

    #@10a
    iget-object v1, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@10c
    const/4 v5, 0x0

    #@10d
    aget v1, v1, v5

    #@10f
    const/4 v5, 0x1

    #@110
    move-object/from16 v0, p0

    #@112
    iget-object v6, v0, Lcom/android/server/location/GpsLocationProvider;->mSvs:[I

    #@114
    aget v6, v6, v11

    #@116
    add-int/lit8 v6, v6, -0x1

    #@118
    shl-int/2addr v5, v6

    #@119
    and-int/2addr v1, v5

    #@11a
    if-nez v1, :cond_164

    #@11c
    const-string v1, "  "

    #@11e
    :goto_11e
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v4

    #@122
    move-object/from16 v0, p0

    #@124
    iget-object v1, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@126
    const/4 v5, 0x1

    #@127
    aget v1, v1, v5

    #@129
    const/4 v5, 0x1

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget-object v6, v0, Lcom/android/server/location/GpsLocationProvider;->mSvs:[I

    #@12e
    aget v6, v6, v11

    #@130
    add-int/lit8 v6, v6, -0x1

    #@132
    shl-int/2addr v5, v6

    #@133
    and-int/2addr v1, v5

    #@134
    if-nez v1, :cond_167

    #@136
    const-string v1, "  "

    #@138
    :goto_138
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v4

    #@13c
    move-object/from16 v0, p0

    #@13e
    iget-object v1, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@140
    const/4 v5, 0x2

    #@141
    aget v1, v1, v5

    #@143
    const/4 v5, 0x1

    #@144
    move-object/from16 v0, p0

    #@146
    iget-object v6, v0, Lcom/android/server/location/GpsLocationProvider;->mSvs:[I

    #@148
    aget v6, v6, v11

    #@14a
    add-int/lit8 v6, v6, -0x1

    #@14c
    shl-int/2addr v5, v6

    #@14d
    and-int/2addr v1, v5

    #@14e
    if-nez v1, :cond_16a

    #@150
    const-string v1, ""

    #@152
    :goto_152
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v1

    #@156
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@159
    move-result-object v1

    #@15a
    invoke-static {v3, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15d
    .line 1435
    add-int/lit8 v11, v11, 0x1

    #@15f
    goto/16 :goto_bc

    #@161
    .line 1429
    .end local v11           #i:I
    .end local v14           #size:I
    :catchall_161
    move-exception v1

    #@162
    :try_start_162
    monitor-exit v15
    :try_end_163
    .catchall {:try_start_162 .. :try_end_163} :catchall_161

    #@163
    throw v1

    #@164
    .line 1436
    .restart local v11       #i:I
    .restart local v14       #size:I
    :cond_164
    const-string v1, " E"

    #@166
    goto :goto_11e

    #@167
    :cond_167
    const-string v1, " A"

    #@169
    goto :goto_138

    #@16a
    :cond_16a
    const-string v1, "U"

    #@16c
    goto :goto_152

    #@16d
    .line 1447
    :cond_16d
    move-object/from16 v0, p0

    #@16f
    iget v1, v0, Lcom/android/server/location/GpsLocationProvider;->mStatus:I

    #@171
    move-object/from16 v0, p0

    #@173
    iget-object v3, v0, Lcom/android/server/location/GpsLocationProvider;->mSvMasks:[I

    #@175
    const/4 v4, 0x2

    #@176
    aget v3, v3, v4

    #@178
    invoke-static {v3}, Ljava/lang/Integer;->bitCount(I)I

    #@17b
    move-result v3

    #@17c
    move-object/from16 v0, p0

    #@17e
    invoke-direct {v0, v1, v3}, Lcom/android/server/location/GpsLocationProvider;->updateStatus(II)V

    #@181
    .line 1449
    move-object/from16 v0, p0

    #@183
    iget-boolean v1, v0, Lcom/android/server/location/GpsLocationProvider;->mNavigating:Z

    #@185
    if-eqz v1, :cond_1c7

    #@187
    move-object/from16 v0, p0

    #@189
    iget v1, v0, Lcom/android/server/location/GpsLocationProvider;->mStatus:I

    #@18b
    const/4 v3, 0x2

    #@18c
    if-ne v1, v3, :cond_1c7

    #@18e
    move-object/from16 v0, p0

    #@190
    iget-wide v3, v0, Lcom/android/server/location/GpsLocationProvider;->mLastFixTime:J

    #@192
    const-wide/16 v5, 0x0

    #@194
    cmp-long v1, v3, v5

    #@196
    if-lez v1, :cond_1c7

    #@198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@19b
    move-result-wide v3

    #@19c
    move-object/from16 v0, p0

    #@19e
    iget-wide v5, v0, Lcom/android/server/location/GpsLocationProvider;->mLastFixTime:J

    #@1a0
    sub-long/2addr v3, v5

    #@1a1
    const-wide/16 v5, 0x2710

    #@1a3
    cmp-long v1, v3, v5

    #@1a5
    if-lez v1, :cond_1c7

    #@1a7
    .line 1452
    new-instance v12, Landroid/content/Intent;

    #@1a9
    const-string v1, "android.location.GPS_FIX_CHANGE"

    #@1ab
    invoke-direct {v12, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1ae
    .line 1453
    .local v12, intent:Landroid/content/Intent;
    const-string v1, "enabled"

    #@1b0
    const/4 v3, 0x0

    #@1b1
    invoke-virtual {v12, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1b4
    .line 1454
    move-object/from16 v0, p0

    #@1b6
    iget-object v1, v0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@1b8
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1ba
    invoke-virtual {v1, v12, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1bd
    .line 1455
    const/4 v1, 0x1

    #@1be
    move-object/from16 v0, p0

    #@1c0
    iget v3, v0, Lcom/android/server/location/GpsLocationProvider;->mSvCount:I

    #@1c2
    move-object/from16 v0, p0

    #@1c4
    invoke-direct {v0, v1, v3}, Lcom/android/server/location/GpsLocationProvider;->updateStatus(II)V

    #@1c7
    .line 1457
    .end local v12           #intent:Landroid/content/Intent;
    :cond_1c7
    return-void
.end method

.method private requestRefLocation(I)V
    .registers 12
    .parameter "flags"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v5, 0x3

    #@2
    .line 2027
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@4
    const-string v4, "phone"

    #@6
    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v8

    #@a
    check-cast v8, Landroid/telephony/TelephonyManager;

    #@c
    .line 2029
    .local v8, phone:Landroid/telephony/TelephonyManager;
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@f
    move-result v0

    #@10
    if-ne v0, v9, :cond_73

    #@12
    .line 2030
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    #@15
    move-result-object v6

    #@16
    check-cast v6, Landroid/telephony/gsm/GsmCellLocation;

    #@18
    .line 2031
    .local v6, gsm_cell:Landroid/telephony/gsm/GsmCellLocation;
    if-eqz v6, :cond_6b

    #@1a
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@1d
    move-result v0

    #@1e
    if-ne v0, v9, :cond_6b

    #@20
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    if-eqz v0, :cond_6b

    #@26
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@2d
    move-result v0

    #@2e
    if-le v0, v5, :cond_6b

    #@30
    .line 2035
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    const/4 v4, 0x0

    #@35
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3c
    move-result v2

    #@3d
    .line 2036
    .local v2, mcc:I
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@48
    move-result v3

    #@49
    .line 2037
    .local v3, mnc:I
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@4c
    move-result v7

    #@4d
    .line 2038
    .local v7, networkType:I
    if-eq v7, v5, :cond_5b

    #@4f
    const/16 v0, 0x8

    #@51
    if-eq v7, v0, :cond_5b

    #@53
    const/16 v0, 0x9

    #@55
    if-eq v7, v0, :cond_5b

    #@57
    const/16 v0, 0xa

    #@59
    if-ne v7, v0, :cond_69

    #@5b
    .line 2042
    :cond_5b
    const/4 v1, 0x2

    #@5c
    .line 2046
    .local v1, type:I
    :goto_5c
    invoke-virtual {v6}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@5f
    move-result v4

    #@60
    invoke-virtual {v6}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@63
    move-result v5

    #@64
    move-object v0, p0

    #@65
    invoke-direct/range {v0 .. v5}, Lcom/android/server/location/GpsLocationProvider;->native_agps_set_ref_location_cellid(IIIII)V

    #@68
    .line 2055
    .end local v1           #type:I
    .end local v2           #mcc:I
    .end local v3           #mnc:I
    .end local v6           #gsm_cell:Landroid/telephony/gsm/GsmCellLocation;
    .end local v7           #networkType:I
    :goto_68
    return-void

    #@69
    .line 2044
    .restart local v2       #mcc:I
    .restart local v3       #mnc:I
    .restart local v6       #gsm_cell:Landroid/telephony/gsm/GsmCellLocation;
    .restart local v7       #networkType:I
    :cond_69
    const/4 v1, 0x1

    #@6a
    .restart local v1       #type:I
    goto :goto_5c

    #@6b
    .line 2049
    .end local v1           #type:I
    .end local v2           #mcc:I
    .end local v3           #mnc:I
    .end local v7           #networkType:I
    :cond_6b
    const-string v0, "GpsLocationProvider"

    #@6d
    const-string v4, "Error getting cell location info."

    #@6f
    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_68

    #@73
    .line 2053
    .end local v6           #gsm_cell:Landroid/telephony/gsm/GsmCellLocation;
    :cond_73
    const-string v0, "GpsLocationProvider"

    #@75
    const-string v4, "CDMA not supported."

    #@77
    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    goto :goto_68
.end method

.method private requestSetID(I)V
    .registers 8
    .parameter "flags"

    #@0
    .prologue
    .line 1986
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "phone"

    #@4
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/telephony/TelephonyManager;

    #@a
    .line 1988
    .local v2, phone:Landroid/telephony/TelephonyManager;
    const/4 v3, 0x0

    #@b
    .line 1989
    .local v3, type:I
    const-string v0, ""

    #@d
    .line 1991
    .local v0, data:Ljava/lang/String;
    and-int/lit8 v4, p1, 0x1

    #@f
    const/4 v5, 0x1

    #@10
    if-ne v4, v5, :cond_1f

    #@12
    .line 1992
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 1993
    .local v1, data_temp:Ljava/lang/String;
    if-nez v1, :cond_1c

    #@18
    .line 2011
    .end local v1           #data_temp:Ljava/lang/String;
    :cond_18
    :goto_18
    invoke-direct {p0, v3, v0}, Lcom/android/server/location/GpsLocationProvider;->native_agps_set_id(ILjava/lang/String;)V

    #@1b
    .line 2012
    return-void

    #@1c
    .line 1997
    .restart local v1       #data_temp:Ljava/lang/String;
    :cond_1c
    move-object v0, v1

    #@1d
    .line 1998
    const/4 v3, 0x1

    #@1e
    goto :goto_18

    #@1f
    .line 2001
    .end local v1           #data_temp:Ljava/lang/String;
    :cond_1f
    and-int/lit8 v4, p1, 0x2

    #@21
    const/4 v5, 0x2

    #@22
    if-ne v4, v5, :cond_18

    #@24
    .line 2002
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 2003
    .restart local v1       #data_temp:Ljava/lang/String;
    if-eqz v1, :cond_18

    #@2a
    .line 2007
    move-object v0, v1

    #@2b
    .line 2008
    const/4 v3, 0x2

    #@2c
    goto :goto_18
.end method

.method private requestUtcTime()V
    .registers 4

    #@0
    .prologue
    .line 2019
    const/4 v0, 0x5

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@6
    .line 2020
    return-void
.end method

.method private restoreOriginalNetworkPreference()V
    .registers 3

    #@0
    .prologue
    .line 1506
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mConnMgr:Landroid/net/ConnectivityManager;

    #@2
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@4
    invoke-static {v1}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3500(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->setNetworkPreference(I)V

    #@b
    .line 1507
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@d
    const/4 v1, -0x1

    #@e
    invoke-static {v0, v1}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$3502(Lcom/android/server/location/GpsLocationProvider$WifiState;I)I

    #@11
    .line 1508
    return-void
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .registers 6
    .parameter "message"
    .parameter "arg"
    .parameter "obj"

    #@0
    .prologue
    .line 2061
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5
    .line 2062
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mHandler:Landroid/os/Handler;

    #@7
    const/4 v1, 0x1

    #@8
    invoke-virtual {v0, p1, p2, v1, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@f
    .line 2063
    return-void
.end method

.method private setEngineCapabilities(I)V
    .registers 3
    .parameter "capabilities"

    #@0
    .prologue
    .line 1852
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineCapabilities:I

    #@2
    .line 1854
    const/16 v0, 0x10

    #@4
    invoke-direct {p0, v0}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mPeriodicTimeInjection:Z

    #@c
    if-nez v0, :cond_14

    #@e
    .line 1855
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mPeriodicTimeInjection:Z

    #@11
    .line 1856
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->requestUtcTime()V

    #@14
    .line 1858
    :cond_14
    return-void
.end method

.method private startNavigating()V
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1180
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@4
    if-nez v0, :cond_35

    #@6
    .line 1181
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@8
    if-eqz v0, :cond_11

    #@a
    const-string v0, "GpsLocationProvider"

    #@c
    const-string v1, "startNavigating"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 1182
    :cond_11
    iput v2, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeToFirstFix:I

    #@13
    .line 1183
    const-wide/16 v0, 0x0

    #@15
    iput-wide v0, p0, Lcom/android/server/location/GpsLocationProvider;->mLastFixTime:J

    #@17
    .line 1184
    iput-boolean v6, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@19
    .line 1198
    invoke-direct {p0, v6}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_36

    #@1f
    iget v3, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@21
    .line 1199
    .local v3, interval:I
    :goto_21
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider;->mPositionMode:I

    #@23
    move-object v0, p0

    #@24
    move v4, v2

    #@25
    move v5, v2

    #@26
    invoke-direct/range {v0 .. v5}, Lcom/android/server/location/GpsLocationProvider;->native_set_position_mode(IIIII)Z

    #@29
    move-result v0

    #@2a
    if-nez v0, :cond_39

    #@2c
    .line 1201
    iput-boolean v2, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@2e
    .line 1202
    const-string v0, "GpsLocationProvider"

    #@30
    const-string v1, "set_position_mode failed in startNavigating()"

    #@32
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1223
    .end local v3           #interval:I
    :cond_35
    :goto_35
    return-void

    #@36
    .line 1198
    :cond_36
    const/16 v3, 0x3e8

    #@38
    goto :goto_21

    #@39
    .line 1205
    .restart local v3       #interval:I
    :cond_39
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_start()Z

    #@3c
    move-result v0

    #@3d
    if-nez v0, :cond_49

    #@3f
    .line 1206
    iput-boolean v2, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@41
    .line 1207
    const-string v0, "GpsLocationProvider"

    #@43
    const-string v1, "native_start failed in startNavigating()"

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_35

    #@49
    .line 1212
    :cond_49
    invoke-direct {p0, v6, v2}, Lcom/android/server/location/GpsLocationProvider;->updateStatus(II)V

    #@4c
    .line 1213
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4f
    move-result-wide v0

    #@50
    iput-wide v0, p0, Lcom/android/server/location/GpsLocationProvider;->mFixRequestTime:J

    #@52
    .line 1214
    invoke-direct {p0, v6}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@55
    move-result v0

    #@56
    if-nez v0, :cond_35

    #@58
    .line 1217
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@5a
    const v1, 0xea60

    #@5d
    if-lt v0, v1, :cond_35

    #@5f
    .line 1218
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mAlarmManager:Landroid/app/AlarmManager;

    #@61
    const/4 v1, 0x2

    #@62
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@65
    move-result-wide v4

    #@66
    const-wide/32 v6, 0xea60

    #@69
    add-long/2addr v4, v6

    #@6a
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeoutIntent:Landroid/app/PendingIntent;

    #@6c
    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@6f
    goto :goto_35
.end method

.method private stopNavigating()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1226
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "GpsLocationProvider"

    #@7
    const-string v1, "stopNavigating"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1227
    :cond_c
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@e
    if-eqz v0, :cond_21

    #@10
    .line 1228
    iput-boolean v2, p0, Lcom/android/server/location/GpsLocationProvider;->mStarted:Z

    #@12
    .line 1229
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_stop()Z

    #@15
    .line 1230
    iput v2, p0, Lcom/android/server/location/GpsLocationProvider;->mTimeToFirstFix:I

    #@17
    .line 1231
    const-wide/16 v0, 0x0

    #@19
    iput-wide v0, p0, Lcom/android/server/location/GpsLocationProvider;->mLastFixTime:J

    #@1b
    .line 1232
    iput v2, p0, Lcom/android/server/location/GpsLocationProvider;->mLocationFlags:I

    #@1d
    .line 1235
    const/4 v0, 0x1

    #@1e
    invoke-direct {p0, v0, v2}, Lcom/android/server/location/GpsLocationProvider;->updateStatus(II)V

    #@21
    .line 1237
    :cond_21
    return-void
.end method

.method private updateClientUids([I)V
    .registers 15
    .parameter "uids"

    #@0
    .prologue
    .line 1077
    move-object v0, p1

    #@1
    .local v0, arr$:[I
    array-length v5, v0

    #@2
    .local v5, len$:I
    const/4 v3, 0x0

    #@3
    .local v3, i$:I
    move v4, v3

    #@4
    .end local v0           #arr$:[I
    .end local v3           #i$:I
    .end local v5           #len$:I
    .local v4, i$:I
    :goto_4
    if-ge v4, v5, :cond_2b

    #@6
    aget v9, v0, v4

    #@8
    .line 1078
    .local v9, uid1:I
    const/4 v7, 0x1

    #@9
    .line 1079
    .local v7, newUid:Z
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mClientUids:[I

    #@b
    .local v1, arr$:[I
    array-length v6, v1

    #@c
    .local v6, len$:I
    const/4 v3, 0x0

    #@d
    .end local v4           #i$:I
    .restart local v3       #i$:I
    :goto_d
    if-ge v3, v6, :cond_14

    #@f
    aget v10, v1, v3

    #@11
    .line 1080
    .local v10, uid2:I
    if-ne v9, v10, :cond_1f

    #@13
    .line 1081
    const/4 v7, 0x0

    #@14
    .line 1085
    .end local v10           #uid2:I
    :cond_14
    if-eqz v7, :cond_1b

    #@16
    .line 1087
    :try_start_16
    iget-object v11, p0, Lcom/android/server/location/GpsLocationProvider;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@18
    invoke-interface {v11, v9}, Lcom/android/internal/app/IBatteryStats;->noteStartGps(I)V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1b} :catch_22

    #@1b
    .line 1077
    :cond_1b
    :goto_1b
    add-int/lit8 v3, v4, 0x1

    #@1d
    move v4, v3

    #@1e
    .end local v3           #i$:I
    .restart local v4       #i$:I
    goto :goto_4

    #@1f
    .line 1079
    .end local v4           #i$:I
    .restart local v3       #i$:I
    .restart local v10       #uid2:I
    :cond_1f
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_d

    #@22
    .line 1088
    .end local v10           #uid2:I
    :catch_22
    move-exception v2

    #@23
    .line 1089
    .local v2, e:Landroid/os/RemoteException;
    const-string v11, "GpsLocationProvider"

    #@25
    const-string v12, "RemoteException"

    #@27
    invoke-static {v11, v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2a
    goto :goto_1b

    #@2b
    .line 1095
    .end local v1           #arr$:[I
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v3           #i$:I
    .end local v6           #len$:I
    .end local v7           #newUid:Z
    .end local v9           #uid1:I
    .restart local v4       #i$:I
    :cond_2b
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mClientUids:[I

    #@2d
    .restart local v0       #arr$:[I
    array-length v5, v0

    #@2e
    .restart local v5       #len$:I
    const/4 v3, 0x0

    #@2f
    .end local v4           #i$:I
    .restart local v3       #i$:I
    move v4, v3

    #@30
    .end local v0           #arr$:[I
    .end local v3           #i$:I
    .end local v5           #len$:I
    .restart local v4       #i$:I
    :goto_30
    if-ge v4, v5, :cond_56

    #@32
    aget v9, v0, v4

    #@34
    .line 1096
    .restart local v9       #uid1:I
    const/4 v8, 0x1

    #@35
    .line 1097
    .local v8, oldUid:Z
    move-object v1, p1

    #@36
    .restart local v1       #arr$:[I
    array-length v6, v1

    #@37
    .restart local v6       #len$:I
    const/4 v3, 0x0

    #@38
    .end local v4           #i$:I
    .restart local v3       #i$:I
    :goto_38
    if-ge v3, v6, :cond_3f

    #@3a
    aget v10, v1, v3

    #@3c
    .line 1098
    .restart local v10       #uid2:I
    if-ne v9, v10, :cond_4a

    #@3e
    .line 1099
    const/4 v8, 0x0

    #@3f
    .line 1103
    .end local v10           #uid2:I
    :cond_3f
    if-eqz v8, :cond_46

    #@41
    .line 1105
    :try_start_41
    iget-object v11, p0, Lcom/android/server/location/GpsLocationProvider;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@43
    invoke-interface {v11, v9}, Lcom/android/internal/app/IBatteryStats;->noteStopGps(I)V
    :try_end_46
    .catch Landroid/os/RemoteException; {:try_start_41 .. :try_end_46} :catch_4d

    #@46
    .line 1095
    :cond_46
    :goto_46
    add-int/lit8 v3, v4, 0x1

    #@48
    move v4, v3

    #@49
    .end local v3           #i$:I
    .restart local v4       #i$:I
    goto :goto_30

    #@4a
    .line 1097
    .end local v4           #i$:I
    .restart local v3       #i$:I
    .restart local v10       #uid2:I
    :cond_4a
    add-int/lit8 v3, v3, 0x1

    #@4c
    goto :goto_38

    #@4d
    .line 1106
    .end local v10           #uid2:I
    :catch_4d
    move-exception v2

    #@4e
    .line 1107
    .restart local v2       #e:Landroid/os/RemoteException;
    const-string v11, "GpsLocationProvider"

    #@50
    const-string v12, "RemoteException"

    #@52
    invoke-static {v11, v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@55
    goto :goto_46

    #@56
    .line 1113
    .end local v1           #arr$:[I
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v3           #i$:I
    .end local v6           #len$:I
    .end local v8           #oldUid:Z
    .end local v9           #uid1:I
    .restart local v4       #i$:I
    :cond_56
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider;->mClientUids:[I

    #@58
    .line 1114
    return-void
.end method

.method private updateStatus(II)V
    .registers 5
    .parameter "status"
    .parameter "svCount"

    #@0
    .prologue
    .line 991
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStatus:I

    #@2
    if-ne p1, v0, :cond_8

    #@4
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mSvCount:I

    #@6
    if-eq p2, v0, :cond_19

    #@8
    .line 992
    :cond_8
    iput p1, p0, Lcom/android/server/location/GpsLocationProvider;->mStatus:I

    #@a
    .line 993
    iput p2, p0, Lcom/android/server/location/GpsLocationProvider;->mSvCount:I

    #@c
    .line 994
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mLocationExtras:Landroid/os/Bundle;

    #@e
    const-string v1, "satellites"

    #@10
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 995
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@16
    move-result-wide v0

    #@17
    iput-wide v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStatusUpdateTime:J

    #@19
    .line 997
    :cond_19
    return-void
.end method

.method private xtraDownloadRequest()V
    .registers 4

    #@0
    .prologue
    .line 1864
    sget-boolean v0, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "GpsLocationProvider"

    #@6
    const-string v1, "xtraDownloadRequest"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1865
    :cond_b
    const/4 v0, 0x6

    #@c
    const/4 v1, 0x0

    #@d
    const/4 v2, 0x0

    #@e
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@11
    .line 1866
    return-void
.end method


# virtual methods
.method public disable()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 950
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 951
    :try_start_4
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mEnabled:Z

    #@6
    if-nez v0, :cond_a

    #@8
    monitor-exit v1

    #@9
    .line 956
    :goto_9
    return-void

    #@a
    .line 952
    :cond_a
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mEnabled:Z

    #@d
    .line 953
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_14

    #@e
    .line 955
    const/4 v0, 0x2

    #@f
    const/4 v1, 0x0

    #@10
    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@13
    goto :goto_9

    #@14
    .line 953
    :catchall_14
    move-exception v0

    #@15
    :try_start_15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2138
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 2139
    .local v0, s:Ljava/lang/StringBuilder;
    const-string v1, "  mFixInterval="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider;->mFixInterval:I

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "\n"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 2140
    const-string v1, "  mEngineCapabilities=0x"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    iget v2, p0, Lcom/android/server/location/GpsLocationProvider;->mEngineCapabilities:I

    #@1e
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, " ("

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 2141
    const/4 v1, 0x1

    #@2c
    invoke-direct {p0, v1}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_37

    #@32
    const-string v1, "SCHED "

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 2142
    :cond_37
    const/4 v1, 0x2

    #@38
    invoke-direct {p0, v1}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_43

    #@3e
    const-string v1, "MSB "

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 2143
    :cond_43
    const/4 v1, 0x4

    #@44
    invoke-direct {p0, v1}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@47
    move-result v1

    #@48
    if-eqz v1, :cond_4f

    #@4a
    const-string v1, "MSA "

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    .line 2144
    :cond_4f
    const/16 v1, 0x8

    #@51
    invoke-direct {p0, v1}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@54
    move-result v1

    #@55
    if-eqz v1, :cond_5c

    #@57
    const-string v1, "SINGLE_SHOT "

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 2145
    :cond_5c
    const/16 v1, 0x10

    #@5e
    invoke-direct {p0, v1}, Lcom/android/server/location/GpsLocationProvider;->hasCapability(I)Z

    #@61
    move-result v1

    #@62
    if-eqz v1, :cond_69

    #@64
    const-string v1, "ON_DEMAND_TIME "

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 2146
    :cond_69
    const-string v1, ")\n"

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    .line 2148
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_get_internal_state()Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    .line 2149
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@78
    .line 2150
    return-void
.end method

.method public enable()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 899
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 900
    :try_start_4
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mEnabled:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    monitor-exit v1

    #@9
    .line 905
    :goto_9
    return-void

    #@a
    .line 901
    :cond_a
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mEnabled:Z

    #@d
    .line 902
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_14

    #@e
    .line 904
    const/4 v0, 0x2

    #@f
    const/4 v1, 0x0

    #@10
    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@13
    goto :goto_9

    #@14
    .line 902
    :catchall_14
    move-exception v0

    #@15
    :try_start_15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public getGpsStatusProvider()Landroid/location/IGpsStatusProvider;
    .registers 2

    #@0
    .prologue
    .line 416
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

    #@2
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 653
    const-string v0, "gps"

    #@2
    return-object v0
.end method

.method public getNetInitiatedListener()Landroid/location/INetInitiatedListener;
    .registers 2

    #@0
    .prologue
    .line 1886
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider;->mNetInitiatedListener:Landroid/location/INetInitiatedListener;

    #@2
    return-object v0
.end method

.method public getProperties()Lcom/android/internal/location/ProviderProperties;
    .registers 2

    #@0
    .prologue
    .line 658
    sget-object v0, Lcom/android/server/location/GpsLocationProvider;->PROPERTIES:Lcom/android/internal/location/ProviderProperties;

    #@2
    return-object v0
.end method

.method public getStatus(Landroid/os/Bundle;)I
    .registers 4
    .parameter "extras"

    #@0
    .prologue
    .line 984
    if-eqz p1, :cond_9

    #@2
    .line 985
    const-string v0, "satellites"

    #@4
    iget v1, p0, Lcom/android/server/location/GpsLocationProvider;->mSvCount:I

    #@6
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@9
    .line 987
    :cond_9
    iget v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStatus:I

    #@b
    return v0
.end method

.method public getStatusUpdateTime()J
    .registers 3

    #@0
    .prologue
    .line 1001
    iget-wide v0, p0, Lcom/android/server/location/GpsLocationProvider;->mStatusUpdateTime:J

    #@2
    return-wide v0
.end method

.method protected handleEnable(I)V
    .registers 6
    .parameter "provider_message_type"

    #@0
    .prologue
    .line 912
    sget-boolean v1, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@2
    if-eqz v1, :cond_b

    #@4
    const-string v1, "GpsLocationProvider"

    #@6
    const-string v2, "handleEnable"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 922
    :cond_b
    invoke-virtual {p0}, Lcom/android/server/location/GpsLocationProvider;->native_init()Z

    #@e
    move-result v0

    #@f
    .line 924
    .local v0, enabled:Z
    if-eqz v0, :cond_30

    #@11
    .line 925
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->native_supports_xtra()Z

    #@14
    move-result v1

    #@15
    iput-boolean v1, p0, Lcom/android/server/location/GpsLocationProvider;->mSupportsXtra:Z

    #@17
    .line 926
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mSuplServerHost:Ljava/lang/String;

    #@19
    if-eqz v1, :cond_23

    #@1b
    .line 927
    const/4 v1, 0x1

    #@1c
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mSuplServerHost:Ljava/lang/String;

    #@1e
    iget v3, p0, Lcom/android/server/location/GpsLocationProvider;->mSuplServerPort:I

    #@20
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/location/GpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@23
    .line 929
    :cond_23
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mC2KServerHost:Ljava/lang/String;

    #@25
    if-eqz v1, :cond_2f

    #@27
    .line 930
    const/4 v1, 0x2

    #@28
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mC2KServerHost:Ljava/lang/String;

    #@2a
    iget v3, p0, Lcom/android/server/location/GpsLocationProvider;->mC2KServerPort:I

    #@2c
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/location/GpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@2f
    .line 938
    :cond_2f
    :goto_2f
    return-void

    #@30
    .line 933
    :cond_30
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider;->mLock:Ljava/lang/Object;

    #@32
    monitor-enter v2

    #@33
    .line 934
    const/4 v1, 0x0

    #@34
    :try_start_34
    iput-boolean v1, p0, Lcom/android/server/location/GpsLocationProvider;->mEnabled:Z

    #@36
    .line 935
    monitor-exit v2
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_3f

    #@37
    .line 936
    const-string v1, "GpsLocationProvider"

    #@39
    const-string v2, "Failed to enable location provider"

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_2f

    #@3f
    .line 935
    :catchall_3f
    move-exception v1

    #@40
    :try_start_40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_40 .. :try_end_41} :catchall_3f

    #@41
    throw v1
.end method

.method public isEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 977
    iget-object v1, p0, Lcom/android/server/location/GpsLocationProvider;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 978
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/location/GpsLocationProvider;->mEnabled:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 979
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public isWifiConnectedToSSID(Landroid/net/NetworkInfo;Ljava/lang/String;)Z
    .registers 9
    .parameter "info"
    .parameter "ssid"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1643
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@4
    move-result-object v0

    #@5
    .line 1644
    .local v0, networkState:Landroid/net/NetworkInfo$DetailedState;
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@7
    if-ne v0, v3, :cond_73

    #@9
    .line 1645
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider;->mWifiState:Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@b
    invoke-static {v3}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@12
    move-result-object v1

    #@13
    .line 1646
    .local v1, wifiInfo:Landroid/net/wifi/WifiInfo;
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@15
    if-eqz v3, :cond_2f

    #@17
    const-string v3, "GpsLocationProvider"

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "wifiInfo  = "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1647
    :cond_2f
    if-eqz p2, :cond_48

    #@31
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_48

    #@3b
    .line 1648
    sget-boolean v2, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@3d
    if-eqz v2, :cond_46

    #@3f
    const-string v2, "GpsLocationProvider"

    #@41
    const-string v3, "wifi connected, and ssid matches expected!"

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1649
    :cond_46
    const/4 v2, 0x1

    #@47
    .line 1656
    .end local v1           #wifiInfo:Landroid/net/wifi/WifiInfo;
    :cond_47
    :goto_47
    return v2

    #@48
    .line 1651
    .restart local v1       #wifiInfo:Landroid/net/wifi/WifiInfo;
    :cond_48
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@4a
    if-eqz v3, :cond_47

    #@4c
    const-string v3, "GpsLocationProvider"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "ssid="

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    const-string v5, " doesn\'t match wifiInfo.getSSID()="

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v4

    #@6f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_47

    #@73
    .line 1655
    .end local v1           #wifiInfo:Landroid/net/wifi/WifiInfo;
    :cond_73
    sget-boolean v3, Lcom/android/server/location/GpsLocationProvider;->DEBUG:Z

    #@75
    if-eqz v3, :cond_47

    #@77
    const-string v3, "GpsLocationProvider"

    #@79
    const-string v4, "not connected"

    #@7b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    goto :goto_47
.end method

.method protected native native_init()Z
.end method

.method protected native_lge_gnss_command([BI)Z
	.registers 4
	const/4 v0, 0x1
	return v0
.end method

.method protected native native_set_agps_server(ILjava/lang/String;I)V
.end method

.method protected readXtraDownloadDate()V
    .registers 9

    #@0
    .prologue
    .line 2654
    :try_start_0
    new-instance v2, Ljava/io/File;

    #@2
    const-string v5, "/data/ext_gps.conf"

    #@4
    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 2655
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@a
    move-result v5

    #@b
    const/4 v6, 0x1

    #@c
    if-ne v5, v6, :cond_2e

    #@e
    .line 2657
    new-instance v3, Ljava/util/Properties;

    #@10
    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    #@13
    .line 2658
    .local v3, properties:Ljava/util/Properties;
    new-instance v4, Ljava/io/FileInputStream;

    #@15
    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@18
    .line 2659
    .local v4, stream:Ljava/io/FileInputStream;
    invoke-virtual {v3, v4}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    #@1b
    .line 2660
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@1e
    .line 2662
    const-string v5, "XTRA_DOWNLOAD_DATE"

    #@20
    const-string v6, "0"

    #@22
    invoke-virtual {v3, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_25} :catch_49

    #@25
    move-result-object v0

    #@26
    .line 2663
    .local v0, date:Ljava/lang/String;
    if-eqz v0, :cond_2e

    #@28
    .line 2665
    :try_start_28
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2b
    move-result-wide v5

    #@2c
    sput-wide v5, Lcom/android/server/location/GpsLocationProvider;->mXtraDownloadDate:J
    :try_end_2e
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_2e} :catch_2f
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_2e} :catch_49

    #@2e
    .line 2674
    .end local v0           #date:Ljava/lang/String;
    .end local v2           #file:Ljava/io/File;
    .end local v3           #properties:Ljava/util/Properties;
    .end local v4           #stream:Ljava/io/FileInputStream;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 2666
    .restart local v0       #date:Ljava/lang/String;
    .restart local v2       #file:Ljava/io/File;
    .restart local v3       #properties:Ljava/util/Properties;
    .restart local v4       #stream:Ljava/io/FileInputStream;
    :catch_2f
    move-exception v1

    #@30
    .line 2667
    .local v1, e:Ljava/lang/NumberFormatException;
    :try_start_30
    const-string v5, "GpsLocationProvider"

    #@32
    new-instance v6, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v7, "unable to parse XTRA_DOWNLOAD_DATE: "

    #@39
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v6

    #@45
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_48} :catch_49

    #@48
    goto :goto_2e

    #@49
    .line 2671
    .end local v0           #date:Ljava/lang/String;
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v2           #file:Ljava/io/File;
    .end local v3           #properties:Ljava/util/Properties;
    .end local v4           #stream:Ljava/io/FileInputStream;
    :catch_49
    move-exception v1

    #@4a
    .line 2672
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "GpsLocationProvider"

    #@4c
    const-string v6, " LG GNSS Ext ext_gps.conf File does not exist"

    #@4e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    goto :goto_2e
.end method

.method public reportNiNotification(IIIIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .registers 21
    .parameter "notificationId"
    .parameter "niType"
    .parameter "notifyFlags"
    .parameter "timeout"
    .parameter "defaultResponse"
    .parameter "requestorId"
    .parameter "text"
    .parameter "requestorIdEncoding"
    .parameter "textEncoding"
    .parameter "extras"

    #@0
    .prologue
    .line 1908
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock2:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5
    .line 1909
    const-string v7, "GpsLocationProvider"

    #@7
    const-string v8, "mSpirentWakeLock2.acquire()"

    #@9
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1911
    const-string v7, "ro.build.target_operator"

    #@e
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v7

    #@12
    const-string v8, "ATT"

    #@14
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v7

    #@18
    if-eqz v7, :cond_4b

    #@1a
    const-string v7, "ro.build.target_country"

    #@1c
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v7

    #@20
    const-string v8, "US"

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_4b

    #@28
    .line 1914
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2a
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@2d
    move-result v7

    #@2e
    if-eqz v7, :cond_3c

    #@30
    .line 1916
    const-string v7, "GpsLocationProvider"

    #@32
    const-string v8, "mSpirentWakeLock.isHeld() -release mSpirentWakeLock"

    #@34
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1917
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock:Landroid/os/PowerManager$WakeLock;

    #@39
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    #@3c
    .line 1919
    :cond_3c
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3e
    const-wide/32 v8, 0x493e0

    #@41
    invoke-virtual {v7, v8, v9}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@44
    .line 1920
    const-string v7, "GpsLocationProvider"

    #@46
    const-string v8, "mSpirentWakeLock.acquire(300000)"

    #@48
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1923
    :cond_4b
    const-string v7, "GpsLocationProvider"

    #@4d
    const-string v8, "reportNiNotification: entered"

    #@4f
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1924
    const-string v7, "GpsLocationProvider"

    #@54
    new-instance v8, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v9, "notificationId: "

    #@5b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    const-string v9, ", niType: "

    #@65
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v8

    #@69
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v8

    #@6d
    const-string v9, ", notifyFlags: "

    #@6f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v8

    #@73
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    const-string v9, ", timeout: "

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v8

    #@81
    const-string v9, ", defaultResponse: "

    #@83
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v8, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v8

    #@8b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v8

    #@8f
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 1930
    const-string v7, "GpsLocationProvider"

    #@94
    new-instance v8, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v9, "requestorId: "

    #@9b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    move-object/from16 v0, p6

    #@a1
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v8

    #@a5
    const-string v9, ", text: "

    #@a7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v8

    #@ab
    move-object/from16 v0, p7

    #@ad
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v8

    #@b1
    const-string v9, ", requestorIdEncoding: "

    #@b3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v8

    #@b7
    move/from16 v0, p8

    #@b9
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v8

    #@bd
    const-string v9, ", textEncoding: "

    #@bf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v8

    #@c3
    move/from16 v0, p9

    #@c5
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v8

    #@c9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v8

    #@cd
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    .line 1935
    new-instance v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;

    #@d2
    invoke-direct {v6}, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;-><init>()V

    #@d5
    .line 1937
    .local v6, notification:Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;
    iput p1, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->notificationId:I

    #@d7
    .line 1938
    iput p2, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->niType:I

    #@d9
    .line 1939
    and-int/lit8 v7, p3, 0x1

    #@db
    if-eqz v7, :cond_13e

    #@dd
    const/4 v7, 0x1

    #@de
    :goto_de
    iput-boolean v7, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needNotify:Z

    #@e0
    .line 1940
    and-int/lit8 v7, p3, 0x2

    #@e2
    if-eqz v7, :cond_140

    #@e4
    const/4 v7, 0x1

    #@e5
    :goto_e5
    iput-boolean v7, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->needVerify:Z

    #@e7
    .line 1941
    and-int/lit8 v7, p3, 0x4

    #@e9
    if-eqz v7, :cond_142

    #@eb
    const/4 v7, 0x1

    #@ec
    :goto_ec
    iput-boolean v7, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->privacyOverride:Z

    #@ee
    .line 1942
    iput p4, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->timeout:I

    #@f0
    .line 1943
    iput p5, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->defaultResponse:I

    #@f2
    .line 1944
    move-object/from16 v0, p6

    #@f4
    iput-object v0, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorId:Ljava/lang/String;

    #@f6
    .line 1945
    move-object/from16 v0, p7

    #@f8
    iput-object v0, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->text:Ljava/lang/String;

    #@fa
    .line 1946
    move/from16 v0, p8

    #@fc
    iput v0, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->requestorIdEncoding:I

    #@fe
    .line 1947
    move/from16 v0, p9

    #@100
    iput v0, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->textEncoding:I

    #@102
    .line 1951
    new-instance v1, Landroid/os/Bundle;

    #@104
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@107
    .line 1953
    .local v1, bundle:Landroid/os/Bundle;
    if-nez p10, :cond_10b

    #@109
    const-string p10, ""

    #@10b
    .line 1954
    :cond_10b
    new-instance v4, Ljava/util/Properties;

    #@10d
    invoke-direct {v4}, Ljava/util/Properties;-><init>()V

    #@110
    .line 1957
    .local v4, extraProp:Ljava/util/Properties;
    :try_start_110
    new-instance v7, Ljava/io/StringReader;

    #@112
    move-object/from16 v0, p10

    #@114
    invoke-direct {v7, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    #@117
    invoke-virtual {v4, v7}, Ljava/util/Properties;->load(Ljava/io/Reader;)V
    :try_end_11a
    .catch Ljava/io/IOException; {:try_start_110 .. :try_end_11a} :catch_144

    #@11a
    .line 1964
    :goto_11a
    invoke-virtual {v4}, Ljava/util/Properties;->entrySet()Ljava/util/Set;

    #@11d
    move-result-object v7

    #@11e
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@121
    move-result-object v5

    #@122
    .local v5, i$:Ljava/util/Iterator;
    :goto_122
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@125
    move-result v7

    #@126
    if-eqz v7, :cond_160

    #@128
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12b
    move-result-object v3

    #@12c
    check-cast v3, Ljava/util/Map$Entry;

    #@12e
    .line 1966
    .local v3, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@131
    move-result-object v7

    #@132
    check-cast v7, Ljava/lang/String;

    #@134
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@137
    move-result-object v8

    #@138
    check-cast v8, Ljava/lang/String;

    #@13a
    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@13d
    goto :goto_122

    #@13e
    .line 1939
    .end local v1           #bundle:Landroid/os/Bundle;
    .end local v3           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/lang/Object;>;"
    .end local v4           #extraProp:Ljava/util/Properties;
    .end local v5           #i$:Ljava/util/Iterator;
    :cond_13e
    const/4 v7, 0x0

    #@13f
    goto :goto_de

    #@140
    .line 1940
    :cond_140
    const/4 v7, 0x0

    #@141
    goto :goto_e5

    #@142
    .line 1941
    :cond_142
    const/4 v7, 0x0

    #@143
    goto :goto_ec

    #@144
    .line 1959
    .restart local v1       #bundle:Landroid/os/Bundle;
    .restart local v4       #extraProp:Ljava/util/Properties;
    :catch_144
    move-exception v2

    #@145
    .line 1961
    .local v2, e:Ljava/io/IOException;
    const-string v7, "GpsLocationProvider"

    #@147
    new-instance v8, Ljava/lang/StringBuilder;

    #@149
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@14c
    const-string v9, "reportNiNotification cannot parse extras data: "

    #@14e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v8

    #@152
    move-object/from16 v0, p10

    #@154
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v8

    #@158
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15b
    move-result-object v8

    #@15c
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    goto :goto_11a

    #@160
    .line 1969
    .end local v2           #e:Ljava/io/IOException;
    .restart local v5       #i$:Ljava/util/Iterator;
    :cond_160
    iput-object v1, v6, Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;->extras:Landroid/os/Bundle;

    #@162
    .line 1971
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mNIHandler:Lcom/android/internal/location/GpsNetInitiatedHandler;

    #@164
    invoke-virtual {v7, v6}, Lcom/android/internal/location/GpsNetInitiatedHandler;->handleNiNotification(Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;)V

    #@167
    .line 1974
    iget-object v7, p0, Lcom/android/server/location/GpsLocationProvider;->mSpirentWakeLock2:Landroid/os/PowerManager$WakeLock;

    #@169
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    #@16c
    .line 1975
    const-string v7, "GpsLocationProvider"

    #@16e
    const-string v8, "mSpirentWakeLock2.release"

    #@170
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@173
    .line 1977
    return-void
.end method

.method protected saveXtraDownloadDate()V
    .registers 11

    #@0
    .prologue
    .line 2633
    const/4 v3, 0x0

    #@1
    .line 2635
    .local v3, writer:Ljava/io/FileWriter;
    :try_start_1
    new-instance v2, Ljava/io/File;

    #@3
    const-string v5, "/data/ext_gps.conf"

    #@5
    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8
    .line 2636
    .local v2, file:Ljava/io/File;
    new-instance v4, Ljava/io/FileWriter;

    #@a
    invoke-direct {v4, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_45
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_d} :catch_2e

    #@d
    .line 2637
    .end local v3           #writer:Ljava/io/FileWriter;
    .local v4, writer:Ljava/io/FileWriter;
    :try_start_d
    const-string v5, "%s=%d\n"

    #@f
    const/4 v6, 0x2

    #@10
    new-array v6, v6, [Ljava/lang/Object;

    #@12
    const/4 v7, 0x0

    #@13
    const-string v8, "XTRA_DOWNLOAD_DATE"

    #@15
    aput-object v8, v6, v7

    #@17
    const/4 v7, 0x1

    #@18
    sget-wide v8, Lcom/android/server/location/GpsLocationProvider;->mXtraDownloadDate:J

    #@1a
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1d
    move-result-object v8

    #@1e
    aput-object v8, v6, v7

    #@20
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    .line 2639
    .local v0, config:Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    :try_end_27
    .catchall {:try_start_d .. :try_end_27} :catchall_5e
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_27} :catch_61

    #@27
    .line 2643
    if-eqz v4, :cond_2c

    #@29
    .line 2644
    :try_start_29
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_2c} :catch_55

    #@2c
    :cond_2c
    :goto_2c
    move-object v3, v4

    #@2d
    .line 2648
    .end local v0           #config:Ljava/lang/String;
    .end local v2           #file:Ljava/io/File;
    .end local v4           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 2640
    :catch_2e
    move-exception v1

    #@2f
    .line 2641
    .local v1, e:Ljava/io/IOException;
    :goto_2f
    :try_start_2f
    const-string v5, "GpsLocationProvider"

    #@31
    const-string v6, "ext_gps.conf File save Fail"

    #@33
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_36
    .catchall {:try_start_2f .. :try_end_36} :catchall_45

    #@36
    .line 2643
    if-eqz v3, :cond_2d

    #@38
    .line 2644
    :try_start_38
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_3b
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_3b} :catch_3c

    #@3b
    goto :goto_2d

    #@3c
    .line 2645
    :catch_3c
    move-exception v1

    #@3d
    const-string v5, "GpsLocationProvider"

    #@3f
    const-string v6, "ext_gps.conf File save Fail"

    #@41
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_2d

    #@45
    .line 2643
    .end local v1           #e:Ljava/io/IOException;
    :catchall_45
    move-exception v5

    #@46
    :goto_46
    if-eqz v3, :cond_4b

    #@48
    .line 2644
    :try_start_48
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4b
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_4b} :catch_4c

    #@4b
    .line 2643
    :cond_4b
    :goto_4b
    throw v5

    #@4c
    .line 2645
    :catch_4c
    move-exception v1

    #@4d
    .restart local v1       #e:Ljava/io/IOException;
    const-string v6, "GpsLocationProvider"

    #@4f
    const-string v7, "ext_gps.conf File save Fail"

    #@51
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_4b

    #@55
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v0       #config:Ljava/lang/String;
    .restart local v2       #file:Ljava/io/File;
    .restart local v4       #writer:Ljava/io/FileWriter;
    :catch_55
    move-exception v1

    #@56
    .restart local v1       #e:Ljava/io/IOException;
    const-string v5, "GpsLocationProvider"

    #@58
    const-string v6, "ext_gps.conf File save Fail"

    #@5a
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_2c

    #@5e
    .line 2643
    .end local v0           #config:Ljava/lang/String;
    .end local v1           #e:Ljava/io/IOException;
    :catchall_5e
    move-exception v5

    #@5f
    move-object v3, v4

    #@60
    .end local v4           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    goto :goto_46

    #@61
    .line 2640
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v4       #writer:Ljava/io/FileWriter;
    :catch_61
    move-exception v1

    #@62
    move-object v3, v4

    #@63
    .end local v4           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    goto :goto_2f
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 9
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    .line 1119
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 1120
    .local v0, identity:J
    const/4 v2, 0x0

    #@5
    .line 1122
    .local v2, result:Z
    const-string v3, "delete_aiding_data"

    #@7
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_15

    #@d
    .line 1123
    invoke-direct {p0, p2}, Lcom/android/server/location/GpsLocationProvider;->deleteAidingData(Landroid/os/Bundle;)Z

    #@10
    move-result v2

    #@11
    .line 1136
    :cond_11
    :goto_11
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    .line 1137
    return v2

    #@15
    .line 1124
    :cond_15
    const-string v3, "force_time_injection"

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_25

    #@1d
    .line 1125
    const/4 v3, 0x5

    #@1e
    const/4 v4, 0x0

    #@1f
    const/4 v5, 0x0

    #@20
    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@23
    .line 1126
    const/4 v2, 0x1

    #@24
    goto :goto_11

    #@25
    .line 1127
    :cond_25
    const-string v3, "force_xtra_injection"

    #@27
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v3

    #@2b
    if-eqz v3, :cond_36

    #@2d
    .line 1128
    iget-boolean v3, p0, Lcom/android/server/location/GpsLocationProvider;->mSupportsXtra:Z

    #@2f
    if-eqz v3, :cond_11

    #@31
    .line 1129
    invoke-direct {p0}, Lcom/android/server/location/GpsLocationProvider;->xtraDownloadRequest()V

    #@34
    .line 1130
    const/4 v2, 0x1

    #@35
    goto :goto_11

    #@36
    .line 1133
    :cond_36
    const-string v3, "GpsLocationProvider"

    #@38
    new-instance v4, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v5, "sendExtraCommand: unknown command "

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_11
.end method

.method public setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 6
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    .line 1006
    const/4 v0, 0x3

    #@1
    const/4 v1, 0x0

    #@2
    new-instance v2, Lcom/android/server/location/GpsLocationProvider$GpsRequest;

    #@4
    invoke-direct {v2, p1, p2}, Lcom/android/server/location/GpsLocationProvider$GpsRequest;-><init>(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V

    #@7
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@a
    .line 1007
    return-void
.end method

.method public switchUser(I)V
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 1012
    return-void
.end method

.method public updateNetworkState(ILandroid/net/NetworkInfo;)V
    .registers 4
    .parameter "state"
    .parameter "info"

    #@0
    .prologue
    .line 662
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@4
    .line 663
    return-void
.end method
