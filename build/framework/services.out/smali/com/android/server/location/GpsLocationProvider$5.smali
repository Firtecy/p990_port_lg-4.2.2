.class Lcom/android/server/location/GpsLocationProvider$5;
.super Ljava/lang/Object;
.source "GpsLocationProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/GpsLocationProvider;->handleInjectNtpTime()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 778
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$5;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    #@0
    .prologue
    .line 797
    new-instance v6, Landroid/net/SntpClient;

    #@2
    invoke-direct {v6}, Landroid/net/SntpClient;-><init>()V

    #@5
    .line 798
    .local v6, client:Landroid/net/SntpClient;
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$5;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@7
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$2500(Lcom/android/server/location/GpsLocationProvider;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    const/16 v11, 0x2710

    #@d
    invoke-virtual {v6, v0, v11}, Landroid/net/SntpClient;->requestTime(Ljava/lang/String;I)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_9b

    #@13
    .line 799
    invoke-virtual {v6}, Landroid/net/SntpClient;->getNtpTime()J

    #@16
    move-result-wide v1

    #@17
    .line 800
    .local v1, time:J
    invoke-virtual {v6}, Landroid/net/SntpClient;->getNtpTimeReference()J

    #@1a
    move-result-wide v3

    #@1b
    .line 801
    .local v3, timeReference:J
    invoke-virtual {v6}, Landroid/net/SntpClient;->getRoundTripTime()J

    #@1e
    move-result-wide v11

    #@1f
    const-wide/16 v13, 0x2

    #@21
    div-long/2addr v11, v13

    #@22
    long-to-int v5, v11

    #@23
    .line 803
    .local v5, certainty:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@26
    move-result-wide v9

    #@27
    .line 805
    .local v9, now:J
    const-string v0, "GpsLocationProvider"

    #@29
    new-instance v11, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v12, "NTP server returned: "

    #@30
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v11

    #@34
    invoke-virtual {v11, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@37
    move-result-object v11

    #@38
    const-string v12, " ("

    #@3a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v11

    #@3e
    new-instance v12, Ljava/util/Date;

    #@40
    invoke-direct {v12, v1, v2}, Ljava/util/Date;-><init>(J)V

    #@43
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v11

    #@47
    const-string v12, ") reference: "

    #@49
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v11

    #@4d
    invoke-virtual {v11, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@50
    move-result-object v11

    #@51
    const-string v12, " certainty: "

    #@53
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v11

    #@57
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v11

    #@5b
    const-string v12, " system time offset: "

    #@5d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v11

    #@61
    sub-long v12, v1, v9

    #@63
    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@66
    move-result-object v11

    #@67
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v11

    #@6b
    invoke-static {v0, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 811
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$5;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@70
    invoke-static/range {v0 .. v5}, Lcom/android/server/location/GpsLocationProvider;->access$2600(Lcom/android/server/location/GpsLocationProvider;JJI)V

    #@73
    .line 812
    const-wide/32 v7, 0x5265c00

    #@76
    .line 818
    .end local v1           #time:J
    .end local v3           #timeReference:J
    .end local v5           #certainty:I
    .end local v9           #now:J
    .local v7, delay:J
    :goto_76
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$5;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@78
    const/16 v11, 0xa

    #@7a
    const/4 v12, 0x0

    #@7b
    const/4 v13, 0x0

    #@7c
    invoke-static {v0, v11, v12, v13}, Lcom/android/server/location/GpsLocationProvider;->access$2700(Lcom/android/server/location/GpsLocationProvider;IILjava/lang/Object;)V

    #@7f
    .line 820
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$5;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@81
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$2800(Lcom/android/server/location/GpsLocationProvider;)Z

    #@84
    move-result v0

    #@85
    if-eqz v0, :cond_91

    #@87
    .line 823
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$5;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@89
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$800(Lcom/android/server/location/GpsLocationProvider;)Landroid/os/Handler;

    #@8c
    move-result-object v0

    #@8d
    const/4 v11, 0x5

    #@8e
    invoke-virtual {v0, v11, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@91
    .line 827
    :cond_91
    iget-object v0, p0, Lcom/android/server/location/GpsLocationProvider$5;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@93
    invoke-static {v0}, Lcom/android/server/location/GpsLocationProvider;->access$2900(Lcom/android/server/location/GpsLocationProvider;)Landroid/os/PowerManager$WakeLock;

    #@96
    move-result-object v0

    #@97
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@9a
    .line 828
    return-void

    #@9b
    .line 814
    .end local v7           #delay:J
    :cond_9b
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@9e
    move-result v0

    #@9f
    if-eqz v0, :cond_a8

    #@a1
    const-string v0, "GpsLocationProvider"

    #@a3
    const-string v11, "requestTime failed"

    #@a5
    invoke-static {v0, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 815
    :cond_a8
    const-wide/32 v7, 0x493e0

    #@ab
    .restart local v7       #delay:J
    goto :goto_76
.end method
