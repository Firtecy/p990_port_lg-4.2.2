.class Lcom/android/server/location/GpsLocationProvider$4;
.super Landroid/content/BroadcastReceiver;
.source "GpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 590
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 593
    const/4 v4, 0x0

    #@1
    .line 594
    .local v4, ssid:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 596
    .local v0, action:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@7
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider;->access$900(Lcom/android/server/location/GpsLocationProvider;)Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@a
    move-result-object v6

    #@b
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1000(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@e
    move-result v6

    #@f
    const/4 v7, 0x1

    #@10
    if-ne v6, v7, :cond_51

    #@12
    .line 597
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@14
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider;->access$900(Lcom/android/server/location/GpsLocationProvider;)Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@17
    move-result-object v6

    #@18
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1100(Lcom/android/server/location/GpsLocationProvider$WifiState;)Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    .line 605
    :goto_1c
    const-string v6, "android.net.wifi.SCAN_RESULTS"

    #@1e
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v6

    #@22
    if-eqz v6, :cond_50

    #@24
    .line 606
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@26
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider;->access$900(Lcom/android/server/location/GpsLocationProvider;)Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@29
    move-result-object v6

    #@2a
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1300(Lcom/android/server/location/GpsLocationProvider$WifiState;)Landroid/net/wifi/WifiManager;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    #@31
    move-result-object v3

    #@32
    .line 607
    .local v3, results:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@35
    move-result-object v1

    #@36
    .local v1, i$:Ljava/util/Iterator;
    :cond_36
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@39
    move-result v6

    #@3a
    if-eqz v6, :cond_69

    #@3c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3f
    move-result-object v2

    #@40
    check-cast v2, Landroid/net/wifi/ScanResult;

    #@42
    .line 608
    .local v2, result:Landroid/net/wifi/ScanResult;
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@44
    iget-object v7, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    #@46
    invoke-static {v6, v7}, Lcom/android/server/location/GpsLocationProvider;->access$1400(Lcom/android/server/location/GpsLocationProvider;Ljava/lang/String;)Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    .line 609
    .local v5, ssid_result:Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v6

    #@4e
    if-eqz v6, :cond_36

    #@50
    .line 616
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #result:Landroid/net/wifi/ScanResult;
    .end local v3           #results:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    .end local v5           #ssid_result:Ljava/lang/String;
    :cond_50
    :goto_50
    return-void

    #@51
    .line 598
    :cond_51
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@53
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider;->access$900(Lcom/android/server/location/GpsLocationProvider;)Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@56
    move-result-object v6

    #@57
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1000(Lcom/android/server/location/GpsLocationProvider$WifiState;)I

    #@5a
    move-result v6

    #@5b
    const/4 v7, 0x3

    #@5c
    if-ne v6, v7, :cond_50

    #@5e
    .line 599
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@60
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider;->access$900(Lcom/android/server/location/GpsLocationProvider;)Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@63
    move-result-object v6

    #@64
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1200(Lcom/android/server/location/GpsLocationProvider$WifiState;)Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    goto :goto_1c

    #@69
    .line 614
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #results:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_69
    iget-object v6, p0, Lcom/android/server/location/GpsLocationProvider$4;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@6b
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider;->access$900(Lcom/android/server/location/GpsLocationProvider;)Lcom/android/server/location/GpsLocationProvider$WifiState;

    #@6e
    move-result-object v6

    #@6f
    invoke-static {v6}, Lcom/android/server/location/GpsLocationProvider$WifiState;->access$1500(Lcom/android/server/location/GpsLocationProvider$WifiState;)V

    #@72
    goto :goto_50
.end method
