.class Lcom/android/server/location/GpsLocationProvider$6;
.super Ljava/lang/Object;
.source "GpsLocationProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/GpsLocationProvider;->handleDownloadXtraData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 851
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 854
    new-instance v1, Lcom/android/server/location/GpsXtraDownloader;

    #@2
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@4
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$600(Lcom/android/server/location/GpsLocationProvider;)Landroid/content/Context;

    #@7
    move-result-object v2

    #@8
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@a
    invoke-static {v3}, Lcom/android/server/location/GpsLocationProvider;->access$3000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/Properties;

    #@d
    move-result-object v3

    #@e
    invoke-direct {v1, v2, v3}, Lcom/android/server/location/GpsXtraDownloader;-><init>(Landroid/content/Context;Ljava/util/Properties;)V

    #@11
    .line 855
    .local v1, xtraDownloader:Lcom/android/server/location/GpsXtraDownloader;
    invoke-virtual {v1}, Lcom/android/server/location/GpsXtraDownloader;->downloadXtraData()[B

    #@14
    move-result-object v0

    #@15
    .line 856
    .local v0, data:[B
    if-eqz v0, :cond_2a

    #@17
    .line 857
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_24

    #@1d
    .line 858
    const-string v2, "GpsLocationProvider"

    #@1f
    const-string v3, "calling native_inject_xtra_data"

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 860
    :cond_24
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@26
    array-length v3, v0

    #@27
    invoke-static {v2, v0, v3}, Lcom/android/server/location/GpsLocationProvider;->access$3100(Lcom/android/server/location/GpsLocationProvider;[BI)V

    #@2a
    .line 863
    :cond_2a
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2c
    const/16 v3, 0xb

    #@2e
    const/4 v4, 0x0

    #@2f
    const/4 v5, 0x0

    #@30
    invoke-static {v2, v3, v4, v5}, Lcom/android/server/location/GpsLocationProvider;->access$2700(Lcom/android/server/location/GpsLocationProvider;IILjava/lang/Object;)V

    #@33
    .line 865
    if-nez v0, :cond_42

    #@35
    .line 868
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@37
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$800(Lcom/android/server/location/GpsLocationProvider;)Landroid/os/Handler;

    #@3a
    move-result-object v2

    #@3b
    const/4 v3, 0x6

    #@3c
    const-wide/32 v4, 0x493e0

    #@3f
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@42
    .line 873
    :cond_42
    const-string v2, "LGU"

    #@44
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$3200()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v2

    #@4c
    if-eqz v2, :cond_53

    #@4e
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@50
    invoke-static {v2, v0}, Lcom/android/server/location/GpsLocationProvider;->access$3300(Lcom/android/server/location/GpsLocationProvider;[B)V

    #@53
    .line 877
    :cond_53
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$6;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@55
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$2900(Lcom/android/server/location/GpsLocationProvider;)Landroid/os/PowerManager$WakeLock;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@5c
    .line 878
    return-void
.end method
