.class Lcom/android/server/location/LgeGpsLocationProvider$1;
.super Landroid/content/BroadcastReceiver;
.source "LgeGpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/LgeGpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/LgeGpsLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/server/location/LgeGpsLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 182
    iput-object p1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    .line 184
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 186
    .local v0, action:Ljava/lang/String;
    const-string v1, "com.skt.intent.action"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_9d

    #@f
    const-string v1, "SKT"

    #@11
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$000()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_9d

    #@1b
    .line 188
    const-string v1, "LgeGpsLocationProvider"

    #@1d
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "Intent - "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 189
    const-string v1, "com.skt.intent.action.AGPS_ON"

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v1

    #@39
    if-nez v1, :cond_43

    #@3b
    const-string v1, "com.skt.intent.action.GPS_TURN_ON"

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v1

    #@41
    if-eqz v1, :cond_6b

    #@43
    .line 190
    :cond_43
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@45
    invoke-static {v1, v4}, Lcom/android/server/location/LgeGpsLocationProvider;->access$102(Lcom/android/server/location/LgeGpsLocationProvider;Z)Z

    #@48
    .line 191
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@4a
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@4c
    invoke-static {v2}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@53
    move-result-object v2

    #@54
    const-string v3, "gps"

    #@56
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@59
    move-result v2

    #@5a
    invoke-static {v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->access$202(Lcom/android/server/location/LgeGpsLocationProvider;Z)Z

    #@5d
    .line 193
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@5f
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$200(Lcom/android/server/location/LgeGpsLocationProvider;)Z

    #@62
    move-result v1

    #@63
    if-nez v1, :cond_6a

    #@65
    .line 194
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@67
    invoke-static {v1, v4, v4, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->access$400(Lcom/android/server/location/LgeGpsLocationProvider;IILjava/lang/Object;)V

    #@6a
    .line 242
    :cond_6a
    :goto_6a
    return-void

    #@6b
    .line 197
    :cond_6b
    const-string v1, "com.skt.intent.action.AGPS_OFF"

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v1

    #@71
    if-nez v1, :cond_7b

    #@73
    const-string v1, "com.skt.intent.action.GPS_TURN_OFF"

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@78
    move-result v1

    #@79
    if-eqz v1, :cond_6a

    #@7b
    .line 198
    :cond_7b
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@7d
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$100(Lcom/android/server/location/LgeGpsLocationProvider;)Z

    #@80
    move-result v1

    #@81
    if-ne v1, v4, :cond_97

    #@83
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@85
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$200(Lcom/android/server/location/LgeGpsLocationProvider;)Z

    #@88
    move-result v1

    #@89
    if-nez v1, :cond_97

    #@8b
    .line 199
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@8d
    invoke-static {v1, v4, v5, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->access$400(Lcom/android/server/location/LgeGpsLocationProvider;IILjava/lang/Object;)V

    #@90
    .line 200
    const-string v1, "LgeGpsLocationProvider"

    #@92
    const-string v2, "settings - LocationProvider = OFF"

    #@94
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 202
    :cond_97
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@99
    invoke-static {v1, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->access$102(Lcom/android/server/location/LgeGpsLocationProvider;Z)Z

    #@9c
    goto :goto_6a

    #@9d
    .line 206
    :cond_9d
    const-string v1, "com.android.lge.action_at_timeout_uplus"

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a2
    move-result v1

    #@a3
    if-eqz v1, :cond_b2

    #@a5
    .line 207
    const-string v1, "LgeGpsLocationProvider"

    #@a7
    const-string v2, "RIL recovery progress!"

    #@a9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 208
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@ae
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$500(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@b1
    goto :goto_6a

    #@b2
    .line 211
    :cond_b2
    const-string v1, "VDF"

    #@b4
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$000()Ljava/lang/String;

    #@b7
    move-result-object v2

    #@b8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bb
    move-result v1

    #@bc
    if-nez v1, :cond_ca

    #@be
    const-string v1, "SFR"

    #@c0
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$000()Ljava/lang/String;

    #@c3
    move-result-object v2

    #@c4
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c7
    move-result v1

    #@c8
    if-eqz v1, :cond_df

    #@ca
    :cond_ca
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    #@cc
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cf
    move-result v1

    #@d0
    if-eqz v1, :cond_df

    #@d2
    .line 212
    const-string v1, "LgeGpsLocationProvider"

    #@d4
    const-string v2, "TelephonyIntents.ACTION_SIM_STATE_CHANGED"

    #@d6
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 213
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@db
    invoke-virtual {v1, p2}, Lcom/android/server/location/LgeGpsLocationProvider;->lgeUpdateSimState(Landroid/content/Intent;)V

    #@de
    goto :goto_6a

    #@df
    .line 216
    :cond_df
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    #@e1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e4
    move-result v1

    #@e5
    if-eqz v1, :cond_160

    #@e7
    const-string v1, "ATT"

    #@e9
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$000()Ljava/lang/String;

    #@ec
    move-result-object v2

    #@ed
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f0
    move-result v1

    #@f1
    if-eqz v1, :cond_160

    #@f3
    .line 217
    const-string v1, "LgeGpsLocationProvider"

    #@f5
    new-instance v2, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v3, "ACTION_AIRPLANE_MODE_CHANGED received, operator= ATT, mSettingGpsEnable ="

    #@fc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v2

    #@100
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$600()Z

    #@103
    move-result v3

    #@104
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@107
    move-result-object v2

    #@108
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v2

    #@10c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10f
    .line 218
    const-string v1, "state"

    #@111
    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@114
    move-result v1

    #@115
    if-ne v1, v4, :cond_153

    #@117
    .line 219
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@119
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@11c
    move-result-object v1

    #@11d
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@120
    move-result-object v1

    #@121
    const-string v2, "gps"

    #@123
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@126
    move-result v1

    #@127
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$602(Z)Z

    #@12a
    .line 220
    const-string v1, "LgeGpsLocationProvider"

    #@12c
    new-instance v2, Ljava/lang/StringBuilder;

    #@12e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@131
    const-string v3, "Air plane ON!!! , mSettingGpsEnable ="

    #@133
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v2

    #@137
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$600()Z

    #@13a
    move-result v3

    #@13b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v2

    #@13f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v2

    #@143
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 221
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$600()Z

    #@149
    move-result v1

    #@14a
    if-ne v1, v4, :cond_6a

    #@14c
    .line 222
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@14e
    invoke-static {v1, v4, v5, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->access$400(Lcom/android/server/location/LgeGpsLocationProvider;IILjava/lang/Object;)V

    #@151
    goto/16 :goto_6a

    #@153
    .line 226
    :cond_153
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$600()Z

    #@156
    move-result v1

    #@157
    if-ne v1, v4, :cond_6a

    #@159
    .line 228
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@15b
    invoke-static {v1, v4, v4, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->access$400(Lcom/android/server/location/LgeGpsLocationProvider;IILjava/lang/Object;)V

    #@15e
    goto/16 :goto_6a

    #@160
    .line 233
    :cond_160
    const-string v1, "DCM"

    #@162
    invoke-static {}, Lcom/android/server/location/LgeGpsLocationProvider;->access$000()Ljava/lang/String;

    #@165
    move-result-object v2

    #@166
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@169
    move-result v1

    #@16a
    if-eqz v1, :cond_6a

    #@16c
    .line 235
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    #@16e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@171
    move-result v1

    #@172
    if-eqz v1, :cond_17b

    #@174
    .line 236
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@176
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$700(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@179
    goto/16 :goto_6a

    #@17b
    .line 238
    :cond_17b
    const-string v1, "com.android.settings.gpsnotification.CHANGED"

    #@17d
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@180
    move-result v1

    #@181
    if-eqz v1, :cond_6a

    #@183
    .line 239
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$1;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@185
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$700(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@188
    goto/16 :goto_6a
.end method
