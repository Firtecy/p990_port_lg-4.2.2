.class Lcom/android/server/location/LocationBasedCountryDetector$3;
.super Ljava/lang/Object;
.source "LocationBasedCountryDetector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/LocationBasedCountryDetector;->queryCountryCode(Landroid/location/Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/LocationBasedCountryDetector;

.field final synthetic val$location:Landroid/location/Location;


# direct methods
.method constructor <init>(Lcom/android/server/location/LocationBasedCountryDetector;Landroid/location/Location;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->this$0:Lcom/android/server/location/LocationBasedCountryDetector;

    #@2
    iput-object p2, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->val$location:Landroid/location/Location;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 230
    const/4 v0, 0x0

    #@2
    .line 231
    .local v0, countryIso:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->val$location:Landroid/location/Location;

    #@4
    if-eqz v1, :cond_e

    #@6
    .line 232
    iget-object v1, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->this$0:Lcom/android/server/location/LocationBasedCountryDetector;

    #@8
    iget-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->val$location:Landroid/location/Location;

    #@a
    invoke-virtual {v1, v2}, Lcom/android/server/location/LocationBasedCountryDetector;->getCountryFromLocation(Landroid/location/Location;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 234
    :cond_e
    if-eqz v0, :cond_28

    #@10
    .line 235
    iget-object v1, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->this$0:Lcom/android/server/location/LocationBasedCountryDetector;

    #@12
    new-instance v2, Landroid/location/Country;

    #@14
    const/4 v3, 0x1

    #@15
    invoke-direct {v2, v0, v3}, Landroid/location/Country;-><init>(Ljava/lang/String;I)V

    #@18
    iput-object v2, v1, Lcom/android/server/location/CountryDetectorBase;->mDetectedCountry:Landroid/location/Country;

    #@1a
    .line 239
    :goto_1a
    iget-object v1, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->this$0:Lcom/android/server/location/LocationBasedCountryDetector;

    #@1c
    iget-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->this$0:Lcom/android/server/location/LocationBasedCountryDetector;

    #@1e
    iget-object v2, v2, Lcom/android/server/location/CountryDetectorBase;->mDetectedCountry:Landroid/location/Country;

    #@20
    invoke-virtual {v1, v2}, Lcom/android/server/location/LocationBasedCountryDetector;->notifyListener(Landroid/location/Country;)V

    #@23
    .line 240
    iget-object v1, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->this$0:Lcom/android/server/location/LocationBasedCountryDetector;

    #@25
    iput-object v4, v1, Lcom/android/server/location/LocationBasedCountryDetector;->mQueryThread:Ljava/lang/Thread;

    #@27
    .line 241
    return-void

    #@28
    .line 237
    :cond_28
    iget-object v1, p0, Lcom/android/server/location/LocationBasedCountryDetector$3;->this$0:Lcom/android/server/location/LocationBasedCountryDetector;

    #@2a
    iput-object v4, v1, Lcom/android/server/location/CountryDetectorBase;->mDetectedCountry:Landroid/location/Country;

    #@2c
    goto :goto_1a
.end method
