.class public Lcom/android/server/location/LgeGpsLocationProvider;
.super Lcom/android/server/location/GpsLocationProvider;
.source "LgeGpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;,
        Lcom/android/server/location/LgeGpsLocationProvider$LgeGpsLocationProviderThread;,
        Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;
    }
.end annotation


# static fields
.field private static final ACTION_SET_USE_LOCATION_FOR_SERVICES:Ljava/lang/String; = "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

.field private static final AGPS_TYPE_SUPL:I = 0x1

.field private static final DEBUG:Z = true

.field public static final EXTRA_DISABLE_USE_LOCATION_FOR_SERVICES:Ljava/lang/String; = "disable"

.field private static final GNSS_LOCK:I = 0x1

.field private static final GNSS_LOCK_NOT_SET:I = 0x2

.field private static final GNSS_UNLOCK:I = 0x0

.field private static final GPS_CAPABILITY_MSA:I = 0x4

.field private static final GPS_CAPABILITY_MSB:I = 0x2

.field private static final GPS_CAPABILITY_SCHEDULING:I = 0x1

.field private static final GPS_CAPABILITY_SINGLE_SHOT:I = 0x8

.field public static final GPS_DISABLED:I = 0x1

.field public static final GPS_ENABLED:I = 0x0

.field public static final GPS_FORCED:I = 0x2

.field private static final GPS_POSITION_MODE_MS_ASSISTED:I = 0x2

.field private static final GPS_POSITION_MODE_MS_BASED:I = 0x1

.field private static final GPS_POSITION_MODE_STANDALONE:I = 0x0

.field private static INTENT_XTRA_SETTING_CHANGED:Ljava/lang/String; = null

.field private static final LGE_GNSS_CONF_FILE:Ljava/lang/String; = "/data/ext_gps.conf"

.field private static final MESSAGE_ASSISTED_GPS_ENABLE:I = 0x2

.field private static final MESSAGE_GPS_PROVIDER_ENABLE:I = 0x1

.field private static final PROPERTIES_FILE:Ljava/lang/String; = "/etc/gps.conf"

.field private static final TAG:Ljava/lang/String; = "LgeGpsLocationProvider"

.field private static final XTRA_SHOW_MESSAGE:I = 0xa

.field private static final lge_xtra_download_start_msg:[Ljava/lang/String;

.field private static mLgeSuplServerHost:Ljava/lang/String;

.field private static mSettingGpsEnable:Z

.field private static mVendorName:Ljava/lang/String;

.field private static mXtraDownloadFrequency:I


# instance fields
.field private final LED_GPS_ID:I

.field private isGpsRecorveryMode:Z

.field private mExtProperties:Ljava/util/Properties;

.field private mGPSSettingEnabled:Z

.field private mGPSSettingObserver:Landroid/database/ContentObserver;

.field private mGPSSoundPlayer:Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;

.field private final mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

.field private mLedOn:Z

.field private final mLgeBroadcastReciever:Landroid/content/BroadcastReceiver;

.field private final mLgeContext:Landroid/content/Context;

.field private mLgeHandler:Landroid/os/Handler;

.field private mLgeProperties:Ljava/util/Properties;

.field private mLgeSuplServerPort:I

.field private mLgeSuplType:I

.field private mLgeTlsMode:I

.field private mLocationPrivacyObserver:Landroid/database/ContentObserver;

.field private mNotificationObserver:Landroid/database/ContentObserver;

.field private mPositionModeSetted:Z

.field private mSKTTmapStarted:Z

.field private mSoundOn:Z

.field private final mThread:Ljava/lang/Thread;

.field mToastText:Ljava/lang/CharSequence;

.field private mTracking:Z

.field private mVibratorOn:Z

.field private mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

.field private nm:Landroid/app/NotificationManager;

.field private notification:Landroid/app/Notification;

.field private oldGnssLockMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 123
    const-string v0, ""

    #@3
    sput-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@5
    .line 138
    sput-boolean v2, Lcom/android/server/location/LgeGpsLocationProvider;->mSettingGpsEnable:Z

    #@7
    .line 146
    const-string v0, "com.lge.location.xtra_setting_changed"

    #@9
    sput-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->INTENT_XTRA_SETTING_CHANGED:Ljava/lang/String;

    #@b
    .line 150
    const/16 v0, 0x5a0

    #@d
    sput v0, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@f
    .line 154
    const/4 v0, 0x2

    #@10
    new-array v0, v0, [Ljava/lang/String;

    #@12
    const-string v1, "GPS \ubcf4\uc870\ub370\uc774\ud130\uac00 \ub2e4\uc6b4\ub85c\ub4dc \ub429\ub2c8\ub2e4."

    #@14
    aput-object v1, v0, v2

    #@16
    const/4 v1, 0x1

    #@17
    const-string v2, "Downloading assisted GPS data."

    #@19
    aput-object v2, v0, v1

    #@1b
    sput-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->lge_xtra_download_start_msg:[Ljava/lang/String;

    #@1d
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/location/ILocationManager;)V
    .registers 12
    .parameter "context"
    .parameter "locationManager"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 253
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;-><init>(Landroid/content/Context;Landroid/location/ILocationManager;)V

    #@5
    .line 111
    const/4 v3, 0x2

    #@6
    iput v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->oldGnssLockMode:I

    #@8
    .line 119
    iput v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplType:I

    #@a
    .line 131
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    #@c
    invoke-direct {v3, v8}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@f
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    #@11
    .line 137
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionModeSetted:Z

    #@13
    .line 139
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSKTTmapStarted:Z

    #@15
    .line 140
    iput-boolean v8, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSettingEnabled:Z

    #@17
    .line 144
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->isGpsRecorveryMode:Z

    #@19
    .line 172
    new-instance v3, Landroid/app/Notification;

    #@1b
    const-string v4, "LED_GPS"

    #@1d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@20
    move-result-wide v5

    #@21
    invoke-direct {v3, v7, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@24
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@26
    .line 173
    const/4 v3, 0x7

    #@27
    iput v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->LED_GPS_ID:I

    #@29
    .line 176
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSoundOn:Z

    #@2b
    .line 177
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVibratorOn:Z

    #@2d
    .line 178
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@2f
    .line 179
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@31
    .line 182
    new-instance v3, Lcom/android/server/location/LgeGpsLocationProvider$1;

    #@33
    invoke-direct {v3, p0}, Lcom/android/server/location/LgeGpsLocationProvider$1;-><init>(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@36
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeBroadcastReciever:Landroid/content/BroadcastReceiver;

    #@38
    .line 245
    new-instance v3, Lcom/android/server/location/LgeGpsLocationProvider$2;

    #@3a
    new-instance v4, Landroid/os/Handler;

    #@3c
    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    #@3f
    invoke-direct {v3, p0, v4}, Lcom/android/server/location/LgeGpsLocationProvider$2;-><init>(Lcom/android/server/location/LgeGpsLocationProvider;Landroid/os/Handler;)V

    #@42
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mNotificationObserver:Landroid/database/ContentObserver;

    #@44
    .line 1015
    new-instance v3, Lcom/android/server/location/LgeGpsLocationProvider$3;

    #@46
    new-instance v4, Landroid/os/Handler;

    #@48
    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    #@4b
    invoke-direct {v3, p0, v4}, Lcom/android/server/location/LgeGpsLocationProvider$3;-><init>(Lcom/android/server/location/LgeGpsLocationProvider;Landroid/os/Handler;)V

    #@4e
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLocationPrivacyObserver:Landroid/database/ContentObserver;

    #@50
    .line 1368
    new-instance v3, Lcom/android/server/location/LgeGpsLocationProvider$4;

    #@52
    new-instance v4, Landroid/os/Handler;

    #@54
    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    #@57
    invoke-direct {v3, p0, v4}, Lcom/android/server/location/LgeGpsLocationProvider$4;-><init>(Lcom/android/server/location/LgeGpsLocationProvider;Landroid/os/Handler;)V

    #@5a
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSettingObserver:Landroid/database/ContentObserver;

    #@5c
    .line 255
    const-string v3, "LgeGpsLocationProvider"

    #@5e
    const-string v4, "LgeGpsLocationProvider"

    #@60
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 256
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->loadConfig()V

    #@66
    .line 258
    iput-object p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@68
    .line 259
    new-instance v2, Landroid/content/IntentFilter;

    #@6a
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@6d
    .line 262
    .local v2, intentFilter:Landroid/content/IntentFilter;
    const-string v3, "SKT"

    #@6f
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@71
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v3

    #@75
    if-eqz v3, :cond_8b

    #@77
    .line 263
    const-string v3, "com.skt.intent.action.AGPS_ON"

    #@79
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@7c
    .line 264
    const-string v3, "com.skt.intent.action.AGPS_OFF"

    #@7e
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@81
    .line 265
    const-string v3, "com.skt.intent.action.GPS_TURN_ON"

    #@83
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@86
    .line 266
    const-string v3, "com.skt.intent.action.GPS_TURN_OFF"

    #@88
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8b
    .line 269
    :cond_8b
    const-string v3, "LGU"

    #@8d
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@8f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v3

    #@93
    if-eqz v3, :cond_9a

    #@95
    .line 270
    const-string v3, "com.android.lge.action_at_timeout_uplus"

    #@97
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9a
    .line 273
    :cond_9a
    const-string v3, "ATT"

    #@9c
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@9e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1
    move-result v3

    #@a2
    if-eqz v3, :cond_a9

    #@a4
    .line 274
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    #@a6
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a9
    .line 278
    :cond_a9
    const-string v3, "VDF"

    #@ab
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@ad
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b0
    move-result v3

    #@b1
    if-nez v3, :cond_bd

    #@b3
    const-string v3, "SFR"

    #@b5
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@b7
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ba
    move-result v3

    #@bb
    if-eqz v3, :cond_c2

    #@bd
    .line 279
    :cond_bd
    const-string v3, "android.intent.action.SIM_STATE_CHANGED"

    #@bf
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c2
    .line 283
    :cond_c2
    const-string v3, "DCM"

    #@c4
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@c6
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v3

    #@ca
    if-eqz v3, :cond_125

    #@cc
    .line 284
    const-string v3, "android.media.RINGER_MODE_CHANGED"

    #@ce
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d1
    .line 285
    const-string v3, "com.android.settings.gpsnotification.CHANGED"

    #@d3
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d6
    .line 288
    :try_start_d6
    sget-object v3, Lcom/lge/loader/RuntimeLibraryLoader;->VOLUME_MANAGER:Ljava/lang/String;

    #@d8
    invoke-static {v3}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;

    #@db
    move-result-object v1

    #@dc
    .line 290
    .local v1, ic:Lcom/lge/loader/InstanceCreator;
    if-eqz v1, :cond_19b

    #@de
    .line 291
    invoke-virtual {v1}, Lcom/lge/loader/InstanceCreator;->getDefaultInstance()Ljava/lang/Object;

    #@e1
    move-result-object v3

    #@e2
    check-cast v3, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@e4
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;
    :try_end_e6
    .catch Ljava/lang/NullPointerException; {:try_start_d6 .. :try_end_e6} :catch_1a4
    .catch Ljava/lang/Exception; {:try_start_d6 .. :try_end_e6} :catch_1aa

    #@e6
    .line 303
    .end local v1           #ic:Lcom/lge/loader/InstanceCreator;
    :goto_e6
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@e8
    const-string v4, "notification"

    #@ea
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@ed
    move-result-object v3

    #@ee
    check-cast v3, Landroid/app/NotificationManager;

    #@f0
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->nm:Landroid/app/NotificationManager;

    #@f2
    .line 305
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@f4
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f7
    move-result-object v3

    #@f8
    const-string v4, "lge_notification_light_pulse"

    #@fa
    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@fd
    move-result-object v4

    #@fe
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mNotificationObserver:Landroid/database/ContentObserver;

    #@100
    invoke-virtual {v3, v4, v8, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@103
    .line 307
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@105
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@108
    move-result-object v3

    #@109
    const-string v4, "emotional_led_gps"

    #@10b
    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@10e
    move-result-object v4

    #@10f
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mNotificationObserver:Landroid/database/ContentObserver;

    #@111
    invoke-virtual {v3, v4, v8, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@114
    .line 310
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@116
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@119
    move-result-object v3

    #@11a
    const-string v4, "location_providers_allowed"

    #@11c
    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@11f
    move-result-object v4

    #@120
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSettingObserver:Landroid/database/ContentObserver;

    #@122
    invoke-virtual {v3, v4, v8, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@125
    .line 314
    :cond_125
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@127
    iget-object v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeBroadcastReciever:Landroid/content/BroadcastReceiver;

    #@129
    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@12c
    .line 317
    invoke-virtual {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->native_init()Z

    #@12f
    move-result v3

    #@130
    if-nez v3, :cond_139

    #@132
    .line 318
    const-string v3, "LgeGpsLocationProvider"

    #@134
    const-string v4, "native_init() failed"

    #@136
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@139
    .line 322
    :cond_139
    new-instance v3, Lcom/android/server/location/LgeGpsLocationProvider$LgeGpsLocationProviderThread;

    #@13b
    invoke-direct {v3, p0}, Lcom/android/server/location/LgeGpsLocationProvider$LgeGpsLocationProviderThread;-><init>(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@13e
    iput-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mThread:Ljava/lang/Thread;

    #@140
    .line 323
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mThread:Ljava/lang/Thread;

    #@142
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    #@145
    .line 326
    :goto_145
    :try_start_145
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    #@147
    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_14a
    .catch Ljava/lang/InterruptedException; {:try_start_145 .. :try_end_14a} :catch_1b0

    #@14a
    .line 334
    const-string v3, "VZW"

    #@14c
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@14e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@151
    move-result v3

    #@152
    if-nez v3, :cond_186

    #@154
    const-string v3, "MPCS"

    #@156
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@158
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15b
    move-result v3

    #@15c
    if-nez v3, :cond_186

    #@15e
    const-string v3, "CRK"

    #@160
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@162
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@165
    move-result v3

    #@166
    if-nez v3, :cond_186

    #@168
    const-string v3, "SPR"

    #@16a
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@16c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16f
    move-result v3

    #@170
    if-nez v3, :cond_186

    #@172
    const-string v3, "USC"

    #@174
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@176
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@179
    move-result v3

    #@17a
    if-nez v3, :cond_186

    #@17c
    const-string v3, "ACG"

    #@17e
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@180
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@183
    move-result v3

    #@184
    if-eqz v3, :cond_19a

    #@186
    .line 336
    :cond_186
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->UpdateGnssLockMode()V

    #@189
    .line 337
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@18b
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18e
    move-result-object v3

    #@18f
    const-string v4, "location_providers_allowed"

    #@191
    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@194
    move-result-object v4

    #@195
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLocationPrivacyObserver:Landroid/database/ContentObserver;

    #@197
    invoke-virtual {v3, v4, v8, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@19a
    .line 341
    :cond_19a
    return-void

    #@19b
    .line 294
    .restart local v1       #ic:Lcom/lge/loader/InstanceCreator;
    :cond_19b
    :try_start_19b
    const-string v3, "LgeGpsLocationProvider"

    #@19d
    const-string v4, "cannot get VOLUME_MANAGER from RuntimeLibraryLoader"

    #@19f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a2
    .catch Ljava/lang/NullPointerException; {:try_start_19b .. :try_end_1a2} :catch_1a4
    .catch Ljava/lang/Exception; {:try_start_19b .. :try_end_1a2} :catch_1aa

    #@1a2
    goto/16 :goto_e6

    #@1a4
    .line 297
    .end local v1           #ic:Lcom/lge/loader/InstanceCreator;
    :catch_1a4
    move-exception v0

    #@1a5
    .line 298
    .local v0, e:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@1a8
    goto/16 :goto_e6

    #@1aa
    .line 299
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catch_1aa
    move-exception v0

    #@1ab
    .line 300
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@1ae
    goto/16 :goto_e6

    #@1b0
    .line 328
    .end local v0           #e:Ljava/lang/Exception;
    :catch_1b0
    move-exception v0

    #@1b1
    .line 329
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@1b4
    move-result-object v3

    #@1b5
    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    #@1b8
    goto :goto_145
.end method

.method private LM_getGpsState()I
    .registers 5

    #@0
    .prologue
    .line 1094
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "gps_device_managerment_enabled"

    #@8
    const/4 v3, 0x0

    #@9
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v0

    #@d
    .line 1096
    .local v0, gpsState:I
    const-string v1, "LgeGpsLocationProvider"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "LM_getGpsState return GPS State as"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1097
    return v0
.end method

.method private LM_setGpsState(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1116
    const-string v0, "LgeGpsLocationProvider"

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "LM_setGpsState sets GPS State to"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1119
    packed-switch p1, :pswitch_data_a6

    #@1d
    .line 1167
    :goto_1d
    return-void

    #@1e
    .line 1124
    :pswitch_1e
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@20
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v0

    #@24
    const-string v1, "network"

    #@26
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@29
    .line 1126
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, "gps"

    #@31
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@34
    .line 1128
    invoke-virtual {p0, v3}, Lcom/android/server/location/LgeGpsLocationProvider;->setUseLocationForServices(Z)V

    #@37
    .line 1130
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@39
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3c
    move-result-object v0

    #@3d
    const-string v1, "gps_device_managerment_enabled"

    #@3f
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@42
    goto :goto_1d

    #@43
    .line 1135
    :pswitch_43
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@45
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v0

    #@49
    const-string v1, "network"

    #@4b
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@4e
    .line 1137
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@50
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@53
    move-result-object v0

    #@54
    const-string v1, "gps"

    #@56
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@59
    .line 1139
    invoke-virtual {p0, v3}, Lcom/android/server/location/LgeGpsLocationProvider;->setUseLocationForServices(Z)V

    #@5c
    .line 1141
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@5e
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@61
    move-result-object v0

    #@62
    const-string v1, "gps_device_managerment_enabled"

    #@64
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@67
    goto :goto_1d

    #@68
    .line 1146
    :pswitch_68
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@6a
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6d
    move-result-object v0

    #@6e
    const-string v1, "use_location_for_services"

    #@70
    const-string v2, "1"

    #@72
    invoke-static {v0, v1, v2}, Lcom/lge/provider/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@75
    .line 1147
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@77
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7a
    move-result-object v0

    #@7b
    const-string v1, "network_location_opt_in"

    #@7d
    const-string v2, "1"

    #@7f
    invoke-static {v0, v1, v2}, Lcom/lge/provider/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@82
    .line 1150
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@84
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@87
    move-result-object v0

    #@88
    const-string v1, "network"

    #@8a
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@8d
    .line 1152
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@8f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@92
    move-result-object v0

    #@93
    const-string v1, "gps"

    #@95
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@98
    .line 1158
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@9a
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9d
    move-result-object v0

    #@9e
    const-string v1, "gps_device_managerment_enabled"

    #@a0
    const/4 v2, 0x2

    #@a1
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@a4
    goto/16 :goto_1d

    #@a6
    .line 1119
    :pswitch_data_a6
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_43
        :pswitch_68
    .end packed-switch
.end method

.method private UpdateGnssLockMode()V
    .registers 8

    #@0
    .prologue
    .line 970
    const/4 v3, 0x1

    #@1
    .line 971
    .local v3, ret:Z
    const/4 v0, 0x0

    #@2
    .line 974
    .local v0, GnssLockMode:I
    iget-object v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@4
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v4

    #@8
    const-string v5, "location_providers_allowed"

    #@a
    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 975
    .local v2, loc_provider:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@10
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v4

    #@14
    const-string v5, "gps"

    #@16
    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@19
    move-result v1

    #@1a
    .line 977
    .local v1, gpsEnable:Z
    if-nez v2, :cond_24

    #@1c
    .line 979
    const-string v4, "LgeGpsLocationProvider"

    #@1e
    const-string v5, "loc_provider is null"

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1013
    :goto_23
    return-void

    #@24
    .line 985
    :cond_24
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@26
    const-string v5, "VZW"

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_39

    #@2e
    .line 986
    iget-object v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@30
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v4

    #@34
    const-string v5, "vzw_lbs"

    #@36
    invoke-static {v4, v5, v1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@39
    .line 989
    :cond_39
    const-string v4, "LgeGpsLocationProvider"

    #@3b
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v6, "mLocationPrivacyObserver. , Loc_Provider ="

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 991
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    #@54
    move-result v4

    #@55
    if-nez v4, :cond_63

    #@57
    sget-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@59
    const-string v5, "SPR"

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v4

    #@5f
    if-eqz v4, :cond_64

    #@61
    if-nez v1, :cond_64

    #@63
    .line 992
    :cond_63
    const/4 v0, 0x1

    #@64
    .line 995
    :cond_64
    iget v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->oldGnssLockMode:I

    #@66
    if-eq v0, v4, :cond_84

    #@68
    .line 997
    const/4 v4, 0x1

    #@69
    if-ne v0, v4, :cond_87

    #@6b
    .line 998
    const-string v4, "lge.gps.extracmd.raw.setgnss_lock.enable"

    #@6d
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    #@70
    move-result-object v4

    #@71
    const-string v5, "lge.gps.extracmd.raw.setgnss_lock.enable"

    #@73
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@76
    move-result v5

    #@77
    invoke-virtual {p0, v4, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@7a
    move-result v3

    #@7b
    .line 1008
    :goto_7b
    if-nez v3, :cond_84

    #@7d
    .line 1009
    const-string v4, "LgeGpsLocationProvider"

    #@7f
    const-string v5, "lgeGnssLockMode fail"

    #@81
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1011
    :cond_84
    iput v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->oldGnssLockMode:I

    #@86
    goto :goto_23

    #@87
    .line 1003
    :cond_87
    const-string v4, "lge.gps.extracmd.raw.setgnss_lock.disable"

    #@89
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    #@8c
    move-result-object v4

    #@8d
    const-string v5, "lge.gps.extracmd.raw.setgnss_lock.disable"

    #@8f
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@92
    move-result v5

    #@93
    invoke-virtual {p0, v4, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@96
    move-result v3

    #@97
    goto :goto_7b
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 88
    sget-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/location/LgeGpsLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSKTTmapStarted:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/server/location/LgeGpsLocationProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSKTTmapStarted:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/server/location/LgeGpsLocationProvider;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/location/LgeGpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->UpdateGnssLockMode()V

    #@3
    return-void
.end method

.method static synthetic access$1302(Lcom/android/server/location/LgeGpsLocationProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/server/location/LgeGpsLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSettingEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/android/server/location/LgeGpsLocationProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSettingEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/location/LgeGpsLocationProvider;IILjava/lang/Object;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/location/LgeGpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/location/LgeGpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->gpsRilRecovery()V

    #@3
    return-void
.end method

.method static synthetic access$600()Z
    .registers 1

    #@0
    .prologue
    .line 88
    sget-boolean v0, Lcom/android/server/location/LgeGpsLocationProvider;->mSettingGpsEnable:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    sput-boolean p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSettingGpsEnable:Z

    #@2
    return p0
.end method

.method static synthetic access$700(Lcom/android/server/location/LgeGpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->soundVibControl()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/location/LgeGpsLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->ledControl()V

    #@3
    return-void
.end method

.method static synthetic access$902(Lcom/android/server/location/LgeGpsLocationProvider;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeHandler:Landroid/os/Handler;

    #@2
    return-object p1
.end method

.method private activateAGPS()Z
    .registers 3

    #@0
    .prologue
    .line 922
    const-string v0, "LgeGpsLocationProvider"

    #@2
    const-string v1, "KT LBSExtension activateAGPS()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 926
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method private activateGPS()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 902
    const-string v1, "LgeGpsLocationProvider"

    #@3
    const-string v2, "KT LBSExtension activateGPS()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 903
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@a
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v1

    #@e
    const-string v2, "gps"

    #@10
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@13
    move-result v0

    #@14
    .line 904
    .local v0, mSettingGpsEnable:Z
    if-nez v0, :cond_1a

    #@16
    .line 906
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, v3, v3, v1}, Lcom/android/server/location/LgeGpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@1a
    .line 908
    :cond_1a
    return v3
.end method

.method private deactivateAGPS()Z
    .registers 3

    #@0
    .prologue
    .line 930
    const-string v0, "LgeGpsLocationProvider"

    #@2
    const-string v1, "KT LBSExtension deactivateAGPS()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 934
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method private deactivateGPS()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 912
    const-string v1, "LgeGpsLocationProvider"

    #@3
    const-string v2, "KT LBSExtension activateGPS()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 913
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@a
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v1

    #@e
    const-string v2, "gps"

    #@10
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@13
    move-result v0

    #@14
    .line 914
    .local v0, mSettingGpsEnable:Z
    if-eqz v0, :cond_1b

    #@16
    .line 916
    const/4 v1, 0x0

    #@17
    const/4 v2, 0x0

    #@18
    invoke-direct {p0, v3, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->sendMessage(IILjava/lang/Object;)V

    #@1b
    .line 918
    :cond_1b
    return v3
.end method

.method private emotionalLedCtrl(Z)V
    .registers 8
    .parameter "isOn"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1459
    const-string v2, "LgeGpsLocationProvider"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "emotionalLedCtrl, isOn:"

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1461
    const/4 v0, 0x0

    #@1a
    .line 1463
    .local v0, bNeedChange:Z
    iget-boolean v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@1c
    if-nez v2, :cond_53

    #@1e
    if-ne p1, v5, :cond_53

    #@20
    .line 1464
    const/4 v0, 0x1

    #@21
    .line 1473
    :goto_21
    if-ne v0, v5, :cond_52

    #@23
    .line 1475
    new-instance v1, Landroid/content/Intent;

    #@25
    const-string v2, "com.lge.android.intent.action.GNSS_ALERT_LED_CHANGED"

    #@27
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2a
    .line 1476
    .local v1, intent:Landroid/content/Intent;
    if-ne p1, v5, :cond_5d

    #@2c
    .line 1477
    const-string v2, "com.lge.android.intent.extra.EXTRA_GNSS_LED_STATE"

    #@2e
    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@31
    .line 1483
    :goto_31
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@33
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@36
    .line 1484
    iput-boolean p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@38
    .line 1486
    const-string v2, "LgeGpsLocationProvider"

    #@3a
    new-instance v3, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v4, "emotionalLedCtrl, change to led status:"

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    iget-boolean v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1488
    .end local v1           #intent:Landroid/content/Intent;
    :cond_52
    return-void

    #@53
    .line 1466
    :cond_53
    iget-boolean v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@55
    if-ne v2, v5, :cond_5b

    #@57
    if-nez p1, :cond_5b

    #@59
    .line 1467
    const/4 v0, 0x1

    #@5a
    goto :goto_21

    #@5b
    .line 1470
    :cond_5b
    const/4 v0, 0x0

    #@5c
    goto :goto_21

    #@5d
    .line 1480
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_5d
    const-string v2, "com.lge.android.intent.extra.EXTRA_GNSS_LED_STATE"

    #@5f
    const/4 v3, 0x0

    #@60
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@63
    goto :goto_31
.end method

.method private getListenerPackageName(I)Ljava/lang/String;
    .registers 8
    .parameter "uid"

    #@0
    .prologue
    .line 395
    const/4 v1, 0x0

    #@1
    .line 397
    .local v1, packagesName:Ljava/lang/String;
    :try_start_1
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@3
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@6
    move-result-object v2

    #@7
    .line 398
    .local v2, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 399
    const-string v3, "LgeGpsLocationProvider"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "getListenerPackageName() packages = ["

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, "]"

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    const-string v5, "  uid = ["

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    const-string v5, "]"

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_39} :catch_3a

    #@39
    .line 404
    .end local v2           #pm:Landroid/content/pm/PackageManager;
    :goto_39
    return-object v1

    #@3a
    .line 400
    :catch_3a
    move-exception v0

    #@3b
    .line 401
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LgeGpsLocationProvider"

    #@3d
    new-instance v4, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v5, "getListenerPackageName() packages e = "

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_39
.end method

.method public static getXTRAUserAgent()Ljava/lang/String;
    .registers 25

    #@0
    .prologue
    .line 1186
    const-string v9, "LG"

    #@2
    .line 1193
    .local v9, mManufacturer:Ljava/lang/String;
    const-string v18, "Android"

    #@4
    .line 1194
    .local v18, mUserAgent:Ljava/lang/String;
    const/16 v21, 0xc

    #@6
    move/from16 v0, v21

    #@8
    new-array v12, v0, [Ljava/lang/String;

    #@a
    const/16 v21, 0x0

    #@c
    const-string v22, "Jan"

    #@e
    aput-object v22, v12, v21

    #@10
    const/16 v21, 0x1

    #@12
    const-string v22, "Feb"

    #@14
    aput-object v22, v12, v21

    #@16
    const/16 v21, 0x2

    #@18
    const-string v22, "Mar"

    #@1a
    aput-object v22, v12, v21

    #@1c
    const/16 v21, 0x3

    #@1e
    const-string v22, "Apr"

    #@20
    aput-object v22, v12, v21

    #@22
    const/16 v21, 0x4

    #@24
    const-string v22, "May"

    #@26
    aput-object v22, v12, v21

    #@28
    const/16 v21, 0x5

    #@2a
    const-string v22, "Jun"

    #@2c
    aput-object v22, v12, v21

    #@2e
    const/16 v21, 0x6

    #@30
    const-string v22, "Jul"

    #@32
    aput-object v22, v12, v21

    #@34
    const/16 v21, 0x7

    #@36
    const-string v22, "Aug"

    #@38
    aput-object v22, v12, v21

    #@3a
    const/16 v21, 0x8

    #@3c
    const-string v22, "Sep"

    #@3e
    aput-object v22, v12, v21

    #@40
    const/16 v21, 0x9

    #@42
    const-string v22, "Oct"

    #@44
    aput-object v22, v12, v21

    #@46
    const/16 v21, 0xa

    #@48
    const-string v22, "Nov"

    #@4a
    aput-object v22, v12, v21

    #@4c
    const/16 v21, 0xb

    #@4e
    const-string v22, "Dec"

    #@50
    aput-object v22, v12, v21

    #@52
    .line 1196
    .local v12, mMonth_db:[Ljava/lang/String;
    const-string v21, "SPR"

    #@54
    sget-object v22, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@56
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v21

    #@5a
    if-nez v21, :cond_5f

    #@5c
    .line 1197
    const-string v21, "Android"

    #@5e
    .line 1274
    :goto_5e
    return-object v21

    #@5f
    .line 1201
    :cond_5f
    const-string v21, "ro.cdma.home.operator.numeric"

    #@61
    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    .line 1202
    .local v4, mCarrierNumeric:Ljava/lang/String;
    const-string v21, "310120"

    #@67
    move-object/from16 v0, v21

    #@69
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v21

    #@6d
    if-eqz v21, :cond_17b

    #@6f
    .line 1204
    const-string v3, "Sprint"

    #@71
    .line 1219
    .local v3, mCarrierName:Ljava/lang/String;
    :goto_71
    const-string v9, "LG"

    #@73
    .line 1221
    const-string v21, "ro.product.model"

    #@75
    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@78
    move-result-object v10

    #@79
    .line 1223
    .local v10, mModelName:Ljava/lang/String;
    const-string v21, "ro.board.platform"

    #@7b
    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    .line 1225
    .local v5, mChipsetName:Ljava/lang/String;
    const-string v13, "LTE-CDMA"

    #@81
    .line 1227
    .local v13, mOtherTech:Ljava/lang/String;
    const-string v21, "ro.lge.swversion"

    #@83
    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@86
    move-result-object v15

    #@87
    .line 1229
    .local v15, mSoftwareVersion:Ljava/lang/String;
    const-string v8, "Android"

    #@89
    .line 1231
    .local v8, mDeviceClass:Ljava/lang/String;
    const-string v21, "ro.build.date"

    #@8b
    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8e
    move-result-object v2

    #@8f
    .line 1233
    .local v2, mBuildDate:Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuffer;

    #@91
    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    #@94
    .line 1235
    .local v14, mParsedDate:Ljava/lang/StringBuffer;
    new-instance v20, Ljava/util/StringTokenizer;

    #@96
    move-object/from16 v0, v20

    #@98
    invoke-direct {v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    #@9b
    .line 1236
    .local v20, stok:Ljava/util/StringTokenizer;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@9e
    move-result-object v7

    #@9f
    .line 1237
    .local v7, mDay:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@a2
    move-result-object v11

    #@a3
    .line 1238
    .local v11, mMonth:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@a6
    move-result-object v6

    #@a7
    .line 1239
    .local v6, mDate:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@aa
    move-result-object v17

    #@ab
    .line 1240
    .local v17, mTime:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@ae
    move-result-object v16

    #@af
    .line 1241
    .local v16, mStandardTime:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@b2
    move-result-object v19

    #@b3
    .line 1244
    .local v19, mYear:Ljava/lang/String;
    move-object/from16 v0, v19

    #@b5
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b8
    .line 1245
    const-string v21, "_"

    #@ba
    move-object/from16 v0, v21

    #@bc
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@bf
    .line 1247
    const-string v21, "Jan"

    #@c1
    move-object/from16 v0, v21

    #@c3
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c6
    move-result v21

    #@c7
    if-eqz v21, :cond_19b

    #@c9
    const-string v21, "01"

    #@cb
    move-object/from16 v0, v21

    #@cd
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d0
    .line 1260
    :cond_d0
    :goto_d0
    const-string v21, "_"

    #@d2
    move-object/from16 v0, v21

    #@d4
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d7
    .line 1261
    const-string v21, "%02d"

    #@d9
    const/16 v22, 0x1

    #@db
    move/from16 v0, v22

    #@dd
    new-array v0, v0, [Ljava/lang/Object;

    #@df
    move-object/from16 v22, v0

    #@e1
    const/16 v23, 0x0

    #@e3
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@e6
    move-result v24

    #@e7
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ea
    move-result-object v24

    #@eb
    aput-object v24, v22, v23

    #@ed
    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@f0
    move-result-object v21

    #@f1
    move-object/from16 v0, v21

    #@f3
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@f6
    .line 1264
    new-instance v21, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    move-object/from16 v0, v21

    #@fd
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v21

    #@101
    const-string v22, "/"

    #@103
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v21

    #@107
    move-object/from16 v0, v21

    #@109
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v21

    #@10d
    const-string v22, "/"

    #@10f
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v21

    #@113
    move-object/from16 v0, v21

    #@115
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v21

    #@119
    const-string v22, "/"

    #@11b
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v21

    #@11f
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@122
    move-result-object v22

    #@123
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v21

    #@127
    const-string v22, "/"

    #@129
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v21

    #@12d
    move-object/from16 v0, v21

    #@12f
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v21

    #@133
    const-string v22, "/"

    #@135
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v21

    #@139
    move-object/from16 v0, v21

    #@13b
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v21

    #@13f
    const-string v22, "/"

    #@141
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v21

    #@145
    move-object/from16 v0, v21

    #@147
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v21

    #@14b
    const-string v22, "/"

    #@14d
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v21

    #@151
    move-object/from16 v0, v21

    #@153
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v21

    #@157
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v18

    #@15b
    .line 1272
    const-string v21, "LgeGpsLocationProvider"

    #@15d
    new-instance v22, Ljava/lang/StringBuilder;

    #@15f
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@162
    const-string v23, "gpsOneXTRA HTTP User-Agent : "

    #@164
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v22

    #@168
    move-object/from16 v0, v22

    #@16a
    move-object/from16 v1, v18

    #@16c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v22

    #@170
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@173
    move-result-object v22

    #@174
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@177
    move-object/from16 v21, v18

    #@179
    .line 1274
    goto/16 :goto_5e

    #@17b
    .line 1206
    .end local v2           #mBuildDate:Ljava/lang/String;
    .end local v3           #mCarrierName:Ljava/lang/String;
    .end local v5           #mChipsetName:Ljava/lang/String;
    .end local v6           #mDate:Ljava/lang/String;
    .end local v7           #mDay:Ljava/lang/String;
    .end local v8           #mDeviceClass:Ljava/lang/String;
    .end local v10           #mModelName:Ljava/lang/String;
    .end local v11           #mMonth:Ljava/lang/String;
    .end local v13           #mOtherTech:Ljava/lang/String;
    .end local v14           #mParsedDate:Ljava/lang/StringBuffer;
    .end local v15           #mSoftwareVersion:Ljava/lang/String;
    .end local v16           #mStandardTime:Ljava/lang/String;
    .end local v17           #mTime:Ljava/lang/String;
    .end local v19           #mYear:Ljava/lang/String;
    .end local v20           #stok:Ljava/util/StringTokenizer;
    :cond_17b
    const-string v21, "311490"

    #@17d
    move-object/from16 v0, v21

    #@17f
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@182
    move-result v21

    #@183
    if-eqz v21, :cond_189

    #@185
    .line 1208
    const-string v3, "Virgin"

    #@187
    .restart local v3       #mCarrierName:Ljava/lang/String;
    goto/16 :goto_71

    #@189
    .line 1210
    .end local v3           #mCarrierName:Ljava/lang/String;
    :cond_189
    const-string v21, "311870"

    #@18b
    move-object/from16 v0, v21

    #@18d
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@190
    move-result v21

    #@191
    if-eqz v21, :cond_197

    #@193
    .line 1212
    const-string v3, "Boost"

    #@195
    .restart local v3       #mCarrierName:Ljava/lang/String;
    goto/16 :goto_71

    #@197
    .line 1216
    .end local v3           #mCarrierName:Ljava/lang/String;
    :cond_197
    const-string v3, "Wholesale"

    #@199
    .restart local v3       #mCarrierName:Ljava/lang/String;
    goto/16 :goto_71

    #@19b
    .line 1248
    .restart local v2       #mBuildDate:Ljava/lang/String;
    .restart local v5       #mChipsetName:Ljava/lang/String;
    .restart local v6       #mDate:Ljava/lang/String;
    .restart local v7       #mDay:Ljava/lang/String;
    .restart local v8       #mDeviceClass:Ljava/lang/String;
    .restart local v10       #mModelName:Ljava/lang/String;
    .restart local v11       #mMonth:Ljava/lang/String;
    .restart local v13       #mOtherTech:Ljava/lang/String;
    .restart local v14       #mParsedDate:Ljava/lang/StringBuffer;
    .restart local v15       #mSoftwareVersion:Ljava/lang/String;
    .restart local v16       #mStandardTime:Ljava/lang/String;
    .restart local v17       #mTime:Ljava/lang/String;
    .restart local v19       #mYear:Ljava/lang/String;
    .restart local v20       #stok:Ljava/util/StringTokenizer;
    :cond_19b
    const-string v21, "Feb"

    #@19d
    move-object/from16 v0, v21

    #@19f
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a2
    move-result v21

    #@1a3
    if-eqz v21, :cond_1ae

    #@1a5
    const-string v21, "02"

    #@1a7
    move-object/from16 v0, v21

    #@1a9
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1ac
    goto/16 :goto_d0

    #@1ae
    .line 1249
    :cond_1ae
    const-string v21, "Mar"

    #@1b0
    move-object/from16 v0, v21

    #@1b2
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b5
    move-result v21

    #@1b6
    if-eqz v21, :cond_1c1

    #@1b8
    const-string v21, "03"

    #@1ba
    move-object/from16 v0, v21

    #@1bc
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1bf
    goto/16 :goto_d0

    #@1c1
    .line 1250
    :cond_1c1
    const-string v21, "Apr"

    #@1c3
    move-object/from16 v0, v21

    #@1c5
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c8
    move-result v21

    #@1c9
    if-eqz v21, :cond_1d4

    #@1cb
    const-string v21, "04"

    #@1cd
    move-object/from16 v0, v21

    #@1cf
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1d2
    goto/16 :goto_d0

    #@1d4
    .line 1251
    :cond_1d4
    const-string v21, "May"

    #@1d6
    move-object/from16 v0, v21

    #@1d8
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1db
    move-result v21

    #@1dc
    if-eqz v21, :cond_1e7

    #@1de
    const-string v21, "05"

    #@1e0
    move-object/from16 v0, v21

    #@1e2
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1e5
    goto/16 :goto_d0

    #@1e7
    .line 1252
    :cond_1e7
    const-string v21, "Jun"

    #@1e9
    move-object/from16 v0, v21

    #@1eb
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ee
    move-result v21

    #@1ef
    if-eqz v21, :cond_1fa

    #@1f1
    const-string v21, "06"

    #@1f3
    move-object/from16 v0, v21

    #@1f5
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1f8
    goto/16 :goto_d0

    #@1fa
    .line 1253
    :cond_1fa
    const-string v21, "Jul"

    #@1fc
    move-object/from16 v0, v21

    #@1fe
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@201
    move-result v21

    #@202
    if-eqz v21, :cond_20d

    #@204
    const-string v21, "07"

    #@206
    move-object/from16 v0, v21

    #@208
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@20b
    goto/16 :goto_d0

    #@20d
    .line 1254
    :cond_20d
    const-string v21, "Aug"

    #@20f
    move-object/from16 v0, v21

    #@211
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@214
    move-result v21

    #@215
    if-eqz v21, :cond_220

    #@217
    const-string v21, "08"

    #@219
    move-object/from16 v0, v21

    #@21b
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@21e
    goto/16 :goto_d0

    #@220
    .line 1255
    :cond_220
    const-string v21, "Sep"

    #@222
    move-object/from16 v0, v21

    #@224
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@227
    move-result v21

    #@228
    if-eqz v21, :cond_233

    #@22a
    const-string v21, "09"

    #@22c
    move-object/from16 v0, v21

    #@22e
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@231
    goto/16 :goto_d0

    #@233
    .line 1256
    :cond_233
    const-string v21, "Oct"

    #@235
    move-object/from16 v0, v21

    #@237
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23a
    move-result v21

    #@23b
    if-eqz v21, :cond_246

    #@23d
    const-string v21, "10"

    #@23f
    move-object/from16 v0, v21

    #@241
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@244
    goto/16 :goto_d0

    #@246
    .line 1257
    :cond_246
    const-string v21, "Nov"

    #@248
    move-object/from16 v0, v21

    #@24a
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24d
    move-result v21

    #@24e
    if-eqz v21, :cond_259

    #@250
    const-string v21, "11"

    #@252
    move-object/from16 v0, v21

    #@254
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@257
    goto/16 :goto_d0

    #@259
    .line 1258
    :cond_259
    const-string v21, "Dec"

    #@25b
    move-object/from16 v0, v21

    #@25d
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@260
    move-result v21

    #@261
    if-eqz v21, :cond_d0

    #@263
    const-string v21, "12"

    #@265
    move-object/from16 v0, v21

    #@267
    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@26a
    goto/16 :goto_d0
.end method

.method private gpsRilRecovery()V
    .registers 5

    #@0
    .prologue
    .line 947
    const-string v1, "1"

    #@2
    const-string v2, "ril.reset_progress"

    #@4
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    .line 949
    .local v0, isRilReset:Z
    const-string v1, "LgeGpsLocationProvider"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "isRilReset = "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 951
    if-eqz v0, :cond_35

    #@26
    .line 952
    const-string v1, "20"

    #@28
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@2b
    move-result-object v1

    #@2c
    const-string v2, "20"

    #@2e
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@31
    move-result v2

    #@32
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@35
    .line 954
    :cond_35
    iget-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->isGpsRecorveryMode:Z

    #@37
    if-nez v1, :cond_43

    #@39
    .line 955
    const-string v1, "LgeGpsLocationProvider"

    #@3b
    const-string v2, "CP is resetting. Set isGpsRecorveryMode flag is TRUE ......"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 956
    const/4 v1, 0x1

    #@41
    iput-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->isGpsRecorveryMode:Z

    #@43
    .line 959
    :cond_43
    iget-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->isGpsRecorveryMode:Z

    #@45
    if-eqz v1, :cond_54

    #@47
    .line 960
    const-string v1, "LgeGpsLocationProvider"

    #@49
    const-string v2, "CP is resetting. Network is available after CP Reset, and then GPS Recorvery Mode Start ...... "

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 961
    const/4 v1, 0x0

    #@4f
    iput-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->isGpsRecorveryMode:Z

    #@51
    .line 962
    invoke-super {p0}, Lcom/android/server/location/GpsLocationProvider;->enable()V

    #@54
    .line 965
    :cond_54
    return-void
.end method

.method private handleGpsXTRAEnable(I)Z
    .registers 6
    .parameter "mode"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1024
    const/4 v0, 0x1

    #@2
    .line 1026
    .local v0, ret:Z
    if-ne p1, v3, :cond_15

    #@4
    .line 1027
    const-string v1, "lge.gps.extracmd.raw.xtra.enable"

    #@6
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@9
    move-result-object v1

    #@a
    const-string v2, "lge.gps.extracmd.raw.xtra.enable"

    #@c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@f
    move-result v2

    #@10
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@13
    move-result v0

    #@14
    .line 1036
    :goto_14
    return v3

    #@15
    .line 1031
    :cond_15
    const-string v1, "lge.gps.extracmd.raw.xtra.disable"

    #@17
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "lge.gps.extracmd.raw.xtra.disable"

    #@1d
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@20
    move-result v2

    #@21
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@24
    move-result v0

    #@25
    goto :goto_14
.end method

.method private isSupportELED()Z
    .registers 3

    #@0
    .prologue
    .line 1454
    const-string v0, "ro.lge.capp_emotional_led"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method private ktHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 12
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 695
    const/4 v4, 0x0

    #@2
    .line 697
    .local v4, result:Z
    const-string v5, "activateGPS"

    #@4
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_f

    #@a
    .line 698
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->activateGPS()Z

    #@d
    move-result v5

    #@e
    .line 758
    :goto_e
    return v5

    #@f
    .line 700
    :cond_f
    const-string v5, "deactivateGPS"

    #@11
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v5

    #@15
    if-eqz v5, :cond_1c

    #@17
    .line 701
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->deactivateGPS()Z

    #@1a
    move-result v5

    #@1b
    goto :goto_e

    #@1c
    .line 703
    :cond_1c
    const-string v5, "activateAGPS"

    #@1e
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v5

    #@22
    if-eqz v5, :cond_29

    #@24
    .line 704
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->activateAGPS()Z

    #@27
    move-result v5

    #@28
    goto :goto_e

    #@29
    .line 706
    :cond_29
    const-string v5, "deactivateAGPS"

    #@2b
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_36

    #@31
    .line 707
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->deactivateAGPS()Z

    #@34
    move-result v5

    #@35
    goto :goto_e

    #@36
    .line 709
    :cond_36
    const-string v5, "setOllehServer"

    #@38
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_8b

    #@3e
    .line 710
    if-eqz p2, :cond_89

    #@40
    .line 711
    const-string v5, "host"

    #@42
    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    .line 712
    .local v0, KTSuplServerHost:Ljava/lang/String;
    const-string v5, "port"

    #@48
    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@4b
    move-result v1

    #@4c
    .line 714
    .local v1, KTSuplServerPort:I
    const-string v5, "LgeGpsLocationProvider"

    #@4e
    new-instance v6, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v7, "KT_setOllehServer() -SUPL_HOST: "

    #@55
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v6

    #@5d
    const-string v7, "SUPL_PORT: "

    #@5f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v6

    #@63
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 716
    if-eqz v0, :cond_88

    #@70
    if-lez v1, :cond_88

    #@72
    .line 718
    new-instance v2, Landroid/os/Bundle;

    #@74
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@77
    .line 719
    .local v2, LgeExtras:Landroid/os/Bundle;
    const-string v5, "time"

    #@79
    invoke-virtual {v2, v5, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@7c
    .line 720
    const-string v5, "delete_aiding_data"

    #@7e
    invoke-super {p0, v5, v2}, Lcom/android/server/location/GpsLocationProvider;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@81
    .line 721
    sput-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@83
    .line 722
    iput v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@85
    .line 723
    invoke-virtual {p0, v8, v0, v1}, Lcom/android/server/location/LgeGpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@88
    .line 725
    .end local v2           #LgeExtras:Landroid/os/Bundle;
    :cond_88
    const/4 v4, 0x1

    #@89
    .end local v0           #KTSuplServerHost:Ljava/lang/String;
    .end local v1           #KTSuplServerPort:I
    :cond_89
    :goto_89
    move v5, v4

    #@8a
    .line 758
    goto :goto_e

    #@8b
    .line 728
    :cond_8b
    const-string v5, "setNativeServer"

    #@8d
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@90
    move-result v5

    #@91
    if-eqz v5, :cond_b5

    #@93
    .line 729
    const-string v5, "LgeGpsLocationProvider"

    #@95
    const-string v6, "KT_setNativeServer()"

    #@97
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 731
    new-instance v2, Landroid/os/Bundle;

    #@9c
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@9f
    .line 732
    .restart local v2       #LgeExtras:Landroid/os/Bundle;
    const-string v5, "time"

    #@a1
    invoke-virtual {v2, v5, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@a4
    .line 733
    const-string v5, "delete_aiding_data"

    #@a6
    invoke-super {p0, v5, v2}, Lcom/android/server/location/GpsLocationProvider;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@a9
    .line 734
    const-string v5, "supl.google.com"

    #@ab
    const/16 v6, 0x1c6c

    #@ad
    invoke-virtual {p0, v8, v5, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@b0
    .line 735
    const/4 v5, 0x0

    #@b1
    iput v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@b3
    .line 736
    const/4 v4, 0x1

    #@b4
    .line 738
    goto :goto_89

    #@b5
    .line 739
    .end local v2           #LgeExtras:Landroid/os/Bundle;
    :cond_b5
    const-string v5, "setMode"

    #@b7
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ba
    move-result v5

    #@bb
    if-eqz v5, :cond_e1

    #@bd
    .line 740
    if-eqz p2, :cond_89

    #@bf
    .line 741
    const-string v5, "mode"

    #@c1
    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@c4
    move-result v3

    #@c5
    .line 743
    .local v3, mode:I
    const-string v5, "LgeGpsLocationProvider"

    #@c7
    new-instance v6, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v7, "KT_setMode() mode = "

    #@ce
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v6

    #@d2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v6

    #@d6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v6

    #@da
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    .line 744
    iput v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@df
    .line 745
    const/4 v4, 0x1

    #@e0
    .line 746
    goto :goto_89

    #@e1
    .line 748
    .end local v3           #mode:I
    :cond_e1
    const-string v5, "getMode"

    #@e3
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e6
    move-result v5

    #@e7
    if-eqz v5, :cond_10f

    #@e9
    .line 749
    if-eqz p2, :cond_89

    #@eb
    .line 750
    const-string v5, "LgeGpsLocationProvider"

    #@ed
    new-instance v6, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v7, "KT_getMode() mode = "

    #@f4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v6

    #@f8
    iget v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@fa
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v6

    #@fe
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v6

    #@102
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@105
    .line 751
    const-string v5, "mode"

    #@107
    iget v6, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@109
    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@10c
    .line 752
    const/4 v4, 0x1

    #@10d
    goto/16 :goto_89

    #@10f
    .line 756
    :cond_10f
    const-string v5, "LgeGpsLocationProvider"

    #@111
    new-instance v6, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v7, "ktHandleExtraCmd: unknown command "

    #@118
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v6

    #@11c
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v6

    #@120
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v6

    #@124
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@127
    goto/16 :goto_89
.end method

.method private ledControl()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1391
    const/4 v0, 0x0

    #@3
    .line 1392
    .local v0, nLedSettingAll:I
    const/4 v1, 0x0

    #@4
    .line 1394
    .local v1, nLedSettingGps:I
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->isSupportELED()Z

    #@7
    move-result v2

    #@8
    if-ne v2, v5, :cond_64

    #@a
    .line 1395
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v2

    #@10
    const-string v3, "lge_notification_light_pulse"

    #@12
    invoke-static {v2, v3, v5}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15
    move-result v0

    #@16
    .line 1397
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@18
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, "emotional_led_gps"

    #@1e
    invoke-static {v2, v3, v5}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@21
    move-result v1

    #@22
    .line 1407
    :goto_22
    const-string v2, "LgeGpsLocationProvider"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "nLedSettingAll :"

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1408
    const-string v2, "LgeGpsLocationProvider"

    #@3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v4, "nLedSettingGps :"

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1410
    if-ne v0, v5, :cond_81

    #@54
    if-ne v1, v5, :cond_81

    #@56
    iget-boolean v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@58
    if-ne v2, v5, :cond_81

    #@5a
    .line 1411
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->isSupportELED()Z

    #@5d
    move-result v2

    #@5e
    if-ne v2, v5, :cond_7d

    #@60
    .line 1412
    invoke-direct {p0, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->emotionalLedCtrl(Z)V

    #@63
    .line 1424
    :cond_63
    :goto_63
    return-void

    #@64
    .line 1401
    :cond_64
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@66
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@69
    move-result-object v2

    #@6a
    const-string v3, "front_key_all"

    #@6c
    invoke-static {v2, v3, v5}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@6f
    move-result v0

    #@70
    .line 1403
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@72
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@75
    move-result-object v2

    #@76
    const-string v3, "front_key_gps"

    #@78
    invoke-static {v2, v3, v5}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7b
    move-result v1

    #@7c
    goto :goto_22

    #@7d
    .line 1414
    :cond_7d
    invoke-direct {p0, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->normalLedCtrl(Z)V

    #@80
    goto :goto_63

    #@81
    .line 1417
    :cond_81
    if-eqz v0, :cond_89

    #@83
    if-eqz v1, :cond_89

    #@85
    iget-boolean v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@87
    if-nez v2, :cond_63

    #@89
    .line 1418
    :cond_89
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->isSupportELED()Z

    #@8c
    move-result v2

    #@8d
    if-ne v2, v5, :cond_93

    #@8f
    .line 1419
    invoke-direct {p0, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->emotionalLedCtrl(Z)V

    #@92
    goto :goto_63

    #@93
    .line 1421
    :cond_93
    invoke-direct {p0, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->normalLedCtrl(Z)V

    #@96
    goto :goto_63
.end method

.method private lgeDefaultGnssConfig()V
    .registers 5

    #@0
    .prologue
    .line 641
    const-string v2, "LgeGpsLocationProvider"

    #@2
    const-string v3, "LGE GNSS Ext Config Default"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 644
    :try_start_7
    new-instance v1, Ljava/io/File;

    #@9
    const-string v2, "/data/ext_gps.conf"

    #@b
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@e
    .line 645
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@11
    move-result v2

    #@12
    const/4 v3, 0x1

    #@13
    if-ne v2, v3, :cond_22

    #@15
    .line 646
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@18
    move-result v2

    #@19
    if-nez v2, :cond_22

    #@1b
    .line 647
    const-string v2, "LgeGpsLocationProvider"

    #@1d
    const-string v3, "%s/data/ext_gps.conf delete fail !!"

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_22} :catch_26

    #@22
    .line 652
    .end local v1           #file:Ljava/io/File;
    :cond_22
    :goto_22
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->loadConfig()V

    #@25
    .line 653
    return-void

    #@26
    .line 649
    :catch_26
    move-exception v0

    #@27
    .line 650
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "LgeGpsLocationProvider"

    #@29
    const-string v3, " LGE GNSS Ext Config Default Fail"

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    goto :goto_22
.end method

.method private lgeHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 14
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 455
    const/4 v8, 0x1

    #@2
    .line 456
    .local v8, ret:Z
    const/4 v7, 0x0

    #@3
    .line 458
    .local v7, init:Z
    const-string v0, "LgeGpsLocationProvider"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "LGE Framework Extra Command : "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 460
    const-string v0, "lge.gps.extracmd.fmw.setgnss_settinginfo"

    #@1d
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_e2

    #@23
    .line 461
    const-string v0, "supl_type"

    #@25
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@28
    move-result v0

    #@29
    iput v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplType:I

    #@2b
    .line 462
    iget v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplType:I

    #@2d
    if-eqz v0, :cond_de

    #@2f
    .line 464
    const-string v0, "pos_mode"

    #@31
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@34
    move-result v0

    #@35
    iput v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@37
    .line 466
    const-string v0, "supl_host"

    #@39
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v9

    #@3d
    .line 467
    .local v9, supl_host:Ljava/lang/String;
    const-string v0, "supl_port"

    #@3f
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@42
    move-result v10

    #@43
    .line 468
    .local v10, supl_port:I
    if-eqz v9, :cond_4d

    #@45
    sget-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@47
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v0

    #@4b
    if-eqz v0, :cond_51

    #@4d
    :cond_4d
    iget v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@4f
    if-eq v10, v0, :cond_86

    #@51
    .line 471
    :cond_51
    sput-object v9, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@53
    .line 472
    iput v10, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@55
    .line 474
    sget-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@57
    if-eqz v0, :cond_86

    #@59
    .line 476
    const-string v0, "LgeGpsLocationProvider"

    #@5b
    new-instance v1, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v2, "changed supl addr, after host : "

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    const-string v2, ", port : "

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    iget v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 477
    sget-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@81
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@83
    invoke-virtual {p0, v3, v0, v1}, Lcom/android/server/location/LgeGpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@86
    .line 480
    :cond_86
    const-string v0, "tls_mode"

    #@88
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@8b
    move-result v0

    #@8c
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@8e
    if-eq v0, v1, :cond_cf

    #@90
    .line 482
    const-string v0, "LgeGpsLocationProvider"

    #@92
    new-instance v1, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v2, "changed tls mode, before mode : "

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    iget v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@9f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v1

    #@a7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 483
    const-string v0, "tls_mode"

    #@ac
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@af
    move-result v0

    #@b0
    iput v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@b2
    .line 485
    const-string v0, "lge.gps.extracmd.raw.tls.mode#%d"

    #@b4
    new-array v1, v3, [Ljava/lang/Object;

    #@b6
    const/4 v2, 0x0

    #@b7
    iget v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@b9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bc
    move-result-object v3

    #@bd
    aput-object v3, v1, v2

    #@bf
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@c2
    move-result-object v6

    #@c3
    .line 486
    .local v6, LgeTlsMode:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    #@c6
    move-result-object v0

    #@c7
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@ca
    move-result v1

    #@cb
    invoke-virtual {p0, v0, v1}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@ce
    move-result v8

    #@cf
    .line 488
    .end local v6           #LgeTlsMode:Ljava/lang/String;
    :cond_cf
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@d1
    iget v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplType:I

    #@d3
    sget-object v3, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@d5
    iget v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@d7
    iget v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@d9
    move-object v0, p0

    #@da
    invoke-direct/range {v0 .. v5}, Lcom/android/server/location/LgeGpsLocationProvider;->lgeSaveGnssConfig(IILjava/lang/String;II)V

    #@dd
    .line 507
    .end local v9           #supl_host:Ljava/lang/String;
    .end local v10           #supl_port:I
    :goto_dd
    return v8

    #@de
    .line 492
    :cond_de
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->lgeDefaultGnssConfig()V

    #@e1
    goto :goto_dd

    #@e2
    .line 495
    :cond_e2
    const-string v0, "lge.gps.extracmd.fmw.getgnss_settinginfo"

    #@e4
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e7
    move-result v0

    #@e8
    if-eqz v0, :cond_10e

    #@ea
    .line 496
    const-string v0, "supl_type"

    #@ec
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplType:I

    #@ee
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@f1
    .line 497
    const-string v0, "supl_host"

    #@f3
    sget-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@f5
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@f8
    .line 498
    const-string v0, "supl_port"

    #@fa
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@fc
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@ff
    .line 499
    const-string v0, "tls_mode"

    #@101
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@103
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@106
    .line 500
    const-string v0, "pos_mode"

    #@108
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@10a
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@10d
    goto :goto_dd

    #@10e
    .line 504
    :cond_10e
    const-string v0, "LgeGpsLocationProvider"

    #@110
    new-instance v1, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v2, "invalid LGE Framework Extra Command : "

    #@117
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v1

    #@11b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v1

    #@11f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v1

    #@123
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    .line 505
    const/4 v8, 0x0

    #@127
    goto :goto_dd
.end method

.method private lgeSaveGnssConfig(IILjava/lang/String;II)V
    .registers 15
    .parameter "PositionMode"
    .parameter "SuplType"
    .parameter "SuplHost"
    .parameter "SuplPort"
    .parameter "TlsMode"

    #@0
    .prologue
    .line 617
    const-string v5, "LgeGpsLocationProvider"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "LGE GNSS Ext Config Save, posMode ="

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    const-string v7, ", SuplType ="

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    const-string v7, ", SUPL_HOST ="

    #@1d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    const-string v7, ", SUPL_PORT ="

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    const-string v7, ", TLS_MODE ="

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 619
    const/4 v3, 0x0

    #@41
    .line 621
    .local v3, writer:Ljava/io/FileWriter;
    :try_start_41
    new-instance v2, Ljava/io/File;

    #@43
    const-string v5, "/data/ext_gps.conf"

    #@45
    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@48
    .line 622
    .local v2, file:Ljava/io/File;
    new-instance v4, Ljava/io/FileWriter;

    #@4a
    invoke-direct {v4, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_4d
    .catchall {:try_start_41 .. :try_end_4d} :catchall_b2
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_4d} :catch_9b

    #@4d
    .line 623
    .end local v3           #writer:Ljava/io/FileWriter;
    .local v4, writer:Ljava/io/FileWriter;
    :try_start_4d
    const-string v5, "%s=%d\n%s=%d\n%s=%s\n%s=%d\n%s=%d\n"

    #@4f
    const/16 v6, 0xa

    #@51
    new-array v6, v6, [Ljava/lang/Object;

    #@53
    const/4 v7, 0x0

    #@54
    const-string v8, "POSITION_MODE"

    #@56
    aput-object v8, v6, v7

    #@58
    const/4 v7, 0x1

    #@59
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v8

    #@5d
    aput-object v8, v6, v7

    #@5f
    const/4 v7, 0x2

    #@60
    const-string v8, "SUPL_TYPE"

    #@62
    aput-object v8, v6, v7

    #@64
    const/4 v7, 0x3

    #@65
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68
    move-result-object v8

    #@69
    aput-object v8, v6, v7

    #@6b
    const/4 v7, 0x4

    #@6c
    const-string v8, "SUPL_HOST"

    #@6e
    aput-object v8, v6, v7

    #@70
    const/4 v7, 0x5

    #@71
    aput-object p3, v6, v7

    #@73
    const/4 v7, 0x6

    #@74
    const-string v8, "SUPL_PORT"

    #@76
    aput-object v8, v6, v7

    #@78
    const/4 v7, 0x7

    #@79
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7c
    move-result-object v8

    #@7d
    aput-object v8, v6, v7

    #@7f
    const/16 v7, 0x8

    #@81
    const-string v8, "TLS_MODE"

    #@83
    aput-object v8, v6, v7

    #@85
    const/16 v7, 0x9

    #@87
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8a
    move-result-object v8

    #@8b
    aput-object v8, v6, v7

    #@8d
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    .line 629
    .local v0, config:Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    :try_end_94
    .catchall {:try_start_4d .. :try_end_94} :catchall_cb
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_94} :catch_ce

    #@94
    .line 633
    if-eqz v4, :cond_99

    #@96
    .line 634
    :try_start_96
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_99
    .catch Ljava/io/IOException; {:try_start_96 .. :try_end_99} :catch_c2

    #@99
    :cond_99
    :goto_99
    move-object v3, v4

    #@9a
    .line 638
    .end local v0           #config:Ljava/lang/String;
    .end local v2           #file:Ljava/io/File;
    .end local v4           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    :cond_9a
    :goto_9a
    return-void

    #@9b
    .line 630
    :catch_9b
    move-exception v1

    #@9c
    .line 631
    .local v1, e:Ljava/io/IOException;
    :goto_9c
    :try_start_9c
    const-string v5, "LgeGpsLocationProvider"

    #@9e
    const-string v6, "LG GNSS Config File save Fail"

    #@a0
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a3
    .catchall {:try_start_9c .. :try_end_a3} :catchall_b2

    #@a3
    .line 633
    if-eqz v3, :cond_9a

    #@a5
    .line 634
    :try_start_a5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_a8
    .catch Ljava/io/IOException; {:try_start_a5 .. :try_end_a8} :catch_a9

    #@a8
    goto :goto_9a

    #@a9
    .line 635
    :catch_a9
    move-exception v1

    #@aa
    const-string v5, "LgeGpsLocationProvider"

    #@ac
    const-string v6, "LG GNSS Config File close Fail"

    #@ae
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    goto :goto_9a

    #@b2
    .line 633
    .end local v1           #e:Ljava/io/IOException;
    :catchall_b2
    move-exception v5

    #@b3
    :goto_b3
    if-eqz v3, :cond_b8

    #@b5
    .line 634
    :try_start_b5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_b8
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_b8} :catch_b9

    #@b8
    .line 633
    :cond_b8
    :goto_b8
    throw v5

    #@b9
    .line 635
    :catch_b9
    move-exception v1

    #@ba
    .restart local v1       #e:Ljava/io/IOException;
    const-string v6, "LgeGpsLocationProvider"

    #@bc
    const-string v7, "LG GNSS Config File close Fail"

    #@be
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    goto :goto_b8

    #@c2
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v0       #config:Ljava/lang/String;
    .restart local v2       #file:Ljava/io/File;
    .restart local v4       #writer:Ljava/io/FileWriter;
    :catch_c2
    move-exception v1

    #@c3
    .restart local v1       #e:Ljava/io/IOException;
    const-string v5, "LgeGpsLocationProvider"

    #@c5
    const-string v6, "LG GNSS Config File close Fail"

    #@c7
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    goto :goto_99

    #@cb
    .line 633
    .end local v0           #config:Ljava/lang/String;
    .end local v1           #e:Ljava/io/IOException;
    :catchall_cb
    move-exception v5

    #@cc
    move-object v3, v4

    #@cd
    .end local v4           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    goto :goto_b3

    #@ce
    .line 630
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v4       #writer:Ljava/io/FileWriter;
    :catch_ce
    move-exception v1

    #@cf
    move-object v3, v4

    #@d0
    .end local v4           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    goto :goto_9c
.end method

.method private lguHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 7
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    const/16 v3, 0x5a0

    #@2
    const/4 v2, 0x1

    #@3
    .line 762
    const/4 v0, 0x0

    #@4
    .line 763
    .local v0, result:Z
    const-string v1, "delete_aiding_data"

    #@6
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_16

    #@c
    .line 764
    const-wide/16 v1, 0x0

    #@e
    sput-wide v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadDate:J

    #@10
    .line 766
    invoke-virtual {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->saveXtraDownloadDate()V

    #@13
    .line 767
    const/4 v0, 0x0

    #@14
    :goto_14
    move v1, v0

    #@15
    .line 814
    :goto_15
    return v1

    #@16
    .line 770
    :cond_16
    const-string v1, "request_xtra_download"

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_25

    #@1e
    .line 771
    const-string v1, "force_xtra_injection"

    #@20
    const/4 v2, 0x0

    #@21
    invoke-super {p0, v1, v2}, Lcom/android/server/location/GpsLocationProvider;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@24
    goto :goto_14

    #@25
    .line 773
    :cond_25
    const-string v1, "call_xtra_setting"

    #@27
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_32

    #@2d
    .line 774
    invoke-virtual {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->broadcastXtraSettings()V

    #@30
    .line 775
    const/4 v0, 0x1

    #@31
    goto :goto_14

    #@32
    .line 777
    :cond_32
    const-string v1, "set_xtra_download_frequency"

    #@34
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_bc

    #@3a
    .line 778
    const-string v1, "set_xtra_download_frequency_024"

    #@3c
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v1

    #@40
    if-ne v1, v2, :cond_60

    #@42
    .line 779
    sput v3, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@44
    .line 807
    :goto_44
    const-string v1, "LgeGpsLocationProvider"

    #@46
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v3, "set_xtra_download_frequency: "

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    sget v3, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 808
    const/4 v0, 0x1

    #@5f
    goto :goto_14

    #@60
    .line 781
    :cond_60
    const-string v1, "set_xtra_download_frequency_048"

    #@62
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v1

    #@66
    if-ne v1, v2, :cond_6d

    #@68
    .line 782
    const/16 v1, 0xb40

    #@6a
    sput v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@6c
    goto :goto_44

    #@6d
    .line 784
    :cond_6d
    const-string v1, "set_xtra_download_frequency_072"

    #@6f
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@72
    move-result v1

    #@73
    if-ne v1, v2, :cond_7a

    #@75
    .line 785
    const/16 v1, 0x10e0

    #@77
    sput v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@79
    goto :goto_44

    #@7a
    .line 787
    :cond_7a
    const-string v1, "set_xtra_download_frequency_096"

    #@7c
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v1

    #@80
    if-ne v1, v2, :cond_87

    #@82
    .line 788
    const/16 v1, 0x1680

    #@84
    sput v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@86
    goto :goto_44

    #@87
    .line 790
    :cond_87
    const-string v1, "set_xtra_download_frequency_120"

    #@89
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v1

    #@8d
    if-ne v1, v2, :cond_94

    #@8f
    .line 791
    const/16 v1, 0x1c20

    #@91
    sput v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@93
    goto :goto_44

    #@94
    .line 793
    :cond_94
    const-string v1, "set_xtra_download_frequency_144"

    #@96
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v1

    #@9a
    if-ne v1, v2, :cond_a1

    #@9c
    .line 794
    const/16 v1, 0x21c0

    #@9e
    sput v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@a0
    goto :goto_44

    #@a1
    .line 796
    :cond_a1
    const-string v1, "set_xtra_download_frequency_168"

    #@a3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a6
    move-result v1

    #@a7
    if-ne v1, v2, :cond_ae

    #@a9
    .line 797
    const/16 v1, 0x2760

    #@ab
    sput v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@ad
    goto :goto_44

    #@ae
    .line 800
    :cond_ae
    if-eqz p2, :cond_b9

    #@b0
    .line 801
    const-string v1, "xtra_download_frequency"

    #@b2
    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@b5
    move-result v1

    #@b6
    sput v1, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@b8
    goto :goto_44

    #@b9
    .line 804
    :cond_b9
    sput v3, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@bb
    goto :goto_44

    #@bc
    .line 811
    :cond_bc
    const-string v1, "LgeGpsLocationProvider"

    #@be
    new-instance v2, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v3, "lguHandleExtraCmd: unknown command "

    #@c5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v2

    #@c9
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v2

    #@cd
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v2

    #@d1
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 812
    const/4 v1, 0x0

    #@d5
    goto/16 :goto_15
.end method

.method private loadConfig()V
    .registers 13

    #@0
    .prologue
    .line 512
    new-instance v9, Ljava/util/Properties;

    #@2
    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    #@5
    iput-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeProperties:Ljava/util/Properties;

    #@7
    .line 514
    :try_start_7
    new-instance v2, Ljava/io/File;

    #@9
    const-string v9, "/etc/gps.conf"

    #@b
    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@e
    .line 515
    .local v2, file:Ljava/io/File;
    new-instance v6, Ljava/io/FileInputStream;

    #@10
    invoke-direct {v6, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@13
    .line 516
    .local v6, stream:Ljava/io/FileInputStream;
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeProperties:Ljava/util/Properties;

    #@15
    invoke-virtual {v9, v6}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    #@18
    .line 517
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    #@1b
    .line 519
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeProperties:Ljava/util/Properties;

    #@1d
    const-string v10, "LGE_GPS_POSITION_MODE"

    #@1f
    const-string v11, "1"

    #@21
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_24} :catch_180

    #@24
    move-result-object v5

    #@25
    .line 520
    .local v5, posMode:Ljava/lang/String;
    if-eqz v5, :cond_2d

    #@27
    .line 522
    :try_start_27
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2a
    move-result v9

    #@2b
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I
    :try_end_2d
    .catch Ljava/lang/NumberFormatException; {:try_start_27 .. :try_end_2d} :catch_165
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_2d} :catch_180

    #@2d
    .line 529
    :cond_2d
    :goto_2d
    :try_start_2d
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeProperties:Ljava/util/Properties;

    #@2f
    const-string v10, "SUPL_HOST"

    #@31
    invoke-virtual {v9, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v9

    #@35
    sput-object v9, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@37
    .line 530
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeProperties:Ljava/util/Properties;

    #@39
    const-string v10, "SUPL_PORT"

    #@3b
    invoke-virtual {v9, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    .line 531
    .local v3, mLgeSuplPortString:Ljava/lang/String;
    sget-object v9, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_41} :catch_180

    #@41
    if-eqz v9, :cond_4b

    #@43
    if-eqz v3, :cond_4b

    #@45
    .line 533
    :try_start_45
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@48
    move-result v9

    #@49
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I
    :try_end_4b
    .catch Ljava/lang/NumberFormatException; {:try_start_45 .. :try_end_4b} :catch_18a
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_4b} :catch_180

    #@4b
    .line 539
    :cond_4b
    :goto_4b
    :try_start_4b
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeProperties:Ljava/util/Properties;

    #@4d
    const-string v10, "LGE_TLS_MODE"

    #@4f
    const-string v11, "0"

    #@51
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_54
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_54} :catch_180

    #@54
    move-result-object v0

    #@55
    .line 540
    .local v0, TlsModeString:Ljava/lang/String;
    if-eqz v0, :cond_5d

    #@57
    .line 542
    :try_start_57
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5a
    move-result v9

    #@5b
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I
    :try_end_5d
    .catch Ljava/lang/NumberFormatException; {:try_start_57 .. :try_end_5d} :catch_1a5
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_5d} :catch_180

    #@5d
    .line 550
    :cond_5d
    :goto_5d
    :try_start_5d
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeProperties:Ljava/util/Properties;

    #@5f
    const-string v10, "VENDOR"

    #@61
    const-string v11, ""

    #@63
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@66
    move-result-object v9

    #@67
    sput-object v9, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@69
    .line 553
    const-string v9, "LgeGpsLocationProvider"

    #@6b
    new-instance v10, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v11, "gps.conf - mPositionMode ="

    #@72
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v10

    #@76
    iget v11, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v10

    #@7c
    const-string v11, ", SUPL_HOST ="

    #@7e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v10

    #@82
    sget-object v11, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@84
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v10

    #@88
    const-string v11, ", SUPL_PORT ="

    #@8a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v10

    #@8e
    iget v11, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@90
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v10

    #@94
    const-string v11, ", TLS_MODE ="

    #@96
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v10

    #@9a
    iget v11, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@9c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v10

    #@a0
    const-string v11, ",VendorName ="

    #@a2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v10

    #@a6
    sget-object v11, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@a8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v10

    #@ac
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v10

    #@b0
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b3
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_b3} :catch_180

    #@b3
    .line 559
    .end local v0           #TlsModeString:Ljava/lang/String;
    .end local v2           #file:Ljava/io/File;
    .end local v3           #mLgeSuplPortString:Ljava/lang/String;
    .end local v5           #posMode:Ljava/lang/String;
    .end local v6           #stream:Ljava/io/FileInputStream;
    :goto_b3
    :try_start_b3
    new-instance v2, Ljava/io/File;

    #@b5
    const-string v9, "/data/ext_gps.conf"

    #@b7
    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@ba
    .line 560
    .restart local v2       #file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@bd
    move-result v9

    #@be
    const/4 v10, 0x1

    #@bf
    if-ne v9, v10, :cond_164

    #@c1
    .line 562
    new-instance v9, Ljava/util/Properties;

    #@c3
    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    #@c6
    iput-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mExtProperties:Ljava/util/Properties;

    #@c8
    .line 563
    new-instance v6, Ljava/io/FileInputStream;

    #@ca
    invoke-direct {v6, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@cd
    .line 564
    .restart local v6       #stream:Ljava/io/FileInputStream;
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mExtProperties:Ljava/util/Properties;

    #@cf
    invoke-virtual {v9, v6}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    #@d2
    .line 565
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    #@d5
    .line 567
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mExtProperties:Ljava/util/Properties;

    #@d7
    const-string v10, "POSITION_MODE"

    #@d9
    const/4 v11, 0x0

    #@da
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_dd
    .catch Ljava/lang/Exception; {:try_start_b3 .. :try_end_dd} :catch_1de

    #@dd
    move-result-object v5

    #@de
    .line 568
    .restart local v5       #posMode:Ljava/lang/String;
    if-eqz v5, :cond_e6

    #@e0
    .line 570
    :try_start_e0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@e3
    move-result v9

    #@e4
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I
    :try_end_e6
    .catch Ljava/lang/NumberFormatException; {:try_start_e0 .. :try_end_e6} :catch_1c3
    .catch Ljava/lang/Exception; {:try_start_e0 .. :try_end_e6} :catch_1de

    #@e6
    .line 576
    :cond_e6
    :goto_e6
    :try_start_e6
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mExtProperties:Ljava/util/Properties;

    #@e8
    const-string v10, "SUPL_TYPE"

    #@ea
    const/4 v11, 0x0

    #@eb
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_ee
    .catch Ljava/lang/Exception; {:try_start_e6 .. :try_end_ee} :catch_1de

    #@ee
    move-result-object v7

    #@ef
    .line 577
    .local v7, suplType:Ljava/lang/String;
    if-eqz v7, :cond_f7

    #@f1
    .line 579
    :try_start_f1
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@f4
    move-result v9

    #@f5
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplType:I
    :try_end_f7
    .catch Ljava/lang/NumberFormatException; {:try_start_f1 .. :try_end_f7} :catch_1e8
    .catch Ljava/lang/Exception; {:try_start_f1 .. :try_end_f7} :catch_1de

    #@f7
    .line 585
    :cond_f7
    :goto_f7
    :try_start_f7
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mExtProperties:Ljava/util/Properties;

    #@f9
    const-string v10, "SUPL_HOST"

    #@fb
    const/4 v11, 0x0

    #@fc
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ff
    move-result-object v4

    #@100
    .line 586
    .local v4, mLgeSuplServerString:Ljava/lang/String;
    if-eqz v4, :cond_104

    #@102
    .line 588
    sput-object v4, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@104
    .line 591
    :cond_104
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mExtProperties:Ljava/util/Properties;

    #@106
    const-string v10, "SUPL_PORT"

    #@108
    const/4 v11, 0x0

    #@109
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_10c
    .catch Ljava/lang/Exception; {:try_start_f7 .. :try_end_10c} :catch_1de

    #@10c
    move-result-object v3

    #@10d
    .line 592
    .restart local v3       #mLgeSuplPortString:Ljava/lang/String;
    if-eqz v3, :cond_115

    #@10f
    .line 594
    :try_start_10f
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@112
    move-result v9

    #@113
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I
    :try_end_115
    .catch Ljava/lang/NumberFormatException; {:try_start_10f .. :try_end_115} :catch_203
    .catch Ljava/lang/Exception; {:try_start_10f .. :try_end_115} :catch_1de

    #@115
    .line 599
    :cond_115
    :goto_115
    :try_start_115
    iget-object v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mExtProperties:Ljava/util/Properties;

    #@117
    const-string v10, "TLS_MODE"

    #@119
    const/4 v11, 0x0

    #@11a
    invoke-virtual {v9, v10, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_11d
    .catch Ljava/lang/Exception; {:try_start_115 .. :try_end_11d} :catch_1de

    #@11d
    move-result-object v8

    #@11e
    .line 600
    .local v8, tlsMode:Ljava/lang/String;
    if-eqz v8, :cond_126

    #@120
    .line 602
    :try_start_120
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@123
    move-result v9

    #@124
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I
    :try_end_126
    .catch Ljava/lang/NumberFormatException; {:try_start_120 .. :try_end_126} :catch_21e
    .catch Ljava/lang/Exception; {:try_start_120 .. :try_end_126} :catch_1de

    #@126
    .line 609
    :cond_126
    :goto_126
    :try_start_126
    const-string v9, "LgeGpsLocationProvider"

    #@128
    new-instance v10, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const-string v11, "ext_gps.conf - posMode ="

    #@12f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v10

    #@133
    iget v11, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@135
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@138
    move-result-object v10

    #@139
    const-string v11, ", SUPL_HOST ="

    #@13b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v10

    #@13f
    sget-object v11, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@141
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v10

    #@145
    const-string v11, ", SUPL_PORT ="

    #@147
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v10

    #@14b
    iget v11, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@14d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@150
    move-result-object v10

    #@151
    const-string v11, ", TLS_MODE ="

    #@153
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v10

    #@157
    iget v11, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@159
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v10

    #@15d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v10

    #@161
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_164
    .catch Ljava/lang/Exception; {:try_start_126 .. :try_end_164} :catch_1de

    #@164
    .line 614
    .end local v2           #file:Ljava/io/File;
    .end local v3           #mLgeSuplPortString:Ljava/lang/String;
    .end local v4           #mLgeSuplServerString:Ljava/lang/String;
    .end local v5           #posMode:Ljava/lang/String;
    .end local v6           #stream:Ljava/io/FileInputStream;
    .end local v7           #suplType:Ljava/lang/String;
    .end local v8           #tlsMode:Ljava/lang/String;
    :cond_164
    :goto_164
    return-void

    #@165
    .line 524
    .restart local v2       #file:Ljava/io/File;
    .restart local v5       #posMode:Ljava/lang/String;
    .restart local v6       #stream:Ljava/io/FileInputStream;
    :catch_165
    move-exception v1

    #@166
    .line 525
    .local v1, e:Ljava/lang/NumberFormatException;
    :try_start_166
    const-string v9, "LgeGpsLocationProvider"

    #@168
    new-instance v10, Ljava/lang/StringBuilder;

    #@16a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@16d
    const-string v11, "unable to parse LGE_GPS_POSITION_MODE: "

    #@16f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v10

    #@173
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v10

    #@177
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17a
    move-result-object v10

    #@17b
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_17e
    .catch Ljava/io/IOException; {:try_start_166 .. :try_end_17e} :catch_180

    #@17e
    goto/16 :goto_2d

    #@180
    .line 554
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v2           #file:Ljava/io/File;
    .end local v5           #posMode:Ljava/lang/String;
    .end local v6           #stream:Ljava/io/FileInputStream;
    :catch_180
    move-exception v1

    #@181
    .line 555
    .local v1, e:Ljava/io/IOException;
    const-string v9, "LgeGpsLocationProvider"

    #@183
    const-string v10, "Could not open GPS configuration file /etc/gps.conf"

    #@185
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@188
    goto/16 :goto_b3

    #@18a
    .line 534
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #file:Ljava/io/File;
    .restart local v3       #mLgeSuplPortString:Ljava/lang/String;
    .restart local v5       #posMode:Ljava/lang/String;
    .restart local v6       #stream:Ljava/io/FileInputStream;
    :catch_18a
    move-exception v1

    #@18b
    .line 535
    .local v1, e:Ljava/lang/NumberFormatException;
    :try_start_18b
    const-string v9, "LgeGpsLocationProvider"

    #@18d
    new-instance v10, Ljava/lang/StringBuilder;

    #@18f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@192
    const-string v11, "unable to parse SUPL_PORT: "

    #@194
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v10

    #@198
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v10

    #@19c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19f
    move-result-object v10

    #@1a0
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a3
    goto/16 :goto_4b

    #@1a5
    .line 543
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .restart local v0       #TlsModeString:Ljava/lang/String;
    :catch_1a5
    move-exception v1

    #@1a6
    .line 544
    .restart local v1       #e:Ljava/lang/NumberFormatException;
    const-string v9, "LgeGpsLocationProvider"

    #@1a8
    new-instance v10, Ljava/lang/StringBuilder;

    #@1aa
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1ad
    const-string v11, "unable to parse LGE_TLS_MODE: "

    #@1af
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v10

    #@1b3
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v10

    #@1b7
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ba
    move-result-object v10

    #@1bb
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1be
    .line 545
    const/4 v9, 0x0

    #@1bf
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I
    :try_end_1c1
    .catch Ljava/io/IOException; {:try_start_18b .. :try_end_1c1} :catch_180

    #@1c1
    goto/16 :goto_5d

    #@1c3
    .line 571
    .end local v0           #TlsModeString:Ljava/lang/String;
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v3           #mLgeSuplPortString:Ljava/lang/String;
    :catch_1c3
    move-exception v1

    #@1c4
    .line 572
    .restart local v1       #e:Ljava/lang/NumberFormatException;
    :try_start_1c4
    const-string v9, "LgeGpsLocationProvider"

    #@1c6
    new-instance v10, Ljava/lang/StringBuilder;

    #@1c8
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1cb
    const-string v11, "unable to parse POSITION_MODE: "

    #@1cd
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v10

    #@1d1
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v10

    #@1d5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d8
    move-result-object v10

    #@1d9
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1dc
    .catch Ljava/lang/Exception; {:try_start_1c4 .. :try_end_1dc} :catch_1de

    #@1dc
    goto/16 :goto_e6

    #@1de
    .line 611
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v2           #file:Ljava/io/File;
    .end local v5           #posMode:Ljava/lang/String;
    .end local v6           #stream:Ljava/io/FileInputStream;
    :catch_1de
    move-exception v1

    #@1df
    .line 612
    .local v1, e:Ljava/lang/Exception;
    const-string v9, "LgeGpsLocationProvider"

    #@1e1
    const-string v10, " LG GNSS Ext Config File does not exist"

    #@1e3
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e6
    goto/16 :goto_164

    #@1e8
    .line 580
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v2       #file:Ljava/io/File;
    .restart local v5       #posMode:Ljava/lang/String;
    .restart local v6       #stream:Ljava/io/FileInputStream;
    .restart local v7       #suplType:Ljava/lang/String;
    :catch_1e8
    move-exception v1

    #@1e9
    .line 581
    .local v1, e:Ljava/lang/NumberFormatException;
    :try_start_1e9
    const-string v9, "LgeGpsLocationProvider"

    #@1eb
    new-instance v10, Ljava/lang/StringBuilder;

    #@1ed
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1f0
    const-string v11, "unable to parse SUPL_TYPE: "

    #@1f2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v10

    #@1f6
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v10

    #@1fa
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fd
    move-result-object v10

    #@1fe
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@201
    goto/16 :goto_f7

    #@203
    .line 595
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .restart local v3       #mLgeSuplPortString:Ljava/lang/String;
    .restart local v4       #mLgeSuplServerString:Ljava/lang/String;
    :catch_203
    move-exception v1

    #@204
    .line 596
    .restart local v1       #e:Ljava/lang/NumberFormatException;
    const-string v9, "LgeGpsLocationProvider"

    #@206
    new-instance v10, Ljava/lang/StringBuilder;

    #@208
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@20b
    const-string v11, "unable to parse SUPL_PORT: "

    #@20d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v10

    #@211
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v10

    #@215
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v10

    #@219
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21c
    goto/16 :goto_115

    #@21e
    .line 603
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .restart local v8       #tlsMode:Ljava/lang/String;
    :catch_21e
    move-exception v1

    #@21f
    .line 604
    .restart local v1       #e:Ljava/lang/NumberFormatException;
    const-string v9, "LgeGpsLocationProvider"

    #@221
    new-instance v10, Ljava/lang/StringBuilder;

    #@223
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@226
    const-string v11, "unable to parse TLS_MODE: "

    #@228
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22b
    move-result-object v10

    #@22c
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v10

    #@230
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@233
    move-result-object v10

    #@234
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@237
    .line 605
    const/4 v9, 0x0

    #@238
    iput v9, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I
    :try_end_23a
    .catch Ljava/lang/Exception; {:try_start_1e9 .. :try_end_23a} :catch_1de

    #@23a
    goto/16 :goto_126
.end method

.method private normalLedCtrl(Z)V
    .registers 9
    .parameter "isOn"

    #@0
    .prologue
    const/16 v6, 0x3e8

    #@2
    const/4 v5, 0x7

    #@3
    const/4 v4, 0x1

    #@4
    .line 1492
    const-string v1, "LgeGpsLocationProvider"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "emotionalLedCtrl, isOn:"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1494
    const/4 v0, 0x0

    #@1d
    .line 1496
    .local v0, bNeedChange:Z
    iget-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@1f
    if-nez v1, :cond_79

    #@21
    if-ne p1, v4, :cond_79

    #@23
    .line 1497
    const/4 v0, 0x1

    #@24
    .line 1506
    :goto_24
    if-ne v0, v4, :cond_78

    #@26
    .line 1507
    if-ne p1, v4, :cond_83

    #@28
    .line 1508
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@2a
    const/16 v2, 0xff

    #@2c
    iput v2, v1, Landroid/app/Notification;->ledARGB:I

    #@2e
    .line 1509
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@30
    iput v6, v1, Landroid/app/Notification;->ledOnMS:I

    #@32
    .line 1510
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@34
    iput v6, v1, Landroid/app/Notification;->ledOffMS:I

    #@36
    .line 1511
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@38
    iget v2, v1, Landroid/app/Notification;->flags:I

    #@3a
    or-int/lit8 v2, v2, 0x1

    #@3c
    iput v2, v1, Landroid/app/Notification;->flags:I

    #@3e
    .line 1512
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@40
    iget v2, v1, Landroid/app/Notification;->flags:I

    #@42
    const/high16 v3, -0x8000

    #@44
    or-int/2addr v2, v3

    #@45
    iput v2, v1, Landroid/app/Notification;->flags:I

    #@47
    .line 1513
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@49
    const/4 v2, 0x0

    #@4a
    iput v2, v1, Landroid/app/Notification;->priority:I

    #@4c
    .line 1514
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@4e
    iget v2, v1, Landroid/app/Notification;->flags:I

    #@50
    const/high16 v3, 0x700

    #@52
    or-int/2addr v2, v3

    #@53
    iput v2, v1, Landroid/app/Notification;->flags:I

    #@55
    .line 1515
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->nm:Landroid/app/NotificationManager;

    #@57
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->notification:Landroid/app/Notification;

    #@59
    invoke-virtual {v1, v5, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@5c
    .line 1521
    :goto_5c
    iput-boolean p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@5e
    .line 1523
    const-string v1, "LgeGpsLocationProvider"

    #@60
    new-instance v2, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v3, "emotionalLedCtrl, change to led status:"

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    iget-boolean v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 1525
    :cond_78
    return-void

    #@79
    .line 1499
    :cond_79
    iget-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLedOn:Z

    #@7b
    if-ne v1, v4, :cond_81

    #@7d
    if-nez p1, :cond_81

    #@7f
    .line 1500
    const/4 v0, 0x1

    #@80
    goto :goto_24

    #@81
    .line 1503
    :cond_81
    const/4 v0, 0x0

    #@82
    goto :goto_24

    #@83
    .line 1518
    :cond_83
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->nm:Landroid/app/NotificationManager;

    #@85
    invoke-virtual {v1, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@88
    goto :goto_5c
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .registers 6
    .parameter "message"
    .parameter "arg"
    .parameter "obj"

    #@0
    .prologue
    .line 877
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    #@5
    .line 878
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeHandler:Landroid/os/Handler;

    #@7
    invoke-static {v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 879
    .local v0, m:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 880
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f
    .line 881
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeHandler:Landroid/os/Handler;

    #@11
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@14
    .line 882
    return-void
.end method

.method private sktHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 12
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 656
    const/4 v1, 0x0

    #@3
    .line 657
    .local v1, result:Z
    if-eqz p2, :cond_57

    #@5
    .line 658
    const-string v4, "opType"

    #@7
    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 659
    .local v3, str_opType:Ljava/lang/String;
    const-string v4, "cmdType"

    #@d
    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    .line 661
    .local v2, str_cmdType:Ljava/lang/String;
    const-string v4, "LgeGpsLocationProvider"

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "com.skt.intent.action.AGPS :cmdType: "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    const-string v6, ", opType: "

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 663
    const-string v4, "on"

    #@35
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v4

    #@39
    if-eqz v4, :cond_64

    #@3b
    .line 664
    const-string v4, "msBased"

    #@3d
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_58

    #@43
    .line 667
    new-instance v0, Landroid/os/Bundle;

    #@45
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@48
    .line 668
    .local v0, LgeExtras:Landroid/os/Bundle;
    const-string v4, "time"

    #@4a
    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@4d
    .line 669
    const-string v4, "delete_aiding_data"

    #@4f
    invoke-super {p0, v4, v0}, Lcom/android/server/location/GpsLocationProvider;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@52
    .line 671
    iput v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@54
    .line 676
    .end local v0           #LgeExtras:Landroid/os/Bundle;
    :cond_54
    :goto_54
    iput-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionModeSetted:Z

    #@56
    .line 677
    const/4 v1, 0x1

    #@57
    .line 691
    .end local v2           #str_cmdType:Ljava/lang/String;
    .end local v3           #str_opType:Ljava/lang/String;
    :cond_57
    :goto_57
    return v1

    #@58
    .line 673
    .restart local v2       #str_cmdType:Ljava/lang/String;
    .restart local v3       #str_opType:Ljava/lang/String;
    :cond_58
    const-string v4, "msAssisted"

    #@5a
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v4

    #@5e
    if-eqz v4, :cond_54

    #@60
    .line 674
    const/4 v4, 0x2

    #@61
    iput v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@63
    goto :goto_54

    #@64
    .line 679
    :cond_64
    const-string v4, "off"

    #@66
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v4

    #@6a
    if-eqz v4, :cond_72

    #@6c
    .line 680
    iput v8, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@6e
    .line 681
    iput-boolean v8, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionModeSetted:Z

    #@70
    .line 682
    const/4 v1, 0x1

    #@71
    goto :goto_57

    #@72
    .line 686
    :cond_72
    const-string v4, "LgeGpsLocationProvider"

    #@74
    new-instance v5, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v6, "invalid sktHandleExtraCmd : "

    #@7b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v5

    #@87
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    goto :goto_57
.end method

.method private soundCtrl(Z)V
    .registers 7
    .parameter "isOn"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1529
    const-string v1, "LgeGpsLocationProvider"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "soundCtrl, mSoundOn: "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget-boolean v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSoundOn:Z

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, ", isOn:"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1531
    const/4 v0, 0x0

    #@26
    .line 1532
    .local v0, bNeedChange:Z
    iget-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSoundOn:Z

    #@28
    if-nez v1, :cond_4b

    #@2a
    if-ne p1, v4, :cond_4b

    #@2c
    .line 1533
    const/4 v0, 0x1

    #@2d
    .line 1542
    :goto_2d
    if-ne v0, v4, :cond_4a

    #@2f
    .line 1543
    if-ne p1, v4, :cond_55

    #@31
    .line 1544
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSoundPlayer:Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;

    #@33
    if-nez v1, :cond_3c

    #@35
    .line 1545
    new-instance v1, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;

    #@37
    invoke-direct {v1}, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;-><init>()V

    #@3a
    iput-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSoundPlayer:Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;

    #@3c
    .line 1547
    :cond_3c
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSoundPlayer:Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;

    #@3e
    invoke-virtual {v1}, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->play()V

    #@41
    .line 1548
    const-string v1, "LgeGpsLocationProvider"

    #@43
    const-string v2, "sound start"

    #@45
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1556
    :goto_48
    iput-boolean p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSoundOn:Z

    #@4a
    .line 1558
    :cond_4a
    return-void

    #@4b
    .line 1535
    :cond_4b
    iget-boolean v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSoundOn:Z

    #@4d
    if-ne v1, v4, :cond_53

    #@4f
    if-nez p1, :cond_53

    #@51
    .line 1536
    const/4 v0, 0x1

    #@52
    goto :goto_2d

    #@53
    .line 1539
    :cond_53
    const/4 v0, 0x0

    #@54
    goto :goto_2d

    #@55
    .line 1551
    :cond_55
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSoundPlayer:Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;

    #@57
    if-eqz v1, :cond_5e

    #@59
    .line 1552
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mGPSSoundPlayer:Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;

    #@5b
    invoke-virtual {v1}, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->release()V

    #@5e
    .line 1554
    :cond_5e
    const-string v1, "LgeGpsLocationProvider"

    #@60
    const-string v2, "sound stop"

    #@62
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    goto :goto_48
.end method

.method private soundVibControl()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1428
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@4
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v3

    #@8
    const-string v4, "sound_gps_enabled"

    #@a
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    .line 1432
    .local v2, nSoundSetting:I
    if-ne v2, v5, :cond_3c

    #@10
    iget-boolean v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@12
    if-ne v3, v5, :cond_3c

    #@14
    .line 1433
    iget-object v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@16
    const-string v4, "audio"

    #@18
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/media/AudioManager;

    #@1e
    .line 1434
    .local v0, mAudioManager:Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    #@21
    move-result v1

    #@22
    .line 1435
    .local v1, nRingerMode:I
    const/4 v3, 0x2

    #@23
    if-ne v1, v3, :cond_2c

    #@25
    .line 1436
    invoke-direct {p0, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->soundCtrl(Z)V

    #@28
    .line 1437
    invoke-direct {p0, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->vibratorCtrl(Z)V

    #@2b
    .line 1451
    .end local v0           #mAudioManager:Landroid/media/AudioManager;
    .end local v1           #nRingerMode:I
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 1438
    .restart local v0       #mAudioManager:Landroid/media/AudioManager;
    .restart local v1       #nRingerMode:I
    :cond_2c
    if-ne v1, v5, :cond_35

    #@2e
    .line 1439
    invoke-direct {p0, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->soundCtrl(Z)V

    #@31
    .line 1440
    invoke-direct {p0, v5}, Lcom/android/server/location/LgeGpsLocationProvider;->vibratorCtrl(Z)V

    #@34
    goto :goto_2b

    #@35
    .line 1443
    :cond_35
    invoke-direct {p0, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->soundCtrl(Z)V

    #@38
    .line 1444
    invoke-direct {p0, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->vibratorCtrl(Z)V

    #@3b
    goto :goto_2b

    #@3c
    .line 1447
    .end local v0           #mAudioManager:Landroid/media/AudioManager;
    .end local v1           #nRingerMode:I
    :cond_3c
    if-eqz v2, :cond_42

    #@3e
    iget-boolean v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@40
    if-nez v3, :cond_2b

    #@42
    .line 1448
    :cond_42
    invoke-direct {p0, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->soundCtrl(Z)V

    #@45
    .line 1449
    invoke-direct {p0, v6}, Lcom/android/server/location/LgeGpsLocationProvider;->vibratorCtrl(Z)V

    #@48
    goto :goto_2b
.end method

.method private sprHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 7
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    .line 1041
    const/4 v0, 0x1

    #@1
    .line 1043
    .local v0, ret:Z
    const-string v1, "LgeGpsLocationProvider"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "SPR Framework Extra Command : "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1046
    const-string v1, "spr.gps.extracmd.fmw.pde_addr_init"

    #@1b
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_42

    #@21
    .line 1047
    const-string v1, "LgeGpsLocationProvider"

    #@23
    const-string v2, "pde_addr_init"

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1048
    const-string v1, "lge.gps.extracmd.raw.pde_addr_init"

    #@2a
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@2d
    move-result-object v1

    #@2e
    const-string v2, "lge.gps.extracmd.raw.pde_addr_init"

    #@30
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@33
    move-result v2

    #@34
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@37
    move-result v0

    #@38
    .line 1079
    :goto_38
    if-nez v0, :cond_41

    #@3a
    const-string v1, "LgeGpsLocationProvider"

    #@3c
    const-string v2, "SPR Framework Extra Command Fail!!"

    #@3e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 1081
    :cond_41
    return v0

    #@42
    .line 1050
    :cond_42
    const-string v1, "spr.gps.extracmd.fmw.gpstestprl"

    #@44
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v1

    #@48
    if-eqz v1, :cond_83

    #@4a
    .line 1051
    const-string v1, "gps_test_mode"

    #@4c
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@4f
    move-result v1

    #@50
    const/4 v2, 0x1

    #@51
    if-ne v1, v2, :cond_6b

    #@53
    .line 1053
    const-string v1, "LgeGpsLocationProvider"

    #@55
    const-string v2, "set gps test prl mode : true"

    #@57
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 1054
    const-string v1, "lge.gps.extracmd.raw.spr_gpstestprl.enable"

    #@5c
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@5f
    move-result-object v1

    #@60
    const-string v2, "lge.gps.extracmd.raw.spr_gpstestprl.enable"

    #@62
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@65
    move-result v2

    #@66
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@69
    move-result v0

    #@6a
    goto :goto_38

    #@6b
    .line 1058
    :cond_6b
    const-string v1, "LgeGpsLocationProvider"

    #@6d
    const-string v2, "set gps test prl mode : false"

    #@6f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 1059
    const-string v1, "lge.gps.extracmd.raw.spr_gpstestprl.disable"

    #@74
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@77
    move-result-object v1

    #@78
    const-string v2, "lge.gps.extracmd.raw.spr_gpstestprl.disable"

    #@7a
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@7d
    move-result v2

    #@7e
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@81
    move-result v0

    #@82
    goto :goto_38

    #@83
    .line 1063
    :cond_83
    const-string v1, "spr.gps.extracmd.fmw.dm_set_gps_state"

    #@85
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v1

    #@89
    if-eqz v1, :cond_9c

    #@8b
    .line 1064
    const-string v1, "LgeGpsLocationProvider"

    #@8d
    const-string v2, "dm_set_gps_state"

    #@8f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 1065
    const-string v1, "mode"

    #@94
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@97
    move-result v1

    #@98
    invoke-direct {p0, v1}, Lcom/android/server/location/LgeGpsLocationProvider;->LM_setGpsState(I)V

    #@9b
    goto :goto_38

    #@9c
    .line 1067
    :cond_9c
    const-string v1, "spr.gps.extracmd.fmw.dm_get_gps_state"

    #@9e
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1
    move-result v1

    #@a2
    if-eqz v1, :cond_b5

    #@a4
    .line 1068
    const-string v1, "LgeGpsLocationProvider"

    #@a6
    const-string v2, "dm_get_gps_state"

    #@a8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    .line 1069
    const-string v1, "mode"

    #@ad
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->LM_getGpsState()I

    #@b0
    move-result v2

    #@b1
    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@b4
    goto :goto_38

    #@b5
    .line 1073
    :cond_b5
    const-string v1, "LgeGpsLocationProvider"

    #@b7
    new-instance v2, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v3, "invalid SPR Framework Extra Command : "

    #@be
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v2

    #@c2
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v2

    #@ca
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 1074
    const/4 v0, 0x0

    #@ce
    goto/16 :goto_38
.end method

.method private vibratorCtrl(Z)V
    .registers 12
    .parameter "isOn"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    .line 1562
    const-string v5, "LgeGpsLocationProvider"

    #@4
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v7, "vibratorCtrl, mVibratorOn: "

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    iget-boolean v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mSoundOn:Z

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    const-string v7, ", isOn:"

    #@17
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v6

    #@23
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1564
    const/4 v0, 0x0

    #@27
    .line 1565
    .local v0, bNeedChange:Z
    iget-boolean v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVibratorOn:Z

    #@29
    if-nez v5, :cond_65

    #@2b
    if-ne p1, v8, :cond_65

    #@2d
    .line 1566
    const/4 v0, 0x1

    #@2e
    .line 1575
    :goto_2e
    if-ne v0, v8, :cond_7e

    #@30
    .line 1576
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@32
    if-eqz v5, :cond_7e

    #@34
    .line 1577
    if-ne p1, v8, :cond_7f

    #@36
    .line 1578
    new-array v2, v9, [J

    #@38
    fill-array-data v2, :array_8c

    #@3b
    .line 1579
    .local v2, sVibratePattern:[J
    new-array v3, v9, [I

    #@3d
    .line 1580
    .local v3, vibPatternVol:[I
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@3f
    iget-object v6, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@41
    invoke-interface {v5, v8}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->getVibrateVolume(I)I

    #@44
    move-result v4

    #@45
    .line 1582
    .local v4, vibVol:I
    const-string v5, "LgeGpsLocationProvider"

    #@47
    new-instance v6, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v7, "mVibrator vibVol"

    #@4e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v6

    #@5a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 1583
    const/4 v1, 0x0

    #@5e
    .local v1, i:I
    :goto_5e
    if-ge v1, v9, :cond_6f

    #@60
    .line 1584
    aput v4, v3, v1

    #@62
    .line 1583
    add-int/lit8 v1, v1, 0x1

    #@64
    goto :goto_5e

    #@65
    .line 1568
    .end local v1           #i:I
    .end local v2           #sVibratePattern:[J
    .end local v3           #vibPatternVol:[I
    .end local v4           #vibVol:I
    :cond_65
    iget-boolean v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVibratorOn:Z

    #@67
    if-ne v5, v8, :cond_6d

    #@69
    if-nez p1, :cond_6d

    #@6b
    .line 1569
    const/4 v0, 0x1

    #@6c
    goto :goto_2e

    #@6d
    .line 1572
    :cond_6d
    const/4 v0, 0x0

    #@6e
    goto :goto_2e

    #@6f
    .line 1587
    .restart local v1       #i:I
    .restart local v2       #sVibratePattern:[J
    .restart local v3       #vibPatternVol:[I
    .restart local v4       #vibVol:I
    :cond_6f
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@71
    const/4 v6, 0x0

    #@72
    invoke-interface {v5, v2, v6, v3}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->vibrate([JI[I)V

    #@75
    .line 1588
    const-string v5, "LgeGpsLocationProvider"

    #@77
    const-string v6, "vib start"

    #@79
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 1594
    .end local v1           #i:I
    .end local v2           #sVibratePattern:[J
    .end local v3           #vibPatternVol:[I
    .end local v4           #vibVol:I
    :goto_7c
    iput-boolean p1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVibratorOn:Z

    #@7e
    .line 1597
    :cond_7e
    return-void

    #@7f
    .line 1591
    :cond_7f
    iget-object v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@81
    invoke-interface {v5}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->cancel()V

    #@84
    .line 1592
    const-string v5, "LgeGpsLocationProvider"

    #@86
    const-string v6, "vib stop"

    #@88
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    goto :goto_7c

    #@8c
    .line 1578
    :array_8c
    .array-data 0x8
        0xe8t 0x3t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0xe8t 0x3t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method


# virtual methods
.method public broadcastXtraSettings()V
    .registers 5

    #@0
    .prologue
    .line 939
    new-instance v0, Landroid/content/Intent;

    #@2
    sget-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->INTENT_XTRA_SETTING_CHANGED:Ljava/lang/String;

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 940
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "xtra_download_date"

    #@9
    sget-wide v2, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadDate:J

    #@b
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@e
    .line 941
    const-string v1, "xtra_download_frequency"

    #@10
    sget v2, Lcom/android/server/location/LgeGpsLocationProvider;->mXtraDownloadFrequency:I

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@15
    .line 942
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@17
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1a
    .line 943
    return-void
.end method

.method public dcmHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 10
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 818
    const-string v4, "com.lge.extraCommand.AGPS"

    #@4
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_93

    #@a
    .line 819
    const-string v4, "LgeGpsLocationProvider"

    #@c
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "agpsTestSetting() : extras = "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 820
    if-eqz p2, :cond_93

    #@24
    .line 823
    const-string v4, "supl_address"

    #@26
    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    .line 824
    .local v0, str_supl_address:Ljava/lang/String;
    const-string v4, "supl_Port"

    #@2c
    invoke-virtual {p2, v4, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@2f
    move-result v1

    #@30
    .line 826
    .local v1, supl_port:I
    if-eqz v0, :cond_6c

    #@32
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@35
    move-result v3

    #@36
    const/4 v4, 0x2

    #@37
    if-le v3, v4, :cond_6c

    #@39
    if-eqz v1, :cond_6c

    #@3b
    .line 827
    sput-object v0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@3d
    .line 828
    iput v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@3f
    .line 830
    sget-object v3, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@41
    iget v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@43
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/location/LgeGpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@46
    .line 831
    const-string v3, "LgeGpsLocationProvider"

    #@48
    new-instance v4, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v5, "set supl address = "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    sget-object v5, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    const-string v5, ":"

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    iget v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 834
    :cond_6c
    const-string v3, "LgeGpsLocationProvider"

    #@6e
    new-instance v4, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v5, "agpsTestSetting() : mSuplServerHost = "

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    sget-object v5, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@7b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    const-string v5, ",Host Port ="

    #@81
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v4

    #@85
    iget v5, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@87
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 838
    .end local v0           #str_supl_address:Ljava/lang/String;
    .end local v1           #supl_port:I
    :goto_92
    return v2

    #@93
    :cond_93
    move v2, v3

    #@94
    goto :goto_92
.end method

.method public handleEnable(I)V
    .registers 7
    .parameter "provider_message_type"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 344
    invoke-super {p0, p1}, Lcom/android/server/location/GpsLocationProvider;->handleEnable(I)V

    #@4
    .line 346
    sget-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@6
    if-eqz v1, :cond_5d

    #@8
    .line 347
    sget-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@a
    iget v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@c
    invoke-virtual {p0, v3, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@f
    .line 348
    const-string v1, "lge.gps.extracmd.raw.tls.mode#%d"

    #@11
    new-array v2, v3, [Ljava/lang/Object;

    #@13
    const/4 v3, 0x0

    #@14
    iget v4, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@16
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v4

    #@1a
    aput-object v4, v2, v3

    #@1c
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 349
    .local v0, LgeTlsMode:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@27
    move-result v2

    #@28
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@2b
    .line 351
    const-string v1, "LgeGpsLocationProvider"

    #@2d
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v3, "handleEnable :native_set_agps_server ="

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    sget-object v3, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    const-string v3, ":"

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    iget v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, ", TlsMode ="

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    iget v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 354
    .end local v0           #LgeTlsMode:Ljava/lang/String;
    :cond_5d
    return-void
.end method

.method public lgeSetSlpFromSim(II)V
    .registers 11
    .parameter "mcc_i"
    .parameter "mnc_i"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v6, 0x1c6b

    #@3
    const/16 v5, 0xa

    #@5
    const/4 v4, 0x2

    #@6
    const/4 v3, 0x1

    #@7
    .line 1314
    iget v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplType:I

    #@9
    if-ne v1, v3, :cond_c

    #@b
    .line 1365
    :goto_b
    return-void

    #@c
    .line 1316
    :cond_c
    if-ne p1, v3, :cond_10

    #@e
    if-eq p2, v3, :cond_14

    #@10
    :cond_10
    if-ne p1, v3, :cond_1c

    #@12
    if-ne p2, v4, :cond_1c

    #@14
    .line 1318
    :cond_14
    const-string v1, "LgeGpsLocationProvider"

    #@16
    const-string v2, "Spirent SIM inserted, ignore Slp setting from SIM"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_b

    #@1c
    .line 1322
    :cond_1c
    const-string v1, "VDF"

    #@1e
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_b4

    #@26
    .line 1323
    const/16 v1, 0x106

    #@28
    if-ne p1, v1, :cond_2c

    #@2a
    if-eq p2, v4, :cond_6e

    #@2c
    :cond_2c
    const/16 v1, 0xd6

    #@2e
    if-ne p1, v1, :cond_32

    #@30
    if-eq p2, v3, :cond_6e

    #@32
    :cond_32
    const/16 v1, 0xd6

    #@34
    if-ne p1, v1, :cond_39

    #@36
    const/4 v1, 0x6

    #@37
    if-eq p2, v1, :cond_6e

    #@39
    :cond_39
    const/16 v1, 0xde

    #@3b
    if-ne p1, v1, :cond_3f

    #@3d
    if-eq p2, v5, :cond_6e

    #@3f
    :cond_3f
    const/16 v1, 0xcc

    #@41
    if-ne p1, v1, :cond_46

    #@43
    const/4 v1, 0x4

    #@44
    if-eq p2, v1, :cond_6e

    #@46
    :cond_46
    const/16 v1, 0x10c

    #@48
    if-ne p1, v1, :cond_4c

    #@4a
    if-eq p2, v3, :cond_6e

    #@4c
    :cond_4c
    const/16 v1, 0xe2

    #@4e
    if-ne p1, v1, :cond_52

    #@50
    if-eq p2, v3, :cond_6e

    #@52
    :cond_52
    const/16 v1, 0xca

    #@54
    if-ne p1, v1, :cond_59

    #@56
    const/4 v1, 0x5

    #@57
    if-eq p2, v1, :cond_6e

    #@59
    :cond_59
    const/16 v1, 0x1f9

    #@5b
    if-ne p1, v1, :cond_60

    #@5d
    const/4 v1, 0x3

    #@5e
    if-eq p2, v1, :cond_6e

    #@60
    :cond_60
    const/16 v1, 0x114

    #@62
    if-ne p1, v1, :cond_66

    #@64
    if-eq p2, v4, :cond_6e

    #@66
    :cond_66
    const/16 v1, 0xea

    #@68
    if-ne p1, v1, :cond_9a

    #@6a
    const/16 v1, 0xf

    #@6c
    if-ne p2, v1, :cond_9a

    #@6e
    .line 1335
    :cond_6e
    const-string v1, "supl.vodafone.com"

    #@70
    sput-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@72
    .line 1336
    iput v6, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@74
    .line 1337
    iput v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@76
    .line 1360
    :cond_76
    :goto_76
    sget-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@78
    iget v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@7a
    invoke-virtual {p0, v3, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_set_agps_server(ILjava/lang/String;I)V

    #@7d
    .line 1362
    const-string v1, "lge.gps.extracmd.raw.tls.mode#%d"

    #@7f
    new-array v2, v3, [Ljava/lang/Object;

    #@81
    iget v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@83
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@86
    move-result-object v3

    #@87
    aput-object v3, v2, v7

    #@89
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@8c
    move-result-object v0

    #@8d
    .line 1363
    .local v0, LgeTlsMode:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@94
    move-result v2

    #@95
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@98
    goto/16 :goto_b

    #@9a
    .line 1339
    .end local v0           #LgeTlsMode:Ljava/lang/String;
    :cond_9a
    const/16 v1, 0xd0

    #@9c
    if-ne p1, v1, :cond_a9

    #@9e
    if-ne p2, v5, :cond_a9

    #@a0
    .line 1341
    const-string v1, "supl.google.com"

    #@a2
    sput-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@a4
    .line 1342
    iput v6, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@a6
    .line 1343
    iput v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@a8
    goto :goto_76

    #@a9
    .line 1346
    :cond_a9
    const-string v1, "supl.google.com"

    #@ab
    sput-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@ad
    .line 1347
    const/16 v1, 0x1c6c

    #@af
    iput v1, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@b1
    .line 1348
    iput v7, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@b3
    goto :goto_76

    #@b4
    .line 1351
    :cond_b4
    const-string v1, "SFR"

    #@b6
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@b8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bb
    move-result v1

    #@bc
    if-eqz v1, :cond_76

    #@be
    .line 1352
    const/16 v1, 0xd0

    #@c0
    if-ne p1, v1, :cond_76

    #@c2
    if-ne p2, v5, :cond_76

    #@c4
    .line 1354
    const-string v1, "supl.google.com"

    #@c6
    sput-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerHost:Ljava/lang/String;

    #@c8
    .line 1355
    iput v6, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeSuplServerPort:I

    #@ca
    .line 1356
    iput v3, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeTlsMode:I

    #@cc
    goto :goto_76
.end method

.method public lgeUpdateSimState(Landroid/content/Intent;)V
    .registers 10
    .parameter "intent"

    #@0
    .prologue
    .line 1279
    const-string v5, "ss"

    #@2
    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    .line 1281
    .local v4, stateExtra:Ljava/lang/String;
    const/4 v3, 0x0

    #@7
    .line 1283
    .local v3, numeric:Ljava/lang/String;
    const-string v5, "LgeGpsLocationProvider"

    #@9
    new-instance v6, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v7, "lgeUpdateSimState : state="

    #@10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v6

    #@1c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1285
    const-string v5, "LOADED"

    #@21
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v5

    #@25
    if-eqz v5, :cond_a4

    #@27
    .line 1289
    const-string v5, "gsm.sim.operator.numeric"

    #@29
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    .line 1290
    const-string v5, "LgeGpsLocationProvider"

    #@2f
    new-instance v6, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v7, " getMccCode numeric "

    #@36
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    const-string v7, "\n"

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1291
    if-eqz v3, :cond_9c

    #@4d
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@50
    move-result v5

    #@51
    const/4 v6, 0x4

    #@52
    if-le v5, v6, :cond_9c

    #@54
    .line 1294
    const/4 v5, 0x0

    #@55
    const/4 v6, 0x3

    #@56
    :try_start_56
    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5d
    move-result v1

    #@5e
    .line 1295
    .local v1, mcc_i:I
    const/4 v5, 0x3

    #@5f
    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@66
    move-result v2

    #@67
    .line 1296
    .local v2, mnc_i:I
    const-string v5, "LgeGpsLocationProvider"

    #@69
    new-instance v6, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v7, "- getMccCode mcc = "

    #@70
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    const-string v7, " mnc = "

    #@7a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    const-string v7, "\n"

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v6

    #@8c
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 1297
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->lgeSetSlpFromSim(II)V
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_56 .. :try_end_92} :catch_93

    #@92
    .line 1309
    .end local v1           #mcc_i:I
    .end local v2           #mnc_i:I
    :goto_92
    return-void

    #@93
    .line 1299
    :catch_93
    move-exception v0

    #@94
    .line 1300
    .local v0, ex:Ljava/lang/Exception;
    const-string v5, "LgeGpsLocationProvider"

    #@96
    const-string v6, "Exception parsing mcc/mnc: "

    #@98
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    goto :goto_92

    #@9c
    .line 1304
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_9c
    const-string v5, "LgeGpsLocationProvider"

    #@9e
    const-string v6, "- getMccCode numeric is null or length is less than 4\n"

    #@a0
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto :goto_92

    #@a4
    .line 1308
    :cond_a4
    const-string v5, "LgeGpsLocationProvider"

    #@a6
    const-string v6, "ICC records Load is not complete"

    #@a8
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto :goto_92
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 6
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    .line 424
    const/4 v0, 0x0

    #@1
    .line 426
    .local v0, result:Z
    const-string v1, "lge.gps.extracmd.fmw"

    #@3
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_14

    #@9
    .line 427
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/LgeGpsLocationProvider;->lgeHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@c
    move-result v0

    #@d
    .line 448
    :cond_d
    :goto_d
    if-nez v0, :cond_13

    #@f
    .line 449
    invoke-super {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@12
    move-result v0

    #@13
    .line 451
    .end local v0           #result:Z
    :cond_13
    return v0

    #@14
    .line 429
    .restart local v0       #result:Z
    :cond_14
    const-string v1, "lge.gps.extracmd.raw"

    #@16
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_29

    #@1c
    .line 430
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@23
    move-result v2

    #@24
    invoke-virtual {p0, v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->native_lge_gnss_command([BI)Z

    #@27
    move-result v0

    #@28
    goto :goto_d

    #@29
    .line 432
    :cond_29
    const-string v1, "SKT"

    #@2b
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v1

    #@31
    if-eqz v1, :cond_38

    #@33
    .line 433
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/LgeGpsLocationProvider;->sktHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@36
    move-result v0

    #@37
    goto :goto_d

    #@38
    .line 435
    :cond_38
    const-string v1, "KT"

    #@3a
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_47

    #@42
    .line 436
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/LgeGpsLocationProvider;->ktHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@45
    move-result v0

    #@46
    goto :goto_d

    #@47
    .line 438
    :cond_47
    const-string v1, "LGU"

    #@49
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_56

    #@51
    .line 439
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/LgeGpsLocationProvider;->lguHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@54
    move-result v0

    #@55
    goto :goto_d

    #@56
    .line 441
    :cond_56
    const-string v1, "DCM"

    #@58
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v1

    #@5e
    if-eqz v1, :cond_65

    #@60
    .line 442
    invoke-virtual {p0, p1, p2}, Lcom/android/server/location/LgeGpsLocationProvider;->dcmHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@63
    move-result v0

    #@64
    goto :goto_d

    #@65
    .line 444
    :cond_65
    const-string v1, "SPR"

    #@67
    sget-object v2, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v1

    #@6d
    if-eqz v1, :cond_d

    #@6f
    .line 445
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/LgeGpsLocationProvider;->sprHandleExtraCmd(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@72
    move-result v0

    #@73
    goto :goto_d
.end method

.method public setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 6
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    .line 359
    const-string v0, "LgeGpsLocationProvider"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "enableLocationTracking - mPositionMode = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mPositionMode:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 360
    iget-boolean v0, p1, Lcom/android/internal/location/ProviderRequest;->reportLocation:Z

    #@1c
    if-eqz v0, :cond_35

    #@1e
    .line 362
    const-string v0, "DCM"

    #@20
    sget-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_31

    #@28
    .line 364
    const/4 v0, 0x1

    #@29
    iput-boolean v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@2b
    .line 365
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->ledControl()V

    #@2e
    .line 366
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->soundVibControl()V

    #@31
    .line 390
    :cond_31
    :goto_31
    invoke-super {p0, p1, p2}, Lcom/android/server/location/GpsLocationProvider;->setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V

    #@34
    .line 391
    return-void

    #@35
    .line 384
    :cond_35
    const-string v0, "DCM"

    #@37
    sget-object v1, Lcom/android/server/location/LgeGpsLocationProvider;->mVendorName:Ljava/lang/String;

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v0

    #@3d
    if-eqz v0, :cond_31

    #@3f
    .line 385
    const/4 v0, 0x0

    #@40
    iput-boolean v0, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mTracking:Z

    #@42
    .line 386
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->ledControl()V

    #@45
    .line 387
    invoke-direct {p0}, Lcom/android/server/location/LgeGpsLocationProvider;->soundVibControl()V

    #@48
    goto :goto_31
.end method

.method public setUseLocationForServices(Z)V
    .registers 6
    .parameter "use"

    #@0
    .prologue
    .line 1171
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1172
    .local v1, i:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@9
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@c
    .line 1173
    const-string v3, "disable"

    #@e
    if-nez p1, :cond_1a

    #@10
    const/4 v2, 0x1

    #@11
    :goto_11
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@14
    .line 1175
    :try_start_14
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider;->mLgeContext:Landroid/content/Context;

    #@16
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_19
    .catch Landroid/content/ActivityNotFoundException; {:try_start_14 .. :try_end_19} :catch_1c

    #@19
    .line 1179
    :goto_19
    return-void

    #@1a
    .line 1173
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_11

    #@1c
    .line 1176
    :catch_1c
    move-exception v0

    #@1d
    .line 1177
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "DeviceManager"

    #@1f
    const-string v3, "Problem while starting GSF location activity"

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_19
.end method
