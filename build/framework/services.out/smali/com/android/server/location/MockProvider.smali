.class public Lcom/android/server/location/MockProvider;
.super Ljava/lang/Object;
.source "MockProvider.java"

# interfaces
.implements Lcom/android/server/location/LocationProviderInterface;


# static fields
.field private static final TAG:Ljava/lang/String; = "MockProvider"


# instance fields
.field private mEnabled:Z

.field private final mExtras:Landroid/os/Bundle;

.field private mHasLocation:Z

.field private mHasStatus:Z

.field private final mLocation:Landroid/location/Location;

.field private final mLocationManager:Landroid/location/ILocationManager;

.field private final mName:Ljava/lang/String;

.field private final mProperties:Lcom/android/internal/location/ProviderProperties;

.field private mStatus:I

.field private mStatusUpdateTime:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/location/ILocationManager;Lcom/android/internal/location/ProviderProperties;)V
    .registers 6
    .parameter "name"
    .parameter "locationManager"
    .parameter "properties"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 47
    new-instance v0, Landroid/os/Bundle;

    #@5
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/location/MockProvider;->mExtras:Landroid/os/Bundle;

    #@a
    .line 59
    if-nez p3, :cond_14

    #@c
    new-instance v0, Ljava/lang/NullPointerException;

    #@e
    const-string v1, "properties is null"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 61
    :cond_14
    iput-object p1, p0, Lcom/android/server/location/MockProvider;->mName:Ljava/lang/String;

    #@16
    .line 62
    iput-object p2, p0, Lcom/android/server/location/MockProvider;->mLocationManager:Landroid/location/ILocationManager;

    #@18
    .line 63
    iput-object p3, p0, Lcom/android/server/location/MockProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@1a
    .line 64
    new-instance v0, Landroid/location/Location;

    #@1c
    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@1f
    iput-object v0, p0, Lcom/android/server/location/MockProvider;->mLocation:Landroid/location/Location;

    #@21
    .line 65
    return-void
.end method


# virtual methods
.method public clearLocation()V
    .registers 2

    #@0
    .prologue
    .line 121
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/location/MockProvider;->mHasLocation:Z

    #@3
    .line 122
    return-void
.end method

.method public clearStatus()V
    .registers 3

    #@0
    .prologue
    .line 135
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/location/MockProvider;->mHasStatus:Z

    #@3
    .line 136
    const-wide/16 v0, 0x0

    #@5
    iput-wide v0, p0, Lcom/android/server/location/MockProvider;->mStatusUpdateTime:J

    #@7
    .line 137
    return-void
.end method

.method public disable()V
    .registers 2

    #@0
    .prologue
    .line 79
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/location/MockProvider;->mEnabled:Z

    #@3
    .line 80
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 141
    const-string v0, ""

    #@2
    invoke-virtual {p0, p2, v0}, Lcom/android/server/location/MockProvider;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@5
    .line 142
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 7
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/server/location/MockProvider;->mName:Ljava/lang/String;

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, "mHasLocation="

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    iget-boolean v1, p0, Lcom/android/server/location/MockProvider;->mHasLocation:Z

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, "mLocation:"

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@48
    .line 148
    iget-object v0, p0, Lcom/android/server/location/MockProvider;->mLocation:Landroid/location/Location;

    #@4a
    new-instance v1, Landroid/util/PrintWriterPrinter;

    #@4c
    invoke-direct {v1, p1}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@4f
    new-instance v2, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v3, "  "

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@65
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    const-string v1, "mHasStatus="

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    iget-boolean v1, p0, Lcom/android/server/location/MockProvider;->mHasStatus:Z

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@79
    move-result-object v0

    #@7a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v0

    #@7e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@81
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v0

    #@8a
    const-string v1, "mStatus="

    #@8c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v0

    #@90
    iget v1, p0, Lcom/android/server/location/MockProvider;->mStatus:I

    #@92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@95
    move-result-object v0

    #@96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v0

    #@9a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9d
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v0

    #@a6
    const-string v1, "mStatusUpdateTime="

    #@a8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v0

    #@ac
    iget-wide v1, p0, Lcom/android/server/location/MockProvider;->mStatusUpdateTime:J

    #@ae
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v0

    #@b2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v0

    #@b6
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b9
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v0

    #@c2
    const-string v1, "mExtras="

    #@c4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v0

    #@c8
    iget-object v1, p0, Lcom/android/server/location/MockProvider;->mExtras:Landroid/os/Bundle;

    #@ca
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v0

    #@ce
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v0

    #@d2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d5
    .line 153
    return-void
.end method

.method public enable()V
    .registers 2

    #@0
    .prologue
    .line 84
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/location/MockProvider;->mEnabled:Z

    #@3
    .line 85
    return-void
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/location/MockProvider;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getProperties()Lcom/android/internal/location/ProviderProperties;
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/server/location/MockProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    return-object v0
.end method

.method public getStatus(Landroid/os/Bundle;)I
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/android/server/location/MockProvider;->mHasStatus:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 95
    invoke-virtual {p1}, Landroid/os/Bundle;->clear()V

    #@7
    .line 96
    iget-object v0, p0, Lcom/android/server/location/MockProvider;->mExtras:Landroid/os/Bundle;

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    #@c
    .line 97
    iget v0, p0, Lcom/android/server/location/MockProvider;->mStatus:I

    #@e
    .line 99
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x2

    #@10
    goto :goto_e
.end method

.method public getStatusUpdateTime()J
    .registers 3

    #@0
    .prologue
    .line 105
    iget-wide v0, p0, Lcom/android/server/location/MockProvider;->mStatusUpdateTime:J

    #@2
    return-wide v0
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/android/server/location/MockProvider;->mEnabled:Z

    #@2
    return v0
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    .line 165
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setLocation(Landroid/location/Location;)V
    .registers 6
    .parameter "l"

    #@0
    .prologue
    .line 109
    iget-object v1, p0, Lcom/android/server/location/MockProvider;->mLocation:Landroid/location/Location;

    #@2
    invoke-virtual {v1, p1}, Landroid/location/Location;->set(Landroid/location/Location;)V

    #@5
    .line 110
    const/4 v1, 0x1

    #@6
    iput-boolean v1, p0, Lcom/android/server/location/MockProvider;->mHasLocation:Z

    #@8
    .line 111
    iget-boolean v1, p0, Lcom/android/server/location/MockProvider;->mEnabled:Z

    #@a
    if-eqz v1, :cond_14

    #@c
    .line 113
    :try_start_c
    iget-object v1, p0, Lcom/android/server/location/MockProvider;->mLocationManager:Landroid/location/ILocationManager;

    #@e
    iget-object v2, p0, Lcom/android/server/location/MockProvider;->mLocation:Landroid/location/Location;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-interface {v1, v2, v3}, Landroid/location/ILocationManager;->reportLocation(Landroid/location/Location;Z)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_14} :catch_15

    #@14
    .line 118
    :cond_14
    :goto_14
    return-void

    #@15
    .line 114
    :catch_15
    move-exception v0

    #@16
    .line 115
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "MockProvider"

    #@18
    const-string v2, "RemoteException calling reportLocation"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_14
.end method

.method public setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 3
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    .line 156
    return-void
.end method

.method public setStatus(ILandroid/os/Bundle;J)V
    .registers 6
    .parameter "status"
    .parameter "extras"
    .parameter "updateTime"

    #@0
    .prologue
    .line 125
    iput p1, p0, Lcom/android/server/location/MockProvider;->mStatus:I

    #@2
    .line 126
    iput-wide p3, p0, Lcom/android/server/location/MockProvider;->mStatusUpdateTime:J

    #@4
    .line 127
    iget-object v0, p0, Lcom/android/server/location/MockProvider;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    #@9
    .line 128
    if-eqz p2, :cond_10

    #@b
    .line 129
    iget-object v0, p0, Lcom/android/server/location/MockProvider;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    #@10
    .line 131
    :cond_10
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Lcom/android/server/location/MockProvider;->mHasStatus:Z

    #@13
    .line 132
    return-void
.end method

.method public switchUser(I)V
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 161
    return-void
.end method
