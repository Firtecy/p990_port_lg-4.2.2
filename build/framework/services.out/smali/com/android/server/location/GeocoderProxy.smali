.class public Lcom/android/server/location/GeocoderProxy;
.super Ljava/lang/Object;
.source "GeocoderProxy.java"


# static fields
.field private static final SERVICE_ACTION:Ljava/lang/String; = "com.android.location.service.GeocodeProvider"

.field private static final TAG:Ljava/lang/String; = "GeocoderProxy"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mServiceWatcher:Lcom/android/server/ServiceWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I)V
    .registers 12
    .parameter "context"
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, initialPackageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    #@1
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 52
    iput-object p1, p0, Lcom/android/server/location/GeocoderProxy;->mContext:Landroid/content/Context;

    #@6
    .line 54
    new-instance v0, Lcom/android/server/ServiceWatcher;

    #@8
    iget-object v1, p0, Lcom/android/server/location/GeocoderProxy;->mContext:Landroid/content/Context;

    #@a
    const-string v2, "GeocoderProxy"

    #@c
    const-string v3, "com.android.location.service.GeocodeProvider"

    #@e
    move-object v4, p2

    #@f
    move-object v6, v5

    #@10
    move v7, p3

    #@11
    invoke-direct/range {v0 .. v7}, Lcom/android/server/ServiceWatcher;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Runnable;Landroid/os/Handler;I)V

    #@14
    iput-object v0, p0, Lcom/android/server/location/GeocoderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@16
    .line 56
    return-void
.end method

.method private bind()Z
    .registers 2

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/server/location/GeocoderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/server/ServiceWatcher;->start()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static createAndBind(Landroid/content/Context;Ljava/util/List;I)Lcom/android/server/location/GeocoderProxy;
    .registers 5
    .parameter "context"
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Lcom/android/server/location/GeocoderProxy;"
        }
    .end annotation

    #@0
    .prologue
    .line 43
    .local p1, initialPackageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/android/server/location/GeocoderProxy;

    #@2
    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/location/GeocoderProxy;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    #@5
    .line 44
    .local v0, proxy:Lcom/android/server/location/GeocoderProxy;
    invoke-direct {v0}, Lcom/android/server/location/GeocoderProxy;->bind()Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    .line 47
    .end local v0           #proxy:Lcom/android/server/location/GeocoderProxy;
    :goto_b
    return-object v0

    #@c
    .restart local v0       #proxy:Lcom/android/server/location/GeocoderProxy;
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private getService()Landroid/location/IGeocodeProvider;
    .registers 2

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/location/GeocoderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/server/ServiceWatcher;->getBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/location/IGeocodeProvider$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/IGeocodeProvider;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method


# virtual methods
.method public getConnectedPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/location/GeocoderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFromLocation(DDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;
    .registers 17
    .parameter "latitude"
    .parameter "longitude"
    .parameter "maxResults"
    .parameter "params"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI",
            "Landroid/location/GeocoderParams;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 72
    .local p7, addrs:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    invoke-direct {p0}, Lcom/android/server/location/GeocoderProxy;->getService()Landroid/location/IGeocodeProvider;

    #@3
    move-result-object v0

    #@4
    .line 73
    .local v0, provider:Landroid/location/IGeocodeProvider;
    if-eqz v0, :cond_17

    #@6
    move-wide v1, p1

    #@7
    move-wide v3, p3

    #@8
    move v5, p5

    #@9
    move-object v6, p6

    #@a
    move-object/from16 v7, p7

    #@c
    .line 75
    :try_start_c
    invoke-interface/range {v0 .. v7}, Landroid/location/IGeocodeProvider;->getFromLocation(DDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_f} :catch_11

    #@f
    move-result-object v1

    #@10
    .line 80
    :goto_10
    return-object v1

    #@11
    .line 76
    :catch_11
    move-exception v8

    #@12
    .line 77
    .local v8, e:Landroid/os/RemoteException;
    const-string v1, "GeocoderProxy"

    #@14
    invoke-static {v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 80
    .end local v8           #e:Landroid/os/RemoteException;
    :cond_17
    const-string v1, "Service not Available"

    #@19
    goto :goto_10
.end method

.method public getFromLocationName(Ljava/lang/String;DDDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;
    .registers 27
    .parameter "locationName"
    .parameter "lowerLeftLatitude"
    .parameter "lowerLeftLongitude"
    .parameter "upperRightLatitude"
    .parameter "upperRightLongitude"
    .parameter "maxResults"
    .parameter "params"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DDDDI",
            "Landroid/location/GeocoderParams;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 87
    .local p12, addrs:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    invoke-direct {p0}, Lcom/android/server/location/GeocoderProxy;->getService()Landroid/location/IGeocodeProvider;

    #@3
    move-result-object v0

    #@4
    .line 88
    .local v0, provider:Landroid/location/IGeocodeProvider;
    if-eqz v0, :cond_20

    #@6
    move-object v1, p1

    #@7
    move-wide/from16 v2, p2

    #@9
    move-wide/from16 v4, p4

    #@b
    move-wide/from16 v6, p6

    #@d
    move-wide/from16 v8, p8

    #@f
    move/from16 v10, p10

    #@11
    move-object/from16 v11, p11

    #@13
    move-object/from16 v12, p12

    #@15
    .line 90
    :try_start_15
    invoke-interface/range {v0 .. v12}, Landroid/location/IGeocodeProvider;->getFromLocationName(Ljava/lang/String;DDDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_18} :catch_1a

    #@18
    move-result-object v1

    #@19
    .line 97
    :goto_19
    return-object v1

    #@1a
    .line 93
    :catch_1a
    move-exception v13

    #@1b
    .line 94
    .local v13, e:Landroid/os/RemoteException;
    const-string v1, "GeocoderProxy"

    #@1d
    invoke-static {v1, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@20
    .line 97
    .end local v13           #e:Landroid/os/RemoteException;
    :cond_20
    const-string v1, "Service not Available"

    #@22
    goto :goto_19
.end method
