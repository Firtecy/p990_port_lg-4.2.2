.class final Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;
.super Landroid/os/Handler;
.source "LgeGpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/LgeGpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LgeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/LgeGpsLocationProvider;


# direct methods
.method private constructor <init>(Lcom/android/server/location/LgeGpsLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 841
    iput-object p1, p0, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/LgeGpsLocationProvider;Lcom/android/server/location/LgeGpsLocationProvider$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 841
    invoke-direct {p0, p1}, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;-><init>(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 844
    const-string v0, "LgeGpsLocationProvider"

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "handleMessage : msg = "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    iget v2, p1, Landroid/os/Message;->what:I

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 846
    iget v0, p1, Landroid/os/Message;->what:I

    #@1e
    sparse-switch v0, :sswitch_data_8c

    #@21
    .line 874
    :goto_21
    return-void

    #@22
    .line 848
    :sswitch_22
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@24
    if-ne v0, v4, :cond_36

    #@26
    .line 849
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@28
    invoke-static {v0}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2f
    move-result-object v0

    #@30
    const-string v1, "gps"

    #@32
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@35
    goto :goto_21

    #@36
    .line 851
    :cond_36
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@38
    invoke-static {v0}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3f
    move-result-object v0

    #@40
    const-string v1, "gps"

    #@42
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@45
    goto :goto_21

    #@46
    .line 856
    :sswitch_46
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@48
    if-ne v0, v4, :cond_5a

    #@4a
    .line 857
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@4c
    invoke-static {v0}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@53
    move-result-object v0

    #@54
    const-string v1, "assisted_gps_enabled"

    #@56
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@59
    goto :goto_21

    #@5a
    .line 859
    :cond_5a
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@5c
    invoke-static {v0}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@5f
    move-result-object v0

    #@60
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@63
    move-result-object v0

    #@64
    const-string v1, "assisted_gps_enabled"

    #@66
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@69
    goto :goto_21

    #@6a
    .line 870
    :sswitch_6a
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@6c
    invoke-static {v0}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@6f
    move-result-object v0

    #@70
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$LgeHandler;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@72
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@75
    move-result-object v1

    #@76
    const v2, 0x20902e4

    #@79
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7c
    move-result-object v1

    #@7d
    new-array v2, v3, [Ljava/lang/Object;

    #@7f
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@86
    move-result-object v0

    #@87
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@8a
    goto :goto_21

    #@8b
    .line 846
    nop

    #@8c
    :sswitch_data_8c
    .sparse-switch
        0x1 -> :sswitch_22
        0x2 -> :sswitch_46
        0xa -> :sswitch_6a
    .end sparse-switch
.end method
