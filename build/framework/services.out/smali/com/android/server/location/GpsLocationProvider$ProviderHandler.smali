.class final Lcom/android/server/location/GpsLocationProvider$ProviderHandler;
.super Landroid/os/Handler;
.source "GpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ProviderHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method public constructor <init>(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 2066
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    .line 2067
    const/4 v0, 0x1

    #@3
    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Z)V

    #@6
    .line 2068
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    .line 2072
    iget v1, p1, Landroid/os/Message;->what:I

    #@4
    .line 2073
    .local v1, message:I
    packed-switch v1, :pswitch_data_76

    #@7
    .line 2113
    :cond_7
    :goto_7
    :pswitch_7
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@9
    if-ne v2, v5, :cond_14

    #@b
    .line 2115
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@d
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$2900(Lcom/android/server/location/GpsLocationProvider;)Landroid/os/PowerManager$WakeLock;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@14
    .line 2117
    :cond_14
    return-void

    #@15
    .line 2075
    :pswitch_15
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@17
    if-ne v2, v5, :cond_21

    #@19
    .line 2078
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@1b
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@1d
    invoke-virtual {v2, v3}, Lcom/android/server/location/GpsLocationProvider;->handleEnable(I)V

    #@20
    goto :goto_7

    #@21
    .line 2082
    :cond_21
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@23
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$4200(Lcom/android/server/location/GpsLocationProvider;)V

    #@26
    goto :goto_7

    #@27
    .line 2086
    :pswitch_27
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29
    check-cast v0, Lcom/android/server/location/GpsLocationProvider$GpsRequest;

    #@2b
    .line 2087
    .local v0, gpsRequest:Lcom/android/server/location/GpsLocationProvider$GpsRequest;
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2d
    iget-object v3, v0, Lcom/android/server/location/GpsLocationProvider$GpsRequest;->request:Lcom/android/internal/location/ProviderRequest;

    #@2f
    iget-object v4, v0, Lcom/android/server/location/GpsLocationProvider$GpsRequest;->source:Landroid/os/WorkSource;

    #@31
    invoke-static {v2, v3, v4}, Lcom/android/server/location/GpsLocationProvider;->access$4300(Lcom/android/server/location/GpsLocationProvider;Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V

    #@34
    goto :goto_7

    #@35
    .line 2090
    .end local v0           #gpsRequest:Lcom/android/server/location/GpsLocationProvider$GpsRequest;
    :pswitch_35
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@37
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@39
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3b
    check-cast v2, Landroid/net/NetworkInfo;

    #@3d
    invoke-static {v3, v4, v2}, Lcom/android/server/location/GpsLocationProvider;->access$4400(Lcom/android/server/location/GpsLocationProvider;ILandroid/net/NetworkInfo;)V

    #@40
    goto :goto_7

    #@41
    .line 2093
    :pswitch_41
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@43
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$4500(Lcom/android/server/location/GpsLocationProvider;)V

    #@46
    goto :goto_7

    #@47
    .line 2096
    :pswitch_47
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@49
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$4600(Lcom/android/server/location/GpsLocationProvider;)Z

    #@4c
    move-result v2

    #@4d
    if-eqz v2, :cond_7

    #@4f
    .line 2097
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@51
    invoke-static {v2}, Lcom/android/server/location/GpsLocationProvider;->access$4700(Lcom/android/server/location/GpsLocationProvider;)V

    #@54
    goto :goto_7

    #@55
    .line 2101
    :pswitch_55
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@57
    invoke-static {v2, v3}, Lcom/android/server/location/GpsLocationProvider;->access$4802(Lcom/android/server/location/GpsLocationProvider;I)I

    #@5a
    goto :goto_7

    #@5b
    .line 2104
    :pswitch_5b
    iget-object v2, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@5d
    invoke-static {v2, v3}, Lcom/android/server/location/GpsLocationProvider;->access$4902(Lcom/android/server/location/GpsLocationProvider;I)I

    #@60
    goto :goto_7

    #@61
    .line 2107
    :pswitch_61
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@63
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@65
    check-cast v2, Landroid/location/Location;

    #@67
    invoke-static {v3, v2}, Lcom/android/server/location/GpsLocationProvider;->access$5000(Lcom/android/server/location/GpsLocationProvider;Landroid/location/Location;)V

    #@6a
    goto :goto_7

    #@6b
    .line 2110
    :pswitch_6b
    iget-object v3, p0, Lcom/android/server/location/GpsLocationProvider$ProviderHandler;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@6d
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6f
    check-cast v2, Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;

    #@71
    invoke-static {v3, v2}, Lcom/android/server/location/GpsLocationProvider;->access$5100(Lcom/android/server/location/GpsLocationProvider;Lcom/android/server/location/GpsLocationProvider$ReportAgpsStatusMessage;)V

    #@74
    goto :goto_7

    #@75
    .line 2073
    nop

    #@76
    :pswitch_data_76
    .packed-switch 0x2
        :pswitch_15
        :pswitch_27
        :pswitch_35
        :pswitch_41
        :pswitch_47
        :pswitch_61
        :pswitch_7
        :pswitch_7
        :pswitch_55
        :pswitch_5b
        :pswitch_6b
    .end packed-switch
.end method
