.class public Lcom/android/server/location/ComprehensiveCountryDetector;
.super Lcom/android/server/location/CountryDetectorBase;
.source "ComprehensiveCountryDetector.java"


# static fields
.field static final DEBUG:Z = false

.field private static final LOCATION_REFRESH_INTERVAL:J = 0x5265c00L

.field private static final MAX_LENGTH_DEBUG_LOGS:I = 0x14

.field private static final TAG:Ljava/lang/String; = "CountryDetector"


# instance fields
.field private mCountServiceStateChanges:I

.field private mCountry:Landroid/location/Country;

.field private mCountryFromLocation:Landroid/location/Country;

.field private final mDebugLogs:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Landroid/location/Country;",
            ">;"
        }
    .end annotation
.end field

.field private mLastCountryAddedToLogs:Landroid/location/Country;

.field private mLocationBasedCountryDetectionListener:Landroid/location/CountryListener;

.field protected mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;

.field protected mLocationRefreshTimer:Ljava/util/Timer;

.field private final mObject:Ljava/lang/Object;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mStartTime:J

.field private mStopTime:J

.field private mStopped:Z

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTotalCountServiceStateChanges:I

.field private mTotalTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/android/server/location/CountryDetectorBase;-><init>(Landroid/content/Context;)V

    #@3
    .line 80
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopped:Z

    #@6
    .line 88
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@8
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mDebugLogs:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@d
    .line 101
    new-instance v0, Ljava/lang/Object;

    #@f
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mObject:Ljava/lang/Object;

    #@14
    .line 133
    new-instance v0, Lcom/android/server/location/ComprehensiveCountryDetector$1;

    #@16
    invoke-direct {v0, p0}, Lcom/android/server/location/ComprehensiveCountryDetector$1;-><init>(Lcom/android/server/location/ComprehensiveCountryDetector;)V

    #@19
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetectionListener:Landroid/location/CountryListener;

    #@1b
    .line 146
    const-string v0, "phone"

    #@1d
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@23
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@25
    .line 147
    return-void
.end method

.method static synthetic access$002(Lcom/android/server/location/ComprehensiveCountryDetector;Landroid/location/Country;)Landroid/location/Country;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountryFromLocation:Landroid/location/Country;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/server/location/ComprehensiveCountryDetector;ZZ)Landroid/location/Country;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/ComprehensiveCountryDetector;->detectCountry(ZZ)Landroid/location/Country;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/location/ComprehensiveCountryDetector;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->stopLocationBasedDetector()V

    #@3
    return-void
.end method

.method static synthetic access$308(Lcom/android/server/location/ComprehensiveCountryDetector;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountServiceStateChanges:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountServiceStateChanges:I

    #@6
    return v0
.end method

.method static synthetic access$408(Lcom/android/server/location/ComprehensiveCountryDetector;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTotalCountServiceStateChanges:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTotalCountServiceStateChanges:I

    #@6
    return v0
.end method

.method static synthetic access$500(Lcom/android/server/location/ComprehensiveCountryDetector;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->isNetworkCountryCodeAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private addToLogs(Landroid/location/Country;)V
    .registers 4
    .parameter "country"

    #@0
    .prologue
    .line 189
    if-nez p1, :cond_3

    #@2
    .line 209
    :goto_2
    return-void

    #@3
    .line 195
    :cond_3
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mObject:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 196
    :try_start_6
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLastCountryAddedToLogs:Landroid/location/Country;

    #@8
    if-eqz v0, :cond_17

    #@a
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLastCountryAddedToLogs:Landroid/location/Country;

    #@c
    invoke-virtual {v0, p1}, Landroid/location/Country;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 197
    monitor-exit v1

    #@13
    goto :goto_2

    #@14
    .line 200
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_6 .. :try_end_16} :catchall_14

    #@16
    throw v0

    #@17
    .line 199
    :cond_17
    :try_start_17
    iput-object p1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLastCountryAddedToLogs:Landroid/location/Country;

    #@19
    .line 200
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_17 .. :try_end_1a} :catchall_14

    #@1a
    .line 202
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mDebugLogs:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@1c
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    #@1f
    move-result v0

    #@20
    const/16 v1, 0x14

    #@22
    if-lt v0, v1, :cond_29

    #@24
    .line 203
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mDebugLogs:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@26
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    #@29
    .line 208
    :cond_29
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mDebugLogs:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@2b
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    #@2e
    goto :goto_2
.end method

.method private declared-synchronized cancelLocationRefresh()V
    .registers 2

    #@0
    .prologue
    .line 430
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationRefreshTimer:Ljava/util/Timer;

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 431
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationRefreshTimer:Ljava/util/Timer;

    #@7
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    #@a
    .line 432
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationRefreshTimer:Ljava/util/Timer;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    #@d
    .line 434
    :cond_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 430
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method private detectCountry(ZZ)Landroid/location/Country;
    .registers 6
    .parameter "notifyChange"
    .parameter "startLocationBasedDetection"

    #@0
    .prologue
    .line 272
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->getCountry()Landroid/location/Country;

    #@3
    move-result-object v0

    #@4
    .line 273
    .local v0, country:Landroid/location/Country;
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountry:Landroid/location/Country;

    #@6
    if-eqz v1, :cond_17

    #@8
    new-instance v1, Landroid/location/Country;

    #@a
    iget-object v2, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountry:Landroid/location/Country;

    #@c
    invoke-direct {v1, v2}, Landroid/location/Country;-><init>(Landroid/location/Country;)V

    #@f
    :goto_f
    invoke-virtual {p0, v1, v0, p1, p2}, Lcom/android/server/location/ComprehensiveCountryDetector;->runAfterDetectionAsync(Landroid/location/Country;Landroid/location/Country;ZZ)V

    #@12
    .line 275
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountry:Landroid/location/Country;

    #@14
    .line 276
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountry:Landroid/location/Country;

    #@16
    return-object v1

    #@17
    .line 273
    :cond_17
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountry:Landroid/location/Country;

    #@19
    goto :goto_f
.end method

.method private getCountry()Landroid/location/Country;
    .registers 2

    #@0
    .prologue
    .line 170
    const/4 v0, 0x0

    #@1
    .line 171
    .local v0, result:Landroid/location/Country;
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->getNetworkBasedCountry()Landroid/location/Country;

    #@4
    move-result-object v0

    #@5
    .line 172
    if-nez v0, :cond_b

    #@7
    .line 173
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->getLastKnownLocationBasedCountry()Landroid/location/Country;

    #@a
    move-result-object v0

    #@b
    .line 175
    :cond_b
    if-nez v0, :cond_11

    #@d
    .line 176
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->getSimBasedCountry()Landroid/location/Country;

    #@10
    move-result-object v0

    #@11
    .line 178
    :cond_11
    if-nez v0, :cond_17

    #@13
    .line 179
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->getLocaleCountry()Landroid/location/Country;

    #@16
    move-result-object v0

    #@17
    .line 181
    :cond_17
    invoke-direct {p0, v0}, Lcom/android/server/location/ComprehensiveCountryDetector;->addToLogs(Landroid/location/Country;)V

    #@1a
    .line 182
    return-object v0
.end method

.method private isNetworkCountryCodeAvailable()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 214
    iget-object v2, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@3
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@6
    move-result v0

    #@7
    .line 216
    .local v0, phoneType:I
    if-ne v0, v1, :cond_a

    #@9
    :goto_9
    return v1

    #@a
    :cond_a
    const/4 v1, 0x0

    #@b
    goto :goto_9
.end method

.method private notifyIfCountryChanged(Landroid/location/Country;Landroid/location/Country;)V
    .registers 4
    .parameter "country"
    .parameter "detectedCountry"

    #@0
    .prologue
    .line 395
    if-eqz p2, :cond_11

    #@2
    iget-object v0, p0, Lcom/android/server/location/CountryDetectorBase;->mListener:Landroid/location/CountryListener;

    #@4
    if-eqz v0, :cond_11

    #@6
    if-eqz p1, :cond_e

    #@8
    invoke-virtual {p1, p2}, Landroid/location/Country;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_11

    #@e
    .line 400
    :cond_e
    invoke-virtual {p0, p2}, Lcom/android/server/location/ComprehensiveCountryDetector;->notifyListener(Landroid/location/Country;)V

    #@11
    .line 402
    :cond_11
    return-void
.end method

.method private declared-synchronized scheduleLocationRefresh()V
    .registers 5

    #@0
    .prologue
    .line 408
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationRefreshTimer:Ljava/util/Timer;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1c

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 424
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 413
    :cond_7
    :try_start_7
    new-instance v0, Ljava/util/Timer;

    #@9
    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    #@c
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationRefreshTimer:Ljava/util/Timer;

    #@e
    .line 414
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationRefreshTimer:Ljava/util/Timer;

    #@10
    new-instance v1, Lcom/android/server/location/ComprehensiveCountryDetector$3;

    #@12
    invoke-direct {v1, p0}, Lcom/android/server/location/ComprehensiveCountryDetector$3;-><init>(Lcom/android/server/location/ComprehensiveCountryDetector;)V

    #@15
    const-wide/32 v2, 0x5265c00

    #@18
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1b
    .catchall {:try_start_7 .. :try_end_1b} :catchall_1c

    #@1b
    goto :goto_5

    #@1c
    .line 408
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit p0

    #@1e
    throw v0
.end method

.method private declared-synchronized startLocationBasedDetector(Landroid/location/CountryListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 359
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_18

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 369
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 366
    :cond_7
    :try_start_7
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->createLocationBasedCountryDetector()Lcom/android/server/location/CountryDetectorBase;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;

    #@d
    .line 367
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;

    #@f
    invoke-virtual {v0, p1}, Lcom/android/server/location/CountryDetectorBase;->setCountryListener(Landroid/location/CountryListener;)V

    #@12
    .line 368
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;

    #@14
    invoke-virtual {v0}, Lcom/android/server/location/CountryDetectorBase;->detectCountry()Landroid/location/Country;
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_18

    #@17
    goto :goto_5

    #@18
    .line 359
    :catchall_18
    move-exception v0

    #@19
    monitor-exit p0

    #@1a
    throw v0
.end method

.method private declared-synchronized stopLocationBasedDetector()V
    .registers 2

    #@0
    .prologue
    .line 376
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 377
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;

    #@7
    invoke-virtual {v0}, Lcom/android/server/location/CountryDetectorBase;->stop()V

    #@a
    .line 378
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetector:Lcom/android/server/location/CountryDetectorBase;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    #@d
    .line 380
    :cond_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 376
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method


# virtual methods
.method protected declared-synchronized addPhoneStateListener()V
    .registers 4

    #@0
    .prologue
    .line 437
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@3
    if-nez v0, :cond_14

    #@5
    .line 438
    new-instance v0, Lcom/android/server/location/ComprehensiveCountryDetector$4;

    #@7
    invoke-direct {v0, p0}, Lcom/android/server/location/ComprehensiveCountryDetector$4;-><init>(Lcom/android/server/location/ComprehensiveCountryDetector;)V

    #@a
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@c
    .line 452
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@e
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@10
    const/4 v2, 0x1

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    #@14
    .line 454
    :cond_14
    monitor-exit p0

    #@15
    return-void

    #@16
    .line 437
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0

    #@18
    throw v0
.end method

.method protected createLocationBasedCountryDetector()Lcom/android/server/location/CountryDetectorBase;
    .registers 3

    #@0
    .prologue
    .line 383
    new-instance v0, Lcom/android/server/location/LocationBasedCountryDetector;

    #@2
    iget-object v1, p0, Lcom/android/server/location/CountryDetectorBase;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Lcom/android/server/location/LocationBasedCountryDetector;-><init>(Landroid/content/Context;)V

    #@7
    return-object v0
.end method

.method public detectCountry()Landroid/location/Country;
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 152
    iget-boolean v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopped:Z

    #@3
    if-nez v0, :cond_b

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    invoke-direct {p0, v1, v0}, Lcom/android/server/location/ComprehensiveCountryDetector;->detectCountry(ZZ)Landroid/location/Country;

    #@9
    move-result-object v0

    #@a
    return-object v0

    #@b
    :cond_b
    move v0, v1

    #@c
    goto :goto_6
.end method

.method protected getLastKnownLocationBasedCountry()Landroid/location/Country;
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountryFromLocation:Landroid/location/Country;

    #@2
    return-object v0
.end method

.method protected getLocaleCountry()Landroid/location/Country;
    .registers 5

    #@0
    .prologue
    .line 256
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    .line 257
    .local v0, defaultLocale:Ljava/util/Locale;
    if-eqz v0, :cond_11

    #@6
    .line 258
    new-instance v1, Landroid/location/Country;

    #@8
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    const/4 v3, 0x3

    #@d
    invoke-direct {v1, v2, v3}, Landroid/location/Country;-><init>(Ljava/lang/String;I)V

    #@10
    .line 260
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method protected getNetworkBasedCountry()Landroid/location/Country;
    .registers 4

    #@0
    .prologue
    .line 223
    const/4 v0, 0x0

    #@1
    .line 224
    .local v0, countryIso:Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->isNetworkCountryCodeAvailable()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_1a

    #@7
    .line 225
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@9
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 226
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_1a

    #@13
    .line 227
    new-instance v1, Landroid/location/Country;

    #@15
    const/4 v2, 0x0

    #@16
    invoke-direct {v1, v0, v2}, Landroid/location/Country;-><init>(Ljava/lang/String;I)V

    #@19
    .line 230
    :goto_19
    return-object v1

    #@1a
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_19
.end method

.method protected getSimBasedCountry()Landroid/location/Country;
    .registers 4

    #@0
    .prologue
    .line 244
    const/4 v0, 0x0

    #@1
    .line 245
    .local v0, countryIso:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@3
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 246
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_14

    #@d
    .line 247
    new-instance v1, Landroid/location/Country;

    #@f
    const/4 v2, 0x2

    #@10
    invoke-direct {v1, v0, v2}, Landroid/location/Country;-><init>(Ljava/lang/String;I)V

    #@13
    .line 249
    :goto_13
    return-object v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method protected isAirplaneModeOff()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 387
    iget-object v1, p0, Lcom/android/server/location/CountryDetectorBase;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "airplane_mode_on"

    #@9
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_10

    #@f
    const/4 v0, 0x1

    #@10
    :cond_10
    return v0
.end method

.method protected isGeoCoderImplemented()Z
    .registers 2

    #@0
    .prologue
    .line 464
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected declared-synchronized removePhoneStateListener()V
    .registers 4

    #@0
    .prologue
    .line 457
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 458
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@7
    iget-object v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@d
    .line 459
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_12

    #@10
    .line 461
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 457
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method runAfterDetection(Landroid/location/Country;Landroid/location/Country;ZZ)V
    .registers 7
    .parameter "country"
    .parameter "detectedCountry"
    .parameter "notifyChange"
    .parameter "startLocationBasedDetection"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 315
    if-eqz p3, :cond_6

    #@3
    .line 316
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/ComprehensiveCountryDetector;->notifyIfCountryChanged(Landroid/location/Country;Landroid/location/Country;)V

    #@6
    .line 328
    :cond_6
    if-eqz p4, :cond_25

    #@8
    if-eqz p2, :cond_10

    #@a
    invoke-virtual {p2}, Landroid/location/Country;->getSource()I

    #@d
    move-result v0

    #@e
    if-le v0, v1, :cond_25

    #@10
    :cond_10
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->isAirplaneModeOff()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_25

    #@16
    iget-object v0, p0, Lcom/android/server/location/CountryDetectorBase;->mListener:Landroid/location/CountryListener;

    #@18
    if-eqz v0, :cond_25

    #@1a
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->isGeoCoderImplemented()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_25

    #@20
    .line 337
    iget-object v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mLocationBasedCountryDetectionListener:Landroid/location/CountryListener;

    #@22
    invoke-direct {p0, v0}, Lcom/android/server/location/ComprehensiveCountryDetector;->startLocationBasedDetector(Landroid/location/CountryListener;)V

    #@25
    .line 339
    :cond_25
    if-eqz p2, :cond_2d

    #@27
    invoke-virtual {p2}, Landroid/location/Country;->getSource()I

    #@2a
    move-result v0

    #@2b
    if-lt v0, v1, :cond_31

    #@2d
    .line 346
    :cond_2d
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->scheduleLocationRefresh()V

    #@30
    .line 353
    :goto_30
    return-void

    #@31
    .line 350
    :cond_31
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->cancelLocationRefresh()V

    #@34
    .line 351
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->stopLocationBasedDetector()V

    #@37
    goto :goto_30
.end method

.method protected runAfterDetectionAsync(Landroid/location/Country;Landroid/location/Country;ZZ)V
    .registers 12
    .parameter "country"
    .parameter "detectedCountry"
    .parameter "notifyChange"
    .parameter "startLocationBasedDetection"

    #@0
    .prologue
    .line 284
    iget-object v6, p0, Lcom/android/server/location/CountryDetectorBase;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v0, Lcom/android/server/location/ComprehensiveCountryDetector$2;

    #@4
    move-object v1, p0

    #@5
    move-object v2, p1

    #@6
    move-object v3, p2

    #@7
    move v4, p3

    #@8
    move v5, p4

    #@9
    invoke-direct/range {v0 .. v5}, Lcom/android/server/location/ComprehensiveCountryDetector$2;-><init>(Lcom/android/server/location/ComprehensiveCountryDetector;Landroid/location/Country;Landroid/location/Country;ZZ)V

    #@c
    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@f
    .line 291
    return-void
.end method

.method public setCountryListener(Landroid/location/CountryListener;)V
    .registers 7
    .parameter "listener"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 295
    iget-object v0, p0, Lcom/android/server/location/CountryDetectorBase;->mListener:Landroid/location/CountryListener;

    #@3
    .line 296
    .local v0, prevListener:Landroid/location/CountryListener;
    iput-object p1, p0, Lcom/android/server/location/CountryDetectorBase;->mListener:Landroid/location/CountryListener;

    #@5
    .line 297
    iget-object v1, p0, Lcom/android/server/location/CountryDetectorBase;->mListener:Landroid/location/CountryListener;

    #@7
    if-nez v1, :cond_20

    #@9
    .line 299
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->removePhoneStateListener()V

    #@c
    .line 300
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->stopLocationBasedDetector()V

    #@f
    .line 301
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->cancelLocationRefresh()V

    #@12
    .line 302
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@15
    move-result-wide v1

    #@16
    iput-wide v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopTime:J

    #@18
    .line 303
    iget-wide v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTotalTime:J

    #@1a
    iget-wide v3, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopTime:J

    #@1c
    add-long/2addr v1, v3

    #@1d
    iput-wide v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTotalTime:J

    #@1f
    .line 311
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 304
    :cond_20
    if-nez v0, :cond_1f

    #@22
    .line 305
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->addPhoneStateListener()V

    #@25
    .line 306
    const/4 v1, 0x1

    #@26
    invoke-direct {p0, v3, v1}, Lcom/android/server/location/ComprehensiveCountryDetector;->detectCountry(ZZ)Landroid/location/Country;

    #@29
    .line 307
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2c
    move-result-wide v1

    #@2d
    iput-wide v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStartTime:J

    #@2f
    .line 308
    const-wide/16 v1, 0x0

    #@31
    iput-wide v1, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopTime:J

    #@33
    .line 309
    iput v3, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountServiceStateChanges:I

    #@35
    goto :goto_1f
.end method

.method public stop()V
    .registers 3

    #@0
    .prologue
    .line 158
    const-string v0, "CountryDetector"

    #@2
    const-string v1, "Stop the detector."

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 159
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->cancelLocationRefresh()V

    #@a
    .line 160
    invoke-virtual {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->removePhoneStateListener()V

    #@d
    .line 161
    invoke-direct {p0}, Lcom/android/server/location/ComprehensiveCountryDetector;->stopLocationBasedDetector()V

    #@10
    .line 162
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Lcom/android/server/location/CountryDetectorBase;->mListener:Landroid/location/CountryListener;

    #@13
    .line 163
    const/4 v0, 0x1

    #@14
    iput-boolean v0, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopped:Z

    #@16
    .line 164
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 13

    #@0
    .prologue
    .line 469
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v3

    #@4
    .line 470
    .local v3, currentTime:J
    const-wide/16 v1, 0x0

    #@6
    .line 471
    .local v1, currentSessionLength:J
    new-instance v6, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    .line 472
    .local v6, sb:Ljava/lang/StringBuilder;
    const-string v7, "ComprehensiveCountryDetector{"

    #@d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 474
    iget-wide v7, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopTime:J

    #@12
    const-wide/16 v9, 0x0

    #@14
    cmp-long v7, v7, v9

    #@16
    if-nez v7, :cond_e1

    #@18
    .line 475
    iget-wide v7, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStartTime:J

    #@1a
    sub-long v1, v3, v7

    #@1c
    .line 476
    new-instance v7, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v8, "timeRunning="

    #@23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    const-string v8, ", "

    #@2d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v7

    #@31
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 481
    :goto_38
    new-instance v7, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v8, "totalCountServiceStateChanges="

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    iget v8, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTotalCountServiceStateChanges:I

    #@45
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    const-string v8, ", "

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    .line 482
    new-instance v7, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v8, "currentCountServiceStateChanges="

    #@5d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v7

    #@61
    iget v8, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mCountServiceStateChanges:I

    #@63
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v7

    #@67
    const-string v8, ", "

    #@69
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v7

    #@6d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v7

    #@71
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    .line 483
    new-instance v7, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v8, "totalTime="

    #@7b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v7

    #@7f
    iget-wide v8, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mTotalTime:J

    #@81
    add-long/2addr v8, v1

    #@82
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@85
    move-result-object v7

    #@86
    const-string v8, ", "

    #@88
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    .line 484
    new-instance v7, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v8, "currentTime="

    #@9a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v7

    #@a2
    const-string v8, ", "

    #@a4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v7

    #@a8
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v7

    #@ac
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    .line 485
    const-string v7, "countries="

    #@b1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    .line 486
    iget-object v7, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mDebugLogs:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@b6
    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    #@b9
    move-result-object v5

    #@ba
    .local v5, i$:Ljava/util/Iterator;
    :goto_ba
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@bd
    move-result v7

    #@be
    if-eqz v7, :cond_104

    #@c0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c3
    move-result-object v0

    #@c4
    check-cast v0, Landroid/location/Country;

    #@c6
    .line 487
    .local v0, country:Landroid/location/Country;
    new-instance v7, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    const-string v8, "\n   "

    #@cd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {v0}, Landroid/location/Country;->toString()Ljava/lang/String;

    #@d4
    move-result-object v8

    #@d5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v7

    #@d9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v7

    #@dd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    goto :goto_ba

    #@e1
    .line 479
    .end local v0           #country:Landroid/location/Country;
    .end local v5           #i$:Ljava/util/Iterator;
    :cond_e1
    new-instance v7, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v8, "lastRunTimeLength="

    #@e8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v7

    #@ec
    iget-wide v8, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStopTime:J

    #@ee
    iget-wide v10, p0, Lcom/android/server/location/ComprehensiveCountryDetector;->mStartTime:J

    #@f0
    sub-long/2addr v8, v10

    #@f1
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v7

    #@f5
    const-string v8, ", "

    #@f7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v7

    #@fb
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v7

    #@ff
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    goto/16 :goto_38

    #@104
    .line 489
    .restart local v5       #i$:Ljava/util/Iterator;
    :cond_104
    const-string v7, "}"

    #@106
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    .line 490
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v7

    #@10d
    return-object v7
.end method
