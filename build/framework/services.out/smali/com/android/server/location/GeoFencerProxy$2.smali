.class Lcom/android/server/location/GeoFencerProxy$2;
.super Landroid/location/IGeoFenceListener$Stub;
.source "GeoFencerProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GeoFencerProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GeoFencerProxy;


# direct methods
.method constructor <init>(Lcom/android/server/location/GeoFencerProxy;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/server/location/GeoFencerProxy$2;->this$0:Lcom/android/server/location/GeoFencerProxy;

    #@2
    invoke-direct {p0}, Landroid/location/IGeoFenceListener$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public geoFenceExpired(Landroid/app/PendingIntent;)V
    .registers 5
    .parameter "intent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy$2;->this$0:Lcom/android/server/location/GeoFencerProxy;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "geoFenceExpired - "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Lcom/android/server/location/GeoFencerProxy;->access$100(Lcom/android/server/location/GeoFencerProxy;Ljava/lang/String;)V

    #@18
    .line 70
    iget-object v0, p0, Lcom/android/server/location/GeoFencerProxy$2;->this$0:Lcom/android/server/location/GeoFencerProxy;

    #@1a
    const/4 v1, 0x1

    #@1b
    invoke-virtual {v0, p1, v1}, Lcom/android/server/location/GeoFencerProxy;->remove(Landroid/app/PendingIntent;Z)V

    #@1e
    .line 71
    return-void
.end method
