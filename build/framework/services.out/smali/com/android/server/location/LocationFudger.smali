.class public Lcom/android/server/location/LocationFudger;
.super Ljava/lang/Object;
.source "LocationFudger.java"


# static fields
.field private static final APPROXIMATE_METERS_PER_DEGREE_AT_EQUATOR:I = 0x1b198

.field private static final CHANGE_INTERVAL_MS:J = 0x36ee80L

.field private static final CHANGE_PER_INTERVAL:D = 0.03

.field private static final COARSE_ACCURACY_CONFIG_NAME:Ljava/lang/String; = "locationCoarseAccuracy"

.field private static final D:Z = false

.field private static final DEFAULT_ACCURACY_IN_METERS:F = 2000.0f

.field public static final FASTEST_INTERVAL_MS:J = 0x927c0L

.field private static final MAX_LATITUDE:D = 89.999990990991

.field private static final MINIMUM_ACCURACY_IN_METERS:F = 200.0f

.field private static final NEW_WEIGHT:D = 0.03

#the value of this static final field might be set in the static constructor
.field private static final PREVIOUS_WEIGHT:D = 0.0

.field private static final TAG:Ljava/lang/String; = "LocationFudge"


# instance fields
.field private mAccuracyInMeters:F

.field private final mContext:Landroid/content/Context;

.field private mGridSizeInMeters:D

.field private final mLock:Ljava/lang/Object;

.field private mNextInterval:J

.field private mOffsetLatitudeMeters:D

.field private mOffsetLongitudeMeters:D

.field private final mRandom:Ljava/security/SecureRandom;

.field private final mSettingsObserver:Landroid/database/ContentObserver;

.field private mStandardDeviationInMeters:D


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 88
    const-wide v0, 0x3feff8a0902de00dL

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    #@8
    move-result-wide v0

    #@9
    sput-wide v0, Lcom/android/server/location/LocationFudger;->PREVIOUS_WEIGHT:D

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 10
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    .line 145
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 106
    new-instance v1, Ljava/lang/Object;

    #@5
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v1, p0, Lcom/android/server/location/LocationFudger;->mLock:Ljava/lang/Object;

    #@a
    .line 107
    new-instance v1, Ljava/security/SecureRandom;

    #@c
    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    #@f
    iput-object v1, p0, Lcom/android/server/location/LocationFudger;->mRandom:Ljava/security/SecureRandom;

    #@11
    .line 146
    iput-object p1, p0, Lcom/android/server/location/LocationFudger;->mContext:Landroid/content/Context;

    #@13
    .line 147
    new-instance v1, Lcom/android/server/location/LocationFudger$1;

    #@15
    invoke-direct {v1, p0, p2}, Lcom/android/server/location/LocationFudger$1;-><init>(Lcom/android/server/location/LocationFudger;Landroid/os/Handler;)V

    #@18
    iput-object v1, p0, Lcom/android/server/location/LocationFudger;->mSettingsObserver:Landroid/database/ContentObserver;

    #@1a
    .line 153
    iget-object v1, p0, Lcom/android/server/location/LocationFudger;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1f
    move-result-object v1

    #@20
    const-string v2, "locationCoarseAccuracy"

    #@22
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v2

    #@26
    const/4 v3, 0x0

    #@27
    iget-object v4, p0, Lcom/android/server/location/LocationFudger;->mSettingsObserver:Landroid/database/ContentObserver;

    #@29
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@2c
    .line 156
    invoke-direct {p0}, Lcom/android/server/location/LocationFudger;->loadCoarseAccuracy()F

    #@2f
    move-result v0

    #@30
    .line 157
    .local v0, accuracy:F
    iget-object v2, p0, Lcom/android/server/location/LocationFudger;->mLock:Ljava/lang/Object;

    #@32
    monitor-enter v2

    #@33
    .line 158
    :try_start_33
    invoke-direct {p0, v0}, Lcom/android/server/location/LocationFudger;->setAccuracyInMetersLocked(F)V

    #@36
    .line 159
    invoke-direct {p0}, Lcom/android/server/location/LocationFudger;->nextOffsetLocked()D

    #@39
    move-result-wide v3

    #@3a
    iput-wide v3, p0, Lcom/android/server/location/LocationFudger;->mOffsetLatitudeMeters:D

    #@3c
    .line 160
    invoke-direct {p0}, Lcom/android/server/location/LocationFudger;->nextOffsetLocked()D

    #@3f
    move-result-wide v3

    #@40
    iput-wide v3, p0, Lcom/android/server/location/LocationFudger;->mOffsetLongitudeMeters:D

    #@42
    .line 161
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@45
    move-result-wide v3

    #@46
    const-wide/32 v5, 0x36ee80

    #@49
    add-long/2addr v3, v5

    #@4a
    iput-wide v3, p0, Lcom/android/server/location/LocationFudger;->mNextInterval:J

    #@4c
    .line 162
    monitor-exit v2

    #@4d
    .line 163
    return-void

    #@4e
    .line 162
    :catchall_4e
    move-exception v1

    #@4f
    monitor-exit v2
    :try_end_50
    .catchall {:try_start_33 .. :try_end_50} :catchall_4e

    #@50
    throw v1
.end method

.method static synthetic access$000(Lcom/android/server/location/LocationFudger;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Lcom/android/server/location/LocationFudger;->loadCoarseAccuracy()F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/android/server/location/LocationFudger;F)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationFudger;->setAccuracyInMeters(F)V

    #@3
    return-void
.end method

.method private addCoarseLocationExtraLocked(Landroid/location/Location;)Landroid/location/Location;
    .registers 4
    .parameter "location"

    #@0
    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationFudger;->createCoarseLocked(Landroid/location/Location;)Landroid/location/Location;

    #@3
    move-result-object v0

    #@4
    .line 183
    .local v0, coarse:Landroid/location/Location;
    const-string v1, "coarseLocation"

    #@6
    invoke-virtual {p1, v1, v0}, Landroid/location/Location;->setExtraLocation(Ljava/lang/String;Landroid/location/Location;)V

    #@9
    .line 184
    return-object v0
.end method

.method private createCoarseLocked(Landroid/location/Location;)Landroid/location/Location;
    .registers 13
    .parameter "fine"

    #@0
    .prologue
    .line 203
    new-instance v0, Landroid/location/Location;

    #@2
    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@5
    .line 207
    .local v0, coarse:Landroid/location/Location;
    invoke-virtual {v0}, Landroid/location/Location;->removeBearing()V

    #@8
    .line 208
    invoke-virtual {v0}, Landroid/location/Location;->removeSpeed()V

    #@b
    .line 209
    invoke-virtual {v0}, Landroid/location/Location;->removeAltitude()V

    #@e
    .line 210
    const/4 v9, 0x0

    #@f
    invoke-virtual {v0, v9}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    #@12
    .line 212
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    #@15
    move-result-wide v1

    #@16
    .line 213
    .local v1, lat:D
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    #@19
    move-result-wide v5

    #@1a
    .line 216
    .local v5, lon:D
    invoke-static {v1, v2}, Lcom/android/server/location/LocationFudger;->wrapLatitude(D)D

    #@1d
    move-result-wide v1

    #@1e
    .line 217
    invoke-static {v5, v6}, Lcom/android/server/location/LocationFudger;->wrapLongitude(D)D

    #@21
    move-result-wide v5

    #@22
    .line 227
    invoke-direct {p0}, Lcom/android/server/location/LocationFudger;->updateRandomOffsetLocked()V

    #@25
    .line 229
    iget-wide v9, p0, Lcom/android/server/location/LocationFudger;->mOffsetLongitudeMeters:D

    #@27
    invoke-static {v9, v10, v1, v2}, Lcom/android/server/location/LocationFudger;->metersToDegreesLongitude(DD)D

    #@2a
    move-result-wide v9

    #@2b
    add-double/2addr v5, v9

    #@2c
    .line 230
    iget-wide v9, p0, Lcom/android/server/location/LocationFudger;->mOffsetLatitudeMeters:D

    #@2e
    invoke-static {v9, v10}, Lcom/android/server/location/LocationFudger;->metersToDegreesLatitude(D)D

    #@31
    move-result-wide v9

    #@32
    add-double/2addr v1, v9

    #@33
    .line 235
    invoke-static {v1, v2}, Lcom/android/server/location/LocationFudger;->wrapLatitude(D)D

    #@36
    move-result-wide v1

    #@37
    .line 236
    invoke-static {v5, v6}, Lcom/android/server/location/LocationFudger;->wrapLongitude(D)D

    #@3a
    move-result-wide v5

    #@3b
    .line 248
    iget-wide v9, p0, Lcom/android/server/location/LocationFudger;->mGridSizeInMeters:D

    #@3d
    invoke-static {v9, v10}, Lcom/android/server/location/LocationFudger;->metersToDegreesLatitude(D)D

    #@40
    move-result-wide v3

    #@41
    .line 249
    .local v3, latGranularity:D
    div-double v9, v1, v3

    #@43
    invoke-static {v9, v10}, Ljava/lang/Math;->round(D)J

    #@46
    move-result-wide v9

    #@47
    long-to-double v9, v9

    #@48
    mul-double v1, v9, v3

    #@4a
    .line 250
    iget-wide v9, p0, Lcom/android/server/location/LocationFudger;->mGridSizeInMeters:D

    #@4c
    invoke-static {v9, v10, v1, v2}, Lcom/android/server/location/LocationFudger;->metersToDegreesLongitude(DD)D

    #@4f
    move-result-wide v7

    #@50
    .line 251
    .local v7, lonGranularity:D
    div-double v9, v5, v7

    #@52
    invoke-static {v9, v10}, Ljava/lang/Math;->round(D)J

    #@55
    move-result-wide v9

    #@56
    long-to-double v9, v9

    #@57
    mul-double v5, v9, v7

    #@59
    .line 254
    invoke-static {v1, v2}, Lcom/android/server/location/LocationFudger;->wrapLatitude(D)D

    #@5c
    move-result-wide v1

    #@5d
    .line 255
    invoke-static {v5, v6}, Lcom/android/server/location/LocationFudger;->wrapLongitude(D)D

    #@60
    move-result-wide v5

    #@61
    .line 258
    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    #@64
    .line 259
    invoke-virtual {v0, v5, v6}, Landroid/location/Location;->setLongitude(D)V

    #@67
    .line 260
    iget v9, p0, Lcom/android/server/location/LocationFudger;->mAccuracyInMeters:F

    #@69
    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    #@6c
    move-result v10

    #@6d
    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    #@70
    move-result v9

    #@71
    invoke-virtual {v0, v9}, Landroid/location/Location;->setAccuracy(F)V

    #@74
    .line 263
    return-object v0
.end method

.method private loadCoarseAccuracy()F
    .registers 6

    #@0
    .prologue
    const/high16 v2, 0x44fa

    #@2
    .line 371
    iget-object v3, p0, Lcom/android/server/location/LocationFudger;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v3

    #@8
    const-string v4, "locationCoarseAccuracy"

    #@a
    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 376
    .local v1, newSetting:Ljava/lang/String;
    if-nez v1, :cond_11

    #@10
    .line 382
    :goto_10
    return v2

    #@11
    .line 380
    :cond_11
    :try_start_11
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_14
    .catch Ljava/lang/NumberFormatException; {:try_start_11 .. :try_end_14} :catch_16

    #@14
    move-result v2

    #@15
    goto :goto_10

    #@16
    .line 381
    :catch_16
    move-exception v0

    #@17
    .line 382
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_10
.end method

.method private static metersToDegreesLatitude(D)D
    .registers 4
    .parameter "distance"

    #@0
    .prologue
    .line 330
    const-wide v0, 0x40fb198000000000L

    #@5
    div-double v0, p0, v0

    #@7
    return-wide v0
.end method

.method private static metersToDegreesLongitude(DD)D
    .registers 8
    .parameter "distance"
    .parameter "lat"

    #@0
    .prologue
    .line 337
    const-wide v0, 0x40fb198000000000L

    #@5
    div-double v0, p0, v0

    #@7
    invoke-static {p2, p3}, Ljava/lang/Math;->toRadians(D)D

    #@a
    move-result-wide v2

    #@b
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    #@e
    move-result-wide v2

    #@f
    div-double/2addr v0, v2

    #@10
    return-wide v0
.end method

.method private nextOffsetLocked()D
    .registers 5

    #@0
    .prologue
    .line 305
    iget-object v0, p0, Lcom/android/server/location/LocationFudger;->mRandom:Ljava/security/SecureRandom;

    #@2
    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextGaussian()D

    #@5
    move-result-wide v0

    #@6
    iget-wide v2, p0, Lcom/android/server/location/LocationFudger;->mStandardDeviationInMeters:D

    #@8
    mul-double/2addr v0, v2

    #@9
    return-wide v0
.end method

.method private setAccuracyInMeters(F)V
    .registers 4
    .parameter "accuracyInMeters"

    #@0
    .prologue
    .line 362
    iget-object v1, p0, Lcom/android/server/location/LocationFudger;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 363
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationFudger;->setAccuracyInMetersLocked(F)V

    #@6
    .line 364
    monitor-exit v1

    #@7
    .line 365
    return-void

    #@8
    .line 364
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private setAccuracyInMetersLocked(F)V
    .registers 6
    .parameter "accuracyInMeters"

    #@0
    .prologue
    .line 350
    const/high16 v0, 0x4348

    #@2
    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    #@5
    move-result v0

    #@6
    iput v0, p0, Lcom/android/server/location/LocationFudger;->mAccuracyInMeters:F

    #@8
    .line 354
    iget v0, p0, Lcom/android/server/location/LocationFudger;->mAccuracyInMeters:F

    #@a
    float-to-double v0, v0

    #@b
    iput-wide v0, p0, Lcom/android/server/location/LocationFudger;->mGridSizeInMeters:D

    #@d
    .line 355
    iget-wide v0, p0, Lcom/android/server/location/LocationFudger;->mGridSizeInMeters:D

    #@f
    const-wide/high16 v2, 0x4010

    #@11
    div-double/2addr v0, v2

    #@12
    iput-wide v0, p0, Lcom/android/server/location/LocationFudger;->mStandardDeviationInMeters:D

    #@14
    .line 356
    return-void
.end method

.method private updateRandomOffsetLocked()V
    .registers 9

    #@0
    .prologue
    const-wide v6, 0x3f9eb851eb851eb8L

    #@5
    .line 284
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@8
    move-result-wide v0

    #@9
    .line 285
    .local v0, now:J
    iget-wide v2, p0, Lcom/android/server/location/LocationFudger;->mNextInterval:J

    #@b
    cmp-long v2, v0, v2

    #@d
    if-gez v2, :cond_10

    #@f
    .line 302
    :goto_f
    return-void

    #@10
    .line 293
    :cond_10
    const-wide/32 v2, 0x36ee80

    #@13
    add-long/2addr v2, v0

    #@14
    iput-wide v2, p0, Lcom/android/server/location/LocationFudger;->mNextInterval:J

    #@16
    .line 295
    iget-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLatitudeMeters:D

    #@18
    sget-wide v4, Lcom/android/server/location/LocationFudger;->PREVIOUS_WEIGHT:D

    #@1a
    mul-double/2addr v2, v4

    #@1b
    iput-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLatitudeMeters:D

    #@1d
    .line 296
    iget-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLatitudeMeters:D

    #@1f
    invoke-direct {p0}, Lcom/android/server/location/LocationFudger;->nextOffsetLocked()D

    #@22
    move-result-wide v4

    #@23
    mul-double/2addr v4, v6

    #@24
    add-double/2addr v2, v4

    #@25
    iput-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLatitudeMeters:D

    #@27
    .line 297
    iget-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLongitudeMeters:D

    #@29
    sget-wide v4, Lcom/android/server/location/LocationFudger;->PREVIOUS_WEIGHT:D

    #@2b
    mul-double/2addr v2, v4

    #@2c
    iput-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLongitudeMeters:D

    #@2e
    .line 298
    iget-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLongitudeMeters:D

    #@30
    invoke-direct {p0}, Lcom/android/server/location/LocationFudger;->nextOffsetLocked()D

    #@33
    move-result-wide v4

    #@34
    mul-double/2addr v4, v6

    #@35
    add-double/2addr v2, v4

    #@36
    iput-wide v2, p0, Lcom/android/server/location/LocationFudger;->mOffsetLongitudeMeters:D

    #@38
    goto :goto_f
.end method

.method private static wrapLatitude(D)D
    .registers 4
    .parameter "lat"

    #@0
    .prologue
    .line 309
    const-wide v0, 0x40567fffda36a676L

    #@5
    cmpl-double v0, p0, v0

    #@7
    if-lez v0, :cond_e

    #@9
    .line 310
    const-wide p0, 0x40567fffda36a676L

    #@e
    .line 312
    :cond_e
    const-wide v0, -0x3fa9800025c9598aL

    #@13
    cmpg-double v0, p0, v0

    #@15
    if-gez v0, :cond_1c

    #@17
    .line 313
    const-wide p0, -0x3fa9800025c9598aL

    #@1c
    .line 315
    :cond_1c
    return-wide p0
.end method

.method private static wrapLongitude(D)D
    .registers 6
    .parameter "lon"

    #@0
    .prologue
    const-wide v2, 0x4076800000000000L

    #@5
    .line 319
    rem-double/2addr p0, v2

    #@6
    .line 320
    const-wide v0, 0x4066800000000000L

    #@b
    cmpl-double v0, p0, v0

    #@d
    if-ltz v0, :cond_10

    #@f
    .line 321
    sub-double/2addr p0, v2

    #@10
    .line 323
    :cond_10
    const-wide v0, -0x3f99800000000000L

    #@15
    cmpg-double v0, p0, v0

    #@17
    if-gez v0, :cond_1a

    #@19
    .line 324
    add-double/2addr p0, v2

    #@1a
    .line 326
    :cond_1a
    return-wide p0
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 341
    const-string v0, "offset: %.0f, %.0f (meters)"

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    iget-wide v3, p0, Lcom/android/server/location/LocationFudger;->mOffsetLongitudeMeters:D

    #@8
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@b
    move-result-object v3

    #@c
    aput-object v3, v1, v2

    #@e
    const/4 v2, 0x1

    #@f
    iget-wide v3, p0, Lcom/android/server/location/LocationFudger;->mOffsetLatitudeMeters:D

    #@11
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v1, v2

    #@17
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e
    .line 343
    return-void
.end method

.method public getOrCreate(Landroid/location/Location;)Landroid/location/Location;
    .registers 6
    .parameter "location"

    #@0
    .prologue
    .line 169
    iget-object v2, p0, Lcom/android/server/location/LocationFudger;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 170
    :try_start_3
    const-string v1, "coarseLocation"

    #@5
    invoke-virtual {p1, v1}, Landroid/location/Location;->getExtraLocation(Ljava/lang/String;)Landroid/location/Location;

    #@8
    move-result-object v0

    #@9
    .line 171
    .local v0, coarse:Landroid/location/Location;
    if-nez v0, :cond_11

    #@b
    .line 172
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationFudger;->addCoarseLocationExtraLocked(Landroid/location/Location;)Landroid/location/Location;

    #@e
    move-result-object v0

    #@f
    .end local v0           #coarse:Landroid/location/Location;
    monitor-exit v2

    #@10
    .line 177
    :goto_10
    return-object v0

    #@11
    .line 174
    .restart local v0       #coarse:Landroid/location/Location;
    :cond_11
    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    #@14
    move-result v1

    #@15
    iget v3, p0, Lcom/android/server/location/LocationFudger;->mAccuracyInMeters:F

    #@17
    cmpg-float v1, v1, v3

    #@19
    if-gez v1, :cond_24

    #@1b
    .line 175
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationFudger;->addCoarseLocationExtraLocked(Landroid/location/Location;)Landroid/location/Location;

    #@1e
    move-result-object v0

    #@1f
    .end local v0           #coarse:Landroid/location/Location;
    monitor-exit v2

    #@20
    goto :goto_10

    #@21
    .line 178
    :catchall_21
    move-exception v1

    #@22
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v1

    #@24
    .line 177
    .restart local v0       #coarse:Landroid/location/Location;
    :cond_24
    :try_start_24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_21

    #@25
    goto :goto_10
.end method
