.class public Lcom/android/server/location/GeofenceManager;
.super Ljava/lang/Object;
.source "GeofenceManager.java"

# interfaces
.implements Landroid/location/LocationListener;
.implements Landroid/app/PendingIntent$OnFinished;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/GeofenceManager$GeofenceHandler;
    }
.end annotation


# static fields
.field private static final D:Z = true

.field private static final MAX_AGE_NANOS:J = 0x45d964b800L

.field private static final MAX_INTERVAL_MS:J = 0x6ddd00L

.field private static final MAX_SPEED_M_S:I = 0x64

.field private static final MIN_INTERVAL_MS:J = 0xea60L

.field private static final MSG_UPDATE_FENCES:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GeofenceManager"


# instance fields
.field private final mBlacklist:Lcom/android/server/location/LocationBlacklist;

.field private final mContext:Landroid/content/Context;

.field private mFences:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/location/GeofenceState;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Lcom/android/server/location/GeofenceManager$GeofenceHandler;

.field private mLastLocationUpdate:Landroid/location/Location;

.field private final mLocationManager:Landroid/location/LocationManager;

.field private mLocationUpdateInterval:J

.field private mLock:Ljava/lang/Object;

.field private mPendingUpdate:Z

.field private mReceivingLocationUpdates:Z

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/location/LocationBlacklist;)V
    .registers 6
    .parameter "context"
    .parameter "blacklist"

    #@0
    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    new-instance v1, Ljava/lang/Object;

    #@5
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v1, p0, Lcom/android/server/location/GeofenceManager;->mLock:Ljava/lang/Object;

    #@a
    .line 81
    new-instance v1, Ljava/util/LinkedList;

    #@c
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    #@f
    iput-object v1, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@11
    .line 108
    iput-object p1, p0, Lcom/android/server/location/GeofenceManager;->mContext:Landroid/content/Context;

    #@13
    .line 109
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mContext:Landroid/content/Context;

    #@15
    const-string v2, "location"

    #@17
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Landroid/location/LocationManager;

    #@1d
    iput-object v1, p0, Lcom/android/server/location/GeofenceManager;->mLocationManager:Landroid/location/LocationManager;

    #@1f
    .line 110
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mContext:Landroid/content/Context;

    #@21
    const-string v2, "power"

    #@23
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, Landroid/os/PowerManager;

    #@29
    .line 111
    .local v0, powerManager:Landroid/os/PowerManager;
    const/4 v1, 0x1

    #@2a
    const-string v2, "GeofenceManager"

    #@2c
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@2f
    move-result-object v1

    #@30
    iput-object v1, p0, Lcom/android/server/location/GeofenceManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@32
    .line 112
    new-instance v1, Lcom/android/server/location/GeofenceManager$GeofenceHandler;

    #@34
    invoke-direct {v1, p0}, Lcom/android/server/location/GeofenceManager$GeofenceHandler;-><init>(Lcom/android/server/location/GeofenceManager;)V

    #@37
    iput-object v1, p0, Lcom/android/server/location/GeofenceManager;->mHandler:Lcom/android/server/location/GeofenceManager$GeofenceHandler;

    #@39
    .line 113
    iput-object p2, p0, Lcom/android/server/location/GeofenceManager;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@3b
    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/location/GeofenceManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/server/location/GeofenceManager;->updateFences()V

    #@3
    return-void
.end method

.method private getFreshLocationLocked()Landroid/location/Location;
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 210
    iget-boolean v4, p0, Lcom/android/server/location/GeofenceManager;->mReceivingLocationUpdates:Z

    #@3
    if-eqz v4, :cond_1b

    #@5
    iget-object v0, p0, Lcom/android/server/location/GeofenceManager;->mLastLocationUpdate:Landroid/location/Location;

    #@7
    .line 211
    .local v0, location:Landroid/location/Location;
    :goto_7
    if-nez v0, :cond_17

    #@9
    iget-object v4, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@b
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_17

    #@11
    .line 212
    iget-object v4, p0, Lcom/android/server/location/GeofenceManager;->mLocationManager:Landroid/location/LocationManager;

    #@13
    invoke-virtual {v4}, Landroid/location/LocationManager;->getLastLocation()Landroid/location/Location;

    #@16
    move-result-object v0

    #@17
    .line 216
    :cond_17
    if-nez v0, :cond_1d

    #@19
    move-object v0, v3

    #@1a
    .line 227
    .end local v0           #location:Landroid/location/Location;
    :cond_1a
    :goto_1a
    return-object v0

    #@1b
    :cond_1b
    move-object v0, v3

    #@1c
    .line 210
    goto :goto_7

    #@1d
    .line 221
    .restart local v0       #location:Landroid/location/Location;
    :cond_1d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@20
    move-result-wide v1

    #@21
    .line 222
    .local v1, now:J
    invoke-virtual {v0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@24
    move-result-wide v4

    #@25
    sub-long v4, v1, v4

    #@27
    const-wide v6, 0x45d964b800L

    #@2c
    cmp-long v4, v4, v6

    #@2e
    if-lez v4, :cond_1a

    #@30
    move-object v0, v3

    #@31
    .line 223
    goto :goto_1a
.end method

.method private removeExpiredFencesLocked()V
    .registers 7

    #@0
    .prologue
    .line 184
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v2

    #@4
    .line 185
    .local v2, time:J
    iget-object v4, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@6
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .line 186
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/location/GeofenceState;>;"
    :cond_a
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_20

    #@10
    .line 187
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/server/location/GeofenceState;

    #@16
    .line 188
    .local v1, state:Lcom/android/server/location/GeofenceState;
    iget-wide v4, v1, Lcom/android/server/location/GeofenceState;->mExpireAt:J

    #@18
    cmp-long v4, v4, v2

    #@1a
    if-gez v4, :cond_a

    #@1c
    .line 189
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@1f
    goto :goto_a

    #@20
    .line 192
    .end local v1           #state:Lcom/android/server/location/GeofenceState;
    :cond_20
    return-void
.end method

.method private scheduleUpdateFencesLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 195
    iget-boolean v0, p0, Lcom/android/server/location/GeofenceManager;->mPendingUpdate:Z

    #@3
    if-nez v0, :cond_c

    #@5
    .line 196
    iput-boolean v1, p0, Lcom/android/server/location/GeofenceManager;->mPendingUpdate:Z

    #@7
    .line 197
    iget-object v0, p0, Lcom/android/server/location/GeofenceManager;->mHandler:Lcom/android/server/location/GeofenceManager$GeofenceHandler;

    #@9
    invoke-virtual {v0, v1}, Lcom/android/server/location/GeofenceManager$GeofenceHandler;->sendEmptyMessage(I)Z

    #@c
    .line 199
    :cond_c
    return-void
.end method

.method private sendIntent(Landroid/app/PendingIntent;Landroid/content/Intent;)V
    .registers 12
    .parameter "pendingIntent"
    .parameter "intent"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 364
    iget-object v0, p0, Lcom/android/server/location/GeofenceManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@6
    .line 366
    :try_start_6
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mContext:Landroid/content/Context;

    #@8
    const/4 v2, 0x0

    #@9
    const/4 v5, 0x0

    #@a
    const-string v6, "android.permission.ACCESS_FINE_LOCATION"

    #@c
    move-object v0, p1

    #@d
    move-object v3, p2

    #@e
    move-object v4, p0

    #@f
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V
    :try_end_12
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_6 .. :try_end_12} :catch_13

    #@12
    .line 373
    :goto_12
    return-void

    #@13
    .line 368
    :catch_13
    move-exception v7

    #@14
    .line 369
    .local v7, e:Landroid/app/PendingIntent$CanceledException;
    invoke-virtual {p0, v8, p1}, Lcom/android/server/location/GeofenceManager;->removeFence(Landroid/location/Geofence;Landroid/app/PendingIntent;)V

    #@17
    .line 370
    iget-object v0, p0, Lcom/android/server/location/GeofenceManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@19
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1c
    goto :goto_12
.end method

.method private sendIntentEnter(Landroid/app/PendingIntent;)V
    .registers 6
    .parameter "pendingIntent"

    #@0
    .prologue
    .line 345
    const-string v1, "GeofenceManager"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendIntentEnter: pendingIntent="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 348
    new-instance v0, Landroid/content/Intent;

    #@1a
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@1d
    .line 349
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "entering"

    #@1f
    const/4 v2, 0x1

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@23
    .line 350
    invoke-direct {p0, p1, v0}, Lcom/android/server/location/GeofenceManager;->sendIntent(Landroid/app/PendingIntent;Landroid/content/Intent;)V

    #@26
    .line 351
    return-void
.end method

.method private sendIntentExit(Landroid/app/PendingIntent;)V
    .registers 6
    .parameter "pendingIntent"

    #@0
    .prologue
    .line 355
    const-string v1, "GeofenceManager"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendIntentExit: pendingIntent="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 358
    new-instance v0, Landroid/content/Intent;

    #@1a
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@1d
    .line 359
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "entering"

    #@1f
    const/4 v2, 0x0

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@23
    .line 360
    invoke-direct {p0, p1, v0}, Lcom/android/server/location/GeofenceManager;->sendIntent(Landroid/app/PendingIntent;Landroid/content/Intent;)V

    #@26
    .line 361
    return-void
.end method

.method private updateFences()V
    .registers 31

    #@0
    .prologue
    .line 238
    new-instance v5, Ljava/util/LinkedList;

    #@2
    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    #@5
    .line 239
    .local v5, enterIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    new-instance v7, Ljava/util/LinkedList;

    #@7
    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    #@a
    .line 241
    .local v7, exitIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    move-object/from16 v0, p0

    #@c
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mLock:Ljava/lang/Object;

    #@e
    move-object/from16 v21, v0

    #@10
    monitor-enter v21

    #@11
    .line 242
    const/16 v20, 0x0

    #@13
    :try_start_13
    move/from16 v0, v20

    #@15
    move-object/from16 v1, p0

    #@17
    iput-boolean v0, v1, Lcom/android/server/location/GeofenceManager;->mPendingUpdate:Z

    #@19
    .line 245
    invoke-direct/range {p0 .. p0}, Lcom/android/server/location/GeofenceManager;->removeExpiredFencesLocked()V

    #@1c
    .line 249
    invoke-direct/range {p0 .. p0}, Lcom/android/server/location/GeofenceManager;->getFreshLocationLocked()Landroid/location/Location;

    #@1f
    move-result-object v14

    #@20
    .line 253
    .local v14, location:Landroid/location/Location;
    const-wide v15, 0x7fefffffffffffffL

    #@25
    .line 254
    .local v15, minFenceDistance:D
    const/16 v17, 0x0

    #@27
    .line 255
    .local v17, needUpdates:Z
    move-object/from16 v0, p0

    #@29
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@2b
    move-object/from16 v20, v0

    #@2d
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@30
    move-result-object v10

    #@31
    .local v10, i$:Ljava/util/Iterator;
    :cond_31
    :goto_31
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@34
    move-result v20

    #@35
    if-eqz v20, :cond_f8

    #@37
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3a
    move-result-object v19

    #@3b
    check-cast v19, Lcom/android/server/location/GeofenceState;

    #@3d
    .line 256
    .local v19, state:Lcom/android/server/location/GeofenceState;
    move-object/from16 v0, p0

    #@3f
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@41
    move-object/from16 v20, v0

    #@43
    move-object/from16 v0, v19

    #@45
    iget-object v0, v0, Lcom/android/server/location/GeofenceState;->mPackageName:Ljava/lang/String;

    #@47
    move-object/from16 v22, v0

    #@49
    move-object/from16 v0, v20

    #@4b
    move-object/from16 v1, v22

    #@4d
    invoke-virtual {v0, v1}, Lcom/android/server/location/LocationBlacklist;->isBlacklisted(Ljava/lang/String;)Z

    #@50
    move-result v20

    #@51
    if-eqz v20, :cond_79

    #@53
    .line 258
    const-string v20, "GeofenceManager"

    #@55
    new-instance v22, Ljava/lang/StringBuilder;

    #@57
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v23, "skipping geofence processing for blacklisted app: "

    #@5c
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v22

    #@60
    move-object/from16 v0, v19

    #@62
    iget-object v0, v0, Lcom/android/server/location/GeofenceState;->mPackageName:Ljava/lang/String;

    #@64
    move-object/from16 v23, v0

    #@66
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v22

    #@6a
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v22

    #@6e
    move-object/from16 v0, v20

    #@70
    move-object/from16 v1, v22

    #@72
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    goto :goto_31

    #@76
    .line 332
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v14           #location:Landroid/location/Location;
    .end local v15           #minFenceDistance:D
    .end local v17           #needUpdates:Z
    .end local v19           #state:Lcom/android/server/location/GeofenceState;
    :catchall_76
    move-exception v20

    #@77
    monitor-exit v21
    :try_end_78
    .catchall {:try_start_13 .. :try_end_78} :catchall_76

    #@78
    throw v20

    #@79
    .line 265
    .restart local v10       #i$:Ljava/util/Iterator;
    .restart local v14       #location:Landroid/location/Location;
    .restart local v15       #minFenceDistance:D
    .restart local v17       #needUpdates:Z
    .restart local v19       #state:Lcom/android/server/location/GeofenceState;
    :cond_79
    :try_start_79
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7c
    move-result-object v20

    #@7d
    if-eqz v20, :cond_c5

    #@7f
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@82
    move-result-object v20

    #@83
    const/16 v22, 0x2

    #@85
    const/16 v23, 0x0

    #@87
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@8a
    move-result v24

    #@8b
    move-object/from16 v0, v19

    #@8d
    iget-object v0, v0, Lcom/android/server/location/GeofenceState;->mPackageName:Ljava/lang/String;

    #@8f
    move-object/from16 v25, v0

    #@91
    move-object/from16 v0, v20

    #@93
    move/from16 v1, v22

    #@95
    move/from16 v2, v23

    #@97
    move/from16 v3, v24

    #@99
    move-object/from16 v4, v25

    #@9b
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@9e
    move-result v20

    #@9f
    if-nez v20, :cond_c5

    #@a1
    .line 269
    const-string v20, "GeofenceManager"

    #@a3
    new-instance v22, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v23, "MDM Control LocationManager Service, state.packageName :"

    #@aa
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v22

    #@ae
    move-object/from16 v0, v19

    #@b0
    iget-object v0, v0, Lcom/android/server/location/GeofenceState;->mPackageName:Ljava/lang/String;

    #@b2
    move-object/from16 v23, v0

    #@b4
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v22

    #@b8
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v22

    #@bc
    move-object/from16 v0, v20

    #@be
    move-object/from16 v1, v22

    #@c0
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    goto/16 :goto_31

    #@c5
    .line 275
    :cond_c5
    const/16 v17, 0x1

    #@c7
    .line 276
    if-eqz v14, :cond_31

    #@c9
    .line 277
    move-object/from16 v0, v19

    #@cb
    invoke-virtual {v0, v14}, Lcom/android/server/location/GeofenceState;->processLocation(Landroid/location/Location;)I

    #@ce
    move-result v6

    #@cf
    .line 278
    .local v6, event:I
    and-int/lit8 v20, v6, 0x1

    #@d1
    if-eqz v20, :cond_de

    #@d3
    .line 279
    move-object/from16 v0, v19

    #@d5
    iget-object v0, v0, Lcom/android/server/location/GeofenceState;->mIntent:Landroid/app/PendingIntent;

    #@d7
    move-object/from16 v20, v0

    #@d9
    move-object/from16 v0, v20

    #@db
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@de
    .line 281
    :cond_de
    and-int/lit8 v20, v6, 0x2

    #@e0
    if-eqz v20, :cond_ed

    #@e2
    .line 282
    move-object/from16 v0, v19

    #@e4
    iget-object v0, v0, Lcom/android/server/location/GeofenceState;->mIntent:Landroid/app/PendingIntent;

    #@e6
    move-object/from16 v20, v0

    #@e8
    move-object/from16 v0, v20

    #@ea
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@ed
    .line 287
    :cond_ed
    invoke-virtual/range {v19 .. v19}, Lcom/android/server/location/GeofenceState;->getDistanceToBoundary()D

    #@f0
    move-result-wide v8

    #@f1
    .line 288
    .local v8, fenceDistance:D
    cmpg-double v20, v8, v15

    #@f3
    if-gez v20, :cond_31

    #@f5
    .line 289
    move-wide v15, v8

    #@f6
    goto/16 :goto_31

    #@f8
    .line 295
    .end local v6           #event:I
    .end local v8           #fenceDistance:D
    .end local v19           #state:Lcom/android/server/location/GeofenceState;
    :cond_f8
    if-eqz v17, :cond_1f9

    #@fa
    .line 299
    if-eqz v14, :cond_1f4

    #@fc
    const-wide v22, 0x7fefffffffffffffL

    #@101
    move-wide v0, v15

    #@102
    move-wide/from16 v2, v22

    #@104
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    #@107
    move-result v20

    #@108
    if-eqz v20, :cond_1f4

    #@10a
    .line 300
    const-wide v22, 0x415b774000000000L

    #@10f
    const-wide v24, 0x40ed4c0000000000L

    #@114
    const-wide v26, 0x408f400000000000L

    #@119
    mul-double v26, v26, v15

    #@11b
    const-wide/high16 v28, 0x4059

    #@11d
    div-double v26, v26, v28

    #@11f
    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->max(DD)D

    #@122
    move-result-wide v24

    #@123
    invoke-static/range {v22 .. v25}, Ljava/lang/Math;->min(DD)D

    #@126
    move-result-wide v22

    #@127
    move-wide/from16 v0, v22

    #@129
    double-to-long v12, v0

    #@12a
    .line 305
    .local v12, intervalMs:J
    :goto_12a
    move-object/from16 v0, p0

    #@12c
    iget-boolean v0, v0, Lcom/android/server/location/GeofenceManager;->mReceivingLocationUpdates:Z

    #@12e
    move/from16 v20, v0

    #@130
    if-eqz v20, :cond_13c

    #@132
    move-object/from16 v0, p0

    #@134
    iget-wide v0, v0, Lcom/android/server/location/GeofenceManager;->mLocationUpdateInterval:J

    #@136
    move-wide/from16 v22, v0

    #@138
    cmp-long v20, v22, v12

    #@13a
    if-eqz v20, :cond_17b

    #@13c
    .line 306
    :cond_13c
    const/16 v20, 0x1

    #@13e
    move/from16 v0, v20

    #@140
    move-object/from16 v1, p0

    #@142
    iput-boolean v0, v1, Lcom/android/server/location/GeofenceManager;->mReceivingLocationUpdates:Z

    #@144
    .line 307
    move-object/from16 v0, p0

    #@146
    iput-wide v12, v0, Lcom/android/server/location/GeofenceManager;->mLocationUpdateInterval:J

    #@148
    .line 308
    move-object/from16 v0, p0

    #@14a
    iput-object v14, v0, Lcom/android/server/location/GeofenceManager;->mLastLocationUpdate:Landroid/location/Location;

    #@14c
    .line 310
    new-instance v18, Landroid/location/LocationRequest;

    #@14e
    invoke-direct/range {v18 .. v18}, Landroid/location/LocationRequest;-><init>()V

    #@151
    .line 311
    .local v18, request:Landroid/location/LocationRequest;
    move-object/from16 v0, v18

    #@153
    invoke-virtual {v0, v12, v13}, Landroid/location/LocationRequest;->setInterval(J)Landroid/location/LocationRequest;

    #@156
    move-result-object v20

    #@157
    const-wide/16 v22, 0x0

    #@159
    move-object/from16 v0, v20

    #@15b
    move-wide/from16 v1, v22

    #@15d
    invoke-virtual {v0, v1, v2}, Landroid/location/LocationRequest;->setFastestInterval(J)Landroid/location/LocationRequest;

    #@160
    .line 312
    move-object/from16 v0, p0

    #@162
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mLocationManager:Landroid/location/LocationManager;

    #@164
    move-object/from16 v20, v0

    #@166
    move-object/from16 v0, p0

    #@168
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mHandler:Lcom/android/server/location/GeofenceManager$GeofenceHandler;

    #@16a
    move-object/from16 v22, v0

    #@16c
    invoke-virtual/range {v22 .. v22}, Lcom/android/server/location/GeofenceManager$GeofenceHandler;->getLooper()Landroid/os/Looper;

    #@16f
    move-result-object v22

    #@170
    move-object/from16 v0, v20

    #@172
    move-object/from16 v1, v18

    #@174
    move-object/from16 v2, p0

    #@176
    move-object/from16 v3, v22

    #@178
    invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/LocationListener;Landroid/os/Looper;)V

    #@17b
    .line 326
    .end local v12           #intervalMs:J
    .end local v18           #request:Landroid/location/LocationRequest;
    :cond_17b
    :goto_17b
    const-string v20, "GeofenceManager"

    #@17d
    new-instance v22, Ljava/lang/StringBuilder;

    #@17f
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@182
    const-string v23, "updateFences: location="

    #@184
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v22

    #@188
    move-object/from16 v0, v22

    #@18a
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v22

    #@18e
    const-string v23, ", mFences.size()="

    #@190
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v22

    #@194
    move-object/from16 v0, p0

    #@196
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@198
    move-object/from16 v23, v0

    #@19a
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    #@19d
    move-result v23

    #@19e
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v22

    #@1a2
    const-string v23, ", mReceivingLocationUpdates="

    #@1a4
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v22

    #@1a8
    move-object/from16 v0, p0

    #@1aa
    iget-boolean v0, v0, Lcom/android/server/location/GeofenceManager;->mReceivingLocationUpdates:Z

    #@1ac
    move/from16 v23, v0

    #@1ae
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v22

    #@1b2
    const-string v23, ", mLocationUpdateInterval="

    #@1b4
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v22

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    iget-wide v0, v0, Lcom/android/server/location/GeofenceManager;->mLocationUpdateInterval:J

    #@1bc
    move-wide/from16 v23, v0

    #@1be
    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v22

    #@1c2
    const-string v23, ", mLastLocationUpdate="

    #@1c4
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v22

    #@1c8
    move-object/from16 v0, p0

    #@1ca
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mLastLocationUpdate:Landroid/location/Location;

    #@1cc
    move-object/from16 v23, v0

    #@1ce
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v22

    #@1d2
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d5
    move-result-object v22

    #@1d6
    move-object/from16 v0, v20

    #@1d8
    move-object/from16 v1, v22

    #@1da
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1dd
    .line 332
    monitor-exit v21
    :try_end_1de
    .catchall {:try_start_79 .. :try_end_1de} :catchall_76

    #@1de
    .line 335
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1e1
    move-result-object v10

    #@1e2
    :goto_1e2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@1e5
    move-result v20

    #@1e6
    if-eqz v20, :cond_228

    #@1e8
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1eb
    move-result-object v11

    #@1ec
    check-cast v11, Landroid/app/PendingIntent;

    #@1ee
    .line 336
    .local v11, intent:Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    #@1f0
    invoke-direct {v0, v11}, Lcom/android/server/location/GeofenceManager;->sendIntentExit(Landroid/app/PendingIntent;)V

    #@1f3
    goto :goto_1e2

    #@1f4
    .line 303
    .end local v11           #intent:Landroid/app/PendingIntent;
    :cond_1f4
    const-wide/32 v12, 0xea60

    #@1f7
    .restart local v12       #intervalMs:J
    goto/16 :goto_12a

    #@1f9
    .line 316
    .end local v12           #intervalMs:J
    :cond_1f9
    :try_start_1f9
    move-object/from16 v0, p0

    #@1fb
    iget-boolean v0, v0, Lcom/android/server/location/GeofenceManager;->mReceivingLocationUpdates:Z

    #@1fd
    move/from16 v20, v0

    #@1ff
    if-eqz v20, :cond_17b

    #@201
    .line 317
    const/16 v20, 0x0

    #@203
    move/from16 v0, v20

    #@205
    move-object/from16 v1, p0

    #@207
    iput-boolean v0, v1, Lcom/android/server/location/GeofenceManager;->mReceivingLocationUpdates:Z

    #@209
    .line 318
    const-wide/16 v22, 0x0

    #@20b
    move-wide/from16 v0, v22

    #@20d
    move-object/from16 v2, p0

    #@20f
    iput-wide v0, v2, Lcom/android/server/location/GeofenceManager;->mLocationUpdateInterval:J

    #@211
    .line 319
    const/16 v20, 0x0

    #@213
    move-object/from16 v0, v20

    #@215
    move-object/from16 v1, p0

    #@217
    iput-object v0, v1, Lcom/android/server/location/GeofenceManager;->mLastLocationUpdate:Landroid/location/Location;

    #@219
    .line 321
    move-object/from16 v0, p0

    #@21b
    iget-object v0, v0, Lcom/android/server/location/GeofenceManager;->mLocationManager:Landroid/location/LocationManager;

    #@21d
    move-object/from16 v20, v0

    #@21f
    move-object/from16 v0, v20

    #@221
    move-object/from16 v1, p0

    #@223
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_226
    .catchall {:try_start_1f9 .. :try_end_226} :catchall_76

    #@226
    goto/16 :goto_17b

    #@228
    .line 338
    :cond_228
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@22b
    move-result-object v10

    #@22c
    :goto_22c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@22f
    move-result v20

    #@230
    if-eqz v20, :cond_23e

    #@232
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@235
    move-result-object v11

    #@236
    check-cast v11, Landroid/app/PendingIntent;

    #@238
    .line 339
    .restart local v11       #intent:Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    #@23a
    invoke-direct {v0, v11}, Lcom/android/server/location/GeofenceManager;->sendIntentEnter(Landroid/app/PendingIntent;)V

    #@23d
    goto :goto_22c

    #@23e
    .line 341
    .end local v11           #intent:Landroid/app/PendingIntent;
    :cond_23e
    return-void
.end method


# virtual methods
.method public addFence(Landroid/location/LocationRequest;Landroid/location/Geofence;Landroid/app/PendingIntent;ILjava/lang/String;)V
    .registers 14
    .parameter "request"
    .parameter "geofence"
    .parameter "intent"
    .parameter "uid"
    .parameter "packageName"

    #@0
    .prologue
    .line 119
    const-string v1, "GeofenceManager"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "addFence: request="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", geofence="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", intent="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, ", uid="

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, ", packageName="

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 123
    new-instance v0, Lcom/android/server/location/GeofenceState;

    #@42
    invoke-virtual {p1}, Landroid/location/LocationRequest;->getExpireAt()J

    #@45
    move-result-wide v2

    #@46
    move-object v1, p2

    #@47
    move-object v4, p5

    #@48
    move-object v5, p3

    #@49
    invoke-direct/range {v0 .. v5}, Lcom/android/server/location/GeofenceState;-><init>(Landroid/location/Geofence;JLjava/lang/String;Landroid/app/PendingIntent;)V

    #@4c
    .line 125
    .local v0, state:Lcom/android/server/location/GeofenceState;
    iget-object v2, p0, Lcom/android/server/location/GeofenceManager;->mLock:Ljava/lang/Object;

    #@4e
    monitor-enter v2

    #@4f
    .line 127
    :try_start_4f
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@51
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@54
    move-result v1

    #@55
    add-int/lit8 v6, v1, -0x1

    #@57
    .local v6, i:I
    :goto_57
    if-ltz v6, :cond_76

    #@59
    .line 128
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@5b
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v7

    #@5f
    check-cast v7, Lcom/android/server/location/GeofenceState;

    #@61
    .line 129
    .local v7, w:Lcom/android/server/location/GeofenceState;
    iget-object v1, v7, Lcom/android/server/location/GeofenceState;->mFence:Landroid/location/Geofence;

    #@63
    invoke-virtual {p2, v1}, Landroid/location/Geofence;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v1

    #@67
    if-eqz v1, :cond_80

    #@69
    iget-object v1, v7, Lcom/android/server/location/GeofenceState;->mIntent:Landroid/app/PendingIntent;

    #@6b
    invoke-virtual {p3, v1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v1

    #@6f
    if-eqz v1, :cond_80

    #@71
    .line 131
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@73
    invoke-interface {v1, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@76
    .line 135
    .end local v7           #w:Lcom/android/server/location/GeofenceState;
    :cond_76
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@78
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@7b
    .line 136
    invoke-direct {p0}, Lcom/android/server/location/GeofenceManager;->scheduleUpdateFencesLocked()V

    #@7e
    .line 137
    monitor-exit v2

    #@7f
    .line 138
    return-void

    #@80
    .line 127
    .restart local v7       #w:Lcom/android/server/location/GeofenceState;
    :cond_80
    add-int/lit8 v6, v6, -0x1

    #@82
    goto :goto_57

    #@83
    .line 137
    .end local v6           #i:I
    .end local v7           #w:Lcom/android/server/location/GeofenceState;
    :catchall_83
    move-exception v1

    #@84
    monitor-exit v2
    :try_end_85
    .catchall {:try_start_4f .. :try_end_85} :catchall_83

    #@85
    throw v1
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .registers 5
    .parameter "pw"

    #@0
    .prologue
    .line 410
    const-string v2, "  Geofences:"

    #@2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 412
    iget-object v2, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v0

    #@b
    .local v0, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_35

    #@11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Lcom/android/server/location/GeofenceState;

    #@17
    .line 413
    .local v1, state:Lcom/android/server/location/GeofenceState;
    const-string v2, "    "

    #@19
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@1c
    .line 414
    iget-object v2, v1, Lcom/android/server/location/GeofenceState;->mPackageName:Ljava/lang/String;

    #@1e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@21
    .line 415
    const-string v2, " "

    #@23
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@26
    .line 416
    iget-object v2, v1, Lcom/android/server/location/GeofenceState;->mFence:Landroid/location/Geofence;

    #@28
    invoke-virtual {v2}, Landroid/location/Geofence;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@2f
    .line 417
    const-string v2, "\n"

    #@31
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@34
    goto :goto_b

    #@35
    .line 419
    .end local v1           #state:Lcom/android/server/location/GeofenceState;
    :cond_35
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 5
    .parameter "location"

    #@0
    .prologue
    .line 378
    iget-object v1, p0, Lcom/android/server/location/GeofenceManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 379
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/location/GeofenceManager;->mReceivingLocationUpdates:Z

    #@5
    if-eqz v0, :cond_9

    #@7
    .line 380
    iput-object p1, p0, Lcom/android/server/location/GeofenceManager;->mLastLocationUpdate:Landroid/location/Location;

    #@9
    .line 385
    :cond_9
    iget-boolean v0, p0, Lcom/android/server/location/GeofenceManager;->mPendingUpdate:Z

    #@b
    if-eqz v0, :cond_18

    #@d
    .line 386
    iget-object v0, p0, Lcom/android/server/location/GeofenceManager;->mHandler:Lcom/android/server/location/GeofenceManager$GeofenceHandler;

    #@f
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Lcom/android/server/location/GeofenceManager$GeofenceHandler;->removeMessages(I)V

    #@13
    .line 390
    :goto_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_1c

    #@14
    .line 391
    invoke-direct {p0}, Lcom/android/server/location/GeofenceManager;->updateFences()V

    #@17
    .line 392
    return-void

    #@18
    .line 388
    :cond_18
    const/4 v0, 0x1

    #@19
    :try_start_19
    iput-boolean v0, p0, Lcom/android/server/location/GeofenceManager;->mPendingUpdate:Z

    #@1b
    goto :goto_13

    #@1c
    .line 390
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_19 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 401
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 398
    return-void
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 7
    .parameter "pendingIntent"
    .parameter "intent"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "resultExtras"

    #@0
    .prologue
    .line 406
    iget-object v0, p0, Lcom/android/server/location/GeofenceManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@5
    .line 407
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    #@0
    .prologue
    .line 395
    return-void
.end method

.method public removeFence(Landroid/location/Geofence;Landroid/app/PendingIntent;)V
    .registers 8
    .parameter "fence"
    .parameter "intent"

    #@0
    .prologue
    .line 142
    const-string v2, "GeofenceManager"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "removeFence: fence="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", intent="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 145
    iget-object v3, p0, Lcom/android/server/location/GeofenceManager;->mLock:Ljava/lang/Object;

    #@24
    monitor-enter v3

    #@25
    .line 146
    :try_start_25
    iget-object v2, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@27
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2a
    move-result-object v0

    #@2b
    .line 147
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/location/GeofenceState;>;"
    :cond_2b
    :goto_2b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_54

    #@31
    .line 148
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Lcom/android/server/location/GeofenceState;

    #@37
    .line 149
    .local v1, state:Lcom/android/server/location/GeofenceState;
    iget-object v2, v1, Lcom/android/server/location/GeofenceState;->mIntent:Landroid/app/PendingIntent;

    #@39
    invoke-virtual {v2, p2}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v2

    #@3d
    if-eqz v2, :cond_2b

    #@3f
    .line 151
    if-nez p1, :cond_48

    #@41
    .line 153
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@44
    goto :goto_2b

    #@45
    .line 163
    .end local v0           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/location/GeofenceState;>;"
    .end local v1           #state:Lcom/android/server/location/GeofenceState;
    :catchall_45
    move-exception v2

    #@46
    monitor-exit v3
    :try_end_47
    .catchall {:try_start_25 .. :try_end_47} :catchall_45

    #@47
    throw v2

    #@48
    .line 156
    .restart local v0       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/location/GeofenceState;>;"
    .restart local v1       #state:Lcom/android/server/location/GeofenceState;
    :cond_48
    :try_start_48
    iget-object v2, v1, Lcom/android/server/location/GeofenceState;->mFence:Landroid/location/Geofence;

    #@4a
    invoke-virtual {p1, v2}, Landroid/location/Geofence;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v2

    #@4e
    if-eqz v2, :cond_2b

    #@50
    .line 157
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@53
    goto :goto_2b

    #@54
    .line 162
    .end local v1           #state:Lcom/android/server/location/GeofenceState;
    :cond_54
    invoke-direct {p0}, Lcom/android/server/location/GeofenceManager;->scheduleUpdateFencesLocked()V

    #@57
    .line 163
    monitor-exit v3
    :try_end_58
    .catchall {:try_start_48 .. :try_end_58} :catchall_45

    #@58
    .line 164
    return-void
.end method

.method public removeFence(Ljava/lang/String;)V
    .registers 7
    .parameter "packageName"

    #@0
    .prologue
    .line 168
    const-string v2, "GeofenceManager"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "removeFence: packageName="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 171
    iget-object v3, p0, Lcom/android/server/location/GeofenceManager;->mLock:Ljava/lang/Object;

    #@1a
    monitor-enter v3

    #@1b
    .line 172
    :try_start_1b
    iget-object v2, p0, Lcom/android/server/location/GeofenceManager;->mFences:Ljava/util/List;

    #@1d
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v0

    #@21
    .line 173
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/location/GeofenceState;>;"
    :cond_21
    :goto_21
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_3c

    #@27
    .line 174
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v1

    #@2b
    check-cast v1, Lcom/android/server/location/GeofenceState;

    #@2d
    .line 175
    .local v1, state:Lcom/android/server/location/GeofenceState;
    iget-object v2, v1, Lcom/android/server/location/GeofenceState;->mPackageName:Ljava/lang/String;

    #@2f
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_21

    #@35
    .line 176
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@38
    goto :goto_21

    #@39
    .line 180
    .end local v0           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/location/GeofenceState;>;"
    .end local v1           #state:Lcom/android/server/location/GeofenceState;
    :catchall_39
    move-exception v2

    #@3a
    monitor-exit v3
    :try_end_3b
    .catchall {:try_start_1b .. :try_end_3b} :catchall_39

    #@3b
    throw v2

    #@3c
    .line 179
    .restart local v0       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/location/GeofenceState;>;"
    :cond_3c
    :try_start_3c
    invoke-direct {p0}, Lcom/android/server/location/GeofenceManager;->scheduleUpdateFencesLocked()V

    #@3f
    .line 180
    monitor-exit v3
    :try_end_40
    .catchall {:try_start_3c .. :try_end_40} :catchall_39

    #@40
    .line 181
    return-void
.end method
