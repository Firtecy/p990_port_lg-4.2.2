.class public Lcom/android/server/location/GeofenceState;
.super Ljava/lang/Object;
.source "GeofenceState.java"


# static fields
.field public static final FLAG_ENTER:I = 0x1

.field public static final FLAG_EXIT:I = 0x2

.field private static final STATE_INSIDE:I = 0x1

.field private static final STATE_OUTSIDE:I = 0x2

.field private static final STATE_UNKNOWN:I


# instance fields
.field mDistanceToCenter:D

.field public final mExpireAt:J

.field public final mFence:Landroid/location/Geofence;

.field public final mIntent:Landroid/app/PendingIntent;

.field private final mLocation:Landroid/location/Location;

.field public final mPackageName:Ljava/lang/String;

.field mState:I


# direct methods
.method public constructor <init>(Landroid/location/Geofence;JLjava/lang/String;Landroid/app/PendingIntent;)V
    .registers 9
    .parameter "fence"
    .parameter "expireAt"
    .parameter "packageName"
    .parameter "intent"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/server/location/GeofenceState;->mState:I

    #@6
    .line 47
    const-wide v0, 0x7fefffffffffffffL

    #@b
    iput-wide v0, p0, Lcom/android/server/location/GeofenceState;->mDistanceToCenter:D

    #@d
    .line 49
    iput-object p1, p0, Lcom/android/server/location/GeofenceState;->mFence:Landroid/location/Geofence;

    #@f
    .line 50
    iput-wide p2, p0, Lcom/android/server/location/GeofenceState;->mExpireAt:J

    #@11
    .line 51
    iput-object p4, p0, Lcom/android/server/location/GeofenceState;->mPackageName:Ljava/lang/String;

    #@13
    .line 52
    iput-object p5, p0, Lcom/android/server/location/GeofenceState;->mIntent:Landroid/app/PendingIntent;

    #@15
    .line 54
    new-instance v0, Landroid/location/Location;

    #@17
    const-string v1, ""

    #@19
    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@1c
    iput-object v0, p0, Lcom/android/server/location/GeofenceState;->mLocation:Landroid/location/Location;

    #@1e
    .line 55
    iget-object v0, p0, Lcom/android/server/location/GeofenceState;->mLocation:Landroid/location/Location;

    #@20
    invoke-virtual {p1}, Landroid/location/Geofence;->getLatitude()D

    #@23
    move-result-wide v1

    #@24
    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    #@27
    .line 56
    iget-object v0, p0, Lcom/android/server/location/GeofenceState;->mLocation:Landroid/location/Location;

    #@29
    invoke-virtual {p1}, Landroid/location/Geofence;->getLongitude()D

    #@2c
    move-result-wide v1

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    #@30
    .line 57
    return-void
.end method


# virtual methods
.method public getDistanceToBoundary()D
    .registers 5

    #@0
    .prologue
    const-wide v0, 0x7fefffffffffffffL

    #@5
    .line 88
    iget-wide v2, p0, Lcom/android/server/location/GeofenceState;->mDistanceToCenter:D

    #@7
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Double;->compare(DD)I

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_e

    #@d
    .line 91
    :goto_d
    return-wide v0

    #@e
    :cond_e
    iget-object v0, p0, Lcom/android/server/location/GeofenceState;->mFence:Landroid/location/Geofence;

    #@10
    invoke-virtual {v0}, Landroid/location/Geofence;->getRadius()F

    #@13
    move-result v0

    #@14
    float-to-double v0, v0

    #@15
    iget-wide v2, p0, Lcom/android/server/location/GeofenceState;->mDistanceToCenter:D

    #@17
    sub-double/2addr v0, v2

    #@18
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    #@1b
    move-result-wide v0

    #@1c
    goto :goto_d
.end method

.method public processLocation(Landroid/location/Location;)I
    .registers 11
    .parameter "location"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v2, 0x1

    #@3
    .line 64
    iget-object v5, p0, Lcom/android/server/location/GeofenceState;->mLocation:Landroid/location/Location;

    #@5
    invoke-virtual {v5, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    #@8
    move-result v5

    #@9
    float-to-double v5, v5

    #@a
    iput-wide v5, p0, Lcom/android/server/location/GeofenceState;->mDistanceToCenter:D

    #@c
    .line 66
    iget v1, p0, Lcom/android/server/location/GeofenceState;->mState:I

    #@e
    .line 68
    .local v1, prevState:I
    iget-wide v5, p0, Lcom/android/server/location/GeofenceState;->mDistanceToCenter:D

    #@10
    iget-object v7, p0, Lcom/android/server/location/GeofenceState;->mFence:Landroid/location/Geofence;

    #@12
    invoke-virtual {v7}, Landroid/location/Geofence;->getRadius()F

    #@15
    move-result v7

    #@16
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@19
    move-result v8

    #@1a
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@1d
    move-result v7

    #@1e
    float-to-double v7, v7

    #@1f
    cmpg-double v5, v5, v7

    #@21
    if-gtz v5, :cond_2b

    #@23
    move v0, v2

    #@24
    .line 69
    .local v0, inside:Z
    :goto_24
    if-eqz v0, :cond_2d

    #@26
    .line 70
    iput v2, p0, Lcom/android/server/location/GeofenceState;->mState:I

    #@28
    .line 71
    if-eq v1, v2, :cond_33

    #@2a
    .line 80
    :goto_2a
    return v2

    #@2b
    .end local v0           #inside:Z
    :cond_2b
    move v0, v4

    #@2c
    .line 68
    goto :goto_24

    #@2d
    .line 75
    .restart local v0       #inside:Z
    :cond_2d
    iput v3, p0, Lcom/android/server/location/GeofenceState;->mState:I

    #@2f
    .line 76
    if-ne v1, v2, :cond_33

    #@31
    move v2, v3

    #@32
    .line 77
    goto :goto_2a

    #@33
    :cond_33
    move v2, v4

    #@34
    .line 80
    goto :goto_2a
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 98
    iget v1, p0, Lcom/android/server/location/GeofenceState;->mState:I

    #@2
    packed-switch v1, :pswitch_data_2c

    #@5
    .line 106
    const-string v0, "?"

    #@7
    .line 108
    .local v0, state:Ljava/lang/String;
    :goto_7
    const-string v1, "%s d=%.0f %s"

    #@9
    const/4 v2, 0x3

    #@a
    new-array v2, v2, [Ljava/lang/Object;

    #@c
    const/4 v3, 0x0

    #@d
    iget-object v4, p0, Lcom/android/server/location/GeofenceState;->mFence:Landroid/location/Geofence;

    #@f
    invoke-virtual {v4}, Landroid/location/Geofence;->toString()Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    aput-object v4, v2, v3

    #@15
    const/4 v3, 0x1

    #@16
    iget-wide v4, p0, Lcom/android/server/location/GeofenceState;->mDistanceToCenter:D

    #@18
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@1b
    move-result-object v4

    #@1c
    aput-object v4, v2, v3

    #@1e
    const/4 v3, 0x2

    #@1f
    aput-object v0, v2, v3

    #@21
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    return-object v1

    #@26
    .line 100
    .end local v0           #state:Ljava/lang/String;
    :pswitch_26
    const-string v0, "IN"

    #@28
    .line 101
    .restart local v0       #state:Ljava/lang/String;
    goto :goto_7

    #@29
    .line 103
    .end local v0           #state:Ljava/lang/String;
    :pswitch_29
    const-string v0, "OUT"

    #@2b
    .line 104
    .restart local v0       #state:Ljava/lang/String;
    goto :goto_7

    #@2c
    .line 98
    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_26
        :pswitch_29
    .end packed-switch
.end method
