.class public Lcom/android/server/location/LocationBasedCountryDetector;
.super Lcom/android/server/location/CountryDetectorBase;
.source "LocationBasedCountryDetector.java"


# static fields
.field private static final QUERY_LOCATION_TIMEOUT:J = 0x493e0L

.field private static final TAG:Ljava/lang/String; = "LocationBasedCountryDetector"


# instance fields
.field private mEnabledProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mLocationListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/location/LocationListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationManager:Landroid/location/LocationManager;

.field protected mQueryThread:Ljava/lang/Thread;

.field protected mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "ctx"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/android/server/location/CountryDetectorBase;-><init>(Landroid/content/Context;)V

    #@3
    .line 68
    const-string v0, "location"

    #@5
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/location/LocationManager;

    #@b
    iput-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationManager:Landroid/location/LocationManager;

    #@d
    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/location/LocationBasedCountryDetector;Landroid/location/Location;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationBasedCountryDetector;->queryCountryCode(Landroid/location/Location;)V

    #@3
    return-void
.end method

.method private declared-synchronized queryCountryCode(Landroid/location/Location;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    .line 222
    monitor-enter p0

    #@1
    if-nez p1, :cond_9

    #@3
    .line 223
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-virtual {p0, v0}, Lcom/android/server/location/LocationBasedCountryDetector;->notifyListener(Landroid/location/Country;)V
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_1f

    #@7
    .line 244
    :cond_7
    :goto_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 226
    :cond_9
    :try_start_9
    iget-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mQueryThread:Ljava/lang/Thread;

    #@b
    if-nez v0, :cond_7

    #@d
    .line 227
    new-instance v0, Ljava/lang/Thread;

    #@f
    new-instance v1, Lcom/android/server/location/LocationBasedCountryDetector$3;

    #@11
    invoke-direct {v1, p0, p1}, Lcom/android/server/location/LocationBasedCountryDetector$3;-><init>(Lcom/android/server/location/LocationBasedCountryDetector;Landroid/location/Location;)V

    #@14
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@17
    iput-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mQueryThread:Ljava/lang/Thread;

    #@19
    .line 243
    iget-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mQueryThread:Ljava/lang/Thread;

    #@1b
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_1f

    #@1e
    goto :goto_7

    #@1f
    .line 222
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit p0

    #@21
    throw v0
.end method


# virtual methods
.method public declared-synchronized detectCountry()Landroid/location/Country;
    .registers 10

    #@0
    .prologue
    .line 149
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v5, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationListeners:Ljava/util/List;

    #@3
    if-eqz v5, :cond_e

    #@5
    .line 150
    new-instance v5, Ljava/lang/IllegalStateException;

    #@7
    invoke-direct {v5}, Ljava/lang/IllegalStateException;-><init>()V

    #@a
    throw v5
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_b

    #@b
    .line 149
    :catchall_b
    move-exception v5

    #@c
    monitor-exit p0

    #@d
    throw v5

    #@e
    .line 153
    :cond_e
    :try_start_e
    invoke-virtual {p0}, Lcom/android/server/location/LocationBasedCountryDetector;->getEnabledProviders()Ljava/util/List;

    #@11
    move-result-object v0

    #@12
    .line 154
    .local v0, enabledProviders:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@15
    move-result v4

    #@16
    .line 155
    .local v4, totalProviders:I
    if-lez v4, :cond_57

    #@18
    .line 156
    new-instance v5, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    #@1d
    iput-object v5, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationListeners:Ljava/util/List;

    #@1f
    .line 157
    const/4 v1, 0x0

    #@20
    .local v1, i:I
    :goto_20
    if-ge v1, v4, :cond_3e

    #@22
    .line 158
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Ljava/lang/String;

    #@28
    .line 159
    .local v3, provider:Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/android/server/location/LocationBasedCountryDetector;->isAcceptableProvider(Ljava/lang/String;)Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_3b

    #@2e
    .line 160
    new-instance v2, Lcom/android/server/location/LocationBasedCountryDetector$1;

    #@30
    invoke-direct {v2, p0}, Lcom/android/server/location/LocationBasedCountryDetector$1;-><init>(Lcom/android/server/location/LocationBasedCountryDetector;)V

    #@33
    .line 178
    .local v2, listener:Landroid/location/LocationListener;
    iget-object v5, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationListeners:Ljava/util/List;

    #@35
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@38
    .line 179
    invoke-virtual {p0, v3, v2}, Lcom/android/server/location/LocationBasedCountryDetector;->registerListener(Ljava/lang/String;Landroid/location/LocationListener;)V

    #@3b
    .line 157
    .end local v2           #listener:Landroid/location/LocationListener;
    :cond_3b
    add-int/lit8 v1, v1, 0x1

    #@3d
    goto :goto_20

    #@3e
    .line 183
    .end local v3           #provider:Ljava/lang/String;
    :cond_3e
    new-instance v5, Ljava/util/Timer;

    #@40
    invoke-direct {v5}, Ljava/util/Timer;-><init>()V

    #@43
    iput-object v5, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mTimer:Ljava/util/Timer;

    #@45
    .line 184
    iget-object v5, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mTimer:Ljava/util/Timer;

    #@47
    new-instance v6, Lcom/android/server/location/LocationBasedCountryDetector$2;

    #@49
    invoke-direct {v6, p0}, Lcom/android/server/location/LocationBasedCountryDetector$2;-><init>(Lcom/android/server/location/LocationBasedCountryDetector;)V

    #@4c
    invoke-virtual {p0}, Lcom/android/server/location/LocationBasedCountryDetector;->getQueryLocationTimeout()J

    #@4f
    move-result-wide v7

    #@50
    invoke-virtual {v5, v6, v7, v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    #@53
    .line 198
    .end local v1           #i:I
    :goto_53
    iget-object v5, p0, Lcom/android/server/location/CountryDetectorBase;->mDetectedCountry:Landroid/location/Country;
    :try_end_55
    .catchall {:try_start_e .. :try_end_55} :catchall_b

    #@55
    monitor-exit p0

    #@56
    return-object v5

    #@57
    .line 196
    :cond_57
    :try_start_57
    invoke-virtual {p0}, Lcom/android/server/location/LocationBasedCountryDetector;->getLastKnownLocation()Landroid/location/Location;

    #@5a
    move-result-object v5

    #@5b
    invoke-direct {p0, v5}, Lcom/android/server/location/LocationBasedCountryDetector;->queryCountryCode(Landroid/location/Location;)V
    :try_end_5e
    .catchall {:try_start_57 .. :try_end_5e} :catchall_b

    #@5e
    goto :goto_53
.end method

.method protected getCountryFromLocation(Landroid/location/Location;)Ljava/lang/String;
    .registers 11
    .parameter "location"

    #@0
    .prologue
    .line 75
    const/4 v7, 0x0

    #@1
    .line 76
    .local v7, country:Ljava/lang/String;
    new-instance v0, Landroid/location/Geocoder;

    #@3
    iget-object v1, p0, Lcom/android/server/location/CountryDetectorBase;->mContext:Landroid/content/Context;

    #@5
    invoke-direct {v0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    #@8
    .line 78
    .local v0, geoCoder:Landroid/location/Geocoder;
    :try_start_8
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    #@b
    move-result-wide v1

    #@c
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    #@f
    move-result-wide v3

    #@10
    const/4 v5, 0x1

    #@11
    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    #@14
    move-result-object v6

    #@15
    .line 80
    .local v6, addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v6, :cond_28

    #@17
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@1a
    move-result v1

    #@1b
    if-lez v1, :cond_28

    #@1d
    .line 81
    const/4 v1, 0x0

    #@1e
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/location/Address;

    #@24
    invoke-virtual {v1}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_27} :catch_29

    #@27
    move-result-object v7

    #@28
    .line 86
    .end local v6           #addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    :cond_28
    :goto_28
    return-object v7

    #@29
    .line 83
    :catch_29
    move-exception v8

    #@2a
    .line 84
    .local v8, e:Ljava/io/IOException;
    const-string v1, "LocationBasedCountryDetector"

    #@2c
    const-string v2, "Exception occurs when getting country from location"

    #@2e
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_28
.end method

.method protected getEnabledProviders()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mEnabledProviders:Ljava/util/List;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 136
    iget-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationManager:Landroid/location/LocationManager;

    #@6
    const/4 v1, 0x1

    #@7
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mEnabledProviders:Ljava/util/List;

    #@d
    .line 138
    :cond_d
    iget-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mEnabledProviders:Ljava/util/List;

    #@f
    return-object v0
.end method

.method protected getLastKnownLocation()Landroid/location/Location;
    .registers 10

    #@0
    .prologue
    .line 112
    iget-object v5, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationManager:Landroid/location/LocationManager;

    #@2
    invoke-virtual {v5}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    #@5
    move-result-object v4

    #@6
    .line 113
    .local v4, providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@7
    .line 114
    .local v0, bestLocation:Landroid/location/Location;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_2f

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Ljava/lang/String;

    #@17
    .line 115
    .local v3, provider:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationManager:Landroid/location/LocationManager;

    #@19
    invoke-virtual {v5, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    #@1c
    move-result-object v2

    #@1d
    .line 116
    .local v2, lastKnownLocation:Landroid/location/Location;
    if-eqz v2, :cond_b

    #@1f
    .line 117
    if-eqz v0, :cond_2d

    #@21
    invoke-virtual {v0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@24
    move-result-wide v5

    #@25
    invoke-virtual {v2}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@28
    move-result-wide v7

    #@29
    cmp-long v5, v5, v7

    #@2b
    if-gez v5, :cond_b

    #@2d
    .line 120
    :cond_2d
    move-object v0, v2

    #@2e
    goto :goto_b

    #@2f
    .line 124
    .end local v2           #lastKnownLocation:Landroid/location/Location;
    .end local v3           #provider:Ljava/lang/String;
    :cond_2f
    return-object v0
.end method

.method protected getQueryLocationTimeout()J
    .registers 3

    #@0
    .prologue
    .line 131
    const-wide/32 v0, 0x493e0

    #@3
    return-wide v0
.end method

.method protected isAcceptableProvider(Ljava/lang/String;)Z
    .registers 3
    .parameter "provider"

    #@0
    .prologue
    .line 91
    const-string v0, "passive"

    #@2
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected registerListener(Ljava/lang/String;Landroid/location/LocationListener;)V
    .registers 9
    .parameter "provider"
    .parameter "listener"

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationManager:Landroid/location/LocationManager;

    #@2
    const-wide/16 v2, 0x0

    #@4
    const/4 v4, 0x0

    #@5
    move-object v1, p1

    #@6
    move-object v5, p2

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    #@a
    .line 99
    return-void
.end method

.method public declared-synchronized stop()V
    .registers 4

    #@0
    .prologue
    .line 206
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationListeners:Ljava/util/List;

    #@3
    if-eqz v2, :cond_21

    #@5
    .line 207
    iget-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationListeners:Ljava/util/List;

    #@7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v0

    #@b
    .local v0, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_1e

    #@11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/location/LocationListener;

    #@17
    .line 208
    .local v1, listener:Landroid/location/LocationListener;
    invoke-virtual {p0, v1}, Lcom/android/server/location/LocationBasedCountryDetector;->unregisterListener(Landroid/location/LocationListener;)V
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1b

    #@1a
    goto :goto_b

    #@1b
    .line 206
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Landroid/location/LocationListener;
    :catchall_1b
    move-exception v2

    #@1c
    monitor-exit p0

    #@1d
    throw v2

    #@1e
    .line 210
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_1e
    const/4 v2, 0x0

    #@1f
    :try_start_1f
    iput-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationListeners:Ljava/util/List;

    #@21
    .line 212
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_21
    iget-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mTimer:Ljava/util/Timer;

    #@23
    if-eqz v2, :cond_2d

    #@25
    .line 213
    iget-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mTimer:Ljava/util/Timer;

    #@27
    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    #@2a
    .line 214
    const/4 v2, 0x0

    #@2b
    iput-object v2, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mTimer:Ljava/util/Timer;
    :try_end_2d
    .catchall {:try_start_1f .. :try_end_2d} :catchall_1b

    #@2d
    .line 216
    :cond_2d
    monitor-exit p0

    #@2e
    return-void
.end method

.method protected unregisterListener(Landroid/location/LocationListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/server/location/LocationBasedCountryDetector;->mLocationManager:Landroid/location/LocationManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@5
    .line 106
    return-void
.end method
