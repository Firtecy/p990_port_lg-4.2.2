.class public Lcom/android/server/location/PassiveProvider;
.super Ljava/lang/Object;
.source "PassiveProvider.java"

# interfaces
.implements Lcom/android/server/location/LocationProviderInterface;


# static fields
.field private static final PROPERTIES:Lcom/android/internal/location/ProviderProperties; = null

.field private static final TAG:Ljava/lang/String; = "PassiveProvider"


# instance fields
.field private final mLocationManager:Landroid/location/ILocationManager;

.field private mReportLocation:Z


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 46
    new-instance v0, Lcom/android/internal/location/ProviderProperties;

    #@3
    const/4 v8, 0x1

    #@4
    const/4 v9, 0x2

    #@5
    move v2, v1

    #@6
    move v3, v1

    #@7
    move v4, v1

    #@8
    move v5, v1

    #@9
    move v6, v1

    #@a
    move v7, v1

    #@b
    invoke-direct/range {v0 .. v9}, Lcom/android/internal/location/ProviderProperties;-><init>(ZZZZZZZII)V

    #@e
    sput-object v0, Lcom/android/server/location/PassiveProvider;->PROPERTIES:Lcom/android/internal/location/ProviderProperties;

    #@10
    return-void
.end method

.method public constructor <init>(Landroid/location/ILocationManager;)V
    .registers 2
    .parameter "locationManager"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    iput-object p1, p0, Lcom/android/server/location/PassiveProvider;->mLocationManager:Landroid/location/ILocationManager;

    #@5
    .line 55
    return-void
.end method


# virtual methods
.method public disable()V
    .registers 1

    #@0
    .prologue
    .line 78
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "mReportLocation="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/server/location/PassiveProvider;->mReportLocation:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@18
    .line 123
    return-void
.end method

.method public enable()V
    .registers 1

    #@0
    .prologue
    .line 74
    return-void
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 59
    const-string v0, "passive"

    #@2
    return-object v0
.end method

.method public getProperties()Lcom/android/internal/location/ProviderProperties;
    .registers 2

    #@0
    .prologue
    .line 64
    sget-object v0, Lcom/android/server/location/PassiveProvider;->PROPERTIES:Lcom/android/internal/location/ProviderProperties;

    #@2
    return-object v0
.end method

.method public getStatus(Landroid/os/Bundle;)I
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/location/PassiveProvider;->mReportLocation:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 83
    const/4 v0, 0x2

    #@5
    .line 85
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x1

    #@7
    goto :goto_5
.end method

.method public getStatusUpdateTime()J
    .registers 3

    #@0
    .prologue
    .line 91
    const-wide/16 v0, -0x1

    #@2
    return-wide v0
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 69
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    .line 117
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 4
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    .line 96
    iget-boolean v0, p1, Lcom/android/internal/location/ProviderRequest;->reportLocation:Z

    #@2
    iput-boolean v0, p0, Lcom/android/server/location/PassiveProvider;->mReportLocation:Z

    #@4
    .line 97
    return-void
.end method

.method public switchUser(I)V
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 102
    return-void
.end method

.method public updateLocation(Landroid/location/Location;)V
    .registers 5
    .parameter "location"

    #@0
    .prologue
    .line 105
    iget-boolean v1, p0, Lcom/android/server/location/PassiveProvider;->mReportLocation:Z

    #@2
    if-eqz v1, :cond_a

    #@4
    .line 108
    :try_start_4
    iget-object v1, p0, Lcom/android/server/location/PassiveProvider;->mLocationManager:Landroid/location/ILocationManager;

    #@6
    const/4 v2, 0x1

    #@7
    invoke-interface {v1, p1, v2}, Landroid/location/ILocationManager;->reportLocation(Landroid/location/Location;Z)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_a} :catch_b

    #@a
    .line 113
    :cond_a
    :goto_a
    return-void

    #@b
    .line 109
    :catch_b
    move-exception v0

    #@c
    .line 110
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "PassiveProvider"

    #@e
    const-string v2, "RemoteException calling reportLocation"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    goto :goto_a
.end method
