.class Lcom/android/server/location/GpsLocationProvider$1;
.super Landroid/location/IGpsStatusProvider$Stub;
.source "GpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 366
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    invoke-direct {p0}, Landroid/location/IGpsStatusProvider$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public addGpsStatusListener(Landroid/location/IGpsStatusListener;)V
    .registers 9
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 369
    if-nez p1, :cond_a

    #@2
    .line 370
    new-instance v5, Ljava/lang/NullPointerException;

    #@4
    const-string v6, "listener is null in addGpsStatusListener"

    #@6
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v5

    #@a
    .line 373
    :cond_a
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@c
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@f
    move-result-object v6

    #@10
    monitor-enter v6

    #@11
    .line 374
    :try_start_11
    invoke-interface {p1}, Landroid/location/IGpsStatusListener;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v0

    #@15
    .line 375
    .local v0, binder:Landroid/os/IBinder;
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@17
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v3

    #@1f
    .line 376
    .local v3, size:I
    const/4 v1, 0x0

    #@20
    .local v1, i:I
    :goto_20
    if-ge v1, v3, :cond_3f

    #@22
    .line 377
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@24
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v4

    #@2c
    check-cast v4, Lcom/android/server/location/GpsLocationProvider$Listener;

    #@2e
    .line 378
    .local v4, test:Lcom/android/server/location/GpsLocationProvider$Listener;
    iget-object v5, v4, Lcom/android/server/location/GpsLocationProvider$Listener;->mListener:Landroid/location/IGpsStatusListener;

    #@30
    invoke-interface {v5}, Landroid/location/IGpsStatusListener;->asBinder()Landroid/os/IBinder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v0, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v5

    #@38
    if-eqz v5, :cond_3c

    #@3a
    .line 380
    monitor-exit v6

    #@3b
    .line 388
    .end local v4           #test:Lcom/android/server/location/GpsLocationProvider$Listener;
    :goto_3b
    return-void

    #@3c
    .line 376
    .restart local v4       #test:Lcom/android/server/location/GpsLocationProvider$Listener;
    :cond_3c
    add-int/lit8 v1, v1, 0x1

    #@3e
    goto :goto_20

    #@3f
    .line 384
    .end local v4           #test:Lcom/android/server/location/GpsLocationProvider$Listener;
    :cond_3f
    new-instance v2, Lcom/android/server/location/GpsLocationProvider$Listener;

    #@41
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@43
    invoke-direct {v2, v5, p1}, Lcom/android/server/location/GpsLocationProvider$Listener;-><init>(Lcom/android/server/location/GpsLocationProvider;Landroid/location/IGpsStatusListener;)V

    #@46
    .line 385
    .local v2, l:Lcom/android/server/location/GpsLocationProvider$Listener;
    const/4 v5, 0x0

    #@47
    invoke-interface {v0, v2, v5}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@4a
    .line 386
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@4c
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    .line 387
    monitor-exit v6

    #@54
    goto :goto_3b

    #@55
    .end local v0           #binder:Landroid/os/IBinder;
    .end local v1           #i:I
    .end local v2           #l:Lcom/android/server/location/GpsLocationProvider$Listener;
    .end local v3           #size:I
    :catchall_55
    move-exception v5

    #@56
    monitor-exit v6
    :try_end_57
    .catchall {:try_start_11 .. :try_end_57} :catchall_55

    #@57
    throw v5
.end method

.method public removeGpsStatusListener(Landroid/location/IGpsStatusListener;)V
    .registers 9
    .parameter "listener"

    #@0
    .prologue
    .line 392
    if-nez p1, :cond_a

    #@2
    .line 393
    new-instance v5, Ljava/lang/NullPointerException;

    #@4
    const-string v6, "listener is null in addGpsStatusListener"

    #@6
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v5

    #@a
    .line 396
    :cond_a
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@c
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@f
    move-result-object v6

    #@10
    monitor-enter v6

    #@11
    .line 397
    :try_start_11
    invoke-interface {p1}, Landroid/location/IGpsStatusListener;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v0

    #@15
    .line 398
    .local v0, binder:Landroid/os/IBinder;
    const/4 v2, 0x0

    #@16
    .line 399
    .local v2, l:Lcom/android/server/location/GpsLocationProvider$Listener;
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@18
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v3

    #@20
    .line 400
    .local v3, size:I
    const/4 v1, 0x0

    #@21
    .local v1, i:I
    :goto_21
    if-ge v1, v3, :cond_41

    #@23
    if-nez v2, :cond_41

    #@25
    .line 401
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@27
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Lcom/android/server/location/GpsLocationProvider$Listener;

    #@31
    .line 402
    .local v4, test:Lcom/android/server/location/GpsLocationProvider$Listener;
    iget-object v5, v4, Lcom/android/server/location/GpsLocationProvider$Listener;->mListener:Landroid/location/IGpsStatusListener;

    #@33
    invoke-interface {v5}, Landroid/location/IGpsStatusListener;->asBinder()Landroid/os/IBinder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v0, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v5

    #@3b
    if-eqz v5, :cond_3e

    #@3d
    .line 403
    move-object v2, v4

    #@3e
    .line 400
    :cond_3e
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_21

    #@41
    .line 407
    .end local v4           #test:Lcom/android/server/location/GpsLocationProvider$Listener;
    :cond_41
    if-eqz v2, :cond_50

    #@43
    .line 408
    iget-object v5, p0, Lcom/android/server/location/GpsLocationProvider$1;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@45
    invoke-static {v5}, Lcom/android/server/location/GpsLocationProvider;->access$000(Lcom/android/server/location/GpsLocationProvider;)Ljava/util/ArrayList;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@4c
    .line 409
    const/4 v5, 0x0

    #@4d
    invoke-interface {v0, v2, v5}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@50
    .line 411
    :cond_50
    monitor-exit v6

    #@51
    .line 412
    return-void

    #@52
    .line 411
    .end local v0           #binder:Landroid/os/IBinder;
    .end local v1           #i:I
    .end local v2           #l:Lcom/android/server/location/GpsLocationProvider$Listener;
    .end local v3           #size:I
    :catchall_52
    move-exception v5

    #@53
    monitor-exit v6
    :try_end_54
    .catchall {:try_start_11 .. :try_end_54} :catchall_52

    #@54
    throw v5
.end method
