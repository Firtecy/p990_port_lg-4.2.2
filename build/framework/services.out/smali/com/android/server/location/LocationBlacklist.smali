.class public final Lcom/android/server/location/LocationBlacklist;
.super Landroid/database/ContentObserver;
.source "LocationBlacklist.java"


# static fields
.field private static final BLACKLIST_CONFIG_NAME:Ljava/lang/String; = "locationPackagePrefixBlacklist"

.field private static final D:Z = true

.field private static final TAG:Ljava/lang/String; = "LocationBlacklist"

.field private static final WHITELIST_CONFIG_NAME:Ljava/lang/String; = "locationPackagePrefixWhitelist"


# instance fields
.field private mBlacklist:[Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mLock:Ljava/lang/Object;

.field private mWhitelist:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 5
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 56
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@4
    .line 47
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mLock:Ljava/lang/Object;

    #@b
    .line 50
    new-array v0, v1, [Ljava/lang/String;

    #@d
    iput-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mWhitelist:[Ljava/lang/String;

    #@f
    .line 51
    new-array v0, v1, [Ljava/lang/String;

    #@11
    iput-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mBlacklist:[Ljava/lang/String;

    #@13
    .line 53
    iput v1, p0, Lcom/android/server/location/LocationBlacklist;->mCurrentUserId:I

    #@15
    .line 57
    iput-object p1, p0, Lcom/android/server/location/LocationBlacklist;->mContext:Landroid/content/Context;

    #@17
    .line 58
    return-void
.end method

.method private getStringArrayLocked(Ljava/lang/String;)[Ljava/lang/String;
    .registers 12
    .parameter "key"

    #@0
    .prologue
    .line 128
    iget-object v8, p0, Lcom/android/server/location/LocationBlacklist;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v8

    #@3
    .line 129
    :try_start_3
    iget-object v7, p0, Lcom/android/server/location/LocationBlacklist;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v7

    #@9
    iget v9, p0, Lcom/android/server/location/LocationBlacklist;->mCurrentUserId:I

    #@b
    invoke-static {v7, p1, v9}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 131
    .local v1, flatString:Ljava/lang/String;
    monitor-exit v8
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_16

    #@10
    .line 132
    if-nez v1, :cond_19

    #@12
    .line 133
    const/4 v7, 0x0

    #@13
    new-array v7, v7, [Ljava/lang/String;

    #@15
    .line 144
    :goto_15
    return-object v7

    #@16
    .line 131
    .end local v1           #flatString:Ljava/lang/String;
    :catchall_16
    move-exception v7

    #@17
    :try_start_17
    monitor-exit v8
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v7

    #@19
    .line 135
    .restart local v1       #flatString:Ljava/lang/String;
    :cond_19
    const-string v7, ","

    #@1b
    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    .line 136
    .local v6, splitStrings:[Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    #@21
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@24
    .line 137
    .local v5, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, v6

    #@25
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@26
    .local v3, len$:I
    const/4 v2, 0x0

    #@27
    .local v2, i$:I
    :goto_27
    if-ge v2, v3, :cond_3c

    #@29
    aget-object v4, v0, v2

    #@2b
    .line 138
    .local v4, pkg:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    .line 139
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@32
    move-result v7

    #@33
    if-eqz v7, :cond_38

    #@35
    .line 137
    :goto_35
    add-int/lit8 v2, v2, 0x1

    #@37
    goto :goto_27

    #@38
    .line 142
    :cond_38
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3b
    goto :goto_35

    #@3c
    .line 144
    .end local v4           #pkg:Ljava/lang/String;
    :cond_3c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v7

    #@40
    new-array v7, v7, [Ljava/lang/String;

    #@42
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@45
    move-result-object v7

    #@46
    check-cast v7, [Ljava/lang/String;

    #@48
    goto :goto_15
.end method

.method private inWhitelist(Ljava/lang/String;)Z
    .registers 8
    .parameter "pkg"

    #@0
    .prologue
    .line 106
    iget-object v5, p0, Lcom/android/server/location/LocationBlacklist;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 107
    :try_start_3
    iget-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mWhitelist:[Ljava/lang/String;

    #@5
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@6
    .local v2, len$:I
    const/4 v1, 0x0

    #@7
    .local v1, i$:I
    :goto_7
    if-ge v1, v2, :cond_17

    #@9
    aget-object v3, v0, v1

    #@b
    .line 108
    .local v3, white:Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_14

    #@11
    const/4 v4, 0x1

    #@12
    monitor-exit v5

    #@13
    .line 111
    .end local v3           #white:Ljava/lang/String;
    :goto_13
    return v4

    #@14
    .line 107
    .restart local v3       #white:Ljava/lang/String;
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_7

    #@17
    .line 110
    .end local v3           #white:Ljava/lang/String;
    :cond_17
    monitor-exit v5

    #@18
    .line 111
    const/4 v4, 0x0

    #@19
    goto :goto_13

    #@1a
    .line 110
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_1a
    move-exception v4

    #@1b
    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v4
.end method

.method private reloadBlacklist()V
    .registers 3

    #@0
    .prologue
    .line 76
    iget-object v1, p0, Lcom/android/server/location/LocationBlacklist;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 77
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/location/LocationBlacklist;->reloadBlacklistLocked()V

    #@6
    .line 78
    monitor-exit v1

    #@7
    .line 79
    return-void

    #@8
    .line 78
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private reloadBlacklistLocked()V
    .registers 4

    #@0
    .prologue
    .line 69
    const-string v0, "locationPackagePrefixWhitelist"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/location/LocationBlacklist;->getStringArrayLocked(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mWhitelist:[Ljava/lang/String;

    #@8
    .line 70
    const-string v0, "LocationBlacklist"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "whitelist: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Lcom/android/server/location/LocationBlacklist;->mWhitelist:[Ljava/lang/String;

    #@17
    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 71
    const-string v0, "locationPackagePrefixBlacklist"

    #@28
    invoke-direct {p0, v0}, Lcom/android/server/location/LocationBlacklist;->getStringArrayLocked(Ljava/lang/String;)[Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mBlacklist:[Ljava/lang/String;

    #@2e
    .line 72
    const-string v0, "LocationBlacklist"

    #@30
    new-instance v1, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v2, "blacklist: "

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    iget-object v2, p0, Lcom/android/server/location/LocationBlacklist;->mBlacklist:[Ljava/lang/String;

    #@3d
    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 73
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "mWhitelist="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/server/location/LocationBlacklist;->mWhitelist:[Ljava/lang/String;

    #@d
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, " mBlacklist="

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-object v1, p0, Lcom/android/server/location/LocationBlacklist;->mBlacklist:[Ljava/lang/String;

    #@1d
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c
    .line 150
    return-void
.end method

.method public init()V
    .registers 5

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "locationPackagePrefixBlacklist"

    #@8
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@b
    move-result-object v1

    #@c
    const/4 v2, 0x0

    #@d
    const/4 v3, -0x1

    #@e
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@11
    .line 65
    invoke-direct {p0}, Lcom/android/server/location/LocationBlacklist;->reloadBlacklist()V

    #@14
    .line 66
    return-void
.end method

.method public isBlacklisted(Ljava/lang/String;)Z
    .registers 10
    .parameter "packageName"

    #@0
    .prologue
    .line 86
    iget-object v5, p0, Lcom/android/server/location/LocationBlacklist;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 87
    :try_start_3
    iget-object v0, p0, Lcom/android/server/location/LocationBlacklist;->mBlacklist:[Ljava/lang/String;

    #@5
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@6
    .local v3, len$:I
    const/4 v2, 0x0

    #@7
    .local v2, i$:I
    :goto_7
    if-ge v2, v3, :cond_3f

    #@9
    aget-object v1, v0, v2

    #@b
    .line 88
    .local v1, black:Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_17

    #@11
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationBlacklist;->inWhitelist(Ljava/lang/String;)Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_1a

    #@17
    .line 87
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_7

    #@1a
    .line 92
    :cond_1a
    const-string v4, "LocationBlacklist"

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "dropping location (blacklisted): "

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    const-string v7, " matches "

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 94
    const/4 v4, 0x1

    #@3d
    monitor-exit v5

    #@3e
    .line 99
    .end local v1           #black:Ljava/lang/String;
    :goto_3e
    return v4

    #@3f
    .line 98
    :cond_3f
    monitor-exit v5

    #@40
    .line 99
    const/4 v4, 0x0

    #@41
    goto :goto_3e

    #@42
    .line 98
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :catchall_42
    move-exception v4

    #@43
    monitor-exit v5
    :try_end_44
    .catchall {:try_start_3 .. :try_end_44} :catchall_42

    #@44
    throw v4
.end method

.method public onChange(Z)V
    .registers 2
    .parameter "selfChange"

    #@0
    .prologue
    .line 116
    invoke-direct {p0}, Lcom/android/server/location/LocationBlacklist;->reloadBlacklist()V

    #@3
    .line 117
    return-void
.end method

.method public switchUser(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 120
    iget-object v1, p0, Lcom/android/server/location/LocationBlacklist;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 121
    :try_start_3
    iput p1, p0, Lcom/android/server/location/LocationBlacklist;->mCurrentUserId:I

    #@5
    .line 122
    invoke-direct {p0}, Lcom/android/server/location/LocationBlacklist;->reloadBlacklistLocked()V

    #@8
    .line 123
    monitor-exit v1

    #@9
    .line 124
    return-void

    #@a
    .line 123
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method
