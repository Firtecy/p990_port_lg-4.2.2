.class Lcom/android/server/location/GpsLocationProvider$2;
.super Landroid/content/BroadcastReceiver;
.source "GpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GpsLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/server/location/GpsLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 419
    iput-object p1, p0, Lcom/android/server/location/GpsLocationProvider$2;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 421
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 423
    .local v0, action:Ljava/lang/String;
    const-string v4, "com.android.internal.location.ALARM_WAKEUP"

    #@6
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_1f

    #@c
    .line 424
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_19

    #@12
    const-string v4, "GpsLocationProvider"

    #@14
    const-string v5, "ALARM_WAKEUP"

    #@16
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 425
    :cond_19
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider$2;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@1b
    invoke-static {v4}, Lcom/android/server/location/GpsLocationProvider;->access$200(Lcom/android/server/location/GpsLocationProvider;)V

    #@1e
    .line 457
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 426
    :cond_1f
    const-string v4, "com.android.internal.location.ALARM_TIMEOUT"

    #@21
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_3a

    #@27
    .line 427
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->access$100()Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_34

    #@2d
    const-string v4, "GpsLocationProvider"

    #@2f
    const-string v5, "ALARM_TIMEOUT"

    #@31
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 428
    :cond_34
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider$2;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@36
    invoke-static {v4}, Lcom/android/server/location/GpsLocationProvider;->access$300(Lcom/android/server/location/GpsLocationProvider;)V

    #@39
    goto :goto_1e

    #@3a
    .line 429
    :cond_3a
    const-string v4, "android.intent.action.DATA_SMS_RECEIVED"

    #@3c
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_48

    #@42
    .line 430
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider$2;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@44
    invoke-static {v4, p2}, Lcom/android/server/location/GpsLocationProvider;->access$400(Lcom/android/server/location/GpsLocationProvider;Landroid/content/Intent;)V

    #@47
    goto :goto_1e

    #@48
    .line 431
    :cond_48
    const-string v4, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    #@4a
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_56

    #@50
    .line 432
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider$2;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@52
    invoke-static {v4, p2}, Lcom/android/server/location/GpsLocationProvider;->access$500(Lcom/android/server/location/GpsLocationProvider;Landroid/content/Intent;)V

    #@55
    goto :goto_1e

    #@56
    .line 435
    :cond_56
    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@58
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v4

    #@5c
    if-eqz v4, :cond_1e

    #@5e
    .line 438
    const-string v4, "noConnectivity"

    #@60
    const/4 v5, 0x0

    #@61
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@64
    move-result v4

    #@65
    if-eqz v4, :cond_96

    #@67
    .line 439
    const/4 v3, 0x1

    #@68
    .line 445
    .local v3, networkState:I
    :goto_68
    const-string v4, "networkInfo"

    #@6a
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@6d
    move-result-object v2

    #@6e
    check-cast v2, Landroid/net/NetworkInfo;

    #@70
    .line 447
    .local v2, info:Landroid/net/NetworkInfo;
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider$2;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@72
    invoke-static {v4}, Lcom/android/server/location/GpsLocationProvider;->access$600(Lcom/android/server/location/GpsLocationProvider;)Landroid/content/Context;

    #@75
    move-result-object v4

    #@76
    const-string v5, "connectivity"

    #@78
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7b
    move-result-object v1

    #@7c
    check-cast v1, Landroid/net/ConnectivityManager;

    #@7e
    .line 449
    .local v1, connManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    #@81
    move-result v4

    #@82
    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@85
    move-result-object v2

    #@86
    .line 451
    if-eqz v2, :cond_98

    #@88
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    #@8b
    move-result v4

    #@8c
    const/16 v5, 0xb

    #@8e
    if-eq v4, v5, :cond_98

    #@90
    .line 452
    iget-object v4, p0, Lcom/android/server/location/GpsLocationProvider$2;->this$0:Lcom/android/server/location/GpsLocationProvider;

    #@92
    invoke-virtual {v4, v3, v2}, Lcom/android/server/location/GpsLocationProvider;->updateNetworkState(ILandroid/net/NetworkInfo;)V

    #@95
    goto :goto_1e

    #@96
    .line 441
    .end local v1           #connManager:Landroid/net/ConnectivityManager;
    .end local v2           #info:Landroid/net/NetworkInfo;
    .end local v3           #networkState:I
    :cond_96
    const/4 v3, 0x2

    #@97
    .restart local v3       #networkState:I
    goto :goto_68

    #@98
    .line 454
    .restart local v1       #connManager:Landroid/net/ConnectivityManager;
    .restart local v2       #info:Landroid/net/NetworkInfo;
    :cond_98
    const-string v4, "GpsLocationProvider"

    #@9a
    const-string v5, "ignore Mobile IMS intent"

    #@9c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    goto/16 :goto_1e
.end method
