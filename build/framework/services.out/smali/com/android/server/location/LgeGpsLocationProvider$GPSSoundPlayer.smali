.class Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;
.super Ljava/lang/Object;
.source "LgeGpsLocationProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/LgeGpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GPSSoundPlayer"
.end annotation


# static fields
.field private static final mGPSSound:Ljava/lang/String; = "/system/media/audio/ui/GPS_Alert.ogg"


# instance fields
.field private mAudioStreamType:I

.field private mExit:Z

.field private mPlayCount:I

.field private mPlayer:Landroid/media/MediaPlayer;

.field private mSoundId:I

.field private mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1639
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1640
    const/4 v0, 0x1

    #@4
    iput v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mAudioStreamType:I

    #@6
    .line 1641
    return-void
.end method


# virtual methods
.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 1684
    invoke-virtual {p0}, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->release()V

    #@3
    .line 1685
    return-void
.end method

.method public play()V
    .registers 2

    #@0
    .prologue
    .line 1644
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mThread:Ljava/lang/Thread;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 1645
    new-instance v0, Ljava/lang/Thread;

    #@6
    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@9
    iput-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mThread:Ljava/lang/Thread;

    #@b
    .line 1646
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mThread:Ljava/lang/Thread;

    #@d
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@10
    .line 1648
    :cond_10
    monitor-enter p0

    #@11
    .line 1650
    :try_start_11
    monitor-exit p0

    #@12
    .line 1651
    return-void

    #@13
    .line 1650
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_11 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1664
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mThread:Ljava/lang/Thread;

    #@3
    if-eqz v0, :cond_e

    #@5
    .line 1665
    monitor-enter p0

    #@6
    .line 1667
    :try_start_6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_6 .. :try_end_7} :catchall_1f

    #@7
    .line 1669
    :try_start_7
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mThread:Ljava/lang/Thread;

    #@9
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_c} :catch_22

    #@c
    .line 1673
    :goto_c
    iput-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mThread:Ljava/lang/Thread;

    #@e
    .line 1675
    :cond_e
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@10
    if-eqz v0, :cond_1e

    #@12
    .line 1676
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@14
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    #@17
    .line 1677
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@19
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@1c
    .line 1678
    iput-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@1e
    .line 1680
    :cond_1e
    return-void

    #@1f
    .line 1667
    :catchall_1f
    move-exception v0

    #@20
    :try_start_20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1f

    #@21
    throw v0

    #@22
    .line 1670
    :catch_22
    move-exception v0

    #@23
    goto :goto_c
.end method

.method public run()V
    .registers 6

    #@0
    .prologue
    .line 1613
    const-string v1, "/system/media/audio/ui/GPS_Alert.ogg"

    #@2
    .line 1614
    .local v1, soundFilePath:Ljava/lang/String;
    new-instance v2, Landroid/media/MediaPlayer;

    #@4
    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    #@7
    iput-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@9
    .line 1616
    :try_start_9
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@b
    iget v3, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mAudioStreamType:I

    #@d
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@10
    .line 1617
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@12
    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_15} :catch_28

    #@15
    .line 1625
    :try_start_15
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@17
    const/4 v3, 0x1

    #@18
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    #@1b
    .line 1626
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@1d
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_20} :catch_36

    #@20
    .line 1633
    monitor-enter p0

    #@21
    .line 1634
    :try_start_21
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@23
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    #@26
    .line 1635
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_21 .. :try_end_27} :catchall_57

    #@27
    .line 1637
    :goto_27
    return-void

    #@28
    .line 1618
    :catch_28
    move-exception v0

    #@29
    .line 1619
    .local v0, e:Ljava/io/IOException;
    const-string v2, "LgeGpsLocationProvider"

    #@2b
    const-string v3, "Please, check sound file path!!!"

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1620
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@32
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    #@35
    goto :goto_27

    #@36
    .line 1627
    .end local v0           #e:Ljava/io/IOException;
    :catch_36
    move-exception v0

    #@37
    .line 1628
    .restart local v0       #e:Ljava/io/IOException;
    const-string v2, "LgeGpsLocationProvider"

    #@39
    new-instance v3, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v4, "Error setting up sound "

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    iget v4, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mSoundId:I

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51
    .line 1629
    iget-object v2, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@53
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    #@56
    goto :goto_27

    #@57
    .line 1635
    .end local v0           #e:Ljava/io/IOException;
    :catchall_57
    move-exception v2

    #@58
    :try_start_58
    monitor-exit p0
    :try_end_59
    .catchall {:try_start_58 .. :try_end_59} :catchall_57

    #@59
    throw v2
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 1654
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mThread:Ljava/lang/Thread;

    #@2
    if-eqz v0, :cond_4

    #@4
    .line 1656
    :cond_4
    monitor-enter p0

    #@5
    .line 1657
    :try_start_5
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1658
    iget-object v0, p0, Lcom/android/server/location/LgeGpsLocationProvider$GPSSoundPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@b
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    #@e
    .line 1660
    :cond_e
    monitor-exit p0

    #@f
    .line 1661
    return-void

    #@10
    .line 1660
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_5 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method
