.class Lcom/android/server/location/LgeGpsLocationProvider$4;
.super Landroid/database/ContentObserver;
.source "LgeGpsLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/LgeGpsLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/LgeGpsLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/server/location/LgeGpsLocationProvider;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 1368
    iput-object p1, p0, Lcom/android/server/location/LgeGpsLocationProvider$4;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@2
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 6
    .parameter "selfChange"

    #@0
    .prologue
    .line 1372
    const-string v1, "LgeGpsLocationProvider"

    #@2
    const-string v2, "mGPSSettingObserver is called"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1373
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$4;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@9
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$300(Lcom/android/server/location/LgeGpsLocationProvider;)Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v1

    #@11
    const-string v2, "gps"

    #@13
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    .line 1376
    .local v0, gpsSetting:Z
    const-string v1, "LgeGpsLocationProvider"

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "mGPSSettingObserver. Gps = "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1380
    if-nez v0, :cond_48

    #@31
    .line 1381
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$4;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@33
    const/4 v2, 0x0

    #@34
    invoke-static {v1, v2}, Lcom/android/server/location/LgeGpsLocationProvider;->access$1302(Lcom/android/server/location/LgeGpsLocationProvider;Z)Z

    #@37
    .line 1382
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$4;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@39
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$800(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@3c
    .line 1383
    iget-object v1, p0, Lcom/android/server/location/LgeGpsLocationProvider$4;->this$0:Lcom/android/server/location/LgeGpsLocationProvider;

    #@3e
    invoke-static {v1}, Lcom/android/server/location/LgeGpsLocationProvider;->access$700(Lcom/android/server/location/LgeGpsLocationProvider;)V

    #@41
    .line 1384
    const-string v1, "LgeGpsLocationProvider"

    #@43
    const-string v2, "[test DCM] gpsSetting is changed so that gps notification is off. "

    #@45
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1386
    :cond_48
    return-void
.end method
