.class public Lcom/android/server/location/LocationProviderProxy;
.super Ljava/lang/Object;
.source "LocationProviderProxy.java"

# interfaces
.implements Lcom/android/server/location/LocationProviderInterface;


# static fields
.field private static final D:Z = true

.field private static final TAG:Ljava/lang/String; = "LocationProviderProxy"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private mLock:Ljava/lang/Object;

.field private final mName:Ljava/lang/String;

.field private mNewServiceWork:Ljava/lang/Runnable;

.field private mProperties:Lcom/android/internal/location/ProviderProperties;

.field private mRequest:Lcom/android/internal/location/ProviderRequest;

.field private final mServiceWatcher:Lcom/android/server/ServiceWatcher;

.field private mWorksource:Landroid/os/WorkSource;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;I)V
    .registers 15
    .parameter "context"
    .parameter "name"
    .parameter "action"
    .parameter
    .parameter "handler"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Handler;",
            "I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 69
    .local p4, initialPackageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mLock:Ljava/lang/Object;

    #@a
    .line 53
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/server/location/LocationProviderProxy;->mEnabled:Z

    #@d
    .line 54
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mRequest:Lcom/android/internal/location/ProviderRequest;

    #@10
    .line 55
    new-instance v0, Landroid/os/WorkSource;

    #@12
    invoke-direct {v0}, Landroid/os/WorkSource;-><init>()V

    #@15
    iput-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mWorksource:Landroid/os/WorkSource;

    #@17
    .line 93
    new-instance v0, Lcom/android/server/location/LocationProviderProxy$1;

    #@19
    invoke-direct {v0, p0}, Lcom/android/server/location/LocationProviderProxy$1;-><init>(Lcom/android/server/location/LocationProviderProxy;)V

    #@1c
    iput-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mNewServiceWork:Ljava/lang/Runnable;

    #@1e
    .line 70
    iput-object p1, p0, Lcom/android/server/location/LocationProviderProxy;->mContext:Landroid/content/Context;

    #@20
    .line 71
    iput-object p2, p0, Lcom/android/server/location/LocationProviderProxy;->mName:Ljava/lang/String;

    #@22
    .line 72
    new-instance v0, Lcom/android/server/ServiceWatcher;

    #@24
    iget-object v1, p0, Lcom/android/server/location/LocationProviderProxy;->mContext:Landroid/content/Context;

    #@26
    const-string v2, "LocationProviderProxy"

    #@28
    iget-object v5, p0, Lcom/android/server/location/LocationProviderProxy;->mNewServiceWork:Ljava/lang/Runnable;

    #@2a
    move-object v3, p3

    #@2b
    move-object v4, p4

    #@2c
    move-object v6, p5

    #@2d
    move v7, p6

    #@2e
    invoke-direct/range {v0 .. v7}, Lcom/android/server/ServiceWatcher;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Runnable;Landroid/os/Handler;I)V

    #@31
    iput-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@33
    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/location/LocationProviderProxy;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/location/LocationProviderProxy;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/android/server/location/LocationProviderProxy;->mEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/internal/location/ProviderRequest;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mRequest:Lcom/android/internal/location/ProviderRequest;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/location/LocationProviderProxy;)Landroid/os/WorkSource;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mWorksource:Landroid/os/WorkSource;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/internal/location/ILocationProvider;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/server/ServiceWatcher;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Lcom/android/server/location/LocationProviderProxy;Lcom/android/internal/location/ProviderProperties;)Lcom/android/internal/location/ProviderProperties;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/server/location/LocationProviderProxy;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    return-object p1
.end method

.method private bind()Z
    .registers 2

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/server/ServiceWatcher;->start()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static createAndBind(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;I)Lcom/android/server/location/LocationProviderProxy;
    .registers 13
    .parameter "context"
    .parameter "name"
    .parameter "action"
    .parameter
    .parameter "handler"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Handler;",
            "I)",
            "Lcom/android/server/location/LocationProviderProxy;"
        }
    .end annotation

    #@0
    .prologue
    .line 59
    .local p3, initialPackageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/android/server/location/LocationProviderProxy;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p1

    #@4
    move-object v3, p2

    #@5
    move-object v4, p3

    #@6
    move-object v5, p4

    #@7
    move v6, p5

    #@8
    invoke-direct/range {v0 .. v6}, Lcom/android/server/location/LocationProviderProxy;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;I)V

    #@b
    .line 61
    .local v0, proxy:Lcom/android/server/location/LocationProviderProxy;
    invoke-direct {v0}, Lcom/android/server/location/LocationProviderProxy;->bind()Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 64
    .end local v0           #proxy:Lcom/android/server/location/LocationProviderProxy;
    :goto_11
    return-object v0

    #@12
    .restart local v0       #proxy:Lcom/android/server/location/LocationProviderProxy;
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private getService()Lcom/android/internal/location/ILocationProvider;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/server/ServiceWatcher;->getBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/internal/location/ILocationProvider$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/location/ILocationProvider;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method


# virtual methods
.method public disable()V
    .registers 6

    #@0
    .prologue
    .line 172
    iget-object v3, p0, Lcom/android/server/location/LocationProviderProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 173
    const/4 v2, 0x0

    #@4
    :try_start_4
    iput-boolean v2, p0, Lcom/android/server/location/LocationProviderProxy;->mEnabled:Z

    #@6
    .line 174
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_e

    #@7
    .line 175
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@a
    move-result-object v1

    #@b
    .line 176
    .local v1, service:Lcom/android/internal/location/ILocationProvider;
    if-nez v1, :cond_11

    #@d
    .line 186
    :goto_d
    return-void

    #@e
    .line 174
    .end local v1           #service:Lcom/android/internal/location/ILocationProvider;
    :catchall_e
    move-exception v2

    #@f
    :try_start_f
    monitor-exit v3
    :try_end_10
    .catchall {:try_start_f .. :try_end_10} :catchall_e

    #@10
    throw v2

    #@11
    .line 179
    .restart local v1       #service:Lcom/android/internal/location/ILocationProvider;
    :cond_11
    :try_start_11
    invoke-interface {v1}, Lcom/android/internal/location/ILocationProvider;->disable()V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_14} :catch_15
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_14} :catch_1c

    #@14
    goto :goto_d

    #@15
    .line 180
    :catch_15
    move-exception v0

    #@16
    .line 181
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "LocationProviderProxy"

    #@18
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_d

    #@1c
    .line 182
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_1c
    move-exception v0

    #@1d
    .line 184
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "LocationProviderProxy"

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "Exception from "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    iget-object v4, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2c
    invoke-virtual {v4}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3b
    goto :goto_d
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 221
    const-string v2, "REMOTE SERVICE"

    #@2
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@5
    .line 222
    const-string v2, " name="

    #@7
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@a
    move-result-object v2

    #@b
    iget-object v3, p0, Lcom/android/server/location/LocationProviderProxy;->mName:Ljava/lang/String;

    #@d
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@10
    .line 223
    const-string v2, " pkg="

    #@12
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@18
    invoke-virtual {v3}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@1f
    .line 224
    const-string v2, " version="

    #@21
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@24
    move-result-object v2

    #@25
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v4, ""

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    iget-object v4, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@32
    invoke-virtual {v4}, Lcom/android/server/ServiceWatcher;->getBestVersion()I

    #@35
    move-result v4

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@41
    .line 225
    const/16 v2, 0xa

    #@43
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    #@46
    .line 227
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@49
    move-result-object v1

    #@4a
    .line 228
    .local v1, service:Lcom/android/internal/location/ILocationProvider;
    if-nez v1, :cond_52

    #@4c
    .line 229
    const-string v2, "service down (null)"

    #@4e
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@51
    .line 244
    :goto_51
    return-void

    #@52
    .line 232
    :cond_52
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@55
    .line 235
    :try_start_55
    invoke-interface {v1}, Lcom/android/internal/location/ILocationProvider;->asBinder()Landroid/os/IBinder;

    #@58
    move-result-object v2

    #@59
    invoke-interface {v2, p1, p3}, Landroid/os/IBinder;->dump(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    :try_end_5c
    .catch Landroid/os/RemoteException; {:try_start_55 .. :try_end_5c} :catch_5d
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_5c} :catch_69

    #@5c
    goto :goto_51

    #@5d
    .line 236
    :catch_5d
    move-exception v0

    #@5e
    .line 237
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "service down (RemoteException)"

    #@60
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@63
    .line 238
    const-string v2, "LocationProviderProxy"

    #@65
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@68
    goto :goto_51

    #@69
    .line 239
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_69
    move-exception v0

    #@6a
    .line 240
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "service down (Exception)"

    #@6c
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6f
    .line 242
    const-string v2, "LocationProviderProxy"

    #@71
    new-instance v3, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v4, "Exception from "

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    iget-object v4, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@7e
    invoke-virtual {v4}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v3

    #@8a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8d
    goto :goto_51
.end method

.method public enable()V
    .registers 6

    #@0
    .prologue
    .line 154
    iget-object v3, p0, Lcom/android/server/location/LocationProviderProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 155
    const/4 v2, 0x1

    #@4
    :try_start_4
    iput-boolean v2, p0, Lcom/android/server/location/LocationProviderProxy;->mEnabled:Z

    #@6
    .line 156
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_e

    #@7
    .line 157
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@a
    move-result-object v1

    #@b
    .line 158
    .local v1, service:Lcom/android/internal/location/ILocationProvider;
    if-nez v1, :cond_11

    #@d
    .line 168
    :goto_d
    return-void

    #@e
    .line 156
    .end local v1           #service:Lcom/android/internal/location/ILocationProvider;
    :catchall_e
    move-exception v2

    #@f
    :try_start_f
    monitor-exit v3
    :try_end_10
    .catchall {:try_start_f .. :try_end_10} :catchall_e

    #@10
    throw v2

    #@11
    .line 161
    .restart local v1       #service:Lcom/android/internal/location/ILocationProvider;
    :cond_11
    :try_start_11
    invoke-interface {v1}, Lcom/android/internal/location/ILocationProvider;->enable()V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_14} :catch_15
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_14} :catch_1c

    #@14
    goto :goto_d

    #@15
    .line 162
    :catch_15
    move-exception v0

    #@16
    .line 163
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "LocationProviderProxy"

    #@18
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_d

    #@1c
    .line 164
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_1c
    move-exception v0

    #@1d
    .line 166
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "LocationProviderProxy"

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "Exception from "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    iget-object v4, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2c
    invoke-virtual {v4}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3b
    goto :goto_d
.end method

.method public getConnectedPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    invoke-virtual {v0}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getProperties()Lcom/android/internal/location/ProviderProperties;
    .registers 3

    #@0
    .prologue
    .line 147
    iget-object v1, p0, Lcom/android/server/location/LocationProviderProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 148
    :try_start_3
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 149
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getStatus(Landroid/os/Bundle;)I
    .registers 8
    .parameter "extras"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 248
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@4
    move-result-object v1

    #@5
    .line 249
    .local v1, service:Lcom/android/internal/location/ILocationProvider;
    if-nez v1, :cond_8

    #@7
    .line 259
    :goto_7
    return v2

    #@8
    .line 252
    :cond_8
    :try_start_8
    invoke-interface {v1, p1}, Lcom/android/internal/location/ILocationProvider;->getStatus(Landroid/os/Bundle;)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_b} :catch_d
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_b} :catch_14

    #@b
    move-result v2

    #@c
    goto :goto_7

    #@d
    .line 253
    :catch_d
    move-exception v0

    #@e
    .line 254
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "LocationProviderProxy"

    #@10
    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    goto :goto_7

    #@14
    .line 255
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_14
    move-exception v0

    #@15
    .line 257
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LocationProviderProxy"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "Exception from "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    iget-object v5, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@24
    invoke-virtual {v5}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_7
.end method

.method public getStatusUpdateTime()J
    .registers 8

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 264
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@5
    move-result-object v1

    #@6
    .line 265
    .local v1, service:Lcom/android/internal/location/ILocationProvider;
    if-nez v1, :cond_9

    #@8
    .line 275
    :goto_8
    return-wide v2

    #@9
    .line 268
    :cond_9
    :try_start_9
    invoke-interface {v1}, Lcom/android/internal/location/ILocationProvider;->getStatusUpdateTime()J
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_c} :catch_e
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_c} :catch_15

    #@c
    move-result-wide v2

    #@d
    goto :goto_8

    #@e
    .line 269
    :catch_e
    move-exception v0

    #@f
    .line 270
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "LocationProviderProxy"

    #@11
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_8

    #@15
    .line 271
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_15
    move-exception v0

    #@16
    .line 273
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "LocationProviderProxy"

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v6, "Exception from "

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    iget-object v6, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@25
    invoke-virtual {v6}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    goto :goto_8
.end method

.method public isEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 190
    iget-object v1, p0, Lcom/android/server/location/LocationProviderProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 191
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/location/LocationProviderProxy;->mEnabled:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 192
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 9
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 280
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@4
    move-result-object v1

    #@5
    .line 281
    .local v1, service:Lcom/android/internal/location/ILocationProvider;
    if-nez v1, :cond_8

    #@7
    .line 291
    :goto_7
    return v2

    #@8
    .line 284
    :cond_8
    :try_start_8
    invoke-interface {v1, p1, p2}, Lcom/android/internal/location/ILocationProvider;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_b} :catch_d
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_b} :catch_14

    #@b
    move-result v2

    #@c
    goto :goto_7

    #@d
    .line 285
    :catch_d
    move-exception v0

    #@e
    .line 286
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "LocationProviderProxy"

    #@10
    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    goto :goto_7

    #@14
    .line 287
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_14
    move-exception v0

    #@15
    .line 289
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LocationProviderProxy"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "Exception from "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    iget-object v5, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@24
    invoke-virtual {v5}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_7
.end method

.method public setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 8
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    .line 197
    iget-object v3, p0, Lcom/android/server/location/LocationProviderProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 198
    :try_start_3
    iput-object p1, p0, Lcom/android/server/location/LocationProviderProxy;->mRequest:Lcom/android/internal/location/ProviderRequest;

    #@5
    .line 199
    iput-object p2, p0, Lcom/android/server/location/LocationProviderProxy;->mWorksource:Landroid/os/WorkSource;

    #@7
    .line 200
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_f

    #@8
    .line 201
    invoke-direct {p0}, Lcom/android/server/location/LocationProviderProxy;->getService()Lcom/android/internal/location/ILocationProvider;

    #@b
    move-result-object v1

    #@c
    .line 202
    .local v1, service:Lcom/android/internal/location/ILocationProvider;
    if-nez v1, :cond_12

    #@e
    .line 212
    :goto_e
    return-void

    #@f
    .line 200
    .end local v1           #service:Lcom/android/internal/location/ILocationProvider;
    :catchall_f
    move-exception v2

    #@10
    :try_start_10
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    #@11
    throw v2

    #@12
    .line 205
    .restart local v1       #service:Lcom/android/internal/location/ILocationProvider;
    :cond_12
    :try_start_12
    invoke-interface {v1, p1, p2}, Lcom/android/internal/location/ILocationProvider;->setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_15} :catch_16
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_15} :catch_1d

    #@15
    goto :goto_e

    #@16
    .line 206
    :catch_16
    move-exception v0

    #@17
    .line 207
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "LocationProviderProxy"

    #@19
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_e

    #@1d
    .line 208
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_1d
    move-exception v0

    #@1e
    .line 210
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "LocationProviderProxy"

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "Exception from "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    iget-object v4, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2d
    invoke-virtual {v4}, Lcom/android/server/ServiceWatcher;->getBestPackageName()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3c
    goto :goto_e
.end method

.method public switchUser(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/server/location/LocationProviderProxy;->mServiceWatcher:Lcom/android/server/ServiceWatcher;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/ServiceWatcher;->switchUser(I)V

    #@5
    .line 217
    return-void
.end method
