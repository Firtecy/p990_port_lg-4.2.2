.class public Lcom/android/server/Watchdog;
.super Ljava/lang/Thread;
.source "Watchdog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/Watchdog$Monitor;,
        Lcom/android/server/Watchdog$RebootRequestReceiver;,
        Lcom/android/server/Watchdog$RebootReceiver;,
        Lcom/android/server/Watchdog$HeartbeatHandler;
    }
.end annotation


# static fields
.field static final DB:Z = false

.field static final MEMCHECK_DEFAULT_MIN_ALARM:I = 0xb4

.field static final MEMCHECK_DEFAULT_MIN_SCREEN_OFF:I = 0x12c

.field static final MEMCHECK_DEFAULT_RECHECK_INTERVAL:I = 0x12c

.field static final MONITOR:I = 0xa9e

.field static final NATIVE_STACKS_OF_INTEREST:[Ljava/lang/String; = null

.field static final REBOOT_ACTION:Ljava/lang/String; = "com.android.service.Watchdog.REBOOT"

.field static final REBOOT_DEFAULT_INTERVAL:I = 0x0

.field static final REBOOT_DEFAULT_START_TIME:I = 0x2a30

.field static final REBOOT_DEFAULT_WINDOW:I = 0xe10

.field static final RECORD_KERNEL_THREADS:Z = true

.field static final TAG:Ljava/lang/String; = "Watchdog"

.field static final TIME_TO_RESTART:I = 0xea60

.field static final TIME_TO_WAIT:I = 0x7530

.field static final localLOGV:Z

.field static sWatchdog:Lcom/android/server/Watchdog;


# instance fields
.field mActivity:Lcom/android/server/am/ActivityManagerService;

.field mActivityControllerPid:I

.field mAlarm:Lcom/android/server/AlarmManagerService;

.field mBattery:Lcom/android/server/BatteryService;

.field mBootTime:J

.field final mCalendar:Ljava/util/Calendar;

.field mCheckupIntent:Landroid/app/PendingIntent;

.field mCompleted:Z

.field mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

.field mForceKillSystem:Z

.field final mHandler:Landroid/os/Handler;

.field mMinAlarm:I

.field mMinScreenOff:I

.field final mMonitors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/Watchdog$Monitor;",
            ">;"
        }
    .end annotation
.end field

.field mNeedScheduledCheck:Z

.field mPhonePid:I

.field mPower:Lcom/android/server/power/PowerManagerService;

.field mRebootIntent:Landroid/app/PendingIntent;

.field mRebootInterval:I

.field mReqMinNextAlarm:I

.field mReqMinScreenOff:I

.field mReqRebootInterval:I

.field mReqRebootNoWait:Z

.field mReqRebootStartTime:I

.field mReqRebootWindow:I

.field mReqRecheckInterval:I

.field mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 73
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "/system/bin/mediaserver"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "/system/bin/sdcard"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "/system/bin/surfaceflinger"

    #@10
    aput-object v2, v0, v1

    #@12
    sput-object v0, Lcom/android/server/Watchdog;->NATIVE_STACKS_OF_INTEREST:[Ljava/lang/String;

    #@14
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 186
    const-string v0, "watchdog"

    #@3
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@6
    .line 83
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/Watchdog;->mMonitors:Ljava/util/ArrayList;

    #@d
    .line 96
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/server/Watchdog;->mCalendar:Ljava/util/Calendar;

    #@13
    .line 97
    const/16 v0, 0x12c

    #@15
    iput v0, p0, Lcom/android/server/Watchdog;->mMinScreenOff:I

    #@17
    .line 98
    const/16 v0, 0xb4

    #@19
    iput v0, p0, Lcom/android/server/Watchdog;->mMinAlarm:I

    #@1b
    .line 107
    iput v1, p0, Lcom/android/server/Watchdog;->mReqRebootInterval:I

    #@1d
    .line 108
    iput v1, p0, Lcom/android/server/Watchdog;->mReqRebootStartTime:I

    #@1f
    .line 109
    iput v1, p0, Lcom/android/server/Watchdog;->mReqRebootWindow:I

    #@21
    .line 110
    iput v1, p0, Lcom/android/server/Watchdog;->mReqMinScreenOff:I

    #@23
    .line 111
    iput v1, p0, Lcom/android/server/Watchdog;->mReqMinNextAlarm:I

    #@25
    .line 112
    iput v1, p0, Lcom/android/server/Watchdog;->mReqRecheckInterval:I

    #@27
    .line 187
    new-instance v0, Lcom/android/server/Watchdog$HeartbeatHandler;

    #@29
    invoke-direct {v0, p0}, Lcom/android/server/Watchdog$HeartbeatHandler;-><init>(Lcom/android/server/Watchdog;)V

    #@2c
    iput-object v0, p0, Lcom/android/server/Watchdog;->mHandler:Landroid/os/Handler;

    #@2e
    .line 188
    return-void
.end method

.method static computeCalendarTime(Ljava/util/Calendar;JJ)J
    .registers 11
    .parameter "c"
    .parameter "curTime"
    .parameter "secondsSinceMidnight"

    #@0
    .prologue
    .line 360
    invoke-virtual {p0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@3
    .line 362
    long-to-int v3, p3

    #@4
    div-int/lit16 v2, v3, 0xe10

    #@6
    .line 363
    .local v2, val:I
    const/16 v3, 0xb

    #@8
    invoke-virtual {p0, v3, v2}, Ljava/util/Calendar;->set(II)V

    #@b
    .line 364
    mul-int/lit16 v3, v2, 0xe10

    #@d
    int-to-long v3, v3

    #@e
    sub-long/2addr p3, v3

    #@f
    .line 365
    long-to-int v3, p3

    #@10
    div-int/lit8 v2, v3, 0x3c

    #@12
    .line 366
    const/16 v3, 0xc

    #@14
    invoke-virtual {p0, v3, v2}, Ljava/util/Calendar;->set(II)V

    #@17
    .line 367
    const/16 v3, 0xd

    #@19
    long-to-int v4, p3

    #@1a
    mul-int/lit8 v5, v2, 0x3c

    #@1c
    sub-int/2addr v4, v5

    #@1d
    invoke-virtual {p0, v3, v4}, Ljava/util/Calendar;->set(II)V

    #@20
    .line 368
    const/16 v3, 0xe

    #@22
    const/4 v4, 0x0

    #@23
    invoke-virtual {p0, v3, v4}, Ljava/util/Calendar;->set(II)V

    #@26
    .line 370
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@29
    move-result-wide v0

    #@2a
    .line 371
    .local v0, newTime:J
    cmp-long v3, v0, p1

    #@2c
    if-gez v3, :cond_37

    #@2e
    .line 374
    const/4 v3, 0x5

    #@2f
    const/4 v4, 0x1

    #@30
    invoke-virtual {p0, v3, v4}, Ljava/util/Calendar;->add(II)V

    #@33
    .line 375
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@36
    move-result-wide v0

    #@37
    .line 378
    :cond_37
    return-wide v0
.end method

.method private dumpKernelStackTraces()Ljava/io/File;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 512
    const-string v2, "dalvik.vm.stack-trace-file"

    #@3
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 513
    .local v0, tracesPath:Ljava/lang/String;
    if-eqz v0, :cond_f

    #@9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_10

    #@f
    .line 518
    :cond_f
    :goto_f
    return-object v1

    #@10
    .line 517
    :cond_10
    invoke-direct {p0, v0}, Lcom/android/server/Watchdog;->native_dumpKernelStacks(Ljava/lang/String;)V

    #@13
    .line 518
    new-instance v1, Ljava/io/File;

    #@15
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@18
    goto :goto_f
.end method

.method public static getInstance()Lcom/android/server/Watchdog;
    .registers 1

    #@0
    .prologue
    .line 178
    sget-object v0, Lcom/android/server/Watchdog;->sWatchdog:Lcom/android/server/Watchdog;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 179
    new-instance v0, Lcom/android/server/Watchdog;

    #@6
    invoke-direct {v0}, Lcom/android/server/Watchdog;-><init>()V

    #@9
    sput-object v0, Lcom/android/server/Watchdog;->sWatchdog:Lcom/android/server/Watchdog;

    #@b
    .line 182
    :cond_b
    sget-object v0, Lcom/android/server/Watchdog;->sWatchdog:Lcom/android/server/Watchdog;

    #@d
    return-object v0
.end method

.method private native native_dumpKernelStacks(Ljava/lang/String;)V
.end method


# virtual methods
.method public addMonitor(Lcom/android/server/Watchdog$Monitor;)V
    .registers 4
    .parameter "monitor"

    #@0
    .prologue
    .line 223
    monitor-enter p0

    #@1
    .line 224
    :try_start_1
    invoke-virtual {p0}, Lcom/android/server/Watchdog;->isAlive()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 225
    new-instance v0, Ljava/lang/RuntimeException;

    #@9
    const-string v1, "Monitors can\'t be added while the Watchdog is running"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 228
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 227
    :cond_12
    :try_start_12
    iget-object v0, p0, Lcom/android/server/Watchdog;->mMonitors:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@17
    .line 228
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_12 .. :try_end_18} :catchall_f

    #@18
    .line 229
    return-void
.end method

.method checkReboot(Z)V
    .registers 23
    .parameter "fromAlarm"

    #@0
    .prologue
    .line 232
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRebootInterval:I

    #@4
    move/from16 v17, v0

    #@6
    if-ltz v17, :cond_22

    #@8
    move-object/from16 v0, p0

    #@a
    iget v8, v0, Lcom/android/server/Watchdog;->mReqRebootInterval:I

    #@c
    .line 234
    .local v8, rebootInterval:I
    :goto_c
    move-object/from16 v0, p0

    #@e
    iput v8, v0, Lcom/android/server/Watchdog;->mRebootInterval:I

    #@10
    .line 235
    if-gtz v8, :cond_24

    #@12
    .line 238
    move-object/from16 v0, p0

    #@14
    iget-object v0, v0, Lcom/android/server/Watchdog;->mAlarm:Lcom/android/server/AlarmManagerService;

    #@16
    move-object/from16 v17, v0

    #@18
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/android/server/Watchdog;->mRebootIntent:Landroid/app/PendingIntent;

    #@1c
    move-object/from16 v18, v0

    #@1e
    invoke-virtual/range {v17 .. v18}, Lcom/android/server/AlarmManagerService;->remove(Landroid/app/PendingIntent;)V

    #@21
    .line 306
    :goto_21
    return-void

    #@22
    .line 232
    .end local v8           #rebootInterval:I
    :cond_22
    const/4 v8, 0x0

    #@23
    goto :goto_c

    #@24
    .line 242
    .restart local v8       #rebootInterval:I
    :cond_24
    move-object/from16 v0, p0

    #@26
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRebootStartTime:I

    #@28
    move/from16 v17, v0

    #@2a
    if-ltz v17, :cond_f5

    #@2c
    move-object/from16 v0, p0

    #@2e
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRebootStartTime:I

    #@30
    move/from16 v17, v0

    #@32
    move/from16 v0, v17

    #@34
    int-to-long v11, v0

    #@35
    .line 244
    .local v11, rebootStartTime:J
    :goto_35
    move-object/from16 v0, p0

    #@37
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRebootWindow:I

    #@39
    move/from16 v17, v0

    #@3b
    if-ltz v17, :cond_f9

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRebootWindow:I

    #@41
    move/from16 v17, v0

    #@43
    :goto_43
    move/from16 v0, v17

    #@45
    mul-int/lit16 v0, v0, 0x3e8

    #@47
    move/from16 v17, v0

    #@49
    move/from16 v0, v17

    #@4b
    int-to-long v13, v0

    #@4c
    .line 246
    .local v13, rebootWindowMillis:J
    move-object/from16 v0, p0

    #@4e
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRecheckInterval:I

    #@50
    move/from16 v17, v0

    #@52
    if-ltz v17, :cond_fd

    #@54
    move-object/from16 v0, p0

    #@56
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRecheckInterval:I

    #@58
    move/from16 v17, v0

    #@5a
    :goto_5a
    move/from16 v0, v17

    #@5c
    mul-int/lit16 v0, v0, 0x3e8

    #@5e
    move/from16 v17, v0

    #@60
    move/from16 v0, v17

    #@62
    int-to-long v15, v0

    #@63
    .line 249
    .local v15, recheckInterval:J
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/Watchdog;->retrieveBrutalityAmount()V

    #@66
    .line 254
    monitor-enter p0

    #@67
    .line 255
    :try_start_67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6a
    move-result-wide v4

    #@6b
    .line 256
    .local v4, now:J
    move-object/from16 v0, p0

    #@6d
    iget-object v0, v0, Lcom/android/server/Watchdog;->mCalendar:Ljava/util/Calendar;

    #@6f
    move-object/from16 v17, v0

    #@71
    move-object/from16 v0, v17

    #@73
    invoke-static {v0, v4, v5, v11, v12}, Lcom/android/server/Watchdog;->computeCalendarTime(Ljava/util/Calendar;JJ)J

    #@76
    move-result-wide v6

    #@77
    .line 259
    .local v6, realStartTime:J
    mul-int/lit8 v17, v8, 0x18

    #@79
    mul-int/lit8 v17, v17, 0x3c

    #@7b
    mul-int/lit8 v17, v17, 0x3c

    #@7d
    move/from16 v0, v17

    #@7f
    mul-int/lit16 v0, v0, 0x3e8

    #@81
    move/from16 v17, v0

    #@83
    move/from16 v0, v17

    #@85
    int-to-long v9, v0

    #@86
    .line 260
    .local v9, rebootIntervalMillis:J
    move-object/from16 v0, p0

    #@88
    iget-boolean v0, v0, Lcom/android/server/Watchdog;->mReqRebootNoWait:Z

    #@8a
    move/from16 v17, v0

    #@8c
    if-nez v17, :cond_9c

    #@8e
    move-object/from16 v0, p0

    #@90
    iget-wide v0, v0, Lcom/android/server/Watchdog;->mBootTime:J

    #@92
    move-wide/from16 v17, v0

    #@94
    sub-long v17, v4, v17

    #@96
    sub-long v19, v9, v13

    #@98
    cmp-long v17, v17, v19

    #@9a
    if-ltz v17, :cond_111

    #@9c
    .line 262
    :cond_9c
    if-eqz p1, :cond_101

    #@9e
    const-wide/16 v17, 0x0

    #@a0
    cmp-long v17, v13, v17

    #@a2
    if-gtz v17, :cond_101

    #@a4
    .line 264
    const/16 v17, 0xaf8

    #@a6
    const/16 v18, 0x5

    #@a8
    move/from16 v0, v18

    #@aa
    new-array v0, v0, [Ljava/lang/Object;

    #@ac
    move-object/from16 v18, v0

    #@ae
    const/16 v19, 0x0

    #@b0
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b3
    move-result-object v20

    #@b4
    aput-object v20, v18, v19

    #@b6
    const/16 v19, 0x1

    #@b8
    long-to-int v0, v9

    #@b9
    move/from16 v20, v0

    #@bb
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@be
    move-result-object v20

    #@bf
    aput-object v20, v18, v19

    #@c1
    const/16 v19, 0x2

    #@c3
    long-to-int v0, v11

    #@c4
    move/from16 v20, v0

    #@c6
    move/from16 v0, v20

    #@c8
    mul-int/lit16 v0, v0, 0x3e8

    #@ca
    move/from16 v20, v0

    #@cc
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cf
    move-result-object v20

    #@d0
    aput-object v20, v18, v19

    #@d2
    const/16 v19, 0x3

    #@d4
    long-to-int v0, v13

    #@d5
    move/from16 v20, v0

    #@d7
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@da
    move-result-object v20

    #@db
    aput-object v20, v18, v19

    #@dd
    const/16 v19, 0x4

    #@df
    const-string v20, ""

    #@e1
    aput-object v20, v18, v19

    #@e3
    invoke-static/range {v17 .. v18}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@e6
    .line 267
    const-string v17, "Checkin scheduled forced"

    #@e8
    move-object/from16 v0, p0

    #@ea
    move-object/from16 v1, v17

    #@ec
    invoke-virtual {v0, v1}, Lcom/android/server/Watchdog;->rebootSystem(Ljava/lang/String;)V

    #@ef
    .line 268
    monitor-exit p0

    #@f0
    goto/16 :goto_21

    #@f2
    .line 300
    .end local v4           #now:J
    .end local v6           #realStartTime:J
    .end local v9           #rebootIntervalMillis:J
    :catchall_f2
    move-exception v17

    #@f3
    monitor-exit p0
    :try_end_f4
    .catchall {:try_start_67 .. :try_end_f4} :catchall_f2

    #@f4
    throw v17

    #@f5
    .line 242
    .end local v11           #rebootStartTime:J
    .end local v13           #rebootWindowMillis:J
    .end local v15           #recheckInterval:J
    :cond_f5
    const-wide/16 v11, 0x2a30

    #@f7
    goto/16 :goto_35

    #@f9
    .line 244
    .restart local v11       #rebootStartTime:J
    :cond_f9
    const/16 v17, 0xe10

    #@fb
    goto/16 :goto_43

    #@fd
    .line 246
    .restart local v13       #rebootWindowMillis:J
    :cond_fd
    const/16 v17, 0x12c

    #@ff
    goto/16 :goto_5a

    #@101
    .line 272
    .restart local v4       #now:J
    .restart local v6       #realStartTime:J
    .restart local v9       #rebootIntervalMillis:J
    .restart local v15       #recheckInterval:J
    :cond_101
    cmp-long v17, v4, v6

    #@103
    if-gez v17, :cond_13a

    #@105
    .line 274
    :try_start_105
    move-object/from16 v0, p0

    #@107
    iget-object v0, v0, Lcom/android/server/Watchdog;->mCalendar:Ljava/util/Calendar;

    #@109
    move-object/from16 v17, v0

    #@10b
    move-object/from16 v0, v17

    #@10d
    invoke-static {v0, v4, v5, v11, v12}, Lcom/android/server/Watchdog;->computeCalendarTime(Ljava/util/Calendar;JJ)J

    #@110
    move-result-wide v6

    #@111
    .line 300
    :cond_111
    :goto_111
    monitor-exit p0
    :try_end_112
    .catchall {:try_start_105 .. :try_end_112} :catchall_f2

    #@112
    .line 304
    move-object/from16 v0, p0

    #@114
    iget-object v0, v0, Lcom/android/server/Watchdog;->mAlarm:Lcom/android/server/AlarmManagerService;

    #@116
    move-object/from16 v17, v0

    #@118
    move-object/from16 v0, p0

    #@11a
    iget-object v0, v0, Lcom/android/server/Watchdog;->mRebootIntent:Landroid/app/PendingIntent;

    #@11c
    move-object/from16 v18, v0

    #@11e
    invoke-virtual/range {v17 .. v18}, Lcom/android/server/AlarmManagerService;->remove(Landroid/app/PendingIntent;)V

    #@121
    .line 305
    move-object/from16 v0, p0

    #@123
    iget-object v0, v0, Lcom/android/server/Watchdog;->mAlarm:Lcom/android/server/AlarmManagerService;

    #@125
    move-object/from16 v17, v0

    #@127
    const/16 v18, 0x0

    #@129
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Lcom/android/server/Watchdog;->mRebootIntent:Landroid/app/PendingIntent;

    #@12d
    move-object/from16 v19, v0

    #@12f
    move-object/from16 v0, v17

    #@131
    move/from16 v1, v18

    #@133
    move-object/from16 v2, v19

    #@135
    invoke-virtual {v0, v1, v6, v7, v2}, Lcom/android/server/AlarmManagerService;->set(IJLandroid/app/PendingIntent;)V

    #@138
    goto/16 :goto_21

    #@13a
    .line 276
    :cond_13a
    add-long v17, v6, v13

    #@13c
    cmp-long v17, v4, v17

    #@13e
    if-gez v17, :cond_1b6

    #@140
    .line 277
    :try_start_140
    move-object/from16 v0, p0

    #@142
    invoke-virtual {v0, v4, v5}, Lcom/android/server/Watchdog;->shouldWeBeBrutalLocked(J)Ljava/lang/String;

    #@145
    move-result-object v3

    #@146
    .line 278
    .local v3, doit:Ljava/lang/String;
    const/16 v18, 0xaf8

    #@148
    const/16 v17, 0x5

    #@14a
    move/from16 v0, v17

    #@14c
    new-array v0, v0, [Ljava/lang/Object;

    #@14e
    move-object/from16 v19, v0

    #@150
    const/16 v17, 0x0

    #@152
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@155
    move-result-object v20

    #@156
    aput-object v20, v19, v17

    #@158
    const/16 v17, 0x1

    #@15a
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15d
    move-result-object v20

    #@15e
    aput-object v20, v19, v17

    #@160
    const/16 v17, 0x2

    #@162
    long-to-int v0, v11

    #@163
    move/from16 v20, v0

    #@165
    move/from16 v0, v20

    #@167
    mul-int/lit16 v0, v0, 0x3e8

    #@169
    move/from16 v20, v0

    #@16b
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16e
    move-result-object v20

    #@16f
    aput-object v20, v19, v17

    #@171
    const/16 v17, 0x3

    #@173
    long-to-int v0, v13

    #@174
    move/from16 v20, v0

    #@176
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@179
    move-result-object v20

    #@17a
    aput-object v20, v19, v17

    #@17c
    const/16 v20, 0x4

    #@17e
    if-eqz v3, :cond_195

    #@180
    move-object/from16 v17, v3

    #@182
    :goto_182
    aput-object v17, v19, v20

    #@184
    invoke-static/range {v18 .. v19}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@187
    .line 281
    if-nez v3, :cond_198

    #@189
    .line 282
    const-string v17, "Checked scheduled range"

    #@18b
    move-object/from16 v0, p0

    #@18d
    move-object/from16 v1, v17

    #@18f
    invoke-virtual {v0, v1}, Lcom/android/server/Watchdog;->rebootSystem(Ljava/lang/String;)V

    #@192
    .line 283
    monitor-exit p0

    #@193
    goto/16 :goto_21

    #@195
    .line 278
    :cond_195
    const-string v17, ""

    #@197
    goto :goto_182

    #@198
    .line 288
    :cond_198
    add-long v17, v4, v15

    #@19a
    add-long v19, v6, v13

    #@19c
    cmp-long v17, v17, v19

    #@19e
    if-ltz v17, :cond_1b2

    #@1a0
    .line 289
    move-object/from16 v0, p0

    #@1a2
    iget-object v0, v0, Lcom/android/server/Watchdog;->mCalendar:Ljava/util/Calendar;

    #@1a4
    move-object/from16 v17, v0

    #@1a6
    add-long v18, v4, v9

    #@1a8
    move-object/from16 v0, v17

    #@1aa
    move-wide/from16 v1, v18

    #@1ac
    invoke-static {v0, v1, v2, v11, v12}, Lcom/android/server/Watchdog;->computeCalendarTime(Ljava/util/Calendar;JJ)J

    #@1af
    move-result-wide v6

    #@1b0
    goto/16 :goto_111

    #@1b2
    .line 292
    :cond_1b2
    add-long v6, v4, v15

    #@1b4
    goto/16 :goto_111

    #@1b6
    .line 296
    .end local v3           #doit:Ljava/lang/String;
    :cond_1b6
    move-object/from16 v0, p0

    #@1b8
    iget-object v0, v0, Lcom/android/server/Watchdog;->mCalendar:Ljava/util/Calendar;

    #@1ba
    move-object/from16 v17, v0

    #@1bc
    add-long v18, v4, v9

    #@1be
    move-object/from16 v0, v17

    #@1c0
    move-wide/from16 v1, v18

    #@1c2
    invoke-static {v0, v1, v2, v11, v12}, Lcom/android/server/Watchdog;->computeCalendarTime(Ljava/util/Calendar;JJ)J
    :try_end_1c5
    .catchall {:try_start_140 .. :try_end_1c5} :catchall_f2

    #@1c5
    move-result-wide v6

    #@1c6
    goto/16 :goto_111
.end method

.method public init(Landroid/content/Context;Lcom/android/server/BatteryService;Lcom/android/server/power/PowerManagerService;Lcom/android/server/AlarmManagerService;Lcom/android/server/am/ActivityManagerService;)V
    .registers 10
    .parameter "context"
    .parameter "battery"
    .parameter "power"
    .parameter "alarm"
    .parameter "activity"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 193
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    iput-object v0, p0, Lcom/android/server/Watchdog;->mResolver:Landroid/content/ContentResolver;

    #@7
    .line 194
    iput-object p2, p0, Lcom/android/server/Watchdog;->mBattery:Lcom/android/server/BatteryService;

    #@9
    .line 195
    iput-object p3, p0, Lcom/android/server/Watchdog;->mPower:Lcom/android/server/power/PowerManagerService;

    #@b
    .line 196
    iput-object p4, p0, Lcom/android/server/Watchdog;->mAlarm:Lcom/android/server/AlarmManagerService;

    #@d
    .line 197
    iput-object p5, p0, Lcom/android/server/Watchdog;->mActivity:Lcom/android/server/am/ActivityManagerService;

    #@f
    .line 199
    new-instance v0, Lcom/android/server/Watchdog$RebootReceiver;

    #@11
    invoke-direct {v0, p0}, Lcom/android/server/Watchdog$RebootReceiver;-><init>(Lcom/android/server/Watchdog;)V

    #@14
    new-instance v1, Landroid/content/IntentFilter;

    #@16
    const-string v2, "com.android.service.Watchdog.REBOOT"

    #@18
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1e
    .line 201
    new-instance v0, Landroid/content/Intent;

    #@20
    const-string v1, "com.android.service.Watchdog.REBOOT"

    #@22
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@25
    invoke-static {p1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Lcom/android/server/Watchdog;->mRebootIntent:Landroid/app/PendingIntent;

    #@2b
    .line 204
    new-instance v0, Lcom/android/server/Watchdog$RebootRequestReceiver;

    #@2d
    invoke-direct {v0, p0}, Lcom/android/server/Watchdog$RebootRequestReceiver;-><init>(Lcom/android/server/Watchdog;)V

    #@30
    new-instance v1, Landroid/content/IntentFilter;

    #@32
    const-string v2, "android.intent.action.REBOOT"

    #@34
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@37
    const-string v2, "android.permission.REBOOT"

    #@39
    const/4 v3, 0x0

    #@3a
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@3d
    .line 208
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@40
    move-result-wide v0

    #@41
    iput-wide v0, p0, Lcom/android/server/Watchdog;->mBootTime:J

    #@43
    .line 209
    return-void
.end method

.method public processStarted(Ljava/lang/String;I)V
    .registers 4
    .parameter "name"
    .parameter "pid"

    #@0
    .prologue
    .line 212
    monitor-enter p0

    #@1
    .line 213
    :try_start_1
    const-string v0, "com.android.phone"

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 214
    iput p2, p0, Lcom/android/server/Watchdog;->mPhonePid:I

    #@b
    .line 219
    :cond_b
    :goto_b
    monitor-exit p0

    #@c
    .line 220
    return-void

    #@d
    .line 216
    :cond_d
    const-string v0, "ActivityController"

    #@f
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_b

    #@15
    .line 217
    iput p2, p0, Lcom/android/server/Watchdog;->mActivityControllerPid:I

    #@17
    goto :goto_b

    #@18
    .line 219
    :catchall_18
    move-exception v0

    #@19
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method rebootSystem(Ljava/lang/String;)V
    .registers 7
    .parameter "reason"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 312
    const-string v1, "Watchdog"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "Rebooting system because: "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 313
    const-string v1, "power"

    #@1b
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Lcom/android/server/power/PowerManagerService;

    #@21
    .line 314
    .local v0, pms:Lcom/android/server/power/PowerManagerService;
    invoke-virtual {v0, v4, p1, v4}, Lcom/android/server/power/PowerManagerService;->reboot(ZLjava/lang/String;Z)V

    #@24
    .line 315
    return-void
.end method

.method retrieveBrutalityAmount()V
    .registers 2

    #@0
    .prologue
    .line 323
    iget v0, p0, Lcom/android/server/Watchdog;->mReqMinScreenOff:I

    #@2
    if-ltz v0, :cond_15

    #@4
    iget v0, p0, Lcom/android/server/Watchdog;->mReqMinScreenOff:I

    #@6
    :goto_6
    mul-int/lit16 v0, v0, 0x3e8

    #@8
    iput v0, p0, Lcom/android/server/Watchdog;->mMinScreenOff:I

    #@a
    .line 325
    iget v0, p0, Lcom/android/server/Watchdog;->mReqMinNextAlarm:I

    #@c
    if-ltz v0, :cond_18

    #@e
    iget v0, p0, Lcom/android/server/Watchdog;->mReqMinNextAlarm:I

    #@10
    :goto_10
    mul-int/lit16 v0, v0, 0x3e8

    #@12
    iput v0, p0, Lcom/android/server/Watchdog;->mMinAlarm:I

    #@14
    .line 327
    return-void

    #@15
    .line 323
    :cond_15
    const/16 v0, 0x12c

    #@17
    goto :goto_6

    #@18
    .line 325
    :cond_18
    const/16 v0, 0xb4

    #@1a
    goto :goto_10
.end method

.method public run()V
    .registers 25

    #@0
    .prologue
    .line 383
    const/16 v19, 0x0

    #@2
    .line 385
    .local v19, waitedHalf:Z
    :goto_2
    const/16 v20, 0x0

    #@4
    move/from16 v0, v20

    #@6
    move-object/from16 v1, p0

    #@8
    iput-boolean v0, v1, Lcom/android/server/Watchdog;->mCompleted:Z

    #@a
    .line 386
    move-object/from16 v0, p0

    #@c
    iget-object v0, v0, Lcom/android/server/Watchdog;->mHandler:Landroid/os/Handler;

    #@e
    move-object/from16 v20, v0

    #@10
    const/16 v21, 0xa9e

    #@12
    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@15
    .line 388
    monitor-enter p0

    #@16
    .line 389
    const-wide/16 v15, 0x7530

    #@18
    .line 395
    .local v15, timeout:J
    :try_start_18
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1b
    move-result-wide v12

    #@1c
    .line 396
    .local v12, start:J
    :goto_1c
    const-wide/16 v20, 0x0

    #@1e
    cmp-long v20, v15, v20

    #@20
    if-lez v20, :cond_47

    #@22
    move-object/from16 v0, p0

    #@24
    iget-boolean v0, v0, Lcom/android/server/Watchdog;->mForceKillSystem:Z

    #@26
    move/from16 v20, v0
    :try_end_28
    .catchall {:try_start_18 .. :try_end_28} :catchall_44

    #@28
    if-nez v20, :cond_47

    #@2a
    .line 398
    :try_start_2a
    move-object/from16 v0, p0

    #@2c
    move-wide v1, v15

    #@2d
    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_30
    .catchall {:try_start_2a .. :try_end_30} :catchall_44
    .catch Ljava/lang/InterruptedException; {:try_start_2a .. :try_end_30} :catch_3b

    #@30
    .line 402
    :goto_30
    const-wide/16 v20, 0x7530

    #@32
    :try_start_32
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@35
    move-result-wide v22

    #@36
    sub-long v22, v22, v12

    #@38
    sub-long v15, v20, v22

    #@3a
    goto :goto_1c

    #@3b
    .line 399
    :catch_3b
    move-exception v5

    #@3c
    .line 400
    .local v5, e:Ljava/lang/InterruptedException;
    const-string v20, "Watchdog"

    #@3e
    move-object/from16 v0, v20

    #@40
    invoke-static {v0, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@43
    goto :goto_30

    #@44
    .line 421
    .end local v5           #e:Ljava/lang/InterruptedException;
    .end local v12           #start:J
    :catchall_44
    move-exception v20

    #@45
    monitor-exit p0
    :try_end_46
    .catchall {:try_start_32 .. :try_end_46} :catchall_44

    #@46
    throw v20

    #@47
    .line 405
    .restart local v12       #start:J
    :cond_47
    :try_start_47
    move-object/from16 v0, p0

    #@49
    iget-boolean v0, v0, Lcom/android/server/Watchdog;->mCompleted:Z

    #@4b
    move/from16 v20, v0

    #@4d
    if-eqz v20, :cond_5b

    #@4f
    move-object/from16 v0, p0

    #@51
    iget-boolean v0, v0, Lcom/android/server/Watchdog;->mForceKillSystem:Z

    #@53
    move/from16 v20, v0

    #@55
    if-nez v20, :cond_5b

    #@57
    .line 407
    const/16 v19, 0x0

    #@59
    .line 408
    monitor-exit p0

    #@5a
    goto :goto_2

    #@5b
    .line 411
    :cond_5b
    if-nez v19, :cond_87

    #@5d
    .line 414
    new-instance v10, Ljava/util/ArrayList;

    #@5f
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@62
    .line 415
    .local v10, pids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@65
    move-result v20

    #@66
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v20

    #@6a
    move-object/from16 v0, v20

    #@6c
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6f
    .line 416
    const/16 v20, 0x1

    #@71
    const/16 v21, 0x0

    #@73
    const/16 v22, 0x0

    #@75
    sget-object v23, Lcom/android/server/Watchdog;->NATIVE_STACKS_OF_INTEREST:[Ljava/lang/String;

    #@77
    move/from16 v0, v20

    #@79
    move-object/from16 v1, v21

    #@7b
    move-object/from16 v2, v22

    #@7d
    move-object/from16 v3, v23

    #@7f
    invoke-static {v0, v10, v1, v2, v3}, Lcom/android/server/am/ActivityManagerService;->dumpStackTraces(ZLjava/util/ArrayList;Lcom/android/internal/os/ProcessStats;Landroid/util/SparseArray;[Ljava/lang/String;)Ljava/io/File;

    #@82
    .line 418
    const/16 v19, 0x1

    #@84
    .line 419
    monitor-exit p0

    #@85
    goto/16 :goto_2

    #@87
    .line 421
    .end local v10           #pids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_87
    monitor-exit p0
    :try_end_88
    .catchall {:try_start_47 .. :try_end_88} :catchall_44

    #@88
    .line 427
    move-object/from16 v0, p0

    #@8a
    iget-object v0, v0, Lcom/android/server/Watchdog;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    #@8c
    move-object/from16 v20, v0

    #@8e
    if-eqz v20, :cond_1c8

    #@90
    move-object/from16 v0, p0

    #@92
    iget-object v0, v0, Lcom/android/server/Watchdog;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    #@94
    move-object/from16 v20, v0

    #@96
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@99
    move-result-object v20

    #@9a
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@9d
    move-result-object v7

    #@9e
    .line 429
    .local v7, name:Ljava/lang/String;
    :goto_9e
    const/16 v20, 0xaf2

    #@a0
    move/from16 v0, v20

    #@a2
    invoke-static {v0, v7}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a5
    .line 431
    new-instance v10, Ljava/util/ArrayList;

    #@a7
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@aa
    .line 432
    .restart local v10       #pids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@ad
    move-result v20

    #@ae
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b1
    move-result-object v20

    #@b2
    move-object/from16 v0, v20

    #@b4
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b7
    .line 433
    move-object/from16 v0, p0

    #@b9
    iget v0, v0, Lcom/android/server/Watchdog;->mPhonePid:I

    #@bb
    move/from16 v20, v0

    #@bd
    if-lez v20, :cond_ce

    #@bf
    move-object/from16 v0, p0

    #@c1
    iget v0, v0, Lcom/android/server/Watchdog;->mPhonePid:I

    #@c3
    move/from16 v20, v0

    #@c5
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c8
    move-result-object v20

    #@c9
    move-object/from16 v0, v20

    #@cb
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ce
    .line 434
    :cond_ce
    move-object/from16 v0, p0

    #@d0
    iget v0, v0, Lcom/android/server/Watchdog;->mActivityControllerPid:I

    #@d2
    move/from16 v20, v0

    #@d4
    if-lez v20, :cond_e5

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget v0, v0, Lcom/android/server/Watchdog;->mActivityControllerPid:I

    #@da
    move/from16 v20, v0

    #@dc
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@df
    move-result-object v20

    #@e0
    move-object/from16 v0, v20

    #@e2
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e5
    .line 437
    :cond_e5
    if-nez v19, :cond_1cc

    #@e7
    const/16 v20, 0x1

    #@e9
    :goto_e9
    const/16 v21, 0x0

    #@eb
    const/16 v22, 0x0

    #@ed
    sget-object v23, Lcom/android/server/Watchdog;->NATIVE_STACKS_OF_INTEREST:[Ljava/lang/String;

    #@ef
    move/from16 v0, v20

    #@f1
    move-object/from16 v1, v21

    #@f3
    move-object/from16 v2, v22

    #@f5
    move-object/from16 v3, v23

    #@f7
    invoke-static {v0, v10, v1, v2, v3}, Lcom/android/server/am/ActivityManagerService;->dumpStackTraces(ZLjava/util/ArrayList;Lcom/android/internal/os/ProcessStats;Landroid/util/SparseArray;[Ljava/lang/String;)Ljava/io/File;

    #@fa
    move-result-object v11

    #@fb
    .line 442
    .local v11, stack:Ljava/io/File;
    const-wide/16 v20, 0x7d0

    #@fd
    invoke-static/range {v20 .. v21}, Landroid/os/SystemClock;->sleep(J)V

    #@100
    .line 446
    invoke-direct/range {p0 .. p0}, Lcom/android/server/Watchdog;->dumpKernelStackTraces()Ljava/io/File;

    #@103
    .line 449
    const-string v20, "dalvik.vm.stack-trace-file"

    #@105
    const/16 v21, 0x0

    #@107
    invoke-static/range {v20 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10a
    move-result-object v18

    #@10b
    .line 450
    .local v18, tracesPath:Ljava/lang/String;
    if-eqz v18, :cond_15f

    #@10d
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@110
    move-result v20

    #@111
    if-eqz v20, :cond_15f

    #@113
    .line 451
    new-instance v17, Ljava/io/File;

    #@115
    invoke-direct/range {v17 .. v18}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@118
    .line 453
    .local v17, traceRenameFile:Ljava/io/File;
    const-string v20, "."

    #@11a
    move-object/from16 v0, v18

    #@11c
    move-object/from16 v1, v20

    #@11e
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@121
    move-result v6

    #@122
    .line 454
    .local v6, lpos:I
    const/16 v20, -0x1

    #@124
    move/from16 v0, v20

    #@126
    if-eq v0, v6, :cond_1d0

    #@128
    .line 455
    new-instance v20, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const/16 v21, 0x0

    #@12f
    move-object/from16 v0, v18

    #@131
    move/from16 v1, v21

    #@133
    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@136
    move-result-object v21

    #@137
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v20

    #@13b
    const-string v21, "_SystemServer_WDT"

    #@13d
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v20

    #@141
    move-object/from16 v0, v18

    #@143
    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@146
    move-result-object v21

    #@147
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v20

    #@14b
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v9

    #@14f
    .line 458
    .local v9, newTracesPath:Ljava/lang/String;
    :goto_14f
    new-instance v20, Ljava/io/File;

    #@151
    move-object/from16 v0, v20

    #@153
    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@156
    move-object/from16 v0, v17

    #@158
    move-object/from16 v1, v20

    #@15a
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@15d
    .line 459
    move-object/from16 v18, v9

    #@15f
    .line 462
    .end local v6           #lpos:I
    .end local v9           #newTracesPath:Ljava/lang/String;
    .end local v17           #traceRenameFile:Ljava/io/File;
    :cond_15f
    new-instance v8, Ljava/io/File;

    #@161
    move-object/from16 v0, v18

    #@163
    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@166
    .line 466
    .local v8, newFd:Ljava/io/File;
    :try_start_166
    new-instance v14, Ljava/io/FileWriter;

    #@168
    const-string v20, "/proc/sysrq-trigger"

    #@16a
    move-object/from16 v0, v20

    #@16c
    invoke-direct {v14, v0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@16f
    .line 467
    .local v14, sysrq_trigger:Ljava/io/FileWriter;
    const-string v20, "w"

    #@171
    move-object/from16 v0, v20

    #@173
    invoke-virtual {v14, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    #@176
    .line 468
    invoke-virtual {v14}, Ljava/io/FileWriter;->close()V
    :try_end_179
    .catch Ljava/io/IOException; {:try_start_166 .. :try_end_179} :catch_1e9

    #@179
    .line 477
    .end local v14           #sysrq_trigger:Ljava/io/FileWriter;
    :goto_179
    new-instance v4, Lcom/android/server/Watchdog$1;

    #@17b
    const-string v20, "watchdogWriteToDropbox"

    #@17d
    move-object/from16 v0, p0

    #@17f
    move-object/from16 v1, v20

    #@181
    invoke-direct {v4, v0, v1, v7, v8}, Lcom/android/server/Watchdog$1;-><init>(Lcom/android/server/Watchdog;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    #@184
    .line 484
    .local v4, dropboxThread:Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    #@187
    .line 486
    const-wide/16 v20, 0x7d0

    #@189
    :try_start_189
    move-wide/from16 v0, v20

    #@18b
    invoke-virtual {v4, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_18e
    .catch Ljava/lang/InterruptedException; {:try_start_189 .. :try_end_18e} :catch_228

    #@18e
    .line 490
    :goto_18e
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    #@191
    move-result v20

    #@192
    if-nez v20, :cond_220

    #@194
    .line 492
    const-string v20, "sys.watchdog.disabled"

    #@196
    const/16 v21, 0x0

    #@198
    invoke-static/range {v20 .. v21}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@19b
    move-result v20

    #@19c
    if-nez v20, :cond_1fc

    #@19e
    .line 493
    const-string v20, "Watchdog"

    #@1a0
    new-instance v21, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v22, "*** WATCHDOG KILLING SYSTEM PROCESS: "

    #@1a7
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v21

    #@1ab
    move-object/from16 v0, v21

    #@1ad
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v21

    #@1b1
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v21

    #@1b5
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b8
    .line 494
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@1bb
    move-result v20

    #@1bc
    invoke-static/range {v20 .. v20}, Landroid/os/Process;->killProcess(I)V

    #@1bf
    .line 495
    const/16 v20, 0xa

    #@1c1
    invoke-static/range {v20 .. v20}, Ljava/lang/System;->exit(I)V

    #@1c4
    .line 507
    :goto_1c4
    const/16 v19, 0x0

    #@1c6
    .line 508
    goto/16 :goto_2

    #@1c8
    .line 427
    .end local v4           #dropboxThread:Ljava/lang/Thread;
    .end local v7           #name:Ljava/lang/String;
    .end local v8           #newFd:Ljava/io/File;
    .end local v10           #pids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v11           #stack:Ljava/io/File;
    .end local v18           #tracesPath:Ljava/lang/String;
    :cond_1c8
    const-string v7, "null"

    #@1ca
    goto/16 :goto_9e

    #@1cc
    .line 437
    .restart local v7       #name:Ljava/lang/String;
    .restart local v10       #pids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1cc
    const/16 v20, 0x0

    #@1ce
    goto/16 :goto_e9

    #@1d0
    .line 457
    .restart local v6       #lpos:I
    .restart local v11       #stack:Ljava/io/File;
    .restart local v17       #traceRenameFile:Ljava/io/File;
    .restart local v18       #tracesPath:Ljava/lang/String;
    :cond_1d0
    new-instance v20, Ljava/lang/StringBuilder;

    #@1d2
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@1d5
    move-object/from16 v0, v20

    #@1d7
    move-object/from16 v1, v18

    #@1d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v20

    #@1dd
    const-string v21, "_SystemServer_WDT"

    #@1df
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v20

    #@1e3
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e6
    move-result-object v9

    #@1e7
    .restart local v9       #newTracesPath:Ljava/lang/String;
    goto/16 :goto_14f

    #@1e9
    .line 469
    .end local v6           #lpos:I
    .end local v9           #newTracesPath:Ljava/lang/String;
    .end local v17           #traceRenameFile:Ljava/io/File;
    .restart local v8       #newFd:Ljava/io/File;
    :catch_1e9
    move-exception v5

    #@1ea
    .line 470
    .local v5, e:Ljava/io/IOException;
    const-string v20, "Watchdog"

    #@1ec
    const-string v21, "Failed to write to /proc/sysrq-trigger"

    #@1ee
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f1
    .line 471
    const-string v20, "Watchdog"

    #@1f3
    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@1f6
    move-result-object v21

    #@1f7
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1fa
    goto/16 :goto_179

    #@1fc
    .line 497
    .end local v5           #e:Ljava/io/IOException;
    .restart local v4       #dropboxThread:Ljava/lang/Thread;
    :cond_1fc
    const-string v20, "Watchdog"

    #@1fe
    new-instance v21, Ljava/lang/StringBuilder;

    #@200
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@203
    const-string v22, "*** WATCHDOG NOT KILLING SYSTEM PROCESS: "

    #@205
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v21

    #@209
    move-object/from16 v0, v21

    #@20b
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v21

    #@20f
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@212
    move-result-object v21

    #@213
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@216
    .line 499
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@219
    move-result v20

    #@21a
    const/16 v21, 0x13

    #@21c
    invoke-static/range {v20 .. v21}, Landroid/os/Process;->sendSignal(II)V

    #@21f
    .line 509
    return-void

    #@220
    .line 504
    :cond_220
    const-string v20, "Watchdog"

    #@222
    const-string v21, "Debugger connected: Watchdog is *not* killing the system process"

    #@224
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@227
    goto :goto_1c4

    #@228
    .line 487
    :catch_228
    move-exception v20

    #@229
    goto/16 :goto_18e
.end method

.method shouldWeBeBrutalLocked(J)Ljava/lang/String;
    .registers 7
    .parameter "curTime"

    #@0
    .prologue
    .line 339
    iget-object v0, p0, Lcom/android/server/Watchdog;->mBattery:Lcom/android/server/BatteryService;

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Lcom/android/server/Watchdog;->mBattery:Lcom/android/server/BatteryService;

    #@6
    const/4 v1, 0x7

    #@7
    invoke-virtual {v0, v1}, Lcom/android/server/BatteryService;->isPowered(I)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_10

    #@d
    .line 340
    :cond_d
    const-string v0, "battery"

    #@f
    .line 353
    :goto_f
    return-object v0

    #@10
    .line 343
    :cond_10
    iget v0, p0, Lcom/android/server/Watchdog;->mMinScreenOff:I

    #@12
    if-ltz v0, :cond_28

    #@14
    iget-object v0, p0, Lcom/android/server/Watchdog;->mPower:Lcom/android/server/power/PowerManagerService;

    #@16
    if-eqz v0, :cond_25

    #@18
    iget-object v0, p0, Lcom/android/server/Watchdog;->mPower:Lcom/android/server/power/PowerManagerService;

    #@1a
    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->timeSinceScreenWasLastOn()J

    #@1d
    move-result-wide v0

    #@1e
    iget v2, p0, Lcom/android/server/Watchdog;->mMinScreenOff:I

    #@20
    int-to-long v2, v2

    #@21
    cmp-long v0, v0, v2

    #@23
    if-gez v0, :cond_28

    #@25
    .line 345
    :cond_25
    const-string v0, "screen"

    #@27
    goto :goto_f

    #@28
    .line 348
    :cond_28
    iget v0, p0, Lcom/android/server/Watchdog;->mMinAlarm:I

    #@2a
    if-ltz v0, :cond_40

    #@2c
    iget-object v0, p0, Lcom/android/server/Watchdog;->mAlarm:Lcom/android/server/AlarmManagerService;

    #@2e
    if-eqz v0, :cond_3d

    #@30
    iget-object v0, p0, Lcom/android/server/Watchdog;->mAlarm:Lcom/android/server/AlarmManagerService;

    #@32
    invoke-virtual {v0}, Lcom/android/server/AlarmManagerService;->timeToNextAlarm()J

    #@35
    move-result-wide v0

    #@36
    iget v2, p0, Lcom/android/server/Watchdog;->mMinAlarm:I

    #@38
    int-to-long v2, v2

    #@39
    cmp-long v0, v0, v2

    #@3b
    if-gez v0, :cond_40

    #@3d
    .line 350
    :cond_3d
    const-string v0, "alarm"

    #@3f
    goto :goto_f

    #@40
    .line 353
    :cond_40
    const/4 v0, 0x0

    #@41
    goto :goto_f
.end method
