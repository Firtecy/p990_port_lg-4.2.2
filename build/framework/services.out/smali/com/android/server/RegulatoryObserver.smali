.class Lcom/android/server/RegulatoryObserver;
.super Landroid/os/UEventObserver;
.source "RegulatoryObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/RegulatoryObserver$RegulatoryObserverBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final MSG_COUNTRY_CODE:I = 0x0

.field private static final REGULATORY_UEVENT_MATCH:Ljava/lang/String; = "MODALIAS=platform:regulatory"

.field private static final TAG:Ljava/lang/String; = null

.field private static final UEVENT_FILE:Ljava/lang/String; = "/sys/devices/platform/regulatory.0/uevent"


# instance fields
.field private mBootCompleted:Z

.field private final mContext:Landroid/content/Context;

.field private mCountryCode:Ljava/lang/String;

.field private mCountryKeyword:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mObserverBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 49
    const-class v0, Lcom/android/server/RegulatoryObserver;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    #@3
    .line 54
    const-string v1, "COUNTRY="

    #@5
    iput-object v1, p0, Lcom/android/server/RegulatoryObserver;->mCountryKeyword:Ljava/lang/String;

    #@7
    .line 60
    const/4 v1, 0x0

    #@8
    iput-boolean v1, p0, Lcom/android/server/RegulatoryObserver;->mBootCompleted:Z

    #@a
    .line 61
    const/4 v1, 0x0

    #@b
    iput-object v1, p0, Lcom/android/server/RegulatoryObserver;->mObserverBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@d
    .line 136
    new-instance v1, Lcom/android/server/RegulatoryObserver$1;

    #@f
    invoke-direct {v1, p0}, Lcom/android/server/RegulatoryObserver$1;-><init>(Lcom/android/server/RegulatoryObserver;)V

    #@12
    iput-object v1, p0, Lcom/android/server/RegulatoryObserver;->mHandler:Landroid/os/Handler;

    #@14
    .line 64
    iput-object p1, p0, Lcom/android/server/RegulatoryObserver;->mContext:Landroid/content/Context;

    #@16
    .line 66
    new-instance v0, Landroid/content/IntentFilter;

    #@18
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@1b
    .line 67
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@20
    .line 68
    new-instance v1, Lcom/android/server/RegulatoryObserver$RegulatoryObserverBroadcastReceiver;

    #@22
    invoke-direct {v1, p0}, Lcom/android/server/RegulatoryObserver$RegulatoryObserverBroadcastReceiver;-><init>(Lcom/android/server/RegulatoryObserver;)V

    #@25
    iput-object v1, p0, Lcom/android/server/RegulatoryObserver;->mObserverBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@27
    .line 69
    iget-object v1, p0, Lcom/android/server/RegulatoryObserver;->mContext:Landroid/content/Context;

    #@29
    iget-object v2, p0, Lcom/android/server/RegulatoryObserver;->mObserverBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@2b
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2e
    .line 71
    invoke-direct {p0}, Lcom/android/server/RegulatoryObserver;->init()V

    #@31
    .line 72
    const-string v1, "MODALIAS=platform:regulatory"

    #@33
    invoke-virtual {p0, v1}, Lcom/android/server/RegulatoryObserver;->startObserving(Ljava/lang/String;)V

    #@36
    .line 73
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 48
    sget-object v0, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/RegulatoryObserver;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/server/RegulatoryObserver;->mCountryCode:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/RegulatoryObserver;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/server/RegulatoryObserver;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/server/RegulatoryObserver;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/android/server/RegulatoryObserver;->mBootCompleted:Z

    #@2
    return p1
.end method

.method private final init()V
    .registers 9

    #@0
    .prologue
    .line 91
    :try_start_0
    sget-object v6, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@2
    const-string v7, "RegulatoryObserver init."

    #@4
    invoke-static {v6, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 94
    new-instance v4, Ljava/io/BufferedReader;

    #@9
    new-instance v6, Ljava/io/FileReader;

    #@b
    const-string v7, "/sys/devices/platform/regulatory.0/uevent"

    #@d
    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@10
    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@13
    .line 97
    .local v4, uevent_buf:Ljava/io/BufferedReader;
    :cond_13
    :goto_13
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    .local v3, line:Ljava/lang/String;
    if-eqz v3, :cond_41

    #@19
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_41

    #@1f
    .line 98
    const-string v6, "="

    #@21
    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    .line 99
    .local v1, event_string:[Ljava/lang/String;
    const/4 v6, 0x0

    #@26
    aget-object v2, v1, v6

    #@28
    .line 100
    .local v2, key:Ljava/lang/String;
    const/4 v6, 0x1

    #@29
    aget-object v5, v1, v6

    #@2b
    .line 101
    .local v5, value:Ljava/lang/String;
    const-string v6, "COUNTRY"

    #@2d
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v6

    #@31
    if-eqz v6, :cond_13

    #@33
    .line 105
    iput-object v5, p0, Lcom/android/server/RegulatoryObserver;->mCountryCode:Ljava/lang/String;

    #@35
    .line 106
    invoke-direct {p0}, Lcom/android/server/RegulatoryObserver;->run_crda()V
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_38} :catch_39

    #@38
    goto :goto_13

    #@39
    .line 109
    .end local v1           #event_string:[Ljava/lang/String;
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #line:Ljava/lang/String;
    .end local v4           #uevent_buf:Ljava/io/BufferedReader;
    .end local v5           #value:Ljava/lang/String;
    :catch_39
    move-exception v0

    #@3a
    .line 110
    .local v0, e:Ljava/lang/Exception;
    sget-object v6, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@3c
    const-string v7, "This kernel may not have CRDA support."

    #@3e
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@41
    .line 112
    .end local v0           #e:Ljava/lang/Exception;
    :cond_41
    return-void
.end method

.method private final run_crda()V
    .registers 5

    #@0
    .prologue
    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/android/server/RegulatoryObserver;->mCountryCode:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_45

    #@4
    .line 118
    const-string v1, "wlan.crda.country"

    #@6
    iget-object v2, p0, Lcom/android/server/RegulatoryObserver;->mCountryCode:Ljava/lang/String;

    #@8
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 119
    const-string v1, "ctl.start"

    #@d
    const-string v2, "wifi-crda"

    #@f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 120
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@14
    const-string v2, "Start wifi-crda service to run crda."

    #@16
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 121
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@1b
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "Country Code is "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    iget-object v3, p0, Lcom/android/server/RegulatoryObserver;->mCountryCode:Ljava/lang/String;

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 124
    iget-boolean v1, p0, Lcom/android/server/RegulatoryObserver;->mBootCompleted:Z

    #@35
    const/4 v2, 0x1

    #@36
    if-ne v1, v2, :cond_46

    #@38
    .line 125
    iget-object v1, p0, Lcom/android/server/RegulatoryObserver;->mHandler:Landroid/os/Handler;

    #@3a
    const/4 v2, 0x0

    #@3b
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@3e
    .line 126
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@40
    const-string v2, "Send broadcast country code message."

    #@42
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 134
    :cond_45
    :goto_45
    return-void

    #@46
    .line 128
    :cond_46
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@48
    const-string v2, "Cannot Send broadcast before boot-up."

    #@4a
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4d} :catch_4e

    #@4d
    goto :goto_45

    #@4e
    .line 131
    :catch_4e
    move-exception v0

    #@4f
    .line 132
    .local v0, e:Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@51
    const-string v2, "Failed to start wifi-crda service to run crda."

    #@53
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@56
    goto :goto_45
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 77
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "uevent:\n"

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {p1}, Landroid/os/UEventObserver$UEvent;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 78
    monitor-enter p0

    #@1d
    .line 80
    :try_start_1d
    const-string v1, "COUNTRY"

    #@1f
    invoke-virtual {p1, v1}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Lcom/android/server/RegulatoryObserver;->mCountryCode:Ljava/lang/String;

    #@25
    .line 81
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@27
    new-instance v2, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v3, "Regulatory Country Code:"

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    iget-object v3, p0, Lcom/android/server/RegulatoryObserver;->mCountryCode:Ljava/lang/String;

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 82
    invoke-direct {p0}, Lcom/android/server/RegulatoryObserver;->run_crda()V
    :try_end_42
    .catchall {:try_start_1d .. :try_end_42} :catchall_5e
    .catch Ljava/lang/NumberFormatException; {:try_start_1d .. :try_end_42} :catch_44

    #@42
    .line 86
    :goto_42
    :try_start_42
    monitor-exit p0

    #@43
    .line 87
    return-void

    #@44
    .line 83
    :catch_44
    move-exception v0

    #@45
    .line 84
    .local v0, e:Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/android/server/RegulatoryObserver;->TAG:Ljava/lang/String;

    #@47
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v3, "Could not parse country code from event "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_42

    #@5e
    .line 86
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catchall_5e
    move-exception v1

    #@5f
    monitor-exit p0
    :try_end_60
    .catchall {:try_start_42 .. :try_end_60} :catchall_5e

    #@60
    throw v1
.end method
