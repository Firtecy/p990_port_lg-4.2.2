.class Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;
.super Ljava/lang/Object;
.source "NetworkManagementService.java"

# interfaces
.implements Lcom/android/server/INativeDaemonConnectorCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NetworkManagementService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetdCallbackReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/NetworkManagementService;


# direct methods
.method private constructor <init>(Lcom/android/server/NetworkManagementService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 496
    iput-object p1, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/NetworkManagementService;Lcom/android/server/NetworkManagementService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 496
    invoke-direct {p0, p1}, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;-><init>(Lcom/android/server/NetworkManagementService;)V

    #@3
    return-void
.end method


# virtual methods
.method public onDaemonConnected()V
    .registers 3

    #@0
    .prologue
    .line 501
    iget-object v0, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@2
    invoke-static {v0}, Lcom/android/server/NetworkManagementService;->access$100(Lcom/android/server/NetworkManagementService;)Ljava/util/concurrent/CountDownLatch;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_18

    #@8
    .line 502
    iget-object v0, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@a
    invoke-static {v0}, Lcom/android/server/NetworkManagementService;->access$100(Lcom/android/server/NetworkManagementService;)Ljava/util/concurrent/CountDownLatch;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@11
    .line 503
    iget-object v0, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-static {v0, v1}, Lcom/android/server/NetworkManagementService;->access$102(Lcom/android/server/NetworkManagementService;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;

    #@17
    .line 512
    :goto_17
    return-void

    #@18
    .line 505
    :cond_18
    iget-object v0, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@1a
    invoke-static {v0}, Lcom/android/server/NetworkManagementService;->access$300(Lcom/android/server/NetworkManagementService;)Landroid/os/Handler;

    #@1d
    move-result-object v0

    #@1e
    new-instance v1, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver$1;

    #@20
    invoke-direct {v1, p0}, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver$1;-><init>(Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;)V

    #@23
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@26
    goto :goto_17
.end method

.method public onEvent(ILjava/lang/String;[Ljava/lang/String;)Z
    .registers 12
    .parameter "code"
    .parameter "raw"
    .parameter "cooked"

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x1

    #@5
    .line 516
    sparse-switch p1, :sswitch_data_150

    #@8
    move v1, v2

    #@9
    .line 590
    :goto_9
    return v1

    #@a
    .line 525
    :sswitch_a
    array-length v3, p3

    #@b
    if-lt v3, v7, :cond_17

    #@d
    aget-object v3, p3, v1

    #@f
    const-string v4, "Iface"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_27

    #@17
    .line 526
    :cond_17
    new-instance v3, Ljava/lang/IllegalStateException;

    #@19
    const-string v4, "Invalid event from daemon (%s)"

    #@1b
    new-array v1, v1, [Ljava/lang/Object;

    #@1d
    aput-object p2, v1, v2

    #@1f
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@26
    throw v3

    #@27
    .line 529
    :cond_27
    aget-object v3, p3, v5

    #@29
    const-string v4, "added"

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v3

    #@2f
    if-eqz v3, :cond_39

    #@31
    .line 530
    iget-object v2, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@33
    aget-object v3, p3, v6

    #@35
    invoke-static {v2, v3}, Lcom/android/server/NetworkManagementService;->access$400(Lcom/android/server/NetworkManagementService;Ljava/lang/String;)V

    #@38
    goto :goto_9

    #@39
    .line 532
    :cond_39
    aget-object v3, p3, v5

    #@3b
    const-string v4, "removed"

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v3

    #@41
    if-eqz v3, :cond_4b

    #@43
    .line 533
    iget-object v2, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@45
    aget-object v3, p3, v6

    #@47
    invoke-static {v2, v3}, Lcom/android/server/NetworkManagementService;->access$500(Lcom/android/server/NetworkManagementService;Ljava/lang/String;)V

    #@4a
    goto :goto_9

    #@4b
    .line 535
    :cond_4b
    aget-object v3, p3, v5

    #@4d
    const-string v4, "changed"

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v3

    #@53
    if-eqz v3, :cond_69

    #@55
    array-length v3, p3

    #@56
    const/4 v4, 0x5

    #@57
    if-ne v3, v4, :cond_69

    #@59
    .line 536
    iget-object v2, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@5b
    aget-object v3, p3, v6

    #@5d
    aget-object v4, p3, v7

    #@5f
    const-string v5, "up"

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v4

    #@65
    invoke-static {v2, v3, v4}, Lcom/android/server/NetworkManagementService;->access$600(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Z)V

    #@68
    goto :goto_9

    #@69
    .line 538
    :cond_69
    aget-object v3, p3, v5

    #@6b
    const-string v4, "linkstate"

    #@6d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v3

    #@71
    if-eqz v3, :cond_87

    #@73
    array-length v3, p3

    #@74
    const/4 v4, 0x5

    #@75
    if-ne v3, v4, :cond_87

    #@77
    .line 539
    iget-object v2, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@79
    aget-object v3, p3, v6

    #@7b
    aget-object v4, p3, v7

    #@7d
    const-string v5, "up"

    #@7f
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v4

    #@83
    invoke-static {v2, v3, v4}, Lcom/android/server/NetworkManagementService;->access$700(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Z)V

    #@86
    goto :goto_9

    #@87
    .line 542
    :cond_87
    new-instance v3, Ljava/lang/IllegalStateException;

    #@89
    const-string v4, "Invalid event from daemon (%s)"

    #@8b
    new-array v1, v1, [Ljava/lang/Object;

    #@8d
    aput-object p2, v1, v2

    #@8f
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@92
    move-result-object v1

    #@93
    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@96
    throw v3

    #@97
    .line 550
    :sswitch_97
    array-length v3, p3

    #@98
    const/4 v4, 0x5

    #@99
    if-lt v3, v4, :cond_a5

    #@9b
    aget-object v3, p3, v1

    #@9d
    const-string v4, "limit"

    #@9f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a2
    move-result v3

    #@a3
    if-nez v3, :cond_b5

    #@a5
    .line 551
    :cond_a5
    new-instance v3, Ljava/lang/IllegalStateException;

    #@a7
    const-string v4, "Invalid event from daemon (%s)"

    #@a9
    new-array v1, v1, [Ljava/lang/Object;

    #@ab
    aput-object p2, v1, v2

    #@ad
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b0
    move-result-object v1

    #@b1
    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b4
    throw v3

    #@b5
    .line 554
    :cond_b5
    aget-object v3, p3, v5

    #@b7
    const-string v4, "alert"

    #@b9
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bc
    move-result v3

    #@bd
    if-eqz v3, :cond_ca

    #@bf
    .line 555
    iget-object v2, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@c1
    aget-object v3, p3, v6

    #@c3
    aget-object v4, p3, v7

    #@c5
    invoke-static {v2, v3, v4}, Lcom/android/server/NetworkManagementService;->access$800(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Ljava/lang/String;)V

    #@c8
    goto/16 :goto_9

    #@ca
    .line 558
    :cond_ca
    new-instance v3, Ljava/lang/IllegalStateException;

    #@cc
    const-string v4, "Invalid event from daemon (%s)"

    #@ce
    new-array v1, v1, [Ljava/lang/Object;

    #@d0
    aput-object p2, v1, v2

    #@d2
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@d5
    move-result-object v1

    #@d6
    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d9
    throw v3

    #@da
    .line 566
    :sswitch_da
    array-length v3, p3

    #@db
    if-lt v3, v7, :cond_e7

    #@dd
    aget-object v3, p3, v1

    #@df
    const-string v4, "IfaceClass"

    #@e1
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e4
    move-result v3

    #@e5
    if-nez v3, :cond_f7

    #@e7
    .line 567
    :cond_e7
    new-instance v3, Ljava/lang/IllegalStateException;

    #@e9
    const-string v4, "Invalid event from daemon (%s)"

    #@eb
    new-array v1, v1, [Ljava/lang/Object;

    #@ed
    aput-object p2, v1, v2

    #@ef
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@f2
    move-result-object v1

    #@f3
    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f6
    throw v3

    #@f7
    .line 570
    :cond_f7
    aget-object v2, p3, v5

    #@f9
    const-string v3, "active"

    #@fb
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fe
    move-result v0

    #@ff
    .line 571
    .local v0, isActive:Z
    iget-object v2, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@101
    aget-object v3, p3, v6

    #@103
    invoke-static {v2, v3, v0}, Lcom/android/server/NetworkManagementService;->access$900(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Z)V

    #@106
    goto/16 :goto_9

    #@108
    .line 576
    .end local v0           #isActive:Z
    :sswitch_108
    array-length v3, p3

    #@109
    const/4 v4, 0x5

    #@10a
    if-lt v3, v4, :cond_116

    #@10c
    aget-object v3, p3, v1

    #@10e
    const-string v4, "DNS"

    #@110
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@113
    move-result v3

    #@114
    if-nez v3, :cond_126

    #@116
    .line 577
    :cond_116
    new-instance v3, Ljava/lang/IllegalStateException;

    #@118
    const-string v4, "Invalid event from daemon (%s)"

    #@11a
    new-array v1, v1, [Ljava/lang/Object;

    #@11c
    aput-object p2, v1, v2

    #@11e
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@121
    move-result-object v1

    #@122
    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@125
    throw v3

    #@126
    .line 580
    :cond_126
    aget-object v3, p3, v5

    #@128
    const-string v4, "Fail"

    #@12a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12d
    move-result v3

    #@12e
    if-eqz v3, :cond_13f

    #@130
    .line 581
    iget-object v2, p0, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;->this$0:Lcom/android/server/NetworkManagementService;

    #@132
    aget-object v3, p3, v6

    #@134
    aget-object v4, p3, v7

    #@136
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@139
    move-result v4

    #@13a
    invoke-static {v2, v3, v4}, Lcom/android/server/NetworkManagementService;->access$1000(Lcom/android/server/NetworkManagementService;Ljava/lang/String;I)V

    #@13d
    goto/16 :goto_9

    #@13f
    .line 584
    :cond_13f
    new-instance v3, Ljava/lang/IllegalStateException;

    #@141
    const-string v4, "Invalid event from daemon (%s)"

    #@143
    new-array v1, v1, [Ljava/lang/Object;

    #@145
    aput-object p2, v1, v2

    #@147
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@14a
    move-result-object v1

    #@14b
    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@14e
    throw v3

    #@14f
    .line 516
    nop

    #@150
    :sswitch_data_150
    .sparse-switch
        0x258 -> :sswitch_a
        0x259 -> :sswitch_97
        0x265 -> :sswitch_da
        0x267 -> :sswitch_108
    .end sparse-switch
.end method
