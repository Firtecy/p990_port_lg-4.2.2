.class public Lcom/android/server/StatusBarManagerService;
.super Lcom/android/internal/statusbar/IStatusBarService$Stub;
.source "StatusBarManagerService.java"

# interfaces
.implements Lcom/android/server/wm/WindowManagerService$OnHardKeyboardStatusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/StatusBarManagerService$NotificationCallbacks;,
        Lcom/android/server/StatusBarManagerService$DisableRecord;
    }
.end annotation


# static fields
.field static final SPEW:Z = false

.field static final TAG:Ljava/lang/String; = "StatusBarManagerService"


# instance fields
.field volatile mBar:Lcom/android/internal/statusbar/IStatusBar;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field final mContext:Landroid/content/Context;

.field mCurrentUserId:I

.field final mDisableRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/StatusBarManagerService$DisableRecord;",
            ">;"
        }
    .end annotation
.end field

.field mDisabled:I

.field mHandler:Landroid/os/Handler;

.field mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

.field mImeBackDisposition:I

.field mImeToken:Landroid/os/IBinder;

.field mImeWindowVis:I

.field mLock:Ljava/lang/Object;

.field mMenuVisible:Z

.field mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

.field mNotifications:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/android/internal/statusbar/StatusBarNotification;",
            ">;"
        }
    .end annotation
.end field

.field mSysUiVisToken:Landroid/os/IBinder;

.field mSystemUiVisibility:I

.field final mWindowManager:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    .registers 6
    .parameter "context"
    .parameter "windowManager"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 106
    invoke-direct {p0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;-><init>()V

    #@4
    .line 59
    new-instance v1, Landroid/os/Handler;

    #@6
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mHandler:Landroid/os/Handler;

    #@b
    .line 62
    new-instance v1, Lcom/android/internal/statusbar/StatusBarIconList;

    #@d
    invoke-direct {v1}, Lcom/android/internal/statusbar/StatusBarIconList;-><init>()V

    #@10
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@12
    .line 63
    new-instance v1, Ljava/util/HashMap;

    #@14
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@17
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@19
    .line 67
    new-instance v1, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@1e
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@20
    .line 68
    new-instance v1, Landroid/os/Binder;

    #@22
    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    #@25
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mSysUiVisToken:Landroid/os/IBinder;

    #@27
    .line 69
    iput v2, p0, Lcom/android/server/StatusBarManagerService;->mDisabled:I

    #@29
    .line 71
    new-instance v1, Ljava/lang/Object;

    #@2b
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@2e
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@30
    .line 73
    iput v2, p0, Lcom/android/server/StatusBarManagerService;->mSystemUiVisibility:I

    #@32
    .line 74
    iput-boolean v2, p0, Lcom/android/server/StatusBarManagerService;->mMenuVisible:Z

    #@34
    .line 75
    iput v2, p0, Lcom/android/server/StatusBarManagerService;->mImeWindowVis:I

    #@36
    .line 77
    const/4 v1, 0x0

    #@37
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mImeToken:Landroid/os/IBinder;

    #@39
    .line 631
    new-instance v1, Lcom/android/server/StatusBarManagerService$7;

    #@3b
    invoke-direct {v1, p0}, Lcom/android/server/StatusBarManagerService$7;-><init>(Lcom/android/server/StatusBarManagerService;)V

    #@3e
    iput-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@40
    .line 107
    iput-object p1, p0, Lcom/android/server/StatusBarManagerService;->mContext:Landroid/content/Context;

    #@42
    .line 108
    iput-object p2, p0, Lcom/android/server/StatusBarManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@44
    .line 109
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@46
    invoke-virtual {v1, p0}, Lcom/android/server/wm/WindowManagerService;->setOnHardKeyboardStatusChangeListener(Lcom/android/server/wm/WindowManagerService$OnHardKeyboardStatusChangeListener;)V

    #@49
    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4c
    move-result-object v0

    #@4d
    .line 112
    .local v0, res:Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@4f
    const v2, 0x1070016

    #@52
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v1, v2}, Lcom/android/internal/statusbar/StatusBarIconList;->defineSlots([Ljava/lang/String;)V

    #@59
    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/StatusBarManagerService;IILandroid/os/IBinder;Ljava/lang/String;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/StatusBarManagerService;->disableInternal(IILandroid/os/IBinder;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private disableInternal(IILandroid/os/IBinder;Ljava/lang/String;)V
    .registers 7
    .parameter "userId"
    .parameter "what"
    .parameter "token"
    .parameter "pkg"

    #@0
    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBar()V

    #@3
    .line 162
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 163
    :try_start_6
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/StatusBarManagerService;->disableLocked(IILandroid/os/IBinder;Ljava/lang/String;)V

    #@9
    .line 164
    monitor-exit v1

    #@a
    .line 165
    return-void

    #@b
    .line 164
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_6 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method private disableLocked(IILandroid/os/IBinder;Ljava/lang/String;)V
    .registers 8
    .parameter "userId"
    .parameter "what"
    .parameter "token"
    .parameter "pkg"

    #@0
    .prologue
    .line 172
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/StatusBarManagerService;->manageDisableListLocked(IILandroid/os/IBinder;Ljava/lang/String;)V

    #@3
    .line 175
    iget v1, p0, Lcom/android/server/StatusBarManagerService;->mCurrentUserId:I

    #@5
    invoke-virtual {p0, v1}, Lcom/android/server/StatusBarManagerService;->gatherDisableActionsLocked(I)I

    #@8
    move-result v0

    #@9
    .line 176
    .local v0, net:I
    iget v1, p0, Lcom/android/server/StatusBarManagerService;->mDisabled:I

    #@b
    if-eq v0, v1, :cond_22

    #@d
    .line 177
    iput v0, p0, Lcom/android/server/StatusBarManagerService;->mDisabled:I

    #@f
    .line 178
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mHandler:Landroid/os/Handler;

    #@11
    new-instance v2, Lcom/android/server/StatusBarManagerService$1;

    #@13
    invoke-direct {v2, p0, v0}, Lcom/android/server/StatusBarManagerService$1;-><init>(Lcom/android/server/StatusBarManagerService;I)V

    #@16
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@19
    .line 183
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 185
    :try_start_1d
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@1f
    invoke-interface {v1, v0}, Lcom/android/internal/statusbar/IStatusBar;->disable(I)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_22} :catch_23

    #@22
    .line 190
    :cond_22
    :goto_22
    return-void

    #@23
    .line 186
    :catch_23
    move-exception v1

    #@24
    goto :goto_22
.end method

.method private enforceExpandStatusBar()V
    .registers 4

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.EXPAND_STATUS_BAR"

    #@4
    const-string v2, "StatusBarManagerService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 410
    return-void
.end method

.method private enforceStatusBar()V
    .registers 4

    #@0
    .prologue
    .line 403
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.STATUS_BAR"

    #@4
    const-string v2, "StatusBarManagerService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 405
    return-void
.end method

.method private enforceStatusBarService()V
    .registers 4

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.STATUS_BAR_SERVICE"

    #@4
    const-string v2, "StatusBarManagerService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 415
    return-void
.end method

.method private updateUiVisibilityLocked(II)V
    .registers 5
    .parameter "vis"
    .parameter "mask"

    #@0
    .prologue
    .line 332
    iget v0, p0, Lcom/android/server/StatusBarManagerService;->mSystemUiVisibility:I

    #@2
    if-eq v0, p1, :cond_10

    #@4
    .line 333
    iput p1, p0, Lcom/android/server/StatusBarManagerService;->mSystemUiVisibility:I

    #@6
    .line 334
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mHandler:Landroid/os/Handler;

    #@8
    new-instance v1, Lcom/android/server/StatusBarManagerService$4;

    #@a
    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/StatusBarManagerService$4;-><init>(Lcom/android/server/StatusBarManagerService;II)V

    #@d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@10
    .line 345
    :cond_10
    return-void
.end method


# virtual methods
.method public addNotification(Lcom/android/internal/statusbar/StatusBarNotification;)Landroid/os/IBinder;
    .registers 5
    .parameter "notification"

    #@0
    .prologue
    .line 489
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 490
    :try_start_3
    new-instance v0, Landroid/os/Binder;

    #@5
    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    #@8
    .line 491
    .local v0, key:Landroid/os/IBinder;
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@a
    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 492
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_18

    #@f
    if-eqz v1, :cond_16

    #@11
    .line 494
    :try_start_11
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@13
    invoke-interface {v1, v0, p1}, Lcom/android/internal/statusbar/IStatusBar;->addNotification(Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;)V
    :try_end_16
    .catchall {:try_start_11 .. :try_end_16} :catchall_18
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_16} :catch_1b

    #@16
    .line 498
    :cond_16
    :goto_16
    :try_start_16
    monitor-exit v2

    #@17
    return-object v0

    #@18
    .line 499
    .end local v0           #key:Landroid/os/IBinder;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_16 .. :try_end_1a} :catchall_18

    #@1a
    throw v1

    #@1b
    .line 495
    .restart local v0       #key:Landroid/os/IBinder;
    :catch_1b
    move-exception v1

    #@1c
    goto :goto_16
.end method

.method public cancelPreloadRecentApps()V
    .registers 2

    #@0
    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 391
    :try_start_4
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@6
    invoke-interface {v0}, Lcom/android/internal/statusbar/IStatusBar;->cancelPreloadRecentApps()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 394
    :cond_9
    :goto_9
    return-void

    #@a
    .line 392
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public collapsePanels()V
    .registers 2

    #@0
    .prologue
    .line 134
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceExpandStatusBar()V

    #@3
    .line 136
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 138
    :try_start_7
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@9
    invoke-interface {v0}, Lcom/android/internal/statusbar/IStatusBar;->animateCollapsePanels()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_d

    #@c
    .line 142
    :cond_c
    :goto_c
    return-void

    #@d
    .line 139
    :catch_d
    move-exception v0

    #@e
    goto :goto_c
.end method

.method public disable(ILandroid/os/IBinder;Ljava/lang/String;)V
    .registers 5
    .parameter "what"
    .parameter "token"
    .parameter "pkg"

    #@0
    .prologue
    .line 156
    iget v0, p0, Lcom/android/server/StatusBarManagerService;->mCurrentUserId:I

    #@2
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/server/StatusBarManagerService;->disableInternal(IILandroid/os/IBinder;Ljava/lang/String;)V

    #@5
    .line 157
    return-void
.end method

.method public disableTouch(Z)V
    .registers 4
    .parameter "disable"

    #@0
    .prologue
    .line 654
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBar()V

    #@3
    .line 656
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 657
    :try_start_6
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;
    :try_end_8
    .catchall {:try_start_6 .. :try_end_8} :catchall_11

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 659
    :try_start_a
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@c
    invoke-interface {v0, p1}, Lcom/android/internal/statusbar/IStatusBar;->disableTouch(Z)V
    :try_end_f
    .catchall {:try_start_a .. :try_end_f} :catchall_11
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_14

    #@f
    .line 663
    :cond_f
    :goto_f
    :try_start_f
    monitor-exit v1

    #@10
    .line 664
    return-void

    #@11
    .line 663
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_f .. :try_end_13} :catchall_11

    #@13
    throw v0

    #@14
    .line 660
    :catch_14
    move-exception v0

    #@15
    goto :goto_f
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 14
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 596
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v6, "android.permission.DUMP"

    #@4
    invoke-virtual {v5, v6}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_33

    #@a
    .line 598
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "Permission Denial: can\'t dump StatusBar from from pid="

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v6

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    const-string v6, ", uid="

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v6

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 629
    :goto_32
    return-void

    #@33
    .line 604
    :cond_33
    iget-object v6, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@35
    monitor-enter v6

    #@36
    .line 605
    :try_start_36
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@38
    invoke-virtual {v5, p2}, Lcom/android/internal/statusbar/StatusBarIconList;->dump(Ljava/io/PrintWriter;)V

    #@3b
    .line 606
    monitor-exit v6
    :try_end_3c
    .catchall {:try_start_36 .. :try_end_3c} :catchall_7a

    #@3c
    .line 608
    iget-object v6, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@3e
    monitor-enter v6

    #@3f
    .line 609
    const/4 v2, 0x0

    #@40
    .line 610
    .local v2, i:I
    :try_start_40
    const-string v5, "Notification list:"

    #@42
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@45
    .line 611
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@47
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@4a
    move-result-object v5

    #@4b
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@4e
    move-result-object v3

    #@4f
    .local v3, i$:Ljava/util/Iterator;
    :goto_4f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@52
    move-result v5

    #@53
    if-eqz v5, :cond_7d

    #@55
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@58
    move-result-object v1

    #@59
    check-cast v1, Ljava/util/Map$Entry;

    #@5b
    .line 612
    .local v1, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;>;"
    const-string v7, "  %2d: %s\n"

    #@5d
    const/4 v5, 0x2

    #@5e
    new-array v8, v5, [Ljava/lang/Object;

    #@60
    const/4 v5, 0x0

    #@61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@64
    move-result-object v9

    #@65
    aput-object v9, v8, v5

    #@67
    const/4 v9, 0x1

    #@68
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@6b
    move-result-object v5

    #@6c
    check-cast v5, Lcom/android/internal/statusbar/StatusBarNotification;

    #@6e
    invoke-virtual {v5}, Lcom/android/internal/statusbar/StatusBarNotification;->toString()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    aput-object v5, v8, v9

    #@74
    invoke-virtual {p2, v7, v8}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_77
    .catchall {:try_start_40 .. :try_end_77} :catchall_111

    #@77
    .line 613
    add-int/lit8 v2, v2, 0x1

    #@79
    goto :goto_4f

    #@7a
    .line 606
    .end local v1           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;>;"
    .end local v2           #i:I
    .end local v3           #i$:Ljava/util/Iterator;
    :catchall_7a
    move-exception v5

    #@7b
    :try_start_7b
    monitor-exit v6
    :try_end_7c
    .catchall {:try_start_7b .. :try_end_7c} :catchall_7a

    #@7c
    throw v5

    #@7d
    .line 615
    .restart local v2       #i:I
    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_7d
    :try_start_7d
    monitor-exit v6
    :try_end_7e
    .catchall {:try_start_7d .. :try_end_7e} :catchall_111

    #@7e
    .line 617
    iget-object v6, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@80
    monitor-enter v6

    #@81
    .line 618
    :try_start_81
    new-instance v5, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v7, "  mDisabled=0x"

    #@88
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v5

    #@8c
    iget v7, p0, Lcom/android/server/StatusBarManagerService;->mDisabled:I

    #@8e
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@91
    move-result-object v7

    #@92
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v5

    #@96
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v5

    #@9a
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9d
    .line 619
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@9f
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@a2
    move-result v0

    #@a3
    .line 620
    .local v0, N:I
    new-instance v5, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v7, "  mDisableRecords.size="

    #@aa
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v5

    #@ae
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v5

    #@b2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b9
    .line 621
    const/4 v2, 0x0

    #@ba
    :goto_ba
    if-ge v2, v0, :cond_114

    #@bc
    .line 622
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@be
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c1
    move-result-object v4

    #@c2
    check-cast v4, Lcom/android/server/StatusBarManagerService$DisableRecord;

    #@c4
    .line 623
    .local v4, tok:Lcom/android/server/StatusBarManagerService$DisableRecord;
    new-instance v5, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v7, "    ["

    #@cb
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v5

    #@cf
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v5

    #@d3
    const-string v7, "] userId="

    #@d5
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v5

    #@d9
    iget v7, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->userId:I

    #@db
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@de
    move-result-object v5

    #@df
    const-string v7, " what=0x"

    #@e1
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v5

    #@e5
    iget v7, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->what:I

    #@e7
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@ea
    move-result-object v7

    #@eb
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v5

    #@ef
    const-string v7, " pkg="

    #@f1
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v5

    #@f5
    iget-object v7, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->pkg:Ljava/lang/String;

    #@f7
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v5

    #@fb
    const-string v7, " token="

    #@fd
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v5

    #@101
    iget-object v7, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->token:Landroid/os/IBinder;

    #@103
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v5

    #@107
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v5

    #@10b
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_10e
    .catchall {:try_start_81 .. :try_end_10e} :catchall_117

    #@10e
    .line 621
    add-int/lit8 v2, v2, 0x1

    #@110
    goto :goto_ba

    #@111
    .line 615
    .end local v0           #N:I
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #tok:Lcom/android/server/StatusBarManagerService$DisableRecord;
    :catchall_111
    move-exception v5

    #@112
    :try_start_112
    monitor-exit v6
    :try_end_113
    .catchall {:try_start_112 .. :try_end_113} :catchall_111

    #@113
    throw v5

    #@114
    .line 628
    .restart local v0       #N:I
    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_114
    :try_start_114
    monitor-exit v6

    #@115
    goto/16 :goto_32

    #@117
    .end local v0           #N:I
    :catchall_117
    move-exception v5

    #@118
    monitor-exit v6
    :try_end_119
    .catchall {:try_start_114 .. :try_end_119} :catchall_117

    #@119
    throw v5
.end method

.method public expandNotificationsPanel()V
    .registers 2

    #@0
    .prologue
    .line 123
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceExpandStatusBar()V

    #@3
    .line 125
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 127
    :try_start_7
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@9
    invoke-interface {v0}, Lcom/android/internal/statusbar/IStatusBar;->animateExpandNotificationsPanel()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_d

    #@c
    .line 131
    :cond_c
    :goto_c
    return-void

    #@d
    .line 128
    :catch_d
    move-exception v0

    #@e
    goto :goto_c
.end method

.method public expandSettingsPanel()V
    .registers 2

    #@0
    .prologue
    .line 145
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceExpandStatusBar()V

    #@3
    .line 147
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 149
    :try_start_7
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@9
    invoke-interface {v0}, Lcom/android/internal/statusbar/IStatusBar;->animateExpandSettingsPanel()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_d

    #@c
    .line 153
    :cond_c
    :goto_c
    return-void

    #@d
    .line 150
    :catch_d
    move-exception v0

    #@e
    goto :goto_c
.end method

.method gatherDisableActionsLocked(I)I
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 579
    iget-object v4, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 581
    .local v0, N:I
    const/4 v2, 0x0

    #@7
    .line 582
    .local v2, net:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1c

    #@a
    .line 583
    iget-object v4, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Lcom/android/server/StatusBarManagerService$DisableRecord;

    #@12
    .line 584
    .local v3, rec:Lcom/android/server/StatusBarManagerService$DisableRecord;
    iget v4, v3, Lcom/android/server/StatusBarManagerService$DisableRecord;->userId:I

    #@14
    if-ne v4, p1, :cond_19

    #@16
    .line 585
    iget v4, v3, Lcom/android/server/StatusBarManagerService$DisableRecord;->what:I

    #@18
    or-int/2addr v2, v4

    #@19
    .line 582
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_8

    #@1c
    .line 588
    .end local v3           #rec:Lcom/android/server/StatusBarManagerService$DisableRecord;
    :cond_1c
    return v2
.end method

.method manageDisableListLocked(IILandroid/os/IBinder;Ljava/lang/String;)V
    .registers 12
    .parameter "userId"
    .parameter "what"
    .parameter "token"
    .parameter "pkg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 544
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 545
    .local v0, N:I
    const/4 v4, 0x0

    #@8
    .line 547
    .local v4, tok:Lcom/android/server/StatusBarManagerService$DisableRecord;
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_1c

    #@b
    .line 548
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Lcom/android/server/StatusBarManagerService$DisableRecord;

    #@13
    .line 549
    .local v3, t:Lcom/android/server/StatusBarManagerService$DisableRecord;
    iget-object v5, v3, Lcom/android/server/StatusBarManagerService$DisableRecord;->token:Landroid/os/IBinder;

    #@15
    if-ne v5, p3, :cond_31

    #@17
    iget v5, v3, Lcom/android/server/StatusBarManagerService$DisableRecord;->userId:I

    #@19
    if-ne v5, p1, :cond_31

    #@1b
    .line 550
    move-object v4, v3

    #@1c
    .line 554
    .end local v3           #t:Lcom/android/server/StatusBarManagerService$DisableRecord;
    :cond_1c
    if-eqz p2, :cond_24

    #@1e
    invoke-interface {p3}, Landroid/os/IBinder;->isBinderAlive()Z

    #@21
    move-result v5

    #@22
    if-nez v5, :cond_34

    #@24
    .line 555
    :cond_24
    if-eqz v4, :cond_30

    #@26
    .line 556
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2b
    .line 557
    iget-object v5, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->token:Landroid/os/IBinder;

    #@2d
    invoke-interface {v5, v4, v6}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@30
    .line 575
    :cond_30
    :goto_30
    return-void

    #@31
    .line 547
    .restart local v3       #t:Lcom/android/server/StatusBarManagerService$DisableRecord;
    :cond_31
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_9

    #@34
    .line 560
    .end local v3           #t:Lcom/android/server/StatusBarManagerService$DisableRecord;
    :cond_34
    if-nez v4, :cond_47

    #@36
    .line 561
    new-instance v4, Lcom/android/server/StatusBarManagerService$DisableRecord;

    #@38
    .end local v4           #tok:Lcom/android/server/StatusBarManagerService$DisableRecord;
    const/4 v5, 0x0

    #@39
    invoke-direct {v4, p0, v5}, Lcom/android/server/StatusBarManagerService$DisableRecord;-><init>(Lcom/android/server/StatusBarManagerService;Lcom/android/server/StatusBarManagerService$1;)V

    #@3c
    .line 562
    .restart local v4       #tok:Lcom/android/server/StatusBarManagerService$DisableRecord;
    iput p1, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->userId:I

    #@3e
    .line 564
    const/4 v5, 0x0

    #@3f
    :try_start_3f
    invoke-interface {p3, v4, v5}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_42
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_42} :catch_4e

    #@42
    .line 569
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mDisableRecords:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@47
    .line 571
    :cond_47
    iput p2, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->what:I

    #@49
    .line 572
    iput-object p3, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->token:Landroid/os/IBinder;

    #@4b
    .line 573
    iput-object p4, v4, Lcom/android/server/StatusBarManagerService$DisableRecord;->pkg:Ljava/lang/String;

    #@4d
    goto :goto_30

    #@4e
    .line 566
    :catch_4e
    move-exception v1

    #@4f
    .line 567
    .local v1, ex:Landroid/os/RemoteException;
    goto :goto_30
.end method

.method public onClearAllNotifications()V
    .registers 2

    #@0
    .prologue
    .line 480
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBarService()V

    #@3
    .line 482
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@5
    invoke-interface {v0}, Lcom/android/server/StatusBarManagerService$NotificationCallbacks;->onClearAll()V

    #@8
    .line 483
    return-void
.end method

.method public onHardKeyboardStatusChange(ZZ)V
    .registers 5
    .parameter "available"
    .parameter "enabled"

    #@0
    .prologue
    .line 357
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/server/StatusBarManagerService$6;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/StatusBarManagerService$6;-><init>(Lcom/android/server/StatusBarManagerService;ZZ)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 367
    return-void
.end method

.method public onNotificationClear(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"

    #@0
    .prologue
    .line 474
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBarService()V

    #@3
    .line 476
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@5
    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/StatusBarManagerService$NotificationCallbacks;->onNotificationClear(Ljava/lang/String;Ljava/lang/String;I)V

    #@8
    .line 477
    return-void
.end method

.method public onNotificationClick(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"

    #@0
    .prologue
    .line 460
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBarService()V

    #@3
    .line 462
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@5
    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/StatusBarManagerService$NotificationCallbacks;->onNotificationClick(Ljava/lang/String;Ljava/lang/String;I)V

    #@8
    .line 463
    return-void
.end method

.method public onNotificationError(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .registers 14
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"
    .parameter "uid"
    .parameter "initialPid"
    .parameter "message"

    #@0
    .prologue
    .line 467
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBarService()V

    #@3
    .line 470
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move v3, p3

    #@8
    move v4, p4

    #@9
    move v5, p5

    #@a
    move-object v6, p6

    #@b
    invoke-interface/range {v0 .. v6}, Lcom/android/server/StatusBarManagerService$NotificationCallbacks;->onNotificationError(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    #@e
    .line 471
    return-void
.end method

.method public onPanelRevealed()V
    .registers 2

    #@0
    .prologue
    .line 453
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBarService()V

    #@3
    .line 456
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@5
    invoke-interface {v0}, Lcom/android/server/StatusBarManagerService$NotificationCallbacks;->onPanelRevealed()V

    #@8
    .line 457
    return-void
.end method

.method public preloadRecentApps()V
    .registers 2

    #@0
    .prologue
    .line 380
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 382
    :try_start_4
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@6
    invoke-interface {v0}, Lcom/android/internal/statusbar/IStatusBar;->preloadRecentApps()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 385
    :cond_9
    :goto_9
    return-void

    #@a
    .line 383
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public registerStatusBar(Lcom/android/internal/statusbar/IStatusBar;Lcom/android/internal/statusbar/StatusBarIconList;Ljava/util/List;Ljava/util/List;[ILjava/util/List;)V
    .registers 14
    .parameter "bar"
    .parameter "iconList"
    .parameter
    .parameter
    .parameter "switches"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/statusbar/IStatusBar;",
            "Lcom/android/internal/statusbar/StatusBarIconList;",
            "Ljava/util/List",
            "<",
            "Landroid/os/IBinder;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/statusbar/StatusBarNotification;",
            ">;[I",
            "Ljava/util/List",
            "<",
            "Landroid/os/IBinder;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, notificationKeys:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    .local p4, notifications:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/statusbar/StatusBarNotification;>;"
    .local p6, binders:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 423
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBarService()V

    #@5
    .line 425
    const-string v2, "StatusBarManagerService"

    #@7
    new-instance v5, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v6, "registerStatusBar bar="

    #@e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    invoke-static {v2, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 426
    iput-object p1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@1f
    .line 427
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@21
    monitor-enter v5

    #@22
    .line 428
    :try_start_22
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@24
    invoke-virtual {p2, v2}, Lcom/android/internal/statusbar/StatusBarIconList;->copyFrom(Lcom/android/internal/statusbar/StatusBarIconList;)V

    #@27
    .line 429
    monitor-exit v5
    :try_end_28
    .catchall {:try_start_22 .. :try_end_28} :catchall_53

    #@28
    .line 430
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@2a
    monitor-enter v5

    #@2b
    .line 431
    :try_start_2b
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@30
    move-result-object v2

    #@31
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@34
    move-result-object v1

    #@35
    .local v1, i$:Ljava/util/Iterator;
    :goto_35
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_56

    #@3b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, Ljava/util/Map$Entry;

    #@41
    .line 432
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@44
    move-result-object v2

    #@45
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@48
    .line 433
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@4b
    move-result-object v2

    #@4c
    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@4f
    goto :goto_35

    #@50
    .line 435
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_50
    move-exception v2

    #@51
    monitor-exit v5
    :try_end_52
    .catchall {:try_start_2b .. :try_end_52} :catchall_50

    #@52
    throw v2

    #@53
    .line 429
    :catchall_53
    move-exception v2

    #@54
    :try_start_54
    monitor-exit v5
    :try_end_55
    .catchall {:try_start_54 .. :try_end_55} :catchall_53

    #@55
    throw v2

    #@56
    .line 435
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_56
    :try_start_56
    monitor-exit v5
    :try_end_57
    .catchall {:try_start_56 .. :try_end_57} :catchall_50

    #@57
    .line 436
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@59
    monitor-enter v5

    #@5a
    .line 437
    const/4 v2, 0x0

    #@5b
    :try_start_5b
    iget v6, p0, Lcom/android/server/StatusBarManagerService;->mCurrentUserId:I

    #@5d
    invoke-virtual {p0, v6}, Lcom/android/server/StatusBarManagerService;->gatherDisableActionsLocked(I)I

    #@60
    move-result v6

    #@61
    aput v6, p5, v2

    #@63
    .line 438
    const/4 v2, 0x1

    #@64
    iget v6, p0, Lcom/android/server/StatusBarManagerService;->mSystemUiVisibility:I

    #@66
    aput v6, p5, v2

    #@68
    .line 439
    const/4 v6, 0x2

    #@69
    iget-boolean v2, p0, Lcom/android/server/StatusBarManagerService;->mMenuVisible:Z

    #@6b
    if-eqz v2, :cond_98

    #@6d
    move v2, v3

    #@6e
    :goto_6e
    aput v2, p5, v6

    #@70
    .line 440
    const/4 v2, 0x3

    #@71
    iget v6, p0, Lcom/android/server/StatusBarManagerService;->mImeWindowVis:I

    #@73
    aput v6, p5, v2

    #@75
    .line 441
    const/4 v2, 0x4

    #@76
    iget v6, p0, Lcom/android/server/StatusBarManagerService;->mImeBackDisposition:I

    #@78
    aput v6, p5, v2

    #@7a
    .line 442
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mImeToken:Landroid/os/IBinder;

    #@7c
    invoke-interface {p6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@7f
    .line 443
    monitor-exit v5
    :try_end_80
    .catchall {:try_start_5b .. :try_end_80} :catchall_9a

    #@80
    .line 444
    const/4 v5, 0x5

    #@81
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@83
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->isHardKeyboardAvailable()Z

    #@86
    move-result v2

    #@87
    if-eqz v2, :cond_9d

    #@89
    move v2, v3

    #@8a
    :goto_8a
    aput v2, p5, v5

    #@8c
    .line 445
    const/4 v2, 0x6

    #@8d
    iget-object v5, p0, Lcom/android/server/StatusBarManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@8f
    invoke-virtual {v5}, Lcom/android/server/wm/WindowManagerService;->isHardKeyboardEnabled()Z

    #@92
    move-result v5

    #@93
    if-eqz v5, :cond_9f

    #@95
    :goto_95
    aput v3, p5, v2

    #@97
    .line 446
    return-void

    #@98
    :cond_98
    move v2, v4

    #@99
    .line 439
    goto :goto_6e

    #@9a
    .line 443
    :catchall_9a
    move-exception v2

    #@9b
    :try_start_9b
    monitor-exit v5
    :try_end_9c
    .catchall {:try_start_9b .. :try_end_9c} :catchall_9a

    #@9c
    throw v2

    #@9d
    :cond_9d
    move v2, v4

    #@9e
    .line 444
    goto :goto_8a

    #@9f
    :cond_9f
    move v3, v4

    #@a0
    .line 445
    goto :goto_95
.end method

.method public removeIcon(Ljava/lang/String;)V
    .registers 7
    .parameter "slot"

    #@0
    .prologue
    .line 245
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBar()V

    #@3
    .line 247
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@5
    monitor-enter v2

    #@6
    .line 248
    :try_start_6
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@8
    invoke-virtual {v1, p1}, Lcom/android/internal/statusbar/StatusBarIconList;->getSlotIndex(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    .line 249
    .local v0, index:I
    if-gez v0, :cond_2a

    #@e
    .line 250
    new-instance v1, Ljava/lang/SecurityException;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "invalid status bar icon slot: "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-direct {v1, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 261
    .end local v0           #index:I
    :catchall_27
    move-exception v1

    #@28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_6 .. :try_end_29} :catchall_27

    #@29
    throw v1

    #@2a
    .line 253
    .restart local v0       #index:I
    :cond_2a
    :try_start_2a
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@2c
    invoke-virtual {v1, v0}, Lcom/android/internal/statusbar/StatusBarIconList;->removeIcon(I)V

    #@2f
    .line 255
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;
    :try_end_31
    .catchall {:try_start_2a .. :try_end_31} :catchall_27

    #@31
    if-eqz v1, :cond_38

    #@33
    .line 257
    :try_start_33
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@35
    invoke-interface {v1, v0}, Lcom/android/internal/statusbar/IStatusBar;->removeIcon(I)V
    :try_end_38
    .catchall {:try_start_33 .. :try_end_38} :catchall_27
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_38} :catch_3a

    #@38
    .line 261
    :cond_38
    :goto_38
    :try_start_38
    monitor-exit v2
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_27

    #@39
    .line 262
    return-void

    #@3a
    .line 258
    :catch_3a
    move-exception v1

    #@3b
    goto :goto_38
.end method

.method public removeNotification(Landroid/os/IBinder;)V
    .registers 7
    .parameter "key"

    #@0
    .prologue
    .line 518
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 519
    :try_start_3
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/internal/statusbar/StatusBarNotification;

    #@b
    .line 520
    .local v0, n:Lcom/android/internal/statusbar/StatusBarNotification;
    if-nez v0, :cond_27

    #@d
    .line 521
    const-string v1, "StatusBarManagerService"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "removeNotification key not found: "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 522
    monitor-exit v2

    #@26
    .line 531
    :goto_26
    return-void

    #@27
    .line 524
    :cond_27
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_32

    #@29
    if-eqz v1, :cond_30

    #@2b
    .line 526
    :try_start_2b
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@2d
    invoke-interface {v1, p1}, Lcom/android/internal/statusbar/IStatusBar;->removeNotification(Landroid/os/IBinder;)V
    :try_end_30
    .catchall {:try_start_2b .. :try_end_30} :catchall_32
    .catch Landroid/os/RemoteException; {:try_start_2b .. :try_end_30} :catch_35

    #@30
    .line 530
    :cond_30
    :goto_30
    :try_start_30
    monitor-exit v2

    #@31
    goto :goto_26

    #@32
    .end local v0           #n:Lcom/android/internal/statusbar/StatusBarNotification;
    :catchall_32
    move-exception v1

    #@33
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_30 .. :try_end_34} :catchall_32

    #@34
    throw v1

    #@35
    .line 527
    .restart local v0       #n:Lcom/android/internal/statusbar/StatusBarNotification;
    :catch_35
    move-exception v1

    #@36
    goto :goto_30
.end method

.method public setCurrentUser(I)V
    .registers 2
    .parameter "newUserId"

    #@0
    .prologue
    .line 399
    iput p1, p0, Lcom/android/server/StatusBarManagerService;->mCurrentUserId:I

    #@2
    .line 400
    return-void
.end method

.method public setHardKeyboardEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 348
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/server/StatusBarManagerService$5;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/server/StatusBarManagerService$5;-><init>(Lcom/android/server/StatusBarManagerService;Z)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 353
    return-void
.end method

.method public setIcon(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .registers 15
    .parameter "slot"
    .parameter "iconPackage"
    .parameter "iconId"
    .parameter "iconLevel"
    .parameter "contentDescription"

    #@0
    .prologue
    .line 194
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBar()V

    #@3
    .line 196
    iget-object v8, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@5
    monitor-enter v8

    #@6
    .line 197
    :try_start_6
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@8
    invoke-virtual {v1, p1}, Lcom/android/internal/statusbar/StatusBarIconList;->getSlotIndex(Ljava/lang/String;)I

    #@b
    move-result v7

    #@c
    .line 198
    .local v7, index:I
    if-gez v7, :cond_2a

    #@e
    .line 199
    new-instance v1, Ljava/lang/SecurityException;

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "invalid status bar icon slot: "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 214
    .end local v7           #index:I
    :catchall_27
    move-exception v1

    #@28
    monitor-exit v8
    :try_end_29
    .catchall {:try_start_6 .. :try_end_29} :catchall_27

    #@29
    throw v1

    #@2a
    .line 202
    .restart local v7       #index:I
    :cond_2a
    :try_start_2a
    new-instance v0, Lcom/android/internal/statusbar/StatusBarIcon;

    #@2c
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@2e
    const/4 v5, 0x0

    #@2f
    move-object v1, p2

    #@30
    move v3, p3

    #@31
    move v4, p4

    #@32
    move-object v6, p5

    #@33
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/statusbar/StatusBarIcon;-><init>(Ljava/lang/String;Landroid/os/UserHandle;IIILjava/lang/CharSequence;)V

    #@36
    .line 206
    .local v0, icon:Lcom/android/internal/statusbar/StatusBarIcon;
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@38
    invoke-virtual {v1, v7, v0}, Lcom/android/internal/statusbar/StatusBarIconList;->setIcon(ILcom/android/internal/statusbar/StatusBarIcon;)V

    #@3b
    .line 208
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;
    :try_end_3d
    .catchall {:try_start_2a .. :try_end_3d} :catchall_27

    #@3d
    if-eqz v1, :cond_44

    #@3f
    .line 210
    :try_start_3f
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@41
    invoke-interface {v1, v7, v0}, Lcom/android/internal/statusbar/IStatusBar;->setIcon(ILcom/android/internal/statusbar/StatusBarIcon;)V
    :try_end_44
    .catchall {:try_start_3f .. :try_end_44} :catchall_27
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_44} :catch_46

    #@44
    .line 214
    :cond_44
    :goto_44
    :try_start_44
    monitor-exit v8
    :try_end_45
    .catchall {:try_start_44 .. :try_end_45} :catchall_27

    #@45
    .line 215
    return-void

    #@46
    .line 211
    :catch_46
    move-exception v1

    #@47
    goto :goto_44
.end method

.method public setIconVisibility(Ljava/lang/String;Z)V
    .registers 9
    .parameter "slot"
    .parameter "visible"

    #@0
    .prologue
    .line 218
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBar()V

    #@3
    .line 220
    iget-object v3, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@5
    monitor-enter v3

    #@6
    .line 221
    :try_start_6
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@8
    invoke-virtual {v2, p1}, Lcom/android/internal/statusbar/StatusBarIconList;->getSlotIndex(Ljava/lang/String;)I

    #@b
    move-result v1

    #@c
    .line 222
    .local v1, index:I
    if-gez v1, :cond_2a

    #@e
    .line 223
    new-instance v2, Ljava/lang/SecurityException;

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "invalid status bar icon slot: "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-direct {v2, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@26
    throw v2

    #@27
    .line 241
    .end local v1           #index:I
    :catchall_27
    move-exception v2

    #@28
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_6 .. :try_end_29} :catchall_27

    #@29
    throw v2

    #@2a
    .line 226
    .restart local v1       #index:I
    :cond_2a
    :try_start_2a
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mIcons:Lcom/android/internal/statusbar/StatusBarIconList;

    #@2c
    invoke-virtual {v2, v1}, Lcom/android/internal/statusbar/StatusBarIconList;->getIcon(I)Lcom/android/internal/statusbar/StatusBarIcon;

    #@2f
    move-result-object v0

    #@30
    .line 227
    .local v0, icon:Lcom/android/internal/statusbar/StatusBarIcon;
    if-nez v0, :cond_34

    #@32
    .line 228
    monitor-exit v3

    #@33
    .line 242
    :goto_33
    return-void

    #@34
    .line 231
    :cond_34
    iget-boolean v2, v0, Lcom/android/internal/statusbar/StatusBarIcon;->visible:Z

    #@36
    if-eq v2, p2, :cond_43

    #@38
    .line 232
    iput-boolean p2, v0, Lcom/android/internal/statusbar/StatusBarIcon;->visible:Z

    #@3a
    .line 234
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;
    :try_end_3c
    .catchall {:try_start_2a .. :try_end_3c} :catchall_27

    #@3c
    if-eqz v2, :cond_43

    #@3e
    .line 236
    :try_start_3e
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@40
    invoke-interface {v2, v1, v0}, Lcom/android/internal/statusbar/IStatusBar;->setIcon(ILcom/android/internal/statusbar/StatusBarIcon;)V
    :try_end_43
    .catchall {:try_start_3e .. :try_end_43} :catchall_27
    .catch Landroid/os/RemoteException; {:try_start_3e .. :try_end_43} :catch_45

    #@43
    .line 241
    :cond_43
    :goto_43
    :try_start_43
    monitor-exit v3
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_27

    #@44
    goto :goto_33

    #@45
    .line 237
    :catch_45
    move-exception v2

    #@46
    goto :goto_43
.end method

.method public setImeWindowStatus(Landroid/os/IBinder;II)V
    .registers 7
    .parameter "token"
    .parameter "vis"
    .parameter "backDisposition"

    #@0
    .prologue
    .line 289
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBar()V

    #@3
    .line 295
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 299
    :try_start_6
    iput p2, p0, Lcom/android/server/StatusBarManagerService;->mImeWindowVis:I

    #@8
    .line 300
    iput p3, p0, Lcom/android/server/StatusBarManagerService;->mImeBackDisposition:I

    #@a
    .line 301
    iput-object p1, p0, Lcom/android/server/StatusBarManagerService;->mImeToken:Landroid/os/IBinder;

    #@c
    .line 302
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mHandler:Landroid/os/Handler;

    #@e
    new-instance v2, Lcom/android/server/StatusBarManagerService$3;

    #@10
    invoke-direct {v2, p0, p1, p2, p3}, Lcom/android/server/StatusBarManagerService$3;-><init>(Lcom/android/server/StatusBarManagerService;Landroid/os/IBinder;II)V

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@16
    .line 312
    monitor-exit v1

    #@17
    .line 313
    return-void

    #@18
    .line 312
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_6 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public setNavigationBackground(Ljava/lang/String;IIII)V
    .registers 12
    .parameter "pkg"
    .parameter "portResId"
    .parameter "landResId"
    .parameter "alpha"
    .parameter "reserved"

    #@0
    .prologue
    .line 678
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 680
    :try_start_4
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@6
    move-object v1, p1

    #@7
    move v2, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    move v5, p5

    #@b
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/statusbar/IStatusBar;->setNavigationBackground(Ljava/lang/String;IIII)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_e} :catch_f

    #@e
    .line 683
    :cond_e
    :goto_e
    return-void

    #@f
    .line 681
    :catch_f
    move-exception v0

    #@10
    goto :goto_e
.end method

.method public setNavigationRotation(III)V
    .registers 5
    .parameter "rotation"
    .parameter "direction"
    .parameter "duration"

    #@0
    .prologue
    .line 686
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 688
    :try_start_4
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@6
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/statusbar/IStatusBar;->setNavigationRotation(III)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 691
    :cond_9
    :goto_9
    return-void

    #@a
    .line 689
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public setNotificationCallbacks(Lcom/android/server/StatusBarManagerService$NotificationCallbacks;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 116
    iput-object p1, p0, Lcom/android/server/StatusBarManagerService;->mNotificationCallbacks:Lcom/android/server/StatusBarManagerService$NotificationCallbacks;

    #@2
    .line 117
    return-void
.end method

.method public setSystemBarType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 671
    :try_start_4
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@6
    invoke-interface {v0, p1}, Lcom/android/internal/statusbar/IStatusBar;->setSystemBarType(I)V

    #@9
    .line 672
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/server/wm/WindowManagerService;->setSystemBarType(I)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_e} :catch_f

    #@e
    .line 675
    :cond_e
    :goto_e
    return-void

    #@f
    .line 673
    :catch_f
    move-exception v0

    #@10
    goto :goto_e
.end method

.method public setSystemUiVisibility(II)V
    .registers 8
    .parameter "vis"
    .parameter "mask"

    #@0
    .prologue
    .line 317
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBarService()V

    #@3
    .line 321
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 322
    :try_start_6
    invoke-direct {p0, p1, p2}, Lcom/android/server/StatusBarManagerService;->updateUiVisibilityLocked(II)V

    #@9
    .line 323
    iget v0, p0, Lcom/android/server/StatusBarManagerService;->mCurrentUserId:I

    #@b
    const/high16 v2, 0x7ff

    #@d
    and-int/2addr v2, p1

    #@e
    iget-object v3, p0, Lcom/android/server/StatusBarManagerService;->mSysUiVisToken:Landroid/os/IBinder;

    #@10
    const-string v4, "WindowManager.LayoutParams"

    #@12
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/android/server/StatusBarManagerService;->disableLocked(IILandroid/os/IBinder;Ljava/lang/String;)V

    #@15
    .line 328
    monitor-exit v1

    #@16
    .line 329
    return-void

    #@17
    .line 328
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_6 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public toggleRecentApps()V
    .registers 2

    #@0
    .prologue
    .line 371
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 373
    :try_start_4
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@6
    invoke-interface {v0}, Lcom/android/internal/statusbar/IStatusBar;->toggleRecentApps()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 376
    :cond_9
    :goto_9
    return-void

    #@a
    .line 374
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public topAppWindowChanged(Z)V
    .registers 5
    .parameter "menuVisible"

    #@0
    .prologue
    .line 269
    invoke-direct {p0}, Lcom/android/server/StatusBarManagerService;->enforceStatusBar()V

    #@3
    .line 273
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 274
    :try_start_6
    iput-boolean p1, p0, Lcom/android/server/StatusBarManagerService;->mMenuVisible:Z

    #@8
    .line 275
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mHandler:Landroid/os/Handler;

    #@a
    new-instance v2, Lcom/android/server/StatusBarManagerService$2;

    #@c
    invoke-direct {v2, p0, p1}, Lcom/android/server/StatusBarManagerService$2;-><init>(Lcom/android/server/StatusBarManagerService;Z)V

    #@f
    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@12
    .line 285
    monitor-exit v1

    #@13
    .line 286
    return-void

    #@14
    .line 285
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_6 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public updateNotification(Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;)V
    .registers 7
    .parameter "key"
    .parameter "notification"

    #@0
    .prologue
    .line 503
    iget-object v1, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 504
    :try_start_3
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_27

    #@b
    .line 505
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "updateNotification key not found: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 514
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v0

    #@27
    .line 507
    :cond_27
    :try_start_27
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mNotifications:Ljava/util/HashMap;

    #@29
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 508
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;
    :try_end_2e
    .catchall {:try_start_27 .. :try_end_2e} :catchall_24

    #@2e
    if-eqz v0, :cond_35

    #@30
    .line 510
    :try_start_30
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@32
    invoke-interface {v0, p1, p2}, Lcom/android/internal/statusbar/IStatusBar;->updateNotification(Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;)V
    :try_end_35
    .catchall {:try_start_30 .. :try_end_35} :catchall_24
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_35} :catch_37

    #@35
    .line 514
    :cond_35
    :goto_35
    :try_start_35
    monitor-exit v1
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_24

    #@36
    .line 515
    return-void

    #@37
    .line 511
    :catch_37
    move-exception v0

    #@38
    goto :goto_35
.end method
