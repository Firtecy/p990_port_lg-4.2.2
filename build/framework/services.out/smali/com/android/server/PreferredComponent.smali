.class public Lcom/android/server/PreferredComponent;
.super Ljava/lang/Object;
.source "PreferredComponent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/PreferredComponent$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/android/server/PreferredComponent$Callbacks;

.field public final mComponent:Landroid/content/ComponentName;

.field public final mMatch:I

.field private mParseError:Ljava/lang/String;

.field private final mSetClasses:[Ljava/lang/String;

.field private final mSetComponents:[Ljava/lang/String;

.field private final mSetPackages:[Ljava/lang/String;

.field private final mShortComponent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/server/PreferredComponent$Callbacks;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V
    .registers 13
    .parameter "callbacks"
    .parameter "match"
    .parameter "set"
    .parameter "component"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 54
    iput-object p1, p0, Lcom/android/server/PreferredComponent;->mCallbacks:Lcom/android/server/PreferredComponent$Callbacks;

    #@6
    .line 55
    const/high16 v6, 0xfff

    #@8
    and-int/2addr v6, p2

    #@9
    iput v6, p0, Lcom/android/server/PreferredComponent;->mMatch:I

    #@b
    .line 56
    iput-object p4, p0, Lcom/android/server/PreferredComponent;->mComponent:Landroid/content/ComponentName;

    #@d
    .line 57
    invoke-virtual {p4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@10
    move-result-object v6

    #@11
    iput-object v6, p0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@13
    .line 58
    iput-object v7, p0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@15
    .line 59
    if-eqz p3, :cond_54

    #@17
    .line 60
    array-length v0, p3

    #@18
    .line 61
    .local v0, N:I
    new-array v5, v0, [Ljava/lang/String;

    #@1a
    .line 62
    .local v5, myPackages:[Ljava/lang/String;
    new-array v3, v0, [Ljava/lang/String;

    #@1c
    .line 63
    .local v3, myClasses:[Ljava/lang/String;
    new-array v4, v0, [Ljava/lang/String;

    #@1e
    .line 64
    .local v4, myComponents:[Ljava/lang/String;
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    if-ge v2, v0, :cond_4d

    #@21
    .line 65
    aget-object v1, p3, v2

    #@23
    .line 66
    .local v1, cn:Landroid/content/ComponentName;
    if-nez v1, :cond_2c

    #@25
    .line 67
    iput-object v7, p0, Lcom/android/server/PreferredComponent;->mSetPackages:[Ljava/lang/String;

    #@27
    .line 68
    iput-object v7, p0, Lcom/android/server/PreferredComponent;->mSetClasses:[Ljava/lang/String;

    #@29
    .line 69
    iput-object v7, p0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@2b
    .line 84
    .end local v0           #N:I
    .end local v1           #cn:Landroid/content/ComponentName;
    .end local v2           #i:I
    .end local v3           #myClasses:[Ljava/lang/String;
    .end local v4           #myComponents:[Ljava/lang/String;
    .end local v5           #myPackages:[Ljava/lang/String;
    :goto_2b
    return-void

    #@2c
    .line 72
    .restart local v0       #N:I
    .restart local v1       #cn:Landroid/content/ComponentName;
    .restart local v2       #i:I
    .restart local v3       #myClasses:[Ljava/lang/String;
    .restart local v4       #myComponents:[Ljava/lang/String;
    .restart local v5       #myPackages:[Ljava/lang/String;
    :cond_2c
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@33
    move-result-object v6

    #@34
    aput-object v6, v5, v2

    #@36
    .line 73
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    aput-object v6, v3, v2

    #@40
    .line 74
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    aput-object v6, v4, v2

    #@4a
    .line 64
    add-int/lit8 v2, v2, 0x1

    #@4c
    goto :goto_1f

    #@4d
    .line 76
    .end local v1           #cn:Landroid/content/ComponentName;
    :cond_4d
    iput-object v5, p0, Lcom/android/server/PreferredComponent;->mSetPackages:[Ljava/lang/String;

    #@4f
    .line 77
    iput-object v3, p0, Lcom/android/server/PreferredComponent;->mSetClasses:[Ljava/lang/String;

    #@51
    .line 78
    iput-object v4, p0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@53
    goto :goto_2b

    #@54
    .line 80
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #myClasses:[Ljava/lang/String;
    .end local v4           #myComponents:[Ljava/lang/String;
    .end local v5           #myPackages:[Ljava/lang/String;
    :cond_54
    iput-object v7, p0, Lcom/android/server/PreferredComponent;->mSetPackages:[Ljava/lang/String;

    #@56
    .line 81
    iput-object v7, p0, Lcom/android/server/PreferredComponent;->mSetClasses:[Ljava/lang/String;

    #@58
    .line 82
    iput-object v7, p0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@5a
    goto :goto_2b
.end method

.method public constructor <init>(Lcom/android/server/PreferredComponent$Callbacks;Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 20
    .parameter "callbacks"
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    move-object/from16 v0, p1

    #@5
    move-object/from16 v1, p0

    #@7
    iput-object v0, v1, Lcom/android/server/PreferredComponent;->mCallbacks:Lcom/android/server/PreferredComponent$Callbacks;

    #@9
    .line 89
    const/4 v14, 0x0

    #@a
    const-string v15, "name"

    #@c
    move-object/from16 v0, p2

    #@e
    invoke-interface {v0, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v14

    #@12
    move-object/from16 v0, p0

    #@14
    iput-object v14, v0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@16
    .line 90
    move-object/from16 v0, p0

    #@18
    iget-object v14, v0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@1a
    invoke-static {v14}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@1d
    move-result-object v14

    #@1e
    move-object/from16 v0, p0

    #@20
    iput-object v14, v0, Lcom/android/server/PreferredComponent;->mComponent:Landroid/content/ComponentName;

    #@22
    .line 91
    move-object/from16 v0, p0

    #@24
    iget-object v14, v0, Lcom/android/server/PreferredComponent;->mComponent:Landroid/content/ComponentName;

    #@26
    if-nez v14, :cond_43

    #@28
    .line 92
    new-instance v14, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v15, "Bad activity name "

    #@2f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v14

    #@33
    move-object/from16 v0, p0

    #@35
    iget-object v15, v0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@37
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v14

    #@3b
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v14

    #@3f
    move-object/from16 v0, p0

    #@41
    iput-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@43
    .line 94
    :cond_43
    const/4 v14, 0x0

    #@44
    const-string v15, "match"

    #@46
    move-object/from16 v0, p2

    #@48
    invoke-interface {v0, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    .line 95
    .local v3, matchStr:Ljava/lang/String;
    if-eqz v3, :cond_ca

    #@4e
    const/16 v14, 0x10

    #@50
    invoke-static {v3, v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@53
    move-result v14

    #@54
    :goto_54
    move-object/from16 v0, p0

    #@56
    iput v14, v0, Lcom/android/server/PreferredComponent;->mMatch:I

    #@58
    .line 96
    const/4 v14, 0x0

    #@59
    const-string v15, "set"

    #@5b
    move-object/from16 v0, p2

    #@5d
    invoke-interface {v0, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@60
    move-result-object v10

    #@61
    .line 97
    .local v10, setCountStr:Ljava/lang/String;
    if-eqz v10, :cond_cc

    #@63
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@66
    move-result v9

    #@67
    .line 99
    .local v9, setCount:I
    :goto_67
    if-lez v9, :cond_ce

    #@69
    new-array v6, v9, [Ljava/lang/String;

    #@6b
    .line 100
    .local v6, myPackages:[Ljava/lang/String;
    :goto_6b
    if-lez v9, :cond_d0

    #@6d
    new-array v4, v9, [Ljava/lang/String;

    #@6f
    .line 101
    .local v4, myClasses:[Ljava/lang/String;
    :goto_6f
    if-lez v9, :cond_d2

    #@71
    new-array v5, v9, [Ljava/lang/String;

    #@73
    .line 103
    .local v5, myComponents:[Ljava/lang/String;
    :goto_73
    const/4 v11, 0x0

    #@74
    .line 105
    .local v11, setPos:I
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@77
    move-result v8

    #@78
    .line 108
    .local v8, outerDepth:I
    :cond_78
    :goto_78
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7b
    move-result v13

    #@7c
    .local v13, type:I
    const/4 v14, 0x1

    #@7d
    if-eq v13, v14, :cond_168

    #@7f
    const/4 v14, 0x3

    #@80
    if-ne v13, v14, :cond_88

    #@82
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@85
    move-result v14

    #@86
    if-le v14, v8, :cond_168

    #@88
    .line 110
    :cond_88
    const/4 v14, 0x3

    #@89
    if-eq v13, v14, :cond_78

    #@8b
    const/4 v14, 0x4

    #@8c
    if-eq v13, v14, :cond_78

    #@8e
    .line 115
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@91
    move-result-object v12

    #@92
    .line 118
    .local v12, tagName:Ljava/lang/String;
    const-string v14, "set"

    #@94
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@97
    move-result v14

    #@98
    if-eqz v14, :cond_13b

    #@9a
    .line 119
    const/4 v14, 0x0

    #@9b
    const-string v15, "name"

    #@9d
    move-object/from16 v0, p2

    #@9f
    invoke-interface {v0, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a2
    move-result-object v7

    #@a3
    .line 120
    .local v7, name:Ljava/lang/String;
    if-nez v7, :cond_d4

    #@a5
    .line 121
    move-object/from16 v0, p0

    #@a7
    iget-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@a9
    if-nez v14, :cond_c6

    #@ab
    .line 122
    new-instance v14, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v15, "No name in set tag in preferred activity "

    #@b2
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v14

    #@b6
    move-object/from16 v0, p0

    #@b8
    iget-object v15, v0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@ba
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v14

    #@be
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v14

    #@c2
    move-object/from16 v0, p0

    #@c4
    iput-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@c6
    .line 144
    :cond_c6
    :goto_c6
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@c9
    goto :goto_78

    #@ca
    .line 95
    .end local v4           #myClasses:[Ljava/lang/String;
    .end local v5           #myComponents:[Ljava/lang/String;
    .end local v6           #myPackages:[Ljava/lang/String;
    .end local v7           #name:Ljava/lang/String;
    .end local v8           #outerDepth:I
    .end local v9           #setCount:I
    .end local v10           #setCountStr:Ljava/lang/String;
    .end local v11           #setPos:I
    .end local v12           #tagName:Ljava/lang/String;
    .end local v13           #type:I
    :cond_ca
    const/4 v14, 0x0

    #@cb
    goto :goto_54

    #@cc
    .line 97
    .restart local v10       #setCountStr:Ljava/lang/String;
    :cond_cc
    const/4 v9, 0x0

    #@cd
    goto :goto_67

    #@ce
    .line 99
    .restart local v9       #setCount:I
    :cond_ce
    const/4 v6, 0x0

    #@cf
    goto :goto_6b

    #@d0
    .line 100
    .restart local v6       #myPackages:[Ljava/lang/String;
    :cond_d0
    const/4 v4, 0x0

    #@d1
    goto :goto_6f

    #@d2
    .line 101
    .restart local v4       #myClasses:[Ljava/lang/String;
    :cond_d2
    const/4 v5, 0x0

    #@d3
    goto :goto_73

    #@d4
    .line 125
    .restart local v5       #myComponents:[Ljava/lang/String;
    .restart local v7       #name:Ljava/lang/String;
    .restart local v8       #outerDepth:I
    .restart local v11       #setPos:I
    .restart local v12       #tagName:Ljava/lang/String;
    .restart local v13       #type:I
    :cond_d4
    if-lt v11, v9, :cond_f8

    #@d6
    .line 126
    move-object/from16 v0, p0

    #@d8
    iget-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@da
    if-nez v14, :cond_c6

    #@dc
    .line 127
    new-instance v14, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v15, "Too many set tags in preferred activity "

    #@e3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v14

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget-object v15, v0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@eb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v14

    #@ef
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v14

    #@f3
    move-object/from16 v0, p0

    #@f5
    iput-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@f7
    goto :goto_c6

    #@f8
    .line 131
    :cond_f8
    invoke-static {v7}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@fb
    move-result-object v2

    #@fc
    .line 132
    .local v2, cn:Landroid/content/ComponentName;
    if-nez v2, :cond_12a

    #@fe
    .line 133
    move-object/from16 v0, p0

    #@100
    iget-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@102
    if-nez v14, :cond_c6

    #@104
    .line 134
    new-instance v14, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v15, "Bad set name "

    #@10b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v14

    #@10f
    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v14

    #@113
    const-string v15, " in preferred activity "

    #@115
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v14

    #@119
    move-object/from16 v0, p0

    #@11b
    iget-object v15, v0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@11d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v14

    #@121
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v14

    #@125
    move-object/from16 v0, p0

    #@127
    iput-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@129
    goto :goto_c6

    #@12a
    .line 138
    :cond_12a
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@12d
    move-result-object v14

    #@12e
    aput-object v14, v6, v11

    #@130
    .line 139
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@133
    move-result-object v14

    #@134
    aput-object v14, v4, v11

    #@136
    .line 140
    aput-object v7, v5, v11

    #@138
    .line 141
    add-int/lit8 v11, v11, 0x1

    #@13a
    goto :goto_c6

    #@13b
    .line 145
    .end local v2           #cn:Landroid/content/ComponentName;
    .end local v7           #name:Ljava/lang/String;
    :cond_13b
    move-object/from16 v0, p0

    #@13d
    iget-object v14, v0, Lcom/android/server/PreferredComponent;->mCallbacks:Lcom/android/server/PreferredComponent$Callbacks;

    #@13f
    move-object/from16 v0, p2

    #@141
    invoke-interface {v14, v12, v0}, Lcom/android/server/PreferredComponent$Callbacks;->onReadTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Z

    #@144
    move-result v14

    #@145
    if-nez v14, :cond_78

    #@147
    .line 146
    const-string v14, "PreferredComponent"

    #@149
    new-instance v15, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v16, "Unknown element: "

    #@150
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v15

    #@154
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@157
    move-result-object v16

    #@158
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v15

    #@15c
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15f
    move-result-object v15

    #@160
    invoke-static {v14, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@163
    .line 147
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@166
    goto/16 :goto_78

    #@168
    .line 151
    .end local v12           #tagName:Ljava/lang/String;
    :cond_168
    if-eq v11, v9, :cond_19f

    #@16a
    .line 152
    move-object/from16 v0, p0

    #@16c
    iget-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@16e
    if-nez v14, :cond_19f

    #@170
    .line 153
    new-instance v14, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    const-string v15, "Not enough set tags (expected "

    #@177
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v14

    #@17b
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v14

    #@17f
    const-string v15, " but found "

    #@181
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    move-result-object v14

    #@185
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@188
    move-result-object v14

    #@189
    const-string v15, ") in "

    #@18b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v14

    #@18f
    move-object/from16 v0, p0

    #@191
    iget-object v15, v0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@193
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    move-result-object v14

    #@197
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19a
    move-result-object v14

    #@19b
    move-object/from16 v0, p0

    #@19d
    iput-object v14, v0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@19f
    .line 158
    :cond_19f
    move-object/from16 v0, p0

    #@1a1
    iput-object v6, v0, Lcom/android/server/PreferredComponent;->mSetPackages:[Ljava/lang/String;

    #@1a3
    .line 159
    move-object/from16 v0, p0

    #@1a5
    iput-object v4, v0, Lcom/android/server/PreferredComponent;->mSetClasses:[Ljava/lang/String;

    #@1a7
    .line 160
    move-object/from16 v0, p0

    #@1a9
    iput-object v5, v0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@1ab
    .line 161
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/Object;)V
    .registers 6
    .parameter "out"
    .parameter "prefix"
    .parameter "ident"

    #@0
    .prologue
    .line 205
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    invoke-static {p3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@6
    move-result v1

    #@7
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e
    .line 207
    const/16 v1, 0x20

    #@10
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    #@13
    .line 208
    iget-object v1, p0, Lcom/android/server/PreferredComponent;->mComponent:Landroid/content/ComponentName;

    #@15
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c
    .line 209
    const-string v1, " match=0x"

    #@1e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    .line 210
    iget v1, p0, Lcom/android/server/PreferredComponent;->mMatch:I

    #@23
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a
    .line 211
    iget-object v1, p0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@2c
    if-eqz v1, :cond_4e

    #@2e
    .line 212
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31
    const-string v1, "  Selected from:"

    #@33
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@36
    .line 213
    const/4 v0, 0x0

    #@37
    .local v0, i:I
    :goto_37
    iget-object v1, p0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@39
    array-length v1, v1

    #@3a
    if-ge v0, v1, :cond_4e

    #@3c
    .line 214
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    const-string v1, "    "

    #@41
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@44
    .line 215
    iget-object v1, p0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@46
    aget-object v1, v1, v0

    #@48
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4b
    .line 213
    add-int/lit8 v0, v0, 0x1

    #@4d
    goto :goto_37

    #@4e
    .line 218
    .end local v0           #i:I
    :cond_4e
    return-void
.end method

.method public getParseError()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/server/PreferredComponent;->mParseError:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public sameSet(Ljava/util/List;I)Z
    .registers 14
    .parameter
    .parameter "priority"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;I)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, query:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v8, 0x0

    #@1
    .line 182
    iget-object v9, p0, Lcom/android/server/PreferredComponent;->mSetPackages:[Ljava/lang/String;

    #@3
    if-nez v9, :cond_6

    #@5
    .line 201
    :cond_5
    :goto_5
    return v8

    #@6
    .line 183
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@9
    move-result v0

    #@a
    .line 184
    .local v0, NQ:I
    iget-object v9, p0, Lcom/android/server/PreferredComponent;->mSetPackages:[Ljava/lang/String;

    #@c
    array-length v1, v9

    #@d
    .line 185
    .local v1, NS:I
    const/4 v6, 0x0

    #@e
    .line 186
    .local v6, numMatch:I
    const/4 v4, 0x0

    #@f
    .local v4, i:I
    :goto_f
    if-ge v4, v0, :cond_45

    #@11
    .line 187
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v7

    #@15
    check-cast v7, Landroid/content/pm/ResolveInfo;

    #@17
    .line 188
    .local v7, ri:Landroid/content/pm/ResolveInfo;
    iget v9, v7, Landroid/content/pm/ResolveInfo;->priority:I

    #@19
    if-eq v9, p2, :cond_1e

    #@1b
    .line 186
    :cond_1b
    add-int/lit8 v4, v4, 0x1

    #@1d
    goto :goto_f

    #@1e
    .line 189
    :cond_1e
    iget-object v2, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@20
    .line 190
    .local v2, ai:Landroid/content/pm/ActivityInfo;
    const/4 v3, 0x0

    #@21
    .line 191
    .local v3, good:Z
    const/4 v5, 0x0

    #@22
    .local v5, j:I
    :goto_22
    if-ge v5, v1, :cond_3f

    #@24
    .line 192
    iget-object v9, p0, Lcom/android/server/PreferredComponent;->mSetPackages:[Ljava/lang/String;

    #@26
    aget-object v9, v9, v5

    #@28
    iget-object v10, v2, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@2a
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v9

    #@2e
    if-eqz v9, :cond_42

    #@30
    iget-object v9, p0, Lcom/android/server/PreferredComponent;->mSetClasses:[Ljava/lang/String;

    #@32
    aget-object v9, v9, v5

    #@34
    iget-object v10, v2, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@36
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v9

    #@3a
    if-eqz v9, :cond_42

    #@3c
    .line 194
    add-int/lit8 v6, v6, 0x1

    #@3e
    .line 195
    const/4 v3, 0x1

    #@3f
    .line 199
    :cond_3f
    if-nez v3, :cond_1b

    #@41
    goto :goto_5

    #@42
    .line 191
    :cond_42
    add-int/lit8 v5, v5, 0x1

    #@44
    goto :goto_22

    #@45
    .line 201
    .end local v2           #ai:Landroid/content/pm/ActivityInfo;
    .end local v3           #good:Z
    .end local v5           #j:I
    .end local v7           #ri:Landroid/content/pm/ResolveInfo;
    :cond_45
    if-ne v6, v1, :cond_5

    #@47
    const/4 v8, 0x1

    #@48
    goto :goto_5
.end method

.method public writeToXml(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 7
    .parameter "serializer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 168
    iget-object v2, p0, Lcom/android/server/PreferredComponent;->mSetClasses:[Ljava/lang/String;

    #@3
    if-eqz v2, :cond_40

    #@5
    iget-object v2, p0, Lcom/android/server/PreferredComponent;->mSetClasses:[Ljava/lang/String;

    #@7
    array-length v0, v2

    #@8
    .line 169
    .local v0, NS:I
    :goto_8
    const-string v2, "name"

    #@a
    iget-object v3, p0, Lcom/android/server/PreferredComponent;->mShortComponent:Ljava/lang/String;

    #@c
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f
    .line 170
    iget v2, p0, Lcom/android/server/PreferredComponent;->mMatch:I

    #@11
    if-eqz v2, :cond_1e

    #@13
    .line 171
    const-string v2, "match"

    #@15
    iget v3, p0, Lcom/android/server/PreferredComponent;->mMatch:I

    #@17
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1e
    .line 173
    :cond_1e
    const-string v2, "set"

    #@20
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@27
    .line 174
    const/4 v1, 0x0

    #@28
    .local v1, s:I
    :goto_28
    if-ge v1, v0, :cond_42

    #@2a
    .line 175
    const-string v2, "set"

    #@2c
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2f
    .line 176
    const-string v2, "name"

    #@31
    iget-object v3, p0, Lcom/android/server/PreferredComponent;->mSetComponents:[Ljava/lang/String;

    #@33
    aget-object v3, v3, v1

    #@35
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@38
    .line 177
    const-string v2, "set"

    #@3a
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3d
    .line 174
    add-int/lit8 v1, v1, 0x1

    #@3f
    goto :goto_28

    #@40
    .line 168
    .end local v0           #NS:I
    .end local v1           #s:I
    :cond_40
    const/4 v0, 0x0

    #@41
    goto :goto_8

    #@42
    .line 179
    .restart local v0       #NS:I
    .restart local v1       #s:I
    :cond_42
    return-void
.end method
