.class public Lcom/android/server/PackageManagerBackupAgent;
.super Landroid/app/backup/BackupAgent;
.source "PackageManagerBackupAgent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/PackageManagerBackupAgent$Metadata;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final GLOBAL_METADATA_KEY:Ljava/lang/String; = "@meta@"

.field private static final TAG:Ljava/lang/String; = "PMBA"


# instance fields
.field private mAllPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mExisting:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHasMetadata:Z

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mRestoredSignatures:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/PackageManagerBackupAgent$Metadata;",
            ">;"
        }
    .end annotation
.end field

.field private mStateVersions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/PackageManagerBackupAgent$Metadata;",
            ">;"
        }
    .end annotation
.end field

.field private mStoredIncrementalVersion:Ljava/lang/String;

.field private mStoredSdkVersion:I


# direct methods
.method constructor <init>(Landroid/content/pm/PackageManager;Ljava/util/List;)V
    .registers 4
    .parameter "packageMgr"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 85
    .local p2, packages:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-direct {p0}, Landroid/app/backup/BackupAgent;-><init>()V

    #@3
    .line 66
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mStateVersions:Ljava/util/HashMap;

    #@a
    .line 68
    new-instance v0, Ljava/util/HashSet;

    #@c
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@11
    .line 86
    iput-object p1, p0, Lcom/android/server/PackageManagerBackupAgent;->mPackageManager:Landroid/content/pm/PackageManager;

    #@13
    .line 87
    iput-object p2, p0, Lcom/android/server/PackageManagerBackupAgent;->mAllPackages:Ljava/util/List;

    #@15
    .line 88
    const/4 v0, 0x0

    #@16
    iput-object v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mRestoredSignatures:Ljava/util/HashMap;

    #@18
    .line 89
    const/4 v0, 0x0

    #@19
    iput-boolean v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mHasMetadata:Z

    #@1b
    .line 90
    return-void
.end method

.method private parseStateFile(Landroid/os/ParcelFileDescriptor;)V
    .registers 12
    .parameter "stateFile"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 364
    iget-object v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@3
    invoke-virtual {v7}, Ljava/util/HashSet;->clear()V

    #@6
    .line 365
    iget-object v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mStateVersions:Ljava/util/HashMap;

    #@8
    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    #@b
    .line 366
    const/4 v7, 0x0

    #@c
    iput v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mStoredSdkVersion:I

    #@e
    .line 367
    iput-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mStoredIncrementalVersion:Ljava/lang/String;

    #@10
    .line 373
    new-instance v4, Ljava/io/FileInputStream;

    #@12
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@15
    move-result-object v7

    #@16
    invoke-direct {v4, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@19
    .line 374
    .local v4, instream:Ljava/io/FileInputStream;
    new-instance v3, Ljava/io/DataInputStream;

    #@1b
    invoke-direct {v3, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@1e
    .line 376
    .local v3, in:Ljava/io/DataInputStream;
    const/16 v1, 0x100

    #@20
    .line 377
    .local v1, bufSize:I
    new-array v0, v1, [B

    #@22
    .line 379
    .local v0, buf:[B
    :try_start_22
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 380
    .local v5, pkg:Ljava/lang/String;
    const-string v7, "@meta@"

    #@28
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v7

    #@2c
    if-eqz v7, :cond_5c

    #@2e
    .line 381
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@31
    move-result v7

    #@32
    iput v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mStoredSdkVersion:I

    #@34
    .line 382
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    iput-object v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mStoredIncrementalVersion:Ljava/lang/String;

    #@3a
    .line 383
    iget-object v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@3c
    const-string v8, "@meta@"

    #@3e
    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@41
    .line 391
    :goto_41
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    .line 392
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@48
    move-result v6

    #@49
    .line 393
    .local v6, versionCode:I
    iget-object v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@4b
    invoke-virtual {v7, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@4e
    .line 394
    iget-object v7, p0, Lcom/android/server/PackageManagerBackupAgent;->mStateVersions:Ljava/util/HashMap;

    #@50
    new-instance v8, Lcom/android/server/PackageManagerBackupAgent$Metadata;

    #@52
    const/4 v9, 0x0

    #@53
    invoke-direct {v8, p0, v6, v9}, Lcom/android/server/PackageManagerBackupAgent$Metadata;-><init>(Lcom/android/server/PackageManagerBackupAgent;I[Landroid/content/pm/Signature;)V

    #@56
    invoke-virtual {v7, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@59
    goto :goto_41

    #@5a
    .line 396
    .end local v5           #pkg:Ljava/lang/String;
    .end local v6           #versionCode:I
    :catch_5a
    move-exception v7

    #@5b
    .line 402
    :goto_5b
    return-void

    #@5c
    .line 385
    .restart local v5       #pkg:Ljava/lang/String;
    :cond_5c
    const-string v7, "PMBA"

    #@5e
    const-string v8, "No global metadata in state file!"

    #@60
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_63
    .catch Ljava/io/EOFException; {:try_start_22 .. :try_end_63} :catch_5a
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_63} :catch_64

    #@63
    goto :goto_5b

    #@64
    .line 398
    .end local v5           #pkg:Ljava/lang/String;
    :catch_64
    move-exception v2

    #@65
    .line 400
    .local v2, e:Ljava/io/IOException;
    const-string v7, "PMBA"

    #@67
    new-instance v8, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v9, "Unable to read Package Manager state file: "

    #@6e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v8

    #@72
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v8

    #@7a
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    goto :goto_5b
.end method

.method private static readSignatureArray(Ljava/io/DataInputStream;)[Landroid/content/pm/Signature;
    .registers 10
    .parameter "in"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 333
    :try_start_1
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I
    :try_end_4
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_4} :catch_22
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_4} :catch_18

    #@4
    move-result v4

    #@5
    .line 343
    .local v4, num:I
    const/16 v7, 0x14

    #@7
    if-le v4, v7, :cond_2c

    #@9
    .line 344
    :try_start_9
    const-string v7, "PMBA"

    #@b
    const-string v8, "Suspiciously large sig count in restore data; aborting"

    #@d
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 345
    new-instance v7, Ljava/lang/IllegalStateException;

    #@12
    const-string v8, "Bad restore state"

    #@14
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v7
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_18} :catch_18

    #@18
    .line 356
    .end local v4           #num:I
    :catch_18
    move-exception v0

    #@19
    .line 357
    .local v0, e:Ljava/io/IOException;
    const-string v7, "PMBA"

    #@1b
    const-string v8, "Unable to read signatures"

    #@1d
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    move-object v5, v6

    #@21
    .line 358
    .end local v0           #e:Ljava/io/IOException;
    :cond_21
    :goto_21
    return-object v5

    #@22
    .line 334
    :catch_22
    move-exception v0

    #@23
    .line 336
    .local v0, e:Ljava/io/EOFException;
    :try_start_23
    const-string v7, "PMBA"

    #@25
    const-string v8, "Read empty signature block"

    #@27
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    move-object v5, v6

    #@2b
    .line 337
    goto :goto_21

    #@2c
    .line 348
    .end local v0           #e:Ljava/io/EOFException;
    .restart local v4       #num:I
    :cond_2c
    new-array v5, v4, [Landroid/content/pm/Signature;

    #@2e
    .line 349
    .local v5, sigs:[Landroid/content/pm/Signature;
    const/4 v2, 0x0

    #@2f
    .local v2, i:I
    :goto_2f
    if-ge v2, v4, :cond_21

    #@31
    .line 350
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    #@34
    move-result v3

    #@35
    .line 351
    .local v3, len:I
    new-array v1, v3, [B

    #@37
    .line 352
    .local v1, flatSig:[B
    invoke-virtual {p0, v1}, Ljava/io/DataInputStream;->read([B)I

    #@3a
    .line 353
    new-instance v7, Landroid/content/pm/Signature;

    #@3c
    invoke-direct {v7, v1}, Landroid/content/pm/Signature;-><init>([B)V

    #@3f
    aput-object v7, v5, v2
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_41} :catch_18

    #@41
    .line 349
    add-int/lit8 v2, v2, 0x1

    #@43
    goto :goto_2f
.end method

.method private static writeEntity(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;[B)V
    .registers 4
    .parameter "data"
    .parameter "key"
    .parameter "bytes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 246
    array-length v0, p2

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    #@4
    .line 247
    array-length v0, p2

    #@5
    invoke-virtual {p0, p2, v0}, Landroid/app/backup/BackupDataOutput;->writeEntityData([BI)I

    #@8
    .line 248
    return-void
.end method

.method private static writeSignatureArray(Ljava/io/DataOutputStream;[Landroid/content/pm/Signature;)V
    .registers 8
    .parameter "out"
    .parameter "sigs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 319
    array-length v5, p1

    #@1
    invoke-virtual {p0, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@4
    .line 322
    move-object v0, p1

    #@5
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v3, v0

    #@6
    .local v3, len$:I
    const/4 v2, 0x0

    #@7
    .local v2, i$:I
    :goto_7
    if-ge v2, v3, :cond_19

    #@9
    aget-object v4, v0, v2

    #@b
    .line 323
    .local v4, sig:Landroid/content/pm/Signature;
    invoke-virtual {v4}, Landroid/content/pm/Signature;->toByteArray()[B

    #@e
    move-result-object v1

    #@f
    .line 324
    .local v1, flat:[B
    array-length v5, v1

    #@10
    invoke-virtual {p0, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@13
    .line 325
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->write([B)V

    #@16
    .line 322
    add-int/lit8 v2, v2, 0x1

    #@18
    goto :goto_7

    #@19
    .line 327
    .end local v1           #flat:[B
    .end local v4           #sig:Landroid/content/pm/Signature;
    :cond_19
    return-void
.end method

.method private writeStateFile(Ljava/util/List;Landroid/os/ParcelFileDescriptor;)V
    .registers 10
    .parameter
    .parameter "stateFile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;",
            "Landroid/os/ParcelFileDescriptor;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 406
    .local p1, pkgs:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    new-instance v3, Ljava/io/FileOutputStream;

    #@2
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@5
    move-result-object v5

    #@6
    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@9
    .line 407
    .local v3, outstream:Ljava/io/FileOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    #@b
    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@e
    .line 411
    .local v2, out:Ljava/io/DataOutputStream;
    :try_start_e
    const-string v5, "@meta@"

    #@10
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@13
    .line 412
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    #@15
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@18
    .line 413
    sget-object v5, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    #@1a
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1d
    .line 416
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v1

    #@21
    .local v1, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v5

    #@25
    if-eqz v5, :cond_40

    #@27
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v4

    #@2b
    check-cast v4, Landroid/content/pm/PackageInfo;

    #@2d
    .line 417
    .local v4, pkg:Landroid/content/pm/PackageInfo;
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@2f
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@32
    .line 418
    iget v5, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    #@34
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_37} :catch_38

    #@37
    goto :goto_21

    #@38
    .line 420
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v4           #pkg:Landroid/content/pm/PackageInfo;
    :catch_38
    move-exception v0

    #@39
    .line 421
    .local v0, e:Ljava/io/IOException;
    const-string v5, "PMBA"

    #@3b
    const-string v6, "Unable to write package manager state file!"

    #@3d
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 424
    .end local v0           #e:Ljava/io/IOException;
    :cond_40
    return-void
.end method


# virtual methods
.method public getRestoredMetadata(Ljava/lang/String;)Lcom/android/server/PackageManagerBackupAgent$Metadata;
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mRestoredSignatures:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 98
    const-string v0, "PMBA"

    #@6
    const-string v1, "getRestoredMetadata() before metadata read!"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 99
    const/4 v0, 0x0

    #@c
    .line 102
    :goto_c
    return-object v0

    #@d
    :cond_d
    iget-object v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mRestoredSignatures:Ljava/util/HashMap;

    #@f
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/android/server/PackageManagerBackupAgent$Metadata;

    #@15
    goto :goto_c
.end method

.method public getRestoredPackages()Ljava/util/Set;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mRestoredSignatures:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 107
    const-string v0, "PMBA"

    #@6
    const-string v1, "getRestoredPackages() before metadata read!"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 108
    const/4 v0, 0x0

    #@c
    .line 116
    :goto_c
    return-object v0

    #@d
    :cond_d
    iget-object v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mRestoredSignatures:Ljava/util/HashMap;

    #@f
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@12
    move-result-object v0

    #@13
    goto :goto_c
.end method

.method public hasMetadata()Z
    .registers 2

    #@0
    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/android/server/PackageManagerBackupAgent;->mHasMetadata:Z

    #@2
    return v0
.end method

.method public onBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .registers 15
    .parameter "oldState"
    .parameter "data"
    .parameter "newState"

    #@0
    .prologue
    .line 125
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@5
    .line 126
    .local v4, outputBuffer:Ljava/io/ByteArrayOutputStream;
    new-instance v5, Ljava/io/DataOutputStream;

    #@7
    invoke-direct {v5, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@a
    .line 127
    .local v5, outputBufferStream:Ljava/io/DataOutputStream;
    invoke-direct {p0, p1}, Lcom/android/server/PackageManagerBackupAgent;->parseStateFile(Landroid/os/ParcelFileDescriptor;)V

    #@d
    .line 132
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mStoredIncrementalVersion:Ljava/lang/String;

    #@f
    if-eqz v8, :cond_1b

    #@11
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mStoredIncrementalVersion:Ljava/lang/String;

    #@13
    sget-object v9, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v8

    #@19
    if-nez v8, :cond_4c

    #@1b
    .line 134
    :cond_1b
    const-string v8, "PMBA"

    #@1d
    new-instance v9, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v10, "Previous metadata "

    #@24
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v9

    #@28
    iget-object v10, p0, Lcom/android/server/PackageManagerBackupAgent;->mStoredIncrementalVersion:Ljava/lang/String;

    #@2a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v9

    #@2e
    const-string v10, " mismatch vs "

    #@30
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v9

    #@34
    sget-object v10, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    #@36
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v9

    #@3a
    const-string v10, " - rewriting"

    #@3c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v9

    #@40
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v9

    #@44
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 136
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@49
    invoke-virtual {v8}, Ljava/util/HashSet;->clear()V

    #@4c
    .line 149
    :cond_4c
    :try_start_4c
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@4e
    const-string v9, "@meta@"

    #@50
    invoke-virtual {v8, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@53
    move-result v8

    #@54
    if-nez v8, :cond_da

    #@56
    .line 151
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    #@58
    invoke-virtual {v5, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@5b
    .line 152
    sget-object v8, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    #@5d
    invoke-virtual {v5, v8}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@60
    .line 153
    const-string v8, "@meta@"

    #@62
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@65
    move-result-object v9

    #@66
    invoke-static {p2, v8, v9}, Lcom/android/server/PackageManagerBackupAgent;->writeEntity(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;[B)V

    #@69
    .line 162
    :goto_69
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mAllPackages:Ljava/util/List;

    #@6b
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@6e
    move-result-object v2

    #@6f
    .local v2, i$:Ljava/util/Iterator;
    :cond_6f
    :goto_6f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@72
    move-result v8

    #@73
    if-eqz v8, :cond_ff

    #@75
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@78
    move-result-object v7

    #@79
    check-cast v7, Landroid/content/pm/PackageInfo;

    #@7b
    .line 163
    .local v7, pkg:Landroid/content/pm/PackageInfo;
    iget-object v6, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@7d
    .line 164
    .local v6, packName:Ljava/lang/String;
    const-string v8, "@meta@"

    #@7f
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_82
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_82} :catch_d1

    #@82
    move-result v8

    #@83
    if-nez v8, :cond_6f

    #@85
    .line 168
    const/4 v3, 0x0

    #@86
    .line 170
    .local v3, info:Landroid/content/pm/PackageInfo;
    :try_start_86
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mPackageManager:Landroid/content/pm/PackageManager;

    #@88
    const/16 v9, 0x40

    #@8a
    invoke-virtual {v8, v6, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_8d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_86 .. :try_end_8d} :catch_e2
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_8d} :catch_d1

    #@8d
    move-result-object v3

    #@8e
    .line 179
    :try_start_8e
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@90
    invoke-virtual {v8, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@93
    move-result v8

    #@94
    if-eqz v8, :cond_a9

    #@96
    .line 185
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@98
    invoke-virtual {v8, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@9b
    .line 186
    iget v9, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    #@9d
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mStateVersions:Ljava/util/HashMap;

    #@9f
    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a2
    move-result-object v8

    #@a3
    check-cast v8, Lcom/android/server/PackageManagerBackupAgent$Metadata;

    #@a5
    iget v8, v8, Lcom/android/server/PackageManagerBackupAgent$Metadata;->versionCode:I

    #@a7
    if-eq v9, v8, :cond_6f

    #@a9
    .line 191
    :cond_a9
    iget-object v8, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@ab
    if-eqz v8, :cond_b2

    #@ad
    iget-object v8, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@af
    array-length v8, v8

    #@b0
    if-nez v8, :cond_e9

    #@b2
    .line 193
    :cond_b2
    const-string v8, "PMBA"

    #@b4
    new-instance v9, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v10, "Not backing up package "

    #@bb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v9

    #@bf
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v9

    #@c3
    const-string v10, " since it appears to have no signatures."

    #@c5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v9

    #@c9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v9

    #@cd
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d0
    .catch Ljava/io/IOException; {:try_start_8e .. :try_end_d0} :catch_d1

    #@d0
    goto :goto_6f

    #@d1
    .line 234
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #info:Landroid/content/pm/PackageInfo;
    .end local v6           #packName:Ljava/lang/String;
    .end local v7           #pkg:Landroid/content/pm/PackageInfo;
    :catch_d1
    move-exception v1

    #@d2
    .line 236
    .local v1, e:Ljava/io/IOException;
    const-string v8, "PMBA"

    #@d4
    const-string v9, "Unable to write package backup data file!"

    #@d6
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 242
    .end local v1           #e:Ljava/io/IOException;
    :goto_d9
    return-void

    #@da
    .line 157
    :cond_da
    :try_start_da
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@dc
    const-string v9, "@meta@"

    #@de
    invoke-virtual {v8, v9}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@e1
    goto :goto_69

    #@e2
    .line 172
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #info:Landroid/content/pm/PackageInfo;
    .restart local v6       #packName:Ljava/lang/String;
    .restart local v7       #pkg:Landroid/content/pm/PackageInfo;
    :catch_e2
    move-exception v1

    #@e3
    .line 175
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@e5
    invoke-virtual {v8, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@e8
    goto :goto_6f

    #@e9
    .line 207
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_e9
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->reset()V

    #@ec
    .line 208
    iget v8, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    #@ee
    invoke-virtual {v5, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@f1
    .line 209
    iget-object v8, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@f3
    invoke-static {v5, v8}, Lcom/android/server/PackageManagerBackupAgent;->writeSignatureArray(Ljava/io/DataOutputStream;[Landroid/content/pm/Signature;)V

    #@f6
    .line 218
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@f9
    move-result-object v8

    #@fa
    invoke-static {p2, v6, v8}, Lcom/android/server/PackageManagerBackupAgent;->writeEntity(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;[B)V

    #@fd
    goto/16 :goto_6f

    #@ff
    .line 225
    .end local v3           #info:Landroid/content/pm/PackageInfo;
    .end local v6           #packName:Ljava/lang/String;
    .end local v7           #pkg:Landroid/content/pm/PackageInfo;
    :cond_ff
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mExisting:Ljava/util/HashSet;

    #@101
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@104
    move-result-object v2

    #@105
    :goto_105
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@108
    move-result v8

    #@109
    if-eqz v8, :cond_11f

    #@10b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10e
    move-result-object v0

    #@10f
    check-cast v0, Ljava/lang/String;
    :try_end_111
    .catch Ljava/io/IOException; {:try_start_da .. :try_end_111} :catch_d1

    #@111
    .line 228
    .local v0, app:Ljava/lang/String;
    const/4 v8, -0x1

    #@112
    :try_start_112
    invoke-virtual {p2, v0, v8}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I
    :try_end_115
    .catch Ljava/io/IOException; {:try_start_112 .. :try_end_115} :catch_116

    #@115
    goto :goto_105

    #@116
    .line 229
    :catch_116
    move-exception v1

    #@117
    .line 230
    .local v1, e:Ljava/io/IOException;
    :try_start_117
    const-string v8, "PMBA"

    #@119
    const-string v9, "Unable to write package deletions!"

    #@11b
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11e
    .catch Ljava/io/IOException; {:try_start_117 .. :try_end_11e} :catch_d1

    #@11e
    goto :goto_d9

    #@11f
    .line 241
    .end local v0           #app:Ljava/lang/String;
    .end local v1           #e:Ljava/io/IOException;
    :cond_11f
    iget-object v8, p0, Lcom/android/server/PackageManagerBackupAgent;->mAllPackages:Ljava/util/List;

    #@121
    invoke-direct {p0, v8, p3}, Lcom/android/server/PackageManagerBackupAgent;->writeStateFile(Ljava/util/List;Landroid/os/ParcelFileDescriptor;)V

    #@124
    goto :goto_d9
.end method

.method public onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V
    .registers 20
    .parameter "data"
    .parameter "appVersionCode"
    .parameter "newState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 255
    new-instance v7, Ljava/util/ArrayList;

    #@2
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 256
    .local v7, restoredApps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    new-instance v8, Ljava/util/HashMap;

    #@7
    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    #@a
    .line 258
    .local v8, sigMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/PackageManagerBackupAgent$Metadata;>;"
    const/4 v11, -0x1

    #@b
    .line 260
    .local v11, storedSystemVersion:I
    :goto_b
    invoke-virtual/range {p1 .. p1}, Landroid/app/backup/BackupDataInput;->readNextHeader()Z

    #@e
    move-result v13

    #@f
    if-eqz v13, :cond_97

    #@11
    .line 261
    invoke-virtual/range {p1 .. p1}, Landroid/app/backup/BackupDataInput;->getKey()Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    .line 262
    .local v6, key:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/app/backup/BackupDataInput;->getDataSize()I

    #@18
    move-result v2

    #@19
    .line 267
    .local v2, dataSize:I
    new-array v5, v2, [B

    #@1b
    .line 268
    .local v5, inputBytes:[B
    const/4 v13, 0x0

    #@1c
    move-object/from16 v0, p1

    #@1e
    invoke-virtual {v0, v5, v13, v2}, Landroid/app/backup/BackupDataInput;->readEntityData([BII)I

    #@21
    .line 269
    new-instance v3, Ljava/io/ByteArrayInputStream;

    #@23
    invoke-direct {v3, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@26
    .line 270
    .local v3, inputBuffer:Ljava/io/ByteArrayInputStream;
    new-instance v4, Ljava/io/DataInputStream;

    #@28
    invoke-direct {v4, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@2b
    .line 272
    .local v4, inputBufferStream:Ljava/io/DataInputStream;
    const-string v13, "@meta@"

    #@2d
    invoke-virtual {v6, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v13

    #@31
    if-eqz v13, :cond_55

    #@33
    .line 273
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    #@36
    move-result v10

    #@37
    .line 275
    .local v10, storedSdkVersion:I
    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    #@39
    if-le v11, v13, :cond_43

    #@3b
    .line 277
    const-string v13, "PMBA"

    #@3d
    const-string v14, "Restore set was from a later version of Android; not restoring"

    #@3f
    invoke-static {v13, v14}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 314
    .end local v2           #dataSize:I
    .end local v3           #inputBuffer:Ljava/io/ByteArrayInputStream;
    .end local v4           #inputBufferStream:Ljava/io/DataInputStream;
    .end local v5           #inputBytes:[B
    .end local v6           #key:Ljava/lang/String;
    .end local v10           #storedSdkVersion:I
    :goto_42
    return-void

    #@43
    .line 280
    .restart local v2       #dataSize:I
    .restart local v3       #inputBuffer:Ljava/io/ByteArrayInputStream;
    .restart local v4       #inputBufferStream:Ljava/io/DataInputStream;
    .restart local v5       #inputBytes:[B
    .restart local v6       #key:Ljava/lang/String;
    .restart local v10       #storedSdkVersion:I
    :cond_43
    move-object/from16 v0, p0

    #@45
    iput v10, v0, Lcom/android/server/PackageManagerBackupAgent;->mStoredSdkVersion:I

    #@47
    .line 281
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@4a
    move-result-object v13

    #@4b
    move-object/from16 v0, p0

    #@4d
    iput-object v13, v0, Lcom/android/server/PackageManagerBackupAgent;->mStoredIncrementalVersion:Ljava/lang/String;

    #@4f
    .line 282
    const/4 v13, 0x1

    #@50
    move-object/from16 v0, p0

    #@52
    iput-boolean v13, v0, Lcom/android/server/PackageManagerBackupAgent;->mHasMetadata:Z

    #@54
    goto :goto_b

    #@55
    .line 291
    .end local v10           #storedSdkVersion:I
    :cond_55
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    #@58
    move-result v12

    #@59
    .line 292
    .local v12, versionCode:I
    invoke-static {v4}, Lcom/android/server/PackageManagerBackupAgent;->readSignatureArray(Ljava/io/DataInputStream;)[Landroid/content/pm/Signature;

    #@5c
    move-result-object v9

    #@5d
    .line 299
    .local v9, sigs:[Landroid/content/pm/Signature;
    if-eqz v9, :cond_62

    #@5f
    array-length v13, v9

    #@60
    if-nez v13, :cond_81

    #@62
    .line 300
    :cond_62
    const-string v13, "PMBA"

    #@64
    new-instance v14, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v15, "Not restoring package "

    #@6b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v14

    #@6f
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v14

    #@73
    const-string v15, " since it appears to have no signatures."

    #@75
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v14

    #@79
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v14

    #@7d
    invoke-static {v13, v14}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    goto :goto_b

    #@81
    .line 305
    :cond_81
    new-instance v1, Landroid/content/pm/ApplicationInfo;

    #@83
    invoke-direct {v1}, Landroid/content/pm/ApplicationInfo;-><init>()V

    #@86
    .line 306
    .local v1, app:Landroid/content/pm/ApplicationInfo;
    iput-object v6, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@88
    .line 307
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@8b
    .line 308
    new-instance v13, Lcom/android/server/PackageManagerBackupAgent$Metadata;

    #@8d
    move-object/from16 v0, p0

    #@8f
    invoke-direct {v13, v0, v12, v9}, Lcom/android/server/PackageManagerBackupAgent$Metadata;-><init>(Lcom/android/server/PackageManagerBackupAgent;I[Landroid/content/pm/Signature;)V

    #@92
    invoke-virtual {v8, v6, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@95
    goto/16 :goto_b

    #@97
    .line 313
    .end local v1           #app:Landroid/content/pm/ApplicationInfo;
    .end local v2           #dataSize:I
    .end local v3           #inputBuffer:Ljava/io/ByteArrayInputStream;
    .end local v4           #inputBufferStream:Ljava/io/DataInputStream;
    .end local v5           #inputBytes:[B
    .end local v6           #key:Ljava/lang/String;
    .end local v9           #sigs:[Landroid/content/pm/Signature;
    .end local v12           #versionCode:I
    :cond_97
    move-object/from16 v0, p0

    #@99
    iput-object v8, v0, Lcom/android/server/PackageManagerBackupAgent;->mRestoredSignatures:Ljava/util/HashMap;

    #@9b
    goto :goto_42
.end method
