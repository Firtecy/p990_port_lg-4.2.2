.class final Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;
.super Ljava/lang/Object;
.source "WiredAccessoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UEventInfo"
.end annotation


# instance fields
.field private final mDevName:Ljava/lang/String;

.field private final mState1Bits:I

.field private final mState2Bits:I

.field final synthetic this$1:Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;


# direct methods
.method public constructor <init>(Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;Ljava/lang/String;II)V
    .registers 5
    .parameter
    .parameter "devName"
    .parameter "state1Bits"
    .parameter "state2Bits"

    #@0
    .prologue
    .line 402
    iput-object p1, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->this$1:Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 403
    iput-object p2, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mDevName:Ljava/lang/String;

    #@7
    .line 404
    iput p3, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mState1Bits:I

    #@9
    .line 405
    iput p4, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mState2Bits:I

    #@b
    .line 406
    return-void
.end method


# virtual methods
.method public checkSwitchExists()Z
    .registers 3

    #@0
    .prologue
    .line 419
    new-instance v0, Ljava/io/File;

    #@2
    invoke-virtual {p0}, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->getSwitchStatePath()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    .line 420
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@c
    move-result v1

    #@d
    return v1
.end method

.method public computeNewHeadsetState(II)I
    .registers 7
    .parameter "headsetState"
    .parameter "switchState"

    #@0
    .prologue
    .line 424
    iget v2, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mState1Bits:I

    #@2
    iget v3, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mState2Bits:I

    #@4
    or-int/2addr v2, v3

    #@5
    xor-int/lit8 v0, v2, -0x1

    #@7
    .line 425
    .local v0, preserveMask:I
    const/4 v2, 0x1

    #@8
    if-ne p2, v2, :cond_10

    #@a
    iget v1, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mState1Bits:I

    #@c
    .line 428
    .local v1, setBits:I
    :goto_c
    and-int v2, p1, v0

    #@e
    or-int/2addr v2, v1

    #@f
    return v2

    #@10
    .line 425
    .end local v1           #setBits:I
    :cond_10
    const/4 v2, 0x2

    #@11
    if-ne p2, v2, :cond_16

    #@13
    iget v1, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mState2Bits:I

    #@15
    goto :goto_c

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_c
.end method

.method public getDevName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mDevName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getDevPath()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 411
    const-string v0, "/devices/virtual/switch/%s"

    #@2
    const/4 v1, 0x1

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    iget-object v3, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mDevName:Ljava/lang/String;

    #@8
    aput-object v3, v1, v2

    #@a
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method public getSwitchStatePath()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 415
    const-string v0, "/sys/class/switch/%s/state"

    #@2
    const/4 v1, 0x1

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    iget-object v3, p0, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver$UEventInfo;->mDevName:Ljava/lang/String;

    #@8
    aput-object v3, v1, v2

    #@a
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method
