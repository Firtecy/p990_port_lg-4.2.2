.class public final Lcom/android/server/OemExtendedApi3LMService;
.super Landroid/os/IOemExtendedApi3LM$Stub;
.source "OemExtendedApi3LMService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;
    }
.end annotation


# static fields
.field private static final API_VERSION:I = 0x1

.field private static final DEBUG:Z = false

.field public static final STATE_DISABLED:I = 0x0

.field public static final STATE_ENABLED:I = 0x1

.field public static final STATE_UNKNOWN:I = -0x1

.field public static final STATE_UNSUPPORTED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "OemExtendedApi3LM"

.field private static final felicaPropertyKey:Ljava/lang/String; = "persist.security.felica.lockout"

.field private static final irdaPropertyKey:Ljava/lang/String; = "persist.security.irda.lockout"

.field private static final onesegPropertyKey:Ljava/lang/String; = "persist.security.oneseg.lockout"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDeviceManager:Landroid/os/IDeviceManager3LM;

.field private final mOem:Landroid/os/IDeviceManagerRestrictable3LM;

.field private mOwnerInfo:Ljava/lang/String;

.field private mOwnerInfoEnabled:I

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPublicKey3LM:Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/IDeviceManagerRestrictable3LM;)V
    .registers 4
    .parameter "context"
    .parameter "oem"

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Landroid/os/IOemExtendedApi3LM$Stub;-><init>()V

    #@3
    .line 68
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/server/OemExtendedApi3LMService;->mOwnerInfoEnabled:I

    #@6
    .line 130
    iput-object p1, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@8
    .line 131
    iput-object p2, p0, Lcom/android/server/OemExtendedApi3LMService;->mOem:Landroid/os/IDeviceManagerRestrictable3LM;

    #@a
    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/server/OemExtendedApi3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@10
    .line 133
    const-string v0, "DeviceManager3LM"

    #@12
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/os/IDeviceManager3LM;

    #@18
    iput-object v0, p0, Lcom/android/server/OemExtendedApi3LMService;->mDeviceManager:Landroid/os/IDeviceManager3LM;

    #@1a
    .line 135
    new-instance v0, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;

    #@1c
    invoke-direct {v0, p1}, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;-><init>(Landroid/content/Context;)V

    #@1f
    iput-object v0, p0, Lcom/android/server/OemExtendedApi3LMService;->mPublicKey3LM:Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;

    #@21
    .line 136
    return-void
.end method

.method private isAccessPermitted()Z
    .registers 16

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    .line 140
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v2

    #@5
    .line 141
    .local v2, callerUid:I
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@8
    move-result v13

    #@9
    if-ne v2, v13, :cond_c

    #@b
    .line 167
    :cond_b
    :goto_b
    return v12

    #@c
    .line 146
    :cond_c
    :try_start_c
    iget-object v13, p0, Lcom/android/server/OemExtendedApi3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@e
    invoke-virtual {v13, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@11
    move-result-object v9

    #@12
    .line 148
    .local v9, packages:[Ljava/lang/String;
    move-object v0, v9

    #@13
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@14
    .local v6, len$:I
    const/4 v4, 0x0

    #@15
    .local v4, i$:I
    move v5, v4

    #@16
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v6           #len$:I
    .local v5, i$:I
    :goto_16
    if-ge v5, v6, :cond_56

    #@18
    aget-object v10, v0, v5

    #@1a
    .line 150
    .local v10, pkg:Ljava/lang/String;
    iget-object v13, p0, Lcom/android/server/OemExtendedApi3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@1c
    const/16 v14, 0x40

    #@1e
    invoke-virtual {v13, v10, v14}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@21
    move-result-object v8

    #@22
    .line 154
    .local v8, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v1, v8, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@24
    .local v1, arr$:[Landroid/content/pm/Signature;
    array-length v7, v1

    #@25
    .local v7, len$:I
    const/4 v4, 0x0

    #@26
    .end local v5           #i$:I
    .restart local v4       #i$:I
    :goto_26
    if-ge v4, v7, :cond_39

    #@28
    aget-object v11, v1, v4

    #@2a
    .line 155
    .local v11, pkgSignature:Landroid/content/pm/Signature;
    iget-object v13, p0, Lcom/android/server/OemExtendedApi3LMService;->mPublicKey3LM:Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;

    #@2c
    invoke-virtual {v11}, Landroid/content/pm/Signature;->toByteArray()[B

    #@2f
    move-result-object v14

    #@30
    invoke-virtual {v13, v14}, Lcom/android/server/OemExtendedApi3LMService$PublicKey3LM;->comparePublicKey([B)Z
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_33} :catch_3d

    #@33
    move-result v13

    #@34
    if-nez v13, :cond_b

    #@36
    .line 154
    add-int/lit8 v4, v4, 0x1

    #@38
    goto :goto_26

    #@39
    .line 148
    .end local v11           #pkgSignature:Landroid/content/pm/Signature;
    :cond_39
    add-int/lit8 v4, v5, 0x1

    #@3b
    move v5, v4

    #@3c
    .end local v4           #i$:I
    .restart local v5       #i$:I
    goto :goto_16

    #@3d
    .line 163
    .end local v1           #arr$:[Landroid/content/pm/Signature;
    .end local v5           #i$:I
    .end local v7           #len$:I
    .end local v8           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v9           #packages:[Ljava/lang/String;
    .end local v10           #pkg:Ljava/lang/String;
    :catch_3d
    move-exception v3

    #@3e
    .line 164
    .local v3, e:Ljava/lang/Exception;
    const-string v12, "OemExtendedApi3LM"

    #@40
    new-instance v13, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v14, "Error trying to verify package public key against 3LM pub key:"

    #@47
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v13

    #@4b
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v13

    #@4f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v13

    #@53
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 166
    .end local v3           #e:Ljava/lang/Exception;
    :cond_56
    const-string v12, "OemExtendedApi3LM"

    #@58
    new-instance v13, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v14, "Access denied to UID: "

    #@5f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v13

    #@63
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v13

    #@67
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v13

    #@6b
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 167
    const/4 v12, 0x0

    #@6f
    goto :goto_b
.end method

.method private setFelicaStateForEmergencyLock()V
    .registers 6

    #@0
    .prologue
    .line 288
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@5
    .line 289
    .local v1, i:Landroid/content/Intent;
    const-string v2, "com.lge.nfclock"

    #@7
    const-string v3, "com.lge.nfclock.NfcLockRemoteActivity"

    #@9
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 290
    const-string v2, "$func"

    #@e
    const-string v3, "r-lock"

    #@10
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13
    .line 291
    const/high16 v2, 0x1000

    #@15
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@18
    .line 292
    iget-object v2, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1d} :catch_1e

    #@1d
    .line 296
    .end local v1           #i:Landroid/content/Intent;
    :goto_1d
    return-void

    #@1e
    .line 293
    :catch_1e
    move-exception v0

    #@1f
    .line 294
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "OemExtendedApi3LM"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v4, "NFC interface exception: "

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_1d
.end method


# virtual methods
.method public clear()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 178
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->isAccessPermitted()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 197
    :goto_7
    return-void

    #@8
    .line 181
    :cond_8
    const-string v1, "persist.security.3lm.activated"

    #@a
    const-string v2, "1"

    #@c
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 182
    new-instance v0, Landroid/content/Intent;

    #@11
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@14
    .line 183
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "3LM_MDM_DEACTIVATED"

    #@16
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@19
    .line 184
    iget-object v1, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1e
    .line 187
    invoke-virtual {p0, v3}, Lcom/android/server/OemExtendedApi3LMService;->setFelicaState(I)V

    #@21
    .line 188
    invoke-virtual {p0, v3}, Lcom/android/server/OemExtendedApi3LMService;->setOneSegState(I)V

    #@24
    .line 189
    invoke-virtual {p0, v3}, Lcom/android/server/OemExtendedApi3LMService;->setInfraredState(I)V

    #@27
    .line 192
    :try_start_27
    iget-object v1, p0, Lcom/android/server/OemExtendedApi3LMService;->mDeviceManager:Landroid/os/IDeviceManager3LM;

    #@29
    const/4 v2, 0x1

    #@2a
    invoke-interface {v1, v2}, Landroid/os/IDeviceManager3LM;->setNfcState(I)V

    #@2d
    .line 193
    iget-object v1, p0, Lcom/android/server/OemExtendedApi3LMService;->mDeviceManager:Landroid/os/IDeviceManager3LM;

    #@2f
    const/4 v2, 0x1

    #@30
    invoke-interface {v1, v2}, Landroid/os/IDeviceManager3LM;->setWifiState(I)V
    :try_end_33
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_33} :catch_34

    #@33
    goto :goto_7

    #@34
    .line 194
    :catch_34
    move-exception v1

    #@35
    goto :goto_7
.end method

.method public getFelicaState()I
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 211
    const-string v0, "OemExtendedApi3LM"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "getFelicaState : "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    const-string v2, "persist.security.felica.lockout"

    #@10
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@13
    move-result v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 212
    const-string v0, "persist.security.felica.lockout"

    #@21
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@24
    move-result v0

    #@25
    return v0
.end method

.method public getInfraredState()I
    .registers 3

    #@0
    .prologue
    .line 325
    const-string v0, "persist.security.irda.lockout"

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getOneSegState()I
    .registers 3

    #@0
    .prologue
    .line 303
    const-string v0, "persist.security.oneseg.lockout"

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getVersion()I
    .registers 2

    #@0
    .prologue
    .line 171
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setEmergencyLock(ZLjava/lang/String;Z)Z
    .registers 13
    .parameter "enable"
    .parameter "text"
    .parameter "alarm"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 460
    const-string v4, "ro.product.name"

    #@4
    const-string v7, "error"

    #@6
    invoke-static {v4, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v4

    #@a
    const-string v7, "geehdc_dcm_jp"

    #@c
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_17

    #@12
    .line 461
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/OemExtendedApi3LMService;->setEmergencyLockGJ(ZLjava/lang/String;Z)Z

    #@15
    move-result v6

    #@16
    .line 510
    :cond_16
    :goto_16
    return v6

    #@17
    .line 465
    :cond_17
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->isAccessPermitted()Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_16

    #@1d
    .line 467
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@20
    .line 470
    iget-object v4, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@22
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@25
    move-result-object v7

    #@26
    const-string v8, "lock_screen_owner_info_enabled"

    #@28
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@2b
    move-result v4

    #@2c
    if-lez v4, :cond_5a

    #@2e
    move v4, v5

    #@2f
    :goto_2f
    invoke-static {v7, v8, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@32
    .line 474
    iget-object v4, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@34
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@37
    move-result-object v4

    #@38
    const-string v7, "lock_screen_owner_info"

    #@3a
    invoke-static {v4, v7, p2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@3d
    .line 478
    if-eqz p1, :cond_5c

    #@3f
    move v2, v5

    #@40
    .line 480
    .local v2, newState:I
    :goto_40
    invoke-virtual {p0, v2}, Lcom/android/server/OemExtendedApi3LMService;->setOneSegState(I)V

    #@43
    .line 481
    invoke-virtual {p0, v2}, Lcom/android/server/OemExtendedApi3LMService;->setInfraredState(I)V

    #@46
    .line 484
    if-eqz p1, :cond_5e

    #@48
    .line 486
    :try_start_48
    invoke-virtual {p0}, Lcom/android/server/OemExtendedApi3LMService;->getFelicaState()I

    #@4b
    move-result v4

    #@4c
    if-ne v4, v5, :cond_52

    #@4e
    .line 487
    const/4 v4, 0x1

    #@4f
    invoke-virtual {p0, v4}, Lcom/android/server/OemExtendedApi3LMService;->setFelicaState(I)V

    #@52
    .line 490
    :cond_52
    iget-object v4, p0, Lcom/android/server/OemExtendedApi3LMService;->mDeviceManager:Landroid/os/IDeviceManager3LM;

    #@54
    const/4 v7, 0x0

    #@55
    invoke-interface {v4, v7}, Landroid/os/IDeviceManager3LM;->blockAdb(Z)V

    #@58
    :goto_58
    move v6, v5

    #@59
    .line 510
    goto :goto_16

    #@5a
    .end local v2           #newState:I
    :cond_5a
    move v4, v6

    #@5b
    .line 470
    goto :goto_2f

    #@5c
    :cond_5c
    move v2, v6

    #@5d
    .line 478
    goto :goto_40

    #@5e
    .line 495
    .restart local v2       #newState:I
    :cond_5e
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->setFelicaStateForEmergencyLock()V

    #@61
    .line 497
    iget-object v4, p0, Lcom/android/server/OemExtendedApi3LMService;->mDeviceManager:Landroid/os/IDeviceManager3LM;

    #@63
    const/4 v7, 0x1

    #@64
    invoke-interface {v4, v7}, Landroid/os/IDeviceManager3LM;->blockAdb(Z)V

    #@67
    .line 499
    iget-object v4, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@69
    const-string v7, "wifi"

    #@6b
    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6e
    move-result-object v3

    #@6f
    check-cast v3, Landroid/net/wifi/WifiManager;

    #@71
    .line 500
    .local v3, wifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@74
    move-result-object v0

    #@75
    .line 501
    .local v0, btAdapter:Landroid/bluetooth/BluetoothAdapter;
    const/4 v4, 0x0

    #@76
    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    #@79
    .line 502
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z
    :try_end_7c
    .catch Ljava/lang/NullPointerException; {:try_start_48 .. :try_end_7c} :catch_7d
    .catch Landroid/os/RemoteException; {:try_start_48 .. :try_end_7c} :catch_97

    #@7c
    goto :goto_58

    #@7d
    .line 504
    .end local v0           #btAdapter:Landroid/bluetooth/BluetoothAdapter;
    .end local v3           #wifiManager:Landroid/net/wifi/WifiManager;
    :catch_7d
    move-exception v1

    #@7e
    .line 505
    .local v1, e:Ljava/lang/NullPointerException;
    const-string v4, "OemExtendedApi3LM"

    #@80
    new-instance v5, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v7, "NullPointerException when disabling wifi"

    #@87
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v5

    #@8f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v5

    #@93
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    goto :goto_16

    #@97
    .line 507
    .end local v1           #e:Ljava/lang/NullPointerException;
    :catch_97
    move-exception v1

    #@98
    .line 508
    .local v1, e:Landroid/os/RemoteException;
    goto/16 :goto_16
.end method

.method public setEmergencyLockGJ(ZLjava/lang/String;Z)Z
    .registers 14
    .parameter "enable"
    .parameter "text"
    .parameter "alarm"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 372
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->isAccessPermitted()Z

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_9

    #@8
    .line 423
    :goto_8
    return v7

    #@9
    .line 374
    :cond_9
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    .line 377
    iget-object v5, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v8

    #@12
    const-string v9, "lock_screen_owner_info_enabled"

    #@14
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@17
    move-result v5

    #@18
    if-lez v5, :cond_3f

    #@1a
    move v5, v6

    #@1b
    :goto_1b
    invoke-static {v8, v9, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1e
    .line 382
    iget-object v5, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@20
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v5

    #@24
    const-string v8, "lock_screen_owner_info"

    #@26
    invoke-static {v5, v8, p2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@29
    .line 386
    if-eqz p1, :cond_41

    #@2b
    move v3, v6

    #@2c
    .line 388
    .local v3, newState:I
    :goto_2c
    invoke-virtual {p0, v3}, Lcom/android/server/OemExtendedApi3LMService;->setFelicaState(I)V

    #@2f
    .line 389
    invoke-virtual {p0, v3}, Lcom/android/server/OemExtendedApi3LMService;->setOneSegState(I)V

    #@32
    .line 390
    invoke-virtual {p0, v3}, Lcom/android/server/OemExtendedApi3LMService;->setInfraredState(I)V

    #@35
    .line 393
    if-eqz p1, :cond_43

    #@37
    .line 396
    :try_start_37
    iget-object v5, p0, Lcom/android/server/OemExtendedApi3LMService;->mDeviceManager:Landroid/os/IDeviceManager3LM;

    #@39
    const/4 v8, 0x0

    #@3a
    invoke-interface {v5, v8}, Landroid/os/IDeviceManager3LM;->blockAdb(Z)V

    #@3d
    :goto_3d
    move v7, v6

    #@3e
    .line 423
    goto :goto_8

    #@3f
    .end local v3           #newState:I
    :cond_3f
    move v5, v7

    #@40
    .line 377
    goto :goto_1b

    #@41
    :cond_41
    move v3, v7

    #@42
    .line 386
    goto :goto_2c

    #@43
    .line 401
    .restart local v3       #newState:I
    :cond_43
    iget-object v5, p0, Lcom/android/server/OemExtendedApi3LMService;->mDeviceManager:Landroid/os/IDeviceManager3LM;

    #@45
    const/4 v8, 0x1

    #@46
    invoke-interface {v5, v8}, Landroid/os/IDeviceManager3LM;->blockAdb(Z)V

    #@49
    .line 403
    iget-object v5, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@4b
    const-string v8, "wifi"

    #@4d
    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@50
    move-result-object v4

    #@51
    check-cast v4, Landroid/net/wifi/WifiManager;

    #@53
    .line 404
    .local v4, wifiManager:Landroid/net/wifi/WifiManager;
    const/4 v5, 0x0

    #@54
    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    #@57
    .line 406
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@5a
    move-result-object v0

    #@5b
    .line 407
    .local v0, btAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z
    :try_end_5e
    .catch Ljava/lang/NullPointerException; {:try_start_37 .. :try_end_5e} :catch_78
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_5e} :catch_93

    #@5e
    .line 410
    :try_start_5e
    iget-object v5, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@60
    const-string v8, "usb"

    #@62
    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@65
    move-result-object v2

    #@66
    check-cast v2, Landroid/hardware/usb/UsbManager;

    #@68
    .line 411
    .local v2, mUsbManager:Landroid/hardware/usb/UsbManager;
    const-string v5, "charge_only"

    #@6a
    const/4 v8, 0x1

    #@6b
    invoke-virtual {v2, v5, v8}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V
    :try_end_6e
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_6e} :catch_6f
    .catch Ljava/lang/NullPointerException; {:try_start_5e .. :try_end_6e} :catch_78
    .catch Landroid/os/RemoteException; {:try_start_5e .. :try_end_6e} :catch_93

    #@6e
    goto :goto_3d

    #@6f
    .line 412
    .end local v2           #mUsbManager:Landroid/hardware/usb/UsbManager;
    :catch_6f
    move-exception v1

    #@70
    .line 413
    .local v1, e:Ljava/lang/Exception;
    :try_start_70
    const-string v5, "OemExtendedApi3LM"

    #@72
    const-string v8, "usbmanager service exception"

    #@74
    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_77
    .catch Ljava/lang/NullPointerException; {:try_start_70 .. :try_end_77} :catch_78
    .catch Landroid/os/RemoteException; {:try_start_70 .. :try_end_77} :catch_93

    #@77
    goto :goto_3d

    #@78
    .line 416
    .end local v0           #btAdapter:Landroid/bluetooth/BluetoothAdapter;
    .end local v1           #e:Ljava/lang/Exception;
    .end local v4           #wifiManager:Landroid/net/wifi/WifiManager;
    :catch_78
    move-exception v1

    #@79
    .line 417
    .local v1, e:Ljava/lang/NullPointerException;
    const-string v5, "OemExtendedApi3LM"

    #@7b
    new-instance v6, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v8, "3lm NullPointerException"

    #@82
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v6

    #@8e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    goto/16 :goto_8

    #@93
    .line 419
    .end local v1           #e:Ljava/lang/NullPointerException;
    :catch_93
    move-exception v1

    #@94
    .line 420
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "OemExtendedApi3LM"

    #@96
    new-instance v6, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v8, "3lm RemoteException"

    #@9d
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v6

    #@a5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v6

    #@a9
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    goto/16 :goto_8
.end method

.method public setFelicaState(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    .line 254
    const-string v2, "ro.product.name"

    #@2
    const-string v3, "error"

    #@4
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    const-string v3, "geehdc_dcm_jp"

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_14

    #@10
    .line 255
    invoke-virtual {p0, p1}, Lcom/android/server/OemExtendedApi3LMService;->setFelicaStateGJ(I)V

    #@13
    .line 284
    :cond_13
    :goto_13
    return-void

    #@14
    .line 260
    :cond_14
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->isAccessPermitted()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_13

    #@1a
    .line 261
    const-string v2, "persist.security.felica.lockout"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, ""

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 264
    if-nez p1, :cond_6c

    #@34
    .line 266
    :try_start_34
    new-instance v1, Landroid/content/Intent;

    #@36
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@39
    .line 267
    .local v1, i:Landroid/content/Intent;
    const-string v2, "com.lge.nfclock"

    #@3b
    const-string v3, "com.lge.nfclock.NfcLockRemoteActivity"

    #@3d
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@40
    .line 268
    const-string v2, "$func"

    #@42
    const-string v3, "r-lock"

    #@44
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@47
    .line 269
    const/high16 v2, 0x1000

    #@49
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@4c
    .line 270
    iget-object v2, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@4e
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_51
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_51} :catch_52

    #@51
    goto :goto_13

    #@52
    .line 280
    .end local v1           #i:Landroid/content/Intent;
    :catch_52
    move-exception v0

    #@53
    .line 281
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "OemExtendedApi3LM"

    #@55
    new-instance v3, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v4, "NFC interface exception: "

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    goto :goto_13

    #@6c
    .line 274
    .end local v0           #e:Ljava/lang/Exception;
    :cond_6c
    :try_start_6c
    new-instance v1, Landroid/content/Intent;

    #@6e
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@71
    .line 275
    .restart local v1       #i:Landroid/content/Intent;
    const-string v2, "com.lge.nfclock"

    #@73
    const-string v3, "com.lge.nfclock.NfcLockRemoteActivity"

    #@75
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@78
    .line 276
    const-string v2, "$func"

    #@7a
    const-string v3, "r-unlock"

    #@7c
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7f
    .line 277
    const/high16 v2, 0x1000

    #@81
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@84
    .line 278
    iget-object v2, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@86
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_89
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_89} :catch_52

    #@89
    goto :goto_13
.end method

.method public setFelicaStateGJ(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    .line 428
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->isAccessPermitted()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_7

    #@6
    .line 457
    :goto_6
    return-void

    #@7
    .line 430
    :cond_7
    const-string v2, "persist.security.felica.lockout"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, ""

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 432
    if-nez p1, :cond_59

    #@21
    .line 434
    :try_start_21
    new-instance v1, Landroid/content/Intent;

    #@23
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@26
    .line 436
    .local v1, i:Landroid/content/Intent;
    const-string v2, "jp.co.fsi.felicalock"

    #@28
    const-string v3, "jp.co.fsi.felicalock.FelicaLockON"

    #@2a
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2d
    .line 437
    const-string v2, "$func"

    #@2f
    const-string v3, "lock"

    #@31
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@34
    .line 439
    const/high16 v2, 0x1000

    #@36
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@39
    .line 441
    iget-object v2, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@3b
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_3e} :catch_3f

    #@3e
    goto :goto_6

    #@3f
    .line 454
    .end local v1           #i:Landroid/content/Intent;
    :catch_3f
    move-exception v0

    #@40
    .line 455
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "OemExtendedApi3LM"

    #@42
    new-instance v3, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v4, "Felica interface exception: "

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    goto :goto_6

    #@59
    .line 445
    .end local v0           #e:Ljava/lang/Exception;
    :cond_59
    :try_start_59
    new-instance v1, Landroid/content/Intent;

    #@5b
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@5e
    .line 447
    .restart local v1       #i:Landroid/content/Intent;
    const-string v2, "jp.co.fsi.felicalock"

    #@60
    const-string v3, "jp.co.fsi.felicalock.FelicaLockON"

    #@62
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@65
    .line 448
    const-string v2, "$func"

    #@67
    const-string v3, "unlock"

    #@69
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6c
    .line 449
    const/high16 v2, 0x1000

    #@6e
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@71
    .line 451
    iget-object v2, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@73
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_76
    .catch Ljava/lang/Exception; {:try_start_59 .. :try_end_76} :catch_3f

    #@76
    goto :goto_6
.end method

.method public setInfraredState(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 334
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->isAccessPermitted()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 354
    :goto_7
    return-void

    #@8
    .line 343
    :cond_8
    const-string v1, "persist.security.3lm.activated"

    #@a
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@d
    move-result v1

    #@e
    if-ne v1, v2, :cond_1f

    #@10
    .line 344
    new-instance v0, Landroid/content/Intent;

    #@12
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@15
    .line 345
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "3LM_MDM_ACTIVATED"

    #@17
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@1a
    .line 346
    iget-object v1, p0, Lcom/android/server/OemExtendedApi3LMService;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1f
    .line 348
    .end local v0           #intent:Landroid/content/Intent;
    :cond_1f
    const-string v1, "persist.security.3lm.activated"

    #@21
    const-string v2, "0"

    #@23
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 353
    const-string v1, "persist.security.irda.lockout"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, ""

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    goto :goto_7
.end method

.method public setOneSegState(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 312
    invoke-direct {p0}, Lcom/android/server/OemExtendedApi3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 318
    :goto_6
    return-void

    #@7
    .line 316
    :cond_7
    const-string v0, "persist.security.oneseg.lockout"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, ""

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    goto :goto_6
.end method
