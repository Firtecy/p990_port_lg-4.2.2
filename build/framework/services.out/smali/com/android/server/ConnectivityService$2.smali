.class Lcom/android/server/ConnectivityService$2;
.super Landroid/net/INetworkPolicyListener$Stub;
.source "ConnectivityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ConnectivityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ConnectivityService;


# direct methods
.method constructor <init>(Lcom/android/server/ConnectivityService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2468
    iput-object p1, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@2
    invoke-direct {p0}, Landroid/net/INetworkPolicyListener$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onMeteredIfacesChanged([Ljava/lang/String;)V
    .registers 8
    .parameter "meteredIfaces"

    #@0
    .prologue
    .line 2491
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v5, "onMeteredIfacesChanged(ifaces="

    #@7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v4

    #@b
    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@e
    move-result-object v5

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, ")"

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-static {v4}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@20
    .line 2494
    iget-object v4, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@22
    invoke-static {v4}, Lcom/android/server/ConnectivityService;->access$400(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;

    #@25
    move-result-object v5

    #@26
    monitor-enter v5

    #@27
    .line 2495
    :try_start_27
    iget-object v4, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@29
    invoke-static {v4}, Lcom/android/server/ConnectivityService;->access$600(Lcom/android/server/ConnectivityService;)Ljava/util/HashSet;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    #@30
    .line 2496
    move-object v0, p1

    #@31
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@32
    .local v3, len$:I
    const/4 v1, 0x0

    #@33
    .local v1, i$:I
    :goto_33
    if-ge v1, v3, :cond_43

    #@35
    aget-object v2, v0, v1

    #@37
    .line 2497
    .local v2, iface:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@39
    invoke-static {v4}, Lcom/android/server/ConnectivityService;->access$600(Lcom/android/server/ConnectivityService;)Ljava/util/HashSet;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@40
    .line 2496
    add-int/lit8 v1, v1, 0x1

    #@42
    goto :goto_33

    #@43
    .line 2499
    .end local v2           #iface:Ljava/lang/String;
    :cond_43
    monitor-exit v5

    #@44
    .line 2500
    return-void

    #@45
    .line 2499
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v3           #len$:I
    :catchall_45
    move-exception v4

    #@46
    monitor-exit v5
    :try_end_47
    .catchall {:try_start_27 .. :try_end_47} :catchall_45

    #@47
    throw v4
.end method

.method public onRestrictBackgroundChanged(Z)V
    .registers 7
    .parameter "restrictBackground"

    #@0
    .prologue
    .line 2506
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "onRestrictBackgroundChanged(restrictBackground="

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    const-string v4, ")"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@1c
    .line 2511
    iget-object v3, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@1e
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$700(Lcom/android/server/ConnectivityService;)I

    #@21
    move-result v1

    #@22
    .line 2512
    .local v1, networkType:I
    invoke-static {v1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_43

    #@28
    .line 2513
    iget-object v3, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@2a
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$800(Lcom/android/server/ConnectivityService;)[Landroid/net/NetworkStateTracker;

    #@2d
    move-result-object v3

    #@2e
    aget-object v2, v3, v1

    #@30
    .line 2514
    .local v2, tracker:Landroid/net/NetworkStateTracker;
    if-eqz v2, :cond_43

    #@32
    .line 2515
    invoke-interface {v2}, Landroid/net/NetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@35
    move-result-object v0

    #@36
    .line 2516
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_43

    #@38
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_43

    #@3e
    .line 2517
    iget-object v3, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@40
    invoke-virtual {v3, v0}, Lcom/android/server/ConnectivityService;->sendConnectedBroadcast(Landroid/net/NetworkInfo;)V

    #@43
    .line 2521
    .end local v0           #info:Landroid/net/NetworkInfo;
    .end local v2           #tracker:Landroid/net/NetworkStateTracker;
    :cond_43
    return-void
.end method

.method public onUidRulesChanged(II)V
    .registers 7
    .parameter "uid"
    .parameter "uidRules"

    #@0
    .prologue
    .line 2473
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "onUidRulesChanged(uid="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ", uidRules="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ")"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@26
    .line 2476
    iget-object v1, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@28
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->access$400(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;

    #@2b
    move-result-object v2

    #@2c
    monitor-enter v2

    #@2d
    .line 2478
    :try_start_2d
    iget-object v1, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@2f
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->access$500(Lcom/android/server/ConnectivityService;)Landroid/util/SparseIntArray;

    #@32
    move-result-object v1

    #@33
    const/4 v3, 0x0

    #@34
    invoke-virtual {v1, p1, v3}, Landroid/util/SparseIntArray;->get(II)I

    #@37
    move-result v0

    #@38
    .line 2479
    .local v0, oldRules:I
    if-ne v0, p2, :cond_3c

    #@3a
    monitor-exit v2

    #@3b
    .line 2485
    :goto_3b
    return-void

    #@3c
    .line 2481
    :cond_3c
    iget-object v1, p0, Lcom/android/server/ConnectivityService$2;->this$0:Lcom/android/server/ConnectivityService;

    #@3e
    invoke-static {v1}, Lcom/android/server/ConnectivityService;->access$500(Lcom/android/server/ConnectivityService;)Landroid/util/SparseIntArray;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    #@45
    .line 2482
    monitor-exit v2

    #@46
    goto :goto_3b

    #@47
    .end local v0           #oldRules:I
    :catchall_47
    move-exception v1

    #@48
    monitor-exit v2
    :try_end_49
    .catchall {:try_start_2d .. :try_end_49} :catchall_47

    #@49
    throw v1
.end method
