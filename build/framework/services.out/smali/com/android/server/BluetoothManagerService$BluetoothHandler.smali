.class Lcom/android/server/BluetoothManagerService$BluetoothHandler;
.super Landroid/os/Handler;
.source "BluetoothManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BluetoothManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BluetoothManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/BluetoothManagerService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 763
    iput-object p1, p0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@2
    .line 764
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 765
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 30
    .parameter "msg"

    #@0
    .prologue
    .line 770
    const-string v23, "BluetoothManagerService"

    #@2
    new-instance v24, Ljava/lang/StringBuilder;

    #@4
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v25, "Message: "

    #@9
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v24

    #@d
    move-object/from16 v0, p1

    #@f
    iget v0, v0, Landroid/os/Message;->what:I

    #@11
    move/from16 v25, v0

    #@13
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v24

    #@17
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v24

    #@1b
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 772
    move-object/from16 v0, p1

    #@20
    iget v0, v0, Landroid/os/Message;->what:I

    #@22
    move/from16 v23, v0

    #@24
    sparse-switch v23, :sswitch_data_a84

    #@27
    .line 1198
    :cond_27
    :goto_27
    return-void

    #@28
    .line 775
    :sswitch_28
    const-string v23, "BluetoothManagerService"

    #@2a
    const-string v24, "MESSAGE_GET_NAME_AND_ADDRESS"

    #@2c
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 777
    move-object/from16 v0, p0

    #@31
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@33
    move-object/from16 v23, v0

    #@35
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@38
    move-result-object v24

    #@39
    monitor-enter v24

    #@3a
    .line 779
    :try_start_3a
    move-object/from16 v0, p0

    #@3c
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3e
    move-object/from16 v23, v0

    #@40
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@43
    move-result-object v23

    #@44
    if-nez v23, :cond_117

    #@46
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@4a
    move-object/from16 v23, v0

    #@4c
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1700(Lcom/android/server/BluetoothManagerService;)Z

    #@4f
    move-result v23

    #@50
    if-nez v23, :cond_117

    #@52
    .line 781
    const-string v23, "BluetoothManagerService"

    #@54
    const-string v25, "Binding to service to get name and address"

    #@56
    move-object/from16 v0, v23

    #@58
    move-object/from16 v1, v25

    #@5a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 783
    move-object/from16 v0, p0

    #@5f
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@61
    move-object/from16 v23, v0

    #@63
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@66
    move-result-object v23

    #@67
    const/16 v25, 0x1

    #@69
    move-object/from16 v0, v23

    #@6b
    move/from16 v1, v25

    #@6d
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->setGetNameAddressOnly(Z)V

    #@70
    .line 785
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@74
    move-object/from16 v23, v0

    #@76
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@79
    move-result-object v23

    #@7a
    const/16 v25, 0x64

    #@7c
    move-object/from16 v0, v23

    #@7e
    move/from16 v1, v25

    #@80
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@83
    move-result-object v20

    #@84
    .line 786
    .local v20, timeoutMsg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@86
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@88
    move-object/from16 v23, v0

    #@8a
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@8d
    move-result-object v23

    #@8e
    const-wide/16 v25, 0xbb8

    #@90
    move-object/from16 v0, v23

    #@92
    move-object/from16 v1, v20

    #@94
    move-wide/from16 v2, v25

    #@96
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@99
    .line 787
    new-instance v9, Landroid/content/Intent;

    #@9b
    const-class v23, Landroid/bluetooth/IBluetooth;

    #@9d
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@a0
    move-result-object v23

    #@a1
    move-object/from16 v0, v23

    #@a3
    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a6
    .line 788
    .local v9, i:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@a8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@aa
    move-object/from16 v23, v0

    #@ac
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1800(Lcom/android/server/BluetoothManagerService;)Landroid/content/Context;

    #@af
    move-result-object v23

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@b4
    move-object/from16 v25, v0

    #@b6
    invoke-static/range {v25 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@b9
    move-result-object v25

    #@ba
    const/16 v26, 0x1

    #@bc
    const/16 v27, -0x2

    #@be
    move-object/from16 v0, v23

    #@c0
    move-object/from16 v1, v25

    #@c2
    move/from16 v2, v26

    #@c4
    move/from16 v3, v27

    #@c6
    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@c9
    move-result v23

    #@ca
    if-nez v23, :cond_107

    #@cc
    .line 790
    move-object/from16 v0, p0

    #@ce
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@d0
    move-object/from16 v23, v0

    #@d2
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@d5
    move-result-object v23

    #@d6
    const/16 v25, 0x64

    #@d8
    move-object/from16 v0, v23

    #@da
    move/from16 v1, v25

    #@dc
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@df
    .line 791
    const-string v23, "BluetoothManagerService"

    #@e1
    new-instance v25, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v26, "fail to bind to: "

    #@e8
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v25

    #@ec
    const-class v26, Landroid/bluetooth/IBluetooth;

    #@ee
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@f1
    move-result-object v26

    #@f2
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v25

    #@f6
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v25

    #@fa
    move-object/from16 v0, v23

    #@fc
    move-object/from16 v1, v25

    #@fe
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 807
    .end local v9           #i:Landroid/content/Intent;
    .end local v20           #timeoutMsg:Landroid/os/Message;
    :goto_101
    monitor-exit v24

    #@102
    goto/16 :goto_27

    #@104
    :catchall_104
    move-exception v23

    #@105
    monitor-exit v24
    :try_end_106
    .catchall {:try_start_3a .. :try_end_106} :catchall_104

    #@106
    throw v23

    #@107
    .line 793
    .restart local v9       #i:Landroid/content/Intent;
    .restart local v20       #timeoutMsg:Landroid/os/Message;
    :cond_107
    :try_start_107
    move-object/from16 v0, p0

    #@109
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@10b
    move-object/from16 v23, v0

    #@10d
    const/16 v25, 0x1

    #@10f
    move-object/from16 v0, v23

    #@111
    move/from16 v1, v25

    #@113
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$1702(Lcom/android/server/BluetoothManagerService;Z)Z

    #@116
    goto :goto_101

    #@117
    .line 797
    .end local v9           #i:Landroid/content/Intent;
    .end local v20           #timeoutMsg:Landroid/os/Message;
    :cond_117
    move-object/from16 v0, p0

    #@119
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@11b
    move-object/from16 v23, v0

    #@11d
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@120
    move-result-object v23

    #@121
    const/16 v25, 0xc9

    #@123
    move-object/from16 v0, v23

    #@125
    move/from16 v1, v25

    #@127
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@12a
    move-result-object v17

    #@12b
    .line 798
    .local v17, saveMsg:Landroid/os/Message;
    const/16 v23, 0x0

    #@12d
    move/from16 v0, v23

    #@12f
    move-object/from16 v1, v17

    #@131
    iput v0, v1, Landroid/os/Message;->arg1:I

    #@133
    .line 799
    move-object/from16 v0, p0

    #@135
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@137
    move-object/from16 v23, v0

    #@139
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@13c
    move-result-object v23

    #@13d
    if-eqz v23, :cond_151

    #@13f
    .line 800
    move-object/from16 v0, p0

    #@141
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@143
    move-object/from16 v23, v0

    #@145
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@148
    move-result-object v23

    #@149
    move-object/from16 v0, v23

    #@14b
    move-object/from16 v1, v17

    #@14d
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@150
    goto :goto_101

    #@151
    .line 804
    :cond_151
    move-object/from16 v0, p0

    #@153
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@155
    move-object/from16 v23, v0

    #@157
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@15a
    move-result-object v23

    #@15b
    const-wide/16 v25, 0x1f4

    #@15d
    move-object/from16 v0, v23

    #@15f
    move-object/from16 v1, v17

    #@161
    move-wide/from16 v2, v25

    #@163
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_166
    .catchall {:try_start_107 .. :try_end_166} :catchall_104

    #@166
    goto :goto_101

    #@167
    .line 811
    .end local v17           #saveMsg:Landroid/os/Message;
    :sswitch_167
    const/16 v21, 0x0

    #@169
    .line 813
    .local v21, unbind:Z
    const/16 v19, 0x0

    #@16b
    .line 816
    .local v19, skipNameUpdate:Z
    const-string v23, "BluetoothManagerService"

    #@16d
    const-string v24, "MESSAGE_SAVE_NAME_AND_ADDRESS"

    #@16f
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    .line 818
    move-object/from16 v0, p0

    #@174
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@176
    move-object/from16 v23, v0

    #@178
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@17b
    move-result-object v24

    #@17c
    monitor-enter v24

    #@17d
    .line 819
    :try_start_17d
    move-object/from16 v0, p0

    #@17f
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@181
    move-object/from16 v23, v0

    #@183
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z

    #@186
    move-result v23

    #@187
    if-nez v23, :cond_1a4

    #@189
    move-object/from16 v0, p0

    #@18b
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@18d
    move-object/from16 v23, v0

    #@18f
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;
    :try_end_192
    .catchall {:try_start_17d .. :try_end_192} :catchall_270

    #@192
    move-result-object v23

    #@193
    if-eqz v23, :cond_1a4

    #@195
    .line 821
    :try_start_195
    move-object/from16 v0, p0

    #@197
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@199
    move-object/from16 v23, v0

    #@19b
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@19e
    move-result-object v23

    #@19f
    invoke-interface/range {v23 .. v23}, Landroid/bluetooth/IBluetooth;->enable()Z
    :try_end_1a2
    .catchall {:try_start_195 .. :try_end_1a2} :catchall_270
    .catch Landroid/os/RemoteException; {:try_start_195 .. :try_end_1a2} :catch_262

    #@1a2
    .line 823
    const/16 v19, 0x1

    #@1a4
    .line 829
    :cond_1a4
    :goto_1a4
    :try_start_1a4
    monitor-exit v24
    :try_end_1a5
    .catchall {:try_start_1a4 .. :try_end_1a5} :catchall_270

    #@1a5
    .line 830
    move-object/from16 v0, p0

    #@1a7
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@1a9
    move-object/from16 v23, v0

    #@1ab
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@1ae
    move-result-object v23

    #@1af
    if-eqz v23, :cond_1be

    #@1b1
    .line 831
    move-object/from16 v0, p0

    #@1b3
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@1b5
    move-object/from16 v23, v0

    #@1b7
    const/16 v24, 0x1

    #@1b9
    const/16 v25, 0x0

    #@1bb
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@1be
    .line 833
    :cond_1be
    move-object/from16 v0, p0

    #@1c0
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@1c2
    move-object/from16 v23, v0

    #@1c4
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@1c7
    move-result-object v24

    #@1c8
    monitor-enter v24

    #@1c9
    .line 834
    :try_start_1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@1cd
    move-object/from16 v23, v0

    #@1cf
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;
    :try_end_1d2
    .catchall {:try_start_1c9 .. :try_end_1d2} :catchall_281

    #@1d2
    move-result-object v23

    #@1d3
    if-eqz v23, :cond_31a

    #@1d5
    .line 835
    const/4 v10, 0x0

    #@1d6
    .line 836
    .local v10, name:Ljava/lang/String;
    const/4 v5, 0x0

    #@1d7
    .line 838
    .local v5, address:Ljava/lang/String;
    :try_start_1d7
    move-object/from16 v0, p0

    #@1d9
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@1db
    move-object/from16 v23, v0

    #@1dd
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@1e0
    move-result-object v23

    #@1e1
    invoke-interface/range {v23 .. v23}, Landroid/bluetooth/IBluetooth;->getName()Ljava/lang/String;

    #@1e4
    move-result-object v10

    #@1e5
    .line 839
    move-object/from16 v0, p0

    #@1e7
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@1e9
    move-object/from16 v23, v0

    #@1eb
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@1ee
    move-result-object v23

    #@1ef
    invoke-interface/range {v23 .. v23}, Landroid/bluetooth/IBluetooth;->getAddress()Ljava/lang/String;
    :try_end_1f2
    .catchall {:try_start_1d7 .. :try_end_1f2} :catchall_281
    .catch Landroid/os/RemoteException; {:try_start_1d7 .. :try_end_1f2} :catch_273

    #@1f2
    move-result-object v5

    #@1f3
    .line 844
    :goto_1f3
    if-eqz v10, :cond_284

    #@1f5
    if-eqz v5, :cond_284

    #@1f7
    .line 847
    :try_start_1f7
    move-object/from16 v0, p0

    #@1f9
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@1fb
    move-object/from16 v23, v0

    #@1fd
    move-object/from16 v0, v23

    #@1ff
    move/from16 v1, v19

    #@201
    invoke-static {v0, v10, v5, v1}, Lcom/android/server/BluetoothManagerService;->access$2000(Lcom/android/server/BluetoothManagerService;Ljava/lang/String;Ljava/lang/String;Z)V

    #@204
    .line 849
    move-object/from16 v0, p0

    #@206
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@208
    move-object/from16 v23, v0

    #@20a
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@20d
    move-result-object v23

    #@20e
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@211
    move-result v23

    #@212
    if-eqz v23, :cond_216

    #@214
    .line 850
    const/16 v21, 0x1

    #@216
    .line 867
    :cond_216
    :goto_216
    move-object/from16 v0, p0

    #@218
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@21a
    move-object/from16 v23, v0

    #@21c
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z
    :try_end_21f
    .catchall {:try_start_1f7 .. :try_end_21f} :catchall_281

    #@21f
    move-result v23

    #@220
    if-nez v23, :cond_22f

    #@222
    .line 869
    :try_start_222
    move-object/from16 v0, p0

    #@224
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@226
    move-object/from16 v23, v0

    #@228
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@22b
    move-result-object v23

    #@22c
    invoke-interface/range {v23 .. v23}, Landroid/bluetooth/IBluetooth;->disable()Z
    :try_end_22f
    .catchall {:try_start_222 .. :try_end_22f} :catchall_281
    .catch Landroid/os/RemoteException; {:try_start_222 .. :try_end_22f} :catch_30c

    #@22f
    .line 881
    .end local v5           #address:Ljava/lang/String;
    .end local v10           #name:Ljava/lang/String;
    :cond_22f
    :goto_22f
    :try_start_22f
    monitor-exit v24
    :try_end_230
    .catchall {:try_start_22f .. :try_end_230} :catchall_281

    #@230
    .line 882
    move-object/from16 v0, p0

    #@232
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@234
    move-object/from16 v23, v0

    #@236
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z

    #@239
    move-result v23

    #@23a
    if-nez v23, :cond_255

    #@23c
    move-object/from16 v0, p0

    #@23e
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@240
    move-object/from16 v23, v0

    #@242
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@245
    move-result-object v23

    #@246
    if-eqz v23, :cond_255

    #@248
    .line 883
    move-object/from16 v0, p0

    #@24a
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@24c
    move-object/from16 v23, v0

    #@24e
    const/16 v24, 0x0

    #@250
    const/16 v25, 0x1

    #@252
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@255
    .line 885
    :cond_255
    if-eqz v21, :cond_27

    #@257
    .line 886
    move-object/from16 v0, p0

    #@259
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@25b
    move-object/from16 v23, v0

    #@25d
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->unbindAndFinish()V

    #@260
    goto/16 :goto_27

    #@262
    .line 825
    :catch_262
    move-exception v7

    #@263
    .line 826
    .local v7, e:Landroid/os/RemoteException;
    :try_start_263
    const-string v23, "BluetoothManagerService"

    #@265
    const-string v25, "Unable to call enable()"

    #@267
    move-object/from16 v0, v23

    #@269
    move-object/from16 v1, v25

    #@26b
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26e
    goto/16 :goto_1a4

    #@270
    .line 829
    .end local v7           #e:Landroid/os/RemoteException;
    :catchall_270
    move-exception v23

    #@271
    monitor-exit v24
    :try_end_272
    .catchall {:try_start_263 .. :try_end_272} :catchall_270

    #@272
    throw v23

    #@273
    .line 840
    .restart local v5       #address:Ljava/lang/String;
    .restart local v10       #name:Ljava/lang/String;
    :catch_273
    move-exception v13

    #@274
    .line 841
    .local v13, re:Landroid/os/RemoteException;
    :try_start_274
    const-string v23, "BluetoothManagerService"

    #@276
    const-string v25, ""

    #@278
    move-object/from16 v0, v23

    #@27a
    move-object/from16 v1, v25

    #@27c
    invoke-static {v0, v1, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27f
    goto/16 :goto_1f3

    #@281
    .line 881
    .end local v5           #address:Ljava/lang/String;
    .end local v10           #name:Ljava/lang/String;
    .end local v13           #re:Landroid/os/RemoteException;
    :catchall_281
    move-exception v23

    #@282
    monitor-exit v24
    :try_end_283
    .catchall {:try_start_274 .. :try_end_283} :catchall_281

    #@283
    throw v23

    #@284
    .line 853
    .restart local v5       #address:Ljava/lang/String;
    .restart local v10       #name:Ljava/lang/String;
    :cond_284
    :try_start_284
    move-object/from16 v0, p1

    #@286
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@288
    move/from16 v23, v0

    #@28a
    const/16 v25, 0x3

    #@28c
    move/from16 v0, v23

    #@28e
    move/from16 v1, v25

    #@290
    if-ge v0, v1, :cond_2ed

    #@292
    .line 854
    move-object/from16 v0, p0

    #@294
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@296
    move-object/from16 v23, v0

    #@298
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@29b
    move-result-object v23

    #@29c
    const/16 v25, 0xc9

    #@29e
    move-object/from16 v0, v23

    #@2a0
    move/from16 v1, v25

    #@2a2
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@2a5
    move-result-object v16

    #@2a6
    .line 855
    .local v16, retryMsg:Landroid/os/Message;
    move-object/from16 v0, p1

    #@2a8
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@2aa
    move/from16 v23, v0

    #@2ac
    add-int/lit8 v23, v23, 0x1

    #@2ae
    move/from16 v0, v23

    #@2b0
    move-object/from16 v1, v16

    #@2b2
    iput v0, v1, Landroid/os/Message;->arg1:I

    #@2b4
    .line 857
    const-string v23, "BluetoothManagerService"

    #@2b6
    new-instance v25, Ljava/lang/StringBuilder;

    #@2b8
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@2bb
    const-string v26, "Retrying name/address remote retrieval and save.....Retry count ="

    #@2bd
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c0
    move-result-object v25

    #@2c1
    move-object/from16 v0, v16

    #@2c3
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@2c5
    move/from16 v26, v0

    #@2c7
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v25

    #@2cb
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ce
    move-result-object v25

    #@2cf
    move-object/from16 v0, v23

    #@2d1
    move-object/from16 v1, v25

    #@2d3
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d6
    .line 859
    move-object/from16 v0, p0

    #@2d8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@2da
    move-object/from16 v23, v0

    #@2dc
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@2df
    move-result-object v23

    #@2e0
    const-wide/16 v25, 0x1f4

    #@2e2
    move-object/from16 v0, v23

    #@2e4
    move-object/from16 v1, v16

    #@2e6
    move-wide/from16 v2, v25

    #@2e8
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2eb
    goto/16 :goto_216

    #@2ed
    .line 861
    .end local v16           #retryMsg:Landroid/os/Message;
    :cond_2ed
    const-string v23, "BluetoothManagerService"

    #@2ef
    const-string v25, "Maximum name/address remote retrieval retry exceeded"

    #@2f1
    move-object/from16 v0, v23

    #@2f3
    move-object/from16 v1, v25

    #@2f5
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f8
    .line 862
    move-object/from16 v0, p0

    #@2fa
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@2fc
    move-object/from16 v23, v0

    #@2fe
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@301
    move-result-object v23

    #@302
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@305
    move-result v23

    #@306
    if-eqz v23, :cond_216

    #@308
    .line 863
    const/16 v21, 0x1

    #@30a
    goto/16 :goto_216

    #@30c
    .line 870
    :catch_30c
    move-exception v7

    #@30d
    .line 871
    .restart local v7       #e:Landroid/os/RemoteException;
    const-string v23, "BluetoothManagerService"

    #@30f
    const-string v25, "Unable to call disable()"

    #@311
    move-object/from16 v0, v23

    #@313
    move-object/from16 v1, v25

    #@315
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@318
    goto/16 :goto_22f

    #@31a
    .line 878
    .end local v5           #address:Ljava/lang/String;
    .end local v7           #e:Landroid/os/RemoteException;
    .end local v10           #name:Ljava/lang/String;
    :cond_31a
    move-object/from16 v0, p0

    #@31c
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@31e
    move-object/from16 v23, v0

    #@320
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@323
    move-result-object v23

    #@324
    const/16 v25, 0xc8

    #@326
    move-object/from16 v0, v23

    #@328
    move/from16 v1, v25

    #@32a
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@32d
    move-result-object v8

    #@32e
    .line 879
    .local v8, getMsg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@330
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@332
    move-object/from16 v23, v0

    #@334
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@337
    move-result-object v23

    #@338
    move-object/from16 v0, v23

    #@33a
    invoke-virtual {v0, v8}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_33d
    .catchall {:try_start_284 .. :try_end_33d} :catchall_281

    #@33d
    goto/16 :goto_22f

    #@33f
    .line 892
    .end local v8           #getMsg:Landroid/os/Message;
    .end local v19           #skipNameUpdate:Z
    .end local v21           #unbind:Z
    :sswitch_33f
    const-string v23, "BluetoothManagerService"

    #@341
    new-instance v24, Ljava/lang/StringBuilder;

    #@343
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@346
    const-string v25, "MESSAGE_ENABLE: mBluetooth = "

    #@348
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34b
    move-result-object v24

    #@34c
    move-object/from16 v0, p0

    #@34e
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@350
    move-object/from16 v25, v0

    #@352
    invoke-static/range {v25 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@355
    move-result-object v25

    #@356
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v24

    #@35a
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35d
    move-result-object v24

    #@35e
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@361
    .line 906
    move-object/from16 v0, p0

    #@363
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@365
    move-object/from16 v23, v0

    #@367
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@36a
    move-result-object v23

    #@36b
    const/16 v24, 0x2a

    #@36d
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@370
    .line 907
    move-object/from16 v0, p0

    #@372
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@374
    move-object/from16 v23, v0

    #@376
    const/16 v24, 0x1

    #@378
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$102(Lcom/android/server/BluetoothManagerService;Z)Z

    #@37b
    .line 908
    move-object/from16 v0, p0

    #@37d
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@37f
    move-object/from16 v24, v0

    #@381
    move-object/from16 v0, p1

    #@383
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@385
    move/from16 v23, v0

    #@387
    const/16 v25, 0x1

    #@389
    move/from16 v0, v23

    #@38b
    move/from16 v1, v25

    #@38d
    if-ne v0, v1, :cond_39a

    #@38f
    const/16 v23, 0x1

    #@391
    :goto_391
    move-object/from16 v0, v24

    #@393
    move/from16 v1, v23

    #@395
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$2100(Lcom/android/server/BluetoothManagerService;Z)V

    #@398
    goto/16 :goto_27

    #@39a
    :cond_39a
    const/16 v23, 0x0

    #@39c
    goto :goto_391

    #@39d
    .line 912
    :sswitch_39d
    move-object/from16 v0, p0

    #@39f
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3a1
    move-object/from16 v23, v0

    #@3a3
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@3a6
    move-result-object v23

    #@3a7
    const/16 v24, 0x2a

    #@3a9
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@3ac
    .line 913
    move-object/from16 v0, p0

    #@3ae
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3b0
    move-object/from16 v23, v0

    #@3b2
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z

    #@3b5
    move-result v23

    #@3b6
    if-eqz v23, :cond_3f4

    #@3b8
    move-object/from16 v0, p0

    #@3ba
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3bc
    move-object/from16 v23, v0

    #@3be
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@3c1
    move-result-object v23

    #@3c2
    if-eqz v23, :cond_3f4

    #@3c4
    .line 914
    move-object/from16 v0, p0

    #@3c6
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3c8
    move-object/from16 v23, v0

    #@3ca
    const/16 v24, 0x1

    #@3cc
    const/16 v25, 0x0

    #@3ce
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@3d1
    .line 915
    move-object/from16 v0, p0

    #@3d3
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3d5
    move-object/from16 v23, v0

    #@3d7
    const/16 v24, 0x0

    #@3d9
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$102(Lcom/android/server/BluetoothManagerService;Z)Z

    #@3dc
    .line 916
    move-object/from16 v0, p0

    #@3de
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3e0
    move-object/from16 v23, v0

    #@3e2
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2200(Lcom/android/server/BluetoothManagerService;)V

    #@3e5
    .line 917
    move-object/from16 v0, p0

    #@3e7
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3e9
    move-object/from16 v23, v0

    #@3eb
    const/16 v24, 0x0

    #@3ed
    const/16 v25, 0x0

    #@3ef
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@3f2
    goto/16 :goto_27

    #@3f4
    .line 919
    :cond_3f4
    move-object/from16 v0, p0

    #@3f6
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3f8
    move-object/from16 v23, v0

    #@3fa
    const/16 v24, 0x0

    #@3fc
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$102(Lcom/android/server/BluetoothManagerService;Z)Z

    #@3ff
    .line 920
    move-object/from16 v0, p0

    #@401
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@403
    move-object/from16 v23, v0

    #@405
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2200(Lcom/android/server/BluetoothManagerService;)V

    #@408
    goto/16 :goto_27

    #@40a
    .line 926
    :sswitch_40a
    move-object/from16 v0, p1

    #@40c
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@40e
    check-cast v6, Landroid/bluetooth/IBluetoothManagerCallback;

    #@410
    .line 927
    .local v6, callback:Landroid/bluetooth/IBluetoothManagerCallback;
    move-object/from16 v0, p0

    #@412
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@414
    move-object/from16 v23, v0

    #@416
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2300(Lcom/android/server/BluetoothManagerService;)Landroid/os/RemoteCallbackList;

    #@419
    move-result-object v23

    #@41a
    move-object/from16 v0, v23

    #@41c
    invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@41f
    move-result v4

    #@420
    .line 928
    .local v4, added:Z
    const-string v23, "BluetoothManagerService"

    #@422
    new-instance v24, Ljava/lang/StringBuilder;

    #@424
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@427
    const-string v25, "Added callback: "

    #@429
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42c
    move-result-object v24

    #@42d
    if-nez v6, :cond_431

    #@42f
    const-string v6, "null"

    #@431
    .end local v6           #callback:Landroid/bluetooth/IBluetoothManagerCallback;
    :cond_431
    move-object/from16 v0, v24

    #@433
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@436
    move-result-object v24

    #@437
    const-string v25, ":"

    #@439
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43c
    move-result-object v24

    #@43d
    move-object/from16 v0, v24

    #@43f
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@442
    move-result-object v24

    #@443
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@446
    move-result-object v24

    #@447
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44a
    goto/16 :goto_27

    #@44c
    .line 933
    .end local v4           #added:Z
    :sswitch_44c
    move-object/from16 v0, p1

    #@44e
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@450
    check-cast v6, Landroid/bluetooth/IBluetoothManagerCallback;

    #@452
    .line 934
    .restart local v6       #callback:Landroid/bluetooth/IBluetoothManagerCallback;
    move-object/from16 v0, p0

    #@454
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@456
    move-object/from16 v23, v0

    #@458
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2300(Lcom/android/server/BluetoothManagerService;)Landroid/os/RemoteCallbackList;

    #@45b
    move-result-object v23

    #@45c
    move-object/from16 v0, v23

    #@45e
    invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@461
    move-result v14

    #@462
    .line 935
    .local v14, removed:Z
    const-string v23, "BluetoothManagerService"

    #@464
    new-instance v24, Ljava/lang/StringBuilder;

    #@466
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@469
    const-string v25, "Removed callback: "

    #@46b
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46e
    move-result-object v24

    #@46f
    if-nez v6, :cond_473

    #@471
    const-string v6, "null"

    #@473
    .end local v6           #callback:Landroid/bluetooth/IBluetoothManagerCallback;
    :cond_473
    move-object/from16 v0, v24

    #@475
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@478
    move-result-object v24

    #@479
    const-string v25, ":"

    #@47b
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47e
    move-result-object v24

    #@47f
    move-object/from16 v0, v24

    #@481
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@484
    move-result-object v24

    #@485
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@488
    move-result-object v24

    #@489
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48c
    goto/16 :goto_27

    #@48e
    .line 941
    .end local v14           #removed:Z
    :sswitch_48e
    const-string v23, "BluetoothManagerService"

    #@490
    new-instance v24, Ljava/lang/StringBuilder;

    #@492
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@495
    const-string v25, "MESSAGE_ENABLE_RADIO: mBluetooth = "

    #@497
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49a
    move-result-object v24

    #@49b
    move-object/from16 v0, p0

    #@49d
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@49f
    move-object/from16 v25, v0

    #@4a1
    invoke-static/range {v25 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@4a4
    move-result-object v25

    #@4a5
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a8
    move-result-object v24

    #@4a9
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4ac
    move-result-object v24

    #@4ad
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b0
    .line 943
    move-object/from16 v0, p0

    #@4b2
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@4b4
    move-object/from16 v23, v0

    #@4b6
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2400(Lcom/android/server/BluetoothManagerService;)V

    #@4b9
    goto/16 :goto_27

    #@4bb
    .line 947
    :sswitch_4bb
    move-object/from16 v0, p0

    #@4bd
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@4bf
    move-object/from16 v23, v0

    #@4c1
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2500(Lcom/android/server/BluetoothManagerService;)V

    #@4c4
    goto/16 :goto_27

    #@4c6
    .line 953
    :sswitch_4c6
    move-object/from16 v0, p1

    #@4c8
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4ca
    check-cast v6, Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@4cc
    .line 954
    .local v6, callback:Landroid/bluetooth/IBluetoothStateChangeCallback;
    move-object/from16 v0, p0

    #@4ce
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@4d0
    move-object/from16 v23, v0

    #@4d2
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2600(Lcom/android/server/BluetoothManagerService;)Landroid/os/RemoteCallbackList;

    #@4d5
    move-result-object v23

    #@4d6
    move-object/from16 v0, v23

    #@4d8
    invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@4db
    goto/16 :goto_27

    #@4dd
    .line 959
    .end local v6           #callback:Landroid/bluetooth/IBluetoothStateChangeCallback;
    :sswitch_4dd
    move-object/from16 v0, p1

    #@4df
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4e1
    check-cast v6, Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@4e3
    .line 964
    .restart local v6       #callback:Landroid/bluetooth/IBluetoothStateChangeCallback;
    if-eqz v6, :cond_27

    #@4e5
    .line 965
    move-object/from16 v0, p0

    #@4e7
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@4e9
    move-object/from16 v23, v0

    #@4eb
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2600(Lcom/android/server/BluetoothManagerService;)Landroid/os/RemoteCallbackList;

    #@4ee
    move-result-object v23

    #@4ef
    move-object/from16 v0, v23

    #@4f1
    invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@4f4
    goto/16 :goto_27

    #@4f6
    .line 973
    .end local v6           #callback:Landroid/bluetooth/IBluetoothStateChangeCallback;
    :sswitch_4f6
    const-string v23, "BluetoothManagerService"

    #@4f8
    const-string v24, "MESSAGE_BLUETOOTH_SERVICE_CONNECTED"

    #@4fa
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4fd
    .line 977
    move-object/from16 v0, p0

    #@4ff
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@501
    move-object/from16 v23, v0

    #@503
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@506
    move-result-object v23

    #@507
    const/16 v24, 0x64

    #@509
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@50c
    .line 979
    move-object/from16 v0, p1

    #@50e
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@510
    move-object/from16 v18, v0

    #@512
    check-cast v18, Landroid/os/IBinder;

    #@514
    .line 980
    .local v18, service:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@516
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@518
    move-object/from16 v23, v0

    #@51a
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@51d
    move-result-object v24

    #@51e
    monitor-enter v24

    #@51f
    .line 981
    :try_start_51f
    move-object/from16 v0, p0

    #@521
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@523
    move-object/from16 v23, v0

    #@525
    const/16 v25, 0x0

    #@527
    move-object/from16 v0, v23

    #@529
    move/from16 v1, v25

    #@52b
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$1702(Lcom/android/server/BluetoothManagerService;Z)Z

    #@52e
    .line 982
    move-object/from16 v0, p0

    #@530
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@532
    move-object/from16 v23, v0

    #@534
    invoke-static/range {v18 .. v18}, Landroid/bluetooth/IBluetooth$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetooth;

    #@537
    move-result-object v25

    #@538
    move-object/from16 v0, v23

    #@53a
    move-object/from16 v1, v25

    #@53c
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$1602(Lcom/android/server/BluetoothManagerService;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    #@53f
    .line 984
    move-object/from16 v0, p0

    #@541
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@543
    move-object/from16 v23, v0

    #@545
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@548
    move-result-object v23

    #@549
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@54c
    move-result v23

    #@54d
    if-eqz v23, :cond_584

    #@54f
    .line 986
    move-object/from16 v0, p0

    #@551
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@553
    move-object/from16 v23, v0

    #@555
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@558
    move-result-object v23

    #@559
    const/16 v25, 0xc8

    #@55b
    move-object/from16 v0, v23

    #@55d
    move/from16 v1, v25

    #@55f
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@562
    move-result-object v8

    #@563
    .line 987
    .restart local v8       #getMsg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@565
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@567
    move-object/from16 v23, v0

    #@569
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@56c
    move-result-object v23

    #@56d
    move-object/from16 v0, v23

    #@56f
    invoke-virtual {v0, v8}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@572
    .line 988
    move-object/from16 v0, p0

    #@574
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@576
    move-object/from16 v23, v0

    #@578
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z

    #@57b
    move-result v23

    #@57c
    if-nez v23, :cond_584

    #@57e
    .line 989
    monitor-exit v24

    #@57f
    goto/16 :goto_27

    #@581
    .line 1035
    .end local v8           #getMsg:Landroid/os/Message;
    :catchall_581
    move-exception v23

    #@582
    monitor-exit v24
    :try_end_583
    .catchall {:try_start_51f .. :try_end_583} :catchall_581

    #@583
    throw v23

    #@584
    .line 993
    :cond_584
    :try_start_584
    move-object/from16 v0, p0

    #@586
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@588
    move-object/from16 v23, v0

    #@58a
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@58d
    move-result-object v23

    #@58e
    const/16 v25, 0x0

    #@590
    move-object/from16 v0, v23

    #@592
    move/from16 v1, v25

    #@594
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->setGetNameAddressOnly(Z)V
    :try_end_597
    .catchall {:try_start_584 .. :try_end_597} :catchall_581

    #@597
    .line 996
    :try_start_597
    move-object/from16 v0, p0

    #@599
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@59b
    move-object/from16 v23, v0

    #@59d
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@5a0
    move-result-object v23

    #@5a1
    move-object/from16 v0, p0

    #@5a3
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@5a5
    move-object/from16 v25, v0

    #@5a7
    invoke-static/range {v25 .. v25}, Lcom/android/server/BluetoothManagerService;->access$2700(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetoothCallback;

    #@5aa
    move-result-object v25

    #@5ab
    move-object/from16 v0, v23

    #@5ad
    move-object/from16 v1, v25

    #@5af
    invoke-interface {v0, v1}, Landroid/bluetooth/IBluetooth;->registerCallback(Landroid/bluetooth/IBluetoothCallback;)V
    :try_end_5b2
    .catchall {:try_start_597 .. :try_end_5b2} :catchall_581
    .catch Landroid/os/RemoteException; {:try_start_597 .. :try_end_5b2} :catch_608

    #@5b2
    .line 1001
    :goto_5b2
    :try_start_5b2
    move-object/from16 v0, p0

    #@5b4
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@5b6
    move-object/from16 v23, v0

    #@5b8
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2800(Lcom/android/server/BluetoothManagerService;)V

    #@5bb
    .line 1005
    invoke-static {}, Lcom/android/server/BluetoothManagerService;->access$000()Z

    #@5be
    move-result v23

    #@5bf
    const/16 v25, 0x1

    #@5c1
    move/from16 v0, v23

    #@5c3
    move/from16 v1, v25

    #@5c5
    if-ne v0, v1, :cond_622

    #@5c7
    .line 1006
    move-object/from16 v0, p0

    #@5c9
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@5cb
    move-object/from16 v23, v0

    #@5cd
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@5d0
    move-result-object v23

    #@5d1
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isTurnOnRadio()Z
    :try_end_5d4
    .catchall {:try_start_5b2 .. :try_end_5d4} :catchall_581

    #@5d4
    move-result v23

    #@5d5
    if-eqz v23, :cond_622

    #@5d7
    .line 1008
    :try_start_5d7
    move-object/from16 v0, p0

    #@5d9
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@5db
    move-object/from16 v23, v0

    #@5dd
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@5e0
    move-result-object v23

    #@5e1
    invoke-interface/range {v23 .. v23}, Landroid/bluetooth/IBluetooth;->enableRadio()Z

    #@5e4
    move-result v23

    #@5e5
    if-nez v23, :cond_5f2

    #@5e7
    .line 1009
    const-string v23, "BluetoothManagerService"

    #@5e9
    const-string v25, "IBluetooth.enableRadio() returned false"

    #@5eb
    move-object/from16 v0, v23

    #@5ed
    move-object/from16 v1, v25

    #@5ef
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5f2
    .line 1011
    :cond_5f2
    move-object/from16 v0, p0

    #@5f4
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@5f6
    move-object/from16 v23, v0

    #@5f8
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@5fb
    move-result-object v23

    #@5fc
    const/16 v25, 0x0

    #@5fe
    move-object/from16 v0, v23

    #@600
    move/from16 v1, v25

    #@602
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->setTurnOnRadio(Z)V
    :try_end_605
    .catchall {:try_start_5d7 .. :try_end_605} :catchall_581
    .catch Landroid/os/RemoteException; {:try_start_5d7 .. :try_end_605} :catch_615

    #@605
    .line 1016
    :goto_605
    :try_start_605
    monitor-exit v24

    #@606
    goto/16 :goto_27

    #@608
    .line 997
    :catch_608
    move-exception v13

    #@609
    .line 998
    .restart local v13       #re:Landroid/os/RemoteException;
    const-string v23, "BluetoothManagerService"

    #@60b
    const-string v25, "Unable to register BluetoothCallback"

    #@60d
    move-object/from16 v0, v23

    #@60f
    move-object/from16 v1, v25

    #@611
    invoke-static {v0, v1, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@614
    goto :goto_5b2

    #@615
    .line 1013
    .end local v13           #re:Landroid/os/RemoteException;
    :catch_615
    move-exception v7

    #@616
    .line 1014
    .restart local v7       #e:Landroid/os/RemoteException;
    const-string v23, "BluetoothManagerService"

    #@618
    const-string v25, "Unable to call enableRadio()"

    #@61a
    move-object/from16 v0, v23

    #@61c
    move-object/from16 v1, v25

    #@61e
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_621
    .catchall {:try_start_605 .. :try_end_621} :catchall_581

    #@621
    goto :goto_605

    #@622
    .line 1021
    .end local v7           #e:Landroid/os/RemoteException;
    :cond_622
    :try_start_622
    move-object/from16 v0, p0

    #@624
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@626
    move-object/from16 v23, v0

    #@628
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2900(Lcom/android/server/BluetoothManagerService;)Z

    #@62b
    move-result v23

    #@62c
    if-nez v23, :cond_67b

    #@62e
    .line 1022
    move-object/from16 v0, p0

    #@630
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@632
    move-object/from16 v23, v0

    #@634
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@637
    move-result-object v23

    #@638
    invoke-interface/range {v23 .. v23}, Landroid/bluetooth/IBluetooth;->enable()Z

    #@63b
    move-result v23

    #@63c
    if-nez v23, :cond_649

    #@63e
    .line 1023
    const-string v23, "BluetoothManagerService"

    #@640
    const-string v25, "IBluetooth.enable() returned false"

    #@642
    move-object/from16 v0, v23

    #@644
    move-object/from16 v1, v25

    #@646
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_649
    .catchall {:try_start_622 .. :try_end_649} :catchall_581
    .catch Landroid/os/RemoteException; {:try_start_622 .. :try_end_649} :catch_697

    #@649
    .line 1035
    :cond_649
    :goto_649
    :try_start_649
    monitor-exit v24
    :try_end_64a
    .catchall {:try_start_649 .. :try_end_64a} :catchall_581

    #@64a
    .line 1037
    move-object/from16 v0, p0

    #@64c
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@64e
    move-object/from16 v23, v0

    #@650
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z

    #@653
    move-result v23

    #@654
    if-nez v23, :cond_27

    #@656
    .line 1038
    move-object/from16 v0, p0

    #@658
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@65a
    move-object/from16 v23, v0

    #@65c
    const/16 v24, 0x1

    #@65e
    const/16 v25, 0x0

    #@660
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@663
    .line 1039
    move-object/from16 v0, p0

    #@665
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@667
    move-object/from16 v23, v0

    #@669
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2200(Lcom/android/server/BluetoothManagerService;)V

    #@66c
    .line 1040
    move-object/from16 v0, p0

    #@66e
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@670
    move-object/from16 v23, v0

    #@672
    const/16 v24, 0x0

    #@674
    const/16 v25, 0x0

    #@676
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@679
    goto/16 :goto_27

    #@67b
    .line 1028
    :cond_67b
    :try_start_67b
    move-object/from16 v0, p0

    #@67d
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@67f
    move-object/from16 v23, v0

    #@681
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@684
    move-result-object v23

    #@685
    invoke-interface/range {v23 .. v23}, Landroid/bluetooth/IBluetooth;->enableNoAutoConnect()Z

    #@688
    move-result v23

    #@689
    if-nez v23, :cond_649

    #@68b
    .line 1029
    const-string v23, "BluetoothManagerService"

    #@68d
    const-string v25, "IBluetooth.enableNoAutoConnect() returned false"

    #@68f
    move-object/from16 v0, v23

    #@691
    move-object/from16 v1, v25

    #@693
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_696
    .catchall {:try_start_67b .. :try_end_696} :catchall_581
    .catch Landroid/os/RemoteException; {:try_start_67b .. :try_end_696} :catch_697

    #@696
    goto :goto_649

    #@697
    .line 1032
    :catch_697
    move-exception v7

    #@698
    .line 1033
    .restart local v7       #e:Landroid/os/RemoteException;
    :try_start_698
    const-string v23, "BluetoothManagerService"

    #@69a
    const-string v25, "Unable to call enable()"

    #@69c
    move-object/from16 v0, v23

    #@69e
    move-object/from16 v1, v25

    #@6a0
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6a3
    .catchall {:try_start_698 .. :try_end_6a3} :catchall_581

    #@6a3
    goto :goto_649

    #@6a4
    .line 1045
    .end local v7           #e:Landroid/os/RemoteException;
    .end local v18           #service:Landroid/os/IBinder;
    :sswitch_6a4
    const-string v23, "BluetoothManagerService"

    #@6a6
    const-string v24, "MESSAGE_TIMEOUT_BIND"

    #@6a8
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6ab
    .line 1046
    move-object/from16 v0, p0

    #@6ad
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@6af
    move-object/from16 v23, v0

    #@6b1
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@6b4
    move-result-object v24

    #@6b5
    monitor-enter v24

    #@6b6
    .line 1047
    :try_start_6b6
    move-object/from16 v0, p0

    #@6b8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@6ba
    move-object/from16 v23, v0

    #@6bc
    const/16 v25, 0x0

    #@6be
    move-object/from16 v0, v23

    #@6c0
    move/from16 v1, v25

    #@6c2
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$1702(Lcom/android/server/BluetoothManagerService;Z)Z

    #@6c5
    .line 1048
    monitor-exit v24

    #@6c6
    goto/16 :goto_27

    #@6c8
    :catchall_6c8
    move-exception v23

    #@6c9
    monitor-exit v24
    :try_end_6ca
    .catchall {:try_start_6b6 .. :try_end_6ca} :catchall_6c8

    #@6ca
    throw v23

    #@6cb
    .line 1053
    :sswitch_6cb
    move-object/from16 v0, p1

    #@6cd
    iget v12, v0, Landroid/os/Message;->arg1:I

    #@6cf
    .line 1054
    .local v12, prevState:I
    move-object/from16 v0, p1

    #@6d1
    iget v11, v0, Landroid/os/Message;->arg2:I

    #@6d3
    .line 1056
    .local v11, newState:I
    const-string v23, "BluetoothManagerService"

    #@6d5
    new-instance v24, Ljava/lang/StringBuilder;

    #@6d7
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@6da
    const-string v25, "MESSAGE_BLUETOOTH_STATE_CHANGE: prevState = "

    #@6dc
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6df
    move-result-object v24

    #@6e0
    move-object/from16 v0, v24

    #@6e2
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e5
    move-result-object v24

    #@6e6
    const-string v25, ", newState="

    #@6e8
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6eb
    move-result-object v24

    #@6ec
    move-object/from16 v0, v24

    #@6ee
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f1
    move-result-object v24

    #@6f2
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f5
    move-result-object v24

    #@6f6
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f9
    .line 1058
    move-object/from16 v0, p0

    #@6fb
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@6fd
    move-object/from16 v23, v0

    #@6ff
    move-object/from16 v0, v23

    #@701
    invoke-static {v0, v11}, Lcom/android/server/BluetoothManagerService;->access$3002(Lcom/android/server/BluetoothManagerService;I)I

    #@704
    .line 1059
    move-object/from16 v0, p0

    #@706
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@708
    move-object/from16 v23, v0

    #@70a
    move-object/from16 v0, v23

    #@70c
    invoke-static {v0, v12, v11}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@70f
    goto/16 :goto_27

    #@711
    .line 1064
    .end local v11           #newState:I
    .end local v12           #prevState:I
    :sswitch_711
    const-string v23, "BluetoothManagerService"

    #@713
    const-string v24, "MESSAGE_BLUETOOTH_SERVICE_DISCONNECTED"

    #@715
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@718
    .line 1065
    move-object/from16 v0, p0

    #@71a
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@71c
    move-object/from16 v23, v0

    #@71e
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@721
    move-result-object v24

    #@722
    monitor-enter v24

    #@723
    .line 1067
    :try_start_723
    move-object/from16 v0, p0

    #@725
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@727
    move-object/from16 v23, v0

    #@729
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@72c
    move-result-object v23

    #@72d
    if-nez v23, :cond_735

    #@72f
    .line 1068
    monitor-exit v24

    #@730
    goto/16 :goto_27

    #@732
    .line 1071
    :catchall_732
    move-exception v23

    #@733
    monitor-exit v24
    :try_end_734
    .catchall {:try_start_723 .. :try_end_734} :catchall_732

    #@734
    throw v23

    #@735
    .line 1070
    :cond_735
    :try_start_735
    move-object/from16 v0, p0

    #@737
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@739
    move-object/from16 v23, v0

    #@73b
    const/16 v25, 0x0

    #@73d
    move-object/from16 v0, v23

    #@73f
    move-object/from16 v1, v25

    #@741
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$1602(Lcom/android/server/BluetoothManagerService;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    #@744
    .line 1071
    monitor-exit v24
    :try_end_745
    .catchall {:try_start_735 .. :try_end_745} :catchall_732

    #@745
    .line 1073
    move-object/from16 v0, p0

    #@747
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@749
    move-object/from16 v23, v0

    #@74b
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z

    #@74e
    move-result v23

    #@74f
    if-eqz v23, :cond_77f

    #@751
    .line 1074
    move-object/from16 v0, p0

    #@753
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@755
    move-object/from16 v23, v0

    #@757
    const/16 v24, 0x0

    #@759
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$102(Lcom/android/server/BluetoothManagerService;Z)Z

    #@75c
    .line 1076
    move-object/from16 v0, p0

    #@75e
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@760
    move-object/from16 v23, v0

    #@762
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@765
    move-result-object v23

    #@766
    const/16 v24, 0x2a

    #@768
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@76b
    move-result-object v15

    #@76c
    .line 1078
    .local v15, restartMsg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@76e
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@770
    move-object/from16 v23, v0

    #@772
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@775
    move-result-object v23

    #@776
    const-wide/16 v24, 0xc8

    #@778
    move-object/from16 v0, v23

    #@77a
    move-wide/from16 v1, v24

    #@77c
    invoke-virtual {v0, v15, v1, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@77f
    .line 1082
    .end local v15           #restartMsg:Landroid/os/Message;
    :cond_77f
    move-object/from16 v0, p0

    #@781
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@783
    move-object/from16 v23, v0

    #@785
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@788
    move-result-object v23

    #@789
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@78c
    move-result v23

    #@78d
    if-nez v23, :cond_27

    #@78f
    .line 1083
    move-object/from16 v0, p0

    #@791
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@793
    move-object/from16 v23, v0

    #@795
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3200(Lcom/android/server/BluetoothManagerService;)V

    #@798
    .line 1087
    move-object/from16 v0, p0

    #@79a
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@79c
    move-object/from16 v23, v0

    #@79e
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@7a1
    move-result v23

    #@7a2
    const/16 v24, 0xb

    #@7a4
    move/from16 v0, v23

    #@7a6
    move/from16 v1, v24

    #@7a8
    if-eq v0, v1, :cond_7bc

    #@7aa
    move-object/from16 v0, p0

    #@7ac
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7ae
    move-object/from16 v23, v0

    #@7b0
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@7b3
    move-result v23

    #@7b4
    const/16 v24, 0xc

    #@7b6
    move/from16 v0, v23

    #@7b8
    move/from16 v1, v24

    #@7ba
    if-ne v0, v1, :cond_7d4

    #@7bc
    .line 1089
    :cond_7bc
    move-object/from16 v0, p0

    #@7be
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7c0
    move-object/from16 v23, v0

    #@7c2
    const/16 v24, 0xc

    #@7c4
    const/16 v25, 0xd

    #@7c6
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@7c9
    .line 1091
    move-object/from16 v0, p0

    #@7cb
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7cd
    move-object/from16 v23, v0

    #@7cf
    const/16 v24, 0xd

    #@7d1
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3002(Lcom/android/server/BluetoothManagerService;I)I

    #@7d4
    .line 1093
    :cond_7d4
    move-object/from16 v0, p0

    #@7d6
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7d8
    move-object/from16 v23, v0

    #@7da
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@7dd
    move-result v23

    #@7de
    const/16 v24, 0xd

    #@7e0
    move/from16 v0, v23

    #@7e2
    move/from16 v1, v24

    #@7e4
    if-ne v0, v1, :cond_7f3

    #@7e6
    .line 1094
    move-object/from16 v0, p0

    #@7e8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7ea
    move-object/from16 v23, v0

    #@7ec
    const/16 v24, 0xd

    #@7ee
    const/16 v25, 0xa

    #@7f0
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@7f3
    .line 1098
    :cond_7f3
    move-object/from16 v0, p0

    #@7f5
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7f7
    move-object/from16 v23, v0

    #@7f9
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@7fc
    move-result-object v23

    #@7fd
    const/16 v24, 0x3c

    #@7ff
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@802
    .line 1099
    move-object/from16 v0, p0

    #@804
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@806
    move-object/from16 v23, v0

    #@808
    const/16 v24, 0xa

    #@80a
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3002(Lcom/android/server/BluetoothManagerService;I)I

    #@80d
    goto/16 :goto_27

    #@80f
    .line 1105
    :sswitch_80f
    const-string v23, "BluetoothManagerService"

    #@811
    const-string v24, "MESSAGE_RESTART_BLUETOOTH_SERVICE: Restart IBluetooth service"

    #@813
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@816
    .line 1110
    move-object/from16 v0, p0

    #@818
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@81a
    move-object/from16 v23, v0

    #@81c
    const/16 v24, 0x1

    #@81e
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$102(Lcom/android/server/BluetoothManagerService;Z)Z

    #@821
    .line 1111
    move-object/from16 v0, p0

    #@823
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@825
    move-object/from16 v23, v0

    #@827
    move-object/from16 v0, p0

    #@829
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@82b
    move-object/from16 v24, v0

    #@82d
    invoke-static/range {v24 .. v24}, Lcom/android/server/BluetoothManagerService;->access$2900(Lcom/android/server/BluetoothManagerService;)Z

    #@830
    move-result v24

    #@831
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$2100(Lcom/android/server/BluetoothManagerService;Z)V

    #@834
    goto/16 :goto_27

    #@836
    .line 1117
    :sswitch_836
    const-string v23, "BluetoothManagerService"

    #@838
    const-string v24, "MESSAGE_TIMEOUT_UNBIND"

    #@83a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@83d
    .line 1118
    move-object/from16 v0, p0

    #@83f
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@841
    move-object/from16 v23, v0

    #@843
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@846
    move-result-object v24

    #@847
    monitor-enter v24

    #@848
    .line 1119
    :try_start_848
    move-object/from16 v0, p0

    #@84a
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@84c
    move-object/from16 v23, v0

    #@84e
    const/16 v25, 0x0

    #@850
    move-object/from16 v0, v23

    #@852
    move/from16 v1, v25

    #@854
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$3302(Lcom/android/server/BluetoothManagerService;Z)Z

    #@857
    .line 1120
    monitor-exit v24

    #@858
    goto/16 :goto_27

    #@85a
    :catchall_85a
    move-exception v23

    #@85b
    monitor-exit v24
    :try_end_85c
    .catchall {:try_start_848 .. :try_end_85c} :catchall_85a

    #@85c
    throw v23

    #@85d
    .line 1127
    :sswitch_85d
    const-string v23, "BluetoothManagerService"

    #@85f
    const-string v24, "MESSAGE_USER_SWITCHED"

    #@861
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@864
    .line 1129
    move-object/from16 v0, p0

    #@866
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@868
    move-object/from16 v23, v0

    #@86a
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@86d
    move-result-object v23

    #@86e
    const/16 v24, 0x12c

    #@870
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@873
    .line 1131
    move-object/from16 v0, p0

    #@875
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@877
    move-object/from16 v23, v0

    #@879
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$100(Lcom/android/server/BluetoothManagerService;)Z

    #@87c
    move-result v23

    #@87d
    if-eqz v23, :cond_a19

    #@87f
    move-object/from16 v0, p0

    #@881
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@883
    move-object/from16 v23, v0

    #@885
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@888
    move-result-object v23

    #@889
    if-eqz v23, :cond_a19

    #@88b
    .line 1132
    move-object/from16 v0, p0

    #@88d
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@88f
    move-object/from16 v23, v0

    #@891
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@894
    move-result-object v24

    #@895
    monitor-enter v24

    #@896
    .line 1133
    :try_start_896
    move-object/from16 v0, p0

    #@898
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@89a
    move-object/from16 v23, v0

    #@89c
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;
    :try_end_89f
    .catchall {:try_start_896 .. :try_end_89f} :catchall_a13

    #@89f
    move-result-object v23

    #@8a0
    if-eqz v23, :cond_8bd

    #@8a2
    .line 1136
    :try_start_8a2
    move-object/from16 v0, p0

    #@8a4
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@8a6
    move-object/from16 v23, v0

    #@8a8
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@8ab
    move-result-object v23

    #@8ac
    move-object/from16 v0, p0

    #@8ae
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@8b0
    move-object/from16 v25, v0

    #@8b2
    invoke-static/range {v25 .. v25}, Lcom/android/server/BluetoothManagerService;->access$2700(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetoothCallback;

    #@8b5
    move-result-object v25

    #@8b6
    move-object/from16 v0, v23

    #@8b8
    move-object/from16 v1, v25

    #@8ba
    invoke-interface {v0, v1}, Landroid/bluetooth/IBluetooth;->unregisterCallback(Landroid/bluetooth/IBluetoothCallback;)V
    :try_end_8bd
    .catchall {:try_start_8a2 .. :try_end_8bd} :catchall_a13
    .catch Landroid/os/RemoteException; {:try_start_8a2 .. :try_end_8bd} :catch_a05

    #@8bd
    .line 1141
    :cond_8bd
    :goto_8bd
    :try_start_8bd
    monitor-exit v24
    :try_end_8be
    .catchall {:try_start_8bd .. :try_end_8be} :catchall_a13

    #@8be
    .line 1143
    move-object/from16 v0, p0

    #@8c0
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@8c2
    move-object/from16 v23, v0

    #@8c4
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@8c7
    move-result v23

    #@8c8
    const/16 v24, 0xd

    #@8ca
    move/from16 v0, v23

    #@8cc
    move/from16 v1, v24

    #@8ce
    if-ne v0, v1, :cond_8f0

    #@8d0
    .line 1145
    move-object/from16 v0, p0

    #@8d2
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@8d4
    move-object/from16 v23, v0

    #@8d6
    move-object/from16 v0, p0

    #@8d8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@8da
    move-object/from16 v24, v0

    #@8dc
    invoke-static/range {v24 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@8df
    move-result v24

    #@8e0
    const/16 v25, 0xa

    #@8e2
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@8e5
    .line 1146
    move-object/from16 v0, p0

    #@8e7
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@8e9
    move-object/from16 v23, v0

    #@8eb
    const/16 v24, 0xa

    #@8ed
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3002(Lcom/android/server/BluetoothManagerService;I)I

    #@8f0
    .line 1148
    :cond_8f0
    move-object/from16 v0, p0

    #@8f2
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@8f4
    move-object/from16 v23, v0

    #@8f6
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@8f9
    move-result v23

    #@8fa
    const/16 v24, 0xa

    #@8fc
    move/from16 v0, v23

    #@8fe
    move/from16 v1, v24

    #@900
    if-ne v0, v1, :cond_922

    #@902
    .line 1149
    move-object/from16 v0, p0

    #@904
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@906
    move-object/from16 v23, v0

    #@908
    move-object/from16 v0, p0

    #@90a
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@90c
    move-object/from16 v24, v0

    #@90e
    invoke-static/range {v24 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@911
    move-result v24

    #@912
    const/16 v25, 0xb

    #@914
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@917
    .line 1150
    move-object/from16 v0, p0

    #@919
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@91b
    move-object/from16 v23, v0

    #@91d
    const/16 v24, 0xb

    #@91f
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3002(Lcom/android/server/BluetoothManagerService;I)I

    #@922
    .line 1153
    :cond_922
    move-object/from16 v0, p0

    #@924
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@926
    move-object/from16 v23, v0

    #@928
    const/16 v24, 0x1

    #@92a
    const/16 v25, 0x0

    #@92c
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@92f
    .line 1155
    move-object/from16 v0, p0

    #@931
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@933
    move-object/from16 v23, v0

    #@935
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@938
    move-result v23

    #@939
    const/16 v24, 0xb

    #@93b
    move/from16 v0, v23

    #@93d
    move/from16 v1, v24

    #@93f
    if-ne v0, v1, :cond_956

    #@941
    .line 1156
    move-object/from16 v0, p0

    #@943
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@945
    move-object/from16 v23, v0

    #@947
    move-object/from16 v0, p0

    #@949
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@94b
    move-object/from16 v24, v0

    #@94d
    invoke-static/range {v24 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3000(Lcom/android/server/BluetoothManagerService;)I

    #@950
    move-result v24

    #@951
    const/16 v25, 0xc

    #@953
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@956
    .line 1160
    :cond_956
    move-object/from16 v0, p0

    #@958
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@95a
    move-object/from16 v23, v0

    #@95c
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$2200(Lcom/android/server/BluetoothManagerService;)V

    #@95f
    .line 1162
    move-object/from16 v0, p0

    #@961
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@963
    move-object/from16 v23, v0

    #@965
    const/16 v24, 0xc

    #@967
    const/16 v25, 0xd

    #@969
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@96c
    .line 1165
    move-object/from16 v0, p0

    #@96e
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@970
    move-object/from16 v23, v0

    #@972
    const/16 v24, 0x0

    #@974
    const/16 v25, 0x1

    #@976
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z

    #@979
    .line 1167
    move-object/from16 v0, p0

    #@97b
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@97d
    move-object/from16 v23, v0

    #@97f
    const/16 v24, 0xd

    #@981
    const/16 v25, 0xa

    #@983
    invoke-static/range {v23 .. v25}, Lcom/android/server/BluetoothManagerService;->access$3100(Lcom/android/server/BluetoothManagerService;II)V

    #@986
    .line 1169
    move-object/from16 v0, p0

    #@988
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@98a
    move-object/from16 v23, v0

    #@98c
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$3200(Lcom/android/server/BluetoothManagerService;)V

    #@98f
    .line 1170
    move-object/from16 v0, p0

    #@991
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@993
    move-object/from16 v23, v0

    #@995
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@998
    move-result-object v24

    #@999
    monitor-enter v24

    #@99a
    .line 1171
    :try_start_99a
    move-object/from16 v0, p0

    #@99c
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@99e
    move-object/from16 v23, v0

    #@9a0
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@9a3
    move-result-object v23

    #@9a4
    if-eqz v23, :cond_9d0

    #@9a6
    .line 1172
    move-object/from16 v0, p0

    #@9a8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@9aa
    move-object/from16 v23, v0

    #@9ac
    const/16 v25, 0x0

    #@9ae
    move-object/from16 v0, v23

    #@9b0
    move-object/from16 v1, v25

    #@9b2
    invoke-static {v0, v1}, Lcom/android/server/BluetoothManagerService;->access$1602(Lcom/android/server/BluetoothManagerService;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    #@9b5
    .line 1174
    move-object/from16 v0, p0

    #@9b7
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@9b9
    move-object/from16 v23, v0

    #@9bb
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1800(Lcom/android/server/BluetoothManagerService;)Landroid/content/Context;

    #@9be
    move-result-object v23

    #@9bf
    move-object/from16 v0, p0

    #@9c1
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@9c3
    move-object/from16 v25, v0

    #@9c5
    invoke-static/range {v25 .. v25}, Lcom/android/server/BluetoothManagerService;->access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@9c8
    move-result-object v25

    #@9c9
    move-object/from16 v0, v23

    #@9cb
    move-object/from16 v1, v25

    #@9cd
    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@9d0
    .line 1176
    :cond_9d0
    monitor-exit v24
    :try_end_9d1
    .catchall {:try_start_99a .. :try_end_9d1} :catchall_a16

    #@9d1
    .line 1177
    const-wide/16 v23, 0x64

    #@9d3
    invoke-static/range {v23 .. v24}, Landroid/os/SystemClock;->sleep(J)V

    #@9d6
    .line 1179
    move-object/from16 v0, p0

    #@9d8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@9da
    move-object/from16 v23, v0

    #@9dc
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@9df
    move-result-object v23

    #@9e0
    const/16 v24, 0x3c

    #@9e2
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@9e5
    .line 1180
    move-object/from16 v0, p0

    #@9e7
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@9e9
    move-object/from16 v23, v0

    #@9eb
    const/16 v24, 0xa

    #@9ed
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$3002(Lcom/android/server/BluetoothManagerService;I)I

    #@9f0
    .line 1182
    move-object/from16 v0, p0

    #@9f2
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@9f4
    move-object/from16 v23, v0

    #@9f6
    move-object/from16 v0, p0

    #@9f8
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@9fa
    move-object/from16 v24, v0

    #@9fc
    invoke-static/range {v24 .. v24}, Lcom/android/server/BluetoothManagerService;->access$2900(Lcom/android/server/BluetoothManagerService;)Z

    #@9ff
    move-result v24

    #@a00
    invoke-static/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService;->access$2100(Lcom/android/server/BluetoothManagerService;Z)V

    #@a03
    goto/16 :goto_27

    #@a05
    .line 1137
    :catch_a05
    move-exception v13

    #@a06
    .line 1138
    .restart local v13       #re:Landroid/os/RemoteException;
    :try_start_a06
    const-string v23, "BluetoothManagerService"

    #@a08
    const-string v25, "Unable to unregister"

    #@a0a
    move-object/from16 v0, v23

    #@a0c
    move-object/from16 v1, v25

    #@a0e
    invoke-static {v0, v1, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a11
    goto/16 :goto_8bd

    #@a13
    .line 1141
    .end local v13           #re:Landroid/os/RemoteException;
    :catchall_a13
    move-exception v23

    #@a14
    monitor-exit v24
    :try_end_a15
    .catchall {:try_start_a06 .. :try_end_a15} :catchall_a13

    #@a15
    throw v23

    #@a16
    .line 1176
    :catchall_a16
    move-exception v23

    #@a17
    :try_start_a17
    monitor-exit v24
    :try_end_a18
    .catchall {:try_start_a17 .. :try_end_a18} :catchall_a16

    #@a18
    throw v23

    #@a19
    .line 1183
    :cond_a19
    move-object/from16 v0, p0

    #@a1b
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@a1d
    move-object/from16 v23, v0

    #@a1f
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1700(Lcom/android/server/BluetoothManagerService;)Z

    #@a22
    move-result v23

    #@a23
    if-nez v23, :cond_a31

    #@a25
    move-object/from16 v0, p0

    #@a27
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@a29
    move-object/from16 v23, v0

    #@a2b
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;

    #@a2e
    move-result-object v23

    #@a2f
    if-eqz v23, :cond_27

    #@a31
    .line 1184
    :cond_a31
    move-object/from16 v0, p0

    #@a33
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@a35
    move-object/from16 v23, v0

    #@a37
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@a3a
    move-result-object v23

    #@a3b
    const/16 v24, 0x12c

    #@a3d
    invoke-virtual/range {v23 .. v24}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@a40
    move-result-object v22

    #@a41
    .line 1185
    .local v22, userMsg:Landroid/os/Message;
    move-object/from16 v0, p1

    #@a43
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@a45
    move/from16 v23, v0

    #@a47
    add-int/lit8 v23, v23, 0x1

    #@a49
    move/from16 v0, v23

    #@a4b
    move-object/from16 v1, v22

    #@a4d
    iput v0, v1, Landroid/os/Message;->arg2:I

    #@a4f
    .line 1188
    move-object/from16 v0, p0

    #@a51
    iget-object v0, v0, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->this$0:Lcom/android/server/BluetoothManagerService;

    #@a53
    move-object/from16 v23, v0

    #@a55
    invoke-static/range {v23 .. v23}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@a58
    move-result-object v23

    #@a59
    const-wide/16 v24, 0xc8

    #@a5b
    move-object/from16 v0, v23

    #@a5d
    move-object/from16 v1, v22

    #@a5f
    move-wide/from16 v2, v24

    #@a61
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@a64
    .line 1190
    const-string v23, "BluetoothManagerService"

    #@a66
    new-instance v24, Ljava/lang/StringBuilder;

    #@a68
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@a6b
    const-string v25, "delay MESSAGE_USER_SWITCHED "

    #@a6d
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a70
    move-result-object v24

    #@a71
    move-object/from16 v0, v22

    #@a73
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@a75
    move/from16 v25, v0

    #@a77
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7a
    move-result-object v24

    #@a7b
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7e
    move-result-object v24

    #@a7f
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a82
    goto/16 :goto_27

    #@a84
    .line 772
    :sswitch_data_a84
    .sparse-switch
        0x1 -> :sswitch_33f
        0x2 -> :sswitch_39d
        0x3 -> :sswitch_48e
        0x4 -> :sswitch_4bb
        0x14 -> :sswitch_40a
        0x15 -> :sswitch_44c
        0x1e -> :sswitch_4c6
        0x1f -> :sswitch_4dd
        0x28 -> :sswitch_4f6
        0x29 -> :sswitch_711
        0x2a -> :sswitch_80f
        0x3c -> :sswitch_6cb
        0x64 -> :sswitch_6a4
        0x65 -> :sswitch_836
        0xc8 -> :sswitch_28
        0xc9 -> :sswitch_167
        0x12c -> :sswitch_85d
    .end sparse-switch
.end method
