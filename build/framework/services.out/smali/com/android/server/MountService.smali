.class Lcom/android/server/MountService;
.super Landroid/os/storage/IMountService$Stub;
.source "MountService.java"

# interfaces
.implements Lcom/android/server/INativeDaemonConnectorCallbacks;
.implements Lcom/android/server/Watchdog$Monitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MountService$UnmountObbAction;,
        Lcom/android/server/MountService$MountObbAction;,
        Lcom/android/server/MountService$ObbAction;,
        Lcom/android/server/MountService$ObbActionHandler;,
        Lcom/android/server/MountService$MountServiceBinderListener;,
        Lcom/android/server/MountService$MountServiceHandler;,
        Lcom/android/server/MountService$ShutdownCallBack;,
        Lcom/android/server/MountService$UmsEnableCallBack;,
        Lcom/android/server/MountService$UnmountCallBack;,
        Lcom/android/server/MountService$DefaultContainerConnection;,
        Lcom/android/server/MountService$ObbState;,
        Lcom/android/server/MountService$VoldResponseCode;,
        Lcom/android/server/MountService$VolumeState;
    }
.end annotation


# static fields
.field private static final CRYPTO_ALGORITHM_KEY_SIZE:I = 0x80

.field private static final DEBUG_EVENTS:Z = false

.field private static final DEBUG_OBB:Z = false

.field private static final DEBUG_UNMOUNT:Z = false

.field static final DEFAULT_CONTAINER_COMPONENT:Landroid/content/ComponentName; = null

.field private static final H_SYSTEM_READY:I = 0x4

.field private static final H_UNMOUNT_MS:I = 0x3

.field private static final H_UNMOUNT_PM_DONE:I = 0x2

.field private static final H_UNMOUNT_PM_UPDATE:I = 0x1

.field private static final LOCAL_LOGD:Z = false

.field private static final MAX_CONTAINERS:I = 0xfa

.field private static final MAX_UNMOUNT_RETRIES:I = 0x4

.field private static final OBB_FLUSH_MOUNT_STATE:I = 0x5

.field private static final OBB_MCS_BOUND:I = 0x2

.field private static final OBB_MCS_RECONNECT:I = 0x4

.field private static final OBB_MCS_UNBIND:I = 0x3

.field private static final OBB_RUN_ACTION:I = 0x1

.field private static final PBKDF2_HASH_ROUNDS:I = 0x400

.field private static final RETRY_UNMOUNT_DELAY:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "MountService"

.field private static final TAG_STORAGE:Ljava/lang/String; = "storage"

.field private static final TAG_STORAGE_LIST:Ljava/lang/String; = "StorageList"

.field private static final VOLD_TAG:Ljava/lang/String; = "VoldConnector"

.field private static final WATCHDOG_ENABLE:Z


# instance fields
.field private final mAsecMountSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mAsecsScanned:Ljava/util/concurrent/CountDownLatch;

.field private final mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

.field private mConnector:Lcom/android/server/NativeDaemonConnector;

.field private mContainerService:Lcom/android/internal/app/IMediaContainerService;

.field private mContext:Landroid/content/Context;

.field private final mDefContainerConn:Lcom/android/server/MountService$DefaultContainerConnection;

.field private mEmulatedTemplate:Landroid/os/storage/StorageVolume;

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/MountService$MountServiceBinderListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

.field private final mObbMounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/os/IBinder;",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/MountService$ObbState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mObbPathToStateMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/MountService$ObbState;",
            ">;"
        }
    .end annotation
.end field

.field private mPms:Lcom/android/server/pm/PackageManagerService;

.field private mSendUmsConnectedOnBoot:Z

.field private volatile mSystemReady:Z

.field private mUmsAvailable:Z

.field private mUmsEnabling:Z

.field private final mUsbReceiver:Landroid/content/BroadcastReceiver;

.field private final mUserReceiver:Landroid/content/BroadcastReceiver;

.field private final mVolumeStates:Ljava/util/HashMap;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mVolumesLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumes:Ljava/util/ArrayList;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mVolumesLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/storage/StorageVolume;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumesByPath:Ljava/util/HashMap;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mVolumesLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/storage/StorageVolume;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumesLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 310
    new-instance v0, Landroid/content/ComponentName;

    #@2
    const-string v1, "com.android.defcontainer"

    #@4
    const-string v2, "com.android.defcontainer.DefaultContainerService"

    #@6
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    sput-object v0, Lcom/android/server/MountService;->DEFAULT_CONTAINER_COMPONENT:Landroid/content/ComponentName;

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 1340
    invoke-direct {p0}, Landroid/os/storage/IMountService$Stub;-><init>()V

    #@6
    .line 183
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@d
    .line 188
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@13
    .line 191
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Lcom/android/server/MountService;->mVolumesByPath:Ljava/util/HashMap;

    #@19
    .line 194
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@1f
    .line 197
    iput-boolean v1, p0, Lcom/android/server/MountService;->mSystemReady:Z

    #@21
    .line 201
    iput-boolean v1, p0, Lcom/android/server/MountService;->mUmsAvailable:Z

    #@23
    .line 203
    new-instance v0, Ljava/util/ArrayList;

    #@25
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@28
    iput-object v0, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@2a
    .line 205
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@2c
    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@2f
    iput-object v0, p0, Lcom/android/server/MountService;->mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

    #@31
    .line 206
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@33
    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@36
    iput-object v0, p0, Lcom/android/server/MountService;->mAsecsScanned:Ljava/util/concurrent/CountDownLatch;

    #@38
    .line 207
    iput-boolean v1, p0, Lcom/android/server/MountService;->mSendUmsConnectedOnBoot:Z

    #@3a
    .line 213
    new-instance v0, Ljava/util/HashSet;

    #@3c
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@3f
    iput-object v0, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@41
    .line 231
    new-instance v0, Ljava/util/HashMap;

    #@43
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@46
    iput-object v0, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@48
    .line 234
    new-instance v0, Ljava/util/HashMap;

    #@4a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@4d
    iput-object v0, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@4f
    .line 313
    new-instance v0, Lcom/android/server/MountService$DefaultContainerConnection;

    #@51
    invoke-direct {v0, p0}, Lcom/android/server/MountService$DefaultContainerConnection;-><init>(Lcom/android/server/MountService;)V

    #@54
    iput-object v0, p0, Lcom/android/server/MountService;->mDefContainerConn:Lcom/android/server/MountService$DefaultContainerConnection;

    #@56
    .line 330
    iput-object v4, p0, Lcom/android/server/MountService;->mContainerService:Lcom/android/internal/app/IMediaContainerService;

    #@58
    .line 563
    new-instance v0, Lcom/android/server/MountService$1;

    #@5a
    invoke-direct {v0, p0}, Lcom/android/server/MountService$1;-><init>(Lcom/android/server/MountService;)V

    #@5d
    iput-object v0, p0, Lcom/android/server/MountService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@5f
    .line 592
    new-instance v0, Lcom/android/server/MountService$2;

    #@61
    invoke-direct {v0, p0}, Lcom/android/server/MountService$2;-><init>(Lcom/android/server/MountService;)V

    #@64
    iput-object v0, p0, Lcom/android/server/MountService;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    #@66
    .line 1341
    iput-object p1, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@68
    .line 1343
    iget-object v1, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@6a
    monitor-enter v1

    #@6b
    .line 1344
    :try_start_6b
    invoke-direct {p0}, Lcom/android/server/MountService;->readStorageListLocked()V

    #@6e
    .line 1345
    monitor-exit v1
    :try_end_6f
    .catchall {:try_start_6b .. :try_end_6f} :catchall_f2

    #@6f
    .line 1348
    const-string v0, "package"

    #@71
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@74
    move-result-object v0

    #@75
    check-cast v0, Lcom/android/server/pm/PackageManagerService;

    #@77
    iput-object v0, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@79
    .line 1350
    new-instance v0, Landroid/os/HandlerThread;

    #@7b
    const-string v1, "MountService"

    #@7d
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@80
    iput-object v0, p0, Lcom/android/server/MountService;->mHandlerThread:Landroid/os/HandlerThread;

    #@82
    .line 1351
    iget-object v0, p0, Lcom/android/server/MountService;->mHandlerThread:Landroid/os/HandlerThread;

    #@84
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@87
    .line 1352
    new-instance v0, Lcom/android/server/MountService$MountServiceHandler;

    #@89
    iget-object v1, p0, Lcom/android/server/MountService;->mHandlerThread:Landroid/os/HandlerThread;

    #@8b
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@8e
    move-result-object v1

    #@8f
    invoke-direct {v0, p0, v1}, Lcom/android/server/MountService$MountServiceHandler;-><init>(Lcom/android/server/MountService;Landroid/os/Looper;)V

    #@92
    iput-object v0, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@94
    .line 1355
    new-instance v8, Landroid/content/IntentFilter;

    #@96
    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    #@99
    .line 1356
    .local v8, userFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.USER_ADDED"

    #@9b
    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9e
    .line 1357
    const-string v0, "android.intent.action.USER_REMOVED"

    #@a0
    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a3
    .line 1358
    iget-object v0, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@a5
    iget-object v1, p0, Lcom/android/server/MountService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@a7
    iget-object v2, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@a9
    invoke-virtual {v0, v1, v8, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@ac
    .line 1361
    invoke-direct {p0}, Lcom/android/server/MountService;->getPrimaryPhysicalVolume()Landroid/os/storage/StorageVolume;

    #@af
    move-result-object v6

    #@b0
    .line 1362
    .local v6, primary:Landroid/os/storage/StorageVolume;
    if-eqz v6, :cond_c8

    #@b2
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->allowMassStorage()Z

    #@b5
    move-result v0

    #@b6
    if-eqz v0, :cond_c8

    #@b8
    .line 1363
    iget-object v0, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@ba
    iget-object v1, p0, Lcom/android/server/MountService;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    #@bc
    new-instance v2, Landroid/content/IntentFilter;

    #@be
    const-string v3, "android.hardware.usb.action.USB_STATE"

    #@c0
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@c3
    iget-object v3, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@c5
    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@c8
    .line 1368
    :cond_c8
    new-instance v0, Lcom/android/server/MountService$ObbActionHandler;

    #@ca
    iget-object v1, p0, Lcom/android/server/MountService;->mHandlerThread:Landroid/os/HandlerThread;

    #@cc
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@cf
    move-result-object v1

    #@d0
    invoke-direct {v0, p0, v1}, Lcom/android/server/MountService$ObbActionHandler;-><init>(Lcom/android/server/MountService;Landroid/os/Looper;)V

    #@d3
    iput-object v0, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@d5
    .line 1375
    new-instance v0, Lcom/android/server/NativeDaemonConnector;

    #@d7
    const-string v2, "vold"

    #@d9
    const/16 v3, 0x1f4

    #@db
    const-string v4, "VoldConnector"

    #@dd
    const/16 v5, 0x19

    #@df
    move-object v1, p0

    #@e0
    invoke-direct/range {v0 .. v5}, Lcom/android/server/NativeDaemonConnector;-><init>(Lcom/android/server/INativeDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V

    #@e3
    iput-object v0, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e5
    .line 1377
    new-instance v7, Ljava/lang/Thread;

    #@e7
    iget-object v0, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e9
    const-string v1, "VoldConnector"

    #@eb
    invoke-direct {v7, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@ee
    .line 1378
    .local v7, thread:Ljava/lang/Thread;
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    #@f1
    .line 1384
    return-void

    #@f2
    .line 1345
    .end local v6           #primary:Landroid/os/storage/StorageVolume;
    .end local v7           #thread:Ljava/lang/Thread;
    .end local v8           #userFilter:Landroid/content/IntentFilter;
    :catchall_f2
    move-exception v0

    #@f3
    :try_start_f3
    monitor-exit v1
    :try_end_f4
    .catchall {:try_start_f3 .. :try_end_f4} :catchall_f2

    #@f4
    throw v0
.end method

.method static synthetic access$000(Lcom/android/server/MountService;)Lcom/android/server/MountService$ObbActionHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/MountService;Ljava/lang/String;ZZ)I
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/MountService;->doUnmountVolume(Ljava/lang/String;ZZ)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(Lcom/android/server/MountService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->notifyShareAvailabilityChange(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/MountService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/MountService;)Lcom/android/server/NativeDaemonConnector;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/MountService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mVolumesByPath:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/server/MountService;Landroid/os/storage/StorageVolume;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/MountService;)Landroid/os/storage/StorageVolume;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/server/MountService;->getPrimaryPhysicalVolume()Landroid/os/storage/StorageVolume;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mAsecsScanned:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/server/MountService;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->doMountVolume(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1900(Lcom/android/server/MountService;)Lcom/android/internal/app/IMediaContainerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mContainerService:Lcom/android/internal/app/IMediaContainerService;

    #@2
    return-object v0
.end method

.method static synthetic access$1902(Lcom/android/server/MountService;Lcom/android/internal/app/IMediaContainerService;)Lcom/android/internal/app/IMediaContainerService;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/server/MountService;->mContainerService:Lcom/android/internal/app/IMediaContainerService;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/server/MountService;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/MountService;->doShareUnshareVolume(Ljava/lang/String;Ljava/lang/String;Z)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Lcom/android/server/MountService;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/server/MountService;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->removeObbStateLocked(Lcom/android/server/MountService$ObbState;)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/server/MountService;)Lcom/android/server/MountService$DefaultContainerConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mDefContainerConn:Lcom/android/server/MountService$DefaultContainerConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/server/MountService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/server/MountService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/server/MountService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/android/server/MountService;Ljava/lang/String;I)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lcom/android/server/MountService;->isUidOwnerOfPackageOrSystem(Ljava/lang/String;I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2800(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->addObbStateLocked(Lcom/android/server/MountService$ObbState;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/MountService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/MountService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/server/MountService;->handleSystemReady()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/MountService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/MountService;Landroid/os/UserHandle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->createEmulatedVolumeForUserLocked(Landroid/os/UserHandle;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/MountService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/MountService;Landroid/os/storage/StorageVolume;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->removeVolumeLocked(Landroid/os/storage/StorageVolume;)V

    #@3
    return-void
.end method

.method private addObbStateLocked(Lcom/android/server/MountService$ObbState;)V
    .registers 9
    .parameter "obbState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2187
    invoke-virtual {p1}, Lcom/android/server/MountService$ObbState;->getBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    .line 2188
    .local v0, binder:Landroid/os/IBinder;
    iget-object v5, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@6
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v4

    #@a
    check-cast v4, Ljava/util/List;

    #@c
    .line 2190
    .local v4, obbStates:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    if-nez v4, :cond_26

    #@e
    .line 2191
    new-instance v4, Ljava/util/ArrayList;

    #@10
    .end local v4           #obbStates:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@13
    .line 2192
    .restart local v4       #obbStates:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    iget-object v5, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@15
    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 2202
    :cond_18
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1b
    .line 2204
    :try_start_1b
    invoke-virtual {p1}, Lcom/android/server/MountService$ObbState;->link()V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1e} :catch_48

    #@1e
    .line 2219
    iget-object v5, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@20
    iget-object v6, p1, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@22
    invoke-interface {v5, v6, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    .line 2220
    return-void

    #@26
    .line 2194
    :cond_26
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@29
    move-result-object v2

    #@2a
    .local v2, i$:Ljava/util/Iterator;
    :cond_2a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2d
    move-result v5

    #@2e
    if-eqz v5, :cond_18

    #@30
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@33
    move-result-object v3

    #@34
    check-cast v3, Lcom/android/server/MountService$ObbState;

    #@36
    .line 2195
    .local v3, o:Lcom/android/server/MountService$ObbState;
    iget-object v5, v3, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@38
    iget-object v6, p1, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_2a

    #@40
    .line 2196
    new-instance v5, Ljava/lang/IllegalStateException;

    #@42
    const-string v6, "Attempt to add ObbState twice. This indicates an error in the MountService logic."

    #@44
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@47
    throw v5

    #@48
    .line 2205
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #o:Lcom/android/server/MountService$ObbState;
    :catch_48
    move-exception v1

    #@49
    .line 2210
    .local v1, e:Landroid/os/RemoteException;
    invoke-interface {v4, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@4c
    .line 2211
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    #@4f
    move-result v5

    #@50
    if-eqz v5, :cond_57

    #@52
    .line 2212
    iget-object v5, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@54
    invoke-interface {v5, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    .line 2216
    :cond_57
    throw v1
.end method

.method private addVolumeLocked(Landroid/os/storage/StorageVolume;)V
    .registers 6
    .parameter "volume"

    #@0
    .prologue
    .line 1308
    const-string v1, "MountService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "addVolumeLocked() "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1309
    iget-object v1, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 1310
    iget-object v1, p0, Lcom/android/server/MountService;->mVolumesByPath:Ljava/util/HashMap;

    #@1f
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, Landroid/os/storage/StorageVolume;

    #@29
    .line 1311
    .local v0, existing:Landroid/os/storage/StorageVolume;
    if-eqz v0, :cond_52

    #@2b
    .line 1312
    new-instance v1, Ljava/lang/IllegalStateException;

    #@2d
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v3, "Volume at "

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    const-string v3, " already exists: "

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@51
    throw v1

    #@52
    .line 1315
    :cond_52
    return-void
.end method

.method public static buildObbPath(Ljava/lang/String;IZ)Ljava/lang/String;
    .registers 11
    .parameter "canonicalPath"
    .parameter "userId"
    .parameter "forVold"

    #@0
    .prologue
    .line 2648
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    #@3
    move-result v6

    #@4
    if-nez v6, :cond_7

    #@6
    .line 2688
    .end local p0
    :cond_6
    :goto_6
    return-object p0

    #@7
    .line 2652
    .restart local p0
    :cond_7
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@a
    move-result-object v4

    #@b
    .line 2655
    .local v4, path:Ljava/lang/String;
    new-instance v5, Landroid/os/Environment$UserEnvironment;

    #@d
    invoke-direct {v5, p1}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    #@10
    .line 2658
    .local v5, userEnv:Landroid/os/Environment$UserEnvironment;
    invoke-virtual {v5}, Landroid/os/Environment$UserEnvironment;->getExternalStorageDirectory()Ljava/io/File;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 2660
    .local v0, externalPath:Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    .line 2663
    .local v1, legacyExternalPath:Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@23
    move-result v6

    #@24
    if-eqz v6, :cond_56

    #@26
    .line 2664
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@29
    move-result v6

    #@2a
    add-int/lit8 v6, v6, 0x1

    #@2c
    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    .line 2672
    :goto_30
    const-string v2, "Android/obb"

    #@32
    .line 2673
    .local v2, obbPath:Ljava/lang/String;
    const-string v6, "Android/obb"

    #@34
    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@37
    move-result v6

    #@38
    if-eqz v6, :cond_7b

    #@3a
    .line 2674
    const-string v6, "Android/obb"

    #@3c
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@3f
    move-result v6

    #@40
    add-int/lit8 v6, v6, 0x1

    #@42
    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    .line 2676
    if-eqz p2, :cond_67

    #@48
    .line 2677
    new-instance v6, Ljava/io/File;

    #@4a
    invoke-static {}, Landroid/os/Environment;->getEmulatedStorageObbSource()Ljava/io/File;

    #@4d
    move-result-object v7

    #@4e
    invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@51
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    #@54
    move-result-object p0

    #@55
    goto :goto_6

    #@56
    .line 2665
    .end local v2           #obbPath:Ljava/lang/String;
    :cond_56
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@59
    move-result v6

    #@5a
    if-eqz v6, :cond_6

    #@5c
    .line 2666
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@5f
    move-result v6

    #@60
    add-int/lit8 v6, v6, 0x1

    #@62
    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@65
    move-result-object v4

    #@66
    goto :goto_30

    #@67
    .line 2679
    .restart local v2       #obbPath:Ljava/lang/String;
    :cond_67
    new-instance v3, Landroid/os/Environment$UserEnvironment;

    #@69
    const/4 v6, 0x0

    #@6a
    invoke-direct {v3, v6}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    #@6d
    .line 2680
    .local v3, ownerEnv:Landroid/os/Environment$UserEnvironment;
    new-instance v6, Ljava/io/File;

    #@6f
    invoke-virtual {v3}, Landroid/os/Environment$UserEnvironment;->getExternalStorageObbDirectory()Ljava/io/File;

    #@72
    move-result-object v7

    #@73
    invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@76
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    #@79
    move-result-object p0

    #@7a
    goto :goto_6

    #@7b
    .line 2685
    .end local v3           #ownerEnv:Landroid/os/Environment$UserEnvironment;
    :cond_7b
    if-eqz p2, :cond_8c

    #@7d
    .line 2686
    new-instance v6, Ljava/io/File;

    #@7f
    invoke-static {p1}, Landroid/os/Environment;->getEmulatedStorageSource(I)Ljava/io/File;

    #@82
    move-result-object v7

    #@83
    invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@86
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    #@89
    move-result-object p0

    #@8a
    goto/16 :goto_6

    #@8c
    .line 2688
    :cond_8c
    new-instance v6, Ljava/io/File;

    #@8e
    invoke-virtual {v5}, Landroid/os/Environment$UserEnvironment;->getExternalStorageDirectory()Ljava/io/File;

    #@91
    move-result-object v7

    #@92
    invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@95
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    #@98
    move-result-object p0

    #@99
    goto/16 :goto_6
.end method

.method private createEmulatedVolumeForUserLocked(Landroid/os/UserHandle;)V
    .registers 8
    .parameter "user"

    #@0
    .prologue
    .line 1289
    iget-object v3, p0, Lcom/android/server/MountService;->mEmulatedTemplate:Landroid/os/storage/StorageVolume;

    #@2
    if-nez v3, :cond_c

    #@4
    .line 1290
    new-instance v3, Ljava/lang/IllegalStateException;

    #@6
    const-string v4, "Missing emulated volume multi-user template"

    #@8
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v3

    #@c
    .line 1293
    :cond_c
    new-instance v1, Landroid/os/Environment$UserEnvironment;

    #@e
    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    #@11
    move-result v3

    #@12
    invoke-direct {v1, v3}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    #@15
    .line 1294
    .local v1, userEnv:Landroid/os/Environment$UserEnvironment;
    invoke-virtual {v1}, Landroid/os/Environment$UserEnvironment;->getExternalStorageDirectory()Ljava/io/File;

    #@18
    move-result-object v0

    #@19
    .line 1295
    .local v0, path:Ljava/io/File;
    iget-object v3, p0, Lcom/android/server/MountService;->mEmulatedTemplate:Landroid/os/storage/StorageVolume;

    #@1b
    invoke-static {v3, v0, p1}, Landroid/os/storage/StorageVolume;->fromTemplate(Landroid/os/storage/StorageVolume;Ljava/io/File;Landroid/os/UserHandle;)Landroid/os/storage/StorageVolume;

    #@1e
    move-result-object v2

    #@1f
    .line 1296
    .local v2, volume:Landroid/os/storage/StorageVolume;
    const/4 v3, 0x0

    #@20
    invoke-virtual {v2, v3}, Landroid/os/storage/StorageVolume;->setStorageId(I)V

    #@23
    .line 1297
    invoke-direct {p0, v2}, Lcom/android/server/MountService;->addVolumeLocked(Landroid/os/storage/StorageVolume;)V

    #@26
    .line 1299
    iget-boolean v3, p0, Lcom/android/server/MountService;->mSystemReady:Z

    #@28
    if-eqz v3, :cond_30

    #@2a
    .line 1300
    const-string v3, "mounted"

    #@2c
    invoke-direct {p0, v2, v3}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@2f
    .line 1305
    :goto_2f
    return-void

    #@30
    .line 1303
    :cond_30
    iget-object v3, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@32
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    const-string v5, "mounted"

    #@38
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    goto :goto_2f
.end method

.method private doFormatVolume(Ljava/lang/String;)I
    .registers 10
    .parameter "path"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1086
    :try_start_1
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@3
    const-string v4, "volume"

    #@5
    const/4 v5, 0x2

    #@6
    new-array v5, v5, [Ljava/lang/Object;

    #@8
    const/4 v6, 0x0

    #@9
    const-string v7, "format"

    #@b
    aput-object v7, v5, v6

    #@d
    const/4 v6, 0x1

    #@e
    aput-object p1, v5, v6

    #@10
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_13
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1 .. :try_end_13} :catch_14

    #@13
    .line 1095
    :goto_13
    return v2

    #@14
    .line 1088
    :catch_14
    move-exception v1

    #@15
    .line 1089
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@18
    move-result v0

    #@19
    .line 1090
    .local v0, code:I
    const/16 v2, 0x191

    #@1b
    if-ne v0, v2, :cond_1f

    #@1d
    .line 1091
    const/4 v2, -0x2

    #@1e
    goto :goto_13

    #@1f
    .line 1092
    :cond_1f
    const/16 v2, 0x193

    #@21
    if-ne v0, v2, :cond_25

    #@23
    .line 1093
    const/4 v2, -0x4

    #@24
    goto :goto_13

    #@25
    .line 1095
    :cond_25
    const/4 v2, -0x1

    #@26
    goto :goto_13
.end method

.method private doGetVolumeShared(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "path"
    .parameter "method"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1103
    :try_start_1
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@3
    const-string v4, "volume"

    #@5
    const/4 v5, 0x3

    #@6
    new-array v5, v5, [Ljava/lang/Object;

    #@8
    const/4 v6, 0x0

    #@9
    const-string v7, "shared"

    #@b
    aput-object v7, v5, v6

    #@d
    const/4 v6, 0x1

    #@e
    aput-object p1, v5, v6

    #@10
    const/4 v6, 0x2

    #@11
    aput-object p2, v5, v6

    #@13
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_16
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1 .. :try_end_16} :catch_2a

    #@16
    move-result-object v0

    #@17
    .line 1109
    .local v0, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonEvent;->getCode()I

    #@1a
    move-result v3

    #@1b
    const/16 v4, 0xd4

    #@1d
    if-ne v3, v4, :cond_29

    #@1f
    .line 1110
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    const-string v3, "enabled"

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@28
    move-result v2

    #@29
    .line 1112
    .end local v0           #event:Lcom/android/server/NativeDaemonEvent;
    :cond_29
    :goto_29
    return v2

    #@2a
    .line 1104
    :catch_2a
    move-exception v1

    #@2b
    .line 1105
    .local v1, ex:Lcom/android/server/NativeDaemonConnectorException;
    const-string v3, "MountService"

    #@2d
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v5, "Failed to read response to volume shared "

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    const-string v5, " "

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_29
.end method

.method private doMountVolume(Ljava/lang/String;)I
    .registers 13
    .parameter "path"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/16 v6, -0x63

    #@3
    .line 959
    const/4 v3, 0x0

    #@4
    .line 962
    .local v3, rc:I
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7
    move-result-object v7

    #@8
    if-eqz v7, :cond_2b

    #@a
    .line 963
    const-string v7, "USBstorage"

    #@c
    invoke-virtual {p1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@f
    move-result v7

    #@10
    if-nez v7, :cond_1f

    #@12
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@15
    move-result-object v7

    #@16
    iget-object v8, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@18
    invoke-interface {v7, v9, v8, p1}, Lcom/lge/cappuccino/IMdm;->checkDisabledMountService(Landroid/content/ComponentName;Landroid/content/Context;Ljava/lang/String;)Z

    #@1b
    move-result v7

    #@1c
    if-eqz v7, :cond_1f

    #@1e
    .line 1028
    :cond_1e
    :goto_1e
    return v6

    #@1f
    .line 967
    :cond_1f
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@22
    move-result-object v7

    #@23
    iget-object v8, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@25
    invoke-interface {v7, v9, v8, p1}, Lcom/lge/cappuccino/IMdm;->checkDisabledMountServiceUSBHostStorage(Landroid/content/ComponentName;Landroid/content/Context;Ljava/lang/String;)Z

    #@28
    move-result v7

    #@29
    if-nez v7, :cond_1e

    #@2b
    .line 975
    :cond_2b
    iget-object v7, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@2d
    monitor-enter v7

    #@2e
    .line 976
    :try_start_2e
    iget-object v6, p0, Lcom/android/server/MountService;->mVolumesByPath:Ljava/util/HashMap;

    #@30
    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    move-result-object v5

    #@34
    check-cast v5, Landroid/os/storage/StorageVolume;

    #@36
    .line 977
    .local v5, volume:Landroid/os/storage/StorageVolume;
    monitor-exit v7
    :try_end_37
    .catchall {:try_start_2e .. :try_end_37} :catchall_4b

    #@37
    .line 981
    :try_start_37
    iget-object v6, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@39
    const-string v7, "volume"

    #@3b
    const/4 v8, 0x2

    #@3c
    new-array v8, v8, [Ljava/lang/Object;

    #@3e
    const/4 v9, 0x0

    #@3f
    const-string v10, "mount"

    #@41
    aput-object v10, v8, v9

    #@43
    const/4 v9, 0x1

    #@44
    aput-object p1, v8, v9

    #@46
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_49
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_37 .. :try_end_49} :catch_4e

    #@49
    :cond_49
    :goto_49
    move v6, v3

    #@4a
    .line 1028
    goto :goto_1e

    #@4b
    .line 977
    .end local v5           #volume:Landroid/os/storage/StorageVolume;
    :catchall_4b
    move-exception v6

    #@4c
    :try_start_4c
    monitor-exit v7
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    #@4d
    throw v6

    #@4e
    .line 982
    .restart local v5       #volume:Landroid/os/storage/StorageVolume;
    :catch_4e
    move-exception v2

    #@4f
    .line 986
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    const/4 v0, 0x0

    #@50
    .line 987
    .local v0, action:Ljava/lang/String;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@53
    move-result v1

    #@54
    .line 990
    .local v1, code:I
    const-string v6, "vold.sdcard_fs_type"

    #@56
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    .line 991
    .local v4, recognizedFS:Ljava/lang/String;
    const/16 v6, 0x190

    #@5c
    if-ne v1, v6, :cond_70

    #@5e
    const-string v6, "ntfs"

    #@60
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v6

    #@64
    if-nez v6, :cond_6e

    #@66
    const-string v6, "hfsplus"

    #@68
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v6

    #@6c
    if-eqz v6, :cond_70

    #@6e
    .line 992
    :cond_6e
    const/16 v1, 0x192

    #@70
    .line 995
    :cond_70
    const/16 v6, 0x191

    #@72
    if-ne v1, v6, :cond_7d

    #@74
    .line 999
    const/4 v3, -0x2

    #@75
    .line 1023
    :goto_75
    if-eqz v0, :cond_49

    #@77
    .line 1024
    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@79
    invoke-direct {p0, v0, v5, v6}, Lcom/android/server/MountService;->sendStorageIntent(Ljava/lang/String;Landroid/os/storage/StorageVolume;Landroid/os/UserHandle;)V

    #@7c
    goto :goto_49

    #@7d
    .line 1000
    :cond_7d
    const/16 v6, 0x192

    #@7f
    if-ne v1, v6, :cond_8a

    #@81
    .line 1005
    const-string v6, "nofs"

    #@83
    invoke-direct {p0, v5, v6}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@86
    .line 1006
    const-string v0, "android.intent.action.MEDIA_NOFS"

    #@88
    .line 1007
    const/4 v3, -0x3

    #@89
    goto :goto_75

    #@8a
    .line 1008
    :cond_8a
    const/16 v6, 0x193

    #@8c
    if-ne v1, v6, :cond_97

    #@8e
    .line 1013
    const-string v6, "unmountable"

    #@90
    invoke-direct {p0, v5, v6}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@93
    .line 1014
    const-string v0, "android.intent.action.MEDIA_UNMOUNTABLE"

    #@95
    .line 1015
    const/4 v3, -0x4

    #@96
    goto :goto_75

    #@97
    .line 1017
    :cond_97
    const/4 v3, -0x1

    #@98
    goto :goto_75
.end method

.method private doShareUnshareVolume(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 10
    .parameter "path"
    .parameter "method"
    .parameter "enable"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 620
    const-string v1, "ums"

    #@4
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_1a

    #@a
    .line 621
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v2, "Method %s not supported"

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    aput-object p2, v3, v4

    #@12
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 625
    :cond_1a
    :try_start_1a
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v3, "volume"

    #@1e
    const/4 v1, 0x3

    #@1f
    new-array v4, v1, [Ljava/lang/Object;

    #@21
    const/4 v5, 0x0

    #@22
    if-eqz p3, :cond_32

    #@24
    const-string v1, "share"

    #@26
    :goto_26
    aput-object v1, v4, v5

    #@28
    const/4 v1, 0x1

    #@29
    aput-object p1, v4, v1

    #@2b
    const/4 v1, 0x2

    #@2c
    aput-object p2, v4, v1

    #@2e
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@31
    .line 629
    :goto_31
    return-void

    #@32
    .line 625
    :cond_32
    const-string v1, "unshare"
    :try_end_34
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_34} :catch_35

    #@34
    goto :goto_26

    #@35
    .line 626
    :catch_35
    move-exception v0

    #@36
    .line 627
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "MountService"

    #@38
    const-string v2, "Failed to share/unshare"

    #@3a
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d
    goto :goto_31
.end method

.method private doUnmountVolume(Ljava/lang/String;ZZ)I
    .registers 13
    .parameter "path"
    .parameter "force"
    .parameter "removeEncryption"

    #@0
    .prologue
    const/16 v3, 0x194

    #@2
    const/4 v4, 0x0

    #@3
    .line 1043
    invoke-virtual {p0, p1}, Lcom/android/server/MountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v5

    #@7
    const-string v6, "mounted"

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v5

    #@d
    if-nez v5, :cond_10

    #@f
    .line 1079
    :goto_f
    return v3

    #@10
    .line 1053
    :cond_10
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5}, Ljava/lang/Runtime;->gc()V

    #@17
    .line 1056
    iget-object v5, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@19
    invoke-virtual {v5, v4, v4}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    #@1c
    .line 1058
    :try_start_1c
    new-instance v0, Lcom/android/server/NativeDaemonConnector$Command;

    #@1e
    const-string v5, "volume"

    #@20
    const/4 v6, 0x2

    #@21
    new-array v6, v6, [Ljava/lang/Object;

    #@23
    const/4 v7, 0x0

    #@24
    const-string v8, "unmount"

    #@26
    aput-object v8, v6, v7

    #@28
    const/4 v7, 0x1

    #@29
    aput-object p1, v6, v7

    #@2b
    invoke-direct {v0, v5, v6}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@2e
    .line 1059
    .local v0, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    if-eqz p3, :cond_45

    #@30
    .line 1060
    const-string v5, "force_and_revert"

    #@32
    invoke-virtual {v0, v5}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@35
    .line 1064
    :cond_35
    :goto_35
    iget-object v5, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@37
    invoke-virtual {v5, v0}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;

    #@3a
    .line 1066
    iget-object v5, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@3c
    monitor-enter v5
    :try_end_3d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1c .. :try_end_3d} :catch_4d

    #@3d
    .line 1067
    :try_start_3d
    iget-object v6, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@3f
    invoke-virtual {v6}, Ljava/util/HashSet;->clear()V

    #@42
    .line 1068
    monitor-exit v5
    :try_end_43
    .catchall {:try_start_3d .. :try_end_43} :catchall_56

    #@43
    move v3, v4

    #@44
    .line 1069
    goto :goto_f

    #@45
    .line 1061
    :cond_45
    if-eqz p2, :cond_35

    #@47
    .line 1062
    :try_start_47
    const-string v5, "force"

    #@49
    invoke-virtual {v0, v5}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;
    :try_end_4c
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_47 .. :try_end_4c} :catch_4d

    #@4c
    goto :goto_35

    #@4d
    .line 1070
    .end local v0           #cmd:Lcom/android/server/NativeDaemonConnector$Command;
    :catch_4d
    move-exception v2

    #@4e
    .line 1073
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@51
    move-result v1

    #@52
    .line 1074
    .local v1, code:I
    if-ne v1, v3, :cond_59

    #@54
    .line 1075
    const/4 v3, -0x5

    #@55
    goto :goto_f

    #@56
    .line 1068
    .end local v1           #code:I
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v0       #cmd:Lcom/android/server/NativeDaemonConnector$Command;
    :catchall_56
    move-exception v4

    #@57
    :try_start_57
    monitor-exit v5
    :try_end_58
    .catchall {:try_start_57 .. :try_end_58} :catchall_56

    #@58
    :try_start_58
    throw v4
    :try_end_59
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_58 .. :try_end_59} :catch_4d

    #@59
    .line 1076
    .end local v0           #cmd:Lcom/android/server/NativeDaemonConnector$Command;
    .restart local v1       #code:I
    .restart local v2       #e:Lcom/android/server/NativeDaemonConnectorException;
    :cond_59
    const/16 v3, 0x195

    #@5b
    if-ne v1, v3, :cond_5f

    #@5d
    .line 1077
    const/4 v3, -0x7

    #@5e
    goto :goto_f

    #@5f
    .line 1079
    :cond_5f
    const/4 v3, -0x1

    #@60
    goto :goto_f
.end method

.method private getPrimaryPhysicalVolume()Landroid/os/storage/StorageVolume;
    .registers 5

    #@0
    .prologue
    .line 1325
    iget-object v3, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 1326
    :try_start_3
    iget-object v2, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v0

    #@9
    .local v0, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_23

    #@f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/os/storage/StorageVolume;

    #@15
    .line 1327
    .local v1, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v1}, Landroid/os/storage/StorageVolume;->isPrimary()Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_9

    #@1b
    invoke-virtual {v1}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    #@1e
    move-result v2

    #@1f
    if-nez v2, :cond_9

    #@21
    .line 1328
    monitor-exit v3

    #@22
    .line 1332
    .end local v1           #volume:Landroid/os/storage/StorageVolume;
    :goto_22
    return-object v1

    #@23
    .line 1331
    :cond_23
    monitor-exit v3

    #@24
    .line 1332
    const/4 v1, 0x0

    #@25
    goto :goto_22

    #@26
    .line 1331
    .end local v0           #i$:Ljava/util/Iterator;
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method

.method private getUmsEnabling()Z
    .registers 3

    #@0
    .prologue
    .line 1479
    iget-object v1, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 1480
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/MountService;->mUmsEnabling:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 1481
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private handleSystemReady()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 520
    iget-object v8, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@4
    monitor-enter v8

    #@5
    .line 521
    :try_start_5
    new-instance v4, Ljava/util/HashMap;

    #@7
    iget-object v7, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@9
    invoke-direct {v4, v7}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@c
    .line 522
    .local v4, snapshot:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-exit v8
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_4f

    #@d
    .line 524
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@10
    move-result-object v7

    #@11
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v1

    #@15
    .local v1, i$:Ljava/util/Iterator;
    :cond_15
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v7

    #@19
    if-eqz v7, :cond_60

    #@1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Ljava/util/Map$Entry;

    #@21
    .line 525
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Ljava/lang/String;

    #@27
    .line 526
    .local v2, path:Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2a
    move-result-object v5

    #@2b
    check-cast v5, Ljava/lang/String;

    #@2d
    .line 528
    .local v5, state:Ljava/lang/String;
    const-string v7, "unmounted"

    #@2f
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v7

    #@33
    if-eqz v7, :cond_52

    #@35
    .line 529
    invoke-direct {p0, v2}, Lcom/android/server/MountService;->doMountVolume(Ljava/lang/String;)I

    #@38
    move-result v3

    #@39
    .line 530
    .local v3, rc:I
    if-eqz v3, :cond_15

    #@3b
    .line 531
    const-string v7, "MountService"

    #@3d
    const-string v8, "Boot-time mount failed (%d)"

    #@3f
    new-array v9, v12, [Ljava/lang/Object;

    #@41
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44
    move-result-object v10

    #@45
    aput-object v10, v9, v11

    #@47
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4a
    move-result-object v8

    #@4b
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_15

    #@4f
    .line 522
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #path:Ljava/lang/String;
    .end local v3           #rc:I
    .end local v4           #snapshot:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5           #state:Ljava/lang/String;
    :catchall_4f
    move-exception v7

    #@50
    :try_start_50
    monitor-exit v8
    :try_end_51
    .catchall {:try_start_50 .. :try_end_51} :catchall_4f

    #@51
    throw v7

    #@52
    .line 534
    .restart local v0       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #path:Ljava/lang/String;
    .restart local v4       #snapshot:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5       #state:Ljava/lang/String;
    :cond_52
    const-string v7, "shared"

    #@54
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v7

    #@58
    if-eqz v7, :cond_15

    #@5a
    .line 539
    const/4 v7, 0x0

    #@5b
    const/4 v8, 0x7

    #@5c
    invoke-direct {p0, v7, v2, v11, v8}, Lcom/android/server/MountService;->notifyVolumeStateChange(Ljava/lang/String;Ljava/lang/String;II)V

    #@5f
    goto :goto_15

    #@60
    .line 545
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2           #path:Ljava/lang/String;
    .end local v5           #state:Ljava/lang/String;
    :cond_60
    iget-object v8, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@62
    monitor-enter v8

    #@63
    .line 546
    :try_start_63
    iget-object v7, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@68
    move-result-object v1

    #@69
    :cond_69
    :goto_69
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@6c
    move-result v7

    #@6d
    if-eqz v7, :cond_84

    #@6f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@72
    move-result-object v6

    #@73
    check-cast v6, Landroid/os/storage/StorageVolume;

    #@75
    .line 547
    .local v6, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    #@78
    move-result v7

    #@79
    if-eqz v7, :cond_69

    #@7b
    .line 548
    const-string v7, "mounted"

    #@7d
    invoke-direct {p0, v6, v7}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@80
    goto :goto_69

    #@81
    .line 551
    .end local v6           #volume:Landroid/os/storage/StorageVolume;
    :catchall_81
    move-exception v7

    #@82
    monitor-exit v8
    :try_end_83
    .catchall {:try_start_63 .. :try_end_83} :catchall_81

    #@83
    throw v7

    #@84
    :cond_84
    :try_start_84
    monitor-exit v8
    :try_end_85
    .catchall {:try_start_84 .. :try_end_85} :catchall_81

    #@85
    .line 557
    iget-boolean v7, p0, Lcom/android/server/MountService;->mSendUmsConnectedOnBoot:Z

    #@87
    if-eqz v7, :cond_8e

    #@89
    .line 558
    invoke-direct {p0, v12}, Lcom/android/server/MountService;->sendUmsIntent(Z)V

    #@8c
    .line 559
    iput-boolean v11, p0, Lcom/android/server/MountService;->mSendUmsConnectedOnBoot:Z

    #@8e
    .line 561
    :cond_8e
    return-void
.end method

.method private isUidOwnerOfPackageOrSystem(Ljava/lang/String;I)Z
    .registers 8
    .parameter "packageName"
    .parameter "callerUid"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1925
    const/16 v3, 0x3e8

    #@4
    if-ne p2, v3, :cond_7

    #@6
    .line 1940
    :cond_6
    :goto_6
    return v1

    #@7
    .line 1929
    :cond_7
    if-nez p1, :cond_b

    #@9
    move v1, v2

    #@a
    .line 1930
    goto :goto_6

    #@b
    .line 1933
    :cond_b
    iget-object v3, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@d
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    #@10
    move-result v4

    #@11
    invoke-virtual {v3, p1, v4}, Lcom/android/server/pm/PackageManagerService;->getPackageUid(Ljava/lang/String;I)I

    #@14
    move-result v0

    #@15
    .line 1940
    .local v0, packageUid:I
    if-eq p2, v0, :cond_6

    #@17
    move v1, v2

    #@18
    goto :goto_6
.end method

.method private notifyShareAvailabilityChange(Z)V
    .registers 11
    .parameter "avail"

    #@0
    .prologue
    .line 1117
    iget-object v7, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v7

    #@3
    .line 1118
    :try_start_3
    iput-boolean p1, p0, Lcom/android/server/MountService;->mUmsAvailable:Z

    #@5
    .line 1119
    iget-object v6, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v6

    #@b
    add-int/lit8 v2, v6, -0x1

    #@d
    .local v2, i:I
    :goto_d
    if-ltz v2, :cond_39

    #@f
    .line 1120
    iget-object v6, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/MountService$MountServiceBinderListener;
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_2d

    #@17
    .line 1122
    .local v0, bl:Lcom/android/server/MountService$MountServiceBinderListener;
    :try_start_17
    iget-object v6, v0, Lcom/android/server/MountService$MountServiceBinderListener;->mListener:Landroid/os/storage/IMountServiceListener;

    #@19
    invoke-interface {v6, p1}, Landroid/os/storage/IMountServiceListener;->onUsbMassStorageConnectionChanged(Z)V
    :try_end_1c
    .catchall {:try_start_17 .. :try_end_1c} :catchall_2d
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_1c} :catch_1f
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1c} :catch_30

    #@1c
    .line 1119
    :goto_1c
    add-int/lit8 v2, v2, -0x1

    #@1e
    goto :goto_d

    #@1f
    .line 1123
    :catch_1f
    move-exception v5

    #@20
    .line 1124
    .local v5, rex:Landroid/os/RemoteException;
    :try_start_20
    const-string v6, "MountService"

    #@22
    const-string v8, "Listener dead"

    #@24
    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1125
    iget-object v6, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2c
    goto :goto_1c

    #@2d
    .line 1130
    .end local v0           #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .end local v2           #i:I
    .end local v5           #rex:Landroid/os/RemoteException;
    :catchall_2d
    move-exception v6

    #@2e
    monitor-exit v7
    :try_end_2f
    .catchall {:try_start_20 .. :try_end_2f} :catchall_2d

    #@2f
    throw v6

    #@30
    .line 1126
    .restart local v0       #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .restart local v2       #i:I
    :catch_30
    move-exception v1

    #@31
    .line 1127
    .local v1, ex:Ljava/lang/Exception;
    :try_start_31
    const-string v6, "MountService"

    #@33
    const-string v8, "Listener failed"

    #@35
    invoke-static {v6, v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    goto :goto_1c

    #@39
    .line 1130
    .end local v0           #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .end local v1           #ex:Ljava/lang/Exception;
    :cond_39
    monitor-exit v7
    :try_end_3a
    .catchall {:try_start_31 .. :try_end_3a} :catchall_2d

    #@3a
    .line 1132
    iget-boolean v6, p0, Lcom/android/server/MountService;->mSystemReady:Z

    #@3c
    const/4 v7, 0x1

    #@3d
    if-ne v6, v7, :cond_67

    #@3f
    .line 1133
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->sendUmsIntent(Z)V

    #@42
    .line 1138
    :goto_42
    invoke-direct {p0}, Lcom/android/server/MountService;->getPrimaryPhysicalVolume()Landroid/os/storage/StorageVolume;

    #@45
    move-result-object v4

    #@46
    .line 1139
    .local v4, primary:Landroid/os/storage/StorageVolume;
    if-nez p1, :cond_66

    #@48
    if-eqz v4, :cond_66

    #@4a
    const-string v6, "shared"

    #@4c
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@4f
    move-result-object v7

    #@50
    invoke-virtual {p0, v7}, Lcom/android/server/MountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v6

    #@58
    if-eqz v6, :cond_66

    #@5a
    .line 1141
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    .line 1145
    .local v3, path:Ljava/lang/String;
    new-instance v6, Lcom/android/server/MountService$5;

    #@60
    invoke-direct {v6, p0, v3}, Lcom/android/server/MountService$5;-><init>(Lcom/android/server/MountService;Ljava/lang/String;)V

    #@63
    invoke-virtual {v6}, Lcom/android/server/MountService$5;->start()V

    #@66
    .line 1163
    .end local v3           #path:Ljava/lang/String;
    :cond_66
    return-void

    #@67
    .line 1135
    .end local v4           #primary:Landroid/os/storage/StorageVolume;
    :cond_67
    iput-boolean p1, p0, Lcom/android/server/MountService;->mSendUmsConnectedOnBoot:Z

    #@69
    goto :goto_42
.end method

.method private notifyVolumeStateChange(Ljava/lang/String;Ljava/lang/String;II)V
    .registers 11
    .parameter "label"
    .parameter "path"
    .parameter "oldState"
    .parameter "newState"

    #@0
    .prologue
    const/4 v5, 0x7

    #@1
    .line 890
    iget-object v4, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@3
    monitor-enter v4

    #@4
    .line 891
    :try_start_4
    iget-object v3, p0, Lcom/android/server/MountService;->mVolumesByPath:Ljava/util/HashMap;

    #@6
    invoke-virtual {v3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    check-cast v2, Landroid/os/storage/StorageVolume;

    #@c
    .line 892
    .local v2, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {p0, p2}, Lcom/android/server/MountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 893
    .local v1, state:Ljava/lang/String;
    monitor-exit v4
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_28

    #@11
    .line 897
    const/4 v0, 0x0

    #@12
    .line 899
    .local v0, action:Ljava/lang/String;
    if-ne p3, v5, :cond_1d

    #@14
    if-eq p4, p3, :cond_1d

    #@16
    .line 901
    const-string v3, "android.intent.action.MEDIA_UNSHARED"

    #@18
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1a
    invoke-direct {p0, v3, v2, v4}, Lcom/android/server/MountService;->sendStorageIntent(Ljava/lang/String;Landroid/os/storage/StorageVolume;Landroid/os/UserHandle;)V

    #@1d
    .line 904
    :cond_1d
    const/4 v3, -0x1

    #@1e
    if-ne p4, v3, :cond_2b

    #@20
    .line 953
    :cond_20
    :goto_20
    if-eqz v0, :cond_27

    #@22
    .line 954
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@24
    invoke-direct {p0, v0, v2, v3}, Lcom/android/server/MountService;->sendStorageIntent(Ljava/lang/String;Landroid/os/storage/StorageVolume;Landroid/os/UserHandle;)V

    #@27
    .line 956
    :cond_27
    :goto_27
    return-void

    #@28
    .line 893
    .end local v0           #action:Ljava/lang/String;
    .end local v1           #state:Ljava/lang/String;
    .end local v2           #volume:Landroid/os/storage/StorageVolume;
    :catchall_28
    move-exception v3

    #@29
    :try_start_29
    monitor-exit v4
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    #@2a
    throw v3

    #@2b
    .line 905
    .restart local v0       #action:Ljava/lang/String;
    .restart local v1       #state:Ljava/lang/String;
    .restart local v2       #volume:Landroid/os/storage/StorageVolume;
    :cond_2b
    if-eqz p4, :cond_20

    #@2d
    .line 907
    const/4 v3, 0x1

    #@2e
    if-ne p4, v3, :cond_56

    #@30
    .line 912
    const-string v3, "bad_removal"

    #@32
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v3

    #@36
    if-nez v3, :cond_20

    #@38
    const-string v3, "nofs"

    #@3a
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v3

    #@3e
    if-nez v3, :cond_20

    #@40
    const-string v3, "unmountable"

    #@42
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v3

    #@46
    if-nez v3, :cond_20

    #@48
    invoke-direct {p0}, Lcom/android/server/MountService;->getUmsEnabling()Z

    #@4b
    move-result v3

    #@4c
    if-nez v3, :cond_20

    #@4e
    .line 917
    const-string v3, "unmounted"

    #@50
    invoke-direct {p0, v2, v3}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@53
    .line 918
    const-string v0, "android.intent.action.MEDIA_UNMOUNTED"

    #@55
    goto :goto_20

    #@56
    .line 920
    :cond_56
    const/16 v3, 0x9

    #@58
    if-ne p4, v3, :cond_62

    #@5a
    .line 922
    const-string v3, "nofs"

    #@5c
    invoke-direct {p0, v2, v3}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@5f
    .line 923
    const-string v0, "android.intent.action.MEDIA_NOFS"

    #@61
    goto :goto_20

    #@62
    .line 924
    :cond_62
    const/4 v3, 0x2

    #@63
    if-eq p4, v3, :cond_20

    #@65
    .line 925
    const/4 v3, 0x3

    #@66
    if-ne p4, v3, :cond_70

    #@68
    .line 927
    const-string v3, "checking"

    #@6a
    invoke-direct {p0, v2, v3}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@6d
    .line 928
    const-string v0, "android.intent.action.MEDIA_CHECKING"

    #@6f
    goto :goto_20

    #@70
    .line 929
    :cond_70
    const/4 v3, 0x4

    #@71
    if-ne p4, v3, :cond_7b

    #@73
    .line 931
    const-string v3, "mounted"

    #@75
    invoke-direct {p0, v2, v3}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@78
    .line 932
    const-string v0, "android.intent.action.MEDIA_MOUNTED"

    #@7a
    goto :goto_20

    #@7b
    .line 933
    :cond_7b
    const/4 v3, 0x5

    #@7c
    if-ne p4, v3, :cond_81

    #@7e
    .line 934
    const-string v0, "android.intent.action.MEDIA_EJECT"

    #@80
    goto :goto_20

    #@81
    .line 935
    :cond_81
    const/4 v3, 0x6

    #@82
    if-eq p4, v3, :cond_20

    #@84
    .line 936
    if-ne p4, v5, :cond_9a

    #@86
    .line 939
    const-string v3, "unmounted"

    #@88
    invoke-direct {p0, v2, v3}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@8b
    .line 940
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    #@8d
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@8f
    invoke-direct {p0, v3, v2, v4}, Lcom/android/server/MountService;->sendStorageIntent(Ljava/lang/String;Landroid/os/storage/StorageVolume;Landroid/os/UserHandle;)V

    #@92
    .line 943
    const-string v3, "shared"

    #@94
    invoke-direct {p0, v2, v3}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@97
    .line 944
    const-string v0, "android.intent.action.MEDIA_SHARED"

    #@99
    goto :goto_20

    #@9a
    .line 946
    :cond_9a
    const/16 v3, 0x8

    #@9c
    if-ne p4, v3, :cond_a6

    #@9e
    .line 947
    const-string v3, "MountService"

    #@a0
    const-string v4, "Live shared mounts not supported yet!"

    #@a2
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    goto :goto_27

    #@a6
    .line 950
    :cond_a6
    const-string v3, "MountService"

    #@a8
    new-instance v4, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v5, "Unhandled VolumeState {"

    #@af
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v4

    #@b3
    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v4

    #@b7
    const-string v5, "}"

    #@b9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v4

    #@bd
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v4

    #@c1
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    goto/16 :goto_20
.end method

.method private readStorageListLocked()V
    .registers 32

    #@0
    .prologue
    .line 1189
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@7
    .line 1190
    move-object/from16 v0, p0

    #@9
    iget-object v3, v0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@b
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@e
    .line 1192
    move-object/from16 v0, p0

    #@10
    iget-object v3, v0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@15
    move-result-object v24

    #@16
    .line 1194
    .local v24, resources:Landroid/content/res/Resources;
    const v19, 0x10f000f

    #@19
    .line 1195
    .local v19, id:I
    move-object/from16 v0, v24

    #@1b
    move/from16 v1, v19

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@20
    move-result-object v22

    #@21
    .line 1196
    .local v22, parser:Landroid/content/res/XmlResourceParser;
    invoke-static/range {v22 .. v22}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@24
    move-result-object v14

    #@25
    .line 1199
    .local v14, attrs:Landroid/util/AttributeSet;
    :try_start_25
    const-string v3, "StorageList"

    #@27
    move-object/from16 v0, v22

    #@29
    invoke-static {v0, v3}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@2c
    .line 1201
    :cond_2c
    :goto_2c
    invoke-static/range {v22 .. v22}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2f
    .line 1203
    invoke-interface/range {v22 .. v22}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;
    :try_end_32
    .catchall {:try_start_25 .. :try_end_32} :catchall_161
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_25 .. :try_end_32} :catch_158
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_32} :catch_19e

    #@32
    move-result-object v17

    #@33
    .line 1204
    .local v17, element:Ljava/lang/String;
    if-nez v17, :cond_61

    #@35
    .line 1274
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/MountService;->isExternalStorageEmulated()Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_1d4

    #@3b
    const/16 v20, 0x1

    #@3d
    .line 1275
    .local v20, index:I
    :goto_3d
    move-object/from16 v0, p0

    #@3f
    iget-object v3, v0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@44
    move-result-object v18

    #@45
    .local v18, i$:Ljava/util/Iterator;
    :cond_45
    :goto_45
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    #@48
    move-result v3

    #@49
    if-eqz v3, :cond_1cd

    #@4b
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4e
    move-result-object v2

    #@4f
    check-cast v2, Landroid/os/storage/StorageVolume;

    #@51
    .line 1276
    .local v2, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    #@54
    move-result v3

    #@55
    if-nez v3, :cond_45

    #@57
    .line 1277
    add-int/lit8 v21, v20, 0x1

    #@59
    .end local v20           #index:I
    .local v21, index:I
    move/from16 v0, v20

    #@5b
    invoke-virtual {v2, v0}, Landroid/os/storage/StorageVolume;->setStorageId(I)V

    #@5e
    move/from16 v20, v21

    #@60
    .end local v21           #index:I
    .restart local v20       #index:I
    goto :goto_45

    #@61
    .line 1206
    .end local v2           #volume:Landroid/os/storage/StorageVolume;
    .end local v18           #i$:Ljava/util/Iterator;
    .end local v20           #index:I
    :cond_61
    :try_start_61
    const-string v3, "storage"

    #@63
    move-object/from16 v0, v17

    #@65
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v3

    #@69
    if-eqz v3, :cond_2c

    #@6b
    .line 1207
    sget-object v3, Lcom/android/internal/R$styleable;->Storage:[I

    #@6d
    move-object/from16 v0, v24

    #@6f
    invoke-virtual {v0, v14, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@72
    move-result-object v13

    #@73
    .line 1210
    .local v13, a:Landroid/content/res/TypedArray;
    const/4 v3, 0x0

    #@74
    invoke-virtual {v13, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@77
    move-result-object v23

    #@78
    .line 1212
    .local v23, path:Ljava/lang/String;
    const/4 v3, 0x1

    #@79
    const/4 v12, -0x1

    #@7a
    invoke-virtual {v13, v3, v12}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@7d
    move-result v4

    #@7e
    .line 1214
    .local v4, descriptionId:I
    const/4 v3, 0x1

    #@7f
    invoke-virtual {v13, v3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@82
    move-result-object v15

    #@83
    .line 1216
    .local v15, description:Ljava/lang/CharSequence;
    const/4 v3, 0x2

    #@84
    const/4 v12, 0x0

    #@85
    invoke-virtual {v13, v3, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@88
    move-result v5

    #@89
    .line 1218
    .local v5, primary:Z
    const/4 v3, 0x3

    #@8a
    const/4 v12, 0x0

    #@8b
    invoke-virtual {v13, v3, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@8e
    move-result v6

    #@8f
    .line 1220
    .local v6, removable:Z
    const/4 v3, 0x4

    #@90
    const/4 v12, 0x0

    #@91
    invoke-virtual {v13, v3, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@94
    move-result v7

    #@95
    .line 1222
    .local v7, emulated:Z
    const/4 v3, 0x5

    #@96
    const/4 v12, 0x0

    #@97
    invoke-virtual {v13, v3, v12}, Landroid/content/res/TypedArray;->getInt(II)I

    #@9a
    move-result v8

    #@9b
    .line 1224
    .local v8, mtpReserve:I
    const/4 v3, 0x6

    #@9c
    const/4 v12, 0x0

    #@9d
    invoke-virtual {v13, v3, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a0
    move-result v9

    #@a1
    .line 1227
    .local v9, allowMassStorage:Z
    const/4 v3, 0x7

    #@a2
    const/4 v12, 0x0

    #@a3
    invoke-virtual {v13, v3, v12}, Landroid/content/res/TypedArray;->getInt(II)I

    #@a6
    move-result v3

    #@a7
    int-to-long v0, v3

    #@a8
    move-wide/from16 v27, v0

    #@aa
    const-wide/16 v29, 0x400

    #@ac
    mul-long v27, v27, v29

    #@ae
    const-wide/16 v29, 0x400

    #@b0
    mul-long v10, v27, v29

    #@b2
    .line 1230
    .local v10, maxFileSize:J
    const-string v3, "MountService"

    #@b4
    new-instance v12, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v27, "got storage path: "

    #@bb
    move-object/from16 v0, v27

    #@bd
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v12

    #@c1
    move-object/from16 v0, v23

    #@c3
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v12

    #@c7
    const-string v27, " description: "

    #@c9
    move-object/from16 v0, v27

    #@cb
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v12

    #@cf
    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v12

    #@d3
    const-string v27, " primary: "

    #@d5
    move-object/from16 v0, v27

    #@d7
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v12

    #@db
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@de
    move-result-object v12

    #@df
    const-string v27, " removable: "

    #@e1
    move-object/from16 v0, v27

    #@e3
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v12

    #@e7
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v12

    #@eb
    const-string v27, " emulated: "

    #@ed
    move-object/from16 v0, v27

    #@ef
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v12

    #@f3
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v12

    #@f7
    const-string v27, " mtpReserve: "

    #@f9
    move-object/from16 v0, v27

    #@fb
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v12

    #@ff
    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@102
    move-result-object v12

    #@103
    const-string v27, " allowMassStorage: "

    #@105
    move-object/from16 v0, v27

    #@107
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v12

    #@10b
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v12

    #@10f
    const-string v27, " maxFileSize: "

    #@111
    move-object/from16 v0, v27

    #@113
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v12

    #@117
    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v12

    #@11b
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v12

    #@11f
    invoke-static {v3, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@122
    .line 1236
    if-eqz v7, :cond_18e

    #@124
    .line 1239
    new-instance v2, Landroid/os/storage/StorageVolume;

    #@126
    const/4 v3, 0x0

    #@127
    const/4 v5, 0x1

    #@128
    const/4 v6, 0x0

    #@129
    const/4 v7, 0x1

    #@12a
    const/4 v9, 0x0

    #@12b
    const/4 v12, 0x0

    #@12c
    invoke-direct/range {v2 .. v12}, Landroid/os/storage/StorageVolume;-><init>(Ljava/io/File;IZZZIZJLandroid/os/UserHandle;)V

    #@12f
    .end local v5           #primary:Z
    .end local v6           #removable:Z
    .end local v7           #emulated:Z
    .end local v9           #allowMassStorage:Z
    move-object/from16 v0, p0

    #@131
    iput-object v2, v0, Lcom/android/server/MountService;->mEmulatedTemplate:Landroid/os/storage/StorageVolume;

    #@133
    .line 1242
    invoke-static {}, Lcom/android/server/pm/UserManagerService;->getInstance()Lcom/android/server/pm/UserManagerService;

    #@136
    move-result-object v26

    #@137
    .line 1243
    .local v26, userManager:Lcom/android/server/pm/UserManagerService;
    const/4 v3, 0x0

    #@138
    move-object/from16 v0, v26

    #@13a
    invoke-virtual {v0, v3}, Lcom/android/server/pm/UserManagerService;->getUsers(Z)Ljava/util/List;

    #@13d
    move-result-object v3

    #@13e
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@141
    move-result-object v18

    #@142
    .restart local v18       #i$:Ljava/util/Iterator;
    :goto_142
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    #@145
    move-result v3

    #@146
    if-eqz v3, :cond_199

    #@148
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14b
    move-result-object v25

    #@14c
    check-cast v25, Landroid/content/pm/UserInfo;

    #@14e
    .line 1244
    .local v25, user:Landroid/content/pm/UserInfo;
    invoke-virtual/range {v25 .. v25}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    #@151
    move-result-object v3

    #@152
    move-object/from16 v0, p0

    #@154
    invoke-direct {v0, v3}, Lcom/android/server/MountService;->createEmulatedVolumeForUserLocked(Landroid/os/UserHandle;)V
    :try_end_157
    .catchall {:try_start_61 .. :try_end_157} :catchall_161
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_61 .. :try_end_157} :catch_158
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_157} :catch_19e

    #@157
    goto :goto_142

    #@158
    .line 1267
    .end local v4           #descriptionId:I
    .end local v8           #mtpReserve:I
    .end local v10           #maxFileSize:J
    .end local v13           #a:Landroid/content/res/TypedArray;
    .end local v15           #description:Ljava/lang/CharSequence;
    .end local v17           #element:Ljava/lang/String;
    .end local v18           #i$:Ljava/util/Iterator;
    .end local v23           #path:Ljava/lang/String;
    .end local v25           #user:Landroid/content/pm/UserInfo;
    .end local v26           #userManager:Lcom/android/server/pm/UserManagerService;
    :catch_158
    move-exception v16

    #@159
    .line 1268
    .local v16, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_159
    new-instance v3, Ljava/lang/RuntimeException;

    #@15b
    move-object/from16 v0, v16

    #@15d
    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@160
    throw v3
    :try_end_161
    .catchall {:try_start_159 .. :try_end_161} :catchall_161

    #@161
    .line 1274
    .end local v16           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_161
    move-exception v3

    #@162
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/MountService;->isExternalStorageEmulated()Z

    #@165
    move-result v12

    #@166
    if-eqz v12, :cond_1d1

    #@168
    const/16 v20, 0x1

    #@16a
    .line 1275
    .restart local v20       #index:I
    :goto_16a
    move-object/from16 v0, p0

    #@16c
    iget-object v12, v0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@16e
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@171
    move-result-object v18

    #@172
    .restart local v18       #i$:Ljava/util/Iterator;
    :cond_172
    :goto_172
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    #@175
    move-result v12

    #@176
    if-eqz v12, :cond_1c9

    #@178
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17b
    move-result-object v2

    #@17c
    check-cast v2, Landroid/os/storage/StorageVolume;

    #@17e
    .line 1276
    .restart local v2       #volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    #@181
    move-result v12

    #@182
    if-nez v12, :cond_172

    #@184
    .line 1277
    add-int/lit8 v21, v20, 0x1

    #@186
    .end local v20           #index:I
    .restart local v21       #index:I
    move/from16 v0, v20

    #@188
    invoke-virtual {v2, v0}, Landroid/os/storage/StorageVolume;->setStorageId(I)V

    #@18b
    move/from16 v20, v21

    #@18d
    .end local v21           #index:I
    .restart local v20       #index:I
    goto :goto_172

    #@18e
    .line 1248
    .end local v2           #volume:Landroid/os/storage/StorageVolume;
    .end local v18           #i$:Ljava/util/Iterator;
    .end local v20           #index:I
    .restart local v4       #descriptionId:I
    .restart local v5       #primary:Z
    .restart local v6       #removable:Z
    .restart local v7       #emulated:Z
    .restart local v8       #mtpReserve:I
    .restart local v9       #allowMassStorage:Z
    .restart local v10       #maxFileSize:J
    .restart local v13       #a:Landroid/content/res/TypedArray;
    .restart local v15       #description:Ljava/lang/CharSequence;
    .restart local v17       #element:Ljava/lang/String;
    .restart local v23       #path:Ljava/lang/String;
    :cond_18e
    if-eqz v23, :cond_192

    #@190
    if-nez v15, :cond_1a7

    #@192
    .line 1249
    :cond_192
    :try_start_192
    const-string v3, "MountService"

    #@194
    const-string v12, "Missing storage path or description in readStorageList"

    #@196
    invoke-static {v3, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@199
    .line 1264
    .end local v5           #primary:Z
    .end local v6           #removable:Z
    .end local v7           #emulated:Z
    .end local v9           #allowMassStorage:Z
    :cond_199
    :goto_199
    invoke-virtual {v13}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_19c
    .catchall {:try_start_192 .. :try_end_19c} :catchall_161
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_192 .. :try_end_19c} :catch_158
    .catch Ljava/io/IOException; {:try_start_192 .. :try_end_19c} :catch_19e

    #@19c
    goto/16 :goto_2c

    #@19e
    .line 1269
    .end local v4           #descriptionId:I
    .end local v8           #mtpReserve:I
    .end local v10           #maxFileSize:J
    .end local v13           #a:Landroid/content/res/TypedArray;
    .end local v15           #description:Ljava/lang/CharSequence;
    .end local v17           #element:Ljava/lang/String;
    .end local v23           #path:Ljava/lang/String;
    :catch_19e
    move-exception v16

    #@19f
    .line 1270
    .local v16, e:Ljava/io/IOException;
    :try_start_19f
    new-instance v3, Ljava/lang/RuntimeException;

    #@1a1
    move-object/from16 v0, v16

    #@1a3
    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@1a6
    throw v3
    :try_end_1a7
    .catchall {:try_start_19f .. :try_end_1a7} :catchall_161

    #@1a7
    .line 1251
    .end local v16           #e:Ljava/io/IOException;
    .restart local v4       #descriptionId:I
    .restart local v5       #primary:Z
    .restart local v6       #removable:Z
    .restart local v7       #emulated:Z
    .restart local v8       #mtpReserve:I
    .restart local v9       #allowMassStorage:Z
    .restart local v10       #maxFileSize:J
    .restart local v13       #a:Landroid/content/res/TypedArray;
    .restart local v15       #description:Ljava/lang/CharSequence;
    .restart local v17       #element:Ljava/lang/String;
    .restart local v23       #path:Ljava/lang/String;
    :cond_1a7
    :try_start_1a7
    new-instance v2, Landroid/os/storage/StorageVolume;

    #@1a9
    new-instance v3, Ljava/io/File;

    #@1ab
    move-object/from16 v0, v23

    #@1ad
    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1b0
    const/4 v12, 0x0

    #@1b1
    invoke-direct/range {v2 .. v12}, Landroid/os/storage/StorageVolume;-><init>(Ljava/io/File;IZZZIZJLandroid/os/UserHandle;)V

    #@1b4
    .line 1254
    .restart local v2       #volume:Landroid/os/storage/StorageVolume;
    move-object/from16 v0, p0

    #@1b6
    invoke-direct {v0, v2}, Lcom/android/server/MountService;->addVolumeLocked(Landroid/os/storage/StorageVolume;)V

    #@1b9
    .line 1259
    move-object/from16 v0, p0

    #@1bb
    iget-object v3, v0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@1bd
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@1c0
    move-result-object v12

    #@1c1
    const-string v27, "removed"

    #@1c3
    move-object/from16 v0, v27

    #@1c5
    invoke-virtual {v3, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1c8
    .catchall {:try_start_1a7 .. :try_end_1c8} :catchall_161
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1a7 .. :try_end_1c8} :catch_158
    .catch Ljava/io/IOException; {:try_start_1a7 .. :try_end_1c8} :catch_19e

    #@1c8
    goto :goto_199

    #@1c9
    .line 1280
    .end local v2           #volume:Landroid/os/storage/StorageVolume;
    .end local v4           #descriptionId:I
    .end local v5           #primary:Z
    .end local v6           #removable:Z
    .end local v7           #emulated:Z
    .end local v8           #mtpReserve:I
    .end local v9           #allowMassStorage:Z
    .end local v10           #maxFileSize:J
    .end local v13           #a:Landroid/content/res/TypedArray;
    .end local v15           #description:Ljava/lang/CharSequence;
    .end local v17           #element:Ljava/lang/String;
    .end local v23           #path:Ljava/lang/String;
    .restart local v18       #i$:Ljava/util/Iterator;
    .restart local v20       #index:I
    :cond_1c9
    invoke-interface/range {v22 .. v22}, Landroid/content/res/XmlResourceParser;->close()V

    #@1cc
    .line 1274
    throw v3

    #@1cd
    .line 1280
    .restart local v17       #element:Ljava/lang/String;
    :cond_1cd
    invoke-interface/range {v22 .. v22}, Landroid/content/res/XmlResourceParser;->close()V

    #@1d0
    .line 1282
    return-void

    #@1d1
    .line 1274
    .end local v17           #element:Ljava/lang/String;
    .end local v18           #i$:Ljava/util/Iterator;
    .end local v20           #index:I
    :cond_1d1
    const/16 v20, 0x0

    #@1d3
    goto :goto_16a

    #@1d4
    .restart local v17       #element:Ljava/lang/String;
    :cond_1d4
    const/16 v20, 0x0

    #@1d6
    goto/16 :goto_3d
.end method

.method private removeObbStateLocked(Lcom/android/server/MountService$ObbState;)V
    .registers 6
    .parameter "obbState"

    #@0
    .prologue
    .line 2223
    invoke-virtual {p1}, Lcom/android/server/MountService$ObbState;->getBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    .line 2224
    .local v0, binder:Landroid/os/IBinder;
    iget-object v2, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@6
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Ljava/util/List;

    #@c
    .line 2225
    .local v1, obbStates:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    if-eqz v1, :cond_22

    #@e
    .line 2226
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_17

    #@14
    .line 2227
    invoke-virtual {p1}, Lcom/android/server/MountService$ObbState;->unlink()V

    #@17
    .line 2229
    :cond_17
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_22

    #@1d
    .line 2230
    iget-object v2, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@1f
    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 2234
    :cond_22
    iget-object v2, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@24
    iget-object v3, p1, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@26
    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    .line 2235
    return-void
.end method

.method private removeVolumeLocked(Landroid/os/storage/StorageVolume;)V
    .registers 5
    .parameter "volume"

    #@0
    .prologue
    .line 1318
    const-string v0, "MountService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "removeVolumeLocked() "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1319
    iget-object v0, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1d
    .line 1320
    iget-object v0, p0, Lcom/android/server/MountService;->mVolumesByPath:Ljava/util/HashMap;

    #@1f
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    .line 1321
    iget-object v0, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@28
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    .line 1322
    return-void
.end method

.method private sendStorageIntent(Ljava/lang/String;Landroid/os/storage/StorageVolume;Landroid/os/UserHandle;)V
    .registers 8
    .parameter "action"
    .parameter "volume"
    .parameter "user"

    #@0
    .prologue
    .line 1166
    new-instance v0, Landroid/content/Intent;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "file://"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@20
    .line 1167
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "storage_volume"

    #@22
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@25
    .line 1168
    const-string v1, "MountService"

    #@27
    new-instance v2, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v3, "sendStorageIntent "

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    const-string v3, " to "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1169
    iget-object v1, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@49
    invoke-virtual {v1, v0, p3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@4c
    .line 1170
    return-void
.end method

.method private sendUmsIntent(Z)V
    .registers 5
    .parameter "c"

    #@0
    .prologue
    .line 1173
    iget-object v1, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@2
    new-instance v2, Landroid/content/Intent;

    #@4
    if-eqz p1, :cond_11

    #@6
    const-string v0, "android.intent.action.UMS_CONNECTED"

    #@8
    :goto_8
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    sget-object v0, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@d
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@10
    .line 1176
    return-void

    #@11
    .line 1173
    :cond_11
    const-string v0, "android.intent.action.UMS_DISCONNECTED"

    #@13
    goto :goto_8
.end method

.method private setUmsEnabling(Z)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 1485
    iget-object v1, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 1486
    :try_start_3
    iput-boolean p1, p0, Lcom/android/server/MountService;->mUmsEnabling:Z

    #@5
    .line 1487
    monitor-exit v1

    #@6
    .line 1488
    return-void

    #@7
    .line 1487
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V
    .registers 15
    .parameter "volume"
    .parameter "state"

    #@0
    .prologue
    const/4 v11, 0x5

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 632
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@6
    move-result-object v4

    #@7
    .line 634
    .local v4, path:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@9
    monitor-enter v7

    #@a
    .line 635
    :try_start_a
    iget-object v6, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@c
    invoke-virtual {v6, v4, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Ljava/lang/String;

    #@12
    .line 636
    .local v3, oldState:Ljava/lang/String;
    monitor-exit v7
    :try_end_13
    .catchall {:try_start_a .. :try_end_13} :catchall_2f

    #@13
    .line 638
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_32

    #@19
    .line 639
    const-string v6, "MountService"

    #@1b
    const-string v7, "Duplicate state transition (%s -> %s) for %s"

    #@1d
    const/4 v8, 0x3

    #@1e
    new-array v8, v8, [Ljava/lang/Object;

    #@20
    aput-object p2, v8, v9

    #@22
    aput-object p2, v8, v10

    #@24
    const/4 v9, 0x2

    #@25
    aput-object v4, v8, v9

    #@27
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2a
    move-result-object v7

    #@2b
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 695
    :goto_2e
    return-void

    #@2f
    .line 636
    .end local v3           #oldState:Ljava/lang/String;
    :catchall_2f
    move-exception v6

    #@30
    :try_start_30
    monitor-exit v7
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    throw v6

    #@32
    .line 644
    .restart local v3       #oldState:Ljava/lang/String;
    :cond_32
    const-string v6, "MountService"

    #@34
    new-instance v7, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v8, "volume state changed for "

    #@3b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    const-string v8, " ("

    #@45
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    const-string v8, " -> "

    #@4f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    const-string v8, ")"

    #@59
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 648
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->isPrimary()Z

    #@67
    move-result v6

    #@68
    if-eqz v6, :cond_b3

    #@6a
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    #@6d
    move-result v6

    #@6e
    if-nez v6, :cond_b3

    #@70
    .line 649
    const-string v6, "unmounted"

    #@72
    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v6

    #@76
    if-eqz v6, :cond_a5

    #@78
    .line 650
    iget-object v6, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@7a
    invoke-virtual {v6, v9, v9}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    #@7d
    .line 657
    iget-object v6, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@7f
    iget-object v7, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@81
    invoke-virtual {v7, v11, v4}, Lcom/android/server/MountService$ObbActionHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@84
    move-result-object v7

    #@85
    invoke-virtual {v6, v7}, Lcom/android/server/MountService$ObbActionHandler;->sendMessage(Landroid/os/Message;)Z

    #@88
    .line 682
    :cond_88
    :goto_88
    iget-object v7, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@8a
    monitor-enter v7

    #@8b
    .line 683
    :try_start_8b
    iget-object v6, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@8d
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@90
    move-result v6

    #@91
    add-int/lit8 v2, v6, -0x1

    #@93
    .local v2, i:I
    :goto_93
    if-ltz v2, :cond_104

    #@95
    .line 684
    iget-object v6, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@97
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9a
    move-result-object v0

    #@9b
    check-cast v0, Lcom/android/server/MountService$MountServiceBinderListener;
    :try_end_9d
    .catchall {:try_start_8b .. :try_end_9d} :catchall_f8

    #@9d
    .line 686
    .local v0, bl:Lcom/android/server/MountService$MountServiceBinderListener;
    :try_start_9d
    iget-object v6, v0, Lcom/android/server/MountService$MountServiceBinderListener;->mListener:Landroid/os/storage/IMountServiceListener;

    #@9f
    invoke-interface {v6, v4, v3, p2}, Landroid/os/storage/IMountServiceListener;->onStorageStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a2
    .catchall {:try_start_9d .. :try_end_a2} :catchall_f8
    .catch Landroid/os/RemoteException; {:try_start_9d .. :try_end_a2} :catch_ea
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_a2} :catch_fb

    #@a2
    .line 683
    :goto_a2
    add-int/lit8 v2, v2, -0x1

    #@a4
    goto :goto_93

    #@a5
    .line 659
    .end local v0           #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .end local v2           #i:I
    :cond_a5
    const-string v6, "mounted"

    #@a7
    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v6

    #@ab
    if-eqz v6, :cond_88

    #@ad
    .line 660
    iget-object v6, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@af
    invoke-virtual {v6, v10, v9}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    #@b2
    goto :goto_88

    #@b3
    .line 664
    :cond_b3
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_MOVE_SDCARD:Z

    #@b5
    if-eqz v6, :cond_88

    #@b7
    const-string v6, "EXTERNAL_ADD_STORAGE"

    #@b9
    invoke-static {v6}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@bc
    move-result-object v6

    #@bd
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v6

    #@c1
    if-eqz v6, :cond_88

    #@c3
    .line 667
    const-string v6, "unmounted"

    #@c5
    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c8
    move-result v6

    #@c9
    if-eqz v6, :cond_dc

    #@cb
    .line 668
    iget-object v6, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@cd
    invoke-virtual {v6, v9, v9}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    #@d0
    .line 675
    iget-object v6, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@d2
    iget-object v7, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@d4
    invoke-virtual {v7, v11, v4}, Lcom/android/server/MountService$ObbActionHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d7
    move-result-object v7

    #@d8
    invoke-virtual {v6, v7}, Lcom/android/server/MountService$ObbActionHandler;->sendMessage(Landroid/os/Message;)Z

    #@db
    goto :goto_88

    #@dc
    .line 677
    :cond_dc
    const-string v6, "mounted"

    #@de
    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e1
    move-result v6

    #@e2
    if-eqz v6, :cond_88

    #@e4
    .line 678
    iget-object v6, p0, Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;

    #@e6
    invoke-virtual {v6, v10, v9}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    #@e9
    goto :goto_88

    #@ea
    .line 687
    .restart local v0       #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .restart local v2       #i:I
    :catch_ea
    move-exception v5

    #@eb
    .line 688
    .local v5, rex:Landroid/os/RemoteException;
    :try_start_eb
    const-string v6, "MountService"

    #@ed
    const-string v8, "Listener dead"

    #@ef
    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f2
    .line 689
    iget-object v6, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@f4
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@f7
    goto :goto_a2

    #@f8
    .line 694
    .end local v0           #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .end local v2           #i:I
    .end local v5           #rex:Landroid/os/RemoteException;
    :catchall_f8
    move-exception v6

    #@f9
    monitor-exit v7
    :try_end_fa
    .catchall {:try_start_eb .. :try_end_fa} :catchall_f8

    #@fa
    throw v6

    #@fb
    .line 690
    .restart local v0       #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .restart local v2       #i:I
    :catch_fb
    move-exception v1

    #@fc
    .line 691
    .local v1, ex:Ljava/lang/Exception;
    :try_start_fc
    const-string v6, "MountService"

    #@fe
    const-string v8, "Listener failed"

    #@100
    invoke-static {v6, v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@103
    goto :goto_a2

    #@104
    .line 694
    .end local v0           #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .end local v1           #ex:Ljava/lang/Exception;
    :cond_104
    monitor-exit v7
    :try_end_105
    .catchall {:try_start_fc .. :try_end_105} :catchall_f8

    #@105
    goto/16 :goto_2e
.end method

.method private validatePermission(Ljava/lang/String;)V
    .registers 6
    .parameter "perm"

    #@0
    .prologue
    .line 1179
    iget-object v0, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 1180
    new-instance v0, Ljava/lang/SecurityException;

    #@a
    const-string v1, "Requires %s permission"

    #@c
    const/4 v2, 0x1

    #@d
    new-array v2, v2, [Ljava/lang/Object;

    #@f
    const/4 v3, 0x0

    #@10
    aput-object p1, v2, v3

    #@12
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 1182
    :cond_1a
    return-void
.end method

.method private waitForLatch(Ljava/util/concurrent/CountDownLatch;)V
    .registers 6
    .parameter "latch"

    #@0
    .prologue
    .line 504
    :goto_0
    const-wide/16 v1, 0x1388

    #@2
    :try_start_2
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@4
    invoke-virtual {p1, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_b

    #@a
    .line 505
    return-void

    #@b
    .line 507
    :cond_b
    const-string v1, "MountService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Thread "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    const-string v3, " still waiting for MountService ready..."

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_31
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_31} :catch_32

    #@31
    goto :goto_0

    #@32
    .line 510
    :catch_32
    move-exception v0

    #@33
    .line 511
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "MountService"

    #@35
    const-string v2, "Interrupt while waiting for MountService to be ready."

    #@37
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_0
.end method

.method private waitForReady()V
    .registers 2

    #@0
    .prologue
    .line 498
    iget-object v0, p0, Lcom/android/server/MountService;->mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/MountService;->waitForLatch(Ljava/util/concurrent/CountDownLatch;)V

    #@5
    .line 499
    return-void
.end method

.method private warnOnNotMounted()V
    .registers 5

    #@0
    .prologue
    .line 1639
    invoke-direct {p0}, Lcom/android/server/MountService;->getPrimaryPhysicalVolume()Landroid/os/storage/StorageVolume;

    #@3
    move-result-object v1

    #@4
    .line 1640
    .local v1, primary:Landroid/os/storage/StorageVolume;
    if-eqz v1, :cond_1e

    #@6
    .line 1641
    const/4 v0, 0x0

    #@7
    .line 1643
    .local v0, mounted:Z
    :try_start_7
    const-string v2, "mounted"

    #@9
    invoke-virtual {v1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {p0, v3}, Lcom/android/server/MountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_14
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_14} :catch_1f

    #@14
    move-result v0

    #@15
    .line 1647
    :goto_15
    if-nez v0, :cond_1e

    #@17
    .line 1648
    const-string v2, "MountService"

    #@19
    const-string v3, "getSecureContainerList() called when storage not mounted"

    #@1b
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1651
    .end local v0           #mounted:Z
    :cond_1e
    return-void

    #@1f
    .line 1644
    .restart local v0       #mounted:Z
    :catch_1f
    move-exception v2

    #@20
    goto :goto_15
.end method


# virtual methods
.method public changeEncryptionPassword(Ljava/lang/String;)I
    .registers 9
    .parameter "password"

    #@0
    .prologue
    .line 2109
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_e

    #@6
    .line 2110
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v3, "password cannot be empty"

    #@a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 2113
    :cond_e
    iget-object v2, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@10
    const-string v3, "android.permission.CRYPT_KEEPER"

    #@12
    const-string v4, "no permission to access the crypt keeper"

    #@14
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 2116
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@1a
    .line 2124
    :try_start_1a
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v3, "cryptfs"

    #@1e
    const/4 v4, 0x2

    #@1f
    new-array v4, v4, [Ljava/lang/Object;

    #@21
    const/4 v5, 0x0

    #@22
    const-string v6, "changepw"

    #@24
    aput-object v6, v4, v5

    #@26
    const/4 v5, 0x1

    #@27
    aput-object p1, v4, v5

    #@29
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@2c
    move-result-object v1

    #@2d
    .line 2125
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_34
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_34} :catch_36

    #@34
    move-result v2

    #@35
    .line 2128
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :goto_35
    return v2

    #@36
    .line 2126
    :catch_36
    move-exception v0

    #@37
    .line 2128
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@3a
    move-result v2

    #@3b
    goto :goto_35
.end method

.method public createSecureContainer(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)I
    .registers 14
    .parameter "id"
    .parameter "sizeMb"
    .parameter "fstype"
    .parameter "key"
    .parameter "ownerUid"
    .parameter "external"

    #@0
    .prologue
    .line 1672
    const-string v2, "android.permission.ASEC_CREATE"

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1673
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1674
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@b
    .line 1676
    const/4 v1, 0x0

    #@c
    .line 1678
    .local v1, rc:I
    :try_start_c
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e
    const-string v4, "asec"

    #@10
    const/4 v2, 0x7

    #@11
    new-array v5, v2, [Ljava/lang/Object;

    #@13
    const/4 v2, 0x0

    #@14
    const-string v6, "create"

    #@16
    aput-object v6, v5, v2

    #@18
    const/4 v2, 0x1

    #@19
    aput-object p1, v5, v2

    #@1b
    const/4 v2, 0x2

    #@1c
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v6

    #@20
    aput-object v6, v5, v2

    #@22
    const/4 v2, 0x3

    #@23
    aput-object p3, v5, v2

    #@25
    const/4 v2, 0x4

    #@26
    aput-object p4, v5, v2

    #@28
    const/4 v2, 0x5

    #@29
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v6

    #@2d
    aput-object v6, v5, v2

    #@2f
    const/4 v6, 0x6

    #@30
    if-eqz p6, :cond_45

    #@32
    const-string v2, "1"

    #@34
    :goto_34
    aput-object v2, v5, v6

    #@36
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_39
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_c .. :try_end_39} :catch_48

    #@39
    .line 1684
    :goto_39
    if-nez v1, :cond_44

    #@3b
    .line 1685
    iget-object v3, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@3d
    monitor-enter v3

    #@3e
    .line 1686
    :try_start_3e
    iget-object v2, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@40
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@43
    .line 1687
    monitor-exit v3
    :try_end_44
    .catchall {:try_start_3e .. :try_end_44} :catchall_4b

    #@44
    .line 1689
    :cond_44
    return v1

    #@45
    .line 1678
    :cond_45
    :try_start_45
    const-string v2, "0"
    :try_end_47
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_45 .. :try_end_47} :catch_48

    #@47
    goto :goto_34

    #@48
    .line 1680
    :catch_48
    move-exception v0

    #@49
    .line 1681
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const/4 v1, -0x1

    #@4a
    goto :goto_39

    #@4b
    .line 1687
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_4b
    move-exception v2

    #@4c
    :try_start_4c
    monitor-exit v3
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    #@4d
    throw v2
.end method

.method public decryptStorage(Ljava/lang/String;)I
    .registers 10
    .parameter "password"

    #@0
    .prologue
    .line 2045
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v3

    #@4
    if-eqz v3, :cond_e

    #@6
    .line 2046
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v4, "password cannot be empty"

    #@a
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v3

    #@e
    .line 2049
    :cond_e
    iget-object v3, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@10
    const-string v4, "android.permission.CRYPT_KEEPER"

    #@12
    const-string v5, "no permission to access the crypt keeper"

    #@14
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 2052
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@1a
    .line 2060
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v4, "cryptfs"

    #@1e
    const/4 v5, 0x2

    #@1f
    new-array v5, v5, [Ljava/lang/Object;

    #@21
    const/4 v6, 0x0

    #@22
    const-string v7, "checkpw"

    #@24
    aput-object v7, v5, v6

    #@26
    const/4 v6, 0x1

    #@27
    aput-object p1, v5, v6

    #@29
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@2c
    move-result-object v2

    #@2d
    .line 2062
    .local v2, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@34
    move-result v0

    #@35
    .line 2063
    .local v0, code:I
    if-nez v0, :cond_43

    #@37
    .line 2066
    iget-object v3, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@39
    new-instance v4, Lcom/android/server/MountService$6;

    #@3b
    invoke-direct {v4, p0}, Lcom/android/server/MountService$6;-><init>(Lcom/android/server/MountService;)V

    #@3e
    const-wide/16 v5, 0x3e8

    #@40
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_43
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_43} :catch_44

    #@43
    .line 2080
    .end local v0           #code:I
    .end local v2           #event:Lcom/android/server/NativeDaemonEvent;
    :cond_43
    :goto_43
    return v0

    #@44
    .line 2078
    :catch_44
    move-exception v1

    #@45
    .line 2080
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@48
    move-result v0

    #@49
    goto :goto_43
.end method

.method public destroySecureContainer(Ljava/lang/String;Z)I
    .registers 11
    .parameter "id"
    .parameter "force"

    #@0
    .prologue
    .line 1727
    const-string v4, "android.permission.ASEC_DESTROY"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1728
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1729
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@b
    .line 1737
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4}, Ljava/lang/Runtime;->gc()V

    #@12
    .line 1739
    const/4 v3, 0x0

    #@13
    .line 1741
    .local v3, rc:I
    :try_start_13
    new-instance v0, Lcom/android/server/NativeDaemonConnector$Command;

    #@15
    const-string v4, "asec"

    #@17
    const/4 v5, 0x2

    #@18
    new-array v5, v5, [Ljava/lang/Object;

    #@1a
    const/4 v6, 0x0

    #@1b
    const-string v7, "destroy"

    #@1d
    aput-object v7, v5, v6

    #@1f
    const/4 v6, 0x1

    #@20
    aput-object p1, v5, v6

    #@22
    invoke-direct {v0, v4, v5}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@25
    .line 1742
    .local v0, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    if-eqz p2, :cond_2c

    #@27
    .line 1743
    const-string v4, "force"

    #@29
    invoke-virtual {v0, v4}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@2c
    .line 1745
    :cond_2c
    iget-object v4, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2e
    invoke-virtual {v4, v0}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_31
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_13 .. :try_end_31} :catch_45

    #@31
    .line 1755
    .end local v0           #cmd:Lcom/android/server/NativeDaemonConnector$Command;
    :goto_31
    if-nez v3, :cond_44

    #@33
    .line 1756
    iget-object v5, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@35
    monitor-enter v5

    #@36
    .line 1757
    :try_start_36
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@38
    invoke-virtual {v4, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@3b
    move-result v4

    #@3c
    if-eqz v4, :cond_43

    #@3e
    .line 1758
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@40
    invoke-virtual {v4, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@43
    .line 1760
    :cond_43
    monitor-exit v5
    :try_end_44
    .catchall {:try_start_36 .. :try_end_44} :catchall_52

    #@44
    .line 1763
    :cond_44
    return v3

    #@45
    .line 1746
    :catch_45
    move-exception v2

    #@46
    .line 1747
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@49
    move-result v1

    #@4a
    .line 1748
    .local v1, code:I
    const/16 v4, 0x195

    #@4c
    if-ne v1, v4, :cond_50

    #@4e
    .line 1749
    const/4 v3, -0x7

    #@4f
    goto :goto_31

    #@50
    .line 1751
    :cond_50
    const/4 v3, -0x1

    #@51
    goto :goto_31

    #@52
    .line 1760
    .end local v1           #code:I
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_52
    move-exception v4

    #@53
    :try_start_53
    monitor-exit v5
    :try_end_54
    .catchall {:try_start_53 .. :try_end_54} :catchall_52

    #@54
    throw v4
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 16
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2694
    iget-object v10, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@2
    const-string v11, "android.permission.DUMP"

    #@4
    invoke-virtual {v10, v11}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v10

    #@8
    if-eqz v10, :cond_3f

    #@a
    .line 2695
    new-instance v10, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v11, "Permission Denial: can\'t dump ActivityManager from from pid="

    #@11
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v10

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v11

    #@19
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v10

    #@1d
    const-string v11, ", uid="

    #@1f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v10

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v11

    #@27
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v10

    #@2b
    const-string v11, " without permission "

    #@2d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v10

    #@31
    const-string v11, "android.permission.DUMP"

    #@33
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v10

    #@37
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e
    .line 2740
    :goto_3e
    return-void

    #@3f
    .line 2701
    :cond_3f
    iget-object v11, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@41
    monitor-enter v11

    #@42
    .line 2702
    :try_start_42
    const-string v10, "  mObbMounts:"

    #@44
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@47
    .line 2704
    iget-object v10, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@49
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@4c
    move-result-object v10

    #@4d
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@50
    move-result-object v1

    #@51
    .line 2705
    .local v1, binders:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/os/IBinder;Ljava/util/List<Lcom/android/server/MountService$ObbState;>;>;>;"
    :cond_51
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@54
    move-result v10

    #@55
    if-eqz v10, :cond_95

    #@57
    .line 2706
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5a
    move-result-object v2

    #@5b
    check-cast v2, Ljava/util/Map$Entry;

    #@5d
    .line 2707
    .local v2, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/os/IBinder;Ljava/util/List<Lcom/android/server/MountService$ObbState;>;>;"
    const-string v10, "    Key="

    #@5f
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@65
    move-result-object v10

    #@66
    check-cast v10, Landroid/os/IBinder;

    #@68
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6b
    move-result-object v10

    #@6c
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6f
    .line 2708
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@72
    move-result-object v8

    #@73
    check-cast v8, Ljava/util/List;

    #@75
    .line 2709
    .local v8, obbStates:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@78
    move-result-object v5

    #@79
    .local v5, i$:Ljava/util/Iterator;
    :goto_79
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@7c
    move-result v10

    #@7d
    if-eqz v10, :cond_51

    #@7f
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@82
    move-result-object v7

    #@83
    check-cast v7, Lcom/android/server/MountService$ObbState;

    #@85
    .line 2710
    .local v7, obbState:Lcom/android/server/MountService$ObbState;
    const-string v10, "      "

    #@87
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8a
    invoke-virtual {v7}, Lcom/android/server/MountService$ObbState;->toString()Ljava/lang/String;

    #@8d
    move-result-object v10

    #@8e
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@91
    goto :goto_79

    #@92
    .line 2722
    .end local v1           #binders:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/os/IBinder;Ljava/util/List<Lcom/android/server/MountService$ObbState;>;>;>;"
    .end local v2           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/os/IBinder;Ljava/util/List<Lcom/android/server/MountService$ObbState;>;>;"
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v7           #obbState:Lcom/android/server/MountService$ObbState;
    .end local v8           #obbStates:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    :catchall_92
    move-exception v10

    #@93
    monitor-exit v11
    :try_end_94
    .catchall {:try_start_42 .. :try_end_94} :catchall_92

    #@94
    throw v10

    #@95
    .line 2714
    .restart local v1       #binders:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/os/IBinder;Ljava/util/List<Lcom/android/server/MountService$ObbState;>;>;>;"
    :cond_95
    :try_start_95
    const-string v10, ""

    #@97
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9a
    .line 2715
    const-string v10, "  mObbPathToStateMap:"

    #@9c
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9f
    .line 2716
    iget-object v10, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@a1
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@a4
    move-result-object v10

    #@a5
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a8
    move-result-object v6

    #@a9
    .line 2717
    .local v6, maps:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/MountService$ObbState;>;>;"
    :goto_a9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@ac
    move-result v10

    #@ad
    if-eqz v10, :cond_d6

    #@af
    .line 2718
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b2
    move-result-object v3

    #@b3
    check-cast v3, Ljava/util/Map$Entry;

    #@b5
    .line 2719
    .local v3, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/MountService$ObbState;>;"
    const-string v10, "    "

    #@b7
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ba
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@bd
    move-result-object v10

    #@be
    check-cast v10, Ljava/lang/String;

    #@c0
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c3
    .line 2720
    const-string v10, " -> "

    #@c5
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c8
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@cb
    move-result-object v10

    #@cc
    check-cast v10, Lcom/android/server/MountService$ObbState;

    #@ce
    invoke-virtual {v10}, Lcom/android/server/MountService$ObbState;->toString()Ljava/lang/String;

    #@d1
    move-result-object v10

    #@d2
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d5
    goto :goto_a9

    #@d6
    .line 2722
    .end local v3           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/MountService$ObbState;>;"
    :cond_d6
    monitor-exit v11
    :try_end_d7
    .catchall {:try_start_95 .. :try_end_d7} :catchall_92

    #@d7
    .line 2724
    const-string v10, ""

    #@d9
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dc
    .line 2726
    iget-object v11, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@de
    monitor-enter v11

    #@df
    .line 2727
    :try_start_df
    const-string v10, "  mVolumes:"

    #@e1
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e4
    .line 2729
    iget-object v10, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@e6
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@e9
    move-result v0

    #@ea
    .line 2730
    .local v0, N:I
    const/4 v4, 0x0

    #@eb
    .local v4, i:I
    :goto_eb
    if-ge v4, v0, :cond_104

    #@ed
    .line 2731
    iget-object v10, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@ef
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f2
    move-result-object v9

    #@f3
    check-cast v9, Landroid/os/storage/StorageVolume;

    #@f5
    .line 2732
    .local v9, v:Landroid/os/storage/StorageVolume;
    const-string v10, "    "

    #@f7
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fa
    .line 2733
    invoke-virtual {v9}, Landroid/os/storage/StorageVolume;->toString()Ljava/lang/String;

    #@fd
    move-result-object v10

    #@fe
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@101
    .line 2730
    add-int/lit8 v4, v4, 0x1

    #@103
    goto :goto_eb

    #@104
    .line 2735
    .end local v9           #v:Landroid/os/storage/StorageVolume;
    :cond_104
    monitor-exit v11
    :try_end_105
    .catchall {:try_start_df .. :try_end_105} :catchall_114

    #@105
    .line 2737
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@108
    .line 2738
    const-string v10, "  mConnection:"

    #@10a
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@10d
    .line 2739
    iget-object v10, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@10f
    invoke-virtual {v10, p1, p2, p3}, Lcom/android/server/NativeDaemonConnector;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@112
    goto/16 :goto_3e

    #@114
    .line 2735
    .end local v0           #N:I
    .end local v4           #i:I
    :catchall_114
    move-exception v10

    #@115
    :try_start_115
    monitor-exit v11
    :try_end_116
    .catchall {:try_start_115 .. :try_end_116} :catchall_114

    #@116
    throw v10
.end method

.method public encryptStorage(Ljava/lang/String;)I
    .registers 9
    .parameter "password"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2085
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_f

    #@7
    .line 2086
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v2, "password cannot be empty"

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 2089
    :cond_f
    iget-object v2, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@11
    const-string v3, "android.permission.CRYPT_KEEPER"

    #@13
    const-string v4, "no permission to access the crypt keeper"

    #@15
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 2092
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@1b
    .line 2099
    :try_start_1b
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1d
    const-string v3, "cryptfs"

    #@1f
    const/4 v4, 0x3

    #@20
    new-array v4, v4, [Ljava/lang/Object;

    #@22
    const/4 v5, 0x0

    #@23
    const-string v6, "enablecrypto"

    #@25
    aput-object v6, v4, v5

    #@27
    const/4 v5, 0x1

    #@28
    const-string v6, "inplace"

    #@2a
    aput-object v6, v4, v5

    #@2c
    const/4 v5, 0x2

    #@2d
    aput-object p1, v4, v5

    #@2f
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_32
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1b .. :try_end_32} :catch_33

    #@32
    .line 2105
    :goto_32
    return v1

    #@33
    .line 2100
    :catch_33
    move-exception v0

    #@34
    .line 2102
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@37
    move-result v1

    #@38
    goto :goto_32
.end method

.method public finalizeSecureContainer(Ljava/lang/String;)I
    .registers 9
    .parameter "id"

    #@0
    .prologue
    .line 1693
    const-string v2, "android.permission.ASEC_CREATE"

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1694
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@8
    .line 1696
    const/4 v1, 0x0

    #@9
    .line 1698
    .local v1, rc:I
    :try_start_9
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v3, "asec"

    #@d
    const/4 v4, 0x2

    #@e
    new-array v4, v4, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    const-string v6, "finalize"

    #@13
    aput-object v6, v4, v5

    #@15
    const/4 v5, 0x1

    #@16
    aput-object p1, v4, v5

    #@18
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1b} :catch_1c

    #@1b
    .line 1706
    :goto_1b
    return v1

    #@1c
    .line 1703
    :catch_1c
    move-exception v0

    #@1d
    .line 1704
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const/4 v1, -0x1

    #@1e
    goto :goto_1b
.end method

.method public finishMediaUpdate()V
    .registers 3

    #@0
    .prologue
    .line 1921
    iget-object v0, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 1922
    return-void
.end method

.method public fixPermissionsSecureContainer(Ljava/lang/String;ILjava/lang/String;)I
    .registers 11
    .parameter "id"
    .parameter "gid"
    .parameter "filename"

    #@0
    .prologue
    .line 1710
    const-string v2, "android.permission.ASEC_CREATE"

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1711
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@8
    .line 1713
    const/4 v1, 0x0

    #@9
    .line 1715
    .local v1, rc:I
    :try_start_9
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v3, "asec"

    #@d
    const/4 v4, 0x4

    #@e
    new-array v4, v4, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    const-string v6, "fixperms"

    #@13
    aput-object v6, v4, v5

    #@15
    const/4 v5, 0x1

    #@16
    aput-object p1, v4, v5

    #@18
    const/4 v5, 0x2

    #@19
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v6

    #@1d
    aput-object v6, v4, v5

    #@1f
    const/4 v5, 0x3

    #@20
    aput-object p3, v4, v5

    #@22
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_25
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    .line 1723
    :goto_25
    return v1

    #@26
    .line 1720
    :catch_26
    move-exception v0

    #@27
    .line 1721
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const/4 v1, -0x1

    #@28
    goto :goto_25
.end method

.method public formatVolume(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 1606
    const-string v0, "android.permission.MOUNT_FORMAT_FILESYSTEMS"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1607
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1609
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->doFormatVolume(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getBackupSecureContainerList()[Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 2751
    iget-object v5, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@2
    monitor-enter v5

    #@3
    .line 2752
    :try_start_3
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@5
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    #@8
    move-result v4

    #@9
    if-lez v4, :cond_2d

    #@b
    .line 2753
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@d
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    #@10
    move-result v4

    #@11
    new-array v3, v4, [Ljava/lang/String;

    #@13
    .line 2754
    .local v3, list:[Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@15
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v2

    #@19
    .line 2755
    .local v2, it:Ljava/util/Iterator;
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    move v1, v0

    #@1b
    .line 2756
    .end local v0           #i:I
    .local v1, i:I
    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v4

    #@1f
    if-eqz v4, :cond_30

    #@21
    .line 2758
    add-int/lit8 v0, v1, 0x1

    #@23
    .end local v1           #i:I
    .restart local v0       #i:I
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v4

    #@27
    check-cast v4, Ljava/lang/String;

    #@29
    aput-object v4, v3, v1

    #@2b
    move v1, v0

    #@2c
    .end local v0           #i:I
    .restart local v1       #i:I
    goto :goto_1b

    #@2d
    .line 2762
    .end local v1           #i:I
    .end local v2           #it:Ljava/util/Iterator;
    .end local v3           #list:[Ljava/lang/String;
    :cond_2d
    const/4 v3, 0x0

    #@2e
    monitor-exit v5

    #@2f
    .line 2765
    :goto_2f
    return-object v3

    #@30
    .line 2764
    .restart local v1       #i:I
    .restart local v2       #it:Ljava/util/Iterator;
    .restart local v3       #list:[Ljava/lang/String;
    :cond_30
    monitor-exit v5

    #@31
    goto :goto_2f

    #@32
    .end local v1           #i:I
    .end local v2           #it:Ljava/util/Iterator;
    .end local v3           #list:[Ljava/lang/String;
    :catchall_32
    move-exception v4

    #@33
    monitor-exit v5
    :try_end_34
    .catchall {:try_start_3 .. :try_end_34} :catchall_32

    #@34
    throw v4
.end method

.method public getEncryptionState()I
    .registers 9

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 2023
    iget-object v3, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@3
    const-string v4, "android.permission.CRYPT_KEEPER"

    #@5
    const-string v5, "no permission to access the crypt keeper"

    #@7
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 2026
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@d
    .line 2030
    :try_start_d
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@f
    const-string v4, "cryptfs"

    #@11
    const/4 v5, 0x1

    #@12
    new-array v5, v5, [Ljava/lang/Object;

    #@14
    const/4 v6, 0x0

    #@15
    const-string v7, "cryptocomplete"

    #@17
    aput-object v7, v5, v6

    #@19
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@1c
    move-result-object v1

    #@1d
    .line 2031
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_24
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_24} :catch_26
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_d .. :try_end_24} :catch_2f

    #@24
    move-result v2

    #@25
    .line 2039
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :goto_25
    return v2

    #@26
    .line 2032
    :catch_26
    move-exception v0

    #@27
    .line 2034
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v3, "MountService"

    #@29
    const-string v4, "Unable to parse result from cryptfs cryptocomplete"

    #@2b
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    goto :goto_25

    #@2f
    .line 2036
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catch_2f
    move-exception v0

    #@30
    .line 2038
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v3, "MountService"

    #@32
    const-string v4, "Error in communicating with cryptfs in validating"

    #@34
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_25
.end method

.method public getMountedObbPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 14
    .parameter "rawPath"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    .line 1944
    const-string v5, "rawPath cannot be null"

    #@5
    invoke-static {p1, v5}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 1946
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@b
    .line 1947
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@e
    .line 1950
    iget-object v5, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@10
    monitor-enter v5

    #@11
    .line 1951
    :try_start_11
    iget-object v6, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@13
    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    check-cast v3, Lcom/android/server/MountService$ObbState;

    #@19
    .line 1952
    .local v3, state:Lcom/android/server/MountService$ObbState;
    monitor-exit v5
    :try_end_1a
    .catchall {:try_start_11 .. :try_end_1a} :catchall_35

    #@1a
    .line 1953
    if-nez v3, :cond_38

    #@1c
    .line 1954
    const-string v5, "MountService"

    #@1e
    new-instance v6, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v7, "Failed to find OBB mounted at "

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v6

    #@31
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1966
    :cond_34
    :goto_34
    return-object v4

    #@35
    .line 1952
    .end local v3           #state:Lcom/android/server/MountService$ObbState;
    :catchall_35
    move-exception v4

    #@36
    :try_start_36
    monitor-exit v5
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v4

    #@38
    .line 1960
    .restart local v3       #state:Lcom/android/server/MountService$ObbState;
    :cond_38
    :try_start_38
    iget-object v5, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@3a
    const-string v6, "obb"

    #@3c
    const/4 v7, 0x2

    #@3d
    new-array v7, v7, [Ljava/lang/Object;

    #@3f
    const/4 v8, 0x0

    #@40
    const-string v9, "path"

    #@42
    aput-object v9, v7, v8

    #@44
    const/4 v8, 0x1

    #@45
    iget-object v9, v3, Lcom/android/server/MountService$ObbState;->voldPath:Ljava/lang/String;

    #@47
    aput-object v9, v7, v8

    #@49
    invoke-virtual {v5, v6, v7}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@4c
    move-result-object v2

    #@4d
    .line 1961
    .local v2, event:Lcom/android/server/NativeDaemonEvent;
    const/16 v5, 0xd3

    #@4f
    invoke-virtual {v2, v5}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@52
    .line 1962
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;
    :try_end_55
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_38 .. :try_end_55} :catch_57

    #@55
    move-result-object v4

    #@56
    goto :goto_34

    #@57
    .line 1963
    .end local v2           #event:Lcom/android/server/NativeDaemonEvent;
    :catch_57
    move-exception v1

    #@58
    .line 1964
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@5b
    move-result v0

    #@5c
    .line 1965
    .local v0, code:I
    const/16 v5, 0x196

    #@5e
    if-eq v0, v5, :cond_34

    #@60
    .line 1968
    new-instance v4, Ljava/lang/IllegalStateException;

    #@62
    const-string v5, "Unexpected response code %d"

    #@64
    new-array v6, v11, [Ljava/lang/Object;

    #@66
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v7

    #@6a
    aput-object v7, v6, v10

    #@6c
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@6f
    move-result-object v5

    #@70
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@73
    throw v4
.end method

.method public getSecureContainerFilesystemPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "id"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 1900
    const-string v3, "android.permission.ASEC_ACCESS"

    #@4
    invoke-direct {p0, v3}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@7
    .line 1901
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@a
    .line 1902
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@d
    .line 1906
    :try_start_d
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@f
    const-string v4, "asec"

    #@11
    const/4 v5, 0x2

    #@12
    new-array v5, v5, [Ljava/lang/Object;

    #@14
    const/4 v6, 0x0

    #@15
    const-string v7, "fspath"

    #@17
    aput-object v7, v5, v6

    #@19
    const/4 v6, 0x1

    #@1a
    aput-object p1, v5, v6

    #@1c
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@1f
    move-result-object v2

    #@20
    .line 1907
    .local v2, event:Lcom/android/server/NativeDaemonEvent;
    const/16 v3, 0xd3

    #@22
    invoke-virtual {v2, v3}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@25
    .line 1908
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;
    :try_end_28
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_d .. :try_end_28} :catch_2a

    #@28
    move-result-object v3

    #@29
    .line 1913
    .end local v2           #event:Lcom/android/server/NativeDaemonEvent;
    :goto_29
    return-object v3

    #@2a
    .line 1909
    :catch_2a
    move-exception v1

    #@2b
    .line 1910
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@2e
    move-result v0

    #@2f
    .line 1911
    .local v0, code:I
    const/16 v3, 0x196

    #@31
    if-ne v0, v3, :cond_44

    #@33
    .line 1912
    const-string v3, "MountService"

    #@35
    const-string v4, "Container \'%s\' not found"

    #@37
    new-array v5, v9, [Ljava/lang/Object;

    #@39
    aput-object p1, v5, v8

    #@3b
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1913
    const/4 v3, 0x0

    #@43
    goto :goto_29

    #@44
    .line 1915
    :cond_44
    new-instance v3, Ljava/lang/IllegalStateException;

    #@46
    const-string v4, "Unexpected response code %d"

    #@48
    new-array v5, v9, [Ljava/lang/Object;

    #@4a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4d
    move-result-object v6

    #@4e
    aput-object v6, v5, v8

    #@50
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@57
    throw v3
.end method

.method public getSecureContainerList()[Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1654
    const-string v1, "android.permission.ASEC_ACCESS"

    #@3
    invoke-direct {p0, v1}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@6
    .line 1655
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@9
    .line 1656
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@c
    .line 1660
    :try_start_c
    const-string v1, "vold.decrypt"

    #@e
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    const-string v2, "trigger_restart_min_framework"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    .line 1661
    const/4 v1, 0x0

    #@1b
    .line 1666
    :goto_1b
    return-object v1

    #@1c
    .line 1663
    :cond_1c
    iget-object v1, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1e
    const-string v2, "asec"

    #@20
    const/4 v3, 0x1

    #@21
    new-array v3, v3, [Ljava/lang/Object;

    #@23
    const/4 v4, 0x0

    #@24
    const-string v5, "list"

    #@26
    aput-object v5, v3, v4

    #@28
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@2b
    move-result-object v1

    #@2c
    const/16 v2, 0x6f

    #@2e
    invoke-static {v1, v2}, Lcom/android/server/NativeDaemonEvent;->filterMessageList([Lcom/android/server/NativeDaemonEvent;I)[Ljava/lang/String;
    :try_end_31
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_c .. :try_end_31} :catch_33

    #@31
    move-result-object v1

    #@32
    goto :goto_1b

    #@33
    .line 1665
    :catch_33
    move-exception v0

    #@34
    .line 1666
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-array v1, v6, [Ljava/lang/String;

    #@36
    goto :goto_1b
.end method

.method public getSecureContainerPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "id"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 1879
    const-string v3, "android.permission.ASEC_ACCESS"

    #@4
    invoke-direct {p0, v3}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@7
    .line 1880
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@a
    .line 1881
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@d
    .line 1885
    :try_start_d
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@f
    const-string v4, "asec"

    #@11
    const/4 v5, 0x2

    #@12
    new-array v5, v5, [Ljava/lang/Object;

    #@14
    const/4 v6, 0x0

    #@15
    const-string v7, "path"

    #@17
    aput-object v7, v5, v6

    #@19
    const/4 v6, 0x1

    #@1a
    aput-object p1, v5, v6

    #@1c
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@1f
    move-result-object v2

    #@20
    .line 1886
    .local v2, event:Lcom/android/server/NativeDaemonEvent;
    const/16 v3, 0xd3

    #@22
    invoke-virtual {v2, v3}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@25
    .line 1887
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;
    :try_end_28
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_d .. :try_end_28} :catch_2a

    #@28
    move-result-object v3

    #@29
    .line 1892
    .end local v2           #event:Lcom/android/server/NativeDaemonEvent;
    :goto_29
    return-object v3

    #@2a
    .line 1888
    :catch_2a
    move-exception v1

    #@2b
    .line 1889
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@2e
    move-result v0

    #@2f
    .line 1890
    .local v0, code:I
    const/16 v3, 0x196

    #@31
    if-ne v0, v3, :cond_44

    #@33
    .line 1891
    const-string v3, "MountService"

    #@35
    const-string v4, "Container \'%s\' not found"

    #@37
    new-array v5, v9, [Ljava/lang/Object;

    #@39
    aput-object p1, v5, v8

    #@3b
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1892
    const/4 v3, 0x0

    #@43
    goto :goto_29

    #@44
    .line 1894
    :cond_44
    new-instance v3, Ljava/lang/IllegalStateException;

    #@46
    const-string v4, "Unexpected response code %d"

    #@48
    new-array v5, v9, [Ljava/lang/Object;

    #@4a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4d
    move-result-object v6

    #@4e
    aput-object v6, v5, v8

    #@50
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@57
    throw v3
.end method

.method public getStorageUsers(Ljava/lang/String;)[I
    .registers 14
    .parameter "path"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 1613
    const-string v6, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@3
    invoke-direct {p0, v6}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@6
    .line 1614
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@9
    .line 1616
    :try_start_9
    iget-object v6, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v7, "storage"

    #@d
    const/4 v8, 0x2

    #@e
    new-array v8, v8, [Ljava/lang/Object;

    #@10
    const/4 v9, 0x0

    #@11
    const-string v10, "users"

    #@13
    aput-object v10, v8, v9

    #@15
    const/4 v9, 0x1

    #@16
    aput-object p1, v8, v9

    #@18
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@1b
    move-result-object v6

    #@1c
    const/16 v7, 0x70

    #@1e
    invoke-static {v6, v7}, Lcom/android/server/NativeDaemonEvent;->filterMessageList([Lcom/android/server/NativeDaemonEvent;I)[Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    .line 1621
    .local v4, r:[Ljava/lang/String;
    array-length v6, v4

    #@23
    new-array v0, v6, [I

    #@25
    .line 1622
    .local v0, data:[I
    const/4 v2, 0x0

    #@26
    .local v2, i:I
    :goto_26
    array-length v6, v4

    #@27
    if-ge v2, v6, :cond_55

    #@29
    .line 1623
    aget-object v6, v4, v2

    #@2b
    const-string v7, " "

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_30
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_30} :catch_56

    #@30
    move-result-object v5

    #@31
    .line 1625
    .local v5, tok:[Ljava/lang/String;
    const/4 v6, 0x0

    #@32
    :try_start_32
    aget-object v6, v5, v6

    #@34
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@37
    move-result v6

    #@38
    aput v6, v0, v2
    :try_end_3a
    .catch Ljava/lang/NumberFormatException; {:try_start_32 .. :try_end_3a} :catch_3d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_32 .. :try_end_3a} :catch_56

    #@3a
    .line 1622
    add-int/lit8 v2, v2, 0x1

    #@3c
    goto :goto_26

    #@3d
    .line 1626
    :catch_3d
    move-exception v3

    #@3e
    .line 1627
    .local v3, nfe:Ljava/lang/NumberFormatException;
    :try_start_3e
    const-string v6, "MountService"

    #@40
    const-string v7, "Error parsing pid %s"

    #@42
    const/4 v8, 0x1

    #@43
    new-array v8, v8, [Ljava/lang/Object;

    #@45
    const/4 v9, 0x0

    #@46
    const/4 v10, 0x0

    #@47
    aget-object v10, v5, v10

    #@49
    aput-object v10, v8, v9

    #@4b
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4e
    move-result-object v7

    #@4f
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1628
    const/4 v6, 0x0

    #@53
    new-array v0, v6, [I
    :try_end_55
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_3e .. :try_end_55} :catch_56

    #@55
    .line 1634
    .end local v0           #data:[I
    .end local v2           #i:I
    .end local v3           #nfe:Ljava/lang/NumberFormatException;
    .end local v4           #r:[Ljava/lang/String;
    .end local v5           #tok:[Ljava/lang/String;
    :cond_55
    :goto_55
    return-object v0

    #@56
    .line 1632
    :catch_56
    move-exception v1

    #@57
    .line 1633
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v6, "MountService"

    #@59
    const-string v7, "Failed to retrieve storage users list"

    #@5b
    invoke-static {v6, v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5e
    .line 1634
    new-array v0, v11, [I

    #@60
    goto :goto_55
.end method

.method public getVolumeList()[Landroid/os/storage/StorageVolume;
    .registers 14

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 2168
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@5
    move-result v1

    #@6
    .line 2169
    .local v1, callingUserId:I
    iget-object v9, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@8
    const-string v10, "android.permission.ACCESS_ALL_EXTERNAL_STORAGE"

    #@a
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@d
    move-result v11

    #@e
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@11
    move-result v12

    #@12
    invoke-virtual {v9, v10, v11, v12}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@15
    move-result v9

    #@16
    if-nez v9, :cond_4a

    #@18
    move v0, v7

    #@19
    .line 2173
    .local v0, accessAll:Z
    :goto_19
    iget-object v9, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@1b
    monitor-enter v9

    #@1c
    .line 2174
    :try_start_1c
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@1f
    move-result-object v2

    #@20
    .line 2175
    .local v2, filtered:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/storage/StorageVolume;>;"
    iget-object v10, p0, Lcom/android/server/MountService;->mVolumes:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v3

    #@26
    .local v3, i$:Ljava/util/Iterator;
    :cond_26
    :goto_26
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v10

    #@2a
    if-eqz v10, :cond_4e

    #@2c
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v6

    #@30
    check-cast v6, Landroid/os/storage/StorageVolume;

    #@32
    .line 2176
    .local v6, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getOwner()Landroid/os/UserHandle;

    #@35
    move-result-object v4

    #@36
    .line 2177
    .local v4, owner:Landroid/os/UserHandle;
    if-eqz v4, :cond_3e

    #@38
    invoke-virtual {v4}, Landroid/os/UserHandle;->getIdentifier()I

    #@3b
    move-result v10

    #@3c
    if-ne v10, v1, :cond_4c

    #@3e
    :cond_3e
    move v5, v7

    #@3f
    .line 2178
    .local v5, ownerMatch:Z
    :goto_3f
    if-nez v0, :cond_43

    #@41
    if-eqz v5, :cond_26

    #@43
    .line 2179
    :cond_43
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@46
    goto :goto_26

    #@47
    .line 2183
    .end local v2           #filtered:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/storage/StorageVolume;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #owner:Landroid/os/UserHandle;
    .end local v5           #ownerMatch:Z
    .end local v6           #volume:Landroid/os/storage/StorageVolume;
    :catchall_47
    move-exception v7

    #@48
    monitor-exit v9
    :try_end_49
    .catchall {:try_start_1c .. :try_end_49} :catchall_47

    #@49
    throw v7

    #@4a
    .end local v0           #accessAll:Z
    :cond_4a
    move v0, v8

    #@4b
    .line 2169
    goto :goto_19

    #@4c
    .restart local v0       #accessAll:Z
    .restart local v2       #filtered:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/storage/StorageVolume;>;"
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #owner:Landroid/os/UserHandle;
    .restart local v6       #volume:Landroid/os/storage/StorageVolume;
    :cond_4c
    move v5, v8

    #@4d
    .line 2177
    goto :goto_3f

    #@4e
    .line 2182
    .end local v4           #owner:Landroid/os/UserHandle;
    .end local v6           #volume:Landroid/os/storage/StorageVolume;
    :cond_4e
    :try_start_4e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@51
    move-result v7

    #@52
    new-array v7, v7, [Landroid/os/storage/StorageVolume;

    #@54
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@57
    move-result-object v7

    #@58
    check-cast v7, [Landroid/os/storage/StorageVolume;

    #@5a
    monitor-exit v9
    :try_end_5b
    .catchall {:try_start_4e .. :try_end_5b} :catchall_47

    #@5b
    return-object v7
.end method

.method public getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "mountPoint"

    #@0
    .prologue
    .line 1556
    iget-object v2, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1557
    :try_start_3
    iget-object v1, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/String;

    #@b
    .line 1558
    .local v0, state:Ljava/lang/String;
    if-nez v0, :cond_39

    #@d
    .line 1559
    const-string v1, "MountService"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "getVolumeState("

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, "): Unknown volume"

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1560
    const-string v1, "vold.encrypt_progress"

    #@2d
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_3b

    #@37
    .line 1561
    const-string v0, "removed"

    #@39
    .line 1567
    :cond_39
    monitor-exit v2

    #@3a
    return-object v0

    #@3b
    .line 1563
    :cond_3b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@3d
    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@40
    throw v1

    #@41
    .line 1568
    .end local v0           #state:Ljava/lang/String;
    :catchall_41
    move-exception v1

    #@42
    monitor-exit v2
    :try_end_43
    .catchall {:try_start_3 .. :try_end_43} :catchall_41

    #@43
    throw v1
.end method

.method public isExternalStorageEmulated()Z
    .registers 2

    #@0
    .prologue
    .line 1573
    iget-object v0, p0, Lcom/android/server/MountService;->mEmulatedTemplate:Landroid/os/storage/StorageVolume;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isObbMounted(Ljava/lang/String;)Z
    .registers 4
    .parameter "rawPath"

    #@0
    .prologue
    .line 1975
    const-string v0, "rawPath cannot be null"

    #@2
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 1976
    iget-object v1, p0, Lcom/android/server/MountService;->mObbMounts:Ljava/util/Map;

    #@7
    monitor-enter v1

    #@8
    .line 1977
    :try_start_8
    iget-object v0, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@a
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 1978
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_8 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public isSecureContainerMounted(Ljava/lang/String;)Z
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 1844
    const-string v0, "android.permission.ASEC_ACCESS"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1845
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1846
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@b
    .line 1848
    iget-object v1, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@d
    monitor-enter v1

    #@e
    .line 1849
    :try_start_e
    iget-object v0, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@10
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    monitor-exit v1

    #@15
    return v0

    #@16
    .line 1850
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public isUsbMassStorageConnected()Z
    .registers 3

    #@0
    .prologue
    .line 1491
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@3
    .line 1493
    invoke-direct {p0}, Lcom/android/server/MountService;->getUmsEnabling()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_b

    #@9
    .line 1494
    const/4 v0, 0x1

    #@a
    .line 1497
    :goto_a
    return v0

    #@b
    .line 1496
    :cond_b
    iget-object v1, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@d
    monitor-enter v1

    #@e
    .line 1497
    :try_start_e
    iget-boolean v0, p0, Lcom/android/server/MountService;->mUmsAvailable:Z

    #@10
    monitor-exit v1

    #@11
    goto :goto_a

    #@12
    .line 1498
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_e .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public isUsbMassStorageEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 1542
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@3
    .line 1544
    invoke-direct {p0}, Lcom/android/server/MountService;->getPrimaryPhysicalVolume()Landroid/os/storage/StorageVolume;

    #@6
    move-result-object v0

    #@7
    .line 1545
    .local v0, primary:Landroid/os/storage/StorageVolume;
    if-eqz v0, :cond_14

    #@9
    .line 1546
    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    const-string v2, "ums"

    #@f
    invoke-direct {p0, v1, v2}, Lcom/android/server/MountService;->doGetVolumeShared(Ljava/lang/String;Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    .line 1548
    :goto_13
    return v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method public monitor()V
    .registers 2

    #@0
    .prologue
    .line 2744
    iget-object v0, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2745
    iget-object v0, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@6
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnector;->monitor()V

    #@9
    .line 2747
    :cond_9
    return-void
.end method

.method public mountNetStorage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 15
    .parameter "mountPoint"
    .parameter "type"
    .parameter "ip"
    .parameter "remotepath"
    .parameter "userid"
    .parameter "password"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 2772
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_NFS:Z

    #@4
    if-nez v4, :cond_7

    #@6
    .line 2792
    :goto_6
    return v2

    #@7
    .line 2776
    :cond_7
    const-string v4, "MountService"

    #@9
    new-instance v5, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v6, "mountNetStorage: Mouting "

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    const-string v6, ", "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    const-string v6, ", "

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    const-string v6, ", "

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    const-string v6, ", "

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    const-string v6, ", "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 2777
    const-string v4, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@53
    invoke-direct {p0, v4}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@56
    .line 2779
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@59
    .line 2781
    const/4 v1, 0x0

    #@5a
    .line 2784
    .local v1, rc:I
    :try_start_5a
    iget-object v4, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@5c
    const-string v5, "volume mountns \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\""

    #@5e
    const/4 v6, 0x6

    #@5f
    new-array v6, v6, [Ljava/lang/Object;

    #@61
    const/4 v7, 0x0

    #@62
    aput-object p1, v6, v7

    #@64
    const/4 v7, 0x1

    #@65
    aput-object p2, v6, v7

    #@67
    const/4 v7, 0x2

    #@68
    aput-object p3, v6, v7

    #@6a
    const/4 v7, 0x3

    #@6b
    aput-object p4, v6, v7

    #@6d
    const/4 v7, 0x4

    #@6e
    aput-object p5, v6, v7

    #@70
    const/4 v7, 0x5

    #@71
    aput-object p6, v6, v7

    #@73
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v4, v5}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_7a
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_5a .. :try_end_7a} :catch_7c

    #@7a
    move v2, v3

    #@7b
    .line 2792
    goto :goto_6

    #@7c
    .line 2785
    :catch_7c
    move-exception v0

    #@7d
    .line 2789
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    goto :goto_6
.end method

.method public mountObb(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/storage/IObbActionListener;I)V
    .registers 14
    .parameter "rawPath"
    .parameter "canonicalPath"
    .parameter "key"
    .parameter "token"
    .parameter "nonce"

    #@0
    .prologue
    .line 1984
    const-string v1, "rawPath cannot be null"

    #@2
    invoke-static {p1, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 1985
    const-string v1, "canonicalPath cannot be null"

    #@7
    invoke-static {p2, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 1986
    const-string v1, "token cannot be null"

    #@c
    invoke-static {p4, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 1988
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@12
    move-result v4

    #@13
    .line 1989
    .local v4, callingUid:I
    new-instance v0, Lcom/android/server/MountService$ObbState;

    #@15
    move-object v1, p0

    #@16
    move-object v2, p1

    #@17
    move-object v3, p2

    #@18
    move-object v5, p4

    #@19
    move v6, p5

    #@1a
    invoke-direct/range {v0 .. v6}, Lcom/android/server/MountService$ObbState;-><init>(Lcom/android/server/MountService;Ljava/lang/String;Ljava/lang/String;ILandroid/os/storage/IObbActionListener;I)V

    #@1d
    .line 1990
    .local v0, obbState:Lcom/android/server/MountService$ObbState;
    new-instance v7, Lcom/android/server/MountService$MountObbAction;

    #@1f
    invoke-direct {v7, p0, v0, p3, v4}, Lcom/android/server/MountService$MountObbAction;-><init>(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;Ljava/lang/String;I)V

    #@22
    .line 1991
    .local v7, action:Lcom/android/server/MountService$ObbAction;
    iget-object v1, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@24
    iget-object v2, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@26
    const/4 v3, 0x1

    #@27
    invoke-virtual {v2, v3, v7}, Lcom/android/server/MountService$ObbActionHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v1, v2}, Lcom/android/server/MountService$ObbActionHandler;->sendMessage(Landroid/os/Message;)Z

    #@2e
    .line 1995
    return-void
.end method

.method public mountSecureContainer(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 12
    .parameter "id"
    .parameter "key"
    .parameter "ownerUid"

    #@0
    .prologue
    .line 1767
    const-string v3, "android.permission.ASEC_MOUNT_UNMOUNT"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1768
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1769
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@b
    .line 1771
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@d
    monitor-enter v4

    #@e
    .line 1772
    :try_start_e
    iget-object v3, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@10
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_19

    #@16
    .line 1773
    const/4 v2, -0x6

    #@17
    monitor-exit v4

    #@18
    .line 1792
    :cond_18
    :goto_18
    return v2

    #@19
    .line 1775
    :cond_19
    monitor-exit v4
    :try_end_1a
    .catchall {:try_start_e .. :try_end_1a} :catchall_46

    #@1a
    .line 1777
    const/4 v2, 0x0

    #@1b
    .line 1779
    .local v2, rc:I
    :try_start_1b
    iget-object v3, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1d
    const-string v4, "asec"

    #@1f
    const/4 v5, 0x4

    #@20
    new-array v5, v5, [Ljava/lang/Object;

    #@22
    const/4 v6, 0x0

    #@23
    const-string v7, "mount"

    #@25
    aput-object v7, v5, v6

    #@27
    const/4 v6, 0x1

    #@28
    aput-object p1, v5, v6

    #@2a
    const/4 v6, 0x2

    #@2b
    aput-object p2, v5, v6

    #@2d
    const/4 v6, 0x3

    #@2e
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v7

    #@32
    aput-object v7, v5, v6

    #@34
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_37
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1b .. :try_end_37} :catch_49

    #@37
    .line 1787
    :cond_37
    :goto_37
    if-nez v2, :cond_18

    #@39
    .line 1788
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@3b
    monitor-enter v4

    #@3c
    .line 1789
    :try_start_3c
    iget-object v3, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@3e
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@41
    .line 1790
    monitor-exit v4

    #@42
    goto :goto_18

    #@43
    :catchall_43
    move-exception v3

    #@44
    monitor-exit v4
    :try_end_45
    .catchall {:try_start_3c .. :try_end_45} :catchall_43

    #@45
    throw v3

    #@46
    .line 1775
    .end local v2           #rc:I
    :catchall_46
    move-exception v3

    #@47
    :try_start_47
    monitor-exit v4
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_46

    #@48
    throw v3

    #@49
    .line 1780
    .restart local v2       #rc:I
    :catch_49
    move-exception v1

    #@4a
    .line 1781
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@4d
    move-result v0

    #@4e
    .line 1782
    .local v0, code:I
    const/16 v3, 0x195

    #@50
    if-eq v0, v3, :cond_37

    #@52
    .line 1783
    const/4 v2, -0x1

    #@53
    goto :goto_37
.end method

.method public mountVolume(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 1577
    const-string v0, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1579
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1580
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->doMountVolume(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public onDaemonConnected()V
    .registers 3

    #@0
    .prologue
    .line 705
    new-instance v0, Lcom/android/server/MountService$3;

    #@2
    const-string v1, "MountService#onDaemonConnected"

    #@4
    invoke-direct {v0, p0, v1}, Lcom/android/server/MountService$3;-><init>(Lcom/android/server/MountService;Ljava/lang/String;)V

    #@7
    invoke-virtual {v0}, Lcom/android/server/MountService$3;->start()V

    #@a
    .line 780
    return-void
.end method

.method public onEvent(ILjava/lang/String;[Ljava/lang/String;)Z
    .registers 20
    .parameter "code"
    .parameter "raw"
    .parameter "cooked"

    #@0
    .prologue
    .line 798
    const/16 v11, 0x25d

    #@2
    move/from16 v0, p1

    #@4
    if-ne v0, v11, :cond_22

    #@6
    .line 804
    const/4 v11, 0x2

    #@7
    aget-object v11, p3, v11

    #@9
    const/4 v12, 0x3

    #@a
    aget-object v12, p3, v12

    #@c
    const/4 v13, 0x7

    #@d
    aget-object v13, p3, v13

    #@f
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@12
    move-result v13

    #@13
    const/16 v14, 0xa

    #@15
    aget-object v14, p3, v14

    #@17
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1a
    move-result v14

    #@1b
    move-object/from16 v0, p0

    #@1d
    invoke-direct {v0, v11, v12, v13, v14}, Lcom/android/server/MountService;->notifyVolumeStateChange(Ljava/lang/String;Ljava/lang/String;II)V

    #@20
    .line 884
    :cond_20
    :goto_20
    const/4 v11, 0x1

    #@21
    :goto_21
    return v11

    #@22
    .line 807
    :cond_22
    const/16 v11, 0x276

    #@24
    move/from16 v0, p1

    #@26
    if-eq v0, v11, :cond_34

    #@28
    const/16 v11, 0x277

    #@2a
    move/from16 v0, p1

    #@2c
    if-eq v0, v11, :cond_34

    #@2e
    const/16 v11, 0x278

    #@30
    move/from16 v0, p1

    #@32
    if-ne v0, v11, :cond_102

    #@34
    .line 813
    :cond_34
    const/4 v1, 0x0

    #@35
    .line 814
    .local v1, action:Ljava/lang/String;
    const/4 v11, 0x2

    #@36
    aget-object v5, p3, v11

    #@38
    .line 815
    .local v5, label:Ljava/lang/String;
    const/4 v11, 0x3

    #@39
    aget-object v8, p3, v11

    #@3b
    .line 816
    .local v8, path:Ljava/lang/String;
    const/4 v6, -0x1

    #@3c
    .line 817
    .local v6, major:I
    const/4 v7, -0x1

    #@3d
    .line 820
    .local v7, minor:I
    const/4 v11, 0x6

    #@3e
    :try_start_3e
    aget-object v11, p3, v11

    #@40
    const/4 v12, 0x1

    #@41
    const/4 v13, 0x6

    #@42
    aget-object v13, p3, v13

    #@44
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@47
    move-result v13

    #@48
    add-int/lit8 v13, v13, -0x1

    #@4a
    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    .line 821
    .local v2, devComp:Ljava/lang/String;
    const-string v11, ":"

    #@50
    invoke-virtual {v2, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    .line 822
    .local v3, devTok:[Ljava/lang/String;
    const/4 v11, 0x0

    #@55
    aget-object v11, v3, v11

    #@57
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5a
    move-result v6

    #@5b
    .line 823
    const/4 v11, 0x1

    #@5c
    aget-object v11, v3, v11

    #@5e
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_61
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_61} :catch_96

    #@61
    move-result v7

    #@62
    .line 830
    .end local v2           #devComp:Ljava/lang/String;
    .end local v3           #devTok:[Ljava/lang/String;
    :goto_62
    move-object/from16 v0, p0

    #@64
    iget-object v12, v0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@66
    monitor-enter v12

    #@67
    .line 831
    :try_start_67
    move-object/from16 v0, p0

    #@69
    iget-object v11, v0, Lcom/android/server/MountService;->mVolumesByPath:Ljava/util/HashMap;

    #@6b
    invoke-virtual {v11, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    move-result-object v10

    #@6f
    check-cast v10, Landroid/os/storage/StorageVolume;

    #@71
    .line 832
    .local v10, volume:Landroid/os/storage/StorageVolume;
    move-object/from16 v0, p0

    #@73
    iget-object v11, v0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@75
    invoke-virtual {v11, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@78
    move-result-object v9

    #@79
    check-cast v9, Ljava/lang/String;

    #@7b
    .line 833
    .local v9, state:Ljava/lang/String;
    monitor-exit v12
    :try_end_7c
    .catchall {:try_start_67 .. :try_end_7c} :catchall_9f

    #@7c
    .line 835
    const/16 v11, 0x276

    #@7e
    move/from16 v0, p1

    #@80
    if-ne v0, v11, :cond_a2

    #@82
    .line 836
    new-instance v11, Lcom/android/server/MountService$4;

    #@84
    move-object/from16 v0, p0

    #@86
    invoke-direct {v11, v0, v8}, Lcom/android/server/MountService$4;-><init>(Lcom/android/server/MountService;Ljava/lang/String;)V

    #@89
    invoke-virtual {v11}, Lcom/android/server/MountService$4;->start()V

    #@8c
    .line 877
    :goto_8c
    if-eqz v1, :cond_20

    #@8e
    .line 878
    sget-object v11, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@90
    move-object/from16 v0, p0

    #@92
    invoke-direct {v0, v1, v10, v11}, Lcom/android/server/MountService;->sendStorageIntent(Ljava/lang/String;Landroid/os/storage/StorageVolume;Landroid/os/UserHandle;)V

    #@95
    goto :goto_20

    #@96
    .line 824
    .end local v9           #state:Ljava/lang/String;
    .end local v10           #volume:Landroid/os/storage/StorageVolume;
    :catch_96
    move-exception v4

    #@97
    .line 825
    .local v4, ex:Ljava/lang/Exception;
    const-string v11, "MountService"

    #@99
    const-string v12, "Failed to parse major/minor"

    #@9b
    invoke-static {v11, v12, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9e
    goto :goto_62

    #@9f
    .line 833
    .end local v4           #ex:Ljava/lang/Exception;
    :catchall_9f
    move-exception v11

    #@a0
    :try_start_a0
    monitor-exit v12
    :try_end_a1
    .catchall {:try_start_a0 .. :try_end_a1} :catchall_9f

    #@a1
    throw v11

    #@a2
    .line 849
    .restart local v9       #state:Ljava/lang/String;
    .restart local v10       #volume:Landroid/os/storage/StorageVolume;
    :cond_a2
    const/16 v11, 0x277

    #@a4
    move/from16 v0, p1

    #@a6
    if-ne v0, v11, :cond_d3

    #@a8
    .line 853
    move-object/from16 v0, p0

    #@aa
    invoke-virtual {v0, v8}, Lcom/android/server/MountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@ad
    move-result-object v11

    #@ae
    const-string v12, "bad_removal"

    #@b0
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b3
    move-result v11

    #@b4
    if-eqz v11, :cond_b9

    #@b6
    .line 854
    const/4 v11, 0x1

    #@b7
    goto/16 :goto_21

    #@b9
    .line 858
    :cond_b9
    const-string v11, "unmounted"

    #@bb
    move-object/from16 v0, p0

    #@bd
    invoke-direct {v0, v10, v11}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@c0
    .line 859
    const-string v11, "unmounted"

    #@c2
    sget-object v12, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@c4
    move-object/from16 v0, p0

    #@c6
    invoke-direct {v0, v11, v10, v12}, Lcom/android/server/MountService;->sendStorageIntent(Ljava/lang/String;Landroid/os/storage/StorageVolume;Landroid/os/UserHandle;)V

    #@c9
    .line 862
    const-string v11, "removed"

    #@cb
    move-object/from16 v0, p0

    #@cd
    invoke-direct {v0, v10, v11}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@d0
    .line 863
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    #@d2
    goto :goto_8c

    #@d3
    .line 864
    :cond_d3
    const/16 v11, 0x278

    #@d5
    move/from16 v0, p1

    #@d7
    if-ne v0, v11, :cond_ec

    #@d9
    .line 867
    const-string v11, "unmounted"

    #@db
    move-object/from16 v0, p0

    #@dd
    invoke-direct {v0, v10, v11}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@e0
    .line 868
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    #@e2
    .line 871
    const-string v11, "bad_removal"

    #@e4
    move-object/from16 v0, p0

    #@e6
    invoke-direct {v0, v10, v11}, Lcom/android/server/MountService;->updatePublicVolumeState(Landroid/os/storage/StorageVolume;Ljava/lang/String;)V

    #@e9
    .line 872
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    #@eb
    goto :goto_8c

    #@ec
    .line 874
    :cond_ec
    const-string v11, "MountService"

    #@ee
    const-string v12, "Unknown code {%d}"

    #@f0
    const/4 v13, 0x1

    #@f1
    new-array v13, v13, [Ljava/lang/Object;

    #@f3
    const/4 v14, 0x0

    #@f4
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f7
    move-result-object v15

    #@f8
    aput-object v15, v13, v14

    #@fa
    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@fd
    move-result-object v12

    #@fe
    invoke-static {v11, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    goto :goto_8c

    #@102
    .line 881
    .end local v1           #action:Ljava/lang/String;
    .end local v5           #label:Ljava/lang/String;
    .end local v6           #major:I
    .end local v7           #minor:I
    .end local v8           #path:Ljava/lang/String;
    .end local v9           #state:Ljava/lang/String;
    .end local v10           #volume:Landroid/os/storage/StorageVolume;
    :cond_102
    const/4 v11, 0x0

    #@103
    goto/16 :goto_21
.end method

.method public registerListener(Landroid/os/storage/IMountServiceListener;)V
    .registers 7
    .parameter "listener"

    #@0
    .prologue
    .line 1396
    iget-object v3, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 1397
    :try_start_3
    new-instance v0, Lcom/android/server/MountService$MountServiceBinderListener;

    #@5
    invoke-direct {v0, p0, p1}, Lcom/android/server/MountService$MountServiceBinderListener;-><init>(Lcom/android/server/MountService;Landroid/os/storage/IMountServiceListener;)V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_20

    #@8
    .line 1399
    .local v0, bl:Lcom/android/server/MountService$MountServiceBinderListener;
    :try_start_8
    invoke-interface {p1}, Landroid/os/storage/IMountServiceListener;->asBinder()Landroid/os/IBinder;

    #@b
    move-result-object v2

    #@c
    const/4 v4, 0x0

    #@d
    invoke-interface {v2, v0, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@10
    .line 1400
    iget-object v2, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_15
    .catchall {:try_start_8 .. :try_end_15} :catchall_20
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_15} :catch_17

    #@15
    .line 1404
    :goto_15
    :try_start_15
    monitor-exit v3

    #@16
    .line 1405
    return-void

    #@17
    .line 1401
    :catch_17
    move-exception v1

    #@18
    .line 1402
    .local v1, rex:Landroid/os/RemoteException;
    const-string v2, "MountService"

    #@1a
    const-string v4, "Failed to link to listener death"

    #@1c
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_15

    #@20
    .line 1404
    .end local v0           #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    .end local v1           #rex:Landroid/os/RemoteException;
    :catchall_20
    move-exception v2

    #@21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_15 .. :try_end_22} :catchall_20

    #@22
    throw v2
.end method

.method public renameSecureContainer(Ljava/lang/String;Ljava/lang/String;)I
    .registers 10
    .parameter "oldId"
    .parameter "newId"

    #@0
    .prologue
    .line 1854
    const-string v2, "android.permission.ASEC_RENAME"

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1855
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1856
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@b
    .line 1858
    iget-object v3, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@d
    monitor-enter v3

    #@e
    .line 1863
    :try_start_e
    iget-object v2, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@10
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_1e

    #@16
    iget-object v2, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@18
    invoke-virtual {v2, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_21

    #@1e
    .line 1864
    :cond_1e
    const/4 v1, -0x6

    #@1f
    monitor-exit v3

    #@20
    .line 1875
    :goto_20
    return v1

    #@21
    .line 1866
    :cond_21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_e .. :try_end_22} :catchall_3c

    #@22
    .line 1868
    const/4 v1, 0x0

    #@23
    .line 1870
    .local v1, rc:I
    :try_start_23
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@25
    const-string v3, "asec"

    #@27
    const/4 v4, 0x3

    #@28
    new-array v4, v4, [Ljava/lang/Object;

    #@2a
    const/4 v5, 0x0

    #@2b
    const-string v6, "rename"

    #@2d
    aput-object v6, v4, v5

    #@2f
    const/4 v5, 0x1

    #@30
    aput-object p1, v4, v5

    #@32
    const/4 v5, 0x2

    #@33
    aput-object p2, v4, v5

    #@35
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_38
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_23 .. :try_end_38} :catch_39

    #@38
    goto :goto_20

    #@39
    .line 1871
    :catch_39
    move-exception v0

    #@3a
    .line 1872
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const/4 v1, -0x1

    #@3b
    goto :goto_20

    #@3c
    .line 1866
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    .end local v1           #rc:I
    :catchall_3c
    move-exception v2

    #@3d
    :try_start_3d
    monitor-exit v3
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3c

    #@3e
    throw v2
.end method

.method public setUsbMassStorageEnabled(Z)V
    .registers 10
    .parameter "enable"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 1502
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@4
    .line 1503
    const-string v5, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@6
    invoke-direct {p0, v5}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@9
    .line 1505
    invoke-direct {p0}, Lcom/android/server/MountService;->getPrimaryPhysicalVolume()Landroid/os/storage/StorageVolume;

    #@c
    move-result-object v2

    #@d
    .line 1506
    .local v2, primary:Landroid/os/storage/StorageVolume;
    if-nez v2, :cond_10

    #@f
    .line 1539
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1513
    :cond_10
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 1514
    .local v1, path:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/server/MountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    .line 1515
    .local v4, vs:Ljava/lang/String;
    const-string v0, "ums"

    #@1a
    .line 1516
    .local v0, method:Ljava/lang/String;
    if-eqz p1, :cond_3b

    #@1c
    const-string v5, "mounted"

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v5

    #@22
    if-eqz v5, :cond_3b

    #@24
    .line 1518
    invoke-direct {p0, p1}, Lcom/android/server/MountService;->setUmsEnabling(Z)V

    #@27
    .line 1519
    new-instance v3, Lcom/android/server/MountService$UmsEnableCallBack;

    #@29
    invoke-direct {v3, p0, v1, v0, v7}, Lcom/android/server/MountService$UmsEnableCallBack;-><init>(Lcom/android/server/MountService;Ljava/lang/String;Ljava/lang/String;Z)V

    #@2c
    .line 1520
    .local v3, umscb:Lcom/android/server/MountService$UmsEnableCallBack;
    iget-object v5, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@2e
    iget-object v6, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@30
    invoke-virtual {v6, v7, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@37
    .line 1522
    const/4 v5, 0x0

    #@38
    invoke-direct {p0, v5}, Lcom/android/server/MountService;->setUmsEnabling(Z)V

    #@3b
    .line 1527
    .end local v3           #umscb:Lcom/android/server/MountService$UmsEnableCallBack;
    :cond_3b
    if-nez p1, :cond_f

    #@3d
    .line 1528
    invoke-direct {p0, v1, v0, p1}, Lcom/android/server/MountService;->doShareUnshareVolume(Ljava/lang/String;Ljava/lang/String;Z)V

    #@40
    .line 1529
    invoke-direct {p0, v1}, Lcom/android/server/MountService;->doMountVolume(Ljava/lang/String;)I

    #@43
    move-result v5

    #@44
    if-eqz v5, :cond_f

    #@46
    .line 1530
    const-string v5, "MountService"

    #@48
    new-instance v6, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v7, "Failed to remount "

    #@4f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v6

    #@57
    const-string v7, " after disabling share method "

    #@59
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v6

    #@5d
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_f
.end method

.method public shutdown(Landroid/os/storage/IMountShutdownObserver;)V
    .registers 14
    .parameter "observer"

    #@0
    .prologue
    .line 1420
    const-string v8, "android.permission.SHUTDOWN"

    #@2
    invoke-direct {p0, v8}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1422
    const-string v8, "MountService"

    #@7
    const-string v9, "Shutting down"

    #@9
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1423
    iget-object v9, p0, Lcom/android/server/MountService;->mVolumesLock:Ljava/lang/Object;

    #@e
    monitor-enter v9

    #@f
    .line 1424
    :try_start_f
    iget-object v8, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@11
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@14
    move-result-object v8

    #@15
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v1

    #@19
    .local v1, i$:Ljava/util/Iterator;
    :cond_19
    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v8

    #@1d
    if-eqz v8, :cond_a3

    #@1f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v3

    #@23
    check-cast v3, Ljava/lang/String;

    #@25
    .line 1425
    .local v3, path:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/MountService;->mVolumeStates:Ljava/util/HashMap;

    #@27
    invoke-virtual {v8, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    move-result-object v6

    #@2b
    check-cast v6, Ljava/lang/String;

    #@2d
    .line 1426
    .local v6, state:Ljava/lang/String;
    const-string v8, "removed"

    #@2f
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v8

    #@33
    if-nez v8, :cond_19

    #@35
    .line 1428
    const-string v8, "shared"

    #@37
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v8

    #@3b
    if-eqz v8, :cond_5e

    #@3d
    .line 1436
    const/4 v8, 0x0

    #@3e
    invoke-virtual {p0, v8}, Lcom/android/server/MountService;->setUsbMassStorageEnabled(Z)V

    #@41
    .line 1458
    :cond_41
    :goto_41
    const-string v8, "mounted"

    #@43
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v8

    #@47
    if-eqz v8, :cond_92

    #@49
    .line 1460
    new-instance v7, Lcom/android/server/MountService$ShutdownCallBack;

    #@4b
    invoke-direct {v7, p0, v3, p1}, Lcom/android/server/MountService$ShutdownCallBack;-><init>(Lcom/android/server/MountService;Ljava/lang/String;Landroid/os/storage/IMountShutdownObserver;)V

    #@4e
    .line 1461
    .local v7, ucb:Lcom/android/server/MountService$ShutdownCallBack;
    iget-object v8, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@50
    iget-object v10, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@52
    const/4 v11, 0x1

    #@53
    invoke-virtual {v10, v11, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@56
    move-result-object v10

    #@57
    invoke-virtual {v8, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@5a
    goto :goto_19

    #@5b
    .line 1475
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #path:Ljava/lang/String;
    .end local v6           #state:Ljava/lang/String;
    .end local v7           #ucb:Lcom/android/server/MountService$ShutdownCallBack;
    :catchall_5b
    move-exception v8

    #@5c
    monitor-exit v9
    :try_end_5d
    .catchall {:try_start_f .. :try_end_5d} :catchall_5b

    #@5d
    throw v8

    #@5e
    .line 1437
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #path:Ljava/lang/String;
    .restart local v6       #state:Ljava/lang/String;
    :cond_5e
    :try_start_5e
    const-string v8, "checking"

    #@60
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v8

    #@64
    if-eqz v8, :cond_41

    #@66
    .line 1443
    const/16 v4, 0x1e

    #@68
    .local v4, retries:I
    move v5, v4

    #@69
    .line 1444
    .end local v4           #retries:I
    .local v5, retries:I
    :goto_69
    const-string v8, "checking"

    #@6b
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_6e
    .catchall {:try_start_5e .. :try_end_6e} :catchall_5b

    #@6e
    move-result v8

    #@6f
    if-eqz v8, :cond_a5

    #@71
    add-int/lit8 v4, v5, -0x1

    #@73
    .end local v5           #retries:I
    .restart local v4       #retries:I
    if-ltz v5, :cond_88

    #@75
    .line 1446
    const-wide/16 v10, 0x3e8

    #@77
    :try_start_77
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7a
    .catchall {:try_start_77 .. :try_end_7a} :catchall_5b
    .catch Ljava/lang/InterruptedException; {:try_start_77 .. :try_end_7a} :catch_80

    #@7a
    .line 1451
    :try_start_7a
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@7d
    move-result-object v6

    #@7e
    move v5, v4

    #@7f
    .end local v4           #retries:I
    .restart local v5       #retries:I
    goto :goto_69

    #@80
    .line 1447
    .end local v5           #retries:I
    .restart local v4       #retries:I
    :catch_80
    move-exception v2

    #@81
    .line 1448
    .local v2, iex:Ljava/lang/InterruptedException;
    const-string v8, "MountService"

    #@83
    const-string v10, "Interrupted while waiting for media"

    #@85
    invoke-static {v8, v10, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@88
    .line 1453
    .end local v2           #iex:Ljava/lang/InterruptedException;
    :cond_88
    :goto_88
    if-nez v4, :cond_41

    #@8a
    .line 1454
    const-string v8, "MountService"

    #@8c
    const-string v10, "Timed out waiting for media to check"

    #@8e
    invoke-static {v8, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_91
    .catchall {:try_start_7a .. :try_end_91} :catchall_5b

    #@91
    goto :goto_41

    #@92
    .line 1462
    .end local v4           #retries:I
    :cond_92
    if-eqz p1, :cond_19

    #@94
    .line 1469
    const/4 v8, 0x0

    #@95
    :try_start_95
    invoke-interface {p1, v8}, Landroid/os/storage/IMountShutdownObserver;->onShutDownComplete(I)V
    :try_end_98
    .catchall {:try_start_95 .. :try_end_98} :catchall_5b
    .catch Landroid/os/RemoteException; {:try_start_95 .. :try_end_98} :catch_99

    #@98
    goto :goto_19

    #@99
    .line 1470
    :catch_99
    move-exception v0

    #@9a
    .line 1471
    .local v0, e:Landroid/os/RemoteException;
    :try_start_9a
    const-string v8, "MountService"

    #@9c
    const-string v10, "RemoteException when shutting down"

    #@9e
    invoke-static {v8, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto/16 :goto_19

    #@a3
    .line 1475
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v3           #path:Ljava/lang/String;
    .end local v6           #state:Ljava/lang/String;
    :cond_a3
    monitor-exit v9
    :try_end_a4
    .catchall {:try_start_9a .. :try_end_a4} :catchall_5b

    #@a4
    .line 1476
    return-void

    #@a5
    .restart local v3       #path:Ljava/lang/String;
    .restart local v5       #retries:I
    .restart local v6       #state:Ljava/lang/String;
    :cond_a5
    move v4, v5

    #@a6
    .end local v5           #retries:I
    .restart local v4       #retries:I
    goto :goto_88
.end method

.method public systemReady()V
    .registers 3

    #@0
    .prologue
    .line 1387
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/MountService;->mSystemReady:Z

    #@3
    .line 1388
    iget-object v0, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@5
    const/4 v1, 0x4

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@d
    .line 1389
    return-void
.end method

.method public unencryptStorage(Ljava/lang/String;)I
    .registers 9
    .parameter "password"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2826
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_f

    #@7
    .line 2827
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v2, "password cannot be empty"

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 2830
    :cond_f
    iget-object v2, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@11
    const-string v3, "android.permission.CRYPT_KEEPER"

    #@13
    const-string v4, "no permission to access the crypt keeper"

    #@15
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 2833
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@1b
    .line 2840
    :try_start_1b
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1d
    const-string v3, "cryptfs"

    #@1f
    const/4 v4, 0x3

    #@20
    new-array v4, v4, [Ljava/lang/Object;

    #@22
    const/4 v5, 0x0

    #@23
    const-string v6, "disablecrypto"

    #@25
    aput-object v6, v4, v5

    #@27
    const/4 v5, 0x1

    #@28
    const-string v6, "inplace"

    #@2a
    aput-object v6, v4, v5

    #@2c
    const/4 v5, 0x2

    #@2d
    aput-object p1, v4, v5

    #@2f
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_32
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1b .. :try_end_32} :catch_33

    #@32
    .line 2846
    :goto_32
    return v1

    #@33
    .line 2841
    :catch_33
    move-exception v0

    #@34
    .line 2843
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@37
    move-result v1

    #@38
    goto :goto_32
.end method

.method public unmountNetStorage(Ljava/lang/String;Z)I
    .registers 12
    .parameter "mountPoint"
    .parameter "force"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 2797
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_NFS:Z

    #@4
    if-nez v4, :cond_7

    #@6
    .line 2819
    :cond_6
    :goto_6
    return v2

    #@7
    .line 2801
    :cond_7
    const-string v4, "MountService"

    #@9
    new-instance v5, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v6, "unmountNetStorage: Unmouting "

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    const-string v6, ", "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 2802
    const-string v4, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@2b
    invoke-direct {p0, v4}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@2e
    .line 2803
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@31
    .line 2806
    :try_start_31
    iget-object v5, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@33
    const-string v6, "volume unmountns \"%s\" \"%s\""

    #@35
    const/4 v4, 0x2

    #@36
    new-array v7, v4, [Ljava/lang/Object;

    #@38
    const/4 v4, 0x0

    #@39
    aput-object p1, v7, v4

    #@3b
    const/4 v8, 0x1

    #@3c
    if-eqz p2, :cond_4b

    #@3e
    const-string v4, "force"

    #@40
    :goto_40
    aput-object v4, v7, v8

    #@42
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v5, v4}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@49
    move v2, v3

    #@4a
    .line 2809
    goto :goto_6

    #@4b
    .line 2806
    :cond_4b
    const-string v4, ""
    :try_end_4d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_31 .. :try_end_4d} :catch_4e

    #@4d
    goto :goto_40

    #@4e
    .line 2810
    :catch_4e
    move-exception v1

    #@4f
    .line 2813
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@52
    move-result v0

    #@53
    .line 2814
    .local v0, code:I
    const/16 v3, 0x194

    #@55
    if-ne v0, v3, :cond_59

    #@57
    .line 2815
    const/4 v2, -0x5

    #@58
    goto :goto_6

    #@59
    .line 2816
    :cond_59
    const/16 v3, 0x195

    #@5b
    if-ne v0, v3, :cond_6

    #@5d
    .line 2817
    const/4 v2, -0x7

    #@5e
    goto :goto_6
.end method

.method public unmountObb(Ljava/lang/String;ZLandroid/os/storage/IObbActionListener;I)V
    .registers 14
    .parameter "rawPath"
    .parameter "force"
    .parameter "token"
    .parameter "nonce"

    #@0
    .prologue
    .line 1999
    const-string v1, "rawPath cannot be null"

    #@2
    invoke-static {p1, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 2002
    iget-object v2, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@7
    monitor-enter v2

    #@8
    .line 2003
    :try_start_8
    iget-object v1, p0, Lcom/android/server/MountService;->mObbPathToStateMap:Ljava/util/Map;

    #@a
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v8

    #@e
    check-cast v8, Lcom/android/server/MountService$ObbState;

    #@10
    .line 2004
    .local v8, existingState:Lcom/android/server/MountService$ObbState;
    monitor-exit v2
    :try_end_11
    .catchall {:try_start_8 .. :try_end_11} :catchall_34

    #@11
    .line 2006
    if-eqz v8, :cond_37

    #@13
    .line 2008
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@16
    move-result v4

    #@17
    .line 2009
    .local v4, callingUid:I
    new-instance v0, Lcom/android/server/MountService$ObbState;

    #@19
    iget-object v3, v8, Lcom/android/server/MountService$ObbState;->canonicalPath:Ljava/lang/String;

    #@1b
    move-object v1, p0

    #@1c
    move-object v2, p1

    #@1d
    move-object v5, p3

    #@1e
    move v6, p4

    #@1f
    invoke-direct/range {v0 .. v6}, Lcom/android/server/MountService$ObbState;-><init>(Lcom/android/server/MountService;Ljava/lang/String;Ljava/lang/String;ILandroid/os/storage/IObbActionListener;I)V

    #@22
    .line 2011
    .local v0, newState:Lcom/android/server/MountService$ObbState;
    new-instance v7, Lcom/android/server/MountService$UnmountObbAction;

    #@24
    invoke-direct {v7, p0, v0, p2}, Lcom/android/server/MountService$UnmountObbAction;-><init>(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;Z)V

    #@27
    .line 2012
    .local v7, action:Lcom/android/server/MountService$ObbAction;
    iget-object v1, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@29
    iget-object v2, p0, Lcom/android/server/MountService;->mObbActionHandler:Lcom/android/server/MountService$ObbActionHandler;

    #@2b
    const/4 v3, 0x1

    #@2c
    invoke-virtual {v2, v3, v7}, Lcom/android/server/MountService$ObbActionHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v1, v2}, Lcom/android/server/MountService$ObbActionHandler;->sendMessage(Landroid/os/Message;)Z

    #@33
    .line 2019
    .end local v0           #newState:Lcom/android/server/MountService$ObbState;
    .end local v4           #callingUid:I
    .end local v7           #action:Lcom/android/server/MountService$ObbAction;
    :goto_33
    return-void

    #@34
    .line 2004
    .end local v8           #existingState:Lcom/android/server/MountService$ObbState;
    :catchall_34
    move-exception v1

    #@35
    :try_start_35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v1

    #@37
    .line 2017
    .restart local v8       #existingState:Lcom/android/server/MountService$ObbState;
    :cond_37
    const-string v1, "MountService"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unknown OBB mount at "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_33
.end method

.method public unmountSecureContainer(Ljava/lang/String;Z)I
    .registers 11
    .parameter "id"
    .parameter "force"

    #@0
    .prologue
    .line 1796
    const-string v4, "android.permission.ASEC_MOUNT_UNMOUNT"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1797
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1798
    invoke-direct {p0}, Lcom/android/server/MountService;->warnOnNotMounted()V

    #@b
    .line 1800
    iget-object v5, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@d
    monitor-enter v5

    #@e
    .line 1801
    :try_start_e
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@10
    invoke-virtual {v4, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_19

    #@16
    .line 1802
    const/4 v3, -0x5

    #@17
    monitor-exit v5

    #@18
    .line 1840
    :cond_18
    :goto_18
    return v3

    #@19
    .line 1804
    :cond_19
    monitor-exit v5
    :try_end_1a
    .catchall {:try_start_e .. :try_end_1a} :catchall_4f

    #@1a
    .line 1812
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/Runtime;->gc()V

    #@21
    .line 1814
    const/4 v3, 0x0

    #@22
    .line 1816
    .local v3, rc:I
    :try_start_22
    new-instance v0, Lcom/android/server/NativeDaemonConnector$Command;

    #@24
    const-string v4, "asec"

    #@26
    const/4 v5, 0x2

    #@27
    new-array v5, v5, [Ljava/lang/Object;

    #@29
    const/4 v6, 0x0

    #@2a
    const-string v7, "unmount"

    #@2c
    aput-object v7, v5, v6

    #@2e
    const/4 v6, 0x1

    #@2f
    aput-object p1, v5, v6

    #@31
    invoke-direct {v0, v4, v5}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@34
    .line 1817
    .local v0, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    if-eqz p2, :cond_3b

    #@36
    .line 1818
    const-string v4, "force"

    #@38
    invoke-virtual {v0, v4}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@3b
    .line 1820
    :cond_3b
    iget-object v4, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@3d
    invoke-virtual {v4, v0}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_40
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_22 .. :try_end_40} :catch_52

    #@40
    .line 1835
    .end local v0           #cmd:Lcom/android/server/NativeDaemonConnector$Command;
    :goto_40
    if-nez v3, :cond_18

    #@42
    .line 1836
    iget-object v5, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@44
    monitor-enter v5

    #@45
    .line 1837
    :try_start_45
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@47
    invoke-virtual {v4, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@4a
    .line 1838
    monitor-exit v5

    #@4b
    goto :goto_18

    #@4c
    :catchall_4c
    move-exception v4

    #@4d
    monitor-exit v5
    :try_end_4e
    .catchall {:try_start_45 .. :try_end_4e} :catchall_4c

    #@4e
    throw v4

    #@4f
    .line 1804
    .end local v3           #rc:I
    :catchall_4f
    move-exception v4

    #@50
    :try_start_50
    monitor-exit v5
    :try_end_51
    .catchall {:try_start_50 .. :try_end_51} :catchall_4f

    #@51
    throw v4

    #@52
    .line 1821
    .restart local v3       #rc:I
    :catch_52
    move-exception v2

    #@53
    .line 1822
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@56
    move-result v1

    #@57
    .line 1823
    .local v1, code:I
    const/16 v4, 0x195

    #@59
    if-ne v1, v4, :cond_5d

    #@5b
    .line 1824
    const/4 v3, -0x7

    #@5c
    goto :goto_40

    #@5d
    .line 1826
    :cond_5d
    const/4 v3, -0x1

    #@5e
    .line 1828
    iget-object v5, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@60
    monitor-enter v5

    #@61
    .line 1829
    :try_start_61
    iget-object v4, p0, Lcom/android/server/MountService;->mAsecMountSet:Ljava/util/HashSet;

    #@63
    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    #@66
    .line 1830
    monitor-exit v5

    #@67
    goto :goto_40

    #@68
    :catchall_68
    move-exception v4

    #@69
    monitor-exit v5
    :try_end_6a
    .catchall {:try_start_61 .. :try_end_6a} :catchall_68

    #@6a
    throw v4
.end method

.method public unmountVolume(Ljava/lang/String;ZZ)V
    .registers 9
    .parameter "path"
    .parameter "force"
    .parameter "removeEncryption"

    #@0
    .prologue
    .line 1584
    const-string v2, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/MountService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 1585
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@8
    .line 1587
    invoke-virtual {p0, p1}, Lcom/android/server/MountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 1593
    .local v1, volState:Ljava/lang/String;
    const-string v2, "unmounted"

    #@e
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_2c

    #@14
    const-string v2, "removed"

    #@16
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-nez v2, :cond_2c

    #@1c
    const-string v2, "shared"

    #@1e
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_2c

    #@24
    const-string v2, "unmountable"

    #@26
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_2d

    #@2c
    .line 1603
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 1601
    :cond_2d
    new-instance v0, Lcom/android/server/MountService$UnmountCallBack;

    #@2f
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/MountService$UnmountCallBack;-><init>(Lcom/android/server/MountService;Ljava/lang/String;ZZ)V

    #@32
    .line 1602
    .local v0, ucb:Lcom/android/server/MountService$UnmountCallBack;
    iget-object v2, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@34
    iget-object v3, p0, Lcom/android/server/MountService;->mHandler:Landroid/os/Handler;

    #@36
    const/4 v4, 0x1

    #@37
    invoke-virtual {v3, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@3e
    goto :goto_2c
.end method

.method public unregisterListener(Landroid/os/storage/IMountServiceListener;)V
    .registers 7
    .parameter "listener"

    #@0
    .prologue
    .line 1408
    iget-object v3, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 1409
    :try_start_3
    iget-object v2, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_2e

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/android/server/MountService$MountServiceBinderListener;

    #@15
    .line 1410
    .local v0, bl:Lcom/android/server/MountService$MountServiceBinderListener;
    iget-object v2, v0, Lcom/android/server/MountService$MountServiceBinderListener;->mListener:Landroid/os/storage/IMountServiceListener;

    #@17
    if-ne v2, p1, :cond_9

    #@19
    .line 1411
    iget-object v2, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@1b
    iget-object v4, p0, Lcom/android/server/MountService;->mListeners:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@20
    move-result v4

    #@21
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@24
    .line 1412
    invoke-interface {p1}, Landroid/os/storage/IMountServiceListener;->asBinder()Landroid/os/IBinder;

    #@27
    move-result-object v2

    #@28
    const/4 v4, 0x0

    #@29
    invoke-interface {v2, v0, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@2c
    .line 1413
    monitor-exit v3

    #@2d
    .line 1417
    .end local v0           #bl:Lcom/android/server/MountService$MountServiceBinderListener;
    :goto_2d
    return-void

    #@2e
    .line 1416
    :cond_2e
    monitor-exit v3

    #@2f
    goto :goto_2d

    #@30
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_30
    move-exception v2

    #@31
    monitor-exit v3
    :try_end_32
    .catchall {:try_start_3 .. :try_end_32} :catchall_30

    #@32
    throw v2
.end method

.method public verifyEncryptionPassword(Ljava/lang/String;)I
    .registers 9
    .parameter "password"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2138
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v2

    #@4
    const/16 v3, 0x3e8

    #@6
    if-eq v2, v3, :cond_10

    #@8
    .line 2139
    new-instance v2, Ljava/lang/SecurityException;

    #@a
    const-string v3, "no permission to access the crypt keeper"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 2142
    :cond_10
    iget-object v2, p0, Lcom/android/server/MountService;->mContext:Landroid/content/Context;

    #@12
    const-string v3, "android.permission.CRYPT_KEEPER"

    #@14
    const-string v4, "no permission to access the crypt keeper"

    #@16
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 2145
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_27

    #@1f
    .line 2146
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@21
    const-string v3, "password cannot be empty"

    #@23
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v2

    #@27
    .line 2149
    :cond_27
    invoke-direct {p0}, Lcom/android/server/MountService;->waitForReady()V

    #@2a
    .line 2157
    :try_start_2a
    iget-object v2, p0, Lcom/android/server/MountService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2c
    const-string v3, "cryptfs"

    #@2e
    const/4 v4, 0x2

    #@2f
    new-array v4, v4, [Ljava/lang/Object;

    #@31
    const/4 v5, 0x0

    #@32
    const-string v6, "verifypw"

    #@34
    aput-object v6, v4, v5

    #@36
    const/4 v5, 0x1

    #@37
    aput-object p1, v4, v5

    #@39
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@3c
    move-result-object v1

    #@3d
    .line 2158
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    const-string v2, "MountService"

    #@3f
    new-instance v3, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v4, "cryptfs verifypw => "

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 2159
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_60
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_2a .. :try_end_60} :catch_62

    #@60
    move-result v2

    #@61
    .line 2162
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :goto_61
    return v2

    #@62
    .line 2160
    :catch_62
    move-exception v0

    #@63
    .line 2162
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@66
    move-result v2

    #@67
    goto :goto_61
.end method

.method waitForAsecScan()V
    .registers 2

    #@0
    .prologue
    .line 494
    iget-object v0, p0, Lcom/android/server/MountService;->mAsecsScanned:Ljava/util/concurrent/CountDownLatch;

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/MountService;->waitForLatch(Ljava/util/concurrent/CountDownLatch;)V

    #@5
    .line 495
    return-void
.end method
