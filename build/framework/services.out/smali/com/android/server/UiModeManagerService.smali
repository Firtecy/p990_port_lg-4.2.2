.class final Lcom/android/server/UiModeManagerService;
.super Landroid/app/IUiModeManager$Stub;
.source "UiModeManagerService.java"


# static fields
.field private static final ENABLE_LAUNCH_CAR_DOCK_APP:Z = true

.field private static final ENABLE_LAUNCH_DESK_DOCK_APP:Z = true

.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBatteryReceiver:Landroid/content/BroadcastReceiver;

.field private mCarModeEnabled:Z

.field private final mCarModeKeepsScreenOn:Z

.field private mCharging:Z

.field private mComputedNightMode:Z

.field private mConfiguration:Landroid/content/res/Configuration;

.field private final mContext:Landroid/content/Context;

.field private mCurUiMode:I

.field private final mDefaultUiModeType:I

.field private final mDeskModeKeepsScreenOn:Z

.field private final mDockModeReceiver:Landroid/content/BroadcastReceiver;

.field private mDockState:I

.field private final mHandler:Landroid/os/Handler;

.field private mHoldingConfiguration:Z

.field private mLastBroadcastState:I

.field final mLock:Ljava/lang/Object;

.field private mNightMode:I

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private final mPowerManager:Landroid/os/PowerManager;

.field private final mResultReceiver:Landroid/content/BroadcastReceiver;

.field private mSetUiMode:I

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mSystemReady:Z

.field private final mTelevision:Z

.field private final mTwilightListener:Lcom/android/server/TwilightService$TwilightListener;

.field private final mTwilightService:Lcom/android/server/TwilightService;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    const-class v0, Landroid/app/UiModeManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/UiModeManagerService;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/TwilightService;)V
    .registers 9
    .parameter "context"
    .parameter "twilight"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 153
    invoke-direct {p0}, Landroid/app/IUiModeManager$Stub;-><init>()V

    #@5
    .line 62
    new-instance v0, Landroid/os/Handler;

    #@7
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mHandler:Landroid/os/Handler;

    #@c
    .line 64
    new-instance v0, Ljava/lang/Object;

    #@e
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@11
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@13
    .line 66
    iput v2, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@15
    .line 67
    iput v2, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@17
    .line 69
    iput v1, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@19
    .line 70
    iput-boolean v2, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@1b
    .line 71
    iput-boolean v2, p0, Lcom/android/server/UiModeManagerService;->mCharging:Z

    #@1d
    .line 78
    iput v2, p0, Lcom/android/server/UiModeManagerService;->mCurUiMode:I

    #@1f
    .line 79
    iput v2, p0, Lcom/android/server/UiModeManagerService;->mSetUiMode:I

    #@21
    .line 81
    iput-boolean v2, p0, Lcom/android/server/UiModeManagerService;->mHoldingConfiguration:Z

    #@23
    .line 82
    new-instance v0, Landroid/content/res/Configuration;

    #@25
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@28
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@2a
    .line 105
    new-instance v0, Lcom/android/server/UiModeManagerService$1;

    #@2c
    invoke-direct {v0, p0}, Lcom/android/server/UiModeManagerService$1;-><init>(Lcom/android/server/UiModeManagerService;)V

    #@2f
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mResultReceiver:Landroid/content/BroadcastReceiver;

    #@31
    .line 124
    new-instance v0, Lcom/android/server/UiModeManagerService$2;

    #@33
    invoke-direct {v0, p0}, Lcom/android/server/UiModeManagerService$2;-><init>(Lcom/android/server/UiModeManagerService;)V

    #@36
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mDockModeReceiver:Landroid/content/BroadcastReceiver;

    #@38
    .line 133
    new-instance v0, Lcom/android/server/UiModeManagerService$3;

    #@3a
    invoke-direct {v0, p0}, Lcom/android/server/UiModeManagerService$3;-><init>(Lcom/android/server/UiModeManagerService;)V

    #@3d
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    #@3f
    .line 145
    new-instance v0, Lcom/android/server/UiModeManagerService$4;

    #@41
    invoke-direct {v0, p0}, Lcom/android/server/UiModeManagerService$4;-><init>(Lcom/android/server/UiModeManagerService;)V

    #@44
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mTwilightListener:Lcom/android/server/TwilightService$TwilightListener;

    #@46
    .line 154
    iput-object p1, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@48
    .line 155
    iput-object p2, p0, Lcom/android/server/UiModeManagerService;->mTwilightService:Lcom/android/server/TwilightService;

    #@4a
    .line 157
    const-string v0, "uimode"

    #@4c
    invoke-static {v0, p0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@4f
    .line 159
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@51
    iget-object v3, p0, Lcom/android/server/UiModeManagerService;->mDockModeReceiver:Landroid/content/BroadcastReceiver;

    #@53
    new-instance v4, Landroid/content/IntentFilter;

    #@55
    const-string v5, "android.intent.action.DOCK_EVENT"

    #@57
    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@5a
    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@5d
    .line 161
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@5f
    iget-object v3, p0, Lcom/android/server/UiModeManagerService;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    #@61
    new-instance v4, Landroid/content/IntentFilter;

    #@63
    const-string v5, "android.intent.action.BATTERY_CHANGED"

    #@65
    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@68
    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@6b
    .line 164
    const-string v0, "power"

    #@6d
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@70
    move-result-object v0

    #@71
    check-cast v0, Landroid/os/PowerManager;

    #@73
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@75
    .line 165
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@77
    const/16 v3, 0x1a

    #@79
    sget-object v4, Lcom/android/server/UiModeManagerService;->TAG:Ljava/lang/String;

    #@7b
    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@7e
    move-result-object v0

    #@7f
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@81
    .line 167
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@83
    invoke-virtual {v0}, Landroid/content/res/Configuration;->setToDefaults()V

    #@86
    .line 169
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@89
    move-result-object v0

    #@8a
    const v3, 0x10e0013

    #@8d
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@90
    move-result v0

    #@91
    iput v0, p0, Lcom/android/server/UiModeManagerService;->mDefaultUiModeType:I

    #@93
    .line 171
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@96
    move-result-object v0

    #@97
    const v3, 0x10e0015

    #@9a
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@9d
    move-result v0

    #@9e
    if-ne v0, v1, :cond_d6

    #@a0
    move v0, v1

    #@a1
    :goto_a1
    iput-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeKeepsScreenOn:Z

    #@a3
    .line 173
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a6
    move-result-object v0

    #@a7
    const v3, 0x10e0014

    #@aa
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@ad
    move-result v0

    #@ae
    if-ne v0, v1, :cond_d8

    #@b0
    :goto_b0
    iput-boolean v1, p0, Lcom/android/server/UiModeManagerService;->mDeskModeKeepsScreenOn:Z

    #@b2
    .line 175
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b5
    move-result-object v0

    #@b6
    const-string v1, "android.hardware.type.television"

    #@b8
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@bb
    move-result v0

    #@bc
    iput-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mTelevision:Z

    #@be
    .line 178
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@c0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c3
    move-result-object v0

    #@c4
    const-string v1, "ui_night_mode"

    #@c6
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c9
    move-result v0

    #@ca
    iput v0, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@cc
    .line 181
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mTwilightService:Lcom/android/server/TwilightService;

    #@ce
    iget-object v1, p0, Lcom/android/server/UiModeManagerService;->mTwilightListener:Lcom/android/server/TwilightService$TwilightListener;

    #@d0
    iget-object v2, p0, Lcom/android/server/UiModeManagerService;->mHandler:Landroid/os/Handler;

    #@d2
    invoke-virtual {v0, v1, v2}, Lcom/android/server/TwilightService;->registerListener(Lcom/android/server/TwilightService$TwilightListener;Landroid/os/Handler;)V

    #@d5
    .line 182
    return-void

    #@d6
    :cond_d6
    move v0, v2

    #@d7
    .line 171
    goto :goto_a1

    #@d8
    :cond_d8
    move v1, v2

    #@d9
    .line 173
    goto :goto_b0
.end method

.method static synthetic access$000(Lcom/android/server/UiModeManagerService;Ljava/lang/String;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/UiModeManagerService;->updateAfterBroadcastLocked(Ljava/lang/String;II)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/UiModeManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/server/UiModeManagerService;->updateDockState(I)V

    #@3
    return-void
.end method

.method static synthetic access$202(Lcom/android/server/UiModeManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/android/server/UiModeManagerService;->mCharging:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/server/UiModeManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mSystemReady:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/server/UiModeManagerService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/UiModeManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->updateTwilight()V

    #@3
    return-void
.end method

.method private adjustStatusBarCarModeLocked()V
    .registers 12

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const v10, 0x10404bb

    #@4
    const/4 v1, 0x0

    #@5
    .line 521
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@7
    if-nez v0, :cond_15

    #@9
    .line 522
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@b
    const-string v3, "statusbar"

    #@d
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/app/StatusBarManager;

    #@13
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@15
    .line 531
    :cond_15
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@17
    if-eqz v0, :cond_24

    #@19
    .line 532
    iget-object v3, p0, Lcom/android/server/UiModeManagerService;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@1b
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@1d
    if-eqz v0, :cond_7e

    #@1f
    const/high16 v0, 0x8

    #@21
    :goto_21
    invoke-virtual {v3, v0}, Landroid/app/StatusBarManager;->disable(I)V

    #@24
    .line 537
    :cond_24
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@26
    if-nez v0, :cond_34

    #@28
    .line 538
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@2a
    const-string v3, "notification"

    #@2c
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Landroid/app/NotificationManager;

    #@32
    iput-object v0, p0, Lcom/android/server/UiModeManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@34
    .line 542
    :cond_34
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@36
    if-eqz v0, :cond_7d

    #@38
    .line 543
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@3a
    if-eqz v0, :cond_80

    #@3c
    .line 544
    new-instance v2, Landroid/content/Intent;

    #@3e
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@40
    const-class v3, Lcom/android/internal/app/DisableCarModeActivity;

    #@42
    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@45
    .line 546
    .local v2, carModeOffIntent:Landroid/content/Intent;
    new-instance v6, Landroid/app/Notification;

    #@47
    invoke-direct {v6}, Landroid/app/Notification;-><init>()V

    #@4a
    .line 547
    .local v6, n:Landroid/app/Notification;
    const v0, 0x1080524

    #@4d
    iput v0, v6, Landroid/app/Notification;->icon:I

    #@4f
    .line 548
    const/4 v0, 0x4

    #@50
    iput v0, v6, Landroid/app/Notification;->defaults:I

    #@52
    .line 549
    const/4 v0, 0x2

    #@53
    iput v0, v6, Landroid/app/Notification;->flags:I

    #@55
    .line 550
    const-wide/16 v7, 0x0

    #@57
    iput-wide v7, v6, Landroid/app/Notification;->when:J

    #@59
    .line 551
    iget-object v7, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@5b
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@5d
    invoke-virtual {v0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@60
    move-result-object v8

    #@61
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@63
    const v3, 0x10404bc

    #@66
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v9

    #@6a
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@6c
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@6e
    move v3, v1

    #@6f
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {v6, v7, v8, v9, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@76
    .line 557
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@78
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@7a
    invoke-virtual {v0, v4, v10, v6, v1}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@7d
    .line 564
    .end local v2           #carModeOffIntent:Landroid/content/Intent;
    .end local v6           #n:Landroid/app/Notification;
    :cond_7d
    :goto_7d
    return-void

    #@7e
    :cond_7e
    move v0, v1

    #@7f
    .line 532
    goto :goto_21

    #@80
    .line 560
    :cond_80
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@82
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@84
    invoke-virtual {v0, v4, v10, v1}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@87
    goto :goto_7d
.end method

.method static buildHomeIntent(Ljava/lang/String;)Landroid/content/Intent;
    .registers 3
    .parameter "category"

    #@0
    .prologue
    .line 94
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.MAIN"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 95
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@a
    .line 96
    const/high16 v1, 0x1020

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@f
    .line 98
    return-object v0
.end method

.method private static isDeskDockState(I)Z
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 291
    packed-switch p0, :pswitch_data_8

    #@3
    .line 297
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 295
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 291
    nop

    #@8
    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private isDoingNightModeLocked()Z
    .registers 2

    #@0
    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@2
    if-nez v0, :cond_8

    #@4
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private sendConfigurationAndStartDreamOrDockAppLocked(Ljava/lang/String;)V
    .registers 16
    .parameter "category"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 480
    iput-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mHoldingConfiguration:Z

    #@3
    .line 481
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->updateConfigurationLocked()V

    #@6
    .line 484
    const/4 v11, 0x1

    #@7
    .line 485
    .local v11, dockAppStarted:Z
    if-eqz p1, :cond_2a

    #@9
    .line 493
    invoke-static {p1}, Lcom/android/server/UiModeManagerService;->buildHomeIntent(Ljava/lang/String;)Landroid/content/Intent;

    #@c
    move-result-object v2

    #@d
    .line 494
    .local v2, homeIntent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@f
    invoke-static {v0, v2}, Landroid/service/dreams/Sandman;->shouldStartDockApp(Landroid/content/Context;Landroid/content/Intent;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_2a

    #@15
    .line 496
    :try_start_15
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@18
    move-result-object v0

    #@19
    const/4 v1, 0x0

    #@1a
    const/4 v3, 0x0

    #@1b
    const/4 v4, 0x0

    #@1c
    const/4 v5, 0x0

    #@1d
    const/4 v6, 0x0

    #@1e
    const/4 v7, 0x0

    #@1f
    iget-object v8, p0, Lcom/android/server/UiModeManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@21
    const/4 v9, 0x0

    #@22
    const/4 v10, -0x2

    #@23
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->startActivityWithConfig(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/res/Configuration;Landroid/os/Bundle;I)I
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_26} :catch_5d

    #@26
    move-result v13

    #@27
    .line 499
    .local v13, result:I
    if-ltz v13, :cond_37

    #@29
    .line 500
    const/4 v11, 0x1

    #@2a
    .line 512
    .end local v2           #homeIntent:Landroid/content/Intent;
    .end local v13           #result:I
    :cond_2a
    :goto_2a
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->sendConfigurationLocked()V

    #@2d
    .line 515
    if-eqz p1, :cond_36

    #@2f
    if-nez v11, :cond_36

    #@31
    .line 516
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@33
    invoke-static {v0}, Landroid/service/dreams/Sandman;->startDreamWhenDockedIfAppropriate(Landroid/content/Context;)V

    #@36
    .line 518
    :cond_36
    return-void

    #@37
    .line 501
    .restart local v2       #homeIntent:Landroid/content/Intent;
    .restart local v13       #result:I
    :cond_37
    const/4 v0, -0x1

    #@38
    if-eq v13, v0, :cond_2a

    #@3a
    .line 502
    :try_start_3a
    sget-object v0, Lcom/android/server/UiModeManagerService;->TAG:Ljava/lang/String;

    #@3c
    new-instance v1, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Could not start dock app: "

    #@43
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    const-string v3, ", startActivityWithConfig result "

    #@4d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5c
    .catch Landroid/os/RemoteException; {:try_start_3a .. :try_end_5c} :catch_5d

    #@5c
    goto :goto_2a

    #@5d
    .line 505
    .end local v13           #result:I
    :catch_5d
    move-exception v12

    #@5e
    .line 506
    .local v12, ex:Landroid/os/RemoteException;
    sget-object v0, Lcom/android/server/UiModeManagerService;->TAG:Ljava/lang/String;

    #@60
    new-instance v1, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v3, "Could not start dock app: "

    #@67
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    invoke-static {v0, v1, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@76
    goto :goto_2a
.end method

.method private sendConfigurationLocked()V
    .registers 4

    #@0
    .prologue
    .line 336
    iget v1, p0, Lcom/android/server/UiModeManagerService;->mSetUiMode:I

    #@2
    iget-object v2, p0, Lcom/android/server/UiModeManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@4
    iget v2, v2, Landroid/content/res/Configuration;->uiMode:I

    #@6
    if-eq v1, v2, :cond_17

    #@8
    .line 337
    iget-object v1, p0, Lcom/android/server/UiModeManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@a
    iget v1, v1, Landroid/content/res/Configuration;->uiMode:I

    #@c
    iput v1, p0, Lcom/android/server/UiModeManagerService;->mSetUiMode:I

    #@e
    .line 340
    :try_start_e
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@11
    move-result-object v1

    #@12
    iget-object v2, p0, Lcom/android/server/UiModeManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@14
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_17} :catch_18

    #@17
    .line 345
    :cond_17
    :goto_17
    return-void

    #@18
    .line 341
    :catch_18
    move-exception v0

    #@19
    .line 342
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Lcom/android/server/UiModeManagerService;->TAG:Ljava/lang/String;

    #@1b
    const-string v2, "Failure communicating with activity manager"

    #@1d
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@20
    goto :goto_17
.end method

.method private setCarModeLocked(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@2
    if-eq v0, p1, :cond_6

    #@4
    .line 274
    iput-boolean p1, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@6
    .line 276
    :cond_6
    return-void
.end method

.method private updateAfterBroadcastLocked(Ljava/lang/String;II)V
    .registers 6
    .parameter "action"
    .parameter "enableFlags"
    .parameter "disableFlags"

    #@0
    .prologue
    .line 445
    const/4 v0, 0x0

    #@1
    .line 446
    .local v0, category:Ljava/lang/String;
    sget-object v1, Landroid/app/UiModeManager;->ACTION_ENTER_CAR_MODE:Ljava/lang/String;

    #@3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_13

    #@9
    .line 449
    and-int/lit8 v1, p2, 0x1

    #@b
    if-eqz v1, :cond_f

    #@d
    .line 451
    const-string v0, "android.intent.category.CAR_DOCK"

    #@f
    .line 475
    :cond_f
    :goto_f
    invoke-direct {p0, v0}, Lcom/android/server/UiModeManagerService;->sendConfigurationAndStartDreamOrDockAppLocked(Ljava/lang/String;)V

    #@12
    .line 476
    return-void

    #@13
    .line 453
    :cond_13
    sget-object v1, Landroid/app/UiModeManager;->ACTION_ENTER_DESK_MODE:Ljava/lang/String;

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_22

    #@1b
    .line 457
    and-int/lit8 v1, p2, 0x1

    #@1d
    if-eqz v1, :cond_f

    #@1f
    .line 459
    const-string v0, "android.intent.category.DESK_DOCK"

    #@21
    goto :goto_f

    #@22
    .line 463
    :cond_22
    and-int/lit8 v1, p3, 0x1

    #@24
    if-eqz v1, :cond_f

    #@26
    .line 464
    const-string v0, "android.intent.category.HOME"

    #@28
    goto :goto_f
.end method

.method private updateComputedNightModeLocked()V
    .registers 3

    #@0
    .prologue
    .line 576
    iget-object v1, p0, Lcom/android/server/UiModeManagerService;->mTwilightService:Lcom/android/server/TwilightService;

    #@2
    invoke-virtual {v1}, Lcom/android/server/TwilightService;->getCurrentState()Lcom/android/server/TwilightService$TwilightState;

    #@5
    move-result-object v0

    #@6
    .line 577
    .local v0, state:Lcom/android/server/TwilightService$TwilightState;
    if-eqz v0, :cond_e

    #@8
    .line 578
    invoke-virtual {v0}, Lcom/android/server/TwilightService$TwilightState;->isNight()Z

    #@b
    move-result v1

    #@c
    iput-boolean v1, p0, Lcom/android/server/UiModeManagerService;->mComputedNightMode:Z

    #@e
    .line 580
    :cond_e
    return-void
.end method

.method private updateConfigurationLocked()V
    .registers 3

    #@0
    .prologue
    .line 302
    iget-boolean v1, p0, Lcom/android/server/UiModeManagerService;->mTelevision:Z

    #@2
    if-eqz v1, :cond_27

    #@4
    const/4 v0, 0x4

    #@5
    .line 303
    .local v0, uiMode:I
    :goto_5
    iget-boolean v1, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@7
    if-eqz v1, :cond_2a

    #@9
    .line 304
    const/4 v0, 0x3

    #@a
    .line 308
    :cond_a
    :goto_a
    iget-boolean v1, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@c
    if-eqz v1, :cond_3d

    #@e
    .line 309
    iget v1, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@10
    if-nez v1, :cond_37

    #@12
    .line 310
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->updateComputedNightModeLocked()V

    #@15
    .line 311
    iget-boolean v1, p0, Lcom/android/server/UiModeManagerService;->mComputedNightMode:Z

    #@17
    if-eqz v1, :cond_34

    #@19
    const/16 v1, 0x20

    #@1b
    :goto_1b
    or-int/2addr v0, v1

    #@1c
    .line 329
    :goto_1c
    iput v0, p0, Lcom/android/server/UiModeManagerService;->mCurUiMode:I

    #@1e
    .line 330
    iget-boolean v1, p0, Lcom/android/server/UiModeManagerService;->mHoldingConfiguration:Z

    #@20
    if-nez v1, :cond_26

    #@22
    .line 331
    iget-object v1, p0, Lcom/android/server/UiModeManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@24
    iput v0, v1, Landroid/content/res/Configuration;->uiMode:I

    #@26
    .line 333
    :cond_26
    return-void

    #@27
    .line 302
    .end local v0           #uiMode:I
    :cond_27
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mDefaultUiModeType:I

    #@29
    goto :goto_5

    #@2a
    .line 305
    .restart local v0       #uiMode:I
    :cond_2a
    iget v1, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@2c
    invoke-static {v1}, Lcom/android/server/UiModeManagerService;->isDeskDockState(I)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_a

    #@32
    .line 306
    const/4 v0, 0x2

    #@33
    goto :goto_a

    #@34
    .line 311
    :cond_34
    const/16 v1, 0x10

    #@36
    goto :goto_1b

    #@37
    .line 314
    :cond_37
    iget v1, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@39
    shl-int/lit8 v1, v1, 0x4

    #@3b
    or-int/2addr v0, v1

    #@3c
    goto :goto_1c

    #@3d
    .line 318
    :cond_3d
    and-int/lit8 v1, v0, -0x31

    #@3f
    or-int/lit8 v0, v1, 0x10

    #@41
    goto :goto_1c
.end method

.method private updateDockState(I)V
    .registers 7
    .parameter "newState"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 279
    iget-object v2, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v2

    #@5
    .line 280
    :try_start_5
    iget v3, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@7
    if-eq p1, v3, :cond_1c

    #@9
    .line 281
    iput p1, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@b
    .line 282
    iget v3, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@d
    const/4 v4, 0x2

    #@e
    if-ne v3, v4, :cond_1e

    #@10
    :goto_10
    invoke-direct {p0, v0}, Lcom/android/server/UiModeManagerService;->setCarModeLocked(Z)V

    #@13
    .line 283
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mSystemReady:Z

    #@15
    if-eqz v0, :cond_1c

    #@17
    .line 284
    const/4 v0, 0x1

    #@18
    const/4 v1, 0x0

    #@19
    invoke-direct {p0, v0, v1}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    #@1c
    .line 287
    :cond_1c
    monitor-exit v2

    #@1d
    .line 288
    return-void

    #@1e
    :cond_1e
    move v0, v1

    #@1f
    .line 282
    goto :goto_10

    #@20
    .line 287
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_5 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method private updateLocked(II)V
    .registers 16
    .parameter "enableFlags"
    .parameter "disableFlags"

    #@0
    .prologue
    .line 348
    const/4 v9, 0x0

    #@1
    .line 349
    .local v9, action:Ljava/lang/String;
    const/4 v12, 0x0

    #@2
    .line 350
    .local v12, oldAction:Ljava/lang/String;
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@4
    const/4 v2, 0x2

    #@5
    if-ne v0, v2, :cond_76

    #@7
    .line 351
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->adjustStatusBarCarModeLocked()V

    #@a
    .line 352
    sget-object v12, Landroid/app/UiModeManager;->ACTION_EXIT_CAR_MODE:Ljava/lang/String;

    #@c
    .line 357
    :cond_c
    :goto_c
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@e
    if-eqz v0, :cond_81

    #@10
    .line 358
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@12
    const/4 v2, 0x2

    #@13
    if-eq v0, v2, :cond_2b

    #@15
    .line 359
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->adjustStatusBarCarModeLocked()V

    #@18
    .line 361
    if-eqz v12, :cond_26

    #@1a
    .line 362
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@1c
    new-instance v2, Landroid/content/Intent;

    #@1e
    invoke-direct {v2, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@21
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@23
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@26
    .line 364
    :cond_26
    const/4 v0, 0x2

    #@27
    iput v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@29
    .line 365
    sget-object v9, Landroid/app/UiModeManager;->ACTION_ENTER_CAR_MODE:Ljava/lang/String;

    #@2b
    .line 380
    :cond_2b
    :goto_2b
    if-eqz v9, :cond_ab

    #@2d
    .line 393
    new-instance v1, Landroid/content/Intent;

    #@2f
    invoke-direct {v1, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    .line 394
    .local v1, intent:Landroid/content/Intent;
    const-string v0, "enableFlags"

    #@34
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@37
    .line 395
    const-string v0, "disableFlags"

    #@39
    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3c
    .line 396
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@3e
    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@40
    const/4 v3, 0x0

    #@41
    iget-object v4, p0, Lcom/android/server/UiModeManagerService;->mResultReceiver:Landroid/content/BroadcastReceiver;

    #@43
    const/4 v5, 0x0

    #@44
    const/4 v6, -0x1

    #@45
    const/4 v7, 0x0

    #@46
    const/4 v8, 0x0

    #@47
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@4a
    .line 402
    const/4 v0, 0x1

    #@4b
    iput-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mHoldingConfiguration:Z

    #@4d
    .line 403
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->updateConfigurationLocked()V

    #@50
    .line 431
    .end local v1           #intent:Landroid/content/Intent;
    :goto_50
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCharging:Z

    #@52
    if-eqz v0, :cond_d0

    #@54
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@56
    if-eqz v0, :cond_5c

    #@58
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeKeepsScreenOn:Z

    #@5a
    if-nez v0, :cond_65

    #@5c
    :cond_5c
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mCurUiMode:I

    #@5e
    const/4 v2, 0x2

    #@5f
    if-ne v0, v2, :cond_d0

    #@61
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mDeskModeKeepsScreenOn:Z

    #@63
    if-eqz v0, :cond_d0

    #@65
    :cond_65
    const/4 v11, 0x1

    #@66
    .line 434
    .local v11, keepScreenOn:Z
    :goto_66
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@68
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@6b
    move-result v0

    #@6c
    if-eq v11, v0, :cond_75

    #@6e
    .line 435
    if-eqz v11, :cond_d2

    #@70
    .line 436
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@72
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@75
    .line 441
    :cond_75
    :goto_75
    return-void

    #@76
    .line 353
    .end local v11           #keepScreenOn:Z
    :cond_76
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@78
    invoke-static {v0}, Lcom/android/server/UiModeManagerService;->isDeskDockState(I)Z

    #@7b
    move-result v0

    #@7c
    if-eqz v0, :cond_c

    #@7e
    .line 354
    sget-object v12, Landroid/app/UiModeManager;->ACTION_EXIT_DESK_MODE:Ljava/lang/String;

    #@80
    goto :goto_c

    #@81
    .line 367
    :cond_81
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@83
    invoke-static {v0}, Lcom/android/server/UiModeManagerService;->isDeskDockState(I)Z

    #@86
    move-result v0

    #@87
    if-eqz v0, :cond_a6

    #@89
    .line 368
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@8b
    invoke-static {v0}, Lcom/android/server/UiModeManagerService;->isDeskDockState(I)Z

    #@8e
    move-result v0

    #@8f
    if-nez v0, :cond_2b

    #@91
    .line 369
    if-eqz v12, :cond_9f

    #@93
    .line 370
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@95
    new-instance v2, Landroid/content/Intent;

    #@97
    invoke-direct {v2, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9a
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@9c
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@9f
    .line 372
    :cond_9f
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@a1
    iput v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@a3
    .line 373
    sget-object v9, Landroid/app/UiModeManager;->ACTION_ENTER_DESK_MODE:Ljava/lang/String;

    #@a5
    goto :goto_2b

    #@a6
    .line 376
    :cond_a6
    const/4 v0, 0x0

    #@a7
    iput v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@a9
    .line 377
    move-object v9, v12

    #@aa
    goto :goto_2b

    #@ab
    .line 405
    :cond_ab
    const/4 v10, 0x0

    #@ac
    .line 406
    .local v10, category:Ljava/lang/String;
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@ae
    if-eqz v0, :cond_ba

    #@b0
    .line 407
    and-int/lit8 v0, p1, 0x1

    #@b2
    if-eqz v0, :cond_b6

    #@b4
    .line 409
    const-string v10, "android.intent.category.CAR_DOCK"

    #@b6
    .line 427
    :cond_b6
    :goto_b6
    invoke-direct {p0, v10}, Lcom/android/server/UiModeManagerService;->sendConfigurationAndStartDreamOrDockAppLocked(Ljava/lang/String;)V

    #@b9
    goto :goto_50

    #@ba
    .line 411
    :cond_ba
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@bc
    invoke-static {v0}, Lcom/android/server/UiModeManagerService;->isDeskDockState(I)Z

    #@bf
    move-result v0

    #@c0
    if-eqz v0, :cond_c9

    #@c2
    .line 412
    and-int/lit8 v0, p1, 0x1

    #@c4
    if-eqz v0, :cond_b6

    #@c6
    .line 414
    const-string v10, "android.intent.category.DESK_DOCK"

    #@c8
    goto :goto_b6

    #@c9
    .line 417
    :cond_c9
    and-int/lit8 v0, p2, 0x1

    #@cb
    if-eqz v0, :cond_b6

    #@cd
    .line 418
    const-string v10, "android.intent.category.HOME"

    #@cf
    goto :goto_b6

    #@d0
    .line 431
    .end local v10           #category:Ljava/lang/String;
    :cond_d0
    const/4 v11, 0x0

    #@d1
    goto :goto_66

    #@d2
    .line 438
    .restart local v11       #keepScreenOn:Z
    :cond_d2
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@d4
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@d7
    goto :goto_75
.end method

.method private updateTwilight()V
    .registers 4

    #@0
    .prologue
    .line 567
    iget-object v1, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 568
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->isDoingNightModeLocked()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_15

    #@9
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@b
    if-nez v0, :cond_15

    #@d
    .line 569
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->updateComputedNightModeLocked()V

    #@10
    .line 570
    const/4 v0, 0x0

    #@11
    const/4 v2, 0x0

    #@12
    invoke-direct {p0, v0, v2}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    #@15
    .line 572
    :cond_15
    monitor-exit v1

    #@16
    .line 573
    return-void

    #@17
    .line 572
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method


# virtual methods
.method public disableCarMode(I)V
    .registers 6
    .parameter "flags"

    #@0
    .prologue
    .line 186
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 188
    .local v0, ident:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@6
    monitor-enter v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_1b

    #@7
    .line 189
    const/4 v2, 0x0

    #@8
    :try_start_8
    invoke-direct {p0, v2}, Lcom/android/server/UiModeManagerService;->setCarModeLocked(Z)V

    #@b
    .line 190
    iget-boolean v2, p0, Lcom/android/server/UiModeManagerService;->mSystemReady:Z

    #@d
    if-eqz v2, :cond_13

    #@f
    .line 191
    const/4 v2, 0x0

    #@10
    invoke-direct {p0, v2, p1}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    #@13
    .line 193
    :cond_13
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_18

    #@14
    .line 195
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    .line 197
    return-void

    #@18
    .line 193
    :catchall_18
    move-exception v2

    #@19
    :try_start_19
    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    #@1a
    :try_start_1a
    throw v2
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_1b

    #@1b
    .line 195
    :catchall_1b
    move-exception v2

    #@1c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    throw v2
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 584
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_33

    #@a
    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "Permission Denial: can\'t dump uimode service from from pid="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", uid="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 607
    :goto_32
    return-void

    #@33
    .line 593
    :cond_33
    iget-object v1, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@35
    monitor-enter v1

    #@36
    .line 594
    :try_start_36
    const-string v0, "Current UI Mode Service state:"

    #@38
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 595
    const-string v0, "  mDockState="

    #@3d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@40
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@42
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@45
    .line 596
    const-string v0, " mLastBroadcastState="

    #@47
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mLastBroadcastState:I

    #@4c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@4f
    .line 597
    const-string v0, "  mNightMode="

    #@51
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@54
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@56
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@59
    .line 598
    const-string v0, " mCarModeEnabled="

    #@5b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5e
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@60
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@63
    .line 599
    const-string v0, " mComputedNightMode="

    #@65
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@68
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mComputedNightMode:Z

    #@6a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@6d
    .line 600
    const-string v0, "  mCurUiMode=0x"

    #@6f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@72
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mCurUiMode:I

    #@74
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@77
    move-result-object v0

    #@78
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7b
    .line 601
    const-string v0, " mSetUiMode=0x"

    #@7d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@80
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mSetUiMode:I

    #@82
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@85
    move-result-object v0

    #@86
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@89
    .line 602
    const-string v0, "  mHoldingConfiguration="

    #@8b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8e
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mHoldingConfiguration:Z

    #@90
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@93
    .line 603
    const-string v0, " mSystemReady="

    #@95
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@98
    iget-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mSystemReady:Z

    #@9a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@9d
    .line 604
    const-string v0, "  mTwilightService.getCurrentState()="

    #@9f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a2
    .line 605
    iget-object v0, p0, Lcom/android/server/UiModeManagerService;->mTwilightService:Lcom/android/server/TwilightService;

    #@a4
    invoke-virtual {v0}, Lcom/android/server/TwilightService;->getCurrentState()Lcom/android/server/TwilightService$TwilightState;

    #@a7
    move-result-object v0

    #@a8
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@ab
    .line 606
    monitor-exit v1

    #@ac
    goto :goto_32

    #@ad
    :catchall_ad
    move-exception v0

    #@ae
    monitor-exit v1
    :try_end_af
    .catchall {:try_start_36 .. :try_end_af} :catchall_ad

    #@af
    throw v0
.end method

.method public enableCarMode(I)V
    .registers 6
    .parameter "flags"

    #@0
    .prologue
    .line 201
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 203
    .local v0, ident:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@6
    monitor-enter v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_1b

    #@7
    .line 204
    const/4 v2, 0x1

    #@8
    :try_start_8
    invoke-direct {p0, v2}, Lcom/android/server/UiModeManagerService;->setCarModeLocked(Z)V

    #@b
    .line 205
    iget-boolean v2, p0, Lcom/android/server/UiModeManagerService;->mSystemReady:Z

    #@d
    if-eqz v2, :cond_13

    #@f
    .line 206
    const/4 v2, 0x0

    #@10
    invoke-direct {p0, p1, v2}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    #@13
    .line 208
    :cond_13
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_18

    #@14
    .line 210
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    .line 212
    return-void

    #@18
    .line 208
    :catchall_18
    move-exception v2

    #@19
    :try_start_19
    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    #@1a
    :try_start_1a
    throw v2
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_1b

    #@1b
    .line 210
    :catchall_1b
    move-exception v2

    #@1c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    throw v2
.end method

.method public getCurrentModeType()I
    .registers 5

    #@0
    .prologue
    .line 216
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 218
    .local v0, ident:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@6
    monitor-enter v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_13

    #@7
    .line 219
    :try_start_7
    iget v2, p0, Lcom/android/server/UiModeManagerService;->mCurUiMode:I

    #@9
    and-int/lit8 v2, v2, 0xf

    #@b
    monitor-exit v3
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_10

    #@c
    .line 222
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@f
    .line 219
    return v2

    #@10
    .line 220
    :catchall_10
    move-exception v2

    #@11
    :try_start_11
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    :try_start_12
    throw v2
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_13

    #@13
    .line 222
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method public getNightMode()I
    .registers 3

    #@0
    .prologue
    .line 254
    iget-object v1, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 255
    :try_start_3
    iget v0, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 256
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public setNightMode(I)V
    .registers 7
    .parameter "mode"

    #@0
    .prologue
    .line 228
    packed-switch p1, :pswitch_data_4c

    #@3
    .line 234
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "Unknown mode: "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v2

    #@1c
    .line 237
    :pswitch_1c
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1f
    move-result-wide v0

    #@20
    .line 239
    .local v0, ident:J
    :try_start_20
    iget-object v3, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@22
    monitor-enter v3
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_47

    #@23
    .line 240
    :try_start_23
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->isDoingNightModeLocked()Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_3f

    #@29
    iget v2, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@2b
    if-eq v2, p1, :cond_3f

    #@2d
    .line 241
    iget-object v2, p0, Lcom/android/server/UiModeManagerService;->mContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@32
    move-result-object v2

    #@33
    const-string v4, "ui_night_mode"

    #@35
    invoke-static {v2, v4, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@38
    .line 243
    iput p1, p0, Lcom/android/server/UiModeManagerService;->mNightMode:I

    #@3a
    .line 244
    const/4 v2, 0x0

    #@3b
    const/4 v4, 0x0

    #@3c
    invoke-direct {p0, v2, v4}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    #@3f
    .line 246
    :cond_3f
    monitor-exit v3
    :try_end_40
    .catchall {:try_start_23 .. :try_end_40} :catchall_44

    #@40
    .line 248
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@43
    .line 250
    return-void

    #@44
    .line 246
    :catchall_44
    move-exception v2

    #@45
    :try_start_45
    monitor-exit v3
    :try_end_46
    .catchall {:try_start_45 .. :try_end_46} :catchall_44

    #@46
    :try_start_46
    throw v2
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_47

    #@47
    .line 248
    :catchall_47
    move-exception v2

    #@48
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b
    throw v2

    #@4c
    .line 228
    :pswitch_data_4c
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
    .end packed-switch
.end method

.method systemReady()V
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 260
    iget-object v2, p0, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v2

    #@5
    .line 261
    const/4 v3, 0x1

    #@6
    :try_start_6
    iput-boolean v3, p0, Lcom/android/server/UiModeManagerService;->mSystemReady:Z

    #@8
    .line 262
    iget v3, p0, Lcom/android/server/UiModeManagerService;->mDockState:I

    #@a
    const/4 v4, 0x2

    #@b
    if-ne v3, v4, :cond_19

    #@d
    :goto_d
    iput-boolean v0, p0, Lcom/android/server/UiModeManagerService;->mCarModeEnabled:Z

    #@f
    .line 263
    invoke-direct {p0}, Lcom/android/server/UiModeManagerService;->updateComputedNightModeLocked()V

    #@12
    .line 264
    const/4 v0, 0x0

    #@13
    const/4 v1, 0x0

    #@14
    invoke-direct {p0, v0, v1}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    #@17
    .line 265
    monitor-exit v2

    #@18
    .line 266
    return-void

    #@19
    :cond_19
    move v0, v1

    #@1a
    .line 262
    goto :goto_d

    #@1b
    .line 265
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_6 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method
