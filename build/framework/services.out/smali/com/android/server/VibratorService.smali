.class public Lcom/android/server/VibratorService;
.super Landroid/os/IVibratorService$Stub;
.source "VibratorService.java"

# interfaces
.implements Landroid/hardware/input/InputManager$InputDeviceListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/VibratorService$VibrateThread;,
        Lcom/android/server/VibratorService$Vibration;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VibratorService"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

.field private final mH:Landroid/os/Handler;

.field private mIm:Landroid/hardware/input/InputManager;

.field private mInputDeviceListenerRegistered:Z

.field private final mInputDeviceVibrators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation
.end field

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field volatile mThread:Lcom/android/server/VibratorService$VibrateThread;

.field private final mTmpWorkSource:Landroid/os/WorkSource;

.field private mVibrateInputDevicesSetting:Z

.field private final mVibrationRunnable:Ljava/lang/Runnable;

.field private final mVibrations:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/server/VibratorService$Vibration;",
            ">;"
        }
    .end annotation
.end field

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 123
    invoke-direct {p0}, Landroid/os/IVibratorService$Stub;-><init>()V

    #@4
    .line 52
    new-instance v2, Landroid/os/WorkSource;

    #@6
    invoke-direct {v2}, Landroid/os/WorkSource;-><init>()V

    #@9
    iput-object v2, p0, Lcom/android/server/VibratorService;->mTmpWorkSource:Landroid/os/WorkSource;

    #@b
    .line 53
    new-instance v2, Landroid/os/Handler;

    #@d
    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    #@10
    iput-object v2, p0, Lcom/android/server/VibratorService;->mH:Landroid/os/Handler;

    #@12
    .line 63
    new-instance v2, Ljava/util/ArrayList;

    #@14
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@17
    iput-object v2, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@19
    .line 274
    new-instance v2, Lcom/android/server/VibratorService$3;

    #@1b
    invoke-direct {v2, p0}, Lcom/android/server/VibratorService$3;-><init>(Lcom/android/server/VibratorService;)V

    #@1e
    iput-object v2, p0, Lcom/android/server/VibratorService;->mVibrationRunnable:Ljava/lang/Runnable;

    #@20
    .line 524
    new-instance v2, Lcom/android/server/VibratorService$4;

    #@22
    invoke-direct {v2, p0}, Lcom/android/server/VibratorService$4;-><init>(Lcom/android/server/VibratorService;)V

    #@25
    iput-object v2, p0, Lcom/android/server/VibratorService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@27
    .line 126
    invoke-static {}, Lcom/android/server/VibratorService;->vibratorOff()V

    #@2a
    .line 128
    iput-object p1, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@2c
    .line 129
    const-string v2, "power"

    #@2e
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Landroid/os/PowerManager;

    #@34
    .line 131
    .local v1, pm:Landroid/os/PowerManager;
    const-string v2, "*vibrator*"

    #@36
    invoke-virtual {v1, v3, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@39
    move-result-object v2

    #@3a
    iput-object v2, p0, Lcom/android/server/VibratorService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3c
    .line 132
    iget-object v2, p0, Lcom/android/server/VibratorService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3e
    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@41
    .line 134
    new-instance v2, Ljava/util/LinkedList;

    #@43
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    #@46
    iput-object v2, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@48
    .line 136
    new-instance v0, Landroid/content/IntentFilter;

    #@4a
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@4d
    .line 137
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_OFF"

    #@4f
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@52
    .line 138
    iget-object v2, p0, Lcom/android/server/VibratorService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@54
    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@57
    .line 139
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/VibratorService;)Lcom/android/server/VibratorService$Vibration;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/server/VibratorService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/VibratorService;J)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/android/server/VibratorService;->doVibratorOn(J)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/VibratorService;Lcom/android/server/VibratorService$Vibration;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/android/server/VibratorService;->unlinkVibration(Lcom/android/server/VibratorService$Vibration;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/VibratorService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/server/VibratorService;->doCancelVibrateLocked()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/VibratorService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/server/VibratorService;->startNextVibrationLocked()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/VibratorService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/server/VibratorService;->updateInputDeviceVibrators()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/VibratorService;)Landroid/os/WorkSource;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/server/VibratorService;->mTmpWorkSource:Landroid/os/WorkSource;

    #@2
    return-object v0
.end method

.method private doCancelVibrateLocked()V
    .registers 4

    #@0
    .prologue
    .line 285
    iget-object v0, p0, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@2
    if-eqz v0, :cond_15

    #@4
    .line 286
    iget-object v1, p0, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@6
    monitor-enter v1

    #@7
    .line 287
    :try_start_7
    iget-object v0, p0, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@9
    const/4 v2, 0x1

    #@a
    iput-boolean v2, v0, Lcom/android/server/VibratorService$VibrateThread;->mDone:Z

    #@c
    .line 288
    iget-object v0, p0, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@e
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@11
    .line 289
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_7 .. :try_end_12} :catchall_20

    #@12
    .line 290
    const/4 v0, 0x0

    #@13
    iput-object v0, p0, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@15
    .line 292
    :cond_15
    invoke-direct {p0}, Lcom/android/server/VibratorService;->doVibratorOff()V

    #@18
    .line 293
    iget-object v0, p0, Lcom/android/server/VibratorService;->mH:Landroid/os/Handler;

    #@1a
    iget-object v1, p0, Lcom/android/server/VibratorService;->mVibrationRunnable:Ljava/lang/Runnable;

    #@1c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@1f
    .line 294
    return-void

    #@20
    .line 289
    :catchall_20
    move-exception v0

    #@21
    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method private doVibratorExists()Z
    .registers 2

    #@0
    .prologue
    .line 413
    invoke-static {}, Lcom/android/server/VibratorService;->vibratorExists()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private doVibratorOff()V
    .registers 5

    #@0
    .prologue
    .line 430
    iget-object v3, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 431
    :try_start_3
    iget-object v2, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    .line 432
    .local v1, vibratorCount:I
    if-eqz v1, :cond_1c

    #@b
    .line 433
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    if-ge v0, v1, :cond_1f

    #@e
    .line 434
    iget-object v2, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/os/Vibrator;

    #@16
    invoke-virtual {v2}, Landroid/os/Vibrator;->cancel()V

    #@19
    .line 433
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_c

    #@1c
    .line 437
    .end local v0           #i:I
    :cond_1c
    invoke-static {}, Lcom/android/server/VibratorService;->vibratorOff()V

    #@1f
    .line 439
    :cond_1f
    monitor-exit v3

    #@20
    .line 440
    return-void

    #@21
    .line 439
    .end local v1           #vibratorCount:I
    :catchall_21
    move-exception v2

    #@22
    monitor-exit v3
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v2
.end method

.method private doVibratorOn(J)V
    .registers 7
    .parameter "millis"

    #@0
    .prologue
    .line 417
    iget-object v3, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 418
    :try_start_3
    iget-object v2, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    .line 419
    .local v1, vibratorCount:I
    if-eqz v1, :cond_1c

    #@b
    .line 420
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    if-ge v0, v1, :cond_1f

    #@e
    .line 421
    iget-object v2, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/os/Vibrator;

    #@16
    invoke-virtual {v2, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V

    #@19
    .line 420
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_c

    #@1c
    .line 424
    .end local v0           #i:I
    :cond_1c
    invoke-static {p1, p2}, Lcom/android/server/VibratorService;->vibratorOn(J)V

    #@1f
    .line 426
    :cond_1f
    monitor-exit v3

    #@20
    .line 427
    return-void

    #@21
    .line 426
    .end local v1           #vibratorCount:I
    :catchall_21
    move-exception v2

    #@22
    monitor-exit v3
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v2
.end method

.method private isAll0([J)Z
    .registers 8
    .parameter "pattern"

    #@0
    .prologue
    .line 193
    array-length v0, p1

    #@1
    .line 194
    .local v0, N:I
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    if-ge v1, v0, :cond_11

    #@4
    .line 195
    aget-wide v2, p1, v1

    #@6
    const-wide/16 v4, 0x0

    #@8
    cmp-long v2, v2, v4

    #@a
    if-eqz v2, :cond_e

    #@c
    .line 196
    const/4 v2, 0x0

    #@d
    .line 199
    :goto_d
    return v2

    #@e
    .line 194
    :cond_e
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_2

    #@11
    .line 199
    :cond_11
    const/4 v2, 0x1

    #@12
    goto :goto_d
.end method

.method private removeVibrationLocked(Landroid/os/IBinder;)Lcom/android/server/VibratorService$Vibration;
    .registers 6
    .parameter "token"

    #@0
    .prologue
    .line 321
    iget-object v2, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@2
    const/4 v3, 0x0

    #@3
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    #@6
    move-result-object v0

    #@7
    .line 322
    .local v0, iter:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Lcom/android/server/VibratorService$Vibration;>;"
    :cond_7
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_20

    #@d
    .line 323
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/android/server/VibratorService$Vibration;

    #@13
    .line 324
    .local v1, vib:Lcom/android/server/VibratorService$Vibration;
    invoke-static {v1}, Lcom/android/server/VibratorService$Vibration;->access$600(Lcom/android/server/VibratorService$Vibration;)Landroid/os/IBinder;

    #@16
    move-result-object v2

    #@17
    if-ne v2, p1, :cond_7

    #@19
    .line 325
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    #@1c
    .line 326
    invoke-direct {p0, v1}, Lcom/android/server/VibratorService;->unlinkVibration(Lcom/android/server/VibratorService$Vibration;)V

    #@1f
    .line 336
    .end local v1           #vib:Lcom/android/server/VibratorService$Vibration;
    :goto_1f
    return-object v1

    #@20
    .line 332
    :cond_20
    iget-object v2, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@22
    if-eqz v2, :cond_34

    #@24
    iget-object v2, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@26
    invoke-static {v2}, Lcom/android/server/VibratorService$Vibration;->access$600(Lcom/android/server/VibratorService$Vibration;)Landroid/os/IBinder;

    #@29
    move-result-object v2

    #@2a
    if-ne v2, p1, :cond_34

    #@2c
    .line 333
    iget-object v2, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@2e
    invoke-direct {p0, v2}, Lcom/android/server/VibratorService;->unlinkVibration(Lcom/android/server/VibratorService$Vibration;)V

    #@31
    .line 334
    iget-object v1, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@33
    goto :goto_1f

    #@34
    .line 336
    :cond_34
    const/4 v1, 0x0

    #@35
    goto :goto_1f
.end method

.method private startNextVibrationLocked()V
    .registers 2

    #@0
    .prologue
    .line 298
    iget-object v0, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@5
    move-result v0

    #@6
    if-gtz v0, :cond_c

    #@8
    .line 299
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@b
    .line 304
    :goto_b
    return-void

    #@c
    .line 302
    :cond_c
    iget-object v0, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@e
    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/server/VibratorService$Vibration;

    #@14
    iput-object v0, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@16
    .line 303
    iget-object v0, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@18
    invoke-direct {p0, v0}, Lcom/android/server/VibratorService;->startVibrationLocked(Lcom/android/server/VibratorService$Vibration;)V

    #@1b
    goto :goto_b
.end method

.method private startVibrationLocked(Lcom/android/server/VibratorService$Vibration;)V
    .registers 6
    .parameter "vib"

    #@0
    .prologue
    .line 308
    invoke-static {p1}, Lcom/android/server/VibratorService$Vibration;->access$500(Lcom/android/server/VibratorService$Vibration;)J

    #@3
    move-result-wide v0

    #@4
    const-wide/16 v2, 0x0

    #@6
    cmp-long v0, v0, v2

    #@8
    if-eqz v0, :cond_1d

    #@a
    .line 309
    invoke-static {p1}, Lcom/android/server/VibratorService$Vibration;->access$500(Lcom/android/server/VibratorService$Vibration;)J

    #@d
    move-result-wide v0

    #@e
    invoke-direct {p0, v0, v1}, Lcom/android/server/VibratorService;->doVibratorOn(J)V

    #@11
    .line 310
    iget-object v0, p0, Lcom/android/server/VibratorService;->mH:Landroid/os/Handler;

    #@13
    iget-object v1, p0, Lcom/android/server/VibratorService;->mVibrationRunnable:Ljava/lang/Runnable;

    #@15
    invoke-static {p1}, Lcom/android/server/VibratorService$Vibration;->access$500(Lcom/android/server/VibratorService$Vibration;)J

    #@18
    move-result-wide v2

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1c
    .line 317
    :goto_1c
    return-void

    #@1d
    .line 314
    :cond_1d
    new-instance v0, Lcom/android/server/VibratorService$VibrateThread;

    #@1f
    invoke-direct {v0, p0, p1}, Lcom/android/server/VibratorService$VibrateThread;-><init>(Lcom/android/server/VibratorService;Lcom/android/server/VibratorService$Vibration;)V

    #@22
    iput-object v0, p0, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@24
    .line 315
    iget-object v0, p0, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@26
    invoke-virtual {v0}, Lcom/android/server/VibratorService$VibrateThread;->start()V

    #@29
    goto :goto_1c
.end method

.method private unlinkVibration(Lcom/android/server/VibratorService$Vibration;)V
    .registers 4
    .parameter "vib"

    #@0
    .prologue
    .line 340
    invoke-static {p1}, Lcom/android/server/VibratorService$Vibration;->access$700(Lcom/android/server/VibratorService$Vibration;)[J

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 343
    invoke-static {p1}, Lcom/android/server/VibratorService$Vibration;->access$600(Lcom/android/server/VibratorService$Vibration;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x0

    #@b
    invoke-interface {v0, p1, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@e
    .line 345
    :cond_e
    return-void
.end method

.method private updateInputDeviceVibrators()V
    .registers 12

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 348
    iget-object v6, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@4
    monitor-enter v6

    #@5
    .line 349
    :try_start_5
    invoke-direct {p0}, Lcom/android/server/VibratorService;->doCancelVibrateLocked()V

    #@8
    .line 351
    iget-object v7, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@a
    monitor-enter v7
    :try_end_b
    .catchall {:try_start_5 .. :try_end_b} :catchall_70

    #@b
    .line 352
    const/4 v8, 0x0

    #@c
    :try_start_c
    iput-boolean v8, p0, Lcom/android/server/VibratorService;->mVibrateInputDevicesSetting:Z
    :try_end_e
    .catchall {:try_start_c .. :try_end_e} :catchall_6d

    #@e
    .line 354
    :try_start_e
    iget-object v8, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v8

    #@14
    const-string v9, "vibrate_input_devices"

    #@16
    const/4 v10, -0x2

    #@17
    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1a
    move-result v8

    #@1b
    if-lez v8, :cond_5e

    #@1d
    :goto_1d
    iput-boolean v4, p0, Lcom/android/server/VibratorService;->mVibrateInputDevicesSetting:Z
    :try_end_1f
    .catchall {:try_start_e .. :try_end_1f} :catchall_6d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_e .. :try_end_1f} :catch_79

    #@1f
    .line 360
    :goto_1f
    :try_start_1f
    iget-boolean v4, p0, Lcom/android/server/VibratorService;->mVibrateInputDevicesSetting:Z

    #@21
    if-eqz v4, :cond_60

    #@23
    .line 361
    iget-boolean v4, p0, Lcom/android/server/VibratorService;->mInputDeviceListenerRegistered:Z

    #@25
    if-nez v4, :cond_31

    #@27
    .line 362
    const/4 v4, 0x1

    #@28
    iput-boolean v4, p0, Lcom/android/server/VibratorService;->mInputDeviceListenerRegistered:Z

    #@2a
    .line 363
    iget-object v4, p0, Lcom/android/server/VibratorService;->mIm:Landroid/hardware/input/InputManager;

    #@2c
    iget-object v5, p0, Lcom/android/server/VibratorService;->mH:Landroid/os/Handler;

    #@2e
    invoke-virtual {v4, p0, v5}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    #@31
    .line 372
    :cond_31
    :goto_31
    iget-object v4, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@36
    .line 373
    iget-boolean v4, p0, Lcom/android/server/VibratorService;->mVibrateInputDevicesSetting:Z

    #@38
    if-eqz v4, :cond_73

    #@3a
    .line 374
    iget-object v4, p0, Lcom/android/server/VibratorService;->mIm:Landroid/hardware/input/InputManager;

    #@3c
    invoke-virtual {v4}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    #@3f
    move-result-object v2

    #@40
    .line 375
    .local v2, ids:[I
    const/4 v1, 0x0

    #@41
    .local v1, i:I
    :goto_41
    array-length v4, v2

    #@42
    if-ge v1, v4, :cond_73

    #@44
    .line 376
    iget-object v4, p0, Lcom/android/server/VibratorService;->mIm:Landroid/hardware/input/InputManager;

    #@46
    aget v5, v2, v1

    #@48
    invoke-virtual {v4, v5}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    #@4b
    move-result-object v0

    #@4c
    .line 377
    .local v0, device:Landroid/view/InputDevice;
    invoke-virtual {v0}, Landroid/view/InputDevice;->getVibrator()Landroid/os/Vibrator;

    #@4f
    move-result-object v3

    #@50
    .line 378
    .local v3, vibrator:Landroid/os/Vibrator;
    invoke-virtual {v3}, Landroid/os/Vibrator;->hasVibrator()Z

    #@53
    move-result v4

    #@54
    if-eqz v4, :cond_5b

    #@56
    .line 379
    iget-object v4, p0, Lcom/android/server/VibratorService;->mInputDeviceVibrators:Ljava/util/ArrayList;

    #@58
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5b
    .line 375
    :cond_5b
    add-int/lit8 v1, v1, 0x1

    #@5d
    goto :goto_41

    #@5e
    .end local v0           #device:Landroid/view/InputDevice;
    .end local v1           #i:I
    .end local v2           #ids:[I
    .end local v3           #vibrator:Landroid/os/Vibrator;
    :cond_5e
    move v4, v5

    #@5f
    .line 354
    goto :goto_1d

    #@60
    .line 366
    :cond_60
    iget-boolean v4, p0, Lcom/android/server/VibratorService;->mInputDeviceListenerRegistered:Z

    #@62
    if-eqz v4, :cond_31

    #@64
    .line 367
    const/4 v4, 0x0

    #@65
    iput-boolean v4, p0, Lcom/android/server/VibratorService;->mInputDeviceListenerRegistered:Z

    #@67
    .line 368
    iget-object v4, p0, Lcom/android/server/VibratorService;->mIm:Landroid/hardware/input/InputManager;

    #@69
    invoke-virtual {v4, p0}, Landroid/hardware/input/InputManager;->unregisterInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;)V

    #@6c
    goto :goto_31

    #@6d
    .line 383
    :catchall_6d
    move-exception v4

    #@6e
    monitor-exit v7
    :try_end_6f
    .catchall {:try_start_1f .. :try_end_6f} :catchall_6d

    #@6f
    :try_start_6f
    throw v4

    #@70
    .line 386
    :catchall_70
    move-exception v4

    #@71
    monitor-exit v6
    :try_end_72
    .catchall {:try_start_6f .. :try_end_72} :catchall_70

    #@72
    throw v4

    #@73
    .line 383
    :cond_73
    :try_start_73
    monitor-exit v7
    :try_end_74
    .catchall {:try_start_73 .. :try_end_74} :catchall_6d

    #@74
    .line 385
    :try_start_74
    invoke-direct {p0}, Lcom/android/server/VibratorService;->startNextVibrationLocked()V

    #@77
    .line 386
    monitor-exit v6
    :try_end_78
    .catchall {:try_start_74 .. :try_end_78} :catchall_70

    #@78
    .line 387
    return-void

    #@79
    .line 357
    :catch_79
    move-exception v4

    #@7a
    goto :goto_1f
.end method

.method static native vibratorExists()Z
.end method

.method static native vibratorOff()V
.end method

.method static native vibratorOn(J)V
.end method


# virtual methods
.method public cancelVibrate(Landroid/os/IBinder;)V
    .registers 8
    .parameter "token"

    #@0
    .prologue
    .line 254
    iget-object v3, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.VIBRATE"

    #@4
    const-string v5, "cancelVibrate"

    #@6
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 259
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v0

    #@d
    .line 261
    .local v0, identity:J
    :try_start_d
    iget-object v4, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@f
    monitor-enter v4
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_26

    #@10
    .line 262
    :try_start_10
    invoke-direct {p0, p1}, Lcom/android/server/VibratorService;->removeVibrationLocked(Landroid/os/IBinder;)Lcom/android/server/VibratorService$Vibration;

    #@13
    move-result-object v2

    #@14
    .line 263
    .local v2, vib:Lcom/android/server/VibratorService$Vibration;
    iget-object v3, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@16
    if-ne v2, v3, :cond_1e

    #@18
    .line 264
    invoke-direct {p0}, Lcom/android/server/VibratorService;->doCancelVibrateLocked()V

    #@1b
    .line 265
    invoke-direct {p0}, Lcom/android/server/VibratorService;->startNextVibrationLocked()V

    #@1e
    .line 267
    :cond_1e
    monitor-exit v4
    :try_end_1f
    .catchall {:try_start_10 .. :try_end_1f} :catchall_23

    #@1f
    .line 270
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    .line 272
    return-void

    #@23
    .line 267
    .end local v2           #vib:Lcom/android/server/VibratorService$Vibration;
    :catchall_23
    move-exception v3

    #@24
    :try_start_24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    #@25
    :try_start_25
    throw v3
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_26

    #@26
    .line 270
    :catchall_26
    move-exception v3

    #@27
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2a
    throw v3
.end method

.method public hasVibrator()Z
    .registers 2

    #@0
    .prologue
    .line 164
    invoke-direct {p0}, Lcom/android/server/VibratorService;->doVibratorExists()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onInputDeviceAdded(I)V
    .registers 2
    .parameter "deviceId"

    #@0
    .prologue
    .line 391
    invoke-direct {p0}, Lcom/android/server/VibratorService;->updateInputDeviceVibrators()V

    #@3
    .line 392
    return-void
.end method

.method public onInputDeviceChanged(I)V
    .registers 2
    .parameter "deviceId"

    #@0
    .prologue
    .line 396
    invoke-direct {p0}, Lcom/android/server/VibratorService;->updateInputDeviceVibrators()V

    #@3
    .line 397
    return-void
.end method

.method public onInputDeviceRemoved(I)V
    .registers 2
    .parameter "deviceId"

    #@0
    .prologue
    .line 401
    invoke-direct {p0}, Lcom/android/server/VibratorService;->updateInputDeviceVibrators()V

    #@3
    .line 402
    return-void
.end method

.method public systemReady()V
    .registers 6

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "input"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/hardware/input/InputManager;

    #@a
    iput-object v0, p0, Lcom/android/server/VibratorService;->mIm:Landroid/hardware/input/InputManager;

    #@c
    .line 144
    iget-object v0, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v0

    #@12
    const-string v1, "vibrate_input_devices"

    #@14
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@17
    move-result-object v1

    #@18
    const/4 v2, 0x1

    #@19
    new-instance v3, Lcom/android/server/VibratorService$1;

    #@1b
    iget-object v4, p0, Lcom/android/server/VibratorService;->mH:Landroid/os/Handler;

    #@1d
    invoke-direct {v3, p0, v4}, Lcom/android/server/VibratorService$1;-><init>(Lcom/android/server/VibratorService;Landroid/os/Handler;)V

    #@20
    const/4 v4, -0x1

    #@21
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@24
    .line 153
    iget-object v0, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@26
    new-instance v1, Lcom/android/server/VibratorService$2;

    #@28
    invoke-direct {v1, p0}, Lcom/android/server/VibratorService$2;-><init>(Lcom/android/server/VibratorService;)V

    #@2b
    new-instance v2, Landroid/content/IntentFilter;

    #@2d
    const-string v3, "android.intent.action.USER_SWITCHED"

    #@2f
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@32
    const/4 v3, 0x0

    #@33
    iget-object v4, p0, Lcom/android/server/VibratorService;->mH:Landroid/os/Handler;

    #@35
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@38
    .line 160
    invoke-direct {p0}, Lcom/android/server/VibratorService;->updateInputDeviceVibrators()V

    #@3b
    .line 161
    return-void
.end method

.method public vibrate(JLandroid/os/IBinder;)V
    .registers 10
    .parameter "milliseconds"
    .parameter "token"

    #@0
    .prologue
    .line 168
    iget-object v1, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.VIBRATE"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_12

    #@a
    .line 170
    new-instance v1, Ljava/lang/SecurityException;

    #@c
    const-string v2, "Requires VIBRATE permission"

    #@e
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 172
    :cond_12
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@15
    move-result v5

    #@16
    .line 176
    .local v5, uid:I
    const-wide/16 v1, 0x0

    #@18
    cmp-long v1, p1, v1

    #@1a
    if-lez v1, :cond_28

    #@1c
    iget-object v1, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@1e
    if-eqz v1, :cond_29

    #@20
    iget-object v1, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@22
    invoke-virtual {v1, p1, p2}, Lcom/android/server/VibratorService$Vibration;->hasLongerTimeout(J)Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_29

    #@28
    .line 190
    :cond_28
    :goto_28
    return-void

    #@29
    .line 183
    :cond_29
    new-instance v0, Lcom/android/server/VibratorService$Vibration;

    #@2b
    move-object v1, p0

    #@2c
    move-object v2, p3

    #@2d
    move-wide v3, p1

    #@2e
    invoke-direct/range {v0 .. v5}, Lcom/android/server/VibratorService$Vibration;-><init>(Lcom/android/server/VibratorService;Landroid/os/IBinder;JI)V

    #@31
    .line 184
    .local v0, vib:Lcom/android/server/VibratorService$Vibration;
    iget-object v2, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@33
    monitor-enter v2

    #@34
    .line 185
    :try_start_34
    invoke-direct {p0, p3}, Lcom/android/server/VibratorService;->removeVibrationLocked(Landroid/os/IBinder;)Lcom/android/server/VibratorService$Vibration;

    #@37
    .line 186
    invoke-direct {p0}, Lcom/android/server/VibratorService;->doCancelVibrateLocked()V

    #@3a
    .line 187
    iput-object v0, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@3c
    .line 188
    invoke-direct {p0, v0}, Lcom/android/server/VibratorService;->startVibrationLocked(Lcom/android/server/VibratorService$Vibration;)V

    #@3f
    .line 189
    monitor-exit v2

    #@40
    goto :goto_28

    #@41
    :catchall_41
    move-exception v1

    #@42
    monitor-exit v2
    :try_end_43
    .catchall {:try_start_34 .. :try_end_43} :catchall_41

    #@43
    throw v1
.end method

.method public vibratePattern([JILandroid/os/IBinder;)V
    .registers 13
    .parameter "pattern"
    .parameter "repeat"
    .parameter "token"

    #@0
    .prologue
    .line 203
    iget-object v1, p0, Lcom/android/server/VibratorService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.VIBRATE"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_12

    #@a
    .line 205
    new-instance v1, Ljava/lang/SecurityException;

    #@c
    const-string v2, "Requires VIBRATE permission"

    #@e
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 207
    :cond_12
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@15
    move-result v5

    #@16
    .line 209
    .local v5, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@19
    move-result-wide v7

    #@1a
    .line 221
    .local v7, identity:J
    if-eqz p1, :cond_2a

    #@1c
    :try_start_1c
    array-length v1, p1

    #@1d
    if-eqz v1, :cond_2a

    #@1f
    invoke-direct {p0, p1}, Lcom/android/server/VibratorService;->isAll0([J)Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_2a

    #@25
    array-length v1, p1
    :try_end_26
    .catchall {:try_start_1c .. :try_end_26} :catchall_53

    #@26
    if-ge p2, v1, :cond_2a

    #@28
    if-nez p3, :cond_2e

    #@2a
    .line 249
    :cond_2a
    :goto_2a
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2d
    .line 251
    return-void

    #@2e
    .line 227
    :cond_2e
    :try_start_2e
    new-instance v0, Lcom/android/server/VibratorService$Vibration;

    #@30
    move-object v1, p0

    #@31
    move-object v2, p3

    #@32
    move-object v3, p1

    #@33
    move v4, p2

    #@34
    invoke-direct/range {v0 .. v5}, Lcom/android/server/VibratorService$Vibration;-><init>(Lcom/android/server/VibratorService;Landroid/os/IBinder;[JII)V
    :try_end_37
    .catchall {:try_start_2e .. :try_end_37} :catchall_53

    #@37
    .line 229
    .local v0, vib:Lcom/android/server/VibratorService$Vibration;
    const/4 v1, 0x0

    #@38
    :try_start_38
    invoke-interface {p3, v0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_53
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_3b} :catch_58

    #@3b
    .line 234
    :try_start_3b
    iget-object v2, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@3d
    monitor-enter v2
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_53

    #@3e
    .line 235
    :try_start_3e
    invoke-direct {p0, p3}, Lcom/android/server/VibratorService;->removeVibrationLocked(Landroid/os/IBinder;)Lcom/android/server/VibratorService$Vibration;

    #@41
    .line 236
    invoke-direct {p0}, Lcom/android/server/VibratorService;->doCancelVibrateLocked()V

    #@44
    .line 237
    if-ltz p2, :cond_5a

    #@46
    .line 238
    iget-object v1, p0, Lcom/android/server/VibratorService;->mVibrations:Ljava/util/LinkedList;

    #@48
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    #@4b
    .line 239
    invoke-direct {p0}, Lcom/android/server/VibratorService;->startNextVibrationLocked()V

    #@4e
    .line 246
    :goto_4e
    monitor-exit v2

    #@4f
    goto :goto_2a

    #@50
    :catchall_50
    move-exception v1

    #@51
    monitor-exit v2
    :try_end_52
    .catchall {:try_start_3e .. :try_end_52} :catchall_50

    #@52
    :try_start_52
    throw v1
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_53

    #@53
    .line 249
    .end local v0           #vib:Lcom/android/server/VibratorService$Vibration;
    :catchall_53
    move-exception v1

    #@54
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@57
    throw v1

    #@58
    .line 230
    .restart local v0       #vib:Lcom/android/server/VibratorService$Vibration;
    :catch_58
    move-exception v6

    #@59
    .local v6, e:Landroid/os/RemoteException;
    goto :goto_2a

    #@5a
    .line 243
    .end local v6           #e:Landroid/os/RemoteException;
    :cond_5a
    :try_start_5a
    iput-object v0, p0, Lcom/android/server/VibratorService;->mCurrentVibration:Lcom/android/server/VibratorService$Vibration;

    #@5c
    .line 244
    invoke-direct {p0, v0}, Lcom/android/server/VibratorService;->startVibrationLocked(Lcom/android/server/VibratorService$Vibration;)V
    :try_end_5f
    .catchall {:try_start_5a .. :try_end_5f} :catchall_50

    #@5f
    goto :goto_4e
.end method
