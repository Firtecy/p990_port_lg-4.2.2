.class Lcom/android/server/AlarmManagerService$AlarmThread;
.super Ljava/lang/Thread;
.source "AlarmManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AlarmManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlarmThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/AlarmManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/AlarmManagerService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 835
    iput-object p1, p0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@2
    .line 836
    const-string v0, "AlarmManager"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 837
    return-void
.end method


# virtual methods
.method public run()V
    .registers 25

    #@0
    .prologue
    .line 843
    :goto_0
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@4
    move-object/from16 v0, p0

    #@6
    iget-object v4, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@8
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$100(Lcom/android/server/AlarmManagerService;)I

    #@b
    move-result v4

    #@c
    invoke-static {v3, v4}, Lcom/android/server/AlarmManagerService;->access$200(Lcom/android/server/AlarmManagerService;I)I

    #@f
    move-result v21

    #@10
    .line 845
    .local v21, result:I
    new-instance v22, Ljava/util/ArrayList;

    #@12
    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    #@15
    .line 847
    .local v22, triggerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    const/high16 v3, 0x1

    #@17
    and-int v3, v3, v21

    #@19
    if-eqz v3, :cond_4e

    #@1b
    .line 848
    move-object/from16 v0, p0

    #@1d
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@1f
    move-object/from16 v0, p0

    #@21
    iget-object v4, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@23
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$300(Lcom/android/server/AlarmManagerService;)Landroid/app/PendingIntent;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v3, v4}, Lcom/android/server/AlarmManagerService;->remove(Landroid/app/PendingIntent;)V

    #@2a
    .line 849
    move-object/from16 v0, p0

    #@2c
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@2e
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$400(Lcom/android/server/AlarmManagerService;)Lcom/android/server/AlarmManagerService$ClockReceiver;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Lcom/android/server/AlarmManagerService$ClockReceiver;->scheduleTimeTickEvent()V

    #@35
    .line 850
    new-instance v14, Landroid/content/Intent;

    #@37
    const-string v3, "android.intent.action.TIME_SET"

    #@39
    invoke-direct {v14, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3c
    .line 851
    .local v14, intent:Landroid/content/Intent;
    const/high16 v3, 0x2800

    #@3e
    invoke-virtual {v14, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@41
    .line 853
    move-object/from16 v0, p0

    #@43
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@45
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;

    #@48
    move-result-object v3

    #@49
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@4b
    invoke-virtual {v3, v14, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@4e
    .line 856
    .end local v14           #intent:Landroid/content/Intent;
    :cond_4e
    move-object/from16 v0, p0

    #@50
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@52
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$600(Lcom/android/server/AlarmManagerService;)Ljava/lang/Object;

    #@55
    move-result-object v23

    #@56
    monitor-enter v23

    #@57
    .line 857
    :try_start_57
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5a
    move-result-wide v19

    #@5b
    .line 858
    .local v19, nowRTC:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5e
    move-result-wide v17

    #@5f
    .line 863
    .local v17, nowELAPSED:J
    and-int/lit8 v3, v21, 0x1

    #@61
    if-eqz v3, :cond_76

    #@63
    .line 864
    move-object/from16 v0, p0

    #@65
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@67
    move-object/from16 v0, p0

    #@69
    iget-object v4, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@6b
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@6e
    move-result-object v4

    #@6f
    move-object/from16 v0, v22

    #@71
    move-wide/from16 v1, v19

    #@73
    invoke-static {v3, v4, v0, v1, v2}, Lcom/android/server/AlarmManagerService;->access$800(Lcom/android/server/AlarmManagerService;Ljava/util/ArrayList;Ljava/util/ArrayList;J)V

    #@76
    .line 866
    :cond_76
    and-int/lit8 v3, v21, 0x2

    #@78
    if-eqz v3, :cond_8d

    #@7a
    .line 867
    move-object/from16 v0, p0

    #@7c
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-object v4, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@82
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$900(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@85
    move-result-object v4

    #@86
    move-object/from16 v0, v22

    #@88
    move-wide/from16 v1, v19

    #@8a
    invoke-static {v3, v4, v0, v1, v2}, Lcom/android/server/AlarmManagerService;->access$800(Lcom/android/server/AlarmManagerService;Ljava/util/ArrayList;Ljava/util/ArrayList;J)V

    #@8d
    .line 869
    :cond_8d
    and-int/lit8 v3, v21, 0x4

    #@8f
    if-eqz v3, :cond_a4

    #@91
    .line 870
    move-object/from16 v0, p0

    #@93
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@95
    move-object/from16 v0, p0

    #@97
    iget-object v4, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@99
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$1000(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@9c
    move-result-object v4

    #@9d
    move-object/from16 v0, v22

    #@9f
    move-wide/from16 v1, v17

    #@a1
    invoke-static {v3, v4, v0, v1, v2}, Lcom/android/server/AlarmManagerService;->access$800(Lcom/android/server/AlarmManagerService;Ljava/util/ArrayList;Ljava/util/ArrayList;J)V

    #@a4
    .line 872
    :cond_a4
    and-int/lit8 v3, v21, 0x8

    #@a6
    if-eqz v3, :cond_bb

    #@a8
    .line 873
    move-object/from16 v0, p0

    #@aa
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@ac
    move-object/from16 v0, p0

    #@ae
    iget-object v4, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@b0
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$1100(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@b3
    move-result-object v4

    #@b4
    move-object/from16 v0, v22

    #@b6
    move-wide/from16 v1, v17

    #@b8
    invoke-static {v3, v4, v0, v1, v2}, Lcom/android/server/AlarmManagerService;->access$800(Lcom/android/server/AlarmManagerService;Ljava/util/ArrayList;Ljava/util/ArrayList;J)V

    #@bb
    .line 876
    :cond_bb
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@be
    move-result-object v16

    #@bf
    .line 877
    .local v16, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/AlarmManagerService$Alarm;>;"
    :cond_bf
    :goto_bf
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    #@c2
    move-result v3

    #@c3
    if-eqz v3, :cond_264

    #@c5
    .line 878
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c8
    move-result-object v9

    #@c9
    check-cast v9, Lcom/android/server/AlarmManagerService$Alarm;
    :try_end_cb
    .catchall {:try_start_57 .. :try_end_cb} :catchall_21b

    #@cb
    .line 883
    .local v9, alarm:Lcom/android/server/AlarmManagerService$Alarm;
    const/4 v15, 0x0

    #@cc
    .line 884
    .local v15, isImsIntent:Z
    :try_start_cc
    iget-object v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@ce
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getIntent()Landroid/content/Intent;

    #@d1
    move-result-object v3

    #@d2
    if-eqz v3, :cond_f3

    #@d4
    iget-object v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@d6
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getIntent()Landroid/content/Intent;

    #@d9
    move-result-object v3

    #@da
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@dd
    move-result-object v3

    #@de
    if-eqz v3, :cond_f3

    #@e0
    iget-object v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@e2
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getIntent()Landroid/content/Intent;

    #@e5
    move-result-object v3

    #@e6
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@e9
    move-result-object v3

    #@ea
    const-string v4, "com.lge.ims.action.ALARM_NATIVE_TIMER"

    #@ec
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ef
    move-result v3

    #@f0
    if-eqz v3, :cond_f3

    #@f2
    .line 886
    const/4 v15, 0x1

    #@f3
    .line 889
    :cond_f3
    if-eqz v15, :cond_21e

    #@f5
    .line 890
    const-string v3, "AlarmManager"

    #@f7
    const-string v4, "Send IMS intent to non-ordered"

    #@f9
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    .line 891
    move-object/from16 v0, p0

    #@fe
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@100
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;

    #@103
    move-result-object v3

    #@104
    iget-object v4, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@106
    invoke-virtual {v4}, Landroid/app/PendingIntent;->getIntent()Landroid/content/Intent;

    #@109
    move-result-object v4

    #@10a
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@10c
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@10f
    .line 901
    :goto_10f
    move-object/from16 v0, p0

    #@111
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@113
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$1500(Lcom/android/server/AlarmManagerService;)I

    #@116
    move-result v3

    #@117
    if-nez v3, :cond_12d

    #@119
    .line 902
    move-object/from16 v0, p0

    #@11b
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@11d
    iget-object v4, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@11f
    invoke-virtual {v3, v4}, Lcom/android/server/AlarmManagerService;->setWakelockWorkSource(Landroid/app/PendingIntent;)V

    #@122
    .line 903
    move-object/from16 v0, p0

    #@124
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@126
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$1600(Lcom/android/server/AlarmManagerService;)Landroid/os/PowerManager$WakeLock;

    #@129
    move-result-object v3

    #@12a
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@12d
    .line 905
    :cond_12d
    new-instance v13, Lcom/android/server/AlarmManagerService$InFlight;

    #@12f
    move-object/from16 v0, p0

    #@131
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@133
    iget-object v4, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@135
    invoke-direct {v13, v3, v4}, Lcom/android/server/AlarmManagerService$InFlight;-><init>(Lcom/android/server/AlarmManagerService;Landroid/app/PendingIntent;)V

    #@138
    .line 907
    .local v13, inflight:Lcom/android/server/AlarmManagerService$InFlight;
    move-object/from16 v0, p0

    #@13a
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@13c
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@13f
    move-result-object v3

    #@140
    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@143
    .line 908
    move-object/from16 v0, p0

    #@145
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@147
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$1508(Lcom/android/server/AlarmManagerService;)I

    #@14a
    .line 909
    move-object/from16 v0, p0

    #@14c
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@14e
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$1800(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@151
    move-result-object v3

    #@152
    new-instance v4, Ljava/lang/Integer;

    #@154
    iget v5, v9, Lcom/android/server/AlarmManagerService$Alarm;->uid:I

    #@156
    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    #@159
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15c
    .line 910
    iget-object v10, v13, Lcom/android/server/AlarmManagerService$InFlight;->mBroadcastStats:Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@15e
    .line 911
    .local v10, bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    iget v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->count:I

    #@160
    add-int/lit8 v3, v3, 0x1

    #@162
    iput v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->count:I

    #@164
    .line 912
    iget v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@166
    if-nez v3, :cond_254

    #@168
    .line 913
    const/4 v3, 0x1

    #@169
    iput v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@16b
    .line 914
    move-wide/from16 v0, v17

    #@16d
    iput-wide v0, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->startTime:J

    #@16f
    .line 918
    :goto_16f
    iget-object v12, v13, Lcom/android/server/AlarmManagerService$InFlight;->mFilterStats:Lcom/android/server/AlarmManagerService$FilterStats;

    #@171
    .line 919
    .local v12, fs:Lcom/android/server/AlarmManagerService$FilterStats;
    iget v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->count:I

    #@173
    add-int/lit8 v3, v3, 0x1

    #@175
    iput v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->count:I

    #@177
    .line 920
    iget v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@179
    if-nez v3, :cond_25c

    #@17b
    .line 921
    const/4 v3, 0x1

    #@17c
    iput v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@17e
    .line 922
    move-wide/from16 v0, v17

    #@180
    iput-wide v0, v12, Lcom/android/server/AlarmManagerService$FilterStats;->startTime:J

    #@182
    .line 926
    :goto_182
    iget v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->type:I

    #@184
    const/4 v4, 0x2

    #@185
    if-eq v3, v4, :cond_18b

    #@187
    iget v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->type:I

    #@189
    if-nez v3, :cond_1ed

    #@18b
    .line 928
    :cond_18b
    iget v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->type:I

    #@18d
    const/4 v4, 0x2

    #@18e
    if-ne v3, v4, :cond_1b4

    #@190
    .line 929
    const-string v3, "AlarmManager"

    #@192
    new-instance v4, Ljava/lang/StringBuilder;

    #@194
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@197
    const-string v5, "AlarmManager.ELAPSED_REALTIME_WAKEUP set : "

    #@199
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v4

    #@19d
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v4

    #@1a1
    const-string v5, " when  "

    #@1a3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v4

    #@1a7
    iget-wide v5, v9, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@1a9
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v4

    #@1ad
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v4

    #@1b1
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b4
    .line 931
    :cond_1b4
    iget v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->type:I

    #@1b6
    if-nez v3, :cond_1dc

    #@1b8
    .line 932
    const-string v3, "AlarmManager"

    #@1ba
    new-instance v4, Ljava/lang/StringBuilder;

    #@1bc
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1bf
    const-string v5, "AlarmManager.RTC_WAKEUP set : "

    #@1c1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v4

    #@1c5
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v4

    #@1c9
    const-string v5, " when  "

    #@1cb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v4

    #@1cf
    iget-wide v5, v9, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@1d1
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v4

    #@1d5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d8
    move-result-object v4

    #@1d9
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1dc
    .line 934
    :cond_1dc
    iget v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->numWakeup:I

    #@1de
    add-int/lit8 v3, v3, 0x1

    #@1e0
    iput v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->numWakeup:I

    #@1e2
    .line 935
    iget v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->numWakeup:I

    #@1e4
    add-int/lit8 v3, v3, 0x1

    #@1e6
    iput v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->numWakeup:I

    #@1e8
    .line 936
    iget-object v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@1ea
    invoke-static {v3}, Landroid/app/ActivityManagerNative;->noteWakeupAlarm(Landroid/app/PendingIntent;)V

    #@1ed
    .line 941
    :cond_1ed
    if-eqz v15, :cond_bf

    #@1ef
    .line 942
    move-object/from16 v0, p0

    #@1f1
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@1f3
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$1300(Lcom/android/server/AlarmManagerService;)Lcom/android/server/AlarmManagerService$ResultReceiver;

    #@1f6
    move-result-object v3

    #@1f7
    iget-object v4, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@1f9
    iget-object v5, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@1fb
    invoke-virtual {v5}, Landroid/app/PendingIntent;->getIntent()Landroid/content/Intent;

    #@1fe
    move-result-object v5

    #@1ff
    const/4 v6, 0x0

    #@200
    const/4 v7, 0x0

    #@201
    const/4 v8, 0x0

    #@202
    invoke-virtual/range {v3 .. v8}, Lcom/android/server/AlarmManagerService$ResultReceiver;->onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_205
    .catchall {:try_start_cc .. :try_end_205} :catchall_21b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_cc .. :try_end_205} :catch_207
    .catch Ljava/lang/RuntimeException; {:try_start_cc .. :try_end_205} :catch_24a

    #@205
    goto/16 :goto_bf

    #@207
    .line 945
    .end local v10           #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .end local v12           #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    .end local v13           #inflight:Lcom/android/server/AlarmManagerService$InFlight;
    :catch_207
    move-exception v11

    #@208
    .line 946
    .local v11, e:Landroid/app/PendingIntent$CanceledException;
    :try_start_208
    iget-wide v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->repeatInterval:J

    #@20a
    const-wide/16 v5, 0x0

    #@20c
    cmp-long v3, v3, v5

    #@20e
    if-lez v3, :cond_bf

    #@210
    .line 949
    move-object/from16 v0, p0

    #@212
    iget-object v3, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@214
    iget-object v4, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@216
    invoke-virtual {v3, v4}, Lcom/android/server/AlarmManagerService;->remove(Landroid/app/PendingIntent;)V

    #@219
    goto/16 :goto_bf

    #@21b
    .line 955
    .end local v9           #alarm:Lcom/android/server/AlarmManagerService$Alarm;
    .end local v11           #e:Landroid/app/PendingIntent$CanceledException;
    .end local v15           #isImsIntent:Z
    .end local v16           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/AlarmManagerService$Alarm;>;"
    .end local v17           #nowELAPSED:J
    .end local v19           #nowRTC:J
    :catchall_21b
    move-exception v3

    #@21c
    monitor-exit v23
    :try_end_21d
    .catchall {:try_start_208 .. :try_end_21d} :catchall_21b

    #@21d
    throw v3

    #@21e
    .line 893
    .restart local v9       #alarm:Lcom/android/server/AlarmManagerService$Alarm;
    .restart local v15       #isImsIntent:Z
    .restart local v16       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/AlarmManagerService$Alarm;>;"
    .restart local v17       #nowELAPSED:J
    .restart local v19       #nowRTC:J
    :cond_21e
    :try_start_21e
    iget-object v3, v9, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@220
    move-object/from16 v0, p0

    #@222
    iget-object v4, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@224
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;

    #@227
    move-result-object v4

    #@228
    const/4 v5, 0x0

    #@229
    invoke-static {}, Lcom/android/server/AlarmManagerService;->access$1200()Landroid/content/Intent;

    #@22c
    move-result-object v6

    #@22d
    const-string v7, "android.intent.extra.ALARM_COUNT"

    #@22f
    iget v8, v9, Lcom/android/server/AlarmManagerService$Alarm;->count:I

    #@231
    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@234
    move-result-object v6

    #@235
    move-object/from16 v0, p0

    #@237
    iget-object v7, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@239
    invoke-static {v7}, Lcom/android/server/AlarmManagerService;->access$1300(Lcom/android/server/AlarmManagerService;)Lcom/android/server/AlarmManagerService$ResultReceiver;

    #@23c
    move-result-object v7

    #@23d
    move-object/from16 v0, p0

    #@23f
    iget-object v8, v0, Lcom/android/server/AlarmManagerService$AlarmThread;->this$0:Lcom/android/server/AlarmManagerService;

    #@241
    invoke-static {v8}, Lcom/android/server/AlarmManagerService;->access$1400(Lcom/android/server/AlarmManagerService;)Lcom/android/server/AlarmManagerService$AlarmHandler;

    #@244
    move-result-object v8

    #@245
    invoke-virtual/range {v3 .. v8}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_248
    .catchall {:try_start_21e .. :try_end_248} :catchall_21b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_21e .. :try_end_248} :catch_207
    .catch Ljava/lang/RuntimeException; {:try_start_21e .. :try_end_248} :catch_24a

    #@248
    goto/16 :goto_10f

    #@24a
    .line 951
    :catch_24a
    move-exception v11

    #@24b
    .line 952
    .local v11, e:Ljava/lang/RuntimeException;
    :try_start_24b
    const-string v3, "AlarmManager"

    #@24d
    const-string v4, "Failure sending alarm."

    #@24f
    invoke-static {v3, v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_252
    .catchall {:try_start_24b .. :try_end_252} :catchall_21b

    #@252
    goto/16 :goto_bf

    #@254
    .line 916
    .end local v11           #e:Ljava/lang/RuntimeException;
    .restart local v10       #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .restart local v13       #inflight:Lcom/android/server/AlarmManagerService$InFlight;
    :cond_254
    :try_start_254
    iget v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@256
    add-int/lit8 v3, v3, 0x1

    #@258
    iput v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@25a
    goto/16 :goto_16f

    #@25c
    .line 924
    .restart local v12       #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    :cond_25c
    iget v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@25e
    add-int/lit8 v3, v3, 0x1

    #@260
    iput v3, v12, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I
    :try_end_262
    .catchall {:try_start_254 .. :try_end_262} :catchall_21b
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_254 .. :try_end_262} :catch_207
    .catch Ljava/lang/RuntimeException; {:try_start_254 .. :try_end_262} :catch_24a

    #@262
    goto/16 :goto_182

    #@264
    .line 955
    .end local v9           #alarm:Lcom/android/server/AlarmManagerService$Alarm;
    .end local v10           #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .end local v12           #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    .end local v13           #inflight:Lcom/android/server/AlarmManagerService$InFlight;
    .end local v15           #isImsIntent:Z
    :cond_264
    :try_start_264
    monitor-exit v23
    :try_end_265
    .catchall {:try_start_264 .. :try_end_265} :catchall_21b

    #@265
    goto/16 :goto_0
.end method
