.class public Lcom/android/server/TwilightCalculator;
.super Ljava/lang/Object;
.source "TwilightCalculator.java"


# static fields
.field private static final ALTIDUTE_CORRECTION_CIVIL_TWILIGHT:F = -0.10471976f

.field private static final C1:F = 0.0334196f

.field private static final C2:F = 3.49066E-4f

.field private static final C3:F = 5.236E-6f

.field public static final DAY:I = 0x0

.field private static final DEGREES_TO_RADIANS:F = 0.017453292f

.field private static final J0:F = 9.0E-4f

.field public static final NIGHT:I = 0x1

.field private static final OBLIQUITY:F = 0.4092797f

.field private static final UTC_2000:J = 0xdc6d62da00L


# instance fields
.field public mState:I

.field public mSunrise:J

.field public mSunset:J


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public calculateTwilight(JDD)V
    .registers 32
    .parameter "time"
    .parameter "latiude"
    .parameter "longitude"

    #@0
    .prologue
    .line 72
    const-wide v19, 0xdc6d62da00L

    #@5
    sub-long v19, p1, v19

    #@7
    move-wide/from16 v0, v19

    #@9
    long-to-float v0, v0

    #@a
    move/from16 v19, v0

    #@c
    const v20, 0x4ca4cb80

    #@f
    div-float v7, v19, v20

    #@11
    .line 75
    .local v7, daysSince2000:F
    const v19, 0x40c7ae92

    #@14
    const v20, 0x3c8ceb25

    #@17
    mul-float v20, v20, v7

    #@19
    add-float v11, v19, v20

    #@1b
    .line 78
    .local v11, meanAnomaly:F
    const v19, 0x3d08e2fe

    #@1e
    invoke-static {v11}, Landroid/util/FloatMath;->sin(F)F

    #@21
    move-result v20

    #@22
    mul-float v19, v19, v20

    #@24
    add-float v19, v19, v11

    #@26
    const v20, 0x39b702d8

    #@29
    const/high16 v21, 0x4000

    #@2b
    mul-float v21, v21, v11

    #@2d
    invoke-static/range {v21 .. v21}, Landroid/util/FloatMath;->sin(F)F

    #@30
    move-result v21

    #@31
    mul-float v20, v20, v21

    #@33
    add-float v19, v19, v20

    #@35
    const v20, 0x36afb0e6

    #@38
    const/high16 v21, 0x4040

    #@3a
    mul-float v21, v21, v11

    #@3c
    invoke-static/range {v21 .. v21}, Landroid/util/FloatMath;->sin(F)F

    #@3f
    move-result v21

    #@40
    mul-float v20, v20, v21

    #@42
    add-float v18, v19, v20

    #@44
    .line 82
    .local v18, trueAnomaly:F
    const v19, 0x3fe5f6c3

    #@47
    add-float v19, v19, v18

    #@49
    const v20, 0x40490fdb

    #@4c
    add-float v15, v19, v20

    #@4e
    .line 85
    .local v15, solarLng:F
    move-wide/from16 v0, p5

    #@50
    neg-double v0, v0

    #@51
    move-wide/from16 v19, v0

    #@53
    const-wide v21, 0x4076800000000000L

    #@58
    div-double v3, v19, v21

    #@5a
    .line 86
    .local v3, arcLongitude:D
    const v19, 0x3a6bedfa

    #@5d
    sub-float v19, v7, v19

    #@5f
    move/from16 v0, v19

    #@61
    float-to-double v0, v0

    #@62
    move-wide/from16 v19, v0

    #@64
    sub-double v19, v19, v3

    #@66
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->round(D)J

    #@69
    move-result-wide v19

    #@6a
    move-wide/from16 v0, v19

    #@6c
    long-to-float v12, v0

    #@6d
    .line 87
    .local v12, n:F
    const v19, 0x3a6bedfa

    #@70
    add-float v19, v19, v12

    #@72
    move/from16 v0, v19

    #@74
    float-to-double v0, v0

    #@75
    move-wide/from16 v19, v0

    #@77
    add-double v19, v19, v3

    #@79
    const v21, 0x3badab9f

    #@7c
    invoke-static {v11}, Landroid/util/FloatMath;->sin(F)F

    #@7f
    move-result v22

    #@80
    mul-float v21, v21, v22

    #@82
    move/from16 v0, v21

    #@84
    float-to-double v0, v0

    #@85
    move-wide/from16 v21, v0

    #@87
    add-double v19, v19, v21

    #@89
    const v21, -0x441de69b

    #@8c
    const/high16 v22, 0x4000

    #@8e
    mul-float v22, v22, v15

    #@90
    invoke-static/range {v22 .. v22}, Landroid/util/FloatMath;->sin(F)F

    #@93
    move-result v22

    #@94
    mul-float v21, v21, v22

    #@96
    move/from16 v0, v21

    #@98
    float-to-double v0, v0

    #@99
    move-wide/from16 v21, v0

    #@9b
    add-double v16, v19, v21

    #@9d
    .line 91
    .local v16, solarTransitJ2000:D
    invoke-static {v15}, Landroid/util/FloatMath;->sin(F)F

    #@a0
    move-result v19

    #@a1
    const v20, 0x3ed18d1c

    #@a4
    invoke-static/range {v20 .. v20}, Landroid/util/FloatMath;->sin(F)F

    #@a7
    move-result v20

    #@a8
    mul-float v19, v19, v20

    #@aa
    move/from16 v0, v19

    #@ac
    float-to-double v0, v0

    #@ad
    move-wide/from16 v19, v0

    #@af
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->asin(D)D

    #@b2
    move-result-wide v13

    #@b3
    .line 93
    .local v13, solarDec:D
    const-wide v19, 0x3f91df46a0000000L

    #@b8
    mul-double v9, p3, v19

    #@ba
    .line 95
    .local v9, latRad:D
    const v19, -0x422988b0

    #@bd
    invoke-static/range {v19 .. v19}, Landroid/util/FloatMath;->sin(F)F

    #@c0
    move-result v19

    #@c1
    move/from16 v0, v19

    #@c3
    float-to-double v0, v0

    #@c4
    move-wide/from16 v19, v0

    #@c6
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    #@c9
    move-result-wide v21

    #@ca
    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    #@cd
    move-result-wide v23

    #@ce
    mul-double v21, v21, v23

    #@d0
    sub-double v19, v19, v21

    #@d2
    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    #@d5
    move-result-wide v21

    #@d6
    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    #@d9
    move-result-wide v23

    #@da
    mul-double v21, v21, v23

    #@dc
    div-double v5, v19, v21

    #@de
    .line 99
    .local v5, cosHourAngle:D
    const-wide/high16 v19, 0x3ff0

    #@e0
    cmpl-double v19, v5, v19

    #@e2
    if-ltz v19, :cond_fd

    #@e4
    .line 100
    const/16 v19, 0x1

    #@e6
    move/from16 v0, v19

    #@e8
    move-object/from16 v1, p0

    #@ea
    iput v0, v1, Lcom/android/server/TwilightCalculator;->mState:I

    #@ec
    .line 101
    const-wide/16 v19, -0x1

    #@ee
    move-wide/from16 v0, v19

    #@f0
    move-object/from16 v2, p0

    #@f2
    iput-wide v0, v2, Lcom/android/server/TwilightCalculator;->mSunset:J

    #@f4
    .line 102
    const-wide/16 v19, -0x1

    #@f6
    move-wide/from16 v0, v19

    #@f8
    move-object/from16 v2, p0

    #@fa
    iput-wide v0, v2, Lcom/android/server/TwilightCalculator;->mSunrise:J

    #@fc
    .line 121
    :goto_fc
    return-void

    #@fd
    .line 104
    :cond_fd
    const-wide/high16 v19, -0x4010

    #@ff
    cmpg-double v19, v5, v19

    #@101
    if-gtz v19, :cond_11c

    #@103
    .line 105
    const/16 v19, 0x0

    #@105
    move/from16 v0, v19

    #@107
    move-object/from16 v1, p0

    #@109
    iput v0, v1, Lcom/android/server/TwilightCalculator;->mState:I

    #@10b
    .line 106
    const-wide/16 v19, -0x1

    #@10d
    move-wide/from16 v0, v19

    #@10f
    move-object/from16 v2, p0

    #@111
    iput-wide v0, v2, Lcom/android/server/TwilightCalculator;->mSunset:J

    #@113
    .line 107
    const-wide/16 v19, -0x1

    #@115
    move-wide/from16 v0, v19

    #@117
    move-object/from16 v2, p0

    #@119
    iput-wide v0, v2, Lcom/android/server/TwilightCalculator;->mSunrise:J

    #@11b
    goto :goto_fc

    #@11c
    .line 111
    :cond_11c
    invoke-static {v5, v6}, Ljava/lang/Math;->acos(D)D

    #@11f
    move-result-wide v19

    #@120
    const-wide v21, 0x401921fb54442d18L

    #@125
    div-double v19, v19, v21

    #@127
    move-wide/from16 v0, v19

    #@129
    double-to-float v8, v0

    #@12a
    .line 113
    .local v8, hourAngle:F
    float-to-double v0, v8

    #@12b
    move-wide/from16 v19, v0

    #@12d
    add-double v19, v19, v16

    #@12f
    const-wide v21, 0x4194997000000000L

    #@134
    mul-double v19, v19, v21

    #@136
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->round(D)J

    #@139
    move-result-wide v19

    #@13a
    const-wide v21, 0xdc6d62da00L

    #@13f
    add-long v19, v19, v21

    #@141
    move-wide/from16 v0, v19

    #@143
    move-object/from16 v2, p0

    #@145
    iput-wide v0, v2, Lcom/android/server/TwilightCalculator;->mSunset:J

    #@147
    .line 114
    float-to-double v0, v8

    #@148
    move-wide/from16 v19, v0

    #@14a
    sub-double v19, v16, v19

    #@14c
    const-wide v21, 0x4194997000000000L

    #@151
    mul-double v19, v19, v21

    #@153
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->round(D)J

    #@156
    move-result-wide v19

    #@157
    const-wide v21, 0xdc6d62da00L

    #@15c
    add-long v19, v19, v21

    #@15e
    move-wide/from16 v0, v19

    #@160
    move-object/from16 v2, p0

    #@162
    iput-wide v0, v2, Lcom/android/server/TwilightCalculator;->mSunrise:J

    #@164
    .line 116
    move-object/from16 v0, p0

    #@166
    iget-wide v0, v0, Lcom/android/server/TwilightCalculator;->mSunrise:J

    #@168
    move-wide/from16 v19, v0

    #@16a
    cmp-long v19, v19, p1

    #@16c
    if-gez v19, :cond_182

    #@16e
    move-object/from16 v0, p0

    #@170
    iget-wide v0, v0, Lcom/android/server/TwilightCalculator;->mSunset:J

    #@172
    move-wide/from16 v19, v0

    #@174
    cmp-long v19, v19, p1

    #@176
    if-lez v19, :cond_182

    #@178
    .line 117
    const/16 v19, 0x0

    #@17a
    move/from16 v0, v19

    #@17c
    move-object/from16 v1, p0

    #@17e
    iput v0, v1, Lcom/android/server/TwilightCalculator;->mState:I

    #@180
    goto/16 :goto_fc

    #@182
    .line 119
    :cond_182
    const/16 v19, 0x1

    #@184
    move/from16 v0, v19

    #@186
    move-object/from16 v1, p0

    #@188
    iput v0, v1, Lcom/android/server/TwilightCalculator;->mState:I

    #@18a
    goto/16 :goto_fc
.end method
