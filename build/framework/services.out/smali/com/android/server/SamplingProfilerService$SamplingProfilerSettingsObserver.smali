.class Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;
.super Landroid/database/ContentObserver;
.source "SamplingProfilerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/SamplingProfilerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SamplingProfilerSettingsObserver"
.end annotation


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/android/server/SamplingProfilerService;


# direct methods
.method public constructor <init>(Lcom/android/server/SamplingProfilerService;Landroid/content/ContentResolver;)V
    .registers 4
    .parameter
    .parameter "contentResolver"

    #@0
    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;->this$0:Lcom/android/server/SamplingProfilerService;

    #@2
    .line 108
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@6
    .line 109
    iput-object p2, p0, Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;->mContentResolver:Landroid/content/ContentResolver;

    #@8
    .line 110
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0}, Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;->onChange(Z)V

    #@c
    .line 111
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 6
    .parameter "selfChange"

    #@0
    .prologue
    .line 114
    iget-object v1, p0, Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v2, "sampling_profiler_ms"

    #@4
    const/4 v3, 0x0

    #@5
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8
    move-result v1

    #@9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v0

    #@d
    .line 118
    .local v0, samplingProfilerMs:Ljava/lang/Integer;
    const-string v1, "persist.sys.profiler_ms"

    #@f
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 119
    return-void
.end method
