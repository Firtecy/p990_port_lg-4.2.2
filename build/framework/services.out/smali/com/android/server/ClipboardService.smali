.class public Lcom/android/server/ClipboardService;
.super Landroid/content/IClipboard$Stub;
.source "ClipboardService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ClipboardService$PerUserClipboard;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "media"

.field private static final CONTENT_AUTHORITY_SLASH:Ljava/lang/String; = "content://media/"

.field private static final EXTERNAL_CONTENT_URI:Landroid/net/Uri; = null

.field private static final MAX_CLIP_COUNT:I = 0x14

.field private static final SECURITY_BRIDGE_NAME:Ljava/lang/String; = "com.android.services.SecurityBridge.core.ClipboardManagerSB"

.field private static final TAG:Ljava/lang/String; = "ClipboardService"


# instance fields
.field private dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

.field private final mAm:Landroid/app/IActivityManager;

.field private mClipboards:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/ClipboardService$PerUserClipboard;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mPermissionOwner:Landroid/os/IBinder;

.field private final mPm:Landroid/content/pm/PackageManager;

.field private mSecurityBridge:Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 89
    const-string v0, "content://media/external/databases"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/ClipboardService;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 122
    invoke-direct {p0}, Landroid/content/IClipboard$Stub;-><init>()V

    #@3
    .line 117
    new-instance v4, Landroid/util/SparseArray;

    #@5
    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v4, p0, Lcom/android/server/ClipboardService;->mClipboards:Landroid/util/SparseArray;

    #@a
    .line 123
    iput-object p1, p0, Lcom/android/server/ClipboardService;->mContext:Landroid/content/Context;

    #@c
    .line 124
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f
    move-result-object v4

    #@10
    iput-object v4, p0, Lcom/android/server/ClipboardService;->mAm:Landroid/app/IActivityManager;

    #@12
    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@15
    move-result-object v4

    #@16
    iput-object v4, p0, Lcom/android/server/ClipboardService;->mPm:Landroid/content/pm/PackageManager;

    #@18
    .line 126
    const/4 v2, 0x0

    #@19
    .line 128
    .local v2, permOwner:Landroid/os/IBinder;
    :try_start_19
    iget-object v4, p0, Lcom/android/server/ClipboardService;->mAm:Landroid/app/IActivityManager;

    #@1b
    const-string v5, "clipboard"

    #@1d
    invoke-interface {v4, v5}, Landroid/app/IActivityManager;->newUriPermissionOwner(Ljava/lang/String;)Landroid/os/IBinder;
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_20} :catch_5e

    #@20
    move-result-object v2

    #@21
    .line 132
    :goto_21
    iput-object v2, p0, Lcom/android/server/ClipboardService;->mPermissionOwner:Landroid/os/IBinder;

    #@23
    .line 135
    new-instance v3, Landroid/content/IntentFilter;

    #@25
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@28
    .line 136
    .local v3, userFilter:Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.USER_REMOVED"

    #@2a
    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2d
    .line 137
    iget-object v4, p0, Lcom/android/server/ClipboardService;->mContext:Landroid/content/Context;

    #@2f
    new-instance v5, Lcom/android/server/ClipboardService$1;

    #@31
    invoke-direct {v5, p0}, Lcom/android/server/ClipboardService$1;-><init>(Lcom/android/server/ClipboardService;)V

    #@34
    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@37
    .line 147
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@39
    if-eqz v4, :cond_47

    #@3b
    .line 148
    new-instance v4, Lcom/lge/cliptray/ClipDBOpenHelper;

    #@3d
    iget-object v5, p0, Lcom/android/server/ClipboardService;->mContext:Landroid/content/Context;

    #@3f
    invoke-direct {v4, v5}, Lcom/lge/cliptray/ClipDBOpenHelper;-><init>(Landroid/content/Context;)V

    #@42
    iput-object v4, p0, Lcom/android/server/ClipboardService;->dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

    #@44
    .line 149
    invoke-virtual {p0}, Lcom/android/server/ClipboardService;->initClipboardDB()V

    #@47
    .line 158
    :cond_47
    :try_start_47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@4e
    move-result-object v4

    #@4f
    const-string v5, "com.android.services.SecurityBridge.core.ClipboardManagerSB"

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@58
    move-result-object v0

    #@59
    .line 159
    .local v0, bridgeObject:Ljava/lang/Object;
    check-cast v0, Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;

    #@5b
    .end local v0           #bridgeObject:Ljava/lang/Object;
    iput-object v0, p0, Lcom/android/server/ClipboardService;->mSecurityBridge:Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;
    :try_end_5d
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_5d} :catch_67

    #@5d
    .line 166
    :goto_5d
    return-void

    #@5e
    .line 129
    .end local v3           #userFilter:Landroid/content/IntentFilter;
    :catch_5e
    move-exception v1

    #@5f
    .line 130
    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "clipboard"

    #@61
    const-string v5, "AM dead"

    #@63
    invoke-static {v4, v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@66
    goto :goto_21

    #@67
    .line 161
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v3       #userFilter:Landroid/content/IntentFilter;
    :catch_67
    move-exception v1

    #@68
    .line 163
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "ClipboardService"

    #@6a
    const-string v5, "No security bridge jar found, using default"

    #@6c
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 164
    new-instance v4, Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;

    #@71
    invoke-direct {v4}, Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;-><init>()V

    #@74
    iput-object v4, p0, Lcom/android/server/ClipboardService;->mSecurityBridge:Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;

    #@76
    goto :goto_5d
.end method

.method static synthetic access$000(Lcom/android/server/ClipboardService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/server/ClipboardService;->removeClipboard(I)V

    #@3
    return-void
.end method

.method private final addActiveOwnerLocked(ILjava/lang/String;)V
    .registers 15
    .parameter "uid"
    .parameter "pkg"

    #@0
    .prologue
    .line 503
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@3
    move-result-object v7

    #@4
    .line 504
    .local v7, pm:Landroid/content/pm/IPackageManager;
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@7
    move-result v8

    #@8
    .line 505
    .local v8, targetUserHandle:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v4

    #@c
    .line 507
    .local v4, oldIdentity:J
    const/4 v9, 0x0

    #@d
    :try_start_d
    invoke-interface {v7, p2, v9, v8}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    #@10
    move-result-object v6

    #@11
    .line 508
    .local v6, pi:Landroid/content/pm/PackageInfo;
    if-nez v6, :cond_5d

    #@13
    .line 509
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@15
    new-instance v10, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v11, "Unknown package "

    #@1c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v10

    #@20
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v10

    #@24
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v10

    #@28
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v9
    :try_end_2c
    .catchall {:try_start_d .. :try_end_2c} :catchall_8a
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_2c} :catch_2c

    #@2c
    .line 515
    .end local v6           #pi:Landroid/content/pm/PackageInfo;
    :catch_2c
    move-exception v9

    #@2d
    .line 518
    :cond_2d
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@30
    .line 520
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@33
    move-result-object v1

    #@34
    .line 521
    .local v1, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@36
    if-eqz v9, :cond_d9

    #@38
    .line 522
    iget-boolean v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@3a
    if-eqz v9, :cond_95

    #@3c
    .line 523
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@3e
    if-eqz v9, :cond_94

    #@40
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@42
    invoke-virtual {v9, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@45
    move-result v9

    #@46
    if-nez v9, :cond_94

    #@48
    .line 524
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@4a
    invoke-virtual {v9}, Landroid/content/ClipData;->getItemCount()I

    #@4d
    move-result v0

    #@4e
    .line 525
    .local v0, N:I
    const/4 v2, 0x0

    #@4f
    .local v2, i:I
    :goto_4f
    if-ge v2, v0, :cond_8f

    #@51
    .line 526
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@53
    invoke-virtual {v9, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@56
    move-result-object v9

    #@57
    invoke-direct {p0, v9, p2}, Lcom/android/server/ClipboardService;->grantItemLocked(Landroid/content/ClipData$Item;Ljava/lang/String;)V

    #@5a
    .line 525
    add-int/lit8 v2, v2, 0x1

    #@5c
    goto :goto_4f

    #@5d
    .line 511
    .end local v0           #N:I
    .end local v1           #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    .end local v2           #i:I
    .restart local v6       #pi:Landroid/content/pm/PackageInfo;
    :cond_5d
    :try_start_5d
    iget-object v9, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5f
    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    #@61
    invoke-static {v9, p1}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@64
    move-result v9

    #@65
    if-nez v9, :cond_2d

    #@67
    .line 512
    new-instance v9, Ljava/lang/SecurityException;

    #@69
    new-instance v10, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v11, "Calling uid "

    #@70
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v10

    #@74
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v10

    #@78
    const-string v11, " does not own package "

    #@7a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v10

    #@7e
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v10

    #@82
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v10

    #@86
    invoke-direct {v9, v10}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@89
    throw v9
    :try_end_8a
    .catchall {:try_start_5d .. :try_end_8a} :catchall_8a
    .catch Landroid/os/RemoteException; {:try_start_5d .. :try_end_8a} :catch_2c

    #@8a
    .line 518
    .end local v6           #pi:Landroid/content/pm/PackageInfo;
    :catchall_8a
    move-exception v9

    #@8b
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@8e
    throw v9

    #@8f
    .line 528
    .restart local v0       #N:I
    .restart local v1       #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    .restart local v2       #i:I
    :cond_8f
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@91
    invoke-virtual {v9, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@94
    .line 550
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_94
    :goto_94
    return-void

    #@95
    .line 531
    :cond_95
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@97
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    #@9a
    move-result v9

    #@9b
    if-nez v9, :cond_94

    #@9d
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@9f
    invoke-virtual {v9, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@a2
    move-result v9

    #@a3
    if-nez v9, :cond_94

    #@a5
    .line 532
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@a7
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@aa
    move-result v9

    #@ab
    add-int/lit8 v3, v9, -0x1

    #@ad
    .local v3, index:I
    :goto_ad
    if-ltz v3, :cond_d3

    #@af
    .line 533
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@b1
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b4
    move-result-object v9

    #@b5
    check-cast v9, Landroid/content/ClipData;

    #@b7
    invoke-virtual {v9}, Landroid/content/ClipData;->getItemCount()I

    #@ba
    move-result v0

    #@bb
    .line 534
    .restart local v0       #N:I
    const/4 v2, 0x0

    #@bc
    .restart local v2       #i:I
    :goto_bc
    if-ge v2, v0, :cond_d0

    #@be
    .line 535
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@c0
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c3
    move-result-object v9

    #@c4
    check-cast v9, Landroid/content/ClipData;

    #@c6
    invoke-virtual {v9, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@c9
    move-result-object v9

    #@ca
    invoke-direct {p0, v9, p2}, Lcom/android/server/ClipboardService;->grantItemLocked(Landroid/content/ClipData$Item;Ljava/lang/String;)V

    #@cd
    .line 534
    add-int/lit8 v2, v2, 0x1

    #@cf
    goto :goto_bc

    #@d0
    .line 532
    :cond_d0
    add-int/lit8 v3, v3, -0x1

    #@d2
    goto :goto_ad

    #@d3
    .line 538
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_d3
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@d5
    invoke-virtual {v9, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@d8
    goto :goto_94

    #@d9
    .line 542
    .end local v3           #index:I
    :cond_d9
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@db
    if-eqz v9, :cond_94

    #@dd
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@df
    invoke-virtual {v9, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@e2
    move-result v9

    #@e3
    if-nez v9, :cond_94

    #@e5
    .line 543
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@e7
    invoke-virtual {v9}, Landroid/content/ClipData;->getItemCount()I

    #@ea
    move-result v0

    #@eb
    .line 544
    .restart local v0       #N:I
    const/4 v2, 0x0

    #@ec
    .restart local v2       #i:I
    :goto_ec
    if-ge v2, v0, :cond_fa

    #@ee
    .line 545
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@f0
    invoke-virtual {v9, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@f3
    move-result-object v9

    #@f4
    invoke-direct {p0, v9, p2}, Lcom/android/server/ClipboardService;->grantItemLocked(Landroid/content/ClipData$Item;Ljava/lang/String;)V

    #@f7
    .line 544
    add-int/lit8 v2, v2, 0x1

    #@f9
    goto :goto_ec

    #@fa
    .line 547
    :cond_fa
    iget-object v9, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@fc
    invoke-virtual {v9, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@ff
    goto :goto_94
.end method

.method private final checkDataOwnerLocked(Landroid/content/ClipData;I)V
    .registers 6
    .parameter "data"
    .parameter "uid"

    #@0
    .prologue
    .line 475
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    #@3
    move-result v0

    #@4
    .line 476
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 477
    invoke-virtual {p1, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@a
    move-result-object v2

    #@b
    invoke-direct {p0, v2, p2}, Lcom/android/server/ClipboardService;->checkItemOwnerLocked(Landroid/content/ClipData$Item;I)V

    #@e
    .line 476
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 479
    :cond_11
    return-void
.end method

.method private final checkItemOwnerLocked(Landroid/content/ClipData$Item;I)V
    .registers 5
    .parameter "item"
    .parameter "uid"

    #@0
    .prologue
    .line 465
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 466
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@9
    move-result-object v1

    #@a
    invoke-direct {p0, v1, p2}, Lcom/android/server/ClipboardService;->checkUriOwnerLocked(Landroid/net/Uri;I)V

    #@d
    .line 468
    :cond_d
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    #@10
    move-result-object v0

    #@11
    .line 469
    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_20

    #@13
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@16
    move-result-object v1

    #@17
    if-eqz v1, :cond_20

    #@19
    .line 470
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {p0, v1, p2}, Lcom/android/server/ClipboardService;->checkUriOwnerLocked(Landroid/net/Uri;I)V

    #@20
    .line 472
    :cond_20
    return-void
.end method

.method private final checkUriOwnerLocked(Landroid/net/Uri;I)V
    .registers 8
    .parameter "uri"
    .parameter "uid"

    #@0
    .prologue
    .line 451
    const-string v2, "content"

    #@2
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_d

    #@c
    .line 462
    :goto_c
    return-void

    #@d
    .line 454
    :cond_d
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@10
    move-result-wide v0

    #@11
    .line 457
    .local v0, ident:J
    :try_start_11
    iget-object v2, p0, Lcom/android/server/ClipboardService;->mAm:Landroid/app/IActivityManager;

    #@13
    const/4 v3, 0x0

    #@14
    const/4 v4, 0x1

    #@15
    invoke-interface {v2, p2, v3, p1, v4}, Landroid/app/IActivityManager;->checkGrantUriPermission(ILjava/lang/String;Landroid/net/Uri;I)I
    :try_end_18
    .catchall {:try_start_11 .. :try_end_18} :catchall_1c
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_18} :catch_21

    #@18
    .line 460
    :goto_18
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1b
    goto :goto_c

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@20
    throw v2

    #@21
    .line 458
    :catch_21
    move-exception v2

    #@22
    goto :goto_18
.end method

.method private final clearActiveOwnersLocked()V
    .registers 6

    #@0
    .prologue
    .line 575
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@3
    move-result-object v1

    #@4
    .line 576
    .local v1, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@6
    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    #@9
    .line 577
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@b
    if-eqz v4, :cond_61

    #@d
    .line 578
    iget-boolean v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@f
    if-eqz v4, :cond_2b

    #@11
    .line 579
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@13
    if-nez v4, :cond_16

    #@15
    .line 606
    :cond_15
    return-void

    #@16
    .line 582
    :cond_16
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@18
    invoke-virtual {v4}, Landroid/content/ClipData;->getItemCount()I

    #@1b
    move-result v0

    #@1c
    .line 583
    .local v0, N:I
    const/4 v2, 0x0

    #@1d
    .local v2, i:I
    :goto_1d
    if-ge v2, v0, :cond_15

    #@1f
    .line 584
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@21
    invoke-virtual {v4, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@24
    move-result-object v4

    #@25
    invoke-direct {p0, v4}, Lcom/android/server/ClipboardService;->revokeItemLocked(Landroid/content/ClipData$Item;)V

    #@28
    .line 583
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_1d

    #@2b
    .line 587
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_2b
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    #@30
    move-result v4

    #@31
    if-nez v4, :cond_15

    #@33
    .line 590
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@38
    move-result v4

    #@39
    add-int/lit8 v3, v4, -0x1

    #@3b
    .local v3, index:I
    :goto_3b
    if-ltz v3, :cond_15

    #@3d
    .line 591
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v4

    #@43
    check-cast v4, Landroid/content/ClipData;

    #@45
    invoke-virtual {v4}, Landroid/content/ClipData;->getItemCount()I

    #@48
    move-result v0

    #@49
    .line 592
    .restart local v0       #N:I
    const/4 v2, 0x0

    #@4a
    .restart local v2       #i:I
    :goto_4a
    if-ge v2, v0, :cond_5e

    #@4c
    .line 593
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@4e
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v4

    #@52
    check-cast v4, Landroid/content/ClipData;

    #@54
    invoke-virtual {v4, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@57
    move-result-object v4

    #@58
    invoke-direct {p0, v4}, Lcom/android/server/ClipboardService;->revokeItemLocked(Landroid/content/ClipData$Item;)V

    #@5b
    .line 592
    add-int/lit8 v2, v2, 0x1

    #@5d
    goto :goto_4a

    #@5e
    .line 590
    :cond_5e
    add-int/lit8 v3, v3, -0x1

    #@60
    goto :goto_3b

    #@61
    .line 598
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #index:I
    :cond_61
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@63
    if-eqz v4, :cond_15

    #@65
    .line 601
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@67
    invoke-virtual {v4}, Landroid/content/ClipData;->getItemCount()I

    #@6a
    move-result v0

    #@6b
    .line 602
    .restart local v0       #N:I
    const/4 v2, 0x0

    #@6c
    .restart local v2       #i:I
    :goto_6c
    if-ge v2, v0, :cond_15

    #@6e
    .line 603
    iget-object v4, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@70
    invoke-virtual {v4, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@73
    move-result-object v4

    #@74
    invoke-direct {p0, v4}, Lcom/android/server/ClipboardService;->revokeItemLocked(Landroid/content/ClipData$Item;)V

    #@77
    .line 602
    add-int/lit8 v2, v2, 0x1

    #@79
    goto :goto_6c
.end method

.method private getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;
    .registers 2

    #@0
    .prologue
    .line 181
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Lcom/android/server/ClipboardService;->getClipboard(I)Lcom/android/server/ClipboardService$PerUserClipboard;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private getClipboard(I)Lcom/android/server/ClipboardService$PerUserClipboard;
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 185
    iget-object v2, p0, Lcom/android/server/ClipboardService;->mClipboards:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 186
    :try_start_3
    iget-object v1, p0, Lcom/android/server/ClipboardService;->mClipboards:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/server/ClipboardService$PerUserClipboard;

    #@b
    .line 187
    .local v0, puc:Lcom/android/server/ClipboardService$PerUserClipboard;
    if-nez v0, :cond_17

    #@d
    .line 188
    new-instance v0, Lcom/android/server/ClipboardService$PerUserClipboard;

    #@f
    .end local v0           #puc:Lcom/android/server/ClipboardService$PerUserClipboard;
    invoke-direct {v0, p0, p1}, Lcom/android/server/ClipboardService$PerUserClipboard;-><init>(Lcom/android/server/ClipboardService;I)V

    #@12
    .line 189
    .restart local v0       #puc:Lcom/android/server/ClipboardService$PerUserClipboard;
    iget-object v1, p0, Lcom/android/server/ClipboardService;->mClipboards:Landroid/util/SparseArray;

    #@14
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@17
    .line 191
    :cond_17
    monitor-exit v2

    #@18
    return-object v0

    #@19
    .line 192
    .end local v0           #puc:Lcom/android/server/ClipboardService$PerUserClipboard;
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method private final grantItemLocked(Landroid/content/ClipData$Item;Ljava/lang/String;)V
    .registers 5
    .parameter "item"
    .parameter "pkg"

    #@0
    .prologue
    .line 493
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 494
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@9
    move-result-object v1

    #@a
    invoke-direct {p0, v1, p2}, Lcom/android/server/ClipboardService;->grantUriLocked(Landroid/net/Uri;Ljava/lang/String;)V

    #@d
    .line 496
    :cond_d
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    #@10
    move-result-object v0

    #@11
    .line 497
    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_20

    #@13
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@16
    move-result-object v1

    #@17
    if-eqz v1, :cond_20

    #@19
    .line 498
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {p0, v1, p2}, Lcom/android/server/ClipboardService;->grantUriLocked(Landroid/net/Uri;Ljava/lang/String;)V

    #@20
    .line 500
    :cond_20
    return-void
.end method

.method private final grantUriLocked(Landroid/net/Uri;Ljava/lang/String;)V
    .registers 11
    .parameter "uri"
    .parameter "pkg"

    #@0
    .prologue
    .line 482
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v6

    #@4
    .line 484
    .local v6, ident:J
    :try_start_4
    iget-object v0, p0, Lcom/android/server/ClipboardService;->mAm:Landroid/app/IActivityManager;

    #@6
    iget-object v1, p0, Lcom/android/server/ClipboardService;->mPermissionOwner:Landroid/os/IBinder;

    #@8
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@b
    move-result v2

    #@c
    const/4 v5, 0x1

    #@d
    move-object v3, p2

    #@e
    move-object v4, p1

    #@f
    invoke-interface/range {v0 .. v5}, Landroid/app/IActivityManager;->grantUriPermissionFromOwner(Landroid/os/IBinder;ILjava/lang/String;Landroid/net/Uri;I)V
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_16
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_12} :catch_1b

    #@12
    .line 488
    :goto_12
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@15
    .line 490
    return-void

    #@16
    .line 488
    :catchall_16
    move-exception v0

    #@17
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1a
    throw v0

    #@1b
    .line 486
    :catch_1b
    move-exception v0

    #@1c
    goto :goto_12
.end method

.method private removeClipboard(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 196
    iget-object v1, p0, Lcom/android/server/ClipboardService;->mClipboards:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 197
    :try_start_3
    iget-object v0, p0, Lcom/android/server/ClipboardService;->mClipboards:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@8
    .line 198
    monitor-exit v1

    #@9
    .line 199
    return-void

    #@a
    .line 198
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method private final revokeItemLocked(Landroid/content/ClipData$Item;)V
    .registers 4
    .parameter "item"

    #@0
    .prologue
    .line 565
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 566
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@9
    move-result-object v1

    #@a
    invoke-direct {p0, v1}, Lcom/android/server/ClipboardService;->revokeUriLocked(Landroid/net/Uri;)V

    #@d
    .line 568
    :cond_d
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    #@10
    move-result-object v0

    #@11
    .line 569
    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_20

    #@13
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@16
    move-result-object v1

    #@17
    if-eqz v1, :cond_20

    #@19
    .line 570
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {p0, v1}, Lcom/android/server/ClipboardService;->revokeUriLocked(Landroid/net/Uri;)V

    #@20
    .line 572
    :cond_20
    return-void
.end method

.method private final revokeUriLocked(Landroid/net/Uri;)V
    .registers 7
    .parameter "uri"

    #@0
    .prologue
    .line 553
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 555
    .local v0, ident:J
    :try_start_4
    iget-object v2, p0, Lcom/android/server/ClipboardService;->mAm:Landroid/app/IActivityManager;

    #@6
    iget-object v3, p0, Lcom/android/server/ClipboardService;->mPermissionOwner:Landroid/os/IBinder;

    #@8
    const/4 v4, 0x3

    #@9
    invoke-interface {v2, v3, p1, v4}, Landroid/app/IActivityManager;->revokeUriPermissionFromOwner(Landroid/os/IBinder;Landroid/net/Uri;I)V
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_10
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_c} :catch_15

    #@c
    .line 560
    :goto_c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@f
    .line 562
    return-void

    #@10
    .line 560
    :catchall_10
    move-exception v2

    #@11
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    throw v2

    #@15
    .line 558
    :catch_15
    move-exception v2

    #@16
    goto :goto_c
.end method


# virtual methods
.method public addPrimaryClipChangedListener(Landroid/content/IOnPrimaryClipChangedListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 417
    monitor-enter p0

    #@1
    .line 418
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@4
    move-result-object v0

    #@5
    iget-object v0, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@7
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@a
    .line 419
    monitor-exit p0

    #@b
    .line 420
    return-void

    #@c
    .line 419
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public getInitialPrimaryClipAt(Ljava/lang/String;I)Landroid/content/ClipData;
    .registers 4
    .parameter "pkg"
    .parameter "index"

    #@0
    .prologue
    .line 720
    monitor-enter p0

    #@1
    .line 721
    :try_start_1
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v0

    #@5
    invoke-direct {p0, v0, p1}, Lcom/android/server/ClipboardService;->addActiveOwnerLocked(ILjava/lang/String;)V

    #@8
    .line 722
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@b
    move-result-object v0

    #@c
    iget-object v0, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/content/ClipData;

    #@14
    monitor-exit p0

    #@15
    return-object v0

    #@16
    .line 723
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public getPrimaryClip(Ljava/lang/String;)Landroid/content/ClipData;
    .registers 7
    .parameter "pkg"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 315
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v2

    #@5
    if-eqz v2, :cond_13

    #@7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v2

    #@b
    invoke-interface {v2, v4}, Lcom/lge/cappuccino/IMdm;->checkDisabledClipboard(Z)Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_13

    #@11
    .line 317
    const/4 v2, 0x0

    #@12
    .line 356
    :goto_12
    return-object v2

    #@13
    .line 321
    :cond_13
    const-string v2, "3LM_cliptray"

    #@15
    const-string v3, "getPrimaryClip() 1"

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 322
    monitor-enter p0

    #@1b
    .line 335
    :try_start_1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-direct {p0, v2, p1}, Lcom/android/server/ClipboardService;->addActiveOwnerLocked(ILjava/lang/String;)V

    #@22
    .line 336
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@24
    if-eqz v2, :cond_67

    #@26
    .line 337
    sget-boolean v2, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@28
    if-eqz v2, :cond_42

    #@2a
    invoke-virtual {p0}, Lcom/android/server/ClipboardService;->isUidSecure()Z

    #@2d
    move-result v2

    #@2e
    if-eqz v2, :cond_42

    #@30
    .line 338
    const-string v2, "3LM_cliptray"

    #@32
    const-string v3, "getPrimaryClip() 4"

    #@34
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 339
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@3a
    move-result-object v2

    #@3b
    iget-object v2, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->mdmClip:Landroid/content/ClipData;

    #@3d
    monitor-exit p0

    #@3e
    goto :goto_12

    #@3f
    .line 357
    :catchall_3f
    move-exception v2

    #@40
    monitor-exit p0
    :try_end_41
    .catchall {:try_start_1b .. :try_end_41} :catchall_3f

    #@41
    throw v2

    #@42
    .line 341
    :cond_42
    :try_start_42
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@45
    move-result-object v1

    #@46
    .line 342
    .local v1, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    const-string v2, "3LM_cliptray"

    #@48
    const-string v3, "getPrimaryClip() 5"

    #@4a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 343
    iget-boolean v2, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@4f
    if-eqz v2, :cond_55

    #@51
    .line 344
    iget-object v2, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@53
    monitor-exit p0

    #@54
    goto :goto_12

    #@55
    .line 346
    :cond_55
    iget-object v2, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@57
    iget-object v3, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@59
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5c
    move-result v3

    #@5d
    add-int/lit8 v3, v3, -0x1

    #@5f
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@62
    move-result-object v2

    #@63
    check-cast v2, Landroid/content/ClipData;

    #@65
    monitor-exit p0

    #@66
    goto :goto_12

    #@67
    .line 349
    .end local v1           #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    :cond_67
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@6a
    move-result-object v2

    #@6b
    iget-object v0, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@6d
    .line 350
    .local v0, clip:Landroid/content/ClipData;
    if-eqz v0, :cond_7c

    #@6f
    .line 351
    iget-object v2, p0, Lcom/android/server/ClipboardService;->mSecurityBridge:Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;

    #@71
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@74
    move-result v3

    #@75
    invoke-virtual {v2, v3, v0}, Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;->approvePasteRequest(ILandroid/content/ClipData;)Z

    #@78
    move-result v2

    #@79
    if-eq v4, v2, :cond_7c

    #@7b
    .line 352
    const/4 v0, 0x0

    #@7c
    .line 355
    :cond_7c
    const-string v2, "3LM_cliptray"

    #@7e
    const-string v3, "getPrimaryClip() 6"

    #@80
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 356
    monitor-exit p0
    :try_end_84
    .catchall {:try_start_42 .. :try_end_84} :catchall_3f

    #@84
    move-object v2, v0

    #@85
    goto :goto_12
.end method

.method public getPrimaryClipAt(Ljava/lang/String;I)Landroid/content/ClipData;
    .registers 5
    .parameter "pkg"
    .parameter "index"

    #@0
    .prologue
    .line 614
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_13

    #@6
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x1

    #@b
    invoke-interface {v0, v1}, Lcom/lge/cappuccino/IMdm;->checkDisabledClipboard(Z)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_13

    #@11
    .line 616
    const/4 v0, 0x0

    #@12
    .line 622
    :goto_12
    return-object v0

    #@13
    .line 620
    :cond_13
    monitor-enter p0

    #@14
    .line 621
    :try_start_14
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@17
    move-result v0

    #@18
    invoke-direct {p0, v0, p1}, Lcom/android/server/ClipboardService;->addActiveOwnerLocked(ILjava/lang/String;)V

    #@1b
    .line 622
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@1e
    move-result-object v0

    #@1f
    iget-object v0, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Landroid/content/ClipData;

    #@27
    monitor-exit p0

    #@28
    goto :goto_12

    #@29
    .line 623
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_14 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method public getPrimaryClipCount()I
    .registers 2

    #@0
    .prologue
    .line 639
    monitor-enter p0

    #@1
    .line 640
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@4
    move-result-object v0

    #@5
    iget-object v0, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    monitor-exit p0

    #@c
    return v0

    #@d
    .line 641
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public getPrimaryClipDescription()Landroid/content/ClipDescription;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 361
    monitor-enter p0

    #@2
    .line 362
    :try_start_2
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@5
    move-result-object v0

    #@6
    .line 363
    .local v0, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@8
    if-eqz v2, :cond_3b

    #@a
    .line 364
    iget-boolean v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@c
    if-eqz v2, :cond_1a

    #@e
    .line 365
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@10
    if-eqz v2, :cond_18

    #@12
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@14
    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    #@17
    move-result-object v1

    #@18
    :cond_18
    monitor-exit p0

    #@19
    .line 369
    :goto_19
    return-object v1

    #@1a
    .line 367
    :cond_1a
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@1f
    move-result v2

    #@20
    if-nez v2, :cond_36

    #@22
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@24
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@29
    move-result v2

    #@2a
    add-int/lit8 v2, v2, -0x1

    #@2c
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2f
    move-result-object v1

    #@30
    check-cast v1, Landroid/content/ClipData;

    #@32
    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    #@35
    move-result-object v1

    #@36
    :cond_36
    monitor-exit p0

    #@37
    goto :goto_19

    #@38
    .line 370
    .end local v0           #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    :catchall_38
    move-exception v1

    #@39
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_2 .. :try_end_3a} :catchall_38

    #@3a
    throw v1

    #@3b
    .line 369
    .restart local v0       #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    :cond_3b
    :try_start_3b
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@3d
    if-eqz v2, :cond_45

    #@3f
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@41
    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    #@44
    move-result-object v1

    #@45
    :cond_45
    monitor-exit p0
    :try_end_46
    .catchall {:try_start_3b .. :try_end_46} :catchall_38

    #@46
    goto :goto_19
.end method

.method public getPrimaryClipDescriptionAt(I)Landroid/content/ClipDescription;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 630
    monitor-enter p0

    #@1
    .line 631
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@4
    move-result-object v0

    #@5
    iget-object v0, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/content/ClipData;

    #@d
    invoke-virtual {v0}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    #@10
    move-result-object v0

    #@11
    monitor-exit p0

    #@12
    return-object v0

    #@13
    .line 632
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public hasClipboardText()Z
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 429
    monitor-enter p0

    #@3
    .line 430
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@6
    move-result-object v0

    #@7
    .line 431
    .local v0, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@9
    if-eqz v2, :cond_5c

    #@b
    .line 432
    iget-boolean v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@d
    if-eqz v2, :cond_2b

    #@f
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@11
    if-eqz v2, :cond_2b

    #@13
    .line 433
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@15
    const/4 v5, 0x0

    #@16
    invoke-virtual {v2, v5}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@1d
    move-result-object v1

    #@1e
    .line 434
    .local v1, text:Ljava/lang/CharSequence;
    if-eqz v1, :cond_29

    #@20
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@23
    move-result v2

    #@24
    if-lez v2, :cond_29

    #@26
    move v2, v4

    #@27
    :goto_27
    monitor-exit p0

    #@28
    .line 446
    .end local v1           #text:Ljava/lang/CharSequence;
    :goto_28
    return v2

    #@29
    .restart local v1       #text:Ljava/lang/CharSequence;
    :cond_29
    move v2, v3

    #@2a
    .line 434
    goto :goto_27

    #@2b
    .line 436
    .end local v1           #text:Ljava/lang/CharSequence;
    :cond_2b
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_78

    #@33
    .line 437
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@35
    iget-object v5, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v5

    #@3b
    add-int/lit8 v5, v5, -0x1

    #@3d
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v2

    #@41
    check-cast v2, Landroid/content/ClipData;

    #@43
    const/4 v5, 0x0

    #@44
    invoke-virtual {v2, v5}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@4b
    move-result-object v1

    #@4c
    .line 438
    .restart local v1       #text:Ljava/lang/CharSequence;
    if-eqz v1, :cond_5a

    #@4e
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@51
    move-result v2

    #@52
    if-lez v2, :cond_5a

    #@54
    move v2, v4

    #@55
    :goto_55
    monitor-exit p0

    #@56
    goto :goto_28

    #@57
    .line 447
    .end local v0           #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    .end local v1           #text:Ljava/lang/CharSequence;
    :catchall_57
    move-exception v2

    #@58
    monitor-exit p0
    :try_end_59
    .catchall {:try_start_3 .. :try_end_59} :catchall_57

    #@59
    throw v2

    #@5a
    .restart local v0       #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    .restart local v1       #text:Ljava/lang/CharSequence;
    :cond_5a
    move v2, v3

    #@5b
    .line 438
    goto :goto_55

    #@5c
    .line 441
    .end local v1           #text:Ljava/lang/CharSequence;
    :cond_5c
    :try_start_5c
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@5e
    if-eqz v2, :cond_78

    #@60
    .line 442
    iget-object v2, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@62
    const/4 v5, 0x0

    #@63
    invoke-virtual {v2, v5}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@6a
    move-result-object v1

    #@6b
    .line 443
    .restart local v1       #text:Ljava/lang/CharSequence;
    if-eqz v1, :cond_76

    #@6d
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@70
    move-result v2

    #@71
    if-lez v2, :cond_76

    #@73
    move v2, v4

    #@74
    :goto_74
    monitor-exit p0

    #@75
    goto :goto_28

    #@76
    :cond_76
    move v2, v3

    #@77
    goto :goto_74

    #@78
    .line 446
    .end local v1           #text:Ljava/lang/CharSequence;
    :cond_78
    monitor-exit p0
    :try_end_79
    .catchall {:try_start_5c .. :try_end_79} :catchall_57

    #@79
    move v2, v3

    #@7a
    goto :goto_28
.end method

.method public hasPrimaryClip()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 374
    const-string v2, "3LM_cliptray"

    #@4
    const-string v3, "hasPrimaryClip() 1"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 375
    monitor-enter p0

    #@a
    .line 393
    :try_start_a
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@c
    if-eqz v2, :cond_59

    #@e
    .line 394
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@11
    move-result-object v2

    #@12
    iget-boolean v2, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@14
    if-eqz v2, :cond_22

    #@16
    .line 395
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@19
    move-result-object v2

    #@1a
    iget-object v2, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@1c
    if-eqz v2, :cond_20

    #@1e
    :goto_1e
    monitor-exit p0

    #@1f
    .line 412
    :goto_1f
    return v0

    #@20
    :cond_20
    move v0, v1

    #@21
    .line 395
    goto :goto_1e

    #@22
    .line 398
    :cond_22
    sget-boolean v2, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@24
    if-eqz v2, :cond_42

    #@26
    invoke-virtual {p0}, Lcom/android/server/ClipboardService;->isUidSecure()Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_42

    #@2c
    .line 399
    const-string v2, "3LM_cliptray"

    #@2e
    const-string v3, "hasPrimaryClip() 5"

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 400
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@36
    move-result-object v2

    #@37
    iget-object v2, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->mdmClip:Landroid/content/ClipData;

    #@39
    if-eqz v2, :cond_40

    #@3b
    :goto_3b
    monitor-exit p0

    #@3c
    goto :goto_1f

    #@3d
    .line 413
    :catchall_3d
    move-exception v1

    #@3e
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_a .. :try_end_3f} :catchall_3d

    #@3f
    throw v1

    #@40
    :cond_40
    move v0, v1

    #@41
    .line 400
    goto :goto_3b

    #@42
    .line 402
    :cond_42
    :try_start_42
    const-string v2, "3LM_cliptray"

    #@44
    const-string v3, "hasPrimaryClip() 6"

    #@46
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 403
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@4c
    move-result-object v2

    #@4d
    iget-object v2, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@4f
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@52
    move-result v2

    #@53
    if-nez v2, :cond_57

    #@55
    :goto_55
    monitor-exit p0

    #@56
    goto :goto_1f

    #@57
    :cond_57
    move v0, v1

    #@58
    goto :goto_55

    #@59
    .line 407
    :cond_59
    const/4 v0, 0x0

    #@5a
    .line 408
    .local v0, hasClip:Z
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@5d
    move-result-object v1

    #@5e
    iget-object v1, v1, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@60
    if-eqz v1, :cond_72

    #@62
    .line 409
    iget-object v1, p0, Lcom/android/server/ClipboardService;->mSecurityBridge:Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;

    #@64
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@67
    move-result v2

    #@68
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@6b
    move-result-object v3

    #@6c
    iget-object v3, v3, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@6e
    invoke-virtual {v1, v2, v3}, Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;->approvePasteRequest(ILandroid/content/ClipData;)Z

    #@71
    move-result v0

    #@72
    .line 411
    :cond_72
    const-string v1, "3LM_cliptray"

    #@74
    const-string v2, "hasPrimaryClip() 6"

    #@76
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 412
    monitor-exit p0
    :try_end_7a
    .catchall {:try_start_42 .. :try_end_7a} :catchall_3d

    #@7a
    goto :goto_1f
.end method

.method public initClipboardDB()V
    .registers 10

    #@0
    .prologue
    .line 684
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 685
    .local v1, clipList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ClipData;>;"
    iget-object v7, p0, Lcom/android/server/ClipboardService;->dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

    #@7
    invoke-virtual {v7}, Lcom/lge/cliptray/ClipDBOpenHelper;->getClipdataList()Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    .line 686
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v3

    #@f
    .line 687
    .local v3, cnt:I
    const/4 v5, 0x0

    #@10
    .local v5, j:I
    :goto_10
    if-ge v5, v3, :cond_5f

    #@12
    .line 688
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/content/ClipData;

    #@18
    .line 690
    .local v0, clip:Landroid/content/ClipData;
    monitor-enter p0

    #@19
    .line 691
    if-eqz v0, :cond_2c

    #@1b
    :try_start_1b
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    #@1e
    move-result v7

    #@1f
    if-gtz v7, :cond_2c

    #@21
    .line 692
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@23
    const-string v8, "No items"

    #@25
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v7

    #@29
    .line 712
    :catchall_29
    move-exception v7

    #@2a
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_1b .. :try_end_2b} :catchall_29

    #@2b
    throw v7

    #@2c
    .line 694
    :cond_2c
    :try_start_2c
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@2f
    move-result-object v2

    #@30
    .line 696
    .local v2, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    const/16 v7, 0x14

    #@32
    if-ge v5, v7, :cond_59

    #@34
    .line 697
    iget-object v7, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 699
    iget-object v7, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@3b
    invoke-virtual {v7}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_3e
    .catchall {:try_start_2c .. :try_end_3e} :catchall_29

    #@3e
    move-result v6

    #@3f
    .line 700
    .local v6, n:I
    const/4 v4, 0x0

    #@40
    .local v4, i:I
    :goto_40
    if-ge v4, v6, :cond_50

    #@42
    .line 702
    :try_start_42
    iget-object v7, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@44
    invoke-virtual {v7, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@47
    move-result-object v7

    #@48
    check-cast v7, Landroid/content/IOnPrimaryClipChangedListener;

    #@4a
    invoke-interface {v7}, Landroid/content/IOnPrimaryClipChangedListener;->dispatchPrimaryClipChanged()V
    :try_end_4d
    .catchall {:try_start_42 .. :try_end_4d} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_42 .. :try_end_4d} :catch_60

    #@4d
    .line 700
    :goto_4d
    add-int/lit8 v4, v4, 0x1

    #@4f
    goto :goto_40

    #@50
    .line 708
    :cond_50
    :try_start_50
    iget-object v7, v2, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@52
    invoke-virtual {v7}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@55
    .line 712
    .end local v4           #i:I
    .end local v6           #n:I
    :goto_55
    monitor-exit p0

    #@56
    .line 687
    add-int/lit8 v5, v5, 0x1

    #@58
    goto :goto_10

    #@59
    .line 710
    :cond_59
    iget-object v7, p0, Lcom/android/server/ClipboardService;->dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

    #@5b
    invoke-virtual {v7, v5}, Lcom/lge/cliptray/ClipDBOpenHelper;->deleteGlobalPosition(I)V
    :try_end_5e
    .catchall {:try_start_50 .. :try_end_5e} :catchall_29

    #@5e
    goto :goto_55

    #@5f
    .line 714
    .end local v0           #clip:Landroid/content/ClipData;
    .end local v2           #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    :cond_5f
    return-void

    #@60
    .line 703
    .restart local v0       #clip:Landroid/content/ClipData;
    .restart local v2       #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    .restart local v4       #i:I
    .restart local v6       #n:I
    :catch_60
    move-exception v7

    #@61
    goto :goto_4d
.end method

.method public isUidSecure()Z
    .registers 12

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 203
    const-string v8, "persist.security.3lm.activated"

    #@4
    invoke-static {v8, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v8

    #@8
    if-ne v8, v7, :cond_b

    #@a
    .line 216
    :cond_a
    :goto_a
    return v6

    #@b
    .line 207
    :cond_b
    const-string v8, "3LM_cliptray"

    #@d
    new-instance v9, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v10, "isUidSecure(), uid: "

    #@14
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v9

    #@18
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1b
    move-result v10

    #@1c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v9

    #@20
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v9

    #@24
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 208
    invoke-static {}, Lcom/android/server/DeviceManager3LMService;->getInstance()Lcom/android/server/DeviceManager3LMService;

    #@2a
    move-result-object v1

    #@2b
    .line 209
    .local v1, dm:Lcom/android/server/DeviceManager3LMService;
    iget-object v8, p0, Lcom/android/server/ClipboardService;->mPm:Landroid/content/pm/PackageManager;

    #@2d
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@30
    move-result v9

    #@31
    invoke-virtual {v8, v9}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    .line 210
    .local v4, packages:[Ljava/lang/String;
    move-object v0, v4

    #@36
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@37
    .local v3, len$:I
    const/4 v2, 0x0

    #@38
    .local v2, i$:I
    :goto_38
    if-ge v2, v3, :cond_a

    #@3a
    aget-object v5, v0, v2

    #@3c
    .line 211
    .local v5, pkg:Ljava/lang/String;
    const-string v8, "3LM_cliptray"

    #@3e
    new-instance v9, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v10, "isUidSecure(), pkg: "

    #@45
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v9

    #@49
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v9

    #@4d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v9

    #@51
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 212
    invoke-virtual {v1, v5}, Lcom/android/server/DeviceManager3LMService;->isPackageSecure(Ljava/lang/String;)Z

    #@57
    move-result v8

    #@58
    if-eqz v8, :cond_5c

    #@5a
    move v6, v7

    #@5b
    .line 213
    goto :goto_a

    #@5c
    .line 210
    :cond_5c
    add-int/lit8 v2, v2, 0x1

    #@5e
    goto :goto_38
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 172
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/content/IClipboard$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result v1

    #@4
    return v1

    #@5
    .line 173
    :catch_5
    move-exception v0

    #@6
    .line 174
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "clipboard"

    #@8
    const-string v2, "Exception: "

    #@a
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d
    .line 175
    throw v0
.end method

.method public removeAllPrimaryClips(Ljava/lang/String;)Z
    .registers 4
    .parameter "pkg"

    #@0
    .prologue
    .line 666
    monitor-enter p0

    #@1
    .line 667
    :try_start_1
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v1

    #@5
    invoke-direct {p0, v1, p1}, Lcom/android/server/ClipboardService;->addActiveOwnerLocked(ILjava/lang/String;)V

    #@8
    .line 668
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@b
    move-result-object v0

    #@c
    .line 670
    .local v0, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_21

    #@14
    .line 671
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@19
    .line 672
    iget-object v1, p0, Lcom/android/server/ClipboardService;->dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

    #@1b
    invoke-virtual {v1}, Lcom/lge/cliptray/ClipDBOpenHelper;->deleteAll()Z

    #@1e
    .line 673
    const/4 v1, 0x1

    #@1f
    monitor-exit p0

    #@20
    .line 675
    :goto_20
    return v1

    #@21
    :cond_21
    const/4 v1, 0x0

    #@22
    monitor-exit p0

    #@23
    goto :goto_20

    #@24
    .line 676
    .end local v0           #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    :catchall_24
    move-exception v1

    #@25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_1 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public removePrimaryClipAt(Ljava/lang/String;I)Z
    .registers 5
    .parameter "pkg"
    .parameter "index"

    #@0
    .prologue
    .line 648
    monitor-enter p0

    #@1
    .line 649
    :try_start_1
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v1

    #@5
    invoke-direct {p0, v1, p1}, Lcom/android/server/ClipboardService;->addActiveOwnerLocked(ILjava/lang/String;)V

    #@8
    .line 651
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@b
    move-result-object v0

    #@c
    .line 653
    .local v0, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_29

    #@14
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v1

    #@1a
    if-le v1, p2, :cond_29

    #@1c
    .line 654
    iget-object v1, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@21
    .line 655
    iget-object v1, p0, Lcom/android/server/ClipboardService;->dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

    #@23
    invoke-virtual {v1, p2}, Lcom/lge/cliptray/ClipDBOpenHelper;->deleteGlobalPosition(I)V

    #@26
    .line 656
    const/4 v1, 0x1

    #@27
    monitor-exit p0

    #@28
    .line 658
    :goto_28
    return v1

    #@29
    :cond_29
    const/4 v1, 0x0

    #@2a
    monitor-exit p0

    #@2b
    goto :goto_28

    #@2c
    .line 659
    .end local v0           #clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    :catchall_2c
    move-exception v1

    #@2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_1 .. :try_end_2e} :catchall_2c

    #@2e
    throw v1
.end method

.method public removePrimaryClipChangedListener(Landroid/content/IOnPrimaryClipChangedListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 423
    monitor-enter p0

    #@1
    .line 424
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@4
    move-result-object v0

    #@5
    iget-object v0, v0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@7
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@a
    .line 425
    monitor-exit p0

    #@b
    .line 426
    return-void

    #@c
    .line 425
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public setPrimaryClip(Landroid/content/ClipData;)V
    .registers 14
    .parameter "clip"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 222
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v0

    #@5
    if-eqz v0, :cond_12

    #@7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v0

    #@b
    invoke-interface {v0, v4}, Lcom/lge/cappuccino/IMdm;->checkDisabledClipboard(Z)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_12

    #@11
    .line 311
    :goto_11
    return-void

    #@12
    .line 228
    :cond_12
    monitor-enter p0

    #@13
    .line 229
    if-eqz p1, :cond_26

    #@15
    :try_start_15
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    #@18
    move-result v0

    #@19
    if-gtz v0, :cond_26

    #@1b
    .line 230
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v4, "No items"

    #@1f
    invoke-direct {v0, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 310
    :catchall_23
    move-exception v0

    #@24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_15 .. :try_end_25} :catchall_23

    #@25
    throw v0

    #@26
    .line 233
    :cond_26
    :try_start_26
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@29
    move-result v0

    #@2a
    invoke-direct {p0, p1, v0}, Lcom/android/server/ClipboardService;->checkDataOwnerLocked(Landroid/content/ClipData;I)V

    #@2d
    .line 234
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->clearActiveOwnersLocked()V

    #@30
    .line 236
    sget-boolean v0, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@32
    if-eqz v0, :cond_46

    #@34
    .line 238
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@37
    move-result v11

    #@38
    .line 239
    .local v11, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3b
    move-result-wide v9

    #@3c
    .line 242
    .local v9, token:J
    invoke-static {}, Lcom/android/server/DeviceManager3LMService;->getInstance()Lcom/android/server/DeviceManager3LMService;

    #@3f
    move-result-object v7

    #@40
    .line 243
    .local v7, dm:Lcom/android/server/DeviceManager3LMService;
    invoke-virtual {v7, v11}, Lcom/android/server/DeviceManager3LMService;->setPrimaryClipOwner(I)V

    #@43
    .line 245
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@46
    .line 248
    .end local v7           #dm:Lcom/android/server/DeviceManager3LMService;
    .end local v9           #token:J
    .end local v11           #uid:I
    :cond_46
    invoke-direct {p0}, Lcom/android/server/ClipboardService;->getClipboard()Lcom/android/server/ClipboardService$PerUserClipboard;

    #@49
    move-result-object v6

    #@4a
    .line 250
    .local v6, clipboard:Lcom/android/server/ClipboardService$PerUserClipboard;
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@4c
    if-eqz v0, :cond_d5

    #@4e
    .line 251
    if-nez p1, :cond_5e

    #@50
    .line 252
    const-string v0, "ClipboardService"

    #@52
    const-string v4, "Cliptray::setPrimaryClip, clip is null - add to clipdata"

    #@54
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 253
    iput-object p1, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@59
    .line 254
    const/4 v0, 0x1

    #@5a
    iput-boolean v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@5c
    .line 255
    monitor-exit p0

    #@5d
    goto :goto_11

    #@5e
    .line 257
    :cond_5e
    const/4 v2, 0x0

    #@5f
    .local v2, i:I
    :goto_5f
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    #@62
    move-result v0

    #@63
    if-ge v2, v0, :cond_a5

    #@65
    .line 258
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@68
    move-result-object v0

    #@69
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    #@6c
    move-result-object v0

    #@6d
    if-nez v0, :cond_93

    #@6f
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@76
    move-result-object v0

    #@77
    if-eqz v0, :cond_89

    #@79
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@80
    move-result-object v0

    #@81
    const-string v4, ""

    #@83
    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v0

    #@87
    if-eqz v0, :cond_a2

    #@89
    :cond_89
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@90
    move-result-object v0

    #@91
    if-nez v0, :cond_a2

    #@93
    .line 259
    :cond_93
    iput-object p1, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@95
    .line 260
    const/4 v0, 0x1

    #@96
    iput-boolean v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@98
    .line 261
    const-string v0, "ClipboardService"

    #@9a
    const-string v4, "Cliptray::setPrimaryClip, clip is not valid - add to clipdata"

    #@9c
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    .line 262
    monitor-exit p0

    #@a0
    goto/16 :goto_11

    #@a2
    .line 257
    :cond_a2
    add-int/lit8 v2, v2, 0x1

    #@a4
    goto :goto_5f

    #@a5
    .line 266
    :cond_a5
    sget-boolean v0, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@a7
    if-eqz v0, :cond_cf

    #@a9
    .line 267
    invoke-virtual {p0}, Lcom/android/server/ClipboardService;->isUidSecure()Z

    #@ac
    move-result v0

    #@ad
    if-nez v0, :cond_b4

    #@af
    .line 268
    iget-object v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@b1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b4
    .line 270
    :cond_b4
    iput-object p1, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->mdmClip:Landroid/content/ClipData;

    #@b6
    .line 275
    :goto_b6
    const/4 v0, 0x0

    #@b7
    iput-boolean v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@b9
    .line 280
    .end local v2           #i:I
    :goto_b9
    iget-object v0, p0, Lcom/android/server/ClipboardService;->mSecurityBridge:Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;

    #@bb
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@be
    move-result v4

    #@bf
    invoke-virtual {v0, v4, p1}, Lcom/android/services/SecurityBridge/api/ClipboardManagerMonitor;->notifyCopy(ILandroid/content/ClipData;)V

    #@c2
    .line 283
    sget-boolean v0, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@c4
    if-eqz v0, :cond_d8

    #@c6
    invoke-virtual {p0}, Lcom/android/server/ClipboardService;->isUidSecure()Z

    #@c9
    move-result v0

    #@ca
    if-eqz v0, :cond_d8

    #@cc
    monitor-exit p0

    #@cd
    goto/16 :goto_11

    #@cf
    .line 272
    .restart local v2       #i:I
    :cond_cf
    iget-object v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@d1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d4
    goto :goto_b6

    #@d5
    .line 277
    .end local v2           #i:I
    :cond_d5
    iput-object p1, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClip:Landroid/content/ClipData;

    #@d7
    goto :goto_b9

    #@d8
    .line 285
    :cond_d8
    iget-object v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@da
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_dd
    .catchall {:try_start_26 .. :try_end_dd} :catchall_23

    #@dd
    move-result v8

    #@de
    .line 286
    .local v8, n:I
    const/4 v2, 0x0

    #@df
    .restart local v2       #i:I
    :goto_df
    if-ge v2, v8, :cond_ef

    #@e1
    .line 288
    :try_start_e1
    iget-object v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@e3
    invoke-virtual {v0, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@e6
    move-result-object v0

    #@e7
    check-cast v0, Landroid/content/IOnPrimaryClipChangedListener;

    #@e9
    invoke-interface {v0}, Landroid/content/IOnPrimaryClipChangedListener;->dispatchPrimaryClipChanged()V
    :try_end_ec
    .catchall {:try_start_e1 .. :try_end_ec} :catchall_23
    .catch Landroid/os/RemoteException; {:try_start_e1 .. :try_end_ec} :catch_146

    #@ec
    .line 286
    :goto_ec
    add-int/lit8 v2, v2, 0x1

    #@ee
    goto :goto_df

    #@ef
    .line 295
    :cond_ef
    :try_start_ef
    iget-object v0, v6, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@f1
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@f4
    .line 297
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@f6
    if-eqz v0, :cond_143

    #@f8
    .line 298
    invoke-virtual {p0}, Lcom/android/server/ClipboardService;->getPrimaryClipCount()I

    #@fb
    move-result v0

    #@fc
    add-int/lit8 v1, v0, -0x1

    #@fe
    .line 300
    .local v1, clipIndex:I
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    #@101
    move-result v3

    #@102
    .line 301
    .local v3, cnt:I
    const/4 v2, 0x0

    #@103
    :goto_103
    if-ge v2, v3, :cond_143

    #@105
    .line 302
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@108
    move-result-object v0

    #@109
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@10c
    move-result-object v0

    #@10d
    if-eqz v0, :cond_125

    #@10f
    .line 303
    iget-object v0, p0, Lcom/android/server/ClipboardService;->dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

    #@111
    const-string v4, "text"

    #@113
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@116
    move-result-object v5

    #@117
    invoke-virtual {v5}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@11a
    move-result-object v5

    #@11b
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@11e
    move-result-object v5

    #@11f
    invoke-virtual/range {v0 .. v5}, Lcom/lge/cliptray/ClipDBOpenHelper;->insert(IIILjava/lang/String;Ljava/lang/String;)Z

    #@122
    .line 301
    :cond_122
    :goto_122
    add-int/lit8 v2, v2, 0x1

    #@124
    goto :goto_103

    #@125
    .line 304
    :cond_125
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@128
    move-result-object v0

    #@129
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@12c
    move-result-object v0

    #@12d
    if-eqz v0, :cond_122

    #@12f
    .line 305
    iget-object v0, p0, Lcom/android/server/ClipboardService;->dbHelper:Lcom/lge/cliptray/ClipDBOpenHelper;

    #@131
    const-string v4, "img"

    #@133
    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@136
    move-result-object v5

    #@137
    invoke-virtual {v5}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@13a
    move-result-object v5

    #@13b
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@13e
    move-result-object v5

    #@13f
    invoke-virtual/range {v0 .. v5}, Lcom/lge/cliptray/ClipDBOpenHelper;->insert(IIILjava/lang/String;Ljava/lang/String;)Z

    #@142
    goto :goto_122

    #@143
    .line 310
    .end local v1           #clipIndex:I
    .end local v3           #cnt:I
    :cond_143
    monitor-exit p0
    :try_end_144
    .catchall {:try_start_ef .. :try_end_144} :catchall_23

    #@144
    goto/16 :goto_11

    #@146
    .line 289
    :catch_146
    move-exception v0

    #@147
    goto :goto_ec
.end method
