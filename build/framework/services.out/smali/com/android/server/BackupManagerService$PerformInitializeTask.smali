.class Lcom/android/server/BackupManagerService$PerformInitializeTask;
.super Ljava/lang/Object;
.source "BackupManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerformInitializeTask"
.end annotation


# instance fields
.field mQueue:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/BackupManagerService;Ljava/util/HashSet;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 4754
    .local p2, transportNames:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4755
    iput-object p2, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->mQueue:Ljava/util/HashSet;

    #@7
    .line 4756
    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    #@0
    .prologue
    .line 4760
    :try_start_0
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->mQueue:Ljava/util/HashSet;

    #@2
    invoke-virtual {v10}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v3

    #@6
    .local v3, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v10

    #@a
    if-eqz v10, :cond_10f

    #@c
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v9

    #@10
    check-cast v9, Ljava/lang/String;

    #@12
    .line 4761
    .local v9, transportName:Ljava/lang/String;
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@14
    invoke-static {v10, v9}, Lcom/android/server/BackupManagerService;->access$100(Lcom/android/server/BackupManagerService;Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@17
    move-result-object v8

    #@18
    .line 4762
    .local v8, transport:Lcom/android/internal/backup/IBackupTransport;
    if-nez v8, :cond_42

    #@1a
    .line 4763
    const-string v10, "BackupManagerService"

    #@1c
    new-instance v11, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v12, "Requested init for "

    #@23
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v11

    #@27
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v11

    #@2b
    const-string v12, " but not found"

    #@2d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v11

    #@31
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v11

    #@35
    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_38
    .catchall {:try_start_0 .. :try_end_38} :catchall_103
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_38} :catch_39
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_38} :catch_c2

    #@38
    goto :goto_6

    #@39
    .line 4802
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v8           #transport:Lcom/android/internal/backup/IBackupTransport;
    .end local v9           #transportName:Ljava/lang/String;
    :catch_39
    move-exception v10

    #@3a
    .line 4808
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3c
    iget-object v10, v10, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@3e
    :goto_3e
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    #@41
    .line 4810
    return-void

    #@42
    .line 4767
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v8       #transport:Lcom/android/internal/backup/IBackupTransport;
    .restart local v9       #transportName:Ljava/lang/String;
    :cond_42
    :try_start_42
    const-string v10, "BackupManagerService"

    #@44
    new-instance v11, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v12, "Initializing (wiping) backup transport storage: "

    #@4b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v11

    #@4f
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v11

    #@53
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v11

    #@57
    invoke-static {v10, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 4768
    const/16 v10, 0xb05

    #@5c
    invoke-interface {v8}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@5f
    move-result-object v11

    #@60
    invoke-static {v10, v11}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@63
    .line 4769
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@66
    move-result-wide v5

    #@67
    .line 4770
    .local v5, startRealtime:J
    invoke-interface {v8}, Lcom/android/internal/backup/IBackupTransport;->initializeDevice()I

    #@6a
    move-result v7

    #@6b
    .line 4772
    .local v7, status:I
    if-nez v7, :cond_71

    #@6d
    .line 4773
    invoke-interface {v8}, Lcom/android/internal/backup/IBackupTransport;->finishBackup()I

    #@70
    move-result v7

    #@71
    .line 4777
    :cond_71
    if-nez v7, :cond_d0

    #@73
    .line 4778
    const-string v10, "BackupManagerService"

    #@75
    const-string v11, "Device init successful"

    #@77
    invoke-static {v10, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 4779
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@7d
    move-result-wide v10

    #@7e
    sub-long/2addr v10, v5

    #@7f
    long-to-int v4, v10

    #@80
    .line 4780
    .local v4, millis:I
    const/16 v10, 0xb0b

    #@82
    const/4 v11, 0x0

    #@83
    new-array v11, v11, [Ljava/lang/Object;

    #@85
    invoke-static {v10, v11}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@88
    .line 4781
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@8a
    new-instance v11, Ljava/io/File;

    #@8c
    iget-object v12, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@8e
    iget-object v12, v12, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@90
    invoke-interface {v8}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@93
    move-result-object v13

    #@94
    invoke-direct {v11, v12, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@97
    invoke-virtual {v10, v11}, Lcom/android/server/BackupManagerService;->resetBackupState(Ljava/io/File;)V

    #@9a
    .line 4782
    const/16 v10, 0xb09

    #@9c
    const/4 v11, 0x2

    #@9d
    new-array v11, v11, [Ljava/lang/Object;

    #@9f
    const/4 v12, 0x0

    #@a0
    const/4 v13, 0x0

    #@a1
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a4
    move-result-object v13

    #@a5
    aput-object v13, v11, v12

    #@a7
    const/4 v12, 0x1

    #@a8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ab
    move-result-object v13

    #@ac
    aput-object v13, v11, v12

    #@ae
    invoke-static {v10, v11}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@b1
    .line 4783
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@b3
    iget-object v11, v10, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@b5
    monitor-enter v11
    :try_end_b6
    .catchall {:try_start_42 .. :try_end_b6} :catchall_103
    .catch Landroid/os/RemoteException; {:try_start_42 .. :try_end_b6} :catch_39
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_b6} :catch_c2

    #@b6
    .line 4784
    :try_start_b6
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@b8
    const/4 v12, 0x0

    #@b9
    invoke-virtual {v10, v12, v9}, Lcom/android/server/BackupManagerService;->recordInitPendingLocked(ZLjava/lang/String;)V

    #@bc
    .line 4785
    monitor-exit v11

    #@bd
    goto/16 :goto_6

    #@bf
    :catchall_bf
    move-exception v10

    #@c0
    monitor-exit v11
    :try_end_c1
    .catchall {:try_start_b6 .. :try_end_c1} :catchall_bf

    #@c1
    :try_start_c1
    throw v10
    :try_end_c2
    .catchall {:try_start_c1 .. :try_end_c2} :catchall_103
    .catch Landroid/os/RemoteException; {:try_start_c1 .. :try_end_c2} :catch_39
    .catch Ljava/lang/Exception; {:try_start_c1 .. :try_end_c2} :catch_c2

    #@c2
    .line 4804
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #millis:I
    .end local v5           #startRealtime:J
    .end local v7           #status:I
    .end local v8           #transport:Lcom/android/internal/backup/IBackupTransport;
    .end local v9           #transportName:Ljava/lang/String;
    :catch_c2
    move-exception v2

    #@c3
    .line 4805
    .local v2, e:Ljava/lang/Exception;
    :try_start_c3
    const-string v10, "BackupManagerService"

    #@c5
    const-string v11, "Unexpected error performing init"

    #@c7
    invoke-static {v10, v11, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ca
    .catchall {:try_start_c3 .. :try_end_ca} :catchall_103

    #@ca
    .line 4808
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@cc
    iget-object v10, v10, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@ce
    goto/16 :goto_3e

    #@d0
    .line 4789
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v5       #startRealtime:J
    .restart local v7       #status:I
    .restart local v8       #transport:Lcom/android/internal/backup/IBackupTransport;
    .restart local v9       #transportName:Ljava/lang/String;
    :cond_d0
    :try_start_d0
    const-string v10, "BackupManagerService"

    #@d2
    const-string v11, "Transport error in initializeDevice()"

    #@d4
    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    .line 4790
    const/16 v10, 0xb06

    #@d9
    const-string v11, "(initialize)"

    #@db
    invoke-static {v10, v11}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@de
    .line 4791
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e0
    iget-object v11, v10, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@e2
    monitor-enter v11
    :try_end_e3
    .catchall {:try_start_d0 .. :try_end_e3} :catchall_103
    .catch Landroid/os/RemoteException; {:try_start_d0 .. :try_end_e3} :catch_39
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_e3} :catch_c2

    #@e3
    .line 4792
    :try_start_e3
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e5
    const/4 v12, 0x1

    #@e6
    invoke-virtual {v10, v12, v9}, Lcom/android/server/BackupManagerService;->recordInitPendingLocked(ZLjava/lang/String;)V

    #@e9
    .line 4793
    monitor-exit v11
    :try_end_ea
    .catchall {:try_start_e3 .. :try_end_ea} :catchall_10c

    #@ea
    .line 4795
    :try_start_ea
    invoke-interface {v8}, Lcom/android/internal/backup/IBackupTransport;->requestBackupTime()J

    #@ed
    move-result-wide v0

    #@ee
    .line 4798
    .local v0, delay:J
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@f0
    invoke-static {v10}, Lcom/android/server/BackupManagerService;->access$400(Lcom/android/server/BackupManagerService;)Landroid/app/AlarmManager;

    #@f3
    move-result-object v10

    #@f4
    const/4 v11, 0x0

    #@f5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f8
    move-result-wide v12

    #@f9
    add-long/2addr v12, v0

    #@fa
    iget-object v14, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@fc
    iget-object v14, v14, Lcom/android/server/BackupManagerService;->mRunInitIntent:Landroid/app/PendingIntent;

    #@fe
    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_101
    .catchall {:try_start_ea .. :try_end_101} :catchall_103
    .catch Landroid/os/RemoteException; {:try_start_ea .. :try_end_101} :catch_39
    .catch Ljava/lang/Exception; {:try_start_ea .. :try_end_101} :catch_c2

    #@101
    goto/16 :goto_6

    #@103
    .line 4808
    .end local v0           #delay:J
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v5           #startRealtime:J
    .end local v7           #status:I
    .end local v8           #transport:Lcom/android/internal/backup/IBackupTransport;
    .end local v9           #transportName:Ljava/lang/String;
    :catchall_103
    move-exception v10

    #@104
    iget-object v11, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@106
    iget-object v11, v11, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@108
    invoke-virtual {v11}, Landroid/os/PowerManager$WakeLock;->release()V

    #@10b
    throw v10

    #@10c
    .line 4793
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v5       #startRealtime:J
    .restart local v7       #status:I
    .restart local v8       #transport:Lcom/android/internal/backup/IBackupTransport;
    .restart local v9       #transportName:Ljava/lang/String;
    :catchall_10c
    move-exception v10

    #@10d
    :try_start_10d
    monitor-exit v11
    :try_end_10e
    .catchall {:try_start_10d .. :try_end_10e} :catchall_10c

    #@10e
    :try_start_10e
    throw v10
    :try_end_10f
    .catchall {:try_start_10e .. :try_end_10f} :catchall_103
    .catch Landroid/os/RemoteException; {:try_start_10e .. :try_end_10f} :catch_39
    .catch Ljava/lang/Exception; {:try_start_10e .. :try_end_10f} :catch_c2

    #@10f
    .line 4808
    .end local v5           #startRealtime:J
    .end local v7           #status:I
    .end local v8           #transport:Lcom/android/internal/backup/IBackupTransport;
    .end local v9           #transportName:Ljava/lang/String;
    :cond_10f
    iget-object v10, p0, Lcom/android/server/BackupManagerService$PerformInitializeTask;->this$0:Lcom/android/server/BackupManagerService;

    #@111
    iget-object v10, v10, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@113
    goto/16 :goto_3e
.end method
