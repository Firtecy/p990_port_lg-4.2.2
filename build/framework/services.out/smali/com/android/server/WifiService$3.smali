.class Lcom/android/server/WifiService$3;
.super Landroid/content/BroadcastReceiver;
.source "WifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/WifiService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/WifiService;


# direct methods
.method constructor <init>(Lcom/android/server/WifiService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 628
    iput-object p1, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 632
    const-string v1, "IMS_LOWSIGNAL_STATUS"

    #@3
    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    .line 635
    .local v0, lowSignal:Z
    if-nez v0, :cond_20

    #@9
    iget-object v1, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@b
    invoke-static {v1}, Lcom/android/server/WifiService;->access$2300(Lcom/android/server/WifiService;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_20

    #@11
    iget-object v1, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@13
    invoke-static {v1}, Lcom/android/server/WifiService;->access$2400(Lcom/android/server/WifiService;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_20

    #@19
    .line 636
    iget-object v1, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@1b
    const-wide/16 v2, 0x0

    #@1d
    invoke-static {v1, v2, v3}, Lcom/android/server/WifiService;->access$2502(Lcom/android/server/WifiService;J)J

    #@20
    .line 637
    :cond_20
    iget-object v1, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@22
    invoke-static {v1, v0}, Lcom/android/server/WifiService;->access$2302(Lcom/android/server/WifiService;Z)Z

    #@25
    .line 638
    iget-object v1, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@27
    invoke-static {v1}, Lcom/android/server/WifiService;->access$2400(Lcom/android/server/WifiService;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_39

    #@2d
    iget-object v1, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@2f
    const/4 v2, 0x1

    #@30
    iget-object v3, p0, Lcom/android/server/WifiService$3;->this$0:Lcom/android/server/WifiService;

    #@32
    invoke-static {v3}, Lcom/android/server/WifiService;->access$2600(Lcom/android/server/WifiService;)I

    #@35
    move-result v3

    #@36
    invoke-static {v1, v2, v3, v4, v4}, Lcom/android/server/WifiService;->access$2700(Lcom/android/server/WifiService;ZIZI)V

    #@39
    .line 639
    :cond_39
    return-void
.end method
