.class public Lcom/android/server/EntropyMixer;
.super Landroid/os/Binder;
.source "EntropyMixer.java"


# static fields
.field private static final ENTROPY_WHAT:I = 0x1

.field private static final ENTROPY_WRITE_PERIOD:I = 0xa4cb80

#the value of this static final field might be set in the static constructor
.field private static final START_NANOTIME:J = 0x0L

#the value of this static final field might be set in the static constructor
.field private static final START_TIME:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "EntropyMixer"


# instance fields
.field private final entropyFile:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private final randomDevice:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    sput-wide v0, Lcom/android/server/EntropyMixer;->START_TIME:J

    #@6
    .line 56
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@9
    move-result-wide v0

    #@a
    sput-wide v0, Lcom/android/server/EntropyMixer;->START_NANOTIME:J

    #@c
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {}, Lcom/android/server/EntropyMixer;->getSystemDir()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "/entropy.dat"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    const-string v1, "/dev/urandom"

    #@19
    invoke-direct {p0, v0, v1}, Lcom/android/server/EntropyMixer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "entropyFile"
    .parameter "randomDevice"

    #@0
    .prologue
    .line 81
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 64
    new-instance v0, Lcom/android/server/EntropyMixer$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/server/EntropyMixer$1;-><init>(Lcom/android/server/EntropyMixer;)V

    #@8
    iput-object v0, p0, Lcom/android/server/EntropyMixer;->mHandler:Landroid/os/Handler;

    #@a
    .line 82
    if-nez p2, :cond_14

    #@c
    new-instance v0, Ljava/lang/NullPointerException;

    #@e
    const-string v1, "randomDevice"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 83
    :cond_14
    if-nez p1, :cond_1e

    #@16
    new-instance v0, Ljava/lang/NullPointerException;

    #@18
    const-string v1, "entropyFile"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 85
    :cond_1e
    iput-object p2, p0, Lcom/android/server/EntropyMixer;->randomDevice:Ljava/lang/String;

    #@20
    .line 86
    iput-object p1, p0, Lcom/android/server/EntropyMixer;->entropyFile:Ljava/lang/String;

    #@22
    .line 87
    invoke-direct {p0}, Lcom/android/server/EntropyMixer;->loadInitialEntropy()V

    #@25
    .line 88
    invoke-direct {p0}, Lcom/android/server/EntropyMixer;->addDeviceSpecificEntropy()V

    #@28
    .line 89
    invoke-direct {p0}, Lcom/android/server/EntropyMixer;->writeEntropy()V

    #@2b
    .line 90
    invoke-direct {p0}, Lcom/android/server/EntropyMixer;->scheduleEntropyWriter()V

    #@2e
    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/EntropyMixer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Lcom/android/server/EntropyMixer;->writeEntropy()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/EntropyMixer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Lcom/android/server/EntropyMixer;->scheduleEntropyWriter()V

    #@3
    return-void
.end method

.method private addDeviceSpecificEntropy()V
    .registers 6

    #@0
    .prologue
    .line 131
    const/4 v1, 0x0

    #@1
    .line 133
    .local v1, out:Ljava/io/PrintWriter;
    :try_start_1
    new-instance v2, Ljava/io/PrintWriter;

    #@3
    new-instance v3, Ljava/io/FileOutputStream;

    #@5
    iget-object v4, p0, Lcom/android/server/EntropyMixer;->randomDevice:Ljava/lang/String;

    #@7
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@a
    invoke-direct {v2, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_8f
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_d} :catch_81

    #@d
    .line 134
    .end local v1           #out:Ljava/io/PrintWriter;
    .local v2, out:Ljava/io/PrintWriter;
    :try_start_d
    const-string v3, "Copyright (C) 2009 The Android Open Source Project"

    #@f
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@12
    .line 135
    const-string v3, "All Your Randomness Are Belong To Us"

    #@14
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@17
    .line 136
    sget-wide v3, Lcom/android/server/EntropyMixer;->START_TIME:J

    #@19
    invoke-virtual {v2, v3, v4}, Ljava/io/PrintWriter;->println(J)V

    #@1c
    .line 137
    sget-wide v3, Lcom/android/server/EntropyMixer;->START_NANOTIME:J

    #@1e
    invoke-virtual {v2, v3, v4}, Ljava/io/PrintWriter;->println(J)V

    #@21
    .line 138
    const-string v3, "ro.serialno"

    #@23
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a
    .line 139
    const-string v3, "ro.bootmode"

    #@2c
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@33
    .line 140
    const-string v3, "ro.baseband"

    #@35
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3c
    .line 141
    const-string v3, "ro.carrier"

    #@3e
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@45
    .line 142
    const-string v3, "ro.bootloader"

    #@47
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4e
    .line 143
    const-string v3, "ro.hardware"

    #@50
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@57
    .line 144
    const-string v3, "ro.revision"

    #@59
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@60
    .line 145
    new-instance v3, Ljava/lang/Object;

    #@62
    invoke-direct/range {v3 .. v3}, Ljava/lang/Object;-><init>()V

    #@65
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    #@68
    move-result v3

    #@69
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(I)V

    #@6c
    .line 146
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6f
    move-result-wide v3

    #@70
    invoke-virtual {v2, v3, v4}, Ljava/io/PrintWriter;->println(J)V

    #@73
    .line 147
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@76
    move-result-wide v3

    #@77
    invoke-virtual {v2, v3, v4}, Ljava/io/PrintWriter;->println(J)V
    :try_end_7a
    .catchall {:try_start_d .. :try_end_7a} :catchall_96
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_7a} :catch_99

    #@7a
    .line 151
    if-eqz v2, :cond_7f

    #@7c
    .line 152
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    #@7f
    :cond_7f
    move-object v1, v2

    #@80
    .line 155
    .end local v2           #out:Ljava/io/PrintWriter;
    .restart local v1       #out:Ljava/io/PrintWriter;
    :cond_80
    :goto_80
    return-void

    #@81
    .line 148
    :catch_81
    move-exception v0

    #@82
    .line 149
    .local v0, e:Ljava/io/IOException;
    :goto_82
    :try_start_82
    const-string v3, "EntropyMixer"

    #@84
    const-string v4, "Unable to add device specific data to the entropy pool"

    #@86
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_89
    .catchall {:try_start_82 .. :try_end_89} :catchall_8f

    #@89
    .line 151
    if-eqz v1, :cond_80

    #@8b
    .line 152
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    #@8e
    goto :goto_80

    #@8f
    .line 151
    .end local v0           #e:Ljava/io/IOException;
    :catchall_8f
    move-exception v3

    #@90
    :goto_90
    if-eqz v1, :cond_95

    #@92
    .line 152
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    #@95
    .line 151
    :cond_95
    throw v3

    #@96
    .end local v1           #out:Ljava/io/PrintWriter;
    .restart local v2       #out:Ljava/io/PrintWriter;
    :catchall_96
    move-exception v3

    #@97
    move-object v1, v2

    #@98
    .end local v2           #out:Ljava/io/PrintWriter;
    .restart local v1       #out:Ljava/io/PrintWriter;
    goto :goto_90

    #@99
    .line 148
    .end local v1           #out:Ljava/io/PrintWriter;
    .restart local v2       #out:Ljava/io/PrintWriter;
    :catch_99
    move-exception v0

    #@9a
    move-object v1, v2

    #@9b
    .end local v2           #out:Ljava/io/PrintWriter;
    .restart local v1       #out:Ljava/io/PrintWriter;
    goto :goto_82
.end method

.method private static getSystemDir()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 158
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    .line 159
    .local v0, dataDir:Ljava/io/File;
    new-instance v1, Ljava/io/File;

    #@6
    const-string v2, "system"

    #@8
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    .line 160
    .local v1, systemDir:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    #@e
    .line 161
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    return-object v2
.end method

.method private loadInitialEntropy()V
    .registers 5

    #@0
    .prologue
    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/android/server/EntropyMixer;->entropyFile:Ljava/lang/String;

    #@2
    invoke-static {v1}, Lcom/android/server/RandomBlock;->fromFile(Ljava/lang/String;)Lcom/android/server/RandomBlock;

    #@5
    move-result-object v1

    #@6
    iget-object v2, p0, Lcom/android/server/EntropyMixer;->randomDevice:Ljava/lang/String;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v1, v2, v3}, Lcom/android/server/RandomBlock;->toFile(Ljava/lang/String;Z)V
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_c} :catch_d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_c} :catch_16

    #@c
    .line 106
    :goto_c
    return-void

    #@d
    .line 101
    :catch_d
    move-exception v0

    #@e
    .line 102
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v1, "EntropyMixer"

    #@10
    const-string v2, "No existing entropy file -- first boot?"

    #@12
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_c

    #@16
    .line 103
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_16
    move-exception v0

    #@17
    .line 104
    .local v0, e:Ljava/io/IOException;
    const-string v1, "EntropyMixer"

    #@19
    const-string v2, "Failure loading existing entropy file"

    #@1b
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    goto :goto_c
.end method

.method private scheduleEntropyWriter()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 94
    iget-object v0, p0, Lcom/android/server/EntropyMixer;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 95
    iget-object v0, p0, Lcom/android/server/EntropyMixer;->mHandler:Landroid/os/Handler;

    #@8
    const-wide/32 v1, 0xa4cb80

    #@b
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@e
    .line 96
    return-void
.end method

.method private writeEntropy()V
    .registers 5

    #@0
    .prologue
    .line 110
    :try_start_0
    iget-object v1, p0, Lcom/android/server/EntropyMixer;->randomDevice:Ljava/lang/String;

    #@2
    invoke-static {v1}, Lcom/android/server/RandomBlock;->fromFile(Ljava/lang/String;)Lcom/android/server/RandomBlock;

    #@5
    move-result-object v1

    #@6
    iget-object v2, p0, Lcom/android/server/EntropyMixer;->entropyFile:Ljava/lang/String;

    #@8
    const/4 v3, 0x1

    #@9
    invoke-virtual {v1, v2, v3}, Lcom/android/server/RandomBlock;->toFile(Ljava/lang/String;Z)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_c} :catch_d

    #@c
    .line 114
    :goto_c
    return-void

    #@d
    .line 111
    :catch_d
    move-exception v0

    #@e
    .line 112
    .local v0, e:Ljava/io/IOException;
    const-string v1, "EntropyMixer"

    #@10
    const-string v2, "Unable to write entropy"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    goto :goto_c
.end method
