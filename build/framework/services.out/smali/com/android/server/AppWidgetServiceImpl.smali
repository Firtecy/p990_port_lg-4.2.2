.class Lcom/android/server/AppWidgetServiceImpl;
.super Ljava/lang/Object;
.source "AppWidgetServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;,
        Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;,
        Lcom/android/server/AppWidgetServiceImpl$Host;,
        Lcom/android/server/AppWidgetServiceImpl$Provider;
    }
.end annotation


# static fields
.field private static DBG:Z = false

.field private static final MIN_UPDATE_PERIOD:I = 0x1b7740

.field private static final SETTINGS_FILENAME:Ljava/lang/String; = "appwidgets.xml"

.field private static final TAG:Ljava/lang/String; = "AppWidgetServiceImpl"


# instance fields
.field mAlarmManager:Landroid/app/AlarmManager;

.field final mAppWidgetIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;",
            ">;"
        }
    .end annotation
.end field

.field private final mBoundRemoteViewsServices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/content/Intent$FilterComparison;",
            ">;",
            "Landroid/content/ServiceConnection;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;

.field mDeletedHosts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AppWidgetServiceImpl$Host;",
            ">;"
        }
    .end annotation
.end field

.field mDeletedProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AppWidgetServiceImpl$Provider;",
            ">;"
        }
    .end annotation
.end field

.field mHosts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AppWidgetServiceImpl$Host;",
            ">;"
        }
    .end annotation
.end field

.field mInstalledProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AppWidgetServiceImpl$Provider;",
            ">;"
        }
    .end annotation
.end field

.field mLocale:Ljava/util/Locale;

.field mMaxWidgetBitmapMemory:I

.field mNextAppWidgetId:I

.field mPackagesWithBindWidgetPermission:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPm:Landroid/content/pm/IPackageManager;

.field private final mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Intent$FilterComparison;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field mSafeMode:Z

.field private final mSaveStateHandler:Landroid/os/Handler;

.field private final mSaveStateRunnable:Ljava/lang/Runnable;

.field mStateLoaded:Z

.field mUserId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 93
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@3
    return-void
.end method

.method constructor <init>(Landroid/content/Context;ILandroid/os/Handler;)V
    .registers 6
    .parameter "context"
    .parameter "userId"
    .parameter "saveStateHandler"

    #@0
    .prologue
    .line 201
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 176
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@a
    .line 178
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@11
    .line 184
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@18
    .line 185
    const/4 v0, 0x1

    #@19
    iput v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mNextAppWidgetId:I

    #@1b
    .line 186
    new-instance v0, Ljava/util/ArrayList;

    #@1d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@20
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@22
    .line 187
    new-instance v0, Ljava/util/ArrayList;

    #@24
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@29
    .line 189
    new-instance v0, Ljava/util/HashSet;

    #@2b
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@2e
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mPackagesWithBindWidgetPermission:Ljava/util/HashSet;

    #@30
    .line 198
    new-instance v0, Ljava/util/ArrayList;

    #@32
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@35
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedProviders:Ljava/util/ArrayList;

    #@37
    .line 199
    new-instance v0, Ljava/util/ArrayList;

    #@39
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3c
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedHosts:Ljava/util/ArrayList;

    #@3e
    .line 918
    new-instance v0, Lcom/android/server/AppWidgetServiceImpl$2;

    #@40
    invoke-direct {v0, p0}, Lcom/android/server/AppWidgetServiceImpl$2;-><init>(Lcom/android/server/AppWidgetServiceImpl;)V

    #@43
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mSaveStateRunnable:Ljava/lang/Runnable;

    #@45
    .line 202
    iput-object p1, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@47
    .line 203
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@4a
    move-result-object v0

    #@4b
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mPm:Landroid/content/pm/IPackageManager;

    #@4d
    .line 204
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@4f
    const-string v1, "alarm"

    #@51
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@54
    move-result-object v0

    #@55
    check-cast v0, Landroid/app/AlarmManager;

    #@57
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mAlarmManager:Landroid/app/AlarmManager;

    #@59
    .line 205
    iput p2, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@5b
    .line 206
    iput-object p3, p0, Lcom/android/server/AppWidgetServiceImpl;->mSaveStateHandler:Landroid/os/Handler;

    #@5d
    .line 207
    invoke-virtual {p0}, Lcom/android/server/AppWidgetServiceImpl;->computeMaximumWidgetBitmapMemory()V

    #@60
    .line 208
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/AppWidgetServiceImpl;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 87
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@3
    return-void
.end method

.method private bindAppWidgetIdImpl(ILandroid/content/ComponentName;Landroid/os/Bundle;)V
    .registers 14
    .parameter "appWidgetId"
    .parameter "provider"
    .parameter "options"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 559
    sget-boolean v6, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@3
    if-eqz v6, :cond_25

    #@5
    new-instance v6, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v7, "bindAppWidgetIdImpl appwid="

    #@c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    const-string v7, " provider="

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v6

    #@22
    invoke-direct {p0, v6}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@25
    .line 561
    :cond_25
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@28
    move-result-wide v1

    #@29
    .line 563
    .local v1, ident:J
    :try_start_29
    iget-object v7, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2b
    monitor-enter v7
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_44

    #@2c
    .line 564
    :try_start_2c
    invoke-direct {p0, p3}, Lcom/android/server/AppWidgetServiceImpl;->cloneIfLocalBinder(Landroid/os/Bundle;)Landroid/os/Bundle;

    #@2f
    move-result-object p3

    #@30
    .line 565
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@33
    .line 566
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@36
    move-result-object v0

    #@37
    .line 567
    .local v0, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-nez v0, :cond_49

    #@39
    .line 568
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@3b
    const-string v8, "bad appWidgetId"

    #@3d
    invoke-direct {v6, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@40
    throw v6

    #@41
    .line 611
    .end local v0           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_41
    move-exception v6

    #@42
    monitor-exit v7
    :try_end_43
    .catchall {:try_start_2c .. :try_end_43} :catchall_41

    #@43
    :try_start_43
    throw v6
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_44

    #@44
    .line 613
    :catchall_44
    move-exception v6

    #@45
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@48
    throw v6

    #@49
    .line 570
    .restart local v0       #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_49
    :try_start_49
    iget-object v6, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@4b
    if-eqz v6, :cond_76

    #@4d
    .line 571
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@4f
    new-instance v8, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v9, "appWidgetId "

    #@56
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v8

    #@5a
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v8

    #@5e
    const-string v9, " already bound to "

    #@60
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    iget-object v9, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@66
    iget-object v9, v9, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@68
    iget-object v9, v9, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@6a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v8

    #@6e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v8

    #@72
    invoke-direct {v6, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@75
    throw v6

    #@76
    .line 574
    :cond_76
    invoke-virtual {p0, p2}, Lcom/android/server/AppWidgetServiceImpl;->lookupProviderLocked(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@79
    move-result-object v5

    #@7a
    .line 575
    .local v5, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-nez v5, :cond_95

    #@7c
    .line 576
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@7e
    new-instance v8, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v9, "not a appwidget provider: "

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v8

    #@8d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v8

    #@91
    invoke-direct {v6, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@94
    throw v6

    #@95
    .line 578
    :cond_95
    iget-boolean v6, v5, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@97
    if-eqz v6, :cond_b2

    #@99
    .line 579
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@9b
    new-instance v8, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v9, "can\'t bind to a 3rd party provider in safe mode: "

    #@a2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v8

    #@a6
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v8

    #@aa
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v8

    #@ae
    invoke-direct {v6, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b1
    throw v6

    #@b2
    .line 583
    :cond_b2
    iput-object v5, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@b4
    .line 584
    if-nez p3, :cond_bc

    #@b6
    .line 585
    new-instance v4, Landroid/os/Bundle;

    #@b8
    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    #@bb
    .end local p3
    .local v4, options:Landroid/os/Bundle;
    move-object p3, v4

    #@bc
    .line 587
    .end local v4           #options:Landroid/os/Bundle;
    .restart local p3
    :cond_bc
    iput-object p3, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@be
    .line 590
    const-string v6, "appWidgetCategory"

    #@c0
    invoke-virtual {p3, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@c3
    move-result v6

    #@c4
    if-nez v6, :cond_cc

    #@c6
    .line 591
    const-string v6, "appWidgetCategory"

    #@c8
    const/4 v8, 0x1

    #@c9
    invoke-virtual {p3, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@cc
    .line 595
    :cond_cc
    iget-object v6, v5, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@ce
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d1
    .line 596
    iget-object v6, v5, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@d3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@d6
    move-result v3

    #@d7
    .line 597
    .local v3, instancesSize:I
    if-ne v3, v9, :cond_dc

    #@d9
    .line 599
    invoke-virtual {p0, v5}, Lcom/android/server/AppWidgetServiceImpl;->sendEnableIntentLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@dc
    .line 606
    :cond_dc
    const/4 v6, 0x1

    #@dd
    new-array v6, v6, [I

    #@df
    const/4 v8, 0x0

    #@e0
    aput p1, v6, v8

    #@e2
    invoke-virtual {p0, v5, v6}, Lcom/android/server/AppWidgetServiceImpl;->sendUpdateIntentLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V

    #@e5
    .line 609
    invoke-static {v5}, Lcom/android/server/AppWidgetServiceImpl;->getAppWidgetIds(Lcom/android/server/AppWidgetServiceImpl$Provider;)[I

    #@e8
    move-result-object v6

    #@e9
    invoke-virtual {p0, v5, v6}, Lcom/android/server/AppWidgetServiceImpl;->registerForBroadcastsLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V

    #@ec
    .line 610
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@ef
    .line 611
    monitor-exit v7
    :try_end_f0
    .catchall {:try_start_49 .. :try_end_f0} :catchall_41

    #@f0
    .line 613
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@f3
    .line 615
    return-void
.end method

.method private callerHasBindAppWidgetPermission(Ljava/lang/String;)Z
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 637
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v0

    #@5
    .line 639
    .local v0, callingUid:I
    :try_start_5
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->getUidForPackage(Ljava/lang/String;)I

    #@8
    move-result v3

    #@9
    invoke-static {v0, v3}, Landroid/os/UserHandle;->isSameApp(II)Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_c} :catch_10

    #@c
    move-result v3

    #@d
    if-nez v3, :cond_12

    #@f
    .line 647
    :goto_f
    return v2

    #@10
    .line 642
    :catch_10
    move-exception v1

    #@11
    .line 643
    .local v1, e:Ljava/lang/Exception;
    goto :goto_f

    #@12
    .line 645
    .end local v1           #e:Ljava/lang/Exception;
    :cond_12
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@14
    monitor-enter v3

    #@15
    .line 646
    :try_start_15
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@18
    .line 647
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mPackagesWithBindWidgetPermission:Ljava/util/HashSet;

    #@1a
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    monitor-exit v3

    #@1f
    goto :goto_f

    #@20
    .line 648
    :catchall_20
    move-exception v2

    #@21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_15 .. :try_end_22} :catchall_20

    #@22
    throw v2
.end method

.method private cloneIfLocalBinder(Landroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetProviderInfo;
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1129
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->isLocalBinder()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    if-eqz p1, :cond_c

    #@8
    .line 1130
    invoke-virtual {p1}, Landroid/appwidget/AppWidgetProviderInfo;->clone()Landroid/appwidget/AppWidgetProviderInfo;

    #@b
    move-result-object p1

    #@c
    .line 1132
    .end local p1
    :cond_c
    return-object p1
.end method

.method private cloneIfLocalBinder(Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 3
    .parameter "bundle"

    #@0
    .prologue
    .line 1139
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->isLocalBinder()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    if-eqz p1, :cond_f

    #@8
    .line 1140
    invoke-virtual {p1}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/os/Bundle;

    #@e
    .line 1142
    :goto_e
    return-object v0

    #@f
    :cond_f
    move-object v0, p1

    #@10
    goto :goto_e
.end method

.method private cloneIfLocalBinder(Landroid/widget/RemoteViews;)Landroid/widget/RemoteViews;
    .registers 3
    .parameter "rv"

    #@0
    .prologue
    .line 1122
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->isLocalBinder()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    if-eqz p1, :cond_c

    #@8
    .line 1123
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    #@b
    move-result-object p1

    #@c
    .line 1125
    .end local p1
    :cond_c
    return-object p1
.end method

.method private decrementAppWidgetServiceRefCount(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V
    .registers 6
    .parameter "id"

    #@0
    .prologue
    .line 830
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@2
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v3

    #@6
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .line 831
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/Intent$FilterComparison;>;"
    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_3b

    #@10
    .line 832
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/content/Intent$FilterComparison;

    #@16
    .line 833
    .local v2, key:Landroid/content/Intent$FilterComparison;
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@18
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Ljava/util/HashSet;

    #@1e
    .line 834
    .local v0, ids:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    iget v3, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@20
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_a

    #@2a
    .line 837
    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_a

    #@30
    .line 838
    invoke-virtual {v2}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@33
    move-result-object v3

    #@34
    invoke-direct {p0, v3, p1}, Lcom/android/server/AppWidgetServiceImpl;->destroyRemoteViewsService(Landroid/content/Intent;Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V

    #@37
    .line 839
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@3a
    goto :goto_a

    #@3b
    .line 843
    .end local v0           #ids:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v2           #key:Landroid/content/Intent$FilterComparison;
    :cond_3b
    return-void
.end method

.method private destroyRemoteViewsService(Landroid/content/Intent;Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V
    .registers 9
    .parameter "intent"
    .parameter "id"

    #@0
    .prologue
    .line 784
    new-instance v0, Lcom/android/server/AppWidgetServiceImpl$1;

    #@2
    invoke-direct {v0, p0, p1}, Lcom/android/server/AppWidgetServiceImpl$1;-><init>(Lcom/android/server/AppWidgetServiceImpl;Landroid/content/Intent;)V

    #@5
    .line 804
    .local v0, conn:Landroid/content/ServiceConnection;
    iget-object v4, p2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@7
    iget v4, v4, Lcom/android/server/AppWidgetServiceImpl$Provider;->uid:I

    #@9
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    #@c
    move-result v3

    #@d
    .line 807
    .local v3, userId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@10
    move-result-wide v1

    #@11
    .line 809
    .local v1, token:J
    :try_start_11
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@13
    const/4 v5, 0x1

    #@14
    invoke-virtual {v4, p1, v0, v5, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_1b

    #@17
    .line 811
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1a
    .line 813
    return-void

    #@1b
    .line 811
    :catchall_1b
    move-exception v4

    #@1c
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    throw v4
.end method

.method private dumpAppWidgetId(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;ILjava/io/PrintWriter;)V
    .registers 5
    .parameter "id"
    .parameter "index"
    .parameter "pw"

    #@0
    .prologue
    .line 363
    const-string v0, "  ["

    #@2
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(I)V

    #@8
    const-string v0, "] id="

    #@a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d
    .line 364
    iget v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@12
    .line 365
    const-string v0, "    hostId="

    #@14
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17
    .line 366
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@19
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->hostId:I

    #@1b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1e
    const/16 v0, 0x20

    #@20
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(C)V

    #@23
    .line 367
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@25
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;

    #@27
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a
    const/16 v0, 0x2f

    #@2c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(C)V

    #@2f
    .line 368
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@31
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->uid:I

    #@33
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@36
    .line 369
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@38
    if-eqz v0, :cond_4c

    #@3a
    .line 370
    const-string v0, "    provider="

    #@3c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    .line 371
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@41
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@43
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@45
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4c
    .line 373
    :cond_4c
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@4e
    if-eqz v0, :cond_5c

    #@50
    .line 374
    const-string v0, "    host.callbacks="

    #@52
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@55
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@57
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@59
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@5c
    .line 376
    :cond_5c
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@5e
    if-eqz v0, :cond_6a

    #@60
    .line 377
    const-string v0, "    views="

    #@62
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@65
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@67
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@6a
    .line 379
    :cond_6a
    return-void
.end method

.method private dumpHost(Lcom/android/server/AppWidgetServiceImpl$Host;ILjava/io/PrintWriter;)V
    .registers 5
    .parameter "host"
    .parameter "index"
    .parameter "pw"

    #@0
    .prologue
    .line 353
    const-string v0, "  ["

    #@2
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(I)V

    #@8
    const-string v0, "] hostId="

    #@a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d
    .line 354
    iget v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->hostId:I

    #@f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@12
    const/16 v0, 0x20

    #@14
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(C)V

    #@17
    .line 355
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;

    #@19
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c
    const/16 v0, 0x2f

    #@1e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(C)V

    #@21
    .line 356
    iget v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->uid:I

    #@23
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@26
    const/16 v0, 0x3a

    #@28
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(C)V

    #@2b
    .line 357
    const-string v0, "    callbacks="

    #@2d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@30
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@32
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@35
    .line 358
    const-string v0, "    instances.size="

    #@37
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v0

    #@40
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@43
    .line 359
    const-string v0, " zombie="

    #@45
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@48
    iget-boolean v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->zombie:Z

    #@4a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@4d
    .line 360
    return-void
.end method

.method private dumpProvider(Lcom/android/server/AppWidgetServiceImpl$Provider;ILjava/io/PrintWriter;)V
    .registers 6
    .parameter "p"
    .parameter "index"
    .parameter "pw"

    #@0
    .prologue
    .line 331
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@2
    .line 332
    .local v0, info:Landroid/appwidget/AppWidgetProviderInfo;
    const-string v1, "  ["

    #@4
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7
    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(I)V

    #@a
    const-string v1, "] provider "

    #@c
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f
    .line 333
    iget-object v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@11
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18
    .line 334
    const/16 v1, 0x3a

    #@1a
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(C)V

    #@1d
    .line 335
    const-string v1, "    min=("

    #@1f
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@24
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@27
    .line 336
    const-string v1, "x"

    #@29
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@2e
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@31
    .line 337
    const-string v1, ")   minResize=("

    #@33
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    #@38
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@3b
    .line 338
    const-string v1, "x"

    #@3d
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@40
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@42
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@45
    .line 339
    const-string v1, ") updatePeriodMillis="

    #@47
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    .line 340
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@4c
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@4f
    .line 341
    const-string v1, " resizeMode="

    #@51
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@54
    .line 342
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    #@56
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@59
    .line 343
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    #@5b
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@5e
    .line 344
    const-string v1, " autoAdvanceViewId="

    #@60
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@63
    .line 345
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    #@65
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@68
    .line 346
    const-string v1, " initialLayout=#"

    #@6a
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6d
    .line 347
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    #@6f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@76
    .line 348
    const-string v1, " uid="

    #@78
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7b
    iget v1, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->uid:I

    #@7d
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    #@80
    .line 349
    const-string v1, " zombie="

    #@82
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@85
    iget-boolean v1, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@87
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@8a
    .line 350
    return-void
.end method

.method private ensureStateLoadedLocked()V
    .registers 2

    #@0
    .prologue
    .line 428
    iget-boolean v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mStateLoaded:Z

    #@2
    if-nez v0, :cond_d

    #@4
    .line 429
    invoke-virtual {p0}, Lcom/android/server/AppWidgetServiceImpl;->loadAppWidgetListLocked()V

    #@7
    .line 430
    invoke-virtual {p0}, Lcom/android/server/AppWidgetServiceImpl;->loadStateLocked()V

    #@a
    .line 431
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mStateLoaded:Z

    #@d
    .line 433
    :cond_d
    return-void
.end method

.method static getAppWidgetIds(Lcom/android/server/AppWidgetServiceImpl$Host;)[I
    .registers 5
    .parameter "h"

    #@0
    .prologue
    .line 1372
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 1373
    .local v2, instancesSize:I
    new-array v0, v2, [I

    #@8
    .line 1374
    .local v0, appWidgetIds:[I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v2, :cond_1a

    #@b
    .line 1375
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@13
    iget v3, v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@15
    aput v3, v0, v1

    #@17
    .line 1374
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_9

    #@1a
    .line 1377
    :cond_1a
    return-object v0
.end method

.method static getAppWidgetIds(Lcom/android/server/AppWidgetServiceImpl$Provider;)[I
    .registers 5
    .parameter "p"

    #@0
    .prologue
    .line 1351
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 1352
    .local v2, instancesSize:I
    new-array v0, v2, [I

    #@8
    .line 1353
    .local v0, appWidgetIds:[I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v2, :cond_1a

    #@b
    .line 1354
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@13
    iget v3, v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@15
    aput v3, v0, v1

    #@17
    .line 1353
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_9

    #@1a
    .line 1356
    :cond_1a
    return-object v0
.end method

.method static getSettingsFile(I)Ljava/io/File;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 1814
    new-instance v0, Ljava/io/File;

    #@2
    invoke-static {p0}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    const-string v2, "appwidgets.xml"

    #@8
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    return-object v0
.end method

.method private incrementAppWidgetServiceRefCount(ILandroid/content/Intent$FilterComparison;)V
    .registers 5
    .parameter "appWidgetId"
    .parameter "fc"

    #@0
    .prologue
    .line 817
    const/4 v0, 0x0

    #@1
    .line 818
    .local v0, appWidgetIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@3
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_19

    #@9
    .line 819
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@b
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    .end local v0           #appWidgetIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    check-cast v0, Ljava/util/HashSet;

    #@11
    .line 824
    .restart local v0       #appWidgetIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :goto_11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@18
    .line 825
    return-void

    #@19
    .line 821
    :cond_19
    new-instance v0, Ljava/util/HashSet;

    #@1b
    .end local v0           #appWidgetIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@1e
    .line 822
    .restart local v0       #appWidgetIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@20
    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    goto :goto_11
.end method

.method private isLocalBinder()Z
    .registers 3

    #@0
    .prologue
    .line 1118
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 229
    const-string v0, "AppWidgetServiceImpl"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "u="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, ": "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 230
    return-void
.end method

.method private parseProviderInfoXml(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;)Lcom/android/server/AppWidgetServiceImpl$Provider;
    .registers 20
    .parameter "component"
    .parameter "ri"

    #@0
    .prologue
    .line 1394
    const/4 v7, 0x0

    #@1
    .line 1396
    .local v7, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    move-object/from16 v0, p2

    #@3
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@5
    .line 1397
    .local v1, activityInfo:Landroid/content/pm/ActivityInfo;
    const/4 v9, 0x0

    #@6
    .line 1399
    .local v9, parser:Landroid/content/res/XmlResourceParser;
    :try_start_6
    move-object/from16 v0, p0

    #@8
    iget-object v14, v0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@d
    move-result-object v14

    #@e
    const-string v15, "android.appwidget.provider"

    #@10
    invoke-virtual {v1, v14, v15}, Landroid/content/pm/ActivityInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@13
    move-result-object v9

    #@14
    .line 1401
    if-nez v9, :cond_3d

    #@16
    .line 1402
    const-string v14, "AppWidgetServiceImpl"

    #@18
    new-instance v15, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v16, "No android.appwidget.provider meta-data for AppWidget provider \'"

    #@1f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v15

    #@23
    move-object/from16 v0, p1

    #@25
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v15

    #@29
    const/16 v16, 0x27

    #@2b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v15

    #@2f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v15

    #@33
    invoke-static {v14, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36
    .catchall {:try_start_6 .. :try_end_36} :catchall_181
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_36} :catch_158

    #@36
    .line 1404
    const/4 v8, 0x0

    #@37
    .line 1479
    if-eqz v9, :cond_3c

    #@39
    .line 1480
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@3c
    .line 1482
    :cond_3c
    :goto_3c
    return-object v8

    #@3d
    .line 1407
    :cond_3d
    :try_start_3d
    invoke-static {v9}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@40
    move-result-object v2

    #@41
    .line 1411
    .local v2, attrs:Landroid/util/AttributeSet;
    :cond_41
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    #@44
    move-result v12

    #@45
    .local v12, type:I
    const/4 v14, 0x1

    #@46
    if-eq v12, v14, :cond_4b

    #@48
    const/4 v14, 0x2

    #@49
    if-ne v12, v14, :cond_41

    #@4b
    .line 1415
    :cond_4b
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    .line 1416
    .local v6, nodeName:Ljava/lang/String;
    const-string v14, "appwidget-provider"

    #@51
    invoke-virtual {v14, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v14

    #@55
    if-nez v14, :cond_7e

    #@57
    .line 1417
    const-string v14, "AppWidgetServiceImpl"

    #@59
    new-instance v15, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v16, "Meta-data does not start with appwidget-provider tag for AppWidget provider \'"

    #@60
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v15

    #@64
    move-object/from16 v0, p1

    #@66
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v15

    #@6a
    const/16 v16, 0x27

    #@6c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v15

    #@70
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v15

    #@74
    invoke-static {v14, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_77
    .catchall {:try_start_3d .. :try_end_77} :catchall_181
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_77} :catch_158

    #@77
    .line 1419
    const/4 v8, 0x0

    #@78
    .line 1479
    if-eqz v9, :cond_3c

    #@7a
    .line 1480
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@7d
    goto :goto_3c

    #@7e
    .line 1422
    :cond_7e
    :try_start_7e
    new-instance v8, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@80
    invoke-direct {v8}, Lcom/android/server/AppWidgetServiceImpl$Provider;-><init>()V
    :try_end_83
    .catchall {:try_start_7e .. :try_end_83} :catchall_181
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_83} :catch_158

    #@83
    .line 1423
    .end local v7           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .local v8, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :try_start_83
    new-instance v5, Landroid/appwidget/AppWidgetProviderInfo;

    #@85
    invoke-direct {v5}, Landroid/appwidget/AppWidgetProviderInfo;-><init>()V

    #@88
    iput-object v5, v8, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@8a
    .line 1424
    .local v5, info:Landroid/appwidget/AppWidgetProviderInfo;
    move-object/from16 v0, p1

    #@8c
    iput-object v0, v5, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@8e
    .line 1425
    iget-object v14, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@90
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->uid:I

    #@92
    iput v14, v8, Lcom/android/server/AppWidgetServiceImpl$Provider;->uid:I

    #@94
    .line 1427
    move-object/from16 v0, p0

    #@96
    iget-object v14, v0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@98
    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@9b
    move-result-object v14

    #@9c
    iget-object v15, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@9e
    move-object/from16 v0, p0

    #@a0
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@a2
    move/from16 v16, v0

    #@a4
    invoke-virtual/range {v14 .. v16}, Landroid/content/pm/PackageManager;->getResourcesForApplicationAsUser(Ljava/lang/String;I)Landroid/content/res/Resources;

    #@a7
    move-result-object v10

    #@a8
    .line 1430
    .local v10, res:Landroid/content/res/Resources;
    sget-object v14, Lcom/android/internal/R$styleable;->AppWidgetProviderInfo:[I

    #@aa
    invoke-virtual {v10, v2, v14}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@ad
    move-result-object v11

    #@ae
    .line 1436
    .local v11, sa:Landroid/content/res/TypedArray;
    const/4 v14, 0x0

    #@af
    invoke-virtual {v11, v14}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@b2
    move-result-object v13

    #@b3
    .line 1438
    .local v13, value:Landroid/util/TypedValue;
    if-eqz v13, :cond_14b

    #@b5
    iget v14, v13, Landroid/util/TypedValue;->data:I

    #@b7
    :goto_b7
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@b9
    .line 1439
    const/4 v14, 0x1

    #@ba
    invoke-virtual {v11, v14}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@bd
    move-result-object v13

    #@be
    .line 1440
    if-eqz v13, :cond_14e

    #@c0
    iget v14, v13, Landroid/util/TypedValue;->data:I

    #@c2
    :goto_c2
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@c4
    .line 1441
    const/16 v14, 0x8

    #@c6
    invoke-virtual {v11, v14}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@c9
    move-result-object v13

    #@ca
    .line 1443
    if-eqz v13, :cond_151

    #@cc
    iget v14, v13, Landroid/util/TypedValue;->data:I

    #@ce
    :goto_ce
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    #@d0
    .line 1444
    const/16 v14, 0x9

    #@d2
    invoke-virtual {v11, v14}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@d5
    move-result-object v13

    #@d6
    .line 1446
    if-eqz v13, :cond_155

    #@d8
    iget v14, v13, Landroid/util/TypedValue;->data:I

    #@da
    :goto_da
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@dc
    .line 1447
    const/4 v14, 0x2

    #@dd
    const/4 v15, 0x0

    #@de
    invoke-virtual {v11, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@e1
    move-result v14

    #@e2
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@e4
    .line 1449
    const/4 v14, 0x3

    #@e5
    const/4 v15, 0x0

    #@e6
    invoke-virtual {v11, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@e9
    move-result v14

    #@ea
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    #@ec
    .line 1451
    const/16 v14, 0xa

    #@ee
    const/4 v15, 0x0

    #@ef
    invoke-virtual {v11, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@f2
    move-result v14

    #@f3
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->initialKeyguardLayout:I

    #@f5
    .line 1453
    const/4 v14, 0x4

    #@f6
    invoke-virtual {v11, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@f9
    move-result-object v3

    #@fa
    .line 1455
    .local v3, className:Ljava/lang/String;
    if-eqz v3, :cond_107

    #@fc
    .line 1456
    new-instance v14, Landroid/content/ComponentName;

    #@fe
    invoke-virtual/range {p1 .. p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@101
    move-result-object v15

    #@102
    invoke-direct {v14, v15, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@105
    iput-object v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    #@107
    .line 1458
    :cond_107
    move-object/from16 v0, p0

    #@109
    iget-object v14, v0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@10b
    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@10e
    move-result-object v14

    #@10f
    invoke-virtual {v1, v14}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@112
    move-result-object v14

    #@113
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@116
    move-result-object v14

    #@117
    iput-object v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    #@119
    .line 1459
    invoke-virtual/range {p2 .. p2}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    #@11c
    move-result v14

    #@11d
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    #@11f
    .line 1460
    const/4 v14, 0x5

    #@120
    const/4 v15, 0x0

    #@121
    invoke-virtual {v11, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@124
    move-result v14

    #@125
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    #@127
    .line 1462
    const/4 v14, 0x6

    #@128
    const/4 v15, -0x1

    #@129
    invoke-virtual {v11, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@12c
    move-result v14

    #@12d
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    #@12f
    .line 1464
    const/4 v14, 0x7

    #@130
    const/4 v15, 0x0

    #@131
    invoke-virtual {v11, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@134
    move-result v14

    #@135
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    #@137
    .line 1467
    const/16 v14, 0xb

    #@139
    const/4 v15, 0x1

    #@13a
    invoke-virtual {v11, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@13d
    move-result v14

    #@13e
    iput v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    #@140
    .line 1471
    invoke-virtual {v11}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_143
    .catchall {:try_start_83 .. :try_end_143} :catchall_188
    .catch Ljava/lang/Exception; {:try_start_83 .. :try_end_143} :catch_18b

    #@143
    .line 1479
    if-eqz v9, :cond_148

    #@145
    .line 1480
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@148
    :cond_148
    move-object v7, v8

    #@149
    .line 1482
    .end local v8           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .restart local v7       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    goto/16 :goto_3c

    #@14b
    .line 1438
    .end local v3           #className:Ljava/lang/String;
    .end local v7           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .restart local v8       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_14b
    const/4 v14, 0x0

    #@14c
    goto/16 :goto_b7

    #@14e
    .line 1440
    :cond_14e
    const/4 v14, 0x0

    #@14f
    goto/16 :goto_c2

    #@151
    .line 1443
    :cond_151
    :try_start_151
    iget v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@153
    goto/16 :goto_ce

    #@155
    .line 1446
    :cond_155
    iget v14, v5, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I
    :try_end_157
    .catchall {:try_start_151 .. :try_end_157} :catchall_188
    .catch Ljava/lang/Exception; {:try_start_151 .. :try_end_157} :catch_18b

    #@157
    goto :goto_da

    #@158
    .line 1472
    .end local v2           #attrs:Landroid/util/AttributeSet;
    .end local v5           #info:Landroid/appwidget/AppWidgetProviderInfo;
    .end local v6           #nodeName:Ljava/lang/String;
    .end local v8           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .end local v10           #res:Landroid/content/res/Resources;
    .end local v11           #sa:Landroid/content/res/TypedArray;
    .end local v12           #type:I
    .end local v13           #value:Landroid/util/TypedValue;
    .restart local v7       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :catch_158
    move-exception v4

    #@159
    .line 1476
    .local v4, e:Ljava/lang/Exception;
    :goto_159
    :try_start_159
    const-string v14, "AppWidgetServiceImpl"

    #@15b
    new-instance v15, Ljava/lang/StringBuilder;

    #@15d
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@160
    const-string v16, "XML parsing failed for AppWidget provider \'"

    #@162
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v15

    #@166
    move-object/from16 v0, p1

    #@168
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v15

    #@16c
    const/16 v16, 0x27

    #@16e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@171
    move-result-object v15

    #@172
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@175
    move-result-object v15

    #@176
    invoke-static {v14, v15, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_179
    .catchall {:try_start_159 .. :try_end_179} :catchall_181

    #@179
    .line 1477
    const/4 v8, 0x0

    #@17a
    .line 1479
    if-eqz v9, :cond_3c

    #@17c
    .line 1480
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@17f
    goto/16 :goto_3c

    #@181
    .line 1479
    .end local v4           #e:Ljava/lang/Exception;
    :catchall_181
    move-exception v14

    #@182
    :goto_182
    if-eqz v9, :cond_187

    #@184
    .line 1480
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@187
    :cond_187
    throw v14

    #@188
    .line 1479
    .end local v7           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .restart local v2       #attrs:Landroid/util/AttributeSet;
    .restart local v6       #nodeName:Ljava/lang/String;
    .restart local v8       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .restart local v12       #type:I
    :catchall_188
    move-exception v14

    #@189
    move-object v7, v8

    #@18a
    .end local v8           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .restart local v7       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    goto :goto_182

    #@18b
    .line 1472
    .end local v7           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .restart local v8       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :catch_18b
    move-exception v4

    #@18c
    move-object v7, v8

    #@18d
    .end local v8           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .restart local v7       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    goto :goto_159
.end method

.method private saveStateAsync()V
    .registers 3

    #@0
    .prologue
    .line 915
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mSaveStateHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mSaveStateRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 916
    return-void
.end method

.method private unbindAppWidgetRemoteViewsServicesLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V
    .registers 7
    .parameter "id"

    #@0
    .prologue
    .line 762
    iget v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@2
    .line 764
    .local v0, appWidgetId:I
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@4
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@7
    move-result-object v4

    #@8
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v2

    #@c
    .line 766
    .local v2, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/Integer;Landroid/content/Intent$FilterComparison;>;>;"
    :cond_c
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_36

    #@12
    .line 767
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    check-cast v3, Landroid/util/Pair;

    #@18
    .line 768
    .local v3, key:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Landroid/content/Intent$FilterComparison;>;"
    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@1a
    check-cast v4, Ljava/lang/Integer;

    #@1c
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@1f
    move-result v4

    #@20
    if-ne v4, v0, :cond_c

    #@22
    .line 769
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@24
    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;

    #@2a
    .line 771
    .local v1, conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    invoke-virtual {v1}, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;->disconnect()V

    #@2d
    .line 772
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v4, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@32
    .line 773
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    #@35
    goto :goto_c

    #@36
    .line 779
    .end local v1           #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    .end local v3           #key:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Landroid/content/Intent$FilterComparison;>;"
    :cond_36
    invoke-direct {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->decrementAppWidgetServiceRefCount(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V

    #@39
    .line 780
    return-void
.end method


# virtual methods
.method addProviderLocked(Landroid/content/pm/ResolveInfo;)Z
    .registers 7
    .parameter "ri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1269
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@3
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@7
    const/high16 v3, 0x4

    #@9
    and-int/2addr v2, v3

    #@a
    if-eqz v2, :cond_d

    #@c
    .line 1281
    :cond_c
    :goto_c
    return v1

    #@d
    .line 1272
    :cond_d
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@f
    invoke-virtual {v2}, Landroid/content/pm/ActivityInfo;->isEnabled()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_c

    #@15
    .line 1275
    new-instance v2, Landroid/content/ComponentName;

    #@17
    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@19
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@1b
    iget-object v4, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@1d
    iget-object v4, v4, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@1f
    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    invoke-direct {p0, v2, p1}, Lcom/android/server/AppWidgetServiceImpl;->parseProviderInfoXml(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;)Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@25
    move-result-object v0

    #@26
    .line 1277
    .local v0, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-eqz v0, :cond_c

    #@28
    .line 1278
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    .line 1279
    const/4 v1, 0x1

    #@2e
    goto :goto_c
.end method

.method addProvidersForPackageLocked(Ljava/lang/String;)Z
    .registers 14
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1847
    const/4 v5, 0x0

    #@2
    .line 1848
    .local v5, providersAdded:Z
    new-instance v4, Landroid/content/Intent;

    #@4
    const-string v8, "android.appwidget.action.APPWIDGET_UPDATE"

    #@6
    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 1849
    .local v4, intent:Landroid/content/Intent;
    invoke-virtual {v4, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 1852
    :try_start_c
    iget-object v8, p0, Lcom/android/server/AppWidgetServiceImpl;->mPm:Landroid/content/pm/IPackageManager;

    #@e
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v9

    #@14
    invoke-virtual {v4, v9}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@17
    move-result-object v9

    #@18
    const/16 v10, 0x80

    #@1a
    iget v11, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@1c
    invoke-interface {v8, v4, v9, v10, v11}, Landroid/content/pm/IPackageManager;->queryIntentReceivers(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_1f} :catch_39

    #@1f
    move-result-object v2

    #@20
    .line 1859
    .local v2, broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v2, :cond_3b

    #@22
    .line 1860
    .local v0, N:I
    :goto_22
    const/4 v3, 0x0

    #@23
    .local v3, i:I
    :goto_23
    if-ge v3, v0, :cond_4d

    #@25
    .line 1861
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v7

    #@29
    check-cast v7, Landroid/content/pm/ResolveInfo;

    #@2b
    .line 1862
    .local v7, ri:Landroid/content/pm/ResolveInfo;
    iget-object v1, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2d
    .line 1863
    .local v1, ai:Landroid/content/pm/ActivityInfo;
    iget-object v8, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2f
    iget v8, v8, Landroid/content/pm/ApplicationInfo;->flags:I

    #@31
    const/high16 v9, 0x4

    #@33
    and-int/2addr v8, v9

    #@34
    if-eqz v8, :cond_40

    #@36
    .line 1860
    :cond_36
    :goto_36
    add-int/lit8 v3, v3, 0x1

    #@38
    goto :goto_23

    #@39
    .line 1855
    .end local v0           #N:I
    .end local v1           #ai:Landroid/content/pm/ActivityInfo;
    .end local v2           #broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3           #i:I
    .end local v7           #ri:Landroid/content/pm/ResolveInfo;
    :catch_39
    move-exception v6

    #@3a
    .line 1872
    :goto_3a
    return v0

    #@3b
    .line 1859
    .restart local v2       #broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_3b
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@3e
    move-result v0

    #@3f
    goto :goto_22

    #@40
    .line 1866
    .restart local v0       #N:I
    .restart local v1       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v3       #i:I
    .restart local v7       #ri:Landroid/content/pm/ResolveInfo;
    :cond_40
    iget-object v8, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@42
    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v8

    #@46
    if-eqz v8, :cond_36

    #@48
    .line 1867
    invoke-virtual {p0, v7}, Lcom/android/server/AppWidgetServiceImpl;->addProviderLocked(Landroid/content/pm/ResolveInfo;)Z

    #@4b
    .line 1868
    const/4 v5, 0x1

    #@4c
    goto :goto_36

    #@4d
    .end local v1           #ai:Landroid/content/pm/ActivityInfo;
    .end local v7           #ri:Landroid/content/pm/ResolveInfo;
    :cond_4d
    move v0, v5

    #@4e
    .line 1872
    goto :goto_3a
.end method

.method public allocateAppWidgetId(Ljava/lang/String;I)I
    .registers 10
    .parameter "packageName"
    .parameter "hostId"

    #@0
    .prologue
    .line 436
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->enforceSystemOrCallingUid(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    .line 437
    .local v1, callingUid:I
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@6
    monitor-enter v5

    #@7
    .line 438
    :try_start_7
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@a
    .line 439
    iget v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mNextAppWidgetId:I

    #@c
    add-int/lit8 v4, v0, 0x1

    #@e
    iput v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mNextAppWidgetId:I

    #@10
    .line 441
    .local v0, appWidgetId:I
    invoke-virtual {p0, v1, p1, p2}, Lcom/android/server/AppWidgetServiceImpl;->lookupOrAddHostLocked(ILjava/lang/String;I)Lcom/android/server/AppWidgetServiceImpl$Host;

    #@13
    move-result-object v2

    #@14
    .line 443
    .local v2, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    new-instance v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@16
    invoke-direct {v3}, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;-><init>()V

    #@19
    .line 444
    .local v3, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    iput v0, v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@1b
    .line 445
    iput-object v2, v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@1d
    .line 447
    iget-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22
    .line 448
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@27
    .line 450
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@2a
    .line 451
    sget-boolean v4, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@2c
    if-eqz v4, :cond_58

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v6, "Allocating AppWidgetId for "

    #@35
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    const-string v6, " host="

    #@3f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    const-string v6, " id="

    #@49
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-direct {p0, v4}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@58
    .line 453
    :cond_58
    monitor-exit v5

    #@59
    return v0

    #@5a
    .line 454
    .end local v0           #appWidgetId:I
    .end local v2           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    .end local v3           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_5a
    move-exception v4

    #@5b
    monitor-exit v5
    :try_end_5c
    .catchall {:try_start_7 .. :try_end_5c} :catchall_5a

    #@5c
    throw v4
.end method

.method public bindAppWidgetId(ILandroid/content/ComponentName;Landroid/os/Bundle;)V
    .registers 8
    .parameter "appWidgetId"
    .parameter "provider"
    .parameter "options"

    #@0
    .prologue
    .line 618
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BIND_APPWIDGET"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "bindAppWidgetId appWidgetId="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " provider="

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 620
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/AppWidgetServiceImpl;->bindAppWidgetIdImpl(ILandroid/content/ComponentName;Landroid/os/Bundle;)V

    #@27
    .line 621
    return-void
.end method

.method public bindAppWidgetIdIfAllowed(Ljava/lang/String;ILandroid/content/ComponentName;Landroid/os/Bundle;)Z
    .registers 9
    .parameter "packageName"
    .parameter "appWidgetId"
    .parameter "provider"
    .parameter "options"

    #@0
    .prologue
    .line 626
    :try_start_0
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.BIND_APPWIDGET"

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_8} :catch_d

    #@8
    .line 632
    :cond_8
    invoke-direct {p0, p2, p3, p4}, Lcom/android/server/AppWidgetServiceImpl;->bindAppWidgetIdImpl(ILandroid/content/ComponentName;Landroid/os/Bundle;)V

    #@b
    .line 633
    const/4 v1, 0x1

    #@c
    :goto_c
    return v1

    #@d
    .line 627
    :catch_d
    move-exception v0

    #@e
    .line 628
    .local v0, se:Ljava/lang/SecurityException;
    invoke-direct {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->callerHasBindAppWidgetPermission(Ljava/lang/String;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_8

    #@14
    .line 629
    const/4 v1, 0x0

    #@15
    goto :goto_c
.end method

.method public bindRemoteViewsService(ILandroid/content/Intent;Landroid/os/IBinder;)V
    .registers 21
    .parameter "appWidgetId"
    .parameter "intent"
    .parameter "connection"

    #@0
    .prologue
    .line 680
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@4
    monitor-enter v14

    #@5
    .line 681
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@8
    .line 682
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@b
    move-result-object v7

    #@c
    .line 683
    .local v7, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-nez v7, :cond_19

    #@e
    .line 684
    new-instance v13, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v15, "bad appWidgetId"

    #@12
    invoke-direct {v13, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v13

    #@16
    .line 731
    .end local v7           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_16
    move-exception v13

    #@17
    monitor-exit v14
    :try_end_18
    .catchall {:try_start_5 .. :try_end_18} :catchall_16

    #@18
    throw v13

    #@19
    .line 686
    .restart local v7       #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_19
    :try_start_19
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
    :try_end_1c
    .catchall {:try_start_19 .. :try_end_1c} :catchall_16

    #@1c
    move-result-object v2

    #@1d
    .line 688
    .local v2, componentName:Landroid/content/ComponentName;
    :try_start_1d
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@20
    move-result-object v13

    #@21
    const/16 v15, 0x1000

    #@23
    move-object/from16 v0, p0

    #@25
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@27
    move/from16 v16, v0

    #@29
    move/from16 v0, v16

    #@2b
    invoke-interface {v13, v2, v15, v0}, Landroid/content/pm/IPackageManager;->getServiceInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;

    #@2e
    move-result-object v9

    #@2f
    .line 690
    .local v9, si:Landroid/content/pm/ServiceInfo;
    const-string v13, "android.permission.BIND_REMOTEVIEWS"

    #@31
    iget-object v15, v9, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@33
    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v13

    #@37
    if-nez v13, :cond_6c

    #@39
    .line 691
    new-instance v13, Ljava/lang/SecurityException;

    #@3b
    new-instance v15, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v16, "Selected service does not require android.permission.BIND_REMOTEVIEWS: "

    #@42
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v15

    #@46
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v15

    #@4a
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v15

    #@4e
    invoke-direct {v13, v15}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@51
    throw v13
    :try_end_52
    .catchall {:try_start_1d .. :try_end_52} :catchall_16
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_52} :catch_52

    #@52
    .line 694
    .end local v9           #si:Landroid/content/pm/ServiceInfo;
    :catch_52
    move-exception v5

    #@53
    .line 695
    .local v5, e:Landroid/os/RemoteException;
    :try_start_53
    new-instance v13, Ljava/lang/IllegalArgumentException;

    #@55
    new-instance v15, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v16, "Unknown component "

    #@5c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v15

    #@60
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v15

    #@64
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v15

    #@68
    invoke-direct {v13, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v13

    #@6c
    .line 701
    .end local v5           #e:Landroid/os/RemoteException;
    .restart local v9       #si:Landroid/content/pm/ServiceInfo;
    :cond_6c
    const/4 v3, 0x0

    #@6d
    .line 702
    .local v3, conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    new-instance v6, Landroid/content/Intent$FilterComparison;

    #@6f
    move-object/from16 v0, p2

    #@71
    invoke-direct {v6, v0}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@74
    .line 703
    .local v6, fc:Landroid/content/Intent$FilterComparison;
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@77
    move-result-object v13

    #@78
    invoke-static {v13, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@7b
    move-result-object v8

    #@7c
    .line 704
    .local v8, key:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Landroid/content/Intent$FilterComparison;>;"
    move-object/from16 v0, p0

    #@7e
    iget-object v13, v0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@80
    invoke-virtual {v13, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@83
    move-result v13

    #@84
    if-eqz v13, :cond_a1

    #@86
    .line 705
    move-object/from16 v0, p0

    #@88
    iget-object v13, v0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@8a
    invoke-virtual {v13, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8d
    move-result-object v3

    #@8e
    .end local v3           #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;

    #@90
    .line 706
    .restart local v3       #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    invoke-virtual {v3}, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;->disconnect()V

    #@93
    .line 707
    move-object/from16 v0, p0

    #@95
    iget-object v13, v0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@97
    invoke-virtual {v13, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@9a
    .line 708
    move-object/from16 v0, p0

    #@9c
    iget-object v13, v0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@9e
    invoke-virtual {v13, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@a1
    :cond_a1
    move-object v4, v3

    #@a2
    .line 711
    .end local v3           #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    .local v4, conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    iget-object v13, v7, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@a4
    iget v13, v13, Lcom/android/server/AppWidgetServiceImpl$Provider;->uid:I

    #@a6
    invoke-static {v13}, Landroid/os/UserHandle;->getUserId(I)I

    #@a9
    move-result v12

    #@aa
    .line 712
    .local v12, userId:I
    move-object/from16 v0, p0

    #@ac
    iget v13, v0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@ae
    if-eq v12, v13, :cond_d8

    #@b0
    .line 713
    const-string v13, "AppWidgetServiceImpl"

    #@b2
    new-instance v15, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v16, "AppWidgetServiceImpl of user "

    #@b9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v15

    #@bd
    move-object/from16 v0, p0

    #@bf
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@c1
    move/from16 v16, v0

    #@c3
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v15

    #@c7
    const-string v16, " binding to provider on user "

    #@c9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v15

    #@cd
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v15

    #@d1
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v15

    #@d5
    invoke-static {v13, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 718
    :cond_d8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_db
    .catchall {:try_start_53 .. :try_end_db} :catchall_16

    #@db
    move-result-wide v10

    #@dc
    .line 720
    .local v10, token:J
    :try_start_dc
    new-instance v3, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;

    #@de
    move-object/from16 v0, p3

    #@e0
    invoke-direct {v3, v8, v0}, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;-><init>(Landroid/util/Pair;Landroid/os/IBinder;)V
    :try_end_e3
    .catchall {:try_start_dc .. :try_end_e3} :catchall_100

    #@e3
    .line 721
    .end local v4           #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    .restart local v3       #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    :try_start_e3
    move-object/from16 v0, p0

    #@e5
    iget-object v13, v0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@e7
    const/4 v15, 0x1

    #@e8
    move-object/from16 v0, p2

    #@ea
    invoke-virtual {v13, v0, v3, v15, v12}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@ed
    .line 722
    move-object/from16 v0, p0

    #@ef
    iget-object v13, v0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@f1
    invoke-virtual {v13, v8, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f4
    .catchall {:try_start_e3 .. :try_end_f4} :catchall_106

    #@f4
    .line 724
    :try_start_f4
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@f7
    .line 730
    move-object/from16 v0, p0

    #@f9
    move/from16 v1, p1

    #@fb
    invoke-direct {v0, v1, v6}, Lcom/android/server/AppWidgetServiceImpl;->incrementAppWidgetServiceRefCount(ILandroid/content/Intent$FilterComparison;)V

    #@fe
    .line 731
    monitor-exit v14

    #@ff
    .line 732
    return-void

    #@100
    .line 724
    .end local v3           #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    .restart local v4       #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    :catchall_100
    move-exception v13

    #@101
    move-object v3, v4

    #@102
    .end local v4           #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    .restart local v3       #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    :goto_102
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@105
    throw v13
    :try_end_106
    .catchall {:try_start_f4 .. :try_end_106} :catchall_16

    #@106
    :catchall_106
    move-exception v13

    #@107
    goto :goto_102
.end method

.method canAccessAppWidgetId(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;I)Z
    .registers 6
    .parameter "id"
    .parameter "callingUid"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1179
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@3
    invoke-virtual {v1, p2}, Lcom/android/server/AppWidgetServiceImpl$Host;->uidMatches(I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    .line 1192
    :cond_9
    :goto_9
    return v0

    #@a
    .line 1183
    :cond_a
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@c
    if-eqz v1, :cond_14

    #@e
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@10
    iget v1, v1, Lcom/android/server/AppWidgetServiceImpl$Provider;->uid:I

    #@12
    if-eq v1, p2, :cond_9

    #@14
    .line 1187
    :cond_14
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@16
    const-string v2, "android.permission.BIND_APPWIDGET"

    #@18
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_9

    #@1e
    .line 1192
    const/4 v0, 0x0

    #@1f
    goto :goto_9
.end method

.method cancelBroadcasts(Lcom/android/server/AppWidgetServiceImpl$Provider;)V
    .registers 6
    .parameter "p"

    #@0
    .prologue
    .line 545
    sget-boolean v2, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@2
    if-eqz v2, :cond_1a

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "cancelBroadcasts for "

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-direct {p0, v2}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@1a
    .line 546
    :cond_1a
    iget-object v2, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->broadcast:Landroid/app/PendingIntent;

    #@1c
    if-eqz v2, :cond_34

    #@1e
    .line 547
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAlarmManager:Landroid/app/AlarmManager;

    #@20
    iget-object v3, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->broadcast:Landroid/app/PendingIntent;

    #@22
    invoke-virtual {v2, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@25
    .line 548
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@28
    move-result-wide v0

    #@29
    .line 550
    .local v0, token:J
    :try_start_29
    iget-object v2, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->broadcast:Landroid/app/PendingIntent;

    #@2b
    invoke-virtual {v2}, Landroid/app/PendingIntent;->cancel()V
    :try_end_2e
    .catchall {:try_start_29 .. :try_end_2e} :catchall_35

    #@2e
    .line 552
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@31
    .line 554
    const/4 v2, 0x0

    #@32
    iput-object v2, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->broadcast:Landroid/app/PendingIntent;

    #@34
    .line 556
    .end local v0           #token:J
    :cond_34
    return-void

    #@35
    .line 552
    .restart local v0       #token:J
    :catchall_35
    move-exception v2

    #@36
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@39
    throw v2
.end method

.method computeMaximumWidgetBitmapMemory()V
    .registers 6

    #@0
    .prologue
    .line 211
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "window"

    #@4
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/view/WindowManager;

    #@a
    .line 212
    .local v2, wm:Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@d
    move-result-object v0

    #@e
    .line 213
    .local v0, display:Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    #@10
    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    #@13
    .line 214
    .local v1, size:Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@16
    .line 217
    iget v3, v1, Landroid/graphics/Point;->x:I

    #@18
    mul-int/lit8 v3, v3, 0x6

    #@1a
    iget v4, v1, Landroid/graphics/Point;->y:I

    #@1c
    mul-int/2addr v3, v4

    #@1d
    iput v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mMaxWidgetBitmapMemory:I

    #@1f
    .line 218
    return-void
.end method

.method public deleteAllHosts()V
    .registers 8

    #@0
    .prologue
    .line 481
    iget-object v6, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v6

    #@3
    .line 482
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 483
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@9
    move-result v1

    #@a
    .line 484
    .local v1, callingUid:I
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v0

    #@10
    .line 485
    .local v0, N:I
    const/4 v2, 0x0

    #@11
    .line 486
    .local v2, changed:Z
    add-int/lit8 v4, v0, -0x1

    #@13
    .local v4, i:I
    :goto_13
    if-ltz v4, :cond_2a

    #@15
    .line 487
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v3

    #@1b
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@1d
    .line 488
    .local v3, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    invoke-virtual {v3, v1}, Lcom/android/server/AppWidgetServiceImpl$Host;->uidMatches(I)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_27

    #@23
    .line 489
    invoke-virtual {p0, v3}, Lcom/android/server/AppWidgetServiceImpl;->deleteHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V

    #@26
    .line 490
    const/4 v2, 0x1

    #@27
    .line 486
    :cond_27
    add-int/lit8 v4, v4, -0x1

    #@29
    goto :goto_13

    #@2a
    .line 493
    .end local v3           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_2a
    if-eqz v2, :cond_2f

    #@2c
    .line 494
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@2f
    .line 496
    :cond_2f
    monitor-exit v6

    #@30
    .line 497
    return-void

    #@31
    .line 496
    .end local v0           #N:I
    .end local v1           #callingUid:I
    .end local v2           #changed:Z
    .end local v4           #i:I
    :catchall_31
    move-exception v5

    #@32
    monitor-exit v6
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_31

    #@33
    throw v5
.end method

.method public deleteAppWidgetId(I)V
    .registers 5
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 458
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 459
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 460
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@9
    move-result-object v0

    #@a
    .line 461
    .local v0, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-eqz v0, :cond_12

    #@c
    .line 462
    invoke-virtual {p0, v0}, Lcom/android/server/AppWidgetServiceImpl;->deleteAppWidgetLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V

    #@f
    .line 463
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@12
    .line 465
    :cond_12
    monitor-exit v2

    #@13
    .line 466
    return-void

    #@14
    .line 465
    .end local v0           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method deleteAppWidgetLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V
    .registers 8
    .parameter "id"

    #@0
    .prologue
    .line 514
    invoke-direct {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->unbindAppWidgetRemoteViewsServicesLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V

    #@3
    .line 516
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@5
    .line 517
    .local v0, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    iget-object v3, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@a
    .line 518
    invoke-virtual {p0, v0}, Lcom/android/server/AppWidgetServiceImpl;->pruneHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V

    #@d
    .line 520
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@12
    .line 522
    iget-object v2, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@14
    .line 523
    .local v2, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-eqz v2, :cond_65

    #@16
    .line 524
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1b
    .line 525
    iget-boolean v3, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@1d
    if-nez v3, :cond_65

    #@1f
    .line 527
    new-instance v1, Landroid/content/Intent;

    #@21
    const-string v3, "android.appwidget.action.APPWIDGET_DELETED"

    #@23
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26
    .line 528
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@28
    iget-object v3, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@2a
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@2d
    .line 529
    const-string v3, "appWidgetId"

    #@2f
    iget v4, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@31
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@34
    .line 530
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@36
    new-instance v4, Landroid/os/UserHandle;

    #@38
    iget v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@3a
    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    #@3d
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@40
    .line 531
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@45
    move-result v3

    #@46
    if-nez v3, :cond_65

    #@48
    .line 533
    invoke-virtual {p0, v2}, Lcom/android/server/AppWidgetServiceImpl;->cancelBroadcasts(Lcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@4b
    .line 536
    new-instance v1, Landroid/content/Intent;

    #@4d
    .end local v1           #intent:Landroid/content/Intent;
    const-string v3, "android.appwidget.action.APPWIDGET_DISABLED"

    #@4f
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@52
    .line 537
    .restart local v1       #intent:Landroid/content/Intent;
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@54
    iget-object v3, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@56
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@59
    .line 538
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@5b
    new-instance v4, Landroid/os/UserHandle;

    #@5d
    iget v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@5f
    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    #@62
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@65
    .line 542
    .end local v1           #intent:Landroid/content/Intent;
    :cond_65
    return-void
.end method

.method public deleteHost(I)V
    .registers 6
    .parameter "hostId"

    #@0
    .prologue
    .line 469
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 470
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 471
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@9
    move-result v0

    #@a
    .line 472
    .local v0, callingUid:I
    invoke-virtual {p0, v0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupHostLocked(II)Lcom/android/server/AppWidgetServiceImpl$Host;

    #@d
    move-result-object v1

    #@e
    .line 473
    .local v1, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    if-eqz v1, :cond_16

    #@10
    .line 474
    invoke-virtual {p0, v1}, Lcom/android/server/AppWidgetServiceImpl;->deleteHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V

    #@13
    .line 475
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@16
    .line 477
    :cond_16
    monitor-exit v3

    #@17
    .line 478
    return-void

    #@18
    .line 477
    .end local v0           #callingUid:I
    .end local v1           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :catchall_18
    move-exception v2

    #@19
    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v2
.end method

.method deleteHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V
    .registers 6
    .parameter "host"

    #@0
    .prologue
    .line 500
    iget-object v3, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 501
    .local v0, N:I
    add-int/lit8 v1, v0, -0x1

    #@8
    .local v1, i:I
    :goto_8
    if-ltz v1, :cond_18

    #@a
    .line 502
    iget-object v3, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@12
    .line 503
    .local v2, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    invoke-virtual {p0, v2}, Lcom/android/server/AppWidgetServiceImpl;->deleteAppWidgetLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V

    #@15
    .line 501
    add-int/lit8 v1, v1, -0x1

    #@17
    goto :goto_8

    #@18
    .line 505
    .end local v2           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_18
    iget-object v3, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@1d
    .line 506
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@22
    .line 507
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedHosts:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@27
    .line 509
    const/4 v3, 0x0

    #@28
    iput-object v3, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@2a
    .line 510
    return-void
.end method

.method dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 382
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DUMP"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_33

    #@a
    .line 384
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Permission Denial: can\'t dump from from pid="

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v3

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ", uid="

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 425
    :goto_32
    return-void

    #@33
    .line 390
    :cond_33
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@35
    monitor-enter v3

    #@36
    .line 391
    :try_start_36
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v0

    #@3c
    .line 392
    .local v0, N:I
    const-string v2, "Providers:"

    #@3e
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@41
    .line 393
    const/4 v1, 0x0

    #@42
    .local v1, i:I
    :goto_42
    if-ge v1, v0, :cond_52

    #@44
    .line 394
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@49
    move-result-object v2

    #@4a
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@4c
    invoke-direct {p0, v2, v1, p2}, Lcom/android/server/AppWidgetServiceImpl;->dumpProvider(Lcom/android/server/AppWidgetServiceImpl$Provider;ILjava/io/PrintWriter;)V

    #@4f
    .line 393
    add-int/lit8 v1, v1, 0x1

    #@51
    goto :goto_42

    #@52
    .line 397
    :cond_52
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@54
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@57
    move-result v0

    #@58
    .line 398
    const-string v2, " "

    #@5a
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5d
    .line 399
    const-string v2, "AppWidgetIds:"

    #@5f
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@62
    .line 400
    const/4 v1, 0x0

    #@63
    :goto_63
    if-ge v1, v0, :cond_73

    #@65
    .line 401
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@67
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6a
    move-result-object v2

    #@6b
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@6d
    invoke-direct {p0, v2, v1, p2}, Lcom/android/server/AppWidgetServiceImpl;->dumpAppWidgetId(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;ILjava/io/PrintWriter;)V

    #@70
    .line 400
    add-int/lit8 v1, v1, 0x1

    #@72
    goto :goto_63

    #@73
    .line 404
    :cond_73
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@75
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@78
    move-result v0

    #@79
    .line 405
    const-string v2, " "

    #@7b
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7e
    .line 406
    const-string v2, "Hosts:"

    #@80
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@83
    .line 407
    const/4 v1, 0x0

    #@84
    :goto_84
    if-ge v1, v0, :cond_94

    #@86
    .line 408
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@88
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8b
    move-result-object v2

    #@8c
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@8e
    invoke-direct {p0, v2, v1, p2}, Lcom/android/server/AppWidgetServiceImpl;->dumpHost(Lcom/android/server/AppWidgetServiceImpl$Host;ILjava/io/PrintWriter;)V

    #@91
    .line 407
    add-int/lit8 v1, v1, 0x1

    #@93
    goto :goto_84

    #@94
    .line 411
    :cond_94
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedProviders:Ljava/util/ArrayList;

    #@96
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@99
    move-result v0

    #@9a
    .line 412
    const-string v2, " "

    #@9c
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9f
    .line 413
    const-string v2, "Deleted Providers:"

    #@a1
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a4
    .line 414
    const/4 v1, 0x0

    #@a5
    :goto_a5
    if-ge v1, v0, :cond_b5

    #@a7
    .line 415
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedProviders:Ljava/util/ArrayList;

    #@a9
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ac
    move-result-object v2

    #@ad
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@af
    invoke-direct {p0, v2, v1, p2}, Lcom/android/server/AppWidgetServiceImpl;->dumpProvider(Lcom/android/server/AppWidgetServiceImpl$Provider;ILjava/io/PrintWriter;)V

    #@b2
    .line 414
    add-int/lit8 v1, v1, 0x1

    #@b4
    goto :goto_a5

    #@b5
    .line 418
    :cond_b5
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedHosts:Ljava/util/ArrayList;

    #@b7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@ba
    move-result v0

    #@bb
    .line 419
    const-string v2, " "

    #@bd
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c0
    .line 420
    const-string v2, "Deleted Hosts:"

    #@c2
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c5
    .line 421
    const/4 v1, 0x0

    #@c6
    :goto_c6
    if-ge v1, v0, :cond_d6

    #@c8
    .line 422
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedHosts:Ljava/util/ArrayList;

    #@ca
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@cd
    move-result-object v2

    #@ce
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@d0
    invoke-direct {p0, v2, v1, p2}, Lcom/android/server/AppWidgetServiceImpl;->dumpHost(Lcom/android/server/AppWidgetServiceImpl$Host;ILjava/io/PrintWriter;)V

    #@d3
    .line 421
    add-int/lit8 v1, v1, 0x1

    #@d5
    goto :goto_c6

    #@d6
    .line 424
    :cond_d6
    monitor-exit v3

    #@d7
    goto/16 :goto_32

    #@d9
    .end local v0           #N:I
    .end local v1           #i:I
    :catchall_d9
    move-exception v2

    #@da
    monitor-exit v3
    :try_end_db
    .catchall {:try_start_36 .. :try_end_db} :catchall_d9

    #@db
    throw v2
.end method

.method enforceCallingUid(Ljava/lang/String;)I
    .registers 8
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 1507
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 1510
    .local v0, callingUid:I
    :try_start_4
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->getUidForPackage(Ljava/lang/String;)I
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_7} :catch_27

    #@7
    move-result v2

    #@8
    .line 1515
    .local v2, packageUid:I
    invoke-static {v0, v2}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_41

    #@e
    .line 1516
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "packageName and uid don\'t match packageName="

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v3

    #@27
    .line 1511
    .end local v2           #packageUid:I
    :catch_27
    move-exception v1

    #@28
    .line 1512
    .local v1, ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "packageName and uid don\'t match packageName="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@40
    throw v3

    #@41
    .line 1519
    .end local v1           #ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2       #packageUid:I
    :cond_41
    return v0
.end method

.method enforceSystemOrCallingUid(Ljava/lang/String;)I
    .registers 5
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 1499
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 1500
    .local v0, callingUid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v1

    #@8
    const/16 v2, 0x3e8

    #@a
    if-eq v1, v2, :cond_e

    #@c
    if-nez v0, :cond_f

    #@e
    .line 1503
    .end local v0           #callingUid:I
    :cond_e
    :goto_e
    return v0

    #@f
    .restart local v0       #callingUid:I
    :cond_f
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->enforceCallingUid(Ljava/lang/String;)I

    #@12
    move-result v0

    #@13
    goto :goto_e
.end method

.method public getAppWidgetIds(Landroid/content/ComponentName;)[I
    .registers 6
    .parameter "provider"

    #@0
    .prologue
    .line 1360
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 1361
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 1362
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupProviderLocked(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@9
    move-result-object v0

    #@a
    .line 1363
    .local v0, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-eqz v0, :cond_1a

    #@c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@f
    move-result v1

    #@10
    iget v3, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->uid:I

    #@12
    if-ne v1, v3, :cond_1a

    #@14
    .line 1364
    invoke-static {v0}, Lcom/android/server/AppWidgetServiceImpl;->getAppWidgetIds(Lcom/android/server/AppWidgetServiceImpl$Provider;)[I

    #@17
    move-result-object v1

    #@18
    monitor-exit v2

    #@19
    .line 1366
    :goto_19
    return-object v1

    #@1a
    :cond_1a
    const/4 v1, 0x0

    #@1b
    new-array v1, v1, [I

    #@1d
    monitor-exit v2

    #@1e
    goto :goto_19

    #@1f
    .line 1368
    .end local v0           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method

.method public getAppWidgetIdsForHost(I)[I
    .registers 6
    .parameter "hostId"

    #@0
    .prologue
    .line 1381
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 1382
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 1383
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@9
    move-result v0

    #@a
    .line 1384
    .local v0, callingUid:I
    invoke-virtual {p0, v0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupHostLocked(II)Lcom/android/server/AppWidgetServiceImpl$Host;

    #@d
    move-result-object v1

    #@e
    .line 1385
    .local v1, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    if-eqz v1, :cond_16

    #@10
    .line 1386
    invoke-static {v1}, Lcom/android/server/AppWidgetServiceImpl;->getAppWidgetIds(Lcom/android/server/AppWidgetServiceImpl$Host;)[I

    #@13
    move-result-object v2

    #@14
    monitor-exit v3

    #@15
    .line 1388
    :goto_15
    return-object v2

    #@16
    :cond_16
    const/4 v2, 0x0

    #@17
    new-array v2, v2, [I

    #@19
    monitor-exit v3

    #@1a
    goto :goto_15

    #@1b
    .line 1390
    .end local v0           #callingUid:I
    .end local v1           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :catchall_1b
    move-exception v2

    #@1c
    monitor-exit v3
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v2
.end method

.method public getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;
    .registers 5
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 846
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 847
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 848
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@9
    move-result-object v0

    #@a
    .line 849
    .local v0, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-eqz v0, :cond_20

    #@c
    iget-object v1, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@e
    if-eqz v1, :cond_20

    #@10
    iget-object v1, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@12
    iget-boolean v1, v1, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@14
    if-nez v1, :cond_20

    #@16
    .line 850
    iget-object v1, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@18
    iget-object v1, v1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@1a
    invoke-direct {p0, v1}, Lcom/android/server/AppWidgetServiceImpl;->cloneIfLocalBinder(Landroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetProviderInfo;

    #@1d
    move-result-object v1

    #@1e
    monitor-exit v2

    #@1f
    .line 852
    :goto_1f
    return-object v1

    #@20
    :cond_20
    const/4 v1, 0x0

    #@21
    monitor-exit v2

    #@22
    goto :goto_1f

    #@23
    .line 853
    .end local v0           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v1
.end method

.method public getAppWidgetOptions(I)Landroid/os/Bundle;
    .registers 5
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 953
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 954
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 955
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@9
    move-result-object v0

    #@a
    .line 956
    .local v0, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-eqz v0, :cond_18

    #@c
    iget-object v1, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@e
    if-eqz v1, :cond_18

    #@10
    .line 957
    iget-object v1, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@12
    invoke-direct {p0, v1}, Lcom/android/server/AppWidgetServiceImpl;->cloneIfLocalBinder(Landroid/os/Bundle;)Landroid/os/Bundle;

    #@15
    move-result-object v1

    #@16
    monitor-exit v2

    #@17
    .line 959
    :goto_17
    return-object v1

    #@18
    :cond_18
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    #@1a
    monitor-exit v2

    #@1b
    goto :goto_17

    #@1c
    .line 961
    .end local v0           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method public getAppWidgetViews(I)Landroid/widget/RemoteViews;
    .registers 5
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 857
    sget-boolean v1, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@2
    if-eqz v1, :cond_1a

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "getAppWidgetViews id="

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {p0, v1}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@1a
    .line 858
    :cond_1a
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@1c
    monitor-enter v2

    #@1d
    .line 859
    :try_start_1d
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@20
    .line 860
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@23
    move-result-object v0

    #@24
    .line 861
    .local v0, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-eqz v0, :cond_2e

    #@26
    .line 862
    iget-object v1, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@28
    invoke-direct {p0, v1}, Lcom/android/server/AppWidgetServiceImpl;->cloneIfLocalBinder(Landroid/widget/RemoteViews;)Landroid/widget/RemoteViews;

    #@2b
    move-result-object v1

    #@2c
    monitor-exit v2

    #@2d
    .line 865
    :goto_2d
    return-object v1

    #@2e
    .line 864
    :cond_2e
    sget-boolean v1, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@30
    if-eqz v1, :cond_37

    #@32
    const-string v1, "   couldn\'t find appwidgetid"

    #@34
    invoke-direct {p0, v1}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@37
    .line 865
    :cond_37
    const/4 v1, 0x0

    #@38
    monitor-exit v2

    #@39
    goto :goto_2d

    #@3a
    .line 866
    .end local v0           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_3a
    move-exception v1

    #@3b
    monitor-exit v2
    :try_end_3c
    .catchall {:try_start_1d .. :try_end_3c} :catchall_3a

    #@3c
    throw v1
.end method

.method public getInstalledProviders(I)Ljava/util/List;
    .registers 8
    .parameter "categoryFilter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 870
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v5

    #@3
    .line 871
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 872
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v0

    #@c
    .line 873
    .local v0, N:I
    new-instance v3, Ljava/util/ArrayList;

    #@e
    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@11
    .line 874
    .local v3, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/appwidget/AppWidgetProviderInfo;>;"
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v0, :cond_33

    #@14
    .line 875
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@1c
    .line 876
    .local v2, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    iget-boolean v4, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@1e
    if-nez v4, :cond_30

    #@20
    iget-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@22
    iget v4, v4, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    #@24
    and-int/2addr v4, p1

    #@25
    if-eqz v4, :cond_30

    #@27
    .line 877
    iget-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@29
    invoke-direct {p0, v4}, Lcom/android/server/AppWidgetServiceImpl;->cloneIfLocalBinder(Landroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetProviderInfo;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    .line 874
    :cond_30
    add-int/lit8 v1, v1, 0x1

    #@32
    goto :goto_12

    #@33
    .line 880
    .end local v2           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_33
    monitor-exit v5

    #@34
    return-object v3

    #@35
    .line 881
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v3           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/appwidget/AppWidgetProviderInfo;>;"
    :catchall_35
    move-exception v4

    #@36
    monitor-exit v5
    :try_end_37
    .catchall {:try_start_3 .. :try_end_37} :catchall_35

    #@37
    throw v4
.end method

.method getUidForPackage(Ljava/lang/String;)I
    .registers 6
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1486
    const/4 v0, 0x0

    #@1
    .line 1488
    .local v0, pkgInfo:Landroid/content/pm/PackageInfo;
    :try_start_1
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mPm:Landroid/content/pm/IPackageManager;

    #@3
    const/4 v2, 0x0

    #@4
    iget v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@6
    invoke-interface {v1, p1, v2, v3}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_9} :catch_1b

    #@9
    move-result-object v0

    #@a
    .line 1492
    :goto_a
    if-eqz v0, :cond_10

    #@c
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@e
    if-nez v1, :cond_16

    #@10
    .line 1493
    :cond_10
    new-instance v1, Landroid/content/pm/PackageManager$NameNotFoundException;

    #@12
    invoke-direct {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;-><init>()V

    #@15
    throw v1

    #@16
    .line 1495
    :cond_16
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@18
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1a
    return v1

    #@1b
    .line 1489
    :catch_1b
    move-exception v1

    #@1c
    goto :goto_a
.end method

.method public hasBindAppWidgetPermission(Ljava/lang/String;)Z
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 652
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MODIFY_APPWIDGET_BIND_PERMISSIONS"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "hasBindAppWidgetPermission packageName="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 656
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@1c
    monitor-enter v1

    #@1d
    .line 657
    :try_start_1d
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@20
    .line 658
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mPackagesWithBindWidgetPermission:Ljava/util/HashSet;

    #@22
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    monitor-exit v1

    #@27
    return v0

    #@28
    .line 659
    :catchall_28
    move-exception v0

    #@29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_1d .. :try_end_2a} :catchall_28

    #@2a
    throw v0
.end method

.method loadAppWidgetListLocked()V
    .registers 10

    #@0
    .prologue
    .line 1252
    new-instance v3, Landroid/content/Intent;

    #@2
    const-string v5, "android.appwidget.action.APPWIDGET_UPDATE"

    #@4
    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1254
    .local v3, intent:Landroid/content/Intent;
    :try_start_7
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mPm:Landroid/content/pm/IPackageManager;

    #@9
    iget-object v6, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {v3, v6}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    const/16 v7, 0x80

    #@15
    iget v8, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@17
    invoke-interface {v5, v3, v6, v7, v8}, Landroid/content/pm/IPackageManager;->queryIntentReceivers(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@1a
    move-result-object v1

    #@1b
    .line 1258
    .local v1, broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v1, :cond_2d

    #@1d
    const/4 v0, 0x0

    #@1e
    .line 1259
    .local v0, N:I
    :goto_1e
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    if-ge v2, v0, :cond_33

    #@21
    .line 1260
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v4

    #@25
    check-cast v4, Landroid/content/pm/ResolveInfo;

    #@27
    .line 1261
    .local v4, ri:Landroid/content/pm/ResolveInfo;
    invoke-virtual {p0, v4}, Lcom/android/server/AppWidgetServiceImpl;->addProviderLocked(Landroid/content/pm/ResolveInfo;)Z

    #@2a
    .line 1259
    add-int/lit8 v2, v2, 0x1

    #@2c
    goto :goto_1f

    #@2d
    .line 1258
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #ri:Landroid/content/pm/ResolveInfo;
    :cond_2d
    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_30} :catch_32

    #@30
    move-result v0

    #@31
    goto :goto_1e

    #@32
    .line 1263
    .end local v1           #broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catch_32
    move-exception v5

    #@33
    .line 1266
    :cond_33
    return-void
.end method

.method loadStateLocked()V
    .registers 7

    #@0
    .prologue
    .line 1540
    invoke-virtual {p0}, Lcom/android/server/AppWidgetServiceImpl;->savedStateFile()Landroid/util/AtomicFile;

    #@3
    move-result-object v1

    #@4
    .line 1542
    .local v1, file:Landroid/util/AtomicFile;
    :try_start_4
    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@7
    move-result-object v2

    #@8
    .line 1543
    .local v2, stream:Ljava/io/FileInputStream;
    invoke-virtual {p0, v2}, Lcom/android/server/AppWidgetServiceImpl;->readStateFromFileLocked(Ljava/io/FileInputStream;)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_b} :catch_2b

    #@b
    .line 1545
    if-eqz v2, :cond_10

    #@d
    .line 1547
    :try_start_d
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_10} :catch_11
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_10} :catch_2b

    #@10
    .line 1555
    .end local v2           #stream:Ljava/io/FileInputStream;
    :cond_10
    :goto_10
    return-void

    #@11
    .line 1548
    .restart local v2       #stream:Ljava/io/FileInputStream;
    :catch_11
    move-exception v0

    #@12
    .line 1549
    .local v0, e:Ljava/io/IOException;
    :try_start_12
    const-string v3, "AppWidgetServiceImpl"

    #@14
    new-instance v4, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v5, "Failed to close state FileInputStream "

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_2a} :catch_2b

    #@2a
    goto :goto_10

    #@2b
    .line 1552
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #stream:Ljava/io/FileInputStream;
    :catch_2b
    move-exception v0

    #@2c
    .line 1553
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v3, "AppWidgetServiceImpl"

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v5, "Failed to read state: "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_10
.end method

.method lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    .registers 7
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 1196
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v1

    #@4
    .line 1197
    .local v1, callingUid:I
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 1198
    .local v0, N:I
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v0, :cond_23

    #@d
    .line 1199
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v3

    #@13
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@15
    .line 1200
    .local v3, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    iget v4, v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@17
    if-ne v4, p1, :cond_20

    #@19
    invoke-virtual {p0, v3, v1}, Lcom/android/server/AppWidgetServiceImpl;->canAccessAppWidgetId(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;I)Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_20

    #@1f
    .line 1204
    .end local v3           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :goto_1f
    return-object v3

    #@20
    .line 1198
    .restart local v3       #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_20
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_b

    #@23
    .line 1204
    .end local v3           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_23
    const/4 v3, 0x0

    #@24
    goto :goto_1f
.end method

.method lookupHostLocked(II)Lcom/android/server/AppWidgetServiceImpl$Host;
    .registers 7
    .parameter "uid"
    .parameter "hostId"

    #@0
    .prologue
    .line 1219
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 1220
    .local v0, N:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v0, :cond_1f

    #@9
    .line 1221
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@11
    .line 1222
    .local v1, h:Lcom/android/server/AppWidgetServiceImpl$Host;
    invoke-virtual {v1, p1}, Lcom/android/server/AppWidgetServiceImpl$Host;->uidMatches(I)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_1c

    #@17
    iget v3, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->hostId:I

    #@19
    if-ne v3, p2, :cond_1c

    #@1b
    .line 1226
    .end local v1           #h:Lcom/android/server/AppWidgetServiceImpl$Host;
    :goto_1b
    return-object v1

    #@1c
    .line 1220
    .restart local v1       #h:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_7

    #@1f
    .line 1226
    .end local v1           #h:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_1b
.end method

.method lookupOrAddHostLocked(ILjava/lang/String;I)Lcom/android/server/AppWidgetServiceImpl$Host;
    .registers 9
    .parameter "uid"
    .parameter "packageName"
    .parameter "hostId"

    #@0
    .prologue
    .line 1230
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 1231
    .local v0, N:I
    const/4 v3, 0x0

    #@7
    .local v3, i:I
    :goto_7
    if-ge v3, v0, :cond_21

    #@9
    .line 1232
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@11
    .line 1233
    .local v1, h:Lcom/android/server/AppWidgetServiceImpl$Host;
    iget v4, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->hostId:I

    #@13
    if-ne v4, p3, :cond_1e

    #@15
    iget-object v4, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_1e

    #@1d
    .line 1242
    .end local v1           #h:Lcom/android/server/AppWidgetServiceImpl$Host;
    :goto_1d
    return-object v1

    #@1e
    .line 1231
    .restart local v1       #h:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_1e
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_7

    #@21
    .line 1237
    .end local v1           #h:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_21
    new-instance v2, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@23
    invoke-direct {v2}, Lcom/android/server/AppWidgetServiceImpl$Host;-><init>()V

    #@26
    .line 1238
    .local v2, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    iput-object p2, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;

    #@28
    .line 1239
    iput p1, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->uid:I

    #@2a
    .line 1240
    iput p3, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->hostId:I

    #@2c
    .line 1241
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31
    move-object v1, v2

    #@32
    .line 1242
    goto :goto_1d
.end method

.method lookupProviderLocked(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;
    .registers 6
    .parameter "provider"

    #@0
    .prologue
    .line 1208
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 1209
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_1f

    #@9
    .line 1210
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@11
    .line 1211
    .local v2, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@13
    iget-object v3, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@15
    invoke-virtual {v3, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_1c

    #@1b
    .line 1215
    .end local v2           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :goto_1b
    return-object v2

    #@1c
    .line 1209
    .restart local v2       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_7

    #@1f
    .line 1215
    .end local v2           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_1f
    const/4 v2, 0x0

    #@20
    goto :goto_1b
.end method

.method public notifyAppWidgetViewDataChanged([II)V
    .registers 8
    .parameter "appWidgetIds"
    .parameter "viewId"

    #@0
    .prologue
    .line 988
    if-nez p1, :cond_3

    #@2
    .line 1003
    :cond_2
    :goto_2
    return-void

    #@3
    .line 991
    :cond_3
    array-length v3, p1

    #@4
    if-eqz v3, :cond_2

    #@6
    .line 994
    array-length v0, p1

    #@7
    .line 996
    .local v0, N:I
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@9
    monitor-enter v4

    #@a
    .line 997
    :try_start_a
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@d
    .line 998
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v0, :cond_1c

    #@10
    .line 999
    aget v3, p1, v1

    #@12
    invoke-virtual {p0, v3}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@15
    move-result-object v2

    #@16
    .line 1000
    .local v2, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    invoke-virtual {p0, v2, p2}, Lcom/android/server/AppWidgetServiceImpl;->notifyAppWidgetViewDataChangedInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;I)V

    #@19
    .line 998
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_e

    #@1c
    .line 1002
    .end local v2           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_1c
    monitor-exit v4

    #@1d
    goto :goto_2

    #@1e
    .end local v1           #i:I
    :catchall_1e
    move-exception v3

    #@1f
    monitor-exit v4
    :try_end_20
    .catchall {:try_start_a .. :try_end_20} :catchall_1e

    #@20
    throw v3
.end method

.method notifyAppWidgetViewDataChangedInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;I)V
    .registers 14
    .parameter "id"
    .parameter "viewId"

    #@0
    .prologue
    .line 1061
    if-eqz p1, :cond_7c

    #@2
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@4
    if-eqz v9, :cond_7c

    #@6
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@8
    iget-boolean v9, v9, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@a
    if-nez v9, :cond_7c

    #@c
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@e
    iget-boolean v9, v9, Lcom/android/server/AppWidgetServiceImpl$Host;->zombie:Z

    #@10
    if-nez v9, :cond_7c

    #@12
    .line 1063
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@14
    iget-object v9, v9, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@16
    if-eqz v9, :cond_21

    #@18
    .line 1066
    :try_start_18
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@1a
    iget-object v9, v9, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@1c
    iget v10, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@1e
    invoke-interface {v9, v10, p2}, Lcom/android/internal/appwidget/IAppWidgetHost;->viewDataChanged(II)V
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_21} :catch_70

    #@21
    .line 1076
    :cond_21
    :goto_21
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@23
    iget-object v9, v9, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@25
    if-nez v9, :cond_7c

    #@27
    .line 1077
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@29
    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@2c
    move-result-object v5

    #@2d
    .line 1078
    .local v5, keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/Intent$FilterComparison;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@30
    move-result-object v2

    #@31
    .local v2, i$:Ljava/util/Iterator;
    :cond_31
    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@34
    move-result v9

    #@35
    if-eqz v9, :cond_7c

    #@37
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3a
    move-result-object v4

    #@3b
    check-cast v4, Landroid/content/Intent$FilterComparison;

    #@3d
    .line 1079
    .local v4, key:Landroid/content/Intent$FilterComparison;
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mRemoteViewsServicesAppWidgets:Ljava/util/HashMap;

    #@3f
    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    move-result-object v9

    #@43
    check-cast v9, Ljava/util/HashSet;

    #@45
    iget v10, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@47
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4a
    move-result-object v10

    #@4b
    invoke-virtual {v9, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@4e
    move-result v9

    #@4f
    if-eqz v9, :cond_31

    #@51
    .line 1080
    invoke-virtual {v4}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@54
    move-result-object v3

    #@55
    .line 1082
    .local v3, intent:Landroid/content/Intent;
    new-instance v0, Lcom/android/server/AppWidgetServiceImpl$3;

    #@57
    invoke-direct {v0, p0}, Lcom/android/server/AppWidgetServiceImpl$3;-><init>(Lcom/android/server/AppWidgetServiceImpl;)V

    #@5a
    .line 1103
    .local v0, conn:Landroid/content/ServiceConnection;
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@5c
    iget v9, v9, Lcom/android/server/AppWidgetServiceImpl$Provider;->uid:I

    #@5e
    invoke-static {v9}, Landroid/os/UserHandle;->getUserId(I)I

    #@61
    move-result v8

    #@62
    .line 1105
    .local v8, userId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@65
    move-result-wide v6

    #@66
    .line 1107
    .local v6, token:J
    :try_start_66
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@68
    const/4 v10, 0x1

    #@69
    invoke-virtual {v9, v3, v0, v10, v8}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_6c
    .catchall {:try_start_66 .. :try_end_6c} :catchall_77

    #@6c
    .line 1109
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6f
    goto :goto_31

    #@70
    .line 1067
    .end local v0           #conn:Landroid/content/ServiceConnection;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #key:Landroid/content/Intent$FilterComparison;
    .end local v5           #keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/Intent$FilterComparison;>;"
    .end local v6           #token:J
    .end local v8           #userId:I
    :catch_70
    move-exception v1

    #@71
    .line 1070
    .local v1, e:Landroid/os/RemoteException;
    iget-object v9, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@73
    const/4 v10, 0x0

    #@74
    iput-object v10, v9, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@76
    goto :goto_21

    #@77
    .line 1109
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v0       #conn:Landroid/content/ServiceConnection;
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #intent:Landroid/content/Intent;
    .restart local v4       #key:Landroid/content/Intent$FilterComparison;
    .restart local v5       #keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/Intent$FilterComparison;>;"
    .restart local v6       #token:J
    .restart local v8       #userId:I
    :catchall_77
    move-exception v9

    #@78
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7b
    throw v9

    #@7c
    .line 1115
    .end local v0           #conn:Landroid/content/ServiceConnection;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #key:Landroid/content/Intent$FilterComparison;
    .end local v5           #keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/Intent$FilterComparison;>;"
    .end local v6           #token:J
    .end local v8           #userId:I
    :cond_7c
    return-void
.end method

.method notifyHostsForProvidersChangedLocked()V
    .registers 6

    #@0
    .prologue
    .line 1996
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 1997
    .local v0, N:I
    add-int/lit8 v3, v0, -0x1

    #@8
    .local v3, i:I
    :goto_8
    if-ltz v3, :cond_23

    #@a
    .line 1998
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@12
    .line 2000
    .local v2, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :try_start_12
    iget-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@14
    if-eqz v4, :cond_1b

    #@16
    .line 2001
    iget-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@18
    invoke-interface {v4}, Lcom/android/internal/appwidget/IAppWidgetHost;->providersChanged()V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_1b} :catch_1e

    #@1b
    .line 1997
    :cond_1b
    :goto_1b
    add-int/lit8 v3, v3, -0x1

    #@1d
    goto :goto_8

    #@1e
    .line 2003
    :catch_1e
    move-exception v1

    #@1f
    .line 2007
    .local v1, ex:Landroid/os/RemoteException;
    const/4 v4, 0x0

    #@20
    iput-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@22
    goto :goto_1b

    #@23
    .line 2010
    .end local v1           #ex:Landroid/os/RemoteException;
    .end local v2           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_23
    return-void
.end method

.method onBroadcastReceived(Landroid/content/Intent;)V
    .registers 16
    .parameter "intent"

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    .line 260
    sget-boolean v11, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@3
    if-eqz v11, :cond_1b

    #@5
    new-instance v11, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v12, "onBroadcast "

    #@c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v11

    #@10
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v11

    #@14
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v11

    #@18
    invoke-direct {p0, v11}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@1b
    .line 261
    :cond_1b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 262
    .local v0, action:Ljava/lang/String;
    const/4 v1, 0x0

    #@20
    .line 263
    .local v1, added:Z
    const/4 v3, 0x0

    #@21
    .line 264
    .local v3, changed:Z
    const/4 v9, 0x0

    #@22
    .line 265
    .local v9, providersModified:Z
    const/4 v7, 0x0

    #@23
    .line 266
    .local v7, pkgList:[Ljava/lang/String;
    const-string v11, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    #@25
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v11

    #@29
    if-eqz v11, :cond_38

    #@2b
    .line 267
    const-string v11, "android.intent.extra.changed_package_list"

    #@2d
    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    .line 268
    const/4 v1, 0x1

    #@32
    .line 285
    :goto_32
    if-eqz v7, :cond_37

    #@34
    array-length v11, v7

    #@35
    if-nez v11, :cond_66

    #@37
    .line 328
    :cond_37
    :goto_37
    return-void

    #@38
    .line 269
    :cond_38
    const-string v11, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@3a
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v11

    #@3e
    if-eqz v11, :cond_48

    #@40
    .line 270
    const-string v11, "android.intent.extra.changed_package_list"

    #@42
    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    .line 271
    const/4 v1, 0x0

    #@47
    goto :goto_32

    #@48
    .line 273
    :cond_48
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@4b
    move-result-object v10

    #@4c
    .line 274
    .local v10, uri:Landroid/net/Uri;
    if-eqz v10, :cond_37

    #@4e
    .line 277
    invoke-virtual {v10}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@51
    move-result-object v8

    #@52
    .line 278
    .local v8, pkgName:Ljava/lang/String;
    if-eqz v8, :cond_37

    #@54
    .line 281
    const/4 v11, 0x1

    #@55
    new-array v7, v11, [Ljava/lang/String;

    #@57
    .end local v7           #pkgList:[Ljava/lang/String;
    aput-object v8, v7, v13

    #@59
    .line 282
    .restart local v7       #pkgList:[Ljava/lang/String;
    const-string v11, "android.intent.action.PACKAGE_ADDED"

    #@5b
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v1

    #@5f
    .line 283
    const-string v11, "android.intent.action.PACKAGE_CHANGED"

    #@61
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v3

    #@65
    goto :goto_32

    #@66
    .line 288
    .end local v8           #pkgName:Ljava/lang/String;
    .end local v10           #uri:Landroid/net/Uri;
    :cond_66
    if-nez v1, :cond_6a

    #@68
    if-eqz v3, :cond_b7

    #@6a
    .line 289
    :cond_6a
    iget-object v12, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@6c
    monitor-enter v12

    #@6d
    .line 290
    :try_start_6d
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@70
    .line 291
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@73
    move-result-object v4

    #@74
    .line 292
    .local v4, extras:Landroid/os/Bundle;
    if-nez v3, :cond_81

    #@76
    if-eqz v4, :cond_91

    #@78
    const-string v11, "android.intent.extra.REPLACING"

    #@7a
    const/4 v13, 0x0

    #@7b
    invoke-virtual {v4, v11, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@7e
    move-result v11

    #@7f
    if-eqz v11, :cond_91

    #@81
    .line 294
    :cond_81
    move-object v2, v7

    #@82
    .local v2, arr$:[Ljava/lang/String;
    array-length v6, v2

    #@83
    .local v6, len$:I
    const/4 v5, 0x0

    #@84
    .local v5, i$:I
    :goto_84
    if-ge v5, v6, :cond_a0

    #@86
    aget-object v8, v2, v5

    #@88
    .line 296
    .restart local v8       #pkgName:Ljava/lang/String;
    const/4 v11, 0x0

    #@89
    invoke-virtual {p0, v8, v11}, Lcom/android/server/AppWidgetServiceImpl;->updateProvidersForPackageLocked(Ljava/lang/String;Ljava/util/Set;)Z

    #@8c
    move-result v11

    #@8d
    or-int/2addr v9, v11

    #@8e
    .line 294
    add-int/lit8 v5, v5, 0x1

    #@90
    goto :goto_84

    #@91
    .line 300
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v8           #pkgName:Ljava/lang/String;
    :cond_91
    move-object v2, v7

    #@92
    .restart local v2       #arr$:[Ljava/lang/String;
    array-length v6, v2

    #@93
    .restart local v6       #len$:I
    const/4 v5, 0x0

    #@94
    .restart local v5       #i$:I
    :goto_94
    if-ge v5, v6, :cond_a0

    #@96
    aget-object v8, v2, v5

    #@98
    .line 301
    .restart local v8       #pkgName:Ljava/lang/String;
    invoke-virtual {p0, v8}, Lcom/android/server/AppWidgetServiceImpl;->addProvidersForPackageLocked(Ljava/lang/String;)Z

    #@9b
    move-result v11

    #@9c
    or-int/2addr v9, v11

    #@9d
    .line 300
    add-int/lit8 v5, v5, 0x1

    #@9f
    goto :goto_94

    #@a0
    .line 304
    .end local v8           #pkgName:Ljava/lang/String;
    :cond_a0
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@a3
    .line 305
    monitor-exit v12
    :try_end_a4
    .catchall {:try_start_6d .. :try_end_a4} :catchall_b4

    #@a4
    .line 321
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :cond_a4
    :goto_a4
    if-eqz v9, :cond_37

    #@a6
    .line 323
    iget-object v12, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@a8
    monitor-enter v12

    #@a9
    .line 324
    :try_start_a9
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@ac
    .line 325
    invoke-virtual {p0}, Lcom/android/server/AppWidgetServiceImpl;->notifyHostsForProvidersChangedLocked()V

    #@af
    .line 326
    monitor-exit v12

    #@b0
    goto :goto_37

    #@b1
    :catchall_b1
    move-exception v11

    #@b2
    monitor-exit v12
    :try_end_b3
    .catchall {:try_start_a9 .. :try_end_b3} :catchall_b1

    #@b3
    throw v11

    #@b4
    .line 305
    .end local v4           #extras:Landroid/os/Bundle;
    :catchall_b4
    move-exception v11

    #@b5
    :try_start_b5
    monitor-exit v12
    :try_end_b6
    .catchall {:try_start_b5 .. :try_end_b6} :catchall_b4

    #@b6
    throw v11

    #@b7
    .line 307
    :cond_b7
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@ba
    move-result-object v4

    #@bb
    .line 308
    .restart local v4       #extras:Landroid/os/Bundle;
    if-eqz v4, :cond_c5

    #@bd
    const-string v11, "android.intent.extra.REPLACING"

    #@bf
    invoke-virtual {v4, v11, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@c2
    move-result v11

    #@c3
    if-nez v11, :cond_a4

    #@c5
    .line 311
    :cond_c5
    iget-object v12, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@c7
    monitor-enter v12

    #@c8
    .line 312
    :try_start_c8
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@cb
    .line 313
    move-object v2, v7

    #@cc
    .restart local v2       #arr$:[Ljava/lang/String;
    array-length v6, v2

    #@cd
    .restart local v6       #len$:I
    const/4 v5, 0x0

    #@ce
    .restart local v5       #i$:I
    :goto_ce
    if-ge v5, v6, :cond_dd

    #@d0
    aget-object v8, v2, v5

    #@d2
    .line 314
    .restart local v8       #pkgName:Ljava/lang/String;
    invoke-virtual {p0, v8}, Lcom/android/server/AppWidgetServiceImpl;->removeProvidersForPackageLocked(Ljava/lang/String;)Z

    #@d5
    move-result v11

    #@d6
    or-int/2addr v9, v11

    #@d7
    .line 315
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@da
    .line 313
    add-int/lit8 v5, v5, 0x1

    #@dc
    goto :goto_ce

    #@dd
    .line 317
    .end local v8           #pkgName:Ljava/lang/String;
    :cond_dd
    monitor-exit v12

    #@de
    goto :goto_a4

    #@df
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :catchall_df
    move-exception v11

    #@e0
    monitor-exit v12
    :try_end_e1
    .catchall {:try_start_c8 .. :try_end_e1} :catchall_df

    #@e1
    throw v11
.end method

.method onConfigurationChanged()V
    .registers 10

    #@0
    .prologue
    .line 233
    sget-boolean v7, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@2
    if-eqz v7, :cond_9

    #@4
    const-string v7, "Got onConfigurationChanged()"

    #@6
    invoke-direct {p0, v7}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@9
    .line 234
    :cond_9
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@c
    move-result-object v6

    #@d
    .line 235
    .local v6, revised:Ljava/util/Locale;
    if-eqz v6, :cond_1b

    #@f
    iget-object v7, p0, Lcom/android/server/AppWidgetServiceImpl;->mLocale:Ljava/util/Locale;

    #@11
    if-eqz v7, :cond_1b

    #@13
    iget-object v7, p0, Lcom/android/server/AppWidgetServiceImpl;->mLocale:Ljava/util/Locale;

    #@15
    invoke-virtual {v6, v7}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v7

    #@19
    if-nez v7, :cond_55

    #@1b
    .line 236
    :cond_1b
    iput-object v6, p0, Lcom/android/server/AppWidgetServiceImpl;->mLocale:Ljava/util/Locale;

    #@1d
    .line 238
    iget-object v8, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@1f
    monitor-enter v8

    #@20
    .line 239
    :try_start_20
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@23
    .line 243
    new-instance v3, Ljava/util/ArrayList;

    #@25
    iget-object v7, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@27
    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@2a
    .line 245
    .local v3, installedProviders:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    new-instance v5, Ljava/util/HashSet;

    #@2c
    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    #@2f
    .line 246
    .local v5, removedProviders:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/content/ComponentName;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@32
    move-result v0

    #@33
    .line 247
    .local v0, N:I
    add-int/lit8 v2, v0, -0x1

    #@35
    .local v2, i:I
    :goto_35
    if-ltz v2, :cond_51

    #@37
    .line 248
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v4

    #@3b
    check-cast v4, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@3d
    .line 249
    .local v4, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    iget-object v7, v4, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@3f
    iget-object v1, v7, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@41
    .line 250
    .local v1, cn:Landroid/content/ComponentName;
    invoke-virtual {v5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@44
    move-result v7

    #@45
    if-nez v7, :cond_4e

    #@47
    .line 251
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {p0, v7, v5}, Lcom/android/server/AppWidgetServiceImpl;->updateProvidersForPackageLocked(Ljava/lang/String;Ljava/util/Set;)Z

    #@4e
    .line 247
    :cond_4e
    add-int/lit8 v2, v2, -0x1

    #@50
    goto :goto_35

    #@51
    .line 254
    .end local v1           #cn:Landroid/content/ComponentName;
    .end local v4           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_51
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@54
    .line 255
    monitor-exit v8

    #@55
    .line 257
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #installedProviders:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .end local v5           #removedProviders:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/content/ComponentName;>;"
    :cond_55
    return-void

    #@56
    .line 255
    :catchall_56
    move-exception v7

    #@57
    monitor-exit v8
    :try_end_58
    .catchall {:try_start_20 .. :try_end_58} :catchall_56

    #@58
    throw v7
.end method

.method onUserRemoved()V
    .registers 2

    #@0
    .prologue
    .line 1843
    iget v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@2
    invoke-static {v0}, Lcom/android/server/AppWidgetServiceImpl;->getSettingsFile(I)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@9
    .line 1844
    return-void
.end method

.method onUserStopping()V
    .registers 5

    #@0
    .prologue
    .line 1835
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 1836
    .local v0, N:I
    add-int/lit8 v1, v0, -0x1

    #@8
    .local v1, i:I
    :goto_8
    if-ltz v1, :cond_18

    #@a
    .line 1837
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@12
    .line 1838
    .local v2, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    invoke-virtual {p0, v2}, Lcom/android/server/AppWidgetServiceImpl;->cancelBroadcasts(Lcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@15
    .line 1836
    add-int/lit8 v1, v1, -0x1

    #@17
    goto :goto_8

    #@18
    .line 1840
    .end local v2           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_18
    return-void
.end method

.method public partiallyUpdateAppWidgetIds([ILandroid/widget/RemoteViews;)V
    .registers 10
    .parameter "appWidgetIds"
    .parameter "views"

    #@0
    .prologue
    .line 965
    if-nez p1, :cond_3

    #@2
    .line 985
    :cond_2
    :goto_2
    return-void

    #@3
    .line 968
    :cond_3
    array-length v3, p1

    #@4
    if-eqz v3, :cond_2

    #@6
    .line 971
    array-length v0, p1

    #@7
    .line 973
    .local v0, N:I
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@9
    monitor-enter v4

    #@a
    .line 974
    :try_start_a
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@d
    .line 975
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v0, :cond_47

    #@10
    .line 976
    aget v3, p1, v1

    #@12
    invoke-virtual {p0, v3}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@15
    move-result-object v2

    #@16
    .line 977
    .local v2, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-nez v2, :cond_3b

    #@18
    .line 978
    const-string v3, "AppWidgetServiceImpl"

    #@1a
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "widget id "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    aget v6, p1, v1

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    const-string v6, " not found!"

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 975
    :cond_38
    :goto_38
    add-int/lit8 v1, v1, 0x1

    #@3a
    goto :goto_e

    #@3b
    .line 979
    :cond_3b
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@3d
    if-eqz v3, :cond_38

    #@3f
    .line 981
    const/4 v3, 0x1

    #@40
    invoke-virtual {p0, v2, p2, v3}, Lcom/android/server/AppWidgetServiceImpl;->updateAppWidgetInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;Landroid/widget/RemoteViews;Z)V

    #@43
    goto :goto_38

    #@44
    .line 984
    .end local v1           #i:I
    .end local v2           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :catchall_44
    move-exception v3

    #@45
    monitor-exit v4
    :try_end_46
    .catchall {:try_start_a .. :try_end_46} :catchall_44

    #@46
    throw v3

    #@47
    .restart local v1       #i:I
    :cond_47
    :try_start_47
    monitor-exit v4
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_44

    #@48
    goto :goto_2
.end method

.method pruneHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V
    .registers 3
    .parameter "host"

    #@0
    .prologue
    .line 1246
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_11

    #@8
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@a
    if-nez v0, :cond_11

    #@c
    .line 1247
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@11
    .line 1249
    :cond_11
    return-void
.end method

.method readStateFromFileLocked(Ljava/io/FileInputStream;)V
    .registers 36
    .parameter "stream"

    #@0
    .prologue
    .line 1649
    const/16 v28, 0x0

    #@2
    .line 1651
    .local v28, success:Z
    :try_start_2
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@5
    move-result-object v23

    #@6
    .line 1652
    .local v23, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/16 v31, 0x0

    #@8
    move-object/from16 v0, v23

    #@a
    move-object/from16 v1, p1

    #@c
    move-object/from16 v2, v31

    #@e
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@11
    .line 1655
    const/16 v26, 0x0

    #@13
    .line 1656
    .local v26, providerIndex:I
    new-instance v13, Ljava/util/HashMap;

    #@15
    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    #@18
    .line 1658
    .local v13, loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    :cond_18
    invoke-interface/range {v23 .. v23}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@1b
    move-result v30

    #@1c
    .line 1659
    .local v30, type:I
    const/16 v31, 0x2

    #@1e
    move/from16 v0, v30

    #@20
    move/from16 v1, v31

    #@22
    if-ne v0, v1, :cond_d4

    #@24
    .line 1660
    invoke-interface/range {v23 .. v23}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@27
    move-result-object v29

    #@28
    .line 1661
    .local v29, tag:Ljava/lang/String;
    const-string v31, "p"

    #@2a
    move-object/from16 v0, v31

    #@2c
    move-object/from16 v1, v29

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v31

    #@32
    if-eqz v31, :cond_127

    #@34
    .line 1664
    const/16 v31, 0x0

    #@36
    const-string v32, "pkg"

    #@38
    move-object/from16 v0, v23

    #@3a
    move-object/from16 v1, v31

    #@3c
    move-object/from16 v2, v32

    #@3e
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v24

    #@42
    .line 1665
    .local v24, pkg:Ljava/lang/String;
    const/16 v31, 0x0

    #@44
    const-string v32, "cl"

    #@46
    move-object/from16 v0, v23

    #@48
    move-object/from16 v1, v31

    #@4a
    move-object/from16 v2, v32

    #@4c
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4f
    move-result-object v6

    #@50
    .line 1667
    .local v6, cl:Ljava/lang/String;
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;
    :try_end_53
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_53} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_53} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_53} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_53} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_53} :catch_3c2

    #@53
    move-result-object v21

    #@54
    .line 1669
    .local v21, packageManager:Landroid/content/pm/IPackageManager;
    :try_start_54
    new-instance v31, Landroid/content/ComponentName;

    #@56
    move-object/from16 v0, v31

    #@58
    move-object/from16 v1, v24

    #@5a
    invoke-direct {v0, v1, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    const/16 v32, 0x0

    #@5f
    move-object/from16 v0, p0

    #@61
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@63
    move/from16 v33, v0

    #@65
    move-object/from16 v0, v21

    #@67
    move-object/from16 v1, v31

    #@69
    move/from16 v2, v32

    #@6b
    move/from16 v3, v33

    #@6d
    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getReceiverInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;
    :try_end_70
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_70} :catch_106
    .catch Ljava/lang/NullPointerException; {:try_start_54 .. :try_end_70} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_54 .. :try_end_70} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_54 .. :try_end_70} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_70} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_54 .. :try_end_70} :catch_3c2

    #@70
    .line 1676
    :goto_70
    :try_start_70
    new-instance v31, Landroid/content/ComponentName;

    #@72
    move-object/from16 v0, v31

    #@74
    move-object/from16 v1, v24

    #@76
    invoke-direct {v0, v1, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@79
    move-object/from16 v0, p0

    #@7b
    move-object/from16 v1, v31

    #@7d
    invoke-virtual {v0, v1}, Lcom/android/server/AppWidgetServiceImpl;->lookupProviderLocked(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@80
    move-result-object v19

    #@81
    .line 1677
    .local v19, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-nez v19, :cond_c5

    #@83
    move-object/from16 v0, p0

    #@85
    iget-boolean v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mSafeMode:Z

    #@87
    move/from16 v31, v0

    #@89
    if-eqz v31, :cond_c5

    #@8b
    .line 1679
    new-instance v19, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@8d
    .end local v19           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    invoke-direct/range {v19 .. v19}, Lcom/android/server/AppWidgetServiceImpl$Provider;-><init>()V

    #@90
    .line 1680
    .restart local v19       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    new-instance v31, Landroid/appwidget/AppWidgetProviderInfo;

    #@92
    invoke-direct/range {v31 .. v31}, Landroid/appwidget/AppWidgetProviderInfo;-><init>()V

    #@95
    move-object/from16 v0, v31

    #@97
    move-object/from16 v1, v19

    #@99
    iput-object v0, v1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@9b
    .line 1681
    move-object/from16 v0, v19

    #@9d
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@9f
    move-object/from16 v31, v0

    #@a1
    new-instance v32, Landroid/content/ComponentName;

    #@a3
    move-object/from16 v0, v32

    #@a5
    move-object/from16 v1, v24

    #@a7
    invoke-direct {v0, v1, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@aa
    move-object/from16 v0, v32

    #@ac
    move-object/from16 v1, v31

    #@ae
    iput-object v0, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@b0
    .line 1682
    const/16 v31, 0x1

    #@b2
    move/from16 v0, v31

    #@b4
    move-object/from16 v1, v19

    #@b6
    iput-boolean v0, v1, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@b8
    .line 1683
    move-object/from16 v0, p0

    #@ba
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@bc
    move-object/from16 v31, v0

    #@be
    move-object/from16 v0, v31

    #@c0
    move-object/from16 v1, v19

    #@c2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c5
    .line 1685
    :cond_c5
    if-eqz v19, :cond_d2

    #@c7
    .line 1687
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ca
    move-result-object v31

    #@cb
    move-object/from16 v0, v31

    #@cd
    move-object/from16 v1, v19

    #@cf
    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d2
    .catch Ljava/lang/NullPointerException; {:try_start_70 .. :try_end_d2} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_70 .. :try_end_d2} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_70 .. :try_end_d2} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_70 .. :try_end_d2} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_70 .. :try_end_d2} :catch_3c2

    #@d2
    .line 1689
    :cond_d2
    add-int/lit8 v26, v26, 0x1

    #@d4
    .line 1780
    .end local v6           #cl:Ljava/lang/String;
    .end local v19           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .end local v21           #packageManager:Landroid/content/pm/IPackageManager;
    .end local v24           #pkg:Ljava/lang/String;
    .end local v29           #tag:Ljava/lang/String;
    :cond_d4
    :goto_d4
    const/16 v31, 0x1

    #@d6
    move/from16 v0, v30

    #@d8
    move/from16 v1, v31

    #@da
    if-ne v0, v1, :cond_18

    #@dc
    .line 1781
    const/16 v28, 0x1

    #@de
    .line 1794
    .end local v13           #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .end local v23           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v26           #providerIndex:I
    .end local v30           #type:I
    :goto_de
    if-eqz v28, :cond_3df

    #@e0
    .line 1797
    move-object/from16 v0, p0

    #@e2
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@e4
    move-object/from16 v31, v0

    #@e6
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    #@e9
    move-result v31

    #@ea
    add-int/lit8 v11, v31, -0x1

    #@ec
    .local v11, i:I
    :goto_ec
    if-ltz v11, :cond_41f

    #@ee
    .line 1798
    move-object/from16 v0, p0

    #@f0
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@f2
    move-object/from16 v31, v0

    #@f4
    move-object/from16 v0, v31

    #@f6
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f9
    move-result-object v31

    #@fa
    check-cast v31, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@fc
    move-object/from16 v0, p0

    #@fe
    move-object/from16 v1, v31

    #@100
    invoke-virtual {v0, v1}, Lcom/android/server/AppWidgetServiceImpl;->pruneHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V

    #@103
    .line 1797
    add-int/lit8 v11, v11, -0x1

    #@105
    goto :goto_ec

    #@106
    .line 1670
    .end local v11           #i:I
    .restart local v6       #cl:Ljava/lang/String;
    .restart local v13       #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .restart local v21       #packageManager:Landroid/content/pm/IPackageManager;
    .restart local v23       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v24       #pkg:Ljava/lang/String;
    .restart local v26       #providerIndex:I
    .restart local v29       #tag:Ljava/lang/String;
    .restart local v30       #type:I
    :catch_106
    move-exception v7

    #@107
    .line 1671
    .local v7, e:Landroid/os/RemoteException;
    :try_start_107
    move-object/from16 v0, p0

    #@109
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@10b
    move-object/from16 v31, v0

    #@10d
    invoke-virtual/range {v31 .. v31}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@110
    move-result-object v31

    #@111
    const/16 v32, 0x1

    #@113
    move/from16 v0, v32

    #@115
    new-array v0, v0, [Ljava/lang/String;

    #@117
    move-object/from16 v32, v0

    #@119
    const/16 v33, 0x0

    #@11b
    aput-object v24, v32, v33

    #@11d
    invoke-virtual/range {v31 .. v32}, Landroid/content/pm/PackageManager;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    #@120
    move-result-object v25

    #@121
    .line 1673
    .local v25, pkgs:[Ljava/lang/String;
    const/16 v31, 0x0

    #@123
    aget-object v24, v25, v31

    #@125
    goto/16 :goto_70

    #@127
    .line 1690
    .end local v6           #cl:Ljava/lang/String;
    .end local v7           #e:Landroid/os/RemoteException;
    .end local v21           #packageManager:Landroid/content/pm/IPackageManager;
    .end local v24           #pkg:Ljava/lang/String;
    .end local v25           #pkgs:[Ljava/lang/String;
    :cond_127
    const-string v31, "h"

    #@129
    move-object/from16 v0, v31

    #@12b
    move-object/from16 v1, v29

    #@12d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@130
    move-result v31

    #@131
    if-eqz v31, :cond_1cf

    #@133
    .line 1691
    new-instance v10, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@135
    invoke-direct {v10}, Lcom/android/server/AppWidgetServiceImpl$Host;-><init>()V

    #@138
    .line 1695
    .local v10, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    const/16 v31, 0x0

    #@13a
    const-string v32, "pkg"

    #@13c
    move-object/from16 v0, v23

    #@13e
    move-object/from16 v1, v31

    #@140
    move-object/from16 v2, v32

    #@142
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@145
    move-result-object v31

    #@146
    move-object/from16 v0, v31

    #@148
    iput-object v0, v10, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;
    :try_end_14a
    .catch Ljava/lang/NullPointerException; {:try_start_107 .. :try_end_14a} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_107 .. :try_end_14a} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_107 .. :try_end_14a} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_107 .. :try_end_14a} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_107 .. :try_end_14a} :catch_3c2

    #@14a
    .line 1697
    :try_start_14a
    iget-object v0, v10, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;

    #@14c
    move-object/from16 v31, v0

    #@14e
    move-object/from16 v0, p0

    #@150
    move-object/from16 v1, v31

    #@152
    invoke-virtual {v0, v1}, Lcom/android/server/AppWidgetServiceImpl;->getUidForPackage(Ljava/lang/String;)I

    #@155
    move-result v31

    #@156
    move/from16 v0, v31

    #@158
    iput v0, v10, Lcom/android/server/AppWidgetServiceImpl$Host;->uid:I
    :try_end_15a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14a .. :try_end_15a} :catch_1aa
    .catch Ljava/lang/NullPointerException; {:try_start_14a .. :try_end_15a} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_14a .. :try_end_15a} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_14a .. :try_end_15a} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_14a .. :try_end_15a} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_14a .. :try_end_15a} :catch_3c2

    #@15a
    .line 1701
    :goto_15a
    :try_start_15a
    iget-boolean v0, v10, Lcom/android/server/AppWidgetServiceImpl$Host;->zombie:Z

    #@15c
    move/from16 v31, v0

    #@15e
    if-eqz v31, :cond_168

    #@160
    move-object/from16 v0, p0

    #@162
    iget-boolean v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mSafeMode:Z

    #@164
    move/from16 v31, v0

    #@166
    if-eqz v31, :cond_d4

    #@168
    .line 1704
    :cond_168
    const/16 v31, 0x0

    #@16a
    const-string v32, "id"

    #@16c
    move-object/from16 v0, v23

    #@16e
    move-object/from16 v1, v31

    #@170
    move-object/from16 v2, v32

    #@172
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@175
    move-result-object v31

    #@176
    const/16 v32, 0x10

    #@178
    invoke-static/range {v31 .. v32}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@17b
    move-result v31

    #@17c
    move/from16 v0, v31

    #@17e
    iput v0, v10, Lcom/android/server/AppWidgetServiceImpl$Host;->hostId:I

    #@180
    .line 1706
    move-object/from16 v0, p0

    #@182
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@184
    move-object/from16 v31, v0

    #@186
    move-object/from16 v0, v31

    #@188
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_18b
    .catch Ljava/lang/NullPointerException; {:try_start_15a .. :try_end_18b} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_15a .. :try_end_18b} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_15a .. :try_end_18b} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_15a .. :try_end_18b} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_15a .. :try_end_18b} :catch_3c2

    #@18b
    goto/16 :goto_d4

    #@18d
    .line 1782
    .end local v10           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    .end local v13           #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .end local v23           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v26           #providerIndex:I
    .end local v29           #tag:Ljava/lang/String;
    .end local v30           #type:I
    :catch_18d
    move-exception v7

    #@18e
    .line 1783
    .local v7, e:Ljava/lang/NullPointerException;
    const-string v31, "AppWidgetServiceImpl"

    #@190
    new-instance v32, Ljava/lang/StringBuilder;

    #@192
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v33, "failed parsing "

    #@197
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v32

    #@19b
    move-object/from16 v0, v32

    #@19d
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v32

    #@1a1
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a4
    move-result-object v32

    #@1a5
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a8
    goto/16 :goto_de

    #@1aa
    .line 1698
    .end local v7           #e:Ljava/lang/NullPointerException;
    .restart local v10       #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    .restart local v13       #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .restart local v23       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v26       #providerIndex:I
    .restart local v29       #tag:Ljava/lang/String;
    .restart local v30       #type:I
    :catch_1aa
    move-exception v8

    #@1ab
    .line 1699
    .local v8, ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v31, 0x1

    #@1ad
    :try_start_1ad
    move/from16 v0, v31

    #@1af
    iput-boolean v0, v10, Lcom/android/server/AppWidgetServiceImpl$Host;->zombie:Z
    :try_end_1b1
    .catch Ljava/lang/NullPointerException; {:try_start_1ad .. :try_end_1b1} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_1ad .. :try_end_1b1} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1ad .. :try_end_1b1} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_1ad .. :try_end_1b1} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1ad .. :try_end_1b1} :catch_3c2

    #@1b1
    goto :goto_15a

    #@1b2
    .line 1784
    .end local v8           #ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v10           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    .end local v13           #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .end local v23           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v26           #providerIndex:I
    .end local v29           #tag:Ljava/lang/String;
    .end local v30           #type:I
    :catch_1b2
    move-exception v7

    #@1b3
    .line 1785
    .local v7, e:Ljava/lang/NumberFormatException;
    const-string v31, "AppWidgetServiceImpl"

    #@1b5
    new-instance v32, Ljava/lang/StringBuilder;

    #@1b7
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@1ba
    const-string v33, "failed parsing "

    #@1bc
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v32

    #@1c0
    move-object/from16 v0, v32

    #@1c2
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v32

    #@1c6
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c9
    move-result-object v32

    #@1ca
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1cd
    goto/16 :goto_de

    #@1cf
    .line 1708
    .end local v7           #e:Ljava/lang/NumberFormatException;
    .restart local v13       #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .restart local v23       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v26       #providerIndex:I
    .restart local v29       #tag:Ljava/lang/String;
    .restart local v30       #type:I
    :cond_1cf
    :try_start_1cf
    const-string v31, "b"

    #@1d1
    move-object/from16 v0, v31

    #@1d3
    move-object/from16 v1, v29

    #@1d5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d8
    move-result v31

    #@1d9
    if-eqz v31, :cond_217

    #@1db
    .line 1709
    const/16 v31, 0x0

    #@1dd
    const-string v32, "packageName"

    #@1df
    move-object/from16 v0, v23

    #@1e1
    move-object/from16 v1, v31

    #@1e3
    move-object/from16 v2, v32

    #@1e5
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e8
    move-result-object v22

    #@1e9
    .line 1710
    .local v22, packageName:Ljava/lang/String;
    if-eqz v22, :cond_d4

    #@1eb
    .line 1711
    move-object/from16 v0, p0

    #@1ed
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mPackagesWithBindWidgetPermission:Ljava/util/HashSet;

    #@1ef
    move-object/from16 v31, v0

    #@1f1
    move-object/from16 v0, v31

    #@1f3
    move-object/from16 v1, v22

    #@1f5
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1f8
    .catch Ljava/lang/NullPointerException; {:try_start_1cf .. :try_end_1f8} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_1cf .. :try_end_1f8} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1cf .. :try_end_1f8} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_1cf .. :try_end_1f8} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1cf .. :try_end_1f8} :catch_3c2

    #@1f8
    goto/16 :goto_d4

    #@1fa
    .line 1786
    .end local v13           #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .end local v22           #packageName:Ljava/lang/String;
    .end local v23           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v26           #providerIndex:I
    .end local v29           #tag:Ljava/lang/String;
    .end local v30           #type:I
    :catch_1fa
    move-exception v7

    #@1fb
    .line 1787
    .local v7, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v31, "AppWidgetServiceImpl"

    #@1fd
    new-instance v32, Ljava/lang/StringBuilder;

    #@1ff
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@202
    const-string v33, "failed parsing "

    #@204
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@207
    move-result-object v32

    #@208
    move-object/from16 v0, v32

    #@20a
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v32

    #@20e
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@211
    move-result-object v32

    #@212
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@215
    goto/16 :goto_de

    #@217
    .line 1713
    .end local v7           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v13       #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .restart local v23       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v26       #providerIndex:I
    .restart local v29       #tag:Ljava/lang/String;
    .restart local v30       #type:I
    :cond_217
    :try_start_217
    const-string v31, "g"

    #@219
    move-object/from16 v0, v31

    #@21b
    move-object/from16 v1, v29

    #@21d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@220
    move-result v31

    #@221
    if-eqz v31, :cond_d4

    #@223
    .line 1714
    new-instance v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@225
    invoke-direct {v12}, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;-><init>()V

    #@228
    .line 1715
    .local v12, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    const/16 v31, 0x0

    #@22a
    const-string v32, "id"

    #@22c
    move-object/from16 v0, v23

    #@22e
    move-object/from16 v1, v31

    #@230
    move-object/from16 v2, v32

    #@232
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@235
    move-result-object v31

    #@236
    const/16 v32, 0x10

    #@238
    invoke-static/range {v31 .. v32}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@23b
    move-result v31

    #@23c
    move/from16 v0, v31

    #@23e
    iput v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@240
    .line 1716
    iget v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@242
    move/from16 v31, v0

    #@244
    move-object/from16 v0, p0

    #@246
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mNextAppWidgetId:I

    #@248
    move/from16 v32, v0

    #@24a
    move/from16 v0, v31

    #@24c
    move/from16 v1, v32

    #@24e
    if-lt v0, v1, :cond_25c

    #@250
    .line 1717
    iget v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@252
    move/from16 v31, v0

    #@254
    add-int/lit8 v31, v31, 0x1

    #@256
    move/from16 v0, v31

    #@258
    move-object/from16 v1, p0

    #@25a
    iput v0, v1, Lcom/android/server/AppWidgetServiceImpl;->mNextAppWidgetId:I

    #@25c
    .line 1720
    :cond_25c
    new-instance v18, Landroid/os/Bundle;

    #@25e
    invoke-direct/range {v18 .. v18}, Landroid/os/Bundle;-><init>()V

    #@261
    .line 1721
    .local v18, options:Landroid/os/Bundle;
    const/16 v31, 0x0

    #@263
    const-string v32, "min_width"

    #@265
    move-object/from16 v0, v23

    #@267
    move-object/from16 v1, v31

    #@269
    move-object/from16 v2, v32

    #@26b
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26e
    move-result-object v17

    #@26f
    .line 1722
    .local v17, minWidthString:Ljava/lang/String;
    if-eqz v17, :cond_286

    #@271
    .line 1723
    const-string v31, "appWidgetMinWidth"

    #@273
    const/16 v32, 0x10

    #@275
    move-object/from16 v0, v17

    #@277
    move/from16 v1, v32

    #@279
    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@27c
    move-result v32

    #@27d
    move-object/from16 v0, v18

    #@27f
    move-object/from16 v1, v31

    #@281
    move/from16 v2, v32

    #@283
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@286
    .line 1726
    :cond_286
    const/16 v31, 0x0

    #@288
    const-string v32, "min_height"

    #@28a
    move-object/from16 v0, v23

    #@28c
    move-object/from16 v1, v31

    #@28e
    move-object/from16 v2, v32

    #@290
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@293
    move-result-object v16

    #@294
    .line 1727
    .local v16, minHeightString:Ljava/lang/String;
    if-eqz v16, :cond_2ab

    #@296
    .line 1728
    const-string v31, "appWidgetMinHeight"

    #@298
    const/16 v32, 0x10

    #@29a
    move-object/from16 v0, v16

    #@29c
    move/from16 v1, v32

    #@29e
    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@2a1
    move-result v32

    #@2a2
    move-object/from16 v0, v18

    #@2a4
    move-object/from16 v1, v31

    #@2a6
    move/from16 v2, v32

    #@2a8
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2ab
    .line 1731
    :cond_2ab
    const/16 v31, 0x0

    #@2ad
    const-string v32, "max_width"

    #@2af
    move-object/from16 v0, v23

    #@2b1
    move-object/from16 v1, v31

    #@2b3
    move-object/from16 v2, v32

    #@2b5
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b8
    move-result-object v15

    #@2b9
    .line 1732
    .local v15, maxWidthString:Ljava/lang/String;
    if-eqz v15, :cond_2ce

    #@2bb
    .line 1733
    const-string v31, "appWidgetMaxWidth"

    #@2bd
    const/16 v32, 0x10

    #@2bf
    move/from16 v0, v32

    #@2c1
    invoke-static {v15, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@2c4
    move-result v32

    #@2c5
    move-object/from16 v0, v18

    #@2c7
    move-object/from16 v1, v31

    #@2c9
    move/from16 v2, v32

    #@2cb
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2ce
    .line 1736
    :cond_2ce
    const/16 v31, 0x0

    #@2d0
    const-string v32, "max_height"

    #@2d2
    move-object/from16 v0, v23

    #@2d4
    move-object/from16 v1, v31

    #@2d6
    move-object/from16 v2, v32

    #@2d8
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2db
    move-result-object v14

    #@2dc
    .line 1737
    .local v14, maxHeightString:Ljava/lang/String;
    if-eqz v14, :cond_2f1

    #@2de
    .line 1738
    const-string v31, "appWidgetMaxHeight"

    #@2e0
    const/16 v32, 0x10

    #@2e2
    move/from16 v0, v32

    #@2e4
    invoke-static {v14, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@2e7
    move-result v32

    #@2e8
    move-object/from16 v0, v18

    #@2ea
    move-object/from16 v1, v31

    #@2ec
    move/from16 v2, v32

    #@2ee
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2f1
    .line 1741
    :cond_2f1
    const/16 v31, 0x0

    #@2f3
    const-string v32, "host_category"

    #@2f5
    move-object/from16 v0, v23

    #@2f7
    move-object/from16 v1, v31

    #@2f9
    move-object/from16 v2, v32

    #@2fb
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2fe
    move-result-object v5

    #@2ff
    .line 1742
    .local v5, categoryString:Ljava/lang/String;
    if-eqz v5, :cond_314

    #@301
    .line 1743
    const-string v31, "appWidgetCategory"

    #@303
    const/16 v32, 0x10

    #@305
    move/from16 v0, v32

    #@307
    invoke-static {v5, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@30a
    move-result v32

    #@30b
    move-object/from16 v0, v18

    #@30d
    move-object/from16 v1, v31

    #@30f
    move/from16 v2, v32

    #@311
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@314
    .line 1746
    :cond_314
    move-object/from16 v0, v18

    #@316
    iput-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@318
    .line 1748
    const/16 v31, 0x0

    #@31a
    const-string v32, "p"

    #@31c
    move-object/from16 v0, v23

    #@31e
    move-object/from16 v1, v31

    #@320
    move-object/from16 v2, v32

    #@322
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@325
    move-result-object v27

    #@326
    .line 1749
    .local v27, providerString:Ljava/lang/String;
    if-eqz v27, :cond_348

    #@328
    .line 1753
    const/16 v31, 0x10

    #@32a
    move-object/from16 v0, v27

    #@32c
    move/from16 v1, v31

    #@32e
    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@331
    move-result v20

    #@332
    .line 1754
    .local v20, pIndex:I
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@335
    move-result-object v31

    #@336
    move-object/from16 v0, v31

    #@338
    invoke-virtual {v13, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@33b
    move-result-object v31

    #@33c
    check-cast v31, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@33e
    move-object/from16 v0, v31

    #@340
    iput-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@342
    .line 1759
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@344
    move-object/from16 v31, v0

    #@346
    if-eqz v31, :cond_d4

    #@348
    .line 1766
    .end local v20           #pIndex:I
    :cond_348
    const/16 v31, 0x0

    #@34a
    const-string v32, "h"

    #@34c
    move-object/from16 v0, v23

    #@34e
    move-object/from16 v1, v31

    #@350
    move-object/from16 v2, v32

    #@352
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@355
    move-result-object v31

    #@356
    const/16 v32, 0x10

    #@358
    invoke-static/range {v31 .. v32}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@35b
    move-result v9

    #@35c
    .line 1767
    .local v9, hIndex:I
    move-object/from16 v0, p0

    #@35e
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@360
    move-object/from16 v31, v0

    #@362
    move-object/from16 v0, v31

    #@364
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@367
    move-result-object v31

    #@368
    check-cast v31, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@36a
    move-object/from16 v0, v31

    #@36c
    iput-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@36e
    .line 1768
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@370
    move-object/from16 v31, v0

    #@372
    if-eqz v31, :cond_d4

    #@374
    .line 1773
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@376
    move-object/from16 v31, v0

    #@378
    if-eqz v31, :cond_389

    #@37a
    .line 1774
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@37c
    move-object/from16 v31, v0

    #@37e
    move-object/from16 v0, v31

    #@380
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@382
    move-object/from16 v31, v0

    #@384
    move-object/from16 v0, v31

    #@386
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@389
    .line 1776
    :cond_389
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@38b
    move-object/from16 v31, v0

    #@38d
    move-object/from16 v0, v31

    #@38f
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@391
    move-object/from16 v31, v0

    #@393
    move-object/from16 v0, v31

    #@395
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@398
    .line 1777
    move-object/from16 v0, p0

    #@39a
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@39c
    move-object/from16 v31, v0

    #@39e
    move-object/from16 v0, v31

    #@3a0
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3a3
    .catch Ljava/lang/NullPointerException; {:try_start_217 .. :try_end_3a3} :catch_18d
    .catch Ljava/lang/NumberFormatException; {:try_start_217 .. :try_end_3a3} :catch_1b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_217 .. :try_end_3a3} :catch_1fa
    .catch Ljava/io/IOException; {:try_start_217 .. :try_end_3a3} :catch_3a5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_217 .. :try_end_3a3} :catch_3c2

    #@3a3
    goto/16 :goto_d4

    #@3a5
    .line 1788
    .end local v5           #categoryString:Ljava/lang/String;
    .end local v9           #hIndex:I
    .end local v12           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    .end local v13           #loadedProviders:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/AppWidgetServiceImpl$Provider;>;"
    .end local v14           #maxHeightString:Ljava/lang/String;
    .end local v15           #maxWidthString:Ljava/lang/String;
    .end local v16           #minHeightString:Ljava/lang/String;
    .end local v17           #minWidthString:Ljava/lang/String;
    .end local v18           #options:Landroid/os/Bundle;
    .end local v23           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v26           #providerIndex:I
    .end local v27           #providerString:Ljava/lang/String;
    .end local v29           #tag:Ljava/lang/String;
    .end local v30           #type:I
    :catch_3a5
    move-exception v7

    #@3a6
    .line 1789
    .local v7, e:Ljava/io/IOException;
    const-string v31, "AppWidgetServiceImpl"

    #@3a8
    new-instance v32, Ljava/lang/StringBuilder;

    #@3aa
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@3ad
    const-string v33, "failed parsing "

    #@3af
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b2
    move-result-object v32

    #@3b3
    move-object/from16 v0, v32

    #@3b5
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b8
    move-result-object v32

    #@3b9
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3bc
    move-result-object v32

    #@3bd
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c0
    goto/16 :goto_de

    #@3c2
    .line 1790
    .end local v7           #e:Ljava/io/IOException;
    :catch_3c2
    move-exception v7

    #@3c3
    .line 1791
    .local v7, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v31, "AppWidgetServiceImpl"

    #@3c5
    new-instance v32, Ljava/lang/StringBuilder;

    #@3c7
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@3ca
    const-string v33, "failed parsing "

    #@3cc
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3cf
    move-result-object v32

    #@3d0
    move-object/from16 v0, v32

    #@3d2
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d5
    move-result-object v32

    #@3d6
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d9
    move-result-object v32

    #@3da
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3dd
    goto/16 :goto_de

    #@3df
    .line 1802
    .end local v7           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_3df
    const-string v31, "AppWidgetServiceImpl"

    #@3e1
    const-string v32, "Failed to read state, clearing widgets and hosts."

    #@3e3
    invoke-static/range {v31 .. v32}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e6
    .line 1804
    move-object/from16 v0, p0

    #@3e8
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@3ea
    move-object/from16 v31, v0

    #@3ec
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->clear()V

    #@3ef
    .line 1805
    move-object/from16 v0, p0

    #@3f1
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@3f3
    move-object/from16 v31, v0

    #@3f5
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->clear()V

    #@3f8
    .line 1806
    move-object/from16 v0, p0

    #@3fa
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@3fc
    move-object/from16 v31, v0

    #@3fe
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    #@401
    move-result v4

    #@402
    .line 1807
    .local v4, N:I
    const/4 v11, 0x0

    #@403
    .restart local v11       #i:I
    :goto_403
    if-ge v11, v4, :cond_41f

    #@405
    .line 1808
    move-object/from16 v0, p0

    #@407
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@409
    move-object/from16 v31, v0

    #@40b
    move-object/from16 v0, v31

    #@40d
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@410
    move-result-object v31

    #@411
    check-cast v31, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@413
    move-object/from16 v0, v31

    #@415
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@417
    move-object/from16 v31, v0

    #@419
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->clear()V

    #@41c
    .line 1807
    add-int/lit8 v11, v11, 0x1

    #@41e
    goto :goto_403

    #@41f
    .line 1811
    .end local v4           #N:I
    :cond_41f
    return-void
.end method

.method registerForBroadcastsLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V
    .registers 14
    .parameter "p"
    .parameter "appWidgetIds"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 1323
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@3
    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@5
    if-lez v0, :cond_55

    #@7
    .line 1327
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->broadcast:Landroid/app/PendingIntent;

    #@9
    if-eqz v0, :cond_56

    #@b
    .line 1328
    .local v7, alreadyRegistered:Z
    :goto_b
    new-instance v8, Landroid/content/Intent;

    #@d
    const-string v0, "android.appwidget.action.APPWIDGET_UPDATE"

    #@f
    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@12
    .line 1329
    .local v8, intent:Landroid/content/Intent;
    const-string v0, "appWidgetIds"

    #@14
    invoke-virtual {v8, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    #@17
    .line 1330
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@19
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@1b
    invoke-virtual {v8, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@1e
    .line 1331
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@21
    move-result-wide v9

    #@22
    .line 1333
    .local v9, token:J
    :try_start_22
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@24
    const/4 v1, 0x1

    #@25
    const/high16 v2, 0x800

    #@27
    new-instance v3, Landroid/os/UserHandle;

    #@29
    iget v6, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@2b
    invoke-direct {v3, v6}, Landroid/os/UserHandle;-><init>(I)V

    #@2e
    invoke-static {v0, v1, v8, v2, v3}, Landroid/app/PendingIntent;->getBroadcastAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->broadcast:Landroid/app/PendingIntent;
    :try_end_34
    .catchall {:try_start_22 .. :try_end_34} :catchall_58

    #@34
    .line 1336
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@37
    .line 1338
    if-nez v7, :cond_55

    #@39
    .line 1339
    iget-object v0, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@3b
    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@3d
    int-to-long v4, v0

    #@3e
    .line 1340
    .local v4, period:J
    const-wide/32 v0, 0x1b7740

    #@41
    cmp-long v0, v4, v0

    #@43
    if-gez v0, :cond_48

    #@45
    .line 1341
    const-wide/32 v4, 0x1b7740

    #@48
    .line 1343
    :cond_48
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mAlarmManager:Landroid/app/AlarmManager;

    #@4a
    const/4 v1, 0x2

    #@4b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4e
    move-result-wide v2

    #@4f
    add-long/2addr v2, v4

    #@50
    iget-object v6, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->broadcast:Landroid/app/PendingIntent;

    #@52
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    #@55
    .line 1348
    .end local v4           #period:J
    .end local v7           #alreadyRegistered:Z
    .end local v8           #intent:Landroid/content/Intent;
    .end local v9           #token:J
    :cond_55
    return-void

    #@56
    .line 1327
    :cond_56
    const/4 v7, 0x0

    #@57
    goto :goto_b

    #@58
    .line 1336
    .restart local v7       #alreadyRegistered:Z
    .restart local v8       #intent:Landroid/content/Intent;
    .restart local v9       #token:J
    :catchall_58
    move-exception v0

    #@59
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5c
    throw v0
.end method

.method removeProviderLocked(ILcom/android/server/AppWidgetServiceImpl$Provider;)V
    .registers 8
    .parameter "index"
    .parameter "p"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1286
    iget-object v3, p2, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 1287
    .local v0, N:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_30

    #@a
    .line 1288
    iget-object v3, p2, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@12
    .line 1290
    .local v2, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    invoke-virtual {p0, v2, v4}, Lcom/android/server/AppWidgetServiceImpl;->updateAppWidgetInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;Landroid/widget/RemoteViews;)V

    #@15
    .line 1292
    invoke-virtual {p0, p2}, Lcom/android/server/AppWidgetServiceImpl;->cancelBroadcasts(Lcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@18
    .line 1294
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@1a
    iget-object v3, v3, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1f
    .line 1295
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@24
    .line 1296
    iput-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@26
    .line 1297
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@28
    invoke-virtual {p0, v3}, Lcom/android/server/AppWidgetServiceImpl;->pruneHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V

    #@2b
    .line 1298
    iput-object v4, v2, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@2d
    .line 1287
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_8

    #@30
    .line 1300
    .end local v2           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_30
    iget-object v3, p2, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@35
    .line 1301
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@3a
    .line 1302
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mDeletedProviders:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3f
    .line 1304
    invoke-virtual {p0, p2}, Lcom/android/server/AppWidgetServiceImpl;->cancelBroadcasts(Lcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@42
    .line 1305
    return-void
.end method

.method removeProvidersForPackageLocked(Ljava/lang/String;)Z
    .registers 8
    .parameter "pkgName"

    #@0
    .prologue
    .line 1970
    const/4 v4, 0x0

    #@1
    .line 1971
    .local v4, providersRemoved:Z
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 1972
    .local v0, N:I
    add-int/lit8 v2, v0, -0x1

    #@9
    .local v2, i:I
    :goto_9
    if-ltz v2, :cond_28

    #@b
    .line 1973
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@13
    .line 1974
    .local v3, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    iget-object v5, v3, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@15
    iget-object v5, v5, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@17
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_25

    #@21
    .line 1975
    invoke-virtual {p0, v2, v3}, Lcom/android/server/AppWidgetServiceImpl;->removeProviderLocked(ILcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@24
    .line 1976
    const/4 v4, 0x1

    #@25
    .line 1972
    :cond_25
    add-int/lit8 v2, v2, -0x1

    #@27
    goto :goto_9

    #@28
    .line 1984
    .end local v3           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_28
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v0

    #@2e
    .line 1985
    add-int/lit8 v2, v0, -0x1

    #@30
    :goto_30
    if-ltz v2, :cond_48

    #@32
    .line 1986
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v1

    #@38
    check-cast v1, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@3a
    .line 1987
    .local v1, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    iget-object v5, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;

    #@3c
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v5

    #@40
    if-eqz v5, :cond_45

    #@42
    .line 1988
    invoke-virtual {p0, v1}, Lcom/android/server/AppWidgetServiceImpl;->deleteHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V

    #@45
    .line 1985
    :cond_45
    add-int/lit8 v2, v2, -0x1

    #@47
    goto :goto_30

    #@48
    .line 1992
    .end local v1           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_48
    return v4
.end method

.method saveStateLocked()V
    .registers 7

    #@0
    .prologue
    .line 1558
    invoke-virtual {p0}, Lcom/android/server/AppWidgetServiceImpl;->savedStateFile()Landroid/util/AtomicFile;

    #@3
    move-result-object v1

    #@4
    .line 1561
    .local v1, file:Landroid/util/AtomicFile;
    :try_start_4
    invoke-virtual {v1}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@7
    move-result-object v2

    #@8
    .line 1562
    .local v2, stream:Ljava/io/FileOutputStream;
    invoke-virtual {p0, v2}, Lcom/android/server/AppWidgetServiceImpl;->writeStateToFileLocked(Ljava/io/FileOutputStream;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_12

    #@e
    .line 1563
    invoke-virtual {v1, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    #@11
    .line 1571
    .end local v2           #stream:Ljava/io/FileOutputStream;
    :goto_11
    return-void

    #@12
    .line 1565
    .restart local v2       #stream:Ljava/io/FileOutputStream;
    :cond_12
    invoke-virtual {v1, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@15
    .line 1566
    const-string v3, "AppWidgetServiceImpl"

    #@17
    const-string v4, "Failed to save state, restoring backup."

    #@19
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_1c} :catch_1d

    #@1c
    goto :goto_11

    #@1d
    .line 1568
    .end local v2           #stream:Ljava/io/FileOutputStream;
    :catch_1d
    move-exception v0

    #@1e
    .line 1569
    .local v0, e:Ljava/io/IOException;
    const-string v3, "AppWidgetServiceImpl"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "Failed open state file for write: "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_11
.end method

.method savedStateFile()Landroid/util/AtomicFile;
    .registers 5

    #@0
    .prologue
    .line 1818
    iget v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@2
    invoke-static {v3}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    .line 1819
    .local v0, dir:Ljava/io/File;
    iget v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@8
    invoke-static {v3}, Lcom/android/server/AppWidgetServiceImpl;->getSettingsFile(I)Ljava/io/File;

    #@b
    move-result-object v2

    #@c
    .line 1820
    .local v2, settingsFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_29

    #@12
    iget v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@14
    if-nez v3, :cond_29

    #@16
    .line 1821
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@19
    move-result v3

    #@1a
    if-nez v3, :cond_1f

    #@1c
    .line 1822
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@1f
    .line 1825
    :cond_1f
    new-instance v1, Ljava/io/File;

    #@21
    const-string v3, "/data/system/appwidgets.xml"

    #@23
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@26
    .line 1828
    .local v1, oldFile:Ljava/io/File;
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@29
    .line 1830
    .end local v1           #oldFile:Ljava/io/File;
    :cond_29
    new-instance v3, Landroid/util/AtomicFile;

    #@2b
    invoke-direct {v3, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@2e
    return-object v3
.end method

.method sendEnableIntentLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;)V
    .registers 6
    .parameter "p"

    #@0
    .prologue
    .line 1308
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.appwidget.action.APPWIDGET_ENABLED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1309
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@9
    iget-object v1, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@e
    .line 1310
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@10
    new-instance v2, Landroid/os/UserHandle;

    #@12
    iget v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@14
    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    #@17
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1a
    .line 1311
    return-void
.end method

.method sendInitialBroadcasts()V
    .registers 7

    #@0
    .prologue
    .line 1523
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v5

    #@3
    .line 1524
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 1525
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v0

    #@c
    .line 1526
    .local v0, N:I
    const/4 v2, 0x0

    #@d
    .local v2, i:I
    :goto_d
    if-ge v2, v0, :cond_2f

    #@f
    .line 1527
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@17
    .line 1528
    .local v3, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    iget-object v4, v3, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v4

    #@1d
    if-lez v4, :cond_2c

    #@1f
    .line 1529
    invoke-virtual {p0, v3}, Lcom/android/server/AppWidgetServiceImpl;->sendEnableIntentLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@22
    .line 1530
    invoke-static {v3}, Lcom/android/server/AppWidgetServiceImpl;->getAppWidgetIds(Lcom/android/server/AppWidgetServiceImpl$Provider;)[I

    #@25
    move-result-object v1

    #@26
    .line 1531
    .local v1, appWidgetIds:[I
    invoke-virtual {p0, v3, v1}, Lcom/android/server/AppWidgetServiceImpl;->sendUpdateIntentLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V

    #@29
    .line 1532
    invoke-virtual {p0, v3, v1}, Lcom/android/server/AppWidgetServiceImpl;->registerForBroadcastsLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V

    #@2c
    .line 1526
    .end local v1           #appWidgetIds:[I
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_d

    #@2f
    .line 1535
    .end local v3           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_2f
    monitor-exit v5

    #@30
    .line 1536
    return-void

    #@31
    .line 1535
    .end local v0           #N:I
    .end local v2           #i:I
    :catchall_31
    move-exception v4

    #@32
    monitor-exit v5
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_31

    #@33
    throw v4
.end method

.method sendUpdateIntentLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V
    .registers 7
    .parameter "p"
    .parameter "appWidgetIds"

    #@0
    .prologue
    .line 1314
    if-eqz p2, :cond_24

    #@2
    array-length v1, p2

    #@3
    if-lez v1, :cond_24

    #@5
    .line 1315
    new-instance v0, Landroid/content/Intent;

    #@7
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    #@9
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    .line 1316
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "appWidgetIds"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    #@11
    .line 1317
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@13
    iget-object v1, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@15
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@18
    .line 1318
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@1a
    new-instance v2, Landroid/os/UserHandle;

    #@1c
    iget v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@1e
    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    #@21
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@24
    .line 1320
    .end local v0           #intent:Landroid/content/Intent;
    :cond_24
    return-void
.end method

.method public setBindAppWidgetPermission(Ljava/lang/String;Z)V
    .registers 7
    .parameter "packageName"
    .parameter "permission"

    #@0
    .prologue
    .line 663
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MODIFY_APPWIDGET_BIND_PERMISSIONS"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "setBindAppWidgetPermission packageName="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 667
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@1c
    monitor-enter v1

    #@1d
    .line 668
    :try_start_1d
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@20
    .line 669
    if-eqz p2, :cond_2c

    #@22
    .line 670
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mPackagesWithBindWidgetPermission:Ljava/util/HashSet;

    #@24
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@27
    .line 674
    :goto_27
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@2a
    .line 675
    monitor-exit v1

    #@2b
    .line 676
    return-void

    #@2c
    .line 672
    :cond_2c
    iget-object v0, p0, Lcom/android/server/AppWidgetServiceImpl;->mPackagesWithBindWidgetPermission:Ljava/util/HashSet;

    #@2e
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@31
    goto :goto_27

    #@32
    .line 675
    :catchall_32
    move-exception v0

    #@33
    monitor-exit v1
    :try_end_34
    .catchall {:try_start_1d .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method

.method public startListening(Lcom/android/internal/appwidget/IAppWidgetHost;Ljava/lang/String;ILjava/util/List;)[I
    .registers 14
    .parameter "callbacks"
    .parameter "packageName"
    .parameter "hostId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/appwidget/IAppWidgetHost;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/widget/RemoteViews;",
            ">;)[I"
        }
    .end annotation

    #@0
    .prologue
    .line 1147
    .local p4, updatedViews:Ljava/util/List;,"Ljava/util/List<Landroid/widget/RemoteViews;>;"
    invoke-virtual {p0, p2}, Lcom/android/server/AppWidgetServiceImpl;->enforceCallingUid(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    .line 1148
    .local v1, callingUid:I
    iget-object v8, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@6
    monitor-enter v8

    #@7
    .line 1149
    :try_start_7
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@a
    .line 1150
    invoke-virtual {p0, v1, p2, p3}, Lcom/android/server/AppWidgetServiceImpl;->lookupOrAddHostLocked(ILjava/lang/String;I)Lcom/android/server/AppWidgetServiceImpl$Host;

    #@d
    move-result-object v2

    #@e
    .line 1151
    .local v2, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    iput-object p1, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@10
    .line 1153
    invoke-interface {p4}, Ljava/util/List;->clear()V

    #@13
    .line 1155
    iget-object v5, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@15
    .line 1156
    .local v5, instances:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v0

    #@19
    .line 1157
    .local v0, N:I
    new-array v6, v0, [I

    #@1b
    .line 1158
    .local v6, updatedIds:[I
    const/4 v3, 0x0

    #@1c
    .local v3, i:I
    :goto_1c
    if-ge v3, v0, :cond_34

    #@1e
    .line 1159
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v4

    #@22
    check-cast v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@24
    .line 1160
    .local v4, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    iget v7, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@26
    aput v7, v6, v3

    #@28
    .line 1161
    iget-object v7, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@2a
    invoke-direct {p0, v7}, Lcom/android/server/AppWidgetServiceImpl;->cloneIfLocalBinder(Landroid/widget/RemoteViews;)Landroid/widget/RemoteViews;

    #@2d
    move-result-object v7

    #@2e
    invoke-interface {p4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@31
    .line 1158
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_1c

    #@34
    .line 1163
    .end local v4           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_34
    monitor-exit v8

    #@35
    return-object v6

    #@36
    .line 1164
    .end local v0           #N:I
    .end local v2           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    .end local v3           #i:I
    .end local v5           #instances:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;>;"
    .end local v6           #updatedIds:[I
    :catchall_36
    move-exception v7

    #@37
    monitor-exit v8
    :try_end_38
    .catchall {:try_start_7 .. :try_end_38} :catchall_36

    #@38
    throw v7
.end method

.method public stopListening(I)V
    .registers 5
    .parameter "hostId"

    #@0
    .prologue
    .line 1168
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 1169
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 1170
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@9
    move-result v1

    #@a
    invoke-virtual {p0, v1, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupHostLocked(II)Lcom/android/server/AppWidgetServiceImpl$Host;

    #@d
    move-result-object v0

    #@e
    .line 1171
    .local v0, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    if-eqz v0, :cond_16

    #@10
    .line 1172
    const/4 v1, 0x0

    #@11
    iput-object v1, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@13
    .line 1173
    invoke-virtual {p0, v0}, Lcom/android/server/AppWidgetServiceImpl;->pruneHostLocked(Lcom/android/server/AppWidgetServiceImpl$Host;)V

    #@16
    .line 1175
    :cond_16
    monitor-exit v2

    #@17
    .line 1176
    return-void

    #@18
    .line 1175
    .end local v0           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public systemReady(Z)V
    .registers 4
    .parameter "safeMode"

    #@0
    .prologue
    .line 221
    iput-boolean p1, p0, Lcom/android/server/AppWidgetServiceImpl;->mSafeMode:Z

    #@2
    .line 223
    iget-object v1, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@4
    monitor-enter v1

    #@5
    .line 224
    :try_start_5
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@8
    .line 225
    monitor-exit v1

    #@9
    .line 226
    return-void

    #@a
    .line 225
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unbindRemoteViewsService(ILandroid/content/Intent;)V
    .registers 9
    .parameter "appWidgetId"
    .parameter "intent"

    #@0
    .prologue
    .line 736
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 737
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 740
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    new-instance v5, Landroid/content/Intent$FilterComparison;

    #@c
    invoke-direct {v5, p2}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@f
    invoke-static {v3, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@12
    move-result-object v2

    #@13
    .line 742
    .local v2, key:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Landroid/content/Intent$FilterComparison;>;"
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@15
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_41

    #@1b
    .line 746
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@1e
    move-result-object v1

    #@1f
    .line 747
    .local v1, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-nez v1, :cond_2c

    #@21
    .line 748
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@23
    const-string v5, "bad appWidgetId"

    #@25
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v3

    #@29
    .line 757
    .end local v1           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    .end local v2           #key:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Landroid/content/Intent$FilterComparison;>;"
    :catchall_29
    move-exception v3

    #@2a
    monitor-exit v4
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_29

    #@2b
    throw v3

    #@2c
    .line 751
    .restart local v1       #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    .restart local v2       #key:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Landroid/content/Intent$FilterComparison;>;"
    :cond_2c
    :try_start_2c
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@2e
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v0

    #@32
    check-cast v0, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;

    #@34
    .line 753
    .local v0, conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    invoke-virtual {v0}, Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;->disconnect()V

    #@37
    .line 754
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@39
    invoke-virtual {v3, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@3c
    .line 755
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mBoundRemoteViewsServices:Ljava/util/HashMap;

    #@3e
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    .line 757
    .end local v0           #conn:Lcom/android/server/AppWidgetServiceImpl$ServiceConnectionProxy;
    .end local v1           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_2c .. :try_end_42} :catchall_29

    #@42
    .line 758
    return-void
.end method

.method public updateAppWidgetIds([ILandroid/widget/RemoteViews;)V
    .registers 10
    .parameter "appWidgetIds"
    .parameter "views"

    #@0
    .prologue
    .line 885
    if-nez p1, :cond_3

    #@2
    .line 912
    :cond_2
    :goto_2
    return-void

    #@3
    .line 888
    :cond_3
    sget-boolean v4, Lcom/android/server/AppWidgetServiceImpl;->DBG:Z

    #@5
    if-eqz v4, :cond_1d

    #@7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v5, "updateAppWidgetIds views: "

    #@e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-direct {p0, v4}, Lcom/android/server/AppWidgetServiceImpl;->log(Ljava/lang/String;)V

    #@1d
    .line 889
    :cond_1d
    const/4 v1, 0x0

    #@1e
    .line 890
    .local v1, bitmapMemoryUsage:I
    if-eqz p2, :cond_24

    #@20
    .line 891
    invoke-virtual {p2}, Landroid/widget/RemoteViews;->estimateMemoryUsage()I

    #@23
    move-result v1

    #@24
    .line 893
    :cond_24
    iget v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mMaxWidgetBitmapMemory:I

    #@26
    if-le v1, v4, :cond_59

    #@28
    .line 894
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@2a
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v6, "RemoteViews for widget update exceeds maximum bitmap memory usage (used: "

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    const-string v6, ", max: "

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    iget v6, p0, Lcom/android/server/AppWidgetServiceImpl;->mMaxWidgetBitmapMemory:I

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    const-string v6, ") The total memory cannot exceed that required to"

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    const-string v6, " fill the device\'s screen once."

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@58
    throw v4

    #@59
    .line 900
    :cond_59
    array-length v4, p1

    #@5a
    if-eqz v4, :cond_2

    #@5c
    .line 903
    array-length v0, p1

    #@5d
    .line 905
    .local v0, N:I
    iget-object v5, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@5f
    monitor-enter v5

    #@60
    .line 906
    :try_start_60
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@63
    .line 907
    const/4 v2, 0x0

    #@64
    .local v2, i:I
    :goto_64
    if-ge v2, v0, :cond_72

    #@66
    .line 908
    aget v4, p1, v2

    #@68
    invoke-virtual {p0, v4}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@6b
    move-result-object v3

    #@6c
    .line 909
    .local v3, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    invoke-virtual {p0, v3, p2}, Lcom/android/server/AppWidgetServiceImpl;->updateAppWidgetInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;Landroid/widget/RemoteViews;)V

    #@6f
    .line 907
    add-int/lit8 v2, v2, 0x1

    #@71
    goto :goto_64

    #@72
    .line 911
    .end local v3           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_72
    monitor-exit v5

    #@73
    goto :goto_2

    #@74
    .end local v2           #i:I
    :catchall_74
    move-exception v4

    #@75
    monitor-exit v5
    :try_end_76
    .catchall {:try_start_60 .. :try_end_76} :catchall_74

    #@76
    throw v4
.end method

.method updateAppWidgetInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;Landroid/widget/RemoteViews;)V
    .registers 4
    .parameter "id"
    .parameter "views"

    #@0
    .prologue
    .line 1026
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/AppWidgetServiceImpl;->updateAppWidgetInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;Landroid/widget/RemoteViews;Z)V

    #@4
    .line 1027
    return-void
.end method

.method updateAppWidgetInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;Landroid/widget/RemoteViews;Z)V
    .registers 7
    .parameter "id"
    .parameter "views"
    .parameter "isPartialUpdate"

    #@0
    .prologue
    .line 1033
    if-eqz p1, :cond_25

    #@2
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@4
    if-eqz v1, :cond_25

    #@6
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@8
    iget-boolean v1, v1, Lcom/android/server/AppWidgetServiceImpl$Provider;->zombie:Z

    #@a
    if-nez v1, :cond_25

    #@c
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@e
    iget-boolean v1, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->zombie:Z

    #@10
    if-nez v1, :cond_25

    #@12
    .line 1035
    if-nez p3, :cond_26

    #@14
    .line 1037
    iput-object p2, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@16
    .line 1044
    :goto_16
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@18
    iget-object v1, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@1a
    if-eqz v1, :cond_25

    #@1c
    .line 1047
    :try_start_1c
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@1e
    iget-object v1, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@20
    iget v2, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@22
    invoke-interface {v1, v2, p2}, Lcom/android/internal/appwidget/IAppWidgetHost;->updateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_25} :catch_2c

    #@25
    .line 1055
    :cond_25
    :goto_25
    return-void

    #@26
    .line 1040
    :cond_26
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@28
    invoke-virtual {v1, p2}, Landroid/widget/RemoteViews;->mergeRemoteViews(Landroid/widget/RemoteViews;)V

    #@2b
    goto :goto_16

    #@2c
    .line 1048
    :catch_2c
    move-exception v0

    #@2d
    .line 1051
    .local v0, e:Landroid/os/RemoteException;
    iget-object v1, p1, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@2f
    const/4 v2, 0x0

    #@30
    iput-object v2, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@32
    goto :goto_25
.end method

.method public updateAppWidgetOptions(ILandroid/os/Bundle;)V
    .registers 10
    .parameter "appWidgetId"
    .parameter "options"

    #@0
    .prologue
    .line 929
    iget-object v4, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 930
    :try_start_3
    invoke-direct {p0, p2}, Lcom/android/server/AppWidgetServiceImpl;->cloneIfLocalBinder(Landroid/os/Bundle;)Landroid/os/Bundle;

    #@6
    move-result-object p2

    #@7
    .line 931
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@a
    .line 932
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupAppWidgetIdLocked(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@d
    move-result-object v0

    #@e
    .line 934
    .local v0, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    if-nez v0, :cond_12

    #@10
    .line 935
    monitor-exit v4

    #@11
    .line 950
    :goto_11
    return-void

    #@12
    .line 938
    :cond_12
    iget-object v2, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@14
    .line 940
    .local v2, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    iget-object v3, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@16
    invoke-virtual {v3, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    #@19
    .line 943
    new-instance v1, Landroid/content/Intent;

    #@1b
    const-string v3, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    #@1d
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@20
    .line 944
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, v2, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@22
    iget-object v3, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@24
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@27
    .line 945
    const-string v3, "appWidgetId"

    #@29
    iget v5, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@2b
    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2e
    .line 946
    const-string v3, "appWidgetOptions"

    #@30
    iget-object v5, v0, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@32
    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@35
    .line 947
    iget-object v3, p0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@37
    new-instance v5, Landroid/os/UserHandle;

    #@39
    iget v6, p0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@3b
    invoke-direct {v5, v6}, Landroid/os/UserHandle;-><init>(I)V

    #@3e
    invoke-virtual {v3, v1, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@41
    .line 948
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->saveStateAsync()V

    #@44
    .line 949
    monitor-exit v4

    #@45
    goto :goto_11

    #@46
    .end local v0           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :catchall_46
    move-exception v3

    #@47
    monitor-exit v4
    :try_end_48
    .catchall {:try_start_3 .. :try_end_48} :catchall_46

    #@48
    throw v3
.end method

.method public updateAppWidgetProvider(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V
    .registers 13
    .parameter "provider"
    .parameter "views"

    #@0
    .prologue
    .line 1006
    iget-object v7, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@2
    monitor-enter v7

    #@3
    .line 1007
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/AppWidgetServiceImpl;->ensureStateLoadedLocked()V

    #@6
    .line 1008
    invoke-virtual {p0, p1}, Lcom/android/server/AppWidgetServiceImpl;->lookupProviderLocked(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@9
    move-result-object v5

    #@a
    .line 1009
    .local v5, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-nez v5, :cond_26

    #@c
    .line 1010
    const-string v6, "AppWidgetServiceImpl"

    #@e
    new-instance v8, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v9, "updateAppWidgetProvider: provider doesn\'t exist: "

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v8

    #@21
    invoke-static {v6, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1011
    monitor-exit v7

    #@25
    .line 1023
    :goto_25
    return-void

    #@26
    .line 1013
    :cond_26
    iget-object v4, v5, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@28
    .line 1014
    .local v4, instances:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2b
    move-result v1

    #@2c
    .line 1015
    .local v1, callingUid:I
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@2f
    move-result v0

    #@30
    .line 1016
    .local v0, N:I
    const/4 v2, 0x0

    #@31
    .local v2, i:I
    :goto_31
    if-ge v2, v0, :cond_45

    #@33
    .line 1017
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@39
    .line 1018
    .local v3, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    invoke-virtual {p0, v3, v1}, Lcom/android/server/AppWidgetServiceImpl;->canAccessAppWidgetId(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;I)Z

    #@3c
    move-result v6

    #@3d
    if-eqz v6, :cond_42

    #@3f
    .line 1019
    invoke-virtual {p0, v3, p2}, Lcom/android/server/AppWidgetServiceImpl;->updateAppWidgetInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;Landroid/widget/RemoteViews;)V

    #@42
    .line 1016
    :cond_42
    add-int/lit8 v2, v2, 0x1

    #@44
    goto :goto_31

    #@45
    .line 1022
    .end local v3           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_45
    monitor-exit v7

    #@46
    goto :goto_25

    #@47
    .end local v0           #N:I
    .end local v1           #callingUid:I
    .end local v2           #i:I
    .end local v4           #instances:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;>;"
    .end local v5           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :catchall_47
    move-exception v6

    #@48
    monitor-exit v7
    :try_end_49
    .catchall {:try_start_3 .. :try_end_49} :catchall_47

    #@49
    throw v6
.end method

.method updateProvidersForPackageLocked(Ljava/lang/String;Ljava/util/Set;)Z
    .registers 28
    .parameter "pkgName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Landroid/content/ComponentName;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1882
    .local p2, removedProviders:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/ComponentName;>;"
    const/16 v18, 0x0

    #@2
    .line 1883
    .local v18, providersUpdated:Z
    new-instance v15, Ljava/util/HashSet;

    #@4
    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    #@7
    .line 1884
    .local v15, keep:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v13, Landroid/content/Intent;

    #@9
    const-string v21, "android.appwidget.action.APPWIDGET_UPDATE"

    #@b
    move-object/from16 v0, v21

    #@d
    invoke-direct {v13, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@10
    .line 1885
    .local v13, intent:Landroid/content/Intent;
    move-object/from16 v0, p1

    #@12
    invoke-virtual {v13, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 1888
    :try_start_15
    move-object/from16 v0, p0

    #@17
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mPm:Landroid/content/pm/IPackageManager;

    #@19
    move-object/from16 v21, v0

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@1f
    move-object/from16 v22, v0

    #@21
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v22

    #@25
    move-object/from16 v0, v22

    #@27
    invoke-virtual {v13, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@2a
    move-result-object v22

    #@2b
    const/16 v23, 0x80

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mUserId:I

    #@31
    move/from16 v24, v0

    #@33
    move-object/from16 v0, v21

    #@35
    move-object/from16 v1, v22

    #@37
    move/from16 v2, v23

    #@39
    move/from16 v3, v24

    #@3b
    invoke-interface {v0, v13, v1, v2, v3}, Landroid/content/pm/IPackageManager;->queryIntentReceivers(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;
    :try_end_3e
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_3e} :catch_62

    #@3e
    move-result-object v8

    #@3f
    .line 1897
    .local v8, broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v8, :cond_66

    #@41
    const/4 v5, 0x0

    #@42
    .line 1898
    .local v5, N:I
    :goto_42
    const/4 v11, 0x0

    #@43
    .local v11, i:I
    :goto_43
    if-ge v11, v5, :cond_141

    #@45
    .line 1899
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@48
    move-result-object v20

    #@49
    check-cast v20, Landroid/content/pm/ResolveInfo;

    #@4b
    .line 1900
    .local v20, ri:Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v20

    #@4d
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4f
    .line 1901
    .local v6, ai:Landroid/content/pm/ActivityInfo;
    iget-object v0, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@51
    move-object/from16 v21, v0

    #@53
    move-object/from16 v0, v21

    #@55
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@57
    move/from16 v21, v0

    #@59
    const/high16 v22, 0x4

    #@5b
    and-int v21, v21, v22

    #@5d
    if-eqz v21, :cond_6b

    #@5f
    .line 1898
    :cond_5f
    :goto_5f
    add-int/lit8 v11, v11, 0x1

    #@61
    goto :goto_43

    #@62
    .line 1891
    .end local v5           #N:I
    .end local v6           #ai:Landroid/content/pm/ActivityInfo;
    .end local v8           #broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v11           #i:I
    .end local v20           #ri:Landroid/content/pm/ResolveInfo;
    :catch_62
    move-exception v19

    #@63
    .line 1893
    .local v19, re:Landroid/os/RemoteException;
    const/16 v21, 0x0

    #@65
    .line 1966
    .end local v19           #re:Landroid/os/RemoteException;
    :goto_65
    return v21

    #@66
    .line 1897
    .restart local v8       #broadcastReceivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_66
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@69
    move-result v5

    #@6a
    goto :goto_42

    #@6b
    .line 1904
    .restart local v5       #N:I
    .restart local v6       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v11       #i:I
    .restart local v20       #ri:Landroid/content/pm/ResolveInfo;
    :cond_6b
    iget-object v0, v6, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@6d
    move-object/from16 v21, v0

    #@6f
    move-object/from16 v0, p1

    #@71
    move-object/from16 v1, v21

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v21

    #@77
    if-eqz v21, :cond_5f

    #@79
    .line 1905
    new-instance v9, Landroid/content/ComponentName;

    #@7b
    iget-object v0, v6, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@7d
    move-object/from16 v21, v0

    #@7f
    iget-object v0, v6, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@81
    move-object/from16 v22, v0

    #@83
    move-object/from16 v0, v21

    #@85
    move-object/from16 v1, v22

    #@87
    invoke-direct {v9, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@8a
    .line 1906
    .local v9, component:Landroid/content/ComponentName;
    move-object/from16 v0, p0

    #@8c
    invoke-virtual {v0, v9}, Lcom/android/server/AppWidgetServiceImpl;->lookupProviderLocked(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@8f
    move-result-object v16

    #@90
    .line 1907
    .local v16, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-nez v16, :cond_a8

    #@92
    .line 1908
    move-object/from16 v0, p0

    #@94
    move-object/from16 v1, v20

    #@96
    invoke-virtual {v0, v1}, Lcom/android/server/AppWidgetServiceImpl;->addProviderLocked(Landroid/content/pm/ResolveInfo;)Z

    #@99
    move-result v21

    #@9a
    if-eqz v21, :cond_5f

    #@9c
    .line 1909
    iget-object v0, v6, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@9e
    move-object/from16 v21, v0

    #@a0
    move-object/from16 v0, v21

    #@a2
    invoke-virtual {v15, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@a5
    .line 1910
    const/16 v18, 0x1

    #@a7
    goto :goto_5f

    #@a8
    .line 1913
    :cond_a8
    move-object/from16 v0, p0

    #@aa
    move-object/from16 v1, v20

    #@ac
    invoke-direct {v0, v9, v1}, Lcom/android/server/AppWidgetServiceImpl;->parseProviderInfoXml(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;)Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@af
    move-result-object v17

    #@b0
    .line 1914
    .local v17, parsed:Lcom/android/server/AppWidgetServiceImpl$Provider;
    if-eqz v17, :cond_5f

    #@b2
    .line 1915
    iget-object v0, v6, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@b4
    move-object/from16 v21, v0

    #@b6
    move-object/from16 v0, v21

    #@b8
    invoke-virtual {v15, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@bb
    .line 1917
    move-object/from16 v0, v17

    #@bd
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@bf
    move-object/from16 v21, v0

    #@c1
    move-object/from16 v0, v21

    #@c3
    move-object/from16 v1, v16

    #@c5
    iput-object v0, v1, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@c7
    .line 1919
    move-object/from16 v0, v16

    #@c9
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@cb
    move-object/from16 v21, v0

    #@cd
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    #@d0
    move-result v4

    #@d1
    .line 1920
    .local v4, M:I
    if-lez v4, :cond_5f

    #@d3
    .line 1921
    invoke-static/range {v16 .. v16}, Lcom/android/server/AppWidgetServiceImpl;->getAppWidgetIds(Lcom/android/server/AppWidgetServiceImpl$Provider;)[I

    #@d6
    move-result-object v7

    #@d7
    .line 1925
    .local v7, appWidgetIds:[I
    move-object/from16 v0, p0

    #@d9
    move-object/from16 v1, v16

    #@db
    invoke-virtual {v0, v1}, Lcom/android/server/AppWidgetServiceImpl;->cancelBroadcasts(Lcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@de
    .line 1926
    move-object/from16 v0, p0

    #@e0
    move-object/from16 v1, v16

    #@e2
    invoke-virtual {v0, v1, v7}, Lcom/android/server/AppWidgetServiceImpl;->registerForBroadcastsLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V

    #@e5
    .line 1929
    const/4 v14, 0x0

    #@e6
    .local v14, j:I
    :goto_e6
    if-ge v14, v4, :cond_136

    #@e8
    .line 1930
    move-object/from16 v0, v16

    #@ea
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@ec
    move-object/from16 v21, v0

    #@ee
    move-object/from16 v0, v21

    #@f0
    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f3
    move-result-object v12

    #@f4
    check-cast v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@f6
    .line 1931
    .local v12, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    const/16 v21, 0x0

    #@f8
    move-object/from16 v0, v21

    #@fa
    iput-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->views:Landroid/widget/RemoteViews;

    #@fc
    .line 1932
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@fe
    move-object/from16 v21, v0

    #@100
    if-eqz v21, :cond_125

    #@102
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@104
    move-object/from16 v21, v0

    #@106
    move-object/from16 v0, v21

    #@108
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@10a
    move-object/from16 v21, v0

    #@10c
    if-eqz v21, :cond_125

    #@10e
    .line 1934
    :try_start_10e
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@110
    move-object/from16 v21, v0

    #@112
    move-object/from16 v0, v21

    #@114
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@116
    move-object/from16 v21, v0

    #@118
    iget v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@11a
    move/from16 v22, v0

    #@11c
    move-object/from16 v0, v16

    #@11e
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@120
    move-object/from16 v23, v0

    #@122
    invoke-interface/range {v21 .. v23}, Lcom/android/internal/appwidget/IAppWidgetHost;->providerChanged(ILandroid/appwidget/AppWidgetProviderInfo;)V
    :try_end_125
    .catch Landroid/os/RemoteException; {:try_start_10e .. :try_end_125} :catch_128

    #@125
    .line 1929
    :cond_125
    :goto_125
    add-int/lit8 v14, v14, 0x1

    #@127
    goto :goto_e6

    #@128
    .line 1935
    :catch_128
    move-exception v10

    #@129
    .line 1939
    .local v10, ex:Landroid/os/RemoteException;
    iget-object v0, v12, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@12b
    move-object/from16 v21, v0

    #@12d
    const/16 v22, 0x0

    #@12f
    move-object/from16 v0, v22

    #@131
    move-object/from16 v1, v21

    #@133
    iput-object v0, v1, Lcom/android/server/AppWidgetServiceImpl$Host;->callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

    #@135
    goto :goto_125

    #@136
    .line 1944
    .end local v10           #ex:Landroid/os/RemoteException;
    .end local v12           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_136
    move-object/from16 v0, p0

    #@138
    move-object/from16 v1, v16

    #@13a
    invoke-virtual {v0, v1, v7}, Lcom/android/server/AppWidgetServiceImpl;->sendUpdateIntentLocked(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V

    #@13d
    .line 1945
    const/16 v18, 0x1

    #@13f
    goto/16 :goto_5f

    #@141
    .line 1953
    .end local v4           #M:I
    .end local v6           #ai:Landroid/content/pm/ActivityInfo;
    .end local v7           #appWidgetIds:[I
    .end local v9           #component:Landroid/content/ComponentName;
    .end local v14           #j:I
    .end local v16           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .end local v17           #parsed:Lcom/android/server/AppWidgetServiceImpl$Provider;
    .end local v20           #ri:Landroid/content/pm/ResolveInfo;
    :cond_141
    move-object/from16 v0, p0

    #@143
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@145
    move-object/from16 v21, v0

    #@147
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    #@14a
    move-result v5

    #@14b
    .line 1954
    add-int/lit8 v11, v5, -0x1

    #@14d
    :goto_14d
    if-ltz v11, :cond_1b0

    #@14f
    .line 1955
    move-object/from16 v0, p0

    #@151
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@153
    move-object/from16 v21, v0

    #@155
    move-object/from16 v0, v21

    #@157
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15a
    move-result-object v16

    #@15b
    check-cast v16, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@15d
    .line 1956
    .restart local v16       #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    move-object/from16 v0, v16

    #@15f
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@161
    move-object/from16 v21, v0

    #@163
    move-object/from16 v0, v21

    #@165
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@167
    move-object/from16 v21, v0

    #@169
    invoke-virtual/range {v21 .. v21}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@16c
    move-result-object v21

    #@16d
    move-object/from16 v0, p1

    #@16f
    move-object/from16 v1, v21

    #@171
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@174
    move-result v21

    #@175
    if-eqz v21, :cond_1ad

    #@177
    move-object/from16 v0, v16

    #@179
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@17b
    move-object/from16 v21, v0

    #@17d
    move-object/from16 v0, v21

    #@17f
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@181
    move-object/from16 v21, v0

    #@183
    invoke-virtual/range {v21 .. v21}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@186
    move-result-object v21

    #@187
    move-object/from16 v0, v21

    #@189
    invoke-virtual {v15, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@18c
    move-result v21

    #@18d
    if-nez v21, :cond_1ad

    #@18f
    .line 1958
    if-eqz p2, :cond_1a4

    #@191
    .line 1959
    move-object/from16 v0, v16

    #@193
    iget-object v0, v0, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@195
    move-object/from16 v21, v0

    #@197
    move-object/from16 v0, v21

    #@199
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@19b
    move-object/from16 v21, v0

    #@19d
    move-object/from16 v0, p2

    #@19f
    move-object/from16 v1, v21

    #@1a1
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@1a4
    .line 1961
    :cond_1a4
    move-object/from16 v0, p0

    #@1a6
    move-object/from16 v1, v16

    #@1a8
    invoke-virtual {v0, v11, v1}, Lcom/android/server/AppWidgetServiceImpl;->removeProviderLocked(ILcom/android/server/AppWidgetServiceImpl$Provider;)V

    #@1ab
    .line 1962
    const/16 v18, 0x1

    #@1ad
    .line 1954
    :cond_1ad
    add-int/lit8 v11, v11, -0x1

    #@1af
    goto :goto_14d

    #@1b0
    .end local v16           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_1b0
    move/from16 v21, v18

    #@1b2
    .line 1966
    goto/16 :goto_65
.end method

.method writeStateToFileLocked(Ljava/io/FileOutputStream;)Z
    .registers 16
    .parameter "stream"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 1577
    :try_start_1
    new-instance v6, Lcom/android/internal/util/FastXmlSerializer;

    #@3
    invoke-direct {v6}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@6
    .line 1578
    .local v6, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v9, "utf-8"

    #@8
    invoke-interface {v6, p1, v9}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@b
    .line 1579
    const/4 v9, 0x0

    #@c
    const/4 v11, 0x1

    #@d
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v11

    #@11
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@14
    .line 1580
    const/4 v9, 0x0

    #@15
    const-string v11, "gs"

    #@17
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1a
    .line 1582
    const/4 v8, 0x0

    #@1b
    .line 1583
    .local v8, providerIndex:I
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@20
    move-result v0

    #@21
    .line 1584
    .local v0, N:I
    const/4 v3, 0x0

    #@22
    .local v3, i:I
    :goto_22
    if-ge v3, v0, :cond_63

    #@24
    .line 1585
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mInstalledProviders:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v7

    #@2a
    check-cast v7, Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@2c
    .line 1586
    .local v7, p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    iget-object v9, v7, Lcom/android/server/AppWidgetServiceImpl$Provider;->instances:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v9

    #@32
    if-lez v9, :cond_60

    #@34
    .line 1587
    const/4 v9, 0x0

    #@35
    const-string v11, "p"

    #@37
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3a
    .line 1588
    const/4 v9, 0x0

    #@3b
    const-string v11, "pkg"

    #@3d
    iget-object v12, v7, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@3f
    iget-object v12, v12, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@41
    invoke-virtual {v12}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@44
    move-result-object v12

    #@45
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@48
    .line 1589
    const/4 v9, 0x0

    #@49
    const-string v11, "cl"

    #@4b
    iget-object v12, v7, Lcom/android/server/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    #@4d
    iget-object v12, v12, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@4f
    invoke-virtual {v12}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@52
    move-result-object v12

    #@53
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@56
    .line 1590
    const/4 v9, 0x0

    #@57
    const-string v11, "p"

    #@59
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5c
    .line 1591
    iput v8, v7, Lcom/android/server/AppWidgetServiceImpl$Provider;->tag:I

    #@5e
    .line 1592
    add-int/lit8 v8, v8, 0x1

    #@60
    .line 1584
    :cond_60
    add-int/lit8 v3, v3, 0x1

    #@62
    goto :goto_22

    #@63
    .line 1596
    .end local v7           #p:Lcom/android/server/AppWidgetServiceImpl$Provider;
    :cond_63
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@68
    move-result v0

    #@69
    .line 1597
    const/4 v3, 0x0

    #@6a
    :goto_6a
    if-ge v3, v0, :cond_99

    #@6c
    .line 1598
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mHosts:Ljava/util/ArrayList;

    #@6e
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@71
    move-result-object v2

    #@72
    check-cast v2, Lcom/android/server/AppWidgetServiceImpl$Host;

    #@74
    .line 1599
    .local v2, host:Lcom/android/server/AppWidgetServiceImpl$Host;
    const/4 v9, 0x0

    #@75
    const-string v11, "h"

    #@77
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7a
    .line 1600
    const/4 v9, 0x0

    #@7b
    const-string v11, "pkg"

    #@7d
    iget-object v12, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->packageName:Ljava/lang/String;

    #@7f
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@82
    .line 1601
    const/4 v9, 0x0

    #@83
    const-string v11, "id"

    #@85
    iget v12, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->hostId:I

    #@87
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@8a
    move-result-object v12

    #@8b
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@8e
    .line 1602
    const/4 v9, 0x0

    #@8f
    const-string v11, "h"

    #@91
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@94
    .line 1603
    iput v3, v2, Lcom/android/server/AppWidgetServiceImpl$Host;->tag:I

    #@96
    .line 1597
    add-int/lit8 v3, v3, 0x1

    #@98
    goto :goto_6a

    #@99
    .line 1606
    .end local v2           #host:Lcom/android/server/AppWidgetServiceImpl$Host;
    :cond_99
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@9b
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@9e
    move-result v0

    #@9f
    .line 1607
    const/4 v3, 0x0

    #@a0
    :goto_a0
    if-ge v3, v0, :cond_144

    #@a2
    .line 1608
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mAppWidgetIds:Ljava/util/ArrayList;

    #@a4
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a7
    move-result-object v4

    #@a8
    check-cast v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;

    #@aa
    .line 1609
    .local v4, id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    const/4 v9, 0x0

    #@ab
    const-string v11, "g"

    #@ad
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b0
    .line 1610
    const/4 v9, 0x0

    #@b1
    const-string v11, "id"

    #@b3
    iget v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->appWidgetId:I

    #@b5
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@b8
    move-result-object v12

    #@b9
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@bc
    .line 1611
    const/4 v9, 0x0

    #@bd
    const-string v11, "h"

    #@bf
    iget-object v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->host:Lcom/android/server/AppWidgetServiceImpl$Host;

    #@c1
    iget v12, v12, Lcom/android/server/AppWidgetServiceImpl$Host;->tag:I

    #@c3
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@c6
    move-result-object v12

    #@c7
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ca
    .line 1612
    iget-object v9, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@cc
    if-eqz v9, :cond_dc

    #@ce
    .line 1613
    const/4 v9, 0x0

    #@cf
    const-string v11, "p"

    #@d1
    iget-object v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->provider:Lcom/android/server/AppWidgetServiceImpl$Provider;

    #@d3
    iget v12, v12, Lcom/android/server/AppWidgetServiceImpl$Provider;->tag:I

    #@d5
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@d8
    move-result-object v12

    #@d9
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@dc
    .line 1615
    :cond_dc
    iget-object v9, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@de
    if-eqz v9, :cond_13a

    #@e0
    .line 1616
    const/4 v9, 0x0

    #@e1
    const-string v11, "min_width"

    #@e3
    iget-object v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@e5
    const-string v13, "appWidgetMinWidth"

    #@e7
    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@ea
    move-result v12

    #@eb
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@ee
    move-result-object v12

    #@ef
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f2
    .line 1618
    const/4 v9, 0x0

    #@f3
    const-string v11, "min_height"

    #@f5
    iget-object v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@f7
    const-string v13, "appWidgetMinHeight"

    #@f9
    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@fc
    move-result v12

    #@fd
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@100
    move-result-object v12

    #@101
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@104
    .line 1620
    const/4 v9, 0x0

    #@105
    const-string v11, "max_width"

    #@107
    iget-object v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@109
    const-string v13, "appWidgetMaxWidth"

    #@10b
    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@10e
    move-result v12

    #@10f
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@112
    move-result-object v12

    #@113
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@116
    .line 1622
    const/4 v9, 0x0

    #@117
    const-string v11, "max_height"

    #@119
    iget-object v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@11b
    const-string v13, "appWidgetMaxHeight"

    #@11d
    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@120
    move-result v12

    #@121
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@124
    move-result-object v12

    #@125
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@128
    .line 1624
    const/4 v9, 0x0

    #@129
    const-string v11, "host_category"

    #@12b
    iget-object v12, v4, Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;->options:Landroid/os/Bundle;

    #@12d
    const-string v13, "appWidgetCategory"

    #@12f
    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@132
    move-result v12

    #@133
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@136
    move-result-object v12

    #@137
    invoke-interface {v6, v9, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@13a
    .line 1627
    :cond_13a
    const/4 v9, 0x0

    #@13b
    const-string v11, "g"

    #@13d
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@140
    .line 1607
    add-int/lit8 v3, v3, 0x1

    #@142
    goto/16 :goto_a0

    #@144
    .line 1630
    .end local v4           #id:Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
    :cond_144
    iget-object v9, p0, Lcom/android/server/AppWidgetServiceImpl;->mPackagesWithBindWidgetPermission:Ljava/util/HashSet;

    #@146
    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@149
    move-result-object v5

    #@14a
    .line 1631
    .local v5, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_14a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@14d
    move-result v9

    #@14e
    if-eqz v9, :cond_184

    #@150
    .line 1632
    const/4 v9, 0x0

    #@151
    const-string v11, "b"

    #@153
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@156
    .line 1633
    const/4 v11, 0x0

    #@157
    const-string v12, "packageName"

    #@159
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15c
    move-result-object v9

    #@15d
    check-cast v9, Ljava/lang/String;

    #@15f
    invoke-interface {v6, v11, v12, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@162
    .line 1634
    const/4 v9, 0x0

    #@163
    const-string v11, "b"

    #@165
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_168
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_168} :catch_169

    #@168
    goto :goto_14a

    #@169
    .line 1641
    .end local v0           #N:I
    .end local v3           #i:I
    .end local v5           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v6           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v8           #providerIndex:I
    :catch_169
    move-exception v1

    #@16a
    .line 1642
    .local v1, e:Ljava/io/IOException;
    const-string v9, "AppWidgetServiceImpl"

    #@16c
    new-instance v10, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v11, "Failed to write state: "

    #@173
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v10

    #@177
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v10

    #@17b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v10

    #@17f
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@182
    .line 1643
    const/4 v9, 0x0

    #@183
    .end local v1           #e:Ljava/io/IOException;
    :goto_183
    return v9

    #@184
    .line 1637
    .restart local v0       #N:I
    .restart local v3       #i:I
    .restart local v5       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v6       #out:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v8       #providerIndex:I
    :cond_184
    const/4 v9, 0x0

    #@185
    :try_start_185
    const-string v11, "gs"

    #@187
    invoke-interface {v6, v9, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@18a
    .line 1639
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V
    :try_end_18d
    .catch Ljava/io/IOException; {:try_start_185 .. :try_end_18d} :catch_169

    #@18d
    move v9, v10

    #@18e
    .line 1640
    goto :goto_183
.end method
