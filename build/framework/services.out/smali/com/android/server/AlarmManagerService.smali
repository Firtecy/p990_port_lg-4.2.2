.class Lcom/android/server/AlarmManagerService;
.super Landroid/app/IAlarmManager$Stub;
.source "AlarmManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/AlarmManagerService$ResultReceiver;,
        Lcom/android/server/AlarmManagerService$UninstallReceiver;,
        Lcom/android/server/AlarmManagerService$ClockReceiver;,
        Lcom/android/server/AlarmManagerService$AlarmHandler;,
        Lcom/android/server/AlarmManagerService$AlarmThread;,
        Lcom/android/server/AlarmManagerService$Alarm;,
        Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;,
        Lcom/android/server/AlarmManagerService$BroadcastStats;,
        Lcom/android/server/AlarmManagerService$FilterStats;,
        Lcom/android/server/AlarmManagerService$InFlight;
    }
.end annotation


# static fields
.field private static final ALARM_EVENT:I = 0x1

.field private static final ClockReceiver_TAG:Ljava/lang/String; = "ClockReceiver"

.field private static final ELAPSED_REALTIME_MASK:I = 0x8

.field private static final ELAPSED_REALTIME_WAKEUP_MASK:I = 0x4

.field private static final LATE_ALARM_THRESHOLD:J = 0x2710L

.field private static final QUANTUM:J = 0xdbba0L

.field private static final RTC_MASK:I = 0x2

.field private static final RTC_WAKEUP_MASK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AlarmManager"

.field private static final TIMEZONE_PROPERTY:Ljava/lang/String; = "persist.sys.timezone"

.field private static final TIME_CHANGED_MASK:I = 0x10000

.field private static final localLOGV:Z

.field private static final mBackgroundIntent:Landroid/content/Intent;


# instance fields
.field private final mBlockedUids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mBroadcastRefCount:I

.field private final mBroadcastStats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/AlarmManagerService$BroadcastStats;",
            ">;"
        }
    .end annotation
.end field

.field private mClockReceiver:Lcom/android/server/AlarmManagerService$ClockReceiver;

.field private final mContext:Landroid/content/Context;

.field private final mDateChangeSender:Landroid/app/PendingIntent;

.field private mDescriptor:I

.field private final mElapsedRealtimeAlarms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;"
        }
    .end annotation
.end field

.field private final mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Lcom/android/server/AlarmManagerService$AlarmHandler;

.field private mInFlight:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$InFlight;",
            ">;"
        }
    .end annotation
.end field

.field private final mIncreasingTimeOrder:Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;

.field private mLock:Ljava/lang/Object;

.field private final mLog:Lcom/android/internal/util/LocalLog;

.field private final mResultReceiver:Lcom/android/server/AlarmManagerService$ResultReceiver;

.field private final mRtcAlarms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;"
        }
    .end annotation
.end field

.field private final mRtcWakeupAlarms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;"
        }
    .end annotation
.end field

.field private final mTimeTickSender:Landroid/app/PendingIntent;

.field private final mTriggeredUids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUninstallReceiver:Lcom/android/server/AlarmManagerService$UninstallReceiver;

.field private final mWaitThread:Lcom/android/server/AlarmManagerService$AlarmThread;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 83
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    const/4 v1, 0x4

    #@6
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@9
    move-result-object v0

    #@a
    sput-object v0, Lcom/android/server/AlarmManagerService;->mBackgroundIntent:Landroid/content/Intent;

    #@c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 170
    invoke-direct {p0}, Landroid/app/IAlarmManager$Stub;-><init>()V

    #@4
    .line 88
    new-instance v3, Lcom/android/internal/util/LocalLog;

    #@6
    const-string v4, "AlarmManager"

    #@8
    invoke-direct {v3, v4}, Lcom/android/internal/util/LocalLog;-><init>(Ljava/lang/String;)V

    #@b
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mLog:Lcom/android/internal/util/LocalLog;

    #@d
    .line 90
    new-instance v3, Ljava/lang/Object;

    #@f
    invoke-direct/range {v3 .. v3}, Ljava/lang/Object;-><init>()V

    #@12
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mLock:Ljava/lang/Object;

    #@14
    .line 92
    new-instance v3, Ljava/util/ArrayList;

    #@16
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@19
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@1b
    .line 93
    new-instance v3, Ljava/util/ArrayList;

    #@1d
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@20
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@22
    .line 94
    new-instance v3, Ljava/util/ArrayList;

    #@24
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@27
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@29
    .line 95
    new-instance v3, Ljava/util/ArrayList;

    #@2b
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@2e
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@30
    .line 96
    new-instance v3, Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;

    #@32
    invoke-direct {v3}, Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;-><init>()V

    #@35
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mIncreasingTimeOrder:Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;

    #@37
    .line 98
    new-instance v3, Ljava/util/ArrayList;

    #@39
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@3c
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mTriggeredUids:Ljava/util/ArrayList;

    #@3e
    .line 99
    new-instance v3, Ljava/util/ArrayList;

    #@40
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@43
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@45
    .line 102
    iput v5, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@47
    .line 104
    new-instance v3, Ljava/util/ArrayList;

    #@49
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@4c
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mInFlight:Ljava/util/ArrayList;

    #@4e
    .line 105
    new-instance v3, Lcom/android/server/AlarmManagerService$AlarmThread;

    #@50
    invoke-direct {v3, p0}, Lcom/android/server/AlarmManagerService$AlarmThread;-><init>(Lcom/android/server/AlarmManagerService;)V

    #@53
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mWaitThread:Lcom/android/server/AlarmManagerService$AlarmThread;

    #@55
    .line 106
    new-instance v3, Lcom/android/server/AlarmManagerService$AlarmHandler;

    #@57
    invoke-direct {v3, p0}, Lcom/android/server/AlarmManagerService$AlarmHandler;-><init>(Lcom/android/server/AlarmManagerService;)V

    #@5a
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mHandler:Lcom/android/server/AlarmManagerService$AlarmHandler;

    #@5c
    .line 109
    new-instance v3, Lcom/android/server/AlarmManagerService$ResultReceiver;

    #@5e
    invoke-direct {v3, p0}, Lcom/android/server/AlarmManagerService$ResultReceiver;-><init>(Lcom/android/server/AlarmManagerService;)V

    #@61
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mResultReceiver:Lcom/android/server/AlarmManagerService$ResultReceiver;

    #@63
    .line 167
    new-instance v3, Ljava/util/HashMap;

    #@65
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@68
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mBroadcastStats:Ljava/util/HashMap;

    #@6a
    .line 171
    iput-object p1, p0, Lcom/android/server/AlarmManagerService;->mContext:Landroid/content/Context;

    #@6c
    .line 172
    invoke-direct {p0}, Lcom/android/server/AlarmManagerService;->init()I

    #@6f
    move-result v3

    #@70
    iput v3, p0, Lcom/android/server/AlarmManagerService;->mDescriptor:I

    #@72
    .line 176
    const-string v3, "persist.sys.timezone"

    #@74
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    .line 177
    .local v2, tz:Ljava/lang/String;
    if-eqz v2, :cond_7d

    #@7a
    .line 178
    invoke-virtual {p0, v2}, Lcom/android/server/AlarmManagerService;->setTimeZone(Ljava/lang/String;)V

    #@7d
    .line 181
    :cond_7d
    const-string v3, "power"

    #@7f
    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@82
    move-result-object v1

    #@83
    check-cast v1, Landroid/os/PowerManager;

    #@85
    .line 182
    .local v1, pm:Landroid/os/PowerManager;
    const/4 v3, 0x1

    #@86
    const-string v4, "AlarmManager"

    #@88
    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@8b
    move-result-object v3

    #@8c
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@8e
    .line 184
    new-instance v3, Landroid/content/Intent;

    #@90
    const-string v4, "android.intent.action.TIME_TICK"

    #@92
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@95
    const/high16 v4, 0x4000

    #@97
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@9a
    move-result-object v3

    #@9b
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@9d
    invoke-static {p1, v5, v3, v5, v4}, Landroid/app/PendingIntent;->getBroadcastAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@a0
    move-result-object v3

    #@a1
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mTimeTickSender:Landroid/app/PendingIntent;

    #@a3
    .line 188
    new-instance v0, Landroid/content/Intent;

    #@a5
    const-string v3, "android.intent.action.DATE_CHANGED"

    #@a7
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@aa
    .line 189
    .local v0, intent:Landroid/content/Intent;
    const/high16 v3, 0x2000

    #@ac
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@af
    .line 190
    const/high16 v3, 0x800

    #@b1
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@b3
    invoke-static {p1, v5, v0, v3, v4}, Landroid/app/PendingIntent;->getBroadcastAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@b6
    move-result-object v3

    #@b7
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mDateChangeSender:Landroid/app/PendingIntent;

    #@b9
    .line 194
    new-instance v3, Lcom/android/server/AlarmManagerService$ClockReceiver;

    #@bb
    invoke-direct {v3, p0}, Lcom/android/server/AlarmManagerService$ClockReceiver;-><init>(Lcom/android/server/AlarmManagerService;)V

    #@be
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mClockReceiver:Lcom/android/server/AlarmManagerService$ClockReceiver;

    #@c0
    .line 195
    iget-object v3, p0, Lcom/android/server/AlarmManagerService;->mClockReceiver:Lcom/android/server/AlarmManagerService$ClockReceiver;

    #@c2
    invoke-virtual {v3}, Lcom/android/server/AlarmManagerService$ClockReceiver;->scheduleTimeTickEvent()V

    #@c5
    .line 196
    iget-object v3, p0, Lcom/android/server/AlarmManagerService;->mClockReceiver:Lcom/android/server/AlarmManagerService$ClockReceiver;

    #@c7
    invoke-virtual {v3}, Lcom/android/server/AlarmManagerService$ClockReceiver;->scheduleDateChangedEvent()V

    #@ca
    .line 197
    new-instance v3, Lcom/android/server/AlarmManagerService$UninstallReceiver;

    #@cc
    invoke-direct {v3, p0}, Lcom/android/server/AlarmManagerService$UninstallReceiver;-><init>(Lcom/android/server/AlarmManagerService;)V

    #@cf
    iput-object v3, p0, Lcom/android/server/AlarmManagerService;->mUninstallReceiver:Lcom/android/server/AlarmManagerService$UninstallReceiver;

    #@d1
    .line 199
    iget v3, p0, Lcom/android/server/AlarmManagerService;->mDescriptor:I

    #@d3
    const/4 v4, -0x1

    #@d4
    if-eq v3, v4, :cond_dc

    #@d6
    .line 200
    iget-object v3, p0, Lcom/android/server/AlarmManagerService;->mWaitThread:Lcom/android/server/AlarmManagerService$AlarmThread;

    #@d8
    invoke-virtual {v3}, Lcom/android/server/AlarmManagerService$AlarmThread;->start()V

    #@db
    .line 204
    :goto_db
    return-void

    #@dc
    .line 202
    :cond_dc
    const-string v3, "AlarmManager"

    #@de
    const-string v4, "Failed to open alarm driver. Falling back to a handler."

    #@e0
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    goto :goto_db
.end method

.method static synthetic access$000(Lcom/android/server/AlarmManagerService;Landroid/app/PendingIntent;)Lcom/android/server/AlarmManagerService$BroadcastStats;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/server/AlarmManagerService;->getStatsLocked(Landroid/app/PendingIntent;)Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/AlarmManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/AlarmManagerService;->mDescriptor:I

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1200()Landroid/content/Intent;
    .registers 1

    #@0
    .prologue
    .line 63
    sget-object v0, Lcom/android/server/AlarmManagerService;->mBackgroundIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/AlarmManagerService;)Lcom/android/server/AlarmManagerService$ResultReceiver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mResultReceiver:Lcom/android/server/AlarmManagerService$ResultReceiver;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/server/AlarmManagerService;)Lcom/android/server/AlarmManagerService$AlarmHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mHandler:Lcom/android/server/AlarmManagerService$AlarmHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/AlarmManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@2
    return v0
.end method

.method static synthetic access$1508(Lcom/android/server/AlarmManagerService;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@6
    return v0
.end method

.method static synthetic access$1510(Lcom/android/server/AlarmManagerService;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@6
    return v0
.end method

.method static synthetic access$1600(Lcom/android/server/AlarmManagerService;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mInFlight:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mTriggeredUids:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/server/AlarmManagerService;II)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/android/server/AlarmManagerService;->setKernelTimezone(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/AlarmManagerService;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/server/AlarmManagerService;->waitForAlarm(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2000(Lcom/android/server/AlarmManagerService;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mDateChangeSender:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/server/AlarmManagerService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mBroadcastStats:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/server/AlarmManagerService;)Lcom/android/internal/util/LocalLog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mLog:Lcom/android/internal/util/LocalLog;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/AlarmManagerService;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mTimeTickSender:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/AlarmManagerService;)Lcom/android/server/AlarmManagerService$ClockReceiver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mClockReceiver:Lcom/android/server/AlarmManagerService$ClockReceiver;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/AlarmManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/AlarmManagerService;Ljava/util/ArrayList;Ljava/util/ArrayList;J)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/AlarmManagerService;->triggerAlarmsLocked(Ljava/util/ArrayList;Ljava/util/ArrayList;J)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method private addAlarmLocked(Lcom/android/server/AlarmManagerService$Alarm;)I
    .registers 5
    .parameter "alarm"

    #@0
    .prologue
    .line 488
    iget v2, p1, Lcom/android/server/AlarmManagerService$Alarm;->type:I

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/AlarmManagerService;->getAlarmList(I)Ljava/util/ArrayList;

    #@5
    move-result-object v0

    #@6
    .line 490
    .local v0, alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    iget-object v2, p0, Lcom/android/server/AlarmManagerService;->mIncreasingTimeOrder:Lcom/android/server/AlarmManagerService$IncreasingTimeOrder;

    #@8
    invoke-static {v0, p1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    #@b
    move-result v1

    #@c
    .line 491
    .local v1, index:I
    if-gez v1, :cond_12

    #@e
    .line 492
    rsub-int/lit8 v2, v1, 0x0

    #@10
    add-int/lit8 v1, v2, -0x1

    #@12
    .line 495
    :cond_12
    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@15
    .line 511
    return v1
.end method

.method private native close(I)V
.end method

.method private static final dumpAlarmList(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V
    .registers 10
    .parameter "pw"
    .parameter
    .parameter "prefix"
    .parameter "label"
    .parameter "now"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    #@0
    .prologue
    .line 700
    .local p1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v2

    #@4
    add-int/lit8 v1, v2, -0x1

    #@6
    .local v1, i:I
    :goto_6
    if-ltz v1, :cond_3d

    #@8
    .line 701
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@e
    .line 702
    .local v0, a:Lcom/android/server/AlarmManagerService$Alarm;
    invoke-virtual {p0, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11
    invoke-virtual {p0, p3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14
    const-string v2, " #"

    #@16
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    invoke-virtual {p0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1c
    .line 703
    const-string v2, ": "

    #@1e
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@24
    .line 704
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "  "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v0, p0, v2, p4, p5}, Lcom/android/server/AlarmManagerService$Alarm;->dump(Ljava/io/PrintWriter;Ljava/lang/String;J)V

    #@3a
    .line 700
    add-int/lit8 v1, v1, -0x1

    #@3c
    goto :goto_6

    #@3d
    .line 706
    .end local v0           #a:Lcom/android/server/AlarmManagerService$Alarm;
    :cond_3d
    return-void
.end method

.method private getAlarmList(I)Ljava/util/ArrayList;
    .registers 3
    .parameter "type"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 477
    packed-switch p1, :pswitch_data_12

    #@3
    .line 484
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 478
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@7
    goto :goto_4

    #@8
    .line 479
    :pswitch_8
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@a
    goto :goto_4

    #@b
    .line 480
    :pswitch_b
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@d
    goto :goto_4

    #@e
    .line 481
    :pswitch_e
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@10
    goto :goto_4

    #@11
    .line 477
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_5
        :pswitch_8
        :pswitch_b
        :pswitch_e
    .end packed-switch
.end method

.method private final getStatsLocked(Landroid/app/PendingIntent;)Lcom/android/server/AlarmManagerService$BroadcastStats;
    .registers 5
    .parameter "pi"

    #@0
    .prologue
    .line 1129
    invoke-virtual {p1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1130
    .local v1, pkg:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/AlarmManagerService;->mBroadcastStats:Ljava/util/HashMap;

    #@6
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@c
    .line 1131
    .local v0, bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    if-nez v0, :cond_18

    #@e
    .line 1132
    new-instance v0, Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@10
    .end local v0           #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    invoke-direct {v0, v1}, Lcom/android/server/AlarmManagerService$BroadcastStats;-><init>(Ljava/lang/String;)V

    #@13
    .line 1133
    .restart local v0       #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    iget-object v2, p0, Lcom/android/server/AlarmManagerService;->mBroadcastStats:Ljava/util/HashMap;

    #@15
    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 1135
    :cond_18
    return-object v0
.end method

.method private native init()I
.end method

.method private lookForPackageLocked(Ljava/util/ArrayList;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    #@0
    .prologue
    .line 468
    .local p1, alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v1

    #@4
    add-int/lit8 v0, v1, -0x1

    #@6
    .local v0, i:I
    :goto_6
    if-ltz v0, :cond_1f

    #@8
    .line 469
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Lcom/android/server/AlarmManagerService$Alarm;

    #@e
    iget-object v1, v1, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@10
    invoke-virtual {v1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    .line 470
    const/4 v1, 0x1

    #@1b
    .line 473
    :goto_1b
    return v1

    #@1c
    .line 468
    :cond_1c
    add-int/lit8 v0, v0, -0x1

    #@1e
    goto :goto_6

    #@1f
    .line 473
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_1b
.end method

.method private removeLocked(Ljava/util/ArrayList;Landroid/app/PendingIntent;)V
    .registers 6
    .parameter
    .parameter "operation"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;",
            "Landroid/app/PendingIntent;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 353
    .local p1, alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v2

    #@4
    if-gtz v2, :cond_7

    #@6
    .line 366
    :cond_6
    return-void

    #@7
    .line 358
    :cond_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 360
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/AlarmManagerService$Alarm;>;"
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_6

    #@11
    .line 361
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@17
    .line 362
    .local v0, alarm:Lcom/android/server/AlarmManagerService$Alarm;
    iget-object v2, v0, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@19
    invoke-virtual {v2, p2}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_b

    #@1f
    .line 363
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@22
    goto :goto_b
.end method

.method private removeLocked(Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 422
    .local p1, alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v2

    #@4
    if-gtz v2, :cond_7

    #@6
    .line 435
    :cond_6
    return-void

    #@7
    .line 427
    :cond_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 429
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/AlarmManagerService$Alarm;>;"
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_6

    #@11
    .line 430
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@17
    .line 431
    .local v0, alarm:Lcom/android/server/AlarmManagerService$Alarm;
    iget-object v2, v0, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@19
    invoke-virtual {v2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_b

    #@23
    .line 432
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@26
    goto :goto_b
.end method

.method private removeUserLocked(Ljava/util/ArrayList;I)V
    .registers 6
    .parameter
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 445
    .local p1, alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v2

    #@4
    if-gtz v2, :cond_7

    #@6
    .line 458
    :cond_6
    return-void

    #@7
    .line 450
    :cond_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 452
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/AlarmManagerService$Alarm;>;"
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_6

    #@11
    .line 453
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@17
    .line 454
    .local v0, alarm:Lcom/android/server/AlarmManagerService$Alarm;
    iget-object v2, v0, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@19
    invoke-virtual {v2}, Landroid/app/PendingIntent;->getCreatorUid()I

    #@1c
    move-result v2

    #@1d
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    #@20
    move-result v2

    #@21
    if-ne v2, p2, :cond_b

    #@23
    .line 455
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@26
    goto :goto_b
.end method

.method private native set(IIJJ)V
.end method

.method private native setKernelTimezone(II)I
.end method

.method private setLocked(Lcom/android/server/AlarmManagerService$Alarm;)V
    .registers 14
    .parameter "alarm"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const-wide/16 v10, 0x3e8

    #@3
    .line 533
    iget v0, p0, Lcom/android/server/AlarmManagerService;->mDescriptor:I

    #@5
    const/4 v1, -0x1

    #@6
    if-eq v0, v1, :cond_28

    #@8
    .line 538
    iget-wide v0, p1, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@a
    const-wide/16 v8, 0x0

    #@c
    cmp-long v0, v0, v8

    #@e
    if-gez v0, :cond_1d

    #@10
    .line 539
    const-wide/16 v3, 0x0

    #@12
    .line 540
    .local v3, alarmSeconds:J
    const-wide/16 v5, 0x0

    #@14
    .line 546
    .local v5, alarmNanoseconds:J
    :goto_14
    iget v1, p0, Lcom/android/server/AlarmManagerService;->mDescriptor:I

    #@16
    iget v2, p1, Lcom/android/server/AlarmManagerService$Alarm;->type:I

    #@18
    move-object v0, p0

    #@19
    invoke-direct/range {v0 .. v6}, Lcom/android/server/AlarmManagerService;->set(IIJJ)V

    #@1c
    .line 556
    .end local v3           #alarmSeconds:J
    .end local v5           #alarmNanoseconds:J
    :goto_1c
    return-void

    #@1d
    .line 542
    :cond_1d
    iget-wide v0, p1, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@1f
    div-long v3, v0, v10

    #@21
    .line 543
    .restart local v3       #alarmSeconds:J
    iget-wide v0, p1, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@23
    rem-long/2addr v0, v10

    #@24
    mul-long/2addr v0, v10

    #@25
    mul-long v5, v0, v10

    #@27
    .restart local v5       #alarmNanoseconds:J
    goto :goto_14

    #@28
    .line 550
    .end local v3           #alarmSeconds:J
    .end local v5           #alarmNanoseconds:J
    :cond_28
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@2b
    move-result-object v7

    #@2c
    .line 551
    .local v7, msg:Landroid/os/Message;
    iput v2, v7, Landroid/os/Message;->what:I

    #@2e
    .line 553
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mHandler:Lcom/android/server/AlarmManagerService$AlarmHandler;

    #@30
    invoke-virtual {v0, v2}, Lcom/android/server/AlarmManagerService$AlarmHandler;->removeMessages(I)V

    #@33
    .line 554
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mHandler:Lcom/android/server/AlarmManagerService$AlarmHandler;

    #@35
    iget-wide v1, p1, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@37
    invoke-virtual {v0, v7, v1, v2}, Lcom/android/server/AlarmManagerService$AlarmHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@3a
    goto :goto_1c
.end method

.method private triggerAlarmsLocked(Ljava/util/ArrayList;Ljava/util/ArrayList;J)V
    .registers 16
    .parameter
    .parameter
    .parameter "now"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AlarmManagerService$Alarm;",
            ">;J)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    .local p2, triggerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    const-wide/16 v9, 0x0

    #@2
    .line 718
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 719
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/AlarmManagerService$Alarm;>;"
    new-instance v2, Ljava/util/ArrayList;

    #@8
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 721
    .local v2, repeats:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1d

    #@11
    .line 723
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@17
    .line 727
    .local v0, alarm:Lcom/android/server/AlarmManagerService$Alarm;
    iget-wide v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@19
    cmp-long v3, v3, p3

    #@1b
    if-lez v3, :cond_3c

    #@1d
    .line 763
    .end local v0           #alarm:Lcom/android/server/AlarmManagerService$Alarm;
    :cond_1d
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v1

    #@21
    .line 764
    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_63

    #@27
    .line 765
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@2d
    .line 766
    .restart local v0       #alarm:Lcom/android/server/AlarmManagerService$Alarm;
    iget-wide v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@2f
    iget v5, v0, Lcom/android/server/AlarmManagerService$Alarm;->count:I

    #@31
    int-to-long v5, v5

    #@32
    iget-wide v7, v0, Lcom/android/server/AlarmManagerService$Alarm;->repeatInterval:J

    #@34
    mul-long/2addr v5, v7

    #@35
    add-long/2addr v3, v5

    #@36
    iput-wide v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@38
    .line 767
    invoke-direct {p0, v0}, Lcom/android/server/AlarmManagerService;->addAlarmLocked(Lcom/android/server/AlarmManagerService$Alarm;)I

    #@3b
    goto :goto_21

    #@3c
    .line 745
    :cond_3c
    const/4 v3, 0x1

    #@3d
    iput v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->count:I

    #@3f
    .line 746
    iget-wide v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->repeatInterval:J

    #@41
    cmp-long v3, v3, v9

    #@43
    if-lez v3, :cond_53

    #@45
    .line 749
    iget v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->count:I

    #@47
    int-to-long v3, v3

    #@48
    iget-wide v5, v0, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@4a
    sub-long v5, p3, v5

    #@4c
    iget-wide v7, v0, Lcom/android/server/AlarmManagerService$Alarm;->repeatInterval:J

    #@4e
    div-long/2addr v5, v7

    #@4f
    add-long/2addr v3, v5

    #@50
    long-to-int v3, v3

    #@51
    iput v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->count:I

    #@53
    .line 751
    :cond_53
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@56
    .line 754
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@59
    .line 757
    iget-wide v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->repeatInterval:J

    #@5b
    cmp-long v3, v3, v9

    #@5d
    if-lez v3, :cond_b

    #@5f
    .line 758
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@62
    goto :goto_b

    #@63
    .line 770
    .end local v0           #alarm:Lcom/android/server/AlarmManagerService$Alarm;
    :cond_63
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@66
    move-result v3

    #@67
    if-lez v3, :cond_73

    #@69
    .line 771
    const/4 v3, 0x0

    #@6a
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6d
    move-result-object v3

    #@6e
    check-cast v3, Lcom/android/server/AlarmManagerService$Alarm;

    #@70
    invoke-direct {p0, v3}, Lcom/android/server/AlarmManagerService;->setLocked(Lcom/android/server/AlarmManagerService$Alarm;)V

    #@73
    .line 773
    :cond_73
    return-void
.end method

.method private native waitForAlarm(I)I
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 28
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 560
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mContext:Landroid/content/Context;

    #@4
    const-string v4, "android.permission.DUMP"

    #@6
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_37

    #@c
    .line 562
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Permission Denial: can\'t dump AlarmManager from from pid="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1a
    move-result v4

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ", uid="

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@28
    move-result v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    move-object/from16 v0, p2

    #@33
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@36
    .line 696
    :goto_36
    return-void

    #@37
    .line 568
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Lcom/android/server/AlarmManagerService;->mLock:Ljava/lang/Object;

    #@3b
    move-object/from16 v23, v0

    #@3d
    monitor-enter v23

    #@3e
    .line 569
    :try_start_3e
    const-string v3, "Current Alarm Manager state:"

    #@40
    move-object/from16 v0, p2

    #@42
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@45
    .line 570
    move-object/from16 v0, p0

    #@47
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@49
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@4c
    move-result v3

    #@4d
    if-gtz v3, :cond_59

    #@4f
    move-object/from16 v0, p0

    #@51
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@53
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@56
    move-result v3

    #@57
    if-lez v3, :cond_b9

    #@59
    .line 571
    :cond_59
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5c
    move-result-wide v7

    #@5d
    .line 572
    .local v7, now:J
    new-instance v20, Ljava/text/SimpleDateFormat;

    #@5f
    const-string v3, "yyyy-MM-dd HH:mm:ss"

    #@61
    move-object/from16 v0, v20

    #@63
    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@66
    .line 573
    .local v20, sdf:Ljava/text/SimpleDateFormat;
    const-string v3, " "

    #@68
    move-object/from16 v0, p2

    #@6a
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6d
    .line 574
    const-string v3, "  Realtime wakeup (now="

    #@6f
    move-object/from16 v0, p2

    #@71
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@74
    .line 575
    new-instance v3, Ljava/util/Date;

    #@76
    invoke-direct {v3, v7, v8}, Ljava/util/Date;-><init>(J)V

    #@79
    move-object/from16 v0, v20

    #@7b
    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@7e
    move-result-object v3

    #@7f
    move-object/from16 v0, p2

    #@81
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@84
    const-string v3, "):"

    #@86
    move-object/from16 v0, p2

    #@88
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8b
    .line 576
    move-object/from16 v0, p0

    #@8d
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@92
    move-result v3

    #@93
    if-lez v3, :cond_a2

    #@95
    .line 577
    move-object/from16 v0, p0

    #@97
    iget-object v4, v0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@99
    const-string v5, "  "

    #@9b
    const-string v6, "RTC_WAKEUP"

    #@9d
    move-object/from16 v3, p2

    #@9f
    invoke-static/range {v3 .. v8}, Lcom/android/server/AlarmManagerService;->dumpAlarmList(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V

    #@a2
    .line 579
    :cond_a2
    move-object/from16 v0, p0

    #@a4
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@a6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@a9
    move-result v3

    #@aa
    if-lez v3, :cond_b9

    #@ac
    .line 580
    move-object/from16 v0, p0

    #@ae
    iget-object v4, v0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@b0
    const-string v5, "  "

    #@b2
    const-string v6, "RTC"

    #@b4
    move-object/from16 v3, p2

    #@b6
    invoke-static/range {v3 .. v8}, Lcom/android/server/AlarmManagerService;->dumpAlarmList(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V

    #@b9
    .line 583
    .end local v7           #now:J
    .end local v20           #sdf:Ljava/text/SimpleDateFormat;
    :cond_b9
    move-object/from16 v0, p0

    #@bb
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@bd
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@c0
    move-result v3

    #@c1
    if-gtz v3, :cond_cd

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@c7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@ca
    move-result v3

    #@cb
    if-lez v3, :cond_119

    #@cd
    .line 584
    :cond_cd
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@d0
    move-result-wide v7

    #@d1
    .line 585
    .restart local v7       #now:J
    const-string v3, " "

    #@d3
    move-object/from16 v0, p2

    #@d5
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d8
    .line 586
    const-string v3, "  Elapsed realtime wakeup (now="

    #@da
    move-object/from16 v0, p2

    #@dc
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@df
    .line 587
    move-object/from16 v0, p2

    #@e1
    invoke-static {v7, v8, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@e4
    const-string v3, "):"

    #@e6
    move-object/from16 v0, p2

    #@e8
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@eb
    .line 588
    move-object/from16 v0, p0

    #@ed
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@ef
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@f2
    move-result v3

    #@f3
    if-lez v3, :cond_102

    #@f5
    .line 589
    move-object/from16 v0, p0

    #@f7
    iget-object v4, v0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@f9
    const-string v5, "  "

    #@fb
    const-string v6, "ELAPSED_WAKEUP"

    #@fd
    move-object/from16 v3, p2

    #@ff
    invoke-static/range {v3 .. v8}, Lcom/android/server/AlarmManagerService;->dumpAlarmList(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V

    #@102
    .line 591
    :cond_102
    move-object/from16 v0, p0

    #@104
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@106
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@109
    move-result v3

    #@10a
    if-lez v3, :cond_119

    #@10c
    .line 592
    move-object/from16 v0, p0

    #@10e
    iget-object v4, v0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@110
    const-string v5, "  "

    #@112
    const-string v6, "ELAPSED"

    #@114
    move-object/from16 v3, p2

    #@116
    invoke-static/range {v3 .. v8}, Lcom/android/server/AlarmManagerService;->dumpAlarmList(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V

    #@119
    .line 596
    .end local v7           #now:J
    :cond_119
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@11c
    .line 597
    const-string v3, "  Broadcast ref count: "

    #@11e
    move-object/from16 v0, p2

    #@120
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@123
    move-object/from16 v0, p0

    #@125
    iget v3, v0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@127
    move-object/from16 v0, p2

    #@129
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(I)V

    #@12c
    .line 598
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@12f
    .line 600
    move-object/from16 v0, p0

    #@131
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mLog:Lcom/android/internal/util/LocalLog;

    #@133
    const-string v4, "  Recent problems"

    #@135
    const-string v5, "    "

    #@137
    move-object/from16 v0, p2

    #@139
    invoke-virtual {v3, v0, v4, v5}, Lcom/android/internal/util/LocalLog;->dump(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;)Z

    #@13c
    move-result v3

    #@13d
    if-eqz v3, :cond_142

    #@13f
    .line 601
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@142
    .line 604
    :cond_142
    const/16 v3, 0xa

    #@144
    new-array v0, v3, [Lcom/android/server/AlarmManagerService$FilterStats;

    #@146
    move-object/from16 v22, v0

    #@148
    .line 605
    .local v22, topFilters:[Lcom/android/server/AlarmManagerService$FilterStats;
    new-instance v11, Lcom/android/server/AlarmManagerService$1;

    #@14a
    move-object/from16 v0, p0

    #@14c
    invoke-direct {v11, v0}, Lcom/android/server/AlarmManagerService$1;-><init>(Lcom/android/server/AlarmManagerService;)V

    #@14f
    .line 616
    .local v11, comparator:Ljava/util/Comparator;,"Ljava/util/Comparator<Lcom/android/server/AlarmManagerService$FilterStats;>;"
    const/16 v18, 0x0

    #@151
    .line 617
    .local v18, len:I
    move-object/from16 v0, p0

    #@153
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mBroadcastStats:Ljava/util/HashMap;

    #@155
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@158
    move-result-object v3

    #@159
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15c
    move-result-object v16

    #@15d
    :cond_15d
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    #@160
    move-result v3

    #@161
    if-eqz v3, :cond_1c7

    #@163
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@166
    move-result-object v9

    #@167
    check-cast v9, Ljava/util/Map$Entry;

    #@169
    .line 618
    .local v9, be:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/AlarmManagerService$BroadcastStats;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@16c
    move-result-object v10

    #@16d
    check-cast v10, Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@16f
    .line 620
    .local v10, bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    iget-object v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->filterStats:Ljava/util/HashMap;

    #@171
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@174
    move-result-object v3

    #@175
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@178
    move-result-object v17

    #@179
    .local v17, i$:Ljava/util/Iterator;
    :cond_179
    :goto_179
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    #@17c
    move-result v3

    #@17d
    if-eqz v3, :cond_15d

    #@17f
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@182
    move-result-object v13

    #@183
    check-cast v13, Ljava/util/Map$Entry;

    #@185
    .line 621
    .local v13, fe:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/util/Pair<Ljava/lang/String;Landroid/content/ComponentName;>;Lcom/android/server/AlarmManagerService$FilterStats;>;"
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@188
    move-result-object v14

    #@189
    check-cast v14, Lcom/android/server/AlarmManagerService$FilterStats;

    #@18b
    .line 622
    .local v14, fs:Lcom/android/server/AlarmManagerService$FilterStats;
    if-lez v18, :cond_1c4

    #@18d
    const/4 v3, 0x0

    #@18e
    move-object/from16 v0, v22

    #@190
    move/from16 v1, v18

    #@192
    invoke-static {v0, v3, v1, v14, v11}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;IILjava/lang/Object;Ljava/util/Comparator;)I

    #@195
    move-result v19

    #@196
    .line 624
    .local v19, pos:I
    :goto_196
    if-gez v19, :cond_19d

    #@198
    .line 625
    move/from16 v0, v19

    #@19a
    neg-int v3, v0

    #@19b
    add-int/lit8 v19, v3, -0x1

    #@19d
    .line 627
    :cond_19d
    move-object/from16 v0, v22

    #@19f
    array-length v3, v0

    #@1a0
    move/from16 v0, v19

    #@1a2
    if-ge v0, v3, :cond_179

    #@1a4
    .line 628
    move-object/from16 v0, v22

    #@1a6
    array-length v3, v0

    #@1a7
    sub-int v3, v3, v19

    #@1a9
    add-int/lit8 v12, v3, -0x1

    #@1ab
    .line 629
    .local v12, copylen:I
    if-lez v12, :cond_1b8

    #@1ad
    .line 630
    add-int/lit8 v3, v19, 0x1

    #@1af
    move-object/from16 v0, v22

    #@1b1
    move/from16 v1, v19

    #@1b3
    move-object/from16 v2, v22

    #@1b5
    invoke-static {v0, v1, v2, v3, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1b8
    .line 632
    :cond_1b8
    aput-object v14, v22, v19

    #@1ba
    .line 633
    move-object/from16 v0, v22

    #@1bc
    array-length v3, v0

    #@1bd
    move/from16 v0, v18

    #@1bf
    if-ge v0, v3, :cond_179

    #@1c1
    .line 634
    add-int/lit8 v18, v18, 0x1

    #@1c3
    goto :goto_179

    #@1c4
    .line 622
    .end local v12           #copylen:I
    .end local v19           #pos:I
    :cond_1c4
    const/16 v19, 0x0

    #@1c6
    goto :goto_196

    #@1c7
    .line 639
    .end local v9           #be:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/AlarmManagerService$BroadcastStats;>;"
    .end local v10           #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .end local v13           #fe:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/util/Pair<Ljava/lang/String;Landroid/content/ComponentName;>;Lcom/android/server/AlarmManagerService$FilterStats;>;"
    .end local v14           #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    .end local v17           #i$:Ljava/util/Iterator;
    :cond_1c7
    if-lez v18, :cond_261

    #@1c9
    .line 640
    const-string v3, "  Top Alarms:"

    #@1cb
    move-object/from16 v0, p2

    #@1cd
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d0
    .line 641
    const/4 v15, 0x0

    #@1d1
    .local v15, i:I
    :goto_1d1
    move/from16 v0, v18

    #@1d3
    if-ge v15, v0, :cond_261

    #@1d5
    .line 642
    aget-object v14, v22, v15

    #@1d7
    .line 643
    .restart local v14       #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    const-string v3, "    "

    #@1d9
    move-object/from16 v0, p2

    #@1db
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1de
    .line 644
    iget v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@1e0
    if-lez v3, :cond_1e9

    #@1e2
    const-string v3, "*ACTIVE* "

    #@1e4
    move-object/from16 v0, p2

    #@1e6
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e9
    .line 645
    :cond_1e9
    iget-wide v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->aggregateTime:J

    #@1eb
    move-object/from16 v0, p2

    #@1ed
    invoke-static {v3, v4, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@1f0
    .line 646
    const-string v3, " running, "

    #@1f2
    move-object/from16 v0, p2

    #@1f4
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f7
    iget v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->numWakeup:I

    #@1f9
    move-object/from16 v0, p2

    #@1fb
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@1fe
    .line 647
    const-string v3, " wakeups, "

    #@200
    move-object/from16 v0, p2

    #@202
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@205
    iget v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->count:I

    #@207
    move-object/from16 v0, p2

    #@209
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@20c
    .line 648
    const-string v3, " alarms: "

    #@20e
    move-object/from16 v0, p2

    #@210
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@213
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mBroadcastStats:Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@215
    iget-object v3, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->mPackageName:Ljava/lang/String;

    #@217
    move-object/from16 v0, p2

    #@219
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21c
    .line 649
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@21f
    .line 650
    const-string v3, "      "

    #@221
    move-object/from16 v0, p2

    #@223
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@226
    .line 651
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@228
    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@22a
    if-eqz v3, :cond_23e

    #@22c
    .line 652
    const-string v3, " act="

    #@22e
    move-object/from16 v0, p2

    #@230
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@233
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@235
    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@237
    check-cast v3, Ljava/lang/String;

    #@239
    move-object/from16 v0, p2

    #@23b
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23e
    .line 654
    :cond_23e
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@240
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@242
    if-eqz v3, :cond_25a

    #@244
    .line 655
    const-string v3, " cmp="

    #@246
    move-object/from16 v0, p2

    #@248
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24b
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@24d
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@24f
    check-cast v3, Landroid/content/ComponentName;

    #@251
    invoke-virtual {v3}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@254
    move-result-object v3

    #@255
    move-object/from16 v0, p2

    #@257
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@25a
    .line 657
    :cond_25a
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@25d
    .line 641
    add-int/lit8 v15, v15, 0x1

    #@25f
    goto/16 :goto_1d1

    #@261
    .line 661
    .end local v14           #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    .end local v15           #i:I
    :cond_261
    const-string v3, " "

    #@263
    move-object/from16 v0, p2

    #@265
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@268
    .line 662
    const-string v3, "  Alarm Stats:"

    #@26a
    move-object/from16 v0, p2

    #@26c
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@26f
    .line 663
    new-instance v21, Ljava/util/ArrayList;

    #@271
    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    #@274
    .line 664
    .local v21, tmpFilters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$FilterStats;>;"
    move-object/from16 v0, p0

    #@276
    iget-object v3, v0, Lcom/android/server/AlarmManagerService;->mBroadcastStats:Ljava/util/HashMap;

    #@278
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@27b
    move-result-object v3

    #@27c
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@27f
    move-result-object v16

    #@280
    :cond_280
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    #@283
    move-result v3

    #@284
    if-eqz v3, :cond_383

    #@286
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@289
    move-result-object v9

    #@28a
    check-cast v9, Ljava/util/Map$Entry;

    #@28c
    .line 665
    .restart local v9       #be:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/AlarmManagerService$BroadcastStats;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@28f
    move-result-object v10

    #@290
    check-cast v10, Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@292
    .line 666
    .restart local v10       #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    const-string v3, "  "

    #@294
    move-object/from16 v0, p2

    #@296
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@299
    .line 667
    iget v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@29b
    if-lez v3, :cond_2a4

    #@29d
    const-string v3, "*ACTIVE* "

    #@29f
    move-object/from16 v0, p2

    #@2a1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a4
    .line 668
    :cond_2a4
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2a7
    move-result-object v3

    #@2a8
    check-cast v3, Ljava/lang/String;

    #@2aa
    move-object/from16 v0, p2

    #@2ac
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2af
    .line 669
    const-string v3, " "

    #@2b1
    move-object/from16 v0, p2

    #@2b3
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b6
    iget-wide v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->aggregateTime:J

    #@2b8
    move-object/from16 v0, p2

    #@2ba
    invoke-static {v3, v4, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@2bd
    .line 670
    const-string v3, " running, "

    #@2bf
    move-object/from16 v0, p2

    #@2c1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c4
    iget v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->numWakeup:I

    #@2c6
    move-object/from16 v0, p2

    #@2c8
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@2cb
    .line 671
    const-string v3, " wakeups:"

    #@2cd
    move-object/from16 v0, p2

    #@2cf
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d2
    .line 672
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->clear()V

    #@2d5
    .line 674
    iget-object v3, v10, Lcom/android/server/AlarmManagerService$BroadcastStats;->filterStats:Ljava/util/HashMap;

    #@2d7
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@2da
    move-result-object v3

    #@2db
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2de
    move-result-object v17

    #@2df
    .restart local v17       #i$:Ljava/util/Iterator;
    :goto_2df
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    #@2e2
    move-result v3

    #@2e3
    if-eqz v3, :cond_2f8

    #@2e5
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e8
    move-result-object v13

    #@2e9
    check-cast v13, Ljava/util/Map$Entry;

    #@2eb
    .line 675
    .restart local v13       #fe:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/util/Pair<Ljava/lang/String;Landroid/content/ComponentName;>;Lcom/android/server/AlarmManagerService$FilterStats;>;"
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2ee
    move-result-object v3

    #@2ef
    move-object/from16 v0, v21

    #@2f1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f4
    goto :goto_2df

    #@2f5
    .line 695
    .end local v9           #be:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/AlarmManagerService$BroadcastStats;>;"
    .end local v10           #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .end local v11           #comparator:Ljava/util/Comparator;,"Ljava/util/Comparator<Lcom/android/server/AlarmManagerService$FilterStats;>;"
    .end local v13           #fe:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/util/Pair<Ljava/lang/String;Landroid/content/ComponentName;>;Lcom/android/server/AlarmManagerService$FilterStats;>;"
    .end local v17           #i$:Ljava/util/Iterator;
    .end local v18           #len:I
    .end local v21           #tmpFilters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$FilterStats;>;"
    .end local v22           #topFilters:[Lcom/android/server/AlarmManagerService$FilterStats;
    :catchall_2f5
    move-exception v3

    #@2f6
    monitor-exit v23
    :try_end_2f7
    .catchall {:try_start_3e .. :try_end_2f7} :catchall_2f5

    #@2f7
    throw v3

    #@2f8
    .line 677
    .restart local v9       #be:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/AlarmManagerService$BroadcastStats;>;"
    .restart local v10       #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .restart local v11       #comparator:Ljava/util/Comparator;,"Ljava/util/Comparator<Lcom/android/server/AlarmManagerService$FilterStats;>;"
    .restart local v17       #i$:Ljava/util/Iterator;
    .restart local v18       #len:I
    .restart local v21       #tmpFilters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$FilterStats;>;"
    .restart local v22       #topFilters:[Lcom/android/server/AlarmManagerService$FilterStats;
    :cond_2f8
    :try_start_2f8
    move-object/from16 v0, v21

    #@2fa
    invoke-static {v0, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@2fd
    .line 678
    const/4 v15, 0x0

    #@2fe
    .restart local v15       #i:I
    :goto_2fe
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    #@301
    move-result v3

    #@302
    if-ge v15, v3, :cond_280

    #@304
    .line 679
    move-object/from16 v0, v21

    #@306
    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@309
    move-result-object v14

    #@30a
    check-cast v14, Lcom/android/server/AlarmManagerService$FilterStats;

    #@30c
    .line 680
    .restart local v14       #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    const-string v3, "    "

    #@30e
    move-object/from16 v0, p2

    #@310
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@313
    .line 681
    iget v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@315
    if-lez v3, :cond_31e

    #@317
    const-string v3, "*ACTIVE* "

    #@319
    move-object/from16 v0, p2

    #@31b
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31e
    .line 682
    :cond_31e
    iget-wide v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->aggregateTime:J

    #@320
    move-object/from16 v0, p2

    #@322
    invoke-static {v3, v4, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@325
    .line 683
    const-string v3, " "

    #@327
    move-object/from16 v0, p2

    #@329
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32c
    iget v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->numWakeup:I

    #@32e
    move-object/from16 v0, p2

    #@330
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@333
    .line 684
    const-string v3, " wakes "

    #@335
    move-object/from16 v0, p2

    #@337
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33a
    iget v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->count:I

    #@33c
    move-object/from16 v0, p2

    #@33e
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@341
    .line 685
    const-string v3, " alarms:"

    #@343
    move-object/from16 v0, p2

    #@345
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@348
    .line 686
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@34a
    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@34c
    if-eqz v3, :cond_360

    #@34e
    .line 687
    const-string v3, " act="

    #@350
    move-object/from16 v0, p2

    #@352
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@355
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@357
    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@359
    check-cast v3, Ljava/lang/String;

    #@35b
    move-object/from16 v0, p2

    #@35d
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@360
    .line 689
    :cond_360
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@362
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@364
    if-eqz v3, :cond_37c

    #@366
    .line 690
    const-string v3, " cmp="

    #@368
    move-object/from16 v0, p2

    #@36a
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36d
    iget-object v3, v14, Lcom/android/server/AlarmManagerService$FilterStats;->mTarget:Landroid/util/Pair;

    #@36f
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@371
    check-cast v3, Landroid/content/ComponentName;

    #@373
    invoke-virtual {v3}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@376
    move-result-object v3

    #@377
    move-object/from16 v0, p2

    #@379
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37c
    .line 692
    :cond_37c
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@37f
    .line 678
    add-int/lit8 v15, v15, 0x1

    #@381
    goto/16 :goto_2fe

    #@383
    .line 695
    .end local v9           #be:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/AlarmManagerService$BroadcastStats;>;"
    .end local v10           #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .end local v14           #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    .end local v15           #i:I
    .end local v17           #i$:Ljava/util/Iterator;
    :cond_383
    monitor-exit v23
    :try_end_384
    .catchall {:try_start_2f8 .. :try_end_384} :catchall_2f5

    #@384
    goto/16 :goto_36
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 208
    :try_start_0
    iget v0, p0, Lcom/android/server/AlarmManagerService;->mDescriptor:I

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/AlarmManagerService;->close(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 210
    invoke-super {p0}, Landroid/app/IAlarmManager$Stub;->finalize()V

    #@8
    .line 212
    return-void

    #@9
    .line 210
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Landroid/app/IAlarmManager$Stub;->finalize()V

    #@d
    throw v0
.end method

.method public lookForPackageLocked(Ljava/lang/String;)Z
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 461
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->lookForPackageLocked(Ljava/util/ArrayList;Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_20

    #@8
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@a
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->lookForPackageLocked(Ljava/util/ArrayList;Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_20

    #@10
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@12
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->lookForPackageLocked(Ljava/util/ArrayList;Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_20

    #@18
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@1a
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->lookForPackageLocked(Ljava/util/ArrayList;Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public remove(Landroid/app/PendingIntent;)V
    .registers 4
    .parameter "operation"

    #@0
    .prologue
    .line 336
    if-nez p1, :cond_3

    #@2
    .line 342
    :goto_2
    return-void

    #@3
    .line 339
    :cond_3
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 340
    :try_start_6
    invoke-virtual {p0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Landroid/app/PendingIntent;)V

    #@9
    .line 341
    monitor-exit v1

    #@a
    goto :goto_2

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_6 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public removeLocked(Landroid/app/PendingIntent;)V
    .registers 3
    .parameter "operation"

    #@0
    .prologue
    .line 345
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Landroid/app/PendingIntent;)V

    #@5
    .line 346
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@7
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Landroid/app/PendingIntent;)V

    #@a
    .line 347
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@c
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Landroid/app/PendingIntent;)V

    #@f
    .line 348
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@11
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Landroid/app/PendingIntent;)V

    #@14
    .line 349
    return-void
.end method

.method public removeLocked(Ljava/lang/String;)V
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Ljava/lang/String;)V

    #@5
    .line 415
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@7
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Ljava/lang/String;)V

    #@a
    .line 416
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@c
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Ljava/lang/String;)V

    #@f
    .line 417
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@11
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/util/ArrayList;Ljava/lang/String;)V

    #@14
    .line 418
    return-void
.end method

.method public removeUserLocked(I)V
    .registers 3
    .parameter "userHandle"

    #@0
    .prologue
    .line 438
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcWakeupAlarms:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeUserLocked(Ljava/util/ArrayList;I)V

    #@5
    .line 439
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mRtcAlarms:Ljava/util/ArrayList;

    #@7
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeUserLocked(Ljava/util/ArrayList;I)V

    #@a
    .line 440
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeWakeupAlarms:Ljava/util/ArrayList;

    #@c
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeUserLocked(Ljava/util/ArrayList;I)V

    #@f
    .line 441
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mElapsedRealtimeAlarms:Ljava/util/ArrayList;

    #@11
    invoke-direct {p0, v0, p1}, Lcom/android/server/AlarmManagerService;->removeUserLocked(Ljava/util/ArrayList;I)V

    #@14
    .line 442
    return-void
.end method

.method public set(IJLandroid/app/PendingIntent;)V
    .registers 12
    .parameter "type"
    .parameter "triggerAtTime"
    .parameter "operation"

    #@0
    .prologue
    .line 215
    const-wide/16 v4, 0x0

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move-wide v2, p2

    #@5
    move-object v6, p4

    #@6
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/AlarmManagerService;->setRepeating(IJJLandroid/app/PendingIntent;)V

    #@9
    .line 216
    return-void
.end method

.method public setInexactRepeating(IJJLandroid/app/PendingIntent;)V
    .registers 21
    .parameter "type"
    .parameter "triggerAtTime"
    .parameter "interval"
    .parameter "operation"

    #@0
    .prologue
    .line 245
    if-nez p6, :cond_a

    #@2
    .line 246
    const-string v2, "AlarmManager"

    #@4
    const-string v3, "setInexactRepeating ignored because there is no intent"

    #@6
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 284
    :goto_9
    return-void

    #@a
    .line 250
    :cond_a
    const-wide/16 v2, 0x0

    #@c
    cmp-long v2, p4, v2

    #@e
    if-gtz v2, :cond_31

    #@10
    .line 251
    const-string v2, "AlarmManager"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "setInexactRepeating ignored because interval "

    #@19
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    move-wide/from16 v0, p4

    #@1f
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v6, " is invalid"

    #@25
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_9

    #@31
    .line 257
    :cond_31
    const-wide/32 v2, 0xdbba0

    #@34
    rem-long v2, p4, v2

    #@36
    const-wide/16 v6, 0x0

    #@38
    cmp-long v2, v2, v6

    #@3a
    if-eqz v2, :cond_40

    #@3c
    .line 259
    invoke-virtual/range {p0 .. p6}, Lcom/android/server/AlarmManagerService;->setRepeating(IJJLandroid/app/PendingIntent;)V

    #@3f
    goto :goto_9

    #@40
    .line 265
    :cond_40
    const/4 v2, 0x1

    #@41
    if-eq p1, v2, :cond_45

    #@43
    if-nez p1, :cond_70

    #@45
    :cond_45
    const/4 v9, 0x1

    #@46
    .line 266
    .local v9, isRtc:Z
    :goto_46
    if-eqz v9, :cond_72

    #@48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4b
    move-result-wide v2

    #@4c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4f
    move-result-wide v6

    #@50
    sub-long v12, v2, v6

    #@52
    .line 273
    .local v12, skew:J
    :goto_52
    sub-long v2, p2, v12

    #@54
    const-wide/32 v6, 0xdbba0

    #@57
    rem-long v10, v2, v6

    #@59
    .line 274
    .local v10, offset:J
    const-wide/16 v2, 0x0

    #@5b
    cmp-long v2, v10, v2

    #@5d
    if-eqz v2, :cond_75

    #@5f
    .line 275
    sub-long v2, p2, v10

    #@61
    const-wide/32 v6, 0xdbba0

    #@64
    add-long v4, v2, v6

    #@66
    .local v4, adjustedTriggerTime:J
    :goto_66
    move-object v2, p0

    #@67
    move v3, p1

    #@68
    move-wide/from16 v6, p4

    #@6a
    move-object/from16 v8, p6

    #@6c
    .line 283
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/AlarmManagerService;->setRepeating(IJJLandroid/app/PendingIntent;)V

    #@6f
    goto :goto_9

    #@70
    .line 265
    .end local v4           #adjustedTriggerTime:J
    .end local v9           #isRtc:Z
    .end local v10           #offset:J
    .end local v12           #skew:J
    :cond_70
    const/4 v9, 0x0

    #@71
    goto :goto_46

    #@72
    .line 266
    .restart local v9       #isRtc:Z
    :cond_72
    const-wide/16 v12, 0x0

    #@74
    goto :goto_52

    #@75
    .line 277
    .restart local v10       #offset:J
    .restart local v12       #skew:J
    :cond_75
    move-wide/from16 v4, p2

    #@77
    .restart local v4       #adjustedTriggerTime:J
    goto :goto_66
.end method

.method public setRepeating(IJJLandroid/app/PendingIntent;)V
    .registers 11
    .parameter "type"
    .parameter "triggerAtTime"
    .parameter "interval"
    .parameter "operation"

    #@0
    .prologue
    .line 220
    if-nez p6, :cond_a

    #@2
    .line 221
    const-string v2, "AlarmManager"

    #@4
    const-string v3, "set/setRepeating ignored because there is no intent"

    #@6
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 241
    :goto_9
    return-void

    #@a
    .line 224
    :cond_a
    iget-object v3, p0, Lcom/android/server/AlarmManagerService;->mLock:Ljava/lang/Object;

    #@c
    monitor-enter v3

    #@d
    .line 225
    :try_start_d
    new-instance v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@f
    invoke-direct {v0}, Lcom/android/server/AlarmManagerService$Alarm;-><init>()V

    #@12
    .line 226
    .local v0, alarm:Lcom/android/server/AlarmManagerService$Alarm;
    iput p1, v0, Lcom/android/server/AlarmManagerService$Alarm;->type:I

    #@14
    .line 227
    iput-wide p2, v0, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@16
    .line 228
    iput-wide p4, v0, Lcom/android/server/AlarmManagerService$Alarm;->repeatInterval:J

    #@18
    .line 229
    iput-object p6, v0, Lcom/android/server/AlarmManagerService$Alarm;->operation:Landroid/app/PendingIntent;

    #@1a
    .line 232
    invoke-virtual {p0, p6}, Lcom/android/server/AlarmManagerService;->removeLocked(Landroid/app/PendingIntent;)V

    #@1d
    .line 236
    invoke-direct {p0, v0}, Lcom/android/server/AlarmManagerService;->addAlarmLocked(Lcom/android/server/AlarmManagerService$Alarm;)I

    #@20
    move-result v1

    #@21
    .line 237
    .local v1, index:I
    if-nez v1, :cond_26

    #@23
    .line 238
    invoke-direct {p0, v0}, Lcom/android/server/AlarmManagerService;->setLocked(Lcom/android/server/AlarmManagerService$Alarm;)V

    #@26
    .line 240
    :cond_26
    monitor-exit v3

    #@27
    goto :goto_9

    #@28
    .end local v0           #alarm:Lcom/android/server/AlarmManagerService$Alarm;
    .end local v1           #index:I
    :catchall_28
    move-exception v2

    #@29
    monitor-exit v3
    :try_end_2a
    .catchall {:try_start_d .. :try_end_2a} :catchall_28

    #@2a
    throw v2
.end method

.method public setTime(J)V
    .registers 6
    .parameter "millis"

    #@0
    .prologue
    .line 287
    iget-object v0, p0, Lcom/android/server/AlarmManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.SET_TIME"

    #@4
    const-string v2, "setTime"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 291
    invoke-static {p1, p2}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    #@c
    .line 292
    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .registers 12
    .parameter "tz"

    #@0
    .prologue
    .line 295
    iget-object v7, p0, Lcom/android/server/AlarmManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v8, "android.permission.SET_TIME_ZONE"

    #@4
    const-string v9, "setTimeZone"

    #@6
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 299
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v3

    #@d
    .line 301
    .local v3, oldId:J
    :try_start_d
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_76

    #@10
    move-result v7

    #@11
    if-eqz v7, :cond_17

    #@13
    .line 331
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@16
    .line 333
    :goto_16
    return-void

    #@17
    .line 302
    :cond_17
    :try_start_17
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@1a
    move-result-object v6

    #@1b
    .line 305
    .local v6, zone:Ljava/util/TimeZone;
    const/4 v5, 0x0

    #@1c
    .line 306
    .local v5, timeZoneWasChanged:Z
    monitor-enter p0
    :try_end_1d
    .catchall {:try_start_17 .. :try_end_1d} :catchall_76

    #@1d
    .line 307
    :try_start_1d
    const-string v7, "persist.sys.timezone"

    #@1f
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 308
    .local v0, current:Ljava/lang/String;
    if-eqz v0, :cond_2f

    #@25
    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v7

    #@2d
    if-nez v7, :cond_39

    #@2f
    .line 312
    :cond_2f
    const/4 v5, 0x1

    #@30
    .line 313
    const-string v7, "persist.sys.timezone"

    #@32
    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@35
    move-result-object v8

    #@36
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 318
    :cond_39
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3c
    move-result-wide v7

    #@3d
    invoke-virtual {v6, v7, v8}, Ljava/util/TimeZone;->getOffset(J)I

    #@40
    move-result v1

    #@41
    .line 319
    .local v1, gmtOffset:I
    iget v7, p0, Lcom/android/server/AlarmManagerService;->mDescriptor:I

    #@43
    const v8, 0xea60

    #@46
    div-int v8, v1, v8

    #@48
    neg-int v8, v8

    #@49
    invoke-direct {p0, v7, v8}, Lcom/android/server/AlarmManagerService;->setKernelTimezone(II)I

    #@4c
    .line 320
    monitor-exit p0
    :try_end_4d
    .catchall {:try_start_1d .. :try_end_4d} :catchall_73

    #@4d
    .line 322
    const/4 v7, 0x0

    #@4e
    :try_start_4e
    invoke-static {v7}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    #@51
    .line 324
    if-eqz v5, :cond_6f

    #@53
    .line 325
    new-instance v2, Landroid/content/Intent;

    #@55
    const-string v7, "android.intent.action.TIMEZONE_CHANGED"

    #@57
    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5a
    .line 326
    .local v2, intent:Landroid/content/Intent;
    const/high16 v7, 0x2000

    #@5c
    invoke-virtual {v2, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5f
    .line 327
    const-string v7, "time-zone"

    #@61
    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@64
    move-result-object v8

    #@65
    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@68
    .line 328
    iget-object v7, p0, Lcom/android/server/AlarmManagerService;->mContext:Landroid/content/Context;

    #@6a
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@6c
    invoke-virtual {v7, v2, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_6f
    .catchall {:try_start_4e .. :try_end_6f} :catchall_76

    #@6f
    .line 331
    .end local v2           #intent:Landroid/content/Intent;
    :cond_6f
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@72
    goto :goto_16

    #@73
    .line 320
    .end local v0           #current:Ljava/lang/String;
    .end local v1           #gmtOffset:I
    :catchall_73
    move-exception v7

    #@74
    :try_start_74
    monitor-exit p0
    :try_end_75
    .catchall {:try_start_74 .. :try_end_75} :catchall_73

    #@75
    :try_start_75
    throw v7
    :try_end_76
    .catchall {:try_start_75 .. :try_end_76} :catchall_76

    #@76
    .line 331
    .end local v5           #timeZoneWasChanged:Z
    .end local v6           #zone:Ljava/util/TimeZone;
    :catchall_76
    move-exception v7

    #@77
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7a
    throw v7
.end method

.method setWakelockWorkSource(Landroid/app/PendingIntent;)V
    .registers 5
    .parameter "pi"

    #@0
    .prologue
    .line 962
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {p1}, Landroid/app/PendingIntent;->getTarget()Landroid/content/IIntentSender;

    #@7
    move-result-object v2

    #@8
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getUidForIntentSender(Landroid/content/IIntentSender;)I

    #@b
    move-result v0

    #@c
    .line 964
    .local v0, uid:I
    if-ltz v0, :cond_19

    #@e
    .line 965
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@10
    new-instance v2, Landroid/os/WorkSource;

    #@12
    invoke-direct {v2, v0}, Landroid/os/WorkSource;-><init>(I)V

    #@15
    invoke-virtual {v1, v2}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    #@18
    .line 972
    .end local v0           #uid:I
    :goto_18
    return-void

    #@19
    .line 969
    .restart local v0       #uid:I
    :cond_19
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1b
    const/4 v2, 0x0

    #@1c
    invoke-virtual {v1, v2}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1f} :catch_20

    #@1f
    goto :goto_18

    #@20
    .line 970
    .end local v0           #uid:I
    :catch_20
    move-exception v1

    #@21
    goto :goto_18
.end method

.method public timeToNextAlarm()J
    .registers 10

    #@0
    .prologue
    .line 515
    const-wide v3, 0x7fffffffffffffffL

    #@5
    .line 516
    .local v3, nextAlarm:J
    iget-object v6, p0, Lcom/android/server/AlarmManagerService;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v6

    #@8
    .line 517
    const/4 v2, 0x0

    #@9
    .line 518
    .local v2, i:I
    :goto_9
    const/4 v5, 0x3

    #@a
    if-gt v2, v5, :cond_28

    #@c
    .line 519
    :try_start_c
    invoke-direct {p0, v2}, Lcom/android/server/AlarmManagerService;->getAlarmList(I)Ljava/util/ArrayList;

    #@f
    move-result-object v1

    #@10
    .line 520
    .local v1, alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v5

    #@14
    if-lez v5, :cond_25

    #@16
    .line 521
    const/4 v5, 0x0

    #@17
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Lcom/android/server/AlarmManagerService$Alarm;

    #@1d
    .line 522
    .local v0, a:Lcom/android/server/AlarmManagerService$Alarm;
    iget-wide v7, v0, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@1f
    cmp-long v5, v7, v3

    #@21
    if-gez v5, :cond_25

    #@23
    .line 523
    iget-wide v3, v0, Lcom/android/server/AlarmManagerService$Alarm;->when:J

    #@25
    .line 518
    .end local v0           #a:Lcom/android/server/AlarmManagerService$Alarm;
    :cond_25
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_9

    #@28
    .line 527
    .end local v1           #alarmList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/AlarmManagerService$Alarm;>;"
    :cond_28
    monitor-exit v6

    #@29
    .line 528
    return-wide v3

    #@2a
    .line 527
    :catchall_2a
    move-exception v5

    #@2b
    monitor-exit v6
    :try_end_2c
    .catchall {:try_start_c .. :try_end_2c} :catchall_2a

    #@2c
    throw v5
.end method

.method updateBlockedUids(IZ)V
    .registers 7
    .parameter "uid"
    .parameter "isBlocked"

    #@0
    .prologue
    .line 373
    iget-object v2, p0, Lcom/android/server/AlarmManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 374
    if-eqz p2, :cond_45

    #@5
    .line 375
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    :try_start_6
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mTriggeredUids:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v1

    #@c
    if-ge v0, v1, :cond_5b

    #@e
    .line 376
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mTriggeredUids:Ljava/util/ArrayList;

    #@10
    new-instance v3, Ljava/lang/Integer;

    #@12
    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    #@15
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_5b

    #@1b
    .line 381
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mTriggeredUids:Ljava/util/ArrayList;

    #@1d
    new-instance v3, Ljava/lang/Integer;

    #@1f
    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    #@22
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@25
    .line 382
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@27
    new-instance v3, Ljava/lang/Integer;

    #@29
    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    #@2c
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 383
    iget v1, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@31
    if-lez v1, :cond_42

    #@33
    .line 384
    iget v1, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@35
    add-int/lit8 v1, v1, -0x1

    #@37
    iput v1, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@39
    .line 385
    iget v1, p0, Lcom/android/server/AlarmManagerService;->mBroadcastRefCount:I

    #@3b
    if-nez v1, :cond_42

    #@3d
    .line 389
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3f
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@42
    .line 375
    :cond_42
    add-int/lit8 v0, v0, 0x1

    #@44
    goto :goto_6

    #@45
    .line 403
    .end local v0           #i:I
    :cond_45
    const/4 v0, 0x0

    #@46
    .restart local v0       #i:I
    :goto_46
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@48
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@4b
    move-result v1

    #@4c
    if-ge v0, v1, :cond_5b

    #@4e
    .line 404
    iget-object v1, p0, Lcom/android/server/AlarmManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@50
    new-instance v3, Ljava/lang/Integer;

    #@52
    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    #@55
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@58
    move-result v1

    #@59
    if-nez v1, :cond_5d

    #@5b
    .line 410
    :cond_5b
    monitor-exit v2

    #@5c
    .line 411
    return-void

    #@5d
    .line 403
    :cond_5d
    add-int/lit8 v0, v0, 0x1

    #@5f
    goto :goto_46

    #@60
    .line 410
    :catchall_60
    move-exception v1

    #@61
    monitor-exit v2
    :try_end_62
    .catchall {:try_start_6 .. :try_end_62} :catchall_60

    #@62
    throw v1
.end method
