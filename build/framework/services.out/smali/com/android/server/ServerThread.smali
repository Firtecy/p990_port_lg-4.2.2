.class Lcom/android/server/ServerThread;
.super Ljava/lang/Thread;
.source "SystemServer.java"


# static fields
.field private static final ENCRYPTED_STATE:Ljava/lang/String; = "1"

.field private static final ENCRYPTING_STATE:Ljava/lang/String; = "trigger_restart_min_framework"

.field private static final TAG:Ljava/lang/String; = "SystemServer"


# instance fields
.field mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@3
    return-void
.end method

.method static final startSystemUi(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 1325
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 1326
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    #@7
    const-string v2, "com.android.systemui"

    #@9
    const-string v3, "com.android.systemui.SystemUIService"

    #@b
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@11
    .line 1329
    sget-object v1, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@13
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    #@16
    .line 1330
    return-void
.end method


# virtual methods
.method reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 6
    .parameter "msg"
    .parameter "e"

    #@0
    .prologue
    .line 126
    const-string v0, "SystemServer"

    #@2
    const-string v1, "***********************************************"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 127
    const-string v0, "SystemServer"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "BOOT FAILURE "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1, p2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    .line 128
    return-void
.end method

.method public run()V
    .registers 167

    #@0
    .prologue
    .line 132
    const/16 v7, 0xbc2

    #@2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5
    move-result-wide v11

    #@6
    invoke-static {v7, v11, v12}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@9
    .line 135
    invoke-static {}, Landroid/os/Looper;->prepareMainLooper()V

    #@c
    .line 137
    const/4 v7, -0x2

    #@d
    invoke-static {v7}, Landroid/os/Process;->setThreadPriority(I)V

    #@10
    .line 140
    const/4 v7, 0x1

    #@11
    invoke-static {v7}, Lcom/android/internal/os/BinderInternal;->disableBackgroundScheduling(Z)V

    #@14
    .line 141
    const/4 v7, 0x0

    #@15
    invoke-static {v7}, Landroid/os/Process;->setCanSelfBackground(Z)V

    #@18
    .line 145
    const-string v7, "sys.shutdown.requested"

    #@1a
    const-string v9, ""

    #@1c
    invoke-static {v7, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v131

    #@20
    .line 147
    .local v131, shutdownAction:Ljava/lang/String;
    if-eqz v131, :cond_4e

    #@22
    invoke-virtual/range {v131 .. v131}, Ljava/lang/String;->length()I

    #@25
    move-result v7

    #@26
    if-lez v7, :cond_4e

    #@28
    .line 148
    const/4 v7, 0x0

    #@29
    move-object/from16 v0, v131

    #@2b
    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    #@2e
    move-result v7

    #@2f
    const/16 v9, 0x31

    #@31
    if-ne v7, v9, :cond_9a0

    #@33
    const/16 v123, 0x1

    #@35
    .line 151
    .local v123, reboot:Z
    :goto_35
    invoke-virtual/range {v131 .. v131}, Ljava/lang/String;->length()I

    #@38
    move-result v7

    #@39
    const/4 v9, 0x1

    #@3a
    if-le v7, v9, :cond_9a4

    #@3c
    .line 152
    const/4 v7, 0x1

    #@3d
    invoke-virtual/range {v131 .. v131}, Ljava/lang/String;->length()I

    #@40
    move-result v9

    #@41
    move-object/from16 v0, v131

    #@43
    invoke-virtual {v0, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v122

    #@47
    .line 157
    .local v122, reason:Ljava/lang/String;
    :goto_47
    move/from16 v0, v123

    #@49
    move-object/from16 v1, v122

    #@4b
    invoke-static {v0, v1}, Lcom/android/server/power/ShutdownThread;->rebootOrShutdown(ZLjava/lang/String;)V

    #@4e
    .line 165
    .end local v122           #reason:Ljava/lang/String;
    .end local v123           #reboot:Z
    :cond_4e
    const/16 v92, 0x0

    #@50
    .line 167
    .local v92, factoryTest:I
    const-string v7, "1"

    #@52
    const-string v9, "ro.config.headless"

    #@54
    const-string v11, "0"

    #@56
    invoke-static {v9, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@59
    move-result-object v9

    #@5a
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v33

    #@5e
    .line 169
    .local v33, headless:Z
    const/16 v98, 0x0

    #@60
    .line 170
    .local v98, installer:Lcom/android/server/pm/Installer;
    const/16 v62, 0x0

    #@62
    .line 171
    .local v62, accountManager:Landroid/accounts/AccountManagerService;
    const/16 v77, 0x0

    #@64
    .line 172
    .local v77, contentService:Landroid/content/ContentService;
    const/16 v100, 0x0

    #@66
    .line 173
    .local v100, lights:Lcom/android/server/LightsService;
    const/16 v117, 0x0

    #@68
    .line 174
    .local v117, power:Lcom/android/server/power/PowerManagerService;
    const/16 v85, 0x0

    #@6a
    .line 175
    .local v85, display:Lcom/android/server/display/DisplayManagerService;
    const/16 v69, 0x0

    #@6c
    .line 176
    .local v69, battery:Lcom/android/server/BatteryService;
    const/16 v150, 0x0

    #@6e
    .line 177
    .local v150, vibrator:Lcom/android/server/VibratorService;
    const/16 v64, 0x0

    #@70
    .line 178
    .local v64, alarm:Lcom/android/server/AlarmManagerService;
    const/16 v106, 0x0

    #@72
    .line 179
    .local v106, mountService:Lcom/android/server/MountService;
    const/16 v30, 0x0

    #@74
    .line 180
    .local v30, networkManagement:Lcom/android/server/NetworkManagementService;
    const/16 v29, 0x0

    #@76
    .line 181
    .local v29, networkStats:Lcom/android/server/net/NetworkStatsService;
    const/16 v110, 0x0

    #@78
    .line 182
    .local v110, networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    const/16 v75, 0x0

    #@7a
    .line 183
    .local v75, connectivity:Lcom/android/server/ConnectivityService;
    const/16 v118, 0x0

    #@7c
    .line 184
    .local v118, qcCon:Ljava/lang/Object;
    const/16 v162, 0x0

    #@7e
    .line 185
    .local v162, wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    const/16 v156, 0x0

    #@80
    .line 186
    .local v156, wifi:Lcom/android/server/WifiService;
    const/16 v130, 0x0

    #@82
    .line 187
    .local v130, serviceDiscovery:Lcom/android/server/NsdService;
    const/16 v116, 0x0

    #@84
    .line 188
    .local v116, pm:Landroid/content/pm/IPackageManager;
    const/4 v5, 0x0

    #@85
    .line 189
    .local v5, context:Landroid/content/Context;
    const/16 v164, 0x0

    #@87
    .line 190
    .local v164, wm:Lcom/android/server/wm/WindowManagerService;
    const/16 v70, 0x0

    #@89
    .line 191
    .local v70, bluetooth:Lcom/android/server/BluetoothManagerService;
    const/16 v86, 0x0

    #@8b
    .line 192
    .local v86, dock:Lcom/android/server/DockObserver;
    const/16 v126, 0x0

    #@8d
    .line 193
    .local v126, regulatory:Lcom/android/server/RegulatoryObserver;
    const/16 v147, 0x0

    #@8f
    .line 194
    .local v147, usb:Lcom/android/server/usb/UsbService;
    const/16 v128, 0x0

    #@91
    .line 195
    .local v128, serial:Lcom/android/server/SerialService;
    const/16 v142, 0x0

    #@93
    .line 196
    .local v142, twilight:Lcom/android/server/TwilightService;
    const/16 v145, 0x0

    #@95
    .line 197
    .local v145, uiMode:Lcom/android/server/UiModeManagerService;
    const/16 v124, 0x0

    #@97
    .line 198
    .local v124, recognition:Lcom/android/server/RecognitionManagerService;
    const/16 v137, 0x0

    #@99
    .line 199
    .local v137, throttle:Lcom/android/server/ThrottleService;
    const/16 v112, 0x0

    #@9b
    .line 200
    .local v112, networkTimeUpdater:Lcom/android/server/NetworkTimeUpdateService;
    const/16 v72, 0x0

    #@9d
    .line 201
    .local v72, commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    const/16 v97, 0x0

    #@9f
    .line 202
    .local v97, inputManager:Lcom/android/server/input/InputManagerService;
    const/16 v82, 0x0

    #@a1
    .line 203
    .local v82, deviceManager:Lcom/android/server/DeviceManager3LMService;
    const/16 v135, 0x0

    #@a3
    .line 204
    .local v135, telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    const/16 v108, 0x0

    #@a5
    .line 211
    .local v108, msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    new-instance v144, Landroid/os/HandlerThread;

    #@a7
    const-string v7, "UI"

    #@a9
    move-object/from16 v0, v144

    #@ab
    invoke-direct {v0, v7}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@ae
    .line 212
    .local v144, uiHandlerThread:Landroid/os/HandlerThread;
    invoke-virtual/range {v144 .. v144}, Landroid/os/HandlerThread;->start()V

    #@b1
    .line 213
    new-instance v20, Landroid/os/Handler;

    #@b3
    invoke-virtual/range {v144 .. v144}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@b6
    move-result-object v7

    #@b7
    move-object/from16 v0, v20

    #@b9
    invoke-direct {v0, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@bc
    .line 214
    .local v20, uiHandler:Landroid/os/Handler;
    new-instance v7, Lcom/android/server/ServerThread$1;

    #@be
    move-object/from16 v0, p0

    #@c0
    invoke-direct {v7, v0}, Lcom/android/server/ServerThread$1;-><init>(Lcom/android/server/ServerThread;)V

    #@c3
    move-object/from16 v0, v20

    #@c5
    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@c8
    .line 231
    new-instance v165, Landroid/os/HandlerThread;

    #@ca
    const-string v7, "WindowManager"

    #@cc
    move-object/from16 v0, v165

    #@ce
    invoke-direct {v0, v7}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@d1
    .line 232
    .local v165, wmHandlerThread:Landroid/os/HandlerThread;
    invoke-virtual/range {v165 .. v165}, Landroid/os/HandlerThread;->start()V

    #@d4
    .line 233
    new-instance v21, Landroid/os/Handler;

    #@d6
    invoke-virtual/range {v165 .. v165}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@d9
    move-result-object v7

    #@da
    move-object/from16 v0, v21

    #@dc
    invoke-direct {v0, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@df
    .line 234
    .local v21, wmHandler:Landroid/os/Handler;
    new-instance v7, Lcom/android/server/ServerThread$2;

    #@e1
    move-object/from16 v0, p0

    #@e3
    invoke-direct {v7, v0}, Lcom/android/server/ServerThread$2;-><init>(Lcom/android/server/ServerThread;)V

    #@e6
    move-object/from16 v0, v21

    #@e8
    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@eb
    .line 251
    const/16 v24, 0x0

    #@ed
    .line 256
    .local v24, onlyCore:Z
    :try_start_ed
    const-string v7, "SystemServer"

    #@ef
    const-string v9, "Waiting for installd to be ready."

    #@f1
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 257
    new-instance v99, Lcom/android/server/pm/Installer;

    #@f6
    invoke-direct/range {v99 .. v99}, Lcom/android/server/pm/Installer;-><init>()V
    :try_end_f9
    .catch Ljava/lang/RuntimeException; {:try_start_ed .. :try_end_f9} :catch_db0

    #@f9
    .line 258
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .local v99, installer:Lcom/android/server/pm/Installer;
    :try_start_f9
    invoke-virtual/range {v99 .. v99}, Lcom/android/server/pm/Installer;->ping()Z

    #@fc
    .line 260
    const-string v7, "SystemServer"

    #@fe
    const-string v9, "Entropy Mixer"

    #@100
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@103
    .line 261
    const-string v7, "entropy"

    #@105
    new-instance v9, Lcom/android/server/EntropyMixer;

    #@107
    invoke-direct {v9}, Lcom/android/server/EntropyMixer;-><init>()V

    #@10a
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@10d
    .line 263
    const-string v7, "SystemServer"

    #@10f
    const-string v9, "Power Manager"

    #@111
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 264
    new-instance v4, Lcom/android/server/power/PowerManagerService;

    #@116
    invoke-direct {v4}, Lcom/android/server/power/PowerManagerService;-><init>()V
    :try_end_119
    .catch Ljava/lang/RuntimeException; {:try_start_f9 .. :try_end_119} :catch_dbf

    #@119
    .line 265
    .end local v117           #power:Lcom/android/server/power/PowerManagerService;
    .local v4, power:Lcom/android/server/power/PowerManagerService;
    :try_start_119
    const-string v7, "power"

    #@11b
    invoke-static {v7, v4}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@11e
    .line 267
    sget-boolean v7, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@120
    if-eqz v7, :cond_134

    #@122
    .line 268
    const-string v7, "SystemServer"

    #@124
    const-string v9, "DeviceManager3LM"

    #@126
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@129
    .line 269
    invoke-static {}, Lcom/android/server/DeviceManager3LMService;->getInstance()Lcom/android/server/DeviceManager3LMService;

    #@12c
    move-result-object v82

    #@12d
    .line 270
    const-string v7, "DeviceManager3LM"

    #@12f
    move-object/from16 v0, v82

    #@131
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@134
    .line 273
    :cond_134
    const-string v7, "SystemServer"

    #@136
    const-string v9, "Activity Manager"

    #@138
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13b
    .line 274
    invoke-static/range {v92 .. v92}, Lcom/android/server/am/ActivityManagerService;->main(I)Landroid/content/Context;

    #@13e
    move-result-object v5

    #@13f
    .line 276
    const-string v7, "SystemServer"

    #@141
    const-string v9, "Display Manager"

    #@143
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 277
    new-instance v10, Lcom/android/server/display/DisplayManagerService;

    #@148
    move-object/from16 v0, v21

    #@14a
    move-object/from16 v1, v20

    #@14c
    invoke-direct {v10, v5, v0, v1}, Lcom/android/server/display/DisplayManagerService;-><init>(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;)V
    :try_end_14f
    .catch Ljava/lang/RuntimeException; {:try_start_119 .. :try_end_14f} :catch_dd0

    #@14f
    .line 278
    .end local v85           #display:Lcom/android/server/display/DisplayManagerService;
    .local v10, display:Lcom/android/server/display/DisplayManagerService;
    :try_start_14f
    const-string v7, "display"

    #@151
    const/4 v9, 0x1

    #@152
    invoke-static {v7, v10, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;Z)V

    #@155
    .line 280
    const-string v7, "SystemServer"

    #@157
    const-string v9, "Telephony Registry"

    #@159
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    .line 281
    new-instance v136, Lcom/android/server/TelephonyRegistry;

    #@15e
    move-object/from16 v0, v136

    #@160
    invoke-direct {v0, v5}, Lcom/android/server/TelephonyRegistry;-><init>(Landroid/content/Context;)V
    :try_end_163
    .catch Ljava/lang/RuntimeException; {:try_start_14f .. :try_end_163} :catch_ddf

    #@163
    .line 282
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .local v136, telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :try_start_163
    const-string v7, "telephony.registry"

    #@165
    move-object/from16 v0, v136

    #@167
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@16a
    .line 284
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@16d
    move-result-object v7

    #@16e
    invoke-virtual {v7}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@171
    move-result v7

    #@172
    if-eqz v7, :cond_18b

    #@174
    .line 285
    const-string v7, "SystemServer"

    #@176
    const-string v9, "MSimTelephony Registry"

    #@178
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    .line 286
    new-instance v109, Lcom/android/server/MSimTelephonyRegistry;

    #@17d
    move-object/from16 v0, v109

    #@17f
    invoke-direct {v0, v5}, Lcom/android/server/MSimTelephonyRegistry;-><init>(Landroid/content/Context;)V
    :try_end_182
    .catch Ljava/lang/RuntimeException; {:try_start_163 .. :try_end_182} :catch_9cc

    #@182
    .line 287
    .end local v108           #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    .local v109, msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    :try_start_182
    const-string v7, "telephony.msim.registry"

    #@184
    move-object/from16 v0, v109

    #@186
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_189
    .catch Ljava/lang/RuntimeException; {:try_start_182 .. :try_end_189} :catch_dec

    #@189
    move-object/from16 v108, v109

    #@18b
    .line 290
    .end local v109           #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    .restart local v108       #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    :cond_18b
    :try_start_18b
    const-string v7, "SystemServer"

    #@18d
    const-string v9, "Scheduling Policy"

    #@18f
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@192
    .line 291
    const-string v7, "scheduling_policy"

    #@194
    new-instance v9, Landroid/os/SchedulingPolicyService;

    #@196
    invoke-direct {v9}, Landroid/os/SchedulingPolicyService;-><init>()V

    #@199
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@19c
    .line 294
    invoke-static {v5}, Lcom/android/server/AttributeCache;->init(Landroid/content/Context;)V

    #@19f
    .line 296
    invoke-virtual {v10}, Lcom/android/server/display/DisplayManagerService;->waitForDefaultDisplay()Z

    #@1a2
    move-result v7

    #@1a3
    if-nez v7, :cond_1b1

    #@1a5
    .line 297
    const-string v7, "Timeout waiting for default display to be initialized."

    #@1a7
    new-instance v9, Ljava/lang/Throwable;

    #@1a9
    invoke-direct {v9}, Ljava/lang/Throwable;-><init>()V

    #@1ac
    move-object/from16 v0, p0

    #@1ae
    invoke-virtual {v0, v7, v9}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1b1
    .line 301
    :cond_1b1
    const-string v7, "SystemServer"

    #@1b3
    const-string v9, "Package Manager"

    #@1b5
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b8
    .line 303
    const-string v7, "vold.decrypt"

    #@1ba
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1bd
    move-result-object v80

    #@1be
    .line 304
    .local v80, cryptState:Ljava/lang/String;
    const-string v7, "trigger_restart_min_framework"

    #@1c0
    move-object/from16 v0, v80

    #@1c2
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c5
    move-result v7

    #@1c6
    if-eqz v7, :cond_9a8

    #@1c8
    .line 305
    const-string v7, "SystemServer"

    #@1ca
    const-string v9, "Detected encryption in progress - only parsing core apps"

    #@1cc
    invoke-static {v7, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf
    .line 306
    const/16 v24, 0x1

    #@1d1
    .line 317
    :cond_1d1
    :goto_1d1
    const/16 v7, -0x13

    #@1d3
    invoke-static {v7}, Landroid/os/Process;->setThreadPriority(I)V

    #@1d6
    .line 321
    if-eqz v92, :cond_9bd

    #@1d8
    const/4 v7, 0x1

    #@1d9
    :goto_1d9
    move-object/from16 v0, v99

    #@1db
    move/from16 v1, v24

    #@1dd
    invoke-static {v5, v0, v7, v1}, Lcom/android/server/pm/PackageManagerService;->main(Landroid/content/Context;Lcom/android/server/pm/Installer;ZZ)Landroid/content/pm/IPackageManager;

    #@1e0
    move-result-object v116

    #@1e1
    .line 331
    const/4 v7, -0x2

    #@1e2
    invoke-static {v7}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_1e5
    .catch Ljava/lang/RuntimeException; {:try_start_18b .. :try_end_1e5} :catch_9cc

    #@1e5
    .line 335
    const/16 v93, 0x0

    #@1e7
    .line 337
    .local v93, firstBoot:Z
    :try_start_1e7
    invoke-interface/range {v116 .. v116}, Landroid/content/pm/IPackageManager;->isFirstBoot()Z
    :try_end_1ea
    .catch Landroid/os/RemoteException; {:try_start_1e7 .. :try_end_1ea} :catch_d48
    .catch Ljava/lang/RuntimeException; {:try_start_1e7 .. :try_end_1ea} :catch_9cc

    #@1ea
    move-result v93

    #@1eb
    .line 341
    :goto_1eb
    :try_start_1eb
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->setSystemProcess()V

    #@1ee
    .line 343
    const-string v7, "SystemServer"

    #@1f0
    const-string v9, "User Service"

    #@1f2
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f5
    .line 344
    const-string v7, "user"

    #@1f7
    invoke-static {}, Lcom/android/server/pm/UserManagerService;->getInstance()Lcom/android/server/pm/UserManagerService;

    #@1fa
    move-result-object v9

    #@1fb
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@1fe
    .line 348
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@201
    move-result-object v7

    #@202
    move-object/from16 v0, p0

    #@204
    iput-object v7, v0, Lcom/android/server/ServerThread;->mContentResolver:Landroid/content/ContentResolver;
    :try_end_206
    .catch Ljava/lang/RuntimeException; {:try_start_1eb .. :try_end_206} :catch_9cc

    #@206
    .line 352
    :try_start_206
    const-string v7, "SystemServer"

    #@208
    const-string v9, "Account Manager"

    #@20a
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@20d
    .line 353
    new-instance v63, Landroid/accounts/AccountManagerService;

    #@20f
    move-object/from16 v0, v63

    #@211
    invoke-direct {v0, v5}, Landroid/accounts/AccountManagerService;-><init>(Landroid/content/Context;)V
    :try_end_214
    .catch Ljava/lang/Throwable; {:try_start_206 .. :try_end_214} :catch_9c0
    .catch Ljava/lang/RuntimeException; {:try_start_206 .. :try_end_214} :catch_9cc

    #@214
    .line 354
    .end local v62           #accountManager:Landroid/accounts/AccountManagerService;
    .local v63, accountManager:Landroid/accounts/AccountManagerService;
    :try_start_214
    const-string v7, "account"

    #@216
    move-object/from16 v0, v63

    #@218
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_21b
    .catch Ljava/lang/Throwable; {:try_start_214 .. :try_end_21b} :catch_e49
    .catch Ljava/lang/RuntimeException; {:try_start_214 .. :try_end_21b} :catch_dfd

    #@21b
    move-object/from16 v62, v63

    #@21d
    .line 359
    .end local v63           #accountManager:Landroid/accounts/AccountManagerService;
    .restart local v62       #accountManager:Landroid/accounts/AccountManagerService;
    :goto_21d
    :try_start_21d
    const-string v7, "SystemServer"

    #@21f
    const-string v9, "Content Manager"

    #@221
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@224
    .line 360
    const/4 v7, 0x1

    #@225
    move/from16 v0, v92

    #@227
    if-ne v0, v7, :cond_9eb

    #@229
    const/4 v7, 0x1

    #@22a
    :goto_22a
    invoke-static {v5, v7}, Landroid/content/ContentService;->main(Landroid/content/Context;Z)Landroid/content/ContentService;

    #@22d
    move-result-object v77

    #@22e
    .line 363
    const-string v7, "SystemServer"

    #@230
    const-string v9, "System Content Providers"

    #@232
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@235
    .line 364
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->installSystemProviders()V

    #@238
    .line 366
    const-string v7, "SystemServer"

    #@23a
    const-string v9, "Lights Service"

    #@23c
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@23f
    .line 367
    new-instance v6, Lcom/android/server/LightsService;

    #@241
    invoke-direct {v6, v5}, Lcom/android/server/LightsService;-><init>(Landroid/content/Context;)V
    :try_end_244
    .catch Ljava/lang/RuntimeException; {:try_start_21d .. :try_end_244} :catch_9cc

    #@244
    .line 369
    .end local v100           #lights:Lcom/android/server/LightsService;
    .local v6, lights:Lcom/android/server/LightsService;
    :try_start_244
    const-string v7, "SystemServer"

    #@246
    const-string v9, "Battery Service"

    #@248
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24b
    .line 370
    new-instance v8, Lcom/android/server/BatteryService;

    #@24d
    invoke-direct {v8, v5, v6}, Lcom/android/server/BatteryService;-><init>(Landroid/content/Context;Lcom/android/server/LightsService;)V
    :try_end_250
    .catch Ljava/lang/RuntimeException; {:try_start_244 .. :try_end_250} :catch_e0e

    #@250
    .line 371
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .local v8, battery:Lcom/android/server/BatteryService;
    :try_start_250
    const-string v7, "battery"

    #@252
    invoke-static {v7, v8}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@255
    .line 373
    const-string v7, "SystemServer"

    #@257
    const-string v9, "Vibrator Service"

    #@259
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@25c
    .line 374
    new-instance v151, Lcom/android/server/VibratorService;

    #@25e
    move-object/from16 v0, v151

    #@260
    invoke-direct {v0, v5}, Lcom/android/server/VibratorService;-><init>(Landroid/content/Context;)V
    :try_end_263
    .catch Ljava/lang/RuntimeException; {:try_start_250 .. :try_end_263} :catch_e1b

    #@263
    .line 375
    .end local v150           #vibrator:Lcom/android/server/VibratorService;
    .local v151, vibrator:Lcom/android/server/VibratorService;
    :try_start_263
    const-string v7, "vibrator"

    #@265
    move-object/from16 v0, v151

    #@267
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@26a
    .line 379
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    #@26d
    move-result-object v7

    #@26e
    invoke-static {}, Lcom/android/server/am/BatteryStatsService;->getService()Lcom/android/internal/app/IBatteryStats;

    #@271
    move-result-object v9

    #@272
    invoke-virtual/range {v4 .. v10}, Lcom/android/server/power/PowerManagerService;->init(Landroid/content/Context;Lcom/android/server/LightsService;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/BatteryService;Lcom/android/internal/app/IBatteryStats;Lcom/android/server/display/DisplayManagerService;)V

    #@275
    .line 382
    const-string v7, "SystemServer"

    #@277
    const-string v9, "Alarm Manager"

    #@279
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@27c
    .line 383
    new-instance v15, Lcom/android/server/AlarmManagerService;

    #@27e
    invoke-direct {v15, v5}, Lcom/android/server/AlarmManagerService;-><init>(Landroid/content/Context;)V
    :try_end_281
    .catch Ljava/lang/RuntimeException; {:try_start_263 .. :try_end_281} :catch_e26

    #@281
    .line 384
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .local v15, alarm:Lcom/android/server/AlarmManagerService;
    :try_start_281
    const-string v7, "alarm"

    #@283
    invoke-static {v7, v15}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@286
    .line 386
    const-string v7, "SystemServer"

    #@288
    const-string v9, "Init Watchdog"

    #@28a
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@28d
    .line 387
    invoke-static {}, Lcom/android/server/Watchdog;->getInstance()Lcom/android/server/Watchdog;

    #@290
    move-result-object v11

    #@291
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    #@294
    move-result-object v16

    #@295
    move-object v12, v5

    #@296
    move-object v13, v8

    #@297
    move-object v14, v4

    #@298
    invoke-virtual/range {v11 .. v16}, Lcom/android/server/Watchdog;->init(Landroid/content/Context;Lcom/android/server/BatteryService;Lcom/android/server/power/PowerManagerService;Lcom/android/server/AlarmManagerService;Lcom/android/server/am/ActivityManagerService;)V

    #@29b
    .line 390
    const-string v7, "SystemServer"

    #@29d
    const-string v9, "Input Manager"

    #@29f
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a2
    .line 391
    new-instance v19, Lcom/android/server/input/InputManagerService;

    #@2a4
    move-object/from16 v0, v19

    #@2a6
    move-object/from16 v1, v21

    #@2a8
    invoke-direct {v0, v5, v1}, Lcom/android/server/input/InputManagerService;-><init>(Landroid/content/Context;Landroid/os/Handler;)V
    :try_end_2ab
    .catch Ljava/lang/RuntimeException; {:try_start_281 .. :try_end_2ab} :catch_e33

    #@2ab
    .line 393
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .local v19, inputManager:Lcom/android/server/input/InputManagerService;
    :try_start_2ab
    const-string v7, "SystemServer"

    #@2ad
    const-string v9, "Window Manager"

    #@2af
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2b2
    .line 394
    const/4 v7, 0x1

    #@2b3
    move/from16 v0, v92

    #@2b5
    if-eq v0, v7, :cond_9ee

    #@2b7
    const/16 v22, 0x1

    #@2b9
    :goto_2b9
    if-nez v93, :cond_9f2

    #@2bb
    const/16 v23, 0x1

    #@2bd
    :goto_2bd
    move-object/from16 v16, v5

    #@2bf
    move-object/from16 v17, v4

    #@2c1
    move-object/from16 v18, v10

    #@2c3
    invoke-static/range {v16 .. v24}, Lcom/android/server/wm/WindowManagerService;->main(Landroid/content/Context;Lcom/android/server/power/PowerManagerService;Lcom/android/server/display/DisplayManagerService;Lcom/android/server/input/InputManagerService;Landroid/os/Handler;Landroid/os/Handler;ZZZ)Lcom/android/server/wm/WindowManagerService;

    #@2c6
    move-result-object v164

    #@2c7
    .line 398
    const-string v7, "window"

    #@2c9
    move-object/from16 v0, v164

    #@2cb
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@2ce
    .line 399
    const-string v7, "input"

    #@2d0
    move-object/from16 v0, v19

    #@2d2
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@2d5
    .line 401
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    #@2d8
    move-result-object v7

    #@2d9
    move-object/from16 v0, v164

    #@2db
    invoke-virtual {v7, v0}, Lcom/android/server/am/ActivityManagerService;->setWindowManager(Lcom/android/server/wm/WindowManagerService;)V

    #@2de
    .line 403
    invoke-virtual/range {v164 .. v164}, Lcom/android/server/wm/WindowManagerService;->getInputMonitor()Lcom/android/server/wm/InputMonitor;

    #@2e1
    move-result-object v7

    #@2e2
    move-object/from16 v0, v19

    #@2e4
    invoke-virtual {v0, v7}, Lcom/android/server/input/InputManagerService;->setWindowManagerCallbacks(Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;)V

    #@2e7
    .line 404
    invoke-virtual/range {v19 .. v19}, Lcom/android/server/input/InputManagerService;->start()V

    #@2ea
    .line 406
    move-object/from16 v0, v164

    #@2ec
    invoke-virtual {v10, v0}, Lcom/android/server/display/DisplayManagerService;->setWindowManager(Lcom/android/server/display/DisplayManagerService$WindowManagerFuncs;)V

    #@2ef
    .line 407
    move-object/from16 v0, v19

    #@2f1
    invoke-virtual {v10, v0}, Lcom/android/server/display/DisplayManagerService;->setInputManager(Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;)V

    #@2f4
    .line 412
    const-string v7, "ro.kernel.qemu"

    #@2f6
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2f9
    move-result-object v7

    #@2fa
    const-string v9, "1"

    #@2fc
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ff
    move-result v7

    #@300
    if-eqz v7, :cond_9f6

    #@302
    .line 413
    const-string v7, "SystemServer"

    #@304
    const-string v9, "No Bluetooh Service (emulator)"

    #@306
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_309
    .catch Ljava/lang/RuntimeException; {:try_start_2ab .. :try_end_309} :catch_a04

    #@309
    :goto_309
    move-object/from16 v135, v136

    #@30b
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v150, v151

    #@30d
    .end local v151           #vibrator:Lcom/android/server/VibratorService;
    .restart local v150       #vibrator:Lcom/android/server/VibratorService;
    move-object/from16 v98, v99

    #@30f
    .line 427
    .end local v80           #cryptState:Ljava/lang/String;
    .end local v93           #firstBoot:Z
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    :goto_30f
    const/16 v83, 0x0

    #@311
    .line 428
    .local v83, devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    const/16 v132, 0x0

    #@313
    .line 429
    .local v132, statusBar:Lcom/android/server/StatusBarManagerService;
    const/16 v95, 0x0

    #@315
    .line 430
    .local v95, imm:Lcom/android/server/InputMethodManagerService;
    const/16 v65, 0x0

    #@317
    .line 431
    .local v65, appWidget:Lcom/android/server/AppWidgetService;
    const/16 v114, 0x0

    #@319
    .line 432
    .local v114, notification:Lcom/android/server/NotificationManagerService;
    const/16 v154, 0x0

    #@31b
    .line 433
    .local v154, wallpaper:Lcom/android/server/WallpaperManagerService;
    const/16 v101, 0x0

    #@31d
    .line 434
    .local v101, location:Lcom/android/server/LocationManagerService;
    const/16 v78, 0x0

    #@31f
    .line 435
    .local v78, countryDetector:Lcom/android/server/CountryDetectorService;
    const/16 v140, 0x0

    #@321
    .line 436
    .local v140, tsms:Lcom/android/server/TextServicesManagerService;
    const/16 v103, 0x0

    #@323
    .line 437
    .local v103, lockSettings:Lcom/android/internal/widget/LockSettingsService;
    const/16 v88, 0x0

    #@325
    .line 441
    .local v88, dreamy:Lcom/android/server/dreams/DreamManagerService;
    const/16 v152, 0x0

    #@327
    .line 445
    .local v152, vzwLocMgrObj:Ljava/lang/Object;
    const/4 v7, 0x1

    #@328
    move/from16 v0, v92

    #@32a
    if-eq v0, v7, :cond_356

    #@32c
    .line 447
    :try_start_32c
    const-string v7, "SystemServer"

    #@32e
    const-string v9, "Input Method Service"

    #@330
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@333
    .line 448
    new-instance v96, Lcom/android/server/InputMethodManagerService;

    #@335
    move-object/from16 v0, v96

    #@337
    move-object/from16 v1, v164

    #@339
    invoke-direct {v0, v5, v1}, Lcom/android/server/InputMethodManagerService;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    :try_end_33c
    .catch Ljava/lang/Throwable; {:try_start_32c .. :try_end_33c} :catch_a25

    #@33c
    .line 449
    .end local v95           #imm:Lcom/android/server/InputMethodManagerService;
    .local v96, imm:Lcom/android/server/InputMethodManagerService;
    :try_start_33c
    const-string v7, "input_method"

    #@33e
    move-object/from16 v0, v96

    #@340
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_343
    .catch Ljava/lang/Throwable; {:try_start_33c .. :try_end_343} :catch_dab

    #@343
    move-object/from16 v95, v96

    #@345
    .line 455
    .end local v96           #imm:Lcom/android/server/InputMethodManagerService;
    .restart local v95       #imm:Lcom/android/server/InputMethodManagerService;
    :goto_345
    :try_start_345
    const-string v7, "SystemServer"

    #@347
    const-string v9, "Accessibility Manager"

    #@349
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34c
    .line 456
    const-string v7, "accessibility"

    #@34e
    new-instance v9, Lcom/android/server/accessibility/AccessibilityManagerService;

    #@350
    invoke-direct {v9, v5}, Lcom/android/server/accessibility/AccessibilityManagerService;-><init>(Landroid/content/Context;)V

    #@353
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_356
    .catch Ljava/lang/Throwable; {:try_start_345 .. :try_end_356} :catch_a31

    #@356
    .line 464
    :cond_356
    :goto_356
    :try_start_356
    invoke-virtual/range {v164 .. v164}, Lcom/android/server/wm/WindowManagerService;->displayReady()V
    :try_end_359
    .catch Ljava/lang/Throwable; {:try_start_356 .. :try_end_359} :catch_a3d

    #@359
    .line 470
    :goto_359
    :try_start_359
    invoke-interface/range {v116 .. v116}, Landroid/content/pm/IPackageManager;->performBootDexOpt()V
    :try_end_35c
    .catch Ljava/lang/Throwable; {:try_start_359 .. :try_end_35c} :catch_a49

    #@35c
    .line 476
    :goto_35c
    :try_start_35c
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@35f
    move-result-object v7

    #@360
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@363
    move-result-object v9

    #@364
    const v11, 0x1040411

    #@367
    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@36a
    move-result-object v9

    #@36b
    const/4 v11, 0x0

    #@36c
    invoke-interface {v7, v9, v11}, Landroid/app/IActivityManager;->showBootMessage(Ljava/lang/CharSequence;Z)V
    :try_end_36f
    .catch Landroid/os/RemoteException; {:try_start_35c .. :try_end_36f} :catch_da8

    #@36f
    .line 483
    :goto_36f
    const/4 v7, 0x1

    #@370
    move/from16 v0, v92

    #@372
    if-eq v0, v7, :cond_e4e

    #@374
    .line 484
    const-string v7, "0"

    #@376
    const-string v9, "system_init.startmountservice"

    #@378
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@37b
    move-result-object v9

    #@37c
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37f
    move-result v7

    #@380
    if-nez v7, :cond_399

    #@382
    .line 490
    :try_start_382
    const-string v7, "SystemServer"

    #@384
    const-string v9, "Mount Service"

    #@386
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@389
    .line 491
    new-instance v107, Lcom/android/server/MountService;

    #@38b
    move-object/from16 v0, v107

    #@38d
    invoke-direct {v0, v5}, Lcom/android/server/MountService;-><init>(Landroid/content/Context;)V
    :try_end_390
    .catch Ljava/lang/Throwable; {:try_start_382 .. :try_end_390} :catch_a55

    #@390
    .line 492
    .end local v106           #mountService:Lcom/android/server/MountService;
    .local v107, mountService:Lcom/android/server/MountService;
    :try_start_390
    const-string v7, "mount"

    #@392
    move-object/from16 v0, v107

    #@394
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_397
    .catch Ljava/lang/Throwable; {:try_start_390 .. :try_end_397} :catch_da3

    #@397
    move-object/from16 v106, v107

    #@399
    .line 499
    .end local v107           #mountService:Lcom/android/server/MountService;
    .restart local v106       #mountService:Lcom/android/server/MountService;
    :cond_399
    :goto_399
    :try_start_399
    const-string v7, "SystemServer"

    #@39b
    const-string v9, "LockSettingsService"

    #@39d
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a0
    .line 500
    new-instance v104, Lcom/android/internal/widget/LockSettingsService;

    #@3a2
    move-object/from16 v0, v104

    #@3a4
    invoke-direct {v0, v5}, Lcom/android/internal/widget/LockSettingsService;-><init>(Landroid/content/Context;)V
    :try_end_3a7
    .catch Ljava/lang/Throwable; {:try_start_399 .. :try_end_3a7} :catch_a61

    #@3a7
    .line 501
    .end local v103           #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .local v104, lockSettings:Lcom/android/internal/widget/LockSettingsService;
    :try_start_3a7
    const-string v7, "lock_settings"

    #@3a9
    move-object/from16 v0, v104

    #@3ab
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3ae
    .catch Ljava/lang/Throwable; {:try_start_3a7 .. :try_end_3ae} :catch_d9e

    #@3ae
    move-object/from16 v103, v104

    #@3b0
    .line 507
    .end local v104           #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .restart local v103       #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    :goto_3b0
    :try_start_3b0
    const-string v7, "SystemServer"

    #@3b2
    const-string v9, "Device Policy"

    #@3b4
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3b7
    .line 508
    new-instance v84, Lcom/android/server/DevicePolicyManagerService;

    #@3b9
    move-object/from16 v0, v84

    #@3bb
    invoke-direct {v0, v5}, Lcom/android/server/DevicePolicyManagerService;-><init>(Landroid/content/Context;)V
    :try_end_3be
    .catch Ljava/lang/Throwable; {:try_start_3b0 .. :try_end_3be} :catch_a6d

    #@3be
    .line 509
    .end local v83           #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .local v84, devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    :try_start_3be
    const-string v7, "device_policy"

    #@3c0
    move-object/from16 v0, v84

    #@3c2
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3c5
    .catch Ljava/lang/Throwable; {:try_start_3be .. :try_end_3c5} :catch_d99

    #@3c5
    move-object/from16 v83, v84

    #@3c7
    .line 515
    .end local v84           #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .restart local v83       #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    :goto_3c7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3ca
    move-result-object v7

    #@3cb
    if-eqz v7, :cond_3d4

    #@3cd
    .line 516
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3d0
    move-result-object v7

    #@3d1
    invoke-interface {v7, v5}, Lcom/lge/cappuccino/IMdm;->launchService(Landroid/content/Context;)V

    #@3d4
    .line 521
    :cond_3d4
    :try_start_3d4
    const-string v7, "SystemServer"

    #@3d6
    const-string v9, "Status Bar"

    #@3d8
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3db
    .line 522
    new-instance v133, Lcom/android/server/StatusBarManagerService;

    #@3dd
    move-object/from16 v0, v133

    #@3df
    move-object/from16 v1, v164

    #@3e1
    invoke-direct {v0, v5, v1}, Lcom/android/server/StatusBarManagerService;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    :try_end_3e4
    .catch Ljava/lang/Throwable; {:try_start_3d4 .. :try_end_3e4} :catch_a79

    #@3e4
    .line 523
    .end local v132           #statusBar:Lcom/android/server/StatusBarManagerService;
    .local v133, statusBar:Lcom/android/server/StatusBarManagerService;
    :try_start_3e4
    const-string v7, "statusbar"

    #@3e6
    move-object/from16 v0, v133

    #@3e8
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3eb
    .catch Ljava/lang/Throwable; {:try_start_3e4 .. :try_end_3eb} :catch_d94

    #@3eb
    move-object/from16 v132, v133

    #@3ed
    .line 529
    .end local v133           #statusBar:Lcom/android/server/StatusBarManagerService;
    .restart local v132       #statusBar:Lcom/android/server/StatusBarManagerService;
    :goto_3ed
    :try_start_3ed
    const-string v7, "SystemServer"

    #@3ef
    const-string v9, "Clipboard Service"

    #@3f1
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3f4
    .line 530
    const-string v7, "clipboard"

    #@3f6
    new-instance v9, Lcom/android/server/ClipboardService;

    #@3f8
    invoke-direct {v9, v5}, Lcom/android/server/ClipboardService;-><init>(Landroid/content/Context;)V

    #@3fb
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3fe
    .catch Ljava/lang/Throwable; {:try_start_3ed .. :try_end_3fe} :catch_a85

    #@3fe
    .line 537
    :goto_3fe
    :try_start_3fe
    const-string v7, "SystemServer"

    #@400
    const-string v9, "NetworkManagement Service"

    #@402
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@405
    .line 538
    invoke-static {v5}, Lcom/android/server/NetworkManagementService;->create(Landroid/content/Context;)Lcom/android/server/NetworkManagementService;

    #@408
    move-result-object v30

    #@409
    .line 539
    const-string v7, "network_management"

    #@40b
    move-object/from16 v0, v30

    #@40d
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_410
    .catch Ljava/lang/Throwable; {:try_start_3fe .. :try_end_410} :catch_a91

    #@410
    .line 545
    :goto_410
    :try_start_410
    const-string v7, "SystemServer"

    #@412
    const-string v9, "Text Service Manager Service"

    #@414
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@417
    .line 546
    new-instance v141, Lcom/android/server/TextServicesManagerService;

    #@419
    move-object/from16 v0, v141

    #@41b
    invoke-direct {v0, v5}, Lcom/android/server/TextServicesManagerService;-><init>(Landroid/content/Context;)V
    :try_end_41e
    .catch Ljava/lang/Throwable; {:try_start_410 .. :try_end_41e} :catch_a9d

    #@41e
    .line 547
    .end local v140           #tsms:Lcom/android/server/TextServicesManagerService;
    .local v141, tsms:Lcom/android/server/TextServicesManagerService;
    :try_start_41e
    const-string v7, "textservices"

    #@420
    move-object/from16 v0, v141

    #@422
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_425
    .catch Ljava/lang/Throwable; {:try_start_41e .. :try_end_425} :catch_d8f

    #@425
    move-object/from16 v140, v141

    #@427
    .line 553
    .end local v141           #tsms:Lcom/android/server/TextServicesManagerService;
    .restart local v140       #tsms:Lcom/android/server/TextServicesManagerService;
    :goto_427
    :try_start_427
    const-string v7, "SystemServer"

    #@429
    const-string v9, "NetworkStats Service"

    #@42b
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@42e
    .line 554
    new-instance v111, Lcom/android/server/net/NetworkStatsService;

    #@430
    move-object/from16 v0, v111

    #@432
    move-object/from16 v1, v30

    #@434
    invoke-direct {v0, v5, v1, v15}, Lcom/android/server/net/NetworkStatsService;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/app/IAlarmManager;)V
    :try_end_437
    .catch Ljava/lang/Throwable; {:try_start_427 .. :try_end_437} :catch_aa9

    #@437
    .line 555
    .end local v29           #networkStats:Lcom/android/server/net/NetworkStatsService;
    .local v111, networkStats:Lcom/android/server/net/NetworkStatsService;
    :try_start_437
    const-string v7, "netstats"

    #@439
    move-object/from16 v0, v111

    #@43b
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_43e
    .catch Ljava/lang/Throwable; {:try_start_437 .. :try_end_43e} :catch_d8a

    #@43e
    move-object/from16 v29, v111

    #@440
    .line 561
    .end local v111           #networkStats:Lcom/android/server/net/NetworkStatsService;
    .restart local v29       #networkStats:Lcom/android/server/net/NetworkStatsService;
    :goto_440
    :try_start_440
    const-string v7, "SystemServer"

    #@442
    const-string v9, "NetworkPolicy Service"

    #@444
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@447
    .line 562
    new-instance v25, Lcom/android/server/net/NetworkPolicyManagerService;

    #@449
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    #@44c
    move-result-object v27

    #@44d
    move-object/from16 v26, v5

    #@44f
    move-object/from16 v28, v4

    #@451
    invoke-direct/range {v25 .. v30}, Lcom/android/server/net/NetworkPolicyManagerService;-><init>(Landroid/content/Context;Landroid/app/IActivityManager;Landroid/os/IPowerManager;Landroid/net/INetworkStatsService;Landroid/os/INetworkManagementService;)V
    :try_end_454
    .catch Ljava/lang/Throwable; {:try_start_440 .. :try_end_454} :catch_ab5

    #@454
    .line 565
    .end local v110           #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .local v25, networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    :try_start_454
    const-string v7, "netpolicy"

    #@456
    move-object/from16 v0, v25

    #@458
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_45b
    .catch Ljava/lang/Throwable; {:try_start_454 .. :try_end_45b} :catch_d87

    #@45b
    .line 571
    :goto_45b
    :try_start_45b
    const-string v7, "SystemServer"

    #@45d
    const-string v9, "Regulatory Observer"

    #@45f
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@462
    .line 573
    new-instance v127, Lcom/android/server/RegulatoryObserver;

    #@464
    move-object/from16 v0, v127

    #@466
    invoke-direct {v0, v5}, Lcom/android/server/RegulatoryObserver;-><init>(Landroid/content/Context;)V
    :try_end_469
    .catch Ljava/lang/Throwable; {:try_start_45b .. :try_end_469} :catch_ac3

    #@469
    .end local v126           #regulatory:Lcom/android/server/RegulatoryObserver;
    .local v127, regulatory:Lcom/android/server/RegulatoryObserver;
    move-object/from16 v126, v127

    #@46b
    .line 579
    .end local v127           #regulatory:Lcom/android/server/RegulatoryObserver;
    .restart local v126       #regulatory:Lcom/android/server/RegulatoryObserver;
    :goto_46b
    :try_start_46b
    const-string v7, "SystemServer"

    #@46d
    const-string v9, "Wi-Fi P2pService"

    #@46f
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@472
    .line 580
    new-instance v163, Landroid/net/wifi/p2p/WifiP2pService;

    #@474
    move-object/from16 v0, v163

    #@476
    invoke-direct {v0, v5}, Landroid/net/wifi/p2p/WifiP2pService;-><init>(Landroid/content/Context;)V
    :try_end_479
    .catch Ljava/lang/Throwable; {:try_start_46b .. :try_end_479} :catch_acf

    #@479
    .line 581
    .end local v162           #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    .local v163, wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    :try_start_479
    const-string v7, "wifip2p"

    #@47b
    move-object/from16 v0, v163

    #@47d
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_480
    .catch Ljava/lang/Throwable; {:try_start_479 .. :try_end_480} :catch_d82

    #@480
    move-object/from16 v162, v163

    #@482
    .line 586
    .end local v163           #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    .restart local v162       #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    :goto_482
    :try_start_482
    const-string v7, "SystemServer"

    #@484
    const-string v9, "Wi-Fi Service"

    #@486
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@489
    .line 587
    new-instance v157, Lcom/android/server/WifiService;

    #@48b
    move-object/from16 v0, v157

    #@48d
    invoke-direct {v0, v5}, Lcom/android/server/WifiService;-><init>(Landroid/content/Context;)V
    :try_end_490
    .catch Ljava/lang/Throwable; {:try_start_482 .. :try_end_490} :catch_adb

    #@490
    .line 588
    .end local v156           #wifi:Lcom/android/server/WifiService;
    .local v157, wifi:Lcom/android/server/WifiService;
    :try_start_490
    const-string v7, "wifi"

    #@492
    move-object/from16 v0, v157

    #@494
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_497
    .catch Ljava/lang/Throwable; {:try_start_490 .. :try_end_497} :catch_d7d

    #@497
    move-object/from16 v156, v157

    #@499
    .line 595
    .end local v157           #wifi:Lcom/android/server/WifiService;
    .restart local v156       #wifi:Lcom/android/server/WifiService;
    :goto_499
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@49b
    if-eqz v7, :cond_4b9

    #@49d
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiOffloading()Z

    #@4a0
    move-result v7

    #@4a1
    if-eqz v7, :cond_4b9

    #@4a3
    .line 597
    :try_start_4a3
    const-string v7, "SystemServer"

    #@4a5
    const-string v9, "WiFi Offloading Service"

    #@4a7
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4aa
    .line 598
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiOffloadingIfaceIface()Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@4ad
    move-result-object v7

    #@4ae
    if-eqz v7, :cond_4b9

    #@4b0
    .line 599
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiOffloadingIfaceIface()Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@4b3
    move-result-object v161

    #@4b4
    .line 600
    .local v161, wifiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;
    move-object/from16 v0, v161

    #@4b6
    invoke-interface {v0, v5}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->startService(Landroid/content/Context;)V
    :try_end_4b9
    .catch Ljava/lang/Throwable; {:try_start_4a3 .. :try_end_4b9} :catch_ae7

    #@4b9
    .line 609
    .end local v161           #wifiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;
    :cond_4b9
    :goto_4b9
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@4bb
    if-eqz v7, :cond_4d9

    #@4bd
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiAggregation()Z

    #@4c0
    move-result v7

    #@4c1
    if-eqz v7, :cond_4d9

    #@4c3
    .line 611
    :try_start_4c3
    const-string v7, "SystemServer"

    #@4c5
    const-string v9, "WiFi Aggregation Service"

    #@4c7
    invoke-static {v7, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4ca
    .line 612
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiAggregationIfaceIface()Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@4cd
    move-result-object v7

    #@4ce
    if-eqz v7, :cond_4d9

    #@4d0
    .line 613
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiAggregationIfaceIface()Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@4d3
    move-result-object v158

    #@4d4
    .line 614
    .local v158, wifiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;
    move-object/from16 v0, v158

    #@4d6
    invoke-interface {v0, v5}, Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;->startService(Landroid/content/Context;)V
    :try_end_4d9
    .catch Ljava/lang/Throwable; {:try_start_4c3 .. :try_end_4d9} :catch_af3

    #@4d9
    .line 623
    .end local v158           #wifiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;
    :cond_4d9
    :goto_4d9
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@4db
    if-eqz v7, :cond_4f3

    #@4dd
    .line 625
    :try_start_4dd
    const-string v7, "SystemServer"

    #@4df
    const-string v9, "Wifi Ext Service"

    #@4e1
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4e4
    .line 626
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiExtServiceIface()Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;

    #@4e7
    move-result-object v7

    #@4e8
    if-eqz v7, :cond_4f3

    #@4ea
    .line 627
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiExtServiceIface()Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;

    #@4ed
    move-result-object v159

    #@4ee
    .line 628
    .local v159, wifiExtServiceIface:Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;
    move-object/from16 v0, v159

    #@4f0
    invoke-interface {v0, v5}, Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;->startService(Landroid/content/Context;)V
    :try_end_4f3
    .catch Ljava/lang/Throwable; {:try_start_4dd .. :try_end_4f3} :catch_aff

    #@4f3
    .line 637
    .end local v159           #wifiExtServiceIface:Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;
    :cond_4f3
    :goto_4f3
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@4f5
    if-eqz v7, :cond_513

    #@4f7
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useMobileHotspot()Z

    #@4fa
    move-result v7

    #@4fb
    if-eqz v7, :cond_513

    #@4fd
    .line 639
    :try_start_4fd
    const-string v7, "SystemServer"

    #@4ff
    const-string v9, "Mobile Hotspot Service"

    #@501
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@504
    .line 640
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@507
    move-result-object v7

    #@508
    if-eqz v7, :cond_513

    #@50a
    .line 641
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@50d
    move-result-object v160

    #@50e
    .line 642
    .local v160, wifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    move-object/from16 v0, v160

    #@510
    invoke-interface {v0, v5}, Lcom/lge/wifi_iface/WifiMHPIfaceIface;->startService(Landroid/content/Context;)V
    :try_end_513
    .catch Ljava/lang/Throwable; {:try_start_4fd .. :try_end_513} :catch_b0b

    #@513
    .line 651
    .end local v160           #wifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    :cond_513
    :goto_513
    :try_start_513
    const-string v7, "persist.cne.feature"

    #@515
    const/4 v9, 0x0

    #@516
    invoke-static {v7, v9}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@519
    move-result v149

    #@51a
    .line 652
    .local v149, value:I
    if-lez v149, :cond_b17

    #@51c
    const/4 v7, 0x7

    #@51d
    move/from16 v0, v149

    #@51f
    if-ge v0, v7, :cond_b17

    #@521
    .line 653
    const-string v7, "SystemServer"

    #@523
    const-string v9, "QcConnectivity Service"

    #@525
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@528
    .line 654
    new-instance v120, Ldalvik/system/PathClassLoader;

    #@52a
    const-string v7, "/system/framework/services-ext.jar"

    #@52c
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@52f
    move-result-object v9

    #@530
    move-object/from16 v0, v120

    #@532
    invoke-direct {v0, v7, v9}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@535
    .line 657
    .local v120, qcsClassLoader:Ldalvik/system/PathClassLoader;
    const-string v7, "com.android.server.QcConnectivityService"

    #@537
    move-object/from16 v0, v120

    #@539
    invoke-virtual {v0, v7}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@53c
    move-result-object v119

    #@53d
    .line 659
    .local v119, qcsClass:Ljava/lang/Class;
    const/4 v7, 0x4

    #@53e
    new-array v7, v7, [Ljava/lang/Class;

    #@540
    const/4 v9, 0x0

    #@541
    const-class v11, Landroid/content/Context;

    #@543
    aput-object v11, v7, v9

    #@545
    const/4 v9, 0x1

    #@546
    const-class v11, Landroid/os/INetworkManagementService;

    #@548
    aput-object v11, v7, v9

    #@54a
    const/4 v9, 0x2

    #@54b
    const-class v11, Landroid/net/INetworkStatsService;

    #@54d
    aput-object v11, v7, v9

    #@54f
    const/4 v9, 0x3

    #@550
    const-class v11, Landroid/net/INetworkPolicyManager;

    #@552
    aput-object v11, v7, v9

    #@554
    move-object/from16 v0, v119

    #@556
    invoke-virtual {v0, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@559
    move-result-object v121

    #@55a
    .line 662
    .local v121, qcsConstructor:Ljava/lang/reflect/Constructor;
    const/4 v7, 0x4

    #@55b
    new-array v7, v7, [Ljava/lang/Object;

    #@55d
    const/4 v9, 0x0

    #@55e
    aput-object v5, v7, v9

    #@560
    const/4 v9, 0x1

    #@561
    aput-object v30, v7, v9

    #@563
    const/4 v9, 0x2

    #@564
    aput-object v29, v7, v9

    #@566
    const/4 v9, 0x3

    #@567
    aput-object v25, v7, v9

    #@569
    move-object/from16 v0, v121

    #@56b
    invoke-virtual {v0, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@56e
    move-result-object v118

    #@56f
    .line 664
    move-object/from16 v0, v118

    #@571
    check-cast v0, Lcom/android/server/ConnectivityService;

    #@573
    move-object/from16 v75, v0

    #@575
    .line 670
    .end local v118           #qcCon:Ljava/lang/Object;
    .end local v119           #qcsClass:Ljava/lang/Class;
    .end local v120           #qcsClassLoader:Ldalvik/system/PathClassLoader;
    .end local v121           #qcsConstructor:Ljava/lang/reflect/Constructor;
    :goto_575
    if-eqz v75, :cond_592

    #@577
    .line 671
    const-string v7, "connectivity"

    #@579
    move-object/from16 v0, v75

    #@57b
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@57e
    .line 672
    move-object/from16 v0, v29

    #@580
    move-object/from16 v1, v75

    #@582
    invoke-virtual {v0, v1}, Lcom/android/server/net/NetworkStatsService;->bindConnectivityManager(Landroid/net/IConnectivityManager;)V

    #@585
    .line 673
    move-object/from16 v0, v25

    #@587
    move-object/from16 v1, v75

    #@589
    invoke-virtual {v0, v1}, Lcom/android/server/net/NetworkPolicyManagerService;->bindConnectivityManager(Landroid/net/IConnectivityManager;)V

    #@58c
    .line 674
    invoke-virtual/range {v156 .. v156}, Lcom/android/server/WifiService;->checkAndStartWifi()V

    #@58f
    .line 675
    invoke-virtual/range {v162 .. v162}, Landroid/net/wifi/p2p/WifiP2pService;->connectivityServiceReady()V
    :try_end_592
    .catch Ljava/lang/Throwable; {:try_start_513 .. :try_end_592} :catch_b2f

    #@592
    .line 682
    .end local v149           #value:I
    :cond_592
    :goto_592
    :try_start_592
    const-string v7, "SystemServer"

    #@594
    const-string v9, "Network Service Discovery Service"

    #@596
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@599
    .line 683
    invoke-static {v5}, Lcom/android/server/NsdService;->create(Landroid/content/Context;)Lcom/android/server/NsdService;

    #@59c
    move-result-object v130

    #@59d
    .line 684
    const-string v7, "servicediscovery"

    #@59f
    move-object/from16 v0, v130

    #@5a1
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_5a4
    .catch Ljava/lang/Throwable; {:try_start_592 .. :try_end_5a4} :catch_b3b

    #@5a4
    .line 691
    :goto_5a4
    :try_start_5a4
    const-string v7, "SystemServer"

    #@5a6
    const-string v9, "Throttle Service"

    #@5a8
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5ab
    .line 692
    new-instance v138, Lcom/android/server/ThrottleService;

    #@5ad
    move-object/from16 v0, v138

    #@5af
    invoke-direct {v0, v5}, Lcom/android/server/ThrottleService;-><init>(Landroid/content/Context;)V
    :try_end_5b2
    .catch Ljava/lang/Throwable; {:try_start_5a4 .. :try_end_5b2} :catch_b47

    #@5b2
    .line 693
    .end local v137           #throttle:Lcom/android/server/ThrottleService;
    .local v138, throttle:Lcom/android/server/ThrottleService;
    :try_start_5b2
    const-string v7, "throttle"

    #@5b4
    move-object/from16 v0, v138

    #@5b6
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_5b9
    .catch Ljava/lang/Throwable; {:try_start_5b2 .. :try_end_5b9} :catch_d78

    #@5b9
    move-object/from16 v137, v138

    #@5bb
    .line 700
    .end local v138           #throttle:Lcom/android/server/ThrottleService;
    .restart local v137       #throttle:Lcom/android/server/ThrottleService;
    :goto_5bb
    :try_start_5bb
    const-string v7, "SystemServer"

    #@5bd
    const-string v9, "UpdateLock Service"

    #@5bf
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5c2
    .line 701
    const-string v7, "updatelock"

    #@5c4
    new-instance v9, Lcom/android/server/UpdateLockService;

    #@5c6
    invoke-direct {v9, v5}, Lcom/android/server/UpdateLockService;-><init>(Landroid/content/Context;)V

    #@5c9
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_5cc
    .catch Ljava/lang/Throwable; {:try_start_5bb .. :try_end_5cc} :catch_b53

    #@5cc
    .line 712
    :goto_5cc
    if-eqz v106, :cond_5d1

    #@5ce
    .line 713
    invoke-virtual/range {v106 .. v106}, Lcom/android/server/MountService;->waitForAsecScan()V

    #@5d1
    .line 717
    :cond_5d1
    if-eqz v62, :cond_5d6

    #@5d3
    .line 718
    :try_start_5d3
    invoke-virtual/range {v62 .. v62}, Landroid/accounts/AccountManagerService;->systemReady()V
    :try_end_5d6
    .catch Ljava/lang/Throwable; {:try_start_5d3 .. :try_end_5d6} :catch_b5f

    #@5d6
    .line 724
    :cond_5d6
    :goto_5d6
    if-eqz v77, :cond_5db

    #@5d8
    .line 725
    :try_start_5d8
    invoke-virtual/range {v77 .. v77}, Landroid/content/ContentService;->systemReady()V
    :try_end_5db
    .catch Ljava/lang/Throwable; {:try_start_5d8 .. :try_end_5db} :catch_b6b

    #@5db
    .line 731
    :cond_5db
    :goto_5db
    :try_start_5db
    const-string v7, "SystemServer"

    #@5dd
    const-string v9, "Notification Manager"

    #@5df
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5e2
    .line 732
    new-instance v115, Lcom/android/server/NotificationManagerService;

    #@5e4
    move-object/from16 v0, v115

    #@5e6
    move-object/from16 v1, v132

    #@5e8
    invoke-direct {v0, v5, v1, v6}, Lcom/android/server/NotificationManagerService;-><init>(Landroid/content/Context;Lcom/android/server/StatusBarManagerService;Lcom/android/server/LightsService;)V
    :try_end_5eb
    .catch Ljava/lang/Throwable; {:try_start_5db .. :try_end_5eb} :catch_b77

    #@5eb
    .line 733
    .end local v114           #notification:Lcom/android/server/NotificationManagerService;
    .local v115, notification:Lcom/android/server/NotificationManagerService;
    :try_start_5eb
    const-string v7, "notification"

    #@5ed
    move-object/from16 v0, v115

    #@5ef
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@5f2
    .line 734
    move-object/from16 v0, v25

    #@5f4
    move-object/from16 v1, v115

    #@5f6
    invoke-virtual {v0, v1}, Lcom/android/server/net/NetworkPolicyManagerService;->bindNotificationManager(Landroid/app/INotificationManager;)V
    :try_end_5f9
    .catch Ljava/lang/Throwable; {:try_start_5eb .. :try_end_5f9} :catch_d73

    #@5f9
    move-object/from16 v114, v115

    #@5fb
    .line 740
    .end local v115           #notification:Lcom/android/server/NotificationManagerService;
    .restart local v114       #notification:Lcom/android/server/NotificationManagerService;
    :goto_5fb
    :try_start_5fb
    const-string v7, "SystemServer"

    #@5fd
    const-string v9, "Device Storage Monitor"

    #@5ff
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@602
    .line 741
    const-string v7, "devicestoragemonitor"

    #@604
    new-instance v9, Lcom/android/server/DeviceStorageMonitorService;

    #@606
    invoke-direct {v9, v5}, Lcom/android/server/DeviceStorageMonitorService;-><init>(Landroid/content/Context;)V

    #@609
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_60c
    .catch Ljava/lang/Throwable; {:try_start_5fb .. :try_end_60c} :catch_b83

    #@60c
    .line 748
    :goto_60c
    :try_start_60c
    const-string v7, "SystemServer"

    #@60e
    const-string v9, "Location Manager"

    #@610
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@613
    .line 749
    new-instance v102, Lcom/android/server/LocationManagerService;

    #@615
    move-object/from16 v0, v102

    #@617
    invoke-direct {v0, v5}, Lcom/android/server/LocationManagerService;-><init>(Landroid/content/Context;)V
    :try_end_61a
    .catch Ljava/lang/Throwable; {:try_start_60c .. :try_end_61a} :catch_b8f

    #@61a
    .line 750
    .end local v101           #location:Lcom/android/server/LocationManagerService;
    .local v102, location:Lcom/android/server/LocationManagerService;
    :try_start_61a
    const-string v7, "location"

    #@61c
    move-object/from16 v0, v102

    #@61e
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_621
    .catch Ljava/lang/Throwable; {:try_start_61a .. :try_end_621} :catch_d6e

    #@621
    move-object/from16 v101, v102

    #@623
    .line 756
    .end local v102           #location:Lcom/android/server/LocationManagerService;
    .restart local v101       #location:Lcom/android/server/LocationManagerService;
    :goto_623
    :try_start_623
    const-string v7, "SystemServer"

    #@625
    const-string v9, "Country Detector"

    #@627
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@62a
    .line 757
    new-instance v79, Lcom/android/server/CountryDetectorService;

    #@62c
    move-object/from16 v0, v79

    #@62e
    invoke-direct {v0, v5}, Lcom/android/server/CountryDetectorService;-><init>(Landroid/content/Context;)V
    :try_end_631
    .catch Ljava/lang/Throwable; {:try_start_623 .. :try_end_631} :catch_b9b

    #@631
    .line 758
    .end local v78           #countryDetector:Lcom/android/server/CountryDetectorService;
    .local v79, countryDetector:Lcom/android/server/CountryDetectorService;
    :try_start_631
    const-string v7, "country_detector"

    #@633
    move-object/from16 v0, v79

    #@635
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_638
    .catch Ljava/lang/Throwable; {:try_start_631 .. :try_end_638} :catch_d69

    #@638
    move-object/from16 v78, v79

    #@63a
    .line 764
    .end local v79           #countryDetector:Lcom/android/server/CountryDetectorService;
    .restart local v78       #countryDetector:Lcom/android/server/CountryDetectorService;
    :goto_63a
    :try_start_63a
    const-string v7, "SystemServer"

    #@63c
    const-string v9, "Search Service"

    #@63e
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@641
    .line 765
    const-string v7, "search"

    #@643
    new-instance v9, Landroid/server/search/SearchManagerService;

    #@645
    invoke-direct {v9, v5}, Landroid/server/search/SearchManagerService;-><init>(Landroid/content/Context;)V

    #@648
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_64b
    .catch Ljava/lang/Throwable; {:try_start_63a .. :try_end_64b} :catch_ba7

    #@64b
    .line 772
    :goto_64b
    :try_start_64b
    const-string v7, "SystemServer"

    #@64d
    const-string v9, "DropBox Service"

    #@64f
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@652
    .line 773
    const-string v7, "dropbox"

    #@654
    new-instance v9, Lcom/android/server/DropBoxManagerService;

    #@656
    new-instance v11, Ljava/io/File;

    #@658
    const-string v12, "/data/system/dropbox"

    #@65a
    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@65d
    invoke-direct {v9, v5, v11}, Lcom/android/server/DropBoxManagerService;-><init>(Landroid/content/Context;Ljava/io/File;)V

    #@660
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_663
    .catch Ljava/lang/Throwable; {:try_start_64b .. :try_end_663} :catch_bb3

    #@663
    .line 779
    :goto_663
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@666
    move-result-object v7

    #@667
    const v9, 0x111002b

    #@66a
    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@66d
    move-result v7

    #@66e
    if-eqz v7, :cond_689

    #@670
    .line 782
    :try_start_670
    const-string v7, "SystemServer"

    #@672
    const-string v9, "Wallpaper Service"

    #@674
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@677
    .line 783
    if-nez v33, :cond_689

    #@679
    .line 784
    new-instance v155, Lcom/android/server/WallpaperManagerService;

    #@67b
    move-object/from16 v0, v155

    #@67d
    invoke-direct {v0, v5}, Lcom/android/server/WallpaperManagerService;-><init>(Landroid/content/Context;)V
    :try_end_680
    .catch Ljava/lang/Throwable; {:try_start_670 .. :try_end_680} :catch_bbf

    #@680
    .line 785
    .end local v154           #wallpaper:Lcom/android/server/WallpaperManagerService;
    .local v155, wallpaper:Lcom/android/server/WallpaperManagerService;
    :try_start_680
    const-string v7, "wallpaper"

    #@682
    move-object/from16 v0, v155

    #@684
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_687
    .catch Ljava/lang/Throwable; {:try_start_680 .. :try_end_687} :catch_d64

    #@687
    move-object/from16 v154, v155

    #@689
    .line 792
    .end local v155           #wallpaper:Lcom/android/server/WallpaperManagerService;
    .restart local v154       #wallpaper:Lcom/android/server/WallpaperManagerService;
    :cond_689
    :goto_689
    const-string v7, "0"

    #@68b
    const-string v9, "system_init.startaudioservice"

    #@68d
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@690
    move-result-object v9

    #@691
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@694
    move-result v7

    #@695
    if-nez v7, :cond_6a8

    #@697
    .line 794
    :try_start_697
    const-string v7, "SystemServer"

    #@699
    const-string v9, "Audio Service"

    #@69b
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@69e
    .line 795
    const-string v7, "audio"

    #@6a0
    new-instance v9, Landroid/media/AudioService;

    #@6a2
    invoke-direct {v9, v5}, Landroid/media/AudioService;-><init>(Landroid/content/Context;)V

    #@6a5
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6a8
    .catch Ljava/lang/Throwable; {:try_start_697 .. :try_end_6a8} :catch_bcb

    #@6a8
    .line 802
    :cond_6a8
    :goto_6a8
    :try_start_6a8
    const-string v7, "SystemServer"

    #@6aa
    const-string v9, "Dock Observer"

    #@6ac
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6af
    .line 804
    new-instance v87, Lcom/android/server/DockObserver;

    #@6b1
    move-object/from16 v0, v87

    #@6b3
    invoke-direct {v0, v5}, Lcom/android/server/DockObserver;-><init>(Landroid/content/Context;)V
    :try_end_6b6
    .catch Ljava/lang/Throwable; {:try_start_6a8 .. :try_end_6b6} :catch_bd7

    #@6b6
    .end local v86           #dock:Lcom/android/server/DockObserver;
    .local v87, dock:Lcom/android/server/DockObserver;
    move-object/from16 v86, v87

    #@6b8
    .line 810
    .end local v87           #dock:Lcom/android/server/DockObserver;
    .restart local v86       #dock:Lcom/android/server/DockObserver;
    :goto_6b8
    :try_start_6b8
    const-string v7, "SystemServer"

    #@6ba
    const-string v9, "Wired Accessory Manager"

    #@6bc
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6bf
    .line 812
    new-instance v7, Lcom/android/server/WiredAccessoryManager;

    #@6c1
    move-object/from16 v0, v19

    #@6c3
    invoke-direct {v7, v5, v0}, Lcom/android/server/WiredAccessoryManager;-><init>(Landroid/content/Context;Lcom/android/server/input/InputManagerService;)V

    #@6c6
    move-object/from16 v0, v19

    #@6c8
    invoke-virtual {v0, v7}, Lcom/android/server/input/InputManagerService;->setWiredAccessoryCallbacks(Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;)V
    :try_end_6cb
    .catch Ljava/lang/Throwable; {:try_start_6b8 .. :try_end_6cb} :catch_be3

    #@6cb
    .line 819
    :goto_6cb
    :try_start_6cb
    const-string v7, "SystemServer"

    #@6cd
    const-string v9, "USB Service"

    #@6cf
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d2
    .line 821
    new-instance v148, Lcom/android/server/usb/UsbService;

    #@6d4
    move-object/from16 v0, v148

    #@6d6
    invoke-direct {v0, v5}, Lcom/android/server/usb/UsbService;-><init>(Landroid/content/Context;)V
    :try_end_6d9
    .catch Ljava/lang/Throwable; {:try_start_6cb .. :try_end_6d9} :catch_bef

    #@6d9
    .line 822
    .end local v147           #usb:Lcom/android/server/usb/UsbService;
    .local v148, usb:Lcom/android/server/usb/UsbService;
    :try_start_6d9
    const-string v7, "usb"

    #@6db
    move-object/from16 v0, v148

    #@6dd
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6e0
    .catch Ljava/lang/Throwable; {:try_start_6d9 .. :try_end_6e0} :catch_d5f

    #@6e0
    move-object/from16 v147, v148

    #@6e2
    .line 828
    .end local v148           #usb:Lcom/android/server/usb/UsbService;
    .restart local v147       #usb:Lcom/android/server/usb/UsbService;
    :goto_6e2
    :try_start_6e2
    const-string v7, "SystemServer"

    #@6e4
    const-string v9, "Serial Service"

    #@6e6
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6e9
    .line 830
    new-instance v129, Lcom/android/server/SerialService;

    #@6eb
    move-object/from16 v0, v129

    #@6ed
    invoke-direct {v0, v5}, Lcom/android/server/SerialService;-><init>(Landroid/content/Context;)V
    :try_end_6f0
    .catch Ljava/lang/Throwable; {:try_start_6e2 .. :try_end_6f0} :catch_bfb

    #@6f0
    .line 831
    .end local v128           #serial:Lcom/android/server/SerialService;
    .local v129, serial:Lcom/android/server/SerialService;
    :try_start_6f0
    const-string v7, "serial"

    #@6f2
    move-object/from16 v0, v129

    #@6f4
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6f7
    .catch Ljava/lang/Throwable; {:try_start_6f0 .. :try_end_6f7} :catch_d5a

    #@6f7
    move-object/from16 v128, v129

    #@6f9
    .line 837
    .end local v129           #serial:Lcom/android/server/SerialService;
    .restart local v128       #serial:Lcom/android/server/SerialService;
    :goto_6f9
    :try_start_6f9
    const-string v7, "SystemServer"

    #@6fb
    const-string v9, "Twilight Service"

    #@6fd
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@700
    .line 838
    new-instance v143, Lcom/android/server/TwilightService;

    #@702
    move-object/from16 v0, v143

    #@704
    invoke-direct {v0, v5}, Lcom/android/server/TwilightService;-><init>(Landroid/content/Context;)V
    :try_end_707
    .catch Ljava/lang/Throwable; {:try_start_6f9 .. :try_end_707} :catch_c07

    #@707
    .end local v142           #twilight:Lcom/android/server/TwilightService;
    .local v143, twilight:Lcom/android/server/TwilightService;
    move-object/from16 v142, v143

    #@709
    .line 844
    .end local v143           #twilight:Lcom/android/server/TwilightService;
    .restart local v142       #twilight:Lcom/android/server/TwilightService;
    :goto_709
    :try_start_709
    const-string v7, "SystemServer"

    #@70b
    const-string v9, "UI Mode Manager Service"

    #@70d
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@710
    .line 846
    new-instance v146, Lcom/android/server/UiModeManagerService;

    #@712
    move-object/from16 v0, v146

    #@714
    move-object/from16 v1, v142

    #@716
    invoke-direct {v0, v5, v1}, Lcom/android/server/UiModeManagerService;-><init>(Landroid/content/Context;Lcom/android/server/TwilightService;)V
    :try_end_719
    .catch Ljava/lang/Throwable; {:try_start_709 .. :try_end_719} :catch_c13

    #@719
    .end local v145           #uiMode:Lcom/android/server/UiModeManagerService;
    .local v146, uiMode:Lcom/android/server/UiModeManagerService;
    move-object/from16 v145, v146

    #@71b
    .line 852
    .end local v146           #uiMode:Lcom/android/server/UiModeManagerService;
    .restart local v145       #uiMode:Lcom/android/server/UiModeManagerService;
    :goto_71b
    :try_start_71b
    const-string v7, "SystemServer"

    #@71d
    const-string v9, "Backup Service"

    #@71f
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@722
    .line 853
    const-string v7, "backup"

    #@724
    new-instance v9, Lcom/android/server/BackupManagerService;

    #@726
    invoke-direct {v9, v5}, Lcom/android/server/BackupManagerService;-><init>(Landroid/content/Context;)V

    #@729
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_72c
    .catch Ljava/lang/Throwable; {:try_start_71b .. :try_end_72c} :catch_c1f

    #@72c
    .line 860
    :goto_72c
    :try_start_72c
    const-string v7, "SystemServer"

    #@72e
    const-string v9, "AppWidget Service"

    #@730
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@733
    .line 861
    new-instance v66, Lcom/android/server/AppWidgetService;

    #@735
    move-object/from16 v0, v66

    #@737
    invoke-direct {v0, v5}, Lcom/android/server/AppWidgetService;-><init>(Landroid/content/Context;)V
    :try_end_73a
    .catch Ljava/lang/Throwable; {:try_start_72c .. :try_end_73a} :catch_c2b

    #@73a
    .line 862
    .end local v65           #appWidget:Lcom/android/server/AppWidgetService;
    .local v66, appWidget:Lcom/android/server/AppWidgetService;
    :try_start_73a
    const-string v7, "appwidget"

    #@73c
    move-object/from16 v0, v66

    #@73e
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_741
    .catch Ljava/lang/Throwable; {:try_start_73a .. :try_end_741} :catch_d55

    #@741
    move-object/from16 v65, v66

    #@743
    .line 868
    .end local v66           #appWidget:Lcom/android/server/AppWidgetService;
    .restart local v65       #appWidget:Lcom/android/server/AppWidgetService;
    :goto_743
    :try_start_743
    const-string v7, "SystemServer"

    #@745
    const-string v9, "Recognition Service"

    #@747
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@74a
    .line 869
    new-instance v125, Lcom/android/server/RecognitionManagerService;

    #@74c
    move-object/from16 v0, v125

    #@74e
    invoke-direct {v0, v5}, Lcom/android/server/RecognitionManagerService;-><init>(Landroid/content/Context;)V
    :try_end_751
    .catch Ljava/lang/Throwable; {:try_start_743 .. :try_end_751} :catch_c37

    #@751
    .end local v124           #recognition:Lcom/android/server/RecognitionManagerService;
    .local v125, recognition:Lcom/android/server/RecognitionManagerService;
    move-object/from16 v124, v125

    #@753
    .line 875
    .end local v125           #recognition:Lcom/android/server/RecognitionManagerService;
    .restart local v124       #recognition:Lcom/android/server/RecognitionManagerService;
    :goto_753
    :try_start_753
    const-string v7, "SystemServer"

    #@755
    const-string v9, "DiskStats Service"

    #@757
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@75a
    .line 876
    const-string v7, "diskstats"

    #@75c
    new-instance v9, Lcom/android/server/DiskStatsService;

    #@75e
    invoke-direct {v9, v5}, Lcom/android/server/DiskStatsService;-><init>(Landroid/content/Context;)V

    #@761
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_764
    .catch Ljava/lang/Throwable; {:try_start_753 .. :try_end_764} :catch_c43

    #@764
    .line 882
    :goto_764
    :try_start_764
    const-string v7, "SystemServer"

    #@766
    const-string v9, "AtCmdFwd Service"

    #@768
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@76b
    .line 883
    new-instance v68, Lcom/android/internal/atfwd/AtCmdFwdService;

    #@76d
    move-object/from16 v0, v68

    #@76f
    invoke-direct {v0, v5}, Lcom/android/internal/atfwd/AtCmdFwdService;-><init>(Landroid/content/Context;)V

    #@772
    .line 884
    .local v68, atfwd:Lcom/android/internal/atfwd/AtCmdFwdService;
    const-string v7, "AtCmdFwd"

    #@774
    move-object/from16 v0, v68

    #@776
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_779
    .catch Ljava/lang/Throwable; {:try_start_764 .. :try_end_779} :catch_c4f

    #@779
    .line 894
    .end local v68           #atfwd:Lcom/android/internal/atfwd/AtCmdFwdService;
    :goto_779
    :try_start_779
    const-string v7, "SystemServer"

    #@77b
    const-string v9, "SamplingProfiler Service"

    #@77d
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@780
    .line 895
    const-string v7, "samplingprofiler"

    #@782
    new-instance v9, Lcom/android/server/SamplingProfilerService;

    #@784
    invoke-direct {v9, v5}, Lcom/android/server/SamplingProfilerService;-><init>(Landroid/content/Context;)V

    #@787
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_78a
    .catch Ljava/lang/Throwable; {:try_start_779 .. :try_end_78a} :catch_c5b

    #@78a
    .line 902
    :goto_78a
    :try_start_78a
    const-string v7, "SystemServer"

    #@78c
    const-string v9, "NetworkTimeUpdateService"

    #@78e
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@791
    .line 903
    new-instance v113, Lcom/android/server/NetworkTimeUpdateService;

    #@793
    move-object/from16 v0, v113

    #@795
    invoke-direct {v0, v5}, Lcom/android/server/NetworkTimeUpdateService;-><init>(Landroid/content/Context;)V
    :try_end_798
    .catch Ljava/lang/Throwable; {:try_start_78a .. :try_end_798} :catch_c67

    #@798
    .end local v112           #networkTimeUpdater:Lcom/android/server/NetworkTimeUpdateService;
    .local v113, networkTimeUpdater:Lcom/android/server/NetworkTimeUpdateService;
    move-object/from16 v112, v113

    #@79a
    .line 909
    .end local v113           #networkTimeUpdater:Lcom/android/server/NetworkTimeUpdateService;
    .restart local v112       #networkTimeUpdater:Lcom/android/server/NetworkTimeUpdateService;
    :goto_79a
    :try_start_79a
    const-string v7, "SystemServer"

    #@79c
    const-string v9, "CommonTimeManagementService"

    #@79e
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7a1
    .line 910
    new-instance v73, Lcom/android/server/CommonTimeManagementService;

    #@7a3
    move-object/from16 v0, v73

    #@7a5
    invoke-direct {v0, v5}, Lcom/android/server/CommonTimeManagementService;-><init>(Landroid/content/Context;)V
    :try_end_7a8
    .catch Ljava/lang/Throwable; {:try_start_79a .. :try_end_7a8} :catch_c73

    #@7a8
    .line 911
    .end local v72           #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    .local v73, commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    :try_start_7a8
    const-string v7, "commontime_management"

    #@7aa
    move-object/from16 v0, v73

    #@7ac
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_7af
    .catch Ljava/lang/Throwable; {:try_start_7a8 .. :try_end_7af} :catch_d50

    #@7af
    move-object/from16 v72, v73

    #@7b1
    .line 917
    .end local v73           #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    .restart local v72       #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    :goto_7b1
    :try_start_7b1
    const-string v7, "SystemServer"

    #@7b3
    const-string v9, "CertBlacklister"

    #@7b5
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7b8
    .line 918
    new-instance v7, Lcom/android/server/CertBlacklister;

    #@7ba
    invoke-direct {v7, v5}, Lcom/android/server/CertBlacklister;-><init>(Landroid/content/Context;)V
    :try_end_7bd
    .catch Ljava/lang/Throwable; {:try_start_7b1 .. :try_end_7bd} :catch_c7f

    #@7bd
    .line 923
    :goto_7bd
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7c0
    move-result-object v7

    #@7c1
    const v9, 0x1110040

    #@7c4
    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@7c7
    move-result v7

    #@7c8
    if-eqz v7, :cond_7e3

    #@7ca
    .line 926
    :try_start_7ca
    const-string v7, "SystemServer"

    #@7cc
    const-string v9, "Dreams Service"

    #@7ce
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7d1
    .line 928
    new-instance v89, Lcom/android/server/dreams/DreamManagerService;

    #@7d3
    move-object/from16 v0, v89

    #@7d5
    move-object/from16 v1, v21

    #@7d7
    invoke-direct {v0, v5, v1}, Lcom/android/server/dreams/DreamManagerService;-><init>(Landroid/content/Context;Landroid/os/Handler;)V
    :try_end_7da
    .catch Ljava/lang/Throwable; {:try_start_7ca .. :try_end_7da} :catch_c8b

    #@7da
    .line 929
    .end local v88           #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .local v89, dreamy:Lcom/android/server/dreams/DreamManagerService;
    :try_start_7da
    const-string v7, "dreams"

    #@7dc
    move-object/from16 v0, v89

    #@7de
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_7e1
    .catch Ljava/lang/Throwable; {:try_start_7da .. :try_end_7e1} :catch_d4b

    #@7e1
    move-object/from16 v88, v89

    #@7e3
    .line 936
    .end local v89           #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .restart local v88       #dreamy:Lcom/android/server/dreams/DreamManagerService;
    :cond_7e3
    :goto_7e3
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_THEMEICON:Z

    #@7e5
    if-eqz v7, :cond_7f8

    #@7e7
    .line 938
    :try_start_7e7
    const-string v7, "SystemServer"

    #@7e9
    const-string v9, "ThemeIconManager"

    #@7eb
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7ee
    .line 939
    const-string v7, "themeicon"

    #@7f0
    new-instance v9, Lcom/android/server/thm/ThemeIconManagerService;

    #@7f2
    invoke-direct {v9, v5}, Lcom/android/server/thm/ThemeIconManagerService;-><init>(Landroid/content/Context;)V

    #@7f5
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_7f8
    .catch Ljava/lang/Throwable; {:try_start_7e7 .. :try_end_7f8} :catch_c97

    #@7f8
    .line 948
    :cond_7f8
    :goto_7f8
    const-string v7, "VZW"

    #@7fa
    const-string v9, "ro.build.target_operator"

    #@7fc
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7ff
    move-result-object v9

    #@800
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@803
    move-result v7

    #@804
    if-eqz v7, :cond_817

    #@806
    .line 951
    :try_start_806
    const-string v7, "vzwconnectivity"

    #@808
    const-string v9, "add vzwconnectivity service"

    #@80a
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80d
    .line 952
    const-string v7, "vzwconnectivity"

    #@80f
    new-instance v9, Lcom/verizon/net/ConnectivityService;

    #@811
    invoke-direct {v9, v5}, Lcom/verizon/net/ConnectivityService;-><init>(Landroid/content/Context;)V

    #@814
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_817
    .catch Ljava/lang/Throwable; {:try_start_806 .. :try_end_817} :catch_ca3

    #@817
    .line 961
    :cond_817
    :goto_817
    const-string v7, "VZW"

    #@819
    const-string v9, "ro.build.target_operator"

    #@81b
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@81e
    move-result-object v9

    #@81f
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@822
    move-result v7

    #@823
    if-eqz v7, :cond_87d

    #@825
    .line 964
    :try_start_825
    const-string v139, "/data/dalvik-cache"

    #@827
    .line 965
    .local v139, tmpdir:Ljava/lang/String;
    const-string v7, "SystemServer"

    #@829
    const-string v9, "loading Vzw LocationManager Service"

    #@82b
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@82e
    .line 966
    new-instance v81, Ldalvik/system/DexClassLoader;

    #@830
    const-string v7, "/system/framework/vzwlbs.jar"

    #@832
    const/4 v9, 0x0

    #@833
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@836
    move-result-object v11

    #@837
    move-object/from16 v0, v81

    #@839
    move-object/from16 v1, v139

    #@83b
    invoke-direct {v0, v7, v1, v9, v11}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@83e
    .line 968
    .local v81, dclVzwLocMgr:Ldalvik/system/DexClassLoader;
    const-string v7, "com.vzw.location.service.VzwLocationManagerService"

    #@840
    move-object/from16 v0, v81

    #@842
    invoke-virtual {v0, v7}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@845
    move-result-object v134

    #@846
    .line 969
    .local v134, svcVzwLocMgr:Ljava/lang/Class;
    const/4 v7, 0x1

    #@847
    new-array v0, v7, [Ljava/lang/Object;

    #@849
    move-object/from16 v67, v0

    #@84b
    const/4 v7, 0x0

    #@84c
    aput-object v5, v67, v7

    #@84e
    .line 970
    .local v67, arglist:[Ljava/lang/Object;
    const/4 v7, 0x1

    #@84f
    new-array v7, v7, [Ljava/lang/Class;

    #@851
    const/4 v9, 0x0

    #@852
    const-class v11, Landroid/content/Context;

    #@854
    aput-object v11, v7, v9

    #@856
    move-object/from16 v0, v134

    #@858
    invoke-virtual {v0, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@85b
    move-result-object v61

    #@85c
    .line 971
    .local v61, VzwLocMgrConstructor:Ljava/lang/reflect/Constructor;
    const-string v7, "SystemServer"

    #@85e
    const-string v9, "Instantiating VzwLocationManagerService"

    #@860
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@863
    .line 973
    move-object/from16 v0, v61

    #@865
    move-object/from16 v1, v67

    #@867
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@86a
    move-result-object v94

    #@86b
    check-cast v94, Landroid/os/IBinder;

    #@86d
    .line 974
    .local v94, ib:Landroid/os/IBinder;
    move-object/from16 v152, v94

    #@86f
    .line 976
    .local v152, vzwLocMgrObj:Landroid/os/IBinder;
    const-string v7, "SystemServer"

    #@871
    const-string v9, "VzwLocation Manager"

    #@873
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@876
    .line 977
    const-string v7, "VZW_LOCATION_SERVICE"

    #@878
    move-object/from16 v0, v94

    #@87a
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_87d
    .catch Ljava/lang/Throwable; {:try_start_825 .. :try_end_87d} :catch_caf

    #@87d
    .line 986
    .end local v61           #VzwLocMgrConstructor:Ljava/lang/reflect/Constructor;
    .end local v67           #arglist:[Ljava/lang/Object;
    .end local v81           #dclVzwLocMgr:Ldalvik/system/DexClassLoader;
    .end local v94           #ib:Landroid/os/IBinder;
    .end local v134           #svcVzwLocMgr:Ljava/lang/Class;
    .end local v139           #tmpdir:Ljava/lang/String;
    .end local v152           #vzwLocMgrObj:Landroid/os/IBinder;
    :cond_87d
    :goto_87d
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@880
    move-result-object v7

    #@881
    const-string v9, "KT"

    #@883
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@886
    move-result v7

    #@887
    if-eqz v7, :cond_89a

    #@889
    .line 989
    :try_start_889
    const-string v7, "KT UCA"

    #@88b
    const-string v9, "KT UCA Service"

    #@88d
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@890
    .line 990
    const-string v7, "ktuca"

    #@892
    new-instance v9, Landroid/ktuca/KtUcaService;

    #@894
    invoke-direct {v9, v5}, Landroid/ktuca/KtUcaService;-><init>(Landroid/content/Context;)V

    #@897
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_89a
    .catch Ljava/lang/Throwable; {:try_start_889 .. :try_end_89a} :catch_cbb

    #@89a
    .line 998
    :cond_89a
    :goto_89a
    sget-boolean v7, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@89c
    if-eqz v7, :cond_8bf

    #@89e
    .line 1000
    move-object/from16 v0, v82

    #@8a0
    invoke-virtual {v0, v5}, Lcom/android/server/DeviceManager3LMService;->init(Landroid/content/Context;)V

    #@8a3
    .line 1003
    const-string v7, "ro.3LM.extended"

    #@8a5
    const/4 v9, 0x0

    #@8a6
    invoke-static {v7, v9}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@8a9
    move-result v7

    #@8aa
    const/4 v9, 0x1

    #@8ab
    if-ne v7, v9, :cond_8bf

    #@8ad
    .line 1004
    const-string v7, "SystemServer"

    #@8af
    const-string v9, "3LM OEM Extended Api Service"

    #@8b1
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8b4
    .line 1005
    const-string v7, "OemExtendedApi3LM"

    #@8b6
    new-instance v9, Lcom/android/server/OemExtendedApi3LMService;

    #@8b8
    const/4 v11, 0x0

    #@8b9
    invoke-direct {v9, v5, v11}, Lcom/android/server/OemExtendedApi3LMService;-><init>(Landroid/content/Context;Landroid/os/IDeviceManagerRestrictable3LM;)V

    #@8bc
    invoke-static {v7, v9}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@8bf
    .line 1014
    :cond_8bf
    :goto_8bf
    invoke-virtual/range {v164 .. v164}, Lcom/android/server/wm/WindowManagerService;->detectSafeMode()Z

    #@8c2
    move-result v47

    #@8c3
    .line 1015
    .local v47, safeMode:Z
    if-eqz v47, :cond_cc7

    #@8c5
    .line 1016
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    #@8c8
    move-result-object v7

    #@8c9
    invoke-virtual {v7}, Lcom/android/server/am/ActivityManagerService;->enterSafeMode()V

    #@8cc
    .line 1018
    const/4 v7, 0x1

    #@8cd
    sput-boolean v7, Ldalvik/system/Zygote;->systemInSafeMode:Z

    #@8cf
    .line 1020
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@8d2
    move-result-object v7

    #@8d3
    invoke-virtual {v7}, Ldalvik/system/VMRuntime;->disableJitCompilation()V

    #@8d6
    .line 1029
    :goto_8d6
    :try_start_8d6
    invoke-virtual/range {v150 .. v150}, Lcom/android/server/VibratorService;->systemReady()V
    :try_end_8d9
    .catch Ljava/lang/Throwable; {:try_start_8d6 .. :try_end_8d9} :catch_cd0

    #@8d9
    .line 1035
    :goto_8d9
    :try_start_8d9
    invoke-virtual/range {v103 .. v103}, Lcom/android/internal/widget/LockSettingsService;->systemReady()V
    :try_end_8dc
    .catch Ljava/lang/Throwable; {:try_start_8d9 .. :try_end_8dc} :catch_cdc

    #@8dc
    .line 1040
    :goto_8dc
    if-eqz v83, :cond_8e1

    #@8de
    .line 1042
    :try_start_8de
    invoke-virtual/range {v83 .. v83}, Lcom/android/server/DevicePolicyManagerService;->systemReady()V
    :try_end_8e1
    .catch Ljava/lang/Throwable; {:try_start_8de .. :try_end_8e1} :catch_ce8

    #@8e1
    .line 1048
    :cond_8e1
    :goto_8e1
    if-eqz v114, :cond_8e6

    #@8e3
    .line 1050
    :try_start_8e3
    invoke-virtual/range {v114 .. v114}, Lcom/android/server/NotificationManagerService;->systemReady()V
    :try_end_8e6
    .catch Ljava/lang/Throwable; {:try_start_8e3 .. :try_end_8e6} :catch_cf4

    #@8e6
    .line 1057
    :cond_8e6
    :goto_8e6
    :try_start_8e6
    invoke-virtual/range {v164 .. v164}, Lcom/android/server/wm/WindowManagerService;->systemReady()V
    :try_end_8e9
    .catch Ljava/lang/Throwable; {:try_start_8e6 .. :try_end_8e9} :catch_d00

    #@8e9
    .line 1062
    :goto_8e9
    if-eqz v47, :cond_8f2

    #@8eb
    .line 1063
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    #@8ee
    move-result-object v7

    #@8ef
    invoke-virtual {v7}, Lcom/android/server/am/ActivityManagerService;->showSafeModeOverlay()V

    #@8f2
    .line 1069
    :cond_8f2
    invoke-virtual/range {v164 .. v164}, Lcom/android/server/wm/WindowManagerService;->computeNewConfiguration()Landroid/content/res/Configuration;

    #@8f5
    move-result-object v74

    #@8f6
    .line 1070
    .local v74, config:Landroid/content/res/Configuration;
    new-instance v105, Landroid/util/DisplayMetrics;

    #@8f8
    invoke-direct/range {v105 .. v105}, Landroid/util/DisplayMetrics;-><init>()V

    #@8fb
    .line 1071
    .local v105, metrics:Landroid/util/DisplayMetrics;
    const-string v7, "window"

    #@8fd
    invoke-virtual {v5, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@900
    move-result-object v153

    #@901
    check-cast v153, Landroid/view/WindowManager;

    #@903
    .line 1072
    .local v153, w:Landroid/view/WindowManager;
    invoke-interface/range {v153 .. v153}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@906
    move-result-object v7

    #@907
    move-object/from16 v0, v105

    #@909
    invoke-virtual {v7, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@90c
    .line 1073
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@90f
    move-result-object v7

    #@910
    move-object/from16 v0, v74

    #@912
    move-object/from16 v1, v105

    #@914
    invoke-virtual {v7, v0, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    #@917
    .line 1076
    :try_start_917
    move-object/from16 v0, v142

    #@919
    move-object/from16 v1, v88

    #@91b
    invoke-virtual {v4, v0, v1}, Lcom/android/server/power/PowerManagerService;->systemReady(Lcom/android/server/TwilightService;Lcom/android/server/dreams/DreamManagerService;)V
    :try_end_91e
    .catch Ljava/lang/Throwable; {:try_start_917 .. :try_end_91e} :catch_d0c

    #@91e
    .line 1082
    :goto_91e
    :try_start_91e
    invoke-interface/range {v116 .. v116}, Landroid/content/pm/IPackageManager;->systemReady()V
    :try_end_921
    .catch Ljava/lang/Throwable; {:try_start_91e .. :try_end_921} :catch_d18

    #@921
    .line 1088
    :goto_921
    :try_start_921
    move/from16 v0, v47

    #@923
    move/from16 v1, v24

    #@925
    invoke-virtual {v10, v0, v1}, Lcom/android/server/display/DisplayManagerService;->systemReady(ZZ)V
    :try_end_928
    .catch Ljava/lang/Throwable; {:try_start_921 .. :try_end_928} :catch_d24

    #@928
    .line 1096
    :goto_928
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@92a
    if-eqz v7, :cond_936

    #@92c
    .line 1098
    :try_start_92c
    const-string v7, "SystemServer"

    #@92e
    const-string v9, "SplitWindowPolicy"

    #@930
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@933
    .line 1105
    invoke-static {v5}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->launchService(Landroid/content/Context;)V
    :try_end_936
    .catch Ljava/lang/Throwable; {:try_start_92c .. :try_end_936} :catch_d30

    #@936
    .line 1112
    :cond_936
    :goto_936
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@938
    if-eqz v7, :cond_944

    #@93a
    .line 1114
    :try_start_93a
    const-string v7, "SystemServer"

    #@93c
    const-string v9, "InteractionManagerService"

    #@93e
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@941
    .line 1115
    invoke-static {v5}, Lcom/lge/loader/interaction/InteractionManagerLoader;->launchService(Landroid/content/Context;)V
    :try_end_944
    .catch Ljava/lang/Throwable; {:try_start_93a .. :try_end_944} :catch_d3c

    #@944
    .line 1123
    :cond_944
    :goto_944
    move-object/from16 v34, v5

    #@946
    .line 1124
    .local v34, contextF:Landroid/content/Context;
    move-object/from16 v35, v106

    #@948
    .line 1125
    .local v35, mountServiceF:Lcom/android/server/MountService;
    move-object/from16 v36, v8

    #@94a
    .line 1126
    .local v36, batteryF:Lcom/android/server/BatteryService;
    move-object/from16 v37, v30

    #@94c
    .line 1127
    .local v37, networkManagementF:Lcom/android/server/NetworkManagementService;
    move-object/from16 v38, v29

    #@94e
    .line 1128
    .local v38, networkStatsF:Lcom/android/server/net/NetworkStatsService;
    move-object/from16 v39, v25

    #@950
    .line 1129
    .local v39, networkPolicyF:Lcom/android/server/net/NetworkPolicyManagerService;
    move-object/from16 v40, v75

    #@952
    .line 1130
    .local v40, connectivityF:Lcom/android/server/ConnectivityService;
    move-object/from16 v41, v86

    #@954
    .line 1131
    .local v41, dockF:Lcom/android/server/DockObserver;
    move-object/from16 v42, v147

    #@956
    .line 1132
    .local v42, usbF:Lcom/android/server/usb/UsbService;
    move-object/from16 v53, v137

    #@958
    .line 1133
    .local v53, throttleF:Lcom/android/server/ThrottleService;
    move-object/from16 v43, v142

    #@95a
    .line 1134
    .local v43, twilightF:Lcom/android/server/TwilightService;
    move-object/from16 v44, v145

    #@95c
    .line 1135
    .local v44, uiModeF:Lcom/android/server/UiModeManagerService;
    move-object/from16 v46, v65

    #@95e
    .line 1136
    .local v46, appWidgetF:Lcom/android/server/AppWidgetService;
    move-object/from16 v48, v154

    #@960
    .line 1137
    .local v48, wallpaperF:Lcom/android/server/WallpaperManagerService;
    move-object/from16 v49, v95

    #@962
    .line 1138
    .local v49, immF:Lcom/android/server/InputMethodManagerService;
    move-object/from16 v45, v124

    #@964
    .line 1139
    .local v45, recognitionF:Lcom/android/server/RecognitionManagerService;
    move-object/from16 v51, v101

    #@966
    .line 1140
    .local v51, locationF:Lcom/android/server/LocationManagerService;
    move-object/from16 v52, v78

    #@968
    .line 1141
    .local v52, countryDetectorF:Lcom/android/server/CountryDetectorService;
    move-object/from16 v54, v112

    #@96a
    .line 1142
    .local v54, networkTimeUpdaterF:Lcom/android/server/NetworkTimeUpdateService;
    move-object/from16 v55, v72

    #@96c
    .line 1143
    .local v55, commonTimeMgmtServiceF:Lcom/android/server/CommonTimeManagementService;
    move-object/from16 v56, v140

    #@96e
    .line 1144
    .local v56, textServiceManagerServiceF:Lcom/android/server/TextServicesManagerService;
    move-object/from16 v50, v132

    #@970
    .line 1145
    .local v50, statusBarF:Lcom/android/server/StatusBarManagerService;
    move-object/from16 v57, v88

    #@972
    .line 1146
    .local v57, dreamyF:Lcom/android/server/dreams/DreamManagerService;
    move-object/from16 v58, v19

    #@974
    .line 1147
    .local v58, inputManagerF:Lcom/android/server/input/InputManagerService;
    move-object/from16 v59, v135

    #@976
    .line 1148
    .local v59, telephonyRegistryF:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v60, v108

    #@978
    .line 1159
    .local v60, msimTelephonyRegistryF:Lcom/android/server/MSimTelephonyRegistry;
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    #@97b
    move-result-object v7

    #@97c
    new-instance v31, Lcom/android/server/ServerThread$3;

    #@97e
    move-object/from16 v32, p0

    #@980
    invoke-direct/range {v31 .. v60}, Lcom/android/server/ServerThread$3;-><init>(Lcom/android/server/ServerThread;ZLandroid/content/Context;Lcom/android/server/MountService;Lcom/android/server/BatteryService;Lcom/android/server/NetworkManagementService;Lcom/android/server/net/NetworkStatsService;Lcom/android/server/net/NetworkPolicyManagerService;Lcom/android/server/ConnectivityService;Lcom/android/server/DockObserver;Lcom/android/server/usb/UsbService;Lcom/android/server/TwilightService;Lcom/android/server/UiModeManagerService;Lcom/android/server/RecognitionManagerService;Lcom/android/server/AppWidgetService;ZLcom/android/server/WallpaperManagerService;Lcom/android/server/InputMethodManagerService;Lcom/android/server/StatusBarManagerService;Lcom/android/server/LocationManagerService;Lcom/android/server/CountryDetectorService;Lcom/android/server/ThrottleService;Lcom/android/server/NetworkTimeUpdateService;Lcom/android/server/CommonTimeManagementService;Lcom/android/server/TextServicesManagerService;Lcom/android/server/dreams/DreamManagerService;Lcom/android/server/input/InputManagerService;Lcom/android/server/TelephonyRegistry;Lcom/android/server/MSimTelephonyRegistry;)V

    #@983
    move-object/from16 v0, v31

    #@985
    invoke-virtual {v7, v0}, Lcom/android/server/am/ActivityManagerService;->systemReady(Ljava/lang/Runnable;)V

    #@988
    .line 1316
    invoke-static {}, Landroid/os/StrictMode;->conditionallyEnableDebugLogging()Z

    #@98b
    move-result v7

    #@98c
    if-eqz v7, :cond_995

    #@98e
    .line 1317
    const-string v7, "SystemServer"

    #@990
    const-string v9, "Enabled StrictMode for system server main thread."

    #@992
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@995
    .line 1320
    :cond_995
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@998
    .line 1321
    const-string v7, "SystemServer"

    #@99a
    const-string v9, "System ServerThread is exiting!"

    #@99c
    invoke-static {v7, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@99f
    .line 1322
    return-void

    #@9a0
    .line 148
    .end local v4           #power:Lcom/android/server/power/PowerManagerService;
    .end local v5           #context:Landroid/content/Context;
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v10           #display:Lcom/android/server/display/DisplayManagerService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v20           #uiHandler:Landroid/os/Handler;
    .end local v21           #wmHandler:Landroid/os/Handler;
    .end local v24           #onlyCore:Z
    .end local v25           #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v29           #networkStats:Lcom/android/server/net/NetworkStatsService;
    .end local v30           #networkManagement:Lcom/android/server/NetworkManagementService;
    .end local v33           #headless:Z
    .end local v34           #contextF:Landroid/content/Context;
    .end local v35           #mountServiceF:Lcom/android/server/MountService;
    .end local v36           #batteryF:Lcom/android/server/BatteryService;
    .end local v37           #networkManagementF:Lcom/android/server/NetworkManagementService;
    .end local v38           #networkStatsF:Lcom/android/server/net/NetworkStatsService;
    .end local v39           #networkPolicyF:Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v40           #connectivityF:Lcom/android/server/ConnectivityService;
    .end local v41           #dockF:Lcom/android/server/DockObserver;
    .end local v42           #usbF:Lcom/android/server/usb/UsbService;
    .end local v43           #twilightF:Lcom/android/server/TwilightService;
    .end local v44           #uiModeF:Lcom/android/server/UiModeManagerService;
    .end local v45           #recognitionF:Lcom/android/server/RecognitionManagerService;
    .end local v46           #appWidgetF:Lcom/android/server/AppWidgetService;
    .end local v47           #safeMode:Z
    .end local v48           #wallpaperF:Lcom/android/server/WallpaperManagerService;
    .end local v49           #immF:Lcom/android/server/InputMethodManagerService;
    .end local v50           #statusBarF:Lcom/android/server/StatusBarManagerService;
    .end local v51           #locationF:Lcom/android/server/LocationManagerService;
    .end local v52           #countryDetectorF:Lcom/android/server/CountryDetectorService;
    .end local v53           #throttleF:Lcom/android/server/ThrottleService;
    .end local v54           #networkTimeUpdaterF:Lcom/android/server/NetworkTimeUpdateService;
    .end local v55           #commonTimeMgmtServiceF:Lcom/android/server/CommonTimeManagementService;
    .end local v56           #textServiceManagerServiceF:Lcom/android/server/TextServicesManagerService;
    .end local v57           #dreamyF:Lcom/android/server/dreams/DreamManagerService;
    .end local v58           #inputManagerF:Lcom/android/server/input/InputManagerService;
    .end local v59           #telephonyRegistryF:Lcom/android/server/TelephonyRegistry;
    .end local v60           #msimTelephonyRegistryF:Lcom/android/server/MSimTelephonyRegistry;
    .end local v62           #accountManager:Landroid/accounts/AccountManagerService;
    .end local v65           #appWidget:Lcom/android/server/AppWidgetService;
    .end local v70           #bluetooth:Lcom/android/server/BluetoothManagerService;
    .end local v72           #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    .end local v74           #config:Landroid/content/res/Configuration;
    .end local v75           #connectivity:Lcom/android/server/ConnectivityService;
    .end local v77           #contentService:Landroid/content/ContentService;
    .end local v78           #countryDetector:Lcom/android/server/CountryDetectorService;
    .end local v82           #deviceManager:Lcom/android/server/DeviceManager3LMService;
    .end local v83           #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .end local v86           #dock:Lcom/android/server/DockObserver;
    .end local v88           #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .end local v92           #factoryTest:I
    .end local v95           #imm:Lcom/android/server/InputMethodManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v101           #location:Lcom/android/server/LocationManagerService;
    .end local v103           #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .end local v105           #metrics:Landroid/util/DisplayMetrics;
    .end local v106           #mountService:Lcom/android/server/MountService;
    .end local v108           #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    .end local v112           #networkTimeUpdater:Lcom/android/server/NetworkTimeUpdateService;
    .end local v114           #notification:Lcom/android/server/NotificationManagerService;
    .end local v116           #pm:Landroid/content/pm/IPackageManager;
    .end local v124           #recognition:Lcom/android/server/RecognitionManagerService;
    .end local v126           #regulatory:Lcom/android/server/RegulatoryObserver;
    .end local v128           #serial:Lcom/android/server/SerialService;
    .end local v130           #serviceDiscovery:Lcom/android/server/NsdService;
    .end local v132           #statusBar:Lcom/android/server/StatusBarManagerService;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .end local v137           #throttle:Lcom/android/server/ThrottleService;
    .end local v140           #tsms:Lcom/android/server/TextServicesManagerService;
    .end local v142           #twilight:Lcom/android/server/TwilightService;
    .end local v144           #uiHandlerThread:Landroid/os/HandlerThread;
    .end local v145           #uiMode:Lcom/android/server/UiModeManagerService;
    .end local v147           #usb:Lcom/android/server/usb/UsbService;
    .end local v150           #vibrator:Lcom/android/server/VibratorService;
    .end local v153           #w:Landroid/view/WindowManager;
    .end local v154           #wallpaper:Lcom/android/server/WallpaperManagerService;
    .end local v156           #wifi:Lcom/android/server/WifiService;
    .end local v162           #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    .end local v164           #wm:Lcom/android/server/wm/WindowManagerService;
    .end local v165           #wmHandlerThread:Landroid/os/HandlerThread;
    :cond_9a0
    const/16 v123, 0x0

    #@9a2
    goto/16 :goto_35

    #@9a4
    .line 154
    .restart local v123       #reboot:Z
    :cond_9a4
    const/16 v122, 0x0

    #@9a6
    .restart local v122       #reason:Ljava/lang/String;
    goto/16 :goto_47

    #@9a8
    .line 307
    .end local v122           #reason:Ljava/lang/String;
    .end local v123           #reboot:Z
    .restart local v4       #power:Lcom/android/server/power/PowerManagerService;
    .restart local v5       #context:Landroid/content/Context;
    .restart local v10       #display:Lcom/android/server/display/DisplayManagerService;
    .restart local v20       #uiHandler:Landroid/os/Handler;
    .restart local v21       #wmHandler:Landroid/os/Handler;
    .restart local v24       #onlyCore:Z
    .restart local v29       #networkStats:Lcom/android/server/net/NetworkStatsService;
    .restart local v30       #networkManagement:Lcom/android/server/NetworkManagementService;
    .restart local v33       #headless:Z
    .restart local v62       #accountManager:Landroid/accounts/AccountManagerService;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v70       #bluetooth:Lcom/android/server/BluetoothManagerService;
    .restart local v72       #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    .restart local v75       #connectivity:Lcom/android/server/ConnectivityService;
    .restart local v77       #contentService:Landroid/content/ContentService;
    .restart local v80       #cryptState:Ljava/lang/String;
    .restart local v82       #deviceManager:Lcom/android/server/DeviceManager3LMService;
    .restart local v86       #dock:Lcom/android/server/DockObserver;
    .restart local v92       #factoryTest:I
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v106       #mountService:Lcom/android/server/MountService;
    .restart local v108       #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    .restart local v110       #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v112       #networkTimeUpdater:Lcom/android/server/NetworkTimeUpdateService;
    .restart local v116       #pm:Landroid/content/pm/IPackageManager;
    .restart local v118       #qcCon:Ljava/lang/Object;
    .restart local v124       #recognition:Lcom/android/server/RecognitionManagerService;
    .restart local v126       #regulatory:Lcom/android/server/RegulatoryObserver;
    .restart local v128       #serial:Lcom/android/server/SerialService;
    .restart local v130       #serviceDiscovery:Lcom/android/server/NsdService;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v137       #throttle:Lcom/android/server/ThrottleService;
    .restart local v142       #twilight:Lcom/android/server/TwilightService;
    .restart local v144       #uiHandlerThread:Landroid/os/HandlerThread;
    .restart local v145       #uiMode:Lcom/android/server/UiModeManagerService;
    .restart local v147       #usb:Lcom/android/server/usb/UsbService;
    .restart local v150       #vibrator:Lcom/android/server/VibratorService;
    .restart local v156       #wifi:Lcom/android/server/WifiService;
    .restart local v162       #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    .restart local v164       #wm:Lcom/android/server/wm/WindowManagerService;
    .restart local v165       #wmHandlerThread:Landroid/os/HandlerThread;
    :cond_9a8
    :try_start_9a8
    const-string v7, "1"

    #@9aa
    move-object/from16 v0, v80

    #@9ac
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9af
    move-result v7

    #@9b0
    if-eqz v7, :cond_1d1

    #@9b2
    .line 308
    const-string v7, "SystemServer"

    #@9b4
    const-string v9, "Device encrypted - only parsing core apps"

    #@9b6
    invoke-static {v7, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9b9
    .line 309
    const/16 v24, 0x1

    #@9bb
    goto/16 :goto_1d1

    #@9bd
    .line 321
    :cond_9bd
    const/4 v7, 0x0

    #@9be
    goto/16 :goto_1d9

    #@9c0
    .line 355
    .restart local v93       #firstBoot:Z
    :catch_9c0
    move-exception v90

    #@9c1
    .line 356
    .local v90, e:Ljava/lang/Throwable;
    :goto_9c1
    const-string v7, "SystemServer"

    #@9c3
    const-string v9, "Failure starting Account Manager"

    #@9c5
    move-object/from16 v0, v90

    #@9c7
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9ca
    .catch Ljava/lang/RuntimeException; {:try_start_9a8 .. :try_end_9ca} :catch_9cc

    #@9ca
    goto/16 :goto_21d

    #@9cc
    .line 422
    .end local v80           #cryptState:Ljava/lang/String;
    .end local v90           #e:Ljava/lang/Throwable;
    .end local v93           #firstBoot:Z
    :catch_9cc
    move-exception v90

    #@9cd
    move-object/from16 v135, v136

    #@9cf
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v19, v97

    #@9d1
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@9d3
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@9d5
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v6, v100

    #@9d7
    .end local v100           #lights:Lcom/android/server/LightsService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    move-object/from16 v98, v99

    #@9d9
    .line 423
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .local v90, e:Ljava/lang/RuntimeException;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    :goto_9d9
    const-string v7, "System"

    #@9db
    const-string v9, "******************************************"

    #@9dd
    invoke-static {v7, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9e0
    .line 424
    const-string v7, "System"

    #@9e2
    const-string v9, "************ Failure starting core service"

    #@9e4
    move-object/from16 v0, v90

    #@9e6
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9e9
    goto/16 :goto_30f

    #@9eb
    .line 360
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v90           #e:Ljava/lang/RuntimeException;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v80       #cryptState:Ljava/lang/String;
    .restart local v93       #firstBoot:Z
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :cond_9eb
    const/4 v7, 0x0

    #@9ec
    goto/16 :goto_22a

    #@9ee
    .line 394
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v100           #lights:Lcom/android/server/LightsService;
    .end local v150           #vibrator:Lcom/android/server/VibratorService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v151       #vibrator:Lcom/android/server/VibratorService;
    :cond_9ee
    const/16 v22, 0x0

    #@9f0
    goto/16 :goto_2b9

    #@9f2
    :cond_9f2
    const/16 v23, 0x0

    #@9f4
    goto/16 :goto_2bd

    #@9f6
    .line 414
    :cond_9f6
    const/4 v7, 0x1

    #@9f7
    move/from16 v0, v92

    #@9f9
    if-ne v0, v7, :cond_a0c

    #@9fb
    .line 415
    :try_start_9fb
    const-string v7, "SystemServer"

    #@9fd
    const-string v9, "No Bluetooth Service (factory test)"

    #@9ff
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a02
    goto/16 :goto_309

    #@a04
    .line 422
    :catch_a04
    move-exception v90

    #@a05
    move-object/from16 v135, v136

    #@a07
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v150, v151

    #@a09
    .end local v151           #vibrator:Lcom/android/server/VibratorService;
    .restart local v150       #vibrator:Lcom/android/server/VibratorService;
    move-object/from16 v98, v99

    #@a0b
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto :goto_9d9

    #@a0c
    .line 417
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .end local v150           #vibrator:Lcom/android/server/VibratorService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v151       #vibrator:Lcom/android/server/VibratorService;
    :cond_a0c
    const-string v7, "SystemServer"

    #@a0e
    const-string v9, "Bluetooth Manager Service"

    #@a10
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a13
    .line 418
    new-instance v71, Lcom/android/server/BluetoothManagerService;

    #@a15
    move-object/from16 v0, v71

    #@a17
    invoke-direct {v0, v5}, Lcom/android/server/BluetoothManagerService;-><init>(Landroid/content/Context;)V
    :try_end_a1a
    .catch Ljava/lang/RuntimeException; {:try_start_9fb .. :try_end_a1a} :catch_a04

    #@a1a
    .line 419
    .end local v70           #bluetooth:Lcom/android/server/BluetoothManagerService;
    .local v71, bluetooth:Lcom/android/server/BluetoothManagerService;
    :try_start_a1a
    const-string v7, "bluetooth_manager"

    #@a1c
    move-object/from16 v0, v71

    #@a1e
    invoke-static {v7, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_a21
    .catch Ljava/lang/RuntimeException; {:try_start_a1a .. :try_end_a21} :catch_e3e

    #@a21
    move-object/from16 v70, v71

    #@a23
    .end local v71           #bluetooth:Lcom/android/server/BluetoothManagerService;
    .restart local v70       #bluetooth:Lcom/android/server/BluetoothManagerService;
    goto/16 :goto_309

    #@a25
    .line 450
    .end local v80           #cryptState:Ljava/lang/String;
    .end local v93           #firstBoot:Z
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .end local v151           #vibrator:Lcom/android/server/VibratorService;
    .restart local v65       #appWidget:Lcom/android/server/AppWidgetService;
    .restart local v78       #countryDetector:Lcom/android/server/CountryDetectorService;
    .restart local v83       #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .restart local v88       #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .restart local v95       #imm:Lcom/android/server/InputMethodManagerService;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    .restart local v101       #location:Lcom/android/server/LocationManagerService;
    .restart local v103       #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .restart local v114       #notification:Lcom/android/server/NotificationManagerService;
    .restart local v132       #statusBar:Lcom/android/server/StatusBarManagerService;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v140       #tsms:Lcom/android/server/TextServicesManagerService;
    .restart local v150       #vibrator:Lcom/android/server/VibratorService;
    .local v152, vzwLocMgrObj:Ljava/lang/Object;
    .restart local v154       #wallpaper:Lcom/android/server/WallpaperManagerService;
    :catch_a25
    move-exception v90

    #@a26
    .line 451
    .local v90, e:Ljava/lang/Throwable;
    :goto_a26
    const-string v7, "starting Input Manager Service"

    #@a28
    move-object/from16 v0, p0

    #@a2a
    move-object/from16 v1, v90

    #@a2c
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a2f
    goto/16 :goto_345

    #@a31
    .line 458
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a31
    move-exception v90

    #@a32
    .line 459
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting Accessibility Manager"

    #@a34
    move-object/from16 v0, p0

    #@a36
    move-object/from16 v1, v90

    #@a38
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a3b
    goto/16 :goto_356

    #@a3d
    .line 465
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a3d
    move-exception v90

    #@a3e
    .line 466
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making display ready"

    #@a40
    move-object/from16 v0, p0

    #@a42
    move-object/from16 v1, v90

    #@a44
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a47
    goto/16 :goto_359

    #@a49
    .line 471
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a49
    move-exception v90

    #@a4a
    .line 472
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "performing boot dexopt"

    #@a4c
    move-object/from16 v0, p0

    #@a4e
    move-object/from16 v1, v90

    #@a50
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a53
    goto/16 :goto_35c

    #@a55
    .line 493
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a55
    move-exception v90

    #@a56
    .line 494
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_a56
    const-string v7, "starting Mount Service"

    #@a58
    move-object/from16 v0, p0

    #@a5a
    move-object/from16 v1, v90

    #@a5c
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a5f
    goto/16 :goto_399

    #@a61
    .line 502
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a61
    move-exception v90

    #@a62
    .line 503
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_a62
    const-string v7, "starting LockSettingsService service"

    #@a64
    move-object/from16 v0, p0

    #@a66
    move-object/from16 v1, v90

    #@a68
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a6b
    goto/16 :goto_3b0

    #@a6d
    .line 510
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a6d
    move-exception v90

    #@a6e
    .line 511
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_a6e
    const-string v7, "starting DevicePolicyService"

    #@a70
    move-object/from16 v0, p0

    #@a72
    move-object/from16 v1, v90

    #@a74
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a77
    goto/16 :goto_3c7

    #@a79
    .line 524
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a79
    move-exception v90

    #@a7a
    .line 525
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_a7a
    const-string v7, "starting StatusBarManagerService"

    #@a7c
    move-object/from16 v0, p0

    #@a7e
    move-object/from16 v1, v90

    #@a80
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a83
    goto/16 :goto_3ed

    #@a85
    .line 532
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a85
    move-exception v90

    #@a86
    .line 533
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting Clipboard Service"

    #@a88
    move-object/from16 v0, p0

    #@a8a
    move-object/from16 v1, v90

    #@a8c
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a8f
    goto/16 :goto_3fe

    #@a91
    .line 540
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a91
    move-exception v90

    #@a92
    .line 541
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting NetworkManagement Service"

    #@a94
    move-object/from16 v0, p0

    #@a96
    move-object/from16 v1, v90

    #@a98
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a9b
    goto/16 :goto_410

    #@a9d
    .line 548
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_a9d
    move-exception v90

    #@a9e
    .line 549
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_a9e
    const-string v7, "starting Text Service Manager Service"

    #@aa0
    move-object/from16 v0, p0

    #@aa2
    move-object/from16 v1, v90

    #@aa4
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@aa7
    goto/16 :goto_427

    #@aa9
    .line 556
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_aa9
    move-exception v90

    #@aaa
    .line 557
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_aaa
    const-string v7, "starting NetworkStats Service"

    #@aac
    move-object/from16 v0, p0

    #@aae
    move-object/from16 v1, v90

    #@ab0
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ab3
    goto/16 :goto_440

    #@ab5
    .line 566
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_ab5
    move-exception v90

    #@ab6
    move-object/from16 v25, v110

    #@ab8
    .line 567
    .end local v110           #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v25       #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_ab8
    const-string v7, "starting NetworkPolicy Service"

    #@aba
    move-object/from16 v0, p0

    #@abc
    move-object/from16 v1, v90

    #@abe
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ac1
    goto/16 :goto_45b

    #@ac3
    .line 574
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_ac3
    move-exception v90

    #@ac4
    .line 575
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting RegulatoryObserver"

    #@ac6
    move-object/from16 v0, p0

    #@ac8
    move-object/from16 v1, v90

    #@aca
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@acd
    goto/16 :goto_46b

    #@acf
    .line 582
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_acf
    move-exception v90

    #@ad0
    .line 583
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_ad0
    const-string v7, "starting Wi-Fi P2pService"

    #@ad2
    move-object/from16 v0, p0

    #@ad4
    move-object/from16 v1, v90

    #@ad6
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ad9
    goto/16 :goto_482

    #@adb
    .line 589
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_adb
    move-exception v90

    #@adc
    .line 590
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_adc
    const-string v7, "starting Wi-Fi Service"

    #@ade
    move-object/from16 v0, p0

    #@ae0
    move-object/from16 v1, v90

    #@ae2
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ae5
    goto/16 :goto_499

    #@ae7
    .line 602
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_ae7
    move-exception v90

    #@ae8
    .line 603
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@aea
    const-string v9, "Failure starting WiFi Offloading Service"

    #@aec
    move-object/from16 v0, v90

    #@aee
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@af1
    goto/16 :goto_4b9

    #@af3
    .line 616
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_af3
    move-exception v90

    #@af4
    .line 617
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@af6
    const-string v9, "Failure starting WiFi Aggregation Service"

    #@af8
    move-object/from16 v0, v90

    #@afa
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@afd
    goto/16 :goto_4d9

    #@aff
    .line 630
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_aff
    move-exception v90

    #@b00
    .line 631
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@b02
    const-string v9, "Failure starting Wifi Ext Service"

    #@b04
    move-object/from16 v0, v90

    #@b06
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b09
    goto/16 :goto_4f3

    #@b0b
    .line 644
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b0b
    move-exception v90

    #@b0c
    .line 645
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@b0e
    const-string v9, "Failure starting Mobile Hotspot Service"

    #@b10
    move-object/from16 v0, v90

    #@b12
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b15
    goto/16 :goto_513

    #@b17
    .line 666
    .end local v90           #e:Ljava/lang/Throwable;
    .restart local v149       #value:I
    :cond_b17
    :try_start_b17
    const-string v7, "SystemServer"

    #@b19
    const-string v9, "Connectivity Service"

    #@b1b
    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b1e
    .line 667
    new-instance v76, Lcom/android/server/ConnectivityService;

    #@b20
    move-object/from16 v0, v76

    #@b22
    move-object/from16 v1, v30

    #@b24
    move-object/from16 v2, v29

    #@b26
    move-object/from16 v3, v25

    #@b28
    invoke-direct {v0, v5, v1, v2, v3}, Lcom/android/server/ConnectivityService;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/net/INetworkStatsService;Landroid/net/INetworkPolicyManager;)V
    :try_end_b2b
    .catch Ljava/lang/Throwable; {:try_start_b17 .. :try_end_b2b} :catch_b2f

    #@b2b
    .end local v75           #connectivity:Lcom/android/server/ConnectivityService;
    .local v76, connectivity:Lcom/android/server/ConnectivityService;
    move-object/from16 v75, v76

    #@b2d
    .end local v76           #connectivity:Lcom/android/server/ConnectivityService;
    .restart local v75       #connectivity:Lcom/android/server/ConnectivityService;
    goto/16 :goto_575

    #@b2f
    .line 677
    .end local v118           #qcCon:Ljava/lang/Object;
    .end local v149           #value:I
    :catch_b2f
    move-exception v90

    #@b30
    .line 678
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting Connectivity Service"

    #@b32
    move-object/from16 v0, p0

    #@b34
    move-object/from16 v1, v90

    #@b36
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b39
    goto/16 :goto_592

    #@b3b
    .line 686
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b3b
    move-exception v90

    #@b3c
    .line 687
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting Service Discovery Service"

    #@b3e
    move-object/from16 v0, p0

    #@b40
    move-object/from16 v1, v90

    #@b42
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b45
    goto/16 :goto_5a4

    #@b47
    .line 695
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b47
    move-exception v90

    #@b48
    .line 696
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_b48
    const-string v7, "starting ThrottleService"

    #@b4a
    move-object/from16 v0, p0

    #@b4c
    move-object/from16 v1, v90

    #@b4e
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b51
    goto/16 :goto_5bb

    #@b53
    .line 703
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b53
    move-exception v90

    #@b54
    .line 704
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting UpdateLockService"

    #@b56
    move-object/from16 v0, p0

    #@b58
    move-object/from16 v1, v90

    #@b5a
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b5d
    goto/16 :goto_5cc

    #@b5f
    .line 719
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b5f
    move-exception v90

    #@b60
    .line 720
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Account Manager Service ready"

    #@b62
    move-object/from16 v0, p0

    #@b64
    move-object/from16 v1, v90

    #@b66
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b69
    goto/16 :goto_5d6

    #@b6b
    .line 726
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b6b
    move-exception v90

    #@b6c
    .line 727
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Content Service ready"

    #@b6e
    move-object/from16 v0, p0

    #@b70
    move-object/from16 v1, v90

    #@b72
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b75
    goto/16 :goto_5db

    #@b77
    .line 735
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b77
    move-exception v90

    #@b78
    .line 736
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_b78
    const-string v7, "starting Notification Manager"

    #@b7a
    move-object/from16 v0, p0

    #@b7c
    move-object/from16 v1, v90

    #@b7e
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b81
    goto/16 :goto_5fb

    #@b83
    .line 743
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b83
    move-exception v90

    #@b84
    .line 744
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting DeviceStorageMonitor service"

    #@b86
    move-object/from16 v0, p0

    #@b88
    move-object/from16 v1, v90

    #@b8a
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b8d
    goto/16 :goto_60c

    #@b8f
    .line 751
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b8f
    move-exception v90

    #@b90
    .line 752
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_b90
    const-string v7, "starting Location Manager"

    #@b92
    move-object/from16 v0, p0

    #@b94
    move-object/from16 v1, v90

    #@b96
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b99
    goto/16 :goto_623

    #@b9b
    .line 759
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_b9b
    move-exception v90

    #@b9c
    .line 760
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_b9c
    const-string v7, "starting Country Detector"

    #@b9e
    move-object/from16 v0, p0

    #@ba0
    move-object/from16 v1, v90

    #@ba2
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ba5
    goto/16 :goto_63a

    #@ba7
    .line 767
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_ba7
    move-exception v90

    #@ba8
    .line 768
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting Search Service"

    #@baa
    move-object/from16 v0, p0

    #@bac
    move-object/from16 v1, v90

    #@bae
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@bb1
    goto/16 :goto_64b

    #@bb3
    .line 775
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_bb3
    move-exception v90

    #@bb4
    .line 776
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting DropBoxManagerService"

    #@bb6
    move-object/from16 v0, p0

    #@bb8
    move-object/from16 v1, v90

    #@bba
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@bbd
    goto/16 :goto_663

    #@bbf
    .line 787
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_bbf
    move-exception v90

    #@bc0
    .line 788
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_bc0
    const-string v7, "starting Wallpaper Service"

    #@bc2
    move-object/from16 v0, p0

    #@bc4
    move-object/from16 v1, v90

    #@bc6
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@bc9
    goto/16 :goto_689

    #@bcb
    .line 796
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_bcb
    move-exception v90

    #@bcc
    .line 797
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting Audio Service"

    #@bce
    move-object/from16 v0, p0

    #@bd0
    move-object/from16 v1, v90

    #@bd2
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@bd5
    goto/16 :goto_6a8

    #@bd7
    .line 805
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_bd7
    move-exception v90

    #@bd8
    .line 806
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting DockObserver"

    #@bda
    move-object/from16 v0, p0

    #@bdc
    move-object/from16 v1, v90

    #@bde
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@be1
    goto/16 :goto_6b8

    #@be3
    .line 814
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_be3
    move-exception v90

    #@be4
    .line 815
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting WiredAccessoryManager"

    #@be6
    move-object/from16 v0, p0

    #@be8
    move-object/from16 v1, v90

    #@bea
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@bed
    goto/16 :goto_6cb

    #@bef
    .line 823
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_bef
    move-exception v90

    #@bf0
    .line 824
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_bf0
    const-string v7, "starting UsbService"

    #@bf2
    move-object/from16 v0, p0

    #@bf4
    move-object/from16 v1, v90

    #@bf6
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@bf9
    goto/16 :goto_6e2

    #@bfb
    .line 832
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_bfb
    move-exception v90

    #@bfc
    .line 833
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_bfc
    const-string v7, "SystemServer"

    #@bfe
    const-string v9, "Failure starting SerialService"

    #@c00
    move-object/from16 v0, v90

    #@c02
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c05
    goto/16 :goto_6f9

    #@c07
    .line 839
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c07
    move-exception v90

    #@c08
    .line 840
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting TwilightService"

    #@c0a
    move-object/from16 v0, p0

    #@c0c
    move-object/from16 v1, v90

    #@c0e
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c11
    goto/16 :goto_709

    #@c13
    .line 847
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c13
    move-exception v90

    #@c14
    .line 848
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting UiModeManagerService"

    #@c16
    move-object/from16 v0, p0

    #@c18
    move-object/from16 v1, v90

    #@c1a
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c1d
    goto/16 :goto_71b

    #@c1f
    .line 855
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c1f
    move-exception v90

    #@c20
    .line 856
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@c22
    const-string v9, "Failure starting Backup Service"

    #@c24
    move-object/from16 v0, v90

    #@c26
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c29
    goto/16 :goto_72c

    #@c2b
    .line 863
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c2b
    move-exception v90

    #@c2c
    .line 864
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_c2c
    const-string v7, "starting AppWidget Service"

    #@c2e
    move-object/from16 v0, p0

    #@c30
    move-object/from16 v1, v90

    #@c32
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c35
    goto/16 :goto_743

    #@c37
    .line 870
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c37
    move-exception v90

    #@c38
    .line 871
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting Recognition Service"

    #@c3a
    move-object/from16 v0, p0

    #@c3c
    move-object/from16 v1, v90

    #@c3e
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c41
    goto/16 :goto_753

    #@c43
    .line 877
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c43
    move-exception v90

    #@c44
    .line 878
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting DiskStats Service"

    #@c46
    move-object/from16 v0, p0

    #@c48
    move-object/from16 v1, v90

    #@c4a
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c4d
    goto/16 :goto_764

    #@c4f
    .line 885
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c4f
    move-exception v90

    #@c50
    .line 886
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting AtCmdFwd Service"

    #@c52
    move-object/from16 v0, p0

    #@c54
    move-object/from16 v1, v90

    #@c56
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c59
    goto/16 :goto_779

    #@c5b
    .line 897
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c5b
    move-exception v90

    #@c5c
    .line 898
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting SamplingProfiler Service"

    #@c5e
    move-object/from16 v0, p0

    #@c60
    move-object/from16 v1, v90

    #@c62
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c65
    goto/16 :goto_78a

    #@c67
    .line 904
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c67
    move-exception v90

    #@c68
    .line 905
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting NetworkTimeUpdate service"

    #@c6a
    move-object/from16 v0, p0

    #@c6c
    move-object/from16 v1, v90

    #@c6e
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c71
    goto/16 :goto_79a

    #@c73
    .line 912
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c73
    move-exception v90

    #@c74
    .line 913
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_c74
    const-string v7, "starting CommonTimeManagementService service"

    #@c76
    move-object/from16 v0, p0

    #@c78
    move-object/from16 v1, v90

    #@c7a
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c7d
    goto/16 :goto_7b1

    #@c7f
    .line 919
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c7f
    move-exception v90

    #@c80
    .line 920
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "starting CertBlacklister"

    #@c82
    move-object/from16 v0, p0

    #@c84
    move-object/from16 v1, v90

    #@c86
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c89
    goto/16 :goto_7bd

    #@c8b
    .line 930
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c8b
    move-exception v90

    #@c8c
    .line 931
    .restart local v90       #e:Ljava/lang/Throwable;
    :goto_c8c
    const-string v7, "starting DreamManagerService"

    #@c8e
    move-object/from16 v0, p0

    #@c90
    move-object/from16 v1, v90

    #@c92
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c95
    goto/16 :goto_7e3

    #@c97
    .line 941
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_c97
    move-exception v90

    #@c98
    .line 942
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@c9a
    const-string v9, "Failure starting ThemeIconManager"

    #@c9c
    move-object/from16 v0, v90

    #@c9e
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ca1
    goto/16 :goto_7f8

    #@ca3
    .line 953
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_ca3
    move-exception v90

    #@ca4
    .line 954
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "vzwconnectivity"

    #@ca6
    const-string v9, "Failure starting vzwconnectivity service"

    #@ca8
    move-object/from16 v0, v90

    #@caa
    invoke-static {v7, v9, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@cad
    goto/16 :goto_817

    #@caf
    .line 978
    .end local v90           #e:Ljava/lang/Throwable;
    .end local v152           #vzwLocMgrObj:Ljava/lang/Object;
    :catch_caf
    move-exception v91

    #@cb0
    .line 979
    .local v91, ex:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@cb2
    const-string v9, "Unable to load Vzw LocationManager Service or register as System Service"

    #@cb4
    move-object/from16 v0, v91

    #@cb6
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@cb9
    goto/16 :goto_87d

    #@cbb
    .line 992
    .end local v91           #ex:Ljava/lang/Throwable;
    :catch_cbb
    move-exception v90

    #@cbc
    .line 993
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "KT UCA"

    #@cbe
    const-string v9, "Failure starting KT UCA Service"

    #@cc0
    move-object/from16 v0, v90

    #@cc2
    invoke-static {v7, v9, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@cc5
    goto/16 :goto_89a

    #@cc7
    .line 1023
    .end local v90           #e:Ljava/lang/Throwable;
    .restart local v47       #safeMode:Z
    :cond_cc7
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@cca
    move-result-object v7

    #@ccb
    invoke-virtual {v7}, Ldalvik/system/VMRuntime;->startJitCompilation()V

    #@cce
    goto/16 :goto_8d6

    #@cd0
    .line 1030
    :catch_cd0
    move-exception v90

    #@cd1
    .line 1031
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Vibrator Service ready"

    #@cd3
    move-object/from16 v0, p0

    #@cd5
    move-object/from16 v1, v90

    #@cd7
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@cda
    goto/16 :goto_8d9

    #@cdc
    .line 1036
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_cdc
    move-exception v90

    #@cdd
    .line 1037
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Lock Settings Service ready"

    #@cdf
    move-object/from16 v0, p0

    #@ce1
    move-object/from16 v1, v90

    #@ce3
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ce6
    goto/16 :goto_8dc

    #@ce8
    .line 1043
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_ce8
    move-exception v90

    #@ce9
    .line 1044
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Device Policy Service ready"

    #@ceb
    move-object/from16 v0, p0

    #@ced
    move-object/from16 v1, v90

    #@cef
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@cf2
    goto/16 :goto_8e1

    #@cf4
    .line 1051
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_cf4
    move-exception v90

    #@cf5
    .line 1052
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Notification Service ready"

    #@cf7
    move-object/from16 v0, p0

    #@cf9
    move-object/from16 v1, v90

    #@cfb
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@cfe
    goto/16 :goto_8e6

    #@d00
    .line 1058
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_d00
    move-exception v90

    #@d01
    .line 1059
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Window Manager Service ready"

    #@d03
    move-object/from16 v0, p0

    #@d05
    move-object/from16 v1, v90

    #@d07
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d0a
    goto/16 :goto_8e9

    #@d0c
    .line 1077
    .end local v90           #e:Ljava/lang/Throwable;
    .restart local v74       #config:Landroid/content/res/Configuration;
    .restart local v105       #metrics:Landroid/util/DisplayMetrics;
    .restart local v153       #w:Landroid/view/WindowManager;
    :catch_d0c
    move-exception v90

    #@d0d
    .line 1078
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Power Manager Service ready"

    #@d0f
    move-object/from16 v0, p0

    #@d11
    move-object/from16 v1, v90

    #@d13
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d16
    goto/16 :goto_91e

    #@d18
    .line 1083
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_d18
    move-exception v90

    #@d19
    .line 1084
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Package Manager Service ready"

    #@d1b
    move-object/from16 v0, p0

    #@d1d
    move-object/from16 v1, v90

    #@d1f
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d22
    goto/16 :goto_921

    #@d24
    .line 1089
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_d24
    move-exception v90

    #@d25
    .line 1090
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "making Display Manager Service ready"

    #@d27
    move-object/from16 v0, p0

    #@d29
    move-object/from16 v1, v90

    #@d2b
    invoke-virtual {v0, v7, v1}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d2e
    goto/16 :goto_928

    #@d30
    .line 1106
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_d30
    move-exception v90

    #@d31
    .line 1107
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@d33
    const-string v9, "Failure starting SplitWindowPolicy"

    #@d35
    move-object/from16 v0, v90

    #@d37
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d3a
    goto/16 :goto_936

    #@d3c
    .line 1116
    .end local v90           #e:Ljava/lang/Throwable;
    :catch_d3c
    move-exception v90

    #@d3d
    .line 1117
    .restart local v90       #e:Ljava/lang/Throwable;
    const-string v7, "SystemServer"

    #@d3f
    const-string v9, "Failure starting InteractionManagerService"

    #@d41
    move-object/from16 v0, v90

    #@d43
    invoke-static {v7, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d46
    goto/16 :goto_944

    #@d48
    .line 338
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v25           #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v47           #safeMode:Z
    .end local v65           #appWidget:Lcom/android/server/AppWidgetService;
    .end local v74           #config:Landroid/content/res/Configuration;
    .end local v78           #countryDetector:Lcom/android/server/CountryDetectorService;
    .end local v83           #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .end local v88           #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .end local v90           #e:Ljava/lang/Throwable;
    .end local v95           #imm:Lcom/android/server/InputMethodManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v101           #location:Lcom/android/server/LocationManagerService;
    .end local v103           #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .end local v105           #metrics:Landroid/util/DisplayMetrics;
    .end local v114           #notification:Lcom/android/server/NotificationManagerService;
    .end local v132           #statusBar:Lcom/android/server/StatusBarManagerService;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .end local v140           #tsms:Lcom/android/server/TextServicesManagerService;
    .end local v153           #w:Landroid/view/WindowManager;
    .end local v154           #wallpaper:Lcom/android/server/WallpaperManagerService;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v80       #cryptState:Ljava/lang/String;
    .restart local v93       #firstBoot:Z
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v110       #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v118       #qcCon:Ljava/lang/Object;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :catch_d48
    move-exception v7

    #@d49
    goto/16 :goto_1eb

    #@d4b
    .line 930
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .end local v80           #cryptState:Ljava/lang/String;
    .end local v93           #firstBoot:Z
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .end local v100           #lights:Lcom/android/server/LightsService;
    .end local v110           #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v118           #qcCon:Ljava/lang/Object;
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v25       #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v65       #appWidget:Lcom/android/server/AppWidgetService;
    .restart local v78       #countryDetector:Lcom/android/server/CountryDetectorService;
    .restart local v83       #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .restart local v89       #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .restart local v95       #imm:Lcom/android/server/InputMethodManagerService;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    .restart local v101       #location:Lcom/android/server/LocationManagerService;
    .restart local v103       #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .restart local v114       #notification:Lcom/android/server/NotificationManagerService;
    .restart local v132       #statusBar:Lcom/android/server/StatusBarManagerService;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v140       #tsms:Lcom/android/server/TextServicesManagerService;
    .restart local v152       #vzwLocMgrObj:Ljava/lang/Object;
    .restart local v154       #wallpaper:Lcom/android/server/WallpaperManagerService;
    :catch_d4b
    move-exception v90

    #@d4c
    move-object/from16 v88, v89

    #@d4e
    .end local v89           #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .restart local v88       #dreamy:Lcom/android/server/dreams/DreamManagerService;
    goto/16 :goto_c8c

    #@d50
    .line 912
    .end local v72           #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    .restart local v73       #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    :catch_d50
    move-exception v90

    #@d51
    move-object/from16 v72, v73

    #@d53
    .end local v73           #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    .restart local v72       #commonTimeMgmtService:Lcom/android/server/CommonTimeManagementService;
    goto/16 :goto_c74

    #@d55
    .line 863
    .end local v65           #appWidget:Lcom/android/server/AppWidgetService;
    .restart local v66       #appWidget:Lcom/android/server/AppWidgetService;
    :catch_d55
    move-exception v90

    #@d56
    move-object/from16 v65, v66

    #@d58
    .end local v66           #appWidget:Lcom/android/server/AppWidgetService;
    .restart local v65       #appWidget:Lcom/android/server/AppWidgetService;
    goto/16 :goto_c2c

    #@d5a
    .line 832
    .end local v128           #serial:Lcom/android/server/SerialService;
    .restart local v129       #serial:Lcom/android/server/SerialService;
    :catch_d5a
    move-exception v90

    #@d5b
    move-object/from16 v128, v129

    #@d5d
    .end local v129           #serial:Lcom/android/server/SerialService;
    .restart local v128       #serial:Lcom/android/server/SerialService;
    goto/16 :goto_bfc

    #@d5f
    .line 823
    .end local v147           #usb:Lcom/android/server/usb/UsbService;
    .restart local v148       #usb:Lcom/android/server/usb/UsbService;
    :catch_d5f
    move-exception v90

    #@d60
    move-object/from16 v147, v148

    #@d62
    .end local v148           #usb:Lcom/android/server/usb/UsbService;
    .restart local v147       #usb:Lcom/android/server/usb/UsbService;
    goto/16 :goto_bf0

    #@d64
    .line 787
    .end local v154           #wallpaper:Lcom/android/server/WallpaperManagerService;
    .restart local v155       #wallpaper:Lcom/android/server/WallpaperManagerService;
    :catch_d64
    move-exception v90

    #@d65
    move-object/from16 v154, v155

    #@d67
    .end local v155           #wallpaper:Lcom/android/server/WallpaperManagerService;
    .restart local v154       #wallpaper:Lcom/android/server/WallpaperManagerService;
    goto/16 :goto_bc0

    #@d69
    .line 759
    .end local v78           #countryDetector:Lcom/android/server/CountryDetectorService;
    .restart local v79       #countryDetector:Lcom/android/server/CountryDetectorService;
    :catch_d69
    move-exception v90

    #@d6a
    move-object/from16 v78, v79

    #@d6c
    .end local v79           #countryDetector:Lcom/android/server/CountryDetectorService;
    .restart local v78       #countryDetector:Lcom/android/server/CountryDetectorService;
    goto/16 :goto_b9c

    #@d6e
    .line 751
    .end local v101           #location:Lcom/android/server/LocationManagerService;
    .restart local v102       #location:Lcom/android/server/LocationManagerService;
    :catch_d6e
    move-exception v90

    #@d6f
    move-object/from16 v101, v102

    #@d71
    .end local v102           #location:Lcom/android/server/LocationManagerService;
    .restart local v101       #location:Lcom/android/server/LocationManagerService;
    goto/16 :goto_b90

    #@d73
    .line 735
    .end local v114           #notification:Lcom/android/server/NotificationManagerService;
    .restart local v115       #notification:Lcom/android/server/NotificationManagerService;
    :catch_d73
    move-exception v90

    #@d74
    move-object/from16 v114, v115

    #@d76
    .end local v115           #notification:Lcom/android/server/NotificationManagerService;
    .restart local v114       #notification:Lcom/android/server/NotificationManagerService;
    goto/16 :goto_b78

    #@d78
    .line 695
    .end local v137           #throttle:Lcom/android/server/ThrottleService;
    .restart local v138       #throttle:Lcom/android/server/ThrottleService;
    :catch_d78
    move-exception v90

    #@d79
    move-object/from16 v137, v138

    #@d7b
    .end local v138           #throttle:Lcom/android/server/ThrottleService;
    .restart local v137       #throttle:Lcom/android/server/ThrottleService;
    goto/16 :goto_b48

    #@d7d
    .line 589
    .end local v156           #wifi:Lcom/android/server/WifiService;
    .restart local v118       #qcCon:Ljava/lang/Object;
    .restart local v157       #wifi:Lcom/android/server/WifiService;
    :catch_d7d
    move-exception v90

    #@d7e
    move-object/from16 v156, v157

    #@d80
    .end local v157           #wifi:Lcom/android/server/WifiService;
    .restart local v156       #wifi:Lcom/android/server/WifiService;
    goto/16 :goto_adc

    #@d82
    .line 582
    .end local v162           #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    .restart local v163       #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    :catch_d82
    move-exception v90

    #@d83
    move-object/from16 v162, v163

    #@d85
    .end local v163           #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    .restart local v162       #wifiP2p:Landroid/net/wifi/p2p/WifiP2pService;
    goto/16 :goto_ad0

    #@d87
    .line 566
    :catch_d87
    move-exception v90

    #@d88
    goto/16 :goto_ab8

    #@d8a
    .line 556
    .end local v25           #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v29           #networkStats:Lcom/android/server/net/NetworkStatsService;
    .restart local v110       #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v111       #networkStats:Lcom/android/server/net/NetworkStatsService;
    :catch_d8a
    move-exception v90

    #@d8b
    move-object/from16 v29, v111

    #@d8d
    .end local v111           #networkStats:Lcom/android/server/net/NetworkStatsService;
    .restart local v29       #networkStats:Lcom/android/server/net/NetworkStatsService;
    goto/16 :goto_aaa

    #@d8f
    .line 548
    .end local v140           #tsms:Lcom/android/server/TextServicesManagerService;
    .restart local v141       #tsms:Lcom/android/server/TextServicesManagerService;
    :catch_d8f
    move-exception v90

    #@d90
    move-object/from16 v140, v141

    #@d92
    .end local v141           #tsms:Lcom/android/server/TextServicesManagerService;
    .restart local v140       #tsms:Lcom/android/server/TextServicesManagerService;
    goto/16 :goto_a9e

    #@d94
    .line 524
    .end local v132           #statusBar:Lcom/android/server/StatusBarManagerService;
    .restart local v133       #statusBar:Lcom/android/server/StatusBarManagerService;
    :catch_d94
    move-exception v90

    #@d95
    move-object/from16 v132, v133

    #@d97
    .end local v133           #statusBar:Lcom/android/server/StatusBarManagerService;
    .restart local v132       #statusBar:Lcom/android/server/StatusBarManagerService;
    goto/16 :goto_a7a

    #@d99
    .line 510
    .end local v83           #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .restart local v84       #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    :catch_d99
    move-exception v90

    #@d9a
    move-object/from16 v83, v84

    #@d9c
    .end local v84           #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .restart local v83       #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    goto/16 :goto_a6e

    #@d9e
    .line 502
    .end local v103           #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .restart local v104       #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    :catch_d9e
    move-exception v90

    #@d9f
    move-object/from16 v103, v104

    #@da1
    .end local v104           #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .restart local v103       #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    goto/16 :goto_a62

    #@da3
    .line 493
    .end local v106           #mountService:Lcom/android/server/MountService;
    .restart local v107       #mountService:Lcom/android/server/MountService;
    :catch_da3
    move-exception v90

    #@da4
    move-object/from16 v106, v107

    #@da6
    .end local v107           #mountService:Lcom/android/server/MountService;
    .restart local v106       #mountService:Lcom/android/server/MountService;
    goto/16 :goto_a56

    #@da8
    .line 480
    :catch_da8
    move-exception v7

    #@da9
    goto/16 :goto_36f

    #@dab
    .line 450
    .end local v95           #imm:Lcom/android/server/InputMethodManagerService;
    .restart local v96       #imm:Lcom/android/server/InputMethodManagerService;
    :catch_dab
    move-exception v90

    #@dac
    move-object/from16 v95, v96

    #@dae
    .end local v96           #imm:Lcom/android/server/InputMethodManagerService;
    .restart local v95       #imm:Lcom/android/server/InputMethodManagerService;
    goto/16 :goto_a26

    #@db0
    .line 422
    .end local v4           #power:Lcom/android/server/power/PowerManagerService;
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v10           #display:Lcom/android/server/display/DisplayManagerService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v65           #appWidget:Lcom/android/server/AppWidgetService;
    .end local v78           #countryDetector:Lcom/android/server/CountryDetectorService;
    .end local v83           #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .end local v88           #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .end local v95           #imm:Lcom/android/server/InputMethodManagerService;
    .end local v101           #location:Lcom/android/server/LocationManagerService;
    .end local v103           #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .end local v114           #notification:Lcom/android/server/NotificationManagerService;
    .end local v132           #statusBar:Lcom/android/server/StatusBarManagerService;
    .end local v140           #tsms:Lcom/android/server/TextServicesManagerService;
    .end local v152           #vzwLocMgrObj:Ljava/lang/Object;
    .end local v154           #wallpaper:Lcom/android/server/WallpaperManagerService;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v85       #display:Lcom/android/server/display/DisplayManagerService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v117       #power:Lcom/android/server/power/PowerManagerService;
    :catch_db0
    move-exception v90

    #@db1
    move-object/from16 v19, v97

    #@db3
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@db5
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@db7
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v10, v85

    #@db9
    .end local v85           #display:Lcom/android/server/display/DisplayManagerService;
    .restart local v10       #display:Lcom/android/server/display/DisplayManagerService;
    move-object/from16 v4, v117

    #@dbb
    .end local v117           #power:Lcom/android/server/power/PowerManagerService;
    .restart local v4       #power:Lcom/android/server/power/PowerManagerService;
    move-object/from16 v6, v100

    #@dbd
    .end local v100           #lights:Lcom/android/server/LightsService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    goto/16 :goto_9d9

    #@dbf
    .end local v4           #power:Lcom/android/server/power/PowerManagerService;
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v10           #display:Lcom/android/server/display/DisplayManagerService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v85       #display:Lcom/android/server/display/DisplayManagerService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v117       #power:Lcom/android/server/power/PowerManagerService;
    :catch_dbf
    move-exception v90

    #@dc0
    move-object/from16 v19, v97

    #@dc2
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@dc4
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@dc6
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v10, v85

    #@dc8
    .end local v85           #display:Lcom/android/server/display/DisplayManagerService;
    .restart local v10       #display:Lcom/android/server/display/DisplayManagerService;
    move-object/from16 v4, v117

    #@dca
    .end local v117           #power:Lcom/android/server/power/PowerManagerService;
    .restart local v4       #power:Lcom/android/server/power/PowerManagerService;
    move-object/from16 v6, v100

    #@dcc
    .end local v100           #lights:Lcom/android/server/LightsService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    move-object/from16 v98, v99

    #@dce
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@dd0
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v10           #display:Lcom/android/server/display/DisplayManagerService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v85       #display:Lcom/android/server/display/DisplayManagerService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    :catch_dd0
    move-exception v90

    #@dd1
    move-object/from16 v19, v97

    #@dd3
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@dd5
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@dd7
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v10, v85

    #@dd9
    .end local v85           #display:Lcom/android/server/display/DisplayManagerService;
    .restart local v10       #display:Lcom/android/server/display/DisplayManagerService;
    move-object/from16 v6, v100

    #@ddb
    .end local v100           #lights:Lcom/android/server/LightsService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    move-object/from16 v98, v99

    #@ddd
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@ddf
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    :catch_ddf
    move-exception v90

    #@de0
    move-object/from16 v19, v97

    #@de2
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@de4
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@de6
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v6, v100

    #@de8
    .end local v100           #lights:Lcom/android/server/LightsService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    move-object/from16 v98, v99

    #@dea
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@dec
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v108           #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v109       #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :catch_dec
    move-exception v90

    #@ded
    move-object/from16 v108, v109

    #@def
    .end local v109           #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    .restart local v108       #msimTelephonyRegistry:Lcom/android/server/MSimTelephonyRegistry;
    move-object/from16 v135, v136

    #@df1
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v19, v97

    #@df3
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@df5
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@df7
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v6, v100

    #@df9
    .end local v100           #lights:Lcom/android/server/LightsService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    move-object/from16 v98, v99

    #@dfb
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@dfd
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v62           #accountManager:Landroid/accounts/AccountManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v63       #accountManager:Landroid/accounts/AccountManagerService;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v80       #cryptState:Ljava/lang/String;
    .restart local v93       #firstBoot:Z
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :catch_dfd
    move-exception v90

    #@dfe
    move-object/from16 v135, v136

    #@e00
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v19, v97

    #@e02
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@e04
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@e06
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v6, v100

    #@e08
    .end local v100           #lights:Lcom/android/server/LightsService;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    move-object/from16 v62, v63

    #@e0a
    .end local v63           #accountManager:Landroid/accounts/AccountManagerService;
    .restart local v62       #accountManager:Landroid/accounts/AccountManagerService;
    move-object/from16 v98, v99

    #@e0c
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@e0e
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :catch_e0e
    move-exception v90

    #@e0f
    move-object/from16 v135, v136

    #@e11
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v19, v97

    #@e13
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@e15
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v8, v69

    #@e17
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    move-object/from16 v98, v99

    #@e19
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@e1b
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :catch_e1b
    move-exception v90

    #@e1c
    move-object/from16 v135, v136

    #@e1e
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v19, v97

    #@e20
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@e22
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v98, v99

    #@e24
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@e26
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .end local v150           #vibrator:Lcom/android/server/VibratorService;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v151       #vibrator:Lcom/android/server/VibratorService;
    :catch_e26
    move-exception v90

    #@e27
    move-object/from16 v135, v136

    #@e29
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v19, v97

    #@e2b
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v15, v64

    #@e2d
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    move-object/from16 v150, v151

    #@e2f
    .end local v151           #vibrator:Lcom/android/server/VibratorService;
    .restart local v150       #vibrator:Lcom/android/server/VibratorService;
    move-object/from16 v98, v99

    #@e31
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@e33
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .end local v150           #vibrator:Lcom/android/server/VibratorService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v151       #vibrator:Lcom/android/server/VibratorService;
    :catch_e33
    move-exception v90

    #@e34
    move-object/from16 v135, v136

    #@e36
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v19, v97

    #@e38
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    move-object/from16 v150, v151

    #@e3a
    .end local v151           #vibrator:Lcom/android/server/VibratorService;
    .restart local v150       #vibrator:Lcom/android/server/VibratorService;
    move-object/from16 v98, v99

    #@e3c
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@e3e
    .end local v70           #bluetooth:Lcom/android/server/BluetoothManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .end local v150           #vibrator:Lcom/android/server/VibratorService;
    .restart local v71       #bluetooth:Lcom/android/server/BluetoothManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v151       #vibrator:Lcom/android/server/VibratorService;
    :catch_e3e
    move-exception v90

    #@e3f
    move-object/from16 v135, v136

    #@e41
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    move-object/from16 v70, v71

    #@e43
    .end local v71           #bluetooth:Lcom/android/server/BluetoothManagerService;
    .restart local v70       #bluetooth:Lcom/android/server/BluetoothManagerService;
    move-object/from16 v150, v151

    #@e45
    .end local v151           #vibrator:Lcom/android/server/VibratorService;
    .restart local v150       #vibrator:Lcom/android/server/VibratorService;
    move-object/from16 v98, v99

    #@e47
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    goto/16 :goto_9d9

    #@e49
    .line 355
    .end local v6           #lights:Lcom/android/server/LightsService;
    .end local v8           #battery:Lcom/android/server/BatteryService;
    .end local v15           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v19           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v62           #accountManager:Landroid/accounts/AccountManagerService;
    .end local v98           #installer:Lcom/android/server/pm/Installer;
    .end local v135           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v63       #accountManager:Landroid/accounts/AccountManagerService;
    .restart local v64       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v69       #battery:Lcom/android/server/BatteryService;
    .restart local v97       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v99       #installer:Lcom/android/server/pm/Installer;
    .restart local v100       #lights:Lcom/android/server/LightsService;
    .restart local v136       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    :catch_e49
    move-exception v90

    #@e4a
    move-object/from16 v62, v63

    #@e4c
    .end local v63           #accountManager:Landroid/accounts/AccountManagerService;
    .restart local v62       #accountManager:Landroid/accounts/AccountManagerService;
    goto/16 :goto_9c1

    #@e4e
    .end local v64           #alarm:Lcom/android/server/AlarmManagerService;
    .end local v69           #battery:Lcom/android/server/BatteryService;
    .end local v80           #cryptState:Ljava/lang/String;
    .end local v93           #firstBoot:Z
    .end local v97           #inputManager:Lcom/android/server/input/InputManagerService;
    .end local v99           #installer:Lcom/android/server/pm/Installer;
    .end local v100           #lights:Lcom/android/server/LightsService;
    .end local v136           #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v6       #lights:Lcom/android/server/LightsService;
    .restart local v8       #battery:Lcom/android/server/BatteryService;
    .restart local v15       #alarm:Lcom/android/server/AlarmManagerService;
    .restart local v19       #inputManager:Lcom/android/server/input/InputManagerService;
    .restart local v65       #appWidget:Lcom/android/server/AppWidgetService;
    .restart local v78       #countryDetector:Lcom/android/server/CountryDetectorService;
    .restart local v83       #devicePolicy:Lcom/android/server/DevicePolicyManagerService;
    .restart local v88       #dreamy:Lcom/android/server/dreams/DreamManagerService;
    .restart local v95       #imm:Lcom/android/server/InputMethodManagerService;
    .restart local v98       #installer:Lcom/android/server/pm/Installer;
    .restart local v101       #location:Lcom/android/server/LocationManagerService;
    .restart local v103       #lockSettings:Lcom/android/internal/widget/LockSettingsService;
    .restart local v114       #notification:Lcom/android/server/NotificationManagerService;
    .restart local v132       #statusBar:Lcom/android/server/StatusBarManagerService;
    .restart local v135       #telephonyRegistry:Lcom/android/server/TelephonyRegistry;
    .restart local v140       #tsms:Lcom/android/server/TextServicesManagerService;
    .restart local v152       #vzwLocMgrObj:Ljava/lang/Object;
    .restart local v154       #wallpaper:Lcom/android/server/WallpaperManagerService;
    :cond_e4e
    move-object/from16 v25, v110

    #@e50
    .end local v110           #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v25       #networkPolicy:Lcom/android/server/net/NetworkPolicyManagerService;
    goto/16 :goto_8bf
.end method
