.class public Lcom/android/server/LocationManagerService;
.super Landroid/location/ILocationManager$Stub;
.source "LocationManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/LocationManagerService$LocationWorkerHandler;,
        Lcom/android/server/LocationManagerService$UpdateRecord;,
        Lcom/android/server/LocationManagerService$Receiver;
    }
.end annotation


# static fields
.field private static final ACCESS_LOCATION_EXTRA_COMMANDS:Ljava/lang/String; = "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS"

.field private static final ACCESS_MOCK_LOCATION:Ljava/lang/String; = "android.permission.ACCESS_MOCK_LOCATION"

.field public static final D:Z = true

.field private static final DEFAULT_LOCATION_REQUEST:Landroid/location/LocationRequest; = null

.field private static final FUSED_LOCATION_SERVICE_ACTION:Ljava/lang/String; = "com.android.location.service.FusedLocationProvider"

.field private static final INSTALL_LOCATION_PROVIDER:Ljava/lang/String; = "android.permission.INSTALL_LOCATION_PROVIDER"

.field private static final MAX_PROVIDER_SCHEDULING_JITTER_MS:I = 0x64

.field private static final MSG_LOCATION_CHANGED:I = 0x1

.field private static final NETWORK_LOCATION_SERVICE_ACTION:Ljava/lang/String; = "com.android.location.service.v2.NetworkLocationProvider"

.field private static final RESOLUTION_LEVEL_COARSE:I = 0x1

.field private static final RESOLUTION_LEVEL_FINE:I = 0x2

.field private static final RESOLUTION_LEVEL_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "LocationManagerService"

.field private static final THREAD_NAME:Ljava/lang/String; = "LocationManagerService"

.field private static final WAKELOCK_KEY:Ljava/lang/String; = "LocationManagerService"


# instance fields
.field private mBlacklist:Lcom/android/server/location/LocationBlacklist;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mDisabledProviders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mEnabledProviders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mGeoFencer:Lcom/android/server/location/GeoFencerBase;

.field private mGeoFencerEnabled:Z

.field private mGeoFencerPackageName:Ljava/lang/String;

.field private mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

.field private mGeofenceManager:Lcom/android/server/location/GeofenceManager;

.field private mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

.field private final mLastLocation:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationFudger:Lcom/android/server/location/LocationFudger;

.field private mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

.field private final mLock:Ljava/lang/Object;

.field private final mMockProviders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/location/MockProvider;",
            ">;"
        }
    .end annotation
.end field

.field private mNetInitiatedListener:Landroid/location/INetInitiatedListener;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

.field private mPassiveProvider:Lcom/android/server/location/PassiveProvider;

.field private mPendingBroadcasts:I

.field private final mProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/location/LocationProviderInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final mProvidersByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/location/LocationProviderInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final mProxyProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/location/LocationProviderProxy;",
            ">;"
        }
    .end annotation
.end field

.field private final mRealProviders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/location/LocationProviderInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final mReceivers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/android/server/LocationManagerService$Receiver;",
            ">;"
        }
    .end annotation
.end field

.field private final mRecordsByProvider:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/LocationManagerService$UpdateRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 135
    new-instance v0, Landroid/location/LocationRequest;

    #@2
    invoke-direct {v0}, Landroid/location/LocationRequest;-><init>()V

    #@5
    sput-object v0, Lcom/android/server/LocationManagerService;->DEFAULT_LOCATION_REQUEST:Landroid/location/LocationRequest;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 201
    invoke-direct {p0}, Landroid/location/ILocationManager$Stub;-><init>()V

    #@4
    .line 140
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@b
    .line 162
    new-instance v0, Ljava/util/HashSet;

    #@d
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@12
    .line 165
    new-instance v0, Ljava/util/HashSet;

    #@14
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mDisabledProviders:Ljava/util/Set;

    #@19
    .line 168
    new-instance v0, Ljava/util/HashMap;

    #@1b
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@20
    .line 172
    new-instance v0, Ljava/util/HashMap;

    #@22
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@27
    .line 175
    new-instance v0, Ljava/util/ArrayList;

    #@29
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2c
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@2e
    .line 179
    new-instance v0, Ljava/util/HashMap;

    #@30
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@33
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mRealProviders:Ljava/util/HashMap;

    #@35
    .line 183
    new-instance v0, Ljava/util/HashMap;

    #@37
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@3a
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@3c
    .line 187
    new-instance v0, Ljava/util/HashMap;

    #@3e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@41
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mRecordsByProvider:Ljava/util/HashMap;

    #@43
    .line 191
    new-instance v0, Ljava/util/HashMap;

    #@45
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@48
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@4a
    .line 194
    new-instance v0, Ljava/util/ArrayList;

    #@4c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@4f
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mProxyProviders:Ljava/util/ArrayList;

    #@51
    .line 198
    iput v1, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@53
    .line 1883
    new-instance v0, Lcom/android/server/LocationManagerService$3;

    #@55
    invoke-direct {v0, p0}, Lcom/android/server/LocationManagerService$3;-><init>(Lcom/android/server/LocationManagerService;)V

    #@58
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@5a
    .line 202
    iput-object p1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@5c
    .line 203
    iput-boolean v1, p0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@5e
    .line 205
    const-string v0, "LocationManagerService"

    #@60
    const-string v1, "Constructed"

    #@62
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 208
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/LocationManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/LocationManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->incrementPendingBroadcasts()V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/LocationManagerService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mRecordsByProvider:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/LocationManagerService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/server/LocationManagerService;->applyRequirementsLocked(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/LocationManagerService;Landroid/location/Location;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/android/server/LocationManagerService;->handleLocationChanged(Landroid/location/Location;Z)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/LocationManagerService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/LocationManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->updateProvidersLocked()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/LocationManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/server/LocationManagerService;->switchUser(I)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/LocationManagerService;II)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/android/server/LocationManagerService;->getAllowedResolutionLevel(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/android/server/LocationManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/LocationManagerService;)Lcom/android/server/LocationManagerService$LocationWorkerHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/LocationManagerService;I)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/server/LocationManagerService;->getResolutionPermission(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/LocationManagerService;Lcom/android/server/LocationManagerService$Receiver;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/server/LocationManagerService;->removeUpdatesLocked(Lcom/android/server/LocationManagerService$Receiver;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/LocationManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->decrementPendingBroadcasts()V

    #@3
    return-void
.end method

.method private addProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    .line 689
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 690
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@7
    invoke-interface {p1}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    .line 691
    return-void
.end method

.method private applyRequirementsLocked(Ljava/lang/String;)V
    .registers 15
    .parameter "provider"

    #@0
    .prologue
    .line 1026
    iget-object v9, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@2
    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Lcom/android/server/location/LocationProviderInterface;

    #@8
    .line 1027
    .local v2, p:Lcom/android/server/location/LocationProviderInterface;
    if-nez v2, :cond_b

    #@a
    .line 1065
    :goto_a
    return-void

    #@b
    .line 1029
    :cond_b
    iget-object v9, p0, Lcom/android/server/LocationManagerService;->mRecordsByProvider:Ljava/util/HashMap;

    #@d
    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v5

    #@11
    check-cast v5, Ljava/util/ArrayList;

    #@13
    .line 1030
    .local v5, records:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    new-instance v8, Landroid/os/WorkSource;

    #@15
    invoke-direct {v8}, Landroid/os/WorkSource;-><init>()V

    #@18
    .line 1031
    .local v8, worksource:Landroid/os/WorkSource;
    new-instance v3, Lcom/android/internal/location/ProviderRequest;

    #@1a
    invoke-direct {v3}, Lcom/android/internal/location/ProviderRequest;-><init>()V

    #@1d
    .line 1033
    .local v3, providerRequest:Lcom/android/internal/location/ProviderRequest;
    if-eqz v5, :cond_94

    #@1f
    .line 1034
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v0

    #@23
    .local v0, i$:Ljava/util/Iterator;
    :cond_23
    :goto_23
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v9

    #@27
    if-eqz v9, :cond_56

    #@29
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v4

    #@2d
    check-cast v4, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@2f
    .line 1035
    .local v4, record:Lcom/android/server/LocationManagerService$UpdateRecord;
    iget-object v9, v4, Lcom/android/server/LocationManagerService$UpdateRecord;->mReceiver:Lcom/android/server/LocationManagerService$Receiver;

    #@31
    iget v9, v9, Lcom/android/server/LocationManagerService$Receiver;->mUid:I

    #@33
    invoke-static {v9}, Landroid/os/UserHandle;->getUserId(I)I

    #@36
    move-result v9

    #@37
    iget v10, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@39
    if-ne v9, v10, :cond_23

    #@3b
    .line 1036
    iget-object v1, v4, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@3d
    .line 1037
    .local v1, locationRequest:Landroid/location/LocationRequest;
    iget-object v9, v3, Lcom/android/internal/location/ProviderRequest;->locationRequests:Ljava/util/List;

    #@3f
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@42
    .line 1038
    invoke-virtual {v1}, Landroid/location/LocationRequest;->getInterval()J

    #@45
    move-result-wide v9

    #@46
    iget-wide v11, v3, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@48
    cmp-long v9, v9, v11

    #@4a
    if-gez v9, :cond_23

    #@4c
    .line 1039
    const/4 v9, 0x1

    #@4d
    iput-boolean v9, v3, Lcom/android/internal/location/ProviderRequest;->reportLocation:Z

    #@4f
    .line 1040
    invoke-virtual {v1}, Landroid/location/LocationRequest;->getInterval()J

    #@52
    move-result-wide v9

    #@53
    iput-wide v9, v3, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@55
    goto :goto_23

    #@56
    .line 1045
    .end local v1           #locationRequest:Landroid/location/LocationRequest;
    .end local v4           #record:Lcom/android/server/LocationManagerService$UpdateRecord;
    :cond_56
    iget-boolean v9, v3, Lcom/android/internal/location/ProviderRequest;->reportLocation:Z

    #@58
    if-eqz v9, :cond_94

    #@5a
    .line 1051
    iget-wide v9, v3, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@5c
    const-wide/16 v11, 0x3e8

    #@5e
    add-long/2addr v9, v11

    #@5f
    const-wide/16 v11, 0x3

    #@61
    mul-long/2addr v9, v11

    #@62
    const-wide/16 v11, 0x2

    #@64
    div-long v6, v9, v11

    #@66
    .line 1052
    .local v6, thresholdInterval:J
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@69
    move-result-object v0

    #@6a
    :cond_6a
    :goto_6a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@6d
    move-result v9

    #@6e
    if-eqz v9, :cond_94

    #@70
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@73
    move-result-object v4

    #@74
    check-cast v4, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@76
    .line 1053
    .restart local v4       #record:Lcom/android/server/LocationManagerService$UpdateRecord;
    iget-object v9, v4, Lcom/android/server/LocationManagerService$UpdateRecord;->mReceiver:Lcom/android/server/LocationManagerService$Receiver;

    #@78
    iget v9, v9, Lcom/android/server/LocationManagerService$Receiver;->mUid:I

    #@7a
    invoke-static {v9}, Landroid/os/UserHandle;->getUserId(I)I

    #@7d
    move-result v9

    #@7e
    iget v10, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@80
    if-ne v9, v10, :cond_6a

    #@82
    .line 1054
    iget-object v1, v4, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@84
    .line 1055
    .restart local v1       #locationRequest:Landroid/location/LocationRequest;
    invoke-virtual {v1}, Landroid/location/LocationRequest;->getInterval()J

    #@87
    move-result-wide v9

    #@88
    cmp-long v9, v9, v6

    #@8a
    if-gtz v9, :cond_6a

    #@8c
    .line 1056
    iget-object v9, v4, Lcom/android/server/LocationManagerService$UpdateRecord;->mReceiver:Lcom/android/server/LocationManagerService$Receiver;

    #@8e
    iget v9, v9, Lcom/android/server/LocationManagerService$Receiver;->mUid:I

    #@90
    invoke-virtual {v8, v9}, Landroid/os/WorkSource;->add(I)Z

    #@93
    goto :goto_6a

    #@94
    .line 1063
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #locationRequest:Landroid/location/LocationRequest;
    .end local v4           #record:Lcom/android/server/LocationManagerService$UpdateRecord;
    .end local v6           #thresholdInterval:J
    :cond_94
    const-string v9, "LocationManagerService"

    #@96
    new-instance v10, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v11, "provider request: "

    #@9d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v10

    #@a1
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v10

    #@a5
    const-string v11, " "

    #@a7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v10

    #@ab
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v10

    #@af
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v10

    #@b3
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 1064
    invoke-interface {v2, v3, v8}, Lcom/android/server/location/LocationProviderInterface;->setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V

    #@b9
    goto/16 :goto_a
.end method

.method private checkCallerIsProvider()V
    .registers 6

    #@0
    .prologue
    .line 1582
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.INSTALL_LOCATION_PROVIDER"

    #@4
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v3

    #@8
    if-nez v3, :cond_b

    #@a
    .line 1600
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1594
    :cond_b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@e
    move-result v2

    #@f
    .line 1596
    .local v2, uid:I
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@11
    if-eqz v3, :cond_1f

    #@13
    .line 1597
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@15
    invoke-virtual {v3}, Lcom/android/server/location/GeocoderProxy;->getConnectedPackageName()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-direct {p0, v2, v3}, Lcom/android/server/LocationManagerService;->doesPackageHaveUid(ILjava/lang/String;)Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_a

    #@1f
    .line 1599
    :cond_1f
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mProxyProviders:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@24
    move-result-object v0

    #@25
    .local v0, i$:Ljava/util/Iterator;
    :cond_25
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_3c

    #@2b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e
    move-result-object v1

    #@2f
    check-cast v1, Lcom/android/server/location/LocationProviderProxy;

    #@31
    .line 1600
    .local v1, proxy:Lcom/android/server/location/LocationProviderProxy;
    invoke-virtual {v1}, Lcom/android/server/location/LocationProviderProxy;->getConnectedPackageName()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-direct {p0, v2, v3}, Lcom/android/server/LocationManagerService;->doesPackageHaveUid(ILjava/lang/String;)Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_25

    #@3b
    goto :goto_a

    #@3c
    .line 1602
    .end local v1           #proxy:Lcom/android/server/location/LocationProviderProxy;
    :cond_3c
    new-instance v3, Ljava/lang/SecurityException;

    #@3e
    const-string v4, "need INSTALL_LOCATION_PROVIDER permission, or UID of a currently bound location provider"

    #@40
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@43
    throw v3
.end method

.method private checkListenerOrIntent(Landroid/location/ILocationListener;Landroid/app/PendingIntent;IILjava/lang/String;)Lcom/android/server/LocationManagerService$Receiver;
    .registers 8
    .parameter "listener"
    .parameter "intent"
    .parameter "pid"
    .parameter "uid"
    .parameter "packageName"

    #@0
    .prologue
    .line 1221
    if-nez p2, :cond_c

    #@2
    if-nez p1, :cond_c

    #@4
    .line 1222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "need eiter listener or intent"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1223
    :cond_c
    if-eqz p2, :cond_18

    #@e
    if-eqz p1, :cond_18

    #@10
    .line 1224
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "cannot register both listener and intent"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 1225
    :cond_18
    if-eqz p2, :cond_22

    #@1a
    .line 1226
    invoke-direct {p0, p2}, Lcom/android/server/LocationManagerService;->checkPendingIntent(Landroid/app/PendingIntent;)V

    #@1d
    .line 1227
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/android/server/LocationManagerService;->getReceiver(Landroid/app/PendingIntent;IILjava/lang/String;)Lcom/android/server/LocationManagerService$Receiver;

    #@20
    move-result-object v0

    #@21
    .line 1229
    :goto_21
    return-object v0

    #@22
    :cond_22
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/android/server/LocationManagerService;->getReceiver(Landroid/location/ILocationListener;IILjava/lang/String;)Lcom/android/server/LocationManagerService$Receiver;

    #@25
    move-result-object v0

    #@26
    goto :goto_21
.end method

.method private checkMockPermissionsSafe()V
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1981
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "mock_location"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_1a

    #@10
    .line 1983
    .local v0, allowMocks:Z
    :goto_10
    if-nez v0, :cond_1c

    #@12
    .line 1984
    new-instance v1, Ljava/lang/SecurityException;

    #@14
    const-string v2, "Requires ACCESS_MOCK_LOCATION secure setting"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .end local v0           #allowMocks:Z
    :cond_1a
    move v0, v1

    #@1b
    .line 1981
    goto :goto_10

    #@1c
    .line 1987
    .restart local v0       #allowMocks:Z
    :cond_1c
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@1e
    const-string v2, "android.permission.ACCESS_MOCK_LOCATION"

    #@20
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_2e

    #@26
    .line 1989
    new-instance v1, Ljava/lang/SecurityException;

    #@28
    const-string v2, "Requires ACCESS_MOCK_LOCATION permission"

    #@2a
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v1

    #@2e
    .line 1991
    :cond_2e
    return-void
.end method

.method private checkPackageName(Ljava/lang/String;)V
    .registers 11
    .parameter "packageName"

    #@0
    .prologue
    .line 1199
    if-nez p1, :cond_1b

    #@2
    .line 1200
    new-instance v6, Ljava/lang/SecurityException;

    #@4
    new-instance v7, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v8, "invalid package name: "

    #@b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v7

    #@f
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v7

    #@17
    invoke-direct {v6, v7}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v6

    #@1b
    .line 1202
    :cond_1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v5

    #@1f
    .line 1203
    .local v5, uid:I
    iget-object v6, p0, Lcom/android/server/LocationManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@21
    invoke-virtual {v6, v5}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    .line 1204
    .local v3, packages:[Ljava/lang/String;
    if-nez v3, :cond_40

    #@27
    .line 1205
    new-instance v6, Ljava/lang/SecurityException;

    #@29
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v8, "invalid UID "

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    invoke-direct {v6, v7}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v6

    #@40
    .line 1207
    :cond_40
    move-object v0, v3

    #@41
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@42
    .local v2, len$:I
    const/4 v1, 0x0

    #@43
    .local v1, i$:I
    :goto_43
    if-ge v1, v2, :cond_51

    #@45
    aget-object v4, v0, v1

    #@47
    .line 1208
    .local v4, pkg:Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v6

    #@4b
    if-eqz v6, :cond_4e

    #@4d
    return-void

    #@4e
    .line 1207
    :cond_4e
    add-int/lit8 v1, v1, 0x1

    #@50
    goto :goto_43

    #@51
    .line 1210
    .end local v4           #pkg:Ljava/lang/String;
    :cond_51
    new-instance v6, Ljava/lang/SecurityException;

    #@53
    new-instance v7, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v8, "invalid package name: "

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v7

    #@66
    invoke-direct {v6, v7}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@69
    throw v6
.end method

.method private checkPendingIntent(Landroid/app/PendingIntent;)V
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 1214
    if-nez p1, :cond_1b

    #@2
    .line 1215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "invalid pending intent: "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 1217
    :cond_1b
    return-void
.end method

.method private checkResolutionLevelIsSufficientForGeofenceUse(I)V
    .registers 4
    .parameter "allowedResolutionLevel"

    #@0
    .prologue
    .line 767
    const/4 v0, 0x2

    #@1
    if-ge p1, v0, :cond_b

    #@3
    .line 768
    new-instance v0, Ljava/lang/SecurityException;

    #@5
    const-string v1, "Geofence usage requires ACCESS_FINE_LOCATION permission"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 770
    :cond_b
    return-void
.end method

.method private checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V
    .registers 7
    .parameter "allowedResolutionLevel"
    .parameter "providerName"

    #@0
    .prologue
    .line 815
    invoke-direct {p0, p2}, Lcom/android/server/LocationManagerService;->getMinimumResolutionLevelForProviderUse(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    .line 816
    .local v0, requiredResolutionLevel:I
    if-ge p1, v0, :cond_72

    #@6
    .line 817
    packed-switch v0, :pswitch_data_74

    #@9
    .line 825
    new-instance v1, Ljava/lang/SecurityException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Insufficient permission for \""

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, "\" location provider."

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 819
    :pswitch_28
    new-instance v1, Ljava/lang/SecurityException;

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "\""

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, "\" location provider "

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    const-string v3, "requires ACCESS_FINE_LOCATION permission."

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v1

    #@4d
    .line 822
    :pswitch_4d
    new-instance v1, Ljava/lang/SecurityException;

    #@4f
    new-instance v2, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v3, "\""

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    const-string v3, "\" location provider "

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    const-string v3, "requires ACCESS_COARSE_LOCATION or ACCESS_FINE_LOCATION permission."

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@71
    throw v1

    #@72
    .line 829
    :cond_72
    return-void

    #@73
    .line 817
    nop

    #@74
    :pswitch_data_74
    .packed-switch 0x1
        :pswitch_4d
        :pswitch_28
    .end packed-switch
.end method

.method private createSanitizedRequest(Landroid/location/LocationRequest;I)Landroid/location/LocationRequest;
    .registers 8
    .parameter "request"
    .parameter "resolutionLevel"

    #@0
    .prologue
    const-wide/32 v3, 0x927c0

    #@3
    .line 1173
    new-instance v0, Landroid/location/LocationRequest;

    #@5
    invoke-direct {v0, p1}, Landroid/location/LocationRequest;-><init>(Landroid/location/LocationRequest;)V

    #@8
    .line 1174
    .local v0, sanitizedRequest:Landroid/location/LocationRequest;
    const/4 v1, 0x2

    #@9
    if-ge p2, v1, :cond_28

    #@b
    .line 1175
    invoke-virtual {v0}, Landroid/location/LocationRequest;->getQuality()I

    #@e
    move-result v1

    #@f
    sparse-switch v1, :sswitch_data_48

    #@12
    .line 1184
    :goto_12
    invoke-virtual {v0}, Landroid/location/LocationRequest;->getInterval()J

    #@15
    move-result-wide v1

    #@16
    cmp-long v1, v1, v3

    #@18
    if-gez v1, :cond_1d

    #@1a
    .line 1185
    invoke-virtual {v0, v3, v4}, Landroid/location/LocationRequest;->setInterval(J)Landroid/location/LocationRequest;

    #@1d
    .line 1187
    :cond_1d
    invoke-virtual {v0}, Landroid/location/LocationRequest;->getFastestInterval()J

    #@20
    move-result-wide v1

    #@21
    cmp-long v1, v1, v3

    #@23
    if-gez v1, :cond_28

    #@25
    .line 1188
    invoke-virtual {v0, v3, v4}, Landroid/location/LocationRequest;->setFastestInterval(J)Landroid/location/LocationRequest;

    #@28
    .line 1192
    :cond_28
    invoke-virtual {v0}, Landroid/location/LocationRequest;->getFastestInterval()J

    #@2b
    move-result-wide v1

    #@2c
    invoke-virtual {v0}, Landroid/location/LocationRequest;->getInterval()J

    #@2f
    move-result-wide v3

    #@30
    cmp-long v1, v1, v3

    #@32
    if-lez v1, :cond_3b

    #@34
    .line 1193
    invoke-virtual {p1}, Landroid/location/LocationRequest;->getInterval()J

    #@37
    move-result-wide v1

    #@38
    invoke-virtual {p1, v1, v2}, Landroid/location/LocationRequest;->setFastestInterval(J)Landroid/location/LocationRequest;

    #@3b
    .line 1195
    :cond_3b
    return-object v0

    #@3c
    .line 1177
    :sswitch_3c
    const/16 v1, 0x66

    #@3e
    invoke-virtual {v0, v1}, Landroid/location/LocationRequest;->setQuality(I)Landroid/location/LocationRequest;

    #@41
    goto :goto_12

    #@42
    .line 1180
    :sswitch_42
    const/16 v1, 0xc9

    #@44
    invoke-virtual {v0, v1}, Landroid/location/LocationRequest;->setQuality(I)Landroid/location/LocationRequest;

    #@47
    goto :goto_12

    #@48
    .line 1175
    :sswitch_data_48
    .sparse-switch
        0x64 -> :sswitch_3c
        0xcb -> :sswitch_42
    .end sparse-switch
.end method

.method private decrementPendingBroadcasts()V
    .registers 5

    #@0
    .prologue
    .line 1927
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    monitor-enter v2

    #@3
    .line 1928
    :try_start_3
    iget v1, p0, Lcom/android/server/LocationManagerService;->mPendingBroadcasts:I

    #@5
    add-int/lit8 v1, v1, -0x1

    #@7
    iput v1, p0, Lcom/android/server/LocationManagerService;->mPendingBroadcasts:I
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_2e

    #@9
    if-nez v1, :cond_1d

    #@b
    .line 1931
    :try_start_b
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@d
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_1f

    #@13
    .line 1932
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@15
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@18
    .line 1933
    const-string v1, "Released wakelock"

    #@1a
    invoke-direct {p0, v1}, Lcom/android/server/LocationManagerService;->log(Ljava/lang/String;)V
    :try_end_1d
    .catchall {:try_start_b .. :try_end_1d} :catchall_2e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_1d} :catch_25

    #@1d
    .line 1943
    :cond_1d
    :goto_1d
    :try_start_1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_2e

    #@1e
    .line 1944
    return-void

    #@1f
    .line 1935
    :cond_1f
    :try_start_1f
    const-string v1, "Can\'t release wakelock again!"

    #@21
    invoke-direct {p0, v1}, Lcom/android/server/LocationManagerService;->log(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_1f .. :try_end_24} :catchall_2e
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_24} :catch_25

    #@24
    goto :goto_1d

    #@25
    .line 1937
    :catch_25
    move-exception v0

    #@26
    .line 1940
    .local v0, e:Ljava/lang/Exception;
    :try_start_26
    const-string v1, "LocationManagerService"

    #@28
    const-string v3, "exception in releaseWakeLock()"

    #@2a
    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2d
    goto :goto_1d

    #@2e
    .line 1943
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_2e
    move-exception v1

    #@2f
    monitor-exit v2
    :try_end_30
    .catchall {:try_start_26 .. :try_end_30} :catchall_2e

    #@30
    throw v1
.end method

.method private doesPackageHaveUid(ILjava/lang/String;)Z
    .registers 8
    .parameter "uid"
    .parameter "packageName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1607
    if-nez p2, :cond_4

    #@3
    .line 1618
    :cond_3
    :goto_3
    return v2

    #@4
    .line 1611
    :cond_4
    :try_start_4
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@6
    const/4 v4, 0x0

    #@7
    invoke-virtual {v3, p2, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@a
    move-result-object v0

    #@b
    .line 1612
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_d} :catch_11

    #@d
    if-ne v3, p1, :cond_3

    #@f
    .line 1618
    const/4 v2, 0x1

    #@10
    goto :goto_3

    #@11
    .line 1615
    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    :catch_11
    move-exception v1

    #@12
    .line 1616
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_3
.end method

.method private ensureFallbackFusedProviderPresentLocked(Ljava/util/ArrayList;)V
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 272
    .local p1, pkgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v4

    #@6
    .line 273
    .local v4, pm:Landroid/content/pm/PackageManager;
    iget-object v10, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v10}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@b
    move-result-object v8

    #@c
    .line 274
    .local v8, systemPackageName:Ljava/lang/String;
    iget-object v10, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@e
    invoke-static {v10, p1}, Lcom/android/server/ServiceWatcher;->getSignatureSets(Landroid/content/Context;Ljava/util/List;)Ljava/util/ArrayList;

    #@11
    move-result-object v7

    #@12
    .line 276
    .local v7, sigSets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashSet<Landroid/content/pm/Signature;>;>;"
    new-instance v10, Landroid/content/Intent;

    #@14
    const-string v11, "com.android.location.service.FusedLocationProvider"

    #@16
    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@19
    const/16 v11, 0x80

    #@1b
    iget v12, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@1d
    invoke-virtual {v4, v10, v11, v12}, Landroid/content/pm/PackageManager;->queryIntentServicesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@20
    move-result-object v6

    #@21
    .line 279
    .local v6, rInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@24
    move-result-object v1

    #@25
    .local v1, i$:Ljava/util/Iterator;
    :goto_25
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@28
    move-result v10

    #@29
    if-eqz v10, :cond_125

    #@2b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e
    move-result-object v5

    #@2f
    check-cast v5, Landroid/content/pm/ResolveInfo;

    #@31
    .line 280
    .local v5, rInfo:Landroid/content/pm/ResolveInfo;
    iget-object v10, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@33
    iget-object v3, v10, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@35
    .line 286
    .local v3, packageName:Ljava/lang/String;
    const/16 v10, 0x40

    #@37
    :try_start_37
    invoke-virtual {v4, v3, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@3a
    move-result-object v2

    #@3b
    .line 287
    .local v2, pInfo:Landroid/content/pm/PackageInfo;
    iget-object v10, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@3d
    invoke-static {v10, v7}, Lcom/android/server/ServiceWatcher;->isSignatureMatch([Landroid/content/pm/Signature;Ljava/util/List;)Z

    #@40
    move-result v10

    #@41
    if-nez v10, :cond_82

    #@43
    .line 288
    const-string v10, "LocationManagerService"

    #@45
    new-instance v11, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v11

    #@4e
    const-string v12, " resolves service "

    #@50
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v11

    #@54
    const-string v12, "com.android.location.service.FusedLocationProvider"

    #@56
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v11

    #@5a
    const-string v12, ", but has wrong signature, ignoring"

    #@5c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v11

    #@60
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v11

    #@64
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_67
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_37 .. :try_end_67} :catch_68

    #@67
    goto :goto_25

    #@68
    .line 292
    .end local v2           #pInfo:Landroid/content/pm/PackageInfo;
    :catch_68
    move-exception v0

    #@69
    .line 293
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v10, "LocationManagerService"

    #@6b
    new-instance v11, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v12, "missing package: "

    #@72
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v11

    #@76
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v11

    #@7a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v11

    #@7e
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_25

    #@82
    .line 298
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2       #pInfo:Landroid/content/pm/PackageInfo;
    :cond_82
    iget-object v10, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@84
    iget-object v10, v10, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    #@86
    if-nez v10, :cond_a1

    #@88
    .line 299
    const-string v10, "LocationManagerService"

    #@8a
    new-instance v11, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v12, "Found fused provider without metadata: "

    #@91
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v11

    #@95
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v11

    #@99
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v11

    #@9d
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    goto :goto_25

    #@a1
    .line 303
    :cond_a1
    iget-object v10, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@a3
    iget-object v10, v10, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    #@a5
    const-string v11, "serviceVersion"

    #@a7
    const/4 v12, -0x1

    #@a8
    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@ab
    move-result v9

    #@ac
    .line 305
    .local v9, version:I
    if-nez v9, :cond_10b

    #@ae
    .line 309
    iget-object v10, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@b0
    iget-object v10, v10, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b2
    iget v10, v10, Landroid/content/pm/ApplicationInfo;->flags:I

    #@b4
    and-int/lit8 v10, v10, 0x1

    #@b6
    if-nez v10, :cond_d2

    #@b8
    .line 310
    const-string v10, "LocationManagerService"

    #@ba
    new-instance v11, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v12, "Fallback candidate not in /system: "

    #@c1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v11

    #@c5
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v11

    #@c9
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v11

    #@cd
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    goto/16 :goto_25

    #@d2
    .line 316
    :cond_d2
    invoke-virtual {v4, v8, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    #@d5
    move-result v10

    #@d6
    if-eqz v10, :cond_f2

    #@d8
    .line 318
    const-string v10, "LocationManagerService"

    #@da
    new-instance v11, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v12, "Fallback candidate not signed the same as system: "

    #@e1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v11

    #@e5
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v11

    #@e9
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v11

    #@ed
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f0
    goto/16 :goto_25

    #@f2
    .line 324
    :cond_f2
    const-string v10, "LocationManagerService"

    #@f4
    new-instance v11, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v12, "Found fallback provider: "

    #@fb
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v11

    #@ff
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v11

    #@103
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v11

    #@107
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 325
    return-void

    #@10b
    .line 327
    :cond_10b
    const-string v10, "LocationManagerService"

    #@10d
    new-instance v11, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v12, "Fallback candidate not version 0: "

    #@114
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v11

    #@118
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v11

    #@11c
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v11

    #@120
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@123
    goto/16 :goto_25

    #@125
    .line 331
    .end local v2           #pInfo:Landroid/content/pm/PackageInfo;
    .end local v3           #packageName:Ljava/lang/String;
    .end local v5           #rInfo:Landroid/content/pm/ResolveInfo;
    .end local v9           #version:I
    :cond_125
    new-instance v10, Ljava/lang/IllegalStateException;

    #@127
    const-string v11, "Unable to find a fused location provider that is in the system partition with version 0 and signed with the platform certificate. Such a package is needed to provide a default fused location provider in the event that no other fused location provider has been installed or is currently available. For example, coreOnly boot mode when decrypting the data partition. The fallback must also be marked coreApp=\"true\" in the manifest"

    #@129
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@12c
    throw v10
.end method

.method private getAllowedResolutionLevel(II)I
    .registers 5
    .parameter "pid"
    .parameter "uid"

    #@0
    .prologue
    .line 741
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    #@4
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    .line 743
    const/4 v0, 0x2

    #@b
    .line 748
    :goto_b
    return v0

    #@c
    .line 744
    :cond_c
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@e
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    #@10
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_18

    #@16
    .line 746
    const/4 v0, 0x1

    #@17
    goto :goto_b

    #@18
    .line 748
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_b
.end method

.method private getCallerAllowedResolutionLevel()I
    .registers 3

    #@0
    .prologue
    .line 758
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@7
    move-result v1

    #@8
    invoke-direct {p0, v0, v1}, Lcom/android/server/LocationManagerService;->getAllowedResolutionLevel(II)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method private getMinimumResolutionLevelForProviderUse(Ljava/lang/String;)I
    .registers 7
    .parameter "provider"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x2

    #@2
    .line 779
    const-string v4, "gps"

    #@4
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v4

    #@8
    if-nez v4, :cond_12

    #@a
    const-string v4, "passive"

    #@c
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_13

    #@12
    .line 803
    :cond_12
    :goto_12
    return v2

    #@13
    .line 783
    :cond_13
    const-string v4, "network"

    #@15
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_23

    #@1b
    const-string v4, "fused"

    #@1d
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_25

    #@23
    :cond_23
    move v2, v3

    #@24
    .line 786
    goto :goto_12

    #@25
    .line 789
    :cond_25
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@27
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Lcom/android/server/location/LocationProviderInterface;

    #@2d
    .line 790
    .local v0, lp:Lcom/android/server/location/LocationProviderInterface;
    if-eqz v0, :cond_12

    #@2f
    .line 791
    invoke-interface {v0}, Lcom/android/server/location/LocationProviderInterface;->getProperties()Lcom/android/internal/location/ProviderProperties;

    #@32
    move-result-object v1

    #@33
    .line 792
    .local v1, properties:Lcom/android/internal/location/ProviderProperties;
    if-eqz v1, :cond_12

    #@35
    .line 793
    iget-boolean v4, v1, Lcom/android/internal/location/ProviderProperties;->mRequiresSatellite:Z

    #@37
    if-nez v4, :cond_12

    #@39
    .line 796
    iget-boolean v4, v1, Lcom/android/internal/location/ProviderProperties;->mRequiresNetwork:Z

    #@3b
    if-nez v4, :cond_41

    #@3d
    iget-boolean v4, v1, Lcom/android/internal/location/ProviderProperties;->mRequiresCell:Z

    #@3f
    if-eqz v4, :cond_12

    #@41
    :cond_41
    move v2, v3

    #@42
    .line 798
    goto :goto_12
.end method

.method private getReceiver(Landroid/app/PendingIntent;IILjava/lang/String;)Lcom/android/server/LocationManagerService$Receiver;
    .registers 12
    .parameter "intent"
    .parameter "pid"
    .parameter "uid"
    .parameter "packageName"

    #@0
    .prologue
    .line 1153
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/LocationManagerService$Receiver;

    #@8
    .line 1154
    .local v0, receiver:Lcom/android/server/LocationManagerService$Receiver;
    if-nez v0, :cond_1a

    #@a
    .line 1155
    new-instance v0, Lcom/android/server/LocationManagerService$Receiver;

    #@c
    .end local v0           #receiver:Lcom/android/server/LocationManagerService$Receiver;
    const/4 v2, 0x0

    #@d
    move-object v1, p0

    #@e
    move-object v3, p1

    #@f
    move v4, p2

    #@10
    move v5, p3

    #@11
    move-object v6, p4

    #@12
    invoke-direct/range {v0 .. v6}, Lcom/android/server/LocationManagerService$Receiver;-><init>(Lcom/android/server/LocationManagerService;Landroid/location/ILocationListener;Landroid/app/PendingIntent;IILjava/lang/String;)V

    #@15
    .line 1156
    .restart local v0       #receiver:Lcom/android/server/LocationManagerService$Receiver;
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@17
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 1158
    :cond_1a
    return-object v0
.end method

.method private getReceiver(Landroid/location/ILocationListener;IILjava/lang/String;)Lcom/android/server/LocationManagerService$Receiver;
    .registers 14
    .parameter "listener"
    .parameter "pid"
    .parameter "uid"
    .parameter "packageName"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1136
    invoke-interface {p1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    #@4
    move-result-object v7

    #@5
    .line 1137
    .local v7, binder:Landroid/os/IBinder;
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@7
    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/android/server/LocationManagerService$Receiver;

    #@d
    .line 1138
    .local v0, receiver:Lcom/android/server/LocationManagerService$Receiver;
    if-nez v0, :cond_2a

    #@f
    .line 1139
    new-instance v0, Lcom/android/server/LocationManagerService$Receiver;

    #@11
    .end local v0           #receiver:Lcom/android/server/LocationManagerService$Receiver;
    move-object v1, p0

    #@12
    move-object v2, p1

    #@13
    move v4, p2

    #@14
    move v5, p3

    #@15
    move-object v6, p4

    #@16
    invoke-direct/range {v0 .. v6}, Lcom/android/server/LocationManagerService$Receiver;-><init>(Lcom/android/server/LocationManagerService;Landroid/location/ILocationListener;Landroid/app/PendingIntent;IILjava/lang/String;)V

    #@19
    .line 1140
    .restart local v0       #receiver:Lcom/android/server/LocationManagerService$Receiver;
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@1b
    invoke-virtual {v1, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    .line 1143
    :try_start_1e
    invoke-virtual {v0}, Lcom/android/server/LocationManagerService$Receiver;->getListener()Landroid/location/ILocationListener;

    #@21
    move-result-object v1

    #@22
    invoke-interface {v1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    #@25
    move-result-object v1

    #@26
    const/4 v2, 0x0

    #@27
    invoke-interface {v1, v0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_2a} :catch_2c

    #@2a
    :cond_2a
    move-object v3, v0

    #@2b
    .line 1149
    :goto_2b
    return-object v3

    #@2c
    .line 1144
    :catch_2c
    move-exception v8

    #@2d
    .line 1145
    .local v8, e:Landroid/os/RemoteException;
    const-string v1, "LocationManagerService"

    #@2f
    const-string v2, "linkToDeath failed:"

    #@31
    invoke-static {v1, v2, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    goto :goto_2b
.end method

.method private getResolutionPermission(I)Ljava/lang/String;
    .registers 3
    .parameter "resolutionLevel"

    #@0
    .prologue
    .line 723
    packed-switch p1, :pswitch_data_c

    #@3
    .line 729
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 725
    :pswitch_5
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    #@7
    goto :goto_4

    #@8
    .line 727
    :pswitch_8
    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    #@a
    goto :goto_4

    #@b
    .line 723
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_8
        :pswitch_5
    .end packed-switch
.end method

.method private handleLocationChanged(Landroid/location/Location;Z)V
    .registers 9
    .parameter "location"
    .parameter "passive"

    #@0
    .prologue
    .line 1860
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1862
    .local v0, provider:Ljava/lang/String;
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7
    move-result-object v1

    #@8
    if-eqz v1, :cond_23

    #@a
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@d
    move-result-object v1

    #@e
    const/4 v2, 0x1

    #@f
    const/4 v3, 0x0

    #@10
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@13
    move-result v4

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_23

    #@1b
    .line 1866
    const-string v1, "LocationManagerService"

    #@1d
    const-string v2, "MDM Block PassiveProvider"

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1867
    const/4 p2, 0x0

    #@23
    .line 1871
    :cond_23
    if-nez p2, :cond_2a

    #@25
    .line 1873
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mPassiveProvider:Lcom/android/server/location/PassiveProvider;

    #@27
    invoke-virtual {v1, p1}, Lcom/android/server/location/PassiveProvider;->updateLocation(Landroid/location/Location;)V

    #@2a
    .line 1876
    :cond_2a
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@2c
    monitor-enter v2

    #@2d
    .line 1877
    :try_start_2d
    iget v1, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@2f
    invoke-direct {p0, v0, v1}, Lcom/android/server/LocationManagerService;->isAllowedBySettingsLocked(Ljava/lang/String;I)Z

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_38

    #@35
    .line 1878
    invoke-direct {p0, p1, p2}, Lcom/android/server/LocationManagerService;->handleLocationChangedLocked(Landroid/location/Location;Z)V

    #@38
    .line 1880
    :cond_38
    monitor-exit v2

    #@39
    .line 1881
    return-void

    #@3a
    .line 1880
    :catchall_3a
    move-exception v1

    #@3b
    monitor-exit v2
    :try_end_3c
    .catchall {:try_start_2d .. :try_end_3c} :catchall_3a

    #@3c
    throw v1
.end method

.method private handleLocationChangedLocked(Landroid/location/Location;Z)V
    .registers 36
    .parameter "location"
    .parameter "passive"

    #@0
    .prologue
    .line 1691
    const-string v28, "ro.build.target_operator"

    #@2
    invoke-static/range {v28 .. v28}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v28

    #@6
    const-string v29, "TMO"

    #@8
    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v28

    #@c
    if-eqz v28, :cond_67

    #@e
    const-string v28, "ro.build.target_country"

    #@10
    invoke-static/range {v28 .. v28}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v28

    #@14
    const-string v29, "US"

    #@16
    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v28

    #@1a
    if-eqz v28, :cond_67

    #@1c
    .line 1694
    const-string v28, "persist.service.privacy.enable"

    #@1e
    const-string v29, "NA"

    #@20
    invoke-static/range {v28 .. v29}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v20

    #@24
    .line 1695
    .local v20, privacy_check:Ljava/lang/String;
    const-string v28, "1"

    #@26
    move-object/from16 v0, v28

    #@28
    move-object/from16 v1, v20

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v28

    #@2e
    if-eqz v28, :cond_4c

    #@30
    .line 1697
    const-string v28, "LocationManagerService"

    #@32
    new-instance v29, Ljava/lang/StringBuilder;

    #@34
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v30, "incoming location: "

    #@39
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v29

    #@3d
    move-object/from16 v0, v29

    #@3f
    move-object/from16 v1, p1

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v29

    #@45
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v29

    #@49
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1706
    .end local v20           #privacy_check:Ljava/lang/String;
    :cond_4c
    :goto_4c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4f
    move-result-wide v15

    #@50
    .line 1707
    .local v15, now:J
    if-eqz p2, :cond_84

    #@52
    const-string v21, "passive"

    #@54
    .line 1710
    .local v21, provider:Ljava/lang/String;
    :goto_54
    move-object/from16 v0, p0

    #@56
    iget-object v0, v0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@58
    move-object/from16 v28, v0

    #@5a
    move-object/from16 v0, v28

    #@5c
    move-object/from16 v1, v21

    #@5e
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@61
    move-result-object v17

    #@62
    check-cast v17, Lcom/android/server/location/LocationProviderInterface;

    #@64
    .line 1711
    .local v17, p:Lcom/android/server/location/LocationProviderInterface;
    if-nez v17, :cond_89

    #@66
    .line 1846
    :cond_66
    :goto_66
    return-void

    #@67
    .line 1702
    .end local v15           #now:J
    .end local v17           #p:Lcom/android/server/location/LocationProviderInterface;
    .end local v21           #provider:Ljava/lang/String;
    :cond_67
    const-string v28, "LocationManagerService"

    #@69
    new-instance v29, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v30, "incoming location: "

    #@70
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v29

    #@74
    move-object/from16 v0, v29

    #@76
    move-object/from16 v1, p1

    #@78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v29

    #@7c
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v29

    #@80
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_4c

    #@84
    .line 1707
    .restart local v15       #now:J
    :cond_84
    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    #@87
    move-result-object v21

    #@88
    goto :goto_54

    #@89
    .line 1714
    .restart local v17       #p:Lcom/android/server/location/LocationProviderInterface;
    .restart local v21       #provider:Ljava/lang/String;
    :cond_89
    const-string v28, "noGPSLocation"

    #@8b
    move-object/from16 v0, p1

    #@8d
    move-object/from16 v1, v28

    #@8f
    invoke-virtual {v0, v1}, Landroid/location/Location;->getExtraLocation(Ljava/lang/String;)Landroid/location/Location;

    #@92
    move-result-object v13

    #@93
    .line 1715
    .local v13, noGPSLocation:Landroid/location/Location;
    const/4 v10, 0x0

    #@94
    .line 1716
    .local v10, lastNoGPSLocation:Landroid/location/Location;
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@98
    move-object/from16 v28, v0

    #@9a
    move-object/from16 v0, v28

    #@9c
    move-object/from16 v1, v21

    #@9e
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a1
    move-result-object v9

    #@a2
    check-cast v9, Landroid/location/Location;

    #@a4
    .line 1717
    .local v9, lastLocation:Landroid/location/Location;
    if-nez v9, :cond_16e

    #@a6
    .line 1718
    new-instance v9, Landroid/location/Location;

    #@a8
    .end local v9           #lastLocation:Landroid/location/Location;
    move-object/from16 v0, v21

    #@aa
    invoke-direct {v9, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@ad
    .line 1719
    .restart local v9       #lastLocation:Landroid/location/Location;
    move-object/from16 v0, p0

    #@af
    iget-object v0, v0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@b1
    move-object/from16 v28, v0

    #@b3
    move-object/from16 v0, v28

    #@b5
    move-object/from16 v1, v21

    #@b7
    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ba
    .line 1728
    :cond_ba
    :goto_ba
    move-object/from16 v0, p1

    #@bc
    invoke-virtual {v9, v0}, Landroid/location/Location;->set(Landroid/location/Location;)V

    #@bf
    .line 1731
    move-object/from16 v0, p0

    #@c1
    iget-object v0, v0, Lcom/android/server/LocationManagerService;->mRecordsByProvider:Ljava/util/HashMap;

    #@c3
    move-object/from16 v28, v0

    #@c5
    move-object/from16 v0, v28

    #@c7
    move-object/from16 v1, v21

    #@c9
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@cc
    move-result-object v26

    #@cd
    check-cast v26, Ljava/util/ArrayList;

    #@cf
    .line 1732
    .local v26, records:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    if-eqz v26, :cond_66

    #@d1
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    #@d4
    move-result v28

    #@d5
    if-eqz v28, :cond_66

    #@d7
    .line 1735
    const/4 v3, 0x0

    #@d8
    .line 1736
    .local v3, coarseLocation:Landroid/location/Location;
    if-eqz v13, :cond_ec

    #@da
    invoke-virtual {v13, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@dd
    move-result v28

    #@de
    if-nez v28, :cond_ec

    #@e0
    .line 1737
    move-object/from16 v0, p0

    #@e2
    iget-object v0, v0, Lcom/android/server/LocationManagerService;->mLocationFudger:Lcom/android/server/location/LocationFudger;

    #@e4
    move-object/from16 v28, v0

    #@e6
    move-object/from16 v0, v28

    #@e8
    invoke-virtual {v0, v13}, Lcom/android/server/location/LocationFudger;->getOrCreate(Landroid/location/Location;)Landroid/location/Location;

    #@eb
    move-result-object v3

    #@ec
    .line 1741
    :cond_ec
    invoke-interface/range {v17 .. v17}, Lcom/android/server/location/LocationProviderInterface;->getStatusUpdateTime()J

    #@ef
    move-result-wide v11

    #@f0
    .line 1744
    .local v11, newStatusUpdateTime:J
    new-instance v6, Landroid/os/Bundle;

    #@f2
    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    #@f5
    .line 1745
    .local v6, extras:Landroid/os/Bundle;
    move-object/from16 v0, v17

    #@f7
    invoke-interface {v0, v6}, Lcom/android/server/location/LocationProviderInterface;->getStatus(Landroid/os/Bundle;)I

    #@fa
    move-result v27

    #@fb
    .line 1747
    .local v27, status:I
    const/4 v4, 0x0

    #@fc
    .line 1748
    .local v4, deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    const/4 v5, 0x0

    #@fd
    .line 1751
    .local v5, deadUpdateRecords:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@100
    move-result-object v7

    #@101
    .local v7, i$:Ljava/util/Iterator;
    :cond_101
    :goto_101
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@104
    move-result v28

    #@105
    if-eqz v28, :cond_2e2

    #@107
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10a
    move-result-object v22

    #@10b
    check-cast v22, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@10d
    .line 1752
    .local v22, r:Lcom/android/server/LocationManagerService$UpdateRecord;
    move-object/from16 v0, v22

    #@10f
    iget-object v0, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mReceiver:Lcom/android/server/LocationManagerService$Receiver;

    #@111
    move-object/from16 v23, v0

    #@113
    .line 1753
    .local v23, receiver:Lcom/android/server/LocationManagerService$Receiver;
    const/16 v24, 0x0

    #@115
    .line 1755
    .local v24, receiverDead:Z
    move-object/from16 v0, v23

    #@117
    iget v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mUid:I

    #@119
    move/from16 v28, v0

    #@11b
    invoke-static/range {v28 .. v28}, Landroid/os/UserHandle;->getUserId(I)I

    #@11e
    move-result v25

    #@11f
    .line 1756
    .local v25, receiverUserId:I
    move-object/from16 v0, p0

    #@121
    iget v0, v0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@123
    move/from16 v28, v0

    #@125
    move/from16 v0, v25

    #@127
    move/from16 v1, v28

    #@129
    if-eq v0, v1, :cond_185

    #@12b
    .line 1758
    const-string v28, "LocationManagerService"

    #@12d
    new-instance v29, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v30, "skipping loc update for background user "

    #@134
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v29

    #@138
    move-object/from16 v0, v29

    #@13a
    move/from16 v1, v25

    #@13c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v29

    #@140
    const-string v30, " (current user: "

    #@142
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v29

    #@146
    move-object/from16 v0, p0

    #@148
    iget v0, v0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@14a
    move/from16 v30, v0

    #@14c
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v29

    #@150
    const-string v30, ", app: "

    #@152
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v29

    #@156
    move-object/from16 v0, v23

    #@158
    iget-object v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mPackageName:Ljava/lang/String;

    #@15a
    move-object/from16 v30, v0

    #@15c
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v29

    #@160
    const-string v30, ")"

    #@162
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v29

    #@166
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v29

    #@16a
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16d
    goto :goto_101

    #@16e
    .line 1721
    .end local v3           #coarseLocation:Landroid/location/Location;
    .end local v4           #deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    .end local v5           #deadUpdateRecords:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    .end local v6           #extras:Landroid/os/Bundle;
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v11           #newStatusUpdateTime:J
    .end local v22           #r:Lcom/android/server/LocationManagerService$UpdateRecord;
    .end local v23           #receiver:Lcom/android/server/LocationManagerService$Receiver;
    .end local v24           #receiverDead:Z
    .end local v25           #receiverUserId:I
    .end local v26           #records:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    .end local v27           #status:I
    :cond_16e
    const-string v28, "noGPSLocation"

    #@170
    move-object/from16 v0, v28

    #@172
    invoke-virtual {v9, v0}, Landroid/location/Location;->getExtraLocation(Ljava/lang/String;)Landroid/location/Location;

    #@175
    move-result-object v10

    #@176
    .line 1722
    if-nez v13, :cond_ba

    #@178
    if-eqz v10, :cond_ba

    #@17a
    .line 1725
    const-string v28, "noGPSLocation"

    #@17c
    move-object/from16 v0, p1

    #@17e
    move-object/from16 v1, v28

    #@180
    invoke-virtual {v0, v1, v10}, Landroid/location/Location;->setExtraLocation(Ljava/lang/String;Landroid/location/Location;)V

    #@183
    goto/16 :goto_ba

    #@185
    .line 1765
    .restart local v3       #coarseLocation:Landroid/location/Location;
    .restart local v4       #deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    .restart local v5       #deadUpdateRecords:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    .restart local v6       #extras:Landroid/os/Bundle;
    .restart local v7       #i$:Ljava/util/Iterator;
    .restart local v11       #newStatusUpdateTime:J
    .restart local v22       #r:Lcom/android/server/LocationManagerService$UpdateRecord;
    .restart local v23       #receiver:Lcom/android/server/LocationManagerService$Receiver;
    .restart local v24       #receiverDead:Z
    .restart local v25       #receiverUserId:I
    .restart local v26       #records:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    .restart local v27       #status:I
    :cond_185
    move-object/from16 v0, p0

    #@187
    iget-object v0, v0, Lcom/android/server/LocationManagerService;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@189
    move-object/from16 v28, v0

    #@18b
    move-object/from16 v0, v23

    #@18d
    iget-object v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mPackageName:Ljava/lang/String;

    #@18f
    move-object/from16 v29, v0

    #@191
    invoke-virtual/range {v28 .. v29}, Lcom/android/server/location/LocationBlacklist;->isBlacklisted(Ljava/lang/String;)Z

    #@194
    move-result v28

    #@195
    if-eqz v28, :cond_1b7

    #@197
    .line 1766
    const-string v28, "LocationManagerService"

    #@199
    new-instance v29, Ljava/lang/StringBuilder;

    #@19b
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@19e
    const-string v30, "skipping loc update for blacklisted app: "

    #@1a0
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v29

    #@1a4
    move-object/from16 v0, v23

    #@1a6
    iget-object v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mPackageName:Ljava/lang/String;

    #@1a8
    move-object/from16 v30, v0

    #@1aa
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v29

    #@1ae
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b1
    move-result-object v29

    #@1b2
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b5
    goto/16 :goto_101

    #@1b7
    .line 1772
    :cond_1b7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1ba
    move-result-object v28

    #@1bb
    if-eqz v28, :cond_1fb

    #@1bd
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1c0
    move-result-object v28

    #@1c1
    const/16 v29, 0x2

    #@1c3
    const/16 v30, 0x0

    #@1c5
    move-object/from16 v0, v23

    #@1c7
    iget v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mUid:I

    #@1c9
    move/from16 v31, v0

    #@1cb
    invoke-static/range {v31 .. v31}, Landroid/os/UserHandle;->getUserId(I)I

    #@1ce
    move-result v31

    #@1cf
    move-object/from16 v0, v23

    #@1d1
    iget-object v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mPackageName:Ljava/lang/String;

    #@1d3
    move-object/from16 v32, v0

    #@1d5
    invoke-interface/range {v28 .. v32}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@1d8
    move-result v28

    #@1d9
    if-nez v28, :cond_1fb

    #@1db
    .line 1776
    const-string v28, "LocationManagerService"

    #@1dd
    new-instance v29, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    const-string v30, "MDM Control LocationManager Service, receiver.packageName :"

    #@1e4
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v29

    #@1e8
    move-object/from16 v0, v23

    #@1ea
    iget-object v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mPackageName:Ljava/lang/String;

    #@1ec
    move-object/from16 v30, v0

    #@1ee
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v29

    #@1f2
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f5
    move-result-object v29

    #@1f6
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f9
    goto/16 :goto_101

    #@1fb
    .line 1782
    :cond_1fb
    const/4 v14, 0x0

    #@1fc
    .line 1783
    .local v14, notifyLocation:Landroid/location/Location;
    move-object/from16 v0, v23

    #@1fe
    iget v0, v0, Lcom/android/server/LocationManagerService$Receiver;->mAllowedResolutionLevel:I

    #@200
    move/from16 v28, v0

    #@202
    const/16 v29, 0x2

    #@204
    move/from16 v0, v28

    #@206
    move/from16 v1, v29

    #@208
    if-ge v0, v1, :cond_2da

    #@20a
    .line 1784
    move-object v14, v3

    #@20b
    .line 1788
    :goto_20b
    if-eqz v14, :cond_256

    #@20d
    .line 1789
    move-object/from16 v0, v22

    #@20f
    iget-object v8, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mLastFixBroadcast:Landroid/location/Location;

    #@211
    .line 1790
    .local v8, lastLoc:Landroid/location/Location;
    if-eqz v8, :cond_21c

    #@213
    move-object/from16 v0, v22

    #@215
    move-wide v1, v15

    #@216
    invoke-static {v14, v8, v0, v1, v2}, Lcom/android/server/LocationManagerService;->shouldBroadcastSafe(Landroid/location/Location;Landroid/location/Location;Lcom/android/server/LocationManagerService$UpdateRecord;J)Z

    #@219
    move-result v28

    #@21a
    if-eqz v28, :cond_256

    #@21c
    .line 1791
    :cond_21c
    if-nez v8, :cond_2dd

    #@21e
    .line 1792
    new-instance v8, Landroid/location/Location;

    #@220
    .end local v8           #lastLoc:Landroid/location/Location;
    invoke-direct {v8, v14}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@223
    .line 1793
    .restart local v8       #lastLoc:Landroid/location/Location;
    move-object/from16 v0, v22

    #@225
    iput-object v8, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mLastFixBroadcast:Landroid/location/Location;

    #@227
    .line 1797
    :goto_227
    move-object/from16 v0, v23

    #@229
    invoke-virtual {v0, v14}, Lcom/android/server/LocationManagerService$Receiver;->callLocationChangedLocked(Landroid/location/Location;)Z

    #@22c
    move-result v28

    #@22d
    if-nez v28, :cond_24d

    #@22f
    .line 1798
    const-string v28, "LocationManagerService"

    #@231
    new-instance v29, Ljava/lang/StringBuilder;

    #@233
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@236
    const-string v30, "RemoteException calling onLocationChanged on "

    #@238
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v29

    #@23c
    move-object/from16 v0, v29

    #@23e
    move-object/from16 v1, v23

    #@240
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@243
    move-result-object v29

    #@244
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@247
    move-result-object v29

    #@248
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24b
    .line 1799
    const/16 v24, 0x1

    #@24d
    .line 1801
    :cond_24d
    move-object/from16 v0, v22

    #@24f
    iget-object v0, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@251
    move-object/from16 v28, v0

    #@253
    invoke-virtual/range {v28 .. v28}, Landroid/location/LocationRequest;->decrementNumUpdates()V

    #@256
    .line 1805
    .end local v8           #lastLoc:Landroid/location/Location;
    :cond_256
    move-object/from16 v0, v22

    #@258
    iget-wide v0, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mLastStatusBroadcast:J

    #@25a
    move-wide/from16 v18, v0

    #@25c
    .line 1806
    .local v18, prevStatusUpdateTime:J
    cmp-long v28, v11, v18

    #@25e
    if-lez v28, :cond_29c

    #@260
    const-wide/16 v28, 0x0

    #@262
    cmp-long v28, v18, v28

    #@264
    if-nez v28, :cond_26e

    #@266
    const/16 v28, 0x2

    #@268
    move/from16 v0, v27

    #@26a
    move/from16 v1, v28

    #@26c
    if-eq v0, v1, :cond_29c

    #@26e
    .line 1809
    :cond_26e
    move-object/from16 v0, v22

    #@270
    iput-wide v11, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mLastStatusBroadcast:J

    #@272
    .line 1810
    move-object/from16 v0, v23

    #@274
    move-object/from16 v1, v21

    #@276
    move/from16 v2, v27

    #@278
    invoke-virtual {v0, v1, v2, v6}, Lcom/android/server/LocationManagerService$Receiver;->callStatusChangedLocked(Ljava/lang/String;ILandroid/os/Bundle;)Z

    #@27b
    move-result v28

    #@27c
    if-nez v28, :cond_29c

    #@27e
    .line 1811
    const/16 v24, 0x1

    #@280
    .line 1812
    const-string v28, "LocationManagerService"

    #@282
    new-instance v29, Ljava/lang/StringBuilder;

    #@284
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@287
    const-string v30, "RemoteException calling onStatusChanged on "

    #@289
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v29

    #@28d
    move-object/from16 v0, v29

    #@28f
    move-object/from16 v1, v23

    #@291
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v29

    #@295
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@298
    move-result-object v29

    #@299
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29c
    .line 1817
    :cond_29c
    move-object/from16 v0, v22

    #@29e
    iget-object v0, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@2a0
    move-object/from16 v28, v0

    #@2a2
    invoke-virtual/range {v28 .. v28}, Landroid/location/LocationRequest;->getNumUpdates()I

    #@2a5
    move-result v28

    #@2a6
    if-lez v28, :cond_2b6

    #@2a8
    move-object/from16 v0, v22

    #@2aa
    iget-object v0, v0, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@2ac
    move-object/from16 v28, v0

    #@2ae
    invoke-virtual/range {v28 .. v28}, Landroid/location/LocationRequest;->getExpireAt()J

    #@2b1
    move-result-wide v28

    #@2b2
    cmp-long v28, v28, v15

    #@2b4
    if-gez v28, :cond_2c2

    #@2b6
    .line 1818
    :cond_2b6
    if-nez v5, :cond_2bd

    #@2b8
    .line 1819
    new-instance v5, Ljava/util/ArrayList;

    #@2ba
    .end local v5           #deadUpdateRecords:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@2bd
    .line 1821
    .restart local v5       #deadUpdateRecords:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    :cond_2bd
    move-object/from16 v0, v22

    #@2bf
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c2
    .line 1824
    :cond_2c2
    if-eqz v24, :cond_101

    #@2c4
    .line 1825
    if-nez v4, :cond_2cb

    #@2c6
    .line 1826
    new-instance v4, Ljava/util/ArrayList;

    #@2c8
    .end local v4           #deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@2cb
    .line 1828
    .restart local v4       #deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    :cond_2cb
    move-object/from16 v0, v23

    #@2cd
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@2d0
    move-result v28

    #@2d1
    if-nez v28, :cond_101

    #@2d3
    .line 1829
    move-object/from16 v0, v23

    #@2d5
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d8
    goto/16 :goto_101

    #@2da
    .line 1786
    .end local v18           #prevStatusUpdateTime:J
    :cond_2da
    move-object v14, v9

    #@2db
    goto/16 :goto_20b

    #@2dd
    .line 1795
    .restart local v8       #lastLoc:Landroid/location/Location;
    :cond_2dd
    invoke-virtual {v8, v14}, Landroid/location/Location;->set(Landroid/location/Location;)V

    #@2e0
    goto/16 :goto_227

    #@2e2
    .line 1835
    .end local v8           #lastLoc:Landroid/location/Location;
    .end local v14           #notifyLocation:Landroid/location/Location;
    .end local v22           #r:Lcom/android/server/LocationManagerService$UpdateRecord;
    .end local v23           #receiver:Lcom/android/server/LocationManagerService$Receiver;
    .end local v24           #receiverDead:Z
    .end local v25           #receiverUserId:I
    :cond_2e2
    if-eqz v4, :cond_2fc

    #@2e4
    .line 1836
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@2e7
    move-result-object v7

    #@2e8
    :goto_2e8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@2eb
    move-result v28

    #@2ec
    if-eqz v28, :cond_2fc

    #@2ee
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f1
    move-result-object v23

    #@2f2
    check-cast v23, Lcom/android/server/LocationManagerService$Receiver;

    #@2f4
    .line 1837
    .restart local v23       #receiver:Lcom/android/server/LocationManagerService$Receiver;
    move-object/from16 v0, p0

    #@2f6
    move-object/from16 v1, v23

    #@2f8
    invoke-direct {v0, v1}, Lcom/android/server/LocationManagerService;->removeUpdatesLocked(Lcom/android/server/LocationManagerService$Receiver;)V

    #@2fb
    goto :goto_2e8

    #@2fc
    .line 1840
    .end local v23           #receiver:Lcom/android/server/LocationManagerService$Receiver;
    :cond_2fc
    if-eqz v5, :cond_66

    #@2fe
    .line 1841
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@301
    move-result-object v7

    #@302
    :goto_302
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@305
    move-result v28

    #@306
    if-eqz v28, :cond_318

    #@308
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30b
    move-result-object v22

    #@30c
    check-cast v22, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@30e
    .line 1842
    .restart local v22       #r:Lcom/android/server/LocationManagerService$UpdateRecord;
    const/16 v28, 0x1

    #@310
    move-object/from16 v0, v22

    #@312
    move/from16 v1, v28

    #@314
    invoke-virtual {v0, v1}, Lcom/android/server/LocationManagerService$UpdateRecord;->disposeLocked(Z)V

    #@317
    goto :goto_302

    #@318
    .line 1844
    .end local v22           #r:Lcom/android/server/LocationManagerService$UpdateRecord;
    :cond_318
    move-object/from16 v0, p0

    #@31a
    move-object/from16 v1, v21

    #@31c
    invoke-direct {v0, v1}, Lcom/android/server/LocationManagerService;->applyRequirementsLocked(Ljava/lang/String;)V

    #@31f
    goto/16 :goto_66
.end method

.method private incrementPendingBroadcasts()V
    .registers 5

    #@0
    .prologue
    .line 1912
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    monitor-enter v2

    #@3
    .line 1913
    :try_start_3
    iget v1, p0, Lcom/android/server/LocationManagerService;->mPendingBroadcasts:I

    #@5
    add-int/lit8 v3, v1, 0x1

    #@7
    iput v3, p0, Lcom/android/server/LocationManagerService;->mPendingBroadcasts:I
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_20

    #@9
    if-nez v1, :cond_15

    #@b
    .line 1915
    :try_start_b
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@d
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@10
    .line 1916
    const-string v1, "Acquired wakelock"

    #@12
    invoke-direct {p0, v1}, Lcom/android/server/LocationManagerService;->log(Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_20
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_15} :catch_17

    #@15
    .line 1923
    :cond_15
    :goto_15
    :try_start_15
    monitor-exit v2

    #@16
    .line 1924
    return-void

    #@17
    .line 1917
    :catch_17
    move-exception v0

    #@18
    .line 1920
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "LocationManagerService"

    #@1a
    const-string v3, "exception in acquireWakeLock()"

    #@1c
    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_15

    #@20
    .line 1923
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_20
    move-exception v1

    #@21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_15 .. :try_end_22} :catchall_20

    #@22
    throw v1
.end method

.method private init()V
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 225
    const-string v0, "LocationManagerService"

    #@4
    const-string v1, "init()"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 227
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@b
    const-string v1, "power"

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v6

    #@11
    check-cast v6, Landroid/os/PowerManager;

    #@13
    .line 228
    .local v6, powerManager:Landroid/os/PowerManager;
    const-string v0, "LocationManagerService"

    #@15
    invoke-virtual {v6, v7, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1b
    .line 229
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@20
    move-result-object v0

    #@21
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@23
    .line 231
    new-instance v0, Lcom/android/server/location/LocationBlacklist;

    #@25
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@27
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@29
    invoke-direct {v0, v1, v2}, Lcom/android/server/location/LocationBlacklist;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@2c
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@2e
    .line 232
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@30
    invoke-virtual {v0}, Lcom/android/server/location/LocationBlacklist;->init()V

    #@33
    .line 233
    new-instance v0, Lcom/android/server/location/LocationFudger;

    #@35
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@37
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@39
    invoke-direct {v0, v1, v2}, Lcom/android/server/location/LocationFudger;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@3c
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mLocationFudger:Lcom/android/server/location/LocationFudger;

    #@3e
    .line 235
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@40
    monitor-enter v1

    #@41
    .line 236
    :try_start_41
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->loadProvidersLocked()V

    #@44
    .line 237
    monitor-exit v1
    :try_end_45
    .catchall {:try_start_41 .. :try_end_45} :catchall_8d

    #@45
    .line 239
    new-instance v0, Lcom/android/server/location/GeofenceManager;

    #@47
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@49
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/server/location/GeofenceManager;-><init>(Landroid/content/Context;Lcom/android/server/location/LocationBlacklist;)V

    #@4e
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mGeofenceManager:Lcom/android/server/location/GeofenceManager;

    #@50
    .line 242
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@52
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@55
    move-result-object v0

    #@56
    const-string v1, "location_providers_allowed"

    #@58
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@5b
    move-result-object v1

    #@5c
    new-instance v2, Lcom/android/server/LocationManagerService$1;

    #@5e
    iget-object v5, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@60
    invoke-direct {v2, p0, v5}, Lcom/android/server/LocationManagerService$1;-><init>(Lcom/android/server/LocationManagerService;Landroid/os/Handler;)V

    #@63
    const/4 v5, -0x1

    #@64
    invoke-virtual {v0, v1, v7, v2, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@67
    .line 252
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@69
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@6b
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v0, v1, v2, v7}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    #@72
    .line 255
    new-instance v3, Landroid/content/IntentFilter;

    #@74
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@77
    .line 256
    .local v3, intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.USER_SWITCHED"

    #@79
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@7c
    .line 258
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@7e
    new-instance v1, Lcom/android/server/LocationManagerService$2;

    #@80
    invoke-direct {v1, p0}, Lcom/android/server/LocationManagerService$2;-><init>(Lcom/android/server/LocationManagerService;)V

    #@83
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@85
    move-object v5, v4

    #@86
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@89
    .line 268
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->updateProvidersLocked()V

    #@8c
    .line 269
    return-void

    #@8d
    .line 237
    .end local v3           #intentFilter:Landroid/content/IntentFilter;
    :catchall_8d
    move-exception v0

    #@8e
    :try_start_8e
    monitor-exit v1
    :try_end_8f
    .catchall {:try_start_8e .. :try_end_8f} :catchall_8d

    #@8f
    throw v0
.end method

.method private isAllowedBySettingsLocked(Ljava/lang/String;I)Z
    .registers 6
    .parameter "provider"
    .parameter "userId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 701
    iget v2, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@3
    if-eq p2, v2, :cond_6

    #@5
    .line 713
    :cond_5
    :goto_5
    return v1

    #@6
    .line 704
    :cond_6
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@8
    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_10

    #@e
    .line 705
    const/4 v1, 0x1

    #@f
    goto :goto_5

    #@10
    .line 707
    :cond_10
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mDisabledProviders:Ljava/util/Set;

    #@12
    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_5

    #@18
    .line 711
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d
    move-result-object v0

    #@1e
    .line 713
    .local v0, resolver:Landroid/content/ContentResolver;
    iget v1, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@20
    invoke-static {v0, p1, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabledForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@23
    move-result v1

    #@24
    goto :goto_5
.end method

.method private loadProvidersLocked()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 341
    new-instance v9, Lcom/android/server/location/PassiveProvider;

    #@3
    invoke-direct {v9, p0}, Lcom/android/server/location/PassiveProvider;-><init>(Landroid/location/ILocationManager;)V

    #@6
    .line 342
    .local v9, passiveProvider:Lcom/android/server/location/PassiveProvider;
    invoke-direct {p0, v9}, Lcom/android/server/LocationManagerService;->addProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@9
    .line 343
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@b
    invoke-virtual {v9}, Lcom/android/server/location/PassiveProvider;->getName()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@12
    .line 344
    iput-object v9, p0, Lcom/android/server/LocationManagerService;->mPassiveProvider:Lcom/android/server/location/PassiveProvider;

    #@14
    .line 346
    invoke-static {}, Lcom/android/server/location/GpsLocationProvider;->isSupported()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_37

    #@1a
    .line 350
    new-instance v7, Lcom/android/server/location/LgeGpsLocationProvider;

    #@1c
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@1e
    invoke-direct {v7, v0, p0}, Lcom/android/server/location/LgeGpsLocationProvider;-><init>(Landroid/content/Context;Landroid/location/ILocationManager;)V

    #@21
    .line 353
    .local v7, gpsProvider:Lcom/android/server/location/GpsLocationProvider;
    invoke-virtual {v7}, Lcom/android/server/location/GpsLocationProvider;->getGpsStatusProvider()Landroid/location/IGpsStatusProvider;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

    #@27
    .line 354
    invoke-virtual {v7}, Lcom/android/server/location/GpsLocationProvider;->getNetInitiatedListener()Landroid/location/INetInitiatedListener;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mNetInitiatedListener:Landroid/location/INetInitiatedListener;

    #@2d
    .line 355
    invoke-direct {p0, v7}, Lcom/android/server/LocationManagerService;->addProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@30
    .line 356
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mRealProviders:Ljava/util/HashMap;

    #@32
    const-string v1, "gps"

    #@34
    invoke-virtual {v0, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    .line 370
    .end local v7           #gpsProvider:Lcom/android/server/location/GpsLocationProvider;
    :cond_37
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@39
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3c
    move-result-object v11

    #@3d
    .line 371
    .local v11, resources:Landroid/content/res/Resources;
    new-instance v3, Ljava/util/ArrayList;

    #@3f
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@42
    .line 372
    .local v3, providerPackageNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v0, 0x1070035

    #@45
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@48
    move-result-object v10

    #@49
    .line 374
    .local v10, pkgs:[Ljava/lang/String;
    const-string v0, "LocationManagerService"

    #@4b
    new-instance v1, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v2, "certificates for location providers pulled from: "

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-static {v10}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 376
    if-eqz v10, :cond_6e

    #@67
    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@6a
    move-result-object v0

    #@6b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@6e
    .line 378
    :cond_6e
    invoke-direct {p0, v3}, Lcom/android/server/LocationManagerService;->ensureFallbackFusedProviderPresentLocked(Ljava/util/ArrayList;)V

    #@71
    .line 381
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@73
    const-string v1, "network"

    #@75
    const-string v2, "com.android.location.service.v2.NetworkLocationProvider"

    #@77
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@79
    iget v5, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@7b
    invoke-static/range {v0 .. v5}, Lcom/android/server/location/LocationProviderProxy;->createAndBind(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;I)Lcom/android/server/location/LocationProviderProxy;

    #@7e
    move-result-object v8

    #@7f
    .line 386
    .local v8, networkProvider:Lcom/android/server/location/LocationProviderProxy;
    if-eqz v8, :cond_f7

    #@81
    .line 387
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mRealProviders:Ljava/util/HashMap;

    #@83
    const-string v1, "network"

    #@85
    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@88
    .line 388
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mProxyProviders:Ljava/util/ArrayList;

    #@8a
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8d
    .line 389
    invoke-direct {p0, v8}, Lcom/android/server/LocationManagerService;->addProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@90
    .line 395
    :goto_90
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@92
    const-string v1, "fused"

    #@94
    const-string v2, "com.android.location.service.FusedLocationProvider"

    #@96
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@98
    iget v5, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@9a
    invoke-static/range {v0 .. v5}, Lcom/android/server/location/LocationProviderProxy;->createAndBind(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;I)Lcom/android/server/location/LocationProviderProxy;

    #@9d
    move-result-object v6

    #@9e
    .line 400
    .local v6, fusedLocationProvider:Lcom/android/server/location/LocationProviderProxy;
    if-eqz v6, :cond_ff

    #@a0
    .line 401
    invoke-direct {p0, v6}, Lcom/android/server/LocationManagerService;->addProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@a3
    .line 402
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mProxyProviders:Ljava/util/ArrayList;

    #@a5
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a8
    .line 403
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@aa
    invoke-virtual {v6}, Lcom/android/server/location/LocationProviderProxy;->getName()Ljava/lang/String;

    #@ad
    move-result-object v1

    #@ae
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@b1
    .line 404
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mRealProviders:Ljava/util/HashMap;

    #@b3
    const-string v1, "fused"

    #@b5
    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b8
    .line 411
    :goto_b8
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@ba
    iget v1, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@bc
    invoke-static {v0, v3, v1}, Lcom/android/server/location/GeocoderProxy;->createAndBind(Landroid/content/Context;Ljava/util/List;I)Lcom/android/server/location/GeocoderProxy;

    #@bf
    move-result-object v0

    #@c0
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@c2
    .line 413
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@c4
    if-nez v0, :cond_cd

    #@c6
    .line 414
    const-string v0, "LocationManagerService"

    #@c8
    const-string v1, "no geocoder provider found"

    #@ca
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 417
    :cond_cd
    const v0, 0x1040039

    #@d0
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d3
    move-result-object v0

    #@d4
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mGeoFencerPackageName:Ljava/lang/String;

    #@d6
    .line 419
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mGeoFencerPackageName:Ljava/lang/String;

    #@d8
    if-eqz v0, :cond_10e

    #@da
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@dc
    new-instance v1, Landroid/content/Intent;

    #@de
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mGeoFencerPackageName:Ljava/lang/String;

    #@e0
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e3
    invoke-virtual {v0, v1, v12}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@e6
    move-result-object v0

    #@e7
    if-eqz v0, :cond_10e

    #@e9
    .line 421
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@eb
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mGeoFencerPackageName:Ljava/lang/String;

    #@ed
    invoke-static {v0, v1}, Lcom/android/server/location/GeoFencerProxy;->getGeoFencerProxy(Landroid/content/Context;Ljava/lang/String;)Lcom/android/server/location/GeoFencerProxy;

    #@f0
    move-result-object v0

    #@f1
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@f3
    .line 422
    const/4 v0, 0x1

    #@f4
    iput-boolean v0, p0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@f6
    .line 428
    :goto_f6
    return-void

    #@f7
    .line 391
    .end local v6           #fusedLocationProvider:Lcom/android/server/location/LocationProviderProxy;
    :cond_f7
    const-string v0, "LocationManagerService"

    #@f9
    const-string v1, "no network location provider found"

    #@fb
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@fe
    goto :goto_90

    #@ff
    .line 406
    .restart local v6       #fusedLocationProvider:Lcom/android/server/location/LocationProviderProxy;
    :cond_ff
    const-string v0, "LocationManagerService"

    #@101
    const-string v1, "no fused location provider found"

    #@103
    new-instance v2, Ljava/lang/IllegalStateException;

    #@105
    const-string v4, "Location service needs a fused location provider"

    #@107
    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10a
    invoke-static {v0, v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10d
    goto :goto_b8

    #@10e
    .line 424
    :cond_10e
    const/4 v0, 0x0

    #@10f
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@111
    .line 425
    iput-boolean v12, p0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@113
    goto :goto_f6
.end method

.method private log(Ljava/lang/String;)V
    .registers 4
    .parameter "log"

    #@0
    .prologue
    .line 2145
    const-string v0, "LocationManagerService"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 2146
    const-string v0, "LocationManagerService"

    #@b
    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 2148
    :cond_e
    return-void
.end method

.method private pickBest(Ljava/util/List;)Ljava/lang/String;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 940
    .local p1, providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v0, "gps"

    #@2
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 941
    const-string v0, "gps"

    #@a
    .line 945
    :goto_a
    return-object v0

    #@b
    .line 942
    :cond_b
    const-string v0, "network"

    #@d
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 943
    const-string v0, "network"

    #@15
    goto :goto_a

    #@16
    .line 945
    :cond_16
    const/4 v0, 0x0

    #@17
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Ljava/lang/String;

    #@1d
    goto :goto_a
.end method

.method private removeProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    .line 694
    invoke-interface {p1}, Lcom/android/server/location/LocationProviderInterface;->disable()V

    #@3
    .line 695
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 696
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@a
    invoke-interface {p1}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 697
    return-void
.end method

.method private removeUpdatesLocked(Lcom/android/server/LocationManagerService$Receiver;)V
    .registers 11
    .parameter "receiver"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1311
    const-string v5, "LocationManagerService"

    #@3
    new-instance v6, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v7, "remove "

    #@a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v6

    #@e
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@11
    move-result v7

    #@12
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@15
    move-result-object v7

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v6

    #@1e
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1313
    iget-object v5, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@23
    iget-object v6, p1, Lcom/android/server/LocationManagerService$Receiver;->mKey:Ljava/lang/Object;

    #@25
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v5

    #@29
    if-eqz v5, :cond_48

    #@2b
    invoke-virtual {p1}, Lcom/android/server/LocationManagerService$Receiver;->isListener()Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_48

    #@31
    .line 1314
    invoke-virtual {p1}, Lcom/android/server/LocationManagerService$Receiver;->getListener()Landroid/location/ILocationListener;

    #@34
    move-result-object v5

    #@35
    invoke-interface {v5}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    #@38
    move-result-object v5

    #@39
    invoke-interface {v5, p1, v8}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@3c
    .line 1315
    monitor-enter p1

    #@3d
    .line 1316
    :try_start_3d
    iget v5, p1, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@3f
    if-lez v5, :cond_47

    #@41
    .line 1317
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->decrementPendingBroadcasts()V

    #@44
    .line 1318
    const/4 v5, 0x0

    #@45
    iput v5, p1, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@47
    .line 1320
    :cond_47
    monitor-exit p1
    :try_end_48
    .catchall {:try_start_3d .. :try_end_48} :catchall_69

    #@48
    .line 1324
    :cond_48
    new-instance v3, Ljava/util/HashSet;

    #@4a
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    #@4d
    .line 1325
    .local v3, providers:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v1, p1, Lcom/android/server/LocationManagerService$Receiver;->mUpdateRecords:Ljava/util/HashMap;

    #@4f
    .line 1326
    .local v1, oldRecords:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    if-eqz v1, :cond_73

    #@51
    .line 1328
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@54
    move-result-object v5

    #@55
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@58
    move-result-object v0

    #@59
    .local v0, i$:Ljava/util/Iterator;
    :goto_59
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@5c
    move-result v5

    #@5d
    if-eqz v5, :cond_6c

    #@5f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@62
    move-result-object v4

    #@63
    check-cast v4, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@65
    .line 1329
    .local v4, record:Lcom/android/server/LocationManagerService$UpdateRecord;
    invoke-virtual {v4, v8}, Lcom/android/server/LocationManagerService$UpdateRecord;->disposeLocked(Z)V

    #@68
    goto :goto_59

    #@69
    .line 1320
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #oldRecords:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    .end local v3           #providers:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v4           #record:Lcom/android/server/LocationManagerService$UpdateRecord;
    :catchall_69
    move-exception v5

    #@6a
    :try_start_6a
    monitor-exit p1
    :try_end_6b
    .catchall {:try_start_6a .. :try_end_6b} :catchall_69

    #@6b
    throw v5

    #@6c
    .line 1332
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v1       #oldRecords:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    .restart local v3       #providers:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_6c
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v3, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@73
    .line 1336
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_73
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@76
    move-result-object v0

    #@77
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_77
    :goto_77
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@7a
    move-result v5

    #@7b
    if-eqz v5, :cond_8f

    #@7d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@80
    move-result-object v2

    #@81
    check-cast v2, Ljava/lang/String;

    #@83
    .line 1338
    .local v2, provider:Ljava/lang/String;
    iget v5, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@85
    invoke-direct {p0, v2, v5}, Lcom/android/server/LocationManagerService;->isAllowedBySettingsLocked(Ljava/lang/String;I)Z

    #@88
    move-result v5

    #@89
    if-eqz v5, :cond_77

    #@8b
    .line 1342
    invoke-direct {p0, v2}, Lcom/android/server/LocationManagerService;->applyRequirementsLocked(Ljava/lang/String;)V

    #@8e
    goto :goto_77

    #@8f
    .line 1344
    .end local v2           #provider:Ljava/lang/String;
    :cond_8f
    return-void
.end method

.method private requestLocationUpdatesLocked(Landroid/location/LocationRequest;Lcom/android/server/LocationManagerService$Receiver;IILjava/lang/String;)V
    .registers 15
    .parameter "request"
    .parameter "receiver"
    .parameter "pid"
    .parameter "uid"
    .parameter "packageName"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1262
    if-nez p1, :cond_5

    #@3
    sget-object p1, Lcom/android/server/LocationManagerService;->DEFAULT_LOCATION_REQUEST:Landroid/location/LocationRequest;

    #@5
    .line 1263
    :cond_5
    invoke-virtual {p1}, Landroid/location/LocationRequest;->getProvider()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 1264
    .local v1, name:Ljava/lang/String;
    if-nez v1, :cond_13

    #@b
    .line 1265
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v6, "provider name must not be null"

    #@f
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v5

    #@13
    .line 1267
    :cond_13
    iget-object v5, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@15
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v3

    #@19
    check-cast v3, Lcom/android/server/location/LocationProviderInterface;

    #@1b
    .line 1268
    .local v3, provider:Lcom/android/server/location/LocationProviderInterface;
    if-nez v3, :cond_36

    #@1d
    .line 1269
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@1f
    new-instance v6, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v7, "provider doesn\'t exisit: "

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v5

    #@36
    .line 1272
    :cond_36
    const-string v5, "LocationManagerService"

    #@38
    new-instance v6, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v7, "request "

    #@3f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@46
    move-result v7

    #@47
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    const-string v7, " "

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    const-string v7, " "

    #@5b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v6

    #@63
    const-string v7, " from "

    #@65
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    const-string v7, "("

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v6

    #@77
    const-string v7, ")"

    #@79
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v6

    #@7d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1275
    new-instance v4, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@86
    invoke-direct {v4, p0, v1, p1, p2}, Lcom/android/server/LocationManagerService$UpdateRecord;-><init>(Lcom/android/server/LocationManagerService;Ljava/lang/String;Landroid/location/LocationRequest;Lcom/android/server/LocationManagerService$Receiver;)V

    #@89
    .line 1276
    .local v4, record:Lcom/android/server/LocationManagerService$UpdateRecord;
    iget-object v5, p2, Lcom/android/server/LocationManagerService$Receiver;->mUpdateRecords:Ljava/util/HashMap;

    #@8b
    invoke-virtual {v5, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8e
    move-result-object v2

    #@8f
    check-cast v2, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@91
    .line 1277
    .local v2, oldRecord:Lcom/android/server/LocationManagerService$UpdateRecord;
    if-eqz v2, :cond_96

    #@93
    .line 1278
    invoke-virtual {v2, v8}, Lcom/android/server/LocationManagerService$UpdateRecord;->disposeLocked(Z)V

    #@96
    .line 1281
    :cond_96
    invoke-static {p4}, Landroid/os/UserHandle;->getUserId(I)I

    #@99
    move-result v5

    #@9a
    invoke-direct {p0, v1, v5}, Lcom/android/server/LocationManagerService;->isAllowedBySettingsLocked(Ljava/lang/String;I)Z

    #@9d
    move-result v0

    #@9e
    .line 1282
    .local v0, isProviderEnabled:Z
    if-eqz v0, :cond_a4

    #@a0
    .line 1283
    invoke-direct {p0, v1}, Lcom/android/server/LocationManagerService;->applyRequirementsLocked(Ljava/lang/String;)V

    #@a3
    .line 1288
    :goto_a3
    return-void

    #@a4
    .line 1286
    :cond_a4
    invoke-virtual {p2, v1, v8}, Lcom/android/server/LocationManagerService$Receiver;->callProviderEnabledLocked(Ljava/lang/String;Z)Z

    #@a7
    goto :goto_a3
.end method

.method private static shouldBroadcastSafe(Landroid/location/Location;Landroid/location/Location;Lcom/android/server/LocationManagerService$UpdateRecord;J)Z
    .registers 13
    .parameter "loc"
    .parameter "lastLoc"
    .parameter "record"
    .parameter "now"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1649
    if-nez p1, :cond_5

    #@4
    .line 1686
    :cond_4
    :goto_4
    return v2

    #@5
    .line 1662
    :cond_5
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    #@8
    move-result-wide v4

    #@9
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    #@c
    move-result-wide v6

    #@d
    cmp-long v4, v4, v6

    #@f
    if-nez v4, :cond_13

    #@11
    move v2, v3

    #@12
    .line 1663
    goto :goto_4

    #@13
    .line 1669
    :cond_13
    iget-object v4, p2, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@15
    invoke-virtual {v4}, Landroid/location/LocationRequest;->getSmallestDisplacement()F

    #@18
    move-result v4

    #@19
    float-to-double v0, v4

    #@1a
    .line 1670
    .local v0, minDistance:D
    const-wide/16 v4, 0x0

    #@1c
    cmpl-double v4, v0, v4

    #@1e
    if-lez v4, :cond_2b

    #@20
    .line 1671
    invoke-virtual {p0, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    #@23
    move-result v4

    #@24
    float-to-double v4, v4

    #@25
    cmpg-double v4, v4, v0

    #@27
    if-gtz v4, :cond_2b

    #@29
    move v2, v3

    #@2a
    .line 1672
    goto :goto_4

    #@2b
    .line 1677
    :cond_2b
    iget-object v4, p2, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@2d
    invoke-virtual {v4}, Landroid/location/LocationRequest;->getNumUpdates()I

    #@30
    move-result v4

    #@31
    if-gtz v4, :cond_35

    #@33
    move v2, v3

    #@34
    .line 1678
    goto :goto_4

    #@35
    .line 1682
    :cond_35
    iget-object v4, p2, Lcom/android/server/LocationManagerService$UpdateRecord;->mRequest:Landroid/location/LocationRequest;

    #@37
    invoke-virtual {v4}, Landroid/location/LocationRequest;->getExpireAt()J

    #@3a
    move-result-wide v4

    #@3b
    cmp-long v4, v4, p3

    #@3d
    if-gez v4, :cond_4

    #@3f
    move v2, v3

    #@40
    .line 1683
    goto :goto_4
.end method

.method private switchUser(I)V
    .registers 8
    .parameter "userId"

    #@0
    .prologue
    .line 435
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@2
    invoke-virtual {v2, p1}, Lcom/android/server/location/LocationBlacklist;->switchUser(I)V

    #@5
    .line 436
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v3

    #@8
    .line 437
    :try_start_8
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@a
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@d
    .line 438
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v0

    #@13
    .local v0, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_30

    #@19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Lcom/android/server/location/LocationProviderInterface;

    #@1f
    .line 439
    .local v1, p:Lcom/android/server/location/LocationProviderInterface;
    invoke-interface {v1}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    const/4 v4, 0x0

    #@24
    iget v5, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@26
    invoke-direct {p0, v2, v4, v5}, Lcom/android/server/LocationManagerService;->updateProviderListenersLocked(Ljava/lang/String;ZI)V

    #@29
    .line 440
    invoke-interface {v1, p1}, Lcom/android/server/location/LocationProviderInterface;->switchUser(I)V

    #@2c
    goto :goto_13

    #@2d
    .line 444
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #p:Lcom/android/server/location/LocationProviderInterface;
    :catchall_2d
    move-exception v2

    #@2e
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_2d

    #@2f
    throw v2

    #@30
    .line 442
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_30
    :try_start_30
    iput p1, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@32
    .line 443
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->updateProvidersLocked()V

    #@35
    .line 444
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_30 .. :try_end_36} :catchall_2d

    #@36
    .line 445
    return-void
.end method

.method private updateProviderListenersLocked(Ljava/lang/String;ZI)V
    .registers 12
    .parameter "provider"
    .parameter "enabled"
    .parameter "userId"

    #@0
    .prologue
    .line 984
    const/4 v3, 0x0

    #@1
    .line 986
    .local v3, listeners:I
    iget-object v7, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@3
    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v4

    #@7
    check-cast v4, Lcom/android/server/location/LocationProviderInterface;

    #@9
    .line 987
    .local v4, p:Lcom/android/server/location/LocationProviderInterface;
    if-nez v4, :cond_c

    #@b
    .line 1023
    :cond_b
    :goto_b
    return-void

    #@c
    .line 989
    :cond_c
    const/4 v1, 0x0

    #@d
    .line 991
    .local v1, deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    iget-object v7, p0, Lcom/android/server/LocationManagerService;->mRecordsByProvider:Ljava/util/HashMap;

    #@f
    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v6

    #@13
    check-cast v6, Ljava/util/ArrayList;

    #@15
    .line 992
    .local v6, records:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;"
    if-eqz v6, :cond_47

    #@17
    .line 993
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v0

    #@1b
    .line 994
    .local v0, N:I
    const/4 v2, 0x0

    #@1c
    .local v2, i:I
    :goto_1c
    if-ge v2, v0, :cond_47

    #@1e
    .line 995
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@24
    .line 996
    .local v5, record:Lcom/android/server/LocationManagerService$UpdateRecord;
    iget-object v7, v5, Lcom/android/server/LocationManagerService$UpdateRecord;->mReceiver:Lcom/android/server/LocationManagerService$Receiver;

    #@26
    iget v7, v7, Lcom/android/server/LocationManagerService$Receiver;->mUid:I

    #@28
    invoke-static {v7}, Landroid/os/UserHandle;->getUserId(I)I

    #@2b
    move-result v7

    #@2c
    if-ne v7, p3, :cond_44

    #@2e
    .line 998
    iget-object v7, v5, Lcom/android/server/LocationManagerService$UpdateRecord;->mReceiver:Lcom/android/server/LocationManagerService$Receiver;

    #@30
    invoke-virtual {v7, p1, p2}, Lcom/android/server/LocationManagerService$Receiver;->callProviderEnabledLocked(Ljava/lang/String;Z)Z

    #@33
    move-result v7

    #@34
    if-nez v7, :cond_42

    #@36
    .line 999
    if-nez v1, :cond_3d

    #@38
    .line 1000
    new-instance v1, Ljava/util/ArrayList;

    #@3a
    .end local v1           #deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@3d
    .line 1002
    .restart local v1       #deadReceivers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$Receiver;>;"
    :cond_3d
    iget-object v7, v5, Lcom/android/server/LocationManagerService$UpdateRecord;->mReceiver:Lcom/android/server/LocationManagerService$Receiver;

    #@3f
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    .line 1004
    :cond_42
    add-int/lit8 v3, v3, 0x1

    #@44
    .line 994
    :cond_44
    add-int/lit8 v2, v2, 0x1

    #@46
    goto :goto_1c

    #@47
    .line 1009
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v5           #record:Lcom/android/server/LocationManagerService$UpdateRecord;
    :cond_47
    if-eqz v1, :cond_5d

    #@49
    .line 1010
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@4c
    move-result v7

    #@4d
    add-int/lit8 v2, v7, -0x1

    #@4f
    .restart local v2       #i:I
    :goto_4f
    if-ltz v2, :cond_5d

    #@51
    .line 1011
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@54
    move-result-object v7

    #@55
    check-cast v7, Lcom/android/server/LocationManagerService$Receiver;

    #@57
    invoke-direct {p0, v7}, Lcom/android/server/LocationManagerService;->removeUpdatesLocked(Lcom/android/server/LocationManagerService$Receiver;)V

    #@5a
    .line 1010
    add-int/lit8 v2, v2, -0x1

    #@5c
    goto :goto_4f

    #@5d
    .line 1015
    .end local v2           #i:I
    :cond_5d
    if-eqz p2, :cond_68

    #@5f
    .line 1016
    invoke-interface {v4}, Lcom/android/server/location/LocationProviderInterface;->enable()V

    #@62
    .line 1017
    if-lez v3, :cond_b

    #@64
    .line 1018
    invoke-direct {p0, p1}, Lcom/android/server/LocationManagerService;->applyRequirementsLocked(Ljava/lang/String;)V

    #@67
    goto :goto_b

    #@68
    .line 1021
    :cond_68
    invoke-interface {v4}, Lcom/android/server/location/LocationProviderInterface;->disable()V

    #@6b
    goto :goto_b
.end method

.method private updateProvidersLocked()V
    .registers 10

    #@0
    .prologue
    .line 963
    const/4 v0, 0x0

    #@1
    .line 964
    .local v0, changesMade:Z
    iget-object v6, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v6

    #@7
    add-int/lit8 v1, v6, -0x1

    #@9
    .local v1, i:I
    :goto_9
    if-ltz v1, :cond_3b

    #@b
    .line 965
    iget-object v6, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v4

    #@11
    check-cast v4, Lcom/android/server/location/LocationProviderInterface;

    #@13
    .line 966
    .local v4, p:Lcom/android/server/location/LocationProviderInterface;
    invoke-interface {v4}, Lcom/android/server/location/LocationProviderInterface;->isEnabled()Z

    #@16
    move-result v2

    #@17
    .line 967
    .local v2, isEnabled:Z
    invoke-interface {v4}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    .line 968
    .local v3, name:Ljava/lang/String;
    iget v6, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@1d
    invoke-direct {p0, v3, v6}, Lcom/android/server/LocationManagerService;->isAllowedBySettingsLocked(Ljava/lang/String;I)Z

    #@20
    move-result v5

    #@21
    .line 969
    .local v5, shouldBeEnabled:Z
    if-eqz v2, :cond_2f

    #@23
    if-nez v5, :cond_2f

    #@25
    .line 970
    const/4 v6, 0x0

    #@26
    iget v7, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@28
    invoke-direct {p0, v3, v6, v7}, Lcom/android/server/LocationManagerService;->updateProviderListenersLocked(Ljava/lang/String;ZI)V

    #@2b
    .line 971
    const/4 v0, 0x1

    #@2c
    .line 964
    :cond_2c
    :goto_2c
    add-int/lit8 v1, v1, -0x1

    #@2e
    goto :goto_9

    #@2f
    .line 972
    :cond_2f
    if-nez v2, :cond_2c

    #@31
    if-eqz v5, :cond_2c

    #@33
    .line 973
    const/4 v6, 0x1

    #@34
    iget v7, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@36
    invoke-direct {p0, v3, v6, v7}, Lcom/android/server/LocationManagerService;->updateProviderListenersLocked(Ljava/lang/String;ZI)V

    #@39
    .line 974
    const/4 v0, 0x1

    #@3a
    goto :goto_2c

    #@3b
    .line 977
    .end local v2           #isEnabled:Z
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #p:Lcom/android/server/location/LocationProviderInterface;
    .end local v5           #shouldBeEnabled:Z
    :cond_3b
    if-eqz v0, :cond_4b

    #@3d
    .line 978
    iget-object v6, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@3f
    new-instance v7, Landroid/content/Intent;

    #@41
    const-string v8, "android.location.PROVIDERS_CHANGED"

    #@43
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@46
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@48
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@4b
    .line 981
    :cond_4b
    return-void
.end method


# virtual methods
.method public addGpsStatusListener(Landroid/location/IGpsStatusListener;)Z
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1476
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1488
    :goto_5
    return v1

    #@6
    .line 1479
    :cond_6
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@9
    move-result v2

    #@a
    const-string v3, "gps"

    #@c
    invoke-direct {p0, v2, v3}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V

    #@f
    .line 1483
    :try_start_f
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

    #@11
    invoke-interface {v2, p1}, Landroid/location/IGpsStatusProvider;->addGpsStatusListener(Landroid/location/IGpsStatusListener;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    .line 1488
    const/4 v1, 0x1

    #@15
    goto :goto_5

    #@16
    .line 1484
    :catch_16
    move-exception v0

    #@17
    .line 1485
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "LocationManagerService"

    #@19
    const-string v3, "mGpsStatusProvider.addGpsStatusListener failed"

    #@1b
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    goto :goto_5
.end method

.method public addTestProvider(Ljava/lang/String;Lcom/android/internal/location/ProviderProperties;)V
    .registers 11
    .parameter "name"
    .parameter "properties"

    #@0
    .prologue
    .line 1995
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 1997
    const-string v4, "passive"

    #@5
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v4

    #@9
    if-eqz v4, :cond_13

    #@b
    .line 1998
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v5, "Cannot mock the passive location provider"

    #@f
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v4

    #@13
    .line 2001
    :cond_13
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@16
    move-result-wide v0

    #@17
    .line 2002
    .local v0, identity:J
    iget-object v5, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@19
    monitor-enter v5

    #@1a
    .line 2003
    :try_start_1a
    new-instance v3, Lcom/android/server/location/MockProvider;

    #@1c
    invoke-direct {v3, p1, p0, p2}, Lcom/android/server/location/MockProvider;-><init>(Ljava/lang/String;Landroid/location/ILocationManager;Lcom/android/internal/location/ProviderProperties;)V

    #@1f
    .line 2005
    .local v3, provider:Lcom/android/server/location/MockProvider;
    const-string v4, "gps"

    #@21
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v4

    #@25
    if-nez v4, :cond_37

    #@27
    const-string v4, "network"

    #@29
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v4

    #@2d
    if-nez v4, :cond_37

    #@2f
    const-string v4, "fused"

    #@31
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v4

    #@35
    if-eqz v4, :cond_44

    #@37
    .line 2008
    :cond_37
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@39
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    move-result-object v2

    #@3d
    check-cast v2, Lcom/android/server/location/LocationProviderInterface;

    #@3f
    .line 2009
    .local v2, p:Lcom/android/server/location/LocationProviderInterface;
    if-eqz v2, :cond_44

    #@41
    .line 2010
    invoke-direct {p0, v2}, Lcom/android/server/LocationManagerService;->removeProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@44
    .line 2013
    .end local v2           #p:Lcom/android/server/location/LocationProviderInterface;
    :cond_44
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@46
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    move-result-object v4

    #@4a
    if-eqz v4, :cond_6e

    #@4c
    .line 2014
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@4e
    new-instance v6, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v7, "Provider \""

    #@55
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v6

    #@5d
    const-string v7, "\" already exists"

    #@5f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v6

    #@63
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v6

    #@67
    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6a
    throw v4

    #@6b
    .line 2023
    .end local v3           #provider:Lcom/android/server/location/MockProvider;
    :catchall_6b
    move-exception v4

    #@6c
    monitor-exit v5
    :try_end_6d
    .catchall {:try_start_1a .. :try_end_6d} :catchall_6b

    #@6d
    throw v4

    #@6e
    .line 2017
    .restart local v3       #provider:Lcom/android/server/location/MockProvider;
    :cond_6e
    const/4 v4, 0x0

    #@6f
    :try_start_6f
    iput-boolean v4, p0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@71
    .line 2019
    invoke-direct {p0, v3}, Lcom/android/server/LocationManagerService;->addProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@74
    .line 2020
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@76
    invoke-virtual {v4, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@79
    .line 2021
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@7b
    const/4 v6, 0x0

    #@7c
    invoke-virtual {v4, p1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7f
    .line 2022
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->updateProvidersLocked()V

    #@82
    .line 2023
    monitor-exit v5
    :try_end_83
    .catchall {:try_start_6f .. :try_end_83} :catchall_6b

    #@83
    .line 2024
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@86
    .line 2025
    return-void
.end method

.method public clearTestProviderEnabled(Ljava/lang/String;)V
    .registers 9
    .parameter "provider"

    #@0
    .prologue
    .line 2106
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 2107
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v4

    #@6
    .line 2108
    :try_start_6
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@8
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Lcom/android/server/location/MockProvider;

    #@e
    .line 2109
    .local v2, mockProvider:Lcom/android/server/location/MockProvider;
    if-nez v2, :cond_32

    #@10
    .line 2110
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "Provider \""

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    const-string v6, "\" unknown"

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v3

    #@2f
    .line 2117
    .end local v2           #mockProvider:Lcom/android/server/location/MockProvider;
    :catchall_2f
    move-exception v3

    #@30
    monitor-exit v4
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_2f

    #@31
    throw v3

    #@32
    .line 2112
    .restart local v2       #mockProvider:Lcom/android/server/location/MockProvider;
    :cond_32
    :try_start_32
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@35
    move-result-wide v0

    #@36
    .line 2113
    .local v0, identity:J
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@38
    invoke-interface {v3, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    #@3b
    .line 2114
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mDisabledProviders:Ljava/util/Set;

    #@3d
    invoke-interface {v3, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    #@40
    .line 2115
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->updateProvidersLocked()V

    #@43
    .line 2116
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@46
    .line 2117
    monitor-exit v4
    :try_end_47
    .catchall {:try_start_32 .. :try_end_47} :catchall_2f

    #@47
    .line 2118
    return-void
.end method

.method public clearTestProviderLocation(Ljava/lang/String;)V
    .registers 7
    .parameter "provider"

    #@0
    .prologue
    .line 2071
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 2072
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v2

    #@6
    .line 2073
    :try_start_6
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Lcom/android/server/location/MockProvider;

    #@e
    .line 2074
    .local v0, mockProvider:Lcom/android/server/location/MockProvider;
    if-nez v0, :cond_32

    #@10
    .line 2075
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Provider \""

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, "\" unknown"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 2078
    .end local v0           #mockProvider:Lcom/android/server/location/MockProvider;
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_2f

    #@31
    throw v1

    #@32
    .line 2077
    .restart local v0       #mockProvider:Lcom/android/server/location/MockProvider;
    :cond_32
    :try_start_32
    invoke-virtual {v0}, Lcom/android/server/location/MockProvider;->clearLocation()V

    #@35
    .line 2078
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_32 .. :try_end_36} :catchall_2f

    #@36
    .line 2079
    return-void
.end method

.method public clearTestProviderStatus(Ljava/lang/String;)V
    .registers 7
    .parameter "provider"

    #@0
    .prologue
    .line 2134
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 2135
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v2

    #@6
    .line 2136
    :try_start_6
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Lcom/android/server/location/MockProvider;

    #@e
    .line 2137
    .local v0, mockProvider:Lcom/android/server/location/MockProvider;
    if-nez v0, :cond_32

    #@10
    .line 2138
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Provider \""

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, "\" unknown"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 2141
    .end local v0           #mockProvider:Lcom/android/server/location/MockProvider;
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_2f

    #@31
    throw v1

    #@32
    .line 2140
    .restart local v0       #mockProvider:Lcom/android/server/location/MockProvider;
    :cond_32
    :try_start_32
    invoke-virtual {v0}, Lcom/android/server/location/MockProvider;->clearStatus()V

    #@35
    .line 2141
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_32 .. :try_end_36} :catchall_2f

    #@36
    .line 2142
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 21
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2152
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@4
    const-string v15, "android.permission.DUMP"

    #@6
    invoke-virtual {v14, v15}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@9
    move-result v14

    #@a
    if-eqz v14, :cond_37

    #@c
    .line 2154
    new-instance v14, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v15, "Permission Denial: can\'t dump LocationManagerService from from pid="

    #@13
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v14

    #@17
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1a
    move-result v15

    #@1b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v14

    #@1f
    const-string v15, ", uid="

    #@21
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v14

    #@25
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@28
    move-result v15

    #@29
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v14

    #@2d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v14

    #@31
    move-object/from16 v0, p2

    #@33
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@36
    .line 2223
    :goto_36
    return-void

    #@37
    .line 2160
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget-object v15, v0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@3b
    monitor-enter v15

    #@3c
    .line 2161
    :try_start_3c
    const-string v14, "Current Location Manager state:"

    #@3e
    move-object/from16 v0, p2

    #@40
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@43
    .line 2162
    const-string v14, "  Location Listeners:"

    #@45
    move-object/from16 v0, p2

    #@47
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4a
    .line 2163
    move-object/from16 v0, p0

    #@4c
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@4e
    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@51
    move-result-object v14

    #@52
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@55
    move-result-object v7

    #@56
    .local v7, i$:Ljava/util/Iterator;
    :goto_56
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@59
    move-result v14

    #@5a
    if-eqz v14, :cond_80

    #@5c
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5f
    move-result-object v12

    #@60
    check-cast v12, Lcom/android/server/LocationManagerService$Receiver;

    #@62
    .line 2164
    .local v12, receiver:Lcom/android/server/LocationManagerService$Receiver;
    new-instance v14, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v16, "    "

    #@69
    move-object/from16 v0, v16

    #@6b
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v14

    #@6f
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v14

    #@73
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v14

    #@77
    move-object/from16 v0, p2

    #@79
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7c
    goto :goto_56

    #@7d
    .line 2222
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v12           #receiver:Lcom/android/server/LocationManagerService$Receiver;
    :catchall_7d
    move-exception v14

    #@7e
    monitor-exit v15
    :try_end_7f
    .catchall {:try_start_3c .. :try_end_7f} :catchall_7d

    #@7f
    throw v14

    #@80
    .line 2166
    .restart local v7       #i$:Ljava/util/Iterator;
    :cond_80
    :try_start_80
    const-string v14, "  Records by Provider:"

    #@82
    move-object/from16 v0, p2

    #@84
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@87
    .line 2167
    move-object/from16 v0, p0

    #@89
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mRecordsByProvider:Ljava/util/HashMap;

    #@8b
    invoke-virtual {v14}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@8e
    move-result-object v14

    #@8f
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@92
    move-result-object v7

    #@93
    .end local v7           #i$:Ljava/util/Iterator;
    :cond_93
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@96
    move-result v14

    #@97
    if-eqz v14, :cond_fa

    #@99
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9c
    move-result-object v4

    #@9d
    check-cast v4, Ljava/util/Map$Entry;

    #@9f
    .line 2168
    .local v4, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;>;"
    new-instance v14, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v16, "    "

    #@a6
    move-object/from16 v0, v16

    #@a8
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v16

    #@ac
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@af
    move-result-object v14

    #@b0
    check-cast v14, Ljava/lang/String;

    #@b2
    move-object/from16 v0, v16

    #@b4
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v14

    #@b8
    const-string v16, ":"

    #@ba
    move-object/from16 v0, v16

    #@bc
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v14

    #@c0
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v14

    #@c4
    move-object/from16 v0, p2

    #@c6
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c9
    .line 2169
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@cc
    move-result-object v14

    #@cd
    check-cast v14, Ljava/util/ArrayList;

    #@cf
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d2
    move-result-object v8

    #@d3
    .local v8, i$:Ljava/util/Iterator;
    :goto_d3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@d6
    move-result v14

    #@d7
    if-eqz v14, :cond_93

    #@d9
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@dc
    move-result-object v13

    #@dd
    check-cast v13, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@df
    .line 2170
    .local v13, record:Lcom/android/server/LocationManagerService$UpdateRecord;
    new-instance v14, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v16, "      "

    #@e6
    move-object/from16 v0, v16

    #@e8
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v14

    #@ec
    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v14

    #@f0
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3
    move-result-object v14

    #@f4
    move-object/from16 v0, p2

    #@f6
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f9
    goto :goto_d3

    #@fa
    .line 2173
    .end local v4           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/server/LocationManagerService$UpdateRecord;>;>;"
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v13           #record:Lcom/android/server/LocationManagerService$UpdateRecord;
    :cond_fa
    const-string v14, "  Last Known Locations:"

    #@fc
    move-object/from16 v0, p2

    #@fe
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@101
    .line 2174
    move-object/from16 v0, p0

    #@103
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@105
    invoke-virtual {v14}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@108
    move-result-object v14

    #@109
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@10c
    move-result-object v7

    #@10d
    .restart local v7       #i$:Ljava/util/Iterator;
    :goto_10d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@110
    move-result v14

    #@111
    if-eqz v14, :cond_14c

    #@113
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@116
    move-result-object v3

    #@117
    check-cast v3, Ljava/util/Map$Entry;

    #@119
    .line 2175
    .local v3, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/location/Location;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@11c
    move-result-object v10

    #@11d
    check-cast v10, Ljava/lang/String;

    #@11f
    .line 2176
    .local v10, provider:Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@122
    move-result-object v9

    #@123
    check-cast v9, Landroid/location/Location;

    #@125
    .line 2177
    .local v9, location:Landroid/location/Location;
    new-instance v14, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v16, "    "

    #@12c
    move-object/from16 v0, v16

    #@12e
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v14

    #@132
    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v14

    #@136
    const-string v16, ": "

    #@138
    move-object/from16 v0, v16

    #@13a
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v14

    #@13e
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v14

    #@142
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v14

    #@146
    move-object/from16 v0, p2

    #@148
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@14b
    goto :goto_10d

    #@14c
    .line 2180
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/location/Location;>;"
    .end local v9           #location:Landroid/location/Location;
    .end local v10           #provider:Ljava/lang/String;
    :cond_14c
    move-object/from16 v0, p0

    #@14e
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mGeofenceManager:Lcom/android/server/location/GeofenceManager;

    #@150
    move-object/from16 v0, p2

    #@152
    invoke-virtual {v14, v0}, Lcom/android/server/location/GeofenceManager;->dump(Ljava/io/PrintWriter;)V

    #@155
    .line 2181
    move-object/from16 v0, p0

    #@157
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@159
    if-eqz v14, :cond_16e

    #@15b
    move-object/from16 v0, p0

    #@15d
    iget-boolean v14, v0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@15f
    if-eqz v14, :cond_16e

    #@161
    .line 2182
    move-object/from16 v0, p0

    #@163
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@165
    const-string v16, ""

    #@167
    move-object/from16 v0, p2

    #@169
    move-object/from16 v1, v16

    #@16b
    invoke-virtual {v14, v0, v1}, Lcom/android/server/location/GeoFencerBase;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@16e
    .line 2185
    :cond_16e
    move-object/from16 v0, p0

    #@170
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@172
    invoke-interface {v14}, Ljava/util/Set;->size()I

    #@175
    move-result v14

    #@176
    if-lez v14, :cond_1ae

    #@178
    .line 2186
    const-string v14, "  Enabled Providers:"

    #@17a
    move-object/from16 v0, p2

    #@17c
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@17f
    .line 2187
    move-object/from16 v0, p0

    #@181
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@183
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@186
    move-result-object v7

    #@187
    :goto_187
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@18a
    move-result v14

    #@18b
    if-eqz v14, :cond_1ae

    #@18d
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@190
    move-result-object v5

    #@191
    check-cast v5, Ljava/lang/String;

    #@193
    .line 2188
    .local v5, i:Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    #@195
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@198
    const-string v16, "    "

    #@19a
    move-object/from16 v0, v16

    #@19c
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v14

    #@1a0
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v14

    #@1a4
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a7
    move-result-object v14

    #@1a8
    move-object/from16 v0, p2

    #@1aa
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1ad
    goto :goto_187

    #@1ae
    .line 2192
    .end local v5           #i:Ljava/lang/String;
    :cond_1ae
    move-object/from16 v0, p0

    #@1b0
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mDisabledProviders:Ljava/util/Set;

    #@1b2
    invoke-interface {v14}, Ljava/util/Set;->size()I

    #@1b5
    move-result v14

    #@1b6
    if-lez v14, :cond_1ee

    #@1b8
    .line 2193
    const-string v14, "  Disabled Providers:"

    #@1ba
    move-object/from16 v0, p2

    #@1bc
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1bf
    .line 2194
    move-object/from16 v0, p0

    #@1c1
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mDisabledProviders:Ljava/util/Set;

    #@1c3
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1c6
    move-result-object v7

    #@1c7
    :goto_1c7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@1ca
    move-result v14

    #@1cb
    if-eqz v14, :cond_1ee

    #@1cd
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d0
    move-result-object v5

    #@1d1
    check-cast v5, Ljava/lang/String;

    #@1d3
    .line 2195
    .restart local v5       #i:Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    #@1d5
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1d8
    const-string v16, "    "

    #@1da
    move-object/from16 v0, v16

    #@1dc
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v14

    #@1e0
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v14

    #@1e4
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e7
    move-result-object v14

    #@1e8
    move-object/from16 v0, p2

    #@1ea
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1ed
    goto :goto_1c7

    #@1ee
    .line 2198
    .end local v5           #i:Ljava/lang/String;
    :cond_1ee
    const-string v14, "  "

    #@1f0
    move-object/from16 v0, p2

    #@1f2
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@1f5
    .line 2199
    move-object/from16 v0, p0

    #@1f7
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@1f9
    move-object/from16 v0, p2

    #@1fb
    invoke-virtual {v14, v0}, Lcom/android/server/location/LocationBlacklist;->dump(Ljava/io/PrintWriter;)V

    #@1fe
    .line 2200
    move-object/from16 v0, p0

    #@200
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@202
    invoke-virtual {v14}, Ljava/util/HashMap;->size()I

    #@205
    move-result v14

    #@206
    if-lez v14, :cond_237

    #@208
    .line 2201
    const-string v14, "  Mock Providers:"

    #@20a
    move-object/from16 v0, p2

    #@20c
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20f
    .line 2202
    move-object/from16 v0, p0

    #@211
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@213
    invoke-virtual {v14}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@216
    move-result-object v14

    #@217
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21a
    move-result-object v7

    #@21b
    :goto_21b
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@21e
    move-result v14

    #@21f
    if-eqz v14, :cond_237

    #@221
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@224
    move-result-object v6

    #@225
    check-cast v6, Ljava/util/Map$Entry;

    #@227
    .line 2203
    .local v6, i:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/MockProvider;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@22a
    move-result-object v14

    #@22b
    check-cast v14, Lcom/android/server/location/MockProvider;

    #@22d
    const-string v16, "      "

    #@22f
    move-object/from16 v0, p2

    #@231
    move-object/from16 v1, v16

    #@233
    invoke-virtual {v14, v0, v1}, Lcom/android/server/location/MockProvider;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@236
    goto :goto_21b

    #@237
    .line 2207
    .end local v6           #i:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/MockProvider;>;"
    :cond_237
    const-string v14, "  fudger: "

    #@239
    move-object/from16 v0, p2

    #@23b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@23e
    .line 2208
    move-object/from16 v0, p0

    #@240
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mLocationFudger:Lcom/android/server/location/LocationFudger;

    #@242
    move-object/from16 v0, p1

    #@244
    move-object/from16 v1, p2

    #@246
    move-object/from16 v2, p3

    #@248
    invoke-virtual {v14, v0, v1, v2}, Lcom/android/server/location/LocationFudger;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@24b
    .line 2210
    move-object/from16 v0, p3

    #@24d
    array-length v14, v0

    #@24e
    if-lez v14, :cond_261

    #@250
    const-string v14, "short"

    #@252
    const/16 v16, 0x0

    #@254
    aget-object v16, p3, v16

    #@256
    move-object/from16 v0, v16

    #@258
    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25b
    move-result v14

    #@25c
    if-eqz v14, :cond_261

    #@25e
    .line 2211
    monitor-exit v15

    #@25f
    goto/16 :goto_36

    #@261
    .line 2213
    :cond_261
    move-object/from16 v0, p0

    #@263
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@265
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@268
    move-result-object v7

    #@269
    :goto_269
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@26c
    move-result v14

    #@26d
    if-eqz v14, :cond_2d6

    #@26f
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@272
    move-result-object v10

    #@273
    check-cast v10, Lcom/android/server/location/LocationProviderInterface;

    #@275
    .line 2214
    .local v10, provider:Lcom/android/server/location/LocationProviderInterface;
    new-instance v14, Ljava/lang/StringBuilder;

    #@277
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@27a
    invoke-interface {v10}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@27d
    move-result-object v16

    #@27e
    move-object/from16 v0, v16

    #@280
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@283
    move-result-object v14

    #@284
    const-string v16, " Internal State"

    #@286
    move-object/from16 v0, v16

    #@288
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28b
    move-result-object v14

    #@28c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28f
    move-result-object v14

    #@290
    move-object/from16 v0, p2

    #@292
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@295
    .line 2215
    instance-of v14, v10, Lcom/android/server/location/LocationProviderProxy;

    #@297
    if-eqz v14, :cond_2c5

    #@299
    .line 2216
    move-object v0, v10

    #@29a
    check-cast v0, Lcom/android/server/location/LocationProviderProxy;

    #@29c
    move-object v11, v0

    #@29d
    .line 2217
    .local v11, proxy:Lcom/android/server/location/LocationProviderProxy;
    new-instance v14, Ljava/lang/StringBuilder;

    #@29f
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2a2
    const-string v16, " ("

    #@2a4
    move-object/from16 v0, v16

    #@2a6
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v14

    #@2aa
    invoke-virtual {v11}, Lcom/android/server/location/LocationProviderProxy;->getConnectedPackageName()Ljava/lang/String;

    #@2ad
    move-result-object v16

    #@2ae
    move-object/from16 v0, v16

    #@2b0
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b3
    move-result-object v14

    #@2b4
    const-string v16, ")"

    #@2b6
    move-object/from16 v0, v16

    #@2b8
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v14

    #@2bc
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bf
    move-result-object v14

    #@2c0
    move-object/from16 v0, p2

    #@2c2
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c5
    .line 2219
    .end local v11           #proxy:Lcom/android/server/location/LocationProviderProxy;
    :cond_2c5
    const-string v14, ":"

    #@2c7
    move-object/from16 v0, p2

    #@2c9
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2cc
    .line 2220
    move-object/from16 v0, p1

    #@2ce
    move-object/from16 v1, p2

    #@2d0
    move-object/from16 v2, p3

    #@2d2
    invoke-interface {v10, v0, v1, v2}, Lcom/android/server/location/LocationProviderInterface;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@2d5
    goto :goto_269

    #@2d6
    .line 2222
    .end local v10           #provider:Lcom/android/server/location/LocationProviderInterface;
    :cond_2d6
    monitor-exit v15
    :try_end_2d7
    .catchall {:try_start_80 .. :try_end_2d7} :catchall_7d

    #@2d7
    goto/16 :goto_36
.end method

.method public geocoderIsPresent()Z
    .registers 2

    #@0
    .prologue
    .line 1950
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public getAllProviders()Ljava/util/List;
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 839
    iget-object v5, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 840
    :try_start_3
    new-instance v2, Ljava/util/ArrayList;

    #@5
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v4

    #@b
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    #@e
    .line 841
    .local v2, out:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v0

    #@14
    .local v0, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_33

    #@1a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Lcom/android/server/location/LocationProviderInterface;

    #@20
    .line 842
    .local v3, provider:Lcom/android/server/location/LocationProviderInterface;
    invoke-interface {v3}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    .line 843
    .local v1, name:Ljava/lang/String;
    const-string v4, "fused"

    #@26
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v4

    #@2a
    if-nez v4, :cond_14

    #@2c
    .line 846
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    goto :goto_14

    #@30
    .line 848
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #out:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3           #provider:Lcom/android/server/location/LocationProviderInterface;
    :catchall_30
    move-exception v4

    #@31
    monitor-exit v5
    :try_end_32
    .catchall {:try_start_3 .. :try_end_32} :catchall_30

    #@32
    throw v4

    #@33
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v2       #out:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_33
    :try_start_33
    monitor-exit v5
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_30

    #@34
    .line 850
    const-string v4, "LocationManagerService"

    #@36
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v6, "getAllProviders()="

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 852
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4f
    move-result-object v4

    #@50
    if-eqz v4, :cond_77

    #@52
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@55
    move-result-object v4

    #@56
    const/4 v5, 0x1

    #@57
    const/4 v6, 0x0

    #@58
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@5b
    move-result v7

    #@5c
    const/4 v8, 0x0

    #@5d
    invoke-interface {v4, v5, v6, v7, v8}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@60
    move-result v4

    #@61
    if-nez v4, :cond_77

    #@63
    const-string v4, "passive"

    #@65
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@68
    move-result v4

    #@69
    if-eqz v4, :cond_77

    #@6b
    .line 856
    const-string v4, "LocationManagerService"

    #@6d
    const-string v5, "MDM Block PassiveProvider"

    #@6f
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 857
    const-string v4, "passive"

    #@74
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@77
    .line 860
    :cond_77
    return-object v2
.end method

.method public getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;
    .registers 9
    .parameter "criteria"
    .parameter "enabledOnly"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 920
    const/4 v1, 0x0

    #@2
    .line 922
    .local v1, result:Ljava/lang/String;
    invoke-virtual {p0, p1, p2}, Lcom/android/server/LocationManagerService;->getProviders(Landroid/location/Criteria;Z)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    .line 923
    .local v0, providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_3e

    #@c
    .line 924
    invoke-direct {p0, v0}, Lcom/android/server/LocationManagerService;->pickBest(Ljava/util/List;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 925
    const-string v2, "LocationManagerService"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "getBestProvider("

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, ", "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, ")="

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    move-object v2, v1

    #@3d
    .line 936
    :goto_3d
    return-object v2

    #@3e
    .line 928
    :cond_3e
    invoke-virtual {p0, v2, p2}, Lcom/android/server/LocationManagerService;->getProviders(Landroid/location/Criteria;Z)Ljava/util/List;

    #@41
    move-result-object v0

    #@42
    .line 929
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@45
    move-result v3

    #@46
    if-nez v3, :cond_7a

    #@48
    .line 930
    invoke-direct {p0, v0}, Lcom/android/server/LocationManagerService;->pickBest(Ljava/util/List;)Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    .line 931
    const-string v2, "LocationManagerService"

    #@4e
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v4, "getBestProvider("

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const-string v4, ", "

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    const-string v4, ")="

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    move-object v2, v1

    #@79
    .line 932
    goto :goto_3d

    #@7a
    .line 935
    :cond_7a
    const-string v3, "LocationManagerService"

    #@7c
    new-instance v4, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v5, "getBestProvider("

    #@83
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v4

    #@87
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v4

    #@8b
    const-string v5, ", "

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    const-string v5, ")="

    #@97
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v4

    #@a3
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    goto :goto_3d
.end method

.method public getFromLocation(DDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;
    .registers 16
    .parameter "latitude"
    .parameter "longitude"
    .parameter "maxResults"
    .parameter "params"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI",
            "Landroid/location/GeocoderParams;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 1956
    .local p7, addrs:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 1957
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@6
    move-wide v1, p1

    #@7
    move-wide v3, p3

    #@8
    move v5, p5

    #@9
    move-object v6, p6

    #@a
    move-object v7, p7

    #@b
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/location/GeocoderProxy;->getFromLocation(DDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 1960
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public getFromLocationName(Ljava/lang/String;DDDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;
    .registers 26
    .parameter "locationName"
    .parameter "lowerLeftLatitude"
    .parameter "lowerLeftLongitude"
    .parameter "upperRightLatitude"
    .parameter "upperRightLongitude"
    .parameter "maxResults"
    .parameter "params"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DDDDI",
            "Landroid/location/GeocoderParams;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 1970
    .local p12, addrs:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@2
    if-eqz v0, :cond_19

    #@4
    .line 1971
    iget-object v0, p0, Lcom/android/server/LocationManagerService;->mGeocodeProvider:Lcom/android/server/location/GeocoderProxy;

    #@6
    move-object v1, p1

    #@7
    move-wide v2, p2

    #@8
    move-wide/from16 v4, p4

    #@a
    move-wide/from16 v6, p6

    #@c
    move-wide/from16 v8, p8

    #@e
    move/from16 v10, p10

    #@10
    move-object/from16 v11, p11

    #@12
    move-object/from16 v12, p12

    #@14
    invoke-virtual/range {v0 .. v12}, Lcom/android/server/location/GeocoderProxy;->getFromLocationName(Ljava/lang/String;DDDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 1975
    :goto_18
    return-object v0

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_18
.end method

.method public getLastLocation(Landroid/location/LocationRequest;Ljava/lang/String;)Landroid/location/Location;
    .registers 15
    .parameter "request"
    .parameter "packageName"

    #@0
    .prologue
    .line 1348
    const-string v7, "LocationManagerService"

    #@2
    new-instance v8, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v9, "getLastLocation: "

    #@9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v8

    #@d
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v8

    #@15
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1349
    if-nez p1, :cond_1c

    #@1a
    sget-object p1, Lcom/android/server/LocationManagerService;->DEFAULT_LOCATION_REQUEST:Landroid/location/LocationRequest;

    #@1c
    .line 1350
    :cond_1c
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@1f
    move-result v0

    #@20
    .line 1351
    .local v0, allowedResolutionLevel:I
    invoke-direct {p0, p2}, Lcom/android/server/LocationManagerService;->checkPackageName(Ljava/lang/String;)V

    #@23
    .line 1352
    invoke-virtual {p1}, Landroid/location/LocationRequest;->getProvider()Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    invoke-direct {p0, v0, v7}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V

    #@2a
    .line 1356
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@2d
    move-result-wide v1

    #@2e
    .line 1358
    .local v1, identity:J
    :try_start_2e
    iget-object v7, p0, Lcom/android/server/LocationManagerService;->mBlacklist:Lcom/android/server/location/LocationBlacklist;

    #@30
    invoke-virtual {v7, p2}, Lcom/android/server/location/LocationBlacklist;->isBlacklisted(Ljava/lang/String;)Z

    #@33
    move-result v7

    #@34
    if-eqz v7, :cond_53

    #@36
    .line 1359
    const-string v7, "LocationManagerService"

    #@38
    new-instance v8, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v9, "not returning last loc for blacklisted app: "

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v8

    #@4b
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4e
    .catchall {:try_start_2e .. :try_end_4e} :catchall_c4

    #@4e
    .line 1361
    const/4 v7, 0x0

    #@4f
    .line 1408
    :goto_4f
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@52
    .line 1406
    return-object v7

    #@53
    .line 1364
    :cond_53
    :try_start_53
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@56
    move-result-object v7

    #@57
    if-eqz v7, :cond_a9

    #@59
    .line 1365
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@5c
    move-result-object v7

    #@5d
    const/4 v8, 0x2

    #@5e
    const/4 v9, 0x0

    #@5f
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@62
    move-result v10

    #@63
    invoke-interface {v7, v8, v9, v10, p2}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@66
    move-result v7

    #@67
    if-nez v7, :cond_83

    #@69
    .line 1368
    const-string v7, "LocationManagerService"

    #@6b
    new-instance v8, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v9, "MDM Control LocationManager Service packageName"

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v8

    #@7e
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 1369
    const/4 v7, 0x0

    #@82
    goto :goto_4f

    #@83
    .line 1373
    :cond_83
    const-string v7, "passive"

    #@85
    invoke-virtual {p1}, Landroid/location/LocationRequest;->getProvider()Ljava/lang/String;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v7

    #@8d
    if-eqz v7, :cond_a9

    #@8f
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@92
    move-result-object v7

    #@93
    const/4 v8, 0x1

    #@94
    const/4 v9, 0x0

    #@95
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@98
    move-result v10

    #@99
    const/4 v11, 0x0

    #@9a
    invoke-interface {v7, v8, v9, v10, v11}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@9d
    move-result v7

    #@9e
    if-nez v7, :cond_a9

    #@a0
    .line 1377
    const-string v7, "LocationManagerService"

    #@a2
    const-string v8, "MDM Block PassiveProvider"

    #@a4
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 1378
    const/4 v7, 0x0

    #@a8
    goto :goto_4f

    #@a9
    .line 1383
    :cond_a9
    iget-object v8, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@ab
    monitor-enter v8
    :try_end_ac
    .catchall {:try_start_53 .. :try_end_ac} :catchall_c4

    #@ac
    .line 1386
    :try_start_ac
    invoke-virtual {p1}, Landroid/location/LocationRequest;->getProvider()Ljava/lang/String;

    #@af
    move-result-object v4

    #@b0
    .line 1387
    .local v4, name:Ljava/lang/String;
    if-nez v4, :cond_b4

    #@b2
    const-string v4, "fused"

    #@b4
    .line 1388
    :cond_b4
    iget-object v7, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@b6
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b9
    move-result-object v6

    #@ba
    check-cast v6, Lcom/android/server/location/LocationProviderInterface;

    #@bc
    .line 1389
    .local v6, provider:Lcom/android/server/location/LocationProviderInterface;
    if-nez v6, :cond_c9

    #@be
    const/4 v7, 0x0

    #@bf
    monitor-exit v8

    #@c0
    goto :goto_4f

    #@c1
    .line 1405
    .end local v4           #name:Ljava/lang/String;
    .end local v6           #provider:Lcom/android/server/location/LocationProviderInterface;
    :catchall_c1
    move-exception v7

    #@c2
    monitor-exit v8
    :try_end_c3
    .catchall {:try_start_ac .. :try_end_c3} :catchall_c1

    #@c3
    :try_start_c3
    throw v7
    :try_end_c4
    .catchall {:try_start_c3 .. :try_end_c4} :catchall_c4

    #@c4
    .line 1408
    :catchall_c4
    move-exception v7

    #@c5
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@c8
    throw v7

    #@c9
    .line 1391
    .restart local v4       #name:Ljava/lang/String;
    .restart local v6       #provider:Lcom/android/server/location/LocationProviderInterface;
    :cond_c9
    :try_start_c9
    iget v7, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@cb
    invoke-direct {p0, v4, v7}, Lcom/android/server/LocationManagerService;->isAllowedBySettingsLocked(Ljava/lang/String;I)Z

    #@ce
    move-result v7

    #@cf
    if-nez v7, :cond_d5

    #@d1
    const/4 v7, 0x0

    #@d2
    monitor-exit v8

    #@d3
    goto/16 :goto_4f

    #@d5
    .line 1393
    :cond_d5
    iget-object v7, p0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@d7
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@da
    move-result-object v3

    #@db
    check-cast v3, Landroid/location/Location;

    #@dd
    .line 1394
    .local v3, location:Landroid/location/Location;
    if-nez v3, :cond_e3

    #@df
    .line 1395
    const/4 v7, 0x0

    #@e0
    monitor-exit v8

    #@e1
    goto/16 :goto_4f

    #@e3
    .line 1397
    :cond_e3
    const/4 v7, 0x2

    #@e4
    if-ge v0, v7, :cond_fc

    #@e6
    .line 1398
    const-string v7, "noGPSLocation"

    #@e8
    invoke-virtual {v3, v7}, Landroid/location/Location;->getExtraLocation(Ljava/lang/String;)Landroid/location/Location;

    #@eb
    move-result-object v5

    #@ec
    .line 1399
    .local v5, noGPSLocation:Landroid/location/Location;
    if-eqz v5, :cond_104

    #@ee
    .line 1400
    new-instance v7, Landroid/location/Location;

    #@f0
    iget-object v9, p0, Lcom/android/server/LocationManagerService;->mLocationFudger:Lcom/android/server/location/LocationFudger;

    #@f2
    invoke-virtual {v9, v5}, Lcom/android/server/location/LocationFudger;->getOrCreate(Landroid/location/Location;)Landroid/location/Location;

    #@f5
    move-result-object v9

    #@f6
    invoke-direct {v7, v9}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@f9
    monitor-exit v8

    #@fa
    goto/16 :goto_4f

    #@fc
    .line 1403
    .end local v5           #noGPSLocation:Landroid/location/Location;
    :cond_fc
    new-instance v7, Landroid/location/Location;

    #@fe
    invoke-direct {v7, v3}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@101
    monitor-exit v8

    #@102
    goto/16 :goto_4f

    #@104
    .line 1405
    .restart local v5       #noGPSLocation:Landroid/location/Location;
    :cond_104
    monitor-exit v8
    :try_end_105
    .catchall {:try_start_c9 .. :try_end_105} :catchall_c1

    #@105
    .line 1406
    const/4 v7, 0x0

    #@106
    goto/16 :goto_4f
.end method

.method public getProviderProperties(Ljava/lang/String;)Lcom/android/internal/location/ProviderProperties;
    .registers 6
    .parameter "provider"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1546
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@3
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    if-nez v2, :cond_a

    #@9
    .line 1559
    :cond_9
    :goto_9
    return-object v1

    #@a
    .line 1550
    :cond_a
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@d
    move-result v2

    #@e
    invoke-direct {p0, v2, p1}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V

    #@11
    .line 1554
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@13
    monitor-enter v2

    #@14
    .line 1555
    :try_start_14
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@16
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/android/server/location/LocationProviderInterface;

    #@1c
    .line 1556
    .local v0, p:Lcom/android/server/location/LocationProviderInterface;
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_14 .. :try_end_1d} :catchall_24

    #@1d
    .line 1558
    if-eqz v0, :cond_9

    #@1f
    .line 1559
    invoke-interface {v0}, Lcom/android/server/location/LocationProviderInterface;->getProperties()Lcom/android/internal/location/ProviderProperties;

    #@22
    move-result-object v1

    #@23
    goto :goto_9

    #@24
    .line 1556
    .end local v0           #p:Lcom/android/server/location/LocationProviderInterface;
    :catchall_24
    move-exception v1

    #@25
    :try_start_25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public getProviders(Landroid/location/Criteria;Z)Ljava/util/List;
    .registers 16
    .parameter "criteria"
    .parameter "enabledOnly"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Criteria;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 870
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@3
    move-result v0

    #@4
    .line 872
    .local v0, allowedResolutionLevel:I
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@7
    move-result v1

    #@8
    .line 873
    .local v1, callingUserId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v3

    #@c
    .line 875
    .local v3, identity:J
    :try_start_c
    iget-object v9, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@e
    monitor-enter v9
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_59

    #@f
    .line 876
    :try_start_f
    new-instance v6, Ljava/util/ArrayList;

    #@11
    iget-object v8, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v8

    #@17
    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    #@1a
    .line 877
    .local v6, out:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/android/server/LocationManagerService;->mProviders:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v2

    #@20
    .local v2, i$:Ljava/util/Iterator;
    :cond_20
    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@23
    move-result v8

    #@24
    if-eqz v8, :cond_5e

    #@26
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@29
    move-result-object v7

    #@2a
    check-cast v7, Lcom/android/server/location/LocationProviderInterface;

    #@2c
    .line 878
    .local v7, provider:Lcom/android/server/location/LocationProviderInterface;
    invoke-interface {v7}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    .line 879
    .local v5, name:Ljava/lang/String;
    const-string v8, "fused"

    #@32
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v8

    #@36
    if-nez v8, :cond_20

    #@38
    .line 882
    invoke-direct {p0, v5}, Lcom/android/server/LocationManagerService;->getMinimumResolutionLevelForProviderUse(Ljava/lang/String;)I

    #@3b
    move-result v8

    #@3c
    if-lt v0, v8, :cond_20

    #@3e
    .line 883
    if-eqz p2, :cond_46

    #@40
    invoke-direct {p0, v5, v1}, Lcom/android/server/LocationManagerService;->isAllowedBySettingsLocked(Ljava/lang/String;I)Z

    #@43
    move-result v8

    #@44
    if-eqz v8, :cond_20

    #@46
    .line 886
    :cond_46
    if-eqz p1, :cond_52

    #@48
    invoke-interface {v7}, Lcom/android/server/location/LocationProviderInterface;->getProperties()Lcom/android/internal/location/ProviderProperties;

    #@4b
    move-result-object v8

    #@4c
    invoke-static {v5, v8, p1}, Landroid/location/LocationProvider;->propertiesMeetCriteria(Ljava/lang/String;Lcom/android/internal/location/ProviderProperties;Landroid/location/Criteria;)Z

    #@4f
    move-result v8

    #@50
    if-eqz v8, :cond_20

    #@52
    .line 890
    :cond_52
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@55
    goto :goto_20

    #@56
    .line 893
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v5           #name:Ljava/lang/String;
    .end local v6           #out:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7           #provider:Lcom/android/server/location/LocationProviderInterface;
    :catchall_56
    move-exception v8

    #@57
    monitor-exit v9
    :try_end_58
    .catchall {:try_start_f .. :try_end_58} :catchall_56

    #@58
    :try_start_58
    throw v8
    :try_end_59
    .catchall {:try_start_58 .. :try_end_59} :catchall_59

    #@59
    .line 895
    :catchall_59
    move-exception v8

    #@5a
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5d
    throw v8

    #@5e
    .line 893
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v6       #out:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5e
    :try_start_5e
    monitor-exit v9
    :try_end_5f
    .catchall {:try_start_5e .. :try_end_5f} :catchall_56

    #@5f
    .line 895
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@62
    .line 898
    const-string v8, "LocationManagerService"

    #@64
    new-instance v9, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v10, "getProviders()="

    #@6b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v9

    #@6f
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v9

    #@73
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v9

    #@77
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 900
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7d
    move-result-object v8

    #@7e
    if-eqz v8, :cond_a5

    #@80
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@83
    move-result-object v8

    #@84
    const/4 v9, 0x1

    #@85
    const/4 v10, 0x0

    #@86
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@89
    move-result v11

    #@8a
    const/4 v12, 0x0

    #@8b
    invoke-interface {v8, v9, v10, v11, v12}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@8e
    move-result v8

    #@8f
    if-nez v8, :cond_a5

    #@91
    const-string v8, "passive"

    #@93
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@96
    move-result v8

    #@97
    if-eqz v8, :cond_a5

    #@99
    .line 904
    const-string v8, "LocationManagerService"

    #@9b
    const-string v9, "MDM Block PassiveProvider"

    #@9d
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    .line 905
    const-string v8, "passive"

    #@a2
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@a5
    .line 908
    :cond_a5
    return-object v6
.end method

.method public isProviderEnabled(Ljava/lang/String;)Z
    .registers 8
    .parameter "provider"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1564
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@4
    move-result v4

    #@5
    invoke-direct {p0, v4, p1}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V

    #@8
    .line 1566
    const-string v4, "fused"

    #@a
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_11

    #@10
    .line 1574
    :goto_10
    return v3

    #@11
    .line 1568
    :cond_11
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@14
    move-result-wide v0

    #@15
    .line 1570
    .local v0, identity:J
    :try_start_15
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@17
    monitor-enter v4
    :try_end_18
    .catchall {:try_start_15 .. :try_end_18} :catchall_32

    #@18
    .line 1571
    :try_start_18
    iget-object v5, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@1a
    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/location/LocationProviderInterface;

    #@20
    .line 1572
    .local v2, p:Lcom/android/server/location/LocationProviderInterface;
    if-nez v2, :cond_27

    #@22
    monitor-exit v4
    :try_end_23
    .catchall {:try_start_18 .. :try_end_23} :catchall_2f

    #@23
    .line 1577
    :goto_23
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@26
    goto :goto_10

    #@27
    .line 1574
    :cond_27
    :try_start_27
    iget v3, p0, Lcom/android/server/LocationManagerService;->mCurrentUserId:I

    #@29
    invoke-direct {p0, p1, v3}, Lcom/android/server/LocationManagerService;->isAllowedBySettingsLocked(Ljava/lang/String;I)Z

    #@2c
    move-result v3

    #@2d
    monitor-exit v4

    #@2e
    goto :goto_23

    #@2f
    .line 1575
    .end local v2           #p:Lcom/android/server/location/LocationProviderInterface;
    :catchall_2f
    move-exception v3

    #@30
    monitor-exit v4
    :try_end_31
    .catchall {:try_start_27 .. :try_end_31} :catchall_2f

    #@31
    :try_start_31
    throw v3
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_32

    #@32
    .line 1577
    :catchall_32
    move-exception v3

    #@33
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@36
    throw v3
.end method

.method public locationCallbackFinished(Landroid/location/ILocationListener;)V
    .registers 7
    .parameter "listener"

    #@0
    .prologue
    .line 676
    invoke-interface {p1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    .line 677
    .local v0, binder:Landroid/os/IBinder;
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mReceivers:Ljava/util/HashMap;

    #@6
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v3

    #@a
    check-cast v3, Lcom/android/server/LocationManagerService$Receiver;

    #@c
    .line 678
    .local v3, receiver:Lcom/android/server/LocationManagerService$Receiver;
    if-eqz v3, :cond_1a

    #@e
    .line 679
    monitor-enter v3

    #@f
    .line 681
    :try_start_f
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@12
    move-result-wide v1

    #@13
    .line 682
    .local v1, identity:J
    invoke-static {v3}, Lcom/android/server/LocationManagerService$Receiver;->access$1100(Lcom/android/server/LocationManagerService$Receiver;)V

    #@16
    .line 683
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    .line 684
    monitor-exit v3

    #@1a
    .line 686
    .end local v1           #identity:J
    :cond_1a
    return-void

    #@1b
    .line 684
    :catchall_1b
    move-exception v4

    #@1c
    monitor-exit v3
    :try_end_1d
    .catchall {:try_start_f .. :try_end_1d} :catchall_1b

    #@1d
    throw v4
.end method

.method public providerMeetsCriteria(Ljava/lang/String;Landroid/location/Criteria;)Z
    .registers 8
    .parameter "provider"
    .parameter "criteria"

    #@0
    .prologue
    .line 951
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/location/LocationProviderInterface;

    #@8
    .line 952
    .local v0, p:Lcom/android/server/location/LocationProviderInterface;
    if-nez v0, :cond_23

    #@a
    .line 953
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "provider="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v2

    #@23
    .line 956
    :cond_23
    invoke-interface {v0}, Lcom/android/server/location/LocationProviderInterface;->getName()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-interface {v0}, Lcom/android/server/location/LocationProviderInterface;->getProperties()Lcom/android/internal/location/ProviderProperties;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v2, v3, p2}, Landroid/location/LocationProvider;->propertiesMeetCriteria(Ljava/lang/String;Lcom/android/internal/location/ProviderProperties;Landroid/location/Criteria;)Z

    #@2e
    move-result v1

    #@2f
    .line 958
    .local v1, result:Z
    const-string v2, "LocationManagerService"

    #@31
    new-instance v3, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v4, "providerMeetsCriteria("

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    const-string v4, ", "

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    const-string v4, ")="

    #@4c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 959
    return v1
.end method

.method public removeGeofence(Landroid/location/Geofence;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 9
    .parameter "geofence"
    .parameter "intent"
    .parameter "packageName"

    #@0
    .prologue
    .line 1454
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@3
    move-result v2

    #@4
    invoke-direct {p0, v2}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForGeofenceUse(I)V

    #@7
    .line 1455
    invoke-direct {p0, p2}, Lcom/android/server/LocationManagerService;->checkPendingIntent(Landroid/app/PendingIntent;)V

    #@a
    .line 1456
    invoke-direct {p0, p3}, Lcom/android/server/LocationManagerService;->checkPackageName(Ljava/lang/String;)V

    #@d
    .line 1458
    const-string v2, "LocationManagerService"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "removeGeofence: "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, " "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1461
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@32
    move-result-wide v0

    #@33
    .line 1463
    .local v0, identity:J
    :try_start_33
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@35
    if-eqz v2, :cond_44

    #@37
    iget-boolean v2, p0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@39
    if-eqz v2, :cond_44

    #@3b
    .line 1464
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@3d
    invoke-virtual {v2, p2}, Lcom/android/server/location/GeoFencerBase;->remove(Landroid/app/PendingIntent;)V
    :try_end_40
    .catchall {:try_start_33 .. :try_end_40} :catchall_4a

    #@40
    .line 1469
    :goto_40
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@43
    .line 1471
    return-void

    #@44
    .line 1466
    :cond_44
    :try_start_44
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mGeofenceManager:Lcom/android/server/location/GeofenceManager;

    #@46
    invoke-virtual {v2, p1, p2}, Lcom/android/server/location/GeofenceManager;->removeFence(Landroid/location/Geofence;Landroid/app/PendingIntent;)V
    :try_end_49
    .catchall {:try_start_44 .. :try_end_49} :catchall_4a

    #@49
    goto :goto_40

    #@4a
    .line 1469
    :catchall_4a
    move-exception v2

    #@4b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4e
    throw v2
.end method

.method public removeGpsStatusListener(Landroid/location/IGpsStatusListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 1493
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1495
    :try_start_3
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mGpsStatusProvider:Landroid/location/IGpsStatusProvider;

    #@5
    invoke-interface {v1, p1}, Landroid/location/IGpsStatusProvider;->removeGpsStatusListener(Landroid/location/IGpsStatusListener;)V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_13
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_8} :catch_a

    #@8
    .line 1499
    :goto_8
    :try_start_8
    monitor-exit v2

    #@9
    .line 1500
    return-void

    #@a
    .line 1496
    :catch_a
    move-exception v0

    #@b
    .line 1497
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "LocationManagerService"

    #@d
    const-string v3, "mGpsStatusProvider.removeGpsStatusListener failed"

    #@f
    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_8

    #@13
    .line 1499
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_8 .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method public removeTestProvider(Ljava/lang/String;)V
    .registers 10
    .parameter "provider"

    #@0
    .prologue
    .line 2029
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 2030
    iget-object v5, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v5

    #@6
    .line 2031
    :try_start_6
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@8
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Lcom/android/server/location/MockProvider;

    #@e
    .line 2032
    .local v2, mockProvider:Lcom/android/server/location/MockProvider;
    if-nez v2, :cond_32

    #@10
    .line 2033
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v6, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v7, "Provider \""

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    const-string v7, "\" unknown"

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v4

    #@2f
    .line 2051
    .end local v2           #mockProvider:Lcom/android/server/location/MockProvider;
    :catchall_2f
    move-exception v4

    #@30
    monitor-exit v5
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_2f

    #@31
    throw v4

    #@32
    .line 2035
    .restart local v2       #mockProvider:Lcom/android/server/location/MockProvider;
    :cond_32
    :try_start_32
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@35
    move-result-wide v0

    #@36
    .line 2036
    .local v0, identity:J
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@38
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    move-result-object v4

    #@3c
    check-cast v4, Lcom/android/server/location/LocationProviderInterface;

    #@3e
    invoke-direct {p0, v4}, Lcom/android/server/LocationManagerService;->removeProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@41
    .line 2037
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@43
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    .line 2039
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@48
    if-eqz v4, :cond_4d

    #@4a
    .line 2040
    const/4 v4, 0x1

    #@4b
    iput-boolean v4, p0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@4d
    .line 2044
    :cond_4d
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mRealProviders:Ljava/util/HashMap;

    #@4f
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@52
    move-result-object v3

    #@53
    check-cast v3, Lcom/android/server/location/LocationProviderInterface;

    #@55
    .line 2045
    .local v3, realProvider:Lcom/android/server/location/LocationProviderInterface;
    if-eqz v3, :cond_5a

    #@57
    .line 2046
    invoke-direct {p0, v3}, Lcom/android/server/LocationManagerService;->addProviderLocked(Lcom/android/server/location/LocationProviderInterface;)V

    #@5a
    .line 2048
    :cond_5a
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLastLocation:Ljava/util/HashMap;

    #@5c
    const/4 v6, 0x0

    #@5d
    invoke-virtual {v4, p1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@60
    .line 2049
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->updateProvidersLocked()V

    #@63
    .line 2050
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@66
    .line 2051
    monitor-exit v5
    :try_end_67
    .catchall {:try_start_32 .. :try_end_67} :catchall_2f

    #@67
    .line 2052
    return-void
.end method

.method public removeUpdates(Landroid/location/ILocationListener;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 13
    .parameter "listener"
    .parameter "intent"
    .parameter "packageName"

    #@0
    .prologue
    .line 1293
    invoke-direct {p0, p3}, Lcom/android/server/LocationManagerService;->checkPackageName(Ljava/lang/String;)V

    #@3
    .line 1295
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@6
    move-result v3

    #@7
    .line 1296
    .local v3, pid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@a
    move-result v4

    #@b
    .local v4, uid:I
    move-object v0, p0

    #@c
    move-object v1, p1

    #@d
    move-object v2, p2

    #@e
    move-object v5, p3

    #@f
    .line 1297
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LocationManagerService;->checkListenerOrIntent(Landroid/location/ILocationListener;Landroid/app/PendingIntent;IILjava/lang/String;)Lcom/android/server/LocationManagerService$Receiver;

    #@12
    move-result-object v8

    #@13
    .line 1300
    .local v8, receiver:Lcom/android/server/LocationManagerService$Receiver;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@16
    move-result-wide v6

    #@17
    .line 1302
    .local v6, identity:J
    :try_start_17
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@19
    monitor-enter v1
    :try_end_1a
    .catchall {:try_start_17 .. :try_end_1a} :catchall_25

    #@1a
    .line 1303
    :try_start_1a
    invoke-direct {p0, v8}, Lcom/android/server/LocationManagerService;->removeUpdatesLocked(Lcom/android/server/LocationManagerService$Receiver;)V

    #@1d
    .line 1304
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_1a .. :try_end_1e} :catchall_22

    #@1e
    .line 1306
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@21
    .line 1308
    return-void

    #@22
    .line 1304
    :catchall_22
    move-exception v0

    #@23
    :try_start_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    #@24
    :try_start_24
    throw v0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_25

    #@25
    .line 1306
    :catchall_25
    move-exception v0

    #@26
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@29
    throw v0
.end method

.method public reportLocation(Landroid/location/Location;Z)V
    .registers 9
    .parameter "location"
    .parameter "passive"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1623
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkCallerIsProvider()V

    #@5
    .line 1625
    invoke-virtual {p1}, Landroid/location/Location;->isComplete()Z

    #@8
    move-result v3

    #@9
    if-nez v3, :cond_24

    #@b
    .line 1626
    const-string v1, "LocationManagerService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Dropping incomplete location: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1643
    :goto_23
    return-void

    #@24
    .line 1630
    :cond_24
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@26
    invoke-virtual {v3, v1, p1}, Lcom/android/server/LocationManagerService$LocationWorkerHandler;->removeMessages(ILjava/lang/Object;)V

    #@29
    .line 1631
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@2b
    invoke-static {v3, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@2e
    move-result-object v0

    #@2f
    .line 1633
    .local v0, m:Landroid/os/Message;
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@32
    move-result-object v3

    #@33
    if-eqz v3, :cond_4c

    #@35
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@38
    move-result-object v3

    #@39
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3c
    move-result v4

    #@3d
    const/4 v5, 0x0

    #@3e
    invoke-interface {v3, v1, v2, v4, v5}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@41
    move-result v3

    #@42
    if-nez v3, :cond_4c

    #@44
    .line 1637
    const-string v3, "LocationManagerService"

    #@46
    const-string v4, "MDM Block PassiveProvider"

    #@48
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1638
    const/4 p2, 0x0

    #@4c
    .line 1641
    :cond_4c
    if-eqz p2, :cond_56

    #@4e
    :goto_4e
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@50
    .line 1642
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@52
    invoke-virtual {v1, v0}, Lcom/android/server/LocationManagerService$LocationWorkerHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    #@55
    goto :goto_23

    #@56
    :cond_56
    move v1, v2

    #@57
    .line 1641
    goto :goto_4e
.end method

.method public requestGeofence(Landroid/location/LocationRequest;Landroid/location/Geofence;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 26
    .parameter "request"
    .parameter "geofence"
    .parameter "intent"
    .parameter "packageName"

    #@0
    .prologue
    .line 1415
    if-nez p1, :cond_4

    #@2
    sget-object p1, Lcom/android/server/LocationManagerService;->DEFAULT_LOCATION_REQUEST:Landroid/location/LocationRequest;

    #@4
    .line 1416
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@7
    move-result v18

    #@8
    .line 1417
    .local v18, allowedResolutionLevel:I
    move-object/from16 v0, p0

    #@a
    move/from16 v1, v18

    #@c
    invoke-direct {v0, v1}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForGeofenceUse(I)V

    #@f
    .line 1418
    move-object/from16 v0, p0

    #@11
    move-object/from16 v1, p3

    #@13
    invoke-direct {v0, v1}, Lcom/android/server/LocationManagerService;->checkPendingIntent(Landroid/app/PendingIntent;)V

    #@16
    .line 1419
    move-object/from16 v0, p0

    #@18
    move-object/from16 v1, p4

    #@1a
    invoke-direct {v0, v1}, Lcom/android/server/LocationManagerService;->checkPackageName(Ljava/lang/String;)V

    #@1d
    .line 1420
    invoke-virtual/range {p1 .. p1}, Landroid/location/LocationRequest;->getProvider()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    move-object/from16 v0, p0

    #@23
    move/from16 v1, v18

    #@25
    invoke-direct {v0, v1, v3}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V

    #@28
    .line 1422
    move-object/from16 v0, p0

    #@2a
    move-object/from16 v1, p1

    #@2c
    move/from16 v2, v18

    #@2e
    invoke-direct {v0, v1, v2}, Lcom/android/server/LocationManagerService;->createSanitizedRequest(Landroid/location/LocationRequest;I)Landroid/location/LocationRequest;

    #@31
    move-result-object v13

    #@32
    .line 1424
    .local v13, sanitizedRequest:Landroid/location/LocationRequest;
    const-string v3, "LocationManagerService"

    #@34
    new-instance v5, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v6, "requestGeofence: "

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    const-string v6, " "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    move-object/from16 v0, p2

    #@4b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    const-string v6, " "

    #@51
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    move-object/from16 v0, p3

    #@57
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 1427
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@65
    move-result v4

    #@66
    .line 1428
    .local v4, uid:I
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    #@69
    move-result v3

    #@6a
    if-eqz v3, :cond_74

    #@6c
    .line 1430
    const-string v3, "LocationManagerService"

    #@6e
    const-string v5, "proximity alerts are currently available only to the primary user"

    #@70
    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 1450
    .end local v13           #sanitizedRequest:Landroid/location/LocationRequest;
    :goto_73
    return-void

    #@74
    .line 1433
    .restart local v13       #sanitizedRequest:Landroid/location/LocationRequest;
    :cond_74
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@77
    move-result-wide v19

    #@78
    .line 1435
    .local v19, identity:J
    :try_start_78
    move-object/from16 v0, p0

    #@7a
    iget-object v3, v0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@7c
    if-eqz v3, :cond_be

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-boolean v3, v0, Lcom/android/server/LocationManagerService;->mGeoFencerEnabled:Z

    #@82
    if-eqz v3, :cond_be

    #@84
    .line 1437
    invoke-virtual {v13}, Landroid/location/LocationRequest;->getExpireAt()J

    #@87
    move-result-wide v5

    #@88
    const-wide v7, 0x7fffffffffffffffL

    #@8d
    cmp-long v3, v5, v7

    #@8f
    if-nez v3, :cond_b3

    #@91
    .line 1438
    const-wide/16 v10, -0x1

    #@93
    .line 1442
    .local v10, expiration:J
    :goto_93
    move-object/from16 v0, p0

    #@95
    iget-object v14, v0, Lcom/android/server/LocationManagerService;->mGeoFencer:Lcom/android/server/location/GeoFencerBase;

    #@97
    new-instance v3, Landroid/location/GeoFenceParams;

    #@99
    invoke-virtual/range {p2 .. p2}, Landroid/location/Geofence;->getLatitude()D

    #@9c
    move-result-wide v5

    #@9d
    invoke-virtual/range {p2 .. p2}, Landroid/location/Geofence;->getLongitude()D

    #@a0
    move-result-wide v7

    #@a1
    invoke-virtual/range {p2 .. p2}, Landroid/location/Geofence;->getRadius()F

    #@a4
    move-result v9

    #@a5
    move-object/from16 v12, p3

    #@a7
    move-object/from16 v13, p4

    #@a9
    invoke-direct/range {v3 .. v13}, Landroid/location/GeoFenceParams;-><init>(IDDFJLandroid/app/PendingIntent;Ljava/lang/String;)V

    #@ac
    .end local v13           #sanitizedRequest:Landroid/location/LocationRequest;
    invoke-virtual {v14, v3}, Lcom/android/server/location/GeoFencerBase;->add(Landroid/location/GeoFenceParams;)V
    :try_end_af
    .catchall {:try_start_78 .. :try_end_af} :catchall_ce

    #@af
    .line 1448
    .end local v10           #expiration:J
    :goto_af
    invoke-static/range {v19 .. v20}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@b2
    goto :goto_73

    #@b3
    .line 1440
    .restart local v13       #sanitizedRequest:Landroid/location/LocationRequest;
    :cond_b3
    :try_start_b3
    invoke-virtual {v13}, Landroid/location/LocationRequest;->getExpireAt()J

    #@b6
    move-result-wide v5

    #@b7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@ba
    move-result-wide v7

    #@bb
    sub-long v10, v5, v7

    #@bd
    .restart local v10       #expiration:J
    goto :goto_93

    #@be
    .line 1445
    .end local v10           #expiration:J
    :cond_be
    move-object/from16 v0, p0

    #@c0
    iget-object v12, v0, Lcom/android/server/LocationManagerService;->mGeofenceManager:Lcom/android/server/location/GeofenceManager;

    #@c2
    move-object/from16 v14, p2

    #@c4
    move-object/from16 v15, p3

    #@c6
    move/from16 v16, v4

    #@c8
    move-object/from16 v17, p4

    #@ca
    invoke-virtual/range {v12 .. v17}, Lcom/android/server/location/GeofenceManager;->addFence(Landroid/location/LocationRequest;Landroid/location/Geofence;Landroid/app/PendingIntent;ILjava/lang/String;)V
    :try_end_cd
    .catchall {:try_start_b3 .. :try_end_cd} :catchall_ce

    #@cd
    goto :goto_af

    #@ce
    .line 1448
    .end local v13           #sanitizedRequest:Landroid/location/LocationRequest;
    :catchall_ce
    move-exception v3

    #@cf
    invoke-static/range {v19 .. v20}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@d2
    throw v3
.end method

.method public requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/ILocationListener;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 16
    .parameter "request"
    .parameter "listener"
    .parameter "intent"
    .parameter "packageName"

    #@0
    .prologue
    .line 1236
    if-nez p1, :cond_4

    #@2
    sget-object p1, Lcom/android/server/LocationManagerService;->DEFAULT_LOCATION_REQUEST:Landroid/location/LocationRequest;

    #@4
    .line 1237
    :cond_4
    invoke-direct {p0, p4}, Lcom/android/server/LocationManagerService;->checkPackageName(Ljava/lang/String;)V

    #@7
    .line 1238
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@a
    move-result v6

    #@b
    .line 1239
    .local v6, allowedResolutionLevel:I
    invoke-virtual {p1}, Landroid/location/LocationRequest;->getProvider()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    invoke-direct {p0, v6, v0}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V

    #@12
    .line 1241
    invoke-direct {p0, p1, v6}, Lcom/android/server/LocationManagerService;->createSanitizedRequest(Landroid/location/LocationRequest;I)Landroid/location/LocationRequest;

    #@15
    move-result-object v9

    #@16
    .line 1243
    .local v9, sanitizedRequest:Landroid/location/LocationRequest;
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@19
    move-result v3

    #@1a
    .line 1244
    .local v3, pid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1d
    move-result v4

    #@1e
    .local v4, uid:I
    move-object v0, p0

    #@1f
    move-object v1, p2

    #@20
    move-object v2, p3

    #@21
    move-object v5, p4

    #@22
    .line 1245
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LocationManagerService;->checkListenerOrIntent(Landroid/location/ILocationListener;Landroid/app/PendingIntent;IILjava/lang/String;)Lcom/android/server/LocationManagerService$Receiver;

    #@25
    move-result-object v2

    #@26
    .line 1248
    .local v2, recevier:Lcom/android/server/LocationManagerService$Receiver;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@29
    move-result-wide v7

    #@2a
    .line 1250
    .local v7, identity:J
    :try_start_2a
    iget-object v10, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@2c
    monitor-enter v10
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_3b

    #@2d
    move-object v0, p0

    #@2e
    move-object v1, v9

    #@2f
    move-object v5, p4

    #@30
    .line 1251
    :try_start_30
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LocationManagerService;->requestLocationUpdatesLocked(Landroid/location/LocationRequest;Lcom/android/server/LocationManagerService$Receiver;IILjava/lang/String;)V

    #@33
    .line 1252
    monitor-exit v10
    :try_end_34
    .catchall {:try_start_30 .. :try_end_34} :catchall_38

    #@34
    .line 1254
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@37
    .line 1256
    return-void

    #@38
    .line 1252
    :catchall_38
    move-exception v0

    #@39
    :try_start_39
    monitor-exit v10
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    #@3a
    :try_start_3a
    throw v0
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_3b

    #@3b
    .line 1254
    :catchall_3b
    move-exception v0

    #@3c
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3f
    throw v0
.end method

.method public run()V
    .registers 3

    #@0
    .prologue
    .line 217
    const/16 v0, 0xa

    #@2
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@5
    .line 218
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@8
    .line 219
    new-instance v0, Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-direct {v0, p0, v1}, Lcom/android/server/LocationManagerService$LocationWorkerHandler;-><init>(Lcom/android/server/LocationManagerService;Lcom/android/server/LocationManagerService$1;)V

    #@e
    iput-object v0, p0, Lcom/android/server/LocationManagerService;->mLocationHandler:Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@10
    .line 220
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->init()V

    #@13
    .line 221
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@16
    .line 222
    return-void
.end method

.method public sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 7
    .parameter "provider"
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    .line 1504
    if-nez p1, :cond_8

    #@2
    .line 1506
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v1

    #@8
    .line 1508
    :cond_8
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->getCallerAllowedResolutionLevel()I

    #@b
    move-result v1

    #@c
    invoke-direct {p0, v1, p1}, Lcom/android/server/LocationManagerService;->checkResolutionLevelIsSufficientForProviderUse(ILjava/lang/String;)V

    #@f
    .line 1512
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mContext:Landroid/content/Context;

    #@11
    const-string v2, "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS"

    #@13
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_21

    #@19
    .line 1514
    new-instance v1, Ljava/lang/SecurityException;

    #@1b
    const-string v2, "Requires ACCESS_LOCATION_EXTRA_COMMANDS permission"

    #@1d
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1

    #@21
    .line 1517
    :cond_21
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@23
    monitor-enter v2

    #@24
    .line 1518
    :try_start_24
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mProvidersByName:Ljava/util/HashMap;

    #@26
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Lcom/android/server/location/LocationProviderInterface;

    #@2c
    .line 1519
    .local v0, p:Lcom/android/server/location/LocationProviderInterface;
    if-nez v0, :cond_31

    #@2e
    const/4 v1, 0x0

    #@2f
    monitor-exit v2

    #@30
    .line 1521
    :goto_30
    return v1

    #@31
    :cond_31
    invoke-interface {v0, p2, p3}, Lcom/android/server/location/LocationProviderInterface;->sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@34
    move-result v1

    #@35
    monitor-exit v2

    #@36
    goto :goto_30

    #@37
    .line 1522
    .end local v0           #p:Lcom/android/server/location/LocationProviderInterface;
    :catchall_37
    move-exception v1

    #@38
    monitor-exit v2
    :try_end_39
    .catchall {:try_start_24 .. :try_end_39} :catchall_37

    #@39
    throw v1
.end method

.method public sendNiResponse(II)Z
    .registers 6
    .parameter "notifId"
    .parameter "userResponse"

    #@0
    .prologue
    .line 1527
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v1

    #@4
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@7
    move-result v2

    #@8
    if-eq v1, v2, :cond_12

    #@a
    .line 1528
    new-instance v1, Ljava/lang/SecurityException;

    #@c
    const-string v2, "calling sendNiResponse from outside of the system is not allowed"

    #@e
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 1532
    :cond_12
    :try_start_12
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mNetInitiatedListener:Landroid/location/INetInitiatedListener;

    #@14
    invoke-interface {v1, p1, p2}, Landroid/location/INetInitiatedListener;->sendNiResponse(II)Z
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    move-result v1

    #@18
    .line 1535
    :goto_18
    return v1

    #@19
    .line 1533
    :catch_19
    move-exception v0

    #@1a
    .line 1534
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "LocationManagerService"

    #@1c
    const-string v2, "RemoteException in LocationManagerService.sendNiResponse"

    #@1e
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1535
    const/4 v1, 0x0

    #@22
    goto :goto_18
.end method

.method public setTestProviderEnabled(Ljava/lang/String;Z)V
    .registers 10
    .parameter "provider"
    .parameter "enabled"

    #@0
    .prologue
    .line 2083
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 2084
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v4

    #@6
    .line 2085
    :try_start_6
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@8
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Lcom/android/server/location/MockProvider;

    #@e
    .line 2086
    .local v2, mockProvider:Lcom/android/server/location/MockProvider;
    if-nez v2, :cond_32

    #@10
    .line 2087
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "Provider \""

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    const-string v6, "\" unknown"

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v3

    #@2f
    .line 2101
    .end local v2           #mockProvider:Lcom/android/server/location/MockProvider;
    :catchall_2f
    move-exception v3

    #@30
    monitor-exit v4
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_2f

    #@31
    throw v3

    #@32
    .line 2089
    .restart local v2       #mockProvider:Lcom/android/server/location/MockProvider;
    :cond_32
    :try_start_32
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@35
    move-result-wide v0

    #@36
    .line 2090
    .local v0, identity:J
    if-eqz p2, :cond_4d

    #@38
    .line 2091
    invoke-virtual {v2}, Lcom/android/server/location/MockProvider;->enable()V

    #@3b
    .line 2092
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@3d
    invoke-interface {v3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@40
    .line 2093
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mDisabledProviders:Ljava/util/Set;

    #@42
    invoke-interface {v3, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    #@45
    .line 2099
    :goto_45
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->updateProvidersLocked()V

    #@48
    .line 2100
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b
    .line 2101
    monitor-exit v4

    #@4c
    .line 2102
    return-void

    #@4d
    .line 2095
    :cond_4d
    invoke-virtual {v2}, Lcom/android/server/location/MockProvider;->disable()V

    #@50
    .line 2096
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mEnabledProviders:Ljava/util/Set;

    #@52
    invoke-interface {v3, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    #@55
    .line 2097
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mDisabledProviders:Ljava/util/Set;

    #@57
    invoke-interface {v3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_5a
    .catchall {:try_start_32 .. :try_end_5a} :catchall_2f

    #@5a
    goto :goto_45
.end method

.method public setTestProviderLocation(Ljava/lang/String;Landroid/location/Location;)V
    .registers 10
    .parameter "provider"
    .parameter "loc"

    #@0
    .prologue
    .line 2056
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 2057
    iget-object v4, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v4

    #@6
    .line 2058
    :try_start_6
    iget-object v3, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@8
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Lcom/android/server/location/MockProvider;

    #@e
    .line 2059
    .local v2, mockProvider:Lcom/android/server/location/MockProvider;
    if-nez v2, :cond_32

    #@10
    .line 2060
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "Provider \""

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    const-string v6, "\" unknown"

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v3

    #@2f
    .line 2066
    .end local v2           #mockProvider:Lcom/android/server/location/MockProvider;
    :catchall_2f
    move-exception v3

    #@30
    monitor-exit v4
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_2f

    #@31
    throw v3

    #@32
    .line 2063
    .restart local v2       #mockProvider:Lcom/android/server/location/MockProvider;
    :cond_32
    :try_start_32
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@35
    move-result-wide v0

    #@36
    .line 2064
    .local v0, identity:J
    invoke-virtual {v2, p2}, Lcom/android/server/location/MockProvider;->setLocation(Landroid/location/Location;)V

    #@39
    .line 2065
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3c
    .line 2066
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_32 .. :try_end_3d} :catchall_2f

    #@3d
    .line 2067
    return-void
.end method

.method public setTestProviderStatus(Ljava/lang/String;ILandroid/os/Bundle;J)V
    .registers 11
    .parameter "provider"
    .parameter "status"
    .parameter "extras"
    .parameter "updateTime"

    #@0
    .prologue
    .line 2122
    invoke-direct {p0}, Lcom/android/server/LocationManagerService;->checkMockPermissionsSafe()V

    #@3
    .line 2123
    iget-object v2, p0, Lcom/android/server/LocationManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v2

    #@6
    .line 2124
    :try_start_6
    iget-object v1, p0, Lcom/android/server/LocationManagerService;->mMockProviders:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Lcom/android/server/location/MockProvider;

    #@e
    .line 2125
    .local v0, mockProvider:Lcom/android/server/location/MockProvider;
    if-nez v0, :cond_32

    #@10
    .line 2126
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Provider \""

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, "\" unknown"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 2129
    .end local v0           #mockProvider:Lcom/android/server/location/MockProvider;
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_2f

    #@31
    throw v1

    #@32
    .line 2128
    .restart local v0       #mockProvider:Lcom/android/server/location/MockProvider;
    :cond_32
    :try_start_32
    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/server/location/MockProvider;->setStatus(ILandroid/os/Bundle;J)V

    #@35
    .line 2129
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_32 .. :try_end_36} :catchall_2f

    #@36
    .line 2130
    return-void
.end method

.method public systemReady()V
    .registers 4

    #@0
    .prologue
    .line 211
    new-instance v0, Ljava/lang/Thread;

    #@2
    const/4 v1, 0x0

    #@3
    const-string v2, "LocationManagerService"

    #@5
    invoke-direct {v0, v1, p0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    #@8
    .line 212
    .local v0, thread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@b
    .line 213
    return-void
.end method
