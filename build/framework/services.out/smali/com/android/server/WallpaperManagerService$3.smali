.class Lcom/android/server/WallpaperManagerService$3;
.super Landroid/os/Handler;
.source "WallpaperManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WallpaperManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/WallpaperManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/WallpaperManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1599
    iput-object p1, p0, Lcom/android/server/WallpaperManagerService$3;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 20
    .parameter "msg"

    #@0
    .prologue
    .line 1601
    move-object/from16 v0, p1

    #@2
    iget v15, v0, Landroid/os/Message;->what:I

    #@4
    .line 1602
    .local v15, usrId:I
    move-object/from16 v0, p0

    #@6
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$3;->this$0:Lcom/android/server/WallpaperManagerService;

    #@8
    iget-object v1, v1, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@a
    invoke-virtual {v1, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v5

    #@e
    check-cast v5, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@10
    .line 1603
    .local v5, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-nez v5, :cond_13

    #@12
    .line 1661
    :cond_12
    :goto_12
    return-void

    #@13
    .line 1605
    :cond_13
    iget-object v1, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperFile:Ljava/io/File;

    #@15
    if-eqz v1, :cond_12

    #@17
    .line 1608
    iget-object v1, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperFile:Ljava/io/File;

    #@19
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@1c
    move-result-object v10

    #@1d
    .line 1609
    .local v10, path:Ljava/lang/String;
    invoke-static {v10}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I

    #@20
    move-result v8

    #@21
    .line 1610
    .local v8, drmType:I
    const/16 v1, 0x10

    #@23
    if-lt v8, v1, :cond_12

    #@25
    const/16 v1, 0x3000

    #@27
    if-gt v8, v1, :cond_12

    #@29
    .line 1612
    const/4 v1, 0x0

    #@2a
    :try_start_2a
    invoke-static {v10, v1}, Lcom/lge/lgdrm/DrmManager;->createContentSession(Ljava/lang/String;Landroid/content/Context;)Lcom/lge/lgdrm/DrmContentSession;

    #@2d
    move-result-object v12

    #@2e
    .line 1613
    .local v12, session:Lcom/lge/lgdrm/DrmContentSession;
    if-eqz v12, :cond_12

    #@30
    .line 1614
    const/4 v1, 0x3

    #@31
    invoke-virtual {v12, v1}, Lcom/lge/lgdrm/DrmContentSession;->isActionSupported(I)Z

    #@34
    move-result v1

    #@35
    if-nez v1, :cond_54

    #@37
    .line 1616
    move-object/from16 v0, p0

    #@39
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$3;->this$0:Lcom/android/server/WallpaperManagerService;

    #@3b
    invoke-static {v1, v5}, Lcom/android/server/WallpaperManagerService;->access$100(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@3e
    .line 1617
    move-object/from16 v0, p0

    #@40
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$3;->this$0:Lcom/android/server/WallpaperManagerService;

    #@42
    sget-object v2, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@44
    const/4 v3, 0x1

    #@45
    const/4 v4, 0x0

    #@46
    const/4 v6, 0x0

    #@47
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z

    #@4a
    .line 1619
    move-object/from16 v0, p0

    #@4c
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$3;->this$0:Lcom/android/server/WallpaperManagerService;

    #@4e
    invoke-static {v1, v5}, Lcom/android/server/WallpaperManagerService;->access$200(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@51
    goto :goto_12

    #@52
    .line 1658
    .end local v12           #session:Lcom/lge/lgdrm/DrmContentSession;
    :catch_52
    move-exception v1

    #@53
    goto :goto_12

    #@54
    .line 1622
    .restart local v12       #session:Lcom/lge/lgdrm/DrmContentSession;
    :cond_54
    const/16 v16, 0x0

    #@56
    .line 1624
    .local v16, validFor:Ljava/lang/String;
    invoke-virtual {v12}, Lcom/lge/lgdrm/DrmContentSession;->getDrmTime()J

    #@59
    move-result-wide v1

    #@5a
    const-wide/16 v3, 0x0

    #@5c
    invoke-virtual {v12, v1, v2, v3, v4}, Lcom/lge/lgdrm/DrmContentSession;->consumeRight(JJ)I

    #@5f
    .line 1625
    const/4 v1, 0x1

    #@60
    invoke-virtual {v12, v1}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedRight(Z)Lcom/lge/lgdrm/DrmRight;

    #@63
    move-result-object v11

    #@64
    .line 1626
    .local v11, right:Lcom/lge/lgdrm/DrmRight;
    if-eqz v11, :cond_71

    #@66
    invoke-virtual {v11}, Lcom/lge/lgdrm/DrmRight;->isUnlimited()Z

    #@69
    move-result v1

    #@6a
    if-nez v1, :cond_71

    #@6c
    .line 1627
    const/4 v1, 0x1

    #@6d
    invoke-virtual {v11, v1}, Lcom/lge/lgdrm/DrmRight;->getSummaryInfo(I)Ljava/lang/String;

    #@70
    move-result-object v16

    #@71
    .line 1630
    :cond_71
    if-eqz v16, :cond_12

    #@73
    .line 1631
    const-string v1, " "

    #@75
    move-object/from16 v0, v16

    #@77
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@7a
    move-result-object v9

    #@7b
    .line 1632
    .local v9, list:[Ljava/lang/String;
    const/4 v7, 0x0

    #@7c
    .line 1633
    .local v7, count:I
    const-wide/16 v13, 0x0

    #@7e
    .line 1634
    .local v13, sum:J
    const/16 v17, 0x0

    #@80
    .line 1635
    .local v17, value:I
    :goto_80
    if-eqz v9, :cond_cf

    #@82
    array-length v1, v9

    #@83
    if-ge v7, v1, :cond_cf

    #@85
    .line 1636
    aget-object v1, v9, v7

    #@87
    const-string v2, "day"

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v1

    #@8d
    if-eqz v1, :cond_99

    #@8f
    .line 1637
    const v1, 0x15180

    #@92
    mul-int v1, v1, v17

    #@94
    int-to-long v1, v1

    #@95
    add-long/2addr v13, v1

    #@96
    .line 1647
    :goto_96
    add-int/lit8 v7, v7, 0x1

    #@98
    goto :goto_80

    #@99
    .line 1638
    :cond_99
    aget-object v1, v9, v7

    #@9b
    const-string v2, "hour"

    #@9d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v1

    #@a1
    if-eqz v1, :cond_aa

    #@a3
    .line 1639
    move/from16 v0, v17

    #@a5
    mul-int/lit16 v1, v0, 0xe10

    #@a7
    int-to-long v1, v1

    #@a8
    add-long/2addr v13, v1

    #@a9
    goto :goto_96

    #@aa
    .line 1640
    :cond_aa
    aget-object v1, v9, v7

    #@ac
    const-string v2, "min"

    #@ae
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v1

    #@b2
    if-eqz v1, :cond_b9

    #@b4
    .line 1641
    mul-int/lit8 v1, v17, 0x3c

    #@b6
    int-to-long v1, v1

    #@b7
    add-long/2addr v13, v1

    #@b8
    goto :goto_96

    #@b9
    .line 1642
    :cond_b9
    aget-object v1, v9, v7

    #@bb
    const-string v2, "sec"

    #@bd
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v1

    #@c1
    if-eqz v1, :cond_c8

    #@c3
    .line 1643
    add-int/lit8 v1, v17, 0x3

    #@c5
    int-to-long v1, v1

    #@c6
    add-long/2addr v13, v1

    #@c7
    goto :goto_96

    #@c8
    .line 1645
    :cond_c8
    aget-object v1, v9, v7

    #@ca
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@cd
    move-result v17

    #@ce
    goto :goto_96

    #@cf
    .line 1649
    :cond_cf
    const-wide/16 v1, 0x0

    #@d1
    cmp-long v1, v13, v1

    #@d3
    if-eqz v1, :cond_12

    #@d5
    .line 1651
    const/4 v1, 0x1

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget-object v2, v0, Lcom/android/server/WallpaperManagerService$3;->this$0:Lcom/android/server/WallpaperManagerService;

    #@da
    invoke-static {v2}, Lcom/android/server/WallpaperManagerService;->access$300(Lcom/android/server/WallpaperManagerService;)Landroid/os/Handler;

    #@dd
    move-result-object v2

    #@de
    const-wide/16 v3, 0x3e8

    #@e0
    mul-long/2addr v3, v13

    #@e1
    invoke-virtual {v2, v15, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_e4
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_e4} :catch_52

    #@e4
    move-result v2

    #@e5
    if-ne v1, v2, :cond_12

    #@e7
    goto/16 :goto_12
.end method
