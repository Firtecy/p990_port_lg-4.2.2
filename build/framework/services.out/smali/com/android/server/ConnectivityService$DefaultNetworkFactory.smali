.class Lcom/android/server/ConnectivityService$DefaultNetworkFactory;
.super Ljava/lang/Object;
.source "ConnectivityService.java"

# interfaces
.implements Lcom/android/server/ConnectivityService$NetworkFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ConnectivityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultNetworkFactory"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field protected mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field private final mTrackerHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3
    .parameter "context"
    .parameter "trackerHandler"

    #@0
    .prologue
    .line 817
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 818
    iput-object p1, p0, Lcom/android/server/ConnectivityService$DefaultNetworkFactory;->mContext:Landroid/content/Context;

    #@5
    .line 819
    iput-object p2, p0, Lcom/android/server/ConnectivityService$DefaultNetworkFactory;->mTrackerHandler:Landroid/os/Handler;

    #@7
    .line 820
    return-void
.end method


# virtual methods
.method public createTracker(ILandroid/net/NetworkConfig;)Landroid/net/NetworkStateTracker;
    .registers 6
    .parameter "targetNetworkType"
    .parameter "config"

    #@0
    .prologue
    .line 824
    iget v0, p2, Landroid/net/NetworkConfig;->radio:I

    #@2
    packed-switch v0, :pswitch_data_4e

    #@5
    .line 848
    :pswitch_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Trying to create a NetworkStateTracker for an unknown radio type: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget v2, p2, Landroid/net/NetworkConfig;->radio:I

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 826
    :pswitch_20
    new-instance v0, Landroid/net/wifi/WifiStateTracker;

    #@22
    iget-object v1, p2, Landroid/net/NetworkConfig;->name:Ljava/lang/String;

    #@24
    invoke-direct {v0, p1, v1}, Landroid/net/wifi/WifiStateTracker;-><init>(ILjava/lang/String;)V

    #@27
    .line 846
    :goto_27
    return-object v0

    #@28
    .line 837
    :pswitch_28
    new-instance v0, Landroid/net/MobileDataStateTracker;

    #@2a
    iget-object v1, p2, Landroid/net/NetworkConfig;->name:Ljava/lang/String;

    #@2c
    iget-object v2, p0, Lcom/android/server/ConnectivityService$DefaultNetworkFactory;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@2e
    invoke-direct {v0, p1, v1, v2}, Landroid/net/MobileDataStateTracker;-><init>(ILjava/lang/String;Lcom/android/internal/telephony/LGfeature;)V

    #@31
    goto :goto_27

    #@32
    .line 840
    :pswitch_32
    new-instance v0, Landroid/net/DummyDataStateTracker;

    #@34
    iget-object v1, p2, Landroid/net/NetworkConfig;->name:Ljava/lang/String;

    #@36
    invoke-direct {v0, p1, v1}, Landroid/net/DummyDataStateTracker;-><init>(ILjava/lang/String;)V

    #@39
    goto :goto_27

    #@3a
    .line 842
    :pswitch_3a
    invoke-static {}, Landroid/bluetooth/BluetoothTetheringDataTracker;->getInstance()Landroid/bluetooth/BluetoothTetheringDataTracker;

    #@3d
    move-result-object v0

    #@3e
    goto :goto_27

    #@3f
    .line 844
    :pswitch_3f
    iget-object v0, p0, Lcom/android/server/ConnectivityService$DefaultNetworkFactory;->mContext:Landroid/content/Context;

    #@41
    iget-object v1, p0, Lcom/android/server/ConnectivityService$DefaultNetworkFactory;->mTrackerHandler:Landroid/os/Handler;

    #@43
    invoke-static {v0, v1}, Lcom/android/server/ConnectivityService;->access$000(Landroid/content/Context;Landroid/os/Handler;)Landroid/net/NetworkStateTracker;

    #@46
    move-result-object v0

    #@47
    goto :goto_27

    #@48
    .line 846
    :pswitch_48
    invoke-static {}, Landroid/net/EthernetDataTracker;->getInstance()Landroid/net/EthernetDataTracker;

    #@4b
    move-result-object v0

    #@4c
    goto :goto_27

    #@4d
    .line 824
    nop

    #@4e
    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_28
        :pswitch_20
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3f
        :pswitch_3a
        :pswitch_32
        :pswitch_48
    .end packed-switch
.end method
