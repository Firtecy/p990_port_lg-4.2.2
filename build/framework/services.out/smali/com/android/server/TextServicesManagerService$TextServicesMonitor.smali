.class Lcom/android/server/TextServicesManagerService$TextServicesMonitor;
.super Lcom/android/internal/content/PackageMonitor;
.source "TextServicesManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/TextServicesManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextServicesMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/TextServicesManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/TextServicesManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 137
    iput-object p1, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@2
    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/TextServicesManagerService;Lcom/android/server/TextServicesManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;-><init>(Lcom/android/server/TextServicesManagerService;)V

    #@3
    return-void
.end method

.method private isChangingPackagesOfCurrentUser()Z
    .registers 4

    #@0
    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->getChangingUserId()I

    #@3
    move-result v1

    #@4
    .line 140
    .local v1, userId:I
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@6
    invoke-static {v2}, Lcom/android/server/TextServicesManagerService;->access$300(Lcom/android/server/TextServicesManagerService;)Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v2}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->getCurrentUserId()I

    #@d
    move-result v2

    #@e
    if-ne v1, v2, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    .line 144
    .local v0, retval:Z
    :goto_11
    return v0

    #@12
    .line 140
    .end local v0           #retval:Z
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method


# virtual methods
.method public onSomePackagesChanged()V
    .registers 9

    #@0
    .prologue
    .line 149
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->isChangingPackagesOfCurrentUser()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_7

    #@6
    .line 170
    :goto_6
    return-void

    #@7
    .line 152
    :cond_7
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@9
    invoke-static {v3}, Lcom/android/server/TextServicesManagerService;->access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@c
    move-result-object v4

    #@d
    monitor-enter v4

    #@e
    .line 153
    :try_start_e
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@10
    invoke-static {v3}, Lcom/android/server/TextServicesManagerService;->access$400(Lcom/android/server/TextServicesManagerService;)Landroid/content/Context;

    #@13
    move-result-object v3

    #@14
    iget-object v5, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@16
    invoke-static {v5}, Lcom/android/server/TextServicesManagerService;->access$500(Lcom/android/server/TextServicesManagerService;)Ljava/util/ArrayList;

    #@19
    move-result-object v5

    #@1a
    iget-object v6, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@1c
    invoke-static {v6}, Lcom/android/server/TextServicesManagerService;->access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@1f
    move-result-object v6

    #@20
    iget-object v7, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@22
    invoke-static {v7}, Lcom/android/server/TextServicesManagerService;->access$300(Lcom/android/server/TextServicesManagerService;)Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@25
    move-result-object v7

    #@26
    invoke-static {v3, v5, v6, v7}, Lcom/android/server/TextServicesManagerService;->access$600(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/HashMap;Lcom/android/server/TextServicesManagerService$TextServicesSettings;)V

    #@29
    .line 156
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual {v3, v5}, Lcom/android/server/TextServicesManagerService;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@2f
    move-result-object v2

    #@30
    .line 157
    .local v2, sci:Landroid/view/textservice/SpellCheckerInfo;
    if-nez v2, :cond_37

    #@32
    monitor-exit v4

    #@33
    goto :goto_6

    #@34
    .line 169
    .end local v2           #sci:Landroid/view/textservice/SpellCheckerInfo;
    :catchall_34
    move-exception v3

    #@35
    monitor-exit v4
    :try_end_36
    .catchall {:try_start_e .. :try_end_36} :catchall_34

    #@36
    throw v3

    #@37
    .line 158
    .restart local v2       #sci:Landroid/view/textservice/SpellCheckerInfo;
    :cond_37
    :try_start_37
    invoke-virtual {v2}, Landroid/view/textservice/SpellCheckerInfo;->getPackageName()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    .line 159
    .local v1, packageName:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->isPackageDisappearing(Ljava/lang/String;)I

    #@3e
    move-result v0

    #@3f
    .line 160
    .local v0, change:I
    const/4 v3, 0x3

    #@40
    if-eq v0, v3, :cond_4b

    #@42
    const/4 v3, 0x2

    #@43
    if-eq v0, v3, :cond_4b

    #@45
    invoke-virtual {p0, v1}, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->isPackageModified(Ljava/lang/String;)Z

    #@48
    move-result v3

    #@49
    if-eqz v3, :cond_5d

    #@4b
    .line 164
    :cond_4b
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@4d
    const/4 v5, 0x0

    #@4e
    invoke-static {v3, v5, v1}, Lcom/android/server/TextServicesManagerService;->access$700(Lcom/android/server/TextServicesManagerService;Ljava/lang/String;Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@51
    move-result-object v2

    #@52
    .line 165
    if-eqz v2, :cond_5d

    #@54
    .line 166
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->this$0:Lcom/android/server/TextServicesManagerService;

    #@56
    invoke-virtual {v2}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-static {v3, v5}, Lcom/android/server/TextServicesManagerService;->access$800(Lcom/android/server/TextServicesManagerService;Ljava/lang/String;)V

    #@5d
    .line 169
    :cond_5d
    monitor-exit v4
    :try_end_5e
    .catchall {:try_start_37 .. :try_end_5e} :catchall_34

    #@5e
    goto :goto_6
.end method
