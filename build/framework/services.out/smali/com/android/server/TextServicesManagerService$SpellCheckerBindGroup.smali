.class Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
.super Ljava/lang/Object;
.source "TextServicesManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/TextServicesManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpellCheckerBindGroup"
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field public mBound:Z

.field public mConnected:Z

.field private final mInternalConnection:Lcom/android/server/TextServicesManagerService$InternalServiceConnection;

.field private final mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;",
            ">;"
        }
    .end annotation
.end field

.field public mSpellChecker:Lcom/android/internal/textservice/ISpellCheckerService;

.field final synthetic this$0:Lcom/android/server/TextServicesManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/TextServicesManagerService;Lcom/android/server/TextServicesManagerService$InternalServiceConnection;Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)V
    .registers 14
    .parameter
    .parameter "connection"
    .parameter "listener"
    .parameter "locale"
    .parameter "scListener"
    .parameter "uid"
    .parameter "bundle"

    #@0
    .prologue
    .line 717
    iput-object p1, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 707
    const-class v0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@7
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->TAG:Ljava/lang/String;

    #@d
    .line 709
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    #@f
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@14
    .line 718
    iput-object p2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mInternalConnection:Lcom/android/server/TextServicesManagerService$InternalServiceConnection;

    #@16
    .line 719
    const/4 v0, 0x1

    #@17
    iput-boolean v0, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mBound:Z

    #@19
    .line 720
    const/4 v0, 0x0

    #@1a
    iput-boolean v0, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mConnected:Z

    #@1c
    move-object v0, p0

    #@1d
    move-object v1, p3

    #@1e
    move-object v2, p4

    #@1f
    move-object v3, p5

    #@20
    move v4, p6

    #@21
    move-object v5, p7

    #@22
    .line 721
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->addListener(Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@25
    .line 722
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 706
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;)Lcom/android/server/TextServicesManagerService$InternalServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 706
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mInternalConnection:Lcom/android/server/TextServicesManagerService$InternalServiceConnection;

    #@2
    return-object v0
.end method

.method private cleanLocked()V
    .registers 5

    #@0
    .prologue
    .line 815
    iget-boolean v2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mBound:Z

    #@2
    if-eqz v2, :cond_37

    #@4
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_37

    #@c
    .line 816
    const/4 v2, 0x0

    #@d
    iput-boolean v2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mBound:Z

    #@f
    .line 817
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mInternalConnection:Lcom/android/server/TextServicesManagerService$InternalServiceConnection;

    #@11
    invoke-static {v2}, Lcom/android/server/TextServicesManagerService$InternalServiceConnection;->access$1200(Lcom/android/server/TextServicesManagerService$InternalServiceConnection;)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 818
    .local v1, sciId:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@17
    invoke-static {v2}, Lcom/android/server/TextServicesManagerService;->access$1300(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@21
    .line 819
    .local v0, cur:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    if-ne v0, p0, :cond_2c

    #@23
    .line 823
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@25
    invoke-static {v2}, Lcom/android/server/TextServicesManagerService;->access$1300(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 825
    :cond_2c
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@2e
    invoke-static {v2}, Lcom/android/server/TextServicesManagerService;->access$400(Lcom/android/server/TextServicesManagerService;)Landroid/content/Context;

    #@31
    move-result-object v2

    #@32
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mInternalConnection:Lcom/android/server/TextServicesManagerService$InternalServiceConnection;

    #@34
    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@37
    .line 827
    .end local v0           #cur:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    .end local v1           #sciId:Ljava/lang/String;
    :cond_37
    return-void
.end method


# virtual methods
.method public addListener(Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .registers 18
    .parameter "tsListener"
    .parameter "locale"
    .parameter "scListener"
    .parameter "uid"
    .parameter "bundle"

    #@0
    .prologue
    .line 756
    const/4 v9, 0x0

    #@1
    .line 757
    .local v9, recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@3
    invoke-static {v1}, Lcom/android/server/TextServicesManagerService;->access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@6
    move-result-object v11

    #@7
    monitor-enter v11

    #@8
    .line 759
    :try_start_8
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@a
    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@d
    move-result v10

    #@e
    .line 760
    .local v10, size:I
    const/4 v8, 0x0

    #@f
    .local v8, i:I
    :goto_f
    if-ge v8, v10, :cond_27

    #@11
    .line 761
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@13
    invoke-virtual {v1, v8}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@19
    invoke-virtual {v1, p3}, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->hasSpellCheckerListener(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)Z
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_49
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_1c} :catch_4f

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_24

    #@1f
    .line 763
    const/4 v0, 0x0

    #@20
    :try_start_20
    monitor-exit v11
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_49

    #@21
    move-object v1, v0

    #@22
    move-object v0, v9

    #@23
    .line 775
    .end local v8           #i:I
    .end local v9           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .end local v10           #size:I
    .local v0, recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :goto_23
    return-object v1

    #@24
    .line 760
    .end local v0           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v8       #i:I
    .restart local v9       #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v10       #size:I
    :cond_24
    add-int/lit8 v8, v8, 0x1

    #@26
    goto :goto_f

    #@27
    .line 766
    :cond_27
    :try_start_27
    new-instance v0, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@29
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@2b
    move-object v2, p0

    #@2c
    move-object v3, p1

    #@2d
    move-object v4, p2

    #@2e
    move-object v5, p3

    #@2f
    move/from16 v6, p4

    #@31
    move-object/from16 v7, p5

    #@33
    invoke-direct/range {v0 .. v7}, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;-><init>(Lcom/android/server/TextServicesManagerService;Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)V
    :try_end_36
    .catchall {:try_start_27 .. :try_end_36} :catchall_49
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_36} :catch_4f

    #@36
    .line 768
    .end local v9           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v0       #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :try_start_36
    invoke-interface {p3}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->asBinder()Landroid/os/IBinder;

    #@39
    move-result-object v1

    #@3a
    const/4 v2, 0x0

    #@3b
    invoke-interface {v1, v0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@3e
    .line 769
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@40
    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z
    :try_end_43
    .catchall {:try_start_36 .. :try_end_43} :catchall_4d
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_43} :catch_52

    #@43
    .line 773
    .end local v8           #i:I
    .end local v10           #size:I
    :goto_43
    :try_start_43
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->cleanLocked()V

    #@46
    .line 774
    monitor-exit v11

    #@47
    move-object v1, v0

    #@48
    .line 775
    goto :goto_23

    #@49
    .line 774
    .end local v0           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v9       #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :catchall_49
    move-exception v1

    #@4a
    move-object v0, v9

    #@4b
    .end local v9           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v0       #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :goto_4b
    monitor-exit v11
    :try_end_4c
    .catchall {:try_start_43 .. :try_end_4c} :catchall_4d

    #@4c
    throw v1

    #@4d
    :catchall_4d
    move-exception v1

    #@4e
    goto :goto_4b

    #@4f
    .line 770
    .end local v0           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v9       #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :catch_4f
    move-exception v1

    #@50
    move-object v0, v9

    #@51
    .end local v9           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v0       #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    goto :goto_43

    #@52
    .restart local v8       #i:I
    .restart local v10       #size:I
    :catch_52
    move-exception v1

    #@53
    goto :goto_43
.end method

.method public onServiceConnected(Lcom/android/internal/textservice/ISpellCheckerService;)V
    .registers 9
    .parameter "spellChecker"

    #@0
    .prologue
    .line 729
    iget-object v4, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_41

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@12
    .line 731
    .local v2, listener:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :try_start_12
    iget-object v4, v2, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScLocale:Ljava/lang/String;

    #@14
    iget-object v5, v2, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@16
    iget-object v6, v2, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mBundle:Landroid/os/Bundle;

    #@18
    invoke-interface {p1, v4, v5, v6}, Lcom/android/internal/textservice/ISpellCheckerService;->getISpellCheckerSession(Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)Lcom/android/internal/textservice/ISpellCheckerSession;

    #@1b
    move-result-object v3

    #@1c
    .line 733
    .local v3, session:Lcom/android/internal/textservice/ISpellCheckerSession;
    iget-object v4, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@1e
    invoke-static {v4}, Lcom/android/server/TextServicesManagerService;->access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@21
    move-result-object v5

    #@22
    monitor-enter v5
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_23} :catch_35

    #@23
    .line 734
    :try_start_23
    iget-object v4, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@25
    invoke-virtual {v4, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    #@28
    move-result v4

    #@29
    if-eqz v4, :cond_30

    #@2b
    .line 735
    iget-object v4, v2, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mTsListener:Lcom/android/internal/textservice/ITextServicesSessionListener;

    #@2d
    invoke-interface {v4, v3}, Lcom/android/internal/textservice/ITextServicesSessionListener;->onServiceConnected(Lcom/android/internal/textservice/ISpellCheckerSession;)V

    #@30
    .line 737
    :cond_30
    monitor-exit v5

    #@31
    goto :goto_6

    #@32
    :catchall_32
    move-exception v4

    #@33
    monitor-exit v5
    :try_end_34
    .catchall {:try_start_23 .. :try_end_34} :catchall_32

    #@34
    :try_start_34
    throw v4
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_35} :catch_35

    #@35
    .line 738
    .end local v3           #session:Lcom/android/internal/textservice/ISpellCheckerSession;
    :catch_35
    move-exception v0

    #@36
    .line 739
    .local v0, e:Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->TAG:Ljava/lang/String;

    #@38
    const-string v5, "Exception in getting the spell checker session.Reconnect to the spellchecker. "

    #@3a
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d
    .line 741
    invoke-virtual {p0}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->removeAll()V

    #@40
    .line 749
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v2           #listener:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :goto_40
    return-void

    #@41
    .line 745
    :cond_41
    iget-object v4, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@43
    invoke-static {v4}, Lcom/android/server/TextServicesManagerService;->access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@46
    move-result-object v5

    #@47
    monitor-enter v5

    #@48
    .line 746
    :try_start_48
    iput-object p1, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mSpellChecker:Lcom/android/internal/textservice/ISpellCheckerService;

    #@4a
    .line 747
    const/4 v4, 0x1

    #@4b
    iput-boolean v4, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mConnected:Z

    #@4d
    .line 748
    monitor-exit v5

    #@4e
    goto :goto_40

    #@4f
    :catchall_4f
    move-exception v4

    #@50
    monitor-exit v5
    :try_end_51
    .catchall {:try_start_48 .. :try_end_51} :catchall_4f

    #@51
    throw v4
.end method

.method public removeAll()V
    .registers 7

    #@0
    .prologue
    .line 830
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->TAG:Ljava/lang/String;

    #@2
    const-string v4, "Remove the spell checker bind unexpectedly."

    #@4
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 831
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@9
    invoke-static {v3}, Lcom/android/server/TextServicesManagerService;->access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@c
    move-result-object v4

    #@d
    monitor-enter v4

    #@e
    .line 832
    :try_start_e
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@13
    move-result v2

    #@14
    .line 833
    .local v2, size:I
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    if-ge v0, v2, :cond_2c

    #@17
    .line 834
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@19
    invoke-virtual {v3, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@1f
    .line 835
    .local v1, idr:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    iget-object v3, v1, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@21
    invoke-interface {v3}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->asBinder()Landroid/os/IBinder;

    #@24
    move-result-object v3

    #@25
    const/4 v5, 0x0

    #@26
    invoke-interface {v3, v1, v5}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@29
    .line 833
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_15

    #@2c
    .line 837
    .end local v1           #idr:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :cond_2c
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2e
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    #@31
    .line 838
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->cleanLocked()V

    #@34
    .line 839
    monitor-exit v4

    #@35
    .line 840
    return-void

    #@36
    .line 839
    .end local v0           #i:I
    .end local v2           #size:I
    :catchall_36
    move-exception v3

    #@37
    monitor-exit v4
    :try_end_38
    .catchall {:try_start_e .. :try_end_38} :catchall_36

    #@38
    throw v3
.end method

.method public removeListener(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)V
    .registers 11
    .parameter "listener"

    #@0
    .prologue
    .line 782
    iget-object v6, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->this$0:Lcom/android/server/TextServicesManagerService;

    #@2
    invoke-static {v6}, Lcom/android/server/TextServicesManagerService;->access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;

    #@5
    move-result-object v7

    #@6
    monitor-enter v7

    #@7
    .line 783
    :try_start_7
    iget-object v6, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@9
    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@c
    move-result v4

    #@d
    .line 784
    .local v4, size:I
    new-instance v2, Ljava/util/ArrayList;

    #@f
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@12
    .line 786
    .local v2, removeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;>;"
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    if-ge v0, v4, :cond_29

    #@15
    .line 787
    iget-object v6, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@17
    invoke-virtual {v6, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v5

    #@1b
    check-cast v5, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@1d
    .line 788
    .local v5, tempRecipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    invoke-virtual {v5, p1}, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->hasSpellCheckerListener(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)Z

    #@20
    move-result v6

    #@21
    if-eqz v6, :cond_26

    #@23
    .line 792
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    .line 786
    :cond_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_13

    #@29
    .line 795
    .end local v5           #tempRecipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :cond_29
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2c
    move-result v3

    #@2d
    .line 796
    .local v3, removeSize:I
    const/4 v0, 0x0

    #@2e
    :goto_2e
    if-ge v0, v3, :cond_48

    #@30
    .line 800
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v1

    #@34
    check-cast v1, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@36
    .line 801
    .local v1, idr:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    iget-object v6, v1, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@38
    invoke-interface {v6}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->asBinder()Landroid/os/IBinder;

    #@3b
    move-result-object v6

    #@3c
    const/4 v8, 0x0

    #@3d
    invoke-interface {v6, v1, v8}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@40
    .line 802
    iget-object v6, p0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@42
    invoke-virtual {v6, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@45
    .line 796
    add-int/lit8 v0, v0, 0x1

    #@47
    goto :goto_2e

    #@48
    .line 804
    .end local v1           #idr:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :cond_48
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->cleanLocked()V

    #@4b
    .line 805
    monitor-exit v7

    #@4c
    .line 806
    return-void

    #@4d
    .line 805
    .end local v0           #i:I
    .end local v2           #removeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;>;"
    .end local v3           #removeSize:I
    .end local v4           #size:I
    :catchall_4d
    move-exception v6

    #@4e
    monitor-exit v7
    :try_end_4f
    .catchall {:try_start_7 .. :try_end_4f} :catchall_4d

    #@4f
    throw v6
.end method
