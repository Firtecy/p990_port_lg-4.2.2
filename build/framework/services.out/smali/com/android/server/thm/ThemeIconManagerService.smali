.class public Lcom/android/server/thm/ThemeIconManagerService;
.super Landroid/content/thm/IThemeIconManager$Stub;
.source "ThemeIconManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    }
.end annotation


# static fields
.field private static final DEFAULT_LOAD_FACTOR:F = 0.75f

.field private static DEFAULT_THEME_PACKAGE:Ljava/lang/String; = null

.field private static final LGHOME_PACKAGE:Ljava/lang/String; = "com.lge.launcher2"

.field private static final LGHOME_THEME_OPTIMUS:Ljava/lang/String; = "com.lge.launcher2.theme.optimus"

.field private static final MAX_REDIRECTIONMAP_ENTRIES:I = 0x32

.field private static final MAX_THEMEPACKAGEINFO_ENTRIES:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ThemeManagerService"

.field public static final THEME_PROPERTY:Ljava/lang/String; = "persist.sys.theme"


# instance fields
.field private mAppStateReceiver:Landroid/content/BroadcastReceiver;

.field private mBackgroundEnabled:Z

.field private final mContext:Landroid/content/Context;

.field private final mLock:Ljava/lang/Object;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mRedirectionMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;",
            "Landroid/content/thm/ThemeIconRedirectionMap;",
            ">;"
        }
    .end annotation
.end field

.field private final mThemePackageInfoMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/thm/ThemePackageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 52
    const-string v0, "com.lge.launcher2.theme.optimus"

    #@2
    sput-object v0, Lcom/android/server/thm/ThemeIconManagerService;->DEFAULT_THEME_PACKAGE:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/high16 v8, 0x3f40

    #@3
    const/4 v7, 0x0

    #@4
    .line 234
    invoke-direct {p0}, Landroid/content/thm/IThemeIconManager$Stub;-><init>()V

    #@7
    .line 62
    new-instance v6, Lcom/android/server/thm/ThemeIconManagerService$1;

    #@9
    invoke-direct {v6, p0, v7, v8, v9}, Lcom/android/server/thm/ThemeIconManagerService$1;-><init>(Lcom/android/server/thm/ThemeIconManagerService;IFZ)V

    #@c
    iput-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mThemePackageInfoMap:Ljava/util/LinkedHashMap;

    #@e
    .line 69
    new-instance v6, Lcom/android/server/thm/ThemeIconManagerService$2;

    #@10
    invoke-direct {v6, p0, v7, v8, v9}, Lcom/android/server/thm/ThemeIconManagerService$2;-><init>(Lcom/android/server/thm/ThemeIconManagerService;IFZ)V

    #@13
    iput-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mRedirectionMap:Ljava/util/LinkedHashMap;

    #@15
    .line 77
    new-instance v6, Ljava/lang/Object;

    #@17
    invoke-direct/range {v6 .. v6}, Ljava/lang/Object;-><init>()V

    #@1a
    iput-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mLock:Ljava/lang/Object;

    #@1c
    .line 201
    new-instance v6, Lcom/android/server/thm/ThemeIconManagerService$3;

    #@1e
    invoke-direct {v6, p0}, Lcom/android/server/thm/ThemeIconManagerService$3;-><init>(Lcom/android/server/thm/ThemeIconManagerService;)V

    #@21
    iput-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mAppStateReceiver:Landroid/content/BroadcastReceiver;

    #@23
    .line 235
    iput-object p1, p0, Lcom/android/server/thm/ThemeIconManagerService;->mContext:Landroid/content/Context;

    #@25
    .line 236
    iget-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@2a
    move-result-object v6

    #@2b
    iput-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2d
    .line 238
    const/4 v0, 0x0

    #@2e
    .line 240
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    :try_start_2e
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@31
    move-result-object v6

    #@32
    const-string v7, "com.lge.launcher2.theme.optimus"

    #@34
    const/4 v8, 0x0

    #@35
    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_38
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2e .. :try_end_38} :catch_77

    #@38
    move-result-object v0

    #@39
    .line 245
    :goto_39
    if-nez v0, :cond_3f

    #@3b
    .line 246
    const-string v6, "com.lge.launcher2"

    #@3d
    sput-object v6, Lcom/android/server/thm/ThemeIconManagerService;->DEFAULT_THEME_PACKAGE:Ljava/lang/String;

    #@3f
    .line 249
    :cond_3f
    sget-object v6, Lcom/android/server/thm/ThemeIconManagerService;->DEFAULT_THEME_PACKAGE:Ljava/lang/String;

    #@41
    invoke-static {p1, v6}, Lcom/android/server/thm/ThemeIconManagerService;->getResourcesForApplication(Landroid/content/Context;Ljava/lang/String;)Landroid/content/res/Resources;

    #@44
    move-result-object v5

    #@45
    .line 250
    .local v5, r:Landroid/content/res/Resources;
    if-eqz v5, :cond_62

    #@47
    .line 251
    const-string v6, "config_feature_use_theme_icon_bg"

    #@49
    const-string v7, "bool"

    #@4b
    sget-object v8, Lcom/android/server/thm/ThemeIconManagerService;->DEFAULT_THEME_PACKAGE:Ljava/lang/String;

    #@4d
    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@50
    move-result v4

    #@51
    .line 253
    .local v4, id:I
    if-eqz v4, :cond_59

    #@53
    .line 254
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@56
    move-result v6

    #@57
    iput-boolean v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mBackgroundEnabled:Z

    #@59
    .line 256
    :cond_59
    invoke-virtual {v5}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@5c
    move-result-object v1

    #@5d
    .line 257
    .local v1, assets:Landroid/content/res/AssetManager;
    if-eqz v1, :cond_62

    #@5f
    .line 258
    invoke-virtual {v1}, Landroid/content/res/AssetManager;->close()V

    #@62
    .line 262
    .end local v1           #assets:Landroid/content/res/AssetManager;
    .end local v4           #id:I
    :cond_62
    new-instance v3, Landroid/content/IntentFilter;

    #@64
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@67
    .line 263
    .local v3, filter:Landroid/content/IntentFilter;
    const-string v6, "android.intent.action.PACKAGE_DATA_CLEARED"

    #@69
    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6c
    .line 264
    const-string v6, "package"

    #@6e
    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@71
    .line 265
    iget-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mAppStateReceiver:Landroid/content/BroadcastReceiver;

    #@73
    invoke-virtual {p1, v6, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@76
    .line 266
    return-void

    #@77
    .line 241
    .end local v3           #filter:Landroid/content/IntentFilter;
    .end local v5           #r:Landroid/content/res/Resources;
    :catch_77
    move-exception v2

    #@78
    .line 242
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "ThemeManagerService"

    #@7a
    const-string v7, "can not find com.lge.launcher2.theme.optimus packages"

    #@7c
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_39
.end method

.method private generatePackageRedirectionMapLocked(Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;)Landroid/content/thm/ThemeIconRedirectionMap;
    .registers 27
    .parameter "key"

    #@0
    .prologue
    .line 448
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@4
    move-object/from16 v21, v0

    #@6
    move-object/from16 v0, p0

    #@8
    move-object/from16 v1, v21

    #@a
    invoke-direct {v0, v1}, Lcom/android/server/thm/ThemeIconManagerService;->getThemePackageInfoLocked(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;

    #@d
    move-result-object v12

    #@e
    .line 449
    .local v12, info:Landroid/content/thm/ThemePackageInfo;
    if-eqz v12, :cond_116

    #@10
    .line 450
    new-instance v17, Landroid/content/thm/ThemeIconRedirectionMap;

    #@12
    move-object/from16 v0, p1

    #@14
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@16
    move-object/from16 v21, v0

    #@18
    move-object/from16 v0, p1

    #@1a
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@1c
    move-object/from16 v22, v0

    #@1e
    move-object/from16 v0, v17

    #@20
    move-object/from16 v1, v21

    #@22
    move-object/from16 v2, v22

    #@24
    invoke-direct {v0, v1, v2}, Landroid/content/thm/ThemeIconRedirectionMap;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 453
    .local v17, map:Landroid/content/thm/ThemeIconRedirectionMap;
    iget-object v0, v12, Landroid/content/thm/ThemePackageInfo;->componentToResMap:Ljava/util/HashMap;

    #@29
    move-object/from16 v21, v0

    #@2b
    move-object/from16 v0, p1

    #@2d
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@2f
    move-object/from16 v22, v0

    #@31
    invoke-virtual/range {v21 .. v22}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    move-result-object v13

    #@35
    check-cast v13, Ljava/util/HashMap;

    #@37
    .line 454
    .local v13, items:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    if-eqz v13, :cond_79

    #@39
    .line 455
    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@3c
    move-result-object v15

    #@3d
    .line 456
    .local v15, keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@40
    move-result-object v14

    #@41
    .line 457
    .local v14, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    :cond_41
    :goto_41
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    #@44
    move-result v21

    #@45
    if-eqz v21, :cond_79

    #@47
    .line 458
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4a
    move-result-object v7

    #@4b
    check-cast v7, Landroid/content/ComponentName;

    #@4d
    .line 460
    .local v7, comp:Landroid/content/ComponentName;
    :try_start_4d
    move-object/from16 v0, p0

    #@4f
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@51
    move-object/from16 v21, v0

    #@53
    const/16 v22, 0x0

    #@55
    move-object/from16 v0, v21

    #@57
    move/from16 v1, v22

    #@59
    invoke-virtual {v0, v7, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@5c
    move-result-object v5

    #@5d
    .line 461
    .local v5, ai:Landroid/content/pm/ActivityInfo;
    if-eqz v5, :cond_41

    #@5f
    .line 462
    invoke-virtual {v5}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    #@62
    move-result v8

    #@63
    .line 463
    .local v8, fromIdent:I
    if-eqz v8, :cond_41

    #@65
    .line 464
    move-object/from16 v0, v17

    #@67
    iget-object v0, v0, Landroid/content/thm/ThemeIconRedirectionMap;->redirectionMap:Landroid/util/SparseArray;

    #@69
    move-object/from16 v21, v0

    #@6b
    invoke-virtual {v13, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    move-result-object v22

    #@6f
    move-object/from16 v0, v21

    #@71
    move-object/from16 v1, v22

    #@73
    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_76
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4d .. :try_end_76} :catch_77

    #@76
    goto :goto_41

    #@77
    .line 467
    .end local v5           #ai:Landroid/content/pm/ActivityInfo;
    .end local v8           #fromIdent:I
    :catch_77
    move-exception v21

    #@78
    goto :goto_41

    #@79
    .line 473
    .end local v7           #comp:Landroid/content/ComponentName;
    .end local v14           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    .end local v15           #keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/ComponentName;>;"
    :cond_79
    iget-object v0, v12, Landroid/content/thm/ThemePackageInfo;->bgIcons:[I

    #@7b
    move-object/from16 v21, v0

    #@7d
    if-eqz v21, :cond_111

    #@7f
    iget-object v0, v12, Landroid/content/thm/ThemePackageInfo;->bgIcons:[I

    #@81
    move-object/from16 v21, v0

    #@83
    move-object/from16 v0, v21

    #@85
    array-length v3, v0

    #@86
    .line 474
    .local v3, N:I
    :goto_86
    if-lez v3, :cond_118

    #@88
    .line 475
    new-instance v16, Landroid/content/Intent;

    #@8a
    const-string v21, "android.intent.action.MAIN"

    #@8c
    const/16 v22, 0x0

    #@8e
    move-object/from16 v0, v16

    #@90
    move-object/from16 v1, v21

    #@92
    move-object/from16 v2, v22

    #@94
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@97
    .line 476
    .local v16, mainIntent:Landroid/content/Intent;
    const-string v21, "android.intent.category.LAUNCHER"

    #@99
    move-object/from16 v0, v16

    #@9b
    move-object/from16 v1, v21

    #@9d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@a0
    .line 477
    move-object/from16 v0, p1

    #@a2
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@a4
    move-object/from16 v21, v0

    #@a6
    move-object/from16 v0, v16

    #@a8
    move-object/from16 v1, v21

    #@aa
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@ad
    .line 478
    move-object/from16 v0, p0

    #@af
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@b1
    move-object/from16 v21, v0

    #@b3
    const/16 v22, 0x200

    #@b5
    move-object/from16 v0, v21

    #@b7
    move-object/from16 v1, v16

    #@b9
    move/from16 v2, v22

    #@bb
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@be
    move-result-object v6

    #@bf
    .line 480
    .local v6, apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v6, :cond_118

    #@c1
    .line 481
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c4
    move-result-object v10

    #@c5
    .local v10, i$:Ljava/util/Iterator;
    :cond_c5
    :goto_c5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@c8
    move-result v21

    #@c9
    if-eqz v21, :cond_118

    #@cb
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ce
    move-result-object v20

    #@cf
    check-cast v20, Landroid/content/pm/ResolveInfo;

    #@d1
    .line 482
    .local v20, ri:Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v20

    #@d3
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@d5
    .line 483
    .local v4, activityInfo:Landroid/content/pm/ActivityInfo;
    invoke-virtual {v4}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    #@d8
    move-result v11

    #@d9
    .line 484
    .local v11, id:I
    if-eqz v11, :cond_c5

    #@db
    .line 487
    iget-object v0, v4, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@dd
    move-object/from16 v19, v0

    #@df
    .line 488
    .local v19, packageName:Ljava/lang/String;
    iget-object v0, v4, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@e1
    move-object/from16 v18, v0

    #@e3
    .line 489
    .local v18, name:Ljava/lang/String;
    if-eqz v19, :cond_114

    #@e5
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->hashCode()I

    #@e8
    move-result v9

    #@e9
    .line 490
    .local v9, hashCode:I
    :goto_e9
    if-eqz v18, :cond_f3

    #@eb
    .line 491
    mul-int/lit8 v21, v9, 0x1f

    #@ed
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->hashCode()I

    #@f0
    move-result v22

    #@f1
    add-int v9, v21, v22

    #@f3
    .line 493
    :cond_f3
    if-gez v9, :cond_f6

    #@f5
    .line 494
    neg-int v9, v9

    #@f6
    .line 496
    :cond_f6
    move-object/from16 v0, v17

    #@f8
    iget-object v0, v0, Landroid/content/thm/ThemeIconRedirectionMap;->backgroundMap:Landroid/util/SparseArray;

    #@fa
    move-object/from16 v21, v0

    #@fc
    new-instance v22, Ljava/lang/Integer;

    #@fe
    iget-object v0, v12, Landroid/content/thm/ThemePackageInfo;->bgIcons:[I

    #@100
    move-object/from16 v23, v0

    #@102
    rem-int v24, v9, v3

    #@104
    aget v23, v23, v24

    #@106
    invoke-direct/range {v22 .. v23}, Ljava/lang/Integer;-><init>(I)V

    #@109
    move-object/from16 v0, v21

    #@10b
    move-object/from16 v1, v22

    #@10d
    invoke-virtual {v0, v11, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@110
    goto :goto_c5

    #@111
    .line 473
    .end local v3           #N:I
    .end local v4           #activityInfo:Landroid/content/pm/ActivityInfo;
    .end local v6           #apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v9           #hashCode:I
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v11           #id:I
    .end local v16           #mainIntent:Landroid/content/Intent;
    .end local v18           #name:Ljava/lang/String;
    .end local v19           #packageName:Ljava/lang/String;
    .end local v20           #ri:Landroid/content/pm/ResolveInfo;
    :cond_111
    const/4 v3, 0x0

    #@112
    goto/16 :goto_86

    #@114
    .line 489
    .restart local v3       #N:I
    .restart local v4       #activityInfo:Landroid/content/pm/ActivityInfo;
    .restart local v6       #apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v10       #i$:Ljava/util/Iterator;
    .restart local v11       #id:I
    .restart local v16       #mainIntent:Landroid/content/Intent;
    .restart local v18       #name:Ljava/lang/String;
    .restart local v19       #packageName:Ljava/lang/String;
    .restart local v20       #ri:Landroid/content/pm/ResolveInfo;
    :cond_114
    const/4 v9, 0x0

    #@115
    goto :goto_e9

    #@116
    .line 502
    .end local v3           #N:I
    .end local v4           #activityInfo:Landroid/content/pm/ActivityInfo;
    .end local v6           #apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v11           #id:I
    .end local v13           #items:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    .end local v16           #mainIntent:Landroid/content/Intent;
    .end local v17           #map:Landroid/content/thm/ThemeIconRedirectionMap;
    .end local v18           #name:Ljava/lang/String;
    .end local v19           #packageName:Ljava/lang/String;
    .end local v20           #ri:Landroid/content/pm/ResolveInfo;
    :cond_116
    const/16 v17, 0x0

    #@118
    :cond_118
    return-object v17
.end method

.method private generateThemePackageInfo(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;
    .registers 30
    .parameter "themePackageName"

    #@0
    .prologue
    .line 320
    if-nez p1, :cond_5

    #@2
    .line 321
    const/16 v20, 0x0

    #@4
    .line 426
    :goto_4
    return-object v20

    #@5
    .line 324
    :cond_5
    move-object/from16 v0, p0

    #@7
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService;->mContext:Landroid/content/Context;

    #@9
    move-object/from16 v25, v0

    #@b
    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e
    move-result-object v9

    #@f
    .line 325
    .local v9, ctxRes:Landroid/content/res/Resources;
    if-nez v9, :cond_14

    #@11
    .line 326
    const/16 v20, 0x0

    #@13
    goto :goto_4

    #@14
    .line 329
    :cond_14
    const/16 v23, 0x0

    #@16
    .line 331
    .local v23, themeInfo:Landroid/content/pm/ApplicationInfo;
    :try_start_16
    move-object/from16 v0, p0

    #@18
    iget-object v0, v0, Lcom/android/server/thm/ThemeIconManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@1a
    move-object/from16 v25, v0

    #@1c
    const/16 v26, 0x0

    #@1e
    move-object/from16 v0, v25

    #@20
    move-object/from16 v1, p1

    #@22
    move/from16 v2, v26

    #@24
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_27
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_16 .. :try_end_27} :catch_2d

    #@27
    move-result-object v23

    #@28
    .line 335
    :goto_28
    if-nez v23, :cond_31

    #@2a
    .line 336
    const/16 v20, 0x0

    #@2c
    goto :goto_4

    #@2d
    .line 332
    :catch_2d
    move-exception v10

    #@2e
    .line 333
    .local v10, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v23, 0x0

    #@30
    goto :goto_28

    #@31
    .line 339
    .end local v10           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_31
    new-instance v6, Landroid/content/res/AssetManager;

    #@33
    invoke-direct {v6}, Landroid/content/res/AssetManager;-><init>()V

    #@36
    .line 340
    .local v6, assets:Landroid/content/res/AssetManager;
    move-object/from16 v0, v23

    #@38
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@3a
    move-object/from16 v25, v0

    #@3c
    move-object/from16 v0, v25

    #@3e
    invoke-virtual {v6, v0}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@41
    move-result v8

    #@42
    .line 341
    .local v8, cookie:I
    if-nez v8, :cond_68

    #@44
    .line 342
    const-string v25, "ThemeManagerService"

    #@46
    new-instance v26, Ljava/lang/StringBuilder;

    #@48
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v27, "Failed adding asset path: "

    #@4d
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v26

    #@51
    move-object/from16 v0, v23

    #@53
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@55
    move-object/from16 v27, v0

    #@57
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v26

    #@5b
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v26

    #@5f
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 343
    invoke-virtual {v6}, Landroid/content/res/AssetManager;->close()V

    #@65
    .line 344
    const/16 v20, 0x0

    #@67
    goto :goto_4

    #@68
    .line 347
    :cond_68
    new-instance v18, Landroid/content/res/Resources;

    #@6a
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@6d
    move-result-object v25

    #@6e
    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@71
    move-result-object v26

    #@72
    move-object/from16 v0, v18

    #@74
    move-object/from16 v1, v25

    #@76
    move-object/from16 v2, v26

    #@78
    invoke-direct {v0, v6, v1, v2}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    #@7b
    .line 349
    .local v18, res:Landroid/content/res/Resources;
    const-string v25, "theme_resources"

    #@7d
    const-string v26, "xml"

    #@7f
    move-object/from16 v0, v23

    #@81
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@83
    move-object/from16 v27, v0

    #@85
    move-object/from16 v0, v18

    #@87
    move-object/from16 v1, v25

    #@89
    move-object/from16 v2, v26

    #@8b
    move-object/from16 v3, v27

    #@8d
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@90
    move-result v24

    #@91
    .line 350
    .local v24, theme_resid:I
    if-nez v24, :cond_a1

    #@93
    .line 351
    const-string v25, "ThemeManagerService"

    #@95
    const-string v26, "theme_resources.xml not found"

    #@97
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 352
    invoke-virtual {v6}, Landroid/content/res/AssetManager;->close()V

    #@9d
    .line 353
    const/16 v20, 0x0

    #@9f
    goto/16 :goto_4

    #@a1
    .line 356
    :cond_a1
    new-instance v20, Landroid/content/thm/ThemePackageInfo;

    #@a3
    invoke-direct/range {v20 .. v20}, Landroid/content/thm/ThemePackageInfo;-><init>()V

    #@a6
    .line 357
    .local v20, result:Landroid/content/thm/ThemePackageInfo;
    move-object/from16 v0, v23

    #@a8
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@aa
    move-object/from16 v25, v0

    #@ac
    move-object/from16 v0, v25

    #@ae
    move-object/from16 v1, v20

    #@b0
    iput-object v0, v1, Landroid/content/thm/ThemePackageInfo;->packageName:Ljava/lang/String;

    #@b2
    .line 358
    move-object/from16 v0, v23

    #@b4
    move-object/from16 v1, v20

    #@b6
    iput-object v0, v1, Landroid/content/thm/ThemePackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b8
    .line 359
    const/16 v25, 0x0

    #@ba
    move/from16 v0, v25

    #@bc
    move-object/from16 v1, v20

    #@be
    iput v0, v1, Landroid/content/thm/ThemePackageInfo;->heightAlpha:I

    #@c0
    .line 360
    const v25, 0x3f4ccccd

    #@c3
    move/from16 v0, v25

    #@c5
    move-object/from16 v1, v20

    #@c7
    iput v0, v1, Landroid/content/thm/ThemePackageInfo;->resizeRate:F

    #@c9
    .line 362
    new-instance v7, Ljava/util/ArrayList;

    #@cb
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@ce
    .line 364
    .local v7, backgroundList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v16, 0x0

    #@d0
    .line 366
    .local v16, parser:Landroid/content/res/XmlResourceParser;
    :try_start_d0
    move-object/from16 v0, v18

    #@d2
    move/from16 v1, v24

    #@d4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
    :try_end_d7
    .catchall {:try_start_d0 .. :try_end_d7} :catchall_1fe
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_d7} :catch_1b7

    #@d7
    move-result-object v16

    #@d8
    .line 367
    if-nez v16, :cond_e6

    #@da
    .line 368
    const/16 v20, 0x0

    #@dc
    .line 421
    .end local v20           #result:Landroid/content/thm/ThemePackageInfo;
    if-eqz v16, :cond_e1

    #@de
    .line 422
    invoke-interface/range {v16 .. v16}, Landroid/content/res/XmlResourceParser;->close()V

    #@e1
    .line 424
    :cond_e1
    :goto_e1
    invoke-virtual {v6}, Landroid/content/res/AssetManager;->close()V

    #@e4
    goto/16 :goto_4

    #@e6
    .line 370
    .restart local v20       #result:Landroid/content/thm/ThemePackageInfo;
    :cond_e6
    :try_start_e6
    invoke-interface/range {v16 .. v16}, Landroid/content/res/XmlResourceParser;->getEventType()I

    #@e9
    move-result v11

    #@ea
    .line 371
    .local v11, eventType:I
    :goto_ea
    const/16 v25, 0x1

    #@ec
    move/from16 v0, v25

    #@ee
    if-eq v11, v0, :cond_1cf

    #@f0
    .line 372
    const/16 v25, 0x2

    #@f2
    move/from16 v0, v25

    #@f4
    if-ne v11, v0, :cond_171

    #@f6
    .line 373
    invoke-interface/range {v16 .. v16}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@f9
    move-result-object v21

    #@fa
    .line 375
    .local v21, tagName:Ljava/lang/String;
    const-string v25, "AppIcon"

    #@fc
    move-object/from16 v0, v25

    #@fe
    move-object/from16 v1, v21

    #@100
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@103
    move-result v25

    #@104
    if-eqz v25, :cond_177

    #@106
    .line 376
    const/16 v25, 0x0

    #@108
    const-string v26, "name"

    #@10a
    move-object/from16 v0, v16

    #@10c
    move-object/from16 v1, v25

    #@10e
    move-object/from16 v2, v26

    #@110
    invoke-interface {v0, v1, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@113
    move-result-object v15

    #@114
    .line 377
    .local v15, nameValue:Ljava/lang/String;
    const/16 v25, 0x0

    #@116
    const-string v26, "image"

    #@118
    move-object/from16 v0, v16

    #@11a
    move-object/from16 v1, v25

    #@11c
    move-object/from16 v2, v26

    #@11e
    invoke-interface {v0, v1, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@121
    move-result-object v13

    #@122
    .line 379
    .local v13, imageValue:Ljava/lang/String;
    if-eqz v15, :cond_171

    #@124
    if-eqz v13, :cond_171

    #@126
    .line 380
    invoke-static {v15}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@129
    move-result-object v5

    #@12a
    .line 381
    .local v5, actComp:Landroid/content/ComponentName;
    if-eqz v5, :cond_171

    #@12c
    .line 382
    const-string v25, "drawable"

    #@12e
    move-object/from16 v0, v23

    #@130
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@132
    move-object/from16 v26, v0

    #@134
    move-object/from16 v0, v18

    #@136
    move-object/from16 v1, v25

    #@138
    move-object/from16 v2, v26

    #@13a
    invoke-virtual {v0, v13, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    move-result v22

    #@13e
    .line 384
    .local v22, themeIcon:I
    if-eqz v22, :cond_171

    #@140
    .line 385
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@143
    move-result-object v17

    #@144
    .line 386
    .local v17, pkgName:Ljava/lang/String;
    move-object/from16 v0, v20

    #@146
    iget-object v0, v0, Landroid/content/thm/ThemePackageInfo;->componentToResMap:Ljava/util/HashMap;

    #@148
    move-object/from16 v25, v0

    #@14a
    move-object/from16 v0, v25

    #@14c
    move-object/from16 v1, v17

    #@14e
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@151
    move-result-object v14

    #@152
    check-cast v14, Ljava/util/HashMap;

    #@154
    .line 387
    .local v14, item:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    if-nez v14, :cond_168

    #@156
    .line 388
    new-instance v14, Ljava/util/HashMap;

    #@158
    .end local v14           #item:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    #@15b
    .line 389
    .restart local v14       #item:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    move-object/from16 v0, v20

    #@15d
    iget-object v0, v0, Landroid/content/thm/ThemePackageInfo;->componentToResMap:Ljava/util/HashMap;

    #@15f
    move-object/from16 v25, v0

    #@161
    move-object/from16 v0, v25

    #@163
    move-object/from16 v1, v17

    #@165
    invoke-virtual {v0, v1, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@168
    .line 391
    :cond_168
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16b
    move-result-object v25

    #@16c
    move-object/from16 v0, v25

    #@16e
    invoke-virtual {v14, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@171
    .line 404
    .end local v5           #actComp:Landroid/content/ComponentName;
    .end local v13           #imageValue:Ljava/lang/String;
    .end local v14           #item:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    .end local v15           #nameValue:Ljava/lang/String;
    .end local v17           #pkgName:Ljava/lang/String;
    .end local v21           #tagName:Ljava/lang/String;
    .end local v22           #themeIcon:I
    :cond_171
    :goto_171
    invoke-interface/range {v16 .. v16}, Landroid/content/res/XmlResourceParser;->next()I

    #@174
    move-result v11

    #@175
    goto/16 :goto_ea

    #@177
    .line 395
    .restart local v21       #tagName:Ljava/lang/String;
    :cond_177
    move-object/from16 v0, p0

    #@179
    iget-boolean v0, v0, Lcom/android/server/thm/ThemeIconManagerService;->mBackgroundEnabled:Z

    #@17b
    move/from16 v25, v0

    #@17d
    if-eqz v25, :cond_171

    #@17f
    const-string v25, "AppIconBG"

    #@181
    move-object/from16 v0, v25

    #@183
    move-object/from16 v1, v21

    #@185
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@188
    move-result v25

    #@189
    if-eqz v25, :cond_171

    #@18b
    .line 396
    const/16 v25, 0x0

    #@18d
    const-string v26, "image"

    #@18f
    move-object/from16 v0, v16

    #@191
    move-object/from16 v1, v25

    #@193
    move-object/from16 v2, v26

    #@195
    invoke-interface {v0, v1, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@198
    move-result-object v13

    #@199
    .line 397
    .restart local v13       #imageValue:Ljava/lang/String;
    const-string v25, "drawable"

    #@19b
    move-object/from16 v0, v23

    #@19d
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@19f
    move-object/from16 v26, v0

    #@1a1
    move-object/from16 v0, v18

    #@1a3
    move-object/from16 v1, v25

    #@1a5
    move-object/from16 v2, v26

    #@1a7
    invoke-virtual {v0, v13, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@1aa
    move-result v19

    #@1ab
    .line 399
    .local v19, resid:I
    if-eqz v19, :cond_171

    #@1ad
    .line 400
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b0
    move-result-object v25

    #@1b1
    move-object/from16 v0, v25

    #@1b3
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1b6
    .catchall {:try_start_e6 .. :try_end_1b6} :catchall_1fe
    .catch Ljava/lang/Exception; {:try_start_e6 .. :try_end_1b6} :catch_1b7

    #@1b6
    goto :goto_171

    #@1b7
    .line 418
    .end local v11           #eventType:I
    .end local v13           #imageValue:Ljava/lang/String;
    .end local v19           #resid:I
    .end local v21           #tagName:Ljava/lang/String;
    :catch_1b7
    move-exception v10

    #@1b8
    .line 419
    .local v10, e:Ljava/lang/Exception;
    :try_start_1b8
    const-string v25, "ThemeManagerService"

    #@1ba
    const-string v26, "Got exception while loading theme resources"

    #@1bc
    move-object/from16 v0, v25

    #@1be
    move-object/from16 v1, v26

    #@1c0
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1c3
    .catchall {:try_start_1b8 .. :try_end_1c3} :catchall_1fe

    #@1c3
    .line 421
    if-eqz v16, :cond_1c8

    #@1c5
    .line 422
    invoke-interface/range {v16 .. v16}, Landroid/content/res/XmlResourceParser;->close()V

    #@1c8
    .line 424
    :cond_1c8
    invoke-virtual {v6}, Landroid/content/res/AssetManager;->close()V

    #@1cb
    .line 426
    const/16 v20, 0x0

    #@1cd
    goto/16 :goto_4

    #@1cf
    .line 407
    .end local v10           #e:Ljava/lang/Exception;
    .restart local v11       #eventType:I
    :cond_1cf
    :try_start_1cf
    const-string v25, "ThemeManagerService"

    #@1d1
    const-string v26, "ThemePackageInfo loaded."

    #@1d3
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d6
    .line 409
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@1d9
    move-result v4

    #@1da
    .line 410
    .local v4, N:I
    if-lez v4, :cond_208

    #@1dc
    .line 411
    new-array v0, v4, [I

    #@1de
    move-object/from16 v25, v0

    #@1e0
    move-object/from16 v0, v25

    #@1e2
    move-object/from16 v1, v20

    #@1e4
    iput-object v0, v1, Landroid/content/thm/ThemePackageInfo;->bgIcons:[I

    #@1e6
    .line 412
    const/4 v12, 0x0

    #@1e7
    .local v12, i:I
    :goto_1e7
    if-ge v12, v4, :cond_208

    #@1e9
    .line 413
    move-object/from16 v0, v20

    #@1eb
    iget-object v0, v0, Landroid/content/thm/ThemePackageInfo;->bgIcons:[I

    #@1ed
    move-object/from16 v26, v0

    #@1ef
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f2
    move-result-object v25

    #@1f3
    check-cast v25, Ljava/lang/Integer;

    #@1f5
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    #@1f8
    move-result v25

    #@1f9
    aput v25, v26, v12
    :try_end_1fb
    .catchall {:try_start_1cf .. :try_end_1fb} :catchall_1fe
    .catch Ljava/lang/Exception; {:try_start_1cf .. :try_end_1fb} :catch_1b7

    #@1fb
    .line 412
    add-int/lit8 v12, v12, 0x1

    #@1fd
    goto :goto_1e7

    #@1fe
    .line 421
    .end local v4           #N:I
    .end local v11           #eventType:I
    .end local v12           #i:I
    :catchall_1fe
    move-exception v25

    #@1ff
    if-eqz v16, :cond_204

    #@201
    .line 422
    invoke-interface/range {v16 .. v16}, Landroid/content/res/XmlResourceParser;->close()V

    #@204
    .line 424
    :cond_204
    invoke-virtual {v6}, Landroid/content/res/AssetManager;->close()V

    #@207
    .line 421
    throw v25

    #@208
    .restart local v4       #N:I
    .restart local v11       #eventType:I
    :cond_208
    if-eqz v16, :cond_e1

    #@20a
    .line 422
    invoke-interface/range {v16 .. v16}, Landroid/content/res/XmlResourceParser;->close()V

    #@20d
    goto/16 :goto_e1
.end method

.method public static getConfiguration(Landroid/content/Context;Landroid/content/res/Configuration;)V
    .registers 8
    .parameter "context"
    .parameter "config"

    #@0
    .prologue
    .line 176
    if-eqz p0, :cond_4

    #@2
    if-nez p1, :cond_5

    #@4
    .line 199
    :cond_4
    :goto_4
    return-void

    #@5
    .line 180
    :cond_5
    const-string v4, ""

    #@7
    iput-object v4, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@9
    .line 182
    const-string v4, "persist.sys.theme"

    #@b
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    .line 184
    .local v3, themePackage:Ljava/lang/String;
    if-eqz v3, :cond_17

    #@11
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_23

    #@17
    .line 185
    :cond_17
    invoke-static {p0}, Lcom/android/server/thm/ThemeIconManagerService;->getDefaultThemePackage(Landroid/content/Context;)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 186
    .local v1, defaultThemePackage:Ljava/lang/String;
    const-string v4, "persist.sys.theme"

    #@1d
    invoke-static {v4, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 187
    iput-object v1, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@22
    goto :goto_4

    #@23
    .line 190
    .end local v1           #defaultThemePackage:Ljava/lang/String;
    :cond_23
    :try_start_23
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@26
    move-result-object v4

    #@27
    const/4 v5, 0x0

    #@28
    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@2b
    move-result-object v0

    #@2c
    .line 191
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    iput-object v3, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_4

    #@2f
    .line 192
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    :catch_2f
    move-exception v2

    #@30
    .line 194
    .local v2, e:Ljava/lang/Exception;
    invoke-static {p0}, Lcom/android/server/thm/ThemeIconManagerService;->getDefaultThemePackage(Landroid/content/Context;)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    .line 195
    .restart local v1       #defaultThemePackage:Ljava/lang/String;
    const-string v4, "persist.sys.theme"

    #@36
    invoke-static {v4, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 196
    iput-object v1, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@3b
    goto :goto_4
.end method

.method private static getDefaultThemePackage(Landroid/content/Context;)Ljava/lang/String;
    .registers 15
    .parameter "context"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 112
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v5

    #@6
    .line 113
    .local v5, pm:Landroid/content/pm/PackageManager;
    sget-object v2, Lcom/android/server/thm/ThemeIconManagerService;->DEFAULT_THEME_PACKAGE:Ljava/lang/String;

    #@8
    .line 114
    .local v2, defaultTheme:Ljava/lang/String;
    const/4 v1, 0x0

    #@9
    .line 118
    .local v1, candidate:Ljava/lang/String;
    sget-object v9, Lcom/android/server/thm/ThemeIconManagerService;->DEFAULT_THEME_PACKAGE:Ljava/lang/String;

    #@b
    invoke-static {p0, v9}, Lcom/android/server/thm/ThemeIconManagerService;->getResourcesForApplication(Landroid/content/Context;Ljava/lang/String;)Landroid/content/res/Resources;

    #@e
    move-result-object v6

    #@f
    .line 119
    .local v6, res:Landroid/content/res/Resources;
    if-eqz v6, :cond_37

    #@11
    .line 120
    const-string v9, "config_feature_default_theme"

    #@13
    const-string v10, "array"

    #@15
    sget-object v11, Lcom/android/server/thm/ThemeIconManagerService;->DEFAULT_THEME_PACKAGE:Ljava/lang/String;

    #@17
    invoke-virtual {v6, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    move-result v4

    #@1b
    .line 122
    .local v4, id:I
    if-eqz v4, :cond_28

    #@1d
    .line 123
    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@20
    move-result-object v8

    #@21
    .line 124
    .local v8, themePackages:[Ljava/lang/String;
    if-eqz v8, :cond_28

    #@23
    .line 125
    array-length v9, v8

    #@24
    if-ne v9, v13, :cond_3a

    #@26
    .line 126
    aget-object v1, v8, v12

    #@28
    .line 139
    .end local v8           #themePackages:[Ljava/lang/String;
    :cond_28
    :goto_28
    invoke-virtual {v6}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@2b
    move-result-object v0

    #@2c
    .line 140
    .local v0, assets:Landroid/content/res/AssetManager;
    if-eqz v0, :cond_31

    #@2e
    .line 141
    invoke-virtual {v0}, Landroid/content/res/AssetManager;->close()V

    #@31
    .line 145
    :cond_31
    if-eqz v1, :cond_37

    #@33
    .line 147
    const/4 v9, 0x0

    #@34
    :try_start_34
    invoke-virtual {v5, v1, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_37
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_34 .. :try_end_37} :catch_4e

    #@37
    .line 154
    .end local v0           #assets:Landroid/content/res/AssetManager;
    .end local v4           #id:I
    :cond_37
    :goto_37
    if-eqz v1, :cond_51

    #@39
    .line 165
    .end local v1           #candidate:Ljava/lang/String;
    :goto_39
    return-object v1

    #@3a
    .line 127
    .restart local v1       #candidate:Ljava/lang/String;
    .restart local v4       #id:I
    .restart local v8       #themePackages:[Ljava/lang/String;
    :cond_3a
    array-length v9, v8

    #@3b
    if-le v9, v13, :cond_28

    #@3d
    .line 129
    :try_start_3d
    const-string v9, "sys.theme"

    #@3f
    const/4 v10, 0x0

    #@40
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@43
    move-result v7

    #@44
    .line 130
    .local v7, sysTheme:I
    if-lez v7, :cond_28

    #@46
    array-length v9, v8

    #@47
    if-gt v7, v9, :cond_28

    #@49
    .line 131
    add-int/lit8 v9, v7, -0x1

    #@4b
    aget-object v1, v8, v9
    :try_end_4d
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_4d} :catch_5b

    #@4d
    goto :goto_28

    #@4e
    .line 148
    .end local v7           #sysTheme:I
    .end local v8           #themePackages:[Ljava/lang/String;
    .restart local v0       #assets:Landroid/content/res/AssetManager;
    :catch_4e
    move-exception v3

    #@4f
    .line 149
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    #@50
    goto :goto_37

    #@51
    .line 160
    .end local v0           #assets:Landroid/content/res/AssetManager;
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4           #id:I
    :cond_51
    const/4 v9, 0x0

    #@52
    :try_start_52
    invoke-virtual {v5, v2, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_55
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_52 .. :try_end_55} :catch_57

    #@55
    :goto_55
    move-object v1, v2

    #@56
    .line 165
    goto :goto_39

    #@57
    .line 161
    :catch_57
    move-exception v3

    #@58
    .line 162
    .restart local v3       #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, ""

    #@5a
    goto :goto_55

    #@5b
    .line 133
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4       #id:I
    .restart local v8       #themePackages:[Ljava/lang/String;
    :catch_5b
    move-exception v9

    #@5c
    goto :goto_28
.end method

.method private static getResourcesForApplication(Landroid/content/Context;Ljava/lang/String;)Landroid/content/res/Resources;
    .registers 10
    .parameter "context"
    .parameter "packageName"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 81
    if-eqz p0, :cond_5

    #@3
    if-nez p1, :cond_6

    #@5
    .line 107
    :cond_5
    :goto_5
    return-object v5

    #@6
    .line 85
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v4

    #@a
    .line 86
    .local v4, res:Landroid/content/res/Resources;
    if-eqz v4, :cond_5

    #@c
    .line 90
    const/4 v0, 0x0

    #@d
    .line 92
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    :try_start_d
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@10
    move-result-object v6

    #@11
    const/4 v7, 0x0

    #@12
    invoke-virtual {v6, p1, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_15} :catch_29

    #@15
    move-result-object v0

    #@16
    .line 96
    :goto_16
    if-eqz v0, :cond_5

    #@18
    .line 100
    new-instance v1, Landroid/content/res/AssetManager;

    #@1a
    invoke-direct {v1}, Landroid/content/res/AssetManager;-><init>()V

    #@1d
    .line 101
    .local v1, assets:Landroid/content/res/AssetManager;
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v6}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@22
    move-result v2

    #@23
    .line 102
    .local v2, cookie:I
    if-nez v2, :cond_2c

    #@25
    .line 103
    invoke-virtual {v1}, Landroid/content/res/AssetManager;->close()V

    #@28
    goto :goto_5

    #@29
    .line 93
    .end local v1           #assets:Landroid/content/res/AssetManager;
    .end local v2           #cookie:I
    :catch_29
    move-exception v3

    #@2a
    .line 94
    .local v3, e:Ljava/lang/Exception;
    const/4 v0, 0x0

    #@2b
    goto :goto_16

    #@2c
    .line 107
    .end local v3           #e:Ljava/lang/Exception;
    .restart local v1       #assets:Landroid/content/res/AssetManager;
    .restart local v2       #cookie:I
    :cond_2c
    new-instance v5, Landroid/content/res/Resources;

    #@2e
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@35
    move-result-object v7

    #@36
    invoke-direct {v5, v1, v6, v7}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    #@39
    goto :goto_5
.end method

.method private getThemePackageInfoLocked(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;
    .registers 5
    .parameter "themePackageName"

    #@0
    .prologue
    .line 300
    iget-object v2, p0, Lcom/android/server/thm/ThemeIconManagerService;->mThemePackageInfoMap:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/thm/ThemePackageInfo;

    #@8
    .line 301
    .local v0, info:Landroid/content/thm/ThemePackageInfo;
    if-eqz v0, :cond_c

    #@a
    move-object v1, v0

    #@b
    .line 308
    .end local v0           #info:Landroid/content/thm/ThemePackageInfo;
    .local v1, info:Landroid/content/thm/ThemePackageInfo;
    :goto_b
    return-object v1

    #@c
    .line 304
    .end local v1           #info:Landroid/content/thm/ThemePackageInfo;
    .restart local v0       #info:Landroid/content/thm/ThemePackageInfo;
    :cond_c
    invoke-direct {p0, p1}, Lcom/android/server/thm/ThemeIconManagerService;->generateThemePackageInfo(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;

    #@f
    move-result-object v0

    #@10
    .line 305
    if-eqz v0, :cond_17

    #@12
    .line 306
    iget-object v2, p0, Lcom/android/server/thm/ThemeIconManagerService;->mThemePackageInfoMap:Ljava/util/LinkedHashMap;

    #@14
    invoke-virtual {v2, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    :cond_17
    move-object v1, v0

    #@18
    .line 308
    .end local v0           #info:Landroid/content/thm/ThemePackageInfo;
    .restart local v1       #info:Landroid/content/thm/ThemePackageInfo;
    goto :goto_b
.end method

.method public static putConfiguration(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 169
    if-nez p0, :cond_3

    #@2
    .line 173
    :goto_2
    return-void

    #@3
    .line 172
    :cond_3
    const-string v0, "persist.sys.theme"

    #@5
    iget-object v1, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@7
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    goto :goto_2
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 11
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 507
    iget-object v5, p0, Lcom/android/server/thm/ThemeIconManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v6, "android.permission.DUMP"

    #@4
    invoke-virtual {v5, v6}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_3f

    #@a
    .line 509
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "Permission Denial: can\'t dump ThemeManager from from pid="

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v6

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    const-string v6, ", uid="

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v6

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    const-string v6, " without permission "

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    const-string v6, "android.permission.DUMP"

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e
    .line 546
    :goto_3e
    return-void

    #@3f
    .line 516
    :cond_3f
    iget-object v6, p0, Lcom/android/server/thm/ThemeIconManagerService;->mLock:Ljava/lang/Object;

    #@41
    monitor-enter v6

    #@42
    .line 517
    :try_start_42
    const-string v5, "mBackgroundEnabled:"

    #@44
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@47
    .line 518
    const-string v5, "    "

    #@49
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    .line 519
    iget-boolean v5, p0, Lcom/android/server/thm/ThemeIconManagerService;->mBackgroundEnabled:Z

    #@4e
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Z)V

    #@51
    .line 521
    const-string v5, "mThemePackageInfoMap:"

    #@53
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@56
    .line 522
    iget-object v5, p0, Lcom/android/server/thm/ThemeIconManagerService;->mThemePackageInfoMap:Ljava/util/LinkedHashMap;

    #@58
    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->size()I

    #@5b
    move-result v5

    #@5c
    if-lez v5, :cond_80

    #@5e
    .line 523
    iget-object v5, p0, Lcom/android/server/thm/ThemeIconManagerService;->mThemePackageInfoMap:Ljava/util/LinkedHashMap;

    #@60
    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    #@63
    move-result-object v4

    #@64
    .line 524
    .local v4, keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@67
    move-result-object v1

    #@68
    .line 525
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_68
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@6b
    move-result v5

    #@6c
    if-eqz v5, :cond_80

    #@6e
    .line 526
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@71
    move-result-object v2

    #@72
    check-cast v2, Ljava/lang/String;

    #@74
    .line 527
    .local v2, key:Ljava/lang/String;
    const-string v5, "    "

    #@76
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@79
    .line 528
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7c
    goto :goto_68

    #@7d
    .line 545
    .end local v1           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v2           #key:Ljava/lang/String;
    .end local v4           #keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_7d
    move-exception v5

    #@7e
    monitor-exit v6
    :try_end_7f
    .catchall {:try_start_42 .. :try_end_7f} :catchall_7d

    #@7f
    throw v5

    #@80
    .line 531
    :cond_80
    :try_start_80
    const-string v5, "mRedirectionMap:"

    #@82
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@85
    .line 532
    iget-object v5, p0, Lcom/android/server/thm/ThemeIconManagerService;->mRedirectionMap:Ljava/util/LinkedHashMap;

    #@87
    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->size()I

    #@8a
    move-result v5

    #@8b
    if-lez v5, :cond_c2

    #@8d
    .line 533
    iget-object v5, p0, Lcom/android/server/thm/ThemeIconManagerService;->mRedirectionMap:Ljava/util/LinkedHashMap;

    #@8f
    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    #@92
    move-result-object v3

    #@93
    .line 534
    .local v3, keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@96
    move-result-object v0

    #@97
    .line 535
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :goto_97
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9a
    move-result v5

    #@9b
    if-eqz v5, :cond_c2

    #@9d
    .line 536
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a0
    move-result-object v2

    #@a1
    check-cast v2, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;

    #@a3
    .line 537
    .local v2, key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    const-string v5, "    "

    #@a5
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a8
    .line 538
    const-string v5, "<"

    #@aa
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ad
    .line 539
    iget-object v5, v2, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@af
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b2
    .line 540
    const-string v5, ", "

    #@b4
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b7
    .line 541
    iget-object v5, v2, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@b9
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bc
    .line 542
    const-string v5, ">"

    #@be
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c1
    goto :goto_97

    #@c2
    .line 545
    .end local v0           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    .end local v2           #key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    .end local v3           #keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :cond_c2
    monitor-exit v6
    :try_end_c3
    .catchall {:try_start_80 .. :try_end_c3} :catchall_7d

    #@c3
    goto/16 :goto_3e
.end method

.method public getPackageRedirectionMap(Ljava/lang/String;Ljava/lang/String;)Landroid/content/thm/ThemeIconRedirectionMap;
    .registers 8
    .parameter "themePackageName"
    .parameter "targetPackageName"

    #@0
    .prologue
    .line 432
    iget-object v4, p0, Lcom/android/server/thm/ThemeIconManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 433
    :try_start_3
    new-instance v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;

    #@5
    invoke-direct {v0, p1, p2}, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 434
    .local v0, key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService;->mRedirectionMap:Ljava/util/LinkedHashMap;

    #@a
    invoke-virtual {v3, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/content/thm/ThemeIconRedirectionMap;

    #@10
    .line 435
    .local v1, map:Landroid/content/thm/ThemeIconRedirectionMap;
    if-eqz v1, :cond_15

    #@12
    .line 436
    monitor-exit v4

    #@13
    move-object v2, v1

    #@14
    .line 442
    .end local v1           #map:Landroid/content/thm/ThemeIconRedirectionMap;
    .local v2, map:Landroid/content/thm/ThemeIconRedirectionMap;
    :goto_14
    return-object v2

    #@15
    .line 438
    .end local v2           #map:Landroid/content/thm/ThemeIconRedirectionMap;
    .restart local v1       #map:Landroid/content/thm/ThemeIconRedirectionMap;
    :cond_15
    invoke-direct {p0, v0}, Lcom/android/server/thm/ThemeIconManagerService;->generatePackageRedirectionMapLocked(Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;)Landroid/content/thm/ThemeIconRedirectionMap;

    #@18
    move-result-object v1

    #@19
    .line 439
    if-eqz v1, :cond_20

    #@1b
    .line 440
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService;->mRedirectionMap:Ljava/util/LinkedHashMap;

    #@1d
    invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    .line 442
    :cond_20
    monitor-exit v4

    #@21
    move-object v2, v1

    #@22
    .end local v1           #map:Landroid/content/thm/ThemeIconRedirectionMap;
    .restart local v2       #map:Landroid/content/thm/ThemeIconRedirectionMap;
    goto :goto_14

    #@23
    .line 444
    .end local v0           #key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    .end local v2           #map:Landroid/content/thm/ThemeIconRedirectionMap;
    :catchall_23
    move-exception v3

    #@24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v3
.end method

.method public getThemePackageInfo(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;
    .registers 4
    .parameter "themePackageName"

    #@0
    .prologue
    .line 314
    iget-object v1, p0, Lcom/android/server/thm/ThemeIconManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 315
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/thm/ThemeIconManagerService;->getThemePackageInfoLocked(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;

    #@6
    move-result-object v0

    #@7
    monitor-exit v1

    #@8
    return-object v0

    #@9
    .line 316
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public removePackageRedirectionMap(Ljava/lang/String;)V
    .registers 7
    .parameter "targetPackageName"

    #@0
    .prologue
    .line 287
    iget-object v4, p0, Lcom/android/server/thm/ThemeIconManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 288
    :try_start_3
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService;->mRedirectionMap:Ljava/util/LinkedHashMap;

    #@5
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    #@8
    move-result-object v2

    #@9
    .line 289
    .local v2, keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v0

    #@d
    .line 290
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :cond_d
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_28

    #@13
    .line 291
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;

    #@19
    .line 292
    .local v1, key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    iget-object v3, v1, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@1b
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_d

    #@21
    .line 293
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@24
    goto :goto_d

    #@25
    .line 296
    .end local v0           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    .end local v1           #key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    .end local v2           #keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :catchall_25
    move-exception v3

    #@26
    monitor-exit v4
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_25

    #@27
    throw v3

    #@28
    .restart local v0       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    .restart local v2       #keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :cond_28
    :try_start_28
    monitor-exit v4
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_25

    #@29
    .line 297
    return-void
.end method

.method public removeThemePackage(Ljava/lang/String;)V
    .registers 7
    .parameter "themePackageName"

    #@0
    .prologue
    .line 270
    iget-object v4, p0, Lcom/android/server/thm/ThemeIconManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 271
    :try_start_3
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService;->mThemePackageInfoMap:Ljava/util/LinkedHashMap;

    #@5
    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_10

    #@b
    .line 272
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService;->mThemePackageInfoMap:Ljava/util/LinkedHashMap;

    #@d
    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 274
    :cond_10
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService;->mRedirectionMap:Ljava/util/LinkedHashMap;

    #@12
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    #@15
    move-result-object v2

    #@16
    .line 275
    .local v2, keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v0

    #@1a
    .line 276
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :cond_1a
    :goto_1a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_35

    #@20
    .line 277
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;

    #@26
    .line 278
    .local v1, key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    iget-object v3, v1, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@28
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_1a

    #@2e
    .line 279
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@31
    goto :goto_1a

    #@32
    .line 282
    .end local v0           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    .end local v1           #key:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    .end local v2           #keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :catchall_32
    move-exception v3

    #@33
    monitor-exit v4
    :try_end_34
    .catchall {:try_start_3 .. :try_end_34} :catchall_32

    #@34
    throw v3

    #@35
    .restart local v0       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    .restart local v2       #keys:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;>;"
    :cond_35
    :try_start_35
    monitor-exit v4
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_32

    #@36
    .line 283
    return-void
.end method
