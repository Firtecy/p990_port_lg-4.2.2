.class Lcom/android/server/thm/ThemeIconManagerService$2;
.super Ljava/util/LinkedHashMap;
.source "ThemeIconManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/thm/ThemeIconManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;",
        "Landroid/content/thm/ThemeIconRedirectionMap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/thm/ThemeIconManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/thm/ThemeIconManagerService;IFZ)V
    .registers 5
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 70
    iput-object p1, p0, Lcom/android/server/thm/ThemeIconManagerService$2;->this$0:Lcom/android/server/thm/ThemeIconManagerService;

    #@2
    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    #@5
    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;",
            "Landroid/content/thm/ThemeIconRedirectionMap;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 73
    .local p1, eldest:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;Landroid/content/thm/ThemeIconRedirectionMap;>;"
    invoke-virtual {p0}, Lcom/android/server/thm/ThemeIconManagerService$2;->size()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x32

    #@6
    if-le v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method
