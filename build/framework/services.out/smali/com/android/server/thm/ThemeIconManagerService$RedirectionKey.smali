.class final Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
.super Ljava/lang/Object;
.source "ThemeIconManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/thm/ThemeIconManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RedirectionKey"
.end annotation


# instance fields
.field final targetPackageName:Ljava/lang/String;

.field final themePackageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "themePackageName"
    .parameter "targetPackageName"

    #@0
    .prologue
    .line 552
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 553
    iput-object p1, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@5
    .line 554
    iput-object p2, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@7
    .line 555
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 559
    if-ne p0, p1, :cond_5

    #@4
    .line 582
    :cond_4
    :goto_4
    return v1

    #@5
    .line 561
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    .line 562
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 564
    check-cast v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;

    #@16
    .line 566
    .local v0, that:Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@18
    if-eqz v3, :cond_26

    #@1a
    .line 567
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@1c
    iget-object v4, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_2c

    #@24
    move v1, v2

    #@25
    .line 568
    goto :goto_4

    #@26
    .line 570
    :cond_26
    iget-object v3, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@28
    if-eqz v3, :cond_2c

    #@2a
    move v1, v2

    #@2b
    .line 571
    goto :goto_4

    #@2c
    .line 574
    :cond_2c
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@2e
    if-eqz v3, :cond_3c

    #@30
    .line 575
    iget-object v3, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@32
    iget-object v4, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v3

    #@38
    if-nez v3, :cond_4

    #@3a
    move v1, v2

    #@3b
    .line 576
    goto :goto_4

    #@3c
    .line 578
    :cond_3c
    iget-object v3, v0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@3e
    if-eqz v3, :cond_4

    #@40
    move v1, v2

    #@41
    .line 579
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 588
    iget-object v1, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->themePackageName:Ljava/lang/String;

    #@2
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    .line 589
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@8
    iget-object v2, p0, Lcom/android/server/thm/ThemeIconManagerService$RedirectionKey;->targetPackageName:Ljava/lang/String;

    #@a
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@d
    move-result v2

    #@e
    add-int v0, v1, v2

    #@10
    .line 590
    return v0
.end method
