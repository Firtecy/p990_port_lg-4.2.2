.class Lcom/android/server/thm/ThemeIconManagerService$3;
.super Landroid/content/BroadcastReceiver;
.source "ThemeIconManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/thm/ThemeIconManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/thm/ThemeIconManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/thm/ThemeIconManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 201
    iput-object p1, p0, Lcom/android/server/thm/ThemeIconManagerService$3;->this$0:Lcom/android/server/thm/ThemeIconManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 205
    if-nez p2, :cond_3

    #@2
    .line 231
    :cond_2
    :goto_2
    return-void

    #@3
    .line 207
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 209
    .local v0, action:Ljava/lang/String;
    const-string v5, "android.intent.action.PACKAGE_DATA_CLEARED"

    #@9
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_2

    #@f
    .line 210
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@12
    move-result-object v4

    #@13
    .line 211
    .local v4, uri:Landroid/net/Uri;
    if-eqz v4, :cond_2

    #@15
    .line 214
    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 215
    .local v3, pkgName:Ljava/lang/String;
    if-eqz v3, :cond_2

    #@1b
    .line 218
    const-string v5, "com.lge.launcher2"

    #@1d
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_2

    #@23
    .line 220
    :try_start_23
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@26
    move-result-object v1

    #@27
    .line 221
    .local v1, am:Landroid/app/IActivityManager;
    invoke-interface {v1}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@2a
    move-result-object v2

    #@2b
    .line 223
    .local v2, config:Landroid/content/res/Configuration;
    const-string v5, "persist.sys.theme"

    #@2d
    const-string v6, ""

    #@2f
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 224
    invoke-static {p1, v2}, Lcom/android/server/thm/ThemeIconManagerService;->getConfiguration(Landroid/content/Context;Landroid/content/res/Configuration;)V

    #@35
    .line 226
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_38} :catch_39

    #@38
    goto :goto_2

    #@39
    .line 227
    .end local v1           #am:Landroid/app/IActivityManager;
    .end local v2           #config:Landroid/content/res/Configuration;
    :catch_39
    move-exception v5

    #@3a
    goto :goto_2
.end method
