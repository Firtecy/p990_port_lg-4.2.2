.class Lcom/android/server/AlarmManagerService$ResultReceiver;
.super Ljava/lang/Object;
.source "AlarmManagerService.java"

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AlarmManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResultReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/AlarmManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/AlarmManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1138
    iput-object p1, p0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 25
    .parameter "pi"
    .parameter "intent"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "resultExtras"

    #@0
    .prologue
    .line 1141
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@4
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$600(Lcom/android/server/AlarmManagerService;)Ljava/lang/Object;

    #@7
    move-result-object v14

    #@8
    monitor-enter v14

    #@9
    .line 1142
    const/4 v7, 0x0

    #@a
    .line 1143
    .local v7, inflight:Lcom/android/server/AlarmManagerService$InFlight;
    const/4 v6, 0x0

    #@b
    .local v6, i:I
    :goto_b
    :try_start_b
    move-object/from16 v0, p0

    #@d
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@f
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@12
    move-result-object v13

    #@13
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v13

    #@17
    if-ge v6, v13, :cond_3d

    #@19
    .line 1144
    move-object/from16 v0, p0

    #@1b
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@1d
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@20
    move-result-object v13

    #@21
    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v13

    #@25
    check-cast v13, Lcom/android/server/AlarmManagerService$InFlight;

    #@27
    iget-object v13, v13, Lcom/android/server/AlarmManagerService$InFlight;->mPendingIntent:Landroid/app/PendingIntent;

    #@29
    move-object/from16 v0, p1

    #@2b
    if-ne v13, v0, :cond_fb

    #@2d
    .line 1145
    move-object/from16 v0, p0

    #@2f
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@31
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@34
    move-result-object v13

    #@35
    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@38
    move-result-object v13

    #@39
    move-object v0, v13

    #@3a
    check-cast v0, Lcom/android/server/AlarmManagerService$InFlight;

    #@3c
    move-object v7, v0

    #@3d
    .line 1149
    :cond_3d
    if-eqz v7, :cond_ff

    #@3f
    .line 1150
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@42
    move-result-wide v8

    #@43
    .line 1151
    .local v8, nowELAPSED:J
    iget-object v3, v7, Lcom/android/server/AlarmManagerService$InFlight;->mBroadcastStats:Lcom/android/server/AlarmManagerService$BroadcastStats;

    #@45
    .line 1152
    .local v3, bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    iget v13, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@47
    add-int/lit8 v13, v13, -0x1

    #@49
    iput v13, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@4b
    .line 1153
    iget v13, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@4d
    if-gtz v13, :cond_5e

    #@4f
    .line 1154
    const/4 v13, 0x0

    #@50
    iput v13, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->nesting:I

    #@52
    .line 1155
    iget-wide v15, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->aggregateTime:J

    #@54
    iget-wide v0, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->startTime:J

    #@56
    move-wide/from16 v17, v0

    #@58
    sub-long v17, v8, v17

    #@5a
    add-long v15, v15, v17

    #@5c
    iput-wide v15, v3, Lcom/android/server/AlarmManagerService$BroadcastStats;->aggregateTime:J

    #@5e
    .line 1157
    :cond_5e
    iget-object v5, v7, Lcom/android/server/AlarmManagerService$InFlight;->mFilterStats:Lcom/android/server/AlarmManagerService$FilterStats;

    #@60
    .line 1158
    .local v5, fs:Lcom/android/server/AlarmManagerService$FilterStats;
    iget v13, v5, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@62
    add-int/lit8 v13, v13, -0x1

    #@64
    iput v13, v5, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@66
    .line 1159
    iget v13, v5, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@68
    if-gtz v13, :cond_79

    #@6a
    .line 1160
    const/4 v13, 0x0

    #@6b
    iput v13, v5, Lcom/android/server/AlarmManagerService$FilterStats;->nesting:I

    #@6d
    .line 1161
    iget-wide v15, v5, Lcom/android/server/AlarmManagerService$FilterStats;->aggregateTime:J

    #@6f
    iget-wide v0, v5, Lcom/android/server/AlarmManagerService$FilterStats;->startTime:J

    #@71
    move-wide/from16 v17, v0

    #@73
    sub-long v17, v8, v17

    #@75
    add-long v15, v15, v17

    #@77
    iput-wide v15, v5, Lcom/android/server/AlarmManagerService$FilterStats;->aggregateTime:J
    :try_end_79
    .catchall {:try_start_b .. :try_end_79} :catchall_12d

    #@79
    .line 1166
    .end local v3           #bs:Lcom/android/server/AlarmManagerService$BroadcastStats;
    .end local v5           #fs:Lcom/android/server/AlarmManagerService$FilterStats;
    .end local v8           #nowELAPSED:J
    :cond_79
    :goto_79
    const/4 v10, 0x0

    #@7a
    .line 1167
    .local v10, pkg:Ljava/lang/String;
    const/4 v12, 0x0

    #@7b
    .line 1169
    .local v12, uid:I
    :try_start_7b
    invoke-virtual/range {p1 .. p1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    #@7e
    move-result-object v10

    #@7f
    .line 1170
    move-object/from16 v0, p0

    #@81
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@83
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;

    #@86
    move-result-object v13

    #@87
    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@8a
    move-result-object v11

    #@8b
    .line 1171
    .local v11, pm:Landroid/content/pm/PackageManager;
    const/16 v13, 0x80

    #@8d
    invoke-virtual {v11, v10, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@90
    move-result-object v2

    #@91
    .line 1173
    .local v2, appInfo:Landroid/content/pm/ApplicationInfo;
    iget v12, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@93
    .line 1174
    move-object/from16 v0, p0

    #@95
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@97
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1800(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@9a
    move-result-object v13

    #@9b
    new-instance v15, Ljava/lang/Integer;

    #@9d
    invoke-direct {v15, v12}, Ljava/lang/Integer;-><init>(I)V

    #@a0
    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_a3
    .catchall {:try_start_7b .. :try_end_a3} :catchall_12d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7b .. :try_end_a3} :catch_130

    #@a3
    .line 1178
    .end local v2           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v11           #pm:Landroid/content/pm/PackageManager;
    :goto_a3
    :try_start_a3
    move-object/from16 v0, p0

    #@a5
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@a7
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$2300(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@aa
    move-result-object v13

    #@ab
    new-instance v15, Ljava/lang/Integer;

    #@ad
    invoke-direct {v15, v12}, Ljava/lang/Integer;-><init>(I)V

    #@b0
    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@b3
    move-result v13

    #@b4
    if-eqz v13, :cond_14b

    #@b6
    .line 1179
    move-object/from16 v0, p0

    #@b8
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@ba
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$2300(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@bd
    move-result-object v13

    #@be
    new-instance v15, Ljava/lang/Integer;

    #@c0
    invoke-direct {v15, v12}, Ljava/lang/Integer;-><init>(I)V

    #@c3
    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@c6
    .line 1200
    :cond_c6
    :goto_c6
    move-object/from16 v0, p0

    #@c8
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@ca
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1500(Lcom/android/server/AlarmManagerService;)I

    #@cd
    move-result v13

    #@ce
    if-eqz v13, :cond_f9

    #@d0
    .line 1202
    move-object/from16 v0, p0

    #@d2
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@d4
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@d7
    move-result-object v13

    #@d8
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@db
    move-result v13

    #@dc
    if-lez v13, :cond_208

    #@de
    .line 1203
    move-object/from16 v0, p0

    #@e0
    iget-object v15, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@e2
    move-object/from16 v0, p0

    #@e4
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@e6
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@e9
    move-result-object v13

    #@ea
    const/16 v16, 0x0

    #@ec
    move/from16 v0, v16

    #@ee
    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f1
    move-result-object v13

    #@f2
    check-cast v13, Lcom/android/server/AlarmManagerService$InFlight;

    #@f4
    iget-object v13, v13, Lcom/android/server/AlarmManagerService$InFlight;->mPendingIntent:Landroid/app/PendingIntent;

    #@f6
    invoke-virtual {v15, v13}, Lcom/android/server/AlarmManagerService;->setWakelockWorkSource(Landroid/app/PendingIntent;)V

    #@f9
    .line 1214
    :cond_f9
    :goto_f9
    monitor-exit v14

    #@fa
    .line 1215
    return-void

    #@fb
    .line 1143
    .end local v10           #pkg:Ljava/lang/String;
    .end local v12           #uid:I
    :cond_fb
    add-int/lit8 v6, v6, 0x1

    #@fd
    goto/16 :goto_b

    #@ff
    .line 1164
    :cond_ff
    move-object/from16 v0, p0

    #@101
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@103
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$2200(Lcom/android/server/AlarmManagerService;)Lcom/android/internal/util/LocalLog;

    #@106
    move-result-object v13

    #@107
    new-instance v15, Ljava/lang/StringBuilder;

    #@109
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@10c
    const-string v16, "No in-flight alarm for "

    #@10e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v15

    #@112
    move-object/from16 v0, p1

    #@114
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v15

    #@118
    const-string v16, " "

    #@11a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v15

    #@11e
    move-object/from16 v0, p2

    #@120
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v15

    #@124
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v15

    #@128
    invoke-virtual {v13, v15}, Lcom/android/internal/util/LocalLog;->w(Ljava/lang/String;)V

    #@12b
    goto/16 :goto_79

    #@12d
    .line 1214
    :catchall_12d
    move-exception v13

    #@12e
    monitor-exit v14
    :try_end_12f
    .catchall {:try_start_a3 .. :try_end_12f} :catchall_12d

    #@12f
    throw v13

    #@130
    .line 1175
    .restart local v10       #pkg:Ljava/lang/String;
    .restart local v12       #uid:I
    :catch_130
    move-exception v4

    #@131
    .line 1176
    .local v4, ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_131
    const-string v13, "AlarmManager"

    #@133
    new-instance v15, Ljava/lang/StringBuilder;

    #@135
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@138
    const-string v16, "onSendFinished NameNotFoundException Pkg = "

    #@13a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v15

    #@13e
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v15

    #@142
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v15

    #@146
    invoke-static {v13, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@149
    goto/16 :goto_a3

    #@14b
    .line 1181
    .end local v4           #ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_14b
    move-object/from16 v0, p0

    #@14d
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@14f
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1500(Lcom/android/server/AlarmManagerService;)I

    #@152
    move-result v13

    #@153
    if-lez v13, :cond_c6

    #@155
    .line 1182
    move-object/from16 v0, p0

    #@157
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@159
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1510(Lcom/android/server/AlarmManagerService;)I

    #@15c
    .line 1183
    move-object/from16 v0, p0

    #@15e
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@160
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1500(Lcom/android/server/AlarmManagerService;)I

    #@163
    move-result v13

    #@164
    if-nez v13, :cond_c6

    #@166
    .line 1184
    move-object/from16 v0, p0

    #@168
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@16a
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1600(Lcom/android/server/AlarmManagerService;)Landroid/os/PowerManager$WakeLock;

    #@16d
    move-result-object v13

    #@16e
    invoke-virtual {v13}, Landroid/os/PowerManager$WakeLock;->release()V

    #@171
    .line 1185
    move-object/from16 v0, p0

    #@173
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@175
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@178
    move-result-object v13

    #@179
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@17c
    move-result v13

    #@17d
    if-lez v13, :cond_c6

    #@17f
    .line 1186
    move-object/from16 v0, p0

    #@181
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@183
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$2200(Lcom/android/server/AlarmManagerService;)Lcom/android/internal/util/LocalLog;

    #@186
    move-result-object v13

    #@187
    new-instance v15, Ljava/lang/StringBuilder;

    #@189
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@18c
    const-string v16, "Finished all broadcasts with "

    #@18e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    move-result-object v15

    #@192
    move-object/from16 v0, p0

    #@194
    iget-object v0, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@196
    move-object/from16 v16, v0

    #@198
    invoke-static/range {v16 .. v16}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@19b
    move-result-object v16

    #@19c
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@19f
    move-result v16

    #@1a0
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v15

    #@1a4
    const-string v16, " remaining inflights"

    #@1a6
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v15

    #@1aa
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ad
    move-result-object v15

    #@1ae
    invoke-virtual {v13, v15}, Lcom/android/internal/util/LocalLog;->w(Ljava/lang/String;)V

    #@1b1
    .line 1188
    const/4 v6, 0x0

    #@1b2
    :goto_1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@1b6
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@1b9
    move-result-object v13

    #@1ba
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@1bd
    move-result v13

    #@1be
    if-ge v6, v13, :cond_1fb

    #@1c0
    .line 1189
    move-object/from16 v0, p0

    #@1c2
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@1c4
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$2200(Lcom/android/server/AlarmManagerService;)Lcom/android/internal/util/LocalLog;

    #@1c7
    move-result-object v13

    #@1c8
    new-instance v15, Ljava/lang/StringBuilder;

    #@1ca
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1cd
    const-string v16, "  Remaining #"

    #@1cf
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v15

    #@1d3
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v15

    #@1d7
    const-string v16, ": "

    #@1d9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v15

    #@1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v0, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@1e1
    move-object/from16 v16, v0

    #@1e3
    invoke-static/range {v16 .. v16}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@1e6
    move-result-object v16

    #@1e7
    move-object/from16 v0, v16

    #@1e9
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1ec
    move-result-object v16

    #@1ed
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v15

    #@1f1
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f4
    move-result-object v15

    #@1f5
    invoke-virtual {v13, v15}, Lcom/android/internal/util/LocalLog;->w(Ljava/lang/String;)V

    #@1f8
    .line 1188
    add-int/lit8 v6, v6, 0x1

    #@1fa
    goto :goto_1b2

    #@1fb
    .line 1191
    :cond_1fb
    move-object/from16 v0, p0

    #@1fd
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@1ff
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1700(Lcom/android/server/AlarmManagerService;)Ljava/util/ArrayList;

    #@202
    move-result-object v13

    #@203
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V
    :try_end_206
    .catchall {:try_start_131 .. :try_end_206} :catchall_12d

    #@206
    goto/16 :goto_c6

    #@208
    .line 1207
    :cond_208
    :try_start_208
    move-object/from16 v0, p0

    #@20a
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@20c
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$2200(Lcom/android/server/AlarmManagerService;)Lcom/android/internal/util/LocalLog;

    #@20f
    move-result-object v13

    #@210
    const-string v15, "Alarm wakelock still held but sent queue empty"

    #@212
    invoke-virtual {v13, v15}, Lcom/android/internal/util/LocalLog;->w(Ljava/lang/String;)V

    #@215
    .line 1208
    move-object/from16 v0, p0

    #@217
    iget-object v13, v0, Lcom/android/server/AlarmManagerService$ResultReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@219
    invoke-static {v13}, Lcom/android/server/AlarmManagerService;->access$1600(Lcom/android/server/AlarmManagerService;)Landroid/os/PowerManager$WakeLock;

    #@21c
    move-result-object v13

    #@21d
    const/4 v15, 0x0

    #@21e
    invoke-virtual {v13, v15}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V
    :try_end_221
    .catchall {:try_start_208 .. :try_end_221} :catchall_12d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_208 .. :try_end_221} :catch_223

    #@221
    goto/16 :goto_f9

    #@223
    .line 1209
    :catch_223
    move-exception v4

    #@224
    .line 1210
    .local v4, ex:Ljava/lang/IllegalArgumentException;
    :try_start_224
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_227
    .catchall {:try_start_224 .. :try_end_227} :catchall_12d

    #@227
    goto/16 :goto_f9
.end method
