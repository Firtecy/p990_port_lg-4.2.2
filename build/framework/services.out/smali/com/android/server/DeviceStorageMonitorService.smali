.class public Lcom/android/server/DeviceStorageMonitorService;
.super Landroid/os/Binder;
.source "DeviceStorageMonitorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/DeviceStorageMonitorService$CacheFileDeletedObserver;,
        Lcom/android/server/DeviceStorageMonitorService$CachePackageDataObserver;
    }
.end annotation


# static fields
.field private static final CACHE_PATH:Ljava/lang/String; = "/cache"

.field private static final DATA_PATH:Ljava/lang/String; = "/data"

.field private static final DEBUG:Z = false

.field private static final DEFAULT_CHECK_INTERVAL:J = 0xea60L

.field private static final DEFAULT_DISK_FREE_CHANGE_REPORTING_THRESHOLD:J = 0x200000L

.field private static final DEFAULT_FREE_STORAGE_LOG_INTERVAL_IN_MINUTES:I = 0x2d0

.field private static final DEFAULT_FULL_THRESHOLD_BYTES:I = 0xa00000

.field private static final DEFAULT_THRESHOLD_MAX_BYTES:I = 0x1f400000

.field private static final DEFAULT_THRESHOLD_PERCENTAGE:I = 0xa

.field private static final DEVICE_MEMORY_WHAT:I = 0x1

.field private static final LOW_MEMORY_NOTIFICATION_ID:I = 0x1

.field private static final MONITOR_INTERVAL:I = 0x1

.field public static final SERVICE:Ljava/lang/String; = "devicestoragemonitor"

.field private static final SYSTEM_PATH:Ljava/lang/String; = "/system"

.field private static final TAG:Ljava/lang/String; = "DeviceStorageMonitorService"

.field private static final _FALSE:I = 0x0

.field private static final _TRUE:I = 0x1

.field private static final localLOGV:Z


# instance fields
.field private final mCacheFileDeletedObserver:Lcom/android/server/DeviceStorageMonitorService$CacheFileDeletedObserver;

.field private mCacheFileStats:Landroid/os/StatFs;

.field private mClearCacheObserver:Lcom/android/server/DeviceStorageMonitorService$CachePackageDataObserver;

.field private mClearSucceeded:Z

.field private mClearingCache:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mDataFileStats:Landroid/os/StatFs;

.field private mFreeMem:J

.field private mFreeMemAfterLastCacheClear:J

.field mHandler:Landroid/os/Handler;

.field private mLastReportedFreeMem:J

.field private mLastReportedFreeMemTime:J

.field private mLowMemFlag:Z

.field private mMemCacheStartTrimThreshold:J

.field private mMemCacheTrimToThreshold:J

.field private mMemFullFlag:Z

.field private mMemFullThreshold:I

.field private mMemLowThreshold:J

.field private mStorageFullIntent:Landroid/content/Intent;

.field private mStorageLowIntent:Landroid/content/Intent;

.field private mStorageNotFullIntent:Landroid/content/Intent;

.field private mStorageOkIntent:Landroid/content/Intent;

.field private mSystemFileStats:Landroid/os/StatFs;

.field private mThreadStartTime:J

.field private mTotalMemory:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/high16 v4, 0x800

    #@3
    .line 348
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@6
    .line 88
    iput-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mLowMemFlag:Z

    #@8
    .line 89
    iput-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullFlag:Z

    #@a
    .line 99
    const-wide/16 v0, -0x1

    #@c
    iput-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mThreadStartTime:J

    #@e
    .line 100
    iput-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearSucceeded:Z

    #@10
    .line 134
    new-instance v0, Lcom/android/server/DeviceStorageMonitorService$1;

    #@12
    invoke-direct {v0, p0}, Lcom/android/server/DeviceStorageMonitorService$1;-><init>(Lcom/android/server/DeviceStorageMonitorService;)V

    #@15
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mHandler:Landroid/os/Handler;

    #@17
    .line 349
    const-wide/16 v0, 0x0

    #@19
    iput-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMemTime:J

    #@1b
    .line 350
    iput-object p1, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@1d
    .line 351
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@1f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@22
    move-result-object v0

    #@23
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContentResolver:Landroid/content/ContentResolver;

    #@25
    .line 353
    new-instance v0, Landroid/os/StatFs;

    #@27
    const-string v1, "/data"

    #@29
    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@2c
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mDataFileStats:Landroid/os/StatFs;

    #@2e
    .line 354
    new-instance v0, Landroid/os/StatFs;

    #@30
    const-string v1, "/system"

    #@32
    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@35
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mSystemFileStats:Landroid/os/StatFs;

    #@37
    .line 355
    new-instance v0, Landroid/os/StatFs;

    #@39
    const-string v1, "/cache"

    #@3b
    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@3e
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mCacheFileStats:Landroid/os/StatFs;

    #@40
    .line 357
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mDataFileStats:Landroid/os/StatFs;

    #@42
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    #@45
    move-result v0

    #@46
    int-to-long v0, v0

    #@47
    iget-object v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mDataFileStats:Landroid/os/StatFs;

    #@49
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    #@4c
    move-result v2

    #@4d
    int-to-long v2, v2

    #@4e
    mul-long/2addr v0, v2

    #@4f
    iput-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mTotalMemory:J

    #@51
    .line 359
    new-instance v0, Landroid/content/Intent;

    #@53
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    #@55
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@58
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageLowIntent:Landroid/content/Intent;

    #@5a
    .line 360
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageLowIntent:Landroid/content/Intent;

    #@5c
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5f
    .line 361
    new-instance v0, Landroid/content/Intent;

    #@61
    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    #@63
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@66
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageOkIntent:Landroid/content/Intent;

    #@68
    .line 362
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageOkIntent:Landroid/content/Intent;

    #@6a
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@6d
    .line 363
    new-instance v0, Landroid/content/Intent;

    #@6f
    const-string v1, "android.intent.action.DEVICE_STORAGE_FULL"

    #@71
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@74
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageFullIntent:Landroid/content/Intent;

    #@76
    .line 364
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageFullIntent:Landroid/content/Intent;

    #@78
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@7b
    .line 365
    new-instance v0, Landroid/content/Intent;

    #@7d
    const-string v1, "android.intent.action.DEVICE_STORAGE_NOT_FULL"

    #@7f
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@82
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageNotFullIntent:Landroid/content/Intent;

    #@84
    .line 366
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageNotFullIntent:Landroid/content/Intent;

    #@86
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@89
    .line 368
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->getMemThreshold()J

    #@8c
    move-result-wide v0

    #@8d
    iput-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@8f
    .line 369
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->getMemFullThreshold()I

    #@92
    move-result v0

    #@93
    iput v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullThreshold:I

    #@95
    .line 370
    iget-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@97
    const-wide/16 v2, 0x3

    #@99
    mul-long/2addr v0, v2

    #@9a
    iget v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullThreshold:I

    #@9c
    int-to-long v2, v2

    #@9d
    add-long/2addr v0, v2

    #@9e
    const-wide/16 v2, 0x4

    #@a0
    div-long/2addr v0, v2

    #@a1
    iput-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheStartTrimThreshold:J

    #@a3
    .line 371
    iget-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@a5
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@a7
    iget-wide v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheStartTrimThreshold:J

    #@a9
    sub-long/2addr v2, v4

    #@aa
    const-wide/16 v4, 0x2

    #@ac
    mul-long/2addr v2, v4

    #@ad
    add-long/2addr v0, v2

    #@ae
    iput-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheTrimToThreshold:J

    #@b0
    .line 373
    iget-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mTotalMemory:J

    #@b2
    iput-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMemAfterLastCacheClear:J

    #@b4
    .line 374
    const/4 v0, 0x1

    #@b5
    invoke-direct {p0, v0}, Lcom/android/server/DeviceStorageMonitorService;->checkMemory(Z)V

    #@b8
    .line 376
    new-instance v0, Lcom/android/server/DeviceStorageMonitorService$CacheFileDeletedObserver;

    #@ba
    invoke-direct {v0}, Lcom/android/server/DeviceStorageMonitorService$CacheFileDeletedObserver;-><init>()V

    #@bd
    iput-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mCacheFileDeletedObserver:Lcom/android/server/DeviceStorageMonitorService$CacheFileDeletedObserver;

    #@bf
    .line 377
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mCacheFileDeletedObserver:Lcom/android/server/DeviceStorageMonitorService$CacheFileDeletedObserver;

    #@c1
    invoke-virtual {v0}, Lcom/android/server/DeviceStorageMonitorService$CacheFileDeletedObserver;->startWatching()V

    #@c4
    .line 378
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/DeviceStorageMonitorService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/android/server/DeviceStorageMonitorService;->checkMemory(Z)V

    #@3
    return-void
.end method

.method static synthetic access$102(Lcom/android/server/DeviceStorageMonitorService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearSucceeded:Z

    #@2
    return p1
.end method

.method static synthetic access$202(Lcom/android/server/DeviceStorageMonitorService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearingCache:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/server/DeviceStorageMonitorService;ZJ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/DeviceStorageMonitorService;->postCheckMemoryMsg(ZJ)V

    #@3
    return-void
.end method

.method private final cancelFullNotification()V
    .registers 4

    #@0
    .prologue
    .line 476
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageFullIntent:Landroid/content/Intent;

    #@4
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->removeStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@9
    .line 477
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@b
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageNotFullIntent:Landroid/content/Intent;

    #@d
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@12
    .line 478
    return-void
.end method

.method private final cancelNotification()V
    .registers 5

    #@0
    .prologue
    .line 453
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "notification"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/NotificationManager;

    #@a
    .line 457
    .local v0, mNotificationMgr:Landroid/app/NotificationManager;
    const/4 v1, 0x0

    #@b
    const/4 v2, 0x1

    #@c
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@e
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@11
    .line 459
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@13
    iget-object v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageLowIntent:Landroid/content/Intent;

    #@15
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@17
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->removeStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1a
    .line 460
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@1c
    iget-object v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageOkIntent:Landroid/content/Intent;

    #@1e
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@20
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@23
    .line 461
    return-void
.end method

.method private final checkMemory(Z)V
    .registers 12
    .parameter "checkCache"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 230
    iget-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearingCache:Z

    #@4
    if-eqz v2, :cond_23

    #@6
    .line 233
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v2

    #@a
    iget-wide v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mThreadStartTime:J

    #@c
    sub-long v0, v2, v4

    #@e
    .line 234
    .local v0, diffTime:J
    const-wide/32 v2, 0x927c0

    #@11
    cmp-long v2, v0, v2

    #@13
    if-lez v2, :cond_1c

    #@15
    .line 235
    const-string v2, "DeviceStorageMonitorService"

    #@17
    const-string v3, "Thread that clears cache file seems to run for ever"

    #@19
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 299
    .end local v0           #diffTime:J
    :cond_1c
    :goto_1c
    const-wide/32 v2, 0xea60

    #@1f
    invoke-direct {p0, v9, v2, v3}, Lcom/android/server/DeviceStorageMonitorService;->postCheckMemoryMsg(ZJ)V

    #@22
    .line 300
    return-void

    #@23
    .line 238
    :cond_23
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->restatDataDir()V

    #@26
    .line 242
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@28
    iget-wide v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@2a
    cmp-long v2, v2, v4

    #@2c
    if-gez v2, :cond_7c

    #@2e
    .line 243
    if-eqz p1, :cond_67

    #@30
    .line 249
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@32
    iget-wide v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheStartTrimThreshold:J

    #@34
    cmp-long v2, v2, v4

    #@36
    if-gez v2, :cond_54

    #@38
    .line 252
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMemAfterLastCacheClear:J

    #@3a
    iget-wide v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@3c
    sub-long/2addr v2, v4

    #@3d
    iget-wide v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@3f
    iget-wide v6, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheStartTrimThreshold:J

    #@41
    sub-long/2addr v4, v6

    #@42
    const-wide/16 v6, 0x4

    #@44
    div-long/2addr v4, v6

    #@45
    cmp-long v2, v2, v4

    #@47
    if-ltz v2, :cond_54

    #@49
    .line 257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4c
    move-result-wide v2

    #@4d
    iput-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mThreadStartTime:J

    #@4f
    .line 258
    iput-boolean v8, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearSucceeded:Z

    #@51
    .line 259
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->clearCache()V

    #@54
    .line 285
    :cond_54
    :goto_54
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@56
    iget v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullThreshold:I

    #@58
    int-to-long v4, v4

    #@59
    cmp-long v2, v2, v4

    #@5b
    if-gez v2, :cond_91

    #@5d
    .line 286
    iget-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullFlag:Z

    #@5f
    if-nez v2, :cond_1c

    #@61
    .line 287
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->sendFullNotification()V

    #@64
    .line 288
    iput-boolean v9, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullFlag:Z

    #@66
    goto :goto_1c

    #@67
    .line 265
    :cond_67
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@69
    iput-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMemAfterLastCacheClear:J

    #@6b
    .line 266
    iget-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mLowMemFlag:Z

    #@6d
    if-nez v2, :cond_54

    #@6f
    .line 269
    const-string v2, "DeviceStorageMonitorService"

    #@71
    const-string v3, "Running low on memory. Sending notification"

    #@73
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 270
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->sendNotification()V

    #@79
    .line 271
    iput-boolean v9, p0, Lcom/android/server/DeviceStorageMonitorService;->mLowMemFlag:Z

    #@7b
    goto :goto_54

    #@7c
    .line 278
    :cond_7c
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@7e
    iput-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMemAfterLastCacheClear:J

    #@80
    .line 279
    iget-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mLowMemFlag:Z

    #@82
    if-eqz v2, :cond_54

    #@84
    .line 280
    const-string v2, "DeviceStorageMonitorService"

    #@86
    const-string v3, "Memory available. Cancelling notification"

    #@88
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 281
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->cancelNotification()V

    #@8e
    .line 282
    iput-boolean v8, p0, Lcom/android/server/DeviceStorageMonitorService;->mLowMemFlag:Z

    #@90
    goto :goto_54

    #@91
    .line 291
    :cond_91
    iget-boolean v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullFlag:Z

    #@93
    if-eqz v2, :cond_1c

    #@95
    .line 292
    invoke-direct {p0}, Lcom/android/server/DeviceStorageMonitorService;->cancelFullNotification()V

    #@98
    .line 293
    iput-boolean v8, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullFlag:Z

    #@9a
    goto :goto_1c
.end method

.method private final clearCache()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 209
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearCacheObserver:Lcom/android/server/DeviceStorageMonitorService$CachePackageDataObserver;

    #@3
    if-nez v1, :cond_c

    #@5
    .line 211
    new-instance v1, Lcom/android/server/DeviceStorageMonitorService$CachePackageDataObserver;

    #@7
    invoke-direct {v1, p0}, Lcom/android/server/DeviceStorageMonitorService$CachePackageDataObserver;-><init>(Lcom/android/server/DeviceStorageMonitorService;)V

    #@a
    iput-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearCacheObserver:Lcom/android/server/DeviceStorageMonitorService$CachePackageDataObserver;

    #@c
    .line 213
    :cond_c
    const/4 v1, 0x1

    #@d
    iput-boolean v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearingCache:Z

    #@f
    .line 216
    :try_start_f
    const-string v1, "package"

    #@11
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@14
    move-result-object v1

    #@15
    invoke-static {v1}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    #@18
    move-result-object v1

    #@19
    iget-wide v2, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheTrimToThreshold:J

    #@1b
    iget-object v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearCacheObserver:Lcom/android/server/DeviceStorageMonitorService$CachePackageDataObserver;

    #@1d
    invoke-interface {v1, v2, v3, v4}, Landroid/content/pm/IPackageManager;->freeStorageAndNotify(JLandroid/content/pm/IPackageDataObserver;)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_20} :catch_21

    #@20
    .line 223
    :goto_20
    return-void

    #@21
    .line 218
    :catch_21
    move-exception v0

    #@22
    .line 219
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "DeviceStorageMonitorService"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Failed to get handle for PackageManger Exception: "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 220
    iput-boolean v5, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearingCache:Z

    #@3c
    .line 221
    iput-boolean v5, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearSucceeded:Z

    #@3e
    goto :goto_20
.end method

.method private getMemFullThreshold()I
    .registers 5

    #@0
    .prologue
    .line 336
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v2, "sys_storage_full_threshold_bytes"

    #@4
    const/high16 v3, 0xa0

    #@6
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@9
    move-result v0

    #@a
    .line 341
    .local v0, value:I
    return v0
.end method

.method private getMemThreshold()J
    .registers 9

    #@0
    .prologue
    .line 316
    iget-object v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v5, "sys_storage_threshold_percentage"

    #@4
    const/16 v6, 0xa

    #@6
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@9
    move-result v4

    #@a
    int-to-long v2, v4

    #@b
    .line 321
    .local v2, value:J
    iget-wide v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mTotalMemory:J

    #@d
    mul-long/2addr v4, v2

    #@e
    const-wide/16 v6, 0x64

    #@10
    div-long v2, v4, v6

    #@12
    .line 322
    iget-object v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mContentResolver:Landroid/content/ContentResolver;

    #@14
    const-string v5, "sys_storage_threshold_max_bytes"

    #@16
    const/high16 v6, 0x1f40

    #@18
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1b
    move-result v4

    #@1c
    int-to-long v0, v4

    #@1d
    .line 327
    .local v0, maxValue:J
    cmp-long v4, v2, v0

    #@1f
    if-gez v4, :cond_22

    #@21
    .end local v2           #value:J
    :goto_21
    return-wide v2

    #@22
    .restart local v2       #value:J
    :cond_22
    move-wide v2, v0

    #@23
    goto :goto_21
.end method

.method private postCheckMemoryMsg(ZJ)V
    .registers 9
    .parameter "clearCache"
    .parameter "delay"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 304
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 305
    iget-object v3, p0, Lcom/android/server/DeviceStorageMonitorService;->mHandler:Landroid/os/Handler;

    #@9
    iget-object v4, p0, Lcom/android/server/DeviceStorageMonitorService;->mHandler:Landroid/os/Handler;

    #@b
    if-eqz p1, :cond_16

    #@d
    move v0, v1

    #@e
    :goto_e
    invoke-virtual {v4, v1, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v3, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@15
    .line 308
    return-void

    #@16
    :cond_16
    move v0, v2

    #@17
    .line 305
    goto :goto_e
.end method

.method private final restatDataDir()V
    .registers 21

    #@0
    .prologue
    .line 158
    :try_start_0
    move-object/from16 v0, p0

    #@2
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mDataFileStats:Landroid/os/StatFs;

    #@4
    const-string v16, "/data"

    #@6
    invoke-virtual/range {v15 .. v16}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    #@9
    .line 159
    move-object/from16 v0, p0

    #@b
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mDataFileStats:Landroid/os/StatFs;

    #@d
    invoke-virtual {v15}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@10
    move-result v15

    #@11
    int-to-long v15, v15

    #@12
    move-object/from16 v0, p0

    #@14
    iget-object v0, v0, Lcom/android/server/DeviceStorageMonitorService;->mDataFileStats:Landroid/os/StatFs;

    #@16
    move-object/from16 v17, v0

    #@18
    invoke-virtual/range {v17 .. v17}, Landroid/os/StatFs;->getBlockSize()I

    #@1b
    move-result v17

    #@1c
    move/from16 v0, v17

    #@1e
    int-to-long v0, v0

    #@1f
    move-wide/from16 v17, v0

    #@21
    mul-long v15, v15, v17

    #@23
    move-object/from16 v0, p0

    #@25
    iput-wide v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J
    :try_end_27
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_27} :catch_11d

    #@27
    .line 165
    :goto_27
    const-string v15, "debug.freemem"

    #@29
    invoke-static {v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    .line 166
    .local v4, debugFreeMem:Ljava/lang/String;
    const-string v15, ""

    #@2f
    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v15

    #@33
    if-nez v15, :cond_3d

    #@35
    .line 167
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@38
    move-result-wide v15

    #@39
    move-object/from16 v0, p0

    #@3b
    iput-wide v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@3d
    .line 170
    :cond_3d
    move-object/from16 v0, p0

    #@3f
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mContentResolver:Landroid/content/ContentResolver;

    #@41
    const-string v16, "sys_free_storage_log_interval"

    #@43
    const-wide/16 v17, 0x2d0

    #@45
    invoke-static/range {v15 .. v18}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@48
    move-result-wide v15

    #@49
    const-wide/16 v17, 0x3c

    #@4b
    mul-long v15, v15, v17

    #@4d
    const-wide/16 v17, 0x3e8

    #@4f
    mul-long v7, v15, v17

    #@51
    .line 174
    .local v7, freeMemLogInterval:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@54
    move-result-wide v2

    #@55
    .line 175
    .local v2, currTime:J
    move-object/from16 v0, p0

    #@57
    iget-wide v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMemTime:J

    #@59
    const-wide/16 v17, 0x0

    #@5b
    cmp-long v15, v15, v17

    #@5d
    if-eqz v15, :cond_69

    #@5f
    move-object/from16 v0, p0

    #@61
    iget-wide v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMemTime:J

    #@63
    sub-long v15, v2, v15

    #@65
    cmp-long v15, v15, v7

    #@67
    if-ltz v15, :cond_e2

    #@69
    .line 177
    :cond_69
    move-object/from16 v0, p0

    #@6b
    iput-wide v2, v0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMemTime:J

    #@6d
    .line 178
    const-wide/16 v11, -0x1

    #@6f
    .local v11, mFreeSystem:J
    const-wide/16 v9, -0x1

    #@71
    .line 180
    .local v9, mFreeCache:J
    :try_start_71
    move-object/from16 v0, p0

    #@73
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mSystemFileStats:Landroid/os/StatFs;

    #@75
    const-string v16, "/system"

    #@77
    invoke-virtual/range {v15 .. v16}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    #@7a
    .line 181
    move-object/from16 v0, p0

    #@7c
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mSystemFileStats:Landroid/os/StatFs;

    #@7e
    invoke-virtual {v15}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@81
    move-result v15

    #@82
    int-to-long v15, v15

    #@83
    move-object/from16 v0, p0

    #@85
    iget-object v0, v0, Lcom/android/server/DeviceStorageMonitorService;->mSystemFileStats:Landroid/os/StatFs;

    #@87
    move-object/from16 v17, v0

    #@89
    invoke-virtual/range {v17 .. v17}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_8c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_71 .. :try_end_8c} :catch_11a

    #@8c
    move-result v17

    #@8d
    move/from16 v0, v17

    #@8f
    int-to-long v0, v0

    #@90
    move-wide/from16 v17, v0

    #@92
    mul-long v11, v15, v17

    #@94
    .line 187
    :goto_94
    :try_start_94
    move-object/from16 v0, p0

    #@96
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mCacheFileStats:Landroid/os/StatFs;

    #@98
    const-string v16, "/cache"

    #@9a
    invoke-virtual/range {v15 .. v16}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    #@9d
    .line 188
    move-object/from16 v0, p0

    #@9f
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mCacheFileStats:Landroid/os/StatFs;

    #@a1
    invoke-virtual {v15}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@a4
    move-result v15

    #@a5
    int-to-long v15, v15

    #@a6
    move-object/from16 v0, p0

    #@a8
    iget-object v0, v0, Lcom/android/server/DeviceStorageMonitorService;->mCacheFileStats:Landroid/os/StatFs;

    #@aa
    move-object/from16 v17, v0

    #@ac
    invoke-virtual/range {v17 .. v17}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_af
    .catch Ljava/lang/IllegalArgumentException; {:try_start_94 .. :try_end_af} :catch_118

    #@af
    move-result v17

    #@b0
    move/from16 v0, v17

    #@b2
    int-to-long v0, v0

    #@b3
    move-wide/from16 v17, v0

    #@b5
    mul-long v9, v15, v17

    #@b7
    .line 193
    :goto_b7
    const/16 v15, 0xaba

    #@b9
    const/16 v16, 0x3

    #@bb
    move/from16 v0, v16

    #@bd
    new-array v0, v0, [Ljava/lang/Object;

    #@bf
    move-object/from16 v16, v0

    #@c1
    const/16 v17, 0x0

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget-wide v0, v0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@c7
    move-wide/from16 v18, v0

    #@c9
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@cc
    move-result-object v18

    #@cd
    aput-object v18, v16, v17

    #@cf
    const/16 v17, 0x1

    #@d1
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@d4
    move-result-object v18

    #@d5
    aput-object v18, v16, v17

    #@d7
    const/16 v17, 0x2

    #@d9
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@dc
    move-result-object v18

    #@dd
    aput-object v18, v16, v17

    #@df
    invoke-static/range {v15 .. v16}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@e2
    .line 197
    .end local v9           #mFreeCache:J
    .end local v11           #mFreeSystem:J
    :cond_e2
    move-object/from16 v0, p0

    #@e4
    iget-object v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mContentResolver:Landroid/content/ContentResolver;

    #@e6
    const-string v16, "disk_free_change_reporting_threshold"

    #@e8
    const-wide/32 v17, 0x200000

    #@eb
    invoke-static/range {v15 .. v18}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@ee
    move-result-wide v13

    #@ef
    .line 201
    .local v13, threshold:J
    move-object/from16 v0, p0

    #@f1
    iget-wide v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@f3
    move-object/from16 v0, p0

    #@f5
    iget-wide v0, v0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMem:J

    #@f7
    move-wide/from16 v17, v0

    #@f9
    sub-long v5, v15, v17

    #@fb
    .line 202
    .local v5, delta:J
    cmp-long v15, v5, v13

    #@fd
    if-gtz v15, :cond_104

    #@ff
    neg-long v15, v13

    #@100
    cmp-long v15, v5, v15

    #@102
    if-gez v15, :cond_117

    #@104
    .line 203
    :cond_104
    move-object/from16 v0, p0

    #@106
    iget-wide v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@108
    move-object/from16 v0, p0

    #@10a
    iput-wide v15, v0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMem:J

    #@10c
    .line 204
    const/16 v15, 0xab8

    #@10e
    move-object/from16 v0, p0

    #@110
    iget-wide v0, v0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@112
    move-wide/from16 v16, v0

    #@114
    invoke-static/range {v15 .. v17}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@117
    .line 206
    :cond_117
    return-void

    #@118
    .line 190
    .end local v5           #delta:J
    .end local v13           #threshold:J
    .restart local v9       #mFreeCache:J
    .restart local v11       #mFreeSystem:J
    :catch_118
    move-exception v15

    #@119
    goto :goto_b7

    #@11a
    .line 183
    :catch_11a
    move-exception v15

    #@11b
    goto/16 :goto_94

    #@11d
    .line 161
    .end local v2           #currTime:J
    .end local v4           #debugFreeMem:Ljava/lang/String;
    .end local v7           #freeMemLogInterval:J
    .end local v9           #mFreeCache:J
    .end local v11           #mFreeSystem:J
    :catch_11d
    move-exception v15

    #@11e
    goto/16 :goto_27
.end method

.method private final sendFullNotification()V
    .registers 4

    #@0
    .prologue
    .line 468
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageFullIntent:Landroid/content/Intent;

    #@4
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@9
    .line 469
    return-void
.end method

.method private final sendNotification()V
    .registers 13

    #@0
    .prologue
    .line 389
    const/16 v0, 0xab9

    #@2
    iget-wide v3, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@4
    invoke-static {v0, v3, v4}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@7
    .line 391
    const-string v0, "ro.build.target_operator"

    #@9
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v6

    #@d
    .line 392
    .local v6, TARGET_OPERATOR:Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    #@f
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_9e

    #@15
    const-string v0, "android.settings.INTERNAL_STORAGE_SETTINGS"

    #@17
    :goto_17
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1a
    .line 395
    .local v2, lowMemIntent:Landroid/content/Intent;
    const-string v0, "memory"

    #@1c
    iget-wide v3, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@1e
    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@21
    .line 396
    const/high16 v0, 0x1000

    #@23
    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@26
    .line 397
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@28
    const-string v1, "notification"

    #@2a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2d
    move-result-object v9

    #@2e
    check-cast v9, Landroid/app/NotificationManager;

    #@30
    .line 402
    .local v9, mNotificationMgr:Landroid/app/NotificationManager;
    const-string v0, "DCM"

    #@32
    const-string v1, "ro.build.target_operator"

    #@34
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_a2

    #@3e
    .line 403
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@40
    const v1, 0x2090326

    #@43
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@46
    move-result-object v11

    #@47
    .line 405
    .local v11, title:Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@49
    const v1, 0x2090327

    #@4c
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@4f
    move-result-object v7

    #@50
    .line 407
    .local v7, details:Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@52
    const/4 v1, 0x0

    #@53
    const/4 v3, 0x0

    #@54
    const/4 v4, 0x0

    #@55
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@57
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@5a
    move-result-object v8

    #@5b
    .line 410
    .local v8, intent:Landroid/app/PendingIntent;
    new-instance v0, Landroid/app/Notification$BigTextStyle;

    #@5d
    new-instance v1, Landroid/app/Notification$Builder;

    #@5f
    iget-object v3, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@61
    invoke-direct {v1, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@64
    invoke-virtual {v1, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@67
    move-result-object v1

    #@68
    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@6b
    move-result-object v1

    #@6c
    const v3, 0x1080526

    #@6f
    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@76
    move-result-object v1

    #@77
    invoke-direct {v0, v1}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    #@7a
    invoke-virtual {v0, v7}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    #@7d
    move-result-object v0

    #@7e
    invoke-virtual {v0}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    #@81
    move-result-object v10

    #@82
    .line 418
    .local v10, notification:Landroid/app/Notification;
    iget v0, v10, Landroid/app/Notification;->flags:I

    #@84
    or-int/lit8 v0, v0, 0x20

    #@86
    iput v0, v10, Landroid/app/Notification;->flags:I

    #@88
    .line 419
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@8a
    invoke-virtual {v10, v0, v11, v7, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@8d
    .line 420
    const/4 v0, 0x0

    #@8e
    const/4 v1, 0x1

    #@8f
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@91
    invoke-virtual {v9, v0, v1, v10, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@94
    .line 445
    :goto_94
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@96
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mStorageLowIntent:Landroid/content/Intent;

    #@98
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@9a
    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@9d
    .line 446
    return-void

    #@9e
    .line 392
    .end local v2           #lowMemIntent:Landroid/content/Intent;
    .end local v7           #details:Ljava/lang/CharSequence;
    .end local v8           #intent:Landroid/app/PendingIntent;
    .end local v9           #mNotificationMgr:Landroid/app/NotificationManager;
    .end local v10           #notification:Landroid/app/Notification;
    .end local v11           #title:Ljava/lang/CharSequence;
    :cond_9e
    const-string v0, "android.intent.action.MANAGE_PACKAGE_STORAGE"

    #@a0
    goto/16 :goto_17

    #@a2
    .line 427
    .restart local v2       #lowMemIntent:Landroid/content/Intent;
    .restart local v9       #mNotificationMgr:Landroid/app/NotificationManager;
    :cond_a2
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@a4
    const v1, 0x10403f0

    #@a7
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@aa
    move-result-object v11

    #@ab
    .line 429
    .restart local v11       #title:Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@ad
    const v1, 0x10403f1

    #@b0
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@b3
    move-result-object v7

    #@b4
    .line 431
    .restart local v7       #details:Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@b6
    const/4 v1, 0x0

    #@b7
    const/4 v3, 0x0

    #@b8
    const/4 v4, 0x0

    #@b9
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@bb
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@be
    move-result-object v8

    #@bf
    .line 434
    .restart local v8       #intent:Landroid/app/PendingIntent;
    new-instance v10, Landroid/app/Notification;

    #@c1
    invoke-direct {v10}, Landroid/app/Notification;-><init>()V

    #@c4
    .line 435
    .restart local v10       #notification:Landroid/app/Notification;
    const v0, 0x1080526

    #@c7
    iput v0, v10, Landroid/app/Notification;->icon:I

    #@c9
    .line 436
    iput-object v11, v10, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@cb
    .line 437
    iget v0, v10, Landroid/app/Notification;->flags:I

    #@cd
    or-int/lit8 v0, v0, 0x20

    #@cf
    iput v0, v10, Landroid/app/Notification;->flags:I

    #@d1
    .line 438
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@d3
    invoke-virtual {v10, v0, v11, v7, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@d6
    .line 439
    const/4 v0, 0x0

    #@d7
    const/4 v1, 0x1

    #@d8
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@da
    invoke-virtual {v9, v0, v1, v10, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@dd
    goto :goto_94
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 522
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_33

    #@a
    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "Permission Denial: can\'t dump devicestoragemonitor from from pid="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", uid="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 554
    :goto_32
    return-void

    #@33
    .line 531
    :cond_33
    const-string v0, "Current DeviceStorageMonitor state:"

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 532
    const-string v0, "  mFreeMem="

    #@3a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@3f
    iget-wide v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMem:J

    #@41
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@48
    .line 533
    const-string v0, " mTotalMemory="

    #@4a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4d
    .line 534
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@4f
    iget-wide v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mTotalMemory:J

    #@51
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@58
    .line 535
    const-string v0, "  mFreeMemAfterLastCacheClear="

    #@5a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d
    .line 536
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@5f
    iget-wide v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mFreeMemAfterLastCacheClear:J

    #@61
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 537
    const-string v0, "  mLastReportedFreeMem="

    #@6a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6d
    .line 538
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@6f
    iget-wide v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMem:J

    #@71
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@78
    .line 539
    const-string v0, " mLastReportedFreeMemTime="

    #@7a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7d
    .line 540
    iget-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mLastReportedFreeMemTime:J

    #@7f
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@82
    move-result-wide v2

    #@83
    invoke-static {v0, v1, v2, v3, p2}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@86
    .line 541
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@89
    .line 542
    const-string v0, "  mLowMemFlag="

    #@8b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8e
    iget-boolean v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mLowMemFlag:Z

    #@90
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@93
    .line 543
    const-string v0, " mMemFullFlag="

    #@95
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@98
    iget-boolean v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullFlag:Z

    #@9a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@9d
    .line 544
    const-string v0, "  mClearSucceeded="

    #@9f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a2
    iget-boolean v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearSucceeded:Z

    #@a4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@a7
    .line 545
    const-string v0, " mClearingCache="

    #@a9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ac
    iget-boolean v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mClearingCache:Z

    #@ae
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@b1
    .line 546
    const-string v0, "  mMemLowThreshold="

    #@b3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b6
    .line 547
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@b8
    iget-wide v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@ba
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@bd
    move-result-object v0

    #@be
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c1
    .line 548
    const-string v0, " mMemFullThreshold="

    #@c3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c6
    .line 549
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@c8
    iget v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemFullThreshold:I

    #@ca
    int-to-long v1, v1

    #@cb
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@ce
    move-result-object v0

    #@cf
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d2
    .line 550
    const-string v0, "  mMemCacheStartTrimThreshold="

    #@d4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d7
    .line 551
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@d9
    iget-wide v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheStartTrimThreshold:J

    #@db
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@de
    move-result-object v0

    #@df
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e2
    .line 552
    const-string v0, " mMemCacheTrimToThreshold="

    #@e4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e7
    .line 553
    iget-object v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mContext:Landroid/content/Context;

    #@e9
    iget-wide v1, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemCacheTrimToThreshold:J

    #@eb
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@ee
    move-result-object v0

    #@ef
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f2
    goto/16 :goto_32
.end method

.method public getMemoryLowThreshold()J
    .registers 3

    #@0
    .prologue
    .line 496
    iget-wide v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mMemLowThreshold:J

    #@2
    return-wide v0
.end method

.method public isMemoryLow()Z
    .registers 2

    #@0
    .prologue
    .line 506
    iget-boolean v0, p0, Lcom/android/server/DeviceStorageMonitorService;->mLowMemFlag:Z

    #@2
    return v0
.end method

.method public updateMemory()V
    .registers 5

    #@0
    .prologue
    .line 481
    invoke-static {}, Lcom/android/server/DeviceStorageMonitorService;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 482
    .local v0, callingUid:I
    const/16 v1, 0x3e8

    #@6
    if-eq v0, v1, :cond_9

    #@8
    .line 487
    :goto_8
    return-void

    #@9
    .line 486
    :cond_9
    const/4 v1, 0x1

    #@a
    const-wide/16 v2, 0x0

    #@c
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/DeviceStorageMonitorService;->postCheckMemoryMsg(ZJ)V

    #@f
    goto :goto_8
.end method
