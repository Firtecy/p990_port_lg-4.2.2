.class public Lcom/android/server/ProcessMap;
.super Ljava/lang/Object;
.source "ProcessMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 23
    .local p0, this:Lcom/android/server/ProcessMap;,"Lcom/android/server/ProcessMap<TE;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 24
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/ProcessMap;->mMap:Ljava/util/HashMap;

    #@a
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;I)Ljava/lang/Object;
    .registers 5
    .parameter "name"
    .parameter "uid"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)TE;"
        }
    .end annotation

    #@0
    .prologue
    .line 28
    .local p0, this:Lcom/android/server/ProcessMap;,"Lcom/android/server/ProcessMap<TE;>;"
    iget-object v1, p0, Lcom/android/server/ProcessMap;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/util/SparseArray;

    #@8
    .line 29
    .local v0, uids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    .line 30
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    goto :goto_b
.end method

.method public getMap()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<TE;>;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 54
    .local p0, this:Lcom/android/server/ProcessMap;,"Lcom/android/server/ProcessMap<TE;>;"
    iget-object v0, p0, Lcom/android/server/ProcessMap;->mMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public put(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter "name"
    .parameter "uid"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ITE;)TE;"
        }
    .end annotation

    #@0
    .prologue
    .line 34
    .local p0, this:Lcom/android/server/ProcessMap;,"Lcom/android/server/ProcessMap<TE;>;"
    .local p3, value:Ljava/lang/Object;,"TE;"
    iget-object v1, p0, Lcom/android/server/ProcessMap;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/util/SparseArray;

    #@8
    .line 35
    .local v0, uids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    if-nez v0, :cond_15

    #@a
    .line 36
    new-instance v0, Landroid/util/SparseArray;

    #@c
    .end local v0           #uids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    const/4 v1, 0x2

    #@d
    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    #@10
    .line 37
    .restart local v0       #uids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget-object v1, p0, Lcom/android/server/ProcessMap;->mMap:Ljava/util/HashMap;

    #@12
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 39
    :cond_15
    invoke-virtual {v0, p2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@18
    .line 40
    return-object p3
.end method

.method public remove(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "uid"

    #@0
    .prologue
    .line 44
    .local p0, this:Lcom/android/server/ProcessMap;,"Lcom/android/server/ProcessMap<TE;>;"
    iget-object v1, p0, Lcom/android/server/ProcessMap;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/util/SparseArray;

    #@8
    .line 45
    .local v0, uids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    if-eqz v0, :cond_18

    #@a
    .line 46
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    #@d
    .line 47
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_18

    #@13
    .line 48
    iget-object v1, p0, Lcom/android/server/ProcessMap;->mMap:Ljava/util/HashMap;

    #@15
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 51
    :cond_18
    return-void
.end method
