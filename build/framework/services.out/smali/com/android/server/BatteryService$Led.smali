.class final Lcom/android/server/BatteryService$Led;
.super Ljava/lang/Object;
.source "BatteryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BatteryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Led"
.end annotation


# instance fields
.field private final mBatteryFullARGB:I

.field private final mBatteryLedOff:I

.field private final mBatteryLedOn:I

.field private final mBatteryLight:Lcom/android/server/LightsService$Light;

.field private final mBatteryLowARGB:I

.field private final mBatteryMediumARGB:I

.field final synthetic this$0:Lcom/android/server/BatteryService;


# direct methods
.method public constructor <init>(Lcom/android/server/BatteryService;Landroid/content/Context;Lcom/android/server/LightsService;)V
    .registers 6
    .parameter
    .parameter "context"
    .parameter "lights"

    #@0
    .prologue
    .line 856
    iput-object p1, p0, Lcom/android/server/BatteryService$Led;->this$0:Lcom/android/server/BatteryService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 857
    const/4 v0, 0x3

    #@6
    invoke-virtual {p3, v0}, Lcom/android/server/LightsService;->getLight(I)Lcom/android/server/LightsService$Light;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/server/BatteryService$Led;->mBatteryLight:Lcom/android/server/LightsService$Light;

    #@c
    .line 859
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v0

    #@10
    const v1, 0x10e001f

    #@13
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@16
    move-result v0

    #@17
    iput v0, p0, Lcom/android/server/BatteryService$Led;->mBatteryLowARGB:I

    #@19
    .line 861
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1c
    move-result-object v0

    #@1d
    const v1, 0x10e0020

    #@20
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@23
    move-result v0

    #@24
    iput v0, p0, Lcom/android/server/BatteryService$Led;->mBatteryMediumARGB:I

    #@26
    .line 863
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29
    move-result-object v0

    #@2a
    const v1, 0x10e0021

    #@2d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@30
    move-result v0

    #@31
    iput v0, p0, Lcom/android/server/BatteryService$Led;->mBatteryFullARGB:I

    #@33
    .line 865
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@36
    move-result-object v0

    #@37
    const v1, 0x10e0022

    #@3a
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@3d
    move-result v0

    #@3e
    iput v0, p0, Lcom/android/server/BatteryService$Led;->mBatteryLedOn:I

    #@40
    .line 867
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@43
    move-result-object v0

    #@44
    const v1, 0x10e0023

    #@47
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@4a
    move-result v0

    #@4b
    iput v0, p0, Lcom/android/server/BatteryService$Led;->mBatteryLedOff:I

    #@4d
    .line 869
    return-void
.end method


# virtual methods
.method public updateLightsLocked()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x5

    #@1
    const/4 v3, 0x2

    #@2
    .line 875
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->this$0:Lcom/android/server/BatteryService;

    #@4
    invoke-static {v2}, Lcom/android/server/BatteryService;->access$700(Lcom/android/server/BatteryService;)I

    #@7
    move-result v0

    #@8
    .line 876
    .local v0, level:I
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->this$0:Lcom/android/server/BatteryService;

    #@a
    invoke-static {v2}, Lcom/android/server/BatteryService;->access$800(Lcom/android/server/BatteryService;)I

    #@d
    move-result v1

    #@e
    .line 877
    .local v1, status:I
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->this$0:Lcom/android/server/BatteryService;

    #@10
    invoke-static {v2}, Lcom/android/server/BatteryService;->access$900(Lcom/android/server/BatteryService;)I

    #@13
    move-result v2

    #@14
    if-ge v0, v2, :cond_2d

    #@16
    .line 878
    if-ne v1, v3, :cond_20

    #@18
    .line 880
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->mBatteryLight:Lcom/android/server/LightsService$Light;

    #@1a
    iget v3, p0, Lcom/android/server/BatteryService$Led;->mBatteryLowARGB:I

    #@1c
    invoke-virtual {v2, v3}, Lcom/android/server/LightsService$Light;->setColor(I)V

    #@1f
    .line 899
    :goto_1f
    return-void

    #@20
    .line 883
    :cond_20
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->mBatteryLight:Lcom/android/server/LightsService$Light;

    #@22
    iget v3, p0, Lcom/android/server/BatteryService$Led;->mBatteryLowARGB:I

    #@24
    const/4 v4, 0x1

    #@25
    iget v5, p0, Lcom/android/server/BatteryService$Led;->mBatteryLedOn:I

    #@27
    iget v6, p0, Lcom/android/server/BatteryService$Led;->mBatteryLedOff:I

    #@29
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/android/server/LightsService$Light;->setFlashing(IIII)V

    #@2c
    goto :goto_1f

    #@2d
    .line 886
    :cond_2d
    if-eq v1, v3, :cond_31

    #@2f
    if-ne v1, v4, :cond_47

    #@31
    .line 888
    :cond_31
    if-eq v1, v4, :cond_37

    #@33
    const/16 v2, 0x5a

    #@35
    if-lt v0, v2, :cond_3f

    #@37
    .line 890
    :cond_37
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->mBatteryLight:Lcom/android/server/LightsService$Light;

    #@39
    iget v3, p0, Lcom/android/server/BatteryService$Led;->mBatteryFullARGB:I

    #@3b
    invoke-virtual {v2, v3}, Lcom/android/server/LightsService$Light;->setColor(I)V

    #@3e
    goto :goto_1f

    #@3f
    .line 893
    :cond_3f
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->mBatteryLight:Lcom/android/server/LightsService$Light;

    #@41
    iget v3, p0, Lcom/android/server/BatteryService$Led;->mBatteryMediumARGB:I

    #@43
    invoke-virtual {v2, v3}, Lcom/android/server/LightsService$Light;->setColor(I)V

    #@46
    goto :goto_1f

    #@47
    .line 897
    :cond_47
    iget-object v2, p0, Lcom/android/server/BatteryService$Led;->mBatteryLight:Lcom/android/server/LightsService$Light;

    #@49
    invoke-virtual {v2}, Lcom/android/server/LightsService$Light;->turnOff()V

    #@4c
    goto :goto_1f
.end method
