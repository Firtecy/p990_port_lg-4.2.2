.class public Lcom/android/server/updates/ConfigUpdateInstallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConfigUpdateInstallReceiver.java"


# static fields
.field private static final EXTRA_CONTENT_PATH:Ljava/lang/String; = "CONTENT_PATH"

.field private static final EXTRA_REQUIRED_HASH:Ljava/lang/String; = "REQUIRED_HASH"

.field private static final EXTRA_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field private static final EXTRA_VERSION_NUMBER:Ljava/lang/String; = "VERSION"

.field private static final TAG:Ljava/lang/String; = "ConfigUpdateInstallReceiver"

.field private static final UPDATE_CERTIFICATE_KEY:Ljava/lang/String; = "config_update_certificate"


# instance fields
.field private final updateContent:Ljava/io/File;

.field private final updateDir:Ljava/io/File;

.field private final updateVersion:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "updateDir"
    .parameter "updateContentPath"
    .parameter "updateMetadataPath"
    .parameter "updateVersionPath"

    #@0
    .prologue
    .line 64
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 65
    new-instance v1, Ljava/io/File;

    #@5
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8
    iput-object v1, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateDir:Ljava/io/File;

    #@a
    .line 66
    new-instance v1, Ljava/io/File;

    #@c
    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    iput-object v1, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateContent:Ljava/io/File;

    #@11
    .line 67
    new-instance v0, Ljava/io/File;

    #@13
    invoke-direct {v0, p1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 68
    .local v0, updateMetadataDir:Ljava/io/File;
    new-instance v1, Ljava/io/File;

    #@18
    invoke-direct {v1, v0, p4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1b
    iput-object v1, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateVersion:Ljava/io/File;

    #@1d
    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/ContentResolver;)Ljava/security/cert/X509Certificate;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getCert(Landroid/content/ContentResolver;)Ljava/security/cert/X509Certificate;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getAltContent(Landroid/content/Intent;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p5}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->verifySignature(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Ljava/lang/String;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->install(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getVersionFromIntent(Landroid/content/Intent;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getRequiredHashFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getSignatureFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/updates/ConfigUpdateInstallReceiver;)I
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getCurrentVersion()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/updates/ConfigUpdateInstallReceiver;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getCurrentContent()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-static {p0}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getCurrentHash(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/updates/ConfigUpdateInstallReceiver;II)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->verifyVersion(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->verifyPreviousHash(Ljava/lang/String;Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getAltContent(Landroid/content/Intent;)Ljava/lang/String;
    .registers 4
    .parameter "i"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->getContentFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Llibcore/io/IoUtils;->readFileAsString(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 177
    .local v0, contents:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method private getCert(Landroid/content/ContentResolver;)Ljava/security/cert/X509Certificate;
    .registers 9
    .parameter "cr"

    #@0
    .prologue
    .line 121
    const-string v5, "config_update_certificate"

    #@2
    invoke-static {p1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 124
    .local v0, cert:Ljava/lang/String;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@9
    move-result-object v5

    #@a
    const/4 v6, 0x0

    #@b
    invoke-static {v5, v6}, Landroid/util/Base64;->decode([BI)[B

    #@e
    move-result-object v2

    #@f
    .line 125
    .local v2, derCert:[B
    new-instance v4, Ljava/io/ByteArrayInputStream;

    #@11
    invoke-direct {v4, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@14
    .line 126
    .local v4, istream:Ljava/io/InputStream;
    const-string v5, "X.509"

    #@16
    invoke-static {v5}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@19
    move-result-object v1

    #@1a
    .line 127
    .local v1, cf:Ljava/security/cert/CertificateFactory;
    invoke-virtual {v1, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@1d
    move-result-object v5

    #@1e
    check-cast v5, Ljava/security/cert/X509Certificate;
    :try_end_20
    .catch Ljava/security/cert/CertificateException; {:try_start_6 .. :try_end_20} :catch_21

    #@20
    return-object v5

    #@21
    .line 128
    .end local v1           #cf:Ljava/security/cert/CertificateFactory;
    .end local v2           #derCert:[B
    .end local v4           #istream:Ljava/io/InputStream;
    :catch_21
    move-exception v3

    #@22
    .line 129
    .local v3, e:Ljava/security/cert/CertificateException;
    new-instance v5, Ljava/lang/IllegalStateException;

    #@24
    const-string v6, "Got malformed certificate from settings, ignoring"

    #@26
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@29
    throw v5
.end method

.method private getContentFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .registers 5
    .parameter "i"

    #@0
    .prologue
    .line 134
    const-string v1, "CONTENT_PATH"

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 135
    .local v0, extraValue:Ljava/lang/String;
    if-nez v0, :cond_10

    #@8
    .line 136
    new-instance v1, Ljava/lang/IllegalStateException;

    #@a
    const-string v2, "Missing required content path, ignoring."

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 138
    :cond_10
    return-object v0
.end method

.method private getCurrentContent()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 182
    :try_start_0
    iget-object v1, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateContent:Ljava/io/File;

    #@2
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Llibcore/io/IoUtils;->readFileAsString(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 185
    :goto_e
    return-object v1

    #@f
    .line 183
    :catch_f
    move-exception v0

    #@10
    .line 184
    .local v0, e:Ljava/io/IOException;
    const-string v1, "ConfigUpdateInstallReceiver"

    #@12
    const-string v2, "Failed to read current content, assuming first update!"

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 185
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method private static getCurrentHash(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "content"

    #@0
    .prologue
    .line 190
    if-nez p0, :cond_5

    #@2
    .line 191
    const-string v4, "0"

    #@4
    .line 197
    :goto_4
    return-object v4

    #@5
    .line 194
    :cond_5
    :try_start_5
    const-string v4, "SHA512"

    #@7
    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    #@a
    move-result-object v0

    #@b
    .line 195
    .local v0, dgst:Ljava/security/MessageDigest;
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    #@e
    move-result-object v2

    #@f
    .line 196
    .local v2, encoded:[B
    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->digest([B)[B

    #@12
    move-result-object v3

    #@13
    .line 197
    .local v3, fingerprint:[B
    const/4 v4, 0x0

    #@14
    invoke-static {v3, v4}, Ljava/lang/IntegralToString;->bytesToHexString([BZ)Ljava/lang/String;
    :try_end_17
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_5 .. :try_end_17} :catch_19

    #@17
    move-result-object v4

    #@18
    goto :goto_4

    #@19
    .line 198
    .end local v0           #dgst:Ljava/security/MessageDigest;
    .end local v2           #encoded:[B
    .end local v3           #fingerprint:[B
    :catch_19
    move-exception v1

    #@1a
    .line 199
    .local v1, e:Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/AssertionError;

    #@1c
    invoke-direct {v4, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@1f
    throw v4
.end method

.method private getCurrentVersion()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 167
    :try_start_0
    iget-object v2, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateVersion:Ljava/io/File;

    #@2
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Llibcore/io/IoUtils;->readFileAsString(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 168
    .local v1, strVersion:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_11} :catch_13

    #@11
    move-result v2

    #@12
    .line 171
    .end local v1           #strVersion:Ljava/lang/String;
    :goto_12
    return v2

    #@13
    .line 169
    :catch_13
    move-exception v0

    #@14
    .line 170
    .local v0, e:Ljava/io/IOException;
    const-string v2, "ConfigUpdateInstallReceiver"

    #@16
    const-string v3, "Couldn\'t find current metadata, assuming first update"

    #@18
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 171
    const/4 v2, 0x0

    #@1c
    goto :goto_12
.end method

.method private getRequiredHashFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .registers 5
    .parameter "i"

    #@0
    .prologue
    .line 150
    const-string v1, "REQUIRED_HASH"

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 151
    .local v0, extraValue:Ljava/lang/String;
    if-nez v0, :cond_10

    #@8
    .line 152
    new-instance v1, Ljava/lang/IllegalStateException;

    #@a
    const-string v2, "Missing required previous hash, ignoring."

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 154
    :cond_10
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    return-object v1
.end method

.method private getSignatureFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .registers 5
    .parameter "i"

    #@0
    .prologue
    .line 158
    const-string v1, "SIGNATURE"

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 159
    .local v0, extraValue:Ljava/lang/String;
    if-nez v0, :cond_10

    #@8
    .line 160
    new-instance v1, Ljava/lang/IllegalStateException;

    #@a
    const-string v2, "Missing required signature, ignoring."

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 162
    :cond_10
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    return-object v1
.end method

.method private getVersionFromIntent(Landroid/content/Intent;)I
    .registers 5
    .parameter "i"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    const-string v1, "VERSION"

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 143
    .local v0, extraValue:Ljava/lang/String;
    if-nez v0, :cond_10

    #@8
    .line 144
    new-instance v1, Ljava/lang/IllegalStateException;

    #@a
    const-string v2, "Missing required version number, ignoring."

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 146
    :cond_10
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@17
    move-result v1

    #@18
    return v1
.end method

.method private install(Ljava/lang/String;I)V
    .registers 7
    .parameter "content"
    .parameter "version"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateDir:Ljava/io/File;

    #@2
    iget-object v1, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateContent:Ljava/io/File;

    #@4
    invoke-direct {p0, v0, v1, p1}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->writeUpdate(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    #@7
    .line 260
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateDir:Ljava/io/File;

    #@9
    iget-object v1, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->updateVersion:Ljava/io/File;

    #@b
    int-to-long v2, p2

    #@c
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->writeUpdate(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    #@13
    .line 261
    return-void
.end method

.method private verifyPreviousHash(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "current"
    .parameter "required"

    #@0
    .prologue
    .line 209
    const-string v0, "NONE"

    #@2
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 210
    const/4 v0, 0x1

    #@9
    .line 213
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method private verifySignature(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z
    .registers 9
    .parameter "content"
    .parameter "version"
    .parameter "requiredPrevious"
    .parameter "signature"
    .parameter "cert"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 218
    const-string v1, "SHA512withRSA"

    #@2
    invoke-static {v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    #@5
    move-result-object v0

    #@6
    .line 219
    .local v0, signer:Ljava/security/Signature;
    invoke-virtual {v0, p5}, Ljava/security/Signature;->initVerify(Ljava/security/cert/Certificate;)V

    #@9
    .line 220
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    #@10
    .line 221
    int-to-long v1, p2

    #@11
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    #@1c
    .line 222
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    #@23
    .line 223
    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    #@26
    move-result-object v1

    #@27
    const/4 v2, 0x0

    #@28
    invoke-static {v1, v2}, Landroid/util/Base64;->decode([BI)[B

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    #@2f
    move-result v1

    #@30
    return v1
.end method

.method private verifyVersion(II)Z
    .registers 4
    .parameter "current"
    .parameter "alternative"

    #@0
    .prologue
    .line 204
    if-ge p1, p2, :cond_4

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method

.method private writeUpdate(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    .registers 11
    .parameter "dir"
    .parameter "file"
    .parameter "content"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 227
    const/4 v0, 0x0

    #@1
    .line 228
    .local v0, out:Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    #@2
    .line 231
    .local v3, tmp:Ljava/io/File;
    :try_start_2
    const-string v4, "journal"

    #@4
    const-string v5, ""

    #@6
    invoke-static {v4, v5, p1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    #@9
    move-result-object v3

    #@a
    .line 233
    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@d
    move-result-object v2

    #@e
    .line 234
    .local v2, parent:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    #@11
    .line 236
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_3e

    #@17
    .line 237
    new-instance v4, Ljava/io/IOException;

    #@19
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v6, "Failed to create directory "

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@33
    throw v4
    :try_end_34
    .catchall {:try_start_2 .. :try_end_34} :catchall_34

    #@34
    .line 251
    .end local v2           #parent:Ljava/io/File;
    :catchall_34
    move-exception v4

    #@35
    :goto_35
    if-eqz v3, :cond_3a

    #@37
    .line 252
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@3a
    .line 254
    :cond_3a
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@3d
    .line 251
    throw v4

    #@3e
    .line 240
    .restart local v2       #parent:Ljava/io/File;
    :cond_3e
    const/4 v4, 0x1

    #@3f
    const/4 v5, 0x0

    #@40
    :try_start_40
    invoke-virtual {v3, v4, v5}, Ljava/io/File;->setReadable(ZZ)Z

    #@43
    .line 242
    new-instance v1, Ljava/io/FileOutputStream;

    #@45
    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_48
    .catchall {:try_start_40 .. :try_end_48} :catchall_34

    #@48
    .line 243
    .end local v0           #out:Ljava/io/FileOutputStream;
    .local v1, out:Ljava/io/FileOutputStream;
    :try_start_48
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write([B)V

    #@4f
    .line 245
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Ljava/io/FileDescriptor;->sync()V

    #@56
    .line 247
    invoke-virtual {v3, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@59
    move-result v4

    #@5a
    if-nez v4, :cond_7c

    #@5c
    .line 248
    new-instance v4, Ljava/io/IOException;

    #@5e
    new-instance v5, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v6, "Failed to atomically rename "

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {p2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v5

    #@75
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@78
    throw v4
    :try_end_79
    .catchall {:try_start_48 .. :try_end_79} :catchall_79

    #@79
    .line 251
    :catchall_79
    move-exception v4

    #@7a
    move-object v0, v1

    #@7b
    .end local v1           #out:Ljava/io/FileOutputStream;
    .restart local v0       #out:Ljava/io/FileOutputStream;
    goto :goto_35

    #@7c
    .end local v0           #out:Ljava/io/FileOutputStream;
    .restart local v1       #out:Ljava/io/FileOutputStream;
    :cond_7c
    if-eqz v3, :cond_81

    #@7e
    .line 252
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@81
    .line 254
    :cond_81
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@84
    .line 256
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 4
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 73
    new-instance v0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;

    #@2
    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;-><init>(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    #@5
    invoke-virtual {v0}, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->start()V

    #@8
    .line 117
    return-void
.end method
