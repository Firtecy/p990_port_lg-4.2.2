.class Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;
.super Ljava/lang/Thread;
.source "ConfigUpdateInstallReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/updates/ConfigUpdateInstallReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@2
    iput-object p2, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->val$context:Landroid/content/Context;

    #@4
    iput-object p3, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->val$intent:Landroid/content/Intent;

    #@6
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 13

    #@0
    .prologue
    const v11, 0xc864

    #@3
    .line 78
    :try_start_3
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@5
    iget-object v10, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->val$context:Landroid/content/Context;

    #@7
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v10

    #@b
    invoke-static {v0, v10}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$000(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/ContentResolver;)Ljava/security/cert/X509Certificate;

    #@e
    move-result-object v5

    #@f
    .line 80
    .local v5, cert:Ljava/security/cert/X509Certificate;
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@11
    iget-object v10, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->val$intent:Landroid/content/Intent;

    #@13
    invoke-static {v0, v10}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$100(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    .line 82
    .local v1, altContent:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@19
    iget-object v10, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->val$intent:Landroid/content/Intent;

    #@1b
    invoke-static {v0, v10}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$200(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)I

    #@1e
    move-result v2

    #@1f
    .line 84
    .local v2, altVersion:I
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@21
    iget-object v10, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->val$intent:Landroid/content/Intent;

    #@23
    invoke-static {v0, v10}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$300(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    .line 86
    .local v3, altRequiredHash:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@29
    iget-object v10, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->val$intent:Landroid/content/Intent;

    #@2b
    invoke-static {v0, v10}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$400(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Landroid/content/Intent;)Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    .line 88
    .local v4, altSig:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@31
    invoke-static {v0}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$500(Lcom/android/server/updates/ConfigUpdateInstallReceiver;)I

    #@34
    move-result v7

    #@35
    .line 90
    .local v7, currentVersion:I
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@37
    invoke-static {v0}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$600(Lcom/android/server/updates/ConfigUpdateInstallReceiver;)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-static {v0}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$700(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v6

    #@3f
    .line 91
    .local v6, currentHash:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@41
    invoke-static {v0, v7, v2}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$800(Lcom/android/server/updates/ConfigUpdateInstallReceiver;II)Z

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_4f

    #@47
    .line 92
    const-string v0, "ConfigUpdateInstallReceiver"

    #@49
    const-string v10, "Not installing, new version is <= current version"

    #@4b
    invoke-static {v0, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 115
    .end local v1           #altContent:Ljava/lang/String;
    .end local v2           #altVersion:I
    .end local v3           #altRequiredHash:Ljava/lang/String;
    .end local v4           #altSig:Ljava/lang/String;
    .end local v5           #cert:Ljava/security/cert/X509Certificate;
    .end local v6           #currentHash:Ljava/lang/String;
    .end local v7           #currentVersion:I
    :goto_4e
    return-void

    #@4f
    .line 93
    .restart local v1       #altContent:Ljava/lang/String;
    .restart local v2       #altVersion:I
    .restart local v3       #altRequiredHash:Ljava/lang/String;
    .restart local v4       #altSig:Ljava/lang/String;
    .restart local v5       #cert:Ljava/security/cert/X509Certificate;
    .restart local v6       #currentHash:Ljava/lang/String;
    .restart local v7       #currentVersion:I
    :cond_4f
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@51
    invoke-static {v0, v6, v3}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$900(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Ljava/lang/String;Ljava/lang/String;)Z

    #@54
    move-result v0

    #@55
    if-nez v0, :cond_7f

    #@57
    .line 94
    const v0, 0xc864

    #@5a
    const-string v10, "Current hash did not match required value"

    #@5c
    invoke-static {v0, v10}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_5f} :catch_60

    #@5f
    goto :goto_4e

    #@60
    .line 106
    .end local v1           #altContent:Ljava/lang/String;
    .end local v2           #altVersion:I
    .end local v3           #altRequiredHash:Ljava/lang/String;
    .end local v4           #altSig:Ljava/lang/String;
    .end local v5           #cert:Ljava/security/cert/X509Certificate;
    .end local v6           #currentHash:Ljava/lang/String;
    .end local v7           #currentVersion:I
    :catch_60
    move-exception v8

    #@61
    .line 107
    .local v8, e:Ljava/lang/Exception;
    const-string v0, "ConfigUpdateInstallReceiver"

    #@63
    const-string v10, "Could not update content!"

    #@65
    invoke-static {v0, v10, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@68
    .line 109
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@6b
    move-result-object v9

    #@6c
    .line 110
    .local v9, errMsg:Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@6f
    move-result v0

    #@70
    const/16 v10, 0x64

    #@72
    if-le v0, v10, :cond_7b

    #@74
    .line 111
    const/4 v0, 0x0

    #@75
    const/16 v10, 0x63

    #@77
    invoke-virtual {v9, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7a
    move-result-object v9

    #@7b
    .line 113
    :cond_7b
    invoke-static {v11, v9}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@7e
    goto :goto_4e

    #@7f
    .line 96
    .end local v8           #e:Ljava/lang/Exception;
    .end local v9           #errMsg:Ljava/lang/String;
    .restart local v1       #altContent:Ljava/lang/String;
    .restart local v2       #altVersion:I
    .restart local v3       #altRequiredHash:Ljava/lang/String;
    .restart local v4       #altSig:Ljava/lang/String;
    .restart local v5       #cert:Ljava/security/cert/X509Certificate;
    .restart local v6       #currentHash:Ljava/lang/String;
    .restart local v7       #currentVersion:I
    :cond_7f
    :try_start_7f
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@81
    invoke-static/range {v0 .. v5}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$1000(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    #@84
    move-result v0

    #@85
    if-nez v0, :cond_90

    #@87
    .line 98
    const v0, 0xc864

    #@8a
    const-string v10, "Signature did not verify"

    #@8c
    invoke-static {v0, v10}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@8f
    goto :goto_4e

    #@90
    .line 102
    :cond_90
    const-string v0, "ConfigUpdateInstallReceiver"

    #@92
    const-string v10, "Found new update, installing..."

    #@94
    invoke-static {v0, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 103
    iget-object v0, p0, Lcom/android/server/updates/ConfigUpdateInstallReceiver$1;->this$0:Lcom/android/server/updates/ConfigUpdateInstallReceiver;

    #@99
    invoke-static {v0, v1, v2}, Lcom/android/server/updates/ConfigUpdateInstallReceiver;->access$1100(Lcom/android/server/updates/ConfigUpdateInstallReceiver;Ljava/lang/String;I)V

    #@9c
    .line 104
    const-string v0, "ConfigUpdateInstallReceiver"

    #@9e
    const-string v10, "Installation successful"

    #@a0
    invoke-static {v0, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a3
    .catch Ljava/lang/Exception; {:try_start_7f .. :try_end_a3} :catch_60

    #@a3
    goto :goto_4e
.end method
