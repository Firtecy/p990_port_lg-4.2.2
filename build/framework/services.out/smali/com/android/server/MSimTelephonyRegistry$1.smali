.class Lcom/android/server/MSimTelephonyRegistry$1;
.super Landroid/os/Handler;
.source "MSimTelephonyRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MSimTelephonyRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MSimTelephonyRegistry;


# direct methods
.method constructor <init>(Lcom/android/server/MSimTelephonyRegistry;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 149
    iput-object p1, p0, Lcom/android/server/MSimTelephonyRegistry$1;->this$0:Lcom/android/server/MSimTelephonyRegistry;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 152
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v2, :pswitch_data_3c

    #@5
    .line 162
    :cond_5
    return-void

    #@6
    .line 154
    :pswitch_6
    const-string v2, "MSimTelephonyRegistry"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "MSG_USER_SWITCHED userId="

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 155
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@27
    move-result v0

    #@28
    .line 156
    .local v0, numPhones:I
    const/4 v1, 0x0

    #@29
    .local v1, sub:I
    :goto_29
    if-ge v1, v0, :cond_5

    #@2b
    .line 157
    iget-object v2, p0, Lcom/android/server/MSimTelephonyRegistry$1;->this$0:Lcom/android/server/MSimTelephonyRegistry;

    #@2d
    iget-object v3, p0, Lcom/android/server/MSimTelephonyRegistry$1;->this$0:Lcom/android/server/MSimTelephonyRegistry;

    #@2f
    invoke-static {v3}, Lcom/android/server/MSimTelephonyRegistry;->access$000(Lcom/android/server/MSimTelephonyRegistry;)[Landroid/os/Bundle;

    #@32
    move-result-object v3

    #@33
    aget-object v3, v3, v1

    #@35
    invoke-virtual {v2, v3, v1}, Lcom/android/server/MSimTelephonyRegistry;->notifyCellLocation(Landroid/os/Bundle;I)V

    #@38
    .line 156
    add-int/lit8 v1, v1, 0x1

    #@3a
    goto :goto_29

    #@3b
    .line 152
    nop

    #@3c
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method
