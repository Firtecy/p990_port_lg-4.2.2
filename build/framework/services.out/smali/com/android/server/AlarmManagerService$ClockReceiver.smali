.class Lcom/android/server/AlarmManagerService$ClockReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AlarmManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClockReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/AlarmManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/AlarmManagerService;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 1013
    iput-object p1, p0, Lcom/android/server/AlarmManagerService$ClockReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 1014
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    .line 1015
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 1016
    const-string v1, "android.intent.action.DATE_CHANGED"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 1017
    invoke-static {p1}, Lcom/android/server/AlarmManagerService;->access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1b
    .line 1018
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1022
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    const-string v3, "android.intent.action.TIME_TICK"

    #@6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_10

    #@c
    .line 1023
    invoke-virtual {p0}, Lcom/android/server/AlarmManagerService$ClockReceiver;->scheduleTimeTickEvent()V

    #@f
    .line 1034
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1024
    :cond_10
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    const-string v3, "android.intent.action.DATE_CHANGED"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_f

    #@1c
    .line 1029
    const-string v2, "persist.sys.timezone"

    #@1e
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@25
    move-result-object v1

    #@26
    .line 1030
    .local v1, zone:Ljava/util/TimeZone;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@29
    move-result-wide v2

    #@2a
    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    #@2d
    move-result v0

    #@2e
    .line 1031
    .local v0, gmtOffset:I
    iget-object v2, p0, Lcom/android/server/AlarmManagerService$ClockReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@30
    iget-object v3, p0, Lcom/android/server/AlarmManagerService$ClockReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@32
    invoke-static {v3}, Lcom/android/server/AlarmManagerService;->access$100(Lcom/android/server/AlarmManagerService;)I

    #@35
    move-result v3

    #@36
    const v4, 0xea60

    #@39
    div-int v4, v0, v4

    #@3b
    neg-int v4, v4

    #@3c
    invoke-static {v2, v3, v4}, Lcom/android/server/AlarmManagerService;->access$1900(Lcom/android/server/AlarmManagerService;II)I

    #@3f
    .line 1032
    invoke-virtual {p0}, Lcom/android/server/AlarmManagerService$ClockReceiver;->scheduleDateChangedEvent()V

    #@42
    goto :goto_f
.end method

.method public scheduleDateChangedEvent()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1056
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@5
    move-result-object v0

    #@6
    .line 1057
    .local v0, calendar:Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v1

    #@a
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@d
    .line 1058
    const/16 v1, 0xa

    #@f
    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    #@12
    .line 1059
    const/16 v1, 0xc

    #@14
    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    #@17
    .line 1060
    const/16 v1, 0xd

    #@19
    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    #@1c
    .line 1061
    const/16 v1, 0xe

    #@1e
    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    #@21
    .line 1062
    const/4 v1, 0x5

    #@22
    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->add(II)V

    #@25
    .line 1064
    iget-object v1, p0, Lcom/android/server/AlarmManagerService$ClockReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@27
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@2a
    move-result-wide v2

    #@2b
    iget-object v4, p0, Lcom/android/server/AlarmManagerService$ClockReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@2d
    invoke-static {v4}, Lcom/android/server/AlarmManagerService;->access$2000(Lcom/android/server/AlarmManagerService;)Landroid/app/PendingIntent;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v1, v5, v2, v3, v4}, Lcom/android/server/AlarmManagerService;->set(IJLandroid/app/PendingIntent;)V

    #@34
    .line 1065
    return-void
.end method

.method public scheduleTimeTickEvent()V
    .registers 11

    #@0
    .prologue
    const/16 v8, 0xe

    #@2
    const/16 v7, 0xd

    #@4
    .line 1037
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@7
    move-result-object v0

    #@8
    .line 1038
    .local v0, calendar:Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@b
    move-result-wide v1

    #@c
    .line 1039
    .local v1, currentTime:J
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@f
    .line 1040
    const/16 v5, 0xc

    #@11
    const/4 v6, 0x1

    #@12
    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    #@15
    .line 1044
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    #@18
    move-result v5

    #@19
    neg-int v5, v5

    #@1a
    invoke-virtual {v0, v7, v5}, Ljava/util/Calendar;->add(II)V

    #@1d
    .line 1045
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    #@20
    move-result v5

    #@21
    neg-int v5, v5

    #@22
    invoke-virtual {v0, v8, v5}, Ljava/util/Calendar;->add(II)V

    #@25
    .line 1049
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@28
    move-result-wide v5

    #@29
    sub-long v3, v5, v1

    #@2b
    .line 1051
    .local v3, tickEventDelay:J
    iget-object v5, p0, Lcom/android/server/AlarmManagerService$ClockReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@2d
    const/4 v6, 0x3

    #@2e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@31
    move-result-wide v7

    #@32
    add-long/2addr v7, v3

    #@33
    iget-object v9, p0, Lcom/android/server/AlarmManagerService$ClockReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@35
    invoke-static {v9}, Lcom/android/server/AlarmManagerService;->access$300(Lcom/android/server/AlarmManagerService;)Landroid/app/PendingIntent;

    #@38
    move-result-object v9

    #@39
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/android/server/AlarmManagerService;->set(IJLandroid/app/PendingIntent;)V

    #@3c
    .line 1053
    return-void
.end method
