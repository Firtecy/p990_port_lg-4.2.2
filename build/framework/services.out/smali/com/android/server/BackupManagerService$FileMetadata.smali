.class Lcom/android/server/BackupManagerService$FileMetadata;
.super Ljava/lang/Object;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FileMetadata"
.end annotation


# instance fields
.field domain:Ljava/lang/String;

.field installerPackageName:Ljava/lang/String;

.field mode:J

.field mtime:J

.field packageName:Ljava/lang/String;

.field path:Ljava/lang/String;

.field size:J

.field type:I


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2874
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x2c

    #@2
    .line 2886
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const/16 v1, 0x80

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    .line 2887
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "FileMetadata{"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 2888
    iget-object v1, p0, Lcom/android/server/BackupManagerService$FileMetadata;->packageName:Ljava/lang/String;

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    .line 2889
    iget v1, p0, Lcom/android/server/BackupManagerService$FileMetadata;->type:I

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e
    .line 2890
    iget-object v1, p0, Lcom/android/server/BackupManagerService$FileMetadata;->domain:Ljava/lang/String;

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    const/16 v1, 0x3a

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    iget-object v1, p0, Lcom/android/server/BackupManagerService$FileMetadata;->path:Ljava/lang/String;

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@30
    .line 2891
    iget-wide v1, p0, Lcom/android/server/BackupManagerService$FileMetadata;->size:J

    #@32
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@35
    .line 2892
    const/16 v1, 0x7d

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3a
    .line 2893
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    return-object v1
.end method
