.class Lcom/android/server/CertBlacklister$BlacklistObserver$1;
.super Ljava/lang/Thread;
.source "CertBlacklister.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/CertBlacklister$BlacklistObserver;->writeBlacklist()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/CertBlacklister$BlacklistObserver;


# direct methods
.method constructor <init>(Lcom/android/server/CertBlacklister$BlacklistObserver;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/server/CertBlacklister$BlacklistObserver$1;->this$0:Lcom/android/server/CertBlacklister$BlacklistObserver;

    #@2
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    .line 79
    iget-object v5, p0, Lcom/android/server/CertBlacklister$BlacklistObserver$1;->this$0:Lcom/android/server/CertBlacklister$BlacklistObserver;

    #@2
    invoke-static {v5}, Lcom/android/server/CertBlacklister$BlacklistObserver;->access$000(Lcom/android/server/CertBlacklister$BlacklistObserver;)Ljava/io/File;

    #@5
    move-result-object v6

    #@6
    monitor-enter v6

    #@7
    .line 80
    :try_start_7
    iget-object v5, p0, Lcom/android/server/CertBlacklister$BlacklistObserver$1;->this$0:Lcom/android/server/CertBlacklister$BlacklistObserver;

    #@9
    invoke-virtual {v5}, Lcom/android/server/CertBlacklister$BlacklistObserver;->getValue()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 81
    .local v0, blacklist:Ljava/lang/String;
    if-eqz v0, :cond_51

    #@f
    .line 82
    const-string v5, "CertBlacklister"

    #@11
    const-string v7, "Certificate blacklist changed, updating..."

    #@13
    invoke-static {v5, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_5f

    #@16
    .line 83
    const/4 v2, 0x0

    #@17
    .line 86
    .local v2, out:Ljava/io/FileOutputStream;
    :try_start_17
    const-string v5, "journal"

    #@19
    const-string v7, ""

    #@1b
    iget-object v8, p0, Lcom/android/server/CertBlacklister$BlacklistObserver$1;->this$0:Lcom/android/server/CertBlacklister$BlacklistObserver;

    #@1d
    invoke-static {v8}, Lcom/android/server/CertBlacklister$BlacklistObserver;->access$000(Lcom/android/server/CertBlacklister$BlacklistObserver;)Ljava/io/File;

    #@20
    move-result-object v8

    #@21
    invoke-static {v5, v7, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    #@24
    move-result-object v4

    #@25
    .line 88
    .local v4, tmp:Ljava/io/File;
    const/4 v5, 0x1

    #@26
    const/4 v7, 0x0

    #@27
    invoke-virtual {v4, v5, v7}, Ljava/io/File;->setReadable(ZZ)Z

    #@2a
    .line 90
    new-instance v3, Ljava/io/FileOutputStream;

    #@2c
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2f
    .catchall {:try_start_17 .. :try_end_2f} :catchall_62
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_2f} :catch_53

    #@2f
    .line 91
    .end local v2           #out:Ljava/io/FileOutputStream;
    .local v3, out:Ljava/io/FileOutputStream;
    :try_start_2f
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v3, v5}, Ljava/io/FileOutputStream;->write([B)V

    #@36
    .line 93
    invoke-static {v3}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@39
    .line 95
    new-instance v5, Ljava/io/File;

    #@3b
    iget-object v7, p0, Lcom/android/server/CertBlacklister$BlacklistObserver$1;->this$0:Lcom/android/server/CertBlacklister$BlacklistObserver;

    #@3d
    invoke-static {v7}, Lcom/android/server/CertBlacklister$BlacklistObserver;->access$100(Lcom/android/server/CertBlacklister$BlacklistObserver;)Ljava/lang/String;

    #@40
    move-result-object v7

    #@41
    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@44
    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@47
    .line 96
    const-string v5, "CertBlacklister"

    #@49
    const-string v7, "Certificate blacklist updated"

    #@4b
    invoke-static {v5, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4e
    .catchall {:try_start_2f .. :try_end_4e} :catchall_67
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_4e} :catch_6a

    #@4e
    .line 100
    :try_start_4e
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@51
    .line 103
    .end local v3           #out:Ljava/io/FileOutputStream;
    .end local v4           #tmp:Ljava/io/File;
    :cond_51
    :goto_51
    monitor-exit v6
    :try_end_52
    .catchall {:try_start_4e .. :try_end_52} :catchall_5f

    #@52
    .line 104
    return-void

    #@53
    .line 97
    .restart local v2       #out:Ljava/io/FileOutputStream;
    :catch_53
    move-exception v1

    #@54
    .line 98
    .local v1, e:Ljava/io/IOException;
    :goto_54
    :try_start_54
    const-string v5, "CertBlacklister"

    #@56
    const-string v7, "Failed to write blacklist"

    #@58
    invoke-static {v5, v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5b
    .catchall {:try_start_54 .. :try_end_5b} :catchall_62

    #@5b
    .line 100
    :try_start_5b
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@5e
    goto :goto_51

    #@5f
    .line 103
    .end local v0           #blacklist:Ljava/lang/String;
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #out:Ljava/io/FileOutputStream;
    :catchall_5f
    move-exception v5

    #@60
    monitor-exit v6
    :try_end_61
    .catchall {:try_start_5b .. :try_end_61} :catchall_5f

    #@61
    throw v5

    #@62
    .line 100
    .restart local v0       #blacklist:Ljava/lang/String;
    .restart local v2       #out:Ljava/io/FileOutputStream;
    :catchall_62
    move-exception v5

    #@63
    :goto_63
    :try_start_63
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@66
    throw v5
    :try_end_67
    .catchall {:try_start_63 .. :try_end_67} :catchall_5f

    #@67
    .end local v2           #out:Ljava/io/FileOutputStream;
    .restart local v3       #out:Ljava/io/FileOutputStream;
    .restart local v4       #tmp:Ljava/io/File;
    :catchall_67
    move-exception v5

    #@68
    move-object v2, v3

    #@69
    .end local v3           #out:Ljava/io/FileOutputStream;
    .restart local v2       #out:Ljava/io/FileOutputStream;
    goto :goto_63

    #@6a
    .line 97
    .end local v2           #out:Ljava/io/FileOutputStream;
    .restart local v3       #out:Ljava/io/FileOutputStream;
    :catch_6a
    move-exception v1

    #@6b
    move-object v2, v3

    #@6c
    .end local v3           #out:Ljava/io/FileOutputStream;
    .restart local v2       #out:Ljava/io/FileOutputStream;
    goto :goto_54
.end method
