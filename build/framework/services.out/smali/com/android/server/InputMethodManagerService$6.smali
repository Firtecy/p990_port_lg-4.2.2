.class Lcom/android/server/InputMethodManagerService$6;
.super Ljava/lang/Object;
.source "InputMethodManagerService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/InputMethodManagerService;->showInputMethodMenuInternal(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/InputMethodManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/InputMethodManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2733
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 8
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 2736
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@2
    iget-object v3, v2, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@4
    monitor-enter v3

    #@5
    .line 2737
    :try_start_5
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@7
    invoke-static {v2}, Lcom/android/server/InputMethodManagerService;->access$1200(Lcom/android/server/InputMethodManagerService;)[Landroid/view/inputmethod/InputMethodInfo;

    #@a
    move-result-object v2

    #@b
    if-eqz v2, :cond_27

    #@d
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@f
    invoke-static {v2}, Lcom/android/server/InputMethodManagerService;->access$1200(Lcom/android/server/InputMethodManagerService;)[Landroid/view/inputmethod/InputMethodInfo;

    #@12
    move-result-object v2

    #@13
    array-length v2, v2

    #@14
    if-le v2, p2, :cond_27

    #@16
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@18
    invoke-static {v2}, Lcom/android/server/InputMethodManagerService;->access$1300(Lcom/android/server/InputMethodManagerService;)[I

    #@1b
    move-result-object v2

    #@1c
    if-eqz v2, :cond_27

    #@1e
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@20
    invoke-static {v2}, Lcom/android/server/InputMethodManagerService;->access$1300(Lcom/android/server/InputMethodManagerService;)[I

    #@23
    move-result-object v2

    #@24
    array-length v2, v2

    #@25
    if-gt v2, p2, :cond_29

    #@27
    .line 2739
    :cond_27
    monitor-exit v3

    #@28
    .line 2752
    :goto_28
    return-void

    #@29
    .line 2741
    :cond_29
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@2b
    invoke-static {v2}, Lcom/android/server/InputMethodManagerService;->access$1200(Lcom/android/server/InputMethodManagerService;)[Landroid/view/inputmethod/InputMethodInfo;

    #@2e
    move-result-object v2

    #@2f
    aget-object v0, v2, p2

    #@31
    .line 2742
    .local v0, im:Landroid/view/inputmethod/InputMethodInfo;
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@33
    invoke-static {v2}, Lcom/android/server/InputMethodManagerService;->access$1300(Lcom/android/server/InputMethodManagerService;)[I

    #@36
    move-result-object v2

    #@37
    aget v1, v2, p2

    #@39
    .line 2743
    .local v1, subtypeId:I
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@3b
    invoke-virtual {v2}, Lcom/android/server/InputMethodManagerService;->hideInputMethodMenu()V

    #@3e
    .line 2744
    if-eqz v0, :cond_52

    #@40
    .line 2745
    if-ltz v1, :cond_48

    #@42
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@45
    move-result v2

    #@46
    if-lt v1, v2, :cond_49

    #@48
    .line 2747
    :cond_48
    const/4 v1, -0x1

    #@49
    .line 2749
    :cond_49
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$6;->this$0:Lcom/android/server/InputMethodManagerService;

    #@4b
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v2, v4, v1}, Lcom/android/server/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    #@52
    .line 2751
    :cond_52
    monitor-exit v3

    #@53
    goto :goto_28

    #@54
    .end local v0           #im:Landroid/view/inputmethod/InputMethodInfo;
    .end local v1           #subtypeId:I
    :catchall_54
    move-exception v2

    #@55
    monitor-exit v3
    :try_end_56
    .catchall {:try_start_5 .. :try_end_56} :catchall_54

    #@56
    throw v2
.end method
