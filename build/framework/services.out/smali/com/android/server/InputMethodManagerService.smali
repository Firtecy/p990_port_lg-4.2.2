.class public Lcom/android/server/InputMethodManagerService;
.super Lcom/android/internal/view/IInputMethodManager$Stub;
.source "InputMethodManagerService.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/InputMethodManagerService$InputMethodFileManager;,
        Lcom/android/server/InputMethodManagerService$InputMethodSettings;,
        Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;,
        Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;,
        Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;,
        Lcom/android/server/InputMethodManagerService$HardKeyboardListener;,
        Lcom/android/server/InputMethodManagerService$MethodCallback;,
        Lcom/android/server/InputMethodManagerService$MyPackageMonitor;,
        Lcom/android/server/InputMethodManagerService$ImmsBroadcastReceiver;,
        Lcom/android/server/InputMethodManagerService$SettingsObserver;,
        Lcom/android/server/InputMethodManagerService$ClientState;,
        Lcom/android/server/InputMethodManagerService$SessionState;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field private static final ENGLISH_LOCALE:Ljava/util/Locale; = null

.field static final MSG_ATTACH_TOKEN:I = 0x410

.field static final MSG_BIND_INPUT:I = 0x3f2

.field static final MSG_BIND_METHOD:I = 0xbc2

.field static final MSG_CREATE_SESSION:I = 0x41a

.field static final MSG_HARD_KEYBOARD_SWITCH_CHANGED:I = 0xfa0

.field static final MSG_HIDE_SOFT_INPUT:I = 0x406

.field static final MSG_RESTART_INPUT:I = 0x7da

.field static final MSG_SCREEN_ON_OFF:I = 0xbd6

.field static final MSG_SET_ACTIVE:I = 0xbcc

.field static final MSG_SHOW_IM_CONFIG:I = 0x4

.field static final MSG_SHOW_IM_PICKER:I = 0x1

.field static final MSG_SHOW_IM_SUBTYPE_ENABLER:I = 0x3

.field static final MSG_SHOW_IM_SUBTYPE_PICKER:I = 0x2

.field static final MSG_SHOW_SOFT_INPUT:I = 0x3fc

.field static final MSG_START_INPUT:I = 0x7d0

.field static final MSG_UNBIND_INPUT:I = 0x3e8

.field static final MSG_UNBIND_METHOD:I = 0xbb8

.field private static final NOT_A_SUBTYPE_ID:I = -0x1

.field private static final NOT_A_SUBTYPE_ID_STR:Ljava/lang/String; = null

.field static final SECURE_SUGGESTION_SPANS_MAX_SIZE:I = 0x14

.field private static final SUBTYPE_MODE_KEYBOARD:Ljava/lang/String; = "keyboard"

.field private static final SUBTYPE_MODE_VOICE:Ljava/lang/String; = "voice"

.field static final TAG:Ljava/lang/String; = "InputMethodManagerService"

.field private static final TAG_ASCII_CAPABLE:Ljava/lang/String; = "AsciiCapable"

.field private static final TAG_ENABLED_WHEN_DEFAULT_IS_NOT_ASCII_CAPABLE:Ljava/lang/String; = "EnabledWhenDefaultIsNotAsciiCapable"

.field private static final TAG_TRY_SUPPRESSING_IME_SWITCHER:Ljava/lang/String; = "TrySuppressingImeSwitcher"

.field static final TIME_TO_RECONNECT:J = 0x2710L


# instance fields
.field mBackDisposition:I

.field mBoundToMethod:Z

.field final mCaller:Lcom/android/internal/os/HandlerCaller;

.field final mClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/InputMethodManagerService$ClientState;",
            ">;"
        }
    .end annotation
.end field

.field final mContext:Landroid/content/Context;

.field mCurAttribute:Landroid/view/inputmethod/EditorInfo;

.field mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

.field mCurFocusedWindow:Landroid/os/IBinder;

.field mCurId:Ljava/lang/String;

.field mCurInputContext:Lcom/android/internal/view/IInputContext;

.field mCurIntent:Landroid/content/Intent;

.field mCurMethod:Lcom/android/internal/view/IInputMethod;

.field mCurMethodId:Ljava/lang/String;

.field mCurSeq:I

.field mCurToken:Landroid/os/IBinder;

.field private mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

.field private mDialogBuilder:Landroid/app/AlertDialog$Builder;

.field mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

.field private mFileManager:Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

.field final mHandler:Landroid/os/Handler;

.field private final mHardKeyboardListener:Lcom/android/server/InputMethodManagerService$HardKeyboardListener;

.field mHaveConnection:Z

.field private final mIPackageManager:Landroid/content/pm/IPackageManager;

.field final mIWindowManager:Landroid/view/IWindowManager;

.field private mImListManager:Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;

.field private final mImeSelectedOnBoot:Z

.field private mImeSwitchPendingIntent:Landroid/app/PendingIntent;

.field private mImeSwitcherNotification:Landroid/app/Notification;

.field mImeWindowVis:I

.field private mIms:[Landroid/view/inputmethod/InputMethodInfo;

.field private mInputBoundToKeyguard:Z

.field mInputShown:Z

.field mIsQuickVoiceUsing:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field mLastBindTime:J

.field private mLastSystemLocale:Ljava/util/Locale;

.field final mMethodList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mMethodMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMyPackageMonitor:Lcom/android/server/InputMethodManagerService$MyPackageMonitor;

.field final mNoBinding:Lcom/android/internal/view/InputBindResult;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mNotificationShown:Z

.field final mRes:Landroid/content/res/Resources;

.field mScreenOn:Z

.field private final mSecureSuggestionSpans:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/text/style/SuggestionSpan;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

.field final mSettingsObserver:Lcom/android/server/InputMethodManagerService$SettingsObserver;

.field private final mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;"
        }
    .end annotation
.end field

.field mShowExplicitlyRequested:Z

.field mShowForced:Z

.field private mShowOngoingImeSwitcherForPhones:Z

.field mShowRequested:Z

.field private mStatusBar:Lcom/android/server/StatusBarManagerService;

.field private mSubtypeIds:[I

.field private mSwitchingDialog:Landroid/app/AlertDialog;

.field private mSwitchingDialogTitleView:Landroid/view/View;

.field mSystemReady:Z

.field mVisibleBound:Z

.field final mVisibleConnection:Landroid/content/ServiceConnection;

.field private final mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 163
    const/4 v0, -0x1

    #@1
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    sput-object v0, Lcom/android/server/InputMethodManagerService;->NOT_A_SUBTYPE_ID_STR:Ljava/lang/String;

    #@7
    .line 170
    new-instance v0, Ljava/util/Locale;

    #@9
    const-string v1, "en"

    #@b
    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    #@e
    sput-object v0, Lcom/android/server/InputMethodManagerService;->ENGLISH_LOCALE:Ljava/util/Locale;

    #@10
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    .registers 15
    .parameter "context"
    .parameter "windowManager"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v11, 0x0

    #@3
    .line 618
    invoke-direct {p0}, Lcom/android/internal/view/IInputMethodManager$Stub;-><init>()V

    #@6
    .line 184
    new-instance v0, Lcom/android/internal/view/InputBindResult;

    #@8
    const/4 v1, -0x1

    #@9
    invoke-direct {v0, v3, v3, v1}, Lcom/android/internal/view/InputBindResult;-><init>(Lcom/android/internal/view/IInputMethodSession;Ljava/lang/String;I)V

    #@c
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mNoBinding:Lcom/android/internal/view/InputBindResult;

    #@e
    .line 188
    new-instance v0, Ljava/util/ArrayList;

    #@10
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@13
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@15
    .line 189
    new-instance v0, Ljava/util/HashMap;

    #@17
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@1c
    .line 190
    new-instance v0, Landroid/util/LruCache;

    #@1e
    const/16 v1, 0x14

    #@20
    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    #@23
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSecureSuggestionSpans:Landroid/util/LruCache;

    #@25
    .line 194
    new-instance v0, Lcom/android/server/InputMethodManagerService$1;

    #@27
    invoke-direct {v0, p0}, Lcom/android/server/InputMethodManagerService$1;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@2a
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mVisibleConnection:Landroid/content/ServiceConnection;

    #@2c
    .line 201
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mVisibleBound:Z

    #@2e
    .line 263
    new-instance v0, Ljava/util/HashMap;

    #@30
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@33
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mClients:Ljava/util/HashMap;

    #@35
    .line 314
    new-instance v0, Ljava/util/HashMap;

    #@37
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@3a
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@3c
    .line 381
    iput-boolean v10, p0, Lcom/android/server/InputMethodManagerService;->mScreenOn:Z

    #@3e
    .line 383
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mIsQuickVoiceUsing:Z

    #@40
    .line 385
    iput v11, p0, Lcom/android/server/InputMethodManagerService;->mBackDisposition:I

    #@42
    .line 394
    new-instance v0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;

    #@44
    invoke-direct {v0, p0}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@47
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mMyPackageMonitor:Lcom/android/server/InputMethodManagerService$MyPackageMonitor;

    #@49
    .line 619
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@4c
    move-result-object v0

    #@4d
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mIPackageManager:Landroid/content/pm/IPackageManager;

    #@4f
    .line 620
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@51
    .line 621
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@54
    move-result-object v0

    #@55
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@57
    .line 622
    new-instance v0, Landroid/os/Handler;

    #@59
    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    #@5c
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mHandler:Landroid/os/Handler;

    #@5e
    .line 623
    const-string v0, "window"

    #@60
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@63
    move-result-object v0

    #@64
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@67
    move-result-object v0

    #@68
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@6a
    .line 625
    new-instance v0, Lcom/android/internal/os/HandlerCaller;

    #@6c
    new-instance v1, Lcom/android/server/InputMethodManagerService$2;

    #@6e
    invoke-direct {v1, p0}, Lcom/android/server/InputMethodManagerService$2;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@71
    invoke-direct {v0, p1, v1}, Lcom/android/internal/os/HandlerCaller;-><init>(Landroid/content/Context;Lcom/android/internal/os/HandlerCaller$Callback;)V

    #@74
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@76
    .line 631
    iput-object p2, p0, Lcom/android/server/InputMethodManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    #@78
    .line 632
    new-instance v0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;

    #@7a
    invoke-direct {v0, p0, v3}, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;-><init>(Lcom/android/server/InputMethodManagerService;Lcom/android/server/InputMethodManagerService$1;)V

    #@7d
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mHardKeyboardListener:Lcom/android/server/InputMethodManagerService$HardKeyboardListener;

    #@7f
    .line 634
    new-instance v0, Landroid/app/Notification;

    #@81
    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    #@84
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@86
    .line 635
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@88
    const v1, 0x1080359

    #@8b
    iput v1, v0, Landroid/app/Notification;->icon:I

    #@8d
    .line 636
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@8f
    const-wide/16 v1, 0x0

    #@91
    iput-wide v1, v0, Landroid/app/Notification;->when:J

    #@93
    .line 637
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@95
    const/4 v1, 0x2

    #@96
    iput v1, v0, Landroid/app/Notification;->flags:I

    #@98
    .line 638
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@9a
    iput-object v3, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@9c
    .line 639
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@9e
    iput v11, v0, Landroid/app/Notification;->defaults:I

    #@a0
    .line 640
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@a2
    iput-object v3, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@a4
    .line 641
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@a6
    iput-object v3, v0, Landroid/app/Notification;->vibrate:[J

    #@a8
    .line 644
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@aa
    new-array v1, v10, [Ljava/lang/String;

    #@ac
    const-string v2, "android.system.imeswitcher"

    #@ae
    aput-object v2, v1, v11

    #@b0
    iput-object v1, v0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@b2
    .line 646
    new-instance v9, Landroid/content/Intent;

    #@b4
    const-string v0, "android.settings.SHOW_INPUT_METHOD_PICKER"

    #@b6
    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b9
    .line 647
    .local v9, intent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@bb
    invoke-static {v0, v11, v9, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@be
    move-result-object v0

    #@bf
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSwitchPendingIntent:Landroid/app/PendingIntent;

    #@c1
    .line 649
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowOngoingImeSwitcherForPhones:Z

    #@c3
    .line 651
    new-instance v6, Landroid/content/IntentFilter;

    #@c5
    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    #@c8
    .line 652
    .local v6, broadcastFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.SCREEN_ON"

    #@ca
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@cd
    .line 653
    const-string v0, "android.intent.action.SCREEN_OFF"

    #@cf
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d2
    .line 654
    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@d4
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d7
    .line 655
    const-string v0, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    #@d9
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@dc
    .line 656
    const-string v0, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    #@de
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@e1
    .line 658
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@e3
    new-instance v1, Lcom/android/server/InputMethodManagerService$ImmsBroadcastReceiver;

    #@e5
    invoke-direct {v1, p0}, Lcom/android/server/InputMethodManagerService$ImmsBroadcastReceiver;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@e8
    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@eb
    .line 660
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mNotificationShown:Z

    #@ed
    .line 661
    const/4 v5, 0x0

    #@ee
    .line 663
    .local v5, userId:I
    :try_start_ee
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f1
    move-result-object v0

    #@f2
    new-instance v1, Lcom/android/server/InputMethodManagerService$3;

    #@f4
    invoke-direct {v1, p0}, Lcom/android/server/InputMethodManagerService$3;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@f7
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V

    #@fa
    .line 682
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@fd
    move-result-object v0

    #@fe
    invoke-interface {v0}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    #@101
    move-result-object v0

    #@102
    iget v5, v0, Landroid/content/pm/UserInfo;->id:I
    :try_end_104
    .catch Landroid/os/RemoteException; {:try_start_ee .. :try_end_104} :catch_175

    #@104
    .line 686
    :goto_104
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mMyPackageMonitor:Lcom/android/server/InputMethodManagerService$MyPackageMonitor;

    #@106
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@108
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@10a
    invoke-virtual {v0, v1, v3, v2, v10}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    #@10d
    .line 689
    new-instance v0, Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@10f
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@111
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@114
    move-result-object v2

    #@115
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@117
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@119
    invoke-direct/range {v0 .. v5}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;-><init>(Landroid/content/res/Resources;Landroid/content/ContentResolver;Ljava/util/HashMap;Ljava/util/ArrayList;I)V

    #@11c
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@11e
    .line 691
    new-instance v0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@120
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@122
    invoke-direct {v0, v1, v5}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;-><init>(Ljava/util/HashMap;I)V

    #@125
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mFileManager:Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@127
    .line 692
    new-instance v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;

    #@129
    invoke-direct {v0, p1, p0}, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;-><init>(Landroid/content/Context;Lcom/android/server/InputMethodManagerService;)V

    #@12c
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mImListManager:Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;

    #@12e
    .line 695
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@130
    invoke-virtual {v0}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@133
    move-result-object v7

    #@134
    .line 696
    .local v7, defaultImiId:Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@137
    move-result v0

    #@138
    if-nez v0, :cond_17e

    #@13a
    move v0, v10

    #@13b
    :goto_13b
    iput-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSelectedOnBoot:Z

    #@13d
    .line 698
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@13f
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@141
    invoke-virtual {p0, v0, v1}, Lcom/android/server/InputMethodManagerService;->buildInputMethodListLocked(Ljava/util/ArrayList;Ljava/util/HashMap;)V

    #@144
    .line 699
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@146
    invoke-virtual {v0}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->enableAllIMEsIfThereIsNoEnabledIME()V

    #@149
    .line 701
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mImeSelectedOnBoot:Z

    #@14b
    if-nez v0, :cond_157

    #@14d
    .line 702
    const-string v0, "InputMethodManagerService"

    #@14f
    const-string v1, "No IME selected. Choose the most applicable IME."

    #@151
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@154
    .line 703
    invoke-direct {p0, p1}, Lcom/android/server/InputMethodManagerService;->resetDefaultImeLocked(Landroid/content/Context;)V

    #@157
    .line 706
    :cond_157
    new-instance v0, Lcom/android/server/InputMethodManagerService$SettingsObserver;

    #@159
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mHandler:Landroid/os/Handler;

    #@15b
    invoke-direct {v0, p0, v1}, Lcom/android/server/InputMethodManagerService$SettingsObserver;-><init>(Lcom/android/server/InputMethodManagerService;Landroid/os/Handler;)V

    #@15e
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSettingsObserver:Lcom/android/server/InputMethodManagerService$SettingsObserver;

    #@160
    .line 707
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->updateFromSettingsLocked()V

    #@163
    .line 710
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@165
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@168
    move-result-object v0

    #@169
    const v1, 0x206000e

    #@16c
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@16f
    move-result v0

    #@170
    if-nez v0, :cond_180

    #@172
    :goto_172
    iput-boolean v10, p0, Lcom/android/server/InputMethodManagerService;->mIsQuickVoiceUsing:Z

    #@174
    .line 731
    return-void

    #@175
    .line 683
    .end local v7           #defaultImiId:Ljava/lang/String;
    :catch_175
    move-exception v8

    #@176
    .line 684
    .local v8, e:Landroid/os/RemoteException;
    const-string v0, "InputMethodManagerService"

    #@178
    const-string v1, "Couldn\'t get current user ID; guessing it\'s 0"

    #@17a
    invoke-static {v0, v1, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17d
    goto :goto_104

    #@17e
    .end local v8           #e:Landroid/os/RemoteException;
    .restart local v7       #defaultImiId:Ljava/lang/String;
    :cond_17e
    move v0, v11

    #@17f
    .line 696
    goto :goto_13b

    #@180
    :cond_180
    move v10, v11

    #@181
    .line 710
    goto :goto_172
.end method

.method static synthetic access$000(Lcom/android/server/InputMethodManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->refreshImeWindowVisibilityLocked()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/InputMethodManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->setImeWindowVisibilityStatusHiddenLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/InputMethodManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/server/InputMethodManagerService;->switchUserLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/InputMethodManagerService;)Lcom/android/server/wm/WindowManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/InputMethodManagerService;)[Landroid/view/inputmethod/InputMethodInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mIms:[Landroid/view/inputmethod/InputMethodInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/InputMethodManagerService;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSubtypeIds:[I

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/server/InputMethodManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->showConfigureInputMethods()V

    #@3
    return-void
.end method

.method static synthetic access$1700(Landroid/view/inputmethod/InputMethodInfo;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 129
    invoke-static {p0, p1}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/InputMethodManagerService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getExplicitlyOrImplicitlyEnabledInputMethodsAndSubtypeListLocked()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1900(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-static {p0}, Lcom/android/server/InputMethodManagerService;->getSubtypes(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/InputMethodManagerService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/server/InputMethodManagerService;->resetSelectedInputMethodAndSubtypeLocked(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$2000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 129
    sget-object v0, Lcom/android/server/InputMethodManagerService;->NOT_A_SUBTYPE_ID_STR:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Landroid/content/res/Resources;Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 129
    invoke-static {p0, p1}, Lcom/android/server/InputMethodManagerService;->getImplicitlyApplicableSubtypesLocked(Landroid/content/res/Resources;Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2200(Landroid/view/inputmethod/InputMethodInfo;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 129
    invoke-static {p0, p1}, Lcom/android/server/InputMethodManagerService;->isValidSubtypeId(Landroid/view/inputmethod/InputMethodInfo;I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/android/server/InputMethodManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->chooseNewDefaultIMELocked()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/android/server/InputMethodManagerService;)Lcom/android/server/InputMethodManagerService$InputMethodFileManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mFileManager:Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/InputMethodManagerService;)Landroid/content/pm/IPackageManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mIPackageManager:Landroid/content/pm/IPackageManager;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/InputMethodManagerService;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/InputMethodManagerService;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialogTitleView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method private addShortcutInputMethodAndSubtypes(Landroid/view/inputmethod/InputMethodInfo;Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 5
    .parameter "imi"
    .parameter "subtype"

    #@0
    .prologue
    .line 3318
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_14

    #@8
    .line 3319
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@a
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Ljava/util/ArrayList;

    #@10
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 3325
    :goto_13
    return-void

    #@14
    .line 3321
    :cond_14
    new-instance v0, Ljava/util/ArrayList;

    #@16
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@19
    .line 3322
    .local v0, subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 3323
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    goto :goto_13
.end method

.method private bindCurrentInputMethodService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .registers 7
    .parameter "service"
    .parameter "conn"
    .parameter "flags"

    #@0
    .prologue
    .line 966
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_28

    #@4
    .line 967
    :cond_4
    const-string v0, "InputMethodManagerService"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "--- bind failed: service = "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", conn = "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 968
    const/4 v0, 0x0

    #@27
    .line 970
    :goto_27
    return v0

    #@28
    :cond_28
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@2a
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@2c
    invoke-virtual {v1}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getCurrentUserId()I

    #@2f
    move-result v1

    #@30
    invoke-virtual {v0, p1, p2, p3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@33
    move-result v0

    #@34
    goto :goto_27
.end method

.method private calledFromValidUser()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 934
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v0

    #@5
    .line 935
    .local v0, uid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@8
    move-result v1

    #@9
    .line 942
    .local v1, userId:I
    const/16 v3, 0x3e8

    #@b
    if-eq v0, v3, :cond_15

    #@d
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@f
    invoke-virtual {v3}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getCurrentUserId()I

    #@12
    move-result v3

    #@13
    if-ne v1, v3, :cond_16

    #@15
    .line 961
    :cond_15
    :goto_15
    return v2

    #@16
    .line 951
    :cond_16
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@18
    const-string v4, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@1a
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_15

    #@20
    .line 960
    const-string v2, "InputMethodManagerService"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "--- IPC called from background users. Ignore. \n"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {}, Lcom/android/server/InputMethodManagerService;->getStackTrace()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 961
    const/4 v2, 0x0

    #@3d
    goto :goto_15
.end method

.method private canAddToLastInputMethod(Landroid/view/inputmethod/InputMethodSubtype;)Z
    .registers 4
    .parameter "subtype"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2961
    if-nez p1, :cond_4

    #@3
    .line 2962
    :cond_3
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_3

    #@a
    const/4 v0, 0x0

    #@b
    goto :goto_3
.end method

.method private checkCurrentLocaleChangedLocked()V
    .registers 2

    #@0
    .prologue
    .line 800
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Lcom/android/server/InputMethodManagerService;->resetAllInternalStateLocked(Z)V

    #@4
    .line 801
    return-void
.end method

.method private chooseNewDefaultIMELocked()Z
    .registers 3

    #@0
    .prologue
    .line 2527
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getMostApplicableDefaultIMELocked()Landroid/view/inputmethod/InputMethodInfo;

    #@3
    move-result-object v0

    #@4
    .line 2528
    .local v0, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v0, :cond_f

    #@6
    .line 2532
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-direct {p0, v1}, Lcom/android/server/InputMethodManagerService;->resetSelectedInputMethodAndSubtypeLocked(Ljava/lang/String;)V

    #@d
    .line 2533
    const/4 v1, 0x1

    #@e
    .line 2536
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method private static containsSubtypeOf(Landroid/view/inputmethod/InputMethodInfo;Ljava/lang/String;)Z
    .registers 5
    .parameter "imi"
    .parameter "language"

    #@0
    .prologue
    .line 844
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@3
    move-result v0

    #@4
    .line 845
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_1a

    #@7
    .line 846
    invoke-virtual {p0, v1}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_17

    #@15
    .line 847
    const/4 v2, 0x1

    #@16
    .line 850
    :goto_16
    return v2

    #@17
    .line 845
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_5

    #@1a
    .line 850
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_16
.end method

.method private findLastResortApplicableShortcutInputMethodAndSubtypeLocked(Ljava/lang/String;)Landroid/util/Pair;
    .registers 16
    .parameter "mode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3185
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@2
    invoke-virtual {v11}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/List;

    #@5
    move-result-object v5

    #@6
    .line 3186
    .local v5, imis:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    const/4 v6, 0x0

    #@7
    .line 3187
    .local v6, mostApplicableIMI:Landroid/view/inputmethod/InputMethodInfo;
    const/4 v7, 0x0

    #@8
    .line 3188
    .local v7, mostApplicableSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    const/4 v1, 0x0

    #@9
    .line 3191
    .local v1, foundInSystemIME:Z
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v2

    #@d
    .local v2, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v11

    #@11
    if-eqz v11, :cond_7f

    #@13
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    #@19
    .line 3192
    .local v3, imi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    .line 3193
    .local v4, imiId:Ljava/lang/String;
    if-eqz v1, :cond_27

    #@1f
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@21
    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v11

    #@25
    if-eqz v11, :cond_d

    #@27
    .line 3196
    :cond_27
    const/4 v9, 0x0

    #@28
    .line 3197
    .local v9, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    const/4 v11, 0x1

    #@29
    invoke-virtual {p0, v3, v11}, Lcom/android/server/InputMethodManagerService;->getEnabledInputMethodSubtypeListLocked(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@2c
    move-result-object v0

    #@2d
    .line 3200
    .local v0, enabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@2f
    if-eqz v11, :cond_3e

    #@31
    .line 3201
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@33
    iget-object v12, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@35
    invoke-virtual {v12}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@38
    move-result-object v12

    #@39
    const/4 v13, 0x0

    #@3a
    invoke-static {v11, v0, p1, v12, v13}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@3d
    move-result-object v9

    #@3e
    .line 3206
    :cond_3e
    if-nez v9, :cond_48

    #@40
    .line 3207
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@42
    const/4 v12, 0x0

    #@43
    const/4 v13, 0x1

    #@44
    invoke-static {v11, v0, p1, v12, v13}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@47
    move-result-object v9

    #@48
    .line 3210
    :cond_48
    invoke-static {v3, p1}, Lcom/android/server/InputMethodManagerService;->getOverridingImplicitlyEnabledSubtypes(Landroid/view/inputmethod/InputMethodInfo;Ljava/lang/String;)Ljava/util/ArrayList;

    #@4b
    move-result-object v8

    #@4c
    .line 3212
    .local v8, overridingImplicitlyEnabledSubtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    #@4f
    move-result v11

    #@50
    if-eqz v11, :cond_87

    #@52
    invoke-static {v3}, Lcom/android/server/InputMethodManagerService;->getSubtypes(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@55
    move-result-object v10

    #@56
    .line 3216
    .local v10, subtypesForSearch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :goto_56
    if-nez v9, :cond_69

    #@58
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@5a
    if-eqz v11, :cond_69

    #@5c
    .line 3217
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@5e
    iget-object v12, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@60
    invoke-virtual {v12}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@63
    move-result-object v12

    #@64
    const/4 v13, 0x0

    #@65
    invoke-static {v11, v10, p1, v12, v13}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@68
    move-result-object v9

    #@69
    .line 3222
    :cond_69
    if-nez v9, :cond_73

    #@6b
    .line 3223
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@6d
    const/4 v12, 0x0

    #@6e
    const/4 v13, 0x1

    #@6f
    invoke-static {v11, v10, p1, v12, v13}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@72
    move-result-object v9

    #@73
    .line 3226
    :cond_73
    if-eqz v9, :cond_d

    #@75
    .line 3227
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@77
    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7a
    move-result v11

    #@7b
    if-eqz v11, :cond_89

    #@7d
    .line 3229
    move-object v6, v3

    #@7e
    .line 3230
    move-object v7, v9

    #@7f
    .line 3254
    .end local v0           #enabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v3           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v4           #imiId:Ljava/lang/String;
    .end local v8           #overridingImplicitlyEnabledSubtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v9           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v10           #subtypesForSearch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_7f
    if-eqz v6, :cond_9c

    #@81
    .line 3255
    new-instance v11, Landroid/util/Pair;

    #@83
    invoke-direct {v11, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@86
    .line 3258
    :goto_86
    return-object v11

    #@87
    .restart local v0       #enabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .restart local v3       #imi:Landroid/view/inputmethod/InputMethodInfo;
    .restart local v4       #imiId:Ljava/lang/String;
    .restart local v8       #overridingImplicitlyEnabledSubtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .restart local v9       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_87
    move-object v10, v8

    #@88
    .line 3212
    goto :goto_56

    #@89
    .line 3232
    .restart local v10       #subtypesForSearch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_89
    if-nez v1, :cond_d

    #@8b
    .line 3234
    move-object v6, v3

    #@8c
    .line 3235
    move-object v7, v9

    #@8d
    .line 3236
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    #@90
    move-result-object v11

    #@91
    iget-object v11, v11, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@93
    iget v11, v11, Landroid/content/pm/ApplicationInfo;->flags:I

    #@95
    and-int/lit8 v11, v11, 0x1

    #@97
    if-eqz v11, :cond_d

    #@99
    .line 3238
    const/4 v1, 0x1

    #@9a
    goto/16 :goto_d

    #@9c
    .line 3258
    .end local v0           #enabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v3           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v4           #imiId:Ljava/lang/String;
    .end local v8           #overridingImplicitlyEnabledSubtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v9           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v10           #subtypesForSearch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_9c
    const/4 v11, 0x0

    #@9d
    goto :goto_86
.end method

.method private static findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;
    .registers 15
    .parameter "res"
    .parameter
    .parameter "mode"
    .parameter "locale"
    .parameter "canIgnoreLocaleAsLastResort"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Landroid/view/inputmethod/InputMethodSubtype;"
        }
    .end annotation

    #@0
    .prologue
    .line 3135
    .local p1, subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    if-eqz p1, :cond_8

    #@2
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@5
    move-result v8

    #@6
    if-nez v8, :cond_a

    #@8
    .line 3136
    :cond_8
    const/4 v2, 0x0

    #@9
    .line 3179
    :cond_9
    :goto_9
    return-object v2

    #@a
    .line 3138
    :cond_a
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v8

    #@e
    if-eqz v8, :cond_1a

    #@10
    .line 3139
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@13
    move-result-object v8

    #@14
    iget-object v8, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@16
    invoke-virtual {v8}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@19
    move-result-object p3

    #@1a
    .line 3141
    :cond_1a
    const/4 v8, 0x0

    #@1b
    const/4 v9, 0x2

    #@1c
    invoke-virtual {p3, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    .line 3142
    .local v4, language:Ljava/lang/String;
    const/4 v5, 0x0

    #@21
    .line 3143
    .local v5, partialMatchFound:Z
    const/4 v1, 0x0

    #@22
    .line 3144
    .local v1, applicableSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    const/4 v2, 0x0

    #@23
    .line 3145
    .local v2, firstMatchedModeSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@26
    move-result v0

    #@27
    .line 3146
    .local v0, N:I
    const/4 v3, 0x0

    #@28
    .local v3, i:I
    :goto_28
    if-ge v3, v0, :cond_50

    #@2a
    .line 3147
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v6

    #@2e
    check-cast v6, Landroid/view/inputmethod/InputMethodSubtype;

    #@30
    .line 3148
    .local v6, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@33
    move-result-object v7

    #@34
    .line 3151
    .local v7, subtypeLocale:Ljava/lang/String;
    if-eqz p2, :cond_46

    #@36
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v8

    #@3a
    check-cast v8, Landroid/view/inputmethod/InputMethodSubtype;

    #@3c
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@3f
    move-result-object v8

    #@40
    invoke-virtual {v8, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@43
    move-result v8

    #@44
    if-eqz v8, :cond_60

    #@46
    .line 3152
    :cond_46
    if-nez v2, :cond_49

    #@48
    .line 3153
    move-object v2, v6

    #@49
    .line 3155
    :cond_49
    invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v8

    #@4d
    if-eqz v8, :cond_56

    #@4f
    .line 3157
    move-object v1, v6

    #@50
    .line 3167
    .end local v6           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v7           #subtypeLocale:Ljava/lang/String;
    :cond_50
    if-nez v1, :cond_54

    #@52
    if-nez p4, :cond_9

    #@54
    :cond_54
    move-object v2, v1

    #@55
    .line 3179
    goto :goto_9

    #@56
    .line 3159
    .restart local v6       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .restart local v7       #subtypeLocale:Ljava/lang/String;
    :cond_56
    if-nez v5, :cond_60

    #@58
    invoke-virtual {v7, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5b
    move-result v8

    #@5c
    if-eqz v8, :cond_60

    #@5e
    .line 3161
    move-object v1, v6

    #@5f
    .line 3162
    const/4 v5, 0x1

    #@60
    .line 3146
    :cond_60
    add-int/lit8 v3, v3, 0x1

    #@62
    goto :goto_28
.end method

.method private finishSession(Lcom/android/server/InputMethodManagerService$SessionState;)V
    .registers 5
    .parameter "sessionState"

    #@0
    .prologue
    .line 1373
    if-eqz p1, :cond_b

    #@2
    iget-object v1, p1, Lcom/android/server/InputMethodManagerService$SessionState;->session:Lcom/android/internal/view/IInputMethodSession;

    #@4
    if-eqz v1, :cond_b

    #@6
    .line 1375
    :try_start_6
    iget-object v1, p1, Lcom/android/server/InputMethodManagerService$SessionState;->session:Lcom/android/internal/view/IInputMethodSession;

    #@8
    invoke-interface {v1}, Lcom/android/internal/view/IInputMethodSession;->finishSession()V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_c

    #@b
    .line 1381
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1376
    :catch_c
    move-exception v0

    #@d
    .line 1377
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManagerService"

    #@f
    const-string v2, "Session failed to close due to remote exception"

    #@11
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    .line 1378
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->setImeWindowVisibilityStatusHiddenLocked()V

    #@17
    goto :goto_b
.end method

.method private getAppShowFlags()I
    .registers 3

    #@0
    .prologue
    .line 1097
    const/4 v0, 0x0

    #@1
    .line 1098
    .local v0, flags:I
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mShowForced:Z

    #@3
    if-eqz v1, :cond_8

    #@5
    .line 1099
    or-int/lit8 v0, v0, 0x2

    #@7
    .line 1103
    :cond_7
    :goto_7
    return v0

    #@8
    .line 1100
    :cond_8
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mShowExplicitlyRequested:Z

    #@a
    if-nez v1, :cond_7

    #@c
    .line 1101
    or-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_7
.end method

.method private getCurrentInputMethodSubtypeLocked()Landroid/view/inputmethod/InputMethodSubtype;
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v9, -0x1

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 3277
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@6
    if-nez v7, :cond_a

    #@8
    move-object v4, v6

    #@9
    .line 3313
    :goto_9
    return-object v4

    #@a
    .line 3280
    :cond_a
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@c
    invoke-virtual {v7}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethodSubtypeHashCode()I

    #@f
    move-result v7

    #@10
    if-eq v7, v9, :cond_27

    #@12
    move v3, v4

    #@13
    .line 3282
    .local v3, subtypeIsSelected:Z
    :goto_13
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@15
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@17
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    #@1d
    .line 3283
    .local v1, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v1, :cond_25

    #@1f
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@22
    move-result v7

    #@23
    if-nez v7, :cond_29

    #@25
    :cond_25
    move-object v4, v6

    #@26
    .line 3284
    goto :goto_9

    #@27
    .end local v1           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v3           #subtypeIsSelected:Z
    :cond_27
    move v3, v5

    #@28
    .line 3280
    goto :goto_13

    #@29
    .line 3286
    .restart local v1       #imi:Landroid/view/inputmethod/InputMethodInfo;
    .restart local v3       #subtypeIsSelected:Z
    :cond_29
    if-eqz v3, :cond_3b

    #@2b
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@2d
    if-eqz v7, :cond_3b

    #@2f
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@31
    invoke-virtual {v7}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@34
    move-result v7

    #@35
    invoke-static {v1, v7}, Lcom/android/server/InputMethodManagerService;->isValidSubtypeId(Landroid/view/inputmethod/InputMethodInfo;I)Z

    #@38
    move-result v7

    #@39
    if-nez v7, :cond_55

    #@3b
    .line 3288
    :cond_3b
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@3d
    invoke-direct {p0, v7}, Lcom/android/server/InputMethodManagerService;->getSelectedInputMethodSubtypeId(Ljava/lang/String;)I

    #@40
    move-result v2

    #@41
    .line 3289
    .local v2, subtypeId:I
    if-ne v2, v9, :cond_75

    #@43
    .line 3293
    invoke-virtual {p0, v1, v4}, Lcom/android/server/InputMethodManagerService;->getEnabledInputMethodSubtypeListLocked(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@46
    move-result-object v0

    #@47
    .line 3297
    .local v0, explicitlyOrImplicitlyEnabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@4a
    move-result v7

    #@4b
    if-ne v7, v4, :cond_58

    #@4d
    .line 3298
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@50
    move-result-object v4

    #@51
    check-cast v4, Landroid/view/inputmethod/InputMethodSubtype;

    #@53
    iput-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@55
    .line 3313
    .end local v0           #explicitlyOrImplicitlyEnabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v2           #subtypeId:I
    :cond_55
    :goto_55
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@57
    goto :goto_9

    #@58
    .line 3299
    .restart local v0       #explicitlyOrImplicitlyEnabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .restart local v2       #subtypeId:I
    :cond_58
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@5b
    move-result v5

    #@5c
    if-le v5, v4, :cond_55

    #@5e
    .line 3300
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@60
    const-string v7, "keyboard"

    #@62
    invoke-static {v5, v0, v7, v6, v4}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@65
    move-result-object v5

    #@66
    iput-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@68
    .line 3303
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@6a
    if-nez v5, :cond_55

    #@6c
    .line 3304
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@6e
    invoke-static {v5, v0, v6, v6, v4}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@71
    move-result-object v4

    #@72
    iput-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@74
    goto :goto_55

    #@75
    .line 3310
    .end local v0           #explicitlyOrImplicitlyEnabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_75
    invoke-static {v1}, Lcom/android/server/InputMethodManagerService;->getSubtypes(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7c
    move-result-object v4

    #@7d
    check-cast v4, Landroid/view/inputmethod/InputMethodSubtype;

    #@7f
    iput-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@81
    goto :goto_55
.end method

.method private getExplicitlyOrImplicitlyEnabledInputMethodsAndSubtypeListLocked()Ljava/util/HashMap;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 997
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 999
    .local v0, enabledInputMethodAndSubtypes:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@7
    invoke-virtual {v3}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/List;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .local v1, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_24

    #@15
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    #@1b
    .line 1000
    .local v2, imi:Landroid/view/inputmethod/InputMethodInfo;
    const/4 v3, 0x1

    #@1c
    invoke-virtual {p0, v2, v3}, Lcom/android/server/InputMethodManagerService;->getEnabledInputMethodSubtypeListLocked(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    goto :goto_f

    #@24
    .line 1003
    .end local v2           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_24
    return-object v0
.end method

.method private getImeShowFlags()I
    .registers 3

    #@0
    .prologue
    .line 1086
    const/4 v0, 0x0

    #@1
    .line 1087
    .local v0, flags:I
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mShowForced:Z

    #@3
    if-eqz v1, :cond_8

    #@5
    .line 1088
    or-int/lit8 v0, v0, 0x3

    #@7
    .line 1093
    :cond_7
    :goto_7
    return v0

    #@8
    .line 1090
    :cond_8
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mShowExplicitlyRequested:Z

    #@a
    if-eqz v1, :cond_7

    #@c
    .line 1091
    or-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_7
.end method

.method private static getImplicitlyApplicableSubtypesLocked(Landroid/content/res/Resources;Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;
    .registers 16
    .parameter "res"
    .parameter "imi"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3057
    invoke-static {p1}, Lcom/android/server/InputMethodManagerService;->getSubtypes(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@3
    move-result-object v10

    #@4
    .line 3058
    .local v10, subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v12

    #@8
    iget-object v12, v12, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@a
    invoke-virtual {v12}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@d
    move-result-object v11

    #@e
    .line 3059
    .local v11, systemLocale:Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v12

    #@12
    if-eqz v12, :cond_1a

    #@14
    new-instance v3, Ljava/util/ArrayList;

    #@16
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@19
    .line 3119
    :cond_19
    :goto_19
    return-object v3

    #@1a
    .line 3060
    :cond_1a
    new-instance v1, Ljava/util/HashMap;

    #@1c
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@1f
    .line 3062
    .local v1, applicableModeAndSubtypesMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@22
    move-result v0

    #@23
    .line 3063
    .local v0, N:I
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v0, :cond_42

    #@26
    .line 3065
    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v9

    #@2a
    check-cast v9, Landroid/view/inputmethod/InputMethodSubtype;

    #@2c
    .line 3066
    .local v9, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v9}, Landroid/view/inputmethod/InputMethodSubtype;->overridesImplicitlyEnabledSubtype()Z

    #@2f
    move-result v12

    #@30
    if-eqz v12, :cond_3f

    #@32
    .line 3067
    invoke-virtual {v9}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@35
    move-result-object v8

    #@36
    .line 3068
    .local v8, mode:Ljava/lang/String;
    invoke-virtual {v1, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@39
    move-result v12

    #@3a
    if-nez v12, :cond_3f

    #@3c
    .line 3069
    invoke-virtual {v1, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 3063
    .end local v8           #mode:Ljava/lang/String;
    :cond_3f
    add-int/lit8 v4, v4, 0x1

    #@41
    goto :goto_24

    #@42
    .line 3073
    .end local v9           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_42
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    #@45
    move-result v12

    #@46
    if-lez v12, :cond_52

    #@48
    .line 3074
    new-instance v3, Ljava/util/ArrayList;

    #@4a
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@4d
    move-result-object v12

    #@4e
    invoke-direct {v3, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@51
    goto :goto_19

    #@52
    .line 3076
    :cond_52
    const/4 v4, 0x0

    #@53
    :goto_53
    if-ge v4, v0, :cond_88

    #@55
    .line 3077
    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@58
    move-result-object v9

    #@59
    check-cast v9, Landroid/view/inputmethod/InputMethodSubtype;

    #@5b
    .line 3078
    .restart local v9       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v9}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@5e
    move-result-object v7

    #@5f
    .line 3079
    .local v7, locale:Ljava/lang/String;
    invoke-virtual {v9}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@62
    move-result-object v8

    #@63
    .line 3088
    .restart local v8       #mode:Ljava/lang/String;
    invoke-virtual {v11, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@66
    move-result v12

    #@67
    if-eqz v12, :cond_7b

    #@69
    .line 3089
    invoke-virtual {v1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6c
    move-result-object v2

    #@6d
    check-cast v2, Landroid/view/inputmethod/InputMethodSubtype;

    #@6f
    .line 3091
    .local v2, applicableSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    if-eqz v2, :cond_84

    #@71
    .line 3092
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@74
    move-result-object v12

    #@75
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@78
    move-result v12

    #@79
    if-eqz v12, :cond_7e

    #@7b
    .line 3076
    .end local v2           #applicableSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_7b
    :goto_7b
    add-int/lit8 v4, v4, 0x1

    #@7d
    goto :goto_53

    #@7e
    .line 3093
    .restart local v2       #applicableSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_7e
    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v12

    #@82
    if-eqz v12, :cond_7b

    #@84
    .line 3095
    :cond_84
    invoke-virtual {v1, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    goto :goto_7b

    #@88
    .line 3098
    .end local v2           #applicableSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v7           #locale:Ljava/lang/String;
    .end local v8           #mode:Ljava/lang/String;
    .end local v9           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_88
    const-string v12, "keyboard"

    #@8a
    invoke-virtual {v1, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8d
    move-result-object v5

    #@8e
    check-cast v5, Landroid/view/inputmethod/InputMethodSubtype;

    #@90
    .line 3100
    .local v5, keyboardSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    new-instance v3, Ljava/util/ArrayList;

    #@92
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@95
    move-result-object v12

    #@96
    invoke-direct {v3, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@99
    .line 3102
    .local v3, applicableSubtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    if-eqz v5, :cond_c6

    #@9b
    const-string v12, "AsciiCapable"

    #@9d
    invoke-virtual {v5, v12}, Landroid/view/inputmethod/InputMethodSubtype;->containsExtraValueKey(Ljava/lang/String;)Z

    #@a0
    move-result v12

    #@a1
    if-nez v12, :cond_c6

    #@a3
    .line 3103
    const/4 v4, 0x0

    #@a4
    :goto_a4
    if-ge v4, v0, :cond_c6

    #@a6
    .line 3104
    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a9
    move-result-object v9

    #@aa
    check-cast v9, Landroid/view/inputmethod/InputMethodSubtype;

    #@ac
    .line 3105
    .restart local v9       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v9}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@af
    move-result-object v8

    #@b0
    .line 3106
    .restart local v8       #mode:Ljava/lang/String;
    const-string v12, "keyboard"

    #@b2
    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b5
    move-result v12

    #@b6
    if-eqz v12, :cond_c3

    #@b8
    const-string v12, "EnabledWhenDefaultIsNotAsciiCapable"

    #@ba
    invoke-virtual {v9, v12}, Landroid/view/inputmethod/InputMethodSubtype;->containsExtraValueKey(Ljava/lang/String;)Z

    #@bd
    move-result v12

    #@be
    if-eqz v12, :cond_c3

    #@c0
    .line 3108
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c3
    .line 3103
    :cond_c3
    add-int/lit8 v4, v4, 0x1

    #@c5
    goto :goto_a4

    #@c6
    .line 3112
    .end local v8           #mode:Ljava/lang/String;
    .end local v9           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_c6
    if-nez v5, :cond_19

    #@c8
    .line 3113
    const-string v12, "keyboard"

    #@ca
    const/4 v13, 0x1

    #@cb
    invoke-static {p0, v10, v12, v11, v13}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@ce
    move-result-object v6

    #@cf
    .line 3115
    .local v6, lastResortKeyboardSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    if-eqz v6, :cond_19

    #@d1
    .line 3116
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d4
    goto/16 :goto_19
.end method

.method private getMostApplicableDefaultIMELocked()Landroid/view/inputmethod/InputMethodInfo;
    .registers 6

    #@0
    .prologue
    .line 2506
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@2
    invoke-virtual {v4}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    .line 2507
    .local v0, enabled:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz v0, :cond_47

    #@8
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v4

    #@c
    if-lez v4, :cond_47

    #@e
    .line 2509
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@11
    move-result v2

    #@12
    .line 2510
    .local v2, i:I
    const/4 v1, -0x1

    #@13
    .line 2511
    .local v1, firstFoundSystemIme:I
    :cond_13
    :goto_13
    if-lez v2, :cond_3a

    #@15
    .line 2512
    add-int/lit8 v2, v2, -0x1

    #@17
    .line 2513
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v3

    #@1b
    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    #@1d
    .line 2514
    .local v3, imi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-static {v3}, Lcom/android/server/InputMethodManagerService;->isSystemImeThatHasEnglishSubtype(Landroid/view/inputmethod/InputMethodInfo;)Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_2a

    #@23
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->isAuxiliaryIme()Z

    #@26
    move-result v4

    #@27
    if-nez v4, :cond_2a

    #@29
    .line 2523
    .end local v1           #firstFoundSystemIme:I
    .end local v2           #i:I
    .end local v3           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :goto_29
    return-object v3

    #@2a
    .line 2517
    .restart local v1       #firstFoundSystemIme:I
    .restart local v2       #i:I
    .restart local v3       #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_2a
    if-gez v1, :cond_13

    #@2c
    invoke-static {v3}, Lcom/android/server/InputMethodManagerService;->isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z

    #@2f
    move-result v4

    #@30
    if-eqz v4, :cond_13

    #@32
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->isAuxiliaryIme()Z

    #@35
    move-result v4

    #@36
    if-nez v4, :cond_13

    #@38
    .line 2518
    move v1, v2

    #@39
    goto :goto_13

    #@3a
    .line 2521
    .end local v3           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_3a
    const/4 v4, 0x0

    #@3b
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    #@3e
    move-result v4

    #@3f
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v4

    #@43
    check-cast v4, Landroid/view/inputmethod/InputMethodInfo;

    #@45
    move-object v3, v4

    #@46
    goto :goto_29

    #@47
    .line 2523
    .end local v1           #firstFoundSystemIme:I
    .end local v2           #i:I
    :cond_47
    const/4 v3, 0x0

    #@48
    goto :goto_29
.end method

.method private static getOverridingImplicitlyEnabledSubtypes(Landroid/view/inputmethod/InputMethodInfo;Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 7
    .parameter "imi"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2494
    new-instance v3, Ljava/util/ArrayList;

    #@2
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 2495
    .local v3, subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@8
    move-result v2

    #@9
    .line 2496
    .local v2, subtypeCount:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v2, :cond_26

    #@c
    .line 2497
    invoke-virtual {p0, v0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@f
    move-result-object v1

    #@10
    .line 2498
    .local v1, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodSubtype;->overridesImplicitlyEnabledSubtype()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_23

    #@16
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_23

    #@20
    .line 2499
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 2496
    :cond_23
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_a

    #@26
    .line 2502
    .end local v1           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_26
    return-object v3
.end method

.method private getSelectedInputMethodSubtypeId(Ljava/lang/String;)I
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 3030
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    #@8
    .line 3031
    .local v0, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-nez v0, :cond_c

    #@a
    .line 3032
    const/4 v2, -0x1

    #@b
    .line 3035
    :goto_b
    return v2

    #@c
    .line 3034
    :cond_c
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@e
    invoke-virtual {v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethodSubtypeHashCode()I

    #@11
    move-result v1

    #@12
    .line 3035
    .local v1, subtypeHashCode:I
    invoke-static {v0, v1}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@15
    move-result v2

    #@16
    goto :goto_b
.end method

.method private static getStackTrace()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 4142
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 4144
    .local v3, sb:Ljava/lang/StringBuilder;
    :try_start_5
    new-instance v4, Ljava/lang/RuntimeException;

    #@7
    invoke-direct {v4}, Ljava/lang/RuntimeException;-><init>()V

    #@a
    throw v4
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_b} :catch_b

    #@b
    .line 4145
    :catch_b
    move-exception v0

    #@c
    .line 4146
    .local v0, e:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@f
    move-result-object v1

    #@10
    .line 4148
    .local v1, frames:[Ljava/lang/StackTraceElement;
    const/4 v2, 0x1

    #@11
    .local v2, j:I
    :goto_11
    array-length v4, v1

    #@12
    if-ge v2, v4, :cond_33

    #@14
    .line 4149
    new-instance v4, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    aget-object v5, v1, v2

    #@1b
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, "\n"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 4148
    add-int/lit8 v2, v2, 0x1

    #@32
    goto :goto_11

    #@33
    .line 4152
    :cond_33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    return-object v4
.end method

.method private static getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I
    .registers 6
    .parameter "imi"
    .parameter "subtypeHashCode"

    #@0
    .prologue
    .line 3043
    if-eqz p0, :cond_17

    #@2
    .line 3044
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@5
    move-result v2

    #@6
    .line 3045
    .local v2, subtypeCount:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v2, :cond_17

    #@9
    .line 3046
    invoke-virtual {p0, v0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@c
    move-result-object v1

    #@d
    .line 3047
    .local v1, ims:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@10
    move-result v3

    #@11
    if-ne p1, v3, :cond_14

    #@13
    .line 3052
    .end local v0           #i:I
    .end local v1           #ims:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v2           #subtypeCount:I
    :goto_13
    return v0

    #@14
    .line 3045
    .restart local v0       #i:I
    .restart local v1       #ims:Landroid/view/inputmethod/InputMethodSubtype;
    .restart local v2       #subtypeCount:I
    :cond_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_7

    #@17
    .line 3052
    .end local v0           #i:I
    .end local v1           #ims:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v2           #subtypeCount:I
    :cond_17
    const/4 v0, -0x1

    #@18
    goto :goto_13
.end method

.method private static getSubtypes(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;
    .registers 5
    .parameter "imi"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2484
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 2485
    .local v2, subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@8
    move-result v1

    #@9
    .line 2486
    .local v1, subtypeCount:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_16

    #@c
    .line 2487
    invoke-virtual {p0, v0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 2486
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_a

    #@16
    .line 2489
    :cond_16
    return-object v2
.end method

.method private static isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z
    .registers 2
    .parameter "inputMethod"

    #@0
    .prologue
    .line 2479
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@6
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@8
    and-int/lit8 v0, v0, 0x1

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private static isSystemImeThatHasEnglishSubtype(Landroid/view/inputmethod/InputMethodInfo;)Z
    .registers 2
    .parameter "imi"

    #@0
    .prologue
    .line 837
    invoke-static {p0}, Lcom/android/server/InputMethodManagerService;->isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 838
    const/4 v0, 0x0

    #@7
    .line 840
    :goto_7
    return v0

    #@8
    :cond_8
    sget-object v0, Lcom/android/server/InputMethodManagerService;->ENGLISH_LOCALE:Ljava/util/Locale;

    #@a
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    invoke-static {p0, v0}, Lcom/android/server/InputMethodManagerService;->containsSubtypeOf(Landroid/view/inputmethod/InputMethodInfo;Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    goto :goto_7
.end method

.method private static isValidSubtypeId(Landroid/view/inputmethod/InputMethodInfo;I)Z
    .registers 4
    .parameter "imi"
    .parameter "subtypeHashCode"

    #@0
    .prologue
    .line 3039
    invoke-static {p0, p1}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@3
    move-result v0

    #@4
    const/4 v1, -0x1

    #@5
    if-eq v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private isValidSystemDefaultIme(Landroid/view/inputmethod/InputMethodInfo;Landroid/content/Context;)Z
    .registers 8
    .parameter "imi"
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 811
    iget-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 833
    :cond_5
    :goto_5
    return v1

    #@6
    .line 814
    :cond_6
    invoke-static {p1}, Lcom/android/server/InputMethodManagerService;->isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_5

    #@c
    .line 817
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getIsDefaultResourceId()I

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_40

    #@12
    .line 819
    :try_start_12
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    const/4 v3, 0x0

    #@17
    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v0

    #@1f
    .line 821
    .local v0, res:Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getIsDefaultResourceId()I

    #@22
    move-result v2

    #@23
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_40

    #@29
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@30
    move-result-object v2

    #@31
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@33
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {p1, v2}, Lcom/android/server/InputMethodManagerService;->containsSubtypeOf(Landroid/view/inputmethod/InputMethodInfo;Ljava/lang/String;)Z
    :try_end_3a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12 .. :try_end_3a} :catch_63
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_12 .. :try_end_3a} :catch_3f

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_40

    #@3d
    .line 824
    const/4 v1, 0x1

    #@3e
    goto :goto_5

    #@3f
    .line 827
    .end local v0           #res:Landroid/content/res/Resources;
    :catch_3f
    move-exception v2

    #@40
    .line 830
    :cond_40
    :goto_40
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@43
    move-result v2

    #@44
    if-nez v2, :cond_5

    #@46
    .line 831
    const-string v2, "InputMethodManagerService"

    #@48
    new-instance v3, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v4, "Found no subtypes in a system IME: "

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_5

    #@63
    .line 826
    :catch_63
    move-exception v2

    #@64
    goto :goto_40
.end method

.method private needsToShowImeSwitchOngoingNotification()Z
    .registers 16

    #@0
    .prologue
    .line 1463
    iget-boolean v12, p0, Lcom/android/server/InputMethodManagerService;->mShowOngoingImeSwitcherForPhones:Z

    #@2
    if-nez v12, :cond_6

    #@4
    const/4 v12, 0x0

    #@5
    .line 1508
    :goto_5
    return v12

    #@6
    .line 1467
    :cond_6
    iget-object v13, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@8
    monitor-enter v13

    #@9
    .line 1468
    :try_start_9
    iget-object v12, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@b
    invoke-virtual {v12}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/List;

    #@e
    move-result-object v5

    #@f
    .line 1469
    .local v5, imis:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@12
    move-result v0

    #@13
    .line 1470
    .local v0, N:I
    const/4 v12, 0x2

    #@14
    if-le v0, v12, :cond_1c

    #@16
    const/4 v12, 0x1

    #@17
    monitor-exit v13

    #@18
    goto :goto_5

    #@19
    .line 1509
    .end local v0           #N:I
    .end local v5           #imis:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :catchall_19
    move-exception v12

    #@1a
    monitor-exit v13
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_19

    #@1b
    throw v12

    #@1c
    .line 1471
    .restart local v0       #N:I
    .restart local v5       #imis:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_1c
    const/4 v12, 0x1

    #@1d
    if-ge v0, v12, :cond_22

    #@1f
    const/4 v12, 0x0

    #@20
    :try_start_20
    monitor-exit v13

    #@21
    goto :goto_5

    #@22
    .line 1472
    :cond_22
    const/4 v7, 0x0

    #@23
    .line 1473
    .local v7, nonAuxCount:I
    const/4 v1, 0x0

    #@24
    .line 1474
    .local v1, auxCount:I
    const/4 v8, 0x0

    #@25
    .line 1475
    .local v8, nonAuxSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    const/4 v2, 0x0

    #@26
    .line 1476
    .local v2, auxSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    const/4 v3, 0x0

    #@27
    .local v3, i:I
    :goto_27
    if-ge v3, v0, :cond_58

    #@29
    .line 1477
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2c
    move-result-object v4

    #@2d
    check-cast v4, Landroid/view/inputmethod/InputMethodInfo;

    #@2f
    .line 1478
    .local v4, imi:Landroid/view/inputmethod/InputMethodInfo;
    const/4 v12, 0x1

    #@30
    invoke-virtual {p0, v4, v12}, Lcom/android/server/InputMethodManagerService;->getEnabledInputMethodSubtypeListLocked(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@33
    move-result-object v11

    #@34
    .line 1480
    .local v11, subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@37
    move-result v10

    #@38
    .line 1481
    .local v10, subtypeCount:I
    if-nez v10, :cond_3f

    #@3a
    .line 1482
    add-int/lit8 v7, v7, 0x1

    #@3c
    .line 1476
    :cond_3c
    add-int/lit8 v3, v3, 0x1

    #@3e
    goto :goto_27

    #@3f
    .line 1484
    :cond_3f
    const/4 v6, 0x0

    #@40
    .local v6, j:I
    :goto_40
    if-ge v6, v10, :cond_3c

    #@42
    .line 1485
    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@45
    move-result-object v9

    #@46
    check-cast v9, Landroid/view/inputmethod/InputMethodSubtype;

    #@48
    .line 1486
    .local v9, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v9}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@4b
    move-result v12

    #@4c
    if-nez v12, :cond_54

    #@4e
    .line 1487
    add-int/lit8 v7, v7, 0x1

    #@50
    .line 1488
    move-object v8, v9

    #@51
    .line 1484
    :goto_51
    add-int/lit8 v6, v6, 0x1

    #@53
    goto :goto_40

    #@54
    .line 1490
    :cond_54
    add-int/lit8 v1, v1, 0x1

    #@56
    .line 1491
    move-object v2, v9

    #@57
    goto :goto_51

    #@58
    .line 1496
    .end local v4           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v6           #j:I
    .end local v9           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v10           #subtypeCount:I
    .end local v11           #subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_58
    const/4 v12, 0x1

    #@59
    if-gt v7, v12, :cond_5e

    #@5b
    const/4 v12, 0x1

    #@5c
    if-le v1, v12, :cond_61

    #@5e
    .line 1497
    :cond_5e
    const/4 v12, 0x1

    #@5f
    monitor-exit v13

    #@60
    goto :goto_5

    #@61
    .line 1498
    :cond_61
    const/4 v12, 0x1

    #@62
    if-ne v7, v12, :cond_95

    #@64
    const/4 v12, 0x1

    #@65
    if-ne v1, v12, :cond_95

    #@67
    .line 1499
    if-eqz v8, :cond_91

    #@69
    if-eqz v2, :cond_91

    #@6b
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@6e
    move-result-object v12

    #@6f
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@72
    move-result-object v14

    #@73
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v12

    #@77
    if-nez v12, :cond_85

    #@79
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->overridesImplicitlyEnabledSubtype()Z

    #@7c
    move-result v12

    #@7d
    if-nez v12, :cond_85

    #@7f
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodSubtype;->overridesImplicitlyEnabledSubtype()Z

    #@82
    move-result v12

    #@83
    if-eqz v12, :cond_91

    #@85
    :cond_85
    const-string v12, "TrySuppressingImeSwitcher"

    #@87
    invoke-virtual {v8, v12}, Landroid/view/inputmethod/InputMethodSubtype;->containsExtraValueKey(Ljava/lang/String;)Z

    #@8a
    move-result v12

    #@8b
    if-eqz v12, :cond_91

    #@8d
    .line 1504
    const/4 v12, 0x0

    #@8e
    monitor-exit v13

    #@8f
    goto/16 :goto_5

    #@91
    .line 1506
    :cond_91
    const/4 v12, 0x1

    #@92
    monitor-exit v13

    #@93
    goto/16 :goto_5

    #@95
    .line 1508
    :cond_95
    const/4 v12, 0x0

    #@96
    monitor-exit v13
    :try_end_97
    .catchall {:try_start_20 .. :try_end_97} :catchall_19

    #@97
    goto/16 :goto_5
.end method

.method private refreshImeWindowVisibilityLocked()V
    .registers 11

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 909
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@4
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v0

    #@8
    .line 910
    .local v0, conf:Landroid/content/res/Configuration;
    iget v8, v0, Landroid/content/res/Configuration;->keyboard:I

    #@a
    if-eq v8, v6, :cond_45

    #@c
    move v2, v6

    #@d
    .line 912
    .local v2, haveHardKeyboard:Z
    :goto_d
    if-eqz v2, :cond_47

    #@f
    iget v8, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@11
    const/4 v9, 0x2

    #@12
    if-eq v8, v9, :cond_47

    #@14
    move v1, v6

    #@15
    .line 915
    .local v1, hardKeyShown:Z
    :goto_15
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@17
    if-eqz v8, :cond_49

    #@19
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@1b
    invoke-virtual {v8}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@1e
    move-result v8

    #@1f
    if-eqz v8, :cond_49

    #@21
    move v4, v6

    #@22
    .line 917
    .local v4, isScreenLocked:Z
    :goto_22
    if-eqz v4, :cond_4b

    #@24
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@26
    invoke-virtual {v8}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    #@29
    move-result v8

    #@2a
    if-eqz v8, :cond_4b

    #@2c
    move v5, v6

    #@2d
    .line 919
    .local v5, isScreenSecurelyLocked:Z
    :goto_2d
    iget-boolean v8, p0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@2f
    if-eqz v8, :cond_4d

    #@31
    if-eqz v4, :cond_37

    #@33
    iget-boolean v8, p0, Lcom/android/server/InputMethodManagerService;->mInputBoundToKeyguard:Z

    #@35
    if-eqz v8, :cond_4d

    #@37
    :cond_37
    move v3, v6

    #@38
    .line 920
    .local v3, inputShown:Z
    :goto_38
    if-nez v5, :cond_3f

    #@3a
    if-nez v3, :cond_3e

    #@3c
    if-eqz v1, :cond_3f

    #@3e
    :cond_3e
    const/4 v7, 0x3

    #@3f
    :cond_3f
    iput v7, p0, Lcom/android/server/InputMethodManagerService;->mImeWindowVis:I

    #@41
    .line 922
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->updateImeWindowStatusLocked()V

    #@44
    .line 923
    return-void

    #@45
    .end local v1           #hardKeyShown:Z
    .end local v2           #haveHardKeyboard:Z
    .end local v3           #inputShown:Z
    .end local v4           #isScreenLocked:Z
    .end local v5           #isScreenSecurelyLocked:Z
    :cond_45
    move v2, v7

    #@46
    .line 910
    goto :goto_d

    #@47
    .restart local v2       #haveHardKeyboard:Z
    :cond_47
    move v1, v7

    #@48
    .line 912
    goto :goto_15

    #@49
    .restart local v1       #hardKeyShown:Z
    :cond_49
    move v4, v7

    #@4a
    .line 915
    goto :goto_22

    #@4b
    .restart local v4       #isScreenLocked:Z
    :cond_4b
    move v5, v7

    #@4c
    .line 917
    goto :goto_2d

    #@4d
    .restart local v5       #isScreenSecurelyLocked:Z
    :cond_4d
    move v3, v7

    #@4e
    .line 919
    goto :goto_38
.end method

.method private resetAllInternalStateLocked(Z)V
    .registers 8
    .parameter "updateOnlyWhenLocaleChanged"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 758
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@4
    if-nez v3, :cond_7

    #@6
    .line 797
    :cond_6
    :goto_6
    return-void

    #@7
    .line 762
    :cond_7
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@9
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@c
    move-result-object v3

    #@d
    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@f
    .line 763
    .local v1, newLocale:Ljava/util/Locale;
    if-eqz p1, :cond_1b

    #@11
    if-eqz v1, :cond_6

    #@13
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mLastSystemLocale:Ljava/util/Locale;

    #@15
    invoke-virtual {v1, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v3

    #@19
    if-nez v3, :cond_6

    #@1b
    .line 765
    :cond_1b
    if-nez p1, :cond_26

    #@1d
    .line 766
    invoke-virtual {p0, v4, v5}, Lcom/android/server/InputMethodManagerService;->hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@20
    .line 767
    iput-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@22
    .line 768
    const/4 v3, 0x1

    #@23
    invoke-virtual {p0, v3, v4}, Lcom/android/server/InputMethodManagerService;->unbindCurrentMethodLocked(ZZ)V

    #@26
    .line 774
    :cond_26
    new-instance v3, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;

    #@28
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@2a
    invoke-direct {v3, v4, p0}, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;-><init>(Landroid/content/Context;Lcom/android/server/InputMethodManagerService;)V

    #@2d
    iput-object v3, p0, Lcom/android/server/InputMethodManagerService;->mImListManager:Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;

    #@2f
    .line 775
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@31
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@33
    invoke-virtual {p0, v3, v4}, Lcom/android/server/InputMethodManagerService;->buildInputMethodListLocked(Ljava/util/ArrayList;Ljava/util/HashMap;)V

    #@36
    .line 776
    if-nez p1, :cond_5d

    #@38
    .line 777
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@3a
    invoke-virtual {v3}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    .line 778
    .local v2, selectedImiId:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_49

    #@44
    .line 781
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@46
    invoke-direct {p0, v3}, Lcom/android/server/InputMethodManagerService;->resetDefaultImeLocked(Landroid/content/Context;)V

    #@49
    .line 787
    .end local v2           #selectedImiId:Ljava/lang/String;
    :cond_49
    :goto_49
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->updateFromSettingsLocked()V

    #@4c
    .line 788
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mLastSystemLocale:Ljava/util/Locale;

    #@4e
    .line 789
    if-nez p1, :cond_6

    #@50
    .line 791
    :try_start_50
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->startInputInnerLocked()Lcom/android/internal/view/InputBindResult;
    :try_end_53
    .catch Ljava/lang/RuntimeException; {:try_start_50 .. :try_end_53} :catch_54

    #@53
    goto :goto_6

    #@54
    .line 792
    :catch_54
    move-exception v0

    #@55
    .line 793
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v3, "InputMethodManagerService"

    #@57
    const-string v4, "Unexpected exception"

    #@59
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5c
    goto :goto_6

    #@5d
    .line 785
    .end local v0           #e:Ljava/lang/RuntimeException;
    :cond_5d
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@5f
    invoke-direct {p0, v3}, Lcom/android/server/InputMethodManagerService;->resetDefaultImeLocked(Landroid/content/Context;)V

    #@62
    goto :goto_49
.end method

.method private resetDefaultImeLocked(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 735
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@2
    if-eqz v3, :cond_15

    #@4
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@6
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@8
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v3

    #@c
    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    #@e
    invoke-static {v3}, Lcom/android/server/InputMethodManagerService;->isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z

    #@11
    move-result v3

    #@12
    if-nez v3, :cond_15

    #@14
    .line 755
    :cond_14
    :goto_14
    return-void

    #@15
    .line 739
    :cond_15
    const/4 v0, 0x0

    #@16
    .line 740
    .local v0, defIm:Landroid/view/inputmethod/InputMethodInfo;
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v1

    #@1c
    .local v1, i$:Ljava/util/Iterator;
    :cond_1c
    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_4e

    #@22
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    #@28
    .line 741
    .local v2, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-nez v0, :cond_1c

    #@2a
    .line 742
    invoke-direct {p0, v2, p1}, Lcom/android/server/InputMethodManagerService;->isValidSystemDefaultIme(Landroid/view/inputmethod/InputMethodInfo;Landroid/content/Context;)Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_1c

    #@30
    .line 743
    move-object v0, v2

    #@31
    .line 744
    const-string v3, "InputMethodManagerService"

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "Selected default: "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_1c

    #@4e
    .line 748
    .end local v2           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_4e
    if-nez v0, :cond_78

    #@50
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@55
    move-result v3

    #@56
    if-lez v3, :cond_78

    #@58
    .line 749
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getMostApplicableDefaultIMELocked()Landroid/view/inputmethod/InputMethodInfo;

    #@5b
    move-result-object v0

    #@5c
    .line 750
    const-string v3, "InputMethodManagerService"

    #@5e
    new-instance v4, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v5, "No default found, using "

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 752
    :cond_78
    if-eqz v0, :cond_14

    #@7a
    .line 753
    const/4 v3, -0x1

    #@7b
    const/4 v4, 0x0

    #@7c
    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/InputMethodManagerService;->setSelectedInputMethodAndSubtypeLocked(Landroid/view/inputmethod/InputMethodInfo;IZ)V

    #@7f
    goto :goto_14
.end method

.method private resetSelectedInputMethodAndSubtypeLocked(Ljava/lang/String;)V
    .registers 9
    .parameter "newDefaultIme"

    #@0
    .prologue
    .line 3012
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    #@8
    .line 3013
    .local v1, imi:Landroid/view/inputmethod/InputMethodInfo;
    const/4 v2, -0x1

    #@9
    .line 3015
    .local v2, lastSubtypeId:I
    if-eqz v1, :cond_25

    #@b
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_25

    #@11
    .line 3016
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@13
    invoke-virtual {v4, p1}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getLastSubtypeForInputMethodLocked(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    .line 3017
    .local v3, subtypeHashCode:Ljava/lang/String;
    if-eqz v3, :cond_25

    #@19
    .line 3019
    :try_start_19
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@20
    move-result v4

    #@21
    invoke-static {v1, v4}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I
    :try_end_24
    .catch Ljava/lang/NumberFormatException; {:try_start_19 .. :try_end_24} :catch_2a

    #@24
    move-result v2

    #@25
    .line 3026
    .end local v3           #subtypeHashCode:Ljava/lang/String;
    :cond_25
    :goto_25
    const/4 v4, 0x0

    #@26
    invoke-direct {p0, v1, v2, v4}, Lcom/android/server/InputMethodManagerService;->setSelectedInputMethodAndSubtypeLocked(Landroid/view/inputmethod/InputMethodInfo;IZ)V

    #@29
    .line 3027
    return-void

    #@2a
    .line 3021
    .restart local v3       #subtypeHashCode:Ljava/lang/String;
    :catch_2a
    move-exception v0

    #@2b
    .line 3022
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v4, "InputMethodManagerService"

    #@2d
    new-instance v5, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v6, "HashCode for subtype looks broken: "

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@43
    goto :goto_25
.end method

.method private saveCurrentInputMethodAndSubtypeToHistory()V
    .registers 4

    #@0
    .prologue
    .line 2966
    sget-object v0, Lcom/android/server/InputMethodManagerService;->NOT_A_SUBTYPE_ID_STR:Ljava/lang/String;

    #@2
    .line 2967
    .local v0, subtypeId:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@4
    if-eqz v1, :cond_10

    #@6
    .line 2968
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@8
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@b
    move-result v1

    #@c
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 2970
    :cond_10
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@12
    invoke-direct {p0, v1}, Lcom/android/server/InputMethodManagerService;->canAddToLastInputMethod(Landroid/view/inputmethod/InputMethodSubtype;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1f

    #@18
    .line 2971
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@1a
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@1c
    invoke-virtual {v1, v2, v0}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->addSubtypeToHistory(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 2973
    :cond_1f
    return-void
.end method

.method private setImeWindowVisibilityStatusHiddenLocked()V
    .registers 2

    #@0
    .prologue
    .line 904
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/android/server/InputMethodManagerService;->mImeWindowVis:I

    #@3
    .line 905
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->updateImeWindowStatusLocked()V

    #@6
    .line 906
    return-void
.end method

.method private setInputMethodWithSubtypeId(Landroid/os/IBinder;Ljava/lang/String;I)V
    .registers 10
    .parameter "token"
    .parameter "id"
    .parameter "subtypeId"

    #@0
    .prologue
    .line 2245
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v3

    #@3
    .line 2246
    if-nez p1, :cond_1a

    #@5
    .line 2247
    :try_start_5
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@7
    const-string v4, "android.permission.WRITE_SECURE_SETTINGS"

    #@9
    invoke-virtual {v2, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_46

    #@f
    .line 2250
    new-instance v2, Ljava/lang/SecurityException;

    #@11
    const-string v4, "Using null token requires permission android.permission.WRITE_SECURE_SETTINGS"

    #@13
    invoke-direct {v2, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@16
    throw v2

    #@17
    .line 2266
    :catchall_17
    move-exception v2

    #@18
    monitor-exit v3
    :try_end_19
    .catchall {:try_start_5 .. :try_end_19} :catchall_17

    #@19
    throw v2

    #@1a
    .line 2254
    :cond_1a
    :try_start_1a
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@1c
    if-eq v2, p1, :cond_46

    #@1e
    .line 2255
    const-string v2, "InputMethodManagerService"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "Ignoring setInputMethod of uid "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, " token: "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 2257
    monitor-exit v3

    #@45
    .line 2267
    :goto_45
    return-void

    #@46
    .line 2260
    :cond_46
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_49
    .catchall {:try_start_1a .. :try_end_49} :catchall_17

    #@49
    move-result-wide v0

    #@4a
    .line 2262
    .local v0, ident:J
    :try_start_4a
    invoke-virtual {p0, p2, p3}, Lcom/android/server/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_52

    #@4d
    .line 2264
    :try_start_4d
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@50
    .line 2266
    monitor-exit v3

    #@51
    goto :goto_45

    #@52
    .line 2264
    :catchall_52
    move-exception v2

    #@53
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@56
    throw v2
    :try_end_57
    .catchall {:try_start_4d .. :try_end_57} :catchall_17
.end method

.method private setSelectedInputMethodAndSubtypeLocked(Landroid/view/inputmethod/InputMethodInfo;IZ)V
    .registers 7
    .parameter "imi"
    .parameter "subtypeId"
    .parameter "setSubtypeOnly"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 2978
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->saveCurrentInputMethodAndSubtypeToHistory()V

    #@4
    .line 2981
    if-eqz p1, :cond_8

    #@6
    if-gez p2, :cond_22

    #@8
    .line 2982
    :cond_8
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@a
    invoke-virtual {v1, v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->putSelectedSubtype(I)V

    #@d
    .line 2983
    const/4 v1, 0x0

    #@e
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@10
    .line 3005
    :goto_10
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@12
    if-eqz v1, :cond_21

    #@14
    if-nez p3, :cond_21

    #@16
    .line 3007
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@18
    if-eqz p1, :cond_44

    #@1a
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    :goto_1e
    invoke-virtual {v2, v1}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->putSelectedInputMethod(Ljava/lang/String;)V

    #@21
    .line 3009
    :cond_21
    return-void

    #@22
    .line 2985
    :cond_22
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@25
    move-result v1

    #@26
    if-ge p2, v1, :cond_38

    #@28
    .line 2986
    invoke-virtual {p1, p2}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@2b
    move-result-object v0

    #@2c
    .line 2987
    .local v0, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@2e
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@31
    move-result v2

    #@32
    invoke-virtual {v1, v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->putSelectedSubtype(I)V

    #@35
    .line 2988
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@37
    goto :goto_10

    #@38
    .line 2990
    .end local v0           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_38
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@3a
    invoke-virtual {v1, v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->putSelectedSubtype(I)V

    #@3d
    .line 2992
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getCurrentInputMethodSubtypeLocked()Landroid/view/inputmethod/InputMethodSubtype;

    #@40
    move-result-object v1

    #@41
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@43
    goto :goto_10

    #@44
    .line 3007
    :cond_44
    const-string v1, ""

    #@46
    goto :goto_1e
.end method

.method private showConfigureInputMethods()V
    .registers 5

    #@0
    .prologue
    .line 2633
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.settings.INPUT_METHOD_SETTINGS"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2634
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1420

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@c
    .line 2637
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@e
    const/4 v2, 0x0

    #@f
    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@11
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@14
    .line 2638
    return-void
.end method

.method private showInputMethodAndSubtypeEnabler(Ljava/lang/String;)V
    .registers 6
    .parameter "inputMethodId"

    #@0
    .prologue
    .line 2622
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.settings.INPUT_METHOD_SUBTYPE_SETTINGS"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2623
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1420

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@c
    .line 2626
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_17

    #@12
    .line 2627
    const-string v1, "input_method_id"

    #@14
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@17
    .line 2629
    :cond_17
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@19
    const/4 v2, 0x0

    #@1a
    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@1c
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@1f
    .line 2630
    return-void
.end method

.method private showInputMethodMenu()V
    .registers 2

    #@0
    .prologue
    .line 2614
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/server/InputMethodManagerService;->showInputMethodMenuInternal(Z)V

    #@4
    .line 2615
    return-void
.end method

.method private showInputMethodMenuInternal(Z)V
    .registers 30
    .parameter "showSubtypes"

    #@0
    .prologue
    .line 2643
    move-object/from16 v0, p0

    #@2
    iget-object v8, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@4
    .line 2644
    .local v8, context:Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v20

    #@8
    .line 2645
    .local v20, pm:Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    #@a
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@c
    move-object/from16 v23, v0

    #@e
    if-eqz v23, :cond_51

    #@10
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@14
    move-object/from16 v23, v0

    #@16
    invoke-virtual/range {v23 .. v23}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@19
    move-result v23

    #@1a
    if-eqz v23, :cond_51

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@20
    move-object/from16 v23, v0

    #@22
    invoke-virtual/range {v23 .. v23}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    #@25
    move-result v23

    #@26
    if-eqz v23, :cond_51

    #@28
    const/16 v16, 0x1

    #@2a
    .line 2648
    .local v16, isScreenLocked:Z
    :goto_2a
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@2e
    move-object/from16 v23, v0

    #@30
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@33
    move-result-object v18

    #@34
    .line 2649
    .local v18, lastInputMethodId:Ljava/lang/String;
    move-object/from16 v0, p0

    #@36
    move-object/from16 v1, v18

    #@38
    invoke-direct {v0, v1}, Lcom/android/server/InputMethodManagerService;->getSelectedInputMethodSubtypeId(Ljava/lang/String;)I

    #@3b
    move-result v19

    #@3c
    .line 2652
    .local v19, lastInputMethodSubtypeId:I
    move-object/from16 v0, p0

    #@3e
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@40
    move-object/from16 v24, v0

    #@42
    monitor-enter v24

    #@43
    .line 2653
    :try_start_43
    invoke-direct/range {p0 .. p0}, Lcom/android/server/InputMethodManagerService;->getExplicitlyOrImplicitlyEnabledInputMethodsAndSubtypeListLocked()Ljava/util/HashMap;

    #@46
    move-result-object v14

    #@47
    .line 2655
    .local v14, immis:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    if-eqz v14, :cond_4f

    #@49
    invoke-virtual {v14}, Ljava/util/HashMap;->size()I

    #@4c
    move-result v23

    #@4d
    if-nez v23, :cond_54

    #@4f
    .line 2656
    :cond_4f
    monitor-exit v24

    #@50
    .line 2774
    :goto_50
    return-void

    #@51
    .line 2645
    .end local v14           #immis:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    .end local v16           #isScreenLocked:Z
    .end local v18           #lastInputMethodId:Ljava/lang/String;
    .end local v19           #lastInputMethodSubtypeId:I
    :cond_51
    const/16 v16, 0x0

    #@53
    goto :goto_2a

    #@54
    .line 2659
    .restart local v14       #immis:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    .restart local v16       #isScreenLocked:Z
    .restart local v18       #lastInputMethodId:Ljava/lang/String;
    .restart local v19       #lastInputMethodSubtypeId:I
    :cond_54
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/InputMethodManagerService;->hideInputMethodMenuLocked()V

    #@57
    .line 2661
    move-object/from16 v0, p0

    #@59
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mImListManager:Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;

    #@5b
    move-object/from16 v23, v0

    #@5d
    move-object/from16 v0, p0

    #@5f
    iget-boolean v0, v0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@61
    move/from16 v25, v0

    #@63
    move-object/from16 v0, v23

    #@65
    move/from16 v1, p1

    #@67
    move/from16 v2, v25

    #@69
    move/from16 v3, v16

    #@6b
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->getSortedInputMethodAndSubtypeList(ZZZ)Ljava/util/List;

    #@6e
    move-result-object v13

    #@6f
    .line 2665
    .local v13, imList:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    const/16 v23, -0x1

    #@71
    move/from16 v0, v19

    #@73
    move/from16 v1, v23

    #@75
    if-ne v0, v1, :cond_9d

    #@77
    .line 2666
    invoke-direct/range {p0 .. p0}, Lcom/android/server/InputMethodManagerService;->getCurrentInputMethodSubtypeLocked()Landroid/view/inputmethod/InputMethodSubtype;

    #@7a
    move-result-object v10

    #@7b
    .line 2667
    .local v10, currentSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    if-eqz v10, :cond_9d

    #@7d
    .line 2668
    move-object/from16 v0, p0

    #@7f
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@81
    move-object/from16 v23, v0

    #@83
    move-object/from16 v0, p0

    #@85
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@87
    move-object/from16 v25, v0

    #@89
    move-object/from16 v0, v23

    #@8b
    move-object/from16 v1, v25

    #@8d
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    move-result-object v9

    #@91
    check-cast v9, Landroid/view/inputmethod/InputMethodInfo;

    #@93
    .line 2669
    .local v9, currentImi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v10}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@96
    move-result v23

    #@97
    move/from16 v0, v23

    #@99
    invoke-static {v9, v0}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@9c
    move-result v19

    #@9d
    .line 2674
    .end local v9           #currentImi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v10           #currentSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_9d
    invoke-interface {v13}, Ljava/util/List;->size()I

    #@a0
    move-result v4

    #@a1
    .line 2675
    .local v4, N:I
    new-array v0, v4, [Landroid/view/inputmethod/InputMethodInfo;

    #@a3
    move-object/from16 v23, v0

    #@a5
    move-object/from16 v0, v23

    #@a7
    move-object/from16 v1, p0

    #@a9
    iput-object v0, v1, Lcom/android/server/InputMethodManagerService;->mIms:[Landroid/view/inputmethod/InputMethodInfo;

    #@ab
    .line 2676
    new-array v0, v4, [I

    #@ad
    move-object/from16 v23, v0

    #@af
    move-object/from16 v0, v23

    #@b1
    move-object/from16 v1, p0

    #@b3
    iput-object v0, v1, Lcom/android/server/InputMethodManagerService;->mSubtypeIds:[I

    #@b5
    .line 2677
    const/4 v7, 0x0

    #@b6
    .line 2678
    .local v7, checkedItem:I
    const/4 v12, 0x0

    #@b7
    .local v12, i:I
    :goto_b7
    if-ge v12, v4, :cond_115

    #@b9
    .line 2679
    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@bc
    move-result-object v17

    #@bd
    check-cast v17, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@bf
    .line 2680
    .local v17, item:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    move-object/from16 v0, p0

    #@c1
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mIms:[Landroid/view/inputmethod/InputMethodInfo;

    #@c3
    move-object/from16 v23, v0

    #@c5
    move-object/from16 v0, v17

    #@c7
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    #@c9
    move-object/from16 v25, v0

    #@cb
    aput-object v25, v23, v12

    #@cd
    .line 2681
    move-object/from16 v0, p0

    #@cf
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSubtypeIds:[I

    #@d1
    move-object/from16 v23, v0

    #@d3
    move-object/from16 v0, v17

    #@d5
    iget v0, v0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mSubtypeId:I

    #@d7
    move/from16 v25, v0

    #@d9
    aput v25, v23, v12

    #@db
    .line 2682
    move-object/from16 v0, p0

    #@dd
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mIms:[Landroid/view/inputmethod/InputMethodInfo;

    #@df
    move-object/from16 v23, v0

    #@e1
    aget-object v23, v23, v12

    #@e3
    invoke-virtual/range {v23 .. v23}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@e6
    move-result-object v23

    #@e7
    move-object/from16 v0, v23

    #@e9
    move-object/from16 v1, v18

    #@eb
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ee
    move-result v23

    #@ef
    if-eqz v23, :cond_112

    #@f1
    .line 2683
    move-object/from16 v0, p0

    #@f3
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSubtypeIds:[I

    #@f5
    move-object/from16 v23, v0

    #@f7
    aget v21, v23, v12

    #@f9
    .line 2684
    .local v21, subtypeId:I
    const/16 v23, -0x1

    #@fb
    move/from16 v0, v21

    #@fd
    move/from16 v1, v23

    #@ff
    if-eq v0, v1, :cond_111

    #@101
    const/16 v23, -0x1

    #@103
    move/from16 v0, v19

    #@105
    move/from16 v1, v23

    #@107
    if-ne v0, v1, :cond_10b

    #@109
    if-eqz v21, :cond_111

    #@10b
    :cond_10b
    move/from16 v0, v21

    #@10d
    move/from16 v1, v19

    #@10f
    if-ne v0, v1, :cond_112

    #@111
    .line 2687
    :cond_111
    move v7, v12

    #@112
    .line 2678
    .end local v21           #subtypeId:I
    :cond_112
    add-int/lit8 v12, v12, 0x1

    #@114
    goto :goto_b7

    #@115
    .line 2691
    .end local v17           #item:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    :cond_115
    const/16 v23, 0x0

    #@117
    sget-object v25, Lcom/android/internal/R$styleable;->DialogPreference:[I

    #@119
    const v26, 0x101005d

    #@11c
    const/16 v27, 0x0

    #@11e
    move-object/from16 v0, v23

    #@120
    move-object/from16 v1, v25

    #@122
    move/from16 v2, v26

    #@124
    move/from16 v3, v27

    #@126
    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@129
    move-result-object v5

    #@12a
    .line 2694
    .local v5, a:Landroid/content/res/TypedArray;
    new-instance v23, Landroid/app/AlertDialog$Builder;

    #@12c
    move-object/from16 v0, v23

    #@12e
    invoke-direct {v0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@131
    new-instance v25, Lcom/android/server/InputMethodManagerService$4;

    #@133
    move-object/from16 v0, v25

    #@135
    move-object/from16 v1, p0

    #@137
    invoke-direct {v0, v1}, Lcom/android/server/InputMethodManagerService$4;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@13a
    move-object/from16 v0, v23

    #@13c
    move-object/from16 v1, v25

    #@13e
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@141
    move-result-object v23

    #@142
    const/16 v25, 0x0

    #@144
    move/from16 v0, v25

    #@146
    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@149
    move-result-object v25

    #@14a
    move-object/from16 v0, v23

    #@14c
    move-object/from16 v1, v25

    #@14e
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    #@151
    move-result-object v23

    #@152
    move-object/from16 v0, v23

    #@154
    move-object/from16 v1, p0

    #@156
    iput-object v0, v1, Lcom/android/server/InputMethodManagerService;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    #@158
    .line 2703
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    #@15b
    .line 2704
    move-object/from16 v0, p0

    #@15d
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@15f
    move-object/from16 v23, v0

    #@161
    const-string v25, "layout_inflater"

    #@163
    move-object/from16 v0, v23

    #@165
    move-object/from16 v1, v25

    #@167
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@16a
    move-result-object v15

    #@16b
    check-cast v15, Landroid/view/LayoutInflater;

    #@16d
    .line 2706
    .local v15, inflater:Landroid/view/LayoutInflater;
    const v23, 0x109004c

    #@170
    const/16 v25, 0x0

    #@172
    move/from16 v0, v23

    #@174
    move-object/from16 v1, v25

    #@176
    invoke-virtual {v15, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@179
    move-result-object v22

    #@17a
    .line 2708
    .local v22, tv:Landroid/view/View;
    move-object/from16 v0, p0

    #@17c
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    #@17e
    move-object/from16 v23, v0

    #@180
    move-object/from16 v0, v23

    #@182
    move-object/from16 v1, v22

    #@184
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@187
    .line 2711
    move-object/from16 v0, v22

    #@189
    move-object/from16 v1, p0

    #@18b
    iput-object v0, v1, Lcom/android/server/InputMethodManagerService;->mSwitchingDialogTitleView:Landroid/view/View;

    #@18d
    .line 2712
    move-object/from16 v0, p0

    #@18f
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialogTitleView:Landroid/view/View;

    #@191
    move-object/from16 v23, v0

    #@193
    const v25, 0x10202ac

    #@196
    move-object/from16 v0, v23

    #@198
    move/from16 v1, v25

    #@19a
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@19d
    move-result-object v25

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    #@1a2
    move-object/from16 v23, v0

    #@1a4
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/wm/WindowManagerService;->isHardKeyboardAvailable()Z

    #@1a7
    move-result v23

    #@1a8
    if-eqz v23, :cond_296

    #@1aa
    const/16 v23, 0x0

    #@1ac
    :goto_1ac
    move-object/from16 v0, v25

    #@1ae
    move/from16 v1, v23

    #@1b0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@1b3
    .line 2716
    move-object/from16 v0, p0

    #@1b5
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialogTitleView:Landroid/view/View;

    #@1b7
    move-object/from16 v23, v0

    #@1b9
    const v25, 0x10202ad

    #@1bc
    move-object/from16 v0, v23

    #@1be
    move/from16 v1, v25

    #@1c0
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1c3
    move-result-object v11

    #@1c4
    check-cast v11, Landroid/widget/Switch;

    #@1c6
    .line 2718
    .local v11, hardKeySwitch:Landroid/widget/Switch;
    move-object/from16 v0, p0

    #@1c8
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    #@1ca
    move-object/from16 v23, v0

    #@1cc
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/wm/WindowManagerService;->isHardKeyboardEnabled()Z

    #@1cf
    move-result v23

    #@1d0
    move/from16 v0, v23

    #@1d2
    invoke-virtual {v11, v0}, Landroid/widget/Switch;->setChecked(Z)V

    #@1d5
    .line 2719
    new-instance v23, Lcom/android/server/InputMethodManagerService$5;

    #@1d7
    move-object/from16 v0, v23

    #@1d9
    move-object/from16 v1, p0

    #@1db
    invoke-direct {v0, v1}, Lcom/android/server/InputMethodManagerService$5;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@1de
    move-object/from16 v0, v23

    #@1e0
    invoke-virtual {v11, v0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    #@1e3
    .line 2728
    new-instance v6, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;

    #@1e5
    const v23, 0x10900cd

    #@1e8
    move/from16 v0, v23

    #@1ea
    invoke-direct {v6, v8, v0, v13, v7}, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;I)V

    #@1ed
    .line 2732
    .local v6, adapter:Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;
    move-object/from16 v0, p0

    #@1ef
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    #@1f1
    move-object/from16 v23, v0

    #@1f3
    new-instance v25, Lcom/android/server/InputMethodManagerService$6;

    #@1f5
    move-object/from16 v0, v25

    #@1f7
    move-object/from16 v1, p0

    #@1f9
    invoke-direct {v0, v1}, Lcom/android/server/InputMethodManagerService$6;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@1fc
    move-object/from16 v0, v23

    #@1fe
    move-object/from16 v1, v25

    #@200
    invoke-virtual {v0, v6, v7, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@203
    .line 2755
    if-eqz p1, :cond_222

    #@205
    if-nez v16, :cond_222

    #@207
    .line 2756
    move-object/from16 v0, p0

    #@209
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    #@20b
    move-object/from16 v23, v0

    #@20d
    const v25, 0x104047c

    #@210
    new-instance v26, Lcom/android/server/InputMethodManagerService$7;

    #@212
    move-object/from16 v0, v26

    #@214
    move-object/from16 v1, p0

    #@216
    invoke-direct {v0, v1}, Lcom/android/server/InputMethodManagerService$7;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@219
    move-object/from16 v0, v23

    #@21b
    move/from16 v1, v25

    #@21d
    move-object/from16 v2, v26

    #@21f
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@222
    .line 2765
    :cond_222
    move-object/from16 v0, p0

    #@224
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    #@226
    move-object/from16 v23, v0

    #@228
    invoke-virtual/range {v23 .. v23}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@22b
    move-result-object v23

    #@22c
    move-object/from16 v0, v23

    #@22e
    move-object/from16 v1, p0

    #@230
    iput-object v0, v1, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@232
    .line 2766
    move-object/from16 v0, p0

    #@234
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@236
    move-object/from16 v23, v0

    #@238
    const/16 v25, 0x1

    #@23a
    move-object/from16 v0, v23

    #@23c
    move/from16 v1, v25

    #@23e
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    #@241
    .line 2767
    move-object/from16 v0, p0

    #@243
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@245
    move-object/from16 v23, v0

    #@247
    invoke-virtual/range {v23 .. v23}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@24a
    move-result-object v23

    #@24b
    const/16 v25, 0x7dc

    #@24d
    move-object/from16 v0, v23

    #@24f
    move/from16 v1, v25

    #@251
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@254
    .line 2769
    move-object/from16 v0, p0

    #@256
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@258
    move-object/from16 v23, v0

    #@25a
    invoke-virtual/range {v23 .. v23}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@25d
    move-result-object v23

    #@25e
    invoke-virtual/range {v23 .. v23}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@261
    move-result-object v23

    #@262
    move-object/from16 v0, v23

    #@264
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@266
    move/from16 v25, v0

    #@268
    or-int/lit8 v25, v25, 0x10

    #@26a
    move/from16 v0, v25

    #@26c
    move-object/from16 v1, v23

    #@26e
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@270
    .line 2771
    move-object/from16 v0, p0

    #@272
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@274
    move-object/from16 v23, v0

    #@276
    invoke-virtual/range {v23 .. v23}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@279
    move-result-object v23

    #@27a
    invoke-virtual/range {v23 .. v23}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@27d
    move-result-object v23

    #@27e
    const-string v25, "Select input method"

    #@280
    move-object/from16 v0, v23

    #@282
    move-object/from16 v1, v25

    #@284
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@287
    .line 2772
    move-object/from16 v0, p0

    #@289
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@28b
    move-object/from16 v23, v0

    #@28d
    invoke-virtual/range {v23 .. v23}, Landroid/app/AlertDialog;->show()V

    #@290
    .line 2773
    monitor-exit v24

    #@291
    goto/16 :goto_50

    #@293
    .end local v4           #N:I
    .end local v5           #a:Landroid/content/res/TypedArray;
    .end local v6           #adapter:Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;
    .end local v7           #checkedItem:I
    .end local v11           #hardKeySwitch:Landroid/widget/Switch;
    .end local v12           #i:I
    .end local v13           #imList:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    .end local v14           #immis:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    .end local v15           #inflater:Landroid/view/LayoutInflater;
    .end local v22           #tv:Landroid/view/View;
    :catchall_293
    move-exception v23

    #@294
    monitor-exit v24
    :try_end_295
    .catchall {:try_start_43 .. :try_end_295} :catchall_293

    #@295
    throw v23

    #@296
    .line 2712
    .restart local v4       #N:I
    .restart local v5       #a:Landroid/content/res/TypedArray;
    .restart local v7       #checkedItem:I
    .restart local v12       #i:I
    .restart local v13       #imList:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    .restart local v14       #immis:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    .restart local v15       #inflater:Landroid/view/LayoutInflater;
    .restart local v22       #tv:Landroid/view/View;
    :cond_296
    const/16 v23, 0x8

    #@298
    goto/16 :goto_1ac
.end method

.method private showInputMethodSubtypeMenu()V
    .registers 2

    #@0
    .prologue
    .line 2618
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Lcom/android/server/InputMethodManagerService;->showInputMethodMenuInternal(Z)V

    #@4
    .line 2619
    return-void
.end method

.method private switchUserLocked(I)V
    .registers 4
    .parameter "newUserId"

    #@0
    .prologue
    .line 804
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->setCurrentUserId(I)V

    #@5
    .line 806
    new-instance v0, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@7
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    invoke-direct {v0, v1, p1}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;-><init>(Ljava/util/HashMap;I)V

    #@c
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mFileManager:Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@e
    .line 807
    const/4 v0, 0x0

    #@f
    invoke-direct {p0, v0}, Lcom/android/server/InputMethodManagerService;->resetAllInternalStateLocked(Z)V

    #@12
    .line 808
    return-void
.end method

.method private updateImeWindowStatusLocked()V
    .registers 4

    #@0
    .prologue
    .line 926
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@2
    iget v1, p0, Lcom/android/server/InputMethodManagerService;->mImeWindowVis:I

    #@4
    iget v2, p0, Lcom/android/server/InputMethodManagerService;->mBackDisposition:I

    #@6
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/InputMethodManagerService;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@9
    .line 927
    return-void
.end method


# virtual methods
.method public addClient(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;II)V
    .registers 14
    .parameter "client"
    .parameter "inputContext"
    .parameter "uid"
    .parameter "pid"

    #@0
    .prologue
    .line 1034
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1041
    :goto_6
    return-void

    #@7
    .line 1037
    :cond_7
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v6

    #@a
    .line 1038
    :try_start_a
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mClients:Ljava/util/HashMap;

    #@c
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@f
    move-result-object v8

    #@10
    new-instance v0, Lcom/android/server/InputMethodManagerService$ClientState;

    #@12
    move-object v1, p0

    #@13
    move-object v2, p1

    #@14
    move-object v3, p2

    #@15
    move v4, p3

    #@16
    move v5, p4

    #@17
    invoke-direct/range {v0 .. v5}, Lcom/android/server/InputMethodManagerService$ClientState;-><init>(Lcom/android/server/InputMethodManagerService;Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;II)V

    #@1a
    invoke-virtual {v7, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    .line 1040
    monitor-exit v6

    #@1e
    goto :goto_6

    #@1f
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v6
    :try_end_21
    .catchall {:try_start_a .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method

.method attachNewInputLocked(Z)Lcom/android/internal/view/InputBindResult;
    .registers 8
    .parameter "initial"

    #@0
    .prologue
    .line 1107
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mBoundToMethod:Z

    #@2
    if-nez v1, :cond_1a

    #@4
    .line 1108
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@6
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@8
    const/16 v3, 0x3f2

    #@a
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@c
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@e
    iget-object v5, v5, Lcom/android/server/InputMethodManagerService$ClientState;->binding:Landroid/view/inputmethod/InputBinding;

    #@10
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {p0, v1, v2}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@17
    .line 1110
    const/4 v1, 0x1

    #@18
    iput-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mBoundToMethod:Z

    #@1a
    .line 1112
    :cond_1a
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@1c
    iget-object v0, v1, Lcom/android/server/InputMethodManagerService$ClientState;->curSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@1e
    .line 1113
    .local v0, session:Lcom/android/server/InputMethodManagerService$SessionState;
    if-eqz p1, :cond_49

    #@20
    .line 1114
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$SessionState;->method:Lcom/android/internal/view/IInputMethod;

    #@22
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@24
    const/16 v3, 0x7d0

    #@26
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurInputContext:Lcom/android/internal/view/IInputContext;

    #@28
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurAttribute:Landroid/view/inputmethod/EditorInfo;

    #@2a
    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOOO(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {p0, v1, v2}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@31
    .line 1120
    :goto_31
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mShowRequested:Z

    #@33
    if-eqz v1, :cond_3d

    #@35
    .line 1122
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getAppShowFlags()I

    #@38
    move-result v1

    #@39
    const/4 v2, 0x0

    #@3a
    invoke-virtual {p0, v1, v2}, Lcom/android/server/InputMethodManagerService;->showCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@3d
    .line 1124
    :cond_3d
    new-instance v1, Lcom/android/internal/view/InputBindResult;

    #@3f
    iget-object v2, v0, Lcom/android/server/InputMethodManagerService$SessionState;->session:Lcom/android/internal/view/IInputMethodSession;

    #@41
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@43
    iget v4, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@45
    invoke-direct {v1, v2, v3, v4}, Lcom/android/internal/view/InputBindResult;-><init>(Lcom/android/internal/view/IInputMethodSession;Ljava/lang/String;I)V

    #@48
    return-object v1

    #@49
    .line 1117
    :cond_49
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$SessionState;->method:Lcom/android/internal/view/IInputMethod;

    #@4b
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4d
    const/16 v3, 0x7da

    #@4f
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurInputContext:Lcom/android/internal/view/IInputContext;

    #@51
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurAttribute:Landroid/view/inputmethod/EditorInfo;

    #@53
    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOOO(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {p0, v1, v2}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@5a
    goto :goto_31
.end method

.method buildInputMethodListLocked(Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .registers 23
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2544
    .local p1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodInfo;>;"
    .local p2, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    #@3
    .line 2545
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->clear()V

    #@6
    .line 2548
    move-object/from16 v0, p0

    #@8
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@a
    move-object/from16 v17, v0

    #@c
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@f
    move-result-object v13

    #@10
    .line 2549
    .local v13, pm:Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@14
    move-object/from16 v17, v0

    #@16
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@19
    move-result-object v5

    #@1a
    .line 2550
    .local v5, config:Landroid/content/res/Configuration;
    iget v0, v5, Landroid/content/res/Configuration;->keyboard:I

    #@1c
    move/from16 v17, v0

    #@1e
    const/16 v18, 0x2

    #@20
    move/from16 v0, v17

    #@22
    move/from16 v1, v18

    #@24
    if-ne v0, v1, :cond_bb

    #@26
    const/4 v9, 0x1

    #@27
    .line 2551
    .local v9, haveHardKeyboard:Z
    :goto_27
    move-object/from16 v0, p0

    #@29
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@2b
    move-object/from16 v17, v0

    #@2d
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getDisabledSystemInputMethods()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    .line 2552
    .local v7, disabledSysImes:Ljava/lang/String;
    if-nez v7, :cond_35

    #@33
    const-string v7, ""

    #@35
    .line 2554
    :cond_35
    new-instance v17, Landroid/content/Intent;

    #@37
    const-string v18, "android.view.InputMethod"

    #@39
    invoke-direct/range {v17 .. v18}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3c
    const/16 v18, 0x80

    #@3e
    move-object/from16 v0, p0

    #@40
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@42
    move-object/from16 v19, v0

    #@44
    invoke-virtual/range {v19 .. v19}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getCurrentUserId()I

    #@47
    move-result v19

    #@48
    move-object/from16 v0, v17

    #@4a
    move/from16 v1, v18

    #@4c
    move/from16 v2, v19

    #@4e
    invoke-virtual {v13, v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentServicesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@51
    move-result-object v15

    #@52
    .line 2558
    .local v15, services:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    move-object/from16 v0, p0

    #@54
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mFileManager:Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@56
    move-object/from16 v17, v0

    #@58
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->getAllAdditionalInputMethodSubtypes()Ljava/util/HashMap;

    #@5b
    move-result-object v3

    #@5c
    .line 2560
    .local v3, additionalSubtypes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    const/4 v10, 0x0

    #@5d
    .local v10, i:I
    :goto_5d
    invoke-interface {v15}, Ljava/util/List;->size()I

    #@60
    move-result v17

    #@61
    move/from16 v0, v17

    #@63
    if-ge v10, v0, :cond_139

    #@65
    .line 2561
    invoke-interface {v15, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@68
    move-result-object v14

    #@69
    check-cast v14, Landroid/content/pm/ResolveInfo;

    #@6b
    .line 2562
    .local v14, ri:Landroid/content/pm/ResolveInfo;
    iget-object v0, v14, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@6d
    move-object/from16 v16, v0

    #@6f
    .line 2563
    .local v16, si:Landroid/content/pm/ServiceInfo;
    new-instance v4, Landroid/content/ComponentName;

    #@71
    move-object/from16 v0, v16

    #@73
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@75
    move-object/from16 v17, v0

    #@77
    move-object/from16 v0, v16

    #@79
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@7b
    move-object/from16 v18, v0

    #@7d
    move-object/from16 v0, v17

    #@7f
    move-object/from16 v1, v18

    #@81
    invoke-direct {v4, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@84
    .line 2564
    .local v4, compName:Landroid/content/ComponentName;
    const-string v17, "android.permission.BIND_INPUT_METHOD"

    #@86
    move-object/from16 v0, v16

    #@88
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@8a
    move-object/from16 v18, v0

    #@8c
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v17

    #@90
    if-nez v17, :cond_be

    #@92
    .line 2566
    const-string v17, "InputMethodManagerService"

    #@94
    new-instance v18, Ljava/lang/StringBuilder;

    #@96
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v19, "Skipping input method "

    #@9b
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v18

    #@9f
    move-object/from16 v0, v18

    #@a1
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v18

    #@a5
    const-string v19, ": it does not require the permission "

    #@a7
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v18

    #@ab
    const-string v19, "android.permission.BIND_INPUT_METHOD"

    #@ad
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v18

    #@b1
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v18

    #@b5
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 2560
    :cond_b8
    :goto_b8
    add-int/lit8 v10, v10, 0x1

    #@ba
    goto :goto_5d

    #@bb
    .line 2550
    .end local v3           #additionalSubtypes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    .end local v4           #compName:Landroid/content/ComponentName;
    .end local v7           #disabledSysImes:Ljava/lang/String;
    .end local v9           #haveHardKeyboard:Z
    .end local v10           #i:I
    .end local v14           #ri:Landroid/content/pm/ResolveInfo;
    .end local v15           #services:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v16           #si:Landroid/content/pm/ServiceInfo;
    :cond_bb
    const/4 v9, 0x0

    #@bc
    goto/16 :goto_27

    #@be
    .line 2575
    .restart local v3       #additionalSubtypes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    .restart local v4       #compName:Landroid/content/ComponentName;
    .restart local v7       #disabledSysImes:Ljava/lang/String;
    .restart local v9       #haveHardKeyboard:Z
    .restart local v10       #i:I
    .restart local v14       #ri:Landroid/content/pm/ResolveInfo;
    .restart local v15       #services:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v16       #si:Landroid/content/pm/ServiceInfo;
    :cond_be
    :try_start_be
    new-instance v12, Landroid/view/inputmethod/InputMethodInfo;

    #@c0
    move-object/from16 v0, p0

    #@c2
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@c4
    move-object/from16 v17, v0

    #@c6
    move-object/from16 v0, v17

    #@c8
    invoke-direct {v12, v0, v14, v3}, Landroid/view/inputmethod/InputMethodInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Ljava/util/Map;)V

    #@cb
    .line 2576
    .local v12, p:Landroid/view/inputmethod/InputMethodInfo;
    move-object/from16 v0, p1

    #@cd
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d0
    .line 2577
    invoke-virtual {v12}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@d3
    move-result-object v11

    #@d4
    .line 2578
    .local v11, id:Ljava/lang/String;
    move-object/from16 v0, p2

    #@d6
    invoke-virtual {v0, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d9
    .line 2582
    move-object/from16 v0, p0

    #@db
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@dd
    move-object/from16 v17, v0

    #@df
    move-object/from16 v0, p0

    #@e1
    move-object/from16 v1, v17

    #@e3
    invoke-direct {v0, v12, v1}, Lcom/android/server/InputMethodManagerService;->isValidSystemDefaultIme(Landroid/view/inputmethod/InputMethodInfo;Landroid/content/Context;)Z

    #@e6
    move-result v17

    #@e7
    if-nez v17, :cond_ef

    #@e9
    invoke-static {v12}, Lcom/android/server/InputMethodManagerService;->isSystemImeThatHasEnglishSubtype(Landroid/view/inputmethod/InputMethodInfo;)Z

    #@ec
    move-result v17

    #@ed
    if-eqz v17, :cond_b8

    #@ef
    .line 2583
    :cond_ef
    const/16 v17, 0x1

    #@f1
    move-object/from16 v0, p0

    #@f3
    move/from16 v1, v17

    #@f5
    invoke-virtual {v0, v11, v1}, Lcom/android/server/InputMethodManagerService;->setInputMethodEnabledLocked(Ljava/lang/String;Z)Z
    :try_end_f8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_be .. :try_end_f8} :catch_f9
    .catch Ljava/io/IOException; {:try_start_be .. :try_end_f8} :catch_119

    #@f8
    goto :goto_b8

    #@f9
    .line 2590
    .end local v11           #id:Ljava/lang/String;
    .end local v12           #p:Landroid/view/inputmethod/InputMethodInfo;
    :catch_f9
    move-exception v8

    #@fa
    .line 2591
    .local v8, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v17, "InputMethodManagerService"

    #@fc
    new-instance v18, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    const-string v19, "Unable to load input method "

    #@103
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v18

    #@107
    move-object/from16 v0, v18

    #@109
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v18

    #@10d
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v18

    #@111
    move-object/from16 v0, v17

    #@113
    move-object/from16 v1, v18

    #@115
    invoke-static {v0, v1, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@118
    goto :goto_b8

    #@119
    .line 2592
    .end local v8           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_119
    move-exception v8

    #@11a
    .line 2593
    .local v8, e:Ljava/io/IOException;
    const-string v17, "InputMethodManagerService"

    #@11c
    new-instance v18, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v19, "Unable to load input method "

    #@123
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v18

    #@127
    move-object/from16 v0, v18

    #@129
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v18

    #@12d
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v18

    #@131
    move-object/from16 v0, v17

    #@133
    move-object/from16 v1, v18

    #@135
    invoke-static {v0, v1, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@138
    goto :goto_b8

    #@139
    .line 2597
    .end local v4           #compName:Landroid/content/ComponentName;
    .end local v8           #e:Ljava/io/IOException;
    .end local v14           #ri:Landroid/content/pm/ResolveInfo;
    .end local v16           #si:Landroid/content/pm/ServiceInfo;
    :cond_139
    move-object/from16 v0, p0

    #@13b
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@13d
    move-object/from16 v17, v0

    #@13f
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@142
    move-result-object v6

    #@143
    .line 2598
    .local v6, defaultImiId:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@146
    move-result v17

    #@147
    if-nez v17, :cond_161

    #@149
    .line 2599
    move-object/from16 v0, p2

    #@14b
    invoke-virtual {v0, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@14e
    move-result v17

    #@14f
    if-nez v17, :cond_162

    #@151
    .line 2600
    const-string v17, "InputMethodManagerService"

    #@153
    const-string v18, "Default IME is uninstalled. Choose new default IME."

    #@155
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    .line 2601
    invoke-direct/range {p0 .. p0}, Lcom/android/server/InputMethodManagerService;->chooseNewDefaultIMELocked()Z

    #@15b
    move-result v17

    #@15c
    if-eqz v17, :cond_161

    #@15e
    .line 2602
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/InputMethodManagerService;->updateFromSettingsLocked()V

    #@161
    .line 2609
    :cond_161
    :goto_161
    return-void

    #@162
    .line 2606
    :cond_162
    const/16 v17, 0x1

    #@164
    move-object/from16 v0, p0

    #@166
    move/from16 v1, v17

    #@168
    invoke-virtual {v0, v6, v1}, Lcom/android/server/InputMethodManagerService;->setInputMethodEnabledLocked(Ljava/lang/String;Z)Z

    #@16b
    goto :goto_161
.end method

.method clearCurMethodLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 1384
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@4
    if-eqz v2, :cond_2f

    #@6
    .line 1385
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mClients:Ljava/util/HashMap;

    #@8
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@b
    move-result-object v2

    #@c
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v1

    #@10
    .local v1, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_26

    #@16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/android/server/InputMethodManagerService$ClientState;

    #@1c
    .line 1386
    .local v0, cs:Lcom/android/server/InputMethodManagerService$ClientState;
    iput-boolean v4, v0, Lcom/android/server/InputMethodManagerService$ClientState;->sessionRequested:Z

    #@1e
    .line 1387
    iget-object v2, v0, Lcom/android/server/InputMethodManagerService$ClientState;->curSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@20
    invoke-direct {p0, v2}, Lcom/android/server/InputMethodManagerService;->finishSession(Lcom/android/server/InputMethodManagerService$SessionState;)V

    #@23
    .line 1388
    iput-object v3, v0, Lcom/android/server/InputMethodManagerService$ClientState;->curSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@25
    goto :goto_10

    #@26
    .line 1391
    .end local v0           #cs:Lcom/android/server/InputMethodManagerService$ClientState;
    :cond_26
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@28
    invoke-direct {p0, v2}, Lcom/android/server/InputMethodManagerService;->finishSession(Lcom/android/server/InputMethodManagerService$SessionState;)V

    #@2b
    .line 1392
    iput-object v3, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@2d
    .line 1393
    iput-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@2f
    .line 1395
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_2f
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@31
    if-eqz v2, :cond_3a

    #@33
    .line 1396
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@35
    const-string v3, "ime"

    #@37
    invoke-virtual {v2, v3, v4}, Lcom/android/server/StatusBarManagerService;->setIconVisibility(Ljava/lang/String;Z)V

    #@3a
    .line 1398
    :cond_3a
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 16
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 4157
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v10, "android.permission.DUMP"

    #@4
    invoke-virtual {v9, v10}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v9

    #@8
    if-eqz v9, :cond_33

    #@a
    .line 4160
    new-instance v9, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v10, "Permission Denial: can\'t dump InputMethodManager from from pid="

    #@11
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v9

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v10

    #@19
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v9

    #@1d
    const-string v10, ", uid="

    #@1f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v9

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v10

    #@27
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v9

    #@2b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v9

    #@2f
    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 4229
    :goto_32
    return-void

    #@33
    .line 4169
    :cond_33
    new-instance v8, Landroid/util/PrintWriterPrinter;

    #@35
    invoke-direct {v8, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@38
    .line 4171
    .local v8, p:Landroid/util/Printer;
    iget-object v10, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@3a
    monitor-enter v10

    #@3b
    .line 4172
    :try_start_3b
    const-string v9, "Current Input Method Manager state:"

    #@3d
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@40
    .line 4173
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@45
    move-result v0

    #@46
    .line 4174
    .local v0, N:I
    const-string v9, "  Input Methods:"

    #@48
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@4b
    .line 4175
    const/4 v4, 0x0

    #@4c
    .local v4, i:I
    :goto_4c
    if-ge v4, v0, :cond_7a

    #@4e
    .line 4176
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@53
    move-result-object v6

    #@54
    check-cast v6, Landroid/view/inputmethod/InputMethodInfo;

    #@56
    .line 4177
    .local v6, info:Landroid/view/inputmethod/InputMethodInfo;
    new-instance v9, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v11, "  InputMethod #"

    #@5d
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v9

    #@61
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    const-string v11, ":"

    #@67
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v9

    #@6b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v9

    #@6f
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@72
    .line 4178
    const-string v9, "    "

    #@74
    invoke-virtual {v6, v8, v9}, Landroid/view/inputmethod/InputMethodInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@77
    .line 4175
    add-int/lit8 v4, v4, 0x1

    #@79
    goto :goto_4c

    #@7a
    .line 4180
    .end local v6           #info:Landroid/view/inputmethod/InputMethodInfo;
    :cond_7a
    const-string v9, "  Clients:"

    #@7c
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@7f
    .line 4181
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService;->mClients:Ljava/util/HashMap;

    #@81
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@84
    move-result-object v9

    #@85
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@88
    move-result-object v5

    #@89
    .local v5, i$:Ljava/util/Iterator;
    :goto_89
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@8c
    move-result v9

    #@8d
    if-eqz v9, :cond_116

    #@8f
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@92
    move-result-object v1

    #@93
    check-cast v1, Lcom/android/server/InputMethodManagerService$ClientState;

    #@95
    .line 4182
    .local v1, ci:Lcom/android/server/InputMethodManagerService$ClientState;
    new-instance v9, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v11, "  Client "

    #@9c
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    const-string v11, ":"

    #@a6
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v9

    #@aa
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v9

    #@ae
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@b1
    .line 4183
    new-instance v9, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v11, "    client="

    #@b8
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v9

    #@bc
    iget-object v11, v1, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@be
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v9

    #@c2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v9

    #@c6
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@c9
    .line 4184
    new-instance v9, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v11, "    inputContext="

    #@d0
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v9

    #@d4
    iget-object v11, v1, Lcom/android/server/InputMethodManagerService$ClientState;->inputContext:Lcom/android/internal/view/IInputContext;

    #@d6
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v9

    #@da
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v9

    #@de
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@e1
    .line 4185
    new-instance v9, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v11, "    sessionRequested="

    #@e8
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v9

    #@ec
    iget-boolean v11, v1, Lcom/android/server/InputMethodManagerService$ClientState;->sessionRequested:Z

    #@ee
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v9

    #@f2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v9

    #@f6
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@f9
    .line 4186
    new-instance v9, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v11, "    curSession="

    #@100
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v9

    #@104
    iget-object v11, v1, Lcom/android/server/InputMethodManagerService$ClientState;->curSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@106
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v9

    #@10a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v9

    #@10e
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@111
    goto/16 :goto_89

    #@113
    .line 4204
    .end local v0           #N:I
    .end local v1           #ci:Lcom/android/server/InputMethodManagerService$ClientState;
    .end local v4           #i:I
    .end local v5           #i$:Ljava/util/Iterator;
    :catchall_113
    move-exception v9

    #@114
    monitor-exit v10
    :try_end_115
    .catchall {:try_start_3b .. :try_end_115} :catchall_113

    #@115
    throw v9

    #@116
    .line 4188
    .restart local v0       #N:I
    .restart local v4       #i:I
    .restart local v5       #i$:Ljava/util/Iterator;
    :cond_116
    :try_start_116
    new-instance v9, Ljava/lang/StringBuilder;

    #@118
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    const-string v11, "  mCurMethodId="

    #@11d
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v9

    #@121
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@123
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v9

    #@127
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v9

    #@12b
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@12e
    .line 4189
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@130
    .line 4190
    .local v2, client:Lcom/android/server/InputMethodManagerService$ClientState;
    new-instance v9, Ljava/lang/StringBuilder;

    #@132
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@135
    const-string v11, "  mCurClient="

    #@137
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v9

    #@13b
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v9

    #@13f
    const-string v11, " mCurSeq="

    #@141
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v9

    #@145
    iget v11, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@147
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v9

    #@14b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v9

    #@14f
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@152
    .line 4191
    new-instance v9, Ljava/lang/StringBuilder;

    #@154
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v11, "  mCurFocusedWindow="

    #@159
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v9

    #@15d
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurFocusedWindow:Landroid/os/IBinder;

    #@15f
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v9

    #@163
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v9

    #@167
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@16a
    .line 4192
    new-instance v9, Ljava/lang/StringBuilder;

    #@16c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@16f
    const-string v11, "  mCurId="

    #@171
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v9

    #@175
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@177
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v9

    #@17b
    const-string v11, " mHaveConnect="

    #@17d
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v9

    #@181
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@183
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@186
    move-result-object v9

    #@187
    const-string v11, " mBoundToMethod="

    #@189
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v9

    #@18d
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mBoundToMethod:Z

    #@18f
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@192
    move-result-object v9

    #@193
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@196
    move-result-object v9

    #@197
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@19a
    .line 4194
    new-instance v9, Ljava/lang/StringBuilder;

    #@19c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@19f
    const-string v11, "  mCurToken="

    #@1a1
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v9

    #@1a5
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@1a7
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v9

    #@1ab
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ae
    move-result-object v9

    #@1af
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1b2
    .line 4195
    new-instance v9, Ljava/lang/StringBuilder;

    #@1b4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1b7
    const-string v11, "  mCurIntent="

    #@1b9
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v9

    #@1bd
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@1bf
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v9

    #@1c3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c6
    move-result-object v9

    #@1c7
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1ca
    .line 4196
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@1cc
    .line 4197
    .local v7, method:Lcom/android/internal/view/IInputMethod;
    new-instance v9, Ljava/lang/StringBuilder;

    #@1ce
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1d1
    const-string v11, "  mCurMethod="

    #@1d3
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v9

    #@1d7
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@1d9
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v9

    #@1dd
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e0
    move-result-object v9

    #@1e1
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1e4
    .line 4198
    new-instance v9, Ljava/lang/StringBuilder;

    #@1e6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1e9
    const-string v11, "  mEnabledSession="

    #@1eb
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v9

    #@1ef
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@1f1
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v9

    #@1f5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f8
    move-result-object v9

    #@1f9
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1fc
    .line 4199
    new-instance v9, Ljava/lang/StringBuilder;

    #@1fe
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@201
    const-string v11, "  mShowRequested="

    #@203
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v9

    #@207
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowRequested:Z

    #@209
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v9

    #@20d
    const-string v11, " mShowExplicitlyRequested="

    #@20f
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v9

    #@213
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowExplicitlyRequested:Z

    #@215
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@218
    move-result-object v9

    #@219
    const-string v11, " mShowForced="

    #@21b
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v9

    #@21f
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowForced:Z

    #@221
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@224
    move-result-object v9

    #@225
    const-string v11, " mInputShown="

    #@227
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v9

    #@22b
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@22d
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@230
    move-result-object v9

    #@231
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@234
    move-result-object v9

    #@235
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@238
    .line 4203
    new-instance v9, Ljava/lang/StringBuilder;

    #@23a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@23d
    const-string v11, "  mSystemReady="

    #@23f
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@242
    move-result-object v9

    #@243
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@245
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@248
    move-result-object v9

    #@249
    const-string v11, " mScreenOn="

    #@24b
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24e
    move-result-object v9

    #@24f
    iget-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mScreenOn:Z

    #@251
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@254
    move-result-object v9

    #@255
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@258
    move-result-object v9

    #@259
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@25c
    .line 4204
    monitor-exit v10
    :try_end_25d
    .catchall {:try_start_116 .. :try_end_25d} :catchall_113

    #@25d
    .line 4206
    const-string v9, " "

    #@25f
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@262
    .line 4207
    if-eqz v2, :cond_2b4

    #@264
    .line 4208
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@267
    .line 4210
    :try_start_267
    iget-object v9, v2, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@269
    invoke-interface {v9}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@26c
    move-result-object v9

    #@26d
    invoke-interface {v9, p1, p3}, Landroid/os/IBinder;->dump(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    :try_end_270
    .catch Landroid/os/RemoteException; {:try_start_267 .. :try_end_270} :catch_29c

    #@270
    .line 4218
    :goto_270
    const-string v9, " "

    #@272
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@275
    .line 4219
    if-eqz v7, :cond_2ba

    #@277
    .line 4220
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    #@27a
    .line 4222
    :try_start_27a
    invoke-interface {v7}, Lcom/android/internal/view/IInputMethod;->asBinder()Landroid/os/IBinder;

    #@27d
    move-result-object v9

    #@27e
    invoke-interface {v9, p1, p3}, Landroid/os/IBinder;->dump(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    :try_end_281
    .catch Landroid/os/RemoteException; {:try_start_27a .. :try_end_281} :catch_283

    #@281
    goto/16 :goto_32

    #@283
    .line 4223
    :catch_283
    move-exception v3

    #@284
    .line 4224
    .local v3, e:Landroid/os/RemoteException;
    new-instance v9, Ljava/lang/StringBuilder;

    #@286
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@289
    const-string v10, "Input method service dead: "

    #@28b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28e
    move-result-object v9

    #@28f
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@292
    move-result-object v9

    #@293
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@296
    move-result-object v9

    #@297
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@29a
    goto/16 :goto_32

    #@29c
    .line 4211
    .end local v3           #e:Landroid/os/RemoteException;
    :catch_29c
    move-exception v3

    #@29d
    .line 4212
    .restart local v3       #e:Landroid/os/RemoteException;
    new-instance v9, Ljava/lang/StringBuilder;

    #@29f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2a2
    const-string v10, "Input method client dead: "

    #@2a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v9

    #@2a8
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2ab
    move-result-object v9

    #@2ac
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2af
    move-result-object v9

    #@2b0
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2b3
    goto :goto_270

    #@2b4
    .line 4215
    .end local v3           #e:Landroid/os/RemoteException;
    :cond_2b4
    const-string v9, "No input method client."

    #@2b6
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2b9
    goto :goto_270

    #@2ba
    .line 4227
    :cond_2ba
    const-string v9, "No input method service."

    #@2bc
    invoke-interface {v8, v9}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2bf
    goto/16 :goto_32
.end method

.method executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V
    .registers 4
    .parameter "target"
    .parameter "msg"

    #@0
    .prologue
    .line 1054
    invoke-interface {p1}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    instance-of v0, v0, Landroid/os/Binder;

    #@6
    if-eqz v0, :cond_e

    #@8
    .line 1055
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    invoke-virtual {v0, p2}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 1060
    :goto_d
    return-void

    #@e
    .line 1057
    :cond_e
    invoke-virtual {p0, p2}, Lcom/android/server/InputMethodManagerService;->handleMessage(Landroid/os/Message;)Z

    #@11
    .line 1058
    invoke-virtual {p2}, Landroid/os/Message;->recycle()V

    #@14
    goto :goto_d
.end method

.method public finishInput(Lcom/android/internal/view/IInputMethodClient;)V
    .registers 2
    .parameter "client"

    #@0
    .prologue
    .line 1295
    return-void
.end method

.method public getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .registers 3

    #@0
    .prologue
    .line 3268
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 3269
    const/4 v0, 0x0

    #@7
    .line 3272
    :goto_7
    return-object v0

    #@8
    .line 3271
    :cond_8
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@a
    monitor-enter v1

    #@b
    .line 3272
    :try_start_b
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getCurrentInputMethodSubtypeLocked()Landroid/view/inputmethod/InputMethodSubtype;

    #@e
    move-result-object v0

    #@f
    monitor-exit v1

    #@10
    goto :goto_7

    #@11
    .line 3273
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_b .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public getEnabledInputMethodList()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 987
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_b

    #@6
    .line 988
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 991
    :goto_a
    return-object v0

    #@b
    .line 990
    :cond_b
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@d
    monitor-enter v1

    #@e
    .line 991
    :try_start_e
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@10
    invoke-virtual {v0}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/List;

    #@13
    move-result-object v0

    #@14
    monitor-exit v1

    #@15
    goto :goto_a

    #@16
    .line 992
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;
    .registers 5
    .parameter "imi"
    .parameter "allowsImplicitlySelectedSubtypes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1023
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_b

    #@6
    .line 1024
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 1027
    :goto_a
    return-object v0

    #@b
    .line 1026
    :cond_b
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@d
    monitor-enter v1

    #@e
    .line 1027
    :try_start_e
    invoke-virtual {p0, p1, p2}, Lcom/android/server/InputMethodManagerService;->getEnabledInputMethodSubtypeListLocked(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@11
    move-result-object v0

    #@12
    monitor-exit v1

    #@13
    goto :goto_a

    #@14
    .line 1028
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_e .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public getEnabledInputMethodSubtypeListLocked(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;
    .registers 6
    .parameter "imi"
    .parameter "allowsImplicitlySelectedSubtypes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1008
    if-nez p1, :cond_10

    #@2
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_10

    #@6
    .line 1009
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@8
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@a
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object p1

    #@e
    .end local p1
    check-cast p1, Landroid/view/inputmethod/InputMethodInfo;

    #@10
    .line 1011
    .restart local p1
    :cond_10
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@12
    invoke-virtual {v1, p1}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getEnabledInputMethodSubtypeListLocked(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/List;

    #@15
    move-result-object v0

    #@16
    .line 1013
    .local v0, enabledSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    if-eqz p2, :cond_24

    #@18
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_24

    #@1e
    .line 1014
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@20
    invoke-static {v1, p1}, Lcom/android/server/InputMethodManagerService;->getImplicitlyApplicableSubtypesLocked(Landroid/content/res/Resources;Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@23
    move-result-object v0

    #@24
    .line 1016
    :cond_24
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@26
    const/4 v2, 0x0

    #@27
    invoke-static {v1, v2, p1, v0}, Landroid/view/inputmethod/InputMethodSubtype;->sort(Landroid/content/Context;ILandroid/view/inputmethod/InputMethodInfo;Ljava/util/List;)Ljava/util/List;

    #@2a
    move-result-object v1

    #@2b
    return-object v1
.end method

.method public getInputMethodList()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 976
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_b

    #@6
    .line 977
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 980
    :goto_a
    return-object v0

    #@b
    .line 979
    :cond_b
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@d
    monitor-enter v1

    #@e
    .line 980
    :try_start_e
    new-instance v0, Ljava/util/ArrayList;

    #@10
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@12
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@15
    monitor-exit v1

    #@16
    goto :goto_a

    #@17
    .line 981
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_e .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public getLastInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .registers 10

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2184
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@4
    move-result v5

    #@5
    if-nez v5, :cond_9

    #@7
    move-object v5, v6

    #@8
    .line 2202
    :goto_8
    return-object v5

    #@9
    .line 2187
    :cond_9
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@b
    monitor-enter v7

    #@c
    .line 2188
    :try_start_c
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@e
    invoke-virtual {v5}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getLastInputMethodAndSubtypeLocked()Landroid/util/Pair;

    #@11
    move-result-object v1

    #@12
    .line 2190
    .local v1, lastIme:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_28

    #@14
    iget-object v5, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@16
    check-cast v5, Ljava/lang/CharSequence;

    #@18
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b
    move-result v5

    #@1c
    if-nez v5, :cond_28

    #@1e
    iget-object v5, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@20
    check-cast v5, Ljava/lang/CharSequence;

    #@22
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@25
    move-result v5

    #@26
    if-eqz v5, :cond_2b

    #@28
    .line 2191
    :cond_28
    monitor-exit v7

    #@29
    move-object v5, v6

    #@2a
    goto :goto_8

    #@2b
    .line 2192
    :cond_2b
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2d
    iget-object v8, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@2f
    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v2

    #@33
    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    #@35
    .line 2193
    .local v2, lastImi:Landroid/view/inputmethod/InputMethodInfo;
    if-nez v2, :cond_3a

    #@37
    monitor-exit v7
    :try_end_38
    .catchall {:try_start_c .. :try_end_38} :catchall_5b

    #@38
    move-object v5, v6

    #@39
    goto :goto_8

    #@3a
    .line 2195
    :cond_3a
    :try_start_3a
    iget-object v5, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@3c
    check-cast v5, Ljava/lang/String;

    #@3e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@45
    move-result v3

    #@46
    .line 2196
    .local v3, lastSubtypeHash:I
    invoke-static {v2, v3}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@49
    move-result v4

    #@4a
    .line 2197
    .local v4, lastSubtypeId:I
    if-ltz v4, :cond_52

    #@4c
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I
    :try_end_4f
    .catchall {:try_start_3a .. :try_end_4f} :catchall_5b
    .catch Ljava/lang/NumberFormatException; {:try_start_3a .. :try_end_4f} :catch_5e

    #@4f
    move-result v5

    #@50
    if-lt v4, v5, :cond_55

    #@52
    .line 2198
    :cond_52
    :try_start_52
    monitor-exit v7
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_5b

    #@53
    move-object v5, v6

    #@54
    goto :goto_8

    #@55
    .line 2200
    :cond_55
    :try_start_55
    invoke-virtual {v2, v4}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;
    :try_end_58
    .catchall {:try_start_55 .. :try_end_58} :catchall_5b
    .catch Ljava/lang/NumberFormatException; {:try_start_55 .. :try_end_58} :catch_5e

    #@58
    move-result-object v5

    #@59
    :try_start_59
    monitor-exit v7

    #@5a
    goto :goto_8

    #@5b
    .line 2204
    .end local v1           #lastIme:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2           #lastImi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v3           #lastSubtypeHash:I
    .end local v4           #lastSubtypeId:I
    :catchall_5b
    move-exception v5

    #@5c
    monitor-exit v7
    :try_end_5d
    .catchall {:try_start_59 .. :try_end_5d} :catchall_5b

    #@5d
    throw v5

    #@5e
    .line 2201
    .restart local v1       #lastIme:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v2       #lastImi:Landroid/view/inputmethod/InputMethodInfo;
    :catch_5e
    move-exception v0

    #@5f
    .line 2202
    .local v0, e:Ljava/lang/NumberFormatException;
    :try_start_5f
    monitor-exit v7
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5b

    #@60
    move-object v5, v6

    #@61
    goto :goto_8
.end method

.method public getShortcutInputMethodsAndSubtypes()Ljava/util/List;
    .registers 9

    #@0
    .prologue
    .line 3331
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v7

    #@3
    .line 3332
    :try_start_3
    new-instance v4, Ljava/util/ArrayList;

    #@5
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@8
    .line 3333
    .local v4, ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@a
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@d
    move-result v6

    #@e
    if-nez v6, :cond_24

    #@10
    .line 3337
    const-string v6, "voice"

    #@12
    invoke-direct {p0, v6}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableShortcutInputMethodAndSubtypeLocked(Ljava/lang/String;)Landroid/util/Pair;

    #@15
    move-result-object v3

    #@16
    .line 3340
    .local v3, info:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/view/inputmethod/InputMethodInfo;Landroid/view/inputmethod/InputMethodSubtype;>;"
    if-eqz v3, :cond_22

    #@18
    .line 3341
    iget-object v6, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@1a
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 3342
    iget-object v6, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@1f
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22
    .line 3344
    :cond_22
    monitor-exit v7

    #@23
    .line 3352
    .end local v3           #info:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/view/inputmethod/InputMethodInfo;Landroid/view/inputmethod/InputMethodSubtype;>;"
    :goto_23
    return-object v4

    #@24
    .line 3346
    :cond_24
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@26
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@29
    move-result-object v6

    #@2a
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2d
    move-result-object v0

    #@2e
    :cond_2e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@31
    move-result v6

    #@32
    if-eqz v6, :cond_5c

    #@34
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@37
    move-result-object v2

    #@38
    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    #@3a
    .line 3347
    .local v2, imi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d
    .line 3348
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@3f
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    move-result-object v6

    #@43
    check-cast v6, Ljava/util/ArrayList;

    #@45
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v1

    #@49
    .local v1, i$:Ljava/util/Iterator;
    :goto_49
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v6

    #@4d
    if-eqz v6, :cond_2e

    #@4f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v5

    #@53
    check-cast v5, Landroid/view/inputmethod/InputMethodSubtype;

    #@55
    .line 3349
    .local v5, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@58
    goto :goto_49

    #@59
    .line 3353
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v4           #ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v5           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :catchall_59
    move-exception v6

    #@5a
    monitor-exit v7
    :try_end_5b
    .catchall {:try_start_3 .. :try_end_5b} :catchall_59

    #@5b
    throw v6

    #@5c
    .line 3352
    .restart local v4       #ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_5c
    :try_start_5c
    monitor-exit v7
    :try_end_5d
    .catchall {:try_start_5c .. :try_end_5d} :catchall_59

    #@5d
    goto :goto_23
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 2332
    iget v3, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v3, :sswitch_data_1b4

    #@7
    move v5, v4

    #@8
    .line 2475
    :goto_8
    return v5

    #@9
    .line 2334
    :sswitch_9
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->showInputMethodMenu()V

    #@c
    goto :goto_8

    #@d
    .line 2338
    :sswitch_d
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->showInputMethodSubtypeMenu()V

    #@10
    goto :goto_8

    #@11
    .line 2342
    :sswitch_11
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@15
    .line 2343
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@17
    check-cast v3, Ljava/lang/String;

    #@19
    invoke-direct {p0, v3}, Lcom/android/server/InputMethodManagerService;->showInputMethodAndSubtypeEnabler(Ljava/lang/String;)V

    #@1c
    .line 2344
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@1f
    goto :goto_8

    #@20
    .line 2348
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_20
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->showConfigureInputMethods()V

    #@23
    goto :goto_8

    #@24
    .line 2355
    :sswitch_24
    :try_start_24
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26
    check-cast v3, Lcom/android/internal/view/IInputMethod;

    #@28
    invoke-interface {v3}, Lcom/android/internal/view/IInputMethod;->unbindInput()V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_8

    #@2c
    .line 2356
    :catch_2c
    move-exception v3

    #@2d
    goto :goto_8

    #@2e
    .line 2361
    :sswitch_2e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@30
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@32
    .line 2363
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_32
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@34
    check-cast v3, Lcom/android/internal/view/IInputMethod;

    #@36
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@38
    check-cast v4, Landroid/view/inputmethod/InputBinding;

    #@3a
    invoke-interface {v3, v4}, Lcom/android/internal/view/IInputMethod;->bindInput(Landroid/view/inputmethod/InputBinding;)V
    :try_end_3d
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_3d} :catch_1b0

    #@3d
    .line 2366
    :goto_3d
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@40
    goto :goto_8

    #@41
    .line 2369
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_41
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@43
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@45
    .line 2371
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_45
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@47
    check-cast v3, Lcom/android/internal/view/IInputMethod;

    #@49
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@4b
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@4d
    check-cast v4, Landroid/os/ResultReceiver;

    #@4f
    invoke-interface {v3, v6, v4}, Lcom/android/internal/view/IInputMethod;->showSoftInput(ILandroid/os/ResultReceiver;)V
    :try_end_52
    .catch Landroid/os/RemoteException; {:try_start_45 .. :try_end_52} :catch_1ad

    #@52
    .line 2375
    :goto_52
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@55
    goto :goto_8

    #@56
    .line 2378
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_56
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@58
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@5a
    .line 2380
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_5a
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@5c
    check-cast v3, Lcom/android/internal/view/IInputMethod;

    #@5e
    const/4 v6, 0x0

    #@5f
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@61
    check-cast v4, Landroid/os/ResultReceiver;

    #@63
    invoke-interface {v3, v6, v4}, Lcom/android/internal/view/IInputMethod;->hideSoftInput(ILandroid/os/ResultReceiver;)V
    :try_end_66
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_66} :catch_1aa

    #@66
    .line 2384
    :goto_66
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@69
    goto :goto_8

    #@6a
    .line 2387
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_6a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6c
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@6e
    .line 2390
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_6e
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@70
    check-cast v3, Lcom/android/internal/view/IInputMethod;

    #@72
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@74
    check-cast v4, Landroid/os/IBinder;

    #@76
    invoke-interface {v3, v4}, Lcom/android/internal/view/IInputMethod;->attachToken(Landroid/os/IBinder;)V
    :try_end_79
    .catch Landroid/os/RemoteException; {:try_start_6e .. :try_end_79} :catch_1a7

    #@79
    .line 2393
    :goto_79
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@7c
    goto :goto_8

    #@7d
    .line 2396
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_7d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7f
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@81
    .line 2398
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_81
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@83
    check-cast v3, Lcom/android/internal/view/IInputMethod;

    #@85
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@87
    check-cast v4, Lcom/android/internal/view/IInputMethodCallback;

    #@89
    invoke-interface {v3, v4}, Lcom/android/internal/view/IInputMethod;->createSession(Lcom/android/internal/view/IInputMethodCallback;)V
    :try_end_8c
    .catch Landroid/os/RemoteException; {:try_start_81 .. :try_end_8c} :catch_1a4

    #@8c
    .line 2402
    :goto_8c
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@8f
    goto/16 :goto_8

    #@91
    .line 2407
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_91
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@93
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@95
    .line 2409
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_95
    iget-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@97
    check-cast v2, Lcom/android/server/InputMethodManagerService$SessionState;

    #@99
    .line 2410
    .local v2, session:Lcom/android/server/InputMethodManagerService$SessionState;
    invoke-virtual {p0, v2}, Lcom/android/server/InputMethodManagerService;->setEnabledSessionInMainThread(Lcom/android/server/InputMethodManagerService$SessionState;)V

    #@9c
    .line 2411
    iget-object v6, v2, Lcom/android/server/InputMethodManagerService$SessionState;->method:Lcom/android/internal/view/IInputMethod;

    #@9e
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@a0
    check-cast v3, Lcom/android/internal/view/IInputContext;

    #@a2
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@a4
    check-cast v4, Landroid/view/inputmethod/EditorInfo;

    #@a6
    invoke-interface {v6, v3, v4}, Lcom/android/internal/view/IInputMethod;->startInput(Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;)V
    :try_end_a9
    .catch Landroid/os/RemoteException; {:try_start_95 .. :try_end_a9} :catch_1a1

    #@a9
    .line 2415
    .end local v2           #session:Lcom/android/server/InputMethodManagerService$SessionState;
    :goto_a9
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@ac
    goto/16 :goto_8

    #@ae
    .line 2418
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_ae
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b0
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@b2
    .line 2420
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_b2
    iget-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@b4
    check-cast v2, Lcom/android/server/InputMethodManagerService$SessionState;

    #@b6
    .line 2421
    .restart local v2       #session:Lcom/android/server/InputMethodManagerService$SessionState;
    invoke-virtual {p0, v2}, Lcom/android/server/InputMethodManagerService;->setEnabledSessionInMainThread(Lcom/android/server/InputMethodManagerService$SessionState;)V

    #@b9
    .line 2422
    iget-object v6, v2, Lcom/android/server/InputMethodManagerService$SessionState;->method:Lcom/android/internal/view/IInputMethod;

    #@bb
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@bd
    check-cast v3, Lcom/android/internal/view/IInputContext;

    #@bf
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@c1
    check-cast v4, Landroid/view/inputmethod/EditorInfo;

    #@c3
    invoke-interface {v6, v3, v4}, Lcom/android/internal/view/IInputMethod;->restartInput(Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;)V
    :try_end_c6
    .catch Landroid/os/RemoteException; {:try_start_b2 .. :try_end_c6} :catch_19e

    #@c6
    .line 2426
    .end local v2           #session:Lcom/android/server/InputMethodManagerService$SessionState;
    :goto_c6
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@c9
    goto/16 :goto_8

    #@cb
    .line 2433
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_cb
    :try_start_cb
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@cd
    check-cast v3, Lcom/android/internal/view/IInputMethodClient;

    #@cf
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@d1
    invoke-interface {v3, v4}, Lcom/android/internal/view/IInputMethodClient;->onUnbindMethod(I)V
    :try_end_d4
    .catch Landroid/os/RemoteException; {:try_start_cb .. :try_end_d4} :catch_d6

    #@d4
    goto/16 :goto_8

    #@d6
    .line 2434
    :catch_d6
    move-exception v3

    #@d7
    goto/16 :goto_8

    #@d9
    .line 2439
    :sswitch_d9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@db
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@dd
    .line 2441
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :try_start_dd
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@df
    check-cast v3, Lcom/android/internal/view/IInputMethodClient;

    #@e1
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@e3
    check-cast v4, Lcom/android/internal/view/InputBindResult;

    #@e5
    invoke-interface {v3, v4}, Lcom/android/internal/view/IInputMethodClient;->onBindMethod(Lcom/android/internal/view/InputBindResult;)V
    :try_end_e8
    .catch Landroid/os/RemoteException; {:try_start_dd .. :try_end_e8} :catch_ed

    #@e8
    .line 2446
    :goto_e8
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@eb
    goto/16 :goto_8

    #@ed
    .line 2443
    :catch_ed
    move-exception v1

    #@ee
    .line 2444
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "InputMethodManagerService"

    #@f0
    new-instance v4, Ljava/lang/StringBuilder;

    #@f2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f5
    const-string v6, "Client died receiving input method "

    #@f7
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v4

    #@fb
    iget-object v6, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@fd
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v4

    #@101
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v4

    #@105
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    goto :goto_e8

    #@109
    .line 2450
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    .end local v1           #e:Landroid/os/RemoteException;
    :sswitch_109
    :try_start_109
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10b
    check-cast v3, Lcom/android/server/InputMethodManagerService$ClientState;

    #@10d
    iget-object v3, v3, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@10f
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@111
    if-eqz v6, :cond_114

    #@113
    move v4, v5

    #@114
    :cond_114
    invoke-interface {v3, v4}, Lcom/android/internal/view/IInputMethodClient;->setActive(Z)V
    :try_end_117
    .catch Landroid/os/RemoteException; {:try_start_109 .. :try_end_117} :catch_119

    #@117
    goto/16 :goto_8

    #@119
    .line 2451
    :catch_119
    move-exception v1

    #@11a
    .line 2452
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v4, "InputMethodManagerService"

    #@11c
    new-instance v3, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v6, "Got RemoteException sending setActive(false) notification to pid "

    #@123
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v6

    #@127
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@129
    check-cast v3, Lcom/android/server/InputMethodManagerService$ClientState;

    #@12b
    iget v3, v3, Lcom/android/server/InputMethodManagerService$ClientState;->pid:I

    #@12d
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@130
    move-result-object v3

    #@131
    const-string v6, " uid "

    #@133
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v6

    #@137
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@139
    check-cast v3, Lcom/android/server/InputMethodManagerService$ClientState;

    #@13b
    iget v3, v3, Lcom/android/server/InputMethodManagerService$ClientState;->uid:I

    #@13d
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@140
    move-result-object v3

    #@141
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v3

    #@145
    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@148
    goto/16 :goto_8

    #@14a
    .line 2461
    .end local v1           #e:Landroid/os/RemoteException;
    :sswitch_14a
    :try_start_14a
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14c
    check-cast v3, Lcom/android/server/InputMethodManagerService$ClientState;

    #@14e
    iget-object v3, v3, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@150
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@152
    if-eqz v6, :cond_155

    #@154
    move v4, v5

    #@155
    :cond_155
    invoke-interface {v3, v4}, Lcom/android/internal/view/IInputMethodClient;->setScreenOnOff(Z)V
    :try_end_158
    .catch Landroid/os/RemoteException; {:try_start_14a .. :try_end_158} :catch_15a

    #@158
    goto/16 :goto_8

    #@15a
    .line 2462
    :catch_15a
    move-exception v1

    #@15b
    .line 2463
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v4, "InputMethodManagerService"

    #@15d
    new-instance v3, Ljava/lang/StringBuilder;

    #@15f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@162
    const-string v6, "Got RemoteException sending setScreenOnOff(false) notification to pid "

    #@164
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v6

    #@168
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16a
    check-cast v3, Lcom/android/server/InputMethodManagerService$ClientState;

    #@16c
    iget v3, v3, Lcom/android/server/InputMethodManagerService$ClientState;->pid:I

    #@16e
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@171
    move-result-object v3

    #@172
    const-string v6, " uid "

    #@174
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v6

    #@178
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@17a
    check-cast v3, Lcom/android/server/InputMethodManagerService$ClientState;

    #@17c
    iget v3, v3, Lcom/android/server/InputMethodManagerService$ClientState;->uid:I

    #@17e
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@181
    move-result-object v3

    #@182
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@185
    move-result-object v3

    #@186
    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@189
    goto/16 :goto_8

    #@18b
    .line 2471
    .end local v1           #e:Landroid/os/RemoteException;
    :sswitch_18b
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mHardKeyboardListener:Lcom/android/server/InputMethodManagerService$HardKeyboardListener;

    #@18d
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@18f
    if-ne v3, v5, :cond_19c

    #@191
    move v3, v5

    #@192
    :goto_192
    iget v7, p1, Landroid/os/Message;->arg2:I

    #@194
    if-ne v7, v5, :cond_197

    #@196
    move v4, v5

    #@197
    :cond_197
    invoke-virtual {v6, v3, v4}, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->handleHardKeyboardStatusChange(ZZ)V

    #@19a
    goto/16 :goto_8

    #@19c
    :cond_19c
    move v3, v4

    #@19d
    goto :goto_192

    #@19e
    .line 2424
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    :catch_19e
    move-exception v3

    #@19f
    goto/16 :goto_c6

    #@1a1
    .line 2413
    :catch_1a1
    move-exception v3

    #@1a2
    goto/16 :goto_a9

    #@1a4
    .line 2400
    :catch_1a4
    move-exception v3

    #@1a5
    goto/16 :goto_8c

    #@1a7
    .line 2391
    :catch_1a7
    move-exception v3

    #@1a8
    goto/16 :goto_79

    #@1aa
    .line 2382
    :catch_1aa
    move-exception v3

    #@1ab
    goto/16 :goto_66

    #@1ad
    .line 2373
    :catch_1ad
    move-exception v3

    #@1ae
    goto/16 :goto_52

    #@1b0
    .line 2364
    :catch_1b0
    move-exception v3

    #@1b1
    goto/16 :goto_3d

    #@1b3
    .line 2332
    nop

    #@1b4
    :sswitch_data_1b4
    .sparse-switch
        0x1 -> :sswitch_9
        0x2 -> :sswitch_d
        0x3 -> :sswitch_11
        0x4 -> :sswitch_20
        0x3e8 -> :sswitch_24
        0x3f2 -> :sswitch_2e
        0x3fc -> :sswitch_41
        0x406 -> :sswitch_56
        0x410 -> :sswitch_6a
        0x41a -> :sswitch_7d
        0x7d0 -> :sswitch_91
        0x7da -> :sswitch_ae
        0xbb8 -> :sswitch_cb
        0xbc2 -> :sswitch_d9
        0xbcc -> :sswitch_109
        0xbd6 -> :sswitch_14a
        0xfa0 -> :sswitch_18b
    .end sparse-switch
.end method

.method hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z
    .registers 10
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1850
    and-int/lit8 v3, p1, 0x1

    #@3
    if-eqz v3, :cond_f

    #@5
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mShowExplicitlyRequested:Z

    #@7
    if-nez v3, :cond_d

    #@9
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mShowForced:Z

    #@b
    if-eqz v3, :cond_f

    #@d
    :cond_d
    move v1, v2

    #@e
    .line 1883
    :goto_e
    return v1

    #@f
    .line 1856
    :cond_f
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mShowForced:Z

    #@11
    if-eqz v3, :cond_19

    #@13
    and-int/lit8 v3, p1, 0x2

    #@15
    if-eqz v3, :cond_19

    #@17
    move v1, v2

    #@18
    .line 1859
    goto :goto_e

    #@19
    .line 1861
    :cond_19
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mIsQuickVoiceUsing:Z

    #@1b
    if-eqz v3, :cond_30

    #@1d
    .line 1862
    const-string v3, "InputMethodManagerService"

    #@1f
    const-string v4, "SoftKeypad HIDE broadcast"

    #@21
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1863
    new-instance v0, Landroid/content/Intent;

    #@26
    const-string v3, "com.lge.softkeypad.intent.HIDE"

    #@28
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2b
    .line 1864
    .local v0, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@2d
    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@30
    .line 1868
    .end local v0           #intent:Landroid/content/Intent;
    :cond_30
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@32
    if-eqz v3, :cond_62

    #@34
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@36
    if-eqz v3, :cond_62

    #@38
    .line 1869
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@3a
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@3c
    const/16 v5, 0x406

    #@3e
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@40
    invoke-virtual {v4, v5, v6, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {p0, v3, v4}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@47
    .line 1871
    const/4 v1, 0x1

    #@48
    .line 1875
    .local v1, res:Z
    :goto_48
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@4a
    if-eqz v3, :cond_59

    #@4c
    iget-boolean v3, p0, Lcom/android/server/InputMethodManagerService;->mVisibleBound:Z

    #@4e
    if-eqz v3, :cond_59

    #@50
    .line 1876
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@52
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mVisibleConnection:Landroid/content/ServiceConnection;

    #@54
    invoke-virtual {v3, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@57
    .line 1877
    iput-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mVisibleBound:Z

    #@59
    .line 1879
    :cond_59
    iput-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@5b
    .line 1880
    iput-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mShowRequested:Z

    #@5d
    .line 1881
    iput-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mShowExplicitlyRequested:Z

    #@5f
    .line 1882
    iput-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mShowForced:Z

    #@61
    goto :goto_e

    #@62
    .line 1873
    .end local v1           #res:Z
    :cond_62
    const/4 v1, 0x0

    #@63
    .restart local v1       #res:Z
    goto :goto_48
.end method

.method hideInputMethodMenu()V
    .registers 3

    #@0
    .prologue
    .line 2876
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 2877
    :try_start_3
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->hideInputMethodMenuLocked()V

    #@6
    .line 2878
    monitor-exit v1

    #@7
    .line 2879
    return-void

    #@8
    .line 2878
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method hideInputMethodMenuLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2884
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 2885
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@7
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@a
    .line 2886
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mSwitchingDialog:Landroid/app/AlertDialog;

    #@c
    .line 2889
    :cond_c
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    #@e
    .line 2890
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mIms:[Landroid/view/inputmethod/InputMethodInfo;

    #@10
    .line 2891
    return-void
.end method

.method public hideMySoftInput(Landroid/os/IBinder;I)V
    .registers 7
    .parameter "token"
    .parameter "flags"

    #@0
    .prologue
    .line 2271
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_7

    #@6
    .line 2287
    :goto_6
    return-void

    #@7
    .line 2274
    :cond_7
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v3

    #@a
    .line 2275
    if-eqz p1, :cond_10

    #@c
    :try_start_c
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@e
    if-eq v2, p1, :cond_15

    #@10
    .line 2278
    :cond_10
    monitor-exit v3

    #@11
    goto :goto_6

    #@12
    .line 2286
    :catchall_12
    move-exception v2

    #@13
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_c .. :try_end_14} :catchall_12

    #@14
    throw v2

    #@15
    .line 2280
    :cond_15
    :try_start_15
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_18
    .catchall {:try_start_15 .. :try_end_18} :catchall_12

    #@18
    move-result-wide v0

    #@19
    .line 2282
    .local v0, ident:J
    const/4 v2, 0x0

    #@1a
    :try_start_1a
    invoke-virtual {p0, p2, v2}, Lcom/android/server/InputMethodManagerService;->hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z
    :try_end_1d
    .catchall {:try_start_1a .. :try_end_1d} :catchall_22

    #@1d
    .line 2284
    :try_start_1d
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@20
    .line 2286
    monitor-exit v3

    #@21
    goto :goto_6

    #@22
    .line 2284
    :catchall_22
    move-exception v2

    #@23
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@26
    throw v2
    :try_end_27
    .catchall {:try_start_1d .. :try_end_27} :catchall_12
.end method

.method public hideSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    .registers 12
    .parameter "client"
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1816
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@4
    move-result v5

    #@5
    if-nez v5, :cond_8

    #@7
    .line 1842
    :goto_7
    return v4

    #@8
    .line 1819
    :cond_8
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@b
    move-result v3

    #@c
    .line 1820
    .local v3, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@f
    move-result-wide v1

    #@10
    .line 1822
    .local v1, ident:J
    :try_start_10
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@12
    monitor-enter v5
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_40

    #@13
    .line 1823
    :try_start_13
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@15
    if-eqz v6, :cond_27

    #@17
    if-eqz p1, :cond_27

    #@19
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@1b
    iget-object v6, v6, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@1d
    invoke-interface {v6}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@20
    move-result-object v6

    #@21
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;
    :try_end_24
    .catchall {:try_start_13 .. :try_end_24} :catchall_3d

    #@24
    move-result-object v7

    #@25
    if-eq v6, v7, :cond_45

    #@27
    .line 1829
    :cond_27
    :try_start_27
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@29
    invoke-interface {v6, p1}, Landroid/view/IWindowManager;->inputMethodClientHasFocus(Lcom/android/internal/view/IInputMethodClient;)Z

    #@2c
    move-result v6

    #@2d
    if-nez v6, :cond_45

    #@2f
    .line 1832
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->setImeWindowVisibilityStatusHiddenLocked()V
    :try_end_32
    .catchall {:try_start_27 .. :try_end_32} :catchall_3d
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_32} :catch_37

    #@32
    .line 1833
    :try_start_32
    monitor-exit v5
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_3d

    #@33
    .line 1845
    :goto_33
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@36
    goto :goto_7

    #@37
    .line 1835
    :catch_37
    move-exception v0

    #@38
    .line 1836
    .local v0, e:Landroid/os/RemoteException;
    :try_start_38
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->setImeWindowVisibilityStatusHiddenLocked()V

    #@3b
    .line 1837
    monitor-exit v5

    #@3c
    goto :goto_33

    #@3d
    .line 1843
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_3d
    move-exception v4

    #@3e
    monitor-exit v5
    :try_end_3f
    .catchall {:try_start_38 .. :try_end_3f} :catchall_3d

    #@3f
    :try_start_3f
    throw v4
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_40

    #@40
    .line 1845
    :catchall_40
    move-exception v4

    #@41
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@44
    throw v4

    #@45
    .line 1842
    :cond_45
    :try_start_45
    invoke-virtual {p0, p2, p3}, Lcom/android/server/InputMethodManagerService;->hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@48
    move-result v4

    #@49
    monitor-exit v5
    :try_end_4a
    .catchall {:try_start_45 .. :try_end_4a} :catchall_3d

    #@4a
    goto :goto_33
.end method

.method public notifySuggestionPicked(Landroid/text/style/SuggestionSpan;Ljava/lang/String;I)Z
    .registers 13
    .parameter "span"
    .parameter "originalString"
    .parameter "index"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1592
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@4
    move-result v7

    #@5
    if-nez v7, :cond_8

    #@7
    .line 1619
    :goto_7
    return v6

    #@8
    .line 1595
    :cond_8
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@a
    monitor-enter v7

    #@b
    .line 1596
    :try_start_b
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mSecureSuggestionSpans:Landroid/util/LruCache;

    #@d
    invoke-virtual {v8, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v5

    #@11
    check-cast v5, Landroid/view/inputmethod/InputMethodInfo;

    #@13
    .line 1598
    .local v5, targetImi:Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v5, :cond_63

    #@15
    .line 1599
    invoke-virtual {p1}, Landroid/text/style/SuggestionSpan;->getSuggestions()[Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    .line 1600
    .local v4, suggestions:[Ljava/lang/String;
    if-ltz p3, :cond_1e

    #@1b
    array-length v8, v4

    #@1c
    if-lt p3, v8, :cond_23

    #@1e
    :cond_1e
    monitor-exit v7

    #@1f
    goto :goto_7

    #@20
    .line 1618
    .end local v4           #suggestions:[Ljava/lang/String;
    .end local v5           #targetImi:Landroid/view/inputmethod/InputMethodInfo;
    :catchall_20
    move-exception v6

    #@21
    monitor-exit v7
    :try_end_22
    .catchall {:try_start_b .. :try_end_22} :catchall_20

    #@22
    throw v6

    #@23
    .line 1601
    .restart local v4       #suggestions:[Ljava/lang/String;
    .restart local v5       #targetImi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_23
    :try_start_23
    invoke-virtual {p1}, Landroid/text/style/SuggestionSpan;->getNotificationTargetClassName()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 1602
    .local v0, className:Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    #@29
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@2c
    .line 1605
    .local v3, intent:Landroid/content/Intent;
    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v3, v6, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@33
    .line 1606
    const-string v6, "android.text.style.SUGGESTION_PICKED"

    #@35
    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@38
    .line 1607
    const-string v6, "before"

    #@3a
    invoke-virtual {v3, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3d
    .line 1608
    const-string v6, "after"

    #@3f
    aget-object v8, v4, p3

    #@41
    invoke-virtual {v3, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@44
    .line 1609
    const-string v6, "hashcode"

    #@46
    invoke-virtual {p1}, Landroid/text/style/SuggestionSpan;->hashCode()I

    #@49
    move-result v8

    #@4a
    invoke-virtual {v3, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4d
    .line 1610
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_50
    .catchall {:try_start_23 .. :try_end_50} :catchall_20

    #@50
    move-result-wide v1

    #@51
    .line 1612
    .local v1, ident:J
    :try_start_51
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@53
    sget-object v8, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@55
    invoke-virtual {v6, v3, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_58
    .catchall {:try_start_51 .. :try_end_58} :catchall_5e

    #@58
    .line 1614
    :try_start_58
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5b
    .line 1616
    const/4 v6, 0x1

    #@5c
    monitor-exit v7

    #@5d
    goto :goto_7

    #@5e
    .line 1614
    :catchall_5e
    move-exception v6

    #@5f
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@62
    throw v6

    #@63
    .line 1618
    .end local v0           #className:Ljava/lang/String;
    .end local v1           #ident:J
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #suggestions:[Ljava/lang/String;
    :cond_63
    monitor-exit v7
    :try_end_64
    .catchall {:try_start_58 .. :try_end_64} :catchall_20

    #@64
    goto :goto_7
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 10
    .parameter "name"
    .parameter "service"

    #@0
    .prologue
    .line 1299
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 1300
    :try_start_3
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@5
    if-eqz v0, :cond_56

    #@7
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@9
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p1, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_56

    #@13
    .line 1301
    invoke-static {p2}, Lcom/android/internal/view/IInputMethod$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethod;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@19
    .line 1302
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@1b
    if-nez v0, :cond_2b

    #@1d
    .line 1303
    const-string v0, "InputMethodManagerService"

    #@1f
    const-string v2, "Service connected without a token!"

    #@21
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1304
    const/4 v0, 0x0

    #@25
    const/4 v2, 0x0

    #@26
    invoke-virtual {p0, v0, v2}, Lcom/android/server/InputMethodManagerService;->unbindCurrentMethodLocked(ZZ)V

    #@29
    .line 1305
    monitor-exit v1

    #@2a
    .line 1319
    :goto_2a
    return-void

    #@2b
    .line 1308
    :cond_2b
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@2d
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2f
    const/16 v3, 0x410

    #@31
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@33
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@35
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {p0, v0, v2}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@3c
    .line 1310
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@3e
    if-eqz v0, :cond_56

    #@40
    .line 1313
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@42
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@44
    const/16 v3, 0x41a

    #@46
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@48
    new-instance v5, Lcom/android/server/InputMethodManagerService$MethodCallback;

    #@4a
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@4c
    invoke-direct {v5, v6, p0}, Lcom/android/server/InputMethodManagerService$MethodCallback;-><init>(Lcom/android/internal/view/IInputMethod;Lcom/android/server/InputMethodManagerService;)V

    #@4f
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {p0, v0, v2}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@56
    .line 1318
    :cond_56
    monitor-exit v1

    #@57
    goto :goto_2a

    #@58
    :catchall_58
    move-exception v0

    #@59
    monitor-exit v1
    :try_end_5a
    .catchall {:try_start_3 .. :try_end_5a} :catchall_58

    #@5a
    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 8
    .parameter "name"

    #@0
    .prologue
    .line 1402
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 1405
    :try_start_3
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@5
    if-eqz v0, :cond_40

    #@7
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@9
    if-eqz v0, :cond_40

    #@b
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@d
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {p1, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_40

    #@17
    .line 1407
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->clearCurMethodLocked()V

    #@1a
    .line 1410
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1d
    move-result-wide v2

    #@1e
    iput-wide v2, p0, Lcom/android/server/InputMethodManagerService;->mLastBindTime:J

    #@20
    .line 1411
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@22
    iput-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mShowRequested:Z

    #@24
    .line 1412
    const/4 v0, 0x0

    #@25
    iput-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@27
    .line 1413
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@29
    if-eqz v0, :cond_40

    #@2b
    .line 1414
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@2d
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@2f
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@31
    const/16 v3, 0xbb8

    #@33
    iget v4, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@35
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@37
    iget-object v5, v5, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@39
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {p0, v0, v2}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@40
    .line 1418
    :cond_40
    monitor-exit v1

    #@41
    .line 1419
    return-void

    #@42
    .line 1418
    :catchall_42
    move-exception v0

    #@43
    monitor-exit v1
    :try_end_44
    .catchall {:try_start_3 .. :try_end_44} :catchall_42

    #@44
    throw v0
.end method

.method onSessionCreated(Lcom/android/internal/view/IInputMethod;Lcom/android/internal/view/IInputMethodSession;)V
    .registers 9
    .parameter "method"
    .parameter "session"

    #@0
    .prologue
    .line 1322
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 1323
    :try_start_3
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@5
    if-eqz v1, :cond_45

    #@7
    if-eqz p1, :cond_45

    #@9
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@b
    invoke-interface {v1}, Lcom/android/internal/view/IInputMethod;->asBinder()Landroid/os/IBinder;

    #@e
    move-result-object v1

    #@f
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethod;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    if-ne v1, v3, :cond_45

    #@15
    .line 1325
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@17
    if-eqz v1, :cond_45

    #@19
    .line 1326
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@1b
    new-instance v3, Lcom/android/server/InputMethodManagerService$SessionState;

    #@1d
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@1f
    invoke-direct {v3, p0, v4, p1, p2}, Lcom/android/server/InputMethodManagerService$SessionState;-><init>(Lcom/android/server/InputMethodManagerService;Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputMethod;Lcom/android/internal/view/IInputMethodSession;)V

    #@22
    iput-object v3, v1, Lcom/android/server/InputMethodManagerService$ClientState;->curSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@24
    .line 1328
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@26
    const/4 v3, 0x0

    #@27
    iput-boolean v3, v1, Lcom/android/server/InputMethodManagerService$ClientState;->sessionRequested:Z

    #@29
    .line 1329
    const/4 v1, 0x1

    #@2a
    invoke-virtual {p0, v1}, Lcom/android/server/InputMethodManagerService;->attachNewInputLocked(Z)Lcom/android/internal/view/InputBindResult;

    #@2d
    move-result-object v0

    #@2e
    .line 1330
    .local v0, res:Lcom/android/internal/view/InputBindResult;
    iget-object v1, v0, Lcom/android/internal/view/InputBindResult;->method:Lcom/android/internal/view/IInputMethodSession;

    #@30
    if-eqz v1, :cond_45

    #@32
    .line 1331
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@34
    iget-object v1, v1, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@36
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@38
    const/16 v4, 0xbc2

    #@3a
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@3c
    iget-object v5, v5, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@3e
    invoke-virtual {v3, v4, v5, v0}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {p0, v1, v3}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@45
    .line 1336
    .end local v0           #res:Lcom/android/internal/view/InputBindResult;
    :cond_45
    monitor-exit v2

    #@46
    .line 1337
    return-void

    #@47
    .line 1336
    :catchall_47
    move-exception v1

    #@48
    monitor-exit v2
    :try_end_49
    .catchall {:try_start_3 .. :try_end_49} :catchall_47

    #@49
    throw v1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 857
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/internal/view/IInputMethodManager$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result v1

    #@4
    return v1

    #@5
    .line 858
    :catch_5
    move-exception v0

    #@6
    .line 861
    .local v0, e:Ljava/lang/RuntimeException;
    instance-of v1, v0, Ljava/lang/SecurityException;

    #@8
    if-nez v1, :cond_11

    #@a
    .line 862
    const-string v1, "InputMethodManagerService"

    #@c
    const-string v2, "Input Method Manager Crash"

    #@e
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 864
    :cond_11
    throw v0
.end method

.method public registerSuggestionSpansForNotification([Landroid/text/style/SuggestionSpan;)V
    .registers 8
    .parameter "spans"

    #@0
    .prologue
    .line 1575
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_7

    #@6
    .line 1588
    :goto_6
    return-void

    #@7
    .line 1578
    :cond_7
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v4

    #@a
    .line 1579
    :try_start_a
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@c
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@e
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    #@14
    .line 1580
    .local v0, currentImi:Landroid/view/inputmethod/InputMethodInfo;
    const/4 v1, 0x0

    #@15
    .local v1, i:I
    :goto_15
    array-length v3, p1

    #@16
    if-ge v1, v3, :cond_34

    #@18
    .line 1581
    aget-object v2, p1, v1

    #@1a
    .line 1582
    .local v2, ss:Landroid/text/style/SuggestionSpan;
    invoke-virtual {v2}, Landroid/text/style/SuggestionSpan;->getNotificationTargetClassName()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_31

    #@24
    .line 1583
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mSecureSuggestionSpans:Landroid/util/LruCache;

    #@26
    invoke-virtual {v3, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    .line 1584
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mSecureSuggestionSpans:Landroid/util/LruCache;

    #@2b
    invoke-virtual {v3, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    #@31
    .line 1580
    :cond_31
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_15

    #@34
    .line 1587
    .end local v2           #ss:Landroid/text/style/SuggestionSpan;
    :cond_34
    monitor-exit v4

    #@35
    goto :goto_6

    #@36
    .end local v0           #currentImi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v1           #i:I
    :catchall_36
    move-exception v3

    #@37
    monitor-exit v4
    :try_end_38
    .catchall {:try_start_a .. :try_end_38} :catchall_36

    #@38
    throw v3
.end method

.method public removeClient(Lcom/android/internal/view/IInputMethodClient;)V
    .registers 5
    .parameter "client"

    #@0
    .prologue
    .line 1045
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1051
    :goto_6
    return-void

    #@7
    .line 1048
    :cond_7
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v1

    #@a
    .line 1049
    :try_start_a
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mClients:Ljava/util/HashMap;

    #@c
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    .line 1050
    monitor-exit v1

    #@14
    goto :goto_6

    #@15
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_a .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 13
    .parameter "imiId"
    .parameter "subtypes"

    #@0
    .prologue
    .line 2209
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v7

    #@4
    if-nez v7, :cond_7

    #@6
    .line 2241
    :cond_6
    :goto_6
    return-void

    #@7
    .line 2214
    :cond_7
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v7

    #@b
    if-nez v7, :cond_6

    #@d
    if-eqz p2, :cond_6

    #@f
    array-length v7, p2

    #@10
    if-eqz v7, :cond_6

    #@12
    .line 2215
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@14
    monitor-enter v8

    #@15
    .line 2216
    :try_start_15
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@17
    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    move-result-object v4

    #@1b
    check-cast v4, Landroid/view/inputmethod/InputMethodInfo;

    #@1d
    .line 2217
    .local v4, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-nez v4, :cond_24

    #@1f
    monitor-exit v8

    #@20
    goto :goto_6

    #@21
    .line 2240
    .end local v4           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :catchall_21
    move-exception v7

    #@22
    monitor-exit v8
    :try_end_23
    .catchall {:try_start_15 .. :try_end_23} :catchall_21

    #@23
    throw v7

    #@24
    .line 2220
    .restart local v4       #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_24
    :try_start_24
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mIPackageManager:Landroid/content/pm/IPackageManager;

    #@26
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@29
    move-result v9

    #@2a
    invoke-interface {v7, v9}, Landroid/content/pm/IPackageManager;->getPackagesForUid(I)[Ljava/lang/String;
    :try_end_2d
    .catchall {:try_start_24 .. :try_end_2d} :catchall_21
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2d} :catch_55

    #@2d
    move-result-object v5

    #@2e
    .line 2225
    .local v5, packageInfos:[Ljava/lang/String;
    if-eqz v5, :cond_67

    #@30
    .line 2226
    :try_start_30
    array-length v6, v5

    #@31
    .line 2227
    .local v6, packageNum:I
    const/4 v1, 0x0

    #@32
    .local v1, i:I
    :goto_32
    if-ge v1, v6, :cond_67

    #@34
    .line 2228
    aget-object v7, v5, v1

    #@36
    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_64

    #@40
    .line 2229
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mFileManager:Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@42
    invoke-virtual {v7, v4, p2}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->addInputMethodSubtypes(Landroid/view/inputmethod/InputMethodInfo;[Landroid/view/inputmethod/InputMethodSubtype;)V

    #@45
    .line 2230
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_48
    .catchall {:try_start_30 .. :try_end_48} :catchall_21

    #@48
    move-result-wide v2

    #@49
    .line 2232
    .local v2, ident:J
    :try_start_49
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@4b
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@4d
    invoke-virtual {p0, v7, v9}, Lcom/android/server/InputMethodManagerService;->buildInputMethodListLocked(Ljava/util/ArrayList;Ljava/util/HashMap;)V
    :try_end_50
    .catchall {:try_start_49 .. :try_end_50} :catchall_5f

    #@50
    .line 2234
    :try_start_50
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@53
    .line 2236
    monitor-exit v8

    #@54
    goto :goto_6

    #@55
    .line 2221
    .end local v1           #i:I
    .end local v2           #ident:J
    .end local v5           #packageInfos:[Ljava/lang/String;
    .end local v6           #packageNum:I
    :catch_55
    move-exception v0

    #@56
    .line 2222
    .local v0, e:Landroid/os/RemoteException;
    const-string v7, "InputMethodManagerService"

    #@58
    const-string v9, "Failed to get package infos"

    #@5a
    invoke-static {v7, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 2223
    monitor-exit v8

    #@5e
    goto :goto_6

    #@5f
    .line 2234
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v1       #i:I
    .restart local v2       #ident:J
    .restart local v5       #packageInfos:[Ljava/lang/String;
    .restart local v6       #packageNum:I
    :catchall_5f
    move-exception v7

    #@60
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@63
    throw v7

    #@64
    .line 2227
    .end local v2           #ident:J
    :cond_64
    add-int/lit8 v1, v1, 0x1

    #@66
    goto :goto_32

    #@67
    .line 2240
    .end local v1           #i:I
    .end local v6           #packageNum:I
    :cond_67
    monitor-exit v8
    :try_end_68
    .catchall {:try_start_50 .. :try_end_68} :catchall_21

    #@68
    goto :goto_6
.end method

.method public setCurrentInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)Z
    .registers 8
    .parameter "subtype"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3359
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_8

    #@7
    .line 3371
    :goto_7
    return v2

    #@8
    .line 3362
    :cond_8
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@a
    monitor-enter v3

    #@b
    .line 3363
    if-eqz p1, :cond_31

    #@d
    :try_start_d
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@f
    if-eqz v4, :cond_31

    #@11
    .line 3364
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@13
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@15
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    #@1b
    .line 3365
    .local v0, imi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@1e
    move-result v4

    #@1f
    invoke-static {v0, v4}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@22
    move-result v1

    #@23
    .line 3366
    .local v1, subtypeId:I
    const/4 v4, -0x1

    #@24
    if-eq v1, v4, :cond_31

    #@26
    .line 3367
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@28
    invoke-virtual {p0, v2, v1}, Lcom/android/server/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    #@2b
    .line 3368
    const/4 v2, 0x1

    #@2c
    monitor-exit v3

    #@2d
    goto :goto_7

    #@2e
    .line 3372
    .end local v0           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v1           #subtypeId:I
    :catchall_2e
    move-exception v2

    #@2f
    monitor-exit v3
    :try_end_30
    .catchall {:try_start_d .. :try_end_30} :catchall_2e

    #@30
    throw v2

    #@31
    .line 3371
    :cond_31
    :try_start_31
    monitor-exit v3
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_2e

    #@32
    goto :goto_7
.end method

.method setEnabledSessionInMainThread(Lcom/android/server/InputMethodManagerService$SessionState;)V
    .registers 5
    .parameter "session"

    #@0
    .prologue
    .line 2310
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@2
    if-eq v0, p1, :cond_1e

    #@4
    .line 2311
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 2314
    :try_start_8
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@a
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$SessionState;->method:Lcom/android/internal/view/IInputMethod;

    #@c
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@e
    iget-object v1, v1, Lcom/android/server/InputMethodManagerService$SessionState;->session:Lcom/android/internal/view/IInputMethodSession;

    #@10
    const/4 v2, 0x0

    #@11
    invoke-interface {v0, v1, v2}, Lcom/android/internal/view/IInputMethod;->setSessionEnabled(Lcom/android/internal/view/IInputMethodSession;Z)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_14} :catch_21

    #@14
    .line 2319
    :cond_14
    :goto_14
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService;->mEnabledSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@16
    .line 2322
    :try_start_16
    iget-object v0, p1, Lcom/android/server/InputMethodManagerService$SessionState;->method:Lcom/android/internal/view/IInputMethod;

    #@18
    iget-object v1, p1, Lcom/android/server/InputMethodManagerService$SessionState;->session:Lcom/android/internal/view/IInputMethodSession;

    #@1a
    const/4 v2, 0x1

    #@1b
    invoke-interface {v0, v1, v2}, Lcom/android/internal/view/IInputMethod;->setSessionEnabled(Lcom/android/internal/view/IInputMethodSession;Z)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1e} :catch_1f

    #@1e
    .line 2327
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 2324
    :catch_1f
    move-exception v0

    #@20
    goto :goto_1e

    #@21
    .line 2316
    :catch_21
    move-exception v0

    #@22
    goto :goto_14
.end method

.method public setImeWindowStatus(Landroid/os/IBinder;II)V
    .registers 23
    .parameter "token"
    .parameter "vis"
    .parameter "backDisposition"

    #@0
    .prologue
    .line 1516
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v4

    #@4
    .line 1518
    .local v4, ident:J
    if-eqz p1, :cond_e

    #@6
    :try_start_6
    move-object/from16 v0, p0

    #@8
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@a
    move-object/from16 v0, p1

    #@c
    if-eq v12, v0, :cond_3a

    #@e
    .line 1519
    :cond_e
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@11
    move-result v11

    #@12
    .line 1520
    .local v11, uid:I
    const-string v12, "InputMethodManagerService"

    #@14
    new-instance v13, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v14, "Ignoring setImeWindowStatus of uid "

    #@1b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v13

    #@1f
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v13

    #@23
    const-string v14, " token: "

    #@25
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v13

    #@29
    move-object/from16 v0, p1

    #@2b
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v13

    #@2f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v13

    #@33
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36
    .catchall {:try_start_6 .. :try_end_36} :catchall_100

    #@36
    .line 1569
    .end local v11           #uid:I
    :goto_36
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@39
    .line 1571
    return-void

    #@3a
    .line 1524
    :cond_3a
    :try_start_3a
    move-object/from16 v0, p0

    #@3c
    iget-object v13, v0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@3e
    monitor-enter v13
    :try_end_3f
    .catchall {:try_start_3a .. :try_end_3f} :catchall_100

    #@3f
    .line 1525
    :try_start_3f
    move/from16 v0, p2

    #@41
    move-object/from16 v1, p0

    #@43
    iput v0, v1, Lcom/android/server/InputMethodManagerService;->mImeWindowVis:I

    #@45
    .line 1526
    move/from16 v0, p3

    #@47
    move-object/from16 v1, p0

    #@49
    iput v0, v1, Lcom/android/server/InputMethodManagerService;->mBackDisposition:I

    #@4b
    .line 1527
    move-object/from16 v0, p0

    #@4d
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@4f
    if-eqz v12, :cond_5e

    #@51
    .line 1528
    move-object/from16 v0, p0

    #@53
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@55
    move-object/from16 v0, p1

    #@57
    move/from16 v1, p2

    #@59
    move/from16 v2, p3

    #@5b
    invoke-virtual {v12, v0, v1, v2}, Lcom/android/server/StatusBarManagerService;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@5e
    .line 1530
    :cond_5e
    and-int/lit8 v12, p2, 0x1

    #@60
    if-eqz v12, :cond_105

    #@62
    const/4 v3, 0x1

    #@63
    .line 1531
    .local v3, iconVisibility:Z
    :goto_63
    move-object/from16 v0, p0

    #@65
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@67
    move-object/from16 v0, p0

    #@69
    iget-object v14, v0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@6b
    invoke-virtual {v12, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    move-result-object v6

    #@6f
    check-cast v6, Landroid/view/inputmethod/InputMethodInfo;

    #@71
    .line 1532
    .local v6, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v6, :cond_120

    #@73
    if-eqz v3, :cond_120

    #@75
    invoke-direct/range {p0 .. p0}, Lcom/android/server/InputMethodManagerService;->needsToShowImeSwitchOngoingNotification()Z

    #@78
    move-result v12

    #@79
    if-eqz v12, :cond_120

    #@7b
    .line 1534
    move-object/from16 v0, p0

    #@7d
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@7f
    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@82
    move-result-object v8

    #@83
    .line 1535
    .local v8, pm:Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    #@85
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@87
    const v14, 0x104047b

    #@8a
    invoke-virtual {v12, v14}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@8d
    move-result-object v10

    #@8e
    .line 1537
    .local v10, title:Ljava/lang/CharSequence;
    invoke-virtual {v6, v8}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@91
    move-result-object v7

    #@92
    .line 1538
    .local v7, imiLabel:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@94
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@96
    if-eqz v12, :cond_11e

    #@98
    const/4 v12, 0x2

    #@99
    new-array v14, v12, [Ljava/lang/CharSequence;

    #@9b
    const/4 v12, 0x0

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget-object v15, v0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@a0
    move-object/from16 v0, p0

    #@a2
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@a4
    move-object/from16 v16, v0

    #@a6
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@a9
    move-result-object v17

    #@aa
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    #@ad
    move-result-object v18

    #@ae
    move-object/from16 v0, v18

    #@b0
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b2
    move-object/from16 v18, v0

    #@b4
    invoke-virtual/range {v15 .. v18}, Landroid/view/inputmethod/InputMethodSubtype;->getDisplayName(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@b7
    move-result-object v15

    #@b8
    aput-object v15, v14, v12

    #@ba
    const/4 v15, 0x1

    #@bb
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@be
    move-result v12

    #@bf
    if-eqz v12, :cond_108

    #@c1
    const-string v12, ""

    #@c3
    :goto_c3
    aput-object v12, v14, v15

    #@c5
    invoke-static {v14}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@c8
    move-result-object v9

    #@c9
    .line 1545
    .local v9, summary:Ljava/lang/CharSequence;
    :goto_c9
    move-object/from16 v0, p0

    #@cb
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@cd
    move-object/from16 v0, p0

    #@cf
    iget-object v14, v0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget-object v15, v0, Lcom/android/server/InputMethodManagerService;->mImeSwitchPendingIntent:Landroid/app/PendingIntent;

    #@d5
    invoke-virtual {v12, v14, v10, v9, v15}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@d8
    .line 1547
    move-object/from16 v0, p0

    #@da
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@dc
    if-eqz v12, :cond_fa

    #@de
    .line 1552
    move-object/from16 v0, p0

    #@e0
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@e2
    const/4 v14, 0x0

    #@e3
    const v15, 0x104047b

    #@e6
    move-object/from16 v0, p0

    #@e8
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mImeSwitcherNotification:Landroid/app/Notification;

    #@ea
    move-object/from16 v16, v0

    #@ec
    sget-object v17, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@ee
    move-object/from16 v0, v16

    #@f0
    move-object/from16 v1, v17

    #@f2
    invoke-virtual {v12, v14, v15, v0, v1}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@f5
    .line 1555
    const/4 v12, 0x1

    #@f6
    move-object/from16 v0, p0

    #@f8
    iput-boolean v12, v0, Lcom/android/server/InputMethodManagerService;->mNotificationShown:Z

    #@fa
    .line 1567
    .end local v7           #imiLabel:Ljava/lang/CharSequence;
    .end local v8           #pm:Landroid/content/pm/PackageManager;
    .end local v9           #summary:Ljava/lang/CharSequence;
    .end local v10           #title:Ljava/lang/CharSequence;
    :cond_fa
    :goto_fa
    monitor-exit v13

    #@fb
    goto/16 :goto_36

    #@fd
    .end local v3           #iconVisibility:Z
    .end local v6           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :catchall_fd
    move-exception v12

    #@fe
    monitor-exit v13
    :try_end_ff
    .catchall {:try_start_3f .. :try_end_ff} :catchall_fd

    #@ff
    :try_start_ff
    throw v12
    :try_end_100
    .catchall {:try_start_ff .. :try_end_100} :catchall_100

    #@100
    .line 1569
    :catchall_100
    move-exception v12

    #@101
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@104
    throw v12

    #@105
    .line 1530
    :cond_105
    const/4 v3, 0x0

    #@106
    goto/16 :goto_63

    #@108
    .line 1538
    .restart local v3       #iconVisibility:Z
    .restart local v6       #imi:Landroid/view/inputmethod/InputMethodInfo;
    .restart local v7       #imiLabel:Ljava/lang/CharSequence;
    .restart local v8       #pm:Landroid/content/pm/PackageManager;
    .restart local v10       #title:Ljava/lang/CharSequence;
    :cond_108
    :try_start_108
    new-instance v12, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v16, " - "

    #@10f
    move-object/from16 v0, v16

    #@111
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v12

    #@115
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v12

    #@119
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v12

    #@11d
    goto :goto_c3

    #@11e
    :cond_11e
    move-object v9, v7

    #@11f
    goto :goto_c9

    #@120
    .line 1558
    .end local v7           #imiLabel:Ljava/lang/CharSequence;
    .end local v8           #pm:Landroid/content/pm/PackageManager;
    .end local v10           #title:Ljava/lang/CharSequence;
    :cond_120
    move-object/from16 v0, p0

    #@122
    iget-boolean v12, v0, Lcom/android/server/InputMethodManagerService;->mNotificationShown:Z

    #@124
    if-eqz v12, :cond_fa

    #@126
    move-object/from16 v0, p0

    #@128
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@12a
    if-eqz v12, :cond_fa

    #@12c
    .line 1562
    move-object/from16 v0, p0

    #@12e
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@130
    const/4 v14, 0x0

    #@131
    const v15, 0x104047b

    #@134
    sget-object v16, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@136
    move-object/from16 v0, v16

    #@138
    invoke-virtual {v12, v14, v15, v0}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@13b
    .line 1564
    const/4 v12, 0x0

    #@13c
    move-object/from16 v0, p0

    #@13e
    iput-boolean v12, v0, Lcom/android/server/InputMethodManagerService;->mNotificationShown:Z
    :try_end_140
    .catchall {:try_start_108 .. :try_end_140} :catchall_fd

    #@140
    goto :goto_fa
.end method

.method public setInputMethod(Landroid/os/IBinder;Ljava/lang/String;)V
    .registers 4
    .parameter "token"
    .parameter "id"

    #@0
    .prologue
    .line 2059
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 2063
    :goto_6
    return-void

    #@7
    .line 2062
    :cond_7
    const/4 v0, -0x1

    #@8
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/InputMethodManagerService;->setInputMethodWithSubtypeId(Landroid/os/IBinder;Ljava/lang/String;I)V

    #@b
    goto :goto_6
.end method

.method public setInputMethodAndSubtype(Landroid/os/IBinder;Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 7
    .parameter "token"
    .parameter "id"
    .parameter "subtype"

    #@0
    .prologue
    .line 2067
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 2078
    :goto_6
    return-void

    #@7
    .line 2070
    :cond_7
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v1

    #@a
    .line 2071
    if-eqz p3, :cond_24

    #@c
    .line 2072
    :try_start_c
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@e
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    #@14
    invoke-virtual {p3}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@17
    move-result v2

    #@18
    invoke-static {v0, v2}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@1b
    move-result v0

    #@1c
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/InputMethodManagerService;->setInputMethodWithSubtypeId(Landroid/os/IBinder;Ljava/lang/String;I)V

    #@1f
    .line 2077
    :goto_1f
    monitor-exit v1

    #@20
    goto :goto_6

    #@21
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_c .. :try_end_23} :catchall_21

    #@23
    throw v0

    #@24
    .line 2075
    :cond_24
    :try_start_24
    invoke-virtual {p0, p1, p2}, Lcom/android/server/InputMethodManagerService;->setInputMethod(Landroid/os/IBinder;Ljava/lang/String;)V
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_21

    #@27
    goto :goto_1f
.end method

.method public setInputMethodEnabled(Ljava/lang/String;Z)Z
    .registers 8
    .parameter "id"
    .parameter "enabled"

    #@0
    .prologue
    .line 2898
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_8

    #@6
    .line 2899
    const/4 v2, 0x0

    #@7
    .line 2912
    :goto_7
    return v2

    #@8
    .line 2901
    :cond_8
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@a
    monitor-enter v3

    #@b
    .line 2902
    :try_start_b
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@d
    const-string v4, "android.permission.WRITE_SECURE_SETTINGS"

    #@f
    invoke-virtual {v2, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_20

    #@15
    .line 2905
    new-instance v2, Ljava/lang/SecurityException;

    #@17
    const-string v4, "Requires permission android.permission.WRITE_SECURE_SETTINGS"

    #@19
    invoke-direct {v2, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v2

    #@1d
    .line 2916
    :catchall_1d
    move-exception v2

    #@1e
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_b .. :try_end_1f} :catchall_1d

    #@1f
    throw v2

    #@20
    .line 2910
    :cond_20
    :try_start_20
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_1d

    #@23
    move-result-wide v0

    #@24
    .line 2912
    .local v0, ident:J
    :try_start_24
    invoke-virtual {p0, p1, p2}, Lcom/android/server/InputMethodManagerService;->setInputMethodEnabledLocked(Ljava/lang/String;Z)Z
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_2d

    #@27
    move-result v2

    #@28
    .line 2914
    :try_start_28
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2b
    .line 2912
    monitor-exit v3

    #@2c
    goto :goto_7

    #@2d
    .line 2914
    :catchall_2d
    move-exception v2

    #@2e
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@31
    throw v2
    :try_end_32
    .catchall {:try_start_28 .. :try_end_32} :catchall_1d
.end method

.method setInputMethodEnabledLocked(Ljava/lang/String;Z)Z
    .registers 12
    .parameter "id"
    .parameter "enabled"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 2921
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@4
    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v3

    #@8
    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    #@a
    .line 2922
    .local v3, imm:Landroid/view/inputmethod/InputMethodInfo;
    if-nez v3, :cond_27

    #@c
    .line 2923
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@e
    new-instance v7, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v8, "Unknown id: "

    #@15
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v7

    #@19
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@1b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v7

    #@23
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v6

    #@27
    .line 2926
    :cond_27
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@29
    invoke-static {v6}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->access$1500(Lcom/android/server/InputMethodManagerService$InputMethodSettings;)Ljava/util/List;

    #@2c
    move-result-object v1

    #@2d
    .line 2929
    .local v1, enabledInputMethodsList:Ljava/util/List;,"Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
    if-eqz p2, :cond_52

    #@2f
    .line 2930
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@32
    move-result-object v2

    #@33
    .local v2, i$:Ljava/util/Iterator;
    :cond_33
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@36
    move-result v6

    #@37
    if-eqz v6, :cond_4b

    #@39
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c
    move-result-object v4

    #@3d
    check-cast v4, Landroid/util/Pair;

    #@3f
    .line 2931
    .local v4, pair:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@41
    check-cast v6, Ljava/lang/String;

    #@43
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v6

    #@47
    if-eqz v6, :cond_33

    #@49
    move v6, v7

    #@4a
    .line 2955
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #pair:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :goto_4a
    return v6

    #@4b
    .line 2937
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_4b
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@4d
    invoke-virtual {v6, p1, v8}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->appendAndPutEnabledInputMethodLocked(Ljava/lang/String;Z)V

    #@50
    move v6, v8

    #@51
    .line 2939
    goto :goto_4a

    #@52
    .line 2941
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_52
    new-instance v0, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    .line 2942
    .local v0, builder:Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@59
    invoke-virtual {v6, v0, v1, p1}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->buildAndPutEnabledInputMethodsStrRemovingIdLocked(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Z

    #@5c
    move-result v6

    #@5d
    if-eqz v6, :cond_7f

    #@5f
    .line 2945
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@61
    invoke-virtual {v6}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    .line 2946
    .local v5, selId:Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v6

    #@69
    if-eqz v6, :cond_7d

    #@6b
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->chooseNewDefaultIMELocked()Z

    #@6e
    move-result v6

    #@6f
    if-nez v6, :cond_7d

    #@71
    .line 2947
    const-string v6, "InputMethodManagerService"

    #@73
    const-string v8, "Can\'t find new IME, unsetting the current input method."

    #@75
    invoke-static {v6, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 2948
    const-string v6, ""

    #@7a
    invoke-direct {p0, v6}, Lcom/android/server/InputMethodManagerService;->resetSelectedInputMethodAndSubtypeLocked(Ljava/lang/String;)V

    #@7d
    :cond_7d
    move v6, v7

    #@7e
    .line 2951
    goto :goto_4a

    #@7f
    .end local v5           #selId:Ljava/lang/String;
    :cond_7f
    move v6, v8

    #@80
    .line 2955
    goto :goto_4a
.end method

.method setInputMethodLocked(Ljava/lang/String;I)V
    .registers 14
    .parameter "id"
    .parameter "subtypeId"

    #@0
    .prologue
    .line 1649
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    #@8
    .line 1650
    .local v3, info:Landroid/view/inputmethod/InputMethodInfo;
    if-nez v3, :cond_23

    #@a
    .line 1651
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v9, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v10, "Unknown id: "

    #@13
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v9

    #@1b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v9

    #@1f
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v8

    #@23
    .line 1655
    :cond_23
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v8

    #@29
    if-eqz v8, :cond_84

    #@2b
    .line 1656
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@2e
    move-result v7

    #@2f
    .line 1657
    .local v7, subtypeCount:I
    if-gtz v7, :cond_32

    #@31
    .line 1709
    .end local v7           #subtypeCount:I
    :cond_31
    :goto_31
    return-void

    #@32
    .line 1660
    .restart local v7       #subtypeCount:I
    :cond_32
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@34
    .line 1662
    .local v6, oldSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    if-ltz p2, :cond_63

    #@36
    if-ge p2, v7, :cond_63

    #@38
    .line 1663
    invoke-virtual {v3, p2}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@3b
    move-result-object v5

    #@3c
    .line 1669
    .local v5, newSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    :goto_3c
    if-eqz v5, :cond_40

    #@3e
    if-nez v6, :cond_68

    #@40
    .line 1670
    :cond_40
    const-string v8, "InputMethodManagerService"

    #@42
    new-instance v9, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v10, "Illegal subtype state: old subtype = "

    #@49
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v9

    #@4d
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v9

    #@51
    const-string v10, ", new subtype = "

    #@53
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v9

    #@57
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v9

    #@5b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v9

    #@5f
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_31

    #@63
    .line 1667
    .end local v5           #newSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_63
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getCurrentInputMethodSubtypeLocked()Landroid/view/inputmethod/InputMethodSubtype;

    #@66
    move-result-object v5

    #@67
    .restart local v5       #newSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    goto :goto_3c

    #@68
    .line 1674
    :cond_68
    if-eq v5, v6, :cond_31

    #@6a
    .line 1675
    const/4 v8, 0x1

    #@6b
    invoke-direct {p0, v3, p2, v8}, Lcom/android/server/InputMethodManagerService;->setSelectedInputMethodAndSubtypeLocked(Landroid/view/inputmethod/InputMethodInfo;IZ)V

    #@6e
    .line 1676
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@70
    if-eqz v8, :cond_31

    #@72
    .line 1678
    :try_start_72
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->refreshImeWindowVisibilityLocked()V

    #@75
    .line 1679
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@77
    invoke-interface {v8, v5}, Lcom/android/internal/view/IInputMethod;->changeInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V
    :try_end_7a
    .catch Landroid/os/RemoteException; {:try_start_72 .. :try_end_7a} :catch_7b

    #@7a
    goto :goto_31

    #@7b
    .line 1680
    :catch_7b
    move-exception v0

    #@7c
    .line 1681
    .local v0, e:Landroid/os/RemoteException;
    const-string v8, "InputMethodManagerService"

    #@7e
    const-string v9, "Failed to call changeInputMethodSubtype"

    #@80
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_31

    #@84
    .line 1689
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v5           #newSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v6           #oldSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v7           #subtypeCount:I
    :cond_84
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@87
    move-result-wide v1

    #@88
    .line 1693
    .local v1, ident:J
    const/4 v8, 0x0

    #@89
    :try_start_89
    invoke-direct {p0, v3, p2, v8}, Lcom/android/server/InputMethodManagerService;->setSelectedInputMethodAndSubtypeLocked(Landroid/view/inputmethod/InputMethodInfo;IZ)V

    #@8c
    .line 1697
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@8e
    .line 1699
    invoke-static {}, Landroid/app/ActivityManagerNative;->isSystemReady()Z

    #@91
    move-result v8

    #@92
    if-eqz v8, :cond_ac

    #@94
    .line 1700
    new-instance v4, Landroid/content/Intent;

    #@96
    const-string v8, "android.intent.action.INPUT_METHOD_CHANGED"

    #@98
    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9b
    .line 1701
    .local v4, intent:Landroid/content/Intent;
    const/high16 v8, 0x2000

    #@9d
    invoke-virtual {v4, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@a0
    .line 1702
    const-string v8, "input_method_id"

    #@a2
    invoke-virtual {v4, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@a5
    .line 1703
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@a7
    sget-object v9, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@a9
    invoke-virtual {v8, v4, v9}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@ac
    .line 1705
    .end local v4           #intent:Landroid/content/Intent;
    :cond_ac
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->unbindCurrentClientLocked()V
    :try_end_af
    .catchall {:try_start_89 .. :try_end_af} :catchall_b4

    #@af
    .line 1707
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@b2
    goto/16 :goto_31

    #@b4
    :catchall_b4
    move-exception v8

    #@b5
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@b8
    throw v8
.end method

.method showCurrentInputLocked(ILandroid/os/ResultReceiver;)Z
    .registers 15
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    .line 1749
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowRequested:Z

    #@4
    .line 1750
    and-int/lit8 v5, p1, 0x1

    #@6
    if-nez v5, :cond_a

    #@8
    .line 1751
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowExplicitlyRequested:Z

    #@a
    .line 1753
    :cond_a
    and-int/lit8 v5, p1, 0x2

    #@c
    if-eqz v5, :cond_12

    #@e
    .line 1754
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowExplicitlyRequested:Z

    #@10
    .line 1755
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mShowForced:Z

    #@12
    .line 1758
    :cond_12
    iget-boolean v5, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@14
    if-nez v5, :cond_18

    #@16
    move v2, v4

    #@17
    .line 1810
    :cond_17
    :goto_17
    return v2

    #@18
    .line 1762
    :cond_18
    iget-boolean v5, p0, Lcom/android/server/InputMethodManagerService;->mIsQuickVoiceUsing:Z

    #@1a
    if-eqz v5, :cond_2f

    #@1c
    .line 1763
    const-string v5, "InputMethodManagerService"

    #@1e
    const-string v6, "Softkeypad show Broadcast"

    #@20
    invoke-static {v5, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1764
    new-instance v0, Landroid/content/Intent;

    #@25
    const-string v5, "com.lge.softkeypad.intent.SHOW"

    #@27
    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2a
    .line 1765
    .local v0, intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@2c
    invoke-virtual {v5, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2f
    .line 1769
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2f
    const/4 v3, 0x0

    #@30
    .line 1770
    .local v3, translucentIme:Z
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurAttribute:Landroid/view/inputmethod/EditorInfo;

    #@32
    if-eqz v5, :cond_44

    #@34
    .line 1771
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurAttribute:Landroid/view/inputmethod/EditorInfo;

    #@36
    iget-object v1, v5, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    #@38
    .line 1772
    .local v1, privateImeOptions:Ljava/lang/String;
    if-eqz v1, :cond_44

    #@3a
    const-string v5, "com.lge.ime.opacity"

    #@3c
    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@3f
    move-result v5

    #@40
    const/4 v6, -0x1

    #@41
    if-eq v5, v6, :cond_44

    #@43
    .line 1774
    const/4 v3, 0x1

    #@44
    .line 1777
    .end local v1           #privateImeOptions:Ljava/lang/String;
    :cond_44
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    #@46
    invoke-virtual {v5, v3}, Lcom/android/server/wm/WindowManagerService;->enableTranslucentImeMode(Z)V

    #@49
    .line 1779
    const/4 v2, 0x0

    #@4a
    .line 1780
    .local v2, res:Z
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@4c
    if-eqz v5, :cond_76

    #@4e
    .line 1781
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@50
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@52
    const/16 v6, 0x3fc

    #@54
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->getImeShowFlags()I

    #@57
    move-result v7

    #@58
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@5a
    invoke-virtual {v5, v6, v7, v8, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIOO(IILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@5d
    move-result-object v5

    #@5e
    invoke-virtual {p0, v4, v5}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@61
    .line 1784
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mInputShown:Z

    #@63
    .line 1785
    iget-boolean v4, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@65
    if-eqz v4, :cond_74

    #@67
    iget-boolean v4, p0, Lcom/android/server/InputMethodManagerService;->mVisibleBound:Z

    #@69
    if-nez v4, :cond_74

    #@6b
    .line 1786
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@6d
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mVisibleConnection:Landroid/content/ServiceConnection;

    #@6f
    invoke-direct {p0, v4, v5, v11}, Lcom/android/server/InputMethodManagerService;->bindCurrentInputMethodService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@72
    .line 1788
    iput-boolean v11, p0, Lcom/android/server/InputMethodManagerService;->mVisibleBound:Z

    #@74
    .line 1790
    :cond_74
    const/4 v2, 0x1

    #@75
    goto :goto_17

    #@76
    .line 1791
    :cond_76
    iget-boolean v5, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@78
    if-eqz v5, :cond_17

    #@7a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7d
    move-result-wide v5

    #@7e
    iget-wide v7, p0, Lcom/android/server/InputMethodManagerService;->mLastBindTime:J

    #@80
    const-wide/16 v9, 0x2710

    #@82
    add-long/2addr v7, v9

    #@83
    cmp-long v5, v5, v7

    #@85
    if-ltz v5, :cond_17

    #@87
    .line 1797
    const/16 v5, 0x7d00

    #@89
    const/4 v6, 0x3

    #@8a
    new-array v6, v6, [Ljava/lang/Object;

    #@8c
    iget-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@8e
    aput-object v7, v6, v4

    #@90
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@93
    move-result-wide v7

    #@94
    iget-wide v9, p0, Lcom/android/server/InputMethodManagerService;->mLastBindTime:J

    #@96
    sub-long/2addr v7, v9

    #@97
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9a
    move-result-object v4

    #@9b
    aput-object v4, v6, v11

    #@9d
    const/4 v4, 0x2

    #@9e
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a1
    move-result-object v7

    #@a2
    aput-object v7, v6, v4

    #@a4
    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@a7
    .line 1799
    const-string v4, "InputMethodManagerService"

    #@a9
    const-string v5, "Force disconnect/connect to the IME in showCurrentInputLocked()"

    #@ab
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 1800
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@b0
    invoke-virtual {v4, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@b3
    .line 1801
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@b5
    const v5, 0x40000001

    #@b8
    invoke-direct {p0, v4, p0, v5}, Lcom/android/server/InputMethodManagerService;->bindCurrentInputMethodService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@bb
    goto/16 :goto_17
.end method

.method public showInputMethodAndSubtypeEnablerFromClient(Lcom/android/internal/view/IInputMethodClient;Ljava/lang/String;)V
    .registers 7
    .parameter "client"
    .parameter "inputMethodId"

    #@0
    .prologue
    .line 2083
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 2094
    :goto_6
    return-void

    #@7
    .line 2086
    :cond_7
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v1

    #@a
    .line 2087
    :try_start_a
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@c
    if-eqz v0, :cond_1e

    #@e
    if-eqz p1, :cond_1e

    #@10
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@12
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@14
    invoke-interface {v0}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@17
    move-result-object v0

    #@18
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v2

    #@1c
    if-eq v0, v2, :cond_36

    #@1e
    .line 2089
    :cond_1e
    const-string v0, "InputMethodManagerService"

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "Ignoring showInputMethodAndSubtypeEnablerFromClient of: "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 2091
    :cond_36
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@38
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@3a
    const/4 v3, 0x3

    #@3b
    invoke-virtual {v2, v3, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {p0, v0, v2}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@42
    .line 2093
    monitor-exit v1

    #@43
    goto :goto_6

    #@44
    :catchall_44
    move-exception v0

    #@45
    monitor-exit v1
    :try_end_46
    .catchall {:try_start_a .. :try_end_46} :catchall_44

    #@46
    throw v0
.end method

.method public showInputMethodPickerFromClient(Lcom/android/internal/view/IInputMethodClient;)V
    .registers 6
    .parameter "client"

    #@0
    .prologue
    .line 2041
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 2055
    :goto_6
    return-void

    #@7
    .line 2044
    :cond_7
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v1

    #@a
    .line 2045
    :try_start_a
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@c
    if-eqz v0, :cond_1e

    #@e
    if-eqz p1, :cond_1e

    #@10
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@12
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@14
    invoke-interface {v0}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@17
    move-result-object v0

    #@18
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v2

    #@1c
    if-eq v0, v2, :cond_44

    #@1e
    .line 2047
    :cond_1e
    const-string v0, "InputMethodManagerService"

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "Ignoring showInputMethodPickerFromClient of uid "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, ": "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 2053
    :cond_44
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mHandler:Landroid/os/Handler;

    #@46
    const/4 v2, 0x2

    #@47
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@4a
    .line 2054
    monitor-exit v1

    #@4b
    goto :goto_6

    #@4c
    :catchall_4c
    move-exception v0

    #@4d
    monitor-exit v1
    :try_end_4e
    .catchall {:try_start_a .. :try_end_4e} :catchall_4c

    #@4e
    throw v0
.end method

.method public showMySoftInput(Landroid/os/IBinder;I)V
    .registers 9
    .parameter "token"
    .parameter "flags"

    #@0
    .prologue
    .line 2291
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_7

    #@6
    .line 2307
    :goto_6
    return-void

    #@7
    .line 2294
    :cond_7
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@9
    monitor-enter v3

    #@a
    .line 2295
    if-eqz p1, :cond_10

    #@c
    :try_start_c
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@e
    if-eq v2, p1, :cond_3b

    #@10
    .line 2296
    :cond_10
    const-string v2, "InputMethodManagerService"

    #@12
    new-instance v4, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v5, "Ignoring showMySoftInput of uid "

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@20
    move-result v5

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, " token: "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 2298
    monitor-exit v3

    #@37
    goto :goto_6

    #@38
    .line 2306
    :catchall_38
    move-exception v2

    #@39
    monitor-exit v3
    :try_end_3a
    .catchall {:try_start_c .. :try_end_3a} :catchall_38

    #@3a
    throw v2

    #@3b
    .line 2300
    :cond_3b
    :try_start_3b
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_38

    #@3e
    move-result-wide v0

    #@3f
    .line 2302
    .local v0, ident:J
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {p0, p2, v2}, Lcom/android/server/InputMethodManagerService;->showCurrentInputLocked(ILandroid/os/ResultReceiver;)Z
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_48

    #@43
    .line 2304
    :try_start_43
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@46
    .line 2306
    monitor-exit v3

    #@47
    goto :goto_6

    #@48
    .line 2304
    :catchall_48
    move-exception v2

    #@49
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4c
    throw v2
    :try_end_4d
    .catchall {:try_start_43 .. :try_end_4d} :catchall_38
.end method

.method public showSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    .registers 13
    .parameter "client"
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1714
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@4
    move-result v5

    #@5
    if-nez v5, :cond_8

    #@7
    .line 1741
    :cond_7
    :goto_7
    return v4

    #@8
    .line 1718
    :cond_8
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@a
    if-eqz v5, :cond_10

    #@c
    iget-boolean v5, p0, Lcom/android/server/InputMethodManagerService;->mScreenOn:Z

    #@e
    if-eqz v5, :cond_7

    #@10
    .line 1721
    :cond_10
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@13
    move-result v3

    #@14
    .line 1722
    .local v3, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@17
    move-result-wide v1

    #@18
    .line 1724
    .local v1, ident:J
    :try_start_18
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@1a
    monitor-enter v5
    :try_end_1b
    .catchall {:try_start_18 .. :try_end_1b} :catchall_64

    #@1b
    .line 1725
    :try_start_1b
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@1d
    if-eqz v6, :cond_2f

    #@1f
    if-eqz p1, :cond_2f

    #@21
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@23
    iget-object v6, v6, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@25
    invoke-interface {v6}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@28
    move-result-object v6

    #@29
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;
    :try_end_2c
    .catchall {:try_start_1b .. :try_end_2c} :catchall_61

    #@2c
    move-result-object v7

    #@2d
    if-eq v6, v7, :cond_69

    #@2f
    .line 1731
    :cond_2f
    :try_start_2f
    iget-object v6, p0, Lcom/android/server/InputMethodManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@31
    invoke-interface {v6, p1}, Landroid/view/IWindowManager;->inputMethodClientHasFocus(Lcom/android/internal/view/IInputMethodClient;)Z

    #@34
    move-result v6

    #@35
    if-nez v6, :cond_69

    #@37
    .line 1732
    const-string v6, "InputMethodManagerService"

    #@39
    new-instance v7, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v8, "Ignoring showSoftInput of uid "

    #@40
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v7

    #@44
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v7

    #@48
    const-string v8, ": "

    #@4a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v7

    #@52
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v7

    #@56
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_59
    .catchall {:try_start_2f .. :try_end_59} :catchall_61
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_59} :catch_5e

    #@59
    .line 1733
    :try_start_59
    monitor-exit v5
    :try_end_5a
    .catchall {:try_start_59 .. :try_end_5a} :catchall_61

    #@5a
    .line 1744
    :goto_5a
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5d
    goto :goto_7

    #@5e
    .line 1735
    :catch_5e
    move-exception v0

    #@5f
    .line 1736
    .local v0, e:Landroid/os/RemoteException;
    :try_start_5f
    monitor-exit v5

    #@60
    goto :goto_5a

    #@61
    .line 1742
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_61
    move-exception v4

    #@62
    monitor-exit v5
    :try_end_63
    .catchall {:try_start_5f .. :try_end_63} :catchall_61

    #@63
    :try_start_63
    throw v4
    :try_end_64
    .catchall {:try_start_63 .. :try_end_64} :catchall_64

    #@64
    .line 1744
    :catchall_64
    move-exception v4

    #@65
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@68
    throw v4

    #@69
    .line 1741
    :cond_69
    :try_start_69
    invoke-virtual {p0, p2, p3}, Lcom/android/server/InputMethodManagerService;->showCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@6c
    move-result v4

    #@6d
    monitor-exit v5
    :try_end_6e
    .catchall {:try_start_69 .. :try_end_6e} :catchall_61

    #@6e
    goto :goto_5a
.end method

.method public startInput(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
    .registers 9
    .parameter "client"
    .parameter "inputContext"
    .parameter "attribute"
    .parameter "controlFlags"

    #@0
    .prologue
    .line 1280
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_8

    #@6
    .line 1281
    const/4 v2, 0x0

    #@7
    .line 1286
    :goto_7
    return-object v2

    #@8
    .line 1283
    :cond_8
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@a
    monitor-enter v3

    #@b
    .line 1284
    :try_start_b
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_e
    .catchall {:try_start_b .. :try_end_e} :catchall_18

    #@e
    move-result-wide v0

    #@f
    .line 1286
    .local v0, ident:J
    :try_start_f
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/InputMethodManagerService;->startInputLocked(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_1b

    #@12
    move-result-object v2

    #@13
    .line 1288
    :try_start_13
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@16
    .line 1286
    monitor-exit v3

    #@17
    goto :goto_7

    #@18
    .line 1290
    .end local v0           #ident:J
    :catchall_18
    move-exception v2

    #@19
    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_18

    #@1a
    throw v2

    #@1b
    .line 1288
    .restart local v0       #ident:J
    :catchall_1b
    move-exception v2

    #@1c
    :try_start_1c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    throw v2
    :try_end_20
    .catchall {:try_start_1c .. :try_end_20} :catchall_18
.end method

.method startInputInnerLocked()Lcom/android/internal/view/InputBindResult;
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 1233
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@5
    if-nez v1, :cond_a

    #@7
    .line 1234
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mNoBinding:Lcom/android/internal/view/InputBindResult;

    #@9
    .line 1274
    :goto_9
    return-object v1

    #@a
    .line 1237
    :cond_a
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@c
    if-nez v1, :cond_18

    #@e
    .line 1240
    new-instance v1, Lcom/android/internal/view/InputBindResult;

    #@10
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@12
    iget v4, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@14
    invoke-direct {v1, v2, v3, v4}, Lcom/android/internal/view/InputBindResult;-><init>(Lcom/android/internal/view/IInputMethodSession;Ljava/lang/String;I)V

    #@17
    goto :goto_9

    #@18
    .line 1243
    :cond_18
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@1a
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@1c
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    #@22
    .line 1244
    .local v0, info:Landroid/view/inputmethod/InputMethodInfo;
    if-nez v0, :cond_3f

    #@24
    .line 1245
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@26
    new-instance v2, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v3, "Unknown id: "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v1

    #@3f
    .line 1248
    :cond_3f
    invoke-virtual {p0, v7, v8}, Lcom/android/server/InputMethodManagerService;->unbindCurrentMethodLocked(ZZ)V

    #@42
    .line 1250
    new-instance v1, Landroid/content/Intent;

    #@44
    const-string v3, "android.view.InputMethod"

    #@46
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@49
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@4b
    .line 1251
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@4d
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getComponent()Landroid/content/ComponentName;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@54
    .line 1252
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@56
    const-string v3, "android.intent.extra.client_label"

    #@58
    const v4, 0x10404a9

    #@5b
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@5e
    .line 1254
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@60
    const-string v3, "android.intent.extra.client_intent"

    #@62
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@64
    new-instance v5, Landroid/content/Intent;

    #@66
    const-string v6, "android.settings.INPUT_METHOD_SETTINGS"

    #@68
    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6b
    invoke-static {v4, v7, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@72
    .line 1256
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@74
    const v3, 0x40000001

    #@77
    invoke-direct {p0, v1, p0, v3}, Lcom/android/server/InputMethodManagerService;->bindCurrentInputMethodService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@7a
    move-result v1

    #@7b
    if-eqz v1, :cond_a6

    #@7d
    .line 1258
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@80
    move-result-wide v3

    #@81
    iput-wide v3, p0, Lcom/android/server/InputMethodManagerService;->mLastBindTime:J

    #@83
    .line 1259
    iput-boolean v8, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@85
    .line 1260
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@8b
    .line 1261
    new-instance v1, Landroid/os/Binder;

    #@8d
    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    #@90
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@92
    .line 1264
    :try_start_92
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@94
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@96
    const/16 v4, 0x7db

    #@98
    invoke-interface {v1, v3, v4}, Landroid/view/IWindowManager;->addWindowToken(Landroid/os/IBinder;I)V
    :try_end_9b
    .catch Landroid/os/RemoteException; {:try_start_92 .. :try_end_9b} :catch_c5

    #@9b
    .line 1268
    :goto_9b
    new-instance v1, Lcom/android/internal/view/InputBindResult;

    #@9d
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@9f
    iget v4, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@a1
    invoke-direct {v1, v2, v3, v4}, Lcom/android/internal/view/InputBindResult;-><init>(Lcom/android/internal/view/IInputMethodSession;Ljava/lang/String;I)V

    #@a4
    goto/16 :goto_9

    #@a6
    .line 1270
    :cond_a6
    iput-object v2, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@a8
    .line 1271
    const-string v1, "InputMethodManagerService"

    #@aa
    new-instance v3, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v4, "Failure connecting to input method service: "

    #@b1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v3

    #@b5
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurIntent:Landroid/content/Intent;

    #@b7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v3

    #@bb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v3

    #@bf
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    move-object v1, v2

    #@c3
    .line 1274
    goto/16 :goto_9

    #@c5
    .line 1266
    :catch_c5
    move-exception v1

    #@c6
    goto :goto_9b
.end method

.method startInputLocked(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
    .registers 9
    .parameter "client"
    .parameter "inputContext"
    .parameter "attribute"
    .parameter "controlFlags"

    #@0
    .prologue
    .line 1130
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@2
    if-nez v1, :cond_7

    #@4
    .line 1131
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mNoBinding:Lcom/android/internal/view/InputBindResult;

    #@6
    .line 1154
    :goto_6
    return-object v1

    #@7
    .line 1134
    :cond_7
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mClients:Ljava/util/HashMap;

    #@9
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/android/server/InputMethodManagerService$ClientState;

    #@13
    .line 1135
    .local v0, cs:Lcom/android/server/InputMethodManagerService$ClientState;
    if-nez v0, :cond_32

    #@15
    .line 1136
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "unknown client "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v1

    #@32
    .line 1141
    :cond_32
    :try_start_32
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@34
    iget-object v2, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@36
    invoke-interface {v1, v2}, Landroid/view/IWindowManager;->inputMethodClientHasFocus(Lcom/android/internal/view/IInputMethodClient;)Z

    #@39
    move-result v1

    #@3a
    if-nez v1, :cond_77

    #@3c
    .line 1147
    const-string v1, "InputMethodManagerService"

    #@3e
    new-instance v2, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v3, "Starting input on non-focused client "

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    iget-object v3, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    const-string v3, " (uid="

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    iget v3, v0, Lcom/android/server/InputMethodManagerService$ClientState;->uid:I

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    const-string v3, " pid="

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    iget v3, v0, Lcom/android/server/InputMethodManagerService$ClientState;->pid:I

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    const-string v3, ")"

    #@69
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v2

    #@71
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_74
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_74} :catch_76

    #@74
    .line 1149
    const/4 v1, 0x0

    #@75
    goto :goto_6

    #@76
    .line 1151
    :catch_76
    move-exception v1

    #@77
    .line 1154
    :cond_77
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/android/server/InputMethodManagerService;->startInputUncheckedLocked(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@7a
    move-result-object v1

    #@7b
    goto :goto_6
.end method

.method startInputUncheckedLocked(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
    .registers 15
    .parameter "cs"
    .parameter "inputContext"
    .parameter "attribute"
    .parameter "controlFlags"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    .line 1160
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@5
    if-nez v0, :cond_a

    #@7
    .line 1161
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mNoBinding:Lcom/android/internal/view/InputBindResult;

    #@9
    .line 1229
    :goto_9
    return-object v0

    #@a
    .line 1164
    :cond_a
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@c
    if-nez v0, :cond_1d

    #@e
    .line 1165
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@10
    if-eqz v0, :cond_67

    #@12
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@14
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_67

    #@1a
    move v0, v1

    #@1b
    :goto_1b
    iput-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mInputBoundToKeyguard:Z

    #@1d
    .line 1171
    :cond_1d
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@1f
    if-eq v0, p1, :cond_3a

    #@21
    .line 1174
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->unbindCurrentClientLocked()V

    #@24
    .line 1179
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mScreenOn:Z

    #@26
    if-eqz v0, :cond_3a

    #@28
    .line 1180
    iget-object v3, p1, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@2a
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2c
    const/16 v5, 0xbcc

    #@2e
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mScreenOn:Z

    #@30
    if-eqz v0, :cond_69

    #@32
    move v0, v1

    #@33
    :goto_33
    invoke-virtual {v4, v5, v0, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {p0, v3, v0}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@3a
    .line 1186
    :cond_3a
    iget v0, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@3c
    add-int/lit8 v0, v0, 0x1

    #@3e
    iput v0, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@40
    .line 1187
    iget v0, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@42
    if-gtz v0, :cond_46

    #@44
    iput v1, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@46
    .line 1188
    :cond_46
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@48
    .line 1189
    iput-object p2, p0, Lcom/android/server/InputMethodManagerService;->mCurInputContext:Lcom/android/internal/view/IInputContext;

    #@4a
    .line 1190
    iput-object p3, p0, Lcom/android/server/InputMethodManagerService;->mCurAttribute:Landroid/view/inputmethod/EditorInfo;

    #@4c
    .line 1193
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@4e
    if-eqz v0, :cond_d4

    #@50
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@52
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@54
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_d4

    #@5a
    .line 1194
    iget-object v0, p1, Lcom/android/server/InputMethodManagerService$ClientState;->curSession:Lcom/android/server/InputMethodManagerService$SessionState;

    #@5c
    if-eqz v0, :cond_6d

    #@5e
    .line 1197
    and-int/lit16 v0, p4, 0x100

    #@60
    if-eqz v0, :cond_6b

    #@62
    :goto_62
    invoke-virtual {p0, v1}, Lcom/android/server/InputMethodManagerService;->attachNewInputLocked(Z)Lcom/android/internal/view/InputBindResult;

    #@65
    move-result-object v0

    #@66
    goto :goto_9

    #@67
    :cond_67
    move v0, v2

    #@68
    .line 1165
    goto :goto_1b

    #@69
    :cond_69
    move v0, v2

    #@6a
    .line 1180
    goto :goto_33

    #@6b
    :cond_6b
    move v1, v2

    #@6c
    .line 1197
    goto :goto_62

    #@6d
    .line 1200
    :cond_6d
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@6f
    if-eqz v0, :cond_d4

    #@71
    .line 1201
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@73
    if-eqz v0, :cond_9c

    #@75
    .line 1202
    iget-boolean v0, p1, Lcom/android/server/InputMethodManagerService$ClientState;->sessionRequested:Z

    #@77
    if-nez v0, :cond_91

    #@79
    .line 1203
    iput-boolean v1, p1, Lcom/android/server/InputMethodManagerService$ClientState;->sessionRequested:Z

    #@7b
    .line 1205
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@7d
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@7f
    const/16 v2, 0x41a

    #@81
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@83
    new-instance v4, Lcom/android/server/InputMethodManagerService$MethodCallback;

    #@85
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@87
    invoke-direct {v4, v5, p0}, Lcom/android/server/InputMethodManagerService$MethodCallback;-><init>(Lcom/android/internal/view/IInputMethod;Lcom/android/server/InputMethodManagerService;)V

    #@8a
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@8d
    move-result-object v1

    #@8e
    invoke-virtual {p0, v0, v1}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@91
    .line 1211
    :cond_91
    new-instance v0, Lcom/android/internal/view/InputBindResult;

    #@93
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@95
    iget v2, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@97
    invoke-direct {v0, v9, v1, v2}, Lcom/android/internal/view/InputBindResult;-><init>(Lcom/android/internal/view/IInputMethodSession;Ljava/lang/String;I)V

    #@9a
    goto/16 :goto_9

    #@9c
    .line 1212
    :cond_9c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@9f
    move-result-wide v3

    #@a0
    iget-wide v5, p0, Lcom/android/server/InputMethodManagerService;->mLastBindTime:J

    #@a2
    const-wide/16 v7, 0x2710

    #@a4
    add-long/2addr v5, v7

    #@a5
    cmp-long v0, v3, v5

    #@a7
    if-gez v0, :cond_b4

    #@a9
    .line 1221
    new-instance v0, Lcom/android/internal/view/InputBindResult;

    #@ab
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@ad
    iget v2, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@af
    invoke-direct {v0, v9, v1, v2}, Lcom/android/internal/view/InputBindResult;-><init>(Lcom/android/internal/view/IInputMethodSession;Ljava/lang/String;I)V

    #@b2
    goto/16 :goto_9

    #@b4
    .line 1223
    :cond_b4
    const/16 v0, 0x7d00

    #@b6
    const/4 v3, 0x3

    #@b7
    new-array v3, v3, [Ljava/lang/Object;

    #@b9
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@bb
    aput-object v4, v3, v2

    #@bd
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c0
    move-result-wide v4

    #@c1
    iget-wide v6, p0, Lcom/android/server/InputMethodManagerService;->mLastBindTime:J

    #@c3
    sub-long/2addr v4, v6

    #@c4
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@c7
    move-result-object v4

    #@c8
    aput-object v4, v3, v1

    #@ca
    const/4 v1, 0x2

    #@cb
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ce
    move-result-object v2

    #@cf
    aput-object v2, v3, v1

    #@d1
    invoke-static {v0, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@d4
    .line 1229
    :cond_d4
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->startInputInnerLocked()Lcom/android/internal/view/InputBindResult;

    #@d7
    move-result-object v0

    #@d8
    goto/16 :goto_9
.end method

.method public switchToLastInputMethod(Landroid/os/IBinder;)Z
    .registers 24
    .parameter "token"

    #@0
    .prologue
    .line 2098
    invoke-direct/range {p0 .. p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v17

    #@4
    if-nez v17, :cond_9

    #@6
    .line 2099
    const/16 v17, 0x0

    #@8
    .line 2161
    :goto_8
    return v17

    #@9
    .line 2101
    :cond_9
    move-object/from16 v0, p0

    #@b
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@d
    move-object/from16 v18, v0

    #@f
    monitor-enter v18

    #@10
    .line 2102
    :try_start_10
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@14
    move-object/from16 v17, v0

    #@16
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getLastInputMethodAndSubtypeLocked()Landroid/util/Pair;

    #@19
    move-result-object v11

    #@1a
    .line 2104
    .local v11, lastIme:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v11, :cond_112

    #@1c
    .line 2105
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@20
    move-object/from16 v17, v0

    #@22
    iget-object v0, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@24
    move-object/from16 v19, v0

    #@26
    move-object/from16 v0, v17

    #@28
    move-object/from16 v1, v19

    #@2a
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    move-result-object v12

    #@2e
    check-cast v12, Landroid/view/inputmethod/InputMethodInfo;

    #@30
    .line 2109
    .local v12, lastImi:Landroid/view/inputmethod/InputMethodInfo;
    :goto_30
    const/16 v16, 0x0

    #@32
    .line 2110
    .local v16, targetLastImiId:Ljava/lang/String;
    const/4 v15, -0x1

    #@33
    .line 2111
    .local v15, subtypeId:I
    if-eqz v11, :cond_6e

    #@35
    if-eqz v12, :cond_6e

    #@37
    .line 2112
    invoke-virtual {v12}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@3a
    move-result-object v17

    #@3b
    move-object/from16 v0, p0

    #@3d
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@3f
    move-object/from16 v19, v0

    #@41
    move-object/from16 v0, v17

    #@43
    move-object/from16 v1, v19

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v9

    #@49
    .line 2113
    .local v9, imiIdIsSame:Z
    iget-object v0, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@4b
    move-object/from16 v17, v0

    #@4d
    check-cast v17, Ljava/lang/String;

    #@4f
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@52
    move-result-object v17

    #@53
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    #@56
    move-result v13

    #@57
    .line 2114
    .local v13, lastSubtypeHash:I
    move-object/from16 v0, p0

    #@59
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@5b
    move-object/from16 v17, v0

    #@5d
    if-nez v17, :cond_115

    #@5f
    const/4 v5, -0x1

    #@60
    .line 2118
    .local v5, currentSubtypeHash:I
    :goto_60
    if-eqz v9, :cond_64

    #@62
    if-eq v13, v5, :cond_6e

    #@64
    .line 2119
    :cond_64
    iget-object v0, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@66
    move-object/from16 v16, v0

    #@68
    .end local v16           #targetLastImiId:Ljava/lang/String;
    check-cast v16, Ljava/lang/String;

    #@6a
    .line 2120
    .restart local v16       #targetLastImiId:Ljava/lang/String;
    invoke-static {v12, v13}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@6d
    move-result v15

    #@6e
    .line 2124
    .end local v5           #currentSubtypeHash:I
    .end local v9           #imiIdIsSame:Z
    .end local v13           #lastSubtypeHash:I
    :cond_6e
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@71
    move-result v17

    #@72
    if-eqz v17, :cond_fb

    #@74
    move-object/from16 v0, p0

    #@76
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@78
    move-object/from16 v17, v0

    #@7a
    move-object/from16 v0, p0

    #@7c
    move-object/from16 v1, v17

    #@7e
    invoke-direct {v0, v1}, Lcom/android/server/InputMethodManagerService;->canAddToLastInputMethod(Landroid/view/inputmethod/InputMethodSubtype;)Z

    #@81
    move-result v17

    #@82
    if-nez v17, :cond_fb

    #@84
    .line 2128
    move-object/from16 v0, p0

    #@86
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@88
    move-object/from16 v17, v0

    #@8a
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/List;

    #@8d
    move-result-object v6

    #@8e
    .line 2129
    .local v6, enabled:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz v6, :cond_fb

    #@90
    .line 2130
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@93
    move-result v4

    #@94
    .line 2131
    .local v4, N:I
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@98
    move-object/from16 v17, v0

    #@9a
    if-nez v17, :cond_121

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@a0
    move-object/from16 v17, v0

    #@a2
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@a5
    move-result-object v17

    #@a6
    move-object/from16 v0, v17

    #@a8
    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@aa
    move-object/from16 v17, v0

    #@ac
    invoke-virtual/range {v17 .. v17}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@af
    move-result-object v14

    #@b0
    .line 2134
    .local v14, locale:Ljava/lang/String;
    :goto_b0
    const/4 v7, 0x0

    #@b1
    .local v7, i:I
    :goto_b1
    if-ge v7, v4, :cond_fb

    #@b3
    .line 2135
    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b6
    move-result-object v8

    #@b7
    check-cast v8, Landroid/view/inputmethod/InputMethodInfo;

    #@b9
    .line 2136
    .local v8, imi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@bc
    move-result v17

    #@bd
    if-lez v17, :cond_12c

    #@bf
    invoke-static {v8}, Lcom/android/server/InputMethodManagerService;->isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z

    #@c2
    move-result v17

    #@c3
    if-eqz v17, :cond_12c

    #@c5
    .line 2137
    move-object/from16 v0, p0

    #@c7
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@c9
    move-object/from16 v17, v0

    #@cb
    invoke-static {v8}, Lcom/android/server/InputMethodManagerService;->getSubtypes(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@ce
    move-result-object v19

    #@cf
    const-string v20, "keyboard"

    #@d1
    const/16 v21, 0x1

    #@d3
    move-object/from16 v0, v17

    #@d5
    move-object/from16 v1, v19

    #@d7
    move-object/from16 v2, v20

    #@d9
    move/from16 v3, v21

    #@db
    invoke-static {v0, v1, v2, v14, v3}, Lcom/android/server/InputMethodManagerService;->findLastResortApplicableSubtypeLocked(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;

    #@de
    move-result-object v10

    #@df
    .line 2140
    .local v10, keyboardSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    if-eqz v10, :cond_12c

    #@e1
    .line 2141
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@e4
    move-result-object v16

    #@e5
    .line 2142
    invoke-virtual {v10}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@e8
    move-result v17

    #@e9
    move/from16 v0, v17

    #@eb
    invoke-static {v8, v0}, Lcom/android/server/InputMethodManagerService;->getSubtypeIdFromHashCode(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@ee
    move-result v15

    #@ef
    .line 2144
    invoke-virtual {v10}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@f2
    move-result-object v17

    #@f3
    move-object/from16 v0, v17

    #@f5
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f8
    move-result v17

    #@f9
    if-eqz v17, :cond_12c

    #@fb
    .line 2153
    .end local v4           #N:I
    .end local v6           #enabled:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    .end local v7           #i:I
    .end local v8           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v10           #keyboardSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v14           #locale:Ljava/lang/String;
    :cond_fb
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@fe
    move-result v17

    #@ff
    if-nez v17, :cond_12f

    #@101
    .line 2158
    move-object/from16 v0, p0

    #@103
    move-object/from16 v1, p1

    #@105
    move-object/from16 v2, v16

    #@107
    invoke-direct {v0, v1, v2, v15}, Lcom/android/server/InputMethodManagerService;->setInputMethodWithSubtypeId(Landroid/os/IBinder;Ljava/lang/String;I)V

    #@10a
    .line 2159
    const/16 v17, 0x1

    #@10c
    monitor-exit v18

    #@10d
    goto/16 :goto_8

    #@10f
    .line 2163
    .end local v11           #lastIme:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12           #lastImi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v15           #subtypeId:I
    .end local v16           #targetLastImiId:Ljava/lang/String;
    :catchall_10f
    move-exception v17

    #@110
    monitor-exit v18
    :try_end_111
    .catchall {:try_start_10 .. :try_end_111} :catchall_10f

    #@111
    throw v17

    #@112
    .line 2107
    .restart local v11       #lastIme:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_112
    const/4 v12, 0x0

    #@113
    .restart local v12       #lastImi:Landroid/view/inputmethod/InputMethodInfo;
    goto/16 :goto_30

    #@115
    .line 2114
    .restart local v9       #imiIdIsSame:Z
    .restart local v13       #lastSubtypeHash:I
    .restart local v15       #subtypeId:I
    .restart local v16       #targetLastImiId:Ljava/lang/String;
    :cond_115
    :try_start_115
    move-object/from16 v0, p0

    #@117
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@119
    move-object/from16 v17, v0

    #@11b
    invoke-virtual/range {v17 .. v17}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@11e
    move-result v5

    #@11f
    goto/16 :goto_60

    #@121
    .line 2131
    .end local v9           #imiIdIsSame:Z
    .end local v13           #lastSubtypeHash:I
    .restart local v4       #N:I
    .restart local v6       #enabled:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_121
    move-object/from16 v0, p0

    #@123
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@125
    move-object/from16 v17, v0

    #@127
    invoke-virtual/range {v17 .. v17}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@12a
    move-result-object v14

    #@12b
    goto :goto_b0

    #@12c
    .line 2134
    .restart local v7       #i:I
    .restart local v8       #imi:Landroid/view/inputmethod/InputMethodInfo;
    .restart local v14       #locale:Ljava/lang/String;
    :cond_12c
    add-int/lit8 v7, v7, 0x1

    #@12e
    goto :goto_b1

    #@12f
    .line 2161
    .end local v4           #N:I
    .end local v6           #enabled:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    .end local v7           #i:I
    .end local v8           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v14           #locale:Ljava/lang/String;
    :cond_12f
    const/16 v17, 0x0

    #@131
    monitor-exit v18
    :try_end_132
    .catchall {:try_start_115 .. :try_end_132} :catchall_10f

    #@132
    goto/16 :goto_8
.end method

.method public switchToNextInputMethod(Landroid/os/IBinder;Z)Z
    .registers 9
    .parameter "token"
    .parameter "onlyCurrentIme"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2168
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_9

    #@7
    move v1, v2

    #@8
    .line 2178
    :goto_8
    return v1

    #@9
    .line 2171
    :cond_9
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@b
    monitor-enter v3

    #@c
    .line 2172
    :try_start_c
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mImListManager:Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;

    #@e
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@10
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@12
    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    #@18
    iget-object v5, p0, Lcom/android/server/InputMethodManagerService;->mCurrentSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    #@1a
    invoke-virtual {v4, p2, v1, v5}, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->getNextInputMethod(ZLandroid/view/inputmethod/InputMethodInfo;Landroid/view/inputmethod/InputMethodSubtype;)Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@1d
    move-result-object v0

    #@1e
    .line 2174
    .local v0, nextSubtype:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    if-nez v0, :cond_23

    #@20
    .line 2175
    monitor-exit v3

    #@21
    move v1, v2

    #@22
    goto :goto_8

    #@23
    .line 2177
    :cond_23
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    #@25
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    iget v2, v0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mSubtypeId:I

    #@2b
    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/InputMethodManagerService;->setInputMethodWithSubtypeId(Landroid/os/IBinder;Ljava/lang/String;I)V

    #@2e
    .line 2178
    const/4 v1, 0x1

    #@2f
    monitor-exit v3

    #@30
    goto :goto_8

    #@31
    .line 2179
    .end local v0           #nextSubtype:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    :catchall_31
    move-exception v1

    #@32
    monitor-exit v3
    :try_end_33
    .catchall {:try_start_c .. :try_end_33} :catchall_31

    #@33
    throw v1
.end method

.method public systemReady(Lcom/android/server/StatusBarManagerService;)V
    .registers 6
    .parameter "statusBar"

    #@0
    .prologue
    .line 869
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 873
    :try_start_3
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@5
    if-nez v1, :cond_65

    #@7
    .line 874
    const/4 v1, 0x1

    #@8
    iput-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mSystemReady:Z

    #@a
    .line 875
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@c
    const-string v3, "keyguard"

    #@e
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/app/KeyguardManager;

    #@14
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@16
    .line 877
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@18
    const-string v3, "notification"

    #@1a
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Landroid/app/NotificationManager;

    #@20
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@22
    .line 879
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@24
    .line 880
    const-string v1, "ime"

    #@26
    const/4 v3, 0x0

    #@27
    invoke-virtual {p1, v1, v3}, Lcom/android/server/StatusBarManagerService;->setIconVisibility(Ljava/lang/String;Z)V

    #@2a
    .line 881
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->updateImeWindowStatusLocked()V

    #@2d
    .line 882
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@2f
    const v3, 0x1110008

    #@32
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@35
    move-result v1

    #@36
    iput-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mShowOngoingImeSwitcherForPhones:Z

    #@38
    .line 884
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mShowOngoingImeSwitcherForPhones:Z

    #@3a
    if-eqz v1, :cond_43

    #@3c
    .line 885
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    #@3e
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mHardKeyboardListener:Lcom/android/server/InputMethodManagerService$HardKeyboardListener;

    #@40
    invoke-virtual {v1, v3}, Lcom/android/server/wm/WindowManagerService;->setOnHardKeyboardStatusChangeListener(Lcom/android/server/wm/WindowManagerService$OnHardKeyboardStatusChangeListener;)V

    #@43
    .line 888
    :cond_43
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@45
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@47
    invoke-virtual {p0, v1, v3}, Lcom/android/server/InputMethodManagerService;->buildInputMethodListLocked(Ljava/util/ArrayList;Ljava/util/HashMap;)V

    #@4a
    .line 889
    iget-boolean v1, p0, Lcom/android/server/InputMethodManagerService;->mImeSelectedOnBoot:Z

    #@4c
    if-nez v1, :cond_58

    #@4e
    .line 890
    const-string v1, "InputMethodManagerService"

    #@50
    const-string v3, "Reset the default IME as \"Resource\" is ready here."

    #@52
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 891
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->checkCurrentLocaleChangedLocked()V

    #@58
    .line 893
    :cond_58
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@5a
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@5d
    move-result-object v1

    #@5e
    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@60
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService;->mLastSystemLocale:Ljava/util/Locale;
    :try_end_62
    .catchall {:try_start_3 .. :try_end_62} :catchall_70

    #@62
    .line 895
    :try_start_62
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->startInputInnerLocked()Lcom/android/internal/view/InputBindResult;
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_70
    .catch Ljava/lang/RuntimeException; {:try_start_62 .. :try_end_65} :catch_67

    #@65
    .line 900
    :cond_65
    :goto_65
    :try_start_65
    monitor-exit v2

    #@66
    .line 901
    return-void

    #@67
    .line 896
    :catch_67
    move-exception v0

    #@68
    .line 897
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "InputMethodManagerService"

    #@6a
    const-string v3, "Unexpected exception"

    #@6c
    invoke-static {v1, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6f
    goto :goto_65

    #@70
    .line 900
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catchall_70
    move-exception v1

    #@71
    monitor-exit v2
    :try_end_72
    .catchall {:try_start_65 .. :try_end_72} :catchall_70

    #@72
    throw v1
.end method

.method unbindCurrentClientLocked()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1063
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@3
    if-eqz v0, :cond_4e

    #@5
    .line 1066
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mBoundToMethod:Z

    #@7
    if-eqz v0, :cond_1e

    #@9
    .line 1067
    iput-boolean v5, p0, Lcom/android/server/InputMethodManagerService;->mBoundToMethod:Z

    #@b
    .line 1068
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@d
    if-eqz v0, :cond_1e

    #@f
    .line 1069
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@11
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@13
    const/16 v2, 0x3e8

    #@15
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurMethod:Lcom/android/internal/view/IInputMethod;

    #@17
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p0, v0, v1}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@1e
    .line 1074
    :cond_1e
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@20
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@22
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@24
    const/16 v2, 0xbcc

    #@26
    iget-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@28
    invoke-virtual {v1, v2, v5, v3}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {p0, v0, v1}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@2f
    .line 1076
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@31
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@33
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@35
    const/16 v2, 0xbb8

    #@37
    iget v3, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@39
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@3b
    iget-object v4, v4, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@3d
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {p0, v0, v1}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@44
    .line 1078
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@46
    iput-boolean v5, v0, Lcom/android/server/InputMethodManagerService$ClientState;->sessionRequested:Z

    #@48
    .line 1079
    const/4 v0, 0x0

    #@49
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@4b
    .line 1081
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->hideInputMethodMenuLocked()V

    #@4e
    .line 1083
    :cond_4e
    return-void
.end method

.method unbindCurrentMethodLocked(ZZ)V
    .registers 8
    .parameter "reportToClient"
    .parameter "savePosition"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1340
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mVisibleBound:Z

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 1341
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@8
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mVisibleConnection:Landroid/content/ServiceConnection;

    #@a
    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@d
    .line 1342
    iput-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mVisibleBound:Z

    #@f
    .line 1345
    :cond_f
    iget-boolean v0, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@11
    if-eqz v0, :cond_1a

    #@13
    .line 1346
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@18
    .line 1347
    iput-boolean v2, p0, Lcom/android/server/InputMethodManagerService;->mHaveConnection:Z

    #@1a
    .line 1350
    :cond_1a
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@1c
    if-eqz v0, :cond_34

    #@1e
    .line 1353
    :try_start_1e
    iget v0, p0, Lcom/android/server/InputMethodManagerService;->mImeWindowVis:I

    #@20
    and-int/lit8 v0, v0, 0x1

    #@22
    if-eqz v0, :cond_2b

    #@24
    if-eqz p2, :cond_2b

    #@26
    .line 1355
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    #@28
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->saveLastInputMethodWindowForTransition()V

    #@2b
    .line 1357
    :cond_2b
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@2d
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@2f
    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->removeWindowToken(Landroid/os/IBinder;)V
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_32} :catch_55

    #@32
    .line 1360
    :goto_32
    iput-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@34
    .line 1363
    :cond_34
    iput-object v3, p0, Lcom/android/server/InputMethodManagerService;->mCurId:Ljava/lang/String;

    #@36
    .line 1364
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService;->clearCurMethodLocked()V

    #@39
    .line 1366
    if-eqz p1, :cond_54

    #@3b
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@3d
    if-eqz v0, :cond_54

    #@3f
    .line 1367
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@41
    iget-object v0, v0, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@43
    iget-object v1, p0, Lcom/android/server/InputMethodManagerService;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@45
    const/16 v2, 0xbb8

    #@47
    iget v3, p0, Lcom/android/server/InputMethodManagerService;->mCurSeq:I

    #@49
    iget-object v4, p0, Lcom/android/server/InputMethodManagerService;->mCurClient:Lcom/android/server/InputMethodManagerService$ClientState;

    #@4b
    iget-object v4, v4, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@4d
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {p0, v0, v1}, Lcom/android/server/InputMethodManagerService;->executeOrSendMessage(Landroid/os/IInterface;Landroid/os/Message;)V

    #@54
    .line 1370
    :cond_54
    return-void

    #@55
    .line 1358
    :catch_55
    move-exception v0

    #@56
    goto :goto_32
.end method

.method updateFromSettingsLocked()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 1627
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@5
    invoke-virtual {v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 1629
    .local v1, id:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1b

    #@f
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->chooseNewDefaultIMELocked()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_1b

    #@15
    .line 1630
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@17
    invoke-virtual {v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 1632
    :cond_1b
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1e
    move-result v2

    #@1f
    if-nez v2, :cond_4d

    #@21
    .line 1634
    :try_start_21
    invoke-direct {p0, v1}, Lcom/android/server/InputMethodManagerService;->getSelectedInputMethodSubtypeId(Ljava/lang/String;)I

    #@24
    move-result v2

    #@25
    invoke-virtual {p0, v1, v2}, Lcom/android/server/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
    :try_end_28
    .catch Ljava/lang/IllegalArgumentException; {:try_start_21 .. :try_end_28} :catch_2e

    #@28
    .line 1640
    :goto_28
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mShortcutInputMethodsAndSubtypes:Ljava/util/HashMap;

    #@2a
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@2d
    .line 1646
    :goto_2d
    return-void

    #@2e
    .line 1635
    :catch_2e
    move-exception v0

    #@2f
    .line 1636
    .local v0, e:Ljava/lang/IllegalArgumentException;
    const-string v2, "InputMethodManagerService"

    #@31
    new-instance v3, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v4, "Unknown input method from prefs: "

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@47
    .line 1637
    iput-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@49
    .line 1638
    invoke-virtual {p0, v6, v5}, Lcom/android/server/InputMethodManagerService;->unbindCurrentMethodLocked(ZZ)V

    #@4c
    goto :goto_28

    #@4d
    .line 1643
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :cond_4d
    iput-object v7, p0, Lcom/android/server/InputMethodManagerService;->mCurMethodId:Ljava/lang/String;

    #@4f
    .line 1644
    invoke-virtual {p0, v6, v5}, Lcom/android/server/InputMethodManagerService;->unbindCurrentMethodLocked(ZZ)V

    #@52
    goto :goto_2d
.end method

.method public updateStatusIcon(Landroid/os/IBinder;Ljava/lang/String;I)V
    .registers 16
    .parameter "token"
    .parameter "packageName"
    .parameter "iconId"

    #@0
    .prologue
    .line 1423
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v10

    #@4
    .line 1424
    .local v10, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@7
    move-result-wide v7

    #@8
    .line 1426
    .local v7, ident:J
    if-eqz p1, :cond_e

    #@a
    :try_start_a
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurToken:Landroid/os/IBinder;

    #@c
    if-eq v0, p1, :cond_34

    #@e
    .line 1427
    :cond_e
    const-string v0, "InputMethodManagerService"

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Ignoring setInputMethod of uid "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " token: "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_30
    .catchall {:try_start_a .. :try_end_30} :catchall_4a

    #@30
    .line 1458
    :goto_30
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@33
    .line 1460
    return-void

    #@34
    .line 1431
    :cond_34
    :try_start_34
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@36
    monitor-enter v11
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_4a

    #@37
    .line 1432
    if-nez p3, :cond_4f

    #@39
    .line 1434
    :try_start_39
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@3b
    if-eqz v0, :cond_45

    #@3d
    .line 1435
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@3f
    const-string v1, "ime"

    #@41
    const/4 v2, 0x0

    #@42
    invoke-virtual {v0, v1, v2}, Lcom/android/server/StatusBarManagerService;->setIconVisibility(Ljava/lang/String;Z)V

    #@45
    .line 1456
    :cond_45
    :goto_45
    monitor-exit v11

    #@46
    goto :goto_30

    #@47
    :catchall_47
    move-exception v0

    #@48
    monitor-exit v11
    :try_end_49
    .catchall {:try_start_39 .. :try_end_49} :catchall_47

    #@49
    :try_start_49
    throw v0
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_4a

    #@4a
    .line 1458
    :catchall_4a
    move-exception v0

    #@4b
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4e
    throw v0

    #@4f
    .line 1437
    :cond_4f
    if-eqz p2, :cond_45

    #@51
    .line 1439
    const/4 v6, 0x0

    #@52
    .line 1442
    .local v6, contentDescription:Ljava/lang/CharSequence;
    :try_start_52
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mContext:Landroid/content/Context;

    #@54
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@57
    move-result-object v9

    #@58
    .line 1443
    .local v9, packageManager:Landroid/content/pm/PackageManager;
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mIPackageManager:Landroid/content/pm/IPackageManager;

    #@5a
    const/4 v1, 0x0

    #@5b
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@5d
    invoke-virtual {v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getCurrentUserId()I

    #@60
    move-result v2

    #@61
    invoke-interface {v0, p2, v1, v2}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v9, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_68
    .catchall {:try_start_52 .. :try_end_68} :catchall_47
    .catch Landroid/os/RemoteException; {:try_start_52 .. :try_end_68} :catch_88

    #@68
    move-result-object v6

    #@69
    .line 1449
    .end local v9           #packageManager:Landroid/content/pm/PackageManager;
    :goto_69
    :try_start_69
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@6b
    if-eqz v0, :cond_45

    #@6d
    .line 1450
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@6f
    const-string v1, "ime"

    #@71
    const/4 v4, 0x0

    #@72
    if-eqz v6, :cond_86

    #@74
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    :goto_78
    move-object v2, p2

    #@79
    move v3, p3

    #@7a
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/StatusBarManagerService;->setIcon(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    #@7d
    .line 1453
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService;->mStatusBar:Lcom/android/server/StatusBarManagerService;

    #@7f
    const-string v1, "ime"

    #@81
    const/4 v2, 0x1

    #@82
    invoke-virtual {v0, v1, v2}, Lcom/android/server/StatusBarManagerService;->setIconVisibility(Ljava/lang/String;Z)V
    :try_end_85
    .catchall {:try_start_69 .. :try_end_85} :catchall_47

    #@85
    goto :goto_45

    #@86
    .line 1450
    :cond_86
    const/4 v5, 0x0

    #@87
    goto :goto_78

    #@88
    .line 1446
    :catch_88
    move-exception v0

    #@89
    goto :goto_69
.end method

.method public windowGainedFocus(Lcom/android/internal/view/IInputMethodClient;Landroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;)Lcom/android/internal/view/InputBindResult;
    .registers 23
    .parameter "client"
    .parameter "windowToken"
    .parameter "controlFlags"
    .parameter "softInputMode"
    .parameter "windowFlags"
    .parameter "attribute"
    .parameter "inputContext"

    #@0
    .prologue
    .line 1891
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService;->calledFromValidUser()Z

    #@3
    move-result v3

    #@4
    .line 1893
    .local v3, calledFromValidUser:Z
    const/4 v10, 0x0

    #@5
    .line 1894
    .local v10, res:Lcom/android/internal/view/InputBindResult;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@8
    move-result-wide v7

    #@9
    .line 1896
    .local v7, ident:J
    :try_start_9
    iget-object v12, p0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@b
    monitor-enter v12
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_3a

    #@c
    .line 1902
    :try_start_c
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mClients:Ljava/util/HashMap;

    #@e
    invoke-interface/range {p1 .. p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v13

    #@12
    invoke-virtual {v11, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v4

    #@16
    check-cast v4, Lcom/android/server/InputMethodManagerService$ClientState;

    #@18
    .line 1903
    .local v4, cs:Lcom/android/server/InputMethodManagerService$ClientState;
    if-nez v4, :cond_3f

    #@1a
    .line 1904
    new-instance v11, Ljava/lang/IllegalArgumentException;

    #@1c
    new-instance v13, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v14, "unknown client "

    #@23
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v13

    #@27
    invoke-interface/range {p1 .. p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@2a
    move-result-object v14

    #@2b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v13

    #@2f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v13

    #@33
    invoke-direct {v11, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v11

    #@37
    .line 2031
    .end local v4           #cs:Lcom/android/server/InputMethodManagerService$ClientState;
    :catchall_37
    move-exception v11

    #@38
    monitor-exit v12
    :try_end_39
    .catchall {:try_start_c .. :try_end_39} :catchall_37

    #@39
    :try_start_39
    throw v11
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_3a

    #@3a
    .line 2033
    :catchall_3a
    move-exception v11

    #@3b
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3e
    throw v11

    #@3f
    .line 1909
    .restart local v4       #cs:Lcom/android/server/InputMethodManagerService$ClientState;
    :cond_3f
    :try_start_3f
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@41
    iget-object v13, v4, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@43
    invoke-interface {v11, v13}, Landroid/view/IWindowManager;->inputMethodClientHasFocus(Lcom/android/internal/view/IInputMethodClient;)Z

    #@46
    move-result v11

    #@47
    if-nez v11, :cond_88

    #@49
    .line 1915
    const-string v11, "InputMethodManagerService"

    #@4b
    new-instance v13, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v14, "Focus gain on non-focused client "

    #@52
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v13

    #@56
    iget-object v14, v4, Lcom/android/server/InputMethodManagerService$ClientState;->client:Lcom/android/internal/view/IInputMethodClient;

    #@58
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v13

    #@5c
    const-string v14, " (uid="

    #@5e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v13

    #@62
    iget v14, v4, Lcom/android/server/InputMethodManagerService$ClientState;->uid:I

    #@64
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v13

    #@68
    const-string v14, " pid="

    #@6a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v13

    #@6e
    iget v14, v4, Lcom/android/server/InputMethodManagerService$ClientState;->pid:I

    #@70
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v13

    #@74
    const-string v14, ")"

    #@76
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v13

    #@7a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v13

    #@7e
    invoke-static {v11, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_81
    .catchall {:try_start_3f .. :try_end_81} :catchall_37
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_81} :catch_87

    #@81
    .line 1917
    const/4 v11, 0x0

    #@82
    :try_start_82
    monitor-exit v12
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_37

    #@83
    .line 2033
    :goto_83
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@86
    .line 2036
    :goto_86
    return-object v11

    #@87
    .line 1919
    :catch_87
    move-exception v11

    #@88
    .line 1922
    :cond_88
    if-nez v3, :cond_a0

    #@8a
    .line 1923
    :try_start_8a
    const-string v11, "InputMethodManagerService"

    #@8c
    const-string v13, "A background user is requesting window. Hiding IME."

    #@8e
    invoke-static {v11, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 1924
    const-string v11, "InputMethodManagerService"

    #@93
    const-string v13, "If you want to interect with IME, you need android.permission.INTERACT_ACROSS_USERS_FULL"

    #@95
    invoke-static {v11, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 1926
    const/4 v11, 0x0

    #@99
    const/4 v13, 0x0

    #@9a
    invoke-virtual {p0, v11, v13}, Lcom/android/server/InputMethodManagerService;->hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@9d
    .line 1927
    const/4 v11, 0x0

    #@9e
    monitor-exit v12

    #@9f
    goto :goto_83

    #@a0
    .line 1930
    :cond_a0
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mCurFocusedWindow:Landroid/os/IBinder;

    #@a2
    move-object/from16 v0, p2

    #@a4
    if-ne v11, v0, :cond_e9

    #@a6
    .line 1931
    const-string v11, "InputMethodManagerService"

    #@a8
    new-instance v13, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v14, "Window already focused, ignoring focus gain of: "

    #@af
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v13

    #@b3
    move-object/from16 v0, p1

    #@b5
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v13

    #@b9
    const-string v14, " attribute="

    #@bb
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v13

    #@bf
    move-object/from16 v0, p6

    #@c1
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v13

    #@c5
    const-string v14, ", token = "

    #@c7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v13

    #@cb
    move-object/from16 v0, p2

    #@cd
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v13

    #@d1
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v13

    #@d5
    invoke-static {v11, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 1933
    if-eqz p6, :cond_e6

    #@da
    .line 1934
    move-object/from16 v0, p7

    #@dc
    move-object/from16 v1, p6

    #@de
    move/from16 v2, p3

    #@e0
    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/android/server/InputMethodManagerService;->startInputUncheckedLocked(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@e3
    move-result-object v11

    #@e4
    monitor-exit v12

    #@e5
    goto :goto_83

    #@e6
    .line 1937
    :cond_e6
    const/4 v11, 0x0

    #@e7
    monitor-exit v12

    #@e8
    goto :goto_83

    #@e9
    .line 1939
    :cond_e9
    move-object/from16 v0, p2

    #@eb
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService;->mCurFocusedWindow:Landroid/os/IBinder;

    #@ed
    .line 1948
    move/from16 v0, p4

    #@ef
    and-int/lit16 v11, v0, 0xf0

    #@f1
    const/16 v13, 0x10

    #@f3
    if-eq v11, v13, :cond_102

    #@f5
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService;->mRes:Landroid/content/res/Resources;

    #@f7
    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@fa
    move-result-object v11

    #@fb
    const/4 v13, 0x3

    #@fc
    invoke-virtual {v11, v13}, Landroid/content/res/Configuration;->isLayoutSizeAtLeast(I)Z

    #@ff
    move-result v11

    #@100
    if-eqz v11, :cond_123

    #@102
    :cond_102
    const/4 v6, 0x1

    #@103
    .line 1953
    .local v6, doAutoShow:Z
    :goto_103
    and-int/lit8 v11, p3, 0x2

    #@105
    if-eqz v11, :cond_125

    #@107
    const/4 v9, 0x1

    #@108
    .line 1960
    .local v9, isTextEditor:Z
    :goto_108
    const/4 v5, 0x0

    #@109
    .line 1962
    .local v5, didStart:Z
    and-int/lit8 v11, p4, 0xf

    #@10b
    packed-switch v11, :pswitch_data_194

    #@10e
    .line 2027
    :cond_10e
    :goto_10e
    :pswitch_10e
    if-nez v5, :cond_11c

    #@110
    if-eqz p6, :cond_11c

    #@112
    .line 2028
    move-object/from16 v0, p7

    #@114
    move-object/from16 v1, p6

    #@116
    move/from16 v2, p3

    #@118
    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/android/server/InputMethodManagerService;->startInputUncheckedLocked(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@11b
    move-result-object v10

    #@11c
    .line 2031
    :cond_11c
    monitor-exit v12
    :try_end_11d
    .catchall {:try_start_8a .. :try_end_11d} :catchall_37

    #@11d
    .line 2033
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@120
    move-object v11, v10

    #@121
    .line 2036
    goto/16 :goto_86

    #@123
    .line 1948
    .end local v5           #didStart:Z
    .end local v6           #doAutoShow:Z
    .end local v9           #isTextEditor:Z
    :cond_123
    const/4 v6, 0x0

    #@124
    goto :goto_103

    #@125
    .line 1953
    .restart local v6       #doAutoShow:Z
    :cond_125
    const/4 v9, 0x0

    #@126
    goto :goto_108

    #@127
    .line 1964
    .restart local v5       #didStart:Z
    .restart local v9       #isTextEditor:Z
    :pswitch_127
    if-eqz v9, :cond_12b

    #@129
    if-nez v6, :cond_137

    #@12b
    .line 1965
    :cond_12b
    :try_start_12b
    invoke-static/range {p5 .. p5}, Landroid/view/WindowManager$LayoutParams;->mayUseInputMethod(I)Z

    #@12e
    move-result v11

    #@12f
    if-eqz v11, :cond_10e

    #@131
    .line 1970
    const/4 v11, 0x2

    #@132
    const/4 v13, 0x0

    #@133
    invoke-virtual {p0, v11, v13}, Lcom/android/server/InputMethodManagerService;->hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@136
    goto :goto_10e

    #@137
    .line 1972
    :cond_137
    if-eqz v9, :cond_10e

    #@139
    if-eqz v6, :cond_10e

    #@13b
    move/from16 v0, p4

    #@13d
    and-int/lit16 v11, v0, 0x100

    #@13f
    if-eqz v11, :cond_10e

    #@141
    .line 1982
    if-eqz p6, :cond_14e

    #@143
    .line 1983
    move-object/from16 v0, p7

    #@145
    move-object/from16 v1, p6

    #@147
    move/from16 v2, p3

    #@149
    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/android/server/InputMethodManagerService;->startInputUncheckedLocked(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@14c
    move-result-object v10

    #@14d
    .line 1985
    const/4 v5, 0x1

    #@14e
    .line 1987
    :cond_14e
    const/4 v11, 0x1

    #@14f
    const/4 v13, 0x0

    #@150
    invoke-virtual {p0, v11, v13}, Lcom/android/server/InputMethodManagerService;->showCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@153
    goto :goto_10e

    #@154
    .line 1994
    :pswitch_154
    move/from16 v0, p4

    #@156
    and-int/lit16 v11, v0, 0x100

    #@158
    if-eqz v11, :cond_10e

    #@15a
    .line 1997
    const/4 v11, 0x0

    #@15b
    const/4 v13, 0x0

    #@15c
    invoke-virtual {p0, v11, v13}, Lcom/android/server/InputMethodManagerService;->hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@15f
    goto :goto_10e

    #@160
    .line 2002
    :pswitch_160
    const/4 v11, 0x0

    #@161
    const/4 v13, 0x0

    #@162
    invoke-virtual {p0, v11, v13}, Lcom/android/server/InputMethodManagerService;->hideCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@165
    goto :goto_10e

    #@166
    .line 2005
    :pswitch_166
    move/from16 v0, p4

    #@168
    and-int/lit16 v11, v0, 0x100

    #@16a
    if-eqz v11, :cond_10e

    #@16c
    .line 2008
    if-eqz p6, :cond_179

    #@16e
    .line 2009
    move-object/from16 v0, p7

    #@170
    move-object/from16 v1, p6

    #@172
    move/from16 v2, p3

    #@174
    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/android/server/InputMethodManagerService;->startInputUncheckedLocked(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@177
    move-result-object v10

    #@178
    .line 2011
    const/4 v5, 0x1

    #@179
    .line 2013
    :cond_179
    const/4 v11, 0x1

    #@17a
    const/4 v13, 0x0

    #@17b
    invoke-virtual {p0, v11, v13}, Lcom/android/server/InputMethodManagerService;->showCurrentInputLocked(ILandroid/os/ResultReceiver;)Z

    #@17e
    goto :goto_10e

    #@17f
    .line 2018
    :pswitch_17f
    if-eqz p6, :cond_18c

    #@181
    .line 2019
    move-object/from16 v0, p7

    #@183
    move-object/from16 v1, p6

    #@185
    move/from16 v2, p3

    #@187
    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/android/server/InputMethodManagerService;->startInputUncheckedLocked(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@18a
    move-result-object v10

    #@18b
    .line 2021
    const/4 v5, 0x1

    #@18c
    .line 2023
    :cond_18c
    const/4 v11, 0x1

    #@18d
    const/4 v13, 0x0

    #@18e
    invoke-virtual {p0, v11, v13}, Lcom/android/server/InputMethodManagerService;->showCurrentInputLocked(ILandroid/os/ResultReceiver;)Z
    :try_end_191
    .catchall {:try_start_12b .. :try_end_191} :catchall_37

    #@191
    goto/16 :goto_10e

    #@193
    .line 1962
    nop

    #@194
    :pswitch_data_194
    .packed-switch 0x0
        :pswitch_127
        :pswitch_10e
        :pswitch_154
        :pswitch_160
        :pswitch_166
        :pswitch_17f
    .end packed-switch
.end method
