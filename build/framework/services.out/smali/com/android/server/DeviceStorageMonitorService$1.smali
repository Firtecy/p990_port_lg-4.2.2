.class Lcom/android/server/DeviceStorageMonitorService$1;
.super Landroid/os/Handler;
.source "DeviceStorageMonitorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DeviceStorageMonitorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DeviceStorageMonitorService;


# direct methods
.method constructor <init>(Lcom/android/server/DeviceStorageMonitorService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 134
    iput-object p1, p0, Lcom/android/server/DeviceStorageMonitorService$1;->this$0:Lcom/android/server/DeviceStorageMonitorService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 138
    iget v1, p1, Landroid/os/Message;->what:I

    #@3
    if-eq v1, v0, :cond_d

    #@5
    .line 139
    const-string v0, "DeviceStorageMonitorService"

    #@7
    const-string v1, "Will not process invalid message"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 143
    :goto_c
    return-void

    #@d
    .line 142
    :cond_d
    iget-object v1, p0, Lcom/android/server/DeviceStorageMonitorService$1;->this$0:Lcom/android/server/DeviceStorageMonitorService;

    #@f
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@11
    if-ne v2, v0, :cond_17

    #@13
    :goto_13
    invoke-static {v1, v0}, Lcom/android/server/DeviceStorageMonitorService;->access$000(Lcom/android/server/DeviceStorageMonitorService;Z)V

    #@16
    goto :goto_c

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_13
.end method
