.class Lcom/android/server/AppWidgetService$2;
.super Landroid/content/BroadcastReceiver;
.source "AppWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AppWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/AppWidgetService;


# direct methods
.method constructor <init>(Lcom/android/server/AppWidgetService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 374
    iput-object p1, p0, Lcom/android/server/AppWidgetService$2;->this$0:Lcom/android/server/AppWidgetService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 376
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 378
    .local v0, action:Ljava/lang/String;
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    #@6
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_39

    #@c
    .line 379
    const-string v5, "android.intent.extra.user_handle"

    #@e
    const/16 v6, -0x2710

    #@10
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@13
    move-result v4

    #@14
    .line 380
    .local v4, userId:I
    if-ltz v4, :cond_20

    #@16
    .line 381
    iget-object v5, p0, Lcom/android/server/AppWidgetService$2;->this$0:Lcom/android/server/AppWidgetService;

    #@18
    invoke-static {v5, v4}, Lcom/android/server/AppWidgetService;->access$000(Lcom/android/server/AppWidgetService;I)Lcom/android/server/AppWidgetServiceImpl;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5}, Lcom/android/server/AppWidgetServiceImpl;->sendInitialBroadcasts()V

    #@1f
    .line 404
    .end local v4           #userId:I
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 383
    .restart local v4       #userId:I
    :cond_20
    const-string v5, "AppWidgetService"

    #@22
    new-instance v6, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v7, "Incorrect user handle supplied in "

    #@29
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_1f

    #@39
    .line 385
    .end local v4           #userId:I
    :cond_39
    const-string v5, "android.intent.action.CONFIGURATION_CHANGED"

    #@3b
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v5

    #@3f
    if-eqz v5, :cond_60

    #@41
    .line 386
    const/4 v1, 0x0

    #@42
    .local v1, i:I
    :goto_42
    iget-object v5, p0, Lcom/android/server/AppWidgetService$2;->this$0:Lcom/android/server/AppWidgetService;

    #@44
    invoke-static {v5}, Lcom/android/server/AppWidgetService;->access$100(Lcom/android/server/AppWidgetService;)Landroid/util/SparseArray;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@4b
    move-result v5

    #@4c
    if-ge v1, v5, :cond_1f

    #@4e
    .line 387
    iget-object v5, p0, Lcom/android/server/AppWidgetService$2;->this$0:Lcom/android/server/AppWidgetService;

    #@50
    invoke-static {v5}, Lcom/android/server/AppWidgetService;->access$100(Lcom/android/server/AppWidgetService;)Landroid/util/SparseArray;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@57
    move-result-object v3

    #@58
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl;

    #@5a
    .line 388
    .local v3, service:Lcom/android/server/AppWidgetServiceImpl;
    invoke-virtual {v3}, Lcom/android/server/AppWidgetServiceImpl;->onConfigurationChanged()V

    #@5d
    .line 386
    add-int/lit8 v1, v1, 0x1

    #@5f
    goto :goto_42

    #@60
    .line 391
    .end local v1           #i:I
    .end local v3           #service:Lcom/android/server/AppWidgetServiceImpl;
    :cond_60
    invoke-virtual {p0}, Lcom/android/server/AppWidgetService$2;->getSendingUserId()I

    #@63
    move-result v2

    #@64
    .line 392
    .local v2, sendingUser:I
    const/4 v5, -0x1

    #@65
    if-ne v2, v5, :cond_86

    #@67
    .line 393
    const/4 v1, 0x0

    #@68
    .restart local v1       #i:I
    :goto_68
    iget-object v5, p0, Lcom/android/server/AppWidgetService$2;->this$0:Lcom/android/server/AppWidgetService;

    #@6a
    invoke-static {v5}, Lcom/android/server/AppWidgetService;->access$100(Lcom/android/server/AppWidgetService;)Landroid/util/SparseArray;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@71
    move-result v5

    #@72
    if-ge v1, v5, :cond_1f

    #@74
    .line 394
    iget-object v5, p0, Lcom/android/server/AppWidgetService$2;->this$0:Lcom/android/server/AppWidgetService;

    #@76
    invoke-static {v5}, Lcom/android/server/AppWidgetService;->access$100(Lcom/android/server/AppWidgetService;)Landroid/util/SparseArray;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@7d
    move-result-object v3

    #@7e
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl;

    #@80
    .line 395
    .restart local v3       #service:Lcom/android/server/AppWidgetServiceImpl;
    invoke-virtual {v3, p2}, Lcom/android/server/AppWidgetServiceImpl;->onBroadcastReceived(Landroid/content/Intent;)V

    #@83
    .line 393
    add-int/lit8 v1, v1, 0x1

    #@85
    goto :goto_68

    #@86
    .line 398
    .end local v1           #i:I
    .end local v3           #service:Lcom/android/server/AppWidgetServiceImpl;
    :cond_86
    iget-object v5, p0, Lcom/android/server/AppWidgetService$2;->this$0:Lcom/android/server/AppWidgetService;

    #@88
    invoke-static {v5}, Lcom/android/server/AppWidgetService;->access$100(Lcom/android/server/AppWidgetService;)Landroid/util/SparseArray;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8f
    move-result-object v3

    #@90
    check-cast v3, Lcom/android/server/AppWidgetServiceImpl;

    #@92
    .line 399
    .restart local v3       #service:Lcom/android/server/AppWidgetServiceImpl;
    if-eqz v3, :cond_1f

    #@94
    .line 400
    invoke-virtual {v3, p2}, Lcom/android/server/AppWidgetServiceImpl;->onBroadcastReceived(Landroid/content/Intent;)V

    #@97
    goto :goto_1f
.end method
