.class Lcom/android/server/WifiService$2;
.super Landroid/content/BroadcastReceiver;
.source "WifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/WifiService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/WifiService;


# direct methods
.method constructor <init>(Lcom/android/server/WifiService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 550
    iput-object p1, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    .line 553
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    const-string v5, "android.net.wifi.WIFI_STATE_CHANGED"

    #@8
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_4d

    #@e
    .line 554
    const-string v3, "wifi_state"

    #@10
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@13
    move-result v2

    #@14
    .line 557
    .local v2, wifiState:I
    iget-object v5, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@16
    if-ne v2, v6, :cond_36

    #@18
    move v3, v4

    #@19
    :goto_19
    invoke-static {v5, v3}, Lcom/android/server/WifiService;->access$1202(Lcom/android/server/WifiService;Z)Z

    #@1c
    .line 560
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@1e
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1300(Lcom/android/server/WifiService;)V

    #@21
    .line 566
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@23
    if-eqz v3, :cond_35

    #@25
    .line 567
    if-ne v4, v2, :cond_38

    #@27
    .line 568
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@29
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1400(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2c
    move-result-object v3

    #@2d
    invoke-interface {v3}, Lcom/lge/wifi_iface/WifiServiceExtIface;->unregBrdcastReceiverDelay()V

    #@30
    .line 569
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@32
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1500(Lcom/android/server/WifiService;)V

    #@35
    .line 622
    .end local v2           #wifiState:I
    :cond_35
    :goto_35
    return-void

    #@36
    .line 557
    .restart local v2       #wifiState:I
    :cond_36
    const/4 v3, 0x0

    #@37
    goto :goto_19

    #@38
    .line 572
    :cond_38
    if-ne v6, v2, :cond_35

    #@3a
    .line 573
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@3c
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1600(Lcom/android/server/WifiService;)Z

    #@3f
    move-result v3

    #@40
    if-nez v3, :cond_35

    #@42
    .line 574
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@44
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1700(Lcom/android/server/WifiService;)V

    #@47
    .line 575
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@49
    invoke-static {v3, v4}, Lcom/android/server/WifiService;->access$1602(Lcom/android/server/WifiService;Z)Z

    #@4c
    goto :goto_35

    #@4d
    .line 580
    .end local v2           #wifiState:I
    :cond_4d
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    const-string v5, "android.net.wifi.STATE_CHANGE"

    #@53
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v3

    #@57
    if-eqz v3, :cond_9d

    #@59
    .line 582
    iget-object v4, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@5b
    const-string v3, "networkInfo"

    #@5d
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@60
    move-result-object v3

    #@61
    check-cast v3, Landroid/net/NetworkInfo;

    #@63
    iput-object v3, v4, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@65
    .line 585
    sget-object v3, Lcom/android/server/WifiService$5;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    #@67
    iget-object v4, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@69
    iget-object v4, v4, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@6b
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    #@72
    move-result v4

    #@73
    aget v3, v3, v4

    #@75
    packed-switch v3, :pswitch_data_e2

    #@78
    .line 594
    :goto_78
    sget-object v3, Lcom/android/server/WifiService$5;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    #@7a
    iget-object v4, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@7c
    iget-object v4, v4, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@7e
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    #@85
    move-result v4

    #@86
    aget v3, v3, v4

    #@88
    packed-switch v3, :pswitch_data_ec

    #@8b
    goto :goto_35

    #@8c
    .line 598
    :pswitch_8c
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@8e
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1900(Lcom/android/server/WifiService;)V

    #@91
    goto :goto_35

    #@92
    .line 589
    :pswitch_92
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@94
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1800(Lcom/android/server/WifiService;)V

    #@97
    .line 590
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@99
    invoke-static {v3}, Lcom/android/server/WifiService;->access$1300(Lcom/android/server/WifiService;)V

    #@9c
    goto :goto_78

    #@9d
    .line 602
    :cond_9d
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a0
    move-result-object v3

    #@a1
    const-string v5, "android.net.wifi.SCAN_RESULTS"

    #@a3
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a6
    move-result v3

    #@a7
    if-eqz v3, :cond_b4

    #@a9
    .line 604
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@ab
    invoke-static {v3}, Lcom/android/server/WifiService;->access$2000(Lcom/android/server/WifiService;)V

    #@ae
    .line 605
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@b0
    invoke-static {v3}, Lcom/android/server/WifiService;->access$2100(Lcom/android/server/WifiService;)V

    #@b3
    goto :goto_35

    #@b4
    .line 608
    :cond_b4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@b7
    move-result-object v3

    #@b8
    const-string v5, "android.net.wifi.HS20_AP_EVENT"

    #@ba
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bd
    move-result v3

    #@be
    if-eqz v3, :cond_d3

    #@c0
    .line 615
    const-string v3, "roamingInd"

    #@c2
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@c5
    move-result v1

    #@c6
    .line 616
    .local v1, roamingInd:I
    const-string v3, "bssid"

    #@c8
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@cb
    move-result-object v0

    #@cc
    .line 617
    .local v0, hs20Bssid:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/WifiService$2;->this$0:Lcom/android/server/WifiService;

    #@ce
    invoke-static {v3, v0, v1}, Lcom/android/server/WifiService;->access$2200(Lcom/android/server/WifiService;Ljava/lang/String;I)V

    #@d1
    goto/16 :goto_35

    #@d3
    .line 618
    .end local v0           #hs20Bssid:Ljava/lang/String;
    .end local v1           #roamingInd:I
    :cond_d3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@d6
    move-result-object v3

    #@d7
    const-string v4, "android.net.wifi.HS20_TRY_CONNECTION"

    #@d9
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc
    move-result v3

    #@dd
    if-eqz v3, :cond_35

    #@df
    goto/16 :goto_35

    #@e1
    .line 585
    nop

    #@e2
    :pswitch_data_e2
    .packed-switch 0x1
        :pswitch_92
        :pswitch_92
        :pswitch_92
    .end packed-switch

    #@ec
    .line 594
    :pswitch_data_ec
    .packed-switch 0x2
        :pswitch_8c
    .end packed-switch
.end method
