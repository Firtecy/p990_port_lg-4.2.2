.class Lcom/android/server/BackupManagerService$BackupHandler;
.super Landroid/os/Handler;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackupHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/BackupManagerService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 462
    iput-object p1, p0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    .line 463
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 464
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 31
    .parameter "msg"

    #@0
    .prologue
    .line 468
    move-object/from16 v0, p1

    #@2
    iget v5, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v5, :pswitch_data_408

    #@7
    .line 689
    :goto_7
    :pswitch_7
    return-void

    #@8
    .line 471
    :pswitch_8
    move-object/from16 v0, p0

    #@a
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v6

    #@10
    iput-wide v6, v5, Lcom/android/server/BackupManagerService;->mLastBackupPass:J

    #@12
    .line 472
    move-object/from16 v0, p0

    #@14
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@16
    move-object/from16 v0, p0

    #@18
    iget-object v6, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@1a
    iget-wide v6, v6, Lcom/android/server/BackupManagerService;->mLastBackupPass:J

    #@1c
    const-wide/32 v8, 0x36ee80

    #@1f
    add-long/2addr v6, v8

    #@20
    iput-wide v6, v5, Lcom/android/server/BackupManagerService;->mNextBackupPass:J

    #@22
    .line 474
    move-object/from16 v0, p0

    #@24
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@26
    move-object/from16 v0, p0

    #@28
    iget-object v6, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@2a
    iget-object v6, v6, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@2c
    invoke-static {v5, v6}, Lcom/android/server/BackupManagerService;->access$100(Lcom/android/server/BackupManagerService;Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@2f
    move-result-object v28

    #@30
    .line 475
    .local v28, transport:Lcom/android/internal/backup/IBackupTransport;
    if-nez v28, :cond_55

    #@32
    .line 476
    const-string v5, "BackupManagerService"

    #@34
    const-string v6, "Backup requested but no transport available"

    #@36
    invoke-static {v5, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 477
    move-object/from16 v0, p0

    #@3b
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@3d
    iget-object v6, v5, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@3f
    monitor-enter v6

    #@40
    .line 478
    :try_start_40
    move-object/from16 v0, p0

    #@42
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@44
    const/4 v7, 0x0

    #@45
    iput-boolean v7, v5, Lcom/android/server/BackupManagerService;->mBackupRunning:Z

    #@47
    .line 479
    monitor-exit v6
    :try_end_48
    .catchall {:try_start_40 .. :try_end_48} :catchall_52

    #@48
    .line 480
    move-object/from16 v0, p0

    #@4a
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@4c
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@4e
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@51
    goto :goto_7

    #@52
    .line 479
    :catchall_52
    move-exception v5

    #@53
    :try_start_53
    monitor-exit v6
    :try_end_54
    .catchall {:try_start_53 .. :try_end_54} :catchall_52

    #@54
    throw v5

    #@55
    .line 485
    :cond_55
    new-instance v23, Ljava/util/ArrayList;

    #@57
    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    #@5a
    .line 486
    .local v23, queue:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/BackupManagerService$BackupRequest;>;"
    move-object/from16 v0, p0

    #@5c
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@5e
    iget-object v0, v5, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@60
    move-object/from16 v19, v0

    #@62
    .line 487
    .local v19, oldJournal:Ljava/io/File;
    move-object/from16 v0, p0

    #@64
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@66
    iget-object v6, v5, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@68
    monitor-enter v6

    #@69
    .line 491
    :try_start_69
    move-object/from16 v0, p0

    #@6b
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@6d
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@6f
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@72
    move-result v5

    #@73
    if-lez v5, :cond_aa

    #@75
    .line 492
    move-object/from16 v0, p0

    #@77
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@79
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@7b
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@7e
    move-result-object v5

    #@7f
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@82
    move-result-object v18

    #@83
    .local v18, i$:Ljava/util/Iterator;
    :goto_83
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    #@86
    move-result v5

    #@87
    if-eqz v5, :cond_9a

    #@89
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8c
    move-result-object v16

    #@8d
    check-cast v16, Lcom/android/server/BackupManagerService$BackupRequest;

    #@8f
    .line 493
    .local v16, b:Lcom/android/server/BackupManagerService$BackupRequest;
    move-object/from16 v0, v23

    #@91
    move-object/from16 v1, v16

    #@93
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@96
    goto :goto_83

    #@97
    .line 502
    .end local v16           #b:Lcom/android/server/BackupManagerService$BackupRequest;
    .end local v18           #i$:Ljava/util/Iterator;
    :catchall_97
    move-exception v5

    #@98
    monitor-exit v6
    :try_end_99
    .catchall {:try_start_69 .. :try_end_99} :catchall_97

    #@99
    throw v5

    #@9a
    .line 496
    .restart local v18       #i$:Ljava/util/Iterator;
    :cond_9a
    :try_start_9a
    move-object/from16 v0, p0

    #@9c
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@9e
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@a0
    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    #@a3
    .line 499
    move-object/from16 v0, p0

    #@a5
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@a7
    const/4 v7, 0x0

    #@a8
    iput-object v7, v5, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@aa
    .line 502
    .end local v18           #i$:Ljava/util/Iterator;
    :cond_aa
    monitor-exit v6
    :try_end_ab
    .catchall {:try_start_9a .. :try_end_ab} :catchall_97

    #@ab
    .line 509
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    #@ae
    move-result v5

    #@af
    if-lez v5, :cond_d5

    #@b1
    .line 511
    new-instance v21, Lcom/android/server/BackupManagerService$PerformBackupTask;

    #@b3
    move-object/from16 v0, p0

    #@b5
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@b7
    move-object/from16 v0, v21

    #@b9
    move-object/from16 v1, v28

    #@bb
    move-object/from16 v2, v23

    #@bd
    move-object/from16 v3, v19

    #@bf
    invoke-direct {v0, v5, v1, v2, v3}, Lcom/android/server/BackupManagerService$PerformBackupTask;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Ljava/util/ArrayList;Ljava/io/File;)V

    #@c2
    .line 512
    .local v21, pbt:Lcom/android/server/BackupManagerService$PerformBackupTask;
    const/16 v5, 0x14

    #@c4
    move-object/from16 v0, p0

    #@c6
    move-object/from16 v1, v21

    #@c8
    invoke-virtual {v0, v5, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@cb
    move-result-object v22

    #@cc
    .line 513
    .local v22, pbtMessage:Landroid/os/Message;
    move-object/from16 v0, p0

    #@ce
    move-object/from16 v1, v22

    #@d0
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@d3
    goto/16 :goto_7

    #@d5
    .line 515
    .end local v21           #pbt:Lcom/android/server/BackupManagerService$PerformBackupTask;
    .end local v22           #pbtMessage:Landroid/os/Message;
    :cond_d5
    const-string v5, "BackupManagerService"

    #@d7
    const-string v6, "Backup requested but nothing pending"

    #@d9
    invoke-static {v5, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 516
    move-object/from16 v0, p0

    #@de
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@e0
    iget-object v6, v5, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@e2
    monitor-enter v6

    #@e3
    .line 517
    :try_start_e3
    move-object/from16 v0, p0

    #@e5
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@e7
    const/4 v7, 0x0

    #@e8
    iput-boolean v7, v5, Lcom/android/server/BackupManagerService;->mBackupRunning:Z

    #@ea
    .line 518
    monitor-exit v6
    :try_end_eb
    .catchall {:try_start_e3 .. :try_end_eb} :catchall_f6

    #@eb
    .line 519
    move-object/from16 v0, p0

    #@ed
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@ef
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@f1
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@f4
    goto/16 :goto_7

    #@f6
    .line 518
    :catchall_f6
    move-exception v5

    #@f7
    :try_start_f7
    monitor-exit v6
    :try_end_f8
    .catchall {:try_start_f7 .. :try_end_f8} :catchall_f6

    #@f8
    throw v5

    #@f9
    .line 527
    .end local v19           #oldJournal:Ljava/io/File;
    .end local v23           #queue:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/BackupManagerService$BackupRequest;>;"
    .end local v28           #transport:Lcom/android/internal/backup/IBackupTransport;
    :pswitch_f9
    :try_start_f9
    move-object/from16 v0, p1

    #@fb
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@fd
    check-cast v4, Lcom/android/server/BackupManagerService$BackupRestoreTask;

    #@ff
    .line 529
    .local v4, task:Lcom/android/server/BackupManagerService$BackupRestoreTask;
    invoke-interface {v4}, Lcom/android/server/BackupManagerService$BackupRestoreTask;->execute()V
    :try_end_102
    .catch Ljava/lang/ClassCastException; {:try_start_f9 .. :try_end_102} :catch_104

    #@102
    goto/16 :goto_7

    #@104
    .line 530
    .end local v4           #task:Lcom/android/server/BackupManagerService$BackupRestoreTask;
    :catch_104
    move-exception v17

    #@105
    .line 531
    .local v17, e:Ljava/lang/ClassCastException;
    const-string v5, "BackupManagerService"

    #@107
    new-instance v6, Ljava/lang/StringBuilder;

    #@109
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10c
    const-string v7, "Invalid backup task in flight, obj="

    #@10e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v6

    #@112
    move-object/from16 v0, p1

    #@114
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@116
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v6

    #@11a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11d
    move-result-object v6

    #@11e
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@121
    goto/16 :goto_7

    #@123
    .line 539
    .end local v17           #e:Ljava/lang/ClassCastException;
    :pswitch_123
    :try_start_123
    move-object/from16 v0, p1

    #@125
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@127
    check-cast v4, Lcom/android/server/BackupManagerService$BackupRestoreTask;

    #@129
    .line 540
    .restart local v4       #task:Lcom/android/server/BackupManagerService$BackupRestoreTask;
    invoke-interface {v4}, Lcom/android/server/BackupManagerService$BackupRestoreTask;->operationComplete()V
    :try_end_12c
    .catch Ljava/lang/ClassCastException; {:try_start_123 .. :try_end_12c} :catch_12e

    #@12c
    goto/16 :goto_7

    #@12e
    .line 541
    .end local v4           #task:Lcom/android/server/BackupManagerService$BackupRestoreTask;
    :catch_12e
    move-exception v17

    #@12f
    .line 542
    .restart local v17       #e:Ljava/lang/ClassCastException;
    const-string v5, "BackupManagerService"

    #@131
    new-instance v6, Ljava/lang/StringBuilder;

    #@133
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v7, "Invalid completion in flight, obj="

    #@138
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v6

    #@13c
    move-object/from16 v0, p1

    #@13e
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@140
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v6

    #@144
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@147
    move-result-object v6

    #@148
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14b
    goto/16 :goto_7

    #@14d
    .line 551
    .end local v17           #e:Ljava/lang/ClassCastException;
    :pswitch_14d
    move-object/from16 v0, p1

    #@14f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@151
    move-object/from16 v20, v0

    #@153
    check-cast v20, Lcom/android/server/BackupManagerService$FullBackupParams;

    #@155
    .line 552
    .local v20, params:Lcom/android/server/BackupManagerService$FullBackupParams;
    new-instance v4, Lcom/android/server/BackupManagerService$PerformFullBackupTask;

    #@157
    move-object/from16 v0, p0

    #@159
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@15b
    move-object/from16 v0, v20

    #@15d
    iget-object v6, v0, Lcom/android/server/BackupManagerService$FullParams;->fd:Landroid/os/ParcelFileDescriptor;

    #@15f
    move-object/from16 v0, v20

    #@161
    iget-object v7, v0, Lcom/android/server/BackupManagerService$FullParams;->observer:Landroid/app/backup/IFullBackupRestoreObserver;

    #@163
    move-object/from16 v0, v20

    #@165
    iget-boolean v8, v0, Lcom/android/server/BackupManagerService$FullBackupParams;->includeApks:Z

    #@167
    move-object/from16 v0, v20

    #@169
    iget-boolean v9, v0, Lcom/android/server/BackupManagerService$FullBackupParams;->includeShared:Z

    #@16b
    move-object/from16 v0, v20

    #@16d
    iget-object v10, v0, Lcom/android/server/BackupManagerService$FullParams;->curPassword:Ljava/lang/String;

    #@16f
    move-object/from16 v0, v20

    #@171
    iget-object v11, v0, Lcom/android/server/BackupManagerService$FullParams;->encryptPassword:Ljava/lang/String;

    #@173
    move-object/from16 v0, v20

    #@175
    iget-boolean v12, v0, Lcom/android/server/BackupManagerService$FullBackupParams;->allApps:Z

    #@177
    move-object/from16 v0, v20

    #@179
    iget-boolean v13, v0, Lcom/android/server/BackupManagerService$FullBackupParams;->includeSystem:Z

    #@17b
    move-object/from16 v0, v20

    #@17d
    iget-object v14, v0, Lcom/android/server/BackupManagerService$FullBackupParams;->packages:[Ljava/lang/String;

    #@17f
    move-object/from16 v0, v20

    #@181
    iget-object v15, v0, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@183
    invoke-direct/range {v4 .. v15}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;-><init>(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;Landroid/app/backup/IFullBackupRestoreObserver;ZZLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    #@186
    .line 556
    .local v4, task:Lcom/android/server/BackupManagerService$PerformFullBackupTask;
    new-instance v5, Ljava/lang/Thread;

    #@188
    invoke-direct {v5, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@18b
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    #@18e
    goto/16 :goto_7

    #@190
    .line 562
    .end local v4           #task:Lcom/android/server/BackupManagerService$PerformFullBackupTask;
    .end local v20           #params:Lcom/android/server/BackupManagerService$FullBackupParams;
    :pswitch_190
    move-object/from16 v0, p1

    #@192
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@194
    move-object/from16 v20, v0

    #@196
    check-cast v20, Lcom/android/server/BackupManagerService$RestoreParams;

    #@198
    .line 563
    .local v20, params:Lcom/android/server/BackupManagerService$RestoreParams;
    const-string v5, "BackupManagerService"

    #@19a
    new-instance v6, Ljava/lang/StringBuilder;

    #@19c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@19f
    const-string v7, "MSG_RUN_RESTORE observer="

    #@1a1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v6

    #@1a5
    move-object/from16 v0, v20

    #@1a7
    iget-object v7, v0, Lcom/android/server/BackupManagerService$RestoreParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@1a9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v6

    #@1ad
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v6

    #@1b1
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b4
    .line 564
    new-instance v4, Lcom/android/server/BackupManagerService$PerformRestoreTask;

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@1ba
    move-object/from16 v0, v20

    #@1bc
    iget-object v6, v0, Lcom/android/server/BackupManagerService$RestoreParams;->transport:Lcom/android/internal/backup/IBackupTransport;

    #@1be
    move-object/from16 v0, v20

    #@1c0
    iget-object v7, v0, Lcom/android/server/BackupManagerService$RestoreParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@1c2
    move-object/from16 v0, v20

    #@1c4
    iget-wide v8, v0, Lcom/android/server/BackupManagerService$RestoreParams;->token:J

    #@1c6
    move-object/from16 v0, v20

    #@1c8
    iget-object v10, v0, Lcom/android/server/BackupManagerService$RestoreParams;->pkgInfo:Landroid/content/pm/PackageInfo;

    #@1ca
    move-object/from16 v0, v20

    #@1cc
    iget v11, v0, Lcom/android/server/BackupManagerService$RestoreParams;->pmToken:I

    #@1ce
    move-object/from16 v0, v20

    #@1d0
    iget-boolean v12, v0, Lcom/android/server/BackupManagerService$RestoreParams;->needFullBackup:Z

    #@1d2
    move-object/from16 v0, v20

    #@1d4
    iget-object v13, v0, Lcom/android/server/BackupManagerService$RestoreParams;->filterSet:[Ljava/lang/String;

    #@1d6
    invoke-direct/range {v4 .. v13}, Lcom/android/server/BackupManagerService$PerformRestoreTask;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;JLandroid/content/pm/PackageInfo;IZ[Ljava/lang/String;)V

    #@1d9
    .line 568
    .local v4, task:Lcom/android/server/BackupManagerService$PerformRestoreTask;
    const/16 v5, 0x14

    #@1db
    move-object/from16 v0, p0

    #@1dd
    invoke-virtual {v0, v5, v4}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1e0
    move-result-object v26

    #@1e1
    .line 569
    .local v26, restoreMsg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@1e3
    move-object/from16 v1, v26

    #@1e5
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@1e8
    goto/16 :goto_7

    #@1ea
    .line 577
    .end local v4           #task:Lcom/android/server/BackupManagerService$PerformRestoreTask;
    .end local v20           #params:Lcom/android/server/BackupManagerService$RestoreParams;
    .end local v26           #restoreMsg:Landroid/os/Message;
    :pswitch_1ea
    move-object/from16 v0, p1

    #@1ec
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ee
    move-object/from16 v20, v0

    #@1f0
    check-cast v20, Lcom/android/server/BackupManagerService$FullRestoreParams;

    #@1f2
    .line 578
    .local v20, params:Lcom/android/server/BackupManagerService$FullRestoreParams;
    new-instance v4, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;

    #@1f4
    move-object/from16 v0, p0

    #@1f6
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@1f8
    move-object/from16 v0, v20

    #@1fa
    iget-object v6, v0, Lcom/android/server/BackupManagerService$FullParams;->fd:Landroid/os/ParcelFileDescriptor;

    #@1fc
    move-object/from16 v0, v20

    #@1fe
    iget-object v7, v0, Lcom/android/server/BackupManagerService$FullParams;->curPassword:Ljava/lang/String;

    #@200
    move-object/from16 v0, v20

    #@202
    iget-object v8, v0, Lcom/android/server/BackupManagerService$FullParams;->encryptPassword:Ljava/lang/String;

    #@204
    move-object/from16 v0, v20

    #@206
    iget-object v9, v0, Lcom/android/server/BackupManagerService$FullParams;->observer:Landroid/app/backup/IFullBackupRestoreObserver;

    #@208
    move-object/from16 v0, v20

    #@20a
    iget-object v10, v0, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@20c
    invoke-direct/range {v4 .. v10}, Lcom/android/server/BackupManagerService$PerformFullRestoreTask;-><init>(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    #@20f
    .line 581
    .local v4, task:Lcom/android/server/BackupManagerService$PerformFullRestoreTask;
    new-instance v5, Ljava/lang/Thread;

    #@211
    invoke-direct {v5, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@214
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    #@217
    goto/16 :goto_7

    #@219
    .line 587
    .end local v4           #task:Lcom/android/server/BackupManagerService$PerformFullRestoreTask;
    .end local v20           #params:Lcom/android/server/BackupManagerService$FullRestoreParams;
    :pswitch_219
    move-object/from16 v0, p1

    #@21b
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21d
    move-object/from16 v20, v0

    #@21f
    check-cast v20, Lcom/android/server/BackupManagerService$ClearParams;

    #@221
    .line 588
    .local v20, params:Lcom/android/server/BackupManagerService$ClearParams;
    new-instance v5, Lcom/android/server/BackupManagerService$PerformClearTask;

    #@223
    move-object/from16 v0, p0

    #@225
    iget-object v6, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@227
    move-object/from16 v0, v20

    #@229
    iget-object v7, v0, Lcom/android/server/BackupManagerService$ClearParams;->transport:Lcom/android/internal/backup/IBackupTransport;

    #@22b
    move-object/from16 v0, v20

    #@22d
    iget-object v8, v0, Lcom/android/server/BackupManagerService$ClearParams;->packageInfo:Landroid/content/pm/PackageInfo;

    #@22f
    invoke-direct {v5, v6, v7, v8}, Lcom/android/server/BackupManagerService$PerformClearTask;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/content/pm/PackageInfo;)V

    #@232
    invoke-virtual {v5}, Lcom/android/server/BackupManagerService$PerformClearTask;->run()V

    #@235
    goto/16 :goto_7

    #@237
    .line 597
    .end local v20           #params:Lcom/android/server/BackupManagerService$ClearParams;
    :pswitch_237
    move-object/from16 v0, p0

    #@239
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@23b
    iget-object v6, v5, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@23d
    monitor-enter v6

    #@23e
    .line 598
    :try_start_23e
    new-instance v24, Ljava/util/HashSet;

    #@240
    move-object/from16 v0, p0

    #@242
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@244
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@246
    move-object/from16 v0, v24

    #@248
    invoke-direct {v0, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@24b
    .line 599
    .local v24, queue:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@24d
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@24f
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@251
    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    #@254
    .line 600
    monitor-exit v6
    :try_end_255
    .catchall {:try_start_23e .. :try_end_255} :catchall_265

    #@255
    .line 602
    new-instance v5, Lcom/android/server/BackupManagerService$PerformInitializeTask;

    #@257
    move-object/from16 v0, p0

    #@259
    iget-object v6, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@25b
    move-object/from16 v0, v24

    #@25d
    invoke-direct {v5, v6, v0}, Lcom/android/server/BackupManagerService$PerformInitializeTask;-><init>(Lcom/android/server/BackupManagerService;Ljava/util/HashSet;)V

    #@260
    invoke-virtual {v5}, Lcom/android/server/BackupManagerService$PerformInitializeTask;->run()V

    #@263
    goto/16 :goto_7

    #@265
    .line 600
    .end local v24           #queue:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :catchall_265
    move-exception v5

    #@266
    :try_start_266
    monitor-exit v6
    :try_end_267
    .catchall {:try_start_266 .. :try_end_267} :catchall_265

    #@267
    throw v5

    #@268
    .line 609
    :pswitch_268
    const/16 v27, 0x0

    #@26a
    .line 610
    .local v27, sets:[Landroid/app/backup/RestoreSet;
    move-object/from16 v0, p1

    #@26c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26e
    move-object/from16 v20, v0

    #@270
    check-cast v20, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;

    #@272
    .line 612
    .local v20, params:Lcom/android/server/BackupManagerService$RestoreGetSetsParams;
    :try_start_272
    move-object/from16 v0, v20

    #@274
    iget-object v5, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->transport:Lcom/android/internal/backup/IBackupTransport;

    #@276
    invoke-interface {v5}, Lcom/android/internal/backup/IBackupTransport;->getAvailableRestoreSets()[Landroid/app/backup/RestoreSet;

    #@279
    move-result-object v27

    #@27a
    .line 614
    move-object/from16 v0, v20

    #@27c
    iget-object v6, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->session:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@27e
    monitor-enter v6
    :try_end_27f
    .catchall {:try_start_272 .. :try_end_27f} :catchall_31b
    .catch Ljava/lang/Exception; {:try_start_272 .. :try_end_27f} :catch_2c0

    #@27f
    .line 615
    :try_start_27f
    move-object/from16 v0, v20

    #@281
    iget-object v5, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->session:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@283
    move-object/from16 v0, v27

    #@285
    iput-object v0, v5, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@287
    .line 616
    monitor-exit v6
    :try_end_288
    .catchall {:try_start_27f .. :try_end_288} :catchall_2bd

    #@288
    .line 617
    if-nez v27, :cond_292

    #@28a
    const/16 v5, 0xb0f

    #@28c
    const/4 v6, 0x0

    #@28d
    :try_start_28d
    new-array v6, v6, [Ljava/lang/Object;

    #@28f
    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I
    :try_end_292
    .catchall {:try_start_28d .. :try_end_292} :catchall_31b
    .catch Ljava/lang/Exception; {:try_start_28d .. :try_end_292} :catch_2c0

    #@292
    .line 621
    :cond_292
    move-object/from16 v0, v20

    #@294
    iget-object v5, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@296
    if-eqz v5, :cond_2a1

    #@298
    .line 623
    :try_start_298
    move-object/from16 v0, v20

    #@29a
    iget-object v5, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@29c
    move-object/from16 v0, v27

    #@29e
    invoke-interface {v5, v0}, Landroid/app/backup/IRestoreObserver;->restoreSetsAvailable([Landroid/app/backup/RestoreSet;)V
    :try_end_2a1
    .catch Landroid/os/RemoteException; {:try_start_298 .. :try_end_2a1} :catch_2f3
    .catch Ljava/lang/Exception; {:try_start_298 .. :try_end_2a1} :catch_2fc

    #@2a1
    .line 632
    :cond_2a1
    :goto_2a1
    const/16 v5, 0x8

    #@2a3
    move-object/from16 v0, p0

    #@2a5
    invoke-virtual {v0, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@2a8
    .line 633
    const/16 v5, 0x8

    #@2aa
    const-wide/32 v6, 0xea60

    #@2ad
    move-object/from16 v0, p0

    #@2af
    invoke-virtual {v0, v5, v6, v7}, Lcom/android/server/BackupManagerService$BackupHandler;->sendEmptyMessageDelayed(IJ)Z

    #@2b2
    .line 635
    move-object/from16 v0, p0

    #@2b4
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@2b6
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@2b8
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@2bb
    goto/16 :goto_7

    #@2bd
    .line 616
    :catchall_2bd
    move-exception v5

    #@2be
    :try_start_2be
    monitor-exit v6
    :try_end_2bf
    .catchall {:try_start_2be .. :try_end_2bf} :catchall_2bd

    #@2bf
    :try_start_2bf
    throw v5
    :try_end_2c0
    .catchall {:try_start_2bf .. :try_end_2c0} :catchall_31b
    .catch Ljava/lang/Exception; {:try_start_2bf .. :try_end_2c0} :catch_2c0

    #@2c0
    .line 618
    :catch_2c0
    move-exception v17

    #@2c1
    .line 619
    .local v17, e:Ljava/lang/Exception;
    :try_start_2c1
    const-string v5, "BackupManagerService"

    #@2c3
    const-string v6, "Error from transport getting set list"

    #@2c5
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2c8
    .catchall {:try_start_2c1 .. :try_end_2c8} :catchall_31b

    #@2c8
    .line 621
    move-object/from16 v0, v20

    #@2ca
    iget-object v5, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@2cc
    if-eqz v5, :cond_2d7

    #@2ce
    .line 623
    :try_start_2ce
    move-object/from16 v0, v20

    #@2d0
    iget-object v5, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@2d2
    move-object/from16 v0, v27

    #@2d4
    invoke-interface {v5, v0}, Landroid/app/backup/IRestoreObserver;->restoreSetsAvailable([Landroid/app/backup/RestoreSet;)V
    :try_end_2d7
    .catch Landroid/os/RemoteException; {:try_start_2ce .. :try_end_2d7} :catch_307
    .catch Ljava/lang/Exception; {:try_start_2ce .. :try_end_2d7} :catch_310

    #@2d7
    .line 632
    :cond_2d7
    :goto_2d7
    const/16 v5, 0x8

    #@2d9
    move-object/from16 v0, p0

    #@2db
    invoke-virtual {v0, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@2de
    .line 633
    const/16 v5, 0x8

    #@2e0
    const-wide/32 v6, 0xea60

    #@2e3
    move-object/from16 v0, p0

    #@2e5
    invoke-virtual {v0, v5, v6, v7}, Lcom/android/server/BackupManagerService$BackupHandler;->sendEmptyMessageDelayed(IJ)Z

    #@2e8
    .line 635
    move-object/from16 v0, p0

    #@2ea
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@2ec
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@2ee
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@2f1
    goto/16 :goto_7

    #@2f3
    .line 624
    .end local v17           #e:Ljava/lang/Exception;
    :catch_2f3
    move-exception v25

    #@2f4
    .line 625
    .local v25, re:Landroid/os/RemoteException;
    const-string v5, "BackupManagerService"

    #@2f6
    const-string v6, "Unable to report listing to observer"

    #@2f8
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2fb
    goto :goto_2a1

    #@2fc
    .line 626
    .end local v25           #re:Landroid/os/RemoteException;
    :catch_2fc
    move-exception v17

    #@2fd
    .line 627
    .restart local v17       #e:Ljava/lang/Exception;
    const-string v5, "BackupManagerService"

    #@2ff
    const-string v6, "Restore observer threw"

    #@301
    move-object/from16 v0, v17

    #@303
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@306
    goto :goto_2a1

    #@307
    .line 624
    :catch_307
    move-exception v25

    #@308
    .line 625
    .restart local v25       #re:Landroid/os/RemoteException;
    const-string v5, "BackupManagerService"

    #@30a
    const-string v6, "Unable to report listing to observer"

    #@30c
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30f
    goto :goto_2d7

    #@310
    .line 626
    .end local v25           #re:Landroid/os/RemoteException;
    :catch_310
    move-exception v17

    #@311
    .line 627
    const-string v5, "BackupManagerService"

    #@313
    const-string v6, "Restore observer threw"

    #@315
    move-object/from16 v0, v17

    #@317
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31a
    goto :goto_2d7

    #@31b
    .line 621
    .end local v17           #e:Ljava/lang/Exception;
    :catchall_31b
    move-exception v5

    #@31c
    move-object/from16 v0, v20

    #@31e
    iget-object v6, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@320
    if-eqz v6, :cond_32b

    #@322
    .line 623
    :try_start_322
    move-object/from16 v0, v20

    #@324
    iget-object v6, v0, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;->observer:Landroid/app/backup/IRestoreObserver;

    #@326
    move-object/from16 v0, v27

    #@328
    invoke-interface {v6, v0}, Landroid/app/backup/IRestoreObserver;->restoreSetsAvailable([Landroid/app/backup/RestoreSet;)V
    :try_end_32b
    .catch Landroid/os/RemoteException; {:try_start_322 .. :try_end_32b} :catch_346
    .catch Ljava/lang/Exception; {:try_start_322 .. :try_end_32b} :catch_34f

    #@32b
    .line 632
    :cond_32b
    :goto_32b
    const/16 v6, 0x8

    #@32d
    move-object/from16 v0, p0

    #@32f
    invoke-virtual {v0, v6}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@332
    .line 633
    const/16 v6, 0x8

    #@334
    const-wide/32 v7, 0xea60

    #@337
    move-object/from16 v0, p0

    #@339
    invoke-virtual {v0, v6, v7, v8}, Lcom/android/server/BackupManagerService$BackupHandler;->sendEmptyMessageDelayed(IJ)Z

    #@33c
    .line 635
    move-object/from16 v0, p0

    #@33e
    iget-object v6, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@340
    iget-object v6, v6, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@342
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    #@345
    throw v5

    #@346
    .line 624
    :catch_346
    move-exception v25

    #@347
    .line 625
    .restart local v25       #re:Landroid/os/RemoteException;
    const-string v6, "BackupManagerService"

    #@349
    const-string v7, "Unable to report listing to observer"

    #@34b
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34e
    goto :goto_32b

    #@34f
    .line 626
    .end local v25           #re:Landroid/os/RemoteException;
    :catch_34f
    move-exception v17

    #@350
    .line 627
    .restart local v17       #e:Ljava/lang/Exception;
    const-string v6, "BackupManagerService"

    #@352
    const-string v7, "Restore observer threw"

    #@354
    move-object/from16 v0, v17

    #@356
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@359
    goto :goto_32b

    #@35a
    .line 642
    .end local v17           #e:Ljava/lang/Exception;
    .end local v20           #params:Lcom/android/server/BackupManagerService$RestoreGetSetsParams;
    .end local v27           #sets:[Landroid/app/backup/RestoreSet;
    :pswitch_35a
    move-object/from16 v0, p0

    #@35c
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@35e
    move-object/from16 v0, p1

    #@360
    iget v6, v0, Landroid/os/Message;->arg1:I

    #@362
    move-object/from16 v0, p1

    #@364
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@366
    invoke-virtual {v5, v6, v7}, Lcom/android/server/BackupManagerService;->handleTimeout(ILjava/lang/Object;)V

    #@369
    goto/16 :goto_7

    #@36b
    .line 648
    :pswitch_36b
    move-object/from16 v0, p0

    #@36d
    iget-object v6, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@36f
    monitor-enter v6

    #@370
    .line 649
    :try_start_370
    move-object/from16 v0, p0

    #@372
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@374
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@376
    if-eqz v5, :cond_39c

    #@378
    .line 654
    const-string v5, "BackupManagerService"

    #@37a
    const-string v7, "Restore session timed out; aborting"

    #@37c
    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37f
    .line 655
    new-instance v5, Lcom/android/server/BackupManagerService$ActiveRestoreSession$EndRestoreRunnable;

    #@381
    move-object/from16 v0, p0

    #@383
    iget-object v7, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@385
    iget-object v7, v7, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@387
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@38a
    move-object/from16 v0, p0

    #@38c
    iget-object v8, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@38e
    move-object/from16 v0, p0

    #@390
    iget-object v9, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@392
    iget-object v9, v9, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@394
    invoke-direct {v5, v7, v8, v9}, Lcom/android/server/BackupManagerService$ActiveRestoreSession$EndRestoreRunnable;-><init>(Lcom/android/server/BackupManagerService$ActiveRestoreSession;Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$ActiveRestoreSession;)V

    #@397
    move-object/from16 v0, p0

    #@399
    invoke-virtual {v0, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->post(Ljava/lang/Runnable;)Z

    #@39c
    .line 658
    :cond_39c
    monitor-exit v6
    :try_end_39d
    .catchall {:try_start_370 .. :try_end_39d} :catchall_3e6

    #@39d
    .line 663
    :pswitch_39d
    move-object/from16 v0, p0

    #@39f
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@3a1
    iget-object v6, v5, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@3a3
    monitor-enter v6

    #@3a4
    .line 664
    :try_start_3a4
    move-object/from16 v0, p0

    #@3a6
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@3a8
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@3aa
    move-object/from16 v0, p1

    #@3ac
    iget v7, v0, Landroid/os/Message;->arg1:I

    #@3ae
    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@3b1
    move-result-object v20

    #@3b2
    check-cast v20, Lcom/android/server/BackupManagerService$FullParams;

    #@3b4
    .line 665
    .local v20, params:Lcom/android/server/BackupManagerService$FullParams;
    if-eqz v20, :cond_3e9

    #@3b6
    .line 666
    const-string v5, "BackupManagerService"

    #@3b8
    const-string v7, "Full backup/restore timed out waiting for user confirmation"

    #@3ba
    invoke-static {v5, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3bd
    .line 669
    move-object/from16 v0, p0

    #@3bf
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@3c1
    move-object/from16 v0, v20

    #@3c3
    invoke-virtual {v5, v0}, Lcom/android/server/BackupManagerService;->signalFullBackupRestoreCompletion(Lcom/android/server/BackupManagerService$FullParams;)V

    #@3c6
    .line 672
    move-object/from16 v0, p0

    #@3c8
    iget-object v5, v0, Lcom/android/server/BackupManagerService$BackupHandler;->this$0:Lcom/android/server/BackupManagerService;

    #@3ca
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@3cc
    move-object/from16 v0, p1

    #@3ce
    iget v7, v0, Landroid/os/Message;->arg1:I

    #@3d0
    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->delete(I)V

    #@3d3
    .line 675
    move-object/from16 v0, v20

    #@3d5
    iget-object v5, v0, Lcom/android/server/BackupManagerService$FullParams;->observer:Landroid/app/backup/IFullBackupRestoreObserver;
    :try_end_3d7
    .catchall {:try_start_3a4 .. :try_end_3d7} :catchall_3e3

    #@3d7
    if-eqz v5, :cond_3e0

    #@3d9
    .line 677
    :try_start_3d9
    move-object/from16 v0, v20

    #@3db
    iget-object v5, v0, Lcom/android/server/BackupManagerService$FullParams;->observer:Landroid/app/backup/IFullBackupRestoreObserver;

    #@3dd
    invoke-interface {v5}, Landroid/app/backup/IFullBackupRestoreObserver;->onTimeout()V
    :try_end_3e0
    .catchall {:try_start_3d9 .. :try_end_3e0} :catchall_3e3
    .catch Landroid/os/RemoteException; {:try_start_3d9 .. :try_end_3e0} :catch_406

    #@3e0
    .line 685
    :cond_3e0
    :goto_3e0
    :try_start_3e0
    monitor-exit v6

    #@3e1
    goto/16 :goto_7

    #@3e3
    .end local v20           #params:Lcom/android/server/BackupManagerService$FullParams;
    :catchall_3e3
    move-exception v5

    #@3e4
    monitor-exit v6
    :try_end_3e5
    .catchall {:try_start_3e0 .. :try_end_3e5} :catchall_3e3

    #@3e5
    throw v5

    #@3e6
    .line 658
    :catchall_3e6
    move-exception v5

    #@3e7
    :try_start_3e7
    monitor-exit v6
    :try_end_3e8
    .catchall {:try_start_3e7 .. :try_end_3e8} :catchall_3e6

    #@3e8
    throw v5

    #@3e9
    .line 683
    .restart local v20       #params:Lcom/android/server/BackupManagerService$FullParams;
    :cond_3e9
    :try_start_3e9
    const-string v5, "BackupManagerService"

    #@3eb
    new-instance v7, Ljava/lang/StringBuilder;

    #@3ed
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3f0
    const-string v8, "couldn\'t find params for token "

    #@3f2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f5
    move-result-object v7

    #@3f6
    move-object/from16 v0, p1

    #@3f8
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@3fa
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3fd
    move-result-object v7

    #@3fe
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@401
    move-result-object v7

    #@402
    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_405
    .catchall {:try_start_3e9 .. :try_end_405} :catchall_3e3

    #@405
    goto :goto_3e0

    #@406
    .line 678
    :catch_406
    move-exception v5

    #@407
    goto :goto_3e0

    #@408
    .line 468
    :pswitch_data_408
    .packed-switch 0x1
        :pswitch_8
        :pswitch_14d
        :pswitch_190
        :pswitch_219
        :pswitch_237
        :pswitch_268
        :pswitch_35a
        :pswitch_36b
        :pswitch_39d
        :pswitch_1ea
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_f9
        :pswitch_123
    .end packed-switch
.end method
