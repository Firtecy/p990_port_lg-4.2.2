.class Lcom/android/server/BluetoothManagerService$2;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BluetoothManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BluetoothManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/BluetoothManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 157
    iput-object p1, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 160
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 161
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.LOCAL_NAME_CHANGED"

    #@7
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_34

    #@d
    .line 162
    const-string v2, "android.bluetooth.adapter.extra.LOCAL_NAME"

    #@f
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 164
    .local v1, newName:Ljava/lang/String;
    const-string v2, "BluetoothManagerService"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "Bluetooth Adapter name changed to "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 166
    if-eqz v1, :cond_33

    #@2d
    .line 167
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@2f
    const/4 v3, 0x0

    #@30
    invoke-static {v2, v1, v3}, Lcom/android/server/BluetoothManagerService;->access$300(Lcom/android/server/BluetoothManagerService;Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 213
    .end local v1           #newName:Ljava/lang/String;
    :cond_33
    :goto_33
    return-void

    #@34
    .line 169
    :cond_34
    const-string v2, "android.intent.action.AIRPLANE_MODE"

    #@36
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_86

    #@3c
    .line 170
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@3e
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$400(Lcom/android/server/BluetoothManagerService;)Landroid/content/BroadcastReceiver;

    #@41
    move-result-object v3

    #@42
    monitor-enter v3

    #@43
    .line 171
    :try_start_43
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@45
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$500(Lcom/android/server/BluetoothManagerService;)Z

    #@48
    move-result v2

    #@49
    if-eqz v2, :cond_59

    #@4b
    .line 172
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@4d
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$600(Lcom/android/server/BluetoothManagerService;)Z

    #@50
    move-result v2

    #@51
    if-eqz v2, :cond_6b

    #@53
    .line 173
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@55
    const/4 v4, 0x2

    #@56
    invoke-static {v2, v4}, Lcom/android/server/BluetoothManagerService;->access$700(Lcom/android/server/BluetoothManagerService;I)V

    #@59
    .line 178
    :cond_59
    :goto_59
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@5b
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$600(Lcom/android/server/BluetoothManagerService;)Z

    #@5e
    move-result v2

    #@5f
    if-eqz v2, :cond_72

    #@61
    .line 180
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@63
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$800(Lcom/android/server/BluetoothManagerService;)V

    #@66
    .line 185
    :cond_66
    :goto_66
    monitor-exit v3

    #@67
    goto :goto_33

    #@68
    :catchall_68
    move-exception v2

    #@69
    monitor-exit v3
    :try_end_6a
    .catchall {:try_start_43 .. :try_end_6a} :catchall_68

    #@6a
    throw v2

    #@6b
    .line 175
    :cond_6b
    :try_start_6b
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@6d
    const/4 v4, 0x1

    #@6e
    invoke-static {v2, v4}, Lcom/android/server/BluetoothManagerService;->access$700(Lcom/android/server/BluetoothManagerService;I)V

    #@71
    goto :goto_59

    #@72
    .line 181
    :cond_72
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@74
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$900(Lcom/android/server/BluetoothManagerService;)Z

    #@77
    move-result v2

    #@78
    if-eqz v2, :cond_66

    #@7a
    .line 183
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7c
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@7e
    invoke-static {v4}, Lcom/android/server/BluetoothManagerService;->access$1000(Lcom/android/server/BluetoothManagerService;)Z

    #@81
    move-result v4

    #@82
    invoke-static {v2, v4}, Lcom/android/server/BluetoothManagerService;->access$1100(Lcom/android/server/BluetoothManagerService;Z)V
    :try_end_85
    .catchall {:try_start_6b .. :try_end_85} :catchall_68

    #@85
    goto :goto_66

    #@86
    .line 186
    :cond_86
    const-string v2, "android.intent.action.USER_SWITCHED"

    #@88
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8b
    move-result v2

    #@8c
    if-eqz v2, :cond_aa

    #@8e
    .line 187
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@90
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@93
    move-result-object v2

    #@94
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@96
    invoke-static {v3}, Lcom/android/server/BluetoothManagerService;->access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@99
    move-result-object v3

    #@9a
    const/16 v4, 0x12c

    #@9c
    const-string v5, "android.intent.extra.user_handle"

    #@9e
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@a1
    move-result v5

    #@a2
    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(III)Landroid/os/Message;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {v2, v3}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@a9
    goto :goto_33

    #@aa
    .line 189
    :cond_aa
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    #@ac
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@af
    move-result v2

    #@b0
    if-eqz v2, :cond_33

    #@b2
    .line 190
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@b4
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$400(Lcom/android/server/BluetoothManagerService;)Landroid/content/BroadcastReceiver;

    #@b7
    move-result-object v3

    #@b8
    monitor-enter v3

    #@b9
    .line 195
    :try_start_b9
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@bb
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$900(Lcom/android/server/BluetoothManagerService;)Z

    #@be
    move-result v2

    #@bf
    if-eqz v2, :cond_e3

    #@c1
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@c3
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$1200(Lcom/android/server/BluetoothManagerService;)Z

    #@c6
    move-result v2

    #@c7
    if-eqz v2, :cond_e3

    #@c9
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@cb
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$600(Lcom/android/server/BluetoothManagerService;)Z

    #@ce
    move-result v2

    #@cf
    if-nez v2, :cond_e3

    #@d1
    .line 199
    const-string v2, "BluetoothManagerService"

    #@d3
    const-string v4, "Auto-enabling Bluetooth."

    #@d5
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 201
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@da
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@dc
    invoke-static {v4}, Lcom/android/server/BluetoothManagerService;->access$1000(Lcom/android/server/BluetoothManagerService;)Z

    #@df
    move-result v4

    #@e0
    invoke-static {v2, v4}, Lcom/android/server/BluetoothManagerService;->access$1100(Lcom/android/server/BluetoothManagerService;Z)V

    #@e3
    .line 203
    :cond_e3
    monitor-exit v3
    :try_end_e4
    .catchall {:try_start_b9 .. :try_end_e4} :catchall_fa

    #@e4
    .line 205
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@e6
    invoke-static {v2}, Lcom/android/server/BluetoothManagerService;->access$1300(Lcom/android/server/BluetoothManagerService;)Z

    #@e9
    move-result v2

    #@ea
    if-nez v2, :cond_33

    #@ec
    .line 208
    const-string v2, "BluetoothManagerService"

    #@ee
    const-string v3, "Retrieving Bluetooth Adapter name and address..."

    #@f0
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    .line 210
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService$2;->this$0:Lcom/android/server/BluetoothManagerService;

    #@f5
    invoke-virtual {v2}, Lcom/android/server/BluetoothManagerService;->getNameAndAddress()V

    #@f8
    goto/16 :goto_33

    #@fa
    .line 203
    :catchall_fa
    move-exception v2

    #@fb
    :try_start_fb
    monitor-exit v3
    :try_end_fc
    .catchall {:try_start_fb .. :try_end_fc} :catchall_fa

    #@fc
    throw v2
.end method
