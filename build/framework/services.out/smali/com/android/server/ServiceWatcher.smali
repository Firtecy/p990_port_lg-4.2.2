.class public Lcom/android/server/ServiceWatcher;
.super Ljava/lang/Object;
.source "ServiceWatcher.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final D:Z = false

.field public static final EXTRA_SERVICE_VERSION:Ljava/lang/String; = "serviceVersion"


# instance fields
.field private final mAction:Ljava/lang/String;

.field private mBinder:Landroid/os/IBinder;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mHandler:Landroid/os/Handler;

.field private mLock:Ljava/lang/Object;

.field private final mNewServiceWork:Ljava/lang/Runnable;

.field private final mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

.field private mPackageName:Ljava/lang/String;

.field private final mPm:Landroid/content/pm/PackageManager;

.field private final mSignatureSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Landroid/content/pm/Signature;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;

.field private mVersion:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Runnable;Landroid/os/Handler;I)V
    .registers 9
    .parameter "context"
    .parameter "logTag"
    .parameter "action"
    .parameter
    .parameter "newServiceWork"
    .parameter "handler"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Runnable;",
            "Landroid/os/Handler;",
            "I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 83
    .local p4, initialPackageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@a
    .line 61
    const/high16 v0, -0x8000

    #@c
    iput v0, p0, Lcom/android/server/ServiceWatcher;->mVersion:I

    #@e
    .line 206
    new-instance v0, Lcom/android/server/ServiceWatcher$1;

    #@10
    invoke-direct {v0, p0}, Lcom/android/server/ServiceWatcher$1;-><init>(Lcom/android/server/ServiceWatcher;)V

    #@13
    iput-object v0, p0, Lcom/android/server/ServiceWatcher;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@15
    .line 84
    iput-object p1, p0, Lcom/android/server/ServiceWatcher;->mContext:Landroid/content/Context;

    #@17
    .line 85
    iput-object p2, p0, Lcom/android/server/ServiceWatcher;->mTag:Ljava/lang/String;

    #@19
    .line 86
    iput-object p3, p0, Lcom/android/server/ServiceWatcher;->mAction:Ljava/lang/String;

    #@1b
    .line 87
    iget-object v0, p0, Lcom/android/server/ServiceWatcher;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@20
    move-result-object v0

    #@21
    iput-object v0, p0, Lcom/android/server/ServiceWatcher;->mPm:Landroid/content/pm/PackageManager;

    #@23
    .line 88
    iput-object p5, p0, Lcom/android/server/ServiceWatcher;->mNewServiceWork:Ljava/lang/Runnable;

    #@25
    .line 89
    iput-object p6, p0, Lcom/android/server/ServiceWatcher;->mHandler:Landroid/os/Handler;

    #@27
    .line 90
    iput p7, p0, Lcom/android/server/ServiceWatcher;->mCurrentUserId:I

    #@29
    .line 92
    invoke-static {p1, p4}, Lcom/android/server/ServiceWatcher;->getSignatureSets(Landroid/content/Context;Ljava/util/List;)Ljava/util/ArrayList;

    #@2c
    move-result-object v0

    #@2d
    iput-object v0, p0, Lcom/android/server/ServiceWatcher;->mSignatureSets:Ljava/util/List;

    #@2f
    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/ServiceWatcher;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/ServiceWatcher;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/server/ServiceWatcher;->mPackageName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/ServiceWatcher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Lcom/android/server/ServiceWatcher;->unbindLocked()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/ServiceWatcher;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/server/ServiceWatcher;->bindBestPackageLocked(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private bindBestPackageLocked(Ljava/lang/String;)Z
    .registers 16
    .parameter "justCheckThisPackage"

    #@0
    .prologue
    .line 112
    new-instance v4, Landroid/content/Intent;

    #@2
    iget-object v10, p0, Lcom/android/server/ServiceWatcher;->mAction:Ljava/lang/String;

    #@4
    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 113
    .local v4, intent:Landroid/content/Intent;
    if-eqz p1, :cond_c

    #@9
    .line 114
    invoke-virtual {v4, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 116
    :cond_c
    iget-object v10, p0, Lcom/android/server/ServiceWatcher;->mPm:Landroid/content/pm/PackageManager;

    #@e
    new-instance v11, Landroid/content/Intent;

    #@10
    iget-object v12, p0, Lcom/android/server/ServiceWatcher;->mAction:Ljava/lang/String;

    #@12
    invoke-direct {v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15
    const/16 v12, 0x80

    #@17
    iget v13, p0, Lcom/android/server/ServiceWatcher;->mCurrentUserId:I

    #@19
    invoke-virtual {v10, v11, v12, v13}, Landroid/content/pm/PackageManager;->queryIntentServicesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@1c
    move-result-object v8

    #@1d
    .line 118
    .local v8, rInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/high16 v1, -0x8000

    #@1f
    .line 119
    .local v1, bestVersion:I
    const/4 v0, 0x0

    #@20
    .line 120
    .local v0, bestPackage:Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v3

    #@24
    .local v3, i$:Ljava/util/Iterator;
    :cond_24
    :goto_24
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@27
    move-result v10

    #@28
    if-eqz v10, :cond_89

    #@2a
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2d
    move-result-object v7

    #@2e
    check-cast v7, Landroid/content/pm/ResolveInfo;

    #@30
    .line 121
    .local v7, rInfo:Landroid/content/pm/ResolveInfo;
    iget-object v10, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@32
    iget-object v6, v10, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@34
    .line 126
    .local v6, packageName:Ljava/lang/String;
    :try_start_34
    iget-object v10, p0, Lcom/android/server/ServiceWatcher;->mPm:Landroid/content/pm/PackageManager;

    #@36
    const/16 v11, 0x40

    #@38
    invoke-virtual {v10, v6, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@3b
    move-result-object v5

    #@3c
    .line 127
    .local v5, pInfo:Landroid/content/pm/PackageInfo;
    iget-object v10, v5, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@3e
    invoke-direct {p0, v10}, Lcom/android/server/ServiceWatcher;->isSignatureMatch([Landroid/content/pm/Signature;)Z

    #@41
    move-result v10

    #@42
    if-nez v10, :cond_70

    #@44
    .line 128
    iget-object v10, p0, Lcom/android/server/ServiceWatcher;->mTag:Ljava/lang/String;

    #@46
    new-instance v11, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v11

    #@4f
    const-string v12, " resolves service "

    #@51
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v11

    #@55
    iget-object v12, p0, Lcom/android/server/ServiceWatcher;->mAction:Ljava/lang/String;

    #@57
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v11

    #@5b
    const-string v12, ", but has wrong signature, ignoring"

    #@5d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v11

    #@61
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v11

    #@65
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_68
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_34 .. :try_end_68} :catch_69

    #@68
    goto :goto_24

    #@69
    .line 132
    .end local v5           #pInfo:Landroid/content/pm/PackageInfo;
    :catch_69
    move-exception v2

    #@6a
    .line 133
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v10, p0, Lcom/android/server/ServiceWatcher;->mTag:Ljava/lang/String;

    #@6c
    invoke-static {v10, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6f
    goto :goto_24

    #@70
    .line 138
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5       #pInfo:Landroid/content/pm/PackageInfo;
    :cond_70
    const/4 v9, 0x0

    #@71
    .line 139
    .local v9, version:I
    iget-object v10, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@73
    iget-object v10, v10, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    #@75
    if-eqz v10, :cond_82

    #@77
    .line 140
    iget-object v10, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@79
    iget-object v10, v10, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    #@7b
    const-string v11, "serviceVersion"

    #@7d
    const/4 v12, 0x0

    #@7e
    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@81
    move-result v9

    #@82
    .line 143
    :cond_82
    iget v10, p0, Lcom/android/server/ServiceWatcher;->mVersion:I

    #@84
    if-le v9, v10, :cond_24

    #@86
    .line 144
    move v1, v9

    #@87
    .line 145
    move-object v0, v6

    #@88
    goto :goto_24

    #@89
    .line 154
    .end local v5           #pInfo:Landroid/content/pm/PackageInfo;
    .end local v6           #packageName:Ljava/lang/String;
    .end local v7           #rInfo:Landroid/content/pm/ResolveInfo;
    .end local v9           #version:I
    :cond_89
    if-eqz v0, :cond_90

    #@8b
    .line 155
    invoke-direct {p0, v0, v1}, Lcom/android/server/ServiceWatcher;->bindToPackageLocked(Ljava/lang/String;I)V

    #@8e
    .line 156
    const/4 v10, 0x1

    #@8f
    .line 158
    :goto_8f
    return v10

    #@90
    :cond_90
    const/4 v10, 0x0

    #@91
    goto :goto_8f
.end method

.method private bindToPackageLocked(Ljava/lang/String;I)V
    .registers 7
    .parameter "packageName"
    .parameter "version"

    #@0
    .prologue
    .line 173
    invoke-direct {p0}, Lcom/android/server/ServiceWatcher;->unbindLocked()V

    #@3
    .line 174
    new-instance v0, Landroid/content/Intent;

    #@5
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mAction:Ljava/lang/String;

    #@7
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a
    .line 175
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@d
    .line 176
    iput-object p1, p0, Lcom/android/server/ServiceWatcher;->mPackageName:Ljava/lang/String;

    #@f
    .line 177
    iput p2, p0, Lcom/android/server/ServiceWatcher;->mVersion:I

    #@11
    .line 179
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mContext:Landroid/content/Context;

    #@13
    const v2, 0x40000005

    #@16
    iget v3, p0, Lcom/android/server/ServiceWatcher;->mCurrentUserId:I

    #@18
    invoke-virtual {v1, v0, p0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@1b
    .line 181
    return-void
.end method

.method public static getSignatureSets(Landroid/content/Context;Ljava/util/List;)Ljava/util/ArrayList;
    .registers 13
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Landroid/content/pm/Signature;",
            ">;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 66
    .local p1, initialPackageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3
    move-result-object v3

    #@4
    .line 67
    .local v3, pm:Landroid/content/pm/PackageManager;
    new-instance v5, Ljava/util/ArrayList;

    #@6
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 68
    .local v5, sigSets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashSet<Landroid/content/pm/Signature;>;>;"
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@d
    move-result v7

    #@e
    .local v7, size:I
    :goto_e
    if-ge v1, v7, :cond_4a

    #@10
    .line 69
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Ljava/lang/String;

    #@16
    .line 71
    .local v2, pkg:Ljava/lang/String;
    :try_start_16
    new-instance v4, Ljava/util/HashSet;

    #@18
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    #@1b
    .line 72
    .local v4, set:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/content/pm/Signature;>;"
    const/16 v8, 0x40

    #@1d
    invoke-virtual {v3, v2, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@20
    move-result-object v8

    #@21
    iget-object v6, v8, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@23
    .line 73
    .local v6, sigs:[Landroid/content/pm/Signature;
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@26
    move-result-object v8

    #@27
    invoke-virtual {v4, v8}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@2a
    .line 74
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_16 .. :try_end_2d} :catch_30

    #@2d
    .line 68
    .end local v4           #set:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/content/pm/Signature;>;"
    .end local v6           #sigs:[Landroid/content/pm/Signature;
    :goto_2d
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_e

    #@30
    .line 75
    :catch_30
    move-exception v0

    #@31
    .line 76
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "ServiceWatcher"

    #@33
    new-instance v9, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v9

    #@3c
    const-string v10, " not found"

    #@3e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v9

    #@42
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v9

    #@46
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_2d

    #@4a
    .line 79
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v2           #pkg:Ljava/lang/String;
    :cond_4a
    return-object v5
.end method

.method private isSignatureMatch([Landroid/content/pm/Signature;)Z
    .registers 3
    .parameter "signatures"

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Lcom/android/server/ServiceWatcher;->mSignatureSets:Ljava/util/List;

    #@2
    invoke-static {p1, v0}, Lcom/android/server/ServiceWatcher;->isSignatureMatch([Landroid/content/pm/Signature;Ljava/util/List;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isSignatureMatch([Landroid/content/pm/Signature;Ljava/util/List;)Z
    .registers 10
    .parameter "signatures"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/content/pm/Signature;",
            "Ljava/util/List",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Landroid/content/pm/Signature;",
            ">;>;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, sigSets:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashSet<Landroid/content/pm/Signature;>;>;"
    const/4 v6, 0x0

    #@1
    .line 185
    if-nez p0, :cond_4

    #@3
    .line 199
    :cond_3
    :goto_3
    return v6

    #@4
    .line 188
    :cond_4
    new-instance v2, Ljava/util/HashSet;

    #@6
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@9
    .line 189
    .local v2, inputSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/content/pm/Signature;>;"
    move-object v0, p0

    #@a
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v3, v0

    #@b
    .local v3, len$:I
    const/4 v1, 0x0

    #@c
    .local v1, i$:I
    :goto_c
    if-ge v1, v3, :cond_16

    #@e
    aget-object v5, v0, v1

    #@10
    .line 190
    .local v5, s:Landroid/content/pm/Signature;
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@13
    .line 189
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_c

    #@16
    .line 194
    .end local v5           #s:Landroid/content/pm/Signature;
    :cond_16
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v1

    #@1a
    .local v1, i$:Ljava/util/Iterator;
    :cond_1a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v7

    #@1e
    if-eqz v7, :cond_3

    #@20
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v4

    #@24
    check-cast v4, Ljava/util/HashSet;

    #@26
    .line 195
    .local v4, referenceSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/content/pm/Signature;>;"
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v7

    #@2a
    if-eqz v7, :cond_1a

    #@2c
    .line 196
    const/4 v6, 0x1

    #@2d
    goto :goto_3
.end method

.method private unbindLocked()V
    .registers 3

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Lcom/android/server/ServiceWatcher;->mPackageName:Ljava/lang/String;

    #@2
    .line 164
    .local v0, pkg:Ljava/lang/String;
    const/4 v1, 0x0

    #@3
    iput-object v1, p0, Lcom/android/server/ServiceWatcher;->mPackageName:Ljava/lang/String;

    #@5
    .line 165
    const/high16 v1, -0x8000

    #@7
    iput v1, p0, Lcom/android/server/ServiceWatcher;->mVersion:I

    #@9
    .line 166
    if-eqz v0, :cond_10

    #@b
    .line 168
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@10
    .line 170
    :cond_10
    return-void
.end method


# virtual methods
.method public getBestPackageName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 276
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 277
    :try_start_3
    iget-object v0, p0, Lcom/android/server/ServiceWatcher;->mPackageName:Ljava/lang/String;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 278
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getBestVersion()I
    .registers 3

    #@0
    .prologue
    .line 282
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 283
    :try_start_3
    iget v0, p0, Lcom/android/server/ServiceWatcher;->mVersion:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 284
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getBinder()Landroid/os/IBinder;
    .registers 3

    #@0
    .prologue
    .line 288
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 289
    :try_start_3
    iget-object v0, p0, Lcom/android/server/ServiceWatcher;->mBinder:Landroid/os/IBinder;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 290
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 8
    .parameter "name"
    .parameter "binder"

    #@0
    .prologue
    .line 249
    iget-object v2, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 250
    :try_start_3
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 251
    .local v0, packageName:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mPackageName:Ljava/lang/String;

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_22

    #@f
    .line 253
    iput-object p2, p0, Lcom/android/server/ServiceWatcher;->mBinder:Landroid/os/IBinder;

    #@11
    .line 254
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mHandler:Landroid/os/Handler;

    #@13
    if-eqz v1, :cond_20

    #@15
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mNewServiceWork:Ljava/lang/Runnable;

    #@17
    if-eqz v1, :cond_20

    #@19
    .line 255
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mHandler:Landroid/os/Handler;

    #@1b
    iget-object v3, p0, Lcom/android/server/ServiceWatcher;->mNewServiceWork:Ljava/lang/Runnable;

    #@1d
    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@20
    .line 260
    :cond_20
    :goto_20
    monitor-exit v2

    #@21
    .line 261
    return-void

    #@22
    .line 258
    :cond_22
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mTag:Ljava/lang/String;

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "unexpected onServiceConnected: "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_20

    #@3b
    .line 260
    .end local v0           #packageName:Ljava/lang/String;
    :catchall_3b
    move-exception v1

    #@3c
    monitor-exit v2
    :try_end_3d
    .catchall {:try_start_3 .. :try_end_3d} :catchall_3b

    #@3d
    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 265
    iget-object v2, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 266
    :try_start_3
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 269
    .local v0, packageName:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mPackageName:Ljava/lang/String;

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_12

    #@f
    .line 270
    const/4 v1, 0x0

    #@10
    iput-object v1, p0, Lcom/android/server/ServiceWatcher;->mBinder:Landroid/os/IBinder;

    #@12
    .line 272
    :cond_12
    monitor-exit v2

    #@13
    .line 273
    return-void

    #@14
    .line 272
    .end local v0           #packageName:Ljava/lang/String;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method public start()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 96
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v1

    #@5
    .line 97
    const/4 v2, 0x0

    #@6
    :try_start_6
    invoke-direct {p0, v2}, Lcom/android/server/ServiceWatcher;->bindBestPackageLocked(Ljava/lang/String;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_f

    #@c
    const/4 v0, 0x0

    #@d
    monitor-exit v1

    #@e
    .line 101
    :goto_e
    return v0

    #@f
    .line 98
    :cond_f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_1a

    #@10
    .line 100
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@12
    iget-object v2, p0, Lcom/android/server/ServiceWatcher;->mContext:Landroid/content/Context;

    #@14
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@16
    invoke-virtual {v1, v2, v4, v3, v0}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    #@19
    goto :goto_e

    #@1a
    .line 98
    :catchall_1a
    move-exception v0

    #@1b
    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public switchUser(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 294
    iget-object v1, p0, Lcom/android/server/ServiceWatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 295
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/ServiceWatcher;->unbindLocked()V

    #@6
    .line 296
    iput p1, p0, Lcom/android/server/ServiceWatcher;->mCurrentUserId:I

    #@8
    .line 297
    const/4 v0, 0x0

    #@9
    invoke-direct {p0, v0}, Lcom/android/server/ServiceWatcher;->bindBestPackageLocked(Ljava/lang/String;)Z

    #@c
    .line 298
    monitor-exit v1

    #@d
    .line 299
    return-void

    #@e
    .line 298
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method
