.class public final Lcom/android/server/DeviceManager3LMService;
.super Landroid/os/IDeviceManager3LM$Stub;
.source "DeviceManager3LMService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/DeviceManager3LMService$ClearUserDataObserver;,
        Lcom/android/server/DeviceManager3LMService$PublicKey3LM;,
        Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;,
        Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;,
        Lcom/android/server/DeviceManager3LMService$RulesEngine;
    }
.end annotation


# static fields
.field private static final API_VERSION:I = 0xc

.field private static final DNS1:Ljava/lang/String; = "net.dns1"

.field private static final DNS2:Ljava/lang/String; = "net.dns2"

.field private static final DNS_DOMAIN_SUFFICES:Ljava/lang/String; = "net.dns.search"

.field private static final PROPERTY_NFC_LOCKOUT:Ljava/lang/String; = "persist.security.nfc.lockout"

.field private static final PROPERTY_WIFI_LOCKOUT:Ljava/lang/String; = "persist.security.wifi.lockout"

.field private static final TAG:Ljava/lang/String; = "DeviceManager3LM"

.field private static final TUN_DNS1:Ljava/lang/String; = "vpn.net.tun.dns1"

.field private static final TUN_DNS2:Ljava/lang/String; = "vpn.net.tun.dns2"

.field static sService:Lcom/android/server/DeviceManager3LMService;


# instance fields
.field private final DEBUG:Z

.field public final MAX_WAIT_TIME:J

.field private final SCAN_3LM_RESULT_ALLOW:I

.field private final SCAN_3LM_RESULT_DENY:I

.field public final STATE_DISABLED:I

.field public final STATE_ENABLED:I

.field public final STATE_UNSUPPORTED:I

.field public final WAIT_TIME_INCR:J

.field private mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

.field private mAndroidIds:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBluetoothEnabled:Z

.field private mBluetoothPan:Landroid/bluetooth/BluetoothPan;

.field private mBootLocked:Z

.field private mConnector:Lcom/android/server/NativeDaemonConnector;

.field mContext:Landroid/content/Context;

.field private mInitialized:Z

.field private mKeyStore:Landroid/security/KeyStore;

.field private mMultiUserEnabled:Z

.field private mNetworkManager:Landroid/os/INetworkManagementService;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mNotificationText:Ljava/lang/String;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPackagePerms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/DeviceManager3LMService$RulesEngine;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageScanFailOnTimeout:Z

.field private mPackageScanTimeoutMillis:I

.field private mPackageScanner:Ljava/lang/String;

.field private mPackageScannerMutex:Ljava/lang/Object;

.field private mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

.field private mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

.field private mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mPubKeyRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

.field private mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

.field private mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

.field private mSecurePrimaryClip:Z

.field private mUid:I

.field private mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

.field private mUseCustomNotification:Z

.field private mVpnHasOriginalData:Z

.field private mVpnOriginalDns1:Ljava/lang/String;

.field private mVpnOriginalDns2:Ljava/lang/String;

.field private mVpnOriginalDnsSuffixes:Ljava/lang/String;

.field private mVpnSettingsMutex:Ljava/lang/String;

.field private mWifiFilter:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 158
    invoke-direct {p0}, Landroid/os/IDeviceManager3LM$Stub;-><init>()V

    #@5
    .line 76
    iput-boolean v2, p0, Lcom/android/server/DeviceManager3LMService;->DEBUG:Z

    #@7
    .line 78
    const-wide/16 v0, 0x61a8

    #@9
    iput-wide v0, p0, Lcom/android/server/DeviceManager3LMService;->MAX_WAIT_TIME:J

    #@b
    .line 79
    const-wide/16 v0, 0x1388

    #@d
    iput-wide v0, p0, Lcom/android/server/DeviceManager3LMService;->WAIT_TIME_INCR:J

    #@f
    .line 81
    const/4 v0, -0x1

    #@10
    iput v0, p0, Lcom/android/server/DeviceManager3LMService;->STATE_UNSUPPORTED:I

    #@12
    .line 82
    iput v2, p0, Lcom/android/server/DeviceManager3LMService;->STATE_DISABLED:I

    #@14
    .line 83
    iput v3, p0, Lcom/android/server/DeviceManager3LMService;->STATE_ENABLED:I

    #@16
    .line 130
    const-string v0, ""

    #@18
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDnsSuffixes:Ljava/lang/String;

    #@1a
    .line 131
    const-string v0, ""

    #@1c
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns1:Ljava/lang/String;

    #@1e
    .line 132
    const-string v0, ""

    #@20
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns2:Ljava/lang/String;

    #@22
    .line 133
    iput-boolean v2, p0, Lcom/android/server/DeviceManager3LMService;->mVpnHasOriginalData:Z

    #@24
    .line 134
    const-string v0, ""

    #@26
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mVpnSettingsMutex:Ljava/lang/String;

    #@28
    .line 141
    new-instance v0, Ljava/lang/Object;

    #@2a
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@2d
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScannerMutex:Ljava/lang/Object;

    #@2f
    .line 142
    const/4 v0, 0x0

    #@30
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanner:Ljava/lang/String;

    #@32
    .line 143
    iput-boolean v2, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanFailOnTimeout:Z

    #@34
    .line 144
    const/16 v0, 0x2710

    #@36
    iput v0, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanTimeoutMillis:I

    #@38
    .line 1909
    const/16 v0, 0x3e8

    #@3a
    iput v0, p0, Lcom/android/server/DeviceManager3LMService;->SCAN_3LM_RESULT_ALLOW:I

    #@3c
    .line 1910
    const/16 v0, 0x3e9

    #@3e
    iput v0, p0, Lcom/android/server/DeviceManager3LMService;->SCAN_3LM_RESULT_DENY:I

    #@40
    .line 1947
    new-instance v0, Lcom/android/server/DeviceManager3LMService$3;

    #@42
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$3;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@45
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@47
    .line 159
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->initAndroidIds()V

    #@4a
    .line 160
    iput-boolean v2, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z

    #@4c
    .line 161
    iput-boolean v3, p0, Lcom/android/server/DeviceManager3LMService;->mBluetoothEnabled:Z

    #@4e
    .line 162
    iput-boolean v3, p0, Lcom/android/server/DeviceManager3LMService;->mMultiUserEnabled:Z

    #@50
    .line 163
    new-instance v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@52
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@55
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@57
    .line 164
    new-instance v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@59
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@5c
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@5e
    .line 165
    new-instance v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@60
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@63
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPubKeyRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@65
    .line 166
    new-instance v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@67
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@6a
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@6c
    .line 167
    new-instance v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@6e
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@71
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@73
    .line 168
    new-instance v0, Ljava/util/HashMap;

    #@75
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@78
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@7a
    .line 169
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@7d
    move-result-object v0

    #@7e
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@80
    .line 170
    new-instance v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@82
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@85
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@87
    .line 171
    iput-boolean v2, p0, Lcom/android/server/DeviceManager3LMService;->mSecurePrimaryClip:Z

    #@89
    .line 172
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/DeviceManager3LMService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mUseCustomNotification:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/server/DeviceManager3LMService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationText:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/server/DeviceManager3LMService;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationText:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/server/DeviceManager3LMService;)Landroid/app/NotificationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationManager:Landroid/app/NotificationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$702(Lcom/android/server/DeviceManager3LMService;Landroid/bluetooth/BluetoothPan;)Landroid/bluetooth/BluetoothPan;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@2
    return-object p1
.end method

.method private getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "pkgName"

    #@0
    .prologue
    .line 346
    :try_start_0
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2
    const/4 v3, 0x0

    #@3
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@6
    move-result-object v1

    #@7
    .line 347
    .local v1, pi:Landroid/content/pm/PackageInfo;
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@9
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b
    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_12
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_12} :catch_14

    #@12
    move-result-object p1

    #@13
    .line 349
    .end local v1           #pi:Landroid/content/pm/PackageInfo;
    .end local p1
    :goto_13
    return-object p1

    #@14
    .line 348
    .restart local p1
    :catch_14
    move-exception v0

    #@15
    .line 349
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_13
.end method

.method public static declared-synchronized getInstance()Lcom/android/server/DeviceManager3LMService;
    .registers 2

    #@0
    .prologue
    .line 152
    const-class v1, Lcom/android/server/DeviceManager3LMService;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/android/server/DeviceManager3LMService;->sService:Lcom/android/server/DeviceManager3LMService;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 153
    new-instance v0, Lcom/android/server/DeviceManager3LMService;

    #@9
    invoke-direct {v0}, Lcom/android/server/DeviceManager3LMService;-><init>()V

    #@c
    sput-object v0, Lcom/android/server/DeviceManager3LMService;->sService:Lcom/android/server/DeviceManager3LMService;

    #@e
    .line 155
    :cond_e
    sget-object v0, Lcom/android/server/DeviceManager3LMService;->sService:Lcom/android/server/DeviceManager3LMService;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 152
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1

    #@14
    throw v0
.end method

.method private initAndroidIds()V
    .registers 4

    #@0
    .prologue
    .line 229
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@7
    .line 230
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@9
    const/16 v1, 0x3ef

    #@b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v1

    #@f
    const-string v2, "android.permission.READ_LOGS"

    #@11
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 231
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@16
    const/16 v1, 0x3f7

    #@18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    #@1e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    .line 232
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@23
    const/16 v1, 0xbb9

    #@25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v1

    #@29
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@2b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    .line 233
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@30
    const/16 v1, 0xbba

    #@32
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35
    move-result-object v1

    #@36
    const-string v2, "android.permission.BLUETOOTH"

    #@38
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 234
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@3d
    const/16 v1, 0xbbb

    #@3f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@42
    move-result-object v1

    #@43
    const-string v2, "android.permission.INTERNET"

    #@45
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 235
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@4a
    const/16 v1, 0xbbc

    #@4c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f
    move-result-object v1

    #@50
    const-string v2, "android.permission.INTERNET"

    #@52
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    .line 236
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@57
    const/16 v1, 0xbbd

    #@59
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v1

    #@5d
    const-string v2, "android.permission.INTERNET"

    #@5f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@62
    .line 237
    return-void
.end method

.method private isAccessPermitted()Z
    .registers 16

    #@0
    .prologue
    .line 1012
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v2

    #@4
    .line 1013
    .local v2, callerUid:I
    iget v12, p0, Lcom/android/server/DeviceManager3LMService;->mUid:I

    #@6
    if-ne v2, v12, :cond_d

    #@8
    .line 1014
    const/4 v12, 0x1

    #@9
    iput-boolean v12, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z

    #@b
    .line 1017
    const/4 v12, 0x1

    #@c
    .line 1049
    :goto_c
    return v12

    #@d
    .line 1021
    :cond_d
    iget-object v12, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@f
    if-nez v12, :cond_13

    #@11
    .line 1022
    const/4 v12, 0x0

    #@12
    goto :goto_c

    #@13
    .line 1027
    :cond_13
    :try_start_13
    iget-object v12, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@15
    invoke-virtual {v12, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@18
    move-result-object v9

    #@19
    .line 1029
    .local v9, packages:[Ljava/lang/String;
    move-object v0, v9

    #@1a
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@1b
    .local v6, len$:I
    const/4 v4, 0x0

    #@1c
    .local v4, i$:I
    move v5, v4

    #@1d
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v6           #len$:I
    .local v5, i$:I
    :goto_1d
    if-ge v5, v6, :cond_62

    #@1f
    aget-object v10, v0, v5

    #@21
    .line 1032
    .local v10, pkg:Ljava/lang/String;
    iget-object v12, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@23
    const/16 v13, 0x40

    #@25
    invoke-virtual {v12, v10, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@28
    move-result-object v8

    #@29
    .line 1036
    .local v8, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v1, v8, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@2b
    .local v1, arr$:[Landroid/content/pm/Signature;
    array-length v7, v1

    #@2c
    .local v7, len$:I
    const/4 v4, 0x0

    #@2d
    .end local v5           #i$:I
    .restart local v4       #i$:I
    :goto_2d
    if-ge v4, v7, :cond_45

    #@2f
    aget-object v11, v1, v4

    #@31
    .line 1037
    .local v11, pkgSignature:Landroid/content/pm/Signature;
    iget-object v12, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@33
    invoke-virtual {v11}, Landroid/content/pm/Signature;->toByteArray()[B

    #@36
    move-result-object v13

    #@37
    invoke-virtual {v12, v13}, Lcom/android/server/DeviceManager3LMService$PublicKey3LM;->comparePublicKey([B)Z

    #@3a
    move-result v12

    #@3b
    if-eqz v12, :cond_42

    #@3d
    .line 1040
    const/4 v12, 0x1

    #@3e
    iput-boolean v12, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_40} :catch_49

    #@40
    .line 1041
    const/4 v12, 0x1

    #@41
    goto :goto_c

    #@42
    .line 1036
    :cond_42
    add-int/lit8 v4, v4, 0x1

    #@44
    goto :goto_2d

    #@45
    .line 1029
    .end local v11           #pkgSignature:Landroid/content/pm/Signature;
    :cond_45
    add-int/lit8 v4, v5, 0x1

    #@47
    move v5, v4

    #@48
    .end local v4           #i$:I
    .restart local v5       #i$:I
    goto :goto_1d

    #@49
    .line 1045
    .end local v1           #arr$:[Landroid/content/pm/Signature;
    .end local v5           #i$:I
    .end local v7           #len$:I
    .end local v8           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v9           #packages:[Ljava/lang/String;
    .end local v10           #pkg:Ljava/lang/String;
    :catch_49
    move-exception v3

    #@4a
    .line 1046
    .local v3, e:Ljava/lang/Exception;
    const-string v12, "DeviceManager3LM"

    #@4c
    new-instance v13, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v14, "Error trying to verify package public key against 3LM pub key:"

    #@53
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v13

    #@57
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v13

    #@5b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v13

    #@5f
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 1048
    .end local v3           #e:Ljava/lang/Exception;
    :cond_62
    const-string v12, "DeviceManager3LM"

    #@64
    new-instance v13, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v14, "Access denied to UID:"

    #@6b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v13

    #@6f
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v13

    #@73
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v13

    #@77
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 1049
    const/4 v12, 0x0

    #@7b
    goto :goto_c
.end method

.method private isBootLocked(Ljava/lang/String;)Z
    .registers 11
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 312
    iget-boolean v7, p0, Lcom/android/server/DeviceManager3LMService;->mBootLocked:Z

    #@4
    if-eqz v7, :cond_18

    #@6
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@8
    if-eqz v7, :cond_18

    #@a
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v7

    #@10
    const-string v8, "boot_lock"

    #@12
    invoke-static {v7, v8, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15
    move-result v7

    #@16
    if-nez v7, :cond_1a

    #@18
    :cond_18
    move v5, v6

    #@19
    .line 341
    :cond_19
    :goto_19
    return v5

    #@1a
    .line 319
    :cond_1a
    if-eqz p1, :cond_19

    #@1c
    .line 322
    :try_start_1c
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@1e
    const/16 v8, 0x40

    #@20
    invoke-virtual {v7, p1, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@23
    move-result-object v3

    #@24
    .line 324
    .local v3, pi:Landroid/content/pm/PackageInfo;
    iget-object v7, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@26
    iget v7, v7, Landroid/content/pm/ApplicationInfo;->flags:I

    #@28
    and-int/lit8 v7, v7, 0x1

    #@2a
    if-eqz v7, :cond_2e

    #@2c
    move v5, v6

    #@2d
    .line 325
    goto :goto_19

    #@2e
    .line 328
    :cond_2e
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@30
    if-eqz v7, :cond_19

    #@32
    .line 333
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@34
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v2, v0

    #@35
    .local v2, len$:I
    const/4 v1, 0x0

    #@36
    .local v1, i$:I
    :goto_36
    if-ge v1, v2, :cond_19

    #@38
    aget-object v4, v0, v1

    #@3a
    .line 334
    .local v4, pkgSignature:Landroid/content/pm/Signature;
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@3c
    invoke-virtual {v4}, Landroid/content/pm/Signature;->toByteArray()[B

    #@3f
    move-result-object v8

    #@40
    invoke-virtual {v7, v8}, Lcom/android/server/DeviceManager3LMService$PublicKey3LM;->comparePublicKey([B)Z
    :try_end_43
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1c .. :try_end_43} :catch_4b

    #@43
    move-result v7

    #@44
    if-eqz v7, :cond_48

    #@46
    move v5, v6

    #@47
    .line 335
    goto :goto_19

    #@48
    .line 333
    :cond_48
    add-int/lit8 v1, v1, 0x1

    #@4a
    goto :goto_36

    #@4b
    .line 338
    .end local v0           #arr$:[Landroid/content/pm/Signature;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #pi:Landroid/content/pm/PackageInfo;
    .end local v4           #pkgSignature:Landroid/content/pm/Signature;
    :catch_4b
    move-exception v6

    #@4c
    goto :goto_19
.end method

.method private notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .registers 13
    .parameter "id"
    .parameter "notificationBar"
    .parameter "title"
    .parameter "text"
    .parameter "intent"

    #@0
    .prologue
    .line 379
    const/16 v6, 0x10

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    move-object v5, p5

    #@8
    invoke-direct/range {v0 .. v6}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    #@b
    .line 380
    return-void
.end method

.method private notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V
    .registers 15
    .parameter "id"
    .parameter "notificationBar"
    .parameter "title"
    .parameter "text"
    .parameter "intent"
    .parameter "flags"

    #@0
    .prologue
    .line 386
    new-instance v0, Lcom/android/server/DeviceManager3LMService$2;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p2

    #@4
    move-object v3, p5

    #@5
    move v4, p6

    #@6
    move-object v5, p4

    #@7
    move-object v6, p3

    #@8
    move v7, p1

    #@9
    invoke-direct/range {v0 .. v7}, Lcom/android/server/DeviceManager3LMService$2;-><init>(Lcom/android/server/DeviceManager3LMService;Ljava/lang/String;Landroid/content/Intent;ILjava/lang/String;Ljava/lang/String;I)V

    #@c
    invoke-virtual {v0}, Lcom/android/server/DeviceManager3LMService$2;->start()V

    #@f
    .line 405
    return-void
.end method

.method private setPackageState(Ljava/lang/String;I)V
    .registers 11
    .parameter "pkgName"
    .parameter "state"

    #@0
    .prologue
    .line 667
    :try_start_0
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2
    const/16 v6, 0x40

    #@4
    invoke-virtual {v5, p1, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@7
    move-result-object v0

    #@8
    .line 669
    .local v0, app:Landroid/content/pm/PackageInfo;
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@a
    const-string v6, "android"

    #@c
    const/16 v7, 0x40

    #@e
    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@11
    move-result-object v4

    #@12
    .line 672
    .local v4, sys:Landroid/content/pm/PackageInfo;
    new-instance v3, Landroid/content/Intent;

    #@14
    const-string v5, "android.intent.action.MAIN"

    #@16
    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@19
    .line 673
    .local v3, intent:Landroid/content/Intent;
    const-string v5, "android.intent.category.HOME"

    #@1b
    invoke-virtual {v3, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@1e
    .line 674
    invoke-virtual {v3, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@21
    .line 675
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@23
    const/4 v6, 0x0

    #@24
    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@27
    move-result-object v2

    #@28
    .line 677
    .local v2, homes:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_30

    #@2a
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@2d
    move-result v5

    #@2e
    if-gtz v5, :cond_40

    #@30
    :cond_30
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@32
    const/4 v6, 0x0

    #@33
    aget-object v5, v5, v6

    #@35
    iget-object v6, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@37
    const/4 v7, 0x0

    #@38
    aget-object v6, v6, v7

    #@3a
    invoke-virtual {v5, v6}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_41

    #@40
    .line 687
    .end local v0           #app:Landroid/content/pm/PackageInfo;
    .end local v2           #homes:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #sys:Landroid/content/pm/PackageInfo;
    :cond_40
    :goto_40
    return-void

    #@41
    .line 682
    .restart local v0       #app:Landroid/content/pm/PackageInfo;
    .restart local v2       #homes:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v3       #intent:Landroid/content/Intent;
    .restart local v4       #sys:Landroid/content/pm/PackageInfo;
    :cond_41
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@43
    const/4 v6, -0x1

    #@44
    invoke-virtual {v5, p1, p2, v6}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_47
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_47} :catch_48

    #@47
    goto :goto_40

    #@48
    .line 684
    .end local v0           #app:Landroid/content/pm/PackageInfo;
    .end local v2           #homes:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #sys:Landroid/content/pm/PackageInfo;
    :catch_48
    move-exception v1

    #@49
    .line 685
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "DeviceManager3LM"

    #@4b
    new-instance v6, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v7, "Package not found, unable to disable: "

    #@52
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v6

    #@5e
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    goto :goto_40
.end method

.method private updateNetworkRules(Lcom/android/server/DeviceManager3LMService$RulesEngine;)V
    .registers 11
    .parameter "re"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1737
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@4
    invoke-virtual {v4, v6}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    #@7
    move-result-object v1

    #@8
    .line 1739
    .local v1, apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v3

    #@c
    .local v3, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_30

    #@12
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/content/pm/ApplicationInfo;

    #@18
    .line 1740
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@1a
    invoke-virtual {p1, v4, v5}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@1d
    move-result v2

    #@1e
    .line 1742
    .local v2, check:Z
    :try_start_1e
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@21
    .line 1743
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@23
    iget v8, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@25
    if-nez v2, :cond_2e

    #@27
    move v4, v5

    #@28
    :goto_28
    invoke-interface {v7, v8, v4}, Landroid/os/INetworkManagementService;->setUidNetworkRules(IZ)V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_c

    #@2c
    .line 1744
    :catch_2c
    move-exception v4

    #@2d
    goto :goto_c

    #@2e
    :cond_2e
    move v4, v6

    #@2f
    .line 1743
    goto :goto_28

    #@30
    .line 1748
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    .end local v2           #check:Z
    :cond_30
    return-void
.end method


# virtual methods
.method public addApn(Ljava/util/Map;)V
    .registers 11
    .parameter "list"

    #@0
    .prologue
    .line 2001
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v7

    #@4
    if-nez v7, :cond_7

    #@6
    .line 2033
    :goto_6
    return-void

    #@7
    .line 2003
    :cond_7
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c
    move-result-object v5

    #@d
    .line 2004
    .local v5, res:Landroid/content/res/Resources;
    const v7, 0x1070005

    #@10
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    .line 2005
    .local v2, key:[Ljava/lang/String;
    const v7, 0x1070006

    #@17
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    .line 2007
    .local v3, oem:[Ljava/lang/String;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1e
    .line 2008
    new-instance v6, Landroid/content/ContentValues;

    #@20
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    #@23
    .line 2009
    .local v6, values:Landroid/content/ContentValues;
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@26
    move-result-object v7

    #@27
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2a
    move-result-object v1

    #@2b
    .local v1, i$:Ljava/util/Iterator;
    :cond_2b
    :goto_2b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2e
    move-result v7

    #@2f
    if-eqz v7, :cond_8d

    #@31
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@34
    move-result-object v4

    #@35
    check-cast v4, Ljava/util/Map$Entry;

    #@37
    .line 2011
    .local v4, pair:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@38
    .local v0, i:I
    :goto_38
    array-length v7, v2

    #@39
    if-ge v0, v7, :cond_47

    #@3b
    .line 2012
    aget-object v7, v2, v0

    #@3d
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_7e

    #@47
    .line 2018
    :cond_47
    array-length v7, v2

    #@48
    if-eq v0, v7, :cond_2b

    #@4a
    .line 2021
    const-string v7, "authtype"

    #@4c
    aget-object v8, v2, v0

    #@4e
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v7

    #@52
    if-nez v7, :cond_5e

    #@54
    const-string v7, "bearer"

    #@56
    aget-object v8, v2, v0

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v7

    #@5c
    if-eqz v7, :cond_81

    #@5e
    .line 2023
    :cond_5e
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@61
    move-result-object v7

    #@62
    check-cast v7, Ljava/lang/String;

    #@64
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@67
    move-result v7

    #@68
    if-lez v7, :cond_2b

    #@6a
    .line 2024
    aget-object v8, v3, v0

    #@6c
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@6f
    move-result-object v7

    #@70
    check-cast v7, Ljava/lang/String;

    #@72
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@75
    move-result v7

    #@76
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {v6, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@7d
    goto :goto_2b

    #@7e
    .line 2011
    :cond_7e
    add-int/lit8 v0, v0, 0x1

    #@80
    goto :goto_38

    #@81
    .line 2027
    :cond_81
    aget-object v8, v3, v0

    #@83
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@86
    move-result-object v7

    #@87
    check-cast v7, Ljava/lang/String;

    #@89
    invoke-virtual {v6, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8c
    goto :goto_2b

    #@8d
    .line 2032
    .end local v0           #i:I
    .end local v4           #pair:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_8d
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@8f
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@92
    move-result-object v7

    #@93
    sget-object v8, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@95
    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@98
    goto/16 :goto_6
.end method

.method public blockAdb(Z)V
    .registers 6
    .parameter "block"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1588
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_8

    #@7
    .line 1596
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1590
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1591
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v2

    #@11
    const-string v3, "adb_blocked"

    #@13
    if-eqz p1, :cond_27

    #@15
    const/4 v0, 0x1

    #@16
    :goto_16
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@19
    .line 1593
    if-eqz p1, :cond_7

    #@1b
    .line 1594
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@20
    move-result-object v0

    #@21
    const-string v2, "adb_enabled"

    #@23
    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@26
    goto :goto_7

    #@27
    :cond_27
    move v0, v1

    #@28
    .line 1591
    goto :goto_16
.end method

.method public blockScreenshot(Z)V
    .registers 5
    .parameter "block"

    #@0
    .prologue
    .line 1967
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1972
    :goto_6
    return-void

    #@7
    .line 1969
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 1970
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string v2, "screenshot_blocked"

    #@12
    if-eqz p1, :cond_19

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@18
    goto :goto_6

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_15
.end method

.method public blockTethering(Z)V
    .registers 8
    .parameter "block"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1927
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 1945
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1929
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1930
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v4

    #@11
    const-string v5, "tethering_blocked"

    #@13
    if-eqz p1, :cond_40

    #@15
    const/4 v2, 0x1

    #@16
    :goto_16
    invoke-static {v4, v5, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@19
    .line 1933
    if-eqz p1, :cond_7

    #@1b
    .line 1934
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@1d
    const-string v4, "connectivity"

    #@1f
    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Landroid/net/ConnectivityManager;

    #@25
    .line 1936
    .local v0, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setUsbTethering(Z)I

    #@28
    .line 1938
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2a
    const-string v4, "wifi"

    #@2c
    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v1

    #@30
    check-cast v1, Landroid/net/wifi/WifiManager;

    #@32
    .line 1939
    .local v1, wm:Landroid/net/wifi/WifiManager;
    const/4 v2, 0x0

    #@33
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    #@36
    .line 1941
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@38
    if-eqz v2, :cond_7

    #@3a
    .line 1942
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@3c
    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothPan;->setBluetoothTethering(Z)V

    #@3f
    goto :goto_7

    #@40
    .end local v0           #cm:Landroid/net/ConnectivityManager;
    .end local v1           #wm:Landroid/net/wifi/WifiManager;
    :cond_40
    move v2, v3

    #@41
    .line 1930
    goto :goto_16
.end method

.method public blockUsb(Z)V
    .registers 5
    .parameter "block"

    #@0
    .prologue
    .line 1638
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1643
    :goto_6
    return-void

    #@7
    .line 1640
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 1641
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string v2, "usb_blocked"

    #@12
    if-eqz p1, :cond_19

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@18
    goto :goto_6

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_15
.end method

.method public checkAppInstallPolicies(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Z
    .registers 18
    .parameter "pkgName"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 899
    .local p2, requestedPermissions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p3, pkgSigs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 900
    const/4 v0, 0x1

    #@5
    .line 941
    :goto_5
    return v0

    #@6
    .line 904
    :cond_6
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Lcom/android/server/DeviceManager3LMService;->isBootLocked(Ljava/lang/String;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_4c

    #@d
    .line 905
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v13

    #@13
    .line 906
    .local v13, res:Landroid/content/res/Resources;
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const v1, 0x1040030

    #@1b
    invoke-virtual {v13, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, " "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    .line 908
    .local v2, title:Ljava/lang/String;
    const v0, 0x104002d

    #@38
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    .line 909
    .local v4, text:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    #@3f
    move-result v1

    #@40
    new-instance v5, Landroid/content/Intent;

    #@42
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@45
    move-object v0, p0

    #@46
    move-object v3, v2

    #@47
    invoke-direct/range {v0 .. v5}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@4a
    .line 910
    const/4 v0, 0x0

    #@4b
    goto :goto_5

    #@4c
    .line 913
    .end local v2           #title:Ljava/lang/String;
    .end local v4           #text:Ljava/lang/String;
    .end local v13           #res:Landroid/content/res/Resources;
    :cond_4c
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@4e
    monitor-enter v1

    #@4f
    .line 914
    :try_start_4f
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@51
    invoke-virtual {v0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->isInitialized()Z

    #@54
    move-result v0

    #@55
    if-eqz v0, :cond_b7

    #@57
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@59
    const/4 v3, 0x1

    #@5a
    invoke-virtual {v0, p1, v3}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@5d
    move-result v0

    #@5e
    if-nez v0, :cond_b7

    #@60
    .line 915
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@62
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@65
    move-result-object v13

    #@66
    .line 916
    .restart local v13       #res:Landroid/content/res/Resources;
    new-instance v0, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const v3, 0x1040030

    #@6e
    invoke-virtual {v13, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v0

    #@76
    const-string v3, " "

    #@78
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v0

    #@7c
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@7f
    move-result-object v3

    #@80
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    .line 918
    .restart local v2       #title:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    #@8b
    move-result v6

    #@8c
    const/4 v9, 0x0

    #@8d
    new-instance v10, Landroid/content/Intent;

    #@8f
    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    #@92
    move-object v5, p0

    #@93
    move-object v7, v2

    #@94
    move-object v8, v2

    #@95
    invoke-direct/range {v5 .. v10}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@98
    .line 919
    const-string v0, "DeviceManager3LM"

    #@9a
    new-instance v3, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v5, "Install blocked: "

    #@a1
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v3

    #@a9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v3

    #@ad
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 920
    const/4 v0, 0x0

    #@b1
    monitor-exit v1

    #@b2
    goto/16 :goto_5

    #@b4
    .line 922
    .end local v2           #title:Ljava/lang/String;
    .end local v13           #res:Landroid/content/res/Resources;
    :catchall_b4
    move-exception v0

    #@b5
    monitor-exit v1
    :try_end_b6
    .catchall {:try_start_4f .. :try_end_b6} :catchall_b4

    #@b6
    throw v0

    #@b7
    :cond_b7
    :try_start_b7
    monitor-exit v1
    :try_end_b8
    .catchall {:try_start_b7 .. :try_end_b8} :catchall_b4

    #@b8
    .line 924
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@ba
    monitor-enter v1

    #@bb
    .line 925
    :try_start_bb
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@bd
    invoke-virtual {v0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->isInitialized()Z

    #@c0
    move-result v0

    #@c1
    if-eqz v0, :cond_13f

    #@c3
    if-eqz p2, :cond_13f

    #@c5
    .line 926
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c8
    move-result-object v11

    #@c9
    .local v11, i$:Ljava/util/Iterator;
    :cond_c9
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@cc
    move-result v0

    #@cd
    if-eqz v0, :cond_13f

    #@cf
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d2
    move-result-object v12

    #@d3
    check-cast v12, Ljava/lang/String;

    #@d5
    .line 927
    .local v12, permissionName:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@d7
    const/4 v3, 0x1

    #@d8
    invoke-virtual {v0, v12, v3}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@db
    move-result v0

    #@dc
    if-nez v0, :cond_c9

    #@de
    .line 928
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@e0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e3
    move-result-object v13

    #@e4
    .line 929
    .restart local v13       #res:Landroid/content/res/Resources;
    new-instance v0, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const v3, 0x1040030

    #@ec
    invoke-virtual {v13, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ef
    move-result-object v3

    #@f0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v0

    #@f4
    const-string v3, " "

    #@f6
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v0

    #@fa
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@fd
    move-result-object v3

    #@fe
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v0

    #@102
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v2

    #@106
    .line 932
    .restart local v2       #title:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    #@109
    move-result v6

    #@10a
    const/4 v9, 0x0

    #@10b
    new-instance v10, Landroid/content/Intent;

    #@10d
    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    #@110
    move-object v5, p0

    #@111
    move-object v7, v2

    #@112
    move-object v8, v2

    #@113
    invoke-direct/range {v5 .. v10}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@116
    .line 933
    const-string v0, "DeviceManager3LM"

    #@118
    new-instance v3, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    const-string v5, "Install blocked: "

    #@11f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v3

    #@123
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v3

    #@127
    const-string v5, " due to blocked permission: "

    #@129
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v3

    #@12d
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v3

    #@131
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v3

    #@135
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@138
    .line 935
    const/4 v0, 0x0

    #@139
    monitor-exit v1

    #@13a
    goto/16 :goto_5

    #@13c
    .line 939
    .end local v2           #title:Ljava/lang/String;
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #permissionName:Ljava/lang/String;
    .end local v13           #res:Landroid/content/res/Resources;
    :catchall_13c
    move-exception v0

    #@13d
    monitor-exit v1
    :try_end_13e
    .catchall {:try_start_bb .. :try_end_13e} :catchall_13c

    #@13e
    throw v0

    #@13f
    :cond_13f
    :try_start_13f
    monitor-exit v1
    :try_end_140
    .catchall {:try_start_13f .. :try_end_140} :catchall_13c

    #@140
    .line 941
    const/4 v0, 0x1

    #@141
    goto/16 :goto_5
.end method

.method public checkAppUninstallPolicies(Ljava/lang/String;)Z
    .registers 15
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const v3, 0x1040031

    #@4
    const/4 v12, 0x0

    #@5
    const/4 v0, 0x1

    #@6
    .line 956
    iget-boolean v1, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z

    #@8
    if-nez v1, :cond_b

    #@a
    .line 980
    :goto_a
    return v0

    #@b
    .line 959
    :cond_b
    invoke-direct {p0, v5}, Lcom/android/server/DeviceManager3LMService;->isBootLocked(Ljava/lang/String;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_4d

    #@11
    .line 960
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v11

    #@17
    .line 961
    .local v11, res:Landroid/content/res/Resources;
    new-instance v0, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    const-string v1, " "

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    .line 963
    .local v2, title:Ljava/lang/String;
    const v0, 0x104002d

    #@39
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    .line 964
    .local v4, text:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    #@40
    move-result v1

    #@41
    new-instance v5, Landroid/content/Intent;

    #@43
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@46
    move-object v0, p0

    #@47
    move-object v3, v2

    #@48
    invoke-direct/range {v0 .. v5}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@4b
    move v0, v12

    #@4c
    .line 965
    goto :goto_a

    #@4d
    .line 968
    .end local v2           #title:Ljava/lang/String;
    .end local v4           #text:Ljava/lang/String;
    .end local v11           #res:Landroid/content/res/Resources;
    :cond_4d
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@4f
    monitor-enter v1

    #@50
    .line 969
    :try_start_50
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@52
    invoke-virtual {v3}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->isInitialized()Z

    #@55
    move-result v3

    #@56
    if-eqz v3, :cond_b5

    #@58
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@5a
    const/4 v5, 0x1

    #@5b
    invoke-virtual {v3, p1, v5}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@5e
    move-result v3

    #@5f
    if-nez v3, :cond_b5

    #@61
    .line 971
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@63
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@66
    move-result-object v11

    #@67
    .line 972
    .restart local v11       #res:Landroid/content/res/Resources;
    new-instance v0, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const v3, 0x1040031

    #@6f
    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    const-string v3, " "

    #@79
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v0

    #@7d
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v2

    #@89
    .line 974
    .restart local v2       #title:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    #@8c
    move-result v6

    #@8d
    const/4 v9, 0x0

    #@8e
    new-instance v10, Landroid/content/Intent;

    #@90
    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    #@93
    move-object v5, p0

    #@94
    move-object v7, v2

    #@95
    move-object v8, v2

    #@96
    invoke-direct/range {v5 .. v10}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@99
    .line 975
    const-string v0, "DeviceManager3LM"

    #@9b
    new-instance v3, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v5, "Pkg name policy does not permit uninstalling "

    #@a2
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v3

    #@ae
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    .line 976
    monitor-exit v1

    #@b2
    move v0, v12

    #@b3
    goto/16 :goto_a

    #@b5
    .line 978
    .end local v2           #title:Ljava/lang/String;
    .end local v11           #res:Landroid/content/res/Resources;
    :cond_b5
    monitor-exit v1

    #@b6
    goto/16 :goto_a

    #@b8
    :catchall_b8
    move-exception v0

    #@b9
    monitor-exit v1
    :try_end_ba
    .catchall {:try_start_50 .. :try_end_ba} :catchall_b8

    #@ba
    throw v0
.end method

.method public checkPackagePermission(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 16
    .parameter "permName"
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v0, 0x1

    #@3
    .line 738
    iget-boolean v1, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z

    #@5
    if-nez v1, :cond_8

    #@7
    .line 783
    :goto_7
    return v0

    #@8
    .line 758
    :cond_8
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v11

    #@e
    .line 760
    .local v11, res:Landroid/content/res/Resources;
    invoke-direct {p0, v4}, Lcom/android/server/DeviceManager3LMService;->isBootLocked(Ljava/lang/String;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_35

    #@14
    if-eqz p1, :cond_1e

    #@16
    const-string v1, "com.android.permission.CAMERA"

    #@18
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_35

    #@1e
    .line 762
    :cond_1e
    const v0, 0x104002f

    #@21
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    .line 763
    .local v2, title:Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    #@28
    move-result v1

    #@29
    new-instance v5, Landroid/content/Intent;

    #@2b
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@2e
    move-object v0, p0

    #@2f
    move-object v3, v2

    #@30
    invoke-direct/range {v0 .. v5}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@33
    move v0, v12

    #@34
    .line 764
    goto :goto_7

    #@35
    .line 767
    .end local v2           #title:Ljava/lang/String;
    :cond_35
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@37
    monitor-enter v1

    #@38
    .line 768
    :try_start_38
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@3a
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    move-result-object v10

    #@3e
    check-cast v10, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@40
    .line 769
    .local v10, re:Lcom/android/server/DeviceManager3LMService$RulesEngine;
    if-eqz v10, :cond_49

    #@42
    const/4 v3, 0x1

    #@43
    invoke-virtual {v10, p2, v3}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@46
    move-result v3

    #@47
    if-eqz v3, :cond_4e

    #@49
    .line 770
    :cond_49
    monitor-exit v1

    #@4a
    goto :goto_7

    #@4b
    .line 772
    .end local v10           #re:Lcom/android/server/DeviceManager3LMService$RulesEngine;
    :catchall_4b
    move-exception v0

    #@4c
    monitor-exit v1
    :try_end_4d
    .catchall {:try_start_38 .. :try_end_4d} :catchall_4b

    #@4d
    throw v0

    #@4e
    .restart local v10       #re:Lcom/android/server/DeviceManager3LMService$RulesEngine;
    :cond_4e
    :try_start_4e
    monitor-exit v1
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4b

    #@4f
    .line 774
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@52
    move-result-wide v7

    #@53
    .line 775
    .local v7, identityToken:J
    invoke-direct {p0, p2}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    .line 776
    .local v6, appLabel:Ljava/lang/String;
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5a
    .line 778
    const/16 v0, 0x2e

    #@5c
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    #@5f
    move-result v0

    #@60
    add-int/lit8 v0, v0, 0x1

    #@62
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@65
    move-result-object v9

    #@66
    .line 779
    .local v9, perm:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const v1, 0x1040025

    #@6e
    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v0

    #@76
    const-string v1, " "

    #@78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v0

    #@7c
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v0

    #@80
    const-string v1, " for "

    #@82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v0

    #@86
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v0

    #@8a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v2

    #@8e
    .line 782
    .restart local v2       #title:Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    #@91
    move-result v1

    #@92
    new-instance v5, Landroid/content/Intent;

    #@94
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@97
    move-object v0, p0

    #@98
    move-object v3, v2

    #@99
    invoke-direct/range {v0 .. v5}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@9c
    move v0, v12

    #@9d
    .line 783
    goto/16 :goto_7
.end method

.method public checkPrimaryClipAccess(Ljava/lang/String;)Z
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 1859
    iget-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mSecurePrimaryClip:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    .line 1863
    :goto_5
    return v0

    #@6
    .line 1862
    :cond_6
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@8
    monitor-enter v1

    #@9
    .line 1863
    :try_start_9
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v0, p1, v2}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@f
    move-result v0

    #@10
    monitor-exit v1

    #@11
    goto :goto_5

    #@12
    .line 1864
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_9 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public checkSignature(I)Z
    .registers 16
    .parameter "uid"

    #@0
    .prologue
    .line 1758
    iget-object v11, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@2
    if-nez v11, :cond_6

    #@4
    .line 1759
    const/4 v11, 0x0

    #@5
    .line 1784
    :goto_5
    return v11

    #@6
    .line 1764
    :cond_6
    :try_start_6
    iget-object v11, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@8
    invoke-virtual {v11, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@b
    move-result-object v8

    #@c
    .line 1766
    .local v8, packages:[Ljava/lang/String;
    move-object v0, v8

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@e
    .local v5, len$:I
    const/4 v3, 0x0

    #@f
    .local v3, i$:I
    move v4, v3

    #@10
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v5           #len$:I
    .local v4, i$:I
    :goto_10
    if-ge v4, v5, :cond_52

    #@12
    aget-object v9, v0, v4

    #@14
    .line 1767
    .local v9, pkg:Ljava/lang/String;
    iget-object v11, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@16
    const/16 v12, 0x40

    #@18
    invoke-virtual {v11, v9, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@1b
    move-result-object v7

    #@1c
    .line 1772
    .local v7, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v1, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@1e
    .local v1, arr$:[Landroid/content/pm/Signature;
    array-length v6, v1

    #@1f
    .local v6, len$:I
    const/4 v3, 0x0

    #@20
    .end local v4           #i$:I
    .restart local v3       #i$:I
    :goto_20
    if-ge v3, v6, :cond_35

    #@22
    aget-object v10, v1, v3

    #@24
    .line 1773
    .local v10, pkgSignature:Landroid/content/pm/Signature;
    iget-object v11, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@26
    invoke-virtual {v10}, Landroid/content/pm/Signature;->toByteArray()[B

    #@29
    move-result-object v12

    #@2a
    invoke-virtual {v11, v12}, Lcom/android/server/DeviceManager3LMService$PublicKey3LM;->comparePublicKey([B)Z
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_2d} :catch_39

    #@2d
    move-result v11

    #@2e
    if-eqz v11, :cond_32

    #@30
    .line 1777
    const/4 v11, 0x1

    #@31
    goto :goto_5

    #@32
    .line 1772
    :cond_32
    add-int/lit8 v3, v3, 0x1

    #@34
    goto :goto_20

    #@35
    .line 1766
    .end local v10           #pkgSignature:Landroid/content/pm/Signature;
    :cond_35
    add-int/lit8 v3, v4, 0x1

    #@37
    move v4, v3

    #@38
    .end local v3           #i$:I
    .restart local v4       #i$:I
    goto :goto_10

    #@39
    .line 1781
    .end local v1           #arr$:[Landroid/content/pm/Signature;
    .end local v4           #i$:I
    .end local v6           #len$:I
    .end local v7           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v8           #packages:[Ljava/lang/String;
    .end local v9           #pkg:Ljava/lang/String;
    :catch_39
    move-exception v2

    #@3a
    .line 1782
    .local v2, e:Ljava/lang/Exception;
    const-string v11, "DeviceManager3LM"

    #@3c
    new-instance v12, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v13, "Error trying to verify package public key against 3LM pub key:"

    #@43
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v12

    #@47
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v12

    #@4b
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v12

    #@4f
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1784
    .end local v2           #e:Ljava/lang/Exception;
    :cond_52
    const/4 v11, 0x0

    #@53
    goto :goto_5
.end method

.method public checkUidPermission(Ljava/lang/String;I)Z
    .registers 10
    .parameter "permName"
    .parameter "uid"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 576
    iget-boolean v6, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z

    #@3
    if-nez v6, :cond_6

    #@5
    .line 589
    :cond_5
    :goto_5
    return v5

    #@6
    .line 581
    :cond_6
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@8
    invoke-virtual {v6, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    .line 582
    .local v4, pkgNames:[Ljava/lang/String;
    if-eqz v4, :cond_5

    #@e
    .line 583
    move-object v0, v4

    #@f
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@10
    .local v2, len$:I
    const/4 v1, 0x0

    #@11
    .local v1, i$:I
    :goto_11
    if-ge v1, v2, :cond_5

    #@13
    aget-object v3, v0, v1

    #@15
    .line 584
    .local v3, name:Ljava/lang/String;
    invoke-virtual {p0, p1, v3}, Lcom/android/server/DeviceManager3LMService;->checkPackagePermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@18
    move-result v6

    #@19
    if-nez v6, :cond_1d

    #@1b
    .line 585
    const/4 v5, 0x0

    #@1c
    goto :goto_5

    #@1d
    .line 583
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_11
.end method

.method public checkVpnDns(Ljava/lang/String;)Z
    .registers 10
    .parameter "vpnSuffixes"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 481
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v6

    #@5
    if-nez v6, :cond_8

    #@7
    .line 511
    :goto_7
    return v5

    #@8
    .line 483
    :cond_8
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService;->mVpnSettingsMutex:Ljava/lang/String;

    #@a
    monitor-enter v6

    #@b
    .line 484
    :try_start_b
    iget-boolean v7, p0, Lcom/android/server/DeviceManager3LMService;->mVpnHasOriginalData:Z

    #@d
    if-nez v7, :cond_14

    #@f
    monitor-exit v6

    #@10
    goto :goto_7

    #@11
    .line 509
    :catchall_11
    move-exception v5

    #@12
    monitor-exit v6
    :try_end_13
    .catchall {:try_start_b .. :try_end_13} :catchall_11

    #@13
    throw v5

    #@14
    .line 486
    :cond_14
    :try_start_14
    const-string v5, "net.dns1"

    #@16
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 487
    .local v0, current_dns1:Ljava/lang/String;
    const-string v5, "net.dns2"

    #@1c
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    .line 488
    .local v1, current_dns2:Ljava/lang/String;
    const-string v5, "net.dns.search"

    #@22
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .line 489
    .local v2, current_suffixes:Ljava/lang/String;
    const-string v5, "vpn.net.tun.dns1"

    #@28
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    .line 490
    .local v3, current_tun_dns1:Ljava/lang/String;
    const-string v5, "vpn.net.tun.dns2"

    #@2e
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    .line 492
    .local v4, current_tun_dns2:Ljava/lang/String;
    if-nez v0, :cond_36

    #@34
    const-string v0, ""

    #@36
    .line 493
    :cond_36
    if-nez v1, :cond_3a

    #@38
    const-string v1, ""

    #@3a
    .line 494
    :cond_3a
    if-nez v2, :cond_3e

    #@3c
    const-string v2, ""

    #@3e
    .line 495
    :cond_3e
    if-nez v3, :cond_42

    #@40
    const-string v3, ""

    #@42
    .line 496
    :cond_42
    if-nez v4, :cond_46

    #@44
    const-string v4, ""

    #@46
    .line 498
    :cond_46
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v5

    #@4a
    if-nez v5, :cond_53

    #@4c
    .line 499
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDnsSuffixes:Ljava/lang/String;

    #@4e
    .line 500
    const-string v5, "net.dns.search"

    #@50
    invoke-static {v5, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    .line 503
    :cond_53
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v5

    #@57
    if-nez v5, :cond_67

    #@59
    .line 504
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns1:Ljava/lang/String;

    #@5b
    .line 505
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns2:Ljava/lang/String;

    #@5d
    .line 506
    const-string v5, "net.dns1"

    #@5f
    invoke-static {v5, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    .line 507
    const-string v5, "net.dns2"

    #@64
    invoke-static {v5, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@67
    .line 509
    :cond_67
    monitor-exit v6
    :try_end_68
    .catchall {:try_start_14 .. :try_end_68} :catchall_11

    #@68
    .line 511
    const/4 v5, 0x1

    #@69
    goto :goto_7
.end method

.method public clear()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 240
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 282
    :cond_8
    :goto_8
    return-void

    #@9
    .line 242
    :cond_9
    iput-boolean v5, p0, Lcom/android/server/DeviceManager3LMService;->mInitialized:Z

    #@b
    .line 243
    iput-boolean v6, p0, Lcom/android/server/DeviceManager3LMService;->mBluetoothEnabled:Z

    #@d
    .line 244
    iput-boolean v6, p0, Lcom/android/server/DeviceManager3LMService;->mMultiUserEnabled:Z

    #@f
    .line 245
    new-instance v2, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@11
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@14
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@16
    .line 246
    new-instance v2, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@18
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@1b
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@1d
    .line 247
    new-instance v2, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@1f
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@22
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPubKeyRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@24
    .line 248
    new-instance v2, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@26
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@29
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@2b
    .line 249
    new-instance v2, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@2d
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@30
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@32
    .line 250
    new-instance v2, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@34
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@37
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@39
    .line 251
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@3b
    monitor-enter v3

    #@3c
    .line 252
    :try_start_3c
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@3e
    invoke-interface {v2}, Ljava/util/Map;->clear()V

    #@41
    .line 253
    monitor-exit v3
    :try_end_42
    .catchall {:try_start_3c .. :try_end_42} :catchall_84

    #@42
    .line 254
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mWifiFilter:Ljava/util/Set;

    #@44
    invoke-interface {v2}, Ljava/util/Set;->clear()V

    #@47
    .line 255
    invoke-virtual {p0, v5}, Lcom/android/server/DeviceManager3LMService;->blockAdb(Z)V

    #@4a
    .line 256
    invoke-virtual {p0, v5}, Lcom/android/server/DeviceManager3LMService;->blockUsb(Z)V

    #@4d
    .line 257
    const-string v2, "1"

    #@4f
    const-string v3, "ro.3LM.extended"

    #@51
    const-string v4, ""

    #@53
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v1

    #@5b
    .line 259
    .local v1, isExtended:Z
    invoke-virtual {p0, v1}, Lcom/android/server/DeviceManager3LMService;->lockAdmin(Z)V

    #@5e
    .line 260
    invoke-virtual {p0, v5}, Lcom/android/server/DeviceManager3LMService;->setOtaDelay(I)V

    #@61
    .line 261
    invoke-virtual {p0, v5}, Lcom/android/server/DeviceManager3LMService;->setBootLock(Z)V

    #@64
    .line 265
    invoke-virtual {p0, v6}, Lcom/android/server/DeviceManager3LMService;->setNfcState(I)V

    #@67
    .line 266
    invoke-virtual {p0, v6}, Lcom/android/server/DeviceManager3LMService;->setWifiState(I)V

    #@6a
    .line 267
    iput-boolean v5, p0, Lcom/android/server/DeviceManager3LMService;->mSecurePrimaryClip:Z

    #@6c
    .line 269
    invoke-virtual {p0, v5}, Lcom/android/server/DeviceManager3LMService;->blockScreenshot(Z)V

    #@6f
    .line 270
    invoke-virtual {p0, v5}, Lcom/android/server/DeviceManager3LMService;->blockTethering(Z)V

    #@72
    .line 273
    invoke-virtual {p0}, Lcom/android/server/DeviceManager3LMService;->isApnLocked()Z

    #@75
    move-result v0

    #@76
    .line 274
    .local v0, apnsModified:Z
    invoke-virtual {p0, v5}, Lcom/android/server/DeviceManager3LMService;->lockApn(Z)V

    #@79
    .line 275
    if-eqz v0, :cond_8

    #@7b
    .line 276
    new-instance v2, Lcom/android/server/DeviceManager3LMService$1;

    #@7d
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$1;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@80
    invoke-virtual {v2}, Lcom/android/server/DeviceManager3LMService$1;->start()V

    #@83
    goto :goto_8

    #@84
    .line 253
    .end local v0           #apnsModified:Z
    .end local v1           #isExtended:Z
    :catchall_84
    move-exception v2

    #@85
    :try_start_85
    monitor-exit v3
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_84

    #@86
    throw v2
.end method

.method public clearApn()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2039
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 2050
    :goto_7
    return-void

    #@8
    .line 2042
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 2043
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v2

    #@11
    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@13
    invoke-virtual {v2, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@16
    .line 2046
    const-string v2, "content://telephony/carriers/preferapn"

    #@18
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1b
    move-result-object v0

    #@1c
    .line 2047
    .local v0, preferedApn:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@1e
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@21
    .line 2048
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "apn_id"

    #@23
    const-string v3, "-1"

    #@25
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 2049
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@31
    goto :goto_7
.end method

.method public clearApplicationUserData(Ljava/lang/String;)Z
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 1576
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    .line 1579
    :goto_7
    return v1

    #@8
    .line 1577
    :cond_8
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@a
    const-string v2, "activity"

    #@c
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/ActivityManager;

    #@12
    .line 1578
    .local v0, am:Landroid/app/ActivityManager;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@15
    .line 1579
    new-instance v1, Lcom/android/server/DeviceManager3LMService$ClearUserDataObserver;

    #@17
    const/4 v2, 0x0

    #@18
    invoke-direct {v1, p0, v2}, Lcom/android/server/DeviceManager3LMService$ClearUserDataObserver;-><init>(Lcom/android/server/DeviceManager3LMService;Lcom/android/server/DeviceManager3LMService$1;)V

    #@1b
    invoke-virtual {v0, p1, v1}, Landroid/app/ActivityManager;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;)Z

    #@1e
    move-result v1

    #@1f
    goto :goto_7
.end method

.method public clearPackagePermissions()V
    .registers 3

    #@0
    .prologue
    .line 1877
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@2
    monitor-enter v1

    #@3
    .line 1878
    :try_start_3
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@5
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@8
    .line 1879
    monitor-exit v1

    #@9
    .line 1880
    return-void

    #@a
    .line 1879
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public completePackageScan(II)V
    .registers 4
    .parameter "scanId"
    .parameter "result"

    #@0
    .prologue
    .line 1913
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1920
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1915
    :cond_7
    const/16 v0, 0x3e8

    #@9
    if-eq p2, v0, :cond_f

    #@b
    const/16 v0, 0x3e9

    #@d
    if-ne p2, v0, :cond_6

    #@f
    .line 1918
    :cond_f
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@12
    .line 1919
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@14
    invoke-virtual {v0, p1, p2}, Landroid/content/pm/PackageManager;->verifyPendingInstall(II)V

    #@17
    goto :goto_6
.end method

.method public connectToVpn(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "abstractSocketName"
    .parameter "vpnSubnets"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 416
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v4

    #@5
    if-nez v4, :cond_8

    #@7
    .line 437
    :goto_7
    return v3

    #@8
    .line 419
    :cond_8
    :try_start_8
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v5, "connect local "

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 420
    .local v0, cmd:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1d
    invoke-virtual {v4, v0}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_20
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_8 .. :try_end_20} :catch_41

    #@20
    .line 428
    :try_start_20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "configure "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    .line 429
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@35
    invoke-virtual {v4, v0}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_38
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_20 .. :try_end_38} :catch_5f

    #@38
    .line 436
    const-string v3, "DeviceManager3LM"

    #@3a
    const-string v4, "tund connected and configured"

    #@3c
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 437
    const/4 v3, 0x1

    #@40
    goto :goto_7

    #@41
    .line 421
    .end local v0           #cmd:Ljava/lang/String;
    :catch_41
    move-exception v2

    #@42
    .line 422
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@45
    move-result v1

    #@46
    .line 423
    .local v1, code:I
    const-string v4, "DeviceManager3LM"

    #@48
    new-instance v5, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v6, "tund connect failed "

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_7

    #@5f
    .line 430
    .end local v1           #code:I
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v0       #cmd:Ljava/lang/String;
    :catch_5f
    move-exception v2

    #@60
    .line 431
    .restart local v2       #e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@63
    move-result v1

    #@64
    .line 432
    .restart local v1       #code:I
    const-string v4, "DeviceManager3LM"

    #@66
    new-instance v5, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v6, "tund configure failed "

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    goto :goto_7
.end method

.method public deletePackage(Ljava/lang/String;Z)V
    .registers 14
    .parameter "pkgName"
    .parameter "deleteData"

    #@0
    .prologue
    const-wide/16 v9, 0x1388

    #@2
    .line 836
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_9

    #@8
    .line 862
    :goto_8
    return-void

    #@9
    .line 838
    :cond_9
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    .line 841
    :try_start_c
    new-instance v2, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;

    #@e
    invoke-direct {v2, p0}, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@11
    .line 842
    .local v2, observer:Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;
    const/4 v1, 0x2

    #@12
    .line 843
    .local v1, flags:I
    if-nez p2, :cond_16

    #@14
    or-int/lit8 v1, v1, 0x1

    #@16
    .line 845
    :cond_16
    monitor-enter v2
    :try_end_17
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_17} :catch_47

    #@17
    .line 846
    :try_start_17
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@19
    monitor-enter v6
    :try_end_1a
    .catchall {:try_start_17 .. :try_end_1a} :catchall_44

    #@1a
    .line 847
    :try_start_1a
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@1c
    invoke-virtual {v5, p1, v2, v1}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    #@1f
    .line 849
    const-wide/16 v3, 0x0

    #@21
    .line 850
    .local v3, waitTime:J
    :goto_21
    invoke-virtual {v2}, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;->isDone()Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_34

    #@27
    const-wide/16 v7, 0x61a8

    #@29
    cmp-long v5, v3, v7

    #@2b
    if-gez v5, :cond_34

    #@2d
    .line 851
    const-wide/16 v7, 0x1388

    #@2f
    invoke-virtual {v2, v7, v8}, Ljava/lang/Object;->wait(J)V

    #@32
    .line 852
    add-long/2addr v3, v9

    #@33
    goto :goto_21

    #@34
    .line 854
    :cond_34
    invoke-virtual {v2}, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;->isDone()Z

    #@37
    move-result v5

    #@38
    if-nez v5, :cond_41

    #@3a
    .line 855
    const-string v5, "DeviceManager3LM"

    #@3c
    const-string v7, "Timed out waiting for packageRemoved callback"

    #@3e
    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 857
    :cond_41
    monitor-exit v6
    :try_end_42
    .catchall {:try_start_1a .. :try_end_42} :catchall_61

    #@42
    .line 858
    :try_start_42
    monitor-exit v2

    #@43
    goto :goto_8

    #@44
    .end local v3           #waitTime:J
    :catchall_44
    move-exception v5

    #@45
    monitor-exit v2
    :try_end_46
    .catchall {:try_start_42 .. :try_end_46} :catchall_44

    #@46
    :try_start_46
    throw v5
    :try_end_47
    .catch Ljava/lang/InterruptedException; {:try_start_46 .. :try_end_47} :catch_47

    #@47
    .line 859
    .end local v1           #flags:I
    .end local v2           #observer:Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;
    :catch_47
    move-exception v0

    #@48
    .line 860
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v5, "DeviceManager3LM"

    #@4a
    new-instance v6, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v7, "Interrupted exception:"

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v6

    #@5d
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_8

    #@61
    .line 857
    .end local v0           #e:Ljava/lang/InterruptedException;
    .restart local v1       #flags:I
    .restart local v2       #observer:Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;
    :catchall_61
    move-exception v5

    #@62
    :try_start_62
    monitor-exit v6
    :try_end_63
    .catchall {:try_start_62 .. :try_end_63} :catchall_61

    #@63
    :try_start_63
    throw v5
    :try_end_64
    .catchall {:try_start_63 .. :try_end_64} :catchall_44
.end method

.method public disablePackage(Ljava/lang/String;)V
    .registers 4
    .parameter "pkgName"

    #@0
    .prologue
    .line 634
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 641
    :goto_6
    return-void

    #@7
    .line 636
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 637
    const/4 v0, 0x3

    #@b
    invoke-direct {p0, p1, v0}, Lcom/android/server/DeviceManager3LMService;->setPackageState(Ljava/lang/String;I)V

    #@e
    .line 640
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@10
    const/4 v1, 0x0

    #@11
    invoke-virtual {v0, p1, v1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->updatePolicy(Ljava/lang/String;Z)Z

    #@14
    goto :goto_6
.end method

.method public disconnectFromVpn(Z)Z
    .registers 8
    .parameter "doReset"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 552
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_8

    #@7
    .line 566
    :goto_7
    return v2

    #@8
    .line 555
    :cond_8
    if-eqz p1, :cond_1a

    #@a
    .line 556
    :try_start_a
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@c
    const-string v4, "reset"

    #@e
    invoke-virtual {v3, v4}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_11
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_a .. :try_end_11} :catch_22

    #@11
    .line 565
    :goto_11
    const-string v2, "DeviceManager3LM"

    #@13
    const-string v3, "tund disconnected"

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 566
    const/4 v2, 0x1

    #@19
    goto :goto_7

    #@1a
    .line 558
    :cond_1a
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v4, "disconnect"

    #@1e
    invoke-virtual {v3, v4}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_21
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_21} :catch_22

    #@21
    goto :goto_11

    #@22
    .line 559
    :catch_22
    move-exception v1

    #@23
    .line 560
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@26
    move-result v0

    #@27
    .line 561
    .local v0, code:I
    const-string v3, "DeviceManager3LM"

    #@29
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v5, "tund connect failed "

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_7
.end method

.method public enablePackage(Ljava/lang/String;)V
    .registers 4
    .parameter "pkgName"

    #@0
    .prologue
    .line 649
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 654
    :goto_6
    return-void

    #@7
    .line 651
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 652
    const/4 v0, 0x0

    #@b
    invoke-direct {p0, p1, v0}, Lcom/android/server/DeviceManager3LMService;->setPackageState(Ljava/lang/String;I)V

    #@e
    .line 653
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@10
    const/4 v1, 0x1

    #@11
    invoke-virtual {v0, p1, v1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->updatePolicy(Ljava/lang/String;Z)Z

    #@14
    goto :goto_6
.end method

.method public encryptPackage(Ljava/lang/String;ZZ)V
    .registers 4
    .parameter "pkgName"
    .parameter "encrypt"
    .parameter "required"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1278
    return-void
.end method

.method public getBluetoothEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 798
    iget-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mBluetoothEnabled:Z

    #@2
    return v0
.end method

.method public getMultiUserEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 2073
    iget-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mMultiUserEnabled:Z

    #@2
    return v0
.end method

.method public getNfcState()I
    .registers 3

    #@0
    .prologue
    .line 1517
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v0

    #@6
    const-string v1, "android.hardware.nfc"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_10

    #@e
    .line 1518
    const/4 v0, -0x1

    #@f
    .line 1520
    :goto_f
    return v0

    #@10
    :cond_10
    const-string v0, "persist.security.nfc.lockout"

    #@12
    const/4 v1, 0x1

    #@13
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@16
    move-result v0

    #@17
    goto :goto_f
.end method

.method public getNotificationText()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2081
    iget-boolean v1, p0, Lcom/android/server/DeviceManager3LMService;->mUseCustomNotification:Z

    #@2
    if-nez v1, :cond_13

    #@4
    .line 2084
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v0

    #@a
    .line 2085
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x1040026

    #@d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationText:Ljava/lang/String;

    #@13
    .line 2088
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_13
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationText:Ljava/lang/String;

    #@15
    return-object v1
.end method

.method public getOtaDelay()I
    .registers 4

    #@0
    .prologue
    .line 1678
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "ota_delay"

    #@8
    const/4 v2, 0x0

    #@9
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getOwnerInfo()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "lock_screen_owner_info"

    #@8
    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getOwnerInfoEnabled()I
    .registers 4

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "lock_screen_owner_info_enabled"

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getPackageGids(Ljava/lang/String;)[I
    .registers 15
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 1486
    const/4 v9, 0x0

    #@2
    .line 1489
    .local v9, skip:I
    :try_start_2
    iget-object v10, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@4
    invoke-virtual {v10, p1}, Landroid/content/pm/PackageManager;->getPackageGids(Ljava/lang/String;)[I

    #@7
    move-result-object v0

    #@8
    .line 1490
    .local v0, appGids:[I
    if-nez v0, :cond_e

    #@a
    .line 1491
    const/4 v10, 0x0

    #@b
    new-array v3, v10, [I

    #@d
    .line 1513
    .end local v0           #appGids:[I
    :cond_d
    :goto_d
    return-object v3

    #@e
    .line 1493
    .restart local v0       #appGids:[I
    :cond_e
    move-object v1, v0

    #@f
    .local v1, arr$:[I
    array-length v7, v1

    #@10
    .local v7, len$:I
    const/4 v6, 0x0

    #@11
    .local v6, i$:I
    :goto_11
    if-ge v6, v7, :cond_2e

    #@13
    aget v4, v1, v6

    #@15
    .line 1494
    .local v4, gid:I
    iget-object v10, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@17
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v11

    #@1b
    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v8

    #@1f
    check-cast v8, Ljava/lang/String;

    #@21
    .line 1495
    .local v8, perm:Ljava/lang/String;
    if-eqz v8, :cond_2b

    #@23
    invoke-virtual {p0, v8, p1}, Lcom/android/server/DeviceManager3LMService;->checkPackagePermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@26
    move-result v10

    #@27
    if-nez v10, :cond_2b

    #@29
    .line 1496
    add-int/lit8 v9, v9, 0x1

    #@2b
    .line 1493
    :cond_2b
    add-int/lit8 v6, v6, 0x1

    #@2d
    goto :goto_11

    #@2e
    .line 1500
    .end local v4           #gid:I
    .end local v8           #perm:Ljava/lang/String;
    :cond_2e
    array-length v10, v0

    #@2f
    sub-int/2addr v10, v9

    #@30
    new-array v3, v10, [I

    #@32
    .line 1501
    .local v3, filteredGids:[I
    const/4 v9, 0x0

    #@33
    .line 1502
    const/4 v5, 0x0

    #@34
    .local v5, i:I
    :goto_34
    array-length v10, v0

    #@35
    if-ge v5, v10, :cond_d

    #@37
    .line 1503
    iget-object v10, p0, Lcom/android/server/DeviceManager3LMService;->mAndroidIds:Ljava/util/HashMap;

    #@39
    aget v11, v0, v5

    #@3b
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v11

    #@3f
    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    move-result-object v8

    #@43
    check-cast v8, Ljava/lang/String;

    #@45
    .line 1504
    .restart local v8       #perm:Ljava/lang/String;
    if-eqz v8, :cond_52

    #@47
    invoke-virtual {p0, v8, p1}, Lcom/android/server/DeviceManager3LMService;->checkPackagePermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@4a
    move-result v10

    #@4b
    if-nez v10, :cond_52

    #@4d
    .line 1505
    add-int/lit8 v9, v9, 0x1

    #@4f
    .line 1502
    :goto_4f
    add-int/lit8 v5, v5, 0x1

    #@51
    goto :goto_34

    #@52
    .line 1508
    :cond_52
    sub-int v10, v5, v9

    #@54
    aget v11, v0, v5

    #@56
    aput v11, v3, v10
    :try_end_58
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_58} :catch_59

    #@58
    goto :goto_4f

    #@59
    .line 1510
    .end local v0           #appGids:[I
    .end local v1           #arr$:[I
    .end local v3           #filteredGids:[I
    .end local v5           #i:I
    .end local v6           #i$:I
    .end local v7           #len$:I
    .end local v8           #perm:Ljava/lang/String;
    :catch_59
    move-exception v2

    #@5a
    .line 1511
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-array v3, v12, [I

    #@5c
    goto :goto_d
.end method

.method public getPackageScanner()[Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1902
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScannerMutex:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1903
    const/4 v0, 0x3

    #@4
    :try_start_4
    new-array v0, v0, [Ljava/lang/String;

    #@6
    const/4 v2, 0x0

    #@7
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanner:Ljava/lang/String;

    #@9
    aput-object v3, v0, v2

    #@b
    const/4 v2, 0x1

    #@c
    iget-boolean v3, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanFailOnTimeout:Z

    #@e
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v0, v2

    #@14
    const/4 v2, 0x2

    #@15
    iget v3, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanTimeoutMillis:I

    #@17
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v0, v2

    #@1d
    monitor-exit v1

    #@1e
    return-object v0

    #@1f
    .line 1906
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_4 .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method

.method public getVersion()I
    .registers 2

    #@0
    .prologue
    .line 201
    const/16 v0, 0xc

    #@2
    return v0
.end method

.method public getWifiState()I
    .registers 3

    #@0
    .prologue
    .line 1552
    const-string v0, "persist.security.wifi.lockout"

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public init(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 175
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    .line 176
    new-instance v1, Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@6
    invoke-direct {v1, p1}, Lcom/android/server/DeviceManager3LMService$PublicKey3LM;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@b
    .line 177
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@13
    .line 178
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@16
    move-result v1

    #@17
    iput v1, p0, Lcom/android/server/DeviceManager3LMService;->mUid:I

    #@19
    .line 179
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@1b
    const-string v4, "notification"

    #@1d
    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Landroid/app/NotificationManager;

    #@23
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationManager:Landroid/app/NotificationManager;

    #@25
    .line 181
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2a
    move-result-object v1

    #@2b
    const-string v4, "boot_lock"

    #@2d
    invoke-static {v1, v4, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@30
    move-result v1

    #@31
    if-ne v1, v2, :cond_63

    #@33
    move v1, v2

    #@34
    :goto_34
    iput-boolean v1, p0, Lcom/android/server/DeviceManager3LMService;->mBootLocked:Z

    #@36
    .line 183
    iget-boolean v1, p0, Lcom/android/server/DeviceManager3LMService;->mBootLocked:Z

    #@38
    if-nez v1, :cond_65

    #@3a
    :goto_3a
    iput-boolean v2, p0, Lcom/android/server/DeviceManager3LMService;->mMultiUserEnabled:Z

    #@3c
    .line 184
    new-instance v1, Ljava/util/HashSet;

    #@3e
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    #@41
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mWifiFilter:Ljava/util/Set;

    #@43
    .line 185
    const-string v1, "network_management"

    #@45
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@48
    move-result-object v1

    #@49
    invoke-static {v1}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@4c
    move-result-object v1

    #@4d
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@4f
    .line 190
    const/4 v1, 0x0

    #@50
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationText:Ljava/lang/String;

    #@52
    .line 191
    iput-boolean v3, p0, Lcom/android/server/DeviceManager3LMService;->mUseCustomNotification:Z

    #@54
    .line 193
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@57
    move-result-object v0

    #@58
    .line 194
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_62

    #@5a
    .line 195
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@5c
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@5e
    const/4 v3, 0x5

    #@5f
    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    #@62
    .line 198
    :cond_62
    return-void

    #@63
    .end local v0           #adapter:Landroid/bluetooth/BluetoothAdapter;
    :cond_63
    move v1, v3

    #@64
    .line 181
    goto :goto_34

    #@65
    :cond_65
    move v2, v3

    #@66
    .line 183
    goto :goto_3a
.end method

.method public installPackage(Ljava/lang/String;)V
    .registers 13
    .parameter "packageURIAsString"

    #@0
    .prologue
    const-wide/16 v9, 0x1388

    #@2
    .line 802
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_9

    #@8
    .line 833
    :goto_8
    return-void

    #@9
    .line 804
    :cond_9
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    .line 807
    :try_start_c
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@f
    move-result-object v2

    #@10
    .line 808
    .local v2, packageURI:Landroid/net/Uri;
    new-instance v1, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;

    #@12
    invoke-direct {v1, p0}, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@15
    .line 809
    .local v1, observer:Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;
    monitor-enter v1
    :try_end_16
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_16} :catch_68
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_16} :catch_85

    #@16
    .line 810
    :try_start_16
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@18
    monitor-enter v6
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_65

    #@19
    .line 811
    :try_start_19
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@1b
    const/16 v7, 0x42

    #@1d
    const/4 v8, 0x0

    #@1e
    invoke-virtual {v5, v2, v1, v7, v8}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    #@21
    .line 815
    const-wide/16 v3, 0x0

    #@23
    .line 816
    .local v3, waitTime:J
    :goto_23
    invoke-virtual {v1}, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->isDone()Z

    #@26
    move-result v5

    #@27
    if-nez v5, :cond_36

    #@29
    const-wide/16 v7, 0x61a8

    #@2b
    cmp-long v5, v3, v7

    #@2d
    if-gez v5, :cond_36

    #@2f
    .line 817
    const-wide/16 v7, 0x1388

    #@31
    invoke-virtual {v1, v7, v8}, Ljava/lang/Object;->wait(J)V

    #@34
    .line 818
    add-long/2addr v3, v9

    #@35
    goto :goto_23

    #@36
    .line 820
    :cond_36
    invoke-virtual {v1}, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->isDone()Z

    #@39
    move-result v5

    #@3a
    if-nez v5, :cond_43

    #@3c
    .line 821
    const-string v5, "DeviceManager3LM"

    #@3e
    const-string v7, "Timed out waiting for packageInstalled callback"

    #@40
    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 823
    :cond_43
    iget v5, v1, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->returnCode:I

    #@45
    const/4 v7, 0x1

    #@46
    if-eq v5, v7, :cond_62

    #@48
    .line 824
    const-string v5, "DeviceManager3LM"

    #@4a
    new-instance v7, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v8, "Failed to install with error code = "

    #@51
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    iget v8, v1, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->returnCode:I

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v7

    #@5f
    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 826
    :cond_62
    monitor-exit v6
    :try_end_63
    .catchall {:try_start_19 .. :try_end_63} :catchall_82

    #@63
    .line 827
    :try_start_63
    monitor-exit v1

    #@64
    goto :goto_8

    #@65
    .end local v3           #waitTime:J
    :catchall_65
    move-exception v5

    #@66
    monitor-exit v1
    :try_end_67
    .catchall {:try_start_63 .. :try_end_67} :catchall_65

    #@67
    :try_start_67
    throw v5
    :try_end_68
    .catch Ljava/lang/NullPointerException; {:try_start_67 .. :try_end_68} :catch_68
    .catch Ljava/lang/InterruptedException; {:try_start_67 .. :try_end_68} :catch_85

    #@68
    .line 828
    .end local v1           #observer:Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;
    .end local v2           #packageURI:Landroid/net/Uri;
    :catch_68
    move-exception v0

    #@69
    .line 829
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v5, "DeviceManager3LM"

    #@6b
    new-instance v6, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v7, "Null URI to install package from:"

    #@72
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v6

    #@7e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_8

    #@82
    .line 826
    .end local v0           #e:Ljava/lang/NullPointerException;
    .restart local v1       #observer:Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;
    .restart local v2       #packageURI:Landroid/net/Uri;
    :catchall_82
    move-exception v5

    #@83
    :try_start_83
    monitor-exit v6
    :try_end_84
    .catchall {:try_start_83 .. :try_end_84} :catchall_82

    #@84
    :try_start_84
    throw v5
    :try_end_85
    .catchall {:try_start_84 .. :try_end_85} :catchall_65

    #@85
    .line 830
    .end local v1           #observer:Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;
    .end local v2           #packageURI:Landroid/net/Uri;
    :catch_85
    move-exception v0

    #@86
    .line 831
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v5, "DeviceManager3LM"

    #@88
    new-instance v6, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v7, "Interrupted exception:"

    #@8f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v6

    #@93
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v6

    #@97
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v6

    #@9b
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    goto/16 :goto_8
.end method

.method public isAdbBlocked()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1603
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "adb_blocked"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method public isAdminLocked()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1627
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "admin_locked"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method public isApnLocked()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1992
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "apn_locked"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method public isDataEncrypted()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1227
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isDataEncryptionRequired()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1234
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isPackage3LM(Ljava/lang/String;)Z
    .registers 11
    .parameter "pkg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 986
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@3
    if-nez v7, :cond_6

    #@5
    .line 1006
    :cond_5
    :goto_5
    return v6

    #@6
    .line 990
    :cond_6
    :try_start_6
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@8
    const/16 v8, 0x40

    #@a
    invoke-virtual {v7, p1, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_d} :catch_24

    #@d
    move-result-object v4

    #@e
    .line 999
    .local v4, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v0, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@10
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v2, v0

    #@11
    .local v2, len$:I
    const/4 v1, 0x0

    #@12
    .local v1, i$:I
    :goto_12
    if-ge v1, v2, :cond_5

    #@14
    aget-object v5, v0, v1

    #@16
    .line 1000
    .local v5, pkgSignature:Landroid/content/pm/Signature;
    iget-object v7, p0, Lcom/android/server/DeviceManager3LMService;->mPublicKey3LM:Lcom/android/server/DeviceManager3LMService$PublicKey3LM;

    #@18
    invoke-virtual {v5}, Landroid/content/pm/Signature;->toByteArray()[B

    #@1b
    move-result-object v8

    #@1c
    invoke-virtual {v7, v8}, Lcom/android/server/DeviceManager3LMService$PublicKey3LM;->comparePublicKey([B)Z

    #@1f
    move-result v7

    #@20
    if-eqz v7, :cond_26

    #@22
    .line 1002
    const/4 v6, 0x1

    #@23
    goto :goto_5

    #@24
    .line 993
    .end local v0           #arr$:[Landroid/content/pm/Signature;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v4           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v5           #pkgSignature:Landroid/content/pm/Signature;
    :catch_24
    move-exception v3

    #@25
    .line 994
    .local v3, nnfe:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_5

    #@26
    .line 999
    .end local v3           #nnfe:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0       #arr$:[Landroid/content/pm/Signature;
    .restart local v1       #i$:I
    .restart local v2       #len$:I
    .restart local v4       #packageInfo:Landroid/content/pm/PackageInfo;
    .restart local v5       #pkgSignature:Landroid/content/pm/Signature;
    :cond_26
    add-int/lit8 v1, v1, 0x1

    #@28
    goto :goto_12
.end method

.method public isPackageDisabled(Ljava/lang/String;)Z
    .registers 11
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 710
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->isBootLocked(Ljava/lang/String;)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_61

    #@7
    .line 711
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c
    move-result-object v7

    #@d
    .line 712
    .local v7, res:Landroid/content/res/Resources;
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const v1, 0x104002b

    #@15
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    .line 714
    .local v2, bar:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    invoke-direct {p0, p1}, Lcom/android/server/DeviceManager3LMService;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    const v1, 0x104002c

    #@3f
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    .line 716
    .local v3, title:Ljava/lang/String;
    const v0, 0x104002d

    #@4e
    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v4

    #@52
    .line 717
    .local v4, text:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    #@55
    move-result v1

    #@56
    new-instance v5, Landroid/content/Intent;

    #@58
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@5b
    move-object v0, p0

    #@5c
    invoke-direct/range {v0 .. v5}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@5f
    move v6, v8

    #@60
    .line 726
    .end local v2           #bar:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    .end local v4           #text:Ljava/lang/String;
    .end local v7           #res:Landroid/content/res/Resources;
    :goto_60
    return v6

    #@61
    .line 721
    :cond_61
    const/4 v6, 0x0

    #@62
    .line 722
    .local v6, disabled:Z
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@64
    monitor-enter v1

    #@65
    .line 723
    :try_start_65
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@67
    const/4 v5, 0x1

    #@68
    invoke-virtual {v0, p1, v5}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@6b
    move-result v0

    #@6c
    if-nez v0, :cond_74

    #@6e
    move v6, v8

    #@6f
    .line 724
    :goto_6f
    monitor-exit v1

    #@70
    goto :goto_60

    #@71
    :catchall_71
    move-exception v0

    #@72
    monitor-exit v1
    :try_end_73
    .catchall {:try_start_65 .. :try_end_73} :catchall_71

    #@73
    throw v0

    #@74
    .line 723
    :cond_74
    const/4 v6, 0x0

    #@75
    goto :goto_6f
.end method

.method public isPackageEncrypted(Ljava/lang/String;)Z
    .registers 3
    .parameter "pkgName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1252
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isPackageEncryptionRequired(Ljava/lang/String;)Z
    .registers 3
    .parameter "pkgName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1264
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isPackageSecure(Ljava/lang/String;)Z
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 1868
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@2
    monitor-enter v1

    #@3
    .line 1869
    :try_start_3
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-virtual {v0, p1, v2}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@9
    move-result v0

    #@a
    monitor-exit v1

    #@b
    return v0

    #@c
    .line 1870
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public isScreenshotBlocked()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1975
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "screenshot_blocked"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method public isSdEncrypted()Z
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1185
    const-string v2, "sd_encryption"

    #@4
    invoke-virtual {p0, v2}, Lcom/android/server/DeviceManager3LMService;->keyStoreContains(Ljava/lang/String;)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_19

    #@a
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v2

    #@10
    const-string v3, "sd_encryption"

    #@12
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15
    move-result v2

    #@16
    if-ne v2, v0, :cond_19

    #@18
    :goto_18
    return v0

    #@19
    :cond_19
    move v0, v1

    #@1a
    goto :goto_18
.end method

.method public isSdEncryptionRequired()Z
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1191
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "sd_encryption"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method public isSsidAllowed(Ljava/lang/String;)Z
    .registers 7
    .parameter "ssid"

    #@0
    .prologue
    const/16 v4, 0x22

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 1717
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mWifiFilter:Ljava/util/Set;

    #@6
    invoke-interface {v3}, Ljava/util/Set;->size()I

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_d

    #@c
    .line 1728
    :goto_c
    return v1

    #@d
    .line 1720
    :cond_d
    if-eqz p1, :cond_15

    #@f
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_17

    #@15
    :cond_15
    move v1, v2

    #@16
    goto :goto_c

    #@17
    .line 1723
    :cond_17
    move-object v0, p1

    #@18
    .line 1724
    .local v0, name:Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@1b
    move-result v2

    #@1c
    if-ne v2, v4, :cond_34

    #@1e
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@21
    move-result v2

    #@22
    add-int/lit8 v2, v2, -0x1

    #@24
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@27
    move-result v2

    #@28
    if-ne v2, v4, :cond_34

    #@2a
    .line 1725
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@2d
    move-result v2

    #@2e
    add-int/lit8 v2, v2, -0x1

    #@30
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    .line 1728
    :cond_34
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mWifiFilter:Ljava/util/Set;

    #@36
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@39
    move-result v1

    #@3a
    goto :goto_c
.end method

.method public isTetheringBlocked()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1958
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "tethering_blocked"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method public isUsbBlocked()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1652
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "usb_blocked"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method public keyStoreChangePassword(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "oldPassword"
    .parameter "newPassword"

    #@0
    .prologue
    .line 1154
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 1155
    const/4 v0, 0x0

    #@7
    .line 1159
    :goto_7
    return v0

    #@8
    .line 1157
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1158
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@d
    invoke-virtual {v0, p1}, Landroid/security/KeyStore;->unlock(Ljava/lang/String;)Z

    #@10
    .line 1159
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@12
    invoke-virtual {v0, p2}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    goto :goto_7
.end method

.method public keyStoreContains(Ljava/lang/String;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 1138
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 1139
    const/4 v0, 0x0

    #@7
    .line 1142
    :goto_7
    return v0

    #@8
    .line 1141
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1142
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@d
    invoke-virtual {v0, p1}, Landroid/security/KeyStore;->contains(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    goto :goto_7
.end method

.method public keyStoreDeleteKey(Ljava/lang/String;)Z
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1093
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 1102
    :goto_7
    return v0

    #@8
    .line 1096
    :cond_8
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@a
    invoke-virtual {v1}, Landroid/security/KeyStore;->state()Landroid/security/KeyStore$State;

    #@d
    move-result-object v1

    #@e
    sget-object v2, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@10
    if-eq v1, v2, :cond_1a

    #@12
    .line 1097
    const-string v1, "DeviceManager3LM"

    #@14
    const-string v2, "Keystore locked or not initialized"

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_7

    #@1a
    .line 1101
    :cond_1a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1d
    .line 1102
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@1f
    invoke-virtual {v0, p1}, Landroid/security/KeyStore;->delete(Ljava/lang/String;)Z

    #@22
    move-result v0

    #@23
    goto :goto_7
.end method

.method public keyStoreGetKey(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1080
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 1089
    :goto_7
    return-object v0

    #@8
    .line 1083
    :cond_8
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@a
    invoke-virtual {v1}, Landroid/security/KeyStore;->state()Landroid/security/KeyStore$State;

    #@d
    move-result-object v1

    #@e
    sget-object v2, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@10
    if-eq v1, v2, :cond_1a

    #@12
    .line 1084
    const-string v1, "DeviceManager3LM"

    #@14
    const-string v2, "Keystore locked or not initialized"

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_7

    #@1a
    .line 1088
    :cond_1a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1d
    .line 1089
    new-instance v0, Ljava/lang/String;

    #@1f
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@21
    invoke-virtual {v1, p1}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    #@28
    goto :goto_7
.end method

.method public keyStoreGetLastError()I
    .registers 2

    #@0
    .prologue
    .line 1131
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 1132
    const/4 v0, 0x0

    #@7
    .line 1134
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@a
    invoke-virtual {v0}, Landroid/security/KeyStore;->getLastError()I

    #@d
    move-result v0

    #@e
    goto :goto_7
.end method

.method public keyStoreLock()Z
    .registers 2

    #@0
    .prologue
    .line 1114
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 1115
    const/4 v0, 0x0

    #@7
    .line 1118
    :goto_7
    return v0

    #@8
    .line 1117
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1118
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@d
    invoke-virtual {v0}, Landroid/security/KeyStore;->lock()Z

    #@10
    move-result v0

    #@11
    goto :goto_7
.end method

.method public keyStorePutKey(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "name"
    .parameter "key"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1066
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 1076
    :goto_7
    return v0

    #@8
    .line 1069
    :cond_8
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@a
    invoke-virtual {v1}, Landroid/security/KeyStore;->state()Landroid/security/KeyStore$State;

    #@d
    move-result-object v1

    #@e
    sget-object v2, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@10
    if-eq v1, v2, :cond_1a

    #@12
    .line 1070
    const-string v1, "DeviceManager3LM"

    #@14
    const-string v2, "Keystore locked or not initialized"

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_7

    #@1a
    .line 1074
    :cond_1a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1d
    .line 1075
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@1f
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, p1, v1}, Landroid/security/KeyStore;->put(Ljava/lang/String;[B)Z

    #@26
    .line 1076
    const/4 v0, 0x1

    #@27
    goto :goto_7
.end method

.method public keyStoreReset()Z
    .registers 2

    #@0
    .prologue
    .line 1146
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 1147
    const/4 v0, 0x0

    #@7
    .line 1150
    :goto_7
    return v0

    #@8
    .line 1149
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1150
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@d
    invoke-virtual {v0}, Landroid/security/KeyStore;->reset()Z

    #@10
    move-result v0

    #@11
    goto :goto_7
.end method

.method public keyStoreSetPassword(Ljava/lang/String;)V
    .registers 3
    .parameter "password"

    #@0
    .prologue
    .line 1106
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1111
    :goto_6
    return-void

    #@7
    .line 1109
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 1110
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@c
    invoke-virtual {v0, p1}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z

    #@f
    goto :goto_6
.end method

.method public keyStoreTest()I
    .registers 2

    #@0
    .prologue
    .line 1057
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 1058
    const/4 v0, -0x1

    #@7
    .line 1062
    :goto_7
    return v0

    #@8
    .line 1060
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1062
    const/4 v0, 0x1

    #@c
    goto :goto_7
.end method

.method public keyStoreUnlock(Ljava/lang/String;)Z
    .registers 3
    .parameter "password"

    #@0
    .prologue
    .line 1122
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 1123
    const/4 v0, 0x0

    #@7
    .line 1127
    :goto_7
    return v0

    #@8
    .line 1126
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 1127
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mKeyStore:Landroid/security/KeyStore;

    #@d
    invoke-virtual {v0, p1}, Landroid/security/KeyStore;->unlock(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    goto :goto_7
.end method

.method public lockAdmin(Z)V
    .registers 5
    .parameter "lock"

    #@0
    .prologue
    .line 1614
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1619
    :goto_6
    return-void

    #@7
    .line 1616
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 1617
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string v2, "admin_locked"

    #@12
    if-eqz p1, :cond_19

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@18
    goto :goto_6

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_15
.end method

.method public lockApn(Z)V
    .registers 5
    .parameter "lock"

    #@0
    .prologue
    .line 1984
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1989
    :goto_6
    return-void

    #@7
    .line 1986
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 1987
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string v2, "apn_locked"

    #@12
    if-eqz p1, :cond_19

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@18
    goto :goto_6

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_15
.end method

.method public notification(III)V
    .registers 11
    .parameter "barId"
    .parameter "titleId"
    .parameter "textId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1795
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_8

    #@7
    .line 1803
    :goto_7
    return-void

    #@8
    .line 1797
    :cond_8
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v6

    #@e
    .line 1798
    .local v6, res:Landroid/content/res/Resources;
    if-nez p1, :cond_24

    #@10
    move-object v2, v4

    #@11
    .line 1799
    .local v2, bar:Ljava/lang/String;
    :goto_11
    if-nez p2, :cond_29

    #@13
    move-object v3, v4

    #@14
    .line 1800
    .local v3, title:Ljava/lang/String;
    :goto_14
    if-nez p3, :cond_2e

    #@16
    .line 1802
    .local v4, text:Ljava/lang/String;
    :goto_16
    add-int v0, p1, p2

    #@18
    add-int v1, v0, p3

    #@1a
    new-instance v5, Landroid/content/Intent;

    #@1c
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@1f
    move-object v0, p0

    #@20
    invoke-direct/range {v0 .. v5}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    #@23
    goto :goto_7

    #@24
    .line 1798
    .end local v2           #bar:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    .end local v4           #text:Ljava/lang/String;
    :cond_24
    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    goto :goto_11

    #@29
    .line 1799
    .restart local v2       #bar:Ljava/lang/String;
    :cond_29
    invoke-virtual {v6, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    goto :goto_14

    #@2e
    .line 1800
    .restart local v3       #title:Ljava/lang/String;
    :cond_2e
    invoke-virtual {v6, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    goto :goto_16
.end method

.method public putSettingsSecureInt(Ljava/lang/String;I)Z
    .registers 7
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 1175
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_8

    #@6
    .line 1176
    const/4 v2, 0x0

    #@7
    .line 1181
    :goto_7
    return v2

    #@8
    .line 1178
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 1179
    .local v0, identityToken:J
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v3

    #@12
    invoke-static {v3, p1, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@15
    move-result v2

    #@16
    .line 1180
    .local v2, ret:Z
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    goto :goto_7
.end method

.method public putSettingsSecureString(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 1164
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_8

    #@6
    .line 1165
    const/4 v2, 0x0

    #@7
    .line 1170
    :goto_7
    return v2

    #@8
    .line 1167
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 1168
    .local v0, identityToken:J
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v3

    #@12
    invoke-static {v3, p1, p2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@15
    move-result v2

    #@16
    .line 1169
    .local v2, ret:Z
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    goto :goto_7
.end method

.method public restoreDefaultApns()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2053
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_8

    #@7
    .line 2065
    :goto_7
    return-void

    #@8
    .line 2055
    :cond_8
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    .line 2057
    const-string v3, "content://telephony/carriers/restore"

    #@d
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@10
    move-result-object v1

    #@11
    .line 2058
    .local v1, restore:Landroid/net/Uri;
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@1a
    .line 2061
    const-string v3, "content://telephony/carriers/preferapn"

    #@1c
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1f
    move-result-object v0

    #@20
    .line 2062
    .local v0, preferedApn:Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    #@22
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@25
    .line 2063
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "apn_id"

    #@27
    const-string v4, "-1"

    #@29
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 2064
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, v0, v2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@35
    goto :goto_7
.end method

.method public restoreOriginalDns(Ljava/lang/String;)Z
    .registers 8
    .parameter "vpnSuffixes"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 519
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v4

    #@5
    if-nez v4, :cond_8

    #@7
    .line 543
    :goto_7
    return v3

    #@8
    .line 521
    :cond_8
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mVpnSettingsMutex:Ljava/lang/String;

    #@a
    monitor-enter v4

    #@b
    .line 522
    :try_start_b
    iget-boolean v5, p0, Lcom/android/server/DeviceManager3LMService;->mVpnHasOriginalData:Z

    #@d
    if-nez v5, :cond_14

    #@f
    monitor-exit v4

    #@10
    goto :goto_7

    #@11
    .line 541
    :catchall_11
    move-exception v3

    #@12
    monitor-exit v4
    :try_end_13
    .catchall {:try_start_b .. :try_end_13} :catchall_11

    #@13
    throw v3

    #@14
    .line 524
    :cond_14
    :try_start_14
    const-string v3, "net.dns1"

    #@16
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 525
    .local v0, current_dns1:Ljava/lang/String;
    const-string v3, "net.dns.search"

    #@1c
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    .line 526
    .local v1, current_suffixes:Ljava/lang/String;
    const-string v3, "vpn.net.tun.dns1"

    #@22
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .line 528
    .local v2, current_tun_dns1:Ljava/lang/String;
    if-nez v0, :cond_2a

    #@28
    const-string v0, ""

    #@2a
    .line 529
    :cond_2a
    if-nez v1, :cond_2e

    #@2c
    const-string v1, ""

    #@2e
    .line 530
    :cond_2e
    if-nez v2, :cond_32

    #@30
    const-string v2, ""

    #@32
    .line 532
    :cond_32
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_46

    #@38
    .line 533
    const-string v3, "net.dns1"

    #@3a
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns1:Ljava/lang/String;

    #@3c
    invoke-static {v3, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    .line 534
    const-string v3, "net.dns2"

    #@41
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns2:Ljava/lang/String;

    #@43
    invoke-static {v3, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 536
    :cond_46
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_53

    #@4c
    .line 537
    const-string v3, "net.dns.search"

    #@4e
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDnsSuffixes:Ljava/lang/String;

    #@50
    invoke-static {v3, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    .line 540
    :cond_53
    const/4 v3, 0x0

    #@54
    iput-boolean v3, p0, Lcom/android/server/DeviceManager3LMService;->mVpnHasOriginalData:Z

    #@56
    .line 541
    monitor-exit v4
    :try_end_57
    .catchall {:try_start_14 .. :try_end_57} :catchall_11

    #@57
    .line 543
    const/4 v3, 0x1

    #@58
    goto :goto_7
.end method

.method public setAllowedPackages(Ljava/util/Map;)Z
    .registers 3
    .parameter "pkgNames"

    #@0
    .prologue
    .line 696
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 698
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mAllowedPackages:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@a
    invoke-virtual {v0, p1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->loadPolicy(Ljava/util/Map;)Z

    #@d
    move-result v0

    #@e
    goto :goto_7
.end method

.method public setAppInstallPermissionPolicies(Ljava/util/Map;)Z
    .registers 7
    .parameter "permNameRegexPermMap"

    #@0
    .prologue
    .line 876
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 882
    :goto_7
    return v0

    #@8
    .line 878
    :cond_8
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@a
    monitor-enter v2

    #@b
    .line 879
    :try_start_b
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPermNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@d
    invoke-virtual {v1, p1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->loadPolicy(Ljava/util/Map;)Z

    #@10
    move-result v0

    #@11
    .line 880
    .local v0, loadResult:Z
    const-string v1, "DeviceManager3LM"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Loading perm name rules result: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 882
    monitor-exit v2

    #@2a
    goto :goto_7

    #@2b
    .line 883
    .end local v0           #loadResult:Z
    :catchall_2b
    move-exception v1

    #@2c
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_b .. :try_end_2d} :catchall_2b

    #@2d
    throw v1
.end method

.method public setAppInstallPkgNamePolicies(Ljava/util/Map;)Z
    .registers 7
    .parameter "pkgNameRegexPermMap"

    #@0
    .prologue
    .line 865
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 871
    :goto_7
    return v0

    #@8
    .line 867
    :cond_8
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@a
    monitor-enter v2

    #@b
    .line 868
    :try_start_b
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@d
    invoke-virtual {v1, p1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->loadPolicy(Ljava/util/Map;)Z

    #@10
    move-result v0

    #@11
    .line 869
    .local v0, loadResult:Z
    const-string v1, "DeviceManager3LM"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Loading pkg name rules result: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 871
    monitor-exit v2

    #@2a
    goto :goto_7

    #@2b
    .line 872
    .end local v0           #loadResult:Z
    :catchall_2b
    move-exception v1

    #@2c
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_b .. :try_end_2d} :catchall_2b

    #@2d
    throw v1
.end method

.method public setAppInstallPubkeyPolicies(Ljava/util/Map;)Z
    .registers 7
    .parameter "pubkeyRegexPermMap"

    #@0
    .prologue
    .line 887
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 893
    :goto_7
    return v0

    #@8
    .line 889
    :cond_8
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mPubKeyRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@a
    monitor-enter v2

    #@b
    .line 890
    :try_start_b
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPubKeyRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@d
    invoke-virtual {v1, p1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->loadPolicy(Ljava/util/Map;)Z

    #@10
    move-result v0

    #@11
    .line 891
    .local v0, loadResult:Z
    const-string v1, "DeviceManager3LM"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Loading perm name rules result: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 893
    monitor-exit v2

    #@2a
    goto :goto_7

    #@2b
    .line 894
    .end local v0           #loadResult:Z
    :catchall_2b
    move-exception v1

    #@2c
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_b .. :try_end_2d} :catchall_2b

    #@2d
    throw v1
.end method

.method public setAppUninstallPkgNamePolicies(Ljava/util/Map;)Z
    .registers 7
    .parameter "uninstallPkgNameRegexPermMap"

    #@0
    .prologue
    .line 945
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 951
    :goto_7
    return v0

    #@8
    .line 947
    :cond_8
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@a
    monitor-enter v2

    #@b
    .line 948
    :try_start_b
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mUninstallPkgNameRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@d
    invoke-virtual {v1, p1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->loadPolicy(Ljava/util/Map;)Z

    #@10
    move-result v0

    #@11
    .line 949
    .local v0, loadResult:Z
    const-string v1, "DeviceManager3LM"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Loading app uninstall package name policy: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 951
    monitor-exit v2

    #@2a
    goto :goto_7

    #@2b
    .line 952
    .end local v0           #loadResult:Z
    :catchall_2b
    move-exception v1

    #@2c
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_b .. :try_end_2d} :catchall_2b

    #@2d
    throw v1
.end method

.method public setBluetoothEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 787
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 790
    :goto_6
    return-void

    #@7
    .line 789
    :cond_7
    iput-boolean p1, p0, Lcom/android/server/DeviceManager3LMService;->mBluetoothEnabled:Z

    #@9
    goto :goto_6
.end method

.method public setBootLock(Z)V
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    .line 285
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 290
    :goto_6
    return-void

    #@7
    .line 287
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 288
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string v2, "boot_lock"

    #@12
    if-eqz p1, :cond_19

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@18
    goto :goto_6

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_15
.end method

.method public setDataEncryptionRequired(Z)V
    .registers 2
    .parameter "required"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1241
    return-void
.end method

.method public setLocationProviderEnabled(Ljava/lang/String;Z)V
    .registers 4
    .parameter "provider"
    .parameter "enabled"

    #@0
    .prologue
    .line 217
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 222
    :goto_6
    return-void

    #@7
    .line 218
    :cond_7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@a
    .line 219
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v0

    #@10
    invoke-static {v0, p1, p2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@13
    goto :goto_6
.end method

.method public setMultiUserEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 2068
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 2070
    :goto_6
    return-void

    #@7
    .line 2069
    :cond_7
    iput-boolean p1, p0, Lcom/android/server/DeviceManager3LMService;->mMultiUserEnabled:Z

    #@9
    goto :goto_6
.end method

.method public setNfcState(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 1528
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_14

    #@6
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b
    move-result-object v1

    #@c
    const-string v2, "android.hardware.nfc"

    #@e
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_15

    #@14
    .line 1543
    :cond_14
    :goto_14
    return-void

    #@15
    .line 1533
    :cond_15
    const-string v1, "persist.security.nfc.lockout"

    #@17
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 1535
    const-string v1, "nfc"

    #@20
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@23
    move-result-object v1

    #@24
    invoke-static {v1}, Landroid/nfc/INfcAdapter$Stub;->asInterface(Landroid/os/IBinder;)Landroid/nfc/INfcAdapter;

    #@27
    move-result-object v0

    #@28
    .line 1537
    .local v0, nfcService:Landroid/nfc/INfcAdapter;
    if-eqz v0, :cond_14

    #@2a
    if-nez p1, :cond_14

    #@2c
    .line 1538
    const/4 v1, 0x1

    #@2d
    :try_start_2d
    invoke-interface {v0, v1}, Landroid/nfc/INfcAdapter;->disable(Z)Z
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_30} :catch_31

    #@30
    goto :goto_14

    #@31
    .line 1540
    :catch_31
    move-exception v1

    #@32
    goto :goto_14
.end method

.method public setNotificationText(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 358
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 368
    :goto_6
    return-void

    #@7
    .line 361
    :cond_7
    if-eqz p1, :cond_11

    #@9
    const-string v0, ""

    #@b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_18

    #@11
    .line 362
    :cond_11
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationText:Ljava/lang/String;

    #@14
    .line 363
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mUseCustomNotification:Z

    #@17
    goto :goto_6

    #@18
    .line 365
    :cond_18
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService;->mNotificationText:Ljava/lang/String;

    #@1a
    .line 366
    const/4 v0, 0x1

    #@1b
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mUseCustomNotification:Z

    #@1d
    goto :goto_6
.end method

.method public setOtaDelay(I)V
    .registers 4
    .parameter "delay"

    #@0
    .prologue
    .line 1664
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1670
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1665
    :cond_7
    if-ltz p1, :cond_6

    #@9
    .line 1667
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    .line 1668
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v0

    #@12
    const-string v1, "ota_delay"

    #@14
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@17
    goto :goto_6
.end method

.method public setPackagePermission(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 9
    .parameter "permName"
    .parameter "pkgName"
    .parameter "enable"

    #@0
    .prologue
    .line 601
    const/4 v1, 0x0

    #@1
    .line 603
    .local v1, ret:Z
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_9

    #@7
    move v2, v1

    #@8
    .line 625
    .end local v1           #ret:Z
    .local v2, ret:I
    :goto_8
    return v2

    #@9
    .line 605
    .end local v2           #ret:I
    .restart local v1       #ret:Z
    :cond_9
    if-nez p2, :cond_d

    #@b
    .line 606
    const-string p2, ".*"

    #@d
    .line 609
    :cond_d
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@f
    monitor-enter v4

    #@10
    .line 610
    :try_start_10
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@12
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@18
    .line 611
    .local v0, re:Lcom/android/server/DeviceManager3LMService$RulesEngine;
    if-nez v0, :cond_24

    #@1a
    .line 612
    new-instance v0, Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@1c
    .end local v0           #re:Lcom/android/server/DeviceManager3LMService$RulesEngine;
    invoke-direct {v0, p0}, Lcom/android/server/DeviceManager3LMService$RulesEngine;-><init>(Lcom/android/server/DeviceManager3LMService;)V

    #@1f
    .line 613
    .restart local v0       #re:Lcom/android/server/DeviceManager3LMService$RulesEngine;
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mPackagePerms:Ljava/util/Map;

    #@21
    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 615
    :cond_24
    invoke-virtual {v0, p2, p3}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->updatePolicy(Ljava/lang/String;Z)Z

    #@27
    move-result v1

    #@28
    .line 616
    monitor-exit v4

    #@29
    move v2, v1

    #@2a
    .line 625
    .restart local v2       #ret:I
    goto :goto_8

    #@2b
    .line 616
    .end local v0           #re:Lcom/android/server/DeviceManager3LMService$RulesEngine;
    .end local v2           #ret:I
    :catchall_2b
    move-exception v3

    #@2c
    monitor-exit v4
    :try_end_2d
    .catchall {:try_start_10 .. :try_end_2d} :catchall_2b

    #@2d
    throw v3
.end method

.method public setPackageScanner(Ljava/lang/String;ZI)V
    .registers 6
    .parameter "scannerComponent"
    .parameter "failOnTimeout"
    .parameter "timeoutMillis"

    #@0
    .prologue
    .line 1885
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1892
    :goto_6
    return-void

    #@7
    .line 1887
    :cond_7
    iget-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScannerMutex:Ljava/lang/Object;

    #@9
    monitor-enter v1

    #@a
    .line 1888
    :try_start_a
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanner:Ljava/lang/String;

    #@c
    .line 1889
    iput-boolean p2, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanFailOnTimeout:Z

    #@e
    .line 1890
    iput p3, p0, Lcom/android/server/DeviceManager3LMService;->mPackageScanTimeoutMillis:I

    #@10
    .line 1891
    monitor-exit v1

    #@11
    goto :goto_6

    #@12
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_a .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public setPrimaryClipOwner(I)V
    .registers 10
    .parameter "uid"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1835
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v5

    #@5
    if-nez v5, :cond_8

    #@7
    .line 1849
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1837
    :cond_8
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@a
    invoke-virtual {v5, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    .line 1838
    .local v3, packages:[Ljava/lang/String;
    if-eqz v3, :cond_7

    #@10
    .line 1840
    iput-boolean v6, p0, Lcom/android/server/DeviceManager3LMService;->mSecurePrimaryClip:Z

    #@12
    .line 1841
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@14
    monitor-enter v6

    #@15
    .line 1842
    move-object v0, v3

    #@16
    .local v0, arr$:[Ljava/lang/String;
    :try_start_16
    array-length v2, v0

    #@17
    .local v2, len$:I
    const/4 v1, 0x0

    #@18
    .local v1, i$:I
    :goto_18
    if-ge v1, v2, :cond_30

    #@1a
    aget-object v4, v0, v1

    #@1c
    .line 1843
    .local v4, pkg:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@1e
    const/4 v7, 0x0

    #@1f
    invoke-virtual {v5, v4, v7}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->checkPolicy(Ljava/lang/String;Z)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_2d

    #@25
    .line 1844
    const/4 v5, 0x1

    #@26
    iput-boolean v5, p0, Lcom/android/server/DeviceManager3LMService;->mSecurePrimaryClip:Z

    #@28
    .line 1845
    monitor-exit v6

    #@29
    goto :goto_7

    #@2a
    .line 1848
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v4           #pkg:Ljava/lang/String;
    :catchall_2a
    move-exception v5

    #@2b
    monitor-exit v6
    :try_end_2c
    .catchall {:try_start_16 .. :try_end_2c} :catchall_2a

    #@2c
    throw v5

    #@2d
    .line 1842
    .restart local v1       #i$:I
    .restart local v2       #len$:I
    .restart local v4       #pkg:Ljava/lang/String;
    :cond_2d
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_18

    #@30
    .line 1848
    .end local v4           #pkg:Ljava/lang/String;
    :cond_30
    :try_start_30
    monitor-exit v6
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2a

    #@31
    goto :goto_7
.end method

.method public setSdEncryptionRequired(Z)V
    .registers 16
    .parameter "required"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1196
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "storage"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v11

    #@8
    check-cast v11, Landroid/os/storage/StorageManager;

    #@a
    .line 1197
    .local v11, sm:Landroid/os/storage/StorageManager;
    invoke-virtual {v11}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@d
    move-result-object v13

    #@e
    .line 1198
    .local v13, volumes:[Landroid/os/storage/StorageVolume;
    if-nez v13, :cond_11

    #@10
    .line 1221
    :cond_10
    return-void

    #@11
    .line 1201
    :cond_11
    move-object v7, v13

    #@12
    .local v7, arr$:[Landroid/os/storage/StorageVolume;
    array-length v9, v7

    #@13
    .local v9, len$:I
    const/4 v8, 0x0

    #@14
    .local v8, i$:I
    :goto_14
    if-ge v8, v9, :cond_10

    #@16
    aget-object v12, v7, v8

    #@18
    .line 1203
    .local v12, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_62

    #@1e
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_62

    #@24
    .line 1204
    new-instance v5, Landroid/content/Intent;

    #@26
    const-string v0, "android.intent.action.VIEW"

    #@28
    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2b
    .line 1205
    .local v5, intent:Landroid/content/Intent;
    const-string v0, "com.android.settings"

    #@2d
    const-string v1, "com.android.settings.MediaFormat"

    #@2f
    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@32
    .line 1207
    const-string v0, "storage_volume"

    #@34
    invoke-virtual {v5, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@37
    .line 1208
    const-string v0, "sd_encryption"

    #@39
    const/4 v1, 0x1

    #@3a
    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3d
    .line 1212
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@3f
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@42
    move-result-object v10

    #@43
    .line 1213
    .local v10, res:Landroid/content/res/Resources;
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->hashCode()I

    #@46
    move-result v1

    #@47
    const v0, 0x104001a

    #@4a
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    const v0, 0x104001a

    #@51
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    const v0, 0x104001b

    #@58
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5b
    move-result-object v4

    #@5c
    const/16 v6, 0x30

    #@5e
    move-object v0, p0

    #@5f
    invoke-direct/range {v0 .. v6}, Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    #@62
    .line 1201
    .end local v5           #intent:Landroid/content/Intent;
    .end local v10           #res:Landroid/content/res/Resources;
    :cond_62
    add-int/lit8 v8, v8, 0x1

    #@64
    goto :goto_14
.end method

.method public setSecureClipboard(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1815
    .local p1, packageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_7

    #@6
    .line 1826
    :goto_6
    return-void

    #@7
    .line 1817
    :cond_7
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@9
    monitor-enter v4

    #@a
    .line 1820
    :try_start_a
    new-instance v1, Ljava/util/HashMap;

    #@c
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@f
    .line 1821
    .local v1, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v0

    #@13
    .local v0, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_2c

    #@19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Ljava/lang/String;

    #@1f
    .line 1822
    .local v2, packageName:Ljava/lang/String;
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@21
    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    goto :goto_13

    #@29
    .line 1825
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2           #packageName:Ljava/lang/String;
    :catchall_29
    move-exception v3

    #@2a
    monitor-exit v4
    :try_end_2b
    .catchall {:try_start_a .. :try_end_2b} :catchall_29

    #@2b
    throw v3

    #@2c
    .line 1824
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v1       #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2c
    :try_start_2c
    iget-object v3, p0, Lcom/android/server/DeviceManager3LMService;->mSecureClipboardRules:Lcom/android/server/DeviceManager3LMService$RulesEngine;

    #@2e
    invoke-virtual {v3, v1}, Lcom/android/server/DeviceManager3LMService$RulesEngine;->loadPolicy(Ljava/util/Map;)Z

    #@31
    .line 1825
    monitor-exit v4
    :try_end_32
    .catchall {:try_start_2c .. :try_end_32} :catchall_29

    #@32
    goto :goto_6
.end method

.method public setSsidFilter(Ljava/util/List;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1689
    .local p1, filter:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v4

    #@4
    if-nez v4, :cond_7

    #@6
    .line 1707
    :cond_6
    return-void

    #@7
    .line 1691
    :cond_7
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mWifiFilter:Ljava/util/Set;

    #@9
    invoke-interface {v4}, Ljava/util/Set;->clear()V

    #@c
    .line 1692
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mWifiFilter:Ljava/util/Set;

    #@e
    invoke-interface {v4, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    #@11
    .line 1695
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mWifiFilter:Ljava/util/Set;

    #@13
    invoke-interface {v4}, Ljava/util/Set;->size()I

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_6

    #@19
    .line 1698
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@1b
    const-string v5, "wifi"

    #@1d
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Landroid/net/wifi/WifiManager;

    #@23
    .line 1699
    .local v3, wm:Landroid/net/wifi/WifiManager;
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    #@26
    move-result-object v1

    #@27
    .line 1700
    .local v1, configList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    if-eqz v1, :cond_6

    #@29
    .line 1701
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2c
    move-result-object v2

    #@2d
    .local v2, i$:Ljava/util/Iterator;
    :cond_2d
    :goto_2d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_6

    #@33
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36
    move-result-object v0

    #@37
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@39
    .line 1702
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@3b
    invoke-virtual {p0, v4}, Lcom/android/server/DeviceManager3LMService;->isSsidAllowed(Ljava/lang/String;)Z

    #@3e
    move-result v4

    #@3f
    if-nez v4, :cond_2d

    #@41
    .line 1704
    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@43
    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    #@46
    goto :goto_2d
.end method

.method public setWifiState(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 1560
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 1561
    const-string v0, "persist.security.wifi.lockout"

    #@8
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 1563
    :cond_f
    return-void
.end method

.method public setupVpnDns(Ljava/lang/String;)Z
    .registers 11
    .parameter "vpnSuffixes"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 447
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@4
    move-result v6

    #@5
    if-nez v6, :cond_9

    #@7
    const/4 v5, 0x0

    #@8
    .line 471
    :goto_8
    return v5

    #@9
    .line 449
    :cond_9
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService;->mVpnSettingsMutex:Ljava/lang/String;

    #@b
    monitor-enter v6

    #@c
    .line 450
    :try_start_c
    const-string v7, "net.dns1"

    #@e
    const-string v8, ""

    #@10
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 451
    .local v0, current_dns1:Ljava/lang/String;
    const-string v7, "net.dns2"

    #@16
    const-string v8, ""

    #@18
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 452
    .local v1, current_dns2:Ljava/lang/String;
    const-string v7, "net.dns.search"

    #@1e
    const-string v8, ""

    #@20
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    .line 453
    .local v2, current_suffixes:Ljava/lang/String;
    const-string v7, "vpn.net.tun.dns1"

    #@26
    const-string v8, ""

    #@28
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    .line 454
    .local v3, current_tun_dns1:Ljava/lang/String;
    const-string v7, "vpn.net.tun.dns2"

    #@2e
    const-string v8, ""

    #@30
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    .line 456
    .local v4, current_tun_dns2:Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v7

    #@38
    if-nez v7, :cond_41

    #@3a
    .line 457
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDnsSuffixes:Ljava/lang/String;

    #@3c
    .line 458
    const-string v7, "net.dns.search"

    #@3e
    invoke-static {v7, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    .line 461
    :cond_41
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v7

    #@45
    if-nez v7, :cond_55

    #@47
    .line 462
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns1:Ljava/lang/String;

    #@49
    .line 463
    iput-object v1, p0, Lcom/android/server/DeviceManager3LMService;->mVpnOriginalDns2:Ljava/lang/String;

    #@4b
    .line 464
    const-string v7, "net.dns1"

    #@4d
    invoke-static {v7, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 465
    const-string v7, "net.dns2"

    #@52
    invoke-static {v7, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 468
    :cond_55
    const/4 v7, 0x1

    #@56
    iput-boolean v7, p0, Lcom/android/server/DeviceManager3LMService;->mVpnHasOriginalData:Z

    #@58
    .line 469
    monitor-exit v6

    #@59
    goto :goto_8

    #@5a
    .end local v0           #current_dns1:Ljava/lang/String;
    .end local v1           #current_dns2:Ljava/lang/String;
    .end local v2           #current_suffixes:Ljava/lang/String;
    .end local v3           #current_tun_dns1:Ljava/lang/String;
    .end local v4           #current_tun_dns2:Ljava/lang/String;
    :catchall_5a
    move-exception v5

    #@5b
    monitor-exit v6
    :try_end_5c
    .catchall {:try_start_c .. :try_end_5c} :catchall_5a

    #@5c
    throw v5
.end method

.method public unlock()V
    .registers 2

    #@0
    .prologue
    .line 293
    invoke-direct {p0}, Lcom/android/server/DeviceManager3LMService;->isAccessPermitted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 296
    :goto_6
    return-void

    #@7
    .line 295
    :cond_7
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService;->mBootLocked:Z

    #@a
    goto :goto_6
.end method
