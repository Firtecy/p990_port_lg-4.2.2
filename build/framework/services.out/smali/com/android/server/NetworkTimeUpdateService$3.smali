.class Lcom/android/server/NetworkTimeUpdateService$3;
.super Landroid/content/BroadcastReceiver;
.source "NetworkTimeUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NetworkTimeUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/NetworkTimeUpdateService;


# direct methods
.method constructor <init>(Lcom/android/server/NetworkTimeUpdateService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 237
    iput-object p1, p0, Lcom/android/server/NetworkTimeUpdateService$3;->this$0:Lcom/android/server/NetworkTimeUpdateService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 241
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 242
    .local v0, action:Ljava/lang/String;
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    #@6
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_3f

    #@c
    .line 244
    const-string v3, "connectivity"

    #@e
    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/net/ConnectivityManager;

    #@14
    .line 246
    .local v1, connManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@17
    move-result-object v2

    #@18
    .line 247
    .local v2, netInfo:Landroid/net/NetworkInfo;
    if-eqz v2, :cond_3f

    #@1a
    .line 249
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@1d
    move-result-object v3

    #@1e
    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@20
    if-ne v3, v4, :cond_3f

    #@22
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    #@25
    move-result v3

    #@26
    const/4 v4, 0x1

    #@27
    if-eq v3, v4, :cond_31

    #@29
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    #@2c
    move-result v3

    #@2d
    const/16 v4, 0x9

    #@2f
    if-ne v3, v4, :cond_3f

    #@31
    .line 252
    :cond_31
    iget-object v3, p0, Lcom/android/server/NetworkTimeUpdateService$3;->this$0:Lcom/android/server/NetworkTimeUpdateService;

    #@33
    invoke-static {v3}, Lcom/android/server/NetworkTimeUpdateService;->access$000(Lcom/android/server/NetworkTimeUpdateService;)Landroid/os/Handler;

    #@36
    move-result-object v3

    #@37
    const/4 v4, 0x3

    #@38
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@3f
    .line 256
    .end local v1           #connManager:Landroid/net/ConnectivityManager;
    .end local v2           #netInfo:Landroid/net/NetworkInfo;
    :cond_3f
    return-void
.end method
