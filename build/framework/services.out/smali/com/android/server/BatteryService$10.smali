.class Lcom/android/server/BatteryService$10;
.super Landroid/os/UEventObserver;
.source "BatteryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BatteryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BatteryService;


# direct methods
.method constructor <init>(Lcom/android/server/BatteryService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 816
    iput-object p1, p0, Lcom/android/server/BatteryService$10;->this$0:Lcom/android/server/BatteryService;

    #@2
    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 819
    const-string v1, "1"

    #@2
    const-string v2, "SWITCH_STATE"

    #@4
    invoke-virtual {p1, v2}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_2a

    #@e
    const/4 v0, 0x1

    #@f
    .line 820
    .local v0, batteryRemoved:I
    :goto_f
    iget-object v1, p0, Lcom/android/server/BatteryService$10;->this$0:Lcom/android/server/BatteryService;

    #@11
    invoke-static {v1}, Lcom/android/server/BatteryService;->access$200(Lcom/android/server/BatteryService;)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    monitor-enter v2

    #@16
    .line 821
    :try_start_16
    iget-object v1, p0, Lcom/android/server/BatteryService$10;->this$0:Lcom/android/server/BatteryService;

    #@18
    invoke-static {v1}, Lcom/android/server/BatteryService;->access$500(Lcom/android/server/BatteryService;)I

    #@1b
    move-result v1

    #@1c
    if-eq v1, v0, :cond_28

    #@1e
    .line 822
    iget-object v1, p0, Lcom/android/server/BatteryService$10;->this$0:Lcom/android/server/BatteryService;

    #@20
    invoke-static {v1, v0}, Lcom/android/server/BatteryService;->access$502(Lcom/android/server/BatteryService;I)I

    #@23
    .line 823
    iget-object v1, p0, Lcom/android/server/BatteryService$10;->this$0:Lcom/android/server/BatteryService;

    #@25
    invoke-static {v1}, Lcom/android/server/BatteryService;->access$300(Lcom/android/server/BatteryService;)V

    #@28
    .line 825
    :cond_28
    monitor-exit v2

    #@29
    .line 826
    return-void

    #@2a
    .line 819
    .end local v0           #batteryRemoved:I
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_f

    #@2c
    .line 825
    .restart local v0       #batteryRemoved:I
    :catchall_2c
    move-exception v1

    #@2d
    monitor-exit v2
    :try_end_2e
    .catchall {:try_start_16 .. :try_end_2e} :catchall_2c

    #@2e
    throw v1
.end method
