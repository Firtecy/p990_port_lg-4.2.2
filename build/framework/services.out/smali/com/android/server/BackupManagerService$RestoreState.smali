.class final enum Lcom/android/server/BackupManagerService$RestoreState;
.super Ljava/lang/Enum;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RestoreState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/BackupManagerService$RestoreState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/BackupManagerService$RestoreState;

.field public static final enum DOWNLOAD_DATA:Lcom/android/server/BackupManagerService$RestoreState;

.field public static final enum FINAL:Lcom/android/server/BackupManagerService$RestoreState;

.field public static final enum INITIAL:Lcom/android/server/BackupManagerService$RestoreState;

.field public static final enum PM_METADATA:Lcom/android/server/BackupManagerService$RestoreState;

.field public static final enum RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 4136
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreState;

    #@7
    const-string v1, "INITIAL"

    #@9
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService$RestoreState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/server/BackupManagerService$RestoreState;->INITIAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@e
    .line 4137
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreState;

    #@10
    const-string v1, "DOWNLOAD_DATA"

    #@12
    invoke-direct {v0, v1, v3}, Lcom/android/server/BackupManagerService$RestoreState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/server/BackupManagerService$RestoreState;->DOWNLOAD_DATA:Lcom/android/server/BackupManagerService$RestoreState;

    #@17
    .line 4138
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreState;

    #@19
    const-string v1, "PM_METADATA"

    #@1b
    invoke-direct {v0, v1, v4}, Lcom/android/server/BackupManagerService$RestoreState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/server/BackupManagerService$RestoreState;->PM_METADATA:Lcom/android/server/BackupManagerService$RestoreState;

    #@20
    .line 4139
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreState;

    #@22
    const-string v1, "RUNNING_QUEUE"

    #@24
    invoke-direct {v0, v1, v5}, Lcom/android/server/BackupManagerService$RestoreState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@29
    .line 4140
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreState;

    #@2b
    const-string v1, "FINAL"

    #@2d
    invoke-direct {v0, v1, v6}, Lcom/android/server/BackupManagerService$RestoreState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@32
    .line 4135
    const/4 v0, 0x5

    #@33
    new-array v0, v0, [Lcom/android/server/BackupManagerService$RestoreState;

    #@35
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->INITIAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@37
    aput-object v1, v0, v2

    #@39
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->DOWNLOAD_DATA:Lcom/android/server/BackupManagerService$RestoreState;

    #@3b
    aput-object v1, v0, v3

    #@3d
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->PM_METADATA:Lcom/android/server/BackupManagerService$RestoreState;

    #@3f
    aput-object v1, v0, v4

    #@41
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@43
    aput-object v1, v0, v5

    #@45
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@47
    aput-object v1, v0, v6

    #@49
    sput-object v0, Lcom/android/server/BackupManagerService$RestoreState;->$VALUES:[Lcom/android/server/BackupManagerService$RestoreState;

    #@4b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 4135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/BackupManagerService$RestoreState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 4135
    const-class v0, Lcom/android/server/BackupManagerService$RestoreState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/BackupManagerService$RestoreState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/server/BackupManagerService$RestoreState;
    .registers 1

    #@0
    .prologue
    .line 4135
    sget-object v0, Lcom/android/server/BackupManagerService$RestoreState;->$VALUES:[Lcom/android/server/BackupManagerService$RestoreState;

    #@2
    invoke-virtual {v0}, [Lcom/android/server/BackupManagerService$RestoreState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/server/BackupManagerService$RestoreState;

    #@8
    return-object v0
.end method
