.class Lcom/android/server/MountService$MountServiceHandler;
.super Landroid/os/Handler;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MountServiceHandler"
.end annotation


# instance fields
.field mForceUnmounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/MountService$UnmountCallBack;",
            ">;"
        }
    .end annotation
.end field

.field mUpdatingStatus:Z

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "l"

    #@0
    .prologue
    .line 399
    iput-object p1, p0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@2
    .line 400
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 396
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/MountService$MountServiceHandler;->mForceUnmounts:Ljava/util/ArrayList;

    #@c
    .line 397
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/android/server/MountService$MountServiceHandler;->mUpdatingStatus:Z

    #@f
    .line 401
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 19
    .parameter "msg"

    #@0
    .prologue
    .line 405
    move-object/from16 v0, p1

    #@2
    iget v12, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v12, :pswitch_data_110

    #@7
    .line 487
    :cond_7
    :goto_7
    return-void

    #@8
    .line 408
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v11, Lcom/android/server/MountService$UnmountCallBack;

    #@e
    .line 409
    .local v11, ucb:Lcom/android/server/MountService$UnmountCallBack;
    move-object/from16 v0, p0

    #@10
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->mForceUnmounts:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 412
    move-object/from16 v0, p0

    #@17
    iget-boolean v12, v0, Lcom/android/server/MountService$MountServiceHandler;->mUpdatingStatus:Z

    #@19
    if-nez v12, :cond_7

    #@1b
    .line 414
    const/4 v12, 0x1

    #@1c
    move-object/from16 v0, p0

    #@1e
    iput-boolean v12, v0, Lcom/android/server/MountService$MountServiceHandler;->mUpdatingStatus:Z

    #@20
    .line 415
    move-object/from16 v0, p0

    #@22
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@24
    invoke-static {v12}, Lcom/android/server/MountService;->access$300(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;

    #@27
    move-result-object v12

    #@28
    const/4 v13, 0x0

    #@29
    const/4 v14, 0x1

    #@2a
    invoke-virtual {v12, v13, v14}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    #@2d
    goto :goto_7

    #@2e
    .line 422
    .end local v11           #ucb:Lcom/android/server/MountService$UnmountCallBack;
    :pswitch_2e
    const/4 v12, 0x0

    #@2f
    move-object/from16 v0, p0

    #@31
    iput-boolean v12, v0, Lcom/android/server/MountService$MountServiceHandler;->mUpdatingStatus:Z

    #@33
    .line 423
    move-object/from16 v0, p0

    #@35
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->mForceUnmounts:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v7

    #@3b
    .line 424
    .local v7, size:I
    new-array v8, v7, [I

    #@3d
    .line 425
    .local v8, sizeArr:[I
    const/4 v9, 0x0

    #@3e
    .line 427
    .local v9, sizeArrN:I
    const-string v12, "activity"

    #@40
    invoke-static {v12}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@43
    move-result-object v1

    #@44
    check-cast v1, Lcom/android/server/am/ActivityManagerService;

    #@46
    .line 429
    .local v1, ams:Lcom/android/server/am/ActivityManagerService;
    const/4 v4, 0x0

    #@47
    .local v4, i:I
    move v10, v9

    #@48
    .end local v9           #sizeArrN:I
    .local v10, sizeArrN:I
    :goto_48
    if-ge v4, v7, :cond_e2

    #@4a
    .line 430
    move-object/from16 v0, p0

    #@4c
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->mForceUnmounts:Ljava/util/ArrayList;

    #@4e
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v11

    #@52
    check-cast v11, Lcom/android/server/MountService$UnmountCallBack;

    #@54
    .line 431
    .restart local v11       #ucb:Lcom/android/server/MountService$UnmountCallBack;
    iget-object v5, v11, Lcom/android/server/MountService$UnmountCallBack;->path:Ljava/lang/String;

    #@56
    .line 432
    .local v5, path:Ljava/lang/String;
    const/4 v2, 0x0

    #@57
    .line 433
    .local v2, done:Z
    iget-boolean v12, v11, Lcom/android/server/MountService$UnmountCallBack;->force:Z

    #@59
    if-nez v12, :cond_95

    #@5b
    .line 434
    const/4 v2, 0x1

    #@5c
    .line 449
    :cond_5c
    :goto_5c
    if-nez v2, :cond_b9

    #@5e
    iget v12, v11, Lcom/android/server/MountService$UnmountCallBack;->retries:I

    #@60
    const/4 v13, 0x4

    #@61
    if-ge v12, v13, :cond_b9

    #@63
    .line 451
    const-string v12, "MountService"

    #@65
    const-string v13, "Retrying to kill storage users again"

    #@67
    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 452
    move-object/from16 v0, p0

    #@6c
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@6e
    invoke-static {v12}, Lcom/android/server/MountService;->access$400(Lcom/android/server/MountService;)Landroid/os/Handler;

    #@71
    move-result-object v12

    #@72
    move-object/from16 v0, p0

    #@74
    iget-object v13, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@76
    invoke-static {v13}, Lcom/android/server/MountService;->access$400(Lcom/android/server/MountService;)Landroid/os/Handler;

    #@79
    move-result-object v13

    #@7a
    const/4 v14, 0x2

    #@7b
    iget v15, v11, Lcom/android/server/MountService$UnmountCallBack;->retries:I

    #@7d
    add-int/lit8 v16, v15, 0x1

    #@7f
    move/from16 v0, v16

    #@81
    iput v0, v11, Lcom/android/server/MountService$UnmountCallBack;->retries:I

    #@83
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@86
    move-result-object v15

    #@87
    invoke-virtual {v13, v14, v15}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@8a
    move-result-object v13

    #@8b
    const-wide/16 v14, 0x1e

    #@8d
    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@90
    move v9, v10

    #@91
    .line 429
    .end local v10           #sizeArrN:I
    .restart local v9       #sizeArrN:I
    :goto_91
    add-int/lit8 v4, v4, 0x1

    #@93
    move v10, v9

    #@94
    .end local v9           #sizeArrN:I
    .restart local v10       #sizeArrN:I
    goto :goto_48

    #@95
    .line 436
    :cond_95
    move-object/from16 v0, p0

    #@97
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@99
    invoke-virtual {v12, v5}, Lcom/android/server/MountService;->getStorageUsers(Ljava/lang/String;)[I

    #@9c
    move-result-object v6

    #@9d
    .line 437
    .local v6, pids:[I
    if-eqz v6, :cond_a2

    #@9f
    array-length v12, v6

    #@a0
    if-nez v12, :cond_a4

    #@a2
    .line 438
    :cond_a2
    const/4 v2, 0x1

    #@a3
    goto :goto_5c

    #@a4
    .line 441
    :cond_a4
    const-string v12, "unmount media"

    #@a6
    const/4 v13, 0x1

    #@a7
    invoke-virtual {v1, v6, v12, v13}, Lcom/android/server/am/ActivityManagerService;->killPids([ILjava/lang/String;Z)Z

    #@aa
    .line 443
    move-object/from16 v0, p0

    #@ac
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@ae
    invoke-virtual {v12, v5}, Lcom/android/server/MountService;->getStorageUsers(Ljava/lang/String;)[I

    #@b1
    move-result-object v6

    #@b2
    .line 444
    if-eqz v6, :cond_b7

    #@b4
    array-length v12, v6

    #@b5
    if-nez v12, :cond_5c

    #@b7
    .line 445
    :cond_b7
    const/4 v2, 0x1

    #@b8
    goto :goto_5c

    #@b9
    .line 457
    .end local v6           #pids:[I
    :cond_b9
    iget v12, v11, Lcom/android/server/MountService$UnmountCallBack;->retries:I

    #@bb
    const/4 v13, 0x4

    #@bc
    if-lt v12, v13, :cond_c5

    #@be
    .line 458
    const-string v12, "MountService"

    #@c0
    const-string v13, "Failed to unmount media inspite of 4 retries. Forcibly killing processes now"

    #@c2
    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 461
    :cond_c5
    add-int/lit8 v9, v10, 0x1

    #@c7
    .end local v10           #sizeArrN:I
    .restart local v9       #sizeArrN:I
    aput v4, v8, v10

    #@c9
    .line 462
    move-object/from16 v0, p0

    #@cb
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@cd
    invoke-static {v12}, Lcom/android/server/MountService;->access$400(Lcom/android/server/MountService;)Landroid/os/Handler;

    #@d0
    move-result-object v12

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget-object v13, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@d5
    invoke-static {v13}, Lcom/android/server/MountService;->access$400(Lcom/android/server/MountService;)Landroid/os/Handler;

    #@d8
    move-result-object v13

    #@d9
    const/4 v14, 0x3

    #@da
    invoke-virtual {v13, v14, v11}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@dd
    move-result-object v13

    #@de
    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@e1
    goto :goto_91

    #@e2
    .line 467
    .end local v2           #done:Z
    .end local v5           #path:Ljava/lang/String;
    .end local v9           #sizeArrN:I
    .end local v11           #ucb:Lcom/android/server/MountService$UnmountCallBack;
    .restart local v10       #sizeArrN:I
    :cond_e2
    add-int/lit8 v4, v10, -0x1

    #@e4
    :goto_e4
    if-ltz v4, :cond_7

    #@e6
    .line 468
    move-object/from16 v0, p0

    #@e8
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->mForceUnmounts:Ljava/util/ArrayList;

    #@ea
    aget v13, v8, v4

    #@ec
    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@ef
    .line 467
    add-int/lit8 v4, v4, -0x1

    #@f1
    goto :goto_e4

    #@f2
    .line 474
    .end local v1           #ams:Lcom/android/server/am/ActivityManagerService;
    .end local v4           #i:I
    .end local v7           #size:I
    .end local v8           #sizeArr:[I
    .end local v10           #sizeArrN:I
    :pswitch_f2
    move-object/from16 v0, p1

    #@f4
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f6
    check-cast v11, Lcom/android/server/MountService$UnmountCallBack;

    #@f8
    .line 475
    .restart local v11       #ucb:Lcom/android/server/MountService$UnmountCallBack;
    invoke-virtual {v11}, Lcom/android/server/MountService$UnmountCallBack;->handleFinished()V

    #@fb
    goto/16 :goto_7

    #@fd
    .line 480
    .end local v11           #ucb:Lcom/android/server/MountService$UnmountCallBack;
    :pswitch_fd
    :try_start_fd
    move-object/from16 v0, p0

    #@ff
    iget-object v12, v0, Lcom/android/server/MountService$MountServiceHandler;->this$0:Lcom/android/server/MountService;

    #@101
    invoke-static {v12}, Lcom/android/server/MountService;->access$500(Lcom/android/server/MountService;)V
    :try_end_104
    .catch Ljava/lang/Exception; {:try_start_fd .. :try_end_104} :catch_106

    #@104
    goto/16 :goto_7

    #@106
    .line 481
    :catch_106
    move-exception v3

    #@107
    .line 482
    .local v3, ex:Ljava/lang/Exception;
    const-string v12, "MountService"

    #@109
    const-string v13, "Boot-time mount exception"

    #@10b
    invoke-static {v12, v13, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10e
    goto/16 :goto_7

    #@110
    .line 405
    :pswitch_data_110
    .packed-switch 0x1
        :pswitch_8
        :pswitch_2e
        :pswitch_f2
        :pswitch_fd
    .end packed-switch
.end method
